package com.oxyloans.dto.enachscheduler;

public class TransactionPayment {

	private TransationInstruction instruction = new TransationInstruction();
	private TransactionInstrument instrument = new TransactionInstrument();

	public TransationInstruction getInstruction() {
		return instruction;
	}

	public void setInstruction(TransationInstruction instruction) {
		this.instruction = instruction;
	}

	public TransactionInstrument getInstrument() {
		return instrument;
	}

	public void setInstrument(TransactionInstrument instrument) {
		this.instrument = instrument;
	}

}
