package com.oxyloans.dto.enachscheduler;

public class TransactionVerificationTempMapDto {

	private Integer id;

	private Integer loanId;

	private Integer emiNumber;

	private Integer emiCardRefId;
	
	private Integer applicationId;

	public Integer getEmiCardRefId() {
		return emiCardRefId;
	}

	public void setEmiCardRefId(Integer emiCardRefId) {
		this.emiCardRefId = emiCardRefId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLoanId() {
		return loanId;
	}

	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}

	public Integer getEmiNumber() {
		return emiNumber;
	}

	public void setEmiNumber(Integer emiNumber) {
		this.emiNumber = emiNumber;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

}
