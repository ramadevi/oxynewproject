package com.oxyloans.dto.enachscheduler;

public class MandatePaymentTransaction {
	private String amount;
	private String balanceAmount;
	private String bankReferenceIdentifier;
	private String dateTime;
	private String errorMessage;
	private String identifier;
	private String refundIdentifier;
	private String statusCode;
	private String statusMessage;
	private String instruction;
	private String reference;
	private String accountNo;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(String balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public String getBankReferenceIdentifier() {
		return bankReferenceIdentifier;
	}

	public void setBankReferenceIdentifier(String bankReferenceIdentifier) {
		this.bankReferenceIdentifier = bankReferenceIdentifier;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getRefundIdentifier() {
		return refundIdentifier;
	}

	public void setRefundIdentifier(String refundIdentifier) {
		this.refundIdentifier = refundIdentifier;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

}
