package com.oxyloans.dto.enachscheduler;

import java.io.Serializable;

public class TransactionVerificationJsonRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7417410318550740658L;
	private EnachMerchant merchant = new EnachMerchant();
	private EnachPayment payment = new EnachPayment();
	private TransactionVerificationTransactionDto transaction = new TransactionVerificationTransactionDto();

	public EnachMerchant getMerchant() {
		return merchant;
	}

	public void setMerchant(EnachMerchant merchant) {
		this.merchant = merchant;
	}

	public EnachPayment getPayment() {
		return payment;
	}

	public void setPayment(EnachPayment payment) {
		this.payment = payment;
	}

	public TransactionVerificationTransactionDto getTransaction() {
		return transaction;
	}

	public void setTransaction(TransactionVerificationTransactionDto transaction) {
		this.transaction = transaction;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
