package com.oxyloans.dto.enachscheduler;

public class EnachMiniumWithDrwUserRequest {

	private String applicationId;
	private Integer borrowerId;
	private Integer tenure;
	private Double amount;
	private String emiDate;
	private Integer noOfRuns;
	private String createdBy;

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public Integer getTenure() {
		return tenure;
	}

	public void setTenure(Integer tenure) {
		this.tenure = tenure;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getEmiDate() {
		return emiDate;
	}

	public void setEmiDate(String emiDate) {
		this.emiDate = emiDate;
	}

	public Integer getNoOfRuns() {
		return noOfRuns;
	}

	public void setNoOfRuns(Integer noOfRuns) {
		this.noOfRuns = noOfRuns;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

}
