package com.oxyloans.dto.enachscheduler;

import java.io.Serializable;

public class TransactionSchedulingJsonRequest implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1701172651639067134L;
	private EnachMerchant merchant = new EnachMerchant();
	private TransactionPayment payment = new TransactionPayment();
	private TransactionSchedulingTransactionDto transaction = new TransactionSchedulingTransactionDto();

	public EnachMerchant getMerchant() {
		return merchant;
	}

	public void setMerchant(EnachMerchant merchant) {
		this.merchant = merchant;
	}

	public TransactionPayment getPayment() {
		return payment;
	}

	public void setPayment(TransactionPayment payment) {
		this.payment = payment;
	}

	public TransactionSchedulingTransactionDto getTransaction() {
		return transaction;
	}

	public void setTransaction(TransactionSchedulingTransactionDto transaction) {
		this.transaction = transaction;
	}

}
