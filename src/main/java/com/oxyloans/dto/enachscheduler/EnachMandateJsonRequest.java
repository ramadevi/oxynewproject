package com.oxyloans.dto.enachscheduler;

import java.io.Serializable;

public class EnachMandateJsonRequest implements Serializable{

	private EnachMerchant merchant = new EnachMerchant();
	private EnachPayment payment = new EnachPayment();
	private EnachTransaction transaction = new EnachTransaction();
	private Consumer consumer = new Consumer();

	public EnachMerchant getMerchant() {
		return merchant;
	}

	public void setMerchant(EnachMerchant merchant) {
		this.merchant = merchant;
	}

	public EnachPayment getPayment() {
		return payment;
	}

	public void setPayment(EnachPayment payment) {
		this.payment = payment;
	}

	public EnachTransaction getTransaction() {
		return transaction;
	}

	public void setTransaction(EnachTransaction transaction) {
		this.transaction = transaction;
	}

	public Consumer getConsumer() {
		return consumer;
	}

	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}

}
