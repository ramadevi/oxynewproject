package com.oxyloans.dto.enachscheduler;

public class EnachMandateVerificationResponse {

	private String merchantCode;
	private String merchantTransactionIdentifier;
	private String merchantTransactionRequestType;
	private String responseType;
	private String transactionState;
	private String merchantAdditionalDetails;
	private String error;
	private MandatePaymentMethod paymentMethod = new MandatePaymentMethod();

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getMerchantTransactionIdentifier() {
		return merchantTransactionIdentifier;
	}

	public void setMerchantTransactionIdentifier(String merchantTransactionIdentifier) {
		this.merchantTransactionIdentifier = merchantTransactionIdentifier;
	}

	public String getMerchantTransactionRequestType() {
		return merchantTransactionRequestType;
	}

	public void setMerchantTransactionRequestType(String merchantTransactionRequestType) {
		this.merchantTransactionRequestType = merchantTransactionRequestType;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getTransactionState() {
		return transactionState;
	}

	public void setTransactionState(String transactionState) {
		this.transactionState = transactionState;
	}

	public String getMerchantAdditionalDetails() {
		return merchantAdditionalDetails;
	}

	public void setMerchantAdditionalDetails(String merchantAdditionalDetails) {
		this.merchantAdditionalDetails = merchantAdditionalDetails;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public MandatePaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(MandatePaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

}
