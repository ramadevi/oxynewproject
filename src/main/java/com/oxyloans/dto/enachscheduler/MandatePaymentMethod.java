package com.oxyloans.dto.enachscheduler;

public class MandatePaymentMethod {
	private String token;
	private String instrumentAliasName;
	private String instrumentToken;
	private String bankSelectionCode;
	private String aCS;
	private String oTP;
	private String authentication;
	private MandatePaymentTransaction paymentTransaction = new MandatePaymentTransaction();
	private MandateVerificationError error = new MandateVerificationError();

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getInstrumentAliasName() {
		return instrumentAliasName;
	}

	public void setInstrumentAliasName(String instrumentAliasName) {
		this.instrumentAliasName = instrumentAliasName;
	}

	public String getInstrumentToken() {
		return instrumentToken;
	}

	public void setInstrumentToken(String instrumentToken) {
		this.instrumentToken = instrumentToken;
	}

	public String getBankSelectionCode() {
		return bankSelectionCode;
	}

	public void setBankSelectionCode(String bankSelectionCode) {
		this.bankSelectionCode = bankSelectionCode;
	}

	public String getaCS() {
		return aCS;
	}

	public void setaCS(String aCS) {
		this.aCS = aCS;
	}

	public String getoTP() {
		return oTP;
	}

	public void setoTP(String oTP) {
		this.oTP = oTP;
	}

	public String getAuthentication() {
		return authentication;
	}

	public void setAuthentication(String authentication) {
		this.authentication = authentication;
	}

	public MandatePaymentTransaction getPaymentTransaction() {
		return paymentTransaction;
	}

	public void setPaymentTransaction(MandatePaymentTransaction paymentTransaction) {
		this.paymentTransaction = paymentTransaction;
	}

	public MandateVerificationError getError() {
		return error;
	}

	public void setError(MandateVerificationError error) {
		this.error = error;
	}

}
