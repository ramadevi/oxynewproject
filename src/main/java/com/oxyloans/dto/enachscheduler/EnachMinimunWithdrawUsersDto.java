package com.oxyloans.dto.enachscheduler;

import com.oxyloans.response.user.UserResponse;

public class EnachMinimunWithdrawUsersDto {

	private String applicationId;
	private Integer borrowerId;
	private Integer tenure;
	private Double amount;
	private String emiDate;
	private Integer noOfRuns;
	private String createdOn;
	private String createdBy;

	private UserResponse borrowerUser;

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public Integer getTenure() {
		return tenure;
	}

	public void setTenure(Integer tenure) {
		this.tenure = tenure;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getEmiDate() {
		return emiDate;
	}

	public void setEmiDate(String emiDate) {
		this.emiDate = emiDate;
	}

	public Integer getNoOfRuns() {
		return noOfRuns;
	}

	public void setNoOfRuns(Integer noOfRuns) {
		this.noOfRuns = noOfRuns;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public UserResponse getBorrowerUser() {
		return borrowerUser;
	}

	public void setBorrowerUser(UserResponse borrowerUser) {
		this.borrowerUser = borrowerUser;
	}

}
