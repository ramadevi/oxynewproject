package com.oxyloans.dto.enachscheduler;

public class EnachTypeRequestDto {

	private String enachType;
	
	private String loanId;	
	

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getEnachType() {
		return enachType;
	}

	public void setEnachType(String enachType) {
		this.enachType = enachType;
	}

}
