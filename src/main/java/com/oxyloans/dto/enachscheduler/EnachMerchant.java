package com.oxyloans.dto.enachscheduler;

import java.io.Serializable;

public class EnachMerchant implements Serializable{

	private String identifier;

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

}
