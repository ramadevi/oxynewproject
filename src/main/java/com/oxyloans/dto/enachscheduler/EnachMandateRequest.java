package com.oxyloans.dto.enachscheduler;

public class EnachMandateRequest {

	private String consumerIdentifier;
	private String transactionIdentifier;
	private String dateTime;
	
	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getConsumerIdentifier() {
		return consumerIdentifier;
	}

	public void setConsumerIdentifier(String consumerIdentifier) {
		this.consumerIdentifier = consumerIdentifier;
	}

	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

}
