package com.oxyloans.dto.partner;

import java.math.BigInteger;

public class LoanResponseStatusUpdationRequestDto {

	private String status;

	private BigInteger totalLendersWalletAmount;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigInteger getTotalLendersWalletAmount() {
		return totalLendersWalletAmount;
	}

	public void setTotalLendersWalletAmount(BigInteger totalLendersWalletAmount) {
		this.totalLendersWalletAmount = totalLendersWalletAmount;
	}

}
