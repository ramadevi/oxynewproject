package com.oxyloans.dto.partner;

public class PartnerRegistrationResponseDto {

	private Integer userId;

	private String virtualAccountNumber;

	private String accountName;

	private String ifscCode;

	private String bankName;

	private String accountType;

	private String status;

	public Integer getUserId() {
		return userId;
	}

	public String getVirtualAccountNumber() {
		return virtualAccountNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public String getBankName() {
		return bankName;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setVirtualAccountNumber(String virtualAccountNumber) {
		this.virtualAccountNumber = virtualAccountNumber;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
