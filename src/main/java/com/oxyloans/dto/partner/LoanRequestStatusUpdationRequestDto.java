package com.oxyloans.dto.partner;

public class LoanRequestStatusUpdationRequestDto {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
