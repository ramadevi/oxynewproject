package com.oxyloans.dto.partner;

public class PartnerRegistrationRequestDto {

	private String name;

	private String mobileNumber;

	private String email;

	private String panNumber;

	private String pinCode;

	private String fatherName;

	private String whatAppNumber;

	private String citizenship;

	private String primaryType;

	private String dob;

	private String locality;

	private String address;

	private String accountNumber;

	private String ifscCode;

	private String city;

	private String state;

	private String employmentType;

	private String totalExperiance;

	private String companyName;

	private Integer netMonthlyIncome;

	private String additionalFiledsToUser;

	public String getName() {
		return name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public String getPinCode() {
		return pinCode;
	}

	public String getFatherName() {
		return fatherName;
	}

	public String getWhatAppNumber() {
		return whatAppNumber;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public String getPrimaryType() {
		return primaryType;
	}

	public String getDob() {
		return dob;
	}

	public String getLocality() {
		return locality;
	}

	public String getAddress() {
		return address;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public void setWhatAppNumber(String whatAppNumber) {
		this.whatAppNumber = whatAppNumber;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public void setPrimaryType(String primaryType) {
		this.primaryType = primaryType;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getEmploymentType() {
		return employmentType;
	}

	public String getTotalExperiance() {
		return totalExperiance;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	public void setTotalExperiance(String totalExperiance) {
		this.totalExperiance = totalExperiance;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getNetMonthlyIncome() {
		return netMonthlyIncome;
	}

	public void setNetMonthlyIncome(Integer netMonthlyIncome) {
		this.netMonthlyIncome = netMonthlyIncome;
	}

	public String getAdditionalFiledsToUser() {
		return additionalFiledsToUser;
	}

	public void setAdditionalFiledsToUser(String additionalFiledsToUser) {
		this.additionalFiledsToUser = additionalFiledsToUser;
	}

}
