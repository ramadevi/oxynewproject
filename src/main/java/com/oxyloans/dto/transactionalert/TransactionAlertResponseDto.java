package com.oxyloans.dto.transactionalert;

public class TransactionAlertResponseDto {

	private String success;

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

}
