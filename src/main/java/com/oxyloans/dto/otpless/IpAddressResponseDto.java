package com.oxyloans.dto.otpless;

public class IpAddressResponseDto {

	private String ipAddressResponse;

	private String ipAddress;

	public String getIpAddressResponse() {
		return ipAddressResponse;
	}

	public void setIpAddressResponse(String ipAddressResponse) {
		this.ipAddressResponse = ipAddressResponse;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

}
