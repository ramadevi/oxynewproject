package com.oxyloans.dto.otpless;

public class OtplessRequestDto {

	private String token;

	private String timestamp;

	private String timezone;

	private MobileRequestDto mobile;

	private EmailRequestDto email;

	private String wanumber;

	private String waname;

	private String ipAddress;

	private Double latitude;

	private Double longitude;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public MobileRequestDto getMobile() {
		return mobile;
	}

	public void setMobile(MobileRequestDto mobile) {
		this.mobile = mobile;
	}

	public EmailRequestDto getEmail() {
		return email;
	}

	public void setEmail(EmailRequestDto email) {
		this.email = email;
	}

	public String getWanumber() {
		return wanumber;
	}

	public void setWanumber(String wanumber) {
		this.wanumber = wanumber;
	}

	public String getWaname() {
		return waname;
	}

	public void setWaname(String waname) {
		this.waname = waname;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

}
