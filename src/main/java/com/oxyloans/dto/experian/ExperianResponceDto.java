package com.oxyloans.dto.experian;

public class ExperianResponceDto {
private Integer score;

private Integer creditAccountTotal;

private Integer creditAccountActive;

private Integer creditAccountClosed;

private Integer outstandingBalanceAll;

private Integer outstandingBalanceSecured;

private Integer outstandingBalanceUnSecured;

public Integer getScore() {
	return score;
}

public void setScore(Integer score) {
	this.score = score;
}

public Integer getCreditAccountTotal() {
	return creditAccountTotal;
}

public void setCreditAccountTotal(Integer creditAccountTotal) {
	this.creditAccountTotal = creditAccountTotal;
}

public Integer getCreditAccountActive() {
	return creditAccountActive;
}

public void setCreditAccountActive(Integer creditAccountActive) {
	this.creditAccountActive = creditAccountActive;
}

public Integer getCreditAccountClosed() {
	return creditAccountClosed;
}

public void setCreditAccountClosed(Integer creditAccountClosed) {
	this.creditAccountClosed = creditAccountClosed;
}

public Integer getOutstandingBalanceAll() {
	return outstandingBalanceAll;
}

public void setOutstandingBalanceAll(Integer outstandingBalanceAll) {
	this.outstandingBalanceAll = outstandingBalanceAll;
}

public Integer getOutstandingBalanceSecured() {
	return outstandingBalanceSecured;
}

public void setOutstandingBalanceSecured(Integer outstandingBalanceSecured) {
	this.outstandingBalanceSecured = outstandingBalanceSecured;
}

public Integer getOutstandingBalanceUnSecured() {
	return outstandingBalanceUnSecured;
}

public void setOutstandingBalanceUnSecured(Integer outstandingBalanceUnSecured) {
	this.outstandingBalanceUnSecured = outstandingBalanceUnSecured;
}

}
