package com.oxyloans.dto.usertypedto;

public class OxyUserTypeRequestDto {

	private Integer id;

	private String utmName;

	private String userType;

	private String repaymentMethod;

	private Double loanAmountCalculation;

	private Integer duration;

	private Double firstCibilScore;

	private Double secondCibilScore;

	private Double thirdCibilScore;

	private Double fourthCibilScore;

	private Double fifthCibilScore;

	private String type;

	private String partnerEmail;

	private String partnerMobileNumber;

	private String partnerName;

	private String pocEmail;

	private String pocMobileNumber;

	private String pocNamne;

	private String mobileOtpValue;

	private String emailOtpValue;

	private String mobileOtpSession;

	private String userName;

	private String accountNumber;

	private String ifscCode;

	private String bankName;

	private String branchName;

	private Double lenderProcessingFee;

	private Double borrowerProcessingFee;
	
	private String referralCheck;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUtmName() {
		return utmName;
	}

	public void setUtmName(String utmName) {
		this.utmName = utmName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getRepaymentMethod() {
		return repaymentMethod;
	}

	public void setRepaymentMethod(String repaymentMethod) {
		this.repaymentMethod = repaymentMethod;
	}

	public Double getLoanAmountCalculation() {
		return loanAmountCalculation;
	}

	public void setLoanAmountCalculation(Double loanAmountCalculation) {
		this.loanAmountCalculation = loanAmountCalculation;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Double getFirstCibilScore() {
		return firstCibilScore;
	}

	public void setFirstCibilScore(Double firstCibilScore) {
		this.firstCibilScore = firstCibilScore;
	}

	public Double getSecondCibilScore() {
		return secondCibilScore;
	}

	public void setSecondCibilScore(Double secondCibilScore) {
		this.secondCibilScore = secondCibilScore;
	}

	public Double getThirdCibilScore() {
		return thirdCibilScore;
	}

	public void setThirdCibilScore(Double thirdCibilScore) {
		this.thirdCibilScore = thirdCibilScore;
	}

	public Double getFourthCibilScore() {
		return fourthCibilScore;
	}

	public void setFourthCibilScore(Double fourthCibilScore) {
		this.fourthCibilScore = fourthCibilScore;
	}

	public Double getFifthCibilScore() {
		return fifthCibilScore;
	}

	public void setFifthCibilScore(Double fifthCibilScore) {
		this.fifthCibilScore = fifthCibilScore;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPartnerEmail() {
		return partnerEmail;
	}

	public void setPartnerEmail(String partnerEmail) {
		this.partnerEmail = partnerEmail;
	}

	public String getPartnerMobileNumber() {
		return partnerMobileNumber;
	}

	public void setPartnerMobileNumber(String partnerMobileNumber) {
		this.partnerMobileNumber = partnerMobileNumber;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPocEmail() {
		return pocEmail;
	}

	public void setPocEmail(String pocEmail) {
		this.pocEmail = pocEmail;
	}

	public String getPocMobileNumber() {
		return pocMobileNumber;
	}

	public void setPocMobileNumber(String pocMobileNumber) {
		this.pocMobileNumber = pocMobileNumber;
	}

	public String getPocNamne() {
		return pocNamne;
	}

	public void setPocNamne(String pocNamne) {
		this.pocNamne = pocNamne;
	}

	public String getMobileOtpValue() {
		return mobileOtpValue;
	}

	public void setMobileOtpValue(String mobileOtpValue) {
		this.mobileOtpValue = mobileOtpValue;
	}

	public String getEmailOtpValue() {
		return emailOtpValue;
	}

	public void setEmailOtpValue(String emailOtpValue) {
		this.emailOtpValue = emailOtpValue;
	}

	public String getMobileOtpSession() {
		return mobileOtpSession;
	}

	public void setMobileOtpSession(String mobileOtpSession) {
		this.mobileOtpSession = mobileOtpSession;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Double getLenderProcessingFee() {
		return lenderProcessingFee;
	}

	public void setLenderProcessingFee(Double lenderProcessingFee) {
		this.lenderProcessingFee = lenderProcessingFee;
	}

	public Double getBorrowerProcessingFee() {
		return borrowerProcessingFee;
	}

	public void setBorrowerProcessingFee(Double borrowerProcessingFee) {
		this.borrowerProcessingFee = borrowerProcessingFee;
	}

	public String getReferralCheck() {
		return referralCheck;
	}

	public void setReferralCheck(String referralCheck) {
		this.referralCheck = referralCheck;
	}

}
