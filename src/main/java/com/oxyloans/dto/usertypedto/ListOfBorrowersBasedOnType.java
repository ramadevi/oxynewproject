package com.oxyloans.dto.usertypedto;

import java.util.List;

public class ListOfBorrowersBasedOnType {

	private List<OxyUserTypeResponseDto> oxyUserTypeResponseDto;

	private Integer countValue;

	public List<OxyUserTypeResponseDto> getOxyUserTypeResponseDto() {
		return oxyUserTypeResponseDto;
	}

	public void setOxyUserTypeResponseDto(List<OxyUserTypeResponseDto> oxyUserTypeResponseDto) {
		this.oxyUserTypeResponseDto = oxyUserTypeResponseDto;
	}

	public Integer getCountValue() {
		return countValue;
	}

	public void setCountValue(Integer countValue) {
		this.countValue = countValue;
	}
}
