package com.oxyloans.dto.usertypedto;

public class OxyUserTypeResponseDto {

	private String status;

	private String utmName;

	private String userType;

	private Integer id;

	private String repaymentMethod;

	private Double loanAmountCalculation;

	private Integer duration;

	private Double firstCibilScore;

	private Double secondCibilScore;

	private Double thirdCibilScore;

	private Double fourthCibilScore;

	private Double fifthCibilScore;

	private String type;

	private String partnerEmail;

	private String partnerMobileNumber;

	private String partnerName;

	private String pocEmail;

	private String pocMobileNumber;

	private String pocNamne;

	private boolean isPartnerUploadedNDA;

	private boolean isPartnerUploadedMOU;

	private String additionalFieldsToUser;

	private double lenderProcessingFee;

	private double borrowerProcessingFee;
	
	private String referralCheckStatus;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUtmName() {
		return utmName;
	}

	public void setUtmName(String utmName) {
		this.utmName = utmName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRepaymentMethod() {
		return repaymentMethod;
	}

	public void setRepaymentMethod(String repaymentMethod) {
		this.repaymentMethod = repaymentMethod;
	}

	public Double getLoanAmountCalculation() {
		return loanAmountCalculation;
	}

	public void setLoanAmountCalculation(Double loanAmountCalculation) {
		this.loanAmountCalculation = loanAmountCalculation;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Double getFirstCibilScore() {
		return firstCibilScore;
	}

	public void setFirstCibilScore(Double firstCibilScore) {
		this.firstCibilScore = firstCibilScore;
	}

	public Double getSecondCibilScore() {
		return secondCibilScore;
	}

	public void setSecondCibilScore(Double secondCibilScore) {
		this.secondCibilScore = secondCibilScore;
	}

	public Double getThirdCibilScore() {
		return thirdCibilScore;
	}

	public void setThirdCibilScore(Double thirdCibilScore) {
		this.thirdCibilScore = thirdCibilScore;
	}

	public Double getFourthCibilScore() {
		return fourthCibilScore;
	}

	public void setFourthCibilScore(Double fourthCibilScore) {
		this.fourthCibilScore = fourthCibilScore;
	}

	public Double getFifthCibilScore() {
		return fifthCibilScore;
	}

	public void setFifthCibilScore(Double fifthCibilScore) {
		this.fifthCibilScore = fifthCibilScore;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPartnerEmail() {
		return partnerEmail;
	}

	public void setPartnerEmail(String partnerEmail) {
		this.partnerEmail = partnerEmail;
	}

	public String getPartnerMobileNumber() {
		return partnerMobileNumber;
	}

	public void setPartnerMobileNumber(String partnerMobileNumber) {
		this.partnerMobileNumber = partnerMobileNumber;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPocEmail() {
		return pocEmail;
	}

	public void setPocEmail(String pocEmail) {
		this.pocEmail = pocEmail;
	}

	public String getPocMobileNumber() {
		return pocMobileNumber;
	}

	public void setPocMobileNumber(String pocMobileNumber) {
		this.pocMobileNumber = pocMobileNumber;
	}

	public String getPocNamne() {
		return pocNamne;
	}

	public void setPocNamne(String pocNamne) {
		this.pocNamne = pocNamne;
	}

	public boolean isPartnerUploadedNDA() {
		return isPartnerUploadedNDA;
	}

	public void setPartnerUploadedNDA(boolean isPartnerUploadedNDA) {
		this.isPartnerUploadedNDA = isPartnerUploadedNDA;
	}

	public boolean isPartnerUploadedMOU() {
		return isPartnerUploadedMOU;
	}

	public void setPartnerUploadedMOU(boolean isPartnerUploadedMOU) {
		this.isPartnerUploadedMOU = isPartnerUploadedMOU;
	}

	public String getAdditionalFieldsToUser() {
		return additionalFieldsToUser;
	}

	public void setAdditionalFieldsToUser(String additionalFieldsToUser) {
		this.additionalFieldsToUser = additionalFieldsToUser;
	}

	public double getLenderProcessingFee() {
		return lenderProcessingFee;
	}

	public void setLenderProcessingFee(double lenderProcessingFee) {
		this.lenderProcessingFee = lenderProcessingFee;
	}

	public double getBorrowerProcessingFee() {
		return borrowerProcessingFee;
	}

	public void setBorrowerProcessingFee(double borrowerProcessingFee) {
		this.borrowerProcessingFee = borrowerProcessingFee;
	}

	public String getReferralCheckStatus() {
		return referralCheckStatus;
	}

	public void setReferralCheckStatus(String referralCheckStatus) {
		this.referralCheckStatus = referralCheckStatus;
	}

}
