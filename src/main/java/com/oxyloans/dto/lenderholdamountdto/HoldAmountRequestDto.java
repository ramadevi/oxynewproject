package com.oxyloans.dto.lenderholdamountdto;

public class HoldAmountRequestDto {

	private int userId;

	private int dealId;

	private Double holdAmount;

	private String comments;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Double getHoldAmount() {
		return holdAmount;
	}

	public void setHoldAmount(Double holdAmount) {
		this.holdAmount = holdAmount;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

}
