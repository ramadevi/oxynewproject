package com.oxyloans.dto.lenderholdamountdto;

public class UserHoldAmountMappedToDealRequestDto {

	private Integer userId;

	private Integer dealId;

	private Double holdAmount;

	private String amountType;

	private String status;

	private String createdDate;

	private String closedDate;

	private String holdAmountCollectedDealName;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Double getHoldAmount() {
		return holdAmount;
	}

	public void setHoldAmount(Double holdAmount) {
		this.holdAmount = holdAmount;
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}

	public String getHoldAmountCollectedDealName() {
		return holdAmountCollectedDealName;
	}

	public void setHoldAmountCollectedDealName(String holdAmountCollectedDealName) {
		this.holdAmountCollectedDealName = holdAmountCollectedDealName;
	}

}
