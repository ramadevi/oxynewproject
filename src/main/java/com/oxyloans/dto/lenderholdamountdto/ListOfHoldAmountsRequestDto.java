package com.oxyloans.dto.lenderholdamountdto;

import java.util.List;

public class ListOfHoldAmountsRequestDto {

	List<UserHoldAmountMappedToDealRequestDto> userHoldAmountMappedToDealRequestDto;

	public List<UserHoldAmountMappedToDealRequestDto> getUserHoldAmountMappedToDealRequestDto() {
		return userHoldAmountMappedToDealRequestDto;
	}

	public void setUserHoldAmountMappedToDealRequestDto(
			List<UserHoldAmountMappedToDealRequestDto> userHoldAmountMappedToDealRequestDto) {
		this.userHoldAmountMappedToDealRequestDto = userHoldAmountMappedToDealRequestDto;
	}

}
