package com.oxyloans.dto.lenderholdamountdto;

public class UserHoldAmountMappedToDealResponseDto {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
