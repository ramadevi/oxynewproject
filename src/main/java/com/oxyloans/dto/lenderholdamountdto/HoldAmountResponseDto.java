package com.oxyloans.dto.lenderholdamountdto;

import java.math.BigInteger;
import java.util.List;

public class HoldAmountResponseDto {

	private Integer id;

	private Integer userId;

	private int dealId;

	private String status;

	private BigInteger holdAmount;

	private BigInteger currentHoldAmount;

	private String createdOn;

	private String comments;

	private String createdDate;

	private List<UserHoldAmountMappedToDealRequestDto> userHoldAmountMappedToDealRequestDto;

	private String HoldAmountGivenDealName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigInteger getHoldAmount() {
		return holdAmount;
	}

	public void setHoldAmount(BigInteger holdAmount) {
		this.holdAmount = holdAmount;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public BigInteger getCurrentHoldAmount() {
		return currentHoldAmount;
	}

	public void setCurrentHoldAmount(BigInteger currentHoldAmount) {
		this.currentHoldAmount = currentHoldAmount;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public List<UserHoldAmountMappedToDealRequestDto> getUserHoldAmountMappedToDealRequestDto() {
		return userHoldAmountMappedToDealRequestDto;
	}

	public void setUserHoldAmountMappedToDealRequestDto(
			List<UserHoldAmountMappedToDealRequestDto> userHoldAmountMappedToDealRequestDto) {
		this.userHoldAmountMappedToDealRequestDto = userHoldAmountMappedToDealRequestDto;
	}

	public String getHoldAmountGivenDealName() {
		return HoldAmountGivenDealName;
	}

	public void setHoldAmountGivenDealName(String holdAmountGivenDealName) {
		HoldAmountGivenDealName = holdAmountGivenDealName;
	}

}
