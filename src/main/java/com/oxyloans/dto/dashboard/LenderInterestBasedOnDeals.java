package com.oxyloans.dto.dashboard;

import java.util.List;

public class LenderInterestBasedOnDeals {

	private Integer numberOfInterestCount;

	private List<NewLenderReturnsResponseDto> newLenderReturnsResponseDto;

	public Integer getNumberOfInterestCount() {
		return numberOfInterestCount;
	}

	public void setNumberOfInterestCount(Integer numberOfInterestCount) {
		this.numberOfInterestCount = numberOfInterestCount;
	}

	public List<NewLenderReturnsResponseDto> getNewLenderReturnsResponseDto() {
		return newLenderReturnsResponseDto;
	}

	public void setNewLenderReturnsResponseDto(List<NewLenderReturnsResponseDto> newLenderReturnsResponseDto) {
		this.newLenderReturnsResponseDto = newLenderReturnsResponseDto;
	}

}
