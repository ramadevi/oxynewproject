package com.oxyloans.dto.dashboard;

import java.math.BigInteger;
import java.util.List;

import com.oxyloans.entity.borrowers.deals.dto.LenderDealsStatisticsInformation;

public class NewLenderDashboardResponseDto {

	private int countValue;

	private List<LenderWalletHistoryResponseDto> lenderWalletHistoryResponseDto;

	private List<NewLenderReturnsResponseDto> lenderReturnsResponseDto;

	private List<LenderTotalPaticipationDealsInfo> lenderTotalPaticipationDealsInfo;

	private List<ReferrerResponseDto> referrerResponseDto;

	private String excelDownloadUrl;

	private LenderDealsStatisticsInformation lenderDealsStatisticsInformation;

	private BigInteger totalWalletCredited;

	private BigInteger totalWalletDedited;

	private BigInteger totalInterest;

	private BigInteger totalPrincipal;

	private BigInteger totalParticipationForNormalDeals;

	private BigInteger totalParticipationForEquityDeals;

	private BigInteger totalReferralBonusPaid;

	private BigInteger totalReferralBonusUnPaid;

	private Double remainingAmountToParticipateInDeals;

	private Double maximumAmount;

	private BigInteger interestEarnedInThisFinancialYear;

	public int getCountValue() {
		return countValue;
	}

	public void setCountValue(int countValue) {
		this.countValue = countValue;
	}

	public List<LenderWalletHistoryResponseDto> getLenderWalletHistoryResponseDto() {
		return lenderWalletHistoryResponseDto;
	}

	public void setLenderWalletHistoryResponseDto(List<LenderWalletHistoryResponseDto> lenderWalletHistoryResponseDto) {
		this.lenderWalletHistoryResponseDto = lenderWalletHistoryResponseDto;
	}

	public List<NewLenderReturnsResponseDto> getLenderReturnsResponseDto() {
		return lenderReturnsResponseDto;
	}

	public void setLenderReturnsResponseDto(List<NewLenderReturnsResponseDto> lenderReturnsResponseDto) {
		this.lenderReturnsResponseDto = lenderReturnsResponseDto;
	}

	public List<LenderTotalPaticipationDealsInfo> getLenderTotalPaticipationDealsInfo() {
		return lenderTotalPaticipationDealsInfo;
	}

	public void setLenderTotalPaticipationDealsInfo(
			List<LenderTotalPaticipationDealsInfo> lenderTotalPaticipationDealsInfo) {
		this.lenderTotalPaticipationDealsInfo = lenderTotalPaticipationDealsInfo;
	}

	public String getExcelDownloadUrl() {
		return excelDownloadUrl;
	}

	public void setExcelDownloadUrl(String excelDownloadUrl) {
		this.excelDownloadUrl = excelDownloadUrl;
	}

	public List<ReferrerResponseDto> getReferrerResponseDto() {
		return referrerResponseDto;
	}

	public void setReferrerResponseDto(List<ReferrerResponseDto> referrerResponseDto) {
		this.referrerResponseDto = referrerResponseDto;
	}

	public LenderDealsStatisticsInformation getLenderDealsStatisticsInformation() {
		return lenderDealsStatisticsInformation;
	}

	public void setLenderDealsStatisticsInformation(LenderDealsStatisticsInformation lenderDealsStatisticsInformation) {
		this.lenderDealsStatisticsInformation = lenderDealsStatisticsInformation;
	}

	public BigInteger getTotalWalletCredited() {
		return totalWalletCredited;
	}

	public void setTotalWalletCredited(BigInteger totalWalletCredited) {
		this.totalWalletCredited = totalWalletCredited;
	}

	public BigInteger getTotalWalletDedited() {
		return totalWalletDedited;
	}

	public void setTotalWalletDedited(BigInteger totalWalletDedited) {
		this.totalWalletDedited = totalWalletDedited;
	}

	public BigInteger getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(BigInteger totalInterest) {
		this.totalInterest = totalInterest;
	}

	public BigInteger getTotalPrincipal() {
		return totalPrincipal;
	}

	public void setTotalPrincipal(BigInteger totalPrincipal) {
		this.totalPrincipal = totalPrincipal;
	}

	public BigInteger getTotalParticipationForNormalDeals() {
		return totalParticipationForNormalDeals;
	}

	public void setTotalParticipationForNormalDeals(BigInteger totalParticipationForNormalDeals) {
		this.totalParticipationForNormalDeals = totalParticipationForNormalDeals;
	}

	public BigInteger getTotalParticipationForEquityDeals() {
		return totalParticipationForEquityDeals;
	}

	public void setTotalParticipationForEquityDeals(BigInteger totalParticipationForEquityDeals) {
		this.totalParticipationForEquityDeals = totalParticipationForEquityDeals;
	}

	public BigInteger getTotalReferralBonusPaid() {
		return totalReferralBonusPaid;
	}

	public void setTotalReferralBonusPaid(BigInteger totalReferralBonusPaid) {
		this.totalReferralBonusPaid = totalReferralBonusPaid;
	}

	public BigInteger getTotalReferralBonusUnPaid() {
		return totalReferralBonusUnPaid;
	}

	public void setTotalReferralBonusUnPaid(BigInteger totalReferralBonusUnPaid) {
		this.totalReferralBonusUnPaid = totalReferralBonusUnPaid;
	}

	public Double getRemainingAmountToParticipateInDeals() {
		return remainingAmountToParticipateInDeals;
	}

	public void setRemainingAmountToParticipateInDeals(Double remainingAmountToParticipateInDeals) {
		this.remainingAmountToParticipateInDeals = remainingAmountToParticipateInDeals;
	}

	public Double getMaximumAmount() {
		return maximumAmount;
	}

	public void setMaximumAmount(Double maximumAmount) {
		this.maximumAmount = maximumAmount;
	}

	public BigInteger getInterestEarnedInThisFinancialYear() {
		return interestEarnedInThisFinancialYear;
	}

	public void setInterestEarnedInThisFinancialYear(BigInteger interestEarnedInThisFinancialYear) {
		this.interestEarnedInThisFinancialYear = interestEarnedInThisFinancialYear;
	}

}
