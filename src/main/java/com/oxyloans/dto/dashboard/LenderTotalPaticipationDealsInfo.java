package com.oxyloans.dto.dashboard;

import java.math.BigInteger;

public class LenderTotalPaticipationDealsInfo {

	private Integer sno;

	private String name;

	private int userId;

	private String participatedDate;

	private BigInteger participatedAmount;

	private double rateofinterest;

	private String dealName;

	private String pricipaleReturnedStatus;

	private String dealClosedDate;

	private String tenure;

	public Integer getSno() {
		return sno;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

	public String getParticipatedDate() {
		return participatedDate;
	}

	public void setParticipatedDate(String participatedDate) {
		this.participatedDate = participatedDate;
	}

	public BigInteger getParticipatedAmount() {
		return participatedAmount;
	}

	public void setParticipatedAmount(BigInteger participatedAmount) {
		this.participatedAmount = participatedAmount;
	}

	public double getRateofinterest() {
		return rateofinterest;
	}

	public void setRateofinterest(double rateofinterest) {
		this.rateofinterest = rateofinterest;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getPricipaleReturnedStatus() {
		return pricipaleReturnedStatus;
	}

	public void setPricipaleReturnedStatus(String pricipaleReturnedStatus) {
		this.pricipaleReturnedStatus = pricipaleReturnedStatus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getDealClosedDate() {
		return dealClosedDate;
	}

	public void setDealClosedDate(String dealClosedDate) {
		this.dealClosedDate = dealClosedDate;
	}

	public String getTenure() {
		return tenure;
	}

	public void setTenure(String tenure) {
		this.tenure = tenure;
	}

}
