package com.oxyloans.dto.dashboard;

import java.math.BigInteger;

public class ReferrerResponseDto {

	private Integer sno;

	private int userId;

	private String name;

	private Integer refereeId;

	private BigInteger amount;

	private String paymentStatus;

	private String transferredOn;

	private String dealName;

	private String participatedDate;

	private BigInteger participatedAmount;

	private String refereeName;

	public Integer getSno() {
		return sno;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

	public Integer getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(Integer refereeId) {
		this.refereeId = refereeId;
	}

	public BigInteger getAmount() {
		return amount;
	}

	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getTransferredOn() {
		return transferredOn;
	}

	public void setTransferredOn(String transferredOn) {
		this.transferredOn = transferredOn;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getParticipatedDate() {
		return participatedDate;
	}

	public void setParticipatedDate(String participatedDate) {
		this.participatedDate = participatedDate;
	}

	public BigInteger getParticipatedAmount() {
		return participatedAmount;
	}

	public void setParticipatedAmount(BigInteger participatedAmount) {
		this.participatedAmount = participatedAmount;
	}

	public String getRefereeName() {
		return refereeName;
	}

	public void setRefereeName(String refereeName) {
		this.refereeName = refereeName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
