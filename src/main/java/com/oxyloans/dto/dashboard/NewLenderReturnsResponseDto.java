package com.oxyloans.dto.dashboard;

import java.math.BigInteger;
import java.util.Date;

public class NewLenderReturnsResponseDto {
	
	private Integer sno;

	private String name;

	private int userId;

	private String returedDate;

	private BigInteger amount;

	private String remarks;

	private BigInteger currentAmount;

	private BigInteger paticiaptedAmount;

	private String paticipatedDate;

	private Double rateOfInterest;

	private String repaymentType;

	private String dealClosedDate;

	private String dealClosingStatus;

	private String dealName;

	private BigInteger dealAmount;

	private Integer differencInDays;
	
	private String LenderReturnType;

	private String lastParticipationDate;

	private String lenderParticipatedDate;

	private String loanActiveDate;

	private BigInteger totalInterestEarned;

	private BigInteger movedFromPaticipationToWallet;

	private BigInteger totalPaticipation;

	private String dealClosedToLender;

	private String fundsAcceptanceStartDate;

	private String fundsAcceptanceEndDate;
	
	private Integer dealId;
	
	private Date dealClosedToLenderUpdated;

	public Integer getSno() {
		return sno;
	}

	public String getName() {
		return name;
	}

	public int getUserId() {
		return userId;
	}

	public String getReturedDate() {
		return returedDate;
	}

	public BigInteger getAmount() {
		return amount;
	}

	public String getRemarks() {
		return remarks;
	}

	public BigInteger getCurrentAmount() {
		return currentAmount;
	}

	public BigInteger getPaticiaptedAmount() {
		return paticiaptedAmount;
	}

	public String getPaticipatedDate() {
		return paticipatedDate;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public String getRepaymentType() {
		return repaymentType;
	}

	public String getDealClosedDate() {
		return dealClosedDate;
	}

	public String getDealClosingStatus() {
		return dealClosingStatus;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setReturedDate(String returedDate) {
		this.returedDate = returedDate;
	}

	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void setCurrentAmount(BigInteger currentAmount) {
		this.currentAmount = currentAmount;
	}

	public void setPaticiaptedAmount(BigInteger paticiaptedAmount) {
		this.paticiaptedAmount = paticiaptedAmount;
	}

	public void setPaticipatedDate(String paticipatedDate) {
		this.paticipatedDate = paticipatedDate;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public void setRepaymentType(String repaymentType) {
		this.repaymentType = repaymentType;
	}

	public void setDealClosedDate(String dealClosedDate) {
		this.dealClosedDate = dealClosedDate;
	}

	public void setDealClosingStatus(String dealClosingStatus) {
		this.dealClosingStatus = dealClosingStatus;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public BigInteger getDealAmount() {
		return dealAmount;
	}

	public void setDealAmount(BigInteger dealAmount) {
		this.dealAmount = dealAmount;
	}

	public Integer getDifferencInDays() {
		return differencInDays;
	}

	public void setDifferencInDays(Integer differencInDays) {
		this.differencInDays = differencInDays;
	}

	public String getLenderReturnType() {
		return LenderReturnType;
	}

	public void setLenderReturnType(String lenderReturnType) {
		LenderReturnType = lenderReturnType;
	}

	public String getLastParticipationDate() {
		return lastParticipationDate;
	}

	public void setLastParticipationDate(String lastParticipationDate) {
		this.lastParticipationDate = lastParticipationDate;
	}

	public String getLenderParticipatedDate() {
		return lenderParticipatedDate;
	}

	public void setLenderParticipatedDate(String lenderParticipatedDate) {
		this.lenderParticipatedDate = lenderParticipatedDate;
	}

	public String getLoanActiveDate() {
		return loanActiveDate;
	}

	public void setLoanActiveDate(String loanActiveDate) {
		this.loanActiveDate = loanActiveDate;
	}

	public BigInteger getTotalInterestEarned() {
		return totalInterestEarned;
	}

	public void setTotalInterestEarned(BigInteger totalInterestEarned) {
		this.totalInterestEarned = totalInterestEarned;
	}

	public BigInteger getMovedFromPaticipationToWallet() {
		return movedFromPaticipationToWallet;
	}

	public void setMovedFromPaticipationToWallet(BigInteger movedFromPaticipationToWallet) {
		this.movedFromPaticipationToWallet = movedFromPaticipationToWallet;
	}

	public BigInteger getTotalPaticipation() {
		return totalPaticipation;
	}

	public void setTotalPaticipation(BigInteger totalPaticipation) {
		this.totalPaticipation = totalPaticipation;
	}

	public String getDealClosedToLender() {
		return dealClosedToLender;
	}

	public void setDealClosedToLender(String dealClosedToLender) {
		this.dealClosedToLender = dealClosedToLender;
	}

	public String getFundsAcceptanceStartDate() {
		return fundsAcceptanceStartDate;
	}

	public void setFundsAcceptanceStartDate(String fundsAcceptanceStartDate) {
		this.fundsAcceptanceStartDate = fundsAcceptanceStartDate;
	}

	public String getFundsAcceptanceEndDate() {
		return fundsAcceptanceEndDate;
	}

	public void setFundsAcceptanceEndDate(String fundsAcceptanceEndDate) {
		this.fundsAcceptanceEndDate = fundsAcceptanceEndDate;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Date getDealClosedToLenderUpdated() {
		return dealClosedToLenderUpdated;
	}

	public void setDealClosedToLenderUpdated(Date dealClosedToLenderUpdated) {
		this.dealClosedToLenderUpdated = dealClosedToLenderUpdated;
	}

}
