package com.oxyloans.dto.dashboard;

import java.math.BigInteger;

public class LenderWalletHistoryResponseDto {

	private Integer sno;

	private String name;

	private String walletLoaded;

	private BigInteger amount;

	private String remarks;

	private Integer userId;

	private String principalReturnAccountType;

	private String walletLoadedDate;

	public Integer getSno() {
		return sno;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWalletLoaded() {
		return walletLoaded;
	}

	public void setWalletLoaded(String walletLoaded) {
		this.walletLoaded = walletLoaded;
	}

	public BigInteger getAmount() {
		return amount;
	}

	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getPrincipalReturnAccountType() {
		return principalReturnAccountType;
	}

	public void setPrincipalReturnAccountType(String principalReturnAccountType) {
		this.principalReturnAccountType = principalReturnAccountType;
	}

	public String getWalletLoadedDate() {
		return walletLoadedDate;
	}

	public void setWalletLoadedDate(String walletLoadedDate) {
		this.walletLoadedDate = walletLoadedDate;
	}

}
