package com.oxyloans.dto.icici.dto;

public class IciciExcelsheetApprovalResponseDto {

	private String approvalStatus;

	public String getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

}
