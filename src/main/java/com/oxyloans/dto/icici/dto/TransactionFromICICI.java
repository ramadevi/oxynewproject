package com.oxyloans.dto.icici.dto;

public class TransactionFromICICI {
	private String ClientCode;

	private String VirtualAccountNumber;

	private String Mode;

	private String UTR;

	private String SenderRemark;

	private String ClientAccountNo;

	private String Amount;

	private String PayerName;

	private String PayerAccNumber;

	private String PayerBankIFSC;

	private String PayerPaymentDate;

	private String BankInternalTransactionNumber;

	public String getClientCode() {
		return ClientCode;
	}

	public void setClientCode(String clientCode) {
		ClientCode = clientCode;
	}

	public String getVirtualAccountNumber() {
		return VirtualAccountNumber;
	}

	public void setVirtualAccountNumber(String virtualAccountNumber) {
		VirtualAccountNumber = virtualAccountNumber;
	}

	public String getMode() {
		return Mode;
	}

	public void setMode(String mode) {
		Mode = mode;
	}

	public String getUTR() {
		return UTR;
	}

	public void setUTR(String uTR) {
		UTR = uTR;
	}

	public String getSenderRemark() {
		return SenderRemark;
	}

	public void setSenderRemark(String senderRemark) {
		SenderRemark = senderRemark;
	}

	public String getClientAccountNo() {
		return ClientAccountNo;
	}

	public void setClientAccountNo(String clientAccountNo) {
		ClientAccountNo = clientAccountNo;
	}

	public String getAmount() {
		return Amount;
	}

	public void setAmount(String amount) {
		Amount = amount;
	}

	public String getPayerName() {
		return PayerName;
	}

	public void setPayerName(String payerName) {
		PayerName = payerName;
	}

	public String getPayerAccNumber() {
		return PayerAccNumber;
	}

	public void setPayerAccNumber(String payerAccNumber) {
		PayerAccNumber = payerAccNumber;
	}

	public String getPayerBankIFSC() {
		return PayerBankIFSC;
	}

	public void setPayerBankIFSC(String payerBankIFSC) {
		PayerBankIFSC = payerBankIFSC;
	}

	public String getPayerPaymentDate() {
		return PayerPaymentDate;
	}

	public void setPayerPaymentDate(String payerPaymentDate) {
		PayerPaymentDate = payerPaymentDate;
	}

	public String getBankInternalTransactionNumber() {
		return BankInternalTransactionNumber;
	}

	public void setBankInternalTransactionNumber(String bankInternalTransactionNumber) {
		BankInternalTransactionNumber = bankInternalTransactionNumber;
	}
}
