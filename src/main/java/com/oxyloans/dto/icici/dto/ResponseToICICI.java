package com.oxyloans.dto.icici.dto;

public class ResponseToICICI {
	private String Response;

	private String Code;

	public String getResponse() {
		return Response;
	}

	public void setResponse(String response) {
		Response = response;
	}

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}
}
