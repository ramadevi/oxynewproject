package com.oxyloans.dto.icici.dto;

import java.util.Date;

public class BeforeApprovalFiles {

	private String fileName;
	private Integer userId;
	private double amount;
	private String lenderReturnType;
	private Integer scrowWalletId;
	private String scrowAccountNumber;
	private String firstName;
	private String mobileNumber;
	private String transActionDate;
	private String lastName;

	private String fileGenerationDate;

	private Date paymentDate;

	private Integer count;

	private String validityDate;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getLenderReturnType() {
		return lenderReturnType;
	}

	public void setLenderReturnType(String lenderReturnType) {
		this.lenderReturnType = lenderReturnType;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Integer getScrowWalletId() {
		return scrowWalletId;
	}

	public void setScrowWalletId(Integer scrowWalletId) {
		this.scrowWalletId = scrowWalletId;
	}

	public String getScrowAccountNumber() {
		return scrowAccountNumber;
	}

	public void setScrowAccountNumber(String scrowAccountNumber) {
		this.scrowAccountNumber = scrowAccountNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTransActionDate() {
		return transActionDate;
	}

	public void setTransActionDate(String transActionDate) {
		this.transActionDate = transActionDate;
	}

	public String getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}

	public String getFileGenerationDate() {
		return fileGenerationDate;
	}

	public void setFileGenerationDate(String fileGenerationDate) {
		this.fileGenerationDate = fileGenerationDate;
	}

}
