package com.oxyloans.dto.icici.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestFromICICI {

	@JsonProperty("requestId")
	private String requestId;

	@JsonProperty("service")
	private String service;

	@JsonProperty("encryptedKey")
	private String encryptedKey;

	@JsonProperty("oaepHashingAlgorithm")
	private String oaepHashingAlgorithm;

	@JsonProperty("iv")
	private String iv;

	@JsonProperty("encryptedData")
	private String encryptedData;

	@JsonProperty("clientInfo")
	private String clientInfo;

	@JsonProperty("optionalParam")
	private String optionalParam;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getEncryptedKey() {
		return encryptedKey;
	}

	public void setEncryptedKey(String encryptedKey) {
		this.encryptedKey = encryptedKey;
	}

	public String getOaepHashingAlgorithm() {
		return oaepHashingAlgorithm;
	}

	public void setOaepHashingAlgorithm(String oaepHashingAlgorithm) {
		this.oaepHashingAlgorithm = oaepHashingAlgorithm;
	}

	public String getIv() {
		return iv;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public String getEncryptedData() {
		return encryptedData;
	}

	public void setEncryptedData(String encryptedData) {
		this.encryptedData = encryptedData;
	}

	public String getClientInfo() {
		return clientInfo;
	}

	public void setClientInfo(String clientInfo) {
		this.clientInfo = clientInfo;
	}

	public String getOptionalParam() {
		return optionalParam;
	}

	public void setOptionalParam(String optionalParam) {
		this.optionalParam = optionalParam;
	}

}
