package com.oxyloans.dto.icici.dto;

import java.util.List;

import com.oxyloans.service.loan.dto.InterestsApprovalDto;

public class IciciExcelsheetApprovalRequestDto {

	private Integer dealId;

	private int userId;
	private String name;

	private String status;

	private String excelUrl;

	private String month;

	private String year;

	private String convertionType;

	private String userIdAndStatus;

	private boolean approvedAll;

	private Boolean checkAll;

	private String remarks;

	private String paymentDate;
	private boolean radhaSirApproved;

	private String fileName;

	private boolean sendMessages;

	private String date;

	private List<InterestsApprovalDto> interestsApprovalDto;

	private int folderPathValue;

	private String monthAndYear;

	private String readingType;

	private Double totalAmount;

	private boolean readDoubleValues;

	private String folderType;

	private String originalPaymentDate;

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExcelUrl() {
		return excelUrl;
	}

	public void setExcelUrl(String excelUrl) {
		this.excelUrl = excelUrl;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getConvertionType() {
		return convertionType;
	}

	public void setConvertionType(String convertionType) {
		this.convertionType = convertionType;
	}

	public String getUserIdAndStatus() {
		return userIdAndStatus;
	}

	public void setUserIdAndStatus(String userIdAndStatus) {
		this.userIdAndStatus = userIdAndStatus;
	}

	public boolean isApprovedAll() {
		return approvedAll;
	}

	public void setApprovedAll(boolean approvedAll) {
		this.approvedAll = approvedAll;
	}

	public Boolean getCheckAll() {
		return checkAll;
	}

	public void setCheckAll(Boolean checkAll) {
		this.checkAll = checkAll;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public boolean isRadhaSirApproved() {
		return radhaSirApproved;
	}

	public void setRadhaSirApproved(boolean radhaSirApproved) {
		this.radhaSirApproved = radhaSirApproved;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public boolean isSendMessages() {
		return sendMessages;
	}

	public void setSendMessages(boolean sendMessages) {
		this.sendMessages = sendMessages;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public List<InterestsApprovalDto> getInterestsApprovalDto() {
		return interestsApprovalDto;
	}

	public void setInterestsApprovalDto(List<InterestsApprovalDto> interestsApprovalDto) {
		this.interestsApprovalDto = interestsApprovalDto;
	}

	public int getFolderPathValue() {
		return folderPathValue;
	}

	public void setFolderPathValue(int folderPathValue) {
		this.folderPathValue = folderPathValue;
	}

	public String getMonthAndYear() {
		return monthAndYear;
	}

	public void setMonthAndYear(String monthAndYear) {
		this.monthAndYear = monthAndYear;
	}

	public String getReadingType() {
		return readingType;
	}

	public void setReadingType(String readingType) {
		this.readingType = readingType;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public boolean isReadDoubleValues() {
		return readDoubleValues;
	}

	public void setReadDoubleValues(boolean readDoubleValues) {
		this.readDoubleValues = readDoubleValues;
	}

	public String getFolderType() {
		return folderType;
	}

	public void setFolderType(String folderType) {
		this.folderType = folderType;
	}

	public String getOriginalPaymentDate() {
		return originalPaymentDate;
	}

	public void setOriginalPaymentDate(String originalPaymentDate) {
		this.originalPaymentDate = originalPaymentDate;
	}

}
