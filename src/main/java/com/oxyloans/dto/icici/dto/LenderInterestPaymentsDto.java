package com.oxyloans.dto.icici.dto;

import java.util.List;

import com.oxyloans.cms.dto.AfterApprovalFilesDto;

public class LenderInterestPaymentsDto {

	List<AfterApprovalFilesDto> afterApprovalFiles;

	private Integer count;

	public List<AfterApprovalFilesDto> getAfterApprovalFiles() {
		return afterApprovalFiles;
	}

	public void setAfterApprovalFiles(List<AfterApprovalFilesDto> afterApprovalFiles) {
		this.afterApprovalFiles = afterApprovalFiles;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
}
