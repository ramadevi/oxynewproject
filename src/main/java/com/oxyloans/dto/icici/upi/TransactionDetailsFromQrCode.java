package com.oxyloans.dto.icici.upi;

import java.util.List;

import com.oxyloans.request.LenderUserDetails;

public class TransactionDetailsFromQrCode {

	private List<LenderUserDetails> qrCodeTransactionDetails;

	private Integer totalCount;

	public List<LenderUserDetails> getQrCodeTransactionDetails() {
		return qrCodeTransactionDetails;
	}

	public void setQrCodeTransactionDetails(List<LenderUserDetails> qrCodeTransactionDetails) {
		this.qrCodeTransactionDetails = qrCodeTransactionDetails;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
}
