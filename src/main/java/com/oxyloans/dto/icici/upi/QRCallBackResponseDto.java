package com.oxyloans.dto.icici.upi;

public class QRCallBackResponseDto {

	private String merchantId;

	private String subMerchantId;

	private String terminalId;

	private String BankRRN;

	private String merchantTranId;

	private String PayerName;

	private String PayerVA;

	private String PayerAmount;

	private String TxnStatus;

	private String TxnInitDate;

	private String TxnCompletionDate;

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getSubMerchantId() {
		return subMerchantId;
	}

	public void setSubMerchantId(String subMerchantId) {
		this.subMerchantId = subMerchantId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getBankRRN() {
		return BankRRN;
	}

	public void setBankRRN(String bankRRN) {
		BankRRN = bankRRN;
	}

	public String getMerchantTranId() {
		return merchantTranId;
	}

	public void setMerchantTranId(String merchantTranId) {
		this.merchantTranId = merchantTranId;
	}

	public String getPayerName() {
		return PayerName;
	}

	public void setPayerName(String payerName) {
		PayerName = payerName;
	}

	public String getPayerVA() {
		return PayerVA;
	}

	public void setPayerVA(String payerVA) {
		PayerVA = payerVA;
	}

	public String getPayerAmount() {
		return PayerAmount;
	}

	public void setPayerAmount(String payerAmount) {
		PayerAmount = payerAmount;
	}

	public String getTxnStatus() {
		return TxnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		TxnStatus = txnStatus;
	}

	public String getTxnInitDate() {
		return TxnInitDate;
	}

	public void setTxnInitDate(String txnInitDate) {
		TxnInitDate = txnInitDate;
	}

	public String getTxnCompletionDate() {
		return TxnCompletionDate;
	}

	public void setTxnCompletionDate(String txnCompletionDate) {
		TxnCompletionDate = txnCompletionDate;
	}
}
