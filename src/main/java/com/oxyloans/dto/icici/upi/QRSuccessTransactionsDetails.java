package com.oxyloans.dto.icici.upi;

import java.util.List;

public class QRSuccessTransactionsDetails {

	private Integer totalCount;

	private List<TransactionResponseDto> transactionResponseDto;

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public List<TransactionResponseDto> getTransactionResponseDto() {
		return transactionResponseDto;
	}

	public void setTransactionResponseDto(List<TransactionResponseDto> transactionResponseDto) {
		this.transactionResponseDto = transactionResponseDto;
	}
}
