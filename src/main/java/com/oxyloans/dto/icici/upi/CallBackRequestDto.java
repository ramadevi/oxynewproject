package com.oxyloans.dto.icici.upi;

public class CallBackRequestDto {

	private String encryptedData;

	public String getEncryptedData() {
		return encryptedData;
	}

	public void setEncryptedData(String encryptedData) {
		this.encryptedData = encryptedData;
	}
}
