package com.oxyloans.dto.icici.upi;

public class QRTransactionCallBackDto {

	private String encryptedDate;

	private String status;

	public String getEncryptedDate() {
		return encryptedDate;
	}

	public void setEncryptedDate(String encryptedDate) {
		this.encryptedDate = encryptedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
