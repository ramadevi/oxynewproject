package com.oxyloans.dto.icici.upi;

public class TransactionResponseDto {

	private String status;

	private String qrGenerationString;

	private Integer qrTableId;

	private Integer userId;

	private String merchantTransactionId;

	private String amount;

	private String billNumber;

	private String bankRRN;

	private String transactionDate;

	public String getStatus() {
		return status;
	}

	public Integer getQrTableId() {
		return qrTableId;
	}

	public void setQrTableId(Integer qrTableId) {
		this.qrTableId = qrTableId;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getQrGenerationString() {
		return qrGenerationString;
	}

	public void setQrGenerationString(String qrGenerationString) {
		this.qrGenerationString = qrGenerationString;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getMerchantTransactionId() {
		return merchantTransactionId;
	}

	public void setMerchantTransactionId(String merchantTransactionId) {
		this.merchantTransactionId = merchantTransactionId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getBankRRN() {
		return bankRRN;
	}

	public void setBankRRN(String bankRRN) {
		this.bankRRN = bankRRN;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

}
