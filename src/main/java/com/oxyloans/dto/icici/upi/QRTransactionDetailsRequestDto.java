package com.oxyloans.dto.icici.upi;

public class QRTransactionDetailsRequestDto {

	private Integer amount;

	private Integer userId;

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	
}
