package com.oxyloans.dto.icici.upi;

public class ICICICallBackData {

	private String merchantId;

	private Integer subMerchantId;

	private Integer terminalId;

	private String BankRRN;

	private String merchantTranId;

	private String PayerName;

	private String PayerMobile;

	private String PayerVA;

	private Double PayerAmount;

	private String TxnStatus;

	private String TxnInitDate;

	private String TxnCompletionDate;

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public Integer getSubMerchantId() {
		return subMerchantId;
	}

	public void setSubMerchantId(Integer subMerchantId) {
		this.subMerchantId = subMerchantId;
	}

	public Integer getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(Integer terminalId) {
		this.terminalId = terminalId;
	}

	public String getBankRRN() {
		return BankRRN;
	}

	public void setBankRRN(String bankRRN) {
		BankRRN = bankRRN;
	}

	public String getMerchantTranId() {
		return merchantTranId;
	}

	public void setMerchantTranId(String merchantTranId) {
		this.merchantTranId = merchantTranId;
	}

	public String getPayerName() {
		return PayerName;
	}

	public void setPayerName(String payerName) {
		PayerName = payerName;
	}

	public String getPayerMobile() {
		return PayerMobile;
	}

	public void setPayerMobile(String payerMobile) {
		PayerMobile = payerMobile;
	}

	public String getPayerVA() {
		return PayerVA;
	}

	public void setPayerVA(String payerVA) {
		PayerVA = payerVA;
	}

	public Double getPayerAmount() {
		return PayerAmount;
	}

	public void setPayerAmount(Double payerAmount) {
		PayerAmount = payerAmount;
	}

	public String getTxnStatus() {
		return TxnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		TxnStatus = txnStatus;
	}

	public String getTxnInitDate() {
		return TxnInitDate;
	}

	public void setTxnInitDate(String txnInitDate) {
		TxnInitDate = txnInitDate;
	}

	public String getTxnCompletionDate() {
		return TxnCompletionDate;
	}

	public void setTxnCompletionDate(String txnCompletionDate) {
		TxnCompletionDate = txnCompletionDate;
	}

}
