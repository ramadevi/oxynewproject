package com.oxyloans.dto.icici.upi;

public class TransactionStatusCheckResponseDto {

	private String response;

	private Integer merchantId;

	private Integer subMerchantId;

	private Integer terminalId;

	private String success;

	private String message;

	private String amount;

	private String merchantTranId;

	private String OriginalBankRRN;

	private String status;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Integer getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}

	public Integer getSubMerchantId() {
		return subMerchantId;
	}

	public void setSubMerchantId(Integer subMerchantId) {
		this.subMerchantId = subMerchantId;
	}

	public Integer getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(Integer terminalId) {
		this.terminalId = terminalId;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMerchantTranId() {
		return merchantTranId;
	}

	public void setMerchantTranId(String merchantTranId) {
		this.merchantTranId = merchantTranId;
	}

	public String getOriginalBankRRN() {
		return OriginalBankRRN;
	}

	public void setOriginalBankRRN(String originalBankRRN) {
		OriginalBankRRN = originalBankRRN;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
