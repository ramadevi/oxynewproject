package com.oxyloans.dto.principal;

import java.util.List;
import java.util.Map;

public class LenderPaymentsResponse {

	private List<LenderPayementDetails> LenderPayementDetailsList;
	private Map<String, List<LenderPayementDetails>> paymentsMap;

	public List<LenderPayementDetails> getLenderPayementDetailsList() {
		return LenderPayementDetailsList;
	}

	public void setLenderPayementDetailsList(List<LenderPayementDetails> lenderPayementDetailsList) {
		LenderPayementDetailsList = lenderPayementDetailsList;
	}

	public Map<String, List<LenderPayementDetails>> getPaymentsMap() {
		return paymentsMap;
	}

	public void setPaymentsMap(Map<String, List<LenderPayementDetails>> paymentsMap) {
		this.paymentsMap = paymentsMap;
	}

}
