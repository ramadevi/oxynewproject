package com.oxyloans.dto.principal;

public class LenderPayementDetails {
	private Integer userId;
	private Double amount;
	private String paidDate;
	private String amountType;
	private String comments;
	private String fileStatus;
	private String fileName;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getFileStatus() {
		return fileStatus;
	}

	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public LenderPayementDetails(Integer userId, Double amount, String paidDate, String amountType, String comments,
			String fileStatus, String fileName) {
		super();
		this.userId = userId;
		this.amount = amount;
		this.paidDate = paidDate;
		this.amountType = amountType;
		this.comments = comments;
		this.fileStatus = fileStatus;
		this.fileName = fileName;
	}

	public LenderPayementDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

}