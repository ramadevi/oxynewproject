package com.oxyloans.dto.ondc;

public class Message {

	private AckResponse ack;

	public AckResponse getAck() {
		return ack;
	}

	public void setAck(AckResponse ack) {
		this.ack = ack;
	}

}
