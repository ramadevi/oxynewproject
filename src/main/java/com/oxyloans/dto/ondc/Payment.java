package com.oxyloans.dto.ondc;

import java.util.List;

public class Payment {

	private String collected_by;

	private List<Tags> tags;

	public String getCollected_by() {
		return collected_by;
	}

	public List<Tags> getTags() {
		return tags;
	}

	public void setCollected_by(String collected_by) {
		this.collected_by = collected_by;
	}

	public void setTags(List<Tags> tags) {
		this.tags = tags;
	}

}
