package com.oxyloans.dto.ondc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContextDto {

	@JsonProperty("domain")
	private String domain;

	@JsonProperty("location")
	private Location location;

	@JsonProperty("action")
	private String action;

	@JsonProperty("version")
	private String version;

	@JsonProperty("bap_id")
	private String bap_id;

	@JsonProperty("bap_uri")
	private String bap_uri;

	@JsonProperty("transaction_id")
	private String transaction_id;

	@JsonProperty("message_id")
	private String message_id;

	@JsonProperty("timestamp")
	private String timestamp;

	@JsonProperty("ttl")
	private String ttl;

	public String getDomain() {
		return domain;
	}

	public Location getLocation() {
		return location;
	}

	public String getAction() {
		return action;
	}

	public String getVersion() {
		return version;
	}

	public String getBap_id() {
		return bap_id;
	}

	public String getBap_uri() {
		return bap_uri;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public String getMessage_id() {
		return message_id;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public String getTtl() {
		return ttl;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setBap_id(String bap_id) {
		this.bap_id = bap_id;
	}

	public void setBap_uri(String bap_uri) {
		this.bap_uri = bap_uri;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public void setMessage_id(String message_id) {
		this.message_id = message_id;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public void setTtl(String ttl) {
		this.ttl = ttl;
	}

}
