package com.oxyloans.dto.ondc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubscriberUrlResponseDto {
	
	@JsonProperty
	private String answer;

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	
}
