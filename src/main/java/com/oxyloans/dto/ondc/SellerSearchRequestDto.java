package com.oxyloans.dto.ondc;

public class SellerSearchRequestDto {

	private ContextDto context;

	private SellerMessageRequestDto message;

	public ContextDto getContext() {
		return context;
	}

	public SellerMessageRequestDto getMessage() {
		return message;
	}

	public void setContext(ContextDto context) {
		this.context = context;
	}

	public void setMessage(SellerMessageRequestDto message) {
		this.message = message;
	}

}
