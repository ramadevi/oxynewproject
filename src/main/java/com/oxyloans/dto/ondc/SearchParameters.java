package com.oxyloans.dto.ondc;

public class SearchParameters {

	private String country;

	private String domain;

	private String type;

	private String city;

	private String subscriber_id;

	public String getCountry() {
		return country;
	}

	public String getDomain() {
		return domain;
	}

	public String getType() {
		return type;
	}

	public String getCity() {
		return city;
	}

	public String getSubscriber_id() {
		return subscriber_id;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setSubscriber_id(String subscriber_id) {
		this.subscriber_id = subscriber_id;
	}

}
