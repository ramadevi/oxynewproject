package com.oxyloans.dto.ondc;

public class SellerSearchResponseDto {

	private ContextDto context;

	private SellerMessageResponseDto message;

	public ContextDto getContext() {
		return context;
	}

	public SellerMessageResponseDto getMessage() {
		return message;
	}

	public void setContext(ContextDto context) {
		this.context = context;
	}

	public void setMessage(SellerMessageResponseDto message) {
		this.message = message;
	}

}
