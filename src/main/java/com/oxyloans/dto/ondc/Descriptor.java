package com.oxyloans.dto.ondc;

public class Descriptor {

	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
