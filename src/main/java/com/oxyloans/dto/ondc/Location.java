package com.oxyloans.dto.ondc;

public class Location {

	private City city;

	private Country country;

	public City getCity() {
		return city;
	}

	public Country getCountry() {
		return country;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

}
