package com.oxyloans.dto.ondc;

public class ListOfDescriptor {

	private Descriptor descriptor;

	private String value;

	public Descriptor getDescriptor() {
		return descriptor;
	}

	public String getValue() {
		return value;
	}

	public void setDescriptor(Descriptor descriptor) {
		this.descriptor = descriptor;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
