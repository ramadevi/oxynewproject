package com.oxyloans.dto.ondc;

public class SellerMessageResponseDto {

	private ContextDto context;
	
	private Message message;

	public ContextDto getContext() {
		return context;
	}

	public void setContext(ContextDto context) {
		this.context = context;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	
}
