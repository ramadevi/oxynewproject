package com.oxyloans.dto.ondc;

public class Category {

	private Descriptor descriptor;

	public Descriptor getDescriptor() {
		return descriptor;
	}

	public void setDescriptor(Descriptor descriptor) {
		this.descriptor = descriptor;
	}

}
