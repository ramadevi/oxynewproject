package com.oxyloans.dto.ondc;

public class SellerMessageRequestDto {

	private ContextDto context;

	public ContextDto getContext() {
		return context;
	}

	public void setContext(ContextDto context) {
		this.context = context;
	}

}
