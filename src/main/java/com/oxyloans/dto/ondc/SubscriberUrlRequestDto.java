package com.oxyloans.dto.ondc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubscriberUrlRequestDto {

	@JsonProperty
	private String subscriber_id;

	@JsonProperty
	private String challenge;

	public String getSubscriber_id() {
		return subscriber_id;
	}

	public String getChallenge() {
		return challenge;
	}

	public void setSubscriber_id(String subscriber_id) {
		this.subscriber_id = subscriber_id;
	}

	public void setChallenge(String challenge) {
		this.challenge = challenge;
	}

}
