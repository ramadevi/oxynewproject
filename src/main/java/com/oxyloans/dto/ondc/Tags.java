package com.oxyloans.dto.ondc;

import java.util.List;

public class Tags {

	private Descriptor descriptor;

	private String display;

	private List<ListOfDescriptor> list;

	public Descriptor getDescriptor() {
		return descriptor;
	}

	public String getDisplay() {
		return display;
	}

	public List<ListOfDescriptor> getList() {
		return list;
	}

	public void setDescriptor(Descriptor descriptor) {
		this.descriptor = descriptor;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public void setList(List<ListOfDescriptor> list) {
		this.list = list;
	}

}
