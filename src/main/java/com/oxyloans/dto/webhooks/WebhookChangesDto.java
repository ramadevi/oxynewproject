
package com.oxyloans.dto.webhooks;

public class WebhookChangesDto {

	private String field;

	private WebhookValueDto value;

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public WebhookValueDto getValue() {
		return value;
	}

	public void setValue(WebhookValueDto value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "WebhookChangesDto [field=" + field + ", value=" + value + "]";
	}
	
	

}
