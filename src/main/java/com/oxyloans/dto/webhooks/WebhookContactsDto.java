package com.oxyloans.dto.webhooks;

public class WebhookContactsDto {

	private ProfileContactsDto profile;
	private String wa_id;

	public ProfileContactsDto getProfile() {
		return profile;
	}

	public void setProfile(ProfileContactsDto profile) {
		this.profile = profile;
	}

	public String getWa_id() {
		return wa_id;
	}

	public void setWa_id(String wa_id) {
		this.wa_id = wa_id;
	}

	@Override
	public String toString() {
		return "WebhookContactsDto [profile=" + profile + ", wa_id=" + wa_id + "]";
	}

}
