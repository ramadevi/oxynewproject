package com.oxyloans.dto.webhooks;

import java.util.Arrays;

public class WebhookNotificationsDto {

	private WebhookEntryDto[] entry;

	private String object;

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public WebhookEntryDto[] getEntry() {
		return entry;
	}

	public void setEntry(WebhookEntryDto[] entry) {
		this.entry = entry;
	}

	@Override
	public String toString() {
		return "WebhookNotificationsDto [entry=" + Arrays.toString(entry) + ", object=" + object + "]";
	}

	
	
	

}
