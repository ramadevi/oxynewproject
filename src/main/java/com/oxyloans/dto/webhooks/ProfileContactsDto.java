package com.oxyloans.dto.webhooks;

public class ProfileContactsDto {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ProfileContactsDto [name=" + name + "]";
	}
	
	
	
	
}
