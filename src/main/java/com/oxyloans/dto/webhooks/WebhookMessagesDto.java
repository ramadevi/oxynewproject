package com.oxyloans.dto.webhooks;

public class WebhookMessagesDto {

	private String from;

	private String id;
	private String timestamp;
	private String type;

	private WebhooksTextDto text;

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public WebhooksTextDto getText() {
		return text;
	}

	public void setText(WebhooksTextDto text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "WebhookMessagesDto [from=" + from + ", id=" + id + ", timestamp=" + timestamp + ", type=" + type
				+ ", text=" + text + "]";
	}

	
}
