
package com.oxyloans.dto.webhooks;

public class WebhooksTextDto {

	private String body;

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
