package com.oxyloans.dto.webhooks;

import java.util.Arrays;

public class WebhookEntryDto {

	private String time;

	private WebhookChangesDto[] changes;
	private String id;
	private String uid;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public WebhookChangesDto[] getChanges() {
		return changes;
	}

	public void setChanges(WebhookChangesDto[] changes) {
		this.changes = changes;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	@Override
	public String toString() {
		return "WebhookEntryDto [time=" + time + ", changes=" + Arrays.toString(changes) + ", id=" + id + ", uid=" + uid
				+ "]";
	}

}
