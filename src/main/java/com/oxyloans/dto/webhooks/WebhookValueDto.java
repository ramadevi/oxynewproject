package com.oxyloans.dto.webhooks;

import java.util.Arrays;

public class WebhookValueDto {

	private String verb;
	private String object_id;

	private String messaging_product;

	private MetaDataDto metadata;

	private WebhookMessagesDto[] messages;

	private WebhookContactsDto[] contacts;

	public String getVerb() {
		return verb;
	}

	public void setVerb(String verb) {
		this.verb = verb;
	}

	public String getObject_id() {
		return object_id;
	}

	public void setObject_id(String object_id) {
		this.object_id = object_id;
	}

	

	public String getMessaging_product() {
		return messaging_product;
	}

	public void setMessaging_product(String messaging_product) {
		this.messaging_product = messaging_product;
	}

	public MetaDataDto getMetadata() {
		return metadata;
	}

	public void setMetadata(MetaDataDto metadata) {
		this.metadata = metadata;
	}

	public WebhookMessagesDto[] getMessages() {
		return messages;
	}

	public void setMessages(WebhookMessagesDto[] messages) {
		this.messages = messages;
	}

	public WebhookContactsDto[] getContacts() {
		return contacts;
	}

	public void setContacts(WebhookContactsDto[] contacts) {
		this.contacts = contacts;
	}

	@Override
	public String toString() {
		return "WebhookValueDto [verb=" + verb + ", object_id=" + object_id + ", messaging_product=" + messaging_product
				+ ", metadata=" + metadata + ", messages=" + Arrays.toString(messages) + ", contacts="
				+ Arrays.toString(contacts) + "]";
	}
	
	

}
