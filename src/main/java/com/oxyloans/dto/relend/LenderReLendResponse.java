package com.oxyloans.dto.relend;

import com.oxyloans.response.user.UserResponse;

public class LenderReLendResponse {

	private Integer id;
	private Integer userId;
	private String adminComments;
	private String createdOn;
	private String status;
	private String message;
	private String errorMessageDescription;
	private String firstName;
	private String lastName;
	private String panNumber;
	private String address;
	private String email;
	private String mobileNumber;
	private String lenderCity;
	private Double lenderWalletAmount;
	private UserResponse user;
	private Double emiAmountAvbleForEscrow;
	private Double emiAmountAddedToEscrow;

	public Double getEmiAmountAddedToEscrow() {
		return emiAmountAddedToEscrow;
	}

	public void setEmiAmountAddedToEscrow(Double emiAmountAddedToEscrow) {
		this.emiAmountAddedToEscrow = emiAmountAddedToEscrow;
	}

	public Double getEmiAmountAvbleForEscrow() {
		return emiAmountAvbleForEscrow;
	}

	public void setEmiAmountAvbleForEscrow(Double emiAmountAvbleForEscrow) {
		this.emiAmountAvbleForEscrow = emiAmountAvbleForEscrow;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getLenderCity() {
		return lenderCity;
	}

	public void setLenderCity(String lenderCity) {
		this.lenderCity = lenderCity;
	}

	public Double getLenderWalletAmount() {
		return lenderWalletAmount;
	}

	public void setLenderWalletAmount(Double lenderWalletAmount) {
		this.lenderWalletAmount = lenderWalletAmount;
	}

	public UserResponse getUser() {
		return user;
	}

	public void setUser(UserResponse user) {
		this.user = user;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public String getErrorMessageDescription() {
		return errorMessageDescription;
	}

	public void setErrorMessageDescription(String errorMessageDescription) {
		this.errorMessageDescription = errorMessageDescription;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getAdminComments() {
		return adminComments;
	}

	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
