package com.oxyloans.dto.cms;

public class RejectedFileDto {
	private String fileName;
	private String deald;
	private String lenderId;
	private String amountType;

	private String description;
	private String amountDate;
	private String amount;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDeald() {
		return deald;
	}

	public void setDeald(String deald) {
		this.deald = deald;
	}

	public String getLenderId() {
		return lenderId;
	}

	public void setLenderId(String lenderId) {
		this.lenderId = lenderId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "RejectedFileDto [fileName=" + fileName + ", deald=" + deald + ", lenderId=" + lenderId
				+ ", description=" + description + "]";
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public String getAmountDate() {
		return amountDate;
	}

	public void setAmountDate(String amountDate) {
		this.amountDate = amountDate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

}
