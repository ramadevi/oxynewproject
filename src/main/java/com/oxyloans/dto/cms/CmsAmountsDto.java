package com.oxyloans.dto.cms;

import java.util.List;

public class CmsAmountsDto {

	private Integer dealId;
	private List<CmsAmountDetailsDto> breakUp;
	private Double amountGiven;
	private Double amountReceived;

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public List<CmsAmountDetailsDto> getBreakUp() {
		return breakUp;
	}

	public void setBreakUp(List<CmsAmountDetailsDto> breakUp) {
		this.breakUp = breakUp;
	}

	public Double getAmountGiven() {
		return amountGiven;
	}

	public void setAmountGiven(Double amountGiven) {
		this.amountGiven = amountGiven;
	}

	public Double getAmountReceived() {
		return amountReceived;
	}

	public void setAmountReceived(Double amountReceived) {
		this.amountReceived = amountReceived;
	}

}
