package com.oxyloans.dto.cms;

import java.util.List;

import com.oxyloans.service.loan.dto.InterestsApprovalDto;

public class CmsJson {

	private List<InterestsApprovalDto> interestsApprovalDto;

	public List<InterestsApprovalDto> getInterestsApprovalDto() {
		return interestsApprovalDto;
	}

	public void setInterestsApprovalDto(List<InterestsApprovalDto> interestsApprovalDto) {
		this.interestsApprovalDto = interestsApprovalDto;
	}

}
