package com.oxyloans.dto.cms;

public class CmsPrincipalInterestStatsDto {

	private CmsInitiatedAndReceivedDto principalInterestBreakUp;
	private String amountsMatched;

	public CmsInitiatedAndReceivedDto getPrincipalInterestBreakUp() {
		return principalInterestBreakUp;
	}

	public void setPrincipalInterestBreakUp(CmsInitiatedAndReceivedDto principalInterestBreakUp) {
		this.principalInterestBreakUp = principalInterestBreakUp;
	}

	public String getAmountsMatched() {
		return amountsMatched;
	}

	public void setAmountsMatched(String amountsMatched) {
		this.amountsMatched = amountsMatched;
	}

}
