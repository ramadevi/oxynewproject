package com.oxyloans.dto.cms;

import java.util.List;

public class RejectedFilesRequest {

	private List<RejectedFileDto> interestRejected;

	private List<RejectedFileDto> principalRejected;

	public List<RejectedFileDto> getInterestRejected() {
		return interestRejected;
	}

	public void setInterestRejected(List<RejectedFileDto> interestRejected) {
		this.interestRejected = interestRejected;
	}

	public List<RejectedFileDto> getPrincipalRejected() {
		return principalRejected;
	}

	public void setPrincipalRejected(List<RejectedFileDto> principalRejected) {
		this.principalRejected = principalRejected;
	}

}
