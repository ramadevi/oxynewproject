package com.oxyloans.dto.cms;

import java.util.Date;

public class PaymentDetailDto {
	
	public static enum  PaymentType{
		
		LENDERINTEREST,LENDERPRINCIPAL,PRINCIPALINTEREST,LENDERWITHDRAW,PRINCIPALTOWALLET
	}
	public static enum FileStatus{
		INITIATED,EXECUTED,BOTH
	}
	private String date;
	
	private String endDate;
	
	private PaymentType paymentType;

	private FileStatus fileStatus;
	
	


	public FileStatus getFileStatus() {
		return fileStatus;
	}

	public void setFileStatus(FileStatus fileStatus) {
		this.fileStatus = fileStatus;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	

}
