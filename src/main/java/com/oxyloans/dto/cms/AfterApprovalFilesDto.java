package com.oxyloans.dto.cms;

public class AfterApprovalFilesDto {

	private String fileName;

	private String fileType;

	private String fileExecutionStatus;

	private String transactionStatus;

	private Integer userId;

	private Integer dealId;
	private Double amount;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileExecutionStatus() {
		return fileExecutionStatus;
	}

	public void setFileExecutionStatus(String fileExecutionStatus) {
		this.fileExecutionStatus = fileExecutionStatus;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	

}
