package com.oxyloans.dto.cms;

public class CmsAmountDetailsDto {

	private Integer userId;
	private Double amount;
	private String paymentDate;

	public CmsAmountDetailsDto(Integer userId, Double amount, String paymentDate) {
		super();

		this.userId = userId;
		this.amount = amount;
		this.paymentDate = paymentDate;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

}
