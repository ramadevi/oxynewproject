package com.oxyloans.dto.cms;

public class CmsPaymentStatsDto {

	private Double totalInterestAmount;
	private InterestAmountsDto interestAmountdto;
	private Double initiatedPrincipalAmount;
	private Double totalLenderPrincipal;
	private Double totalBorrowerLoan;

	public Double getTotalInterestAmount() {
		return totalInterestAmount;
	}

	public void setTotalInterestAmount(Double totalInterestAmount) {
		this.totalInterestAmount = totalInterestAmount;
	}

	public InterestAmountsDto getInterestAmountdto() {
		return interestAmountdto;
	}

	public void setInterestAmountdto(InterestAmountsDto interestAmountdto) {
		this.interestAmountdto = interestAmountdto;
	}

	public Double getTotalLenderPrincipal() {
		return totalLenderPrincipal;
	}

	public void setTotalLenderPrincipal(Double totalLenderPrincipal) {
		this.totalLenderPrincipal = totalLenderPrincipal;
	}

	public Double getTotalBorrowerLoan() {
		return totalBorrowerLoan;
	}

	public void setTotalBorrowerLoan(Double totalBorrowerLoan) {
		this.totalBorrowerLoan = totalBorrowerLoan;
	}

	public Double getInitiatedPrincipalAmount() {
		return initiatedPrincipalAmount;
	}

	public void setInitiatedPrincipalAmount(Double initiatedPrincipalAmount) {
		this.initiatedPrincipalAmount = initiatedPrincipalAmount;
	}

}
