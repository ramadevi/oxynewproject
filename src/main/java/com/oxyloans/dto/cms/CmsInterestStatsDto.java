package com.oxyloans.dto.cms;

public class CmsInterestStatsDto {

	private CmsInitiatedAndReceivedDto interestsBreakUp;
	private String amountsMatched;
	private Double totalAmountInitiated;
	private Double totalAmountReceived;

	public CmsInitiatedAndReceivedDto getInterestsBreakUp() {
		return interestsBreakUp;
	}

	public void setInterestsBreakUp(CmsInitiatedAndReceivedDto interestsBreakUp) {
		this.interestsBreakUp = interestsBreakUp;
	}

	public String getAmountsMatched() {
		return amountsMatched;
	}

	public void setAmountsMatched(String amountsMatched) {
		this.amountsMatched = amountsMatched;
	}

	public Double getTotalAmountInitiated() {
		return totalAmountInitiated;
	}

	public void setTotalAmountInitiated(Double totalAmountInitiated) {
		this.totalAmountInitiated = totalAmountInitiated;
	}

	public Double getTotalAmountReceived() {
		return totalAmountReceived;
	}

	public void setTotalAmountReceived(Double totalAmountReceived) {
		this.totalAmountReceived = totalAmountReceived;
	}

}
