package com.oxyloans.dto.cms;

import java.math.BigInteger;

public class BeforeApprovalFilesDto {

	private int sno;
	private String fileName;
	private BigInteger amount;
	private String paymentDate;
	private String fileStatus;
	private String fileType;
	private int dealId;
	private String borrowerName;
	private String accountNumber;
	private String ifsc;

	public int getSno() {
		return sno;
	}

	public String getFileName() {
		return fileName;
	}

	public BigInteger getAmount() {
		return amount;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public String getFileStatus() {
		return fileStatus;
	}

	public void setSno(int sno) {
		this.sno = sno;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

}
