package com.oxyloans.dto.cms;

import java.util.Comparator;
import java.util.List;

import com.oxyloans.service.loan.dto.DealLevelInterestPaymentsDto;

public class TotalDealLevelInterestsDto {

	private String paymentDate;
	private Double amount;
	private String status;
	private List<DealLevelInterestPaymentsDto> dealLevelInterestPaymentsDto;

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public List<DealLevelInterestPaymentsDto> getDealLevelInterestPaymentsDto() {
		return dealLevelInterestPaymentsDto;
	}

	public void setDealLevelInterestPaymentsDto(List<DealLevelInterestPaymentsDto> dealLevelInterestPaymentsDto) {
		this.dealLevelInterestPaymentsDto = dealLevelInterestPaymentsDto;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Comparator<TotalDealLevelInterestsDto> comparator = (c1, c2) -> {
		return (c1.getPaymentDate()).compareTo(c2.getPaymentDate());

	};

}
