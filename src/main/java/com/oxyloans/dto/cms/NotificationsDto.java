package com.oxyloans.dto.cms;

public class NotificationsDto {

	private boolean notify;

	private Integer folderPath;

	public boolean isNotify() {
		return notify;
	}

	public void setNotify(boolean notify) {
		this.notify = notify;
	}

	public Integer getFolderPath() {
		return folderPath;
	}

	public void setFolderPath(Integer folderPath) {
		this.folderPath = folderPath;
	}

}
