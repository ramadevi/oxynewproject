package com.oxyloans.dto.cms;

public class InterestAmountsDto {

	private Double lenderInterest;
	private Double initiatedLenderInterest;
	private Double principalInterest;
	private Double intiatedPrincipalInterest;
	private Double withDrawalInterest;

	public Double getLenderInterest() {
		return lenderInterest;
	}

	public void setLenderInterest(Double lenderInterest) {
		this.lenderInterest = lenderInterest;
	}

	public Double getPrincipalInterest() {
		return principalInterest;
	}

	public void setPrincipalInterest(Double principalInterest) {
		this.principalInterest = principalInterest;
	}

	public Double getWithDrawalInterest() {
		return withDrawalInterest;
	}

	public void setWithDrawalInterest(Double withDrawalInterest) {
		this.withDrawalInterest = withDrawalInterest;
	}

	public Double getInitiatedLenderInterest() {
		return initiatedLenderInterest;
	}

	public void setInitiatedLenderInterest(Double initiatedLenderInterest) {
		this.initiatedLenderInterest = initiatedLenderInterest;
	}

	public Double getIntiatedPrincipalInterest() {
		return intiatedPrincipalInterest;
	}

	public void setIntiatedPrincipalInterest(Double intiatedPrincipalInterest) {
		this.intiatedPrincipalInterest = intiatedPrincipalInterest;
	}

}
