package com.oxyloans.dto.cms;

import java.util.List;

public class CmsInitiatedAndReceivedDto {

	private List<CmsAmountsDto> initiatedAmounts;
	private List<CmsAmountsDto> receivedAmount;
	private List<CmsAmountsDto> unMatchedAmounts;

	public List<CmsAmountsDto> getInitiatedAmounts() {
		return initiatedAmounts;
	}

	public void setInitiatedAmounts(List<CmsAmountsDto> initiatedAmounts) {
		this.initiatedAmounts = initiatedAmounts;
	}

	public List<CmsAmountsDto> getReceivedAmount() {
		return receivedAmount;
	}

	public void setReceivedAmount(List<CmsAmountsDto> receivedAmount) {
		this.receivedAmount = receivedAmount;
	}

	public List<CmsAmountsDto> getUnMatchedAmounts() {
		return unMatchedAmounts;
	}

	public void setUnMatchedAmounts(List<CmsAmountsDto> unMatchedAmounts) {
		this.unMatchedAmounts = unMatchedAmounts;
	}

}
