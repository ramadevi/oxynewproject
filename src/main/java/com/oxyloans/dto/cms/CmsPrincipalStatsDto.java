package com.oxyloans.dto.cms;

import java.util.List;

public class CmsPrincipalStatsDto {

	private List<CmsInitiatedAndReceivedDto> pricipal2BankBreakUp;

	private String amountsMatchedForP2B;

	private Double totalAmountInitiatedForP2B;

	private List<CmsInitiatedAndReceivedDto> pricipal2WalletBreakUp;

	private String amountsMatchedForP2W;

	private Double totalAmountInitiatedForP2W;

	private Double totalAmountReceivedP2W;

	public List<CmsInitiatedAndReceivedDto> getPricipal2BankBreakUp() {
		return pricipal2BankBreakUp;
	}

	public void setPricipal2BankBreakUp(List<CmsInitiatedAndReceivedDto> pricipal2BankBreakUp) {
		this.pricipal2BankBreakUp = pricipal2BankBreakUp;
	}

	public String getAmountsMatchedForP2B() {
		return amountsMatchedForP2B;
	}

	public void setAmountsMatchedForP2B(String amountsMatchedForP2B) {
		this.amountsMatchedForP2B = amountsMatchedForP2B;
	}

	public List<CmsInitiatedAndReceivedDto> getPricipal2WalletBreakUp() {
		return pricipal2WalletBreakUp;
	}

	public void setPricipal2WalletBreakUp(List<CmsInitiatedAndReceivedDto> pricipal2WalletBreakUp) {
		this.pricipal2WalletBreakUp = pricipal2WalletBreakUp;
	}

	public String getAmountsMatchedForP2W() {
		return amountsMatchedForP2W;
	}

	public void setAmountsMatchedForP2W(String amountsMatchedForP2W) {
		this.amountsMatchedForP2W = amountsMatchedForP2W;
	}

	public Double getTotalAmountInitiatedForP2B() {
		return totalAmountInitiatedForP2B;
	}

	public void setTotalAmountInitiatedForP2B(Double totalAmountInitiatedForP2B) {
		this.totalAmountInitiatedForP2B = totalAmountInitiatedForP2B;
	}

	public Double getTotalAmountReceivedP2W() {
		return totalAmountReceivedP2W;
	}

	public void setTotalAmountReceivedP2W(Double totalAmountReceivedP2W) {
		this.totalAmountReceivedP2W = totalAmountReceivedP2W;
	}

	public Double getTotalAmountInitiatedForP2W() {
		return totalAmountInitiatedForP2W;
	}

	public void setTotalAmountInitiatedForP2W(Double totalAmountInitiatedForP2W) {
		this.totalAmountInitiatedForP2W = totalAmountInitiatedForP2W;
	}

}
