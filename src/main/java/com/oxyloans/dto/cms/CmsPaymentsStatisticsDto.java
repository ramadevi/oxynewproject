package com.oxyloans.dto.cms;

public class CmsPaymentsStatisticsDto {

	private CmsInterestStatsDto interests;
	private CmsPrincipalStatsDto principal;
	private CmsPrincipalInterestStatsDto principalInterest;

	public CmsInterestStatsDto getInterests() {
		return interests;
	}

	public void setInterests(CmsInterestStatsDto interests) {
		this.interests = interests;
	}

	public CmsPrincipalStatsDto getPrincipal() {
		return principal;
	}

	public void setPrincipal(CmsPrincipalStatsDto principal) {
		this.principal = principal;
	}

	public CmsPrincipalInterestStatsDto getPrincipalInterest() {
		return principalInterest;
	}

	public void setPrincipalInterest(CmsPrincipalInterestStatsDto principalInterest) {
		this.principalInterest = principalInterest;
	}

}
