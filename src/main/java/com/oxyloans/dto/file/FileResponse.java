package com.oxyloans.dto.file;

import java.io.OutputStream;

public class FileResponse {

	private String fileName;

	private String filePath;

	private OutputStream outPutStream;

	private String downloadUrl;

	private String uploadedDate;

	private String uploadedType;

	public OutputStream getOutPutStream() {
		return outPutStream;
	}

	public void setOutPutStream(OutputStream outPutStream) {
		this.outPutStream = outPutStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getUploadedDate() {
		return uploadedDate;
	}

	public void setUploadedDate(String uploadedDate) {
		this.uploadedDate = uploadedDate;
	}

	public String getUploadedType() {
		return uploadedType;
	}

	public void setUploadedType(String uploadedType) {
		this.uploadedType = uploadedType;
	}

}
