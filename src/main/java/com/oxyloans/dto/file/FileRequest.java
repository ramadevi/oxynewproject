package com.oxyloans.dto.file;

import java.io.InputStream;

public class FileRequest {

	public static enum FileType {

		Kyc(null), Agreement("agreements"), EnachScheduledReport("EnachScheduledReport"),
		TransactionResponses("TransactionResponses"), LendersLoansInfo("LendersLoansInfo"),
		LenderScrowWalletDetails("LenderScrowWalletDetails"),
		CurrentMonthClosedAndActiveLenderInfo("CurrentMonthClosedAndActiveLenderInfo"), EXPERIAN(null),
		CICReport("CICREPORT"), Statement(null), thirtydaysbucket(null), sixtydaysbucket(null), ninetydaysbucket(null),
		npabucket(null), dealStructure(null), lenderLoanInformation(null), borrowerLoanInformation(null),
		lenderNoc(null), LenderReferenceInfo(null), LenderWalletInfo(null), Participatedlenders(null),
		ReferralBonusEarnedInfo("ReferralBonusEarnedInfo"), LenderInterestInfo(null), RenewalDetails(null),
		ParticipationInfo(null), PrincipalReturned(null), ReferalBonus(null), ReferralBonus(null), UTM(null),
		TransactionAlerts(null), RBIstatsInfo(null), LENDERINCOME(null), PartnerAgreementType(null), CMSFILE(null),
		ClosedDeals(null), borrowerNoc(null), RunningDealsInfo(null), ActiveAmountInfo(null), TotalLendersWallet(null),
		PartiallyClosedDealsInfo(null), LENDERFEE(null), MaximumAmountInfo(null), EquityDealsInfo(null),
		QUARTERLY(null), ReferralAmount(null), OPENDEALS(null), Invoice(null), FdDetails(null),
		newLenderInformation(null), LENDERFEEDETAILS(null), FeeInvoice(null), FdClosedDetails(null),LENDERFEEAMOUNT(null),FDAMOUNT(null),LENDERTOPFIFTYLIST(null);

		private String bucketPrefix;

		private FileType(String bucketPrefix) {
			this.bucketPrefix = bucketPrefix;
		}

		public String getBucketPrefix() {
			return bucketPrefix;
		}

		public void setBucketPrefix(String bucketPrefix) {
			this.bucketPrefix = bucketPrefix;
		}
	}

	private InputStream inputStream;
	private String filePath;
	private Integer userId;
	private String fileName;
	private FileType fileType = FileType.Kyc;
	private String filePrifix;

	public String getFilePrifix() {
		return filePrifix;
	}

	public void setFilePrifix(String filePrifix) {
		this.filePrifix = filePrifix;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
