package com.oxyloans.dto.paytm;

public class PaytmResponseDto {
	private String vanNumber;

	private Double amount;

	private String beneficiaryAccountnumber;

	private String beneficiaryIfsc;

	private String remitterAccountnumber;

	private String remitterIfsc;

	private String remitterName;

	private String bankTxnIdentifier;

	private String transactionRequestId;

	private String transferMode;

	private String transactionDate;

	private Integer borrowerId;

	private String borrowerName;

	public String getVanNumber() {
		return vanNumber;
	}

	public void setVanNumber(String vanNumber) {
		this.vanNumber = vanNumber;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getBeneficiaryAccountnumber() {
		return beneficiaryAccountnumber;
	}

	public void setBeneficiaryAccountnumber(String beneficiaryAccountnumber) {
		this.beneficiaryAccountnumber = beneficiaryAccountnumber;
	}

	public String getBeneficiaryIfsc() {
		return beneficiaryIfsc;
	}

	public void setBeneficiaryIfsc(String beneficiaryIfsc) {
		this.beneficiaryIfsc = beneficiaryIfsc;
	}

	public String getRemitterAccountnumber() {
		return remitterAccountnumber;
	}

	public void setRemitterAccountnumber(String remitterAccountnumber) {
		this.remitterAccountnumber = remitterAccountnumber;
	}

	public String getRemitterIfsc() {
		return remitterIfsc;
	}

	public void setRemitterIfsc(String remitterIfsc) {
		this.remitterIfsc = remitterIfsc;
	}

	public String getRemitterName() {
		return remitterName;
	}

	public void setRemitterName(String remitterName) {
		this.remitterName = remitterName;
	}

	public String getBankTxnIdentifier() {
		return bankTxnIdentifier;
	}

	public void setBankTxnIdentifier(String bankTxnIdentifier) {
		this.bankTxnIdentifier = bankTxnIdentifier;
	}

	public String getTransactionRequestId() {
		return transactionRequestId;
	}

	public void setTransactionRequestId(String transactionRequestId) {
		this.transactionRequestId = transactionRequestId;
	}

	public String getTransferMode() {
		return transferMode;
	}

	public void setTransferMode(String transferMode) {
		this.transferMode = transferMode;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

}
