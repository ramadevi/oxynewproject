package com.oxyloans.dto.paytm;

import java.math.BigInteger;
import java.util.List;

public class ListOfPaytmTransferedUsersDto {

	private Integer totalCount;

	private List<PaytmResponseDto> paytmResponseDto;

	private BigInteger sumOfPaytmAmount;

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public List<PaytmResponseDto> getPaytmResponseDto() {
		return paytmResponseDto;
	}

	public void setPaytmResponseDto(List<PaytmResponseDto> paytmResponseDto) {
		this.paytmResponseDto = paytmResponseDto;
	}

	public BigInteger getSumOfPaytmAmount() {
		return sumOfPaytmAmount;
	}

	public void setSumOfPaytmAmount(BigInteger sumOfPaytmAmount) {
		this.sumOfPaytmAmount = sumOfPaytmAmount;
	}

}
