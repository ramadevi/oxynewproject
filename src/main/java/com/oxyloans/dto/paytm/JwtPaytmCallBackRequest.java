package com.oxyloans.dto.paytm;

import com.oxyloans.response.user.Data;

public class JwtPaytmCallBackRequest {

	private Data data = new Data();

	private String iss = "PAYTMBANK";

	private String ca_id = "A0kdhktzth040";

	public Data getData() {
		return data;
	}

	public String getIss() {
		return iss;
	}

	public String getCa_id() {
		return ca_id;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public void setIss(String iss) {
		this.iss = iss;
	}

	public void setCa_id(String ca_id) {
		this.ca_id = ca_id;
	}

}
