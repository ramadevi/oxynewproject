package com.oxyloans.dto.loan;

public class FdSearchDownloadRequest {

	private String inputType;

	private String inputSubType;

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	public String getInputSubType() {
		return inputSubType;
	}

	public void setInputSubType(String inputSubType) {
		this.inputSubType = inputSubType;
	}

}
