package com.oxyloans.dto.loan;

import java.util.List;

public class SearchTypes {

	private List<String> searchTypes;

	public List<String> getSearchTypes() {
		return searchTypes;
	}

	public void setSearchTypes(List<String> searchTypes) {
		this.searchTypes = searchTypes;
	}

}
