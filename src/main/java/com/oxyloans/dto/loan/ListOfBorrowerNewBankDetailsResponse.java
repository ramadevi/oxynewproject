package com.oxyloans.dto.loan;

import java.util.List;

public class ListOfBorrowerNewBankDetailsResponse {

	private Integer count;

	private List<BorrowerNewBankDetailsResponseDto> borrowerNewBankDetailsResponseDto;

	public Integer getCount() {
		return count;
	}

	public List<BorrowerNewBankDetailsResponseDto> getBorrowerNewBankDetailsResponseDto() {
		return borrowerNewBankDetailsResponseDto;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void setBorrowerNewBankDetailsResponseDto(
			List<BorrowerNewBankDetailsResponseDto> borrowerNewBankDetailsResponseDto) {
		this.borrowerNewBankDetailsResponseDto = borrowerNewBankDetailsResponseDto;
	}

}
