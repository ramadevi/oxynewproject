package com.oxyloans.dto.loan;

import java.util.List;

public class PaymentUploadedRequestDto {

	private List<PaymentsInfo> paymentsInfo;

	public List<PaymentsInfo> getPaymentsInfo() {
		return paymentsInfo;
	}

	public void setPaymentsInfo(List<PaymentsInfo> paymentsInfo) {
		this.paymentsInfo = paymentsInfo;
	}

}
