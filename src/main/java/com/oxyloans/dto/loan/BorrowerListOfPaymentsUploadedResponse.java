package com.oxyloans.dto.loan;

public class BorrowerListOfPaymentsUploadedResponse {

	private String accountType;

	private Double amountPaid;

	private String approvedDate;

	private String documentStatus;

	private String url;

	public String getAccountType() {
		return accountType;
	}

	public Double getAmountPaid() {
		return amountPaid;
	}

	public String getApprovedDate() {
		return approvedDate;
	}

	public String getDocumentStatus() {
		return documentStatus;
	}

	public String getUrl() {
		return url;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public void setAmountPaid(Double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}

	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
