package com.oxyloans.dto.loan;

public class BorrowerNewBankDetailsRequestDto {

	private Integer userId;

	private String accountNumber;

	private String ifsc;

	private String city;

	private String branch;

	private String bankName;

	private String userName;

	private String bankChoosen;

	private String leadBy;

	private String consultancy;

	private Double roi;

	private String fundingType;

	private String country;

	private String university;

	private String studentMobileNumber;

	private Double fdAmount;

	private String createdDate;

	private String paymentsCollection;

	private String loanType;

	public Integer getUserId() {
		return userId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getIfsc() {
		return ifsc;
	}

	public String getCity() {
		return city;
	}

	public String getBranch() {
		return branch;
	}

	public String getBankName() {
		return bankName;
	}

	public String getUserName() {
		return userName;
	}

	public String getBankChoosen() {
		return bankChoosen;
	}

	public String getLeadBy() {
		return leadBy;
	}

	public String getConsultancy() {
		return consultancy;
	}

	public Double getRoi() {
		return roi;
	}

	public String getFundingType() {
		return fundingType;
	}

	public String getCountry() {
		return country;
	}

	public String getUniversity() {
		return university;
	}

	public String getStudentMobileNumber() {
		return studentMobileNumber;
	}

	public Double getFdAmount() {
		return fdAmount;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setBankChoosen(String bankChoosen) {
		this.bankChoosen = bankChoosen;
	}

	public void setLeadBy(String leadBy) {
		this.leadBy = leadBy;
	}

	public void setConsultancy(String consultancy) {
		this.consultancy = consultancy;
	}

	public void setRoi(Double roi) {
		this.roi = roi;
	}

	public void setFundingType(String fundingType) {
		this.fundingType = fundingType;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public void setStudentMobileNumber(String studentMobileNumber) {
		this.studentMobileNumber = studentMobileNumber;
	}

	public void setFdAmount(Double fdAmount) {
		this.fdAmount = fdAmount;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getPaymentsCollection() {
		return paymentsCollection;
	}

	public void setPaymentsCollection(String paymentsCollection) {
		this.paymentsCollection = paymentsCollection;
	}

	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

}
