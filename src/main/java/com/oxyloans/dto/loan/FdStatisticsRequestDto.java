package com.oxyloans.dto.loan;

import java.util.Date;

public class FdStatisticsRequestDto {

	private String type;

	private Date startDate;

	private Date endDate;

	public String getType() {
		return type;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
