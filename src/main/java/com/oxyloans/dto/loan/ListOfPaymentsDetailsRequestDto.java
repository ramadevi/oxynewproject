package com.oxyloans.dto.loan;

public class ListOfPaymentsDetailsRequestDto {

	private String bankType;

	private String status;

	private int pageNo;

	private int pageSize;

	public String getBankType() {
		return bankType;
	}

	public String getStatus() {
		return status;
	}

	public int getPageNo() {
		return pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

}
