package com.oxyloans.dto.loan;

public class InvoiceResponceDto {

	private String invoice;

	public String getInvoice() {
		return invoice;
	}

	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}

}
