package com.oxyloans.dto.loan;

import java.util.List;

public class BorrowersMappedToDealResponseDto {

	private String status;

	private List<BorrowerLoanResponseDto> borrowerLoanResponseDto;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<BorrowerLoanResponseDto> getBorrowerLoanResponseDto() {
		return borrowerLoanResponseDto;
	}

	public void setBorrowerLoanResponseDto(List<BorrowerLoanResponseDto> borrowerLoanResponseDto) {
		this.borrowerLoanResponseDto = borrowerLoanResponseDto;
	}

}
