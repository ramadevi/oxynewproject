package com.oxyloans.dto.loan;

public class BorrowersMappedToDealRequestDto {

	private int userId;

	private Double amountFromSystem;

	private Double feeAmount;
	
	private String fdFundTransferRemarks;

	public int getUserId() {
		return userId;
	}

	public Double getAmountFromSystem() {
		return amountFromSystem;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setAmountFromSystem(Double amountFromSystem) {
		this.amountFromSystem = amountFromSystem;
	}

	public Double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(Double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public String getFdFundTransferRemarks() {
		return fdFundTransferRemarks;
	}

	public void setFdFundTransferRemarks(String fdFundTransferRemarks) {
		this.fdFundTransferRemarks = fdFundTransferRemarks;
	}

}
