package com.oxyloans.dto.loan;

import java.util.List;

public class MonthWiseBorrowersFdResponseDto {

	private List<FdInformation> fdInformation;

	private String monthDownloadUrl;

	public List<FdInformation> getFdInformation() {
		return fdInformation;
	}

	public String getMonthDownloadUrl() {
		return monthDownloadUrl;
	}

	public void setFdInformation(List<FdInformation> fdInformation) {
		this.fdInformation = fdInformation;
	}

	public void setMonthDownloadUrl(String monthDownloadUrl) {
		this.monthDownloadUrl = monthDownloadUrl;
	}

}
