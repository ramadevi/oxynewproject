package com.oxyloans.dto.loan;

import java.util.List;
import java.util.Map;

public class FdCalculationResponseDto {

	private Integer id;

	private Integer userId;

	private Double fdAmount;

	private Double perDayInterest;

	private List<Map<String, Double>> calculation;

	public Integer getUserId() {
		return userId;
	}

	public Double getFdAmount() {
		return fdAmount;
	}

	public List<Map<String, Double>> getCalculation() {
		return calculation;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setFdAmount(Double fdAmount) {
		this.fdAmount = fdAmount;
	}

	public void setCalculation(List<Map<String, Double>> calculation) {
		this.calculation = calculation;
	}

	public Double getPerDayInterest() {
		return perDayInterest;
	}

	public void setPerDayInterest(Double perDayInterest) {
		this.perDayInterest = perDayInterest;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
