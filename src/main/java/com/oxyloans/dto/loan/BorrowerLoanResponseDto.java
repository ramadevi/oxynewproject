package com.oxyloans.dto.loan;

import java.util.List;
import java.util.Map;

public class BorrowerLoanResponseDto {

	private int id;

	private int borrowerId;

	private double loanAmount;

	private String accountNumber;

	private String userNameAccoringToBank;

	private String ifsc;

	private int dealId;
	
	private String fdFundTransferRemarks;

	private List<Map<String, String>> paymentsUrls;

	public int getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(int borrowerId) {
		this.borrowerId = borrowerId;
	}

	public double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getUserNameAccoringToBank() {
		return userNameAccoringToBank;
	}

	public void setUserNameAccoringToBank(String userNameAccoringToBank) {
		this.userNameAccoringToBank = userNameAccoringToBank;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public List<Map<String, String>> getPaymentsUrls() {
		return paymentsUrls;
	}

	public void setPaymentsUrls(List<Map<String, String>> paymentsUrls) {
		this.paymentsUrls = paymentsUrls;
	}

	public String getFdFundTransferRemarks() {
		return fdFundTransferRemarks;
	}

	public void setFdFundTransferRemarks(String fdFundTransferRemarks) {
		this.fdFundTransferRemarks = fdFundTransferRemarks;
	}

}
