package com.oxyloans.dto.loan;

import java.util.List;

public class FdSearchDownloadResponse {

	private String url;

	private List<FdInformation> listOfFds;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<FdInformation> getListOfFds() {
		return listOfFds;
	}

	public void setListOfFds(List<FdInformation> listOfFds) {
		this.listOfFds = listOfFds;
	}

}
