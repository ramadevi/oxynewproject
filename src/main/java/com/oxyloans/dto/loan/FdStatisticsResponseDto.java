package com.oxyloans.dto.loan;

import java.math.BigInteger;

public class FdStatisticsResponseDto {

	private Integer noOfFdsDone;

	private BigInteger valueOfFd;

	private Integer noOfActiveFds;

	private BigInteger noOfActiveFdsAmount;

	private BigInteger amountReceivedToIcici;

	private BigInteger amountReceivedToHdfc;

	private BigInteger totalFdClosedInterest;

	public Integer getNoOfFdsDone() {
		return noOfFdsDone;
	}

	public BigInteger getValueOfFd() {
		return valueOfFd;
	}

	public Integer getNoOfActiveFds() {
		return noOfActiveFds;
	}

	public BigInteger getAmountReceivedToIcici() {
		return amountReceivedToIcici;
	}

	public BigInteger getAmountReceivedToHdfc() {
		return amountReceivedToHdfc;
	}

	public BigInteger getTotalFdClosedInterest() {
		return totalFdClosedInterest;
	}

	public void setNoOfFdsDone(Integer noOfFdsDone) {
		this.noOfFdsDone = noOfFdsDone;
	}

	public void setValueOfFd(BigInteger valueOfFd) {
		this.valueOfFd = valueOfFd;
	}

	public void setNoOfActiveFds(Integer noOfActiveFds) {
		this.noOfActiveFds = noOfActiveFds;
	}

	public void setAmountReceivedToIcici(BigInteger amountReceivedToIcici) {
		this.amountReceivedToIcici = amountReceivedToIcici;
	}

	public void setAmountReceivedToHdfc(BigInteger amountReceivedToHdfc) {
		this.amountReceivedToHdfc = amountReceivedToHdfc;
	}

	public void setTotalFdClosedInterest(BigInteger totalFdClosedInterest) {
		this.totalFdClosedInterest = totalFdClosedInterest;
	}

	public BigInteger getNoOfActiveFdsAmount() {
		return noOfActiveFdsAmount;
	}

	public void setNoOfActiveFdsAmount(BigInteger noOfActiveFdsAmount) {
		this.noOfActiveFdsAmount = noOfActiveFdsAmount;
	}

}
