package com.oxyloans.dto.loan;

public class LenderInvestmentDataDtoResponse {

	private Integer userId;

	private String status;

	private Double lenderTotalParticipationAmount;

	private Integer participatedDealsCount;

	private Double participatedStudentDealsAmount;

	private Double participatedEscrowDeals;

	private Double participatedEquityDealsAmount;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getParticipatedEscrowDeals() {
		return participatedEscrowDeals;
	}

	public void setParticipatedEscrowDeals(Double participatedEscrowDeals) {
		this.participatedEscrowDeals = participatedEscrowDeals;
	}

	public Double getLenderTotalParticipationAmount() {
		return lenderTotalParticipationAmount;
	}

	public void setLenderTotalParticipationAmount(Double lenderTotalParticipationAmount) {
		this.lenderTotalParticipationAmount = lenderTotalParticipationAmount;
	}

	public Integer getParticipatedDealsCount() {
		return participatedDealsCount;
	}

	public void setParticipatedDealsCount(Integer participatedDealsCount) {
		this.participatedDealsCount = participatedDealsCount;
	}

	public Double getParticipatedStudentDealsAmount() {
		return participatedStudentDealsAmount;
	}

	public void setParticipatedStudentDealsAmount(Double participatedStudentDealsAmount) {
		this.participatedStudentDealsAmount = participatedStudentDealsAmount;
	}

	public Double getParticipatedEquityDealsAmount() {
		return participatedEquityDealsAmount;
	}

	public void setParticipatedEquityDealsAmount(Double participatedEquityDealsAmount) {
		this.participatedEquityDealsAmount = participatedEquityDealsAmount;
	}

}
