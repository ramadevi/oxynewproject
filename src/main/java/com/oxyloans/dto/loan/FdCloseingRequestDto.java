package com.oxyloans.dto.loan;

public class FdCloseingRequestDto {

	private int borrowerPaymentId;

	private Double totalInterestOnFd;

	private String fdClosedDate;

	private Double amountReturnedToRepayment;

	private Double amountReturnedToAnother;

	private String comments;

	public int getBorrowerPaymentId() {
		return borrowerPaymentId;
	}

	public Double getTotalInterestOnFd() {
		return totalInterestOnFd;
	}

	public void setBorrowerPaymentId(int borrowerPaymentId) {
		this.borrowerPaymentId = borrowerPaymentId;
	}

	public void setTotalInterestOnFd(Double totalInterestOnFd) {
		this.totalInterestOnFd = totalInterestOnFd;
	}

	public String getFdClosedDate() {
		return fdClosedDate;
	}

	public void setFdClosedDate(String fdClosedDate) {
		this.fdClosedDate = fdClosedDate;
	}

	public Double getAmountReturnedToRepayment() {
		return amountReturnedToRepayment;
	}

	public void setAmountReturnedToRepayment(Double amountReturnedToRepayment) {
		this.amountReturnedToRepayment = amountReturnedToRepayment;
	}

	public Double getAmountReturnedToAnother() {
		return amountReturnedToAnother;
	}

	public void setAmountReturnedToAnother(Double amountReturnedToAnother) {
		this.amountReturnedToAnother = amountReturnedToAnother;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
