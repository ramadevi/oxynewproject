package com.oxyloans.dto.loan;

import java.util.List;

public class ListOfPaymentsDetailsResponseDto {

	private Integer count;

	private List<PaymentsInfo> paymentsInfo;

	public Integer getCount() {
		return count;
	}

	public List<PaymentsInfo> getPaymentsInfo() {
		return paymentsInfo;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void setPaymentsInfo(List<PaymentsInfo> paymentsInfo) {
		this.paymentsInfo = paymentsInfo;
	}

}
