package com.oxyloans.dto.loan;

import java.math.BigInteger;
import java.util.Date;

public class FdInformation {

	private Date fdCreatedDate;

	private String validityDate;

	private String createdDate;

	private String accountNumber;

	private String ifscCode;

	private String bankName;

	private String location;

	private String leadBy;

	private String consultancyName;

	private String country;

	private String closeDate;

	private String fdStatus;

	private String university;

	private String name;

	private String borrowerId;

	private BigInteger fdFromSystem;

	private BigInteger totalFdAmount;

	private String dob;

	private Integer gender;

	private String panNumber;

	private String registeredDate;

	private String address;

	private String stateCode = "TS";

	private String pincode;

	private String registeredMobileNumber;

	private String fdClosedDate;

	private String fdType;

	private double roi;

	private double perDayInterest;

	private String feeInvoice;

	private double amountRetunredToRepayment;

	private double amountReturnedToAnother;

	private String comments;

	private String closingStatus;

	private Integer loanId;

	public Date getFdCreatedDate() {
		return fdCreatedDate;
	}

	public String getName() {
		return name;
	}

	public String getBorrowerId() {
		return borrowerId;
	}

	public BigInteger getFdFromSystem() {
		return fdFromSystem;
	}

	public BigInteger getTotalFdAmount() {
		return totalFdAmount;
	}

	public String getDob() {
		return dob;
	}

	public Integer getGender() {
		return gender;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public String getRegisteredDate() {
		return registeredDate;
	}

	public String getAddress() {
		return address;
	}

	public String getStateCode() {
		return stateCode;
	}

	public String getPincode() {
		return pincode;
	}

	public String getRegisteredMobileNumber() {
		return registeredMobileNumber;
	}

	public void setFdCreatedDate(Date fdCreatedDate) {
		this.fdCreatedDate = fdCreatedDate;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBorrowerId(String borrowerId) {
		this.borrowerId = borrowerId;
	}

	public void setFdFromSystem(BigInteger fdFromSystem) {
		this.fdFromSystem = fdFromSystem;
	}

	public void setTotalFdAmount(BigInteger totalFdAmount) {
		this.totalFdAmount = totalFdAmount;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public void setRegisteredDate(String registeredDate) {
		this.registeredDate = registeredDate;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public void setRegisteredMobileNumber(String registeredMobileNumber) {
		this.registeredMobileNumber = registeredMobileNumber;
	}

	public String getFdClosedDate() {
		return fdClosedDate;
	}

	public void setFdClosedDate(String fdClosedDate) {
		this.fdClosedDate = fdClosedDate;
	}

	public String getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLeadBy() {
		return leadBy;
	}

	public void setLeadBy(String leadBy) {
		this.leadBy = leadBy;
	}

	public String getConsultancyName() {
		return consultancyName;
	}

	public void setConsultancyName(String consultancyName) {
		this.consultancyName = consultancyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}

	public String getFdStatus() {
		return fdStatus;
	}

	public void setFdStatus(String fdStatus) {
		this.fdStatus = fdStatus;
	}

	public String getUniversity() {
		return university;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getFdType() {
		return fdType;
	}

	public void setFdType(String fdType) {
		this.fdType = fdType;
	}

	public double getRoi() {
		return roi;
	}

	public void setRoi(double roi) {
		this.roi = roi;
	}

	public double getPerDayInterest() {
		return perDayInterest;
	}

	public void setPerDayInterest(double perDayInterest) {
		this.perDayInterest = perDayInterest;
	}

	public String getFeeInvoice() {
		return feeInvoice;
	}

	public void setFeeInvoice(String feeInvoice) {
		this.feeInvoice = feeInvoice;
	}

	public double getAmountRetunredToRepayment() {
		return amountRetunredToRepayment;
	}

	public void setAmountRetunredToRepayment(double amountRetunredToRepayment) {
		this.amountRetunredToRepayment = amountRetunredToRepayment;
	}

	public double getAmountReturnedToAnother() {
		return amountReturnedToAnother;
	}

	public void setAmountReturnedToAnother(double amountReturnedToAnother) {
		this.amountReturnedToAnother = amountReturnedToAnother;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getClosingStatus() {
		return closingStatus;
	}

	public void setClosingStatus(String closingStatus) {
		this.closingStatus = closingStatus;
	}

	public Integer getLoanId() {
		return loanId;
	}

	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}

}
