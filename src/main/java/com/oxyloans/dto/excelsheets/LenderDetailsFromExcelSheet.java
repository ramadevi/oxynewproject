package com.oxyloans.dto.excelsheets;

public class LenderDetailsFromExcelSheet {

	private String debitAccount;

	private String accountNumber;

	private String lenderName;

	private String payMode;

	private String lenderIfsc;

	public String getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public String getPayMode() {
		return payMode;
	}

	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	public String getLenderIfsc() {
		return lenderIfsc;
	}

	public void setLenderIfsc(String lenderIfsc) {
		this.lenderIfsc = lenderIfsc;
	}

}
