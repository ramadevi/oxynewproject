package com.oxyloans.dto.excelsheets;

public class OxyTransactionDetailsFromExcelSheetsResponse {
	private String status;

	private Integer countValueForUpdateLender;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getCountValueForUpdateLender() {
		return countValueForUpdateLender;
	}

	public void setCountValueForUpdateLender(Integer countValueForUpdateLender) {
		this.countValueForUpdateLender = countValueForUpdateLender;
	}

}
