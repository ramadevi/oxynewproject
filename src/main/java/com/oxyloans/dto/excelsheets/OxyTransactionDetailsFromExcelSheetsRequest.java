package com.oxyloans.dto.excelsheets;

public class OxyTransactionDetailsFromExcelSheetsRequest {
	private String debitedAccountType;

	private String debitedTowords;

	private String transferredDate;

	private String remarks;

	private String fileName;

	public String getDebitedAccountType() {
		return debitedAccountType;
	}

	public void setDebitedAccountType(String debitedAccountType) {
		this.debitedAccountType = debitedAccountType;
	}

	public String getDebitedTowords() {
		return debitedTowords;
	}

	public void setDebitedTowords(String debitedTowords) {
		this.debitedTowords = debitedTowords;
	}

	public String getTransferredDate() {
		return transferredDate;
	}

	public void setTransferredDate(String transferredDate) {
		this.transferredDate = transferredDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
