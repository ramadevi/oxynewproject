package com.oxyloans.dto.mobileDevice;

public class MobileDeviceResponseDto {

	private String uuid;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
