package com.oxyloans.dto.lender.wallet;

import java.util.Date;

public class LenderMemberShipDetails {

	private Integer id;

	private Integer userId;

	private String scrowAccountNumber;

	private Double transactionAmount;

	private String transactionDate;

	private String status;

	private Date createdDate;

	private String createdBy;

	private String lenderId;

	private String firstName;

	private String lastName;

	private String comments;

	private String LenderFeeLoadedDate;

	private String validityDate;

	private String lenderRenewalStatus;

	private String feeStatus;

	private String mobileNumber;

	private String lenderMembership;

	private String paymentType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getScrowAccountNumber() {
		return scrowAccountNumber;
	}

	public void setScrowAccountNumber(String scrowAccountNumber) {
		this.scrowAccountNumber = scrowAccountNumber;
	}

	public Double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLenderId() {
		return lenderId;
	}

	public void setLenderId(String lenderId) {
		this.lenderId = lenderId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getLenderFeeLoadedDate() {
		return LenderFeeLoadedDate;
	}

	public void setLenderFeeLoadedDate(String lenderFeeLoadedDate) {
		LenderFeeLoadedDate = lenderFeeLoadedDate;
	}

	public String getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}

	public String getLenderRenewalStatus() {
		return lenderRenewalStatus;
	}

	public void setLenderRenewalStatus(String lenderRenewalStatus) {
		this.lenderRenewalStatus = lenderRenewalStatus;
	}

	public String getFeeStatus() {
		return feeStatus;
	}

	public void setFeeStatus(String feeStatus) {
		this.feeStatus = feeStatus;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getLenderMembership() {
		return lenderMembership;
	}

	public void setLenderMembership(String lenderMembership) {
		this.lenderMembership = lenderMembership;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

}
