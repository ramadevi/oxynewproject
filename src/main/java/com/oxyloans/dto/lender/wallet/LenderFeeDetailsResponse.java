package com.oxyloans.dto.lender.wallet;

import java.util.Date;

public class LenderFeeDetailsResponse {

	private Integer id;

	private Double feeAmount;

	private Double feeAmountWithGst;

	private boolean isCurrent;

	private String lenderFeePayments;

	private Date currentOn = new Date();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(Double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public Double getFeeAmountWithGst() {
		return feeAmountWithGst;
	}

	public void setFeeAmountWithGst(Double feeAmountWithGst) {
		this.feeAmountWithGst = feeAmountWithGst;
	}

	public boolean isCurrent() {
		return isCurrent;
	}

	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public String getLenderFeePayments() {
		return lenderFeePayments;
	}

	public void setLenderFeePayments(String lenderFeePayments) {
		this.lenderFeePayments = lenderFeePayments;
	}

	public Date getCurrentOn() {
		return currentOn;
	}

	public void setCurrentOn(Date currentOn) {
		this.currentOn = currentOn;
	}

}
