package com.oxyloans.dto.lender.wallet;

public class WalletRequestDto {

	private Integer userId;

	private Integer dealId;

	private Integer feeAmount;

	private String type;

	private Integer id;

	private Integer pageNo;

	private Integer pageSize;

	private String lenderFeePayments;

	private String paidFrom;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Integer getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(Integer feeAmount) {
		this.feeAmount = feeAmount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getLenderFeePayments() {
		return lenderFeePayments;
	}

	public void setLenderFeePayments(String lenderFeePayments) {
		this.lenderFeePayments = lenderFeePayments;
	}

	public String getPaidFrom() {
		return paidFrom;
	}

	public void setPaidFrom(String paidFrom) {
		this.paidFrom = paidFrom;
	}

}
