package com.oxyloans.dto.lender.wallet;

import java.util.List;

public class LenderFeeTransactions {
	
	private List<LenderFeeTransactionResponse> listOfTransactions;
	
	private Integer count;

	public List<LenderFeeTransactionResponse> getListOfTransactions() {
		return listOfTransactions;
	}

	public void setListOfTransactions(List<LenderFeeTransactionResponse> listOfTransactions) {
		this.listOfTransactions = listOfTransactions;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
	

}
