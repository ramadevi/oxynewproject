package com.oxyloans.dto.lender.wallet;

public class UpdateLenderFeeData {

	private Integer userId;

	private String validityDate;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}

}
