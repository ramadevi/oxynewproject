package com.oxyloans.dto.lender.wallet;

import java.util.List;

import com.oxyloans.icici.dto.BeforeApprovalFiles;

public class LenderFeeFilesResponse {

	private String lenderExcelDownload;

	private List<BeforeApprovalFiles> beforeLenderFee;

	public String getLenderExcelDownload() {
		return lenderExcelDownload;
	}

	public void setLenderExcelDownload(String lenderExcelDownload) {
		this.lenderExcelDownload = lenderExcelDownload;
	}

	public List<BeforeApprovalFiles> getBeforeLenderFee() {
		return beforeLenderFee;
	}

	public void setBeforeLenderFee(List<BeforeApprovalFiles> beforeLenderFee) {
		this.beforeLenderFee = beforeLenderFee;
	}

	
}
