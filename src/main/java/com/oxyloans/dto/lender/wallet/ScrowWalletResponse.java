package com.oxyloans.dto.lender.wallet;

import java.util.List;

public class ScrowWalletResponse {

	private List<ScrowLenderTransactionResponse> results;

	private Integer totalCount;

	public List<ScrowLenderTransactionResponse> getResults() {
		return results;
	}

	public void setResults(List<ScrowLenderTransactionResponse> results) {
		this.results = results;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

}
