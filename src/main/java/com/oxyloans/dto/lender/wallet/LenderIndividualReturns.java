package com.oxyloans.dto.lender.wallet;

public class LenderIndividualReturns {
	private Integer sno;

	private String remarksFromSheet;

	private double amountPaid;

	private String paidDate;

	private String remarksFromAdmin;

	public String getRemarksFromSheet() {
		return remarksFromSheet;
	}

	public void setRemarksFromSheet(String remarksFromSheet) {
		this.remarksFromSheet = remarksFromSheet;
	}

	public String getRemarksFromAdmin() {
		return remarksFromAdmin;
	}

	public void setRemarksFromAdmin(String remarksFromAdmin) {
		this.remarksFromAdmin = remarksFromAdmin;
	}

	public double getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public Integer getSno() {
		return sno;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

}
