package com.oxyloans.dto.lender.wallet.transfer.dto;

import java.util.List;

public class ListOfWalletToWalletTransactionsResponse {

	private List<WalletTransferLenderToLenderResponseDto> walletTransferLenderToLenderResponseDto;

	private Integer totalCount;

	private String senderName;

	public List<WalletTransferLenderToLenderResponseDto> getWalletTransferLenderToLenderResponseDto() {
		return walletTransferLenderToLenderResponseDto;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setWalletTransferLenderToLenderResponseDto(
			List<WalletTransferLenderToLenderResponseDto> walletTransferLenderToLenderResponseDto) {
		this.walletTransferLenderToLenderResponseDto = walletTransferLenderToLenderResponseDto;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

}
