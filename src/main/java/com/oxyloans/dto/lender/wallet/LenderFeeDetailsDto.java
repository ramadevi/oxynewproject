package com.oxyloans.dto.lender.wallet;

import java.util.List;

public class LenderFeeDetailsDto {

	private List<LenderFeeTransactionResponse> lenderFeeTransactionResponse;

	private String lenderExcelDownload;

	public List<LenderFeeTransactionResponse> getLenderFeeTransactionResponse() {
		return lenderFeeTransactionResponse;
	}

	public void setLenderFeeTransactionResponse(List<LenderFeeTransactionResponse> lenderFeeTransactionResponse) {
		this.lenderFeeTransactionResponse = lenderFeeTransactionResponse;
	}

	public String getLenderExcelDownload() {
		return lenderExcelDownload;
	}

	public void setLenderExcelDownload(String lenderExcelDownload) {
		this.lenderExcelDownload = lenderExcelDownload;
	}
}
