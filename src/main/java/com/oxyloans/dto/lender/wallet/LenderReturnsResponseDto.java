package com.oxyloans.dto.lender.wallet;

import java.util.List;

public class LenderReturnsResponseDto {
	private double totalPrincipalOnFirstApril;

	private double sumOfInterestAmount;

	private double totalPrincipalInLending;

	private List<LenderCreditedDetails> lenderPricipalAmountThroughWalletTransaction;

	private List<LenderIndividualReturns> lenderInterestEarned;

	private List<LenderIndividualReturns> lenderPrincipalReturned;

	private List<LenderIndividualReturns> lenderEmiReturned;

	private List<LenderIndividualReturns> lenderWithDraw;

	private List<LenderIndividualReturns> lenderReLend;

	private List<LenderIndividualReturns> lenderPoolingInterest;

	public double getTotalPrincipalOnFirstApril() {
		return totalPrincipalOnFirstApril;
	}

	public void setTotalPrincipalOnFirstApril(double totalPrincipalOnFirstApril) {
		this.totalPrincipalOnFirstApril = totalPrincipalOnFirstApril;
	}

	public List<LenderCreditedDetails> getLenderPricipalAmountThroughWalletTransaction() {
		return lenderPricipalAmountThroughWalletTransaction;
	}

	public void setLenderPricipalAmountThroughWalletTransaction(
			List<LenderCreditedDetails> lenderPricipalAmountThroughWalletTransaction) {
		this.lenderPricipalAmountThroughWalletTransaction = lenderPricipalAmountThroughWalletTransaction;
	}

	public List<LenderIndividualReturns> getLenderInterestEarned() {
		return lenderInterestEarned;
	}

	public void setLenderInterestEarned(List<LenderIndividualReturns> lenderInterestEarned) {
		this.lenderInterestEarned = lenderInterestEarned;
	}

	public List<LenderIndividualReturns> getLenderPrincipalReturned() {
		return lenderPrincipalReturned;
	}

	public void setLenderPrincipalReturned(List<LenderIndividualReturns> lenderPrincipalReturned) {
		this.lenderPrincipalReturned = lenderPrincipalReturned;
	}

	public List<LenderIndividualReturns> getLenderEmiReturned() {
		return lenderEmiReturned;
	}

	public void setLenderEmiReturned(List<LenderIndividualReturns> lenderEmiReturned) {
		this.lenderEmiReturned = lenderEmiReturned;
	}

	public List<LenderIndividualReturns> getLenderWithDraw() {
		return lenderWithDraw;
	}

	public void setLenderWithDraw(List<LenderIndividualReturns> lenderWithDraw) {
		this.lenderWithDraw = lenderWithDraw;
	}

	public List<LenderIndividualReturns> getLenderReLend() {
		return lenderReLend;
	}

	public void setLenderReLend(List<LenderIndividualReturns> lenderReLend) {
		this.lenderReLend = lenderReLend;
	}

	public List<LenderIndividualReturns> getLenderPoolingInterest() {
		return lenderPoolingInterest;
	}

	public void setLenderPoolingInterest(List<LenderIndividualReturns> lenderPoolingInterest) {
		this.lenderPoolingInterest = lenderPoolingInterest;
	}

	public double getSumOfInterestAmount() {
		return sumOfInterestAmount;
	}

	public void setSumOfInterestAmount(double sumOfInterestAmount) {
		this.sumOfInterestAmount = sumOfInterestAmount;
	}

	public double getTotalPrincipalInLending() {
		return totalPrincipalInLending;
	}

	public void setTotalPrincipalInLending(double totalPrincipalInLending) {
		this.totalPrincipalInLending = totalPrincipalInLending;
	}

}
