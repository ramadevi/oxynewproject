package com.oxyloans.dto.lender.wallet.transfer.dto;

public class ActionOnRequestedTransaction {

	private Integer id;

	private String status;

	public Integer getId() {
		return id;
	}

	public String getStatus() {
		return status;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
