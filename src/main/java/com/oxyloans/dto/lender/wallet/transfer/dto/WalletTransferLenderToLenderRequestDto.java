package com.oxyloans.dto.lender.wallet.transfer.dto;

public class WalletTransferLenderToLenderRequestDto {

	private Integer senderId;

	private Integer receiverId;

	private double amount;
	private String status;
	private Integer id;

	public Integer getSenderId() {
		return senderId;
	}

	public Integer getReceiverId() {
		return receiverId;
	}

	public double getAmount() {
		return amount;
	}

	public void setSenderId(Integer senderId) {
		this.senderId = senderId;
	}

	public void setReceiverId(Integer receiverId) {
		this.receiverId = receiverId;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	

}
