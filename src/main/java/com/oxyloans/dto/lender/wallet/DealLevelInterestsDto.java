package com.oxyloans.dto.lender.wallet;

import java.util.Date;

public class DealLevelInterestsDto {

	private Integer dealId;

	private Date paymentDate;

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

}
