package com.oxyloans.dto.lender.wallet;

public class PrincpleReturnsResponseDto {
	
	
	private double totalReturnedAmount;
	private double lenderTotalInvestment;
	
	public double getTotalReturnedAmount() {
		return totalReturnedAmount;
	}
	public void setTotalReturnedAmount(double totalReturnedAmount) {
		this.totalReturnedAmount = totalReturnedAmount;
	}
	public double getLenderTotalInvestment() {
		return lenderTotalInvestment;
	}
	public void setLenderTotalInvestment(double lenderTotalInvestment) {
		this.lenderTotalInvestment = lenderTotalInvestment;
	}

}
