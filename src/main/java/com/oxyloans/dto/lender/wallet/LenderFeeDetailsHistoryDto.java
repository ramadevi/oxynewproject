package com.oxyloans.dto.lender.wallet;

public class LenderFeeDetailsHistoryDto {

	private Integer id;

	private Double feeAmount;

	private Double feeAmountWithGst;

	private boolean isCurrent;

	private String lenderFeePayments;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(Double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public Double getFeeAmountWithGst() {
		return feeAmountWithGst;
	}

	public void setFeeAmountWithGst(Double feeAmountWithGst) {
		this.feeAmountWithGst = feeAmountWithGst;
	}

	public boolean isCurrent() {
		return isCurrent;
	}

	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public String getLenderFeePayments() {
		return lenderFeePayments;
	}

	public void setLenderFeePayments(String lenderFeePayments) {
		this.lenderFeePayments = lenderFeePayments;
	}

}
