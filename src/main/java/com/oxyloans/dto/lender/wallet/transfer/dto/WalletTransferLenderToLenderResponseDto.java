package com.oxyloans.dto.lender.wallet.transfer.dto;

import java.math.BigInteger;

public class WalletTransferLenderToLenderResponseDto {

	private String status;

	private Integer senderId;

	private String senderName;

	private Integer receiverId;

	private String receiverName;

	private BigInteger amount;

	private String requestedDate;

	private Integer id;

	private String transformedDate;

	public String getStatus() {
		return status;
	}

	public Integer getSenderId() {
		return senderId;
	}

	public String getSenderName() {
		return senderName;
	}

	public Integer getReceiverId() {
		return receiverId;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public BigInteger getAmount() {
		return amount;
	}

	public String getRequestedDate() {
		return requestedDate;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setSenderId(Integer senderId) {
		this.senderId = senderId;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public void setReceiverId(Integer receiverId) {
		this.receiverId = receiverId;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}

	public void setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTransformedDate() {
		return transformedDate;
	}

	public void setTransformedDate(String transformedDate) {
		this.transformedDate = transformedDate;
	}

}
