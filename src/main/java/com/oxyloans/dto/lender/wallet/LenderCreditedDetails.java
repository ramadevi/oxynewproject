package com.oxyloans.dto.lender.wallet;

public class LenderCreditedDetails {

	private Integer sno;

	private Double amountCredited;

	private String paidDate;

	public Double getAmountCredited() {
		return amountCredited;
	}

	public void setAmountCredited(Double amountCredited) {
		this.amountCredited = amountCredited;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public Integer getSno() {
		return sno;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

}
