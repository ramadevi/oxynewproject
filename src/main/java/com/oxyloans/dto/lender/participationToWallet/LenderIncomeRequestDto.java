package com.oxyloans.dto.lender.participationToWallet;

import java.util.Date;

public class LenderIncomeRequestDto {

	private Date startDate;

	private Date endDate;

	private String inputType;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}
}
