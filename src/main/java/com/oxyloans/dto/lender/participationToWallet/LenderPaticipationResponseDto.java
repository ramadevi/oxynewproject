package com.oxyloans.dto.lender.participationToWallet;

import java.math.BigInteger;

public class LenderPaticipationResponseDto {

	private String status;

	private Integer userId;

	private BigInteger interestAmount;

	private BigInteger walletLoadedAmount;

	private String userName;

	private String accountNumber;

	private String ifscCode;
	
	private String downloadUrl;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public BigInteger getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(BigInteger interestAmount) {
		this.interestAmount = interestAmount;
	}

	public BigInteger getWalletLoadedAmount() {
		return walletLoadedAmount;
	}

	public void setWalletLoadedAmount(BigInteger walletLoadedAmount) {
		this.walletLoadedAmount = walletLoadedAmount;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

}
