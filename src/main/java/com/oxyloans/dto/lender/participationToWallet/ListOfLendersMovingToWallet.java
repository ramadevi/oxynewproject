package com.oxyloans.dto.lender.participationToWallet;

import java.util.List;

public class ListOfLendersMovingToWallet {

	private List<LenderPaticipationRequestDto> lenderPaticipationRequest;

	private String debitAccountNo;

	public List<LenderPaticipationRequestDto> getLenderPaticipationRequest() {
		return lenderPaticipationRequest;
	}

	public void setLenderPaticipationRequest(List<LenderPaticipationRequestDto> lenderPaticipationRequest) {
		this.lenderPaticipationRequest = lenderPaticipationRequest;
	}

	public String getDebitAccountNo() {
		return debitAccountNo;
	}

	public void setDebitAccountNo(String debitAccountNo) {
		this.debitAccountNo = debitAccountNo;
	}

}
