package com.oxyloans.dto.lender.participationToWallet;

import java.math.BigInteger;

public class LenderIncomeResponseDto {

	private Integer sNo;

	private Integer dealId;

	private String dealName;

	private BigInteger participatedAmount;

	private String loanActiveDate;

	private BigInteger incomeEarnedToDeal;

	private String loanStatus;

	public Integer getsNo() {
		return sNo;
	}

	public void setsNo(Integer sNo) {
		this.sNo = sNo;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public BigInteger getParticipatedAmount() {
		return participatedAmount;
	}

	public void setParticipatedAmount(BigInteger participatedAmount) {
		this.participatedAmount = participatedAmount;
	}

	public String getLoanActiveDate() {
		return loanActiveDate;
	}

	public void setLoanActiveDate(String loanActiveDate) {
		this.loanActiveDate = loanActiveDate;
	}

	public BigInteger getIncomeEarnedToDeal() {
		return incomeEarnedToDeal;
	}

	public void setIncomeEarnedToDeal(BigInteger incomeEarnedToDeal) {
		this.incomeEarnedToDeal = incomeEarnedToDeal;
	}

	public String getLoanStatus() {
		return loanStatus;
	}

	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}

}
