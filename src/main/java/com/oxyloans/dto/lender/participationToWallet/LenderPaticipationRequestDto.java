package com.oxyloans.dto.lender.participationToWallet;

public class LenderPaticipationRequestDto {

	private Integer userId;

	private Integer dealId;

	private double currentAmount;

	private double previousRequestedAmount;

	private double movingToWallet;

	private String accountNumber;

	private String ifscCode;

	private String userName;
	private int daysDifference;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public double getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(double currentAmount) {
		this.currentAmount = currentAmount;
	}

	public double getPreviousRequestedAmount() {
		return previousRequestedAmount;
	}

	public void setPreviousRequestedAmount(double previousRequestedAmount) {
		this.previousRequestedAmount = previousRequestedAmount;
	}

	public double getMovingToWallet() {
		return movingToWallet;
	}

	public void setMovingToWallet(double movingToWallet) {
		this.movingToWallet = movingToWallet;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getDaysDifference() {
		return daysDifference;
	}

	public void setDaysDifference(int daysDifference) {
		this.daysDifference = daysDifference;
	}

}
