package com.oxyloans.dto.lender.withdrawalfunds.dto;

public class LenderWithdrawalFundsFromDealsRequestDto {

	private Integer dealId;

	private Integer userId;

	private double currentAmount;

	private double requestedAmount;

	private double withDrawalFunds;

	private Integer id;

	private String paidDate;

	private String status;

	private String accountType;
	
	private String requestFrom;

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public double getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(double currentAmount) {
		this.currentAmount = currentAmount;
	}

	public double getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}

	public double getWithDrawalFunds() {
		return withDrawalFunds;
	}

	public void setWithDrawalFunds(double withDrawalFunds) {
		this.withDrawalFunds = withDrawalFunds;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getRequestFrom() {
		return requestFrom;
	}

	public void setRequestFrom(String requestFrom) {
		this.requestFrom = requestFrom;
	}

}
