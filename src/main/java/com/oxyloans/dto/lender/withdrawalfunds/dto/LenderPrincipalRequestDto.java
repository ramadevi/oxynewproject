package com.oxyloans.dto.lender.withdrawalfunds.dto;

public class LenderPrincipalRequestDto {

	private Integer userId;

	private Double currentAmount;

	private Double pricipalReturnAmount;

	private String lenderName;

	private String accountNumber;

	private String ifscCode;

	private Double amount;

	private Integer differenceInDays;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Double getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(Double currentAmount) {
		this.currentAmount = currentAmount;
	}

	public Double getPricipalReturnAmount() {
		return pricipalReturnAmount;
	}

	public void setPricipalReturnAmount(Double pricipalReturnAmount) {
		this.pricipalReturnAmount = pricipalReturnAmount;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Integer getDifferenceInDays() {
		return differenceInDays;
	}

	public void setDifferenceInDays(Integer differenceInDays) {
		this.differenceInDays = differenceInDays;
	}

}
