package com.oxyloans.dto.lender.withdrawalfunds.dto;

import java.util.List;

public class ListOfLendersWithdrawalFundsInfo {

	private Integer Id;

	private List<LenderWithdrawalFundsFromDealsResponseDto> lenderWithdrawalFundsFromDealsResponseDto;

	private Integer totalCount;

	private String status;

	private String accountType;

	private String amountType;

	public List<LenderWithdrawalFundsFromDealsResponseDto> getLenderWithdrawalFundsFromDealsResponseDto() {
		return lenderWithdrawalFundsFromDealsResponseDto;
	}

	public void setLenderWithdrawalFundsFromDealsResponseDto(
			List<LenderWithdrawalFundsFromDealsResponseDto> lenderWithdrawalFundsFromDealsResponseDto) {
		this.lenderWithdrawalFundsFromDealsResponseDto = lenderWithdrawalFundsFromDealsResponseDto;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

}
