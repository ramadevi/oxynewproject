package com.oxyloans.dto.lender.withdrawalfunds.dto;

public class LenderPrincipalResponseDto {
	private String status;

	private Integer differenceInDays;

	private double interestAmount;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getDifferenceInDays() {
		return differenceInDays;
	}

	public void setDifferenceInDays(Integer differenceInDays) {
		this.differenceInDays = differenceInDays;
	}

	public double getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(double interestAmount) {
		this.interestAmount = interestAmount;
	}

}
