package com.oxyloans.dto.lender.withdrawalfunds.dto;

import com.oxyloans.request.PageDto;

public class LenderWithdrawalFundsSearchRequest {

	private Integer userId;
	private String firstName;
	private String lastName;
	private PageDto page;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public PageDto getPage() {
		return page;
	}

	public void setPage(PageDto page) {
		this.page = page;
	}

}
