package com.oxyloans.dto.lender.withdrawalfunds.dto;

import java.util.List;

public class LenderPrincipal {

	private List<LenderPrincipalRequestDto> lenderPrincipalRequestDto;

	private String date;

	private String status;

	private double amount;

	private String accountType;

	public List<LenderPrincipalRequestDto> getLenderPrincipalRequestDto() {
		return lenderPrincipalRequestDto;
	}

	public void setLenderPrincipalRequestDto(List<LenderPrincipalRequestDto> lenderPrincipalRequestDto) {
		this.lenderPrincipalRequestDto = lenderPrincipalRequestDto;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

}
