package com.oxyloans.dto.autoinvest;

import com.oxyloans.request.PageDto;

public class LenderAutoInvestConfigSearchRequest {

	private Integer userId;
	private String firstName;
	private String lastName;
	private PageDto page;

	public PageDto getPage() {
		return page;
	}

	public void setPage(PageDto page) {
		this.page = page;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
