package com.oxyloans.dto.autoinvest;

import java.util.List;

import com.oxyloans.entity.borrowers.deals.dto.LenderPaticipatedResponseDto;

public class LenderParticipationUsingAutoLendingResponse {

	private int totalCount;

	private List<LenderPaticipatedResponseDto> lenderPaticipatedResponseDto;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<LenderPaticipatedResponseDto> getLenderPaticipatedResponseDto() {
		return lenderPaticipatedResponseDto;
	}

	public void setLenderPaticipatedResponseDto(List<LenderPaticipatedResponseDto> lenderPaticipatedResponseDto) {
		this.lenderPaticipatedResponseDto = lenderPaticipatedResponseDto;
	}
}
