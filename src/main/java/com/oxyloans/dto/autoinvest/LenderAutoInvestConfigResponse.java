package com.oxyloans.dto.autoinvest;

import com.oxyloans.response.user.UserResponse;

public class LenderAutoInvestConfigResponse {

	private Integer id;
	private Integer userId;
	private String riskCategory;
	private String gender;
	private String oxyScore;
	private String city;
	private String employmentType;
	private String salaryRange;
	private Double maxAmount;
	private String duration;
	private String rateOfInterest;
	private Boolean autoEsigin = false;
	private Boolean termsAndConditions = false;
	private String comments;
	private String status;
	private String message;
	private String createdOn;
	private String firstName;
	private String lastName;
	private String panNumber;
	private String address;
	private String email;
	private String mobileNumber;
	private String lenderCity;
	private Double lenderWalletAmount;
	private UserResponse user;

	public Double getLenderWalletAmount() {
		return lenderWalletAmount;
	}

	public void setLenderWalletAmount(Double lenderWalletAmount) {
		this.lenderWalletAmount = lenderWalletAmount;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getLenderCity() {
		return lenderCity;
	}

	public void setLenderCity(String lenderCity) {
		this.lenderCity = lenderCity;
	}

	public UserResponse getUser() {
		return user;
	}

	public void setUser(UserResponse user) {
		this.user = user;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	private String errorMessageDescription;

	public String getErrorMessageDescription() {
		return errorMessageDescription;
	}

	public void setErrorMessageDescription(String errorMessageDescription) {
		this.errorMessageDescription = errorMessageDescription;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getRiskCategory() {
		return riskCategory;
	}

	public void setRiskCategory(String riskCategory) {
		this.riskCategory = riskCategory;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	public String getSalaryRange() {
		return salaryRange;
	}

	public void setSalaryRange(String salaryRange) {
		this.salaryRange = salaryRange;
	}

	public Double getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(Double maxAmount) {
		this.maxAmount = maxAmount;
	}

	public String getOxyScore() {
		return oxyScore;
	}

	public void setOxyScore(String oxyScore) {
		this.oxyScore = oxyScore;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(String rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public Boolean getAutoEsigin() {
		return autoEsigin;
	}

	public void setAutoEsigin(Boolean autoEsigin) {
		this.autoEsigin = autoEsigin;
	}

	public Boolean getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(Boolean termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
