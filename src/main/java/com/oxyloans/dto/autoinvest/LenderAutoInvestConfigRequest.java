package com.oxyloans.dto.autoinvest;

public class LenderAutoInvestConfigRequest {
	private Integer userId;

	private String dealType;

	private Double maxAmount;

	private String principalReturningAccountType;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public Double getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(Double maxAmount) {
		this.maxAmount = maxAmount;
	}

	public String getPrincipalReturningAccountType() {
		return principalReturningAccountType;
	}

	public void setPrincipalReturningAccountType(String principalReturningAccountType) {
		this.principalReturningAccountType = principalReturningAccountType;
	}

}
