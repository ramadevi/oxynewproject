package com.oxyloans.dto.autoinvest;

public class AutoInvestBorrowersList {

	private String firstName;
	private String lastName;
	private String gender;
	private String mobileNumber;
	private String email;
	private String city;
	private String grade;
	private Integer score;
	private String employment;
	private Integer net_monthly_income;
	private Double rate_of_interest;
	private Integer duration;
	private Integer userId;
	private Double loanRequestAmount;

	public Double getLoanRequestAmount() {
		return loanRequestAmount;
	}

	public void setLoanRequestAmount(Double loanRequestAmount) {
		this.loanRequestAmount = loanRequestAmount;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getEmployment() {
		return employment;
	}

	public void setEmployment(String employment) {
		this.employment = employment;
	}

	public Integer getNet_monthly_income() {
		return net_monthly_income;
	}

	public void setNet_monthly_income(Integer net_monthly_income) {
		this.net_monthly_income = net_monthly_income;
	}

	public Double getRate_of_interest() {
		return rate_of_interest;
	}

	public void setRate_of_interest(Double rate_of_interest) {
		this.rate_of_interest = rate_of_interest;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

}
