package com.oxyloans.dto.autoinvest;

import java.util.List;

import com.oxyloans.response.user.UserResponse;

public class AutoInvestConfigResponse {

	List<LenderAutoInvestConfigResponse> configHistory;
	private UserResponse user;

	public List<LenderAutoInvestConfigResponse> getConfigHistory() {
		return configHistory;
	}

	public void setConfigHistory(List<LenderAutoInvestConfigResponse> configHistory) {
		this.configHistory = configHistory;
	}

	public UserResponse getUser() {
		return user;
	}

	public void setUser(UserResponse user) {
		this.user = user;
	}

}
