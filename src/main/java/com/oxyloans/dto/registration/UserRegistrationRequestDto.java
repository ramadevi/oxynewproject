package com.oxyloans.dto.registration;

public class UserRegistrationRequestDto {

	private String primaryType;

	private String name;

	private String email;

	private String mobileNumber;

	private String mobileOtpValue;

	private String mobileOtpSession;

	private String password;

	private String utm;

	private String dob;

	private String address;

	private Integer userId;

	private String citizenship;

	private String panNumber;

	private String uniqueNumber;

	private String uuid;

	private String utmForPartner;

	private String timeInMilliSeconds;

	private String cifNumber;

	private String finoEmployeeMobileNumber;

	private String emailOtpSession;

	private String emailOtp;

	private String projectType;

	public String getPrimaryType() {
		return primaryType;
	}

	public void setPrimaryType(String primaryType) {
		this.primaryType = primaryType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getMobileOtpValue() {
		return mobileOtpValue;
	}

	public void setMobileOtpValue(String mobileOtpValue) {
		this.mobileOtpValue = mobileOtpValue;
	}

	public String getMobileOtpSession() {
		return mobileOtpSession;
	}

	public void setMobileOtpSession(String mobileOtpSession) {
		this.mobileOtpSession = mobileOtpSession;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUtm() {
		return utm;
	}

	public void setUtm(String utm) {
		this.utm = utm;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(String uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUtmForPartner() {
		return utmForPartner;
	}

	public void setUtmForPartner(String utmForPartner) {
		this.utmForPartner = utmForPartner;
	}

	public String getTimeInMilliSeconds() {
		return timeInMilliSeconds;
	}

	public void setTimeInMilliSeconds(String timeInMilliSeconds) {
		this.timeInMilliSeconds = timeInMilliSeconds;
	}

	public String getCifNumber() {
		return cifNumber;
	}

	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}

	public String getFinoEmployeeMobileNumber() {
		return finoEmployeeMobileNumber;
	}

	public void setFinoEmployeeMobileNumber(String finoEmployeeMobileNumber) {
		this.finoEmployeeMobileNumber = finoEmployeeMobileNumber;
	}

	public String getEmailOtpSession() {
		return emailOtpSession;
	}

	public String getEmailOtp() {
		return emailOtp;
	}

	public void setEmailOtpSession(String emailOtpSession) {
		this.emailOtpSession = emailOtpSession;
	}

	public void setEmailOtp(String emailOtp) {
		this.emailOtp = emailOtp;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

}
