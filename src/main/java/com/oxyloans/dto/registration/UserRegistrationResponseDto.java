package com.oxyloans.dto.registration;

public class UserRegistrationResponseDto {
	private Integer userId;

	private String mobileOtpSession;

	private String mobileNumber;

	private String status;

	private String primaryType;

	private String timeInMilliSeconds;

	private String emailOtpSession;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getMobileOtpSession() {
		return mobileOtpSession;
	}

	public void setMobileOtpSession(String mobileOtpSession) {
		this.mobileOtpSession = mobileOtpSession;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPrimaryType() {
		return primaryType;
	}

	public void setPrimaryType(String primaryType) {
		this.primaryType = primaryType;
	}

	public String getTimeInMilliSeconds() {
		return timeInMilliSeconds;
	}

	public void setTimeInMilliSeconds(String timeInMilliSeconds) {
		this.timeInMilliSeconds = timeInMilliSeconds;
	}

	public String getEmailOtpSession() {
		return emailOtpSession;
	}

	public void setEmailOtpSession(String emailOtpSession) {
		this.emailOtpSession = emailOtpSession;
	}

}
