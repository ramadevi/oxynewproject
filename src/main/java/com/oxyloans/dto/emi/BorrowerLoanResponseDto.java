package com.oxyloans.dto.emi;

import java.util.List;

public class BorrowerLoanResponseDto {

	private List<BorrowerEmiDetails> flatCalculationEmi;

	private List<BorrowerEmiDetails> ReduceCalculationEmi;

	private double loanAmountCalculated;

	private Integer borrowerId;

	private double loanAmount;

	public List<BorrowerEmiDetails> getFlatCalculationEmi() {
		return flatCalculationEmi;
	}

	public void setFlatCalculationEmi(List<BorrowerEmiDetails> flatCalculationEmi) {
		this.flatCalculationEmi = flatCalculationEmi;
	}

	public List<BorrowerEmiDetails> getReduceCalculationEmi() {
		return ReduceCalculationEmi;
	}

	public void setReduceCalculationEmi(List<BorrowerEmiDetails> reduceCalculationEmi) {
		ReduceCalculationEmi = reduceCalculationEmi;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public double getLoanAmountCalculated() {
		return loanAmountCalculated;
	}

	public void setLoanAmountCalculated(double loanAmountCalculated) {
		this.loanAmountCalculated = loanAmountCalculated;
	}

}
