package com.oxyloans.dto.emi;

public class BorrowerEmiDetails {

	private Integer emiNumber;

	private double principalAmount;

	private double interestAmount;

	private double emiAmount;

	private double balanceAndOutstanding;

	public Integer getEmiNumber() {
		return emiNumber;
	}

	public void setEmiNumber(Integer emiNumber) {
		this.emiNumber = emiNumber;
	}

	public double getPrincipalAmount() {
		return principalAmount;
	}

	public void setPrincipalAmount(double principalAmount) {
		this.principalAmount = principalAmount;
	}

	public double getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(double interestAmount) {
		this.interestAmount = interestAmount;
	}

	public double getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(double emiAmount) {
		this.emiAmount = emiAmount;
	}

	public double getBalanceAndOutstanding() {
		return balanceAndOutstanding;
	}

	public void setBalanceAndOutstanding(double balanceAndOutstanding) {
		this.balanceAndOutstanding = balanceAndOutstanding;
	}

}
