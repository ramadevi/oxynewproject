package com.oxyloans.dto.emi;

public class BorrowerLoanRequestDto {

	private Double loanAmount;

	private Double rateOfInterest;

	private Integer tenure;

	private String calculationType;

	private Double borrowerSalary;

	private Double sumOfEmis;

	private Integer multiplyingValueWithLoanAmount;

	public Double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public Integer getTenure() {
		return tenure;
	}

	public void setTenure(Integer tenure) {
		this.tenure = tenure;
	}

	public String getCalculationType() {
		return calculationType;
	}

	public void setCalculationType(String calculationType) {
		this.calculationType = calculationType;
	}

	public Double getBorrowerSalary() {
		return borrowerSalary;
	}

	public void setBorrowerSalary(Double borrowerSalary) {
		this.borrowerSalary = borrowerSalary;
	}

	public Double getSumOfEmis() {
		return sumOfEmis;
	}

	public void setSumOfEmis(Double sumOfEmis) {
		this.sumOfEmis = sumOfEmis;
	}

	public Integer getMultiplyingValueWithLoanAmount() {
		return multiplyingValueWithLoanAmount;
	}

	public void setMultiplyingValueWithLoanAmount(Integer multiplyingValueWithLoanAmount) {
		this.multiplyingValueWithLoanAmount = multiplyingValueWithLoanAmount;
	}

}
