package com.oxyloans.dto.stats;

import java.math.BigInteger;
import java.util.List;

import com.oxyloans.response.user.InterestDetailsResponse;

public class InterestAmountResponse {
	
	private BigInteger totalInterestEarned;
	
	private List<InterestDetailsResponse> listOfInterestDetails;

	public BigInteger getTotalInterestEarned() {
		return totalInterestEarned;
	}

	public void setTotalInterestEarned(BigInteger totalInterestEarned) {
		this.totalInterestEarned = totalInterestEarned;
	}

	public List<InterestDetailsResponse> getListOfInterestDetails() {
		return listOfInterestDetails;
	}

	public void setListOfInterestDetails(List<InterestDetailsResponse> listOfInterestDetails) {
		this.listOfInterestDetails = listOfInterestDetails;
	}
	
	

}
