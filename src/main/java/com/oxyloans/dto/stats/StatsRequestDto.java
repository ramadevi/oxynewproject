package com.oxyloans.dto.stats;

public class StatsRequestDto {

	private String startDate;
	
	private String endDate;
	
	private String type;
	
	private Integer userId;
	
	private String sortBasedOn;

	private String sortingType;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getSortBasedOn() {
		return sortBasedOn;
	}

	public void setSortBasedOn(String sortBasedOn) {
		this.sortBasedOn = sortBasedOn;
	}

	public String getSortingType() {
		return sortingType;
	}

	public void setSortingType(String sortingType) {
		this.sortingType = sortingType;
	}
	
	
}
