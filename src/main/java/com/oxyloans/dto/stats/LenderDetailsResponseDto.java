package com.oxyloans.dto.stats;

public class LenderDetailsResponseDto {

	private Integer userId;
	
	private String registeredDate;
	
	private Double participatedAmount;
	
	private String name;
	
	private String email;
	
	private String mobileNumber;
	
	private String validityDate;
	
	private Integer noOfReferees;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getRegisteredDate() {
		return registeredDate;
	}

	public void setRegisteredDate(String registeredDate) {
		this.registeredDate = registeredDate;
	}

	public Double getParticipatedAmount() {
		return participatedAmount;
	}

	public void setParticipatedAmount(Double participatedAmount) {
		this.participatedAmount = participatedAmount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}

	public Integer getNoOfReferees() {
		return noOfReferees;
	}

	public void setNoOfReferees(Integer noOfReferees) {
		this.noOfReferees = noOfReferees;
	}
	
	
}
