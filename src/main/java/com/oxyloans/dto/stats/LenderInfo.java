package com.oxyloans.dto.stats;

import java.util.List;

public class LenderInfo {

	private List<LenderDetailsResponseDto> listOfLenderDetailsResponse;
	
	private String downloadUrl;

	public List<LenderDetailsResponseDto> getListOfLenderDetailsResponse() {
		return listOfLenderDetailsResponse;
	}

	public void setListOfLenderDetailsResponse(List<LenderDetailsResponseDto> listOfLenderDetailsResponse) {
		this.listOfLenderDetailsResponse = listOfLenderDetailsResponse;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	
	
}
