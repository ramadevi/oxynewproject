package com.oxyloans.dto.cicreport;

public class CICReportReponseDto {

	private String consumerName;
	private String dateOfBirth;
	private String gender;
	private String incomeTaxIdNumber;
	private String passportNumber;
	private String passportIssueDate;
	private String passportExpiryDate;
	private String voterIdNumber;
	private String drivingLicenseNumber;
	private String drivingLicenseIssueDate;
	private String drivingLicenseExpiryDate;
	private String rationCardNumber;
	private String universalIdNumber;
	private String additionalId1;
	private String additionalId2;
	private String telephoneNumberMobile;
	private String telephoneNumberResidence;
	private String telephoneNumberOffice;
	private String extensionOffice;
	private String telephoneNumberOther;
	private String extensionOther;
	private String emailId1;
	private String emailId2;
	private String permanantAdd;
	private String stateCode1;
	private String pinCode1;
	private String addressCategory1;
	private String residenceCode1;
	private String correspondingAdd;
	private String satateCode2;
	private String pinCode2;
	private String addressCategory2;
	private String residenceCategory2;
	private String currentNewMemberCode;
	private String currentNewMemberShortName;
	private String currentNewAccountNumber;
	private String accountType;
	private String ownershipIndicator;
	private String dateOpenedDisbursed;
	private String dateOfLastPayment;
	private String dateClosed;
	private String dateReported;
	private String highCreditSanctionedAmount;
	private String currentBalance;
	private String amountOverdue;
	private String numberOfDaysPastDue;
	private String oldMbrCode;
	private String oldMbrShortName;
	private String oldAccountNumber;
	private String pldAccountType;
	private String oldOwnerShipIndicator;
	private String suitFiledWilfulDefault;
	private String writtenOffAndSettledStatus;
	private String assetClassification;
	private String valueOfCollateral;
	private String typeOfCollateral;
	private String creditLimit;
	private String cashLimit;
	private String rateOfInterest;
	private String repaymentTenure;
	private String emiAmount;
	private String writtenOffAmountTotal;
	private String writtenOffPrincipalAmount;
	private String settlementAmount;
	private String paymentFreequency;
	private String actualPaymentAmount;
	private String occupationCode;
	private String income;
	private String netGrossIncomeIndicator;
	private String monthlyAnnualIncomeIndicator;

	public String getConsumerName() {
		return consumerName;
	}

	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getIncomeTaxIdNumber() {
		return incomeTaxIdNumber;
	}

	public void setIncomeTaxIdNumber(String incomeTaxIdNumber) {
		this.incomeTaxIdNumber = incomeTaxIdNumber;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getPassportIssueDate() {
		return passportIssueDate;
	}

	public void setPassportIssueDate(String passportIssueDate) {
		this.passportIssueDate = passportIssueDate;
	}

	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}

	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}

	public String getVoterIdNumber() {
		return voterIdNumber;
	}

	public void setVoterIdNumber(String voterIdNumber) {
		this.voterIdNumber = voterIdNumber;
	}

	public String getDrivingLicenseNumber() {
		return drivingLicenseNumber;
	}

	public void setDrivingLicenseNumber(String drivingLicenseNumber) {
		this.drivingLicenseNumber = drivingLicenseNumber;
	}

	public String getDrivingLicenseIssueDate() {
		return drivingLicenseIssueDate;
	}

	public void setDrivingLicenseIssueDate(String drivingLicenseIssueDate) {
		this.drivingLicenseIssueDate = drivingLicenseIssueDate;
	}

	public String getDrivingLicenseExpiryDate() {
		return drivingLicenseExpiryDate;
	}

	public void setDrivingLicenseExpiryDate(String drivingLicenseExpiryDate) {
		this.drivingLicenseExpiryDate = drivingLicenseExpiryDate;
	}

	public String getRationCardNumber() {
		return rationCardNumber;
	}

	public void setRationCardNumber(String rationCardNumber) {
		this.rationCardNumber = rationCardNumber;
	}

	public String getUniversalIdNumber() {
		return universalIdNumber;
	}

	public void setUniversalIdNumber(String universalIdNumber) {
		this.universalIdNumber = universalIdNumber;
	}

	public String getAdditionalId1() {
		return additionalId1;
	}

	public void setAdditionalId1(String additionalId1) {
		this.additionalId1 = additionalId1;
	}

	public String getAdditionalId2() {
		return additionalId2;
	}

	public void setAdditionalId2(String additionalId2) {
		this.additionalId2 = additionalId2;
	}

	public String getTelephoneNumberMobile() {
		return telephoneNumberMobile;
	}

	public void setTelephoneNumberMobile(String telephoneNumberMobile) {
		this.telephoneNumberMobile = telephoneNumberMobile;
	}

	public String getTelephoneNumberResidence() {
		return telephoneNumberResidence;
	}

	public void setTelephoneNumberResidence(String telephoneNumberResidence) {
		this.telephoneNumberResidence = telephoneNumberResidence;
	}

	public String getTelephoneNumberOffice() {
		return telephoneNumberOffice;
	}

	public void setTelephoneNumberOffice(String telephoneNumberOffice) {
		this.telephoneNumberOffice = telephoneNumberOffice;
	}

	public String getExtensionOffice() {
		return extensionOffice;
	}

	public void setExtensionOffice(String extensionOffice) {
		this.extensionOffice = extensionOffice;
	}

	public String getTelephoneNumberOther() {
		return telephoneNumberOther;
	}

	public void setTelephoneNumberOther(String telephoneNumberOther) {
		this.telephoneNumberOther = telephoneNumberOther;
	}

	public String getExtensionOther() {
		return extensionOther;
	}

	public void setExtensionOther(String extensionOther) {
		this.extensionOther = extensionOther;
	}

	public String getEmailId1() {
		return emailId1;
	}

	public void setEmailId1(String emailId1) {
		this.emailId1 = emailId1;
	}

	public String getEmailId2() {
		return emailId2;
	}

	public void setEmailId2(String emailId2) {
		this.emailId2 = emailId2;
	}

	public String getPermanantAdd() {
		return permanantAdd;
	}

	public void setPermanantAdd(String permanantAdd) {
		this.permanantAdd = permanantAdd;
	}

	public String getStateCode1() {
		return stateCode1;
	}

	public void setStateCode1(String stateCode1) {
		this.stateCode1 = stateCode1;
	}

	public String getPinCode1() {
		return pinCode1;
	}

	public void setPinCode1(String pinCode1) {
		this.pinCode1 = pinCode1;
	}

	public String getAddressCategory1() {
		return addressCategory1;
	}

	public void setAddressCategory1(String addressCategory1) {
		this.addressCategory1 = addressCategory1;
	}

	public String getResidenceCode1() {
		return residenceCode1;
	}

	public void setResidenceCode1(String residenceCode1) {
		this.residenceCode1 = residenceCode1;
	}

	public String getCorrespondingAdd() {
		return correspondingAdd;
	}

	public void setCorrespondingAdd(String correspondingAdd) {
		this.correspondingAdd = correspondingAdd;
	}

	public String getSatateCode2() {
		return satateCode2;
	}

	public void setSatateCode2(String satateCode2) {
		this.satateCode2 = satateCode2;
	}

	public String getPinCode2() {
		return pinCode2;
	}

	public void setPinCode2(String pinCode2) {
		this.pinCode2 = pinCode2;
	}

	public String getAddressCategory2() {
		return addressCategory2;
	}

	public void setAddressCategory2(String addressCategory2) {
		this.addressCategory2 = addressCategory2;
	}

	public String getResidenceCategory2() {
		return residenceCategory2;
	}

	public void setResidenceCategory2(String residenceCategory2) {
		this.residenceCategory2 = residenceCategory2;
	}

	public String getCurrentNewMemberCode() {
		return currentNewMemberCode;
	}

	public void setCurrentNewMemberCode(String currentNewMemberCode) {
		this.currentNewMemberCode = currentNewMemberCode;
	}

	public String getCurrentNewMemberShortName() {
		return currentNewMemberShortName;
	}

	public void setCurrentNewMemberShortName(String currentNewMemberShortName) {
		this.currentNewMemberShortName = currentNewMemberShortName;
	}

	public String getCurrentNewAccountNumber() {
		return currentNewAccountNumber;
	}

	public void setCurrentNewAccountNumber(String currentNewAccountNumber) {
		this.currentNewAccountNumber = currentNewAccountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getOwnershipIndicator() {
		return ownershipIndicator;
	}

	public void setOwnershipIndicator(String ownershipIndicator) {
		this.ownershipIndicator = ownershipIndicator;
	}

	public String getDateOpenedDisbursed() {
		return dateOpenedDisbursed;
	}

	public void setDateOpenedDisbursed(String dateOpenedDisbursed) {
		this.dateOpenedDisbursed = dateOpenedDisbursed;
	}

	public String getDateOfLastPayment() {
		return dateOfLastPayment;
	}

	public void setDateOfLastPayment(String dateOfLastPayment) {
		this.dateOfLastPayment = dateOfLastPayment;
	}

	public String getDateClosed() {
		return dateClosed;
	}

	public void setDateClosed(String dateClosed) {
		this.dateClosed = dateClosed;
	}

	public String getDateReported() {
		return dateReported;
	}

	public void setDateReported(String dateReported) {
		this.dateReported = dateReported;
	}

	public String getHighCreditSanctionedAmount() {
		return highCreditSanctionedAmount;
	}

	public void setHighCreditSanctionedAmount(String highCreditSanctionedAmount) {
		this.highCreditSanctionedAmount = highCreditSanctionedAmount;
	}

	public String getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(String currentBalance) {
		this.currentBalance = currentBalance;
	}

	public String getAmountOverdue() {
		return amountOverdue;
	}

	public void setAmountOverdue(String amountOverdue) {
		this.amountOverdue = amountOverdue;
	}

	public String getNumberOfDaysPastDue() {
		return numberOfDaysPastDue;
	}

	public void setNumberOfDaysPastDue(String numberOfDaysPastDue) {
		this.numberOfDaysPastDue = numberOfDaysPastDue;
	}

	public String getOldMbrCode() {
		return oldMbrCode;
	}

	public void setOldMbrCode(String oldMbrCode) {
		this.oldMbrCode = oldMbrCode;
	}

	public String getOldMbrShortName() {
		return oldMbrShortName;
	}

	public void setOldMbrShortName(String oldMbrShortName) {
		this.oldMbrShortName = oldMbrShortName;
	}

	public String getOldAccountNumber() {
		return oldAccountNumber;
	}

	public void setOldAccountNumber(String oldAccountNumber) {
		this.oldAccountNumber = oldAccountNumber;
	}

	public String getPldAccountType() {
		return pldAccountType;
	}

	public void setPldAccountType(String pldAccountType) {
		this.pldAccountType = pldAccountType;
	}

	public String getOldOwnerShipIndicator() {
		return oldOwnerShipIndicator;
	}

	public void setOldOwnerShipIndicator(String oldOwnerShipIndicator) {
		this.oldOwnerShipIndicator = oldOwnerShipIndicator;
	}

	public String getSuitFiledWilfulDefault() {
		return suitFiledWilfulDefault;
	}

	public void setSuitFiledWilfulDefault(String suitFiledWilfulDefault) {
		this.suitFiledWilfulDefault = suitFiledWilfulDefault;
	}

	public String getWrittenOffAndSettledStatus() {
		return writtenOffAndSettledStatus;
	}

	public void setWrittenOffAndSettledStatus(String writtenOffAndSettledStatus) {
		this.writtenOffAndSettledStatus = writtenOffAndSettledStatus;
	}

	public String getAssetClassification() {
		return assetClassification;
	}

	public void setAssetClassification(String assetClassification) {
		this.assetClassification = assetClassification;
	}

	public String getValueOfCollateral() {
		return valueOfCollateral;
	}

	public void setValueOfCollateral(String valueOfCollateral) {
		this.valueOfCollateral = valueOfCollateral;
	}

	public String getTypeOfCollateral() {
		return typeOfCollateral;
	}

	public void setTypeOfCollateral(String typeOfCollateral) {
		this.typeOfCollateral = typeOfCollateral;
	}

	public String getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getCashLimit() {
		return cashLimit;
	}

	public void setCashLimit(String cashLimit) {
		this.cashLimit = cashLimit;
	}

	public String getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(String rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public String getRepaymentTenure() {
		return repaymentTenure;
	}

	public void setRepaymentTenure(String repaymentTenure) {
		this.repaymentTenure = repaymentTenure;
	}

	public String getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(String emiAmount) {
		this.emiAmount = emiAmount;
	}

	public String getWrittenOffAmountTotal() {
		return writtenOffAmountTotal;
	}

	public void setWrittenOffAmountTotal(String writtenOffAmountTotal) {
		this.writtenOffAmountTotal = writtenOffAmountTotal;
	}

	public String getWrittenOffPrincipalAmount() {
		return writtenOffPrincipalAmount;
	}

	public void setWrittenOffPrincipalAmount(String writtenOffPrincipalAmount) {
		this.writtenOffPrincipalAmount = writtenOffPrincipalAmount;
	}

	public String getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(String settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public String getPaymentFreequency() {
		return paymentFreequency;
	}

	public void setPaymentFreequency(String paymentFreequency) {
		this.paymentFreequency = paymentFreequency;
	}

	public String getActualPaymentAmount() {
		return actualPaymentAmount;
	}

	public void setActualPaymentAmount(String actualPaymentAmount) {
		this.actualPaymentAmount = actualPaymentAmount;
	}

	public String getOccupationCode() {
		return occupationCode;
	}

	public void setOccupationCode(String occupationCode) {
		this.occupationCode = occupationCode;
	}

	public String getIncome() {
		return income;
	}

	public void setIncome(String income) {
		this.income = income;
	}

	public String getNetGrossIncomeIndicator() {
		return netGrossIncomeIndicator;
	}

	public void setNetGrossIncomeIndicator(String netGrossIncomeIndicator) {
		this.netGrossIncomeIndicator = netGrossIncomeIndicator;
	}

	public String getMonthlyAnnualIncomeIndicator() {
		return monthlyAnnualIncomeIndicator;
	}

	public void setMonthlyAnnualIncomeIndicator(String monthlyAnnualIncomeIndicator) {
		this.monthlyAnnualIncomeIndicator = monthlyAnnualIncomeIndicator;
	}

}
