package com.oxyloans.dto.equitydealsinfo;

import java.util.List;

public class EquityDealsDto {
	
	private Integer lenderId;
	
	private Integer dealId;
	
	private Double participatedAmount;
	
	private String lenderReturnType;
	
	private Double rateOfInterest;
	
	private String dealName;
	
	private String participatedOn;
	
	private List<EquityInterestDetails> listOfEquityInterestDetails;
	
	private Integer shares;

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Double getParticipatedAmount() {
		return participatedAmount;
	}

	public void setParticipatedAmount(Double participatedAmount) {
		this.participatedAmount = participatedAmount;
	}

	public String getLenderReturnType() {
		return lenderReturnType;
	}

	public void setLenderReturnType(String lenderReturnType) {
		this.lenderReturnType = lenderReturnType;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getParticipatedOn() {
		return participatedOn;
	}

	public void setParticipatedOn(String participatedOn) {
		this.participatedOn = participatedOn;
	}

	public List<EquityInterestDetails> getListOfEquityInterestDetails() {
		return listOfEquityInterestDetails;
	}

	public void setListOfEquityInterestDetails(List<EquityInterestDetails> listOfEquityInterestDetails) {
		this.listOfEquityInterestDetails = listOfEquityInterestDetails;
	}

	public Integer getShares() {
		return shares;
	}

	public void setShares(Integer shares) {
		this.shares = shares;
	}

}
