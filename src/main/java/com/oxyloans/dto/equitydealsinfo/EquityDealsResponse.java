package com.oxyloans.dto.equitydealsinfo;

import java.util.List;

public class EquityDealsResponse {

	private Double totalEquityInterestEarned;
	
	private List<EquityDealsDto> listOfEquityInterestDetails;
	
	private String downloadUrl;
	
	private Double totalAmountParticipatedInEquityDeals;

	public Double getTotalEquityInterestEarned() {
		return totalEquityInterestEarned;
	}

	public void setTotalEquityInterestEarned(Double totalEquityInterestEarned) {
		this.totalEquityInterestEarned = totalEquityInterestEarned;
	}

	public List<EquityDealsDto> getListOfEquityInterestDetails() {
		return listOfEquityInterestDetails;
	}

	public void setListOfEquityInterestDetails(List<EquityDealsDto> listOfEquityInterestDetails) {
		this.listOfEquityInterestDetails = listOfEquityInterestDetails;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public Double getTotalAmountParticipatedInEquityDeals() {
		return totalAmountParticipatedInEquityDeals;
	}

	public void setTotalAmountParticipatedInEquityDeals(Double totalAmountParticipatedInEquityDeals) {
		this.totalAmountParticipatedInEquityDeals = totalAmountParticipatedInEquityDeals;
	}

	
	
	
}
