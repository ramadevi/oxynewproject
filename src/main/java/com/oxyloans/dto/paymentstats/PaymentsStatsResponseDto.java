package com.oxyloans.dto.paymentstats;

import java.util.List;

import com.oxyloans.request.LenderPayuDto;
import com.oxyloans.response.user.BorrowerProcessingFeeResponseDto;

public class PaymentsStatsResponseDto {

	private Double totalBorrowerFee;
	private List<BorrowerProcessingFeeResponseDto> listOfBorrowersIncludingFee;

	private Double totalLenderFee;
	private Double totalLenderFeeInCurrentMonth;
	private List<LenderPayuDto> lenderPayuList;

	private Double amountThroughPaymentScreenShot;
	private Double paymentScreenshotFullPayment;
	private Double paymentScreenshotPartPayment;

	private int countOfLoansDisbursed;
	private Double totalAmountDisbursed;

	private String url;

	public Double getTotalBorrowerFee() {
		return totalBorrowerFee;
	}

	public void setTotalBorrowerFee(Double totalBorrowerFee) {
		this.totalBorrowerFee = totalBorrowerFee;
	}

	public Double getTotalLenderFee() {
		return totalLenderFee;
	}

	public void setTotalLenderFee(Double totalLenderFee) {
		this.totalLenderFee = totalLenderFee;
	}

	public int getCountOfLoansDisbursed() {
		return countOfLoansDisbursed;
	}

	public void setCountOfLoansDisbursed(int countOfLoansDisbursed) {
		this.countOfLoansDisbursed = countOfLoansDisbursed;
	}

	public Double getTotalAmountDisbursed() {
		return totalAmountDisbursed;
	}

	public void setTotalAmountDisbursed(Double totalAmountDisbursed) {
		this.totalAmountDisbursed = totalAmountDisbursed;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<BorrowerProcessingFeeResponseDto> getListOfBorrowersIncludingFee() {
		return listOfBorrowersIncludingFee;
	}

	public void setListOfBorrowersIncludingFee(List<BorrowerProcessingFeeResponseDto> listOfBorrowersIncludingFee) {
		this.listOfBorrowersIncludingFee = listOfBorrowersIncludingFee;
	}

	public List<LenderPayuDto> getLenderPayuList() {
		return lenderPayuList;
	}

	public void setLenderPayuList(List<LenderPayuDto> lenderPayuList) {
		this.lenderPayuList = lenderPayuList;
	}

	public Double getTotalLenderFeeInCurrentMonth() {
		return totalLenderFeeInCurrentMonth;
	}

	public void setTotalLenderFeeInCurrentMonth(Double totalLenderFeeInCurrentMonth) {
		this.totalLenderFeeInCurrentMonth = totalLenderFeeInCurrentMonth;
	}

	public Double getAmountThroughPaymentScreenShot() {
		return amountThroughPaymentScreenShot;
	}

	public void setAmountThroughPaymentScreenShot(Double amountThroughPaymentScreenShot) {
		this.amountThroughPaymentScreenShot = amountThroughPaymentScreenShot;
	}

	public Double getPaymentScreenshotFullPayment() {
		return paymentScreenshotFullPayment;
	}

	public void setPaymentScreenshotFullPayment(Double paymentScreenshotFullPayment) {
		this.paymentScreenshotFullPayment = paymentScreenshotFullPayment;
	}

	public Double getPaymentScreenshotPartPayment() {
		return paymentScreenshotPartPayment;
	}

	public void setPaymentScreenshotPartPayment(Double paymentScreenshotPartPayment) {
		this.paymentScreenshotPartPayment = paymentScreenshotPartPayment;
	}

}
