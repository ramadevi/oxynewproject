package com.oxyloans.dto.disbursment;

import java.math.BigInteger;

public class BorrowerDisbusmentResponseDto {

	private BigInteger dealCreatedAmount;

	private BigInteger dealAchievedAmount;

	private String borrowerCMSApprovalStatus;

	public String getBorrowerCMSApprovalStatus() {
		return borrowerCMSApprovalStatus;
	}

	public void setBorrowerCMSApprovalStatus(String borrowerCMSApprovalStatus) {
		this.borrowerCMSApprovalStatus = borrowerCMSApprovalStatus;
	}

	public BigInteger getDealCreatedAmount() {
		return dealCreatedAmount;
	}

	public void setDealCreatedAmount(BigInteger dealCreatedAmount) {
		this.dealCreatedAmount = dealCreatedAmount;
	}

	public BigInteger getDealAchievedAmount() {
		return dealAchievedAmount;
	}

	public void setDealAchievedAmount(BigInteger dealAchievedAmount) {
		this.dealAchievedAmount = dealAchievedAmount;
	}

}
