package com.oxyloans.dto.borrower.noc;

public class BorrowerDealNotificationRequestfordeals {
	
		private String StartDate;
		private String EndDate;
		private String dealParticipationStatus;
		private String borrowerClosingStatus;
		public String getBorrowerClosingStatus() {
			return borrowerClosingStatus;
		}
		public void setBorrowerClosingStatus(String borrowerClosingStatus) {
			this.borrowerClosingStatus = borrowerClosingStatus;
		}
		public String getDealParticipationStatus() {
			return dealParticipationStatus;
		}
		public void setDealParticipationStatus(String dealParticipationStatus) {
			this.dealParticipationStatus = dealParticipationStatus;
		}
		public String getStartDate() {
			return StartDate;
		}
		public void setStartDate(String startDate) {
			StartDate = startDate;
		}
		public String getEndDate() {
			return EndDate;
		}
		public void setEndDate(String endDate) {
			EndDate = endDate;
		}
	}



