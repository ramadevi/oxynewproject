package com.oxyloans.dto.borrower.noc;

public class BorrowerNocResponseDto {

	private String downloadUrl;

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

}
