package com.oxyloans.dto.borrower.noc;

public class BorrowerNocRequestDto {
	private Integer id;

	private Double loanAmount;

	private String paidDate;

	public Integer getId() {
		return id;
	}

	public Double getLoanAmount() {
		return loanAmount;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

}
