package com.oxyloans.dto.borrower.noc;


import java.util.Date;

public class NewLendersExcelsheetResponseDto1 {

	
	private int id;
	private String dealname;
	private Double dealamount;
	private Date funds_acceptance_start_date;
	private Date funds_acceptance_end_date;
	private Date loan_active_date;
    private double totalPaticipation;
  
    private Date borrowercloseddate;
    private String deal_paticipation_status; // Add this field

    public String getDeal_paticipation_status() {
        return deal_paticipation_status;
    }

    public void setDeal_paticipation_status(String deal_paticipation_status) {
        this.deal_paticipation_status = deal_paticipation_status;
    }
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDealname() {
		return dealname;
	}
	public void setDealname(String dealname) {
		this.dealname = dealname;
	}
	public Double getDealamount() {
		return dealamount;
	}
	public void setDealamount(Double dealamount) {
		this.dealamount = dealamount;
	}
	public Date getFunds_acceptance_start_date() {
		return funds_acceptance_start_date;
	}
	public void setFunds_acceptance_start_date(Date funds_acceptance_start_date) {
		this.funds_acceptance_start_date = funds_acceptance_start_date;
	}
	public Date getFunds_acceptance_end_date() {
		return funds_acceptance_end_date;
	}
	public void setFunds_acceptance_end_date(Date funds_acceptance_end_date) {
		this.funds_acceptance_end_date = funds_acceptance_end_date;
	}
	public Date getLoan_active_date() {
		return loan_active_date;
	}
	public void setLoan_active_date(Date loan_active_date) {
		this.loan_active_date = loan_active_date;
	}
	public double getTotalPaticipation() {
		return totalPaticipation;
	}
	public void setTotalPaticipation(double totalPaticipation) {
		this.totalPaticipation = totalPaticipation;
	}
	
	public Date getBorrowercloseddate() {
		return borrowercloseddate;
	}
	public void setBorrowercloseddate(Date borrowercloseddate) {
		this.borrowercloseddate = borrowercloseddate;
	}
    
    
}
