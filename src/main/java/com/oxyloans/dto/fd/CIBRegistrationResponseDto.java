package com.oxyloans.dto.fd;

public class CIBRegistrationResponseDto {

	private String aggr_name;

	private String aggr_id;

	private String urn;

	private String corp_id;

	private String user_id;

	private String response;

	private String message;

	public String getAggr_name() {
		return aggr_name;
	}

	public void setAggr_name(String aggr_name) {
		this.aggr_name = aggr_name;
	}

	public String getAggr_id() {
		return aggr_id;
	}

	public void setAggr_id(String aggr_id) {
		this.aggr_id = aggr_id;
	}

	public String getUrn() {
		return urn;
	}

	public void setUrn(String urn) {
		this.urn = urn;
	}

	public String getCorp_id() {
		return corp_id;
	}

	public void setCorp_id(String corp_id) {
		this.corp_id = corp_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
