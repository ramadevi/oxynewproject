package com.oxyloans.dto.fd;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CIBRegistrationRequestDto {

	@JsonProperty("AGGRNAME")
	private String AGGRNAME;

	@JsonProperty("AGGRID")
	private String AGGRID;

	@JsonProperty("URN")
	private String URN;

	@JsonProperty("CORPID")
	private String CORPID;

	@JsonProperty("USERID")
	private String USERID;

	@JsonProperty("ALIASID")
	private String ALIASID;

	public String getAGGRNAME() {
		return AGGRNAME;
	}

	public void setAGGRNAME(String aGGRNAME) {
		AGGRNAME = aGGRNAME;
	}

	public String getAGGRID() {
		return AGGRID;
	}

	public void setAGGRID(String aGGRID) {
		AGGRID = aGGRID;
	}

	public String getURN() {
		return URN;
	}

	public void setURN(String uRN) {
		URN = uRN;
	}

	public String getCORPID() {
		return CORPID;
	}

	public void setCORPID(String cORPID) {
		CORPID = cORPID;
	}

	public String getUSERID() {
		return USERID;
	}

	public void setUSERID(String uSERID) {
		USERID = uSERID;
	}

	public String getALIASID() {
		return ALIASID;
	}

	public void setALIASID(String aLIASID) {
		ALIASID = aLIASID;
	}

}
