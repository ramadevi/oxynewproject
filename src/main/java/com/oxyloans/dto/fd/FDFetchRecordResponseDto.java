package com.oxyloans.dto.fd;

public class FDFetchRecordResponseDto {

	private String ACCOUNT_NAME;

	private String ACCOUNT_NUMBER;

	private String DEPOSIT_AMOUNT;

	private String CURRENCY;

	private String MATURITY_AMOUNT;

	private String MATURITY_DATE;

	private String DEPOSIT_START_DATE;

	private String INTEREST_RATE;

	public String getACCOUNT_NAME() {
		return ACCOUNT_NAME;
	}

	public void setACCOUNT_NAME(String aCCOUNT_NAME) {
		ACCOUNT_NAME = aCCOUNT_NAME;
	}

	public String getACCOUNT_NUMBER() {
		return ACCOUNT_NUMBER;
	}

	public void setACCOUNT_NUMBER(String aCCOUNT_NUMBER) {
		ACCOUNT_NUMBER = aCCOUNT_NUMBER;
	}

	public String getDEPOSIT_AMOUNT() {
		return DEPOSIT_AMOUNT;
	}

	public void setDEPOSIT_AMOUNT(String dEPOSIT_AMOUNT) {
		DEPOSIT_AMOUNT = dEPOSIT_AMOUNT;
	}

	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

	public String getMATURITY_AMOUNT() {
		return MATURITY_AMOUNT;
	}

	public void setMATURITY_AMOUNT(String mATURITY_AMOUNT) {
		MATURITY_AMOUNT = mATURITY_AMOUNT;
	}

	public String getMATURITY_DATE() {
		return MATURITY_DATE;
	}

	public void setMATURITY_DATE(String mATURITY_DATE) {
		MATURITY_DATE = mATURITY_DATE;
	}

	public String getDEPOSIT_START_DATE() {
		return DEPOSIT_START_DATE;
	}

	public void setDEPOSIT_START_DATE(String dEPOSIT_START_DATE) {
		DEPOSIT_START_DATE = dEPOSIT_START_DATE;
	}

	public String getINTEREST_RATE() {
		return INTEREST_RATE;
	}

	public void setINTEREST_RATE(String iNTEREST_RATE) {
		INTEREST_RATE = iNTEREST_RATE;
	}

}
