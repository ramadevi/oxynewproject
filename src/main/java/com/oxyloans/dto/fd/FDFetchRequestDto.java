package com.oxyloans.dto.fd;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FDFetchRequestDto {

	@JsonProperty
	private String AGGR_NAME;

	@JsonProperty
	private String AGGR_ID;

	@JsonProperty
	private String CORP_ID;

	@JsonProperty
	private String USER_ID;

	@JsonProperty
	private String URN;

	public String getAGGR_NAME() {
		return AGGR_NAME;
	}

	public void setAGGR_NAME(String aGGR_NAME) {
		AGGR_NAME = aGGR_NAME;
	}

	public String getAGGR_ID() {
		return AGGR_ID;
	}

	public void setAGGR_ID(String aGGR_ID) {
		AGGR_ID = aGGR_ID;
	}

	public String getCORP_ID() {
		return CORP_ID;
	}

	public void setCORP_ID(String cORP_ID) {
		CORP_ID = cORP_ID;
	}

	public String getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}

	public String getURN() {
		return URN;
	}

	public void setURN(String uRN) {
		URN = uRN;
	}

}
