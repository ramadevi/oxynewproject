package com.oxyloans.dto.fd;

import java.util.List;

public class FDFetchDto {

	private String CORP_ID;

	private String USER_ID;

	private String RESPONSE;

	private List<FDFetchRecordResponseDto> Record;

	public String getCORP_ID() {
		return CORP_ID;
	}

	public void setCORP_ID(String cORP_ID) {
		CORP_ID = cORP_ID;
	}

	public String getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}

	public String getRESPONSE() {
		return RESPONSE;
	}

	public void setRESPONSE(String rESPONSE) {
		RESPONSE = rESPONSE;
	}

	public List<FDFetchRecordResponseDto> getRecord() {
		return Record;
	}

	public void setRecord(List<FDFetchRecordResponseDto> record) {
		Record = record;
	}

}
