package com.oxyloans.dto.fd;

public class FDCommonResponseDto {

	private FDCreationResponseDto SMSAWAY;

	public FDCreationResponseDto getSMSAWAY() {
		return SMSAWAY;
	}

	public void setSMSAWAY(FDCreationResponseDto sMSAWAY) {
		SMSAWAY = sMSAWAY;
	}

}
