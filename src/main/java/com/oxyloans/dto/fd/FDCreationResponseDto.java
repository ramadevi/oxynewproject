package com.oxyloans.dto.fd;

public class FDCreationResponseDto {

	private String Message;

	private String Response;

	private String CORP_ID;

	private String USER_ID;

	private String AGGR_ID;

	private String AGGR_NAME;

	private String URN;

	private String FDACCOUNT;

	private String MATURITY_DATE;

	private String MATURITY_AMOUNT;

	private String INTEREST_RATE;

	private String MATURITY_PROCEEDS;

	private String TYPE_OF_FD;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getResponse() {
		return Response;
	}

	public void setResponse(String response) {
		Response = response;
	}

	public String getCORP_ID() {
		return CORP_ID;
	}

	public void setCORP_ID(String cORP_ID) {
		CORP_ID = cORP_ID;
	}

	public String getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}

	public String getAGGR_ID() {
		return AGGR_ID;
	}

	public void setAGGR_ID(String aGGR_ID) {
		AGGR_ID = aGGR_ID;
	}

	public String getAGGR_NAME() {
		return AGGR_NAME;
	}

	public void setAGGR_NAME(String aGGR_NAME) {
		AGGR_NAME = aGGR_NAME;
	}

	public String getURN() {
		return URN;
	}

	public void setURN(String uRN) {
		URN = uRN;
	}

	public String getFDACCOUNT() {
		return FDACCOUNT;
	}

	public void setFDACCOUNT(String fDACCOUNT) {
		FDACCOUNT = fDACCOUNT;
	}

	public String getMATURITY_DATE() {
		return MATURITY_DATE;
	}

	public void setMATURITY_DATE(String mATURITY_DATE) {
		MATURITY_DATE = mATURITY_DATE;
	}

	public String getMATURITY_AMOUNT() {
		return MATURITY_AMOUNT;
	}

	public void setMATURITY_AMOUNT(String mATURITY_AMOUNT) {
		MATURITY_AMOUNT = mATURITY_AMOUNT;
	}

	public String getINTEREST_RATE() {
		return INTEREST_RATE;
	}

	public void setINTEREST_RATE(String iNTEREST_RATE) {
		INTEREST_RATE = iNTEREST_RATE;
	}

	public String getMATURITY_PROCEEDS() {
		return MATURITY_PROCEEDS;
	}

	public void setMATURITY_PROCEEDS(String mATURITY_PROCEEDS) {
		MATURITY_PROCEEDS = mATURITY_PROCEEDS;
	}

	public String getTYPE_OF_FD() {
		return TYPE_OF_FD;
	}

	public void setTYPE_OF_FD(String tYPE_OF_FD) {
		TYPE_OF_FD = tYPE_OF_FD;
	}

}
