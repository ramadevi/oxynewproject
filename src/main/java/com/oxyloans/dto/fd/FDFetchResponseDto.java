package com.oxyloans.dto.fd;

public class FDFetchResponseDto {

	private FDFetchDto SMSAWAY;

	public FDFetchDto getSMSAWAY() {
		return SMSAWAY;
	}

	public void setSMSAWAY(FDFetchDto sMSAWAY) {
		SMSAWAY = sMSAWAY;
	}

}
