package com.oxyloans.dto.fd;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FDCreationRequestDto {

	
	@JsonProperty
	private String AGGR_NAME = "CUST0939";

	@JsonProperty
	private String AGGR_ID = "OXYIDEAS";

	@JsonProperty
	private String CORP_ID = "PRACHICIB1";

	@JsonProperty
	private String USER_ID = "USER3";

	@JsonProperty
	private String URN = "OXYIDEAS20190201";

	@JsonProperty
	private String ALIAS_ID = "";

	@JsonProperty
	private String UNIQUE_ID;

	@JsonProperty
	private String ACCOUNT;

	@JsonProperty
	private String DAYS_ONLY;

	@JsonProperty
	private String FD_AMOUNT;

	@JsonProperty
	private String TYPEOFDEPOSIT;

	@JsonProperty
	private String DAYS;

	@JsonProperty
	private String MONTHS;

	@JsonProperty
	private String YEARS;

	@JsonProperty
	private String FD_TYPE;

	@JsonProperty
	private String MATURITY_PROCEEDS;

	public String getAGGR_NAME() {
		return AGGR_NAME;
	}

	public void setAGGR_NAME(String aGGR_NAME) {
		AGGR_NAME = aGGR_NAME;
	}

	public String getAGGR_ID() {
		return AGGR_ID;
	}

	public void setAGGR_ID(String aGGR_ID) {
		AGGR_ID = aGGR_ID;
	}

	public String getCORP_ID() {
		return CORP_ID;
	}

	public void setCORP_ID(String cORP_ID) {
		CORP_ID = cORP_ID;
	}

	public String getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}

	public String getURN() {
		return URN;
	}

	public void setURN(String uRN) {
		URN = uRN;
	}

	public String getALIAS_ID() {
		return ALIAS_ID;
	}

	public void setALIAS_ID(String aLIAS_ID) {
		ALIAS_ID = aLIAS_ID;
	}

	public String getUNIQUE_ID() {
		return UNIQUE_ID;
	}

	public void setUNIQUE_ID(String uNIQUE_ID) {
		UNIQUE_ID = uNIQUE_ID;
	}

	public String getACCOUNT() {
		return ACCOUNT;
	}

	public void setACCOUNT(String aCCOUNT) {
		ACCOUNT = aCCOUNT;
	}

	public String getDAYS_ONLY() {
		return DAYS_ONLY;
	}

	public void setDAYS_ONLY(String dAYS_ONLY) {
		DAYS_ONLY = dAYS_ONLY;
	}

	public String getFD_AMOUNT() {
		return FD_AMOUNT;
	}

	public void setFD_AMOUNT(String fD_AMOUNT) {
		FD_AMOUNT = fD_AMOUNT;
	}

	public String getTYPEOFDEPOSIT() {
		return TYPEOFDEPOSIT;
	}

	public void setTYPEOFDEPOSIT(String tYPEOFDEPOSIT) {
		TYPEOFDEPOSIT = tYPEOFDEPOSIT;
	}

	public String getDAYS() {
		return DAYS;
	}

	public void setDAYS(String dAYS) {
		DAYS = dAYS;
	}

	public String getMONTHS() {
		return MONTHS;
	}

	public void setMONTHS(String mONTHS) {
		MONTHS = mONTHS;
	}

	public String getYEARS() {
		return YEARS;
	}

	public void setYEARS(String yEARS) {
		YEARS = yEARS;
	}

	public String getFD_TYPE() {
		return FD_TYPE;
	}

	public void setFD_TYPE(String fD_TYPE) {
		FD_TYPE = fD_TYPE;
	}

	public String getMATURITY_PROCEEDS() {
		return MATURITY_PROCEEDS;
	}

	public void setMATURITY_PROCEEDS(String mATURITY_PROCEEDS) {
		MATURITY_PROCEEDS = mATURITY_PROCEEDS;
	}

}
