package com.oxyloans.dto.entityborrowersdealsdto;

public class OxyLendersAcceptedDealsRequestDto {

	private Integer userId;

	private Integer dealId;

	private Integer groupId;

	private Double participatedAmount;

	private Double rateofInterest;

	private String lenderReturnType;

	private Double processingFee;

	private String paticipationStatus;

	private String participatedDate;

	private int lenderFeeId;

	private String feeStatus;

	private String accountType;

	private String paticipationState;

	private Double lenderRemainingWalletAmount;

	private Double lenderTotalPanLimit;

	private Double totalParticipatedAmount;

	private String lenderParticipationFrom;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Double getParticipatedAmount() {
		return participatedAmount;
	}

	public void setParticipatedAmount(Double participatedAmount) {
		this.participatedAmount = participatedAmount;
	}

	public Double getRateofInterest() {
		return rateofInterest;
	}

	public void setRateofInterest(Double rateofInterest) {
		this.rateofInterest = rateofInterest;
	}

	public String getLenderReturnType() {
		return lenderReturnType;
	}

	public void setLenderReturnType(String lenderReturnType) {
		this.lenderReturnType = lenderReturnType;
	}

	public Double getProcessingFee() {
		return processingFee;
	}

	public void setProcessingFee(Double processingFee) {
		this.processingFee = processingFee;
	}

	public String getPaticipationStatus() {
		return paticipationStatus;
	}

	public void setPaticipationStatus(String paticipationStatus) {
		this.paticipationStatus = paticipationStatus;
	}

	public String getParticipatedDate() {
		return participatedDate;
	}

	public void setParticipatedDate(String participatedDate) {
		this.participatedDate = participatedDate;
	}

	public int getLenderFeeId() {
		return lenderFeeId;
	}

	public void setLenderFeeId(int lenderFeeId) {
		this.lenderFeeId = lenderFeeId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getPaticipationState() {
		return paticipationState;
	}

	public void setPaticipationState(String paticipationState) {
		this.paticipationState = paticipationState;
	}

	public String getFeeStatus() {
		return feeStatus;
	}

	public void setFeeStatus(String feeStatus) {
		this.feeStatus = feeStatus;
	}

	public Double getLenderRemainingWalletAmount() {
		return lenderRemainingWalletAmount;
	}

	public void setLenderRemainingWalletAmount(Double lenderRemainingWalletAmount) {
		this.lenderRemainingWalletAmount = lenderRemainingWalletAmount;
	}

	public Double getLenderTotalPanLimit() {
		return lenderTotalPanLimit;
	}

	public void setLenderTotalPanLimit(Double lenderTotalPanLimit) {
		this.lenderTotalPanLimit = lenderTotalPanLimit;
	}

	public Double getTotalParticipatedAmount() {
		return totalParticipatedAmount;
	}

	public void setTotalParticipatedAmount(Double totalParticipatedAmount) {
		this.totalParticipatedAmount = totalParticipatedAmount;
	}

	public String getLenderParticipationFrom() {
		return lenderParticipationFrom;
	}

	public void setLenderParticipationFrom(String lenderParticipationFrom) {
		this.lenderParticipationFrom = lenderParticipationFrom;
	}

}
