package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

import com.oxyloans.lender.wallet.LenderIndividualReturns;

public class ClosedDealsInformation {
	private Integer closedDealsCount;

	private List<BorrowersDealsResponseDto> borrowersDealsResponseDto;

	private List<LenderIndividualReturns> lenderDealsIndividualReturns;

	private String closedDealsDownloadUrl;

	public Integer getClosedDealsCount() {
		return closedDealsCount;
	}

	public void setClosedDealsCount(Integer closedDealsCount) {
		this.closedDealsCount = closedDealsCount;
	}

	public List<BorrowersDealsResponseDto> getBorrowersDealsResponseDto() {
		return borrowersDealsResponseDto;
	}

	public void setBorrowersDealsResponseDto(List<BorrowersDealsResponseDto> borrowersDealsResponseDto) {
		this.borrowersDealsResponseDto = borrowersDealsResponseDto;
	}

	public List<LenderIndividualReturns> getLenderDealsIndividualReturns() {
		return lenderDealsIndividualReturns;
	}

	public void setLenderDealsIndividualReturns(List<LenderIndividualReturns> lenderDealsIndividualReturns) {
		this.lenderDealsIndividualReturns = lenderDealsIndividualReturns;
	}

	public String getClosedDealsDownloadUrl() {
		return closedDealsDownloadUrl;
	}

	public void setClosedDealsDownloadUrl(String closedDealsDownloadUrl) {
		this.closedDealsDownloadUrl = closedDealsDownloadUrl;
	}
}
