package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;

public class DealInformationRoiToLender {

	private Integer groupId;

	private String dealName;

	private String borrowerName;

	private Double dealAmount;

	private Double rateOfInterest;

	private Double monthlyInterest;

	private Double quartlyInterest;

	private Double halfInterest;

	private Double yearlyInterest;

	private Double endofthedealInterest;

	private Integer duration;

	private String groupName;

	private String fundStartDate;

	private String fundEndDate;

	private String dealLink;

	private Double lenderParticiptionLimit;

	private String fundingStatus;

	private String dealType;

	private LenderPaticipatedResponseDto lenderPaticipatedResponseDto;

	private BigInteger remainingAmountInDeal;

	private BigInteger minimumPaticipationAmount;

	private String feeStatusToParticipate;

	private BigInteger lenderParticipationTotal;

	private Boolean lifeTimeWaiver;

	private BigInteger lifeTimeWaiverLimit;

	private BigInteger lenderRemainingPanLimit;

	private BigInteger lenderTotalParticipationAmount;

	private BigInteger lenderRemainingWalletAmount;

	private boolean validityStatus;

	private boolean lenderValidityStatus;

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public Double getDealAmount() {
		return dealAmount;
	}

	public void setDealAmount(Double dealAmount) {
		this.dealAmount = dealAmount;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public Double getMonthlyInterest() {
		return monthlyInterest;
	}

	public void setMonthlyInterest(Double monthlyInterest) {
		this.monthlyInterest = monthlyInterest;
	}

	public Double getQuartlyInterest() {
		return quartlyInterest;
	}

	public void setQuartlyInterest(Double quartlyInterest) {
		this.quartlyInterest = quartlyInterest;
	}

	public Double getHalfInterest() {
		return halfInterest;
	}

	public void setHalfInterest(Double halfInterest) {
		this.halfInterest = halfInterest;
	}

	public Double getYearlyInterest() {
		return yearlyInterest;
	}

	public void setYearlyInterest(Double yearlyInterest) {
		this.yearlyInterest = yearlyInterest;
	}

	public Double getEndofthedealInterest() {
		return endofthedealInterest;
	}

	public void setEndofthedealInterest(Double endofthedealInterest) {
		this.endofthedealInterest = endofthedealInterest;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getFundStartDate() {
		return fundStartDate;
	}

	public void setFundStartDate(String fundStartDate) {
		this.fundStartDate = fundStartDate;
	}

	public String getFundEndDate() {
		return fundEndDate;
	}

	public void setFundEndDate(String fundEndDate) {
		this.fundEndDate = fundEndDate;
	}

	public String getDealLink() {
		return dealLink;
	}

	public void setDealLink(String dealLink) {
		this.dealLink = dealLink;
	}

	public Double getLenderParticiptionLimit() {
		return lenderParticiptionLimit;
	}

	public void setLenderParticiptionLimit(Double lenderParticiptionLimit) {
		this.lenderParticiptionLimit = lenderParticiptionLimit;
	}

	public String getFundingStatus() {
		return fundingStatus;
	}

	public void setFundingStatus(String fundingStatus) {
		this.fundingStatus = fundingStatus;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public LenderPaticipatedResponseDto getLenderPaticipatedResponseDto() {
		return lenderPaticipatedResponseDto;
	}

	public void setLenderPaticipatedResponseDto(LenderPaticipatedResponseDto lenderPaticipatedResponseDto) {
		this.lenderPaticipatedResponseDto = lenderPaticipatedResponseDto;
	}

	public BigInteger getRemainingAmountInDeal() {
		return remainingAmountInDeal;
	}

	public void setRemainingAmountInDeal(BigInteger remainingAmountInDeal) {
		this.remainingAmountInDeal = remainingAmountInDeal;
	}

	public BigInteger getMinimumPaticipationAmount() {
		return minimumPaticipationAmount;
	}

	public void setMinimumPaticipationAmount(BigInteger minimumPaticipationAmount) {
		this.minimumPaticipationAmount = minimumPaticipationAmount;
	}

	public String getFeeStatusToParticipate() {
		return feeStatusToParticipate;
	}

	public void setFeeStatusToParticipate(String feeStatusToParticipate) {
		this.feeStatusToParticipate = feeStatusToParticipate;
	}

	public BigInteger getLenderParticipationTotal() {
		return lenderParticipationTotal;
	}

	public void setLenderParticipationTotal(BigInteger lenderParticipationTotal) {
		this.lenderParticipationTotal = lenderParticipationTotal;
	}

	public Boolean getLifeTimeWaiver() {
		return lifeTimeWaiver;
	}

	public void setLifeTimeWaiver(Boolean lifeTimeWaiver) {
		this.lifeTimeWaiver = lifeTimeWaiver;
	}

	public BigInteger getLifeTimeWaiverLimit() {
		return lifeTimeWaiverLimit;
	}

	public void setLifeTimeWaiverLimit(BigInteger lifeTimeWaiverLimit) {
		this.lifeTimeWaiverLimit = lifeTimeWaiverLimit;
	}

	public BigInteger getLenderRemainingPanLimit() {
		return lenderRemainingPanLimit;
	}

	public void setLenderRemainingPanLimit(BigInteger lenderRemainingPanLimit) {
		this.lenderRemainingPanLimit = lenderRemainingPanLimit;
	}

	public BigInteger getLenderTotalParticipationAmount() {
		return lenderTotalParticipationAmount;
	}

	public void setLenderTotalParticipationAmount(BigInteger lenderTotalParticipationAmount) {
		this.lenderTotalParticipationAmount = lenderTotalParticipationAmount;
	}

	public BigInteger getLenderRemainingWalletAmount() {
		return lenderRemainingWalletAmount;
	}

	public void setLenderRemainingWalletAmount(BigInteger lenderRemainingWalletAmount) {
		this.lenderRemainingWalletAmount = lenderRemainingWalletAmount;
	}

	public boolean isLenderValidityStatus() {
		return lenderValidityStatus;
	}

	public void setLenderValidityStatus(boolean lenderValidityStatus) {
		this.lenderValidityStatus = lenderValidityStatus;
	}

	public boolean isValidityStatus() {
		return validityStatus;
	}

	public void setValidityStatus(boolean validityStatus) {
		this.validityStatus = validityStatus;
	}

}
