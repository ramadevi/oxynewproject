package com.oxyloans.dto.entityborrowersdealsdto;

public class NewLendersExcelsheetResponseDto {
	private String message ;
	private int userId;
	private String name;
	private String mobileNumber;
	private String pan;
	private String userRegisterOn;
	private Double totalParticipation;
	private String email;
	private Double totalSumParticipatedAmount;
	private Double totalSumUpdationAmount;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getUserRegisterOn() {
		return userRegisterOn;
	}
	public void setUserRegisterOn(String userRegisterOn) {
		this.userRegisterOn = userRegisterOn;
	}
	public Double getTotalParticipation() {
		return totalParticipation;
	}
	public void setTotalParticipation(Double totalParticipation) {
		this.totalParticipation = totalParticipation;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Double getTotalSumParticipatedAmount() {
		return totalSumParticipatedAmount;
	}
	public void setTotalSumParticipatedAmount(Double totalSumParticipatedAmount) {
		this.totalSumParticipatedAmount = totalSumParticipatedAmount;
	}
	public Double getTotalSumUpdationAmount() {
		return totalSumUpdationAmount;
	}
	public void setTotalSumUpdationAmount(Double totalSumUpdationAmount) {
		this.totalSumUpdationAmount = totalSumUpdationAmount;
	}

}
