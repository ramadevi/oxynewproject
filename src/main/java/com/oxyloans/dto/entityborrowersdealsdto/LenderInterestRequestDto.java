package com.oxyloans.dto.entityborrowersdealsdto;

public class LenderInterestRequestDto {

	private String adminStatus;

	private String month;

	private String year;

	private String listOfLenderIds;

	private String remarks;

	private String LenderReturnType;

	private Integer userId;

	public String getAdminStatus() {
		return adminStatus;
	}

	public void setAdminStatus(String adminStatus) {
		this.adminStatus = adminStatus;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getListOfLenderIds() {
		return listOfLenderIds;
	}

	public void setListOfLenderIds(String listOfLenderIds) {
		this.listOfLenderIds = listOfLenderIds;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getLenderReturnType() {
		return LenderReturnType;
	}

	public void setLenderReturnType(String lenderReturnType) {
		LenderReturnType = lenderReturnType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
