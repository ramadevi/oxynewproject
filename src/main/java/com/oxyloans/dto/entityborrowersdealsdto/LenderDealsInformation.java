package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

public class LenderDealsInformation {

	private Double totalParticipatedAmount;

	private Integer userId;

	private List<OxyLendersAcceptedDealsResponseDto> listOfLenderAcceptedDeals;

	public Double getTotalParticipatedAmount() {
		return totalParticipatedAmount;
	}

	public void setTotalParticipatedAmount(Double totalParticipatedAmount) {
		this.totalParticipatedAmount = totalParticipatedAmount;
	}

	public List<OxyLendersAcceptedDealsResponseDto> getListOfLenderAcceptedDeals() {
		return listOfLenderAcceptedDeals;
	}

	public void setListOfLenderAcceptedDeals(List<OxyLendersAcceptedDealsResponseDto> listOfLenderAcceptedDeals) {
		this.listOfLenderAcceptedDeals = listOfLenderAcceptedDeals;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
