package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

public class BorrowerPaymentsStatusDto {

	private Integer count;

	private List<BorrowersTotalFdsResponse> borrowersTotalFdsResponse;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<BorrowersTotalFdsResponse> getBorrowersTotalFdsResponse() {
		return borrowersTotalFdsResponse;
	}

	public void setBorrowersTotalFdsResponse(List<BorrowersTotalFdsResponse> borrowersTotalFdsResponse) {
		this.borrowersTotalFdsResponse = borrowersTotalFdsResponse;
	}

}
