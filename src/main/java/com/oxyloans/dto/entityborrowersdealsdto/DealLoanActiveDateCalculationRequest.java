package com.oxyloans.dto.entityborrowersdealsdto;

public class DealLoanActiveDateCalculationRequest {

	private String fundsStartDate;

	public String getFundsStartDate() {
		return fundsStartDate;
	}

	public void setFundsStartDate(String fundsStartDate) {
		this.fundsStartDate = fundsStartDate;
	}
}
