package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

public class ListOfLenderInterestDetails {

	private Double totalAmount;

	private List<DealLevelLoanEmiCardInformation> DealLevelLoanEmiCardInformation;

	private Integer dealId;

	private String dealName;

	private String interestStartDate;

	private String downloadLink;

	private int noOfLenders;

	private String excelSheet;

	public List<DealLevelLoanEmiCardInformation> getDealLevelLoanEmiCardInformation() {
		return DealLevelLoanEmiCardInformation;
	}

	public void setDealLevelLoanEmiCardInformation(
			List<DealLevelLoanEmiCardInformation> dealLevelLoanEmiCardInformation) {
		DealLevelLoanEmiCardInformation = dealLevelLoanEmiCardInformation;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public String getExcelSheet() {
		return excelSheet;
	}

	public void setExcelSheet(String excelSheet) {
		this.excelSheet = excelSheet;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getInterestStartDate() {
		return interestStartDate;
	}

	public void setInterestStartDate(String interestStartDate) {
		this.interestStartDate = interestStartDate;
	}

	public String getDownloadLink() {
		return downloadLink;
	}

	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}

	public int getNoOfLenders() {
		return noOfLenders;
	}

	public void setNoOfLenders(int noOfLenders) {
		this.noOfLenders = noOfLenders;
	}
}
