package com.oxyloans.dto.entityborrowersdealsdto;

public class RemainderForInterestPaying {

	private String dealName;
	
	private String loanActiveDate;
	
	private ListOfLenderInterestDetails listOfLenderInterestDetails;

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getLoanActiveDate() {
		return loanActiveDate;
	}

	public void setLoanActiveDate(String loanActiveDate) {
		this.loanActiveDate = loanActiveDate;
	}

	public ListOfLenderInterestDetails getListOfLenderInterestDetails() {
		return listOfLenderInterestDetails;
	}

	public void setListOfLenderInterestDetails(ListOfLenderInterestDetails listOfLenderInterestDetails) {
		this.listOfLenderInterestDetails = listOfLenderInterestDetails;
	}	
}
