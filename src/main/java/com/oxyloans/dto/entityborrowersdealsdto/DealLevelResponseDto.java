package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

public class DealLevelResponseDto {
	private String status;

	private Integer dealId;

	private List<BorrowersDealsResponseDto> borrowerDealsResponseDto;

	private Integer assertDealsCount;
	
	private Integer userId;


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public List<BorrowersDealsResponseDto> getBorrowerDealsResponseDto() {
		return borrowerDealsResponseDto;
	}

	public void setBorrowerDealsResponseDto(List<BorrowersDealsResponseDto> borrowerDealsResponseDto) {
		this.borrowerDealsResponseDto = borrowerDealsResponseDto;
	}

	public Integer getAssertDealsCount() {
		return assertDealsCount;
	}

	public void setAssertDealsCount(Integer assertDealsCount) {
		this.assertDealsCount = assertDealsCount;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
