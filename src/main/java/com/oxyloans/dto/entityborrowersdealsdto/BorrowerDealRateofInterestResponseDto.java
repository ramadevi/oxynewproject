package com.oxyloans.dto.entityborrowersdealsdto;

public class BorrowerDealRateofInterestResponseDto {

	private Integer groupId;

	private String groupownerName;

	private Double monthlyInterest;

	private Double QuartelyInterest;

	private Double halfInterest;

	private Double yearlyInterest;

	private Double endOfTheDealInterest;

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupownerName() {
		return groupownerName;
	}

	public void setGroupownerName(String groupownerName) {
		this.groupownerName = groupownerName;
	}

	public Double getMonthlyInterest() {
		return monthlyInterest;
	}

	public void setMonthlyInterest(Double monthlyInterest) {
		this.monthlyInterest = monthlyInterest;
	}

	public Double getQuartelyInterest() {
		return QuartelyInterest;
	}

	public void setQuartelyInterest(Double quartelyInterest) {
		QuartelyInterest = quartelyInterest;
	}

	public Double getHalfInterest() {
		return halfInterest;
	}

	public void setHalfInterest(Double halfInterest) {
		this.halfInterest = halfInterest;
	}

	public Double getYearlyInterest() {
		return yearlyInterest;
	}

	public void setYearlyInterest(Double yearlyInterest) {
		this.yearlyInterest = yearlyInterest;
	}

	public Double getEndOfTheDealInterest() {
		return endOfTheDealInterest;
	}

	public void setEndOfTheDealInterest(Double endOfTheDealInterest) {
		this.endOfTheDealInterest = endOfTheDealInterest;
	}

}
