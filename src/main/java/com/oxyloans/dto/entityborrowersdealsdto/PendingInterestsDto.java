package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

public class PendingInterestsDto {

	private int dealId;

	private List<UnmatchedInterestsDto> unmatchedInterestsDto;

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public List<UnmatchedInterestsDto> getUnmatchedInterestsDto() {
		return unmatchedInterestsDto;
	}

	public void setUnmatchedInterestsDto(List<UnmatchedInterestsDto> unmatchedInterestsDto) {
		this.unmatchedInterestsDto = unmatchedInterestsDto;
	}

}
