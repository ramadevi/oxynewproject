package com.oxyloans.dto.entityborrowersdealsdto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class DealAndParticipationCount {
	
	@JsonProperty
	private int dealCount;
	
	@JsonProperty
	private int participationCount;

	public DealAndParticipationCount(int dealCount, int participationCount) {
	
		this.dealCount = dealCount;
		this.participationCount = participationCount;
	}
	
	

}
