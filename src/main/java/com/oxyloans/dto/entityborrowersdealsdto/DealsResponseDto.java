package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.Date;

public class DealsResponseDto {

	private int dealId;
	private double amount;
	private int userId;
private String dealName;
	
	private int duration;
	
	private Date loanActiveDate;
	
	
	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Date getLoanActiveDate() {
		return loanActiveDate;
	}

	public void setLoanActiveDate(Date loanActiveDate) {
		this.loanActiveDate = loanActiveDate;
	}

	
}
