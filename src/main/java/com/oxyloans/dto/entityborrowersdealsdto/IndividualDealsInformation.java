package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;

public class IndividualDealsInformation {

	private Integer dealId;

	private String dealName;

	private BigInteger sumOfDealAmount;

	private BigInteger sumOfDealAmountByMonthAndYear;

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public BigInteger getSumOfDealAmount() {
		return sumOfDealAmount;
	}

	public void setSumOfDealAmount(BigInteger sumOfDealAmount) {
		this.sumOfDealAmount = sumOfDealAmount;
	}

	public BigInteger getSumOfDealAmountByMonthAndYear() {
		return sumOfDealAmountByMonthAndYear;
	}

	public void setSumOfDealAmountByMonthAndYear(BigInteger sumOfDealAmountByMonthAndYear) {
		this.sumOfDealAmountByMonthAndYear = sumOfDealAmountByMonthAndYear;
	}

}
