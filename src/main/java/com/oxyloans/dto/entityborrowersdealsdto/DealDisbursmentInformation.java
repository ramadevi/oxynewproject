package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;

public class DealDisbursmentInformation {

	private BigInteger sumOfLendersParticipatedAmount;

	private BigInteger sumOfDisbursmentAmountPending;

	private Integer dealId;

	public BigInteger getSumOfLendersParticipatedAmount() {
		return sumOfLendersParticipatedAmount;
	}

	public void setSumOfLendersParticipatedAmount(BigInteger sumOfLendersParticipatedAmount) {
		this.sumOfLendersParticipatedAmount = sumOfLendersParticipatedAmount;
	}

	public BigInteger getSumOfDisbursmentAmountPending() {
		return sumOfDisbursmentAmountPending;
	}

	public void setSumOfDisbursmentAmountPending(BigInteger sumOfDisbursmentAmountPending) {
		this.sumOfDisbursmentAmountPending = sumOfDisbursmentAmountPending;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}
}
