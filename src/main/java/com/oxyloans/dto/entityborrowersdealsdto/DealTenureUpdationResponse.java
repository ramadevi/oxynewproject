package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.Calendar;
import java.util.Date;

public class DealTenureUpdationResponse {

	private int dealId;
	
	private int negativeDealId;
	
	private String negativeDealName;
	
	private String dealName;
	
	private String negativeDealCloseDate;
	
	private String dealCloseDate;
	
	private Integer tenure;
	
	private Integer negativeTenure;
	
	private Date loanActiveDate;
	
	private Date negativeLoanActiveDate;
	
	private Calendar remainderDate;

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getDealCloseDate() {
		return dealCloseDate;
	}

	public void setDealCloseDate(String dealCloseDate) {
		this.dealCloseDate = dealCloseDate;
	}

	public Integer getTenure() {
		return tenure;
	}

	public void setTenure(Integer tenure) {
		this.tenure = tenure;
	}

	

	public Calendar getRemainderDate() {
		return remainderDate;
	}

	public void setRemainderDate(Calendar remainderDate) {
		this.remainderDate = remainderDate;
	}

	public int getNegativeDealId() {
		return negativeDealId;
	}

	public void setNegativeDealId(int negativeDealId) {
		this.negativeDealId = negativeDealId;
	}

	public String getNegativeDealName() {
		return negativeDealName;
	}

	public void setNegativeDealName(String negativeDealName) {
		this.negativeDealName = negativeDealName;
	}

	public String getNegativeDealCloseDate() {
		return negativeDealCloseDate;
	}

	public void setNegativeDealCloseDate(String negativeDealCloseDate) {
		this.negativeDealCloseDate = negativeDealCloseDate;
	}

	public Integer getNegativeTenure() {
		return negativeTenure;
	}

	public void setNegativeTenure(Integer negativeTenure) {
		this.negativeTenure = negativeTenure;
	}

	public Date getLoanActiveDate() {
		return loanActiveDate;
	}

	public void setLoanActiveDate(Date loanActiveDate) {
		this.loanActiveDate = loanActiveDate;
	}

	public Date getNegativeLoanActiveDate() {
		return negativeLoanActiveDate;
	}

	public void setNegativeLoanActiveDate(Date negativeLoanActiveDate) {
		this.negativeLoanActiveDate = negativeLoanActiveDate;
	}

	

	
	
	
	
}
