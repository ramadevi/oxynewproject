package com.oxyloans.dto.entityborrowersdealsdto;

import java.sql.Timestamp;

public class DealLevelRequestDto {

	private String disbursedDate;

	private Timestamp dealCloseDate;

	private Integer dealId;

	private String statusType;

	private int borrowerId;

	private String utm;

	private String uploadedDate;

	private double loanAmount;

	private String idsMappedToDeals;

	private String amountMappedToDeals;

	private String amountType;

	private String dealDiscription;

	private double minimumParticipation;

	private double maxmumparticipation;

	private double rateofinterest;

	private Integer tenure;

	private double participationExtendingAmount;

	private String messageType;

	private String firstInput;

	private String secondInput;

	private String dealName;

	public String getDisbursedDate() {
		return disbursedDate;
	}

	public void setDisbursedDate(String disbursedDate) {
		this.disbursedDate = disbursedDate;
	}

	public Timestamp getDealCloseDate() {
		return dealCloseDate;
	}

	public void setDealCloseDate(Timestamp dealCloseDate) {
		this.dealCloseDate = dealCloseDate;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getStatusType() {
		return statusType;
	}

	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}

	public int getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(int borrowerId) {
		this.borrowerId = borrowerId;
	}

	public String getUtm() {
		return utm;
	}

	public void setUtm(String utm) {
		this.utm = utm;
	}

	public String getUploadedDate() {
		return uploadedDate;
	}

	public void setUploadedDate(String uploadedDate) {
		this.uploadedDate = uploadedDate;
	}

	public double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getIdsMappedToDeals() {
		return idsMappedToDeals;
	}

	public String getAmountMappedToDeals() {
		return amountMappedToDeals;
	}

	public void setIdsMappedToDeals(String idsMappedToDeals) {
		this.idsMappedToDeals = idsMappedToDeals;
	}

	public void setAmountMappedToDeals(String amountMappedToDeals) {
		this.amountMappedToDeals = amountMappedToDeals;
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public String getDealDiscription() {
		return dealDiscription;
	}

	public void setDealDiscription(String dealDiscription) {
		this.dealDiscription = dealDiscription;
	}

	public double getMinimumParticipation() {
		return minimumParticipation;
	}

	public void setMinimumParticipation(double minimumParticipation) {
		this.minimumParticipation = minimumParticipation;
	}

	public double getMaxmumparticipation() {
		return maxmumparticipation;
	}

	public void setMaxmumparticipation(double maxmumparticipation) {
		this.maxmumparticipation = maxmumparticipation;
	}

	public double getRateofinterest() {
		return rateofinterest;
	}

	public void setRateofinterest(double rateofinterest) {
		this.rateofinterest = rateofinterest;
	}

	public Integer getTenure() {
		return tenure;
	}

	public void setTenure(Integer tenure) {
		this.tenure = tenure;
	}

	public double getParticipationExtendingAmount() {
		return participationExtendingAmount;
	}

	public void setParticipationExtendingAmount(double participationExtendingAmount) {
		this.participationExtendingAmount = participationExtendingAmount;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getFirstInput() {
		return firstInput;
	}

	public void setFirstInput(String firstInput) {
		this.firstInput = firstInput;
	}

	public String getSecondInput() {
		return secondInput;
	}

	public void setSecondInput(String secondInput) {
		this.secondInput = secondInput;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

}
