package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;

public class LenderFurtherPaymentDetails {
	private String date;

	private BigInteger amount;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public BigInteger getAmount() {
		return amount;
	}

	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}
}
