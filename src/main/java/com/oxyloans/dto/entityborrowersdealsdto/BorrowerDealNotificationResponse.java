package com.oxyloans.dto.entityborrowersdealsdto;

public class BorrowerDealNotificationResponse {
	
	
	private String message ;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
