package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;

public class DealQuarterlyReportsResponse {

	private int id;
	private String name;
	private String pan;
	private BigInteger totalWalletLoaded;
	private Double totalparticipatedAmount;
	private Double outStandingAmount;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public BigInteger getTotalWalletLoaded() {
		return totalWalletLoaded;
	}

	public void setTotalWalletLoaded(BigInteger totalWalletLoaded) {
		this.totalWalletLoaded = totalWalletLoaded;
	}

	public Double getTotalparticipatedAmount() {
		return totalparticipatedAmount;
	}

	public void setTotalparticipatedAmount(Double totalparticipatedAmount) {
		this.totalparticipatedAmount = totalparticipatedAmount;
	}

	public Double getOutStandingAmount() {
		return outStandingAmount;
	}

	public void setOutStandingAmount(Double outStandingAmount) {
		this.outStandingAmount = outStandingAmount;
	}

	@Override
	public String toString() {
		return "DealQuarterlyReportsResponse [id=" + id + ", outStandingAmount=" + outStandingAmount + "]";
	}

}
