package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.Date;

public class BorrowerPaymentsResponse {

	private int id;

	private int userId;

	private int dealId;

	private double amount;

	private Date paymentDate;

	private String remarks;

	private String borrowerPaymentType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getBorrowerPaymentType() {
		return borrowerPaymentType;
	}

	public void setBorrowerPaymentType(String borrowerPaymentType) {
		this.borrowerPaymentType = borrowerPaymentType;
	}

}
