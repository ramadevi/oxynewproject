package com.oxyloans.dto.entityborrowersdealsdto;

public class DealLoanActiveDateCalculationResponse {

	private String firstInterestDate;

	public String getFirstInterestDate() {
		return firstInterestDate;
	}

	public void setFirstInterestDate(String firstInterestDate) {
		this.firstInterestDate = firstInterestDate;
	}
}
