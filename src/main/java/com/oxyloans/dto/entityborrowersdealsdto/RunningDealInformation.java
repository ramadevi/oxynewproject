package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;
import java.util.List;

public class RunningDealInformation {

	private List<BorrowersDealsResponseDto> listOfRunningDeals;

	private Integer totalRunningDealsCount;

	private BigInteger totalRunningDealsAmount;

	private BigInteger allDealsParticipationSum;

	private BigInteger totalCurrentDealsAmount;

	public List<BorrowersDealsResponseDto> getListOfRunningDeals() {
		return listOfRunningDeals;
	}

	public void setListOfRunningDeals(List<BorrowersDealsResponseDto> listOfRunningDeals) {
		this.listOfRunningDeals = listOfRunningDeals;
	}

	public Integer getTotalRunningDealsCount() {
		return totalRunningDealsCount;
	}

	public void setTotalRunningDealsCount(Integer totalRunningDealsCount) {
		this.totalRunningDealsCount = totalRunningDealsCount;
	}

	public BigInteger getTotalRunningDealsAmount() {
		return totalRunningDealsAmount;
	}

	public void setTotalRunningDealsAmount(BigInteger totalRunningDealsAmount) {
		this.totalRunningDealsAmount = totalRunningDealsAmount;
	}

	public BigInteger getAllDealsParticipationSum() {
		return allDealsParticipationSum;
	}

	public void setAllDealsParticipationSum(BigInteger allDealsParticipationSum) {
		this.allDealsParticipationSum = allDealsParticipationSum;
	}

	public BigInteger getTotalCurrentDealsAmount() {
		return totalCurrentDealsAmount;
	}

	public void setTotalCurrentDealsAmount(BigInteger totalCurrentDealsAmount) {
		this.totalCurrentDealsAmount = totalCurrentDealsAmount;
	}

}
