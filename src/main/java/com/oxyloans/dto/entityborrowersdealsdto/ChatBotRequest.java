package com.oxyloans.dto.entityborrowersdealsdto;

public class ChatBotRequest {
	   private String chatId;
		
		private int limit;;
		private String message;
		public String getChatId() {
			return chatId;
		}
		public void setChatId(String chatId) {
			this.chatId = chatId;
		}
		public int getLimit() {
			return limit;
		}
		public void setLimit(int limit) {
			this.limit = limit;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		
}
