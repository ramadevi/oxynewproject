package com.oxyloans.dto.entityborrowersdealsdto;

public class LenderPaidAmountRequestDto {
	
	private String lenderFeePayment;
	
    private String startDate;
	
	private String endDate;
	
	
	
	

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getLenderFeePayment() {
		return lenderFeePayment;
	}

	public void setLenderFeePayment(String lenderFeePayment) {
		this.lenderFeePayment = lenderFeePayment;
	}
	

}
