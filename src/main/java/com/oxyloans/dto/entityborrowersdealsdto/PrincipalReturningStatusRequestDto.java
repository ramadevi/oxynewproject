package com.oxyloans.dto.entityborrowersdealsdto;

public class PrincipalReturningStatusRequestDto {

	private Integer dealId;

	private String status;

	private String type;

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
