package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;
import java.util.List;

public class LenderParticipationUpdatedInfo {

	private int userId;

	private double roi;

	private Double amount;

	private String lenderReturnType;

	private String upatedDate;

	private Double InterestAmount;

	private Integer differenceInDays;

	private String emiStartDate;

	private BigInteger principalAmount;

	private Integer sno;

	private List<LenderParticipationUpdatedInfo> lenderParticipationUpdatedInfo;

	private Integer tableId;

	private String tableType;

	public Integer getDifferenceInDays() {
		return differenceInDays;
	}

	public void setDifferenceInDays(Integer differenceInDays) {
		this.differenceInDays = differenceInDays;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getUpatedDate() {
		return upatedDate;
	}

	public void setUpatedDate(String upatedDate) {
		this.upatedDate = upatedDate;
	}

	public Double getInterestAmount() {
		return InterestAmount;
	}

	public void setInterestAmount(Double interestAmount) {
		InterestAmount = interestAmount;
	}

	public List<LenderParticipationUpdatedInfo> getLenderParticipationUpdatedInfo() {
		return lenderParticipationUpdatedInfo;
	}

	public void setLenderParticipationUpdatedInfo(List<LenderParticipationUpdatedInfo> lenderParticipationUpdatedInfo) {
		this.lenderParticipationUpdatedInfo = lenderParticipationUpdatedInfo;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public double getRoi() {
		return roi;
	}

	public void setRoi(double roi) {
		this.roi = roi;
	}

	public String getLenderReturnType() {
		return lenderReturnType;
	}

	public void setLenderReturnType(String lenderReturnType) {
		this.lenderReturnType = lenderReturnType;
	}

	public String getEmiStartDate() {
		return emiStartDate;
	}

	public void setEmiStartDate(String emiStartDate) {
		this.emiStartDate = emiStartDate;
	}

	public BigInteger getPrincipalAmount() {
		return principalAmount;
	}

	public void setPrincipalAmount(BigInteger principalAmount) {
		this.principalAmount = principalAmount;
	}

	public Integer getSno() {
		return sno;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public String getTableType() {
		return tableType;
	}

	public void setTableType(String tableType) {
		this.tableType = tableType;
	}

}
