package com.oxyloans.dto.entityborrowersdealsdto;

public class BorrowerFdDataResponse {
	
	private String name;
	private String address;
	private Integer userId;
	private String BankName;
	private String universityName;
	private Double fdAmount;
	private String mobileNumber;
	private String AcouuntNumber;
	private String panNumber;
	private String city;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getBankName() {
		return BankName;
	}
	public void setBankName(String bankName) {
		BankName = bankName;
	}
	public String getUniversityName() {
		return universityName;
	}
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
	public Double getFdAmount() {
		return fdAmount;
	}
	public void setFdAmount(Double fdAmount) {
		this.fdAmount = fdAmount;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAcouuntNumber() {
		return AcouuntNumber;
	}
	public void setAcouuntNumber(String acouuntNumber) {
		AcouuntNumber = acouuntNumber;
	}
	public String getPanNumber() {
		return panNumber;
	}
	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	

}
