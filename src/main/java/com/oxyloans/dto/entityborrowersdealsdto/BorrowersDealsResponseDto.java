package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

public class BorrowersDealsResponseDto {
	private Integer dealId;

	private String dealName;

	private String borrowerName;

	private Double dealAmount;

	private String fundsAcceptanceStartDate;

	private String fundsAcceptanceEndDate;

	private Double borrowerRateOfInterest;

	private Double averageValueForLender;

	private String dealLink;

	private Integer duration;

	private String groupName;

	private String linkDescription;

	private String linkTitle;

	private String linkUrl;

	private String whatAppResponseUrl;

	private String loanActiveDate;

	private List<BorrowerDealRateofInterestResponseDto> getIndividualDealsInformation;

	private String whatAppLinkSentToLendersGroup;

	private String messageSentToLenders;

	private Double paticipationLimitToLenders;

	private String fundingStatus;

	private Double feeCollectedFromBorrower;

	private Double feeROIforBorrower;

	private String AgreementsGenerationStatus;

	private String dealType;

	private String borrowerDealClosingStatus;

	private String borrowerDealClosedDate;

	private BigInteger totalPaticipation;

	private BigInteger dealAmountGivenBysir;

	private String borrowerClosedDate;

	private BigInteger dealPaticipatedAmount;

	private BigInteger dealCurrentAmount;

	private String withDrawalStatus;

	private double withDrawalRoi;

	private String participationLenderType;

	private Integer totalLendersCount;

	private String statusToDisplayTotalGroups;

	private BigInteger totalInterestEarned;

	private BigInteger movedFromPaticipationToWallet;

	private String lenderParticipatedDate;

	private double rateOfInterest;

	private Integer dealMappedToBorrowerId;

	private Integer adminId;

	private BigInteger remainingAmountToPaticipateInDeal;

	private String LenderReturnType;

	private BigInteger withdrawalAndPrincipalReturned;

	private BigInteger dealAmountReturnedToWallet;

	private String borrowerIdsMappedTodeal;

	private boolean enachStatus;

	private Boolean lenderPaticipateStatus;

	private String dealCreatedType;

	private BigInteger minimumPaticipationAmount;

	private Boolean lifeTimeWaiver;

	private Double lifeTimeWaiverLimit;

	private BigInteger totalPaticipatedAmount;

	private BigInteger lenderPaticipationAmount;

	private String dealSubtype;

	private String dealClosedDate;

	private Integer numberOfLendersParticipated;

	private List<DealLevelRequestDto> idsWithLoanAmount;

	private String firstParticipationDate;

	private String lastParticipationDate;

	private String whatsappGroupChatId;

	private String whatsappResponseLink;

	private String feeStatusToParticipate;

	private String emiEndDate;

	private List<Map<String, Integer>> listOfBorrowersMappedTodeal;

	private String withdrawStatus;

	private Double roiForWithdraw;

	private Boolean borrowersMappedStatus = false;

	private String repaymentType;

	private String dealOpenStatus;

	private String dealParticipationStatus;

	public String getDealOpenStatus() {
		return dealOpenStatus;
	}

	public void setDealOpenStatus(String dealOpenStatus) {
		this.dealOpenStatus = dealOpenStatus;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public Double getDealAmount() {
		return dealAmount;
	}

	public void setDealAmount(Double dealAmount) {
		this.dealAmount = dealAmount;
	}

	public String getFundsAcceptanceStartDate() {
		return fundsAcceptanceStartDate;
	}

	public void setFundsAcceptanceStartDate(String fundsAcceptanceStartDate) {
		this.fundsAcceptanceStartDate = fundsAcceptanceStartDate;
	}

	public String getFundsAcceptanceEndDate() {
		return fundsAcceptanceEndDate;
	}

	public void setFundsAcceptanceEndDate(String fundsAcceptanceEndDate) {
		this.fundsAcceptanceEndDate = fundsAcceptanceEndDate;
	}

	public Double getBorrowerRateOfInterest() {
		return borrowerRateOfInterest;
	}

	public void setBorrowerRateOfInterest(Double borrowerRateOfInterest) {
		this.borrowerRateOfInterest = borrowerRateOfInterest;
	}

	public List<BorrowerDealRateofInterestResponseDto> getGetIndividualDealsInformation() {
		return getIndividualDealsInformation;
	}

	public void setGetIndividualDealsInformation(
			List<BorrowerDealRateofInterestResponseDto> getIndividualDealsInformation) {
		this.getIndividualDealsInformation = getIndividualDealsInformation;
	}

	public Double getAverageValueForLender() {
		return averageValueForLender;
	}

	public void setAverageValueForLender(Double averageValueForLender) {
		this.averageValueForLender = averageValueForLender;
	}

	public String getDealLink() {
		return dealLink;
	}

	public void setDealLink(String dealLink) {
		this.dealLink = dealLink;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getLinkDescription() {
		return linkDescription;
	}

	public void setLinkDescription(String linkDescription) {
		this.linkDescription = linkDescription;
	}

	public String getLinkTitle() {
		return linkTitle;
	}

	public void setLinkTitle(String linkTitle) {
		this.linkTitle = linkTitle;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public String getWhatAppResponseUrl() {
		return whatAppResponseUrl;
	}

	public void setWhatAppResponseUrl(String whatAppResponseUrl) {
		this.whatAppResponseUrl = whatAppResponseUrl;
	}

	public String getWhatAppLinkSentToLendersGroup() {
		return whatAppLinkSentToLendersGroup;
	}

	public void setWhatAppLinkSentToLendersGroup(String whatAppLinkSentToLendersGroup) {
		this.whatAppLinkSentToLendersGroup = whatAppLinkSentToLendersGroup;
	}

	public String getLoanActiveDate() {
		return loanActiveDate;
	}

	public void setLoanActiveDate(String loanActiveDate) {
		this.loanActiveDate = loanActiveDate;
	}

	public String getMessageSentToLenders() {
		return messageSentToLenders;
	}

	public void setMessageSentToLenders(String messageSentToLenders) {
		this.messageSentToLenders = messageSentToLenders;
	}

	public Double getPaticipationLimitToLenders() {
		return paticipationLimitToLenders;
	}

	public void setPaticipationLimitToLenders(Double paticipationLimitToLenders) {
		this.paticipationLimitToLenders = paticipationLimitToLenders;
	}

	public String getFundingStatus() {
		return fundingStatus;
	}

	public void setFundingStatus(String fundingStatus) {
		this.fundingStatus = fundingStatus;
	}

	public Double getFeeCollectedFromBorrower() {
		return feeCollectedFromBorrower;
	}

	public void setFeeCollectedFromBorrower(Double feeCollectedFromBorrower) {
		this.feeCollectedFromBorrower = feeCollectedFromBorrower;
	}

	public Double getFeeROIforBorrower() {
		return feeROIforBorrower;
	}

	public void setFeeROIforBorrower(Double feeROIforBorrower) {
		this.feeROIforBorrower = feeROIforBorrower;
	}

	public String getAgreementsGenerationStatus() {
		return AgreementsGenerationStatus;
	}

	public void setAgreementsGenerationStatus(String agreementsGenerationStatus) {
		AgreementsGenerationStatus = agreementsGenerationStatus;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getBorrowerDealClosingStatus() {
		return borrowerDealClosingStatus;
	}

	public void setBorrowerDealClosingStatus(String borrowerDealClosingStatus) {
		this.borrowerDealClosingStatus = borrowerDealClosingStatus;
	}

	public String getBorrowerDealClosedDate() {
		return borrowerDealClosedDate;
	}

	public void setBorrowerDealClosedDate(String borrowerDealClosedDate) {
		this.borrowerDealClosedDate = borrowerDealClosedDate;
	}

	public BigInteger getTotalPaticipation() {
		return totalPaticipation;
	}

	public void setTotalPaticipation(BigInteger totalPaticipation) {
		this.totalPaticipation = totalPaticipation;
	}

	public BigInteger getDealAmountGivenBysir() {
		return dealAmountGivenBysir;
	}

	public void setDealAmountGivenBysir(BigInteger dealAmountGivenBysir) {
		this.dealAmountGivenBysir = dealAmountGivenBysir;
	}

	public String getBorrowerClosedDate() {
		return borrowerClosedDate;
	}

	public void setBorrowerClosedDate(String borrowerClosedDate) {
		this.borrowerClosedDate = borrowerClosedDate;
	}

	public BigInteger getDealPaticipatedAmount() {
		return dealPaticipatedAmount;
	}

	public void setDealPaticipatedAmount(BigInteger dealPaticipatedAmount) {
		this.dealPaticipatedAmount = dealPaticipatedAmount;
	}

	public BigInteger getDealCurrentAmount() {
		return dealCurrentAmount;
	}

	public void setDealCurrentAmount(BigInteger dealCurrentAmount) {
		this.dealCurrentAmount = dealCurrentAmount;
	}

	public String getWithDrawalStatus() {
		return withDrawalStatus;
	}

	public void setWithDrawalStatus(String withDrawalStatus) {
		this.withDrawalStatus = withDrawalStatus;
	}

	public double getWithDrawalRoi() {
		return withDrawalRoi;
	}

	public void setWithDrawalRoi(double withDrawalRoi) {
		this.withDrawalRoi = withDrawalRoi;
	}

	public String getParticipationLenderType() {
		return participationLenderType;
	}

	public void setParticipationLenderType(String participationLenderType) {
		this.participationLenderType = participationLenderType;
	}

	public Integer getTotalLendersCount() {
		return totalLendersCount;
	}

	public void setTotalLendersCount(Integer totalLendersCount) {
		this.totalLendersCount = totalLendersCount;
	}

	public String getStatusToDisplayTotalGroups() {
		return statusToDisplayTotalGroups;
	}

	public void setStatusToDisplayTotalGroups(String statusToDisplayTotalGroups) {
		this.statusToDisplayTotalGroups = statusToDisplayTotalGroups;
	}

	public BigInteger getTotalInterestEarned() {
		return totalInterestEarned;
	}

	public void setTotalInterestEarned(BigInteger totalInterestEarned) {
		this.totalInterestEarned = totalInterestEarned;
	}

	public BigInteger getMovedFromPaticipationToWallet() {
		return movedFromPaticipationToWallet;
	}

	public void setMovedFromPaticipationToWallet(BigInteger movedFromPaticipationToWallet) {
		this.movedFromPaticipationToWallet = movedFromPaticipationToWallet;
	}

	public String getLenderParticipatedDate() {
		return lenderParticipatedDate;
	}

	public void setLenderParticipatedDate(String lenderParticipatedDate) {
		this.lenderParticipatedDate = lenderParticipatedDate;
	}

	public double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public Integer getDealMappedToBorrowerId() {
		return dealMappedToBorrowerId;
	}

	public void setDealMappedToBorrowerId(Integer dealMappedToBorrowerId) {
		this.dealMappedToBorrowerId = dealMappedToBorrowerId;
	}

	public Integer getAdminId() {
		return adminId;
	}

	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}

	public BigInteger getRemainingAmountToPaticipateInDeal() {
		return remainingAmountToPaticipateInDeal;
	}

	public void setRemainingAmountToPaticipateInDeal(BigInteger remainingAmountToPaticipateInDeal) {
		this.remainingAmountToPaticipateInDeal = remainingAmountToPaticipateInDeal;
	}

	public String getLenderReturnType() {
		return LenderReturnType;
	}

	public void setLenderReturnType(String lenderReturnType) {
		LenderReturnType = lenderReturnType;
	}

	public BigInteger getWithdrawalAndPrincipalReturned() {
		return withdrawalAndPrincipalReturned;
	}

	public BigInteger getDealAmountReturnedToWallet() {
		return dealAmountReturnedToWallet;
	}

	public void setWithdrawalAndPrincipalReturned(BigInteger withdrawalAndPrincipalReturned) {
		this.withdrawalAndPrincipalReturned = withdrawalAndPrincipalReturned;
	}

	public void setDealAmountReturnedToWallet(BigInteger dealAmountReturnedToWallet) {
		this.dealAmountReturnedToWallet = dealAmountReturnedToWallet;
	}

	public String getBorrowerIdsMappedTodeal() {
		return borrowerIdsMappedTodeal;
	}

	public boolean isEnachStatus() {
		return enachStatus;
	}

	public void setBorrowerIdsMappedTodeal(String borrowerIdsMappedTodeal) {
		this.borrowerIdsMappedTodeal = borrowerIdsMappedTodeal;
	}

	public void setEnachStatus(boolean enachStatus) {
		this.enachStatus = enachStatus;
	}

	public Boolean getLenderPaticipateStatus() {
		return lenderPaticipateStatus;
	}

	public String getDealCreatedType() {
		return dealCreatedType;
	}

	public void setLenderPaticipateStatus(Boolean lenderPaticipateStatus) {
		this.lenderPaticipateStatus = lenderPaticipateStatus;
	}

	public void setDealCreatedType(String dealCreatedType) {
		this.dealCreatedType = dealCreatedType;
	}

	public BigInteger getMinimumPaticipationAmount() {
		return minimumPaticipationAmount;
	}

	public BigInteger getLenderPaticipationAmount() {
		return lenderPaticipationAmount;
	}

	public void setMinimumPaticipationAmount(BigInteger minimumPaticipationAmount) {
		this.minimumPaticipationAmount = minimumPaticipationAmount;
	}

	public void setLenderPaticipationAmount(BigInteger lenderPaticipationAmount) {
		this.lenderPaticipationAmount = lenderPaticipationAmount;
	}

	public BigInteger getTotalPaticipatedAmount() {
		return totalPaticipatedAmount;
	}

	public void setTotalPaticipatedAmount(BigInteger totalPaticipatedAmount) {
		this.totalPaticipatedAmount = totalPaticipatedAmount;
	}

	public String getDealSubtype() {
		return dealSubtype;
	}

	public void setDealSubtype(String dealSubtype) {
		this.dealSubtype = dealSubtype;
	}

	public String getDealClosedDate() {
		return dealClosedDate;
	}

	public Integer getNumberOfLendersParticipated() {
		return numberOfLendersParticipated;
	}

	public void setDealClosedDate(String dealClosedDate) {
		this.dealClosedDate = dealClosedDate;
	}

	public void setNumberOfLendersParticipated(Integer numberOfLendersParticipated) {
		this.numberOfLendersParticipated = numberOfLendersParticipated;
	}

	public List<DealLevelRequestDto> getIdsWithLoanAmount() {
		return idsWithLoanAmount;
	}

	public void setIdsWithLoanAmount(List<DealLevelRequestDto> idsWithLoanAmount) {
		this.idsWithLoanAmount = idsWithLoanAmount;
	}

	public String getFirstParticipationDate() {
		return firstParticipationDate;
	}

	public void setFirstParticipationDate(String firstParticipationDate) {
		this.firstParticipationDate = firstParticipationDate;
	}

	public String getLastParticipationDate() {
		return lastParticipationDate;
	}

	public void setLastParticipationDate(String lastParticipationDate) {
		this.lastParticipationDate = lastParticipationDate;
	}

	public String getWhatsappGroupChatId() {
		return whatsappGroupChatId;
	}

	public String getWhatsappResponseLink() {
		return whatsappResponseLink;
	}

	public void setWhatsappGroupChatId(String whatsappGroupChatId) {
		this.whatsappGroupChatId = whatsappGroupChatId;
	}

	public void setWhatsappResponseLink(String whatsappResponseLink) {
		this.whatsappResponseLink = whatsappResponseLink;
	}

	public String getFeeStatusToParticipate() {
		return feeStatusToParticipate;
	}

	public void setFeeStatusToParticipate(String feeStatusToParticipate) {
		this.feeStatusToParticipate = feeStatusToParticipate;
	}

	public String getEmiEndDate() {
		return emiEndDate;
	}

	public void setEmiEndDate(String emiEndDate) {
		this.emiEndDate = emiEndDate;
	}

	public List<Map<String, Integer>> getListOfBorrowersMappedTodeal() {
		return listOfBorrowersMappedTodeal;
	}

	public void setListOfBorrowersMappedTodeal(List<Map<String, Integer>> listOfBorrowersMappedTodeal) {
		this.listOfBorrowersMappedTodeal = listOfBorrowersMappedTodeal;
	}

	public String getWithdrawStatus() {
		return withdrawStatus;
	}

	public void setWithdrawStatus(String withdrawStatus) {
		this.withdrawStatus = withdrawStatus;
	}

	public Double getRoiForWithdraw() {
		return roiForWithdraw;
	}

	public void setRoiForWithdraw(Double roiForWithdraw) {
		this.roiForWithdraw = roiForWithdraw;
	}

	public Boolean getBorrowersMappedStatus() {
		return borrowersMappedStatus;
	}

	public void setBorrowersMappedStatus(Boolean borrowersMappedStatus) {
		this.borrowersMappedStatus = borrowersMappedStatus;
	}

	public Boolean getLifeTimeWaiver() {
		return lifeTimeWaiver;
	}

	public void setLifeTimeWaiver(Boolean lifeTimeWaiver) {
		this.lifeTimeWaiver = lifeTimeWaiver;
	}

	public Double getLifeTimeWaiverLimit() {
		return lifeTimeWaiverLimit;
	}

	public void setLifeTimeWaiverLimit(Double lifeTimeWaiverLimit) {
		this.lifeTimeWaiverLimit = lifeTimeWaiverLimit;
	}

	public String getRepaymentType() {
		return repaymentType;
	}

	public void setRepaymentType(String repaymentType) {
		this.repaymentType = repaymentType;
	}

	public String getDealParticipationStatus() {
		return dealParticipationStatus;
	}

	public void setDealParticipationStatus(String dealParticipationStatus) {
		this.dealParticipationStatus = dealParticipationStatus;
	}

}
