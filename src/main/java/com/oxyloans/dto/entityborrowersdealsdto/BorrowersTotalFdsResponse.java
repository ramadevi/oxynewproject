package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.Date;

public class BorrowersTotalFdsResponse {
	private Integer borrowerId;

	private Date FdCreatedDate;

	private double amountRetunredToRepayment;

	private double amountReturnedToAnother;

	private String loanId;

	private double totalAmount;

	private String s3_download_link;

	private String lenderReturnsType;

	private Integer count;

	private Integer userId;

	private String bankName;

	private String repaymentStatus;

	private Double remainingRepaymentAmount;

	public double getAmountRetunredToRepayment() {
		return amountRetunredToRepayment;
	}

	public double getAmountReturnedToAnother() {
		return amountReturnedToAnother;
	}

	public void setAmountRetunredToRepayment(double amountRetunredToRepayment) {
		this.amountRetunredToRepayment = amountRetunredToRepayment;
	}

	public void setAmountReturnedToAnother(double amountReturnedToAnother) {
		this.amountReturnedToAnother = amountReturnedToAnother;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getS3_download_link() {
		return s3_download_link;
	}

	public void setS3_download_link(String s3_download_link) {
		this.s3_download_link = s3_download_link;
	}

	public String getLenderReturnsType() {
		return lenderReturnsType;
	}

	public void setLenderReturnsType(String lenderReturnsType) {
		this.lenderReturnsType = lenderReturnsType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public String getRepaymentStatus() {
		return repaymentStatus;
	}

	public void setRepaymentStatus(String repaymentStatus) {
		this.repaymentStatus = repaymentStatus;
	}

	public Double getRemainingRepaymentAmount() {
		return remainingRepaymentAmount;
	}

	public void setRemainingRepaymentAmount(Double remainingRepaymentAmount) {
		this.remainingRepaymentAmount = remainingRepaymentAmount;
	}

	public Date getFdCreatedDate() {
		return FdCreatedDate;
	}

	public void setFdCreatedDate(Date fdCreatedDate) {
		FdCreatedDate = fdCreatedDate;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
}