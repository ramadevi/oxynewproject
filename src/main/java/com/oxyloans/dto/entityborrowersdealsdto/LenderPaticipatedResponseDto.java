package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class LenderPaticipatedResponseDto {

	private Integer dealId;

	private Integer groupId;

	private Double paticipatedAmount;

	private Double processingFee;

	private String feeStatus;

	private String lederReturnType;

	private Double rateOfInterest;

	private String registeredDate;

	private String participatedOn;

	private String dealName;

	private String dealBorrowerName;

	private Double dealAmount;

	private Double dealRateofinterest;

	private Integer dealDuration;

	private Integer lenderId;

	private String paticipatedState;

	private String groupName;

	private String lenderName;

	private String accountNumber;

	private String ifscCode;

	private String editOptionConformation;

	private String participationStatus;

	private String pricipalReturnedStatus;

	private String firstInterestDate;

	private BigInteger currentValue;

	private String currentStatus;

	private String loanStatementValueCheck;

	private String pricipalReturned;

	private String dealType;

	private Double requestedAmount;

	private String statusToDisplayHistory;

	private BigInteger currentAmount;

	private List<String> listOfPrincipalReturned;

	private Integer loanId;

	private Boolean lenderPaticipateStatus;

	private String dealCreatedType;

	private String accountType;

	private String status;

	private String lastParticipationDate;

	private String firstParticipationDate;

	private String principalStatus;

	private BigInteger interestEarned;

	private String groupLink;

	private Date loanActiveDate;

	private LenderFurtherPaymentDetails lenderFurtherPaymentDetails;

	private List<Map<String, Integer>> listOfBorrowersMappedTodeal;

	private String withdrawStatus;

	private Double roiForWithdraw;

	private String messageSentToLenders;

	private Boolean borrowersMappedStatus = false;

	private BigInteger remaningingLimitToLender;

	private BigInteger lenderRemainingPanLimit;

	private BigInteger lenderTotalParticipationAmount;

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Double getPaticipatedAmount() {
		return paticipatedAmount;
	}

	public void setPaticipatedAmount(Double paticipatedAmount) {
		this.paticipatedAmount = paticipatedAmount;
	}

	public Double getProcessingFee() {
		return processingFee;
	}

	public void setProcessingFee(Double processingFee) {
		this.processingFee = processingFee;
	}

	public String getFeeStatus() {
		return feeStatus;
	}

	public void setFeeStatus(String feeStatus) {
		this.feeStatus = feeStatus;
	}

	public String getLederReturnType() {
		return lederReturnType;
	}

	public void setLederReturnType(String lederReturnType) {
		this.lederReturnType = lederReturnType;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public String getRegisteredDate() {
		return registeredDate;
	}

	public void setRegisteredDate(String registeredDate) {
		this.registeredDate = registeredDate;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getDealBorrowerName() {
		return dealBorrowerName;
	}

	public void setDealBorrowerName(String dealBorrowerName) {
		this.dealBorrowerName = dealBorrowerName;
	}

	public Double getDealAmount() {
		return dealAmount;
	}

	public void setDealAmount(Double dealAmount) {
		this.dealAmount = dealAmount;
	}

	public Double getDealRateofinterest() {
		return dealRateofinterest;
	}

	public void setDealRateofinterest(Double dealRateofinterest) {
		this.dealRateofinterest = dealRateofinterest;
	}

	public Integer getDealDuration() {
		return dealDuration;
	}

	public void setDealDuration(Integer dealDuration) {
		this.dealDuration = dealDuration;
	}

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public String getPaticipatedState() {
		return paticipatedState;
	}

	public void setPaticipatedState(String paticipatedState) {
		this.paticipatedState = paticipatedState;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getEditOptionConformation() {
		return editOptionConformation;
	}

	public void setEditOptionConformation(String editOptionConformation) {
		this.editOptionConformation = editOptionConformation;
	}

	public String getParticipationStatus() {
		return participationStatus;
	}

	public void setParticipationStatus(String participationStatus) {
		this.participationStatus = participationStatus;
	}

	public String getPricipalReturnedStatus() {
		return pricipalReturnedStatus;
	}

	public void setPricipalReturnedStatus(String pricipalReturnedStatus) {
		this.pricipalReturnedStatus = pricipalReturnedStatus;
	}

	public String getFirstInterestDate() {
		return firstInterestDate;
	}

	public void setFirstInterestDate(String firstInterestDate) {
		this.firstInterestDate = firstInterestDate;
	}

	public BigInteger getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(BigInteger currentValue) {
		this.currentValue = currentValue;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getLoanStatementValueCheck() {
		return loanStatementValueCheck;
	}

	public void setLoanStatementValueCheck(String loanStatementValueCheck) {
		this.loanStatementValueCheck = loanStatementValueCheck;
	}

	public String getPricipalReturned() {
		return pricipalReturned;
	}

	public void setPricipalReturned(String pricipalReturned) {
		this.pricipalReturned = pricipalReturned;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public Double getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(Double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}

	public String getStatusToDisplayHistory() {
		return statusToDisplayHistory;
	}

	public void setStatusToDisplayHistory(String statusToDisplayHistory) {
		this.statusToDisplayHistory = statusToDisplayHistory;
	}

	public BigInteger getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(BigInteger currentAmount) {
		this.currentAmount = currentAmount;
	}

	public List<String> getListOfPrincipalReturned() {
		return listOfPrincipalReturned;
	}

	public void setListOfPrincipalReturned(List<String> listOfPrincipalReturned) {
		this.listOfPrincipalReturned = listOfPrincipalReturned;
	}

	public Integer getLoanId() {
		return loanId;
	}

	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}

	public Boolean getLenderPaticipateStatus() {
		return lenderPaticipateStatus;
	}

	public void setLenderPaticipateStatus(Boolean lenderPaticipateStatus) {
		this.lenderPaticipateStatus = lenderPaticipateStatus;
	}

	public String getDealCreatedType() {
		return dealCreatedType;
	}

	public void setDealCreatedType(String dealCreatedType) {
		this.dealCreatedType = dealCreatedType;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLastParticipationDate() {
		return lastParticipationDate;
	}

	public void setLastParticipationDate(String lastParticipationDate) {
		this.lastParticipationDate = lastParticipationDate;
	}

	public String getFirstParticipationDate() {
		return firstParticipationDate;
	}

	public void setFirstParticipationDate(String firstParticipationDate) {
		this.firstParticipationDate = firstParticipationDate;
	}

	public String getPrincipalStatus() {
		return principalStatus;
	}

	public void setPrincipalStatus(String principalStatus) {
		this.principalStatus = principalStatus;
	}

	public BigInteger getInterestEarned() {
		return interestEarned;
	}

	public void setInterestEarned(BigInteger interestEarned) {
		this.interestEarned = interestEarned;
	}

	public String getGroupLink() {
		return groupLink;
	}

	public void setGroupLink(String groupLink) {
		this.groupLink = groupLink;
	}

	public LenderFurtherPaymentDetails getLenderFurtherPaymentDetails() {
		return lenderFurtherPaymentDetails;
	}

	public void setLenderFurtherPaymentDetails(LenderFurtherPaymentDetails lenderFurtherPaymentDetails) {
		this.lenderFurtherPaymentDetails = lenderFurtherPaymentDetails;
	}

	public List<Map<String, Integer>> getListOfBorrowersMappedTodeal() {
		return listOfBorrowersMappedTodeal;
	}

	public void setListOfBorrowersMappedTodeal(List<Map<String, Integer>> listOfBorrowersMappedTodeal) {
		this.listOfBorrowersMappedTodeal = listOfBorrowersMappedTodeal;
	}

	public String getWithdrawStatus() {
		return withdrawStatus;
	}

	public void setWithdrawStatus(String withdrawStatus) {
		this.withdrawStatus = withdrawStatus;
	}

	public Double getRoiForWithdraw() {
		return roiForWithdraw;
	}

	public void setRoiForWithdraw(Double roiForWithdraw) {
		this.roiForWithdraw = roiForWithdraw;
	}

	public String getMessageSentToLenders() {
		return messageSentToLenders;
	}

	public void setMessageSentToLenders(String messageSentToLenders) {
		this.messageSentToLenders = messageSentToLenders;
	}

	public Boolean getBorrowersMappedStatus() {
		return borrowersMappedStatus;
	}

	public void setBorrowersMappedStatus(Boolean borrowersMappedStatus) {
		this.borrowersMappedStatus = borrowersMappedStatus;
	}

	public BigInteger getRemaningingLimitToLender() {
		return remaningingLimitToLender;
	}

	public void setRemaningingLimitToLender(BigInteger remaningingLimitToLender) {
		this.remaningingLimitToLender = remaningingLimitToLender;
	}

	public Date getLoanActiveDate() {
		return loanActiveDate;
	}

	public void setLoanActiveDate(Date loanActiveDate) {
		this.loanActiveDate = loanActiveDate;
	}

	public BigInteger getLenderRemainingPanLimit() {
		return lenderRemainingPanLimit;
	}

	public void setLenderRemainingPanLimit(BigInteger lenderRemainingPanLimit) {
		this.lenderRemainingPanLimit = lenderRemainingPanLimit;
	}

	public BigInteger getLenderTotalParticipationAmount() {
		return lenderTotalParticipationAmount;
	}

	public void setLenderTotalParticipationAmount(BigInteger lenderTotalParticipationAmount) {
		this.lenderTotalParticipationAmount = lenderTotalParticipationAmount;
	}

	public String getParticipatedOn() {
		return participatedOn;
	}

	public void setParticipatedOn(String participatedOn) {
		this.participatedOn = participatedOn;
	}

}
