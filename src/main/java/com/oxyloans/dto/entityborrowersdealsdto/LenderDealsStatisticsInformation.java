package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;

public class LenderDealsStatisticsInformation {

	private BigInteger totalWalletCreditedAmount;

	private BigInteger totalWalletDebitedAmount;

	private Integer numberOfActiveDealsCount;

	private Integer numberOfClosedDealsCount;

	private Integer numberOfDisbursedDealsCount;

	private BigInteger activeDealsAmount;

	private BigInteger closedDealsAmount;

	private BigInteger disbursedDealsAmount;

	private Integer equityDealsParticipatedCount;

	private BigInteger equityDealsParticipatedAmount;

	private String oldValidityDate;

	private String validityDate;

	private Integer countOfPartiallyClosedDeals;

	private BigInteger sumOfPartiallyRunningAmount;

	private BigInteger sumOfPartiallyClosedAmount;

	private Integer id;

	private Integer totalCreatedDealsCount;

	public BigInteger getTotalWalletCreditedAmount() {
		return totalWalletCreditedAmount;
	}

	public void setTotalWalletCreditedAmount(BigInteger totalWalletCreditedAmount) {
		this.totalWalletCreditedAmount = totalWalletCreditedAmount;
	}

	public BigInteger getTotalWalletDebitedAmount() {
		return totalWalletDebitedAmount;
	}

	public void setTotalWalletDebitedAmount(BigInteger totalWalletDebitedAmount) {
		this.totalWalletDebitedAmount = totalWalletDebitedAmount;
	}

	public Integer getNumberOfActiveDealsCount() {
		return numberOfActiveDealsCount;
	}

	public void setNumberOfActiveDealsCount(Integer numberOfActiveDealsCount) {
		this.numberOfActiveDealsCount = numberOfActiveDealsCount;
	}

	public Integer getNumberOfClosedDealsCount() {
		return numberOfClosedDealsCount;
	}

	public void setNumberOfClosedDealsCount(Integer numberOfClosedDealsCount) {
		this.numberOfClosedDealsCount = numberOfClosedDealsCount;
	}

	public Integer getNumberOfDisbursedDealsCount() {
		return numberOfDisbursedDealsCount;
	}

	public void setNumberOfDisbursedDealsCount(Integer numberOfDisbursedDealsCount) {
		this.numberOfDisbursedDealsCount = numberOfDisbursedDealsCount;
	}

	public BigInteger getActiveDealsAmount() {
		return activeDealsAmount;
	}

	public void setActiveDealsAmount(BigInteger activeDealsAmount) {
		this.activeDealsAmount = activeDealsAmount;
	}

	public BigInteger getClosedDealsAmount() {
		return closedDealsAmount;
	}

	public void setClosedDealsAmount(BigInteger closedDealsAmount) {
		this.closedDealsAmount = closedDealsAmount;
	}

	public BigInteger getDisbursedDealsAmount() {
		return disbursedDealsAmount;
	}

	public void setDisbursedDealsAmount(BigInteger disbursedDealsAmount) {
		this.disbursedDealsAmount = disbursedDealsAmount;
	}

	public Integer getEquityDealsParticipatedCount() {
		return equityDealsParticipatedCount;
	}

	public void setEquityDealsParticipatedCount(Integer equityDealsParticipatedCount) {
		this.equityDealsParticipatedCount = equityDealsParticipatedCount;
	}

	public BigInteger getEquityDealsParticipatedAmount() {
		return equityDealsParticipatedAmount;
	}

	public void setEquityDealsParticipatedAmount(BigInteger equityDealsParticipatedAmount) {
		this.equityDealsParticipatedAmount = equityDealsParticipatedAmount;
	}

	public String getOldValidityDate() {
		return oldValidityDate;
	}

	public void setOldValidityDate(String oldValidityDate) {
		this.oldValidityDate = oldValidityDate;
	}

	public String getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}

	public Integer getCountOfPartiallyClosedDeals() {
		return countOfPartiallyClosedDeals;
	}

	public void setCountOfPartiallyClosedDeals(Integer countOfPartiallyClosedDeals) {
		this.countOfPartiallyClosedDeals = countOfPartiallyClosedDeals;
	}

	public BigInteger getSumOfPartiallyRunningAmount() {
		return sumOfPartiallyRunningAmount;
	}

	public void setSumOfPartiallyRunningAmount(BigInteger sumOfPartiallyRunningAmount) {
		this.sumOfPartiallyRunningAmount = sumOfPartiallyRunningAmount;
	}

	public BigInteger getSumOfPartiallyClosedAmount() {
		return sumOfPartiallyClosedAmount;
	}

	public void setSumOfPartiallyClosedAmount(BigInteger sumOfPartiallyClosedAmount) {
		this.sumOfPartiallyClosedAmount = sumOfPartiallyClosedAmount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTotalCreatedDealsCount() {
		return totalCreatedDealsCount;
	}

	public void setTotalCreatedDealsCount(Integer totalCreatedDealsCount) {
		this.totalCreatedDealsCount = totalCreatedDealsCount;
	}

}
