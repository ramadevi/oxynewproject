package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.Date;

public class UserPanNumberResponse {
	
    private String success;
    private Date orginalDob;
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public Date getOrginalDob() {
		return orginalDob;
	}
	public void setOrginalDob(Date orginalDob) {
		this.orginalDob = orginalDob;
	}
    

}
