package com.oxyloans.dto.entityborrowersdealsdto;

public class QuotedMsgDto {
	
  private String id;
	
	private String body;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
