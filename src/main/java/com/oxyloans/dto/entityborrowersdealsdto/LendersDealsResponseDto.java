package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;
import java.util.List;

public class LendersDealsResponseDto {

	private Integer totalCount;

	private String mobileNumber;

	private String email;

	private Integer userId;

	private String lenderName;

	private BigInteger totalRunningDealsAmount;

	private List<ListOfDealsInformationToLender> listOfDealsInformationToLender;

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public List<ListOfDealsInformationToLender> getListOfDealsInformationToLender() {
		return listOfDealsInformationToLender;
	}

	public void setListOfDealsInformationToLender(List<ListOfDealsInformationToLender> listOfDealsInformationToLender) {
		this.listOfDealsInformationToLender = listOfDealsInformationToLender;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public BigInteger getTotalRunningDealsAmount() {
		return totalRunningDealsAmount;
	}

	public void setTotalRunningDealsAmount(BigInteger totalRunningDealsAmount) {
		this.totalRunningDealsAmount = totalRunningDealsAmount;
	}

}
