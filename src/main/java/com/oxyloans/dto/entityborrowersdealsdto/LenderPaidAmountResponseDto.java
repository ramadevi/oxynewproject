package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.ArrayList;
import java.util.List;

public class LenderPaidAmountResponseDto {
	
	List<LenderPaidAmountResponse>excelResponse=new ArrayList<>();
	private String downloadUrl;
	public List<LenderPaidAmountResponse> getExcelResponse() {
		return excelResponse;
	}
	public void setExcelResponse(List<LenderPaidAmountResponse> excelResponse) {
		this.excelResponse = excelResponse;
	}
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

}
