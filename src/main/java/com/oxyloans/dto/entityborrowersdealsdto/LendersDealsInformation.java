package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;
import java.util.List;

public class LendersDealsInformation {

	private BigInteger totalDealsAmount;

	private Integer totalNumberOfDeals;

	private List<IndividualDealsInformation> individualDealsInformation;

	private BigInteger totalDealAmountByMonthAndYear;

	private String currentDate;

	private String givenMonthAndYear;

	public BigInteger getTotalDealsAmount() {
		return totalDealsAmount;
	}

	public void setTotalDealsAmount(BigInteger totalDealsAmount) {
		this.totalDealsAmount = totalDealsAmount;
	}

	public List<IndividualDealsInformation> getIndividualDealsInformation() {
		return individualDealsInformation;
	}

	public void setIndividualDealsInformation(List<IndividualDealsInformation> individualDealsInformation) {
		this.individualDealsInformation = individualDealsInformation;
	}

	public Integer getTotalNumberOfDeals() {
		return totalNumberOfDeals;
	}

	public void setTotalNumberOfDeals(Integer totalNumberOfDeals) {
		this.totalNumberOfDeals = totalNumberOfDeals;
	}

	public BigInteger getTotalDealAmountByMonthAndYear() {
		return totalDealAmountByMonthAndYear;
	}

	public void setTotalDealAmountByMonthAndYear(BigInteger totalDealAmountByMonthAndYear) {
		this.totalDealAmountByMonthAndYear = totalDealAmountByMonthAndYear;
	}

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public String getGivenMonthAndYear() {
		return givenMonthAndYear;
	}

	public void setGivenMonthAndYear(String givenMonthAndYear) {
		this.givenMonthAndYear = givenMonthAndYear;
	}

}
