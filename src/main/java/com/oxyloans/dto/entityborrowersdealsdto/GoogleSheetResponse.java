package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;
import java.util.Map;

public class GoogleSheetResponse {
	
	private String principalReceivedDate;
    private String lenderName;
    private Integer lenderId;
    private String group;
    private String typeOfTransaction;
    private String principal;
    private String principalReturned;
    private String balancePrincipal;
    private String Status;
    private String closedDate;
    private String assetSize;
    private String roi;
    private String prorataInterestDate;
    private String prorataDays;
    private String prorata;
    private String monthlyInterest;
    private String payoutType;
    private String bankName;
    private String bankNo;
    private String ifsCode;
    private String AcoountAdded;
    private String TobePaid;
    private String paid;
    private String  paidDate;
    private String oct2023;
    private String nov2023;
    private String dec2023;
    private String interestDate;
    private String password;
    
    private String monthName;
    private List<Map<String, String>> listOfData1;
    private List<Map<String, String>> listOfData2;
    private String standingInstructions;
    
    
    
	public List<Map<String, String>> getListOfData2() {
		return listOfData2;
	}
	public void setListOfData2(List<Map<String, String>> listOfData2) {
		this.listOfData2 = listOfData2;
	}
	public String getStandingInstructions() {
		return standingInstructions;
	}
	public void setStandingInstructions(String standingInstructions) {
		this.standingInstructions = standingInstructions;
	}
	public String getPrincipalReceivedDate() {
		return principalReceivedDate;
	}
	public void setPrincipalReceivedDate(String principalReceivedDate) {
		this.principalReceivedDate = principalReceivedDate;
	}
	public String getLenderName() {
		return lenderName;
	}
	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}
	public Integer getLenderId() {
		return lenderId;
	}
	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getTypeOfTransaction() {
		return typeOfTransaction;
	}
	public void setTypeOfTransaction(String typeOfTransaction) {
		this.typeOfTransaction = typeOfTransaction;
	}

	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getClosedDate() {
		return closedDate;
	}
	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}
	public String getAssetSize() {
		return assetSize;
	}
	public void setAssetSize(String assetSize) {
		this.assetSize = assetSize;
	}

	public String getProrataInterestDate() {
		return prorataInterestDate;
	}
	public void setProrataInterestDate(String prorataInterestDate) {
		this.prorataInterestDate = prorataInterestDate;
	}
	public String getProrataDays() {
		return prorataDays;
	}
	public void setProrataDays(String prorataDays) {
		this.prorataDays = prorataDays;
	}
	public String getProrata() {
		return prorata;
	}
	public void setProrata(String prorata) {
		this.prorata = prorata;
	}

	public String getPayoutType() {
		return payoutType;
	}
	public void setPayoutType(String payoutType) {
		this.payoutType = payoutType;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankNo() {
		return bankNo;
	}
	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}
	public String getIfsCode() {
		return ifsCode;
	}
	public void setIfsCode(String ifsCode) {
		this.ifsCode = ifsCode;
	}
	public String getAcoountAdded() {
		return AcoountAdded;
	}
	public void setAcoountAdded(String acoountAdded) {
		AcoountAdded = acoountAdded;
	}

	public String getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}
	public String getOct2023() {
		return oct2023;
	}
	public void setOct2023(String oct2023) {
		this.oct2023 = oct2023;
	}
	public String getNov2023() {
		return nov2023;
	}
	public void setNov2023(String nov2023) {
		this.nov2023 = nov2023;
	}
	public String getDec2023() {
		return dec2023;
	}
	public void setDec2023(String dec2023) {
		this.dec2023 = dec2023;
	}
	public String getPrincipal() {
		return principal;
	}
	public void setPrincipal(String principal) {
		this.principal = principal;
	}
	public String getPrincipalReturned() {
		return principalReturned;
	}
	public void setPrincipalReturned(String principalReturned) {
		this.principalReturned = principalReturned;
	}
	public String getBalancePrincipal() {
		return balancePrincipal;
	}
	public void setBalancePrincipal(String balancePrincipal) {
		this.balancePrincipal = balancePrincipal;
	}
	public String getMonthlyInterest() {
		return monthlyInterest;
	}
	public void setMonthlyInterest(String monthlyInterest) {
		this.monthlyInterest = monthlyInterest;
	}
	public String getTobePaid() {
		return TobePaid;
	}
	public void setTobePaid(String tobePaid) {
		TobePaid = tobePaid;
	}
	public String getPaid() {
		return paid;
	}
	public void setPaid(String paid) {
		this.paid = paid;
	}
	public String getRoi() {
		return roi;
	}
	public void setRoi(String roi) {
		this.roi = roi;
	}
	public String getInterestDate() {
		return interestDate;
	}
	public void setInterestDate(String interestDate) {
		this.interestDate = interestDate;
	}
	public String getMonthName() {
		return monthName;
	}
	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}
	public List<Map<String, String>> getListOfData1() {
		return listOfData1;
	}
	public void setListOfData1(List<Map<String, String>> listOfData1) {
		this.listOfData1 = listOfData1;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	    
    

	

}
