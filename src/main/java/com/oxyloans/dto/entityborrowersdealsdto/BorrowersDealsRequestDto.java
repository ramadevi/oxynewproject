package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

public class BorrowersDealsRequestDto {

	private Integer dealId;

	private String dealName;

	private String borrowerName;

	private Double dealAmount;

	private String fundsAcceptanceStartDate;

	private String fundsAcceptanceEndDate;

	private Double borrowerRateOfInterest;

	private String dealLink;

	private Double satishOxyFoundingMonthlyInterest;

	private Double satishOxyFoundingQuartelyInterest;

	private Double satishOxyFoundingHalfInterest;

	private Double satishOxyFoundingYearlyInterest;

	private Double satishOxyFoundingEndOfTheDealInterest;

	private Double oxyFoundingMonthlyInterest;

	private Double oxyFoundingQuartelyInterest;

	private Double oxyFoundingHalfInterest;

	private Double oxyFoundingYearlyInterest;

	private Double oxyFoundingEndOfTheDealInterest;

	private Double oxyPremiumMonthlyInterest;

	private Double oxyPremiumQuartelyInterest;

	private Double oxyPremiumHalfInterest;

	private Double oxyPremiumYearlyInterest;

	private Double oxyPremiumEndOfTheDealInterest;

	private Double newLendersMonthlyInterest;

	private Double newLendersQuartelyInterest;

	private Double newLendersHalfInterest;

	private Double newLendersYearlyInterest;

	private Double newLendersEndOfTheDealInterest;

	private Integer duration;

	private String whatappGroupNames;

	private String loanActiveDate;

	private String whatappMessageToLenders;

	private Double participationLimitToLenders;

	private String dealType;

	private Double feeCollectedFromBorrower;

	private Double feeROIforBorrower;

	private String withdrawalStatus;

	private Double withdrawalRoi;

	private String participcationLenderType;

	private Integer oxyLoanRequestId;

	private String borrowersIdsMappedToDeal;

	private Boolean enachStatus;

	private Double minimumPaticipationAmount;

	private String dealSubtype;

	private String dealFutureDate;

	private String dealLaunchHoure;

	private String dealOpenStatus;

	private List<DealLevelRequestDto> idsWithLoanAmount;

	private String whatsappChatId;

	private String whatsappResponseLink;

	private String feeStatusToParticipate;

	private Boolean lifeTimeWaiver;

	private Double lifeTimeWaiverLimit;

	public Double getFeeCollectedFromBorrower() {
		return feeCollectedFromBorrower;
	}

	public void setFeeCollectedFromBorrower(Double feeCollectedFromBorrower) {
		this.feeCollectedFromBorrower = feeCollectedFromBorrower;
	}

	public Double getFeeROIforBorrower() {
		return feeROIforBorrower;
	}

	public void setFeeROIforBorrower(Double feeROIforBorrower) {
		this.feeROIforBorrower = feeROIforBorrower;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public Double getDealAmount() {
		return dealAmount;
	}

	public void setDealAmount(Double dealAmount) {
		this.dealAmount = dealAmount;
	}

	public String getFundsAcceptanceStartDate() {
		return fundsAcceptanceStartDate;
	}

	public void setFundsAcceptanceStartDate(String fundsAcceptanceStartDate) {
		this.fundsAcceptanceStartDate = fundsAcceptanceStartDate;
	}

	public String getFundsAcceptanceEndDate() {
		return fundsAcceptanceEndDate;
	}

	public void setFundsAcceptanceEndDate(String fundsAcceptanceEndDate) {
		this.fundsAcceptanceEndDate = fundsAcceptanceEndDate;
	}

	public Double getBorrowerRateOfInterest() {
		return borrowerRateOfInterest;
	}

	public void setBorrowerRateOfInterest(Double borrowerRateOfInterest) {
		this.borrowerRateOfInterest = borrowerRateOfInterest;
	}

	public String getDealLink() {
		return dealLink;
	}

	public void setDealLink(String dealLink) {
		this.dealLink = dealLink;
	}

	public Double getSatishOxyFoundingMonthlyInterest() {
		return satishOxyFoundingMonthlyInterest;
	}

	public void setSatishOxyFoundingMonthlyInterest(Double satishOxyFoundingMonthlyInterest) {
		this.satishOxyFoundingMonthlyInterest = satishOxyFoundingMonthlyInterest;
	}

	public Double getSatishOxyFoundingQuartelyInterest() {
		return satishOxyFoundingQuartelyInterest;
	}

	public void setSatishOxyFoundingQuartelyInterest(Double satishOxyFoundingQuartelyInterest) {
		this.satishOxyFoundingQuartelyInterest = satishOxyFoundingQuartelyInterest;
	}

	public Double getSatishOxyFoundingHalfInterest() {
		return satishOxyFoundingHalfInterest;
	}

	public void setSatishOxyFoundingHalfInterest(Double satishOxyFoundingHalfInterest) {
		this.satishOxyFoundingHalfInterest = satishOxyFoundingHalfInterest;
	}

	public Double getSatishOxyFoundingYearlyInterest() {
		return satishOxyFoundingYearlyInterest;
	}

	public void setSatishOxyFoundingYearlyInterest(Double satishOxyFoundingYearlyInterest) {
		this.satishOxyFoundingYearlyInterest = satishOxyFoundingYearlyInterest;
	}

	public Double getSatishOxyFoundingEndOfTheDealInterest() {
		return satishOxyFoundingEndOfTheDealInterest;
	}

	public void setSatishOxyFoundingEndOfTheDealInterest(Double satishOxyFoundingEndOfTheDealInterest) {
		this.satishOxyFoundingEndOfTheDealInterest = satishOxyFoundingEndOfTheDealInterest;
	}

	public Double getOxyFoundingMonthlyInterest() {
		return oxyFoundingMonthlyInterest;
	}

	public void setOxyFoundingMonthlyInterest(Double oxyFoundingMonthlyInterest) {
		this.oxyFoundingMonthlyInterest = oxyFoundingMonthlyInterest;
	}

	public Double getOxyFoundingQuartelyInterest() {
		return oxyFoundingQuartelyInterest;
	}

	public void setOxyFoundingQuartelyInterest(Double oxyFoundingQuartelyInterest) {
		this.oxyFoundingQuartelyInterest = oxyFoundingQuartelyInterest;
	}

	public Double getOxyFoundingHalfInterest() {
		return oxyFoundingHalfInterest;
	}

	public void setOxyFoundingHalfInterest(Double oxyFoundingHalfInterest) {
		this.oxyFoundingHalfInterest = oxyFoundingHalfInterest;
	}

	public Double getOxyFoundingYearlyInterest() {
		return oxyFoundingYearlyInterest;
	}

	public void setOxyFoundingYearlyInterest(Double oxyFoundingYearlyInterest) {
		this.oxyFoundingYearlyInterest = oxyFoundingYearlyInterest;
	}

	public Double getOxyFoundingEndOfTheDealInterest() {
		return oxyFoundingEndOfTheDealInterest;
	}

	public void setOxyFoundingEndOfTheDealInterest(Double oxyFoundingEndOfTheDealInterest) {
		this.oxyFoundingEndOfTheDealInterest = oxyFoundingEndOfTheDealInterest;
	}

	public Double getOxyPremiumMonthlyInterest() {
		return oxyPremiumMonthlyInterest;
	}

	public void setOxyPremiumMonthlyInterest(Double oxyPremiumMonthlyInterest) {
		this.oxyPremiumMonthlyInterest = oxyPremiumMonthlyInterest;
	}

	public Double getOxyPremiumQuartelyInterest() {
		return oxyPremiumQuartelyInterest;
	}

	public void setOxyPremiumQuartelyInterest(Double oxyPremiumQuartelyInterest) {
		this.oxyPremiumQuartelyInterest = oxyPremiumQuartelyInterest;
	}

	public Double getOxyPremiumHalfInterest() {
		return oxyPremiumHalfInterest;
	}

	public void setOxyPremiumHalfInterest(Double oxyPremiumHalfInterest) {
		this.oxyPremiumHalfInterest = oxyPremiumHalfInterest;
	}

	public Double getOxyPremiumYearlyInterest() {
		return oxyPremiumYearlyInterest;
	}

	public void setOxyPremiumYearlyInterest(Double oxyPremiumYearlyInterest) {
		this.oxyPremiumYearlyInterest = oxyPremiumYearlyInterest;
	}

	public Double getOxyPremiumEndOfTheDealInterest() {
		return oxyPremiumEndOfTheDealInterest;
	}

	public void setOxyPremiumEndOfTheDealInterest(Double oxyPremiumEndOfTheDealInterest) {
		this.oxyPremiumEndOfTheDealInterest = oxyPremiumEndOfTheDealInterest;
	}

	public Double getNewLendersMonthlyInterest() {
		return newLendersMonthlyInterest;
	}

	public void setNewLendersMonthlyInterest(Double newLendersMonthlyInterest) {
		this.newLendersMonthlyInterest = newLendersMonthlyInterest;
	}

	public Double getNewLendersQuartelyInterest() {
		return newLendersQuartelyInterest;
	}

	public void setNewLendersQuartelyInterest(Double newLendersQuartelyInterest) {
		this.newLendersQuartelyInterest = newLendersQuartelyInterest;
	}

	public Double getNewLendersHalfInterest() {
		return newLendersHalfInterest;
	}

	public void setNewLendersHalfInterest(Double newLendersHalfInterest) {
		this.newLendersHalfInterest = newLendersHalfInterest;
	}

	public Double getNewLendersYearlyInterest() {
		return newLendersYearlyInterest;
	}

	public void setNewLendersYearlyInterest(Double newLendersYearlyInterest) {
		this.newLendersYearlyInterest = newLendersYearlyInterest;
	}

	public Double getNewLendersEndOfTheDealInterest() {
		return newLendersEndOfTheDealInterest;
	}

	public void setNewLendersEndOfTheDealInterest(Double newLendersEndOfTheDealInterest) {
		this.newLendersEndOfTheDealInterest = newLendersEndOfTheDealInterest;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getWhatappGroupNames() {
		return whatappGroupNames;
	}

	public void setWhatappGroupNames(String whatappGroupNames) {
		this.whatappGroupNames = whatappGroupNames;
	}

	public String getLoanActiveDate() {
		return loanActiveDate;
	}

	public void setLoanActiveDate(String loanActiveDate) {
		this.loanActiveDate = loanActiveDate;
	}

	public String getWhatappMessageToLenders() {
		return whatappMessageToLenders;
	}

	public void setWhatappMessageToLenders(String whatappMessageToLenders) {
		this.whatappMessageToLenders = whatappMessageToLenders;
	}

	public Double getParticipationLimitToLenders() {
		return participationLimitToLenders;
	}

	public void setParticipationLimitToLenders(Double participationLimitToLenders) {
		this.participationLimitToLenders = participationLimitToLenders;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getWithdrawalStatus() {
		return withdrawalStatus;
	}

	public void setWithdrawalStatus(String withdrawalStatus) {
		this.withdrawalStatus = withdrawalStatus;
	}

	public Double getWithdrawalRoi() {
		return withdrawalRoi;
	}

	public void setWithdrawalRoi(Double withdrawalRoi) {
		this.withdrawalRoi = withdrawalRoi;
	}

	public String getParticipcationLenderType() {
		return participcationLenderType;
	}

	public void setParticipcationLenderType(String participcationLenderType) {
		this.participcationLenderType = participcationLenderType;
	}

	public Integer getOxyLoanRequestId() {
		return oxyLoanRequestId;
	}

	public void setOxyLoanRequestId(Integer oxyLoanRequestId) {
		this.oxyLoanRequestId = oxyLoanRequestId;
	}

	public String getBorrowersIdsMappedToDeal() {
		return borrowersIdsMappedToDeal;
	}

	public Boolean getEnachStatus() {
		return enachStatus;
	}

	public void setBorrowersIdsMappedToDeal(String borrowersIdsMappedToDeal) {
		this.borrowersIdsMappedToDeal = borrowersIdsMappedToDeal;
	}

	public void setEnachStatus(Boolean enachStatus) {
		this.enachStatus = enachStatus;
	}

	public Double getMinimumPaticipationAmount() {
		return minimumPaticipationAmount;
	}

	public void setMinimumPaticipationAmount(Double minimumPaticipationAmount) {
		this.minimumPaticipationAmount = minimumPaticipationAmount;
	}

	public String getDealSubtype() {
		return dealSubtype;
	}

	public void setDealSubtype(String dealSubtype) {
		this.dealSubtype = dealSubtype;
	}

	public List<DealLevelRequestDto> getIdsWithLoanAmount() {
		return idsWithLoanAmount;
	}

	public void setIdsWithLoanAmount(List<DealLevelRequestDto> idsWithLoanAmount) {
		this.idsWithLoanAmount = idsWithLoanAmount;
	}

	public String getWhatsappChatId() {
		return whatsappChatId;
	}

	public void setWhatsappChatId(String whatsappChatId) {
		this.whatsappChatId = whatsappChatId;
	}

	public String getWhatsappResponseLink() {
		return whatsappResponseLink;
	}

	public void setWhatsappResponseLink(String whatsappResponseLink) {
		this.whatsappResponseLink = whatsappResponseLink;
	}

	public String getFeeStatusToParticipate() {
		return feeStatusToParticipate;
	}

	public void setFeeStatusToParticipate(String feeStatusToParticipate) {
		this.feeStatusToParticipate = feeStatusToParticipate;
	}

	public String getDealFutureDate() {
		return dealFutureDate;
	}

	public void setDealFutureDate(String dealFutureDate) {
		this.dealFutureDate = dealFutureDate;
	}

	public String getDealLaunchHoure() {
		return dealLaunchHoure;
	}

	public void setDealLaunchHoure(String dealLaunchHoure) {
		this.dealLaunchHoure = dealLaunchHoure;
	}

	public String getDealOpenStatus() {
		return dealOpenStatus;
	}

	public void setDealOpenStatus(String dealOpenStatus) {
		this.dealOpenStatus = dealOpenStatus;
	}

	public Boolean getLifeTimeWaiver() {
		return lifeTimeWaiver;
	}

	public void setLifeTimeWaiver(Boolean lifeTimeWaiver) {
		this.lifeTimeWaiver = lifeTimeWaiver;
	}

	public Double getLifeTimeWaiverLimit() {
		return lifeTimeWaiverLimit;
	}

	public void setLifeTimeWaiverLimit(Double lifeTimeWaiverLimit) {
		this.lifeTimeWaiverLimit = lifeTimeWaiverLimit;
	}
	

}