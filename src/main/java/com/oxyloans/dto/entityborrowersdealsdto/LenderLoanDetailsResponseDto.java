package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

public class LenderLoanDetailsResponseDto {

	private Integer sno;

	private String date;

	private Double interestAmount;

	private Double principalAmount;

	private String paymentStatus;

	private Double amountRecevied;

	private Integer lenderId;

	private String name;

	private Double rateOfInterest;

	private String lenderReturnType;

	private List<LenderParticipationUpdatedInfo> listOfPaticipatedInfo;

	private Double firstParticipationInterest;

	private Double firstParticipationAmount;

	private Integer differenceInDaysForFirstParticipation;

	private String interestReturnType;

	private String participatedDate;

	private Integer duration;

	private boolean automaticInterestCalculation;

	private String interestPaidDate;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Double getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(Double interestAmount) {
		this.interestAmount = interestAmount;
	}

	public Double getPrincipalAmount() {
		return principalAmount;
	}

	public void setPrincipalAmount(Double principalAmount) {
		this.principalAmount = principalAmount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Double getAmountRecevied() {
		return amountRecevied;
	}

	public void setAmountRecevied(Double amountRecevied) {
		this.amountRecevied = amountRecevied;
	}

	public Integer getSno() {
		return sno;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public String getLenderReturnType() {
		return lenderReturnType;
	}

	public void setLenderReturnType(String lenderReturnType) {
		this.lenderReturnType = lenderReturnType;
	}

	public List<LenderParticipationUpdatedInfo> getListOfPaticipatedInfo() {
		return listOfPaticipatedInfo;
	}

	public void setListOfPaticipatedInfo(List<LenderParticipationUpdatedInfo> listOfPaticipatedInfo) {
		this.listOfPaticipatedInfo = listOfPaticipatedInfo;
	}

	public Double getFirstParticipationInterest() {
		return firstParticipationInterest;
	}

	public void setFirstParticipationInterest(Double firstParticipationInterest) {
		this.firstParticipationInterest = firstParticipationInterest;
	}

	public Double getFirstParticipationAmount() {
		return firstParticipationAmount;
	}

	public void setFirstParticipationAmount(Double firstParticipationAmount) {
		this.firstParticipationAmount = firstParticipationAmount;
	}

	public Integer getDifferenceInDaysForFirstParticipation() {
		return differenceInDaysForFirstParticipation;
	}

	public void setDifferenceInDaysForFirstParticipation(Integer differenceInDaysForFirstParticipation) {
		this.differenceInDaysForFirstParticipation = differenceInDaysForFirstParticipation;
	}

	public String getInterestReturnType() {
		return interestReturnType;
	}

	public void setInterestReturnType(String interestReturnType) {
		this.interestReturnType = interestReturnType;
	}

	public String getParticipatedDate() {
		return participatedDate;
	}

	public void setParticipatedDate(String participatedDate) {
		this.participatedDate = participatedDate;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public boolean isAutomaticInterestCalculation() {
		return automaticInterestCalculation;
	}

	public void setAutomaticInterestCalculation(boolean automaticInterestCalculation) {
		this.automaticInterestCalculation = automaticInterestCalculation;
	}

	public String getInterestPaidDate() {
		return interestPaidDate;
	}

	public void setInterestPaidDate(String interestPaidDate) {
		this.interestPaidDate = interestPaidDate;
	}

}
