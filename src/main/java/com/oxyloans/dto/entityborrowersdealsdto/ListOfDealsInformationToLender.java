package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

public class ListOfDealsInformationToLender {

	private Integer groupId;

	private String groupName;

	private Integer dealId;

	private String dealName;

	private String borrowerName;

	private Double dealAmount;

	private Double rateOfInterest;

	private String fundsAcceptanceStartDate;

	private String fundsAcceptanceEndDate;

	private String dealLink;

	private String fundingStatus;

	private Integer duration;

	private Double lenderMonthlyRateOfInterest;

	private BigInteger remainingAmountToPaticipateInDeal;

	private Boolean lenderPaticipateStatus;

	private String dealCreatedType;

	private BigInteger totalPaticipatedAmount;

	private BigInteger minimumAmountInDeal;

	private BigInteger lenderPaticipationLimit;

	private String firstParticipationDate;

	private String lastParticipationDate;

	private List<Map<String, Integer>> listOfBorrowersMappedTodeal;

	private String withdrawStatus;

	private Double roiForWithdraw;

	private String messageSentToLenders;

	private Boolean borrowersMappedStatus = false;

	private String repaymentType;
	
	private String dealStatus;
	

	public String getDealStatus() {
		return dealStatus;
	}

	public void setDealStatus(String dealStatus) {
		this.dealStatus = dealStatus;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public Double getDealAmount() {
		return dealAmount;
	}

	public void setDealAmount(Double dealAmount) {
		this.dealAmount = dealAmount;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public String getFundsAcceptanceStartDate() {
		return fundsAcceptanceStartDate;
	}

	public void setFundsAcceptanceStartDate(String fundsAcceptanceStartDate) {
		this.fundsAcceptanceStartDate = fundsAcceptanceStartDate;
	}

	public String getFundsAcceptanceEndDate() {
		return fundsAcceptanceEndDate;
	}

	public void setFundsAcceptanceEndDate(String fundsAcceptanceEndDate) {
		this.fundsAcceptanceEndDate = fundsAcceptanceEndDate;
	}

	public String getDealLink() {
		return dealLink;
	}

	public void setDealLink(String dealLink) {
		this.dealLink = dealLink;
	}

	public String getFundingStatus() {
		return fundingStatus;
	}

	public void setFundingStatus(String fundingStatus) {
		this.fundingStatus = fundingStatus;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Double getLenderMonthlyRateOfInterest() {
		return lenderMonthlyRateOfInterest;
	}

	public void setLenderMonthlyRateOfInterest(Double lenderMonthlyRateOfInterest) {
		this.lenderMonthlyRateOfInterest = lenderMonthlyRateOfInterest;
	}

	public BigInteger getRemainingAmountToPaticipateInDeal() {
		return remainingAmountToPaticipateInDeal;
	}

	public void setRemainingAmountToPaticipateInDeal(BigInteger remainingAmountToPaticipateInDeal) {
		this.remainingAmountToPaticipateInDeal = remainingAmountToPaticipateInDeal;
	}

	public Boolean getLenderPaticipateStatus() {
		return lenderPaticipateStatus;
	}

	public void setLenderPaticipateStatus(Boolean lenderPaticipateStatus) {
		this.lenderPaticipateStatus = lenderPaticipateStatus;
	}

	public String getDealCreatedType() {
		return dealCreatedType;
	}

	public void setDealCreatedType(String dealCreatedType) {
		this.dealCreatedType = dealCreatedType;
	}

	public BigInteger getTotalPaticipatedAmount() {
		return totalPaticipatedAmount;
	}

	public void setTotalPaticipatedAmount(BigInteger totalPaticipatedAmount) {
		this.totalPaticipatedAmount = totalPaticipatedAmount;
	}

	public BigInteger getMinimumAmountInDeal() {
		return minimumAmountInDeal;
	}

	public void setMinimumAmountInDeal(BigInteger minimumAmountInDeal) {
		this.minimumAmountInDeal = minimumAmountInDeal;
	}

	public BigInteger getLenderPaticipationLimit() {
		return lenderPaticipationLimit;
	}

	public void setLenderPaticipationLimit(BigInteger lenderPaticipationLimit) {
		this.lenderPaticipationLimit = lenderPaticipationLimit;
	}

	public String getFirstParticipationDate() {
		return firstParticipationDate;
	}

	public void setFirstParticipationDate(String firstParticipationDate) {
		this.firstParticipationDate = firstParticipationDate;
	}

	public String getLastParticipationDate() {
		return lastParticipationDate;
	}

	public void setLastParticipationDate(String lastParticipationDate) {
		this.lastParticipationDate = lastParticipationDate;
	}

	public List<Map<String, Integer>> getListOfBorrowersMappedTodeal() {
		return listOfBorrowersMappedTodeal;
	}

	public void setListOfBorrowersMappedTodeal(List<Map<String, Integer>> listOfBorrowersMappedTodeal) {
		this.listOfBorrowersMappedTodeal = listOfBorrowersMappedTodeal;
	}

	public String getWithdrawStatus() {
		return withdrawStatus;
	}

	public void setWithdrawStatus(String withdrawStatus) {
		this.withdrawStatus = withdrawStatus;
	}

	public Double getRoiForWithdraw() {
		return roiForWithdraw;
	}

	public void setRoiForWithdraw(Double roiForWithdraw) {
		this.roiForWithdraw = roiForWithdraw;
	}

	public String getMessageSentToLenders() {
		return messageSentToLenders;
	}

	public void setMessageSentToLenders(String messageSentToLenders) {
		this.messageSentToLenders = messageSentToLenders;
	}

	public Boolean getBorrowersMappedStatus() {
		return borrowersMappedStatus;
	}

	public void setBorrowersMappedStatus(Boolean borrowersMappedStatus) {
		this.borrowersMappedStatus = borrowersMappedStatus;
	}

	public String getRepaymentType() {
		return repaymentType;
	}

	public void setRepaymentType(String repaymentType) {
		this.repaymentType = repaymentType;
	}

}
