package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;
import java.util.List;

public class LendersWalletResponseDto {
	private BigInteger sumOflendersCurrentWalletAmount;

	private BigInteger totalWalletAmountOnGivenDate;

	private List<LendersIndividualWalletResponseDto> lendersIndividualWalletResponseDto;

	public BigInteger getSumOflendersCurrentWalletAmount() {
		return sumOflendersCurrentWalletAmount;
	}

	public void setSumOflendersCurrentWalletAmount(BigInteger sumOflendersCurrentWalletAmount) {
		this.sumOflendersCurrentWalletAmount = sumOflendersCurrentWalletAmount;
	}

	public BigInteger getTotalWalletAmountOnGivenDate() {
		return totalWalletAmountOnGivenDate;
	}

	public void setTotalWalletAmountOnGivenDate(BigInteger totalWalletAmountOnGivenDate) {
		this.totalWalletAmountOnGivenDate = totalWalletAmountOnGivenDate;
	}

	public List<LendersIndividualWalletResponseDto> getLendersIndividualWalletResponseDto() {
		return lendersIndividualWalletResponseDto;
	}

	public void setLendersIndividualWalletResponseDto(
			List<LendersIndividualWalletResponseDto> lendersIndividualWalletResponseDto) {
		this.lendersIndividualWalletResponseDto = lendersIndividualWalletResponseDto;
	}

}
