package com.oxyloans.dto.entityborrowersdealsdto;

public class GoogleSheetClosedDealsResponse {
	
	private Integer closedCount;
	private Integer openCount;
	public Integer getClosedCount() {
		return closedCount;
	}
	public void setClosedCount(Integer closedCount) {
		this.closedCount = closedCount;
	}
	public Integer getOpenCount() {
		return openCount;
	}
	public void setOpenCount(Integer openCount) {
		this.openCount = openCount;
	}
	

}
