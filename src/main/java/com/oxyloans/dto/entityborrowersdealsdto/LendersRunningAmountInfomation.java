package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

public class LendersRunningAmountInfomation {

	private Integer totalLendersParticipatedCount;

	private List<LendersDealsResponseDto> lendersCurrentRunningDealsInfo;

	private String status;

	public Integer getTotalLendersParticipatedCount() {
		return totalLendersParticipatedCount;
	}

	public void setTotalLendersParticipatedCount(Integer totalLendersParticipatedCount) {
		this.totalLendersParticipatedCount = totalLendersParticipatedCount;
	}

	public List<LendersDealsResponseDto> getLendersCurrentRunningDealsInfo() {
		return lendersCurrentRunningDealsInfo;
	}

	public void setLendersCurrentRunningDealsInfo(List<LendersDealsResponseDto> lendersCurrentRunningDealsInfo) {
		this.lendersCurrentRunningDealsInfo = lendersCurrentRunningDealsInfo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
