package com.oxyloans.dto.entityborrowersdealsdto;

public class BorrowerAutoLendingResponse {

	
	private Integer openDeals;
	private Integer activeLenders;
	private String message;
	
	
	public Integer getOpenDeals() {
		return openDeals;
	}
	public void setOpenDeals(Integer openDeals) {
		this.openDeals = openDeals;
	}
	public Integer getActiveLenders() {
		return activeLenders;
	}
	public void setActiveLenders(Integer activeLenders) {
		this.activeLenders = activeLenders;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
