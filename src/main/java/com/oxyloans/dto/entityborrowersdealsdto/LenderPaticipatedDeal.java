package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;
import java.util.List;

public class LenderPaticipatedDeal {

	private Integer count;

	private BigInteger totalAmountPaticipatedByLenders;

	private List<LenderPaticipatedResponseDto> lenderPaticipatedResponseDto;

	private String participatedLendersExcel;

	private String lenderName;

	private String mobileNumber;
	
	private BigInteger totalPartiallyRunningAmount;

	private BigInteger totalPartiallyClosedAmount;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<LenderPaticipatedResponseDto> getLenderPaticipatedResponseDto() {
		return lenderPaticipatedResponseDto;
	}

	public void setLenderPaticipatedResponseDto(List<LenderPaticipatedResponseDto> lenderPaticipatedResponseDto) {
		this.lenderPaticipatedResponseDto = lenderPaticipatedResponseDto;
	}

	public BigInteger getTotalAmountPaticipatedByLenders() {
		return totalAmountPaticipatedByLenders;
	}

	public void setTotalAmountPaticipatedByLenders(BigInteger totalAmountPaticipatedByLenders) {
		this.totalAmountPaticipatedByLenders = totalAmountPaticipatedByLenders;
	}

	public String getParticipatedLendersExcel() {
		return participatedLendersExcel;
	}

	public void setParticipatedLendersExcel(String participatedLendersExcel) {
		this.participatedLendersExcel = participatedLendersExcel;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public BigInteger getTotalPartiallyRunningAmount() {
		return totalPartiallyRunningAmount;
	}

	public void setTotalPartiallyRunningAmount(BigInteger totalPartiallyRunningAmount) {
		this.totalPartiallyRunningAmount = totalPartiallyRunningAmount;
	}

	public BigInteger getTotalPartiallyClosedAmount() {
		return totalPartiallyClosedAmount;
	}

	public void setTotalPartiallyClosedAmount(BigInteger totalPartiallyClosedAmount) {
		this.totalPartiallyClosedAmount = totalPartiallyClosedAmount;
	}

}
