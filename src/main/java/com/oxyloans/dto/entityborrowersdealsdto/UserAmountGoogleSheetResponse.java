package com.oxyloans.dto.entityborrowersdealsdto;

public class UserAmountGoogleSheetResponse {
	
	private String totalInvestment;
	private String currentInvestment;
	private String closedInvestment;
	private String monthlyExpertReturns;
	public String getTotalInvestment() {
		return totalInvestment;
	}
	public void setTotalInvestment(String totalInvestment) {
		this.totalInvestment = totalInvestment;
	}
	public String getCurrentInvestment() {
		return currentInvestment;
	}
	public void setCurrentInvestment(String currentInvestment) {
		this.currentInvestment = currentInvestment;
	}
	public String getClosedInvestment() {
		return closedInvestment;
	}
	public void setClosedInvestment(String closedInvestment) {
		this.closedInvestment = closedInvestment;
	}
	public String getMonthlyExpertReturns() {
		return monthlyExpertReturns;
	}
	public void setMonthlyExpertReturns(String monthlyExpertReturns) {
		this.monthlyExpertReturns = monthlyExpertReturns;
	}
	

}
