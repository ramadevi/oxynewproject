package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

public class DealLevelLoanEmiCardInformation {
	
    private Double sumOfAmount;

	private Integer userId;

	private List<LenderLoanDetailsResponseDto> dealLevelLoanEmiCard;

	private List<LenderParticipationUpdatedInfo> participationUpdatedInfoList;

	private LenderLoanDetailsResponseDto singleLenderInterestDetails;

	private String firstInterestStartDate;

	private String interestStartDate;
	
	private int duration;

	private LenderParticipationUpdatedInfo lenderParticipationUpdatedInfo;

	public List<LenderLoanDetailsResponseDto> getDealLevelLoanEmiCard() {
		return dealLevelLoanEmiCard;
	}

	public void setDealLevelLoanEmiCard(List<LenderLoanDetailsResponseDto> dealLevelLoanEmiCard) {
		this.dealLevelLoanEmiCard = dealLevelLoanEmiCard;
	}

	public LenderLoanDetailsResponseDto getSingleLenderInterestDetails() {
		return singleLenderInterestDetails;
	}

	public void setSingleLenderInterestDetails(LenderLoanDetailsResponseDto singleLenderInterestDetails) {
		this.singleLenderInterestDetails = singleLenderInterestDetails;
	}

	public Double getSumOfAmount() {
		return sumOfAmount;
	}

	public void setSumOfAmount(Double sumOfAmount) {
		this.sumOfAmount = sumOfAmount;
	}

	public String getInterestStartDate() {
		return interestStartDate;
	}

	public void setInterestStartDate(String interestStartDate) {
		this.interestStartDate = interestStartDate;
	}

	public List<LenderParticipationUpdatedInfo> getParticipationUpdatedInfoList() {
		return participationUpdatedInfoList;
	}

	public void setParticipationUpdatedInfoList(List<LenderParticipationUpdatedInfo> participationUpdatedInfoList) {
		this.participationUpdatedInfoList = participationUpdatedInfoList;
	}

	public String getFirstInterestStartDate() {
		return firstInterestStartDate;
	}

	public void setFirstInterestStartDate(String firstInterestStartDate) {
		this.firstInterestStartDate = firstInterestStartDate;
	}

	public LenderParticipationUpdatedInfo getLenderParticipationUpdatedInfo() {
		return lenderParticipationUpdatedInfo;
	}

	public void setLenderParticipationUpdatedInfo(LenderParticipationUpdatedInfo lenderParticipationUpdatedInfo) {
		this.lenderParticipationUpdatedInfo = lenderParticipationUpdatedInfo;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

}
