package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;
import java.util.List;

import com.oxyloans.response.user.LenderCurrentWalletBalanceResponse;

public class LenderMappedToGroupIdResponseDto {

	private Integer count;

	private List<LenderDetailsByGroupId> listOfLendersDetailsByGroupId;

	private List<LenderCurrentWalletBalanceResponse> listOfLenderCurrentWalletBalanceResponse;

	private String lenderWalletsExcelLink;

	private BigInteger totalWalletAmount;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<LenderDetailsByGroupId> getListOfLendersDetailsByGroupId() {
		return listOfLendersDetailsByGroupId;
	}

	public void setListOfLendersDetailsByGroupId(List<LenderDetailsByGroupId> listOfLendersDetailsByGroupId) {
		this.listOfLendersDetailsByGroupId = listOfLendersDetailsByGroupId;
	}

	public List<LenderCurrentWalletBalanceResponse> getListOfLenderCurrentWalletBalanceResponse() {
		return listOfLenderCurrentWalletBalanceResponse;
	}

	public void setListOfLenderCurrentWalletBalanceResponse(
			List<LenderCurrentWalletBalanceResponse> listOfLenderCurrentWalletBalanceResponse) {
		this.listOfLenderCurrentWalletBalanceResponse = listOfLenderCurrentWalletBalanceResponse;
	}

	public String getLenderWalletsExcelLink() {
		return lenderWalletsExcelLink;
	}

	public void setLenderWalletsExcelLink(String lenderWalletsExcelLink) {
		this.lenderWalletsExcelLink = lenderWalletsExcelLink;
	}

	public BigInteger getTotalWalletAmount() {
		return totalWalletAmount;
	}

	public void setTotalWalletAmount(BigInteger totalWalletAmount) {
		this.totalWalletAmount = totalWalletAmount;
	}

}
