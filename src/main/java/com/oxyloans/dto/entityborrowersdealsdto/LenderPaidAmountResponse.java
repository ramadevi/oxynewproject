package com.oxyloans.dto.entityborrowersdealsdto;

public class LenderPaidAmountResponse {
	
	
	private String name;
	private Double amount;
    private String email;
    private String mobileNumber;
    private String pincode;
	private Integer lenderId;
	private String address;
	private String panNumber;
	
	private String paymentDate;
	private String paymentReceivedOn;
	
	
	
	
	
	
	
	
	
	
	
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getPaymentReceivedOn() {
		return paymentReceivedOn;
	}
	public void setPaymentReceivedOn(String paymentReceivedOn) {
		this.paymentReceivedOn = paymentReceivedOn;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public Integer getLenderId() {
		return lenderId;
	}
	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPanNumber() {
		return panNumber;
	}
	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
	

}
