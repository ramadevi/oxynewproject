package com.oxyloans.dto.entityborrowersdealsdto;

public class UserPanNumberRequest {

	private Integer userId;
	private String orginalDob;
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getOrginalDob() {
		return orginalDob;
	}
	public void setOrginalDob(String orginalDob) {
		this.orginalDob = orginalDob;
	}
	
}
