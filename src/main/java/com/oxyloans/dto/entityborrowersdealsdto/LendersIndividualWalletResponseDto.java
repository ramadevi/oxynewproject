package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;

public class LendersIndividualWalletResponseDto {

	private String lenderId;

	private String name;

	private BigInteger amount;

	public String getLenderId() {
		return lenderId;
	}

	public void setLenderId(String lenderId) {
		this.lenderId = lenderId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigInteger getAmount() {
		return amount;
	}

	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}

}
