package com.oxyloans.dto.entityborrowersdealsdto;



import java.util.List;

import com.oxyloans.borrower.noc.NewLendersExcelsheetResponseDto1;

public class NewLenderExcelSheetGenerateResponse {

	List<NewLendersExcelsheetResponseDto1> borrowersDealsResponseDto;

	private String fileStatus;
	private String emailStatus; 
	private String downloadUrl;

	public String getFileStatus() {
		return fileStatus;
	}

	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}

	public String getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public List<NewLendersExcelsheetResponseDto1> getBorrowersDealsResponseDto() {
		return borrowersDealsResponseDto;
	}

	public void setBorrowersDealsResponseDto(List<NewLendersExcelsheetResponseDto1> response) {
		this.borrowersDealsResponseDto = response;
	}

	

}
