package com.oxyloans.dto.entityborrowersdealsdto;

public class LoanResponseForDealsDto {

	private Integer loanRequestId;

	public Integer getLoanRequestId() {
		return loanRequestId;
	}

	public void setLoanRequestId(Integer loanRequestId) {
		this.loanRequestId = loanRequestId;
	}

}
