package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;

public class NotificationGrapicalImageResponse {
	
	private Integer lenders;
	private Integer borrowers;
	private Integer activeLenders;
	private Integer borrowersLoansCount;
	private BigInteger borrowersLoanAmount;
	private Integer monthlyPaidFeeLenders;
	private Integer quarterlyPaidFeeLenders;
	private Integer halfyearlyPaidFeeLenders;
	private Integer yearlyPaidFeeLenders;
	private Integer fiveYearsPaidFeeLenders;
	private Integer tenYearsPaidFeeLenders;
	private Integer lifeTimeFeeLenders;
	public Integer getLenders() {
		return lenders;
	}
	public void setLenders(Integer lenders) {
		this.lenders = lenders;
	}
	public Integer getBorrowers() {
		return borrowers;
	}
	public void setBorrowers(Integer borrowers) {
		this.borrowers = borrowers;
	}
	public Integer getActiveLenders() {
		return activeLenders;
	}
	public void setActiveLenders(Integer activeLenders) {
		this.activeLenders = activeLenders;
	}
	public Integer getBorrowersLoansCount() {
		return borrowersLoansCount;
	}
	public void setBorrowersLoansCount(Integer borrowersLoansCount) {
		this.borrowersLoansCount = borrowersLoansCount;
	}
	public BigInteger getBorrowersLoanAmount() {
		return borrowersLoanAmount;
	}
	public void setBorrowersLoanAmount(BigInteger borrowersLoanAmount) {
		this.borrowersLoanAmount = borrowersLoanAmount;
	}
	public Integer getMonthlyPaidFeeLenders() {
		return monthlyPaidFeeLenders;
	}
	public void setMonthlyPaidFeeLenders(Integer monthlyPaidFeeLenders) {
		this.monthlyPaidFeeLenders = monthlyPaidFeeLenders;
	}
	public Integer getQuarterlyPaidFeeLenders() {
		return quarterlyPaidFeeLenders;
	}
	public void setQuarterlyPaidFeeLenders(Integer quarterlyPaidFeeLenders) {
		this.quarterlyPaidFeeLenders = quarterlyPaidFeeLenders;
	}
	public Integer getHalfyearlyPaidFeeLenders() {
		return halfyearlyPaidFeeLenders;
	}
	public void setHalfyearlyPaidFeeLenders(Integer halfyearlyPaidFeeLenders) {
		this.halfyearlyPaidFeeLenders = halfyearlyPaidFeeLenders;
	}
	public Integer getYearlyPaidFeeLenders() {
		return yearlyPaidFeeLenders;
	}
	public void setYearlyPaidFeeLenders(Integer yearlyPaidFeeLenders) {
		this.yearlyPaidFeeLenders = yearlyPaidFeeLenders;
	}
	public Integer getFiveYearsPaidFeeLenders() {
		return fiveYearsPaidFeeLenders;
	}
	public void setFiveYearsPaidFeeLenders(Integer fiveYearsPaidFeeLenders) {
		this.fiveYearsPaidFeeLenders = fiveYearsPaidFeeLenders;
	}
	public Integer getTenYearsPaidFeeLenders() {
		return tenYearsPaidFeeLenders;
	}
	public void setTenYearsPaidFeeLenders(Integer tenYearsPaidFeeLenders) {
		this.tenYearsPaidFeeLenders = tenYearsPaidFeeLenders;
	}
	public Integer getLifeTimeFeeLenders() {
		return lifeTimeFeeLenders;
	}
	public void setLifeTimeFeeLenders(Integer lifeTimeFeeLenders) {
		this.lifeTimeFeeLenders = lifeTimeFeeLenders;
	}
	

}
