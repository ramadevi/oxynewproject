package com.oxyloans.dto.entityborrowersdealsdto;

public class BorrowerOfferAcceptedStatusResponse {

	private String loanOfferStatus;

	private double rateOfInterestToBorrower;

	private String repaymentMethodToBorrower;

	private double rateOfInterestToLender;

	private String repaymentMethodToLender;

	private Integer duration;

	private String durationType;

	private String borrowerName;

	public String getLoanOfferStatus() {
		return loanOfferStatus;
	}

	public void setLoanOfferStatus(String loanOfferStatus) {
		this.loanOfferStatus = loanOfferStatus;
	}

	public double getRateOfInterestToBorrower() {
		return rateOfInterestToBorrower;
	}

	public void setRateOfInterestToBorrower(double rateOfInterestToBorrower) {
		this.rateOfInterestToBorrower = rateOfInterestToBorrower;
	}

	public String getRepaymentMethodToBorrower() {
		return repaymentMethodToBorrower;
	}

	public void setRepaymentMethodToBorrower(String repaymentMethodToBorrower) {
		this.repaymentMethodToBorrower = repaymentMethodToBorrower;
	}

	public double getRateOfInterestToLender() {
		return rateOfInterestToLender;
	}

	public void setRateOfInterestToLender(double rateOfInterestToLender) {
		this.rateOfInterestToLender = rateOfInterestToLender;
	}

	public String getRepaymentMethodToLender() {
		return repaymentMethodToLender;
	}

	public void setRepaymentMethodToLender(String repaymentMethodToLender) {
		this.repaymentMethodToLender = repaymentMethodToLender;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getDurationType() {
		return durationType;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

}
