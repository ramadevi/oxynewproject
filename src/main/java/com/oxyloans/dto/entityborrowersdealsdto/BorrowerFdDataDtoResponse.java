package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

public class BorrowerFdDataDtoResponse {
	
	List<BorrowerFdDataResponse> listOfData;
	private String downloadUrl;
	public List<BorrowerFdDataResponse> getListOfData() {
		return listOfData;
	}
	public void setListOfData(List<BorrowerFdDataResponse> listOfData) {
		this.listOfData = listOfData;
	}
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

}
