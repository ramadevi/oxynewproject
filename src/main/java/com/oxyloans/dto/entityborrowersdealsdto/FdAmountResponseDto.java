package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.ArrayList;
import java.util.List;

public class FdAmountResponseDto {
	
	private String downloadUrl;
	List<FdAmountResponse>fdAmountResponse=new ArrayList<>();
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	public List<FdAmountResponse> getFdAmountResponse() {
		return fdAmountResponse;
	}
	public void setFdAmountResponse(List<FdAmountResponse> fdAmountResponse) {
		this.fdAmountResponse = fdAmountResponse;
	}


}
