package com.oxyloans.dto.entityborrowersdealsdto;

public class OxyLendersAcceptedDealsResponseDto {

	private String status;

	private Double processingFee;

	private Integer dealId;

	private Integer lenderId;
	private Integer participatedId;
	private String feeStatus;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getProcessingFee() {
		return processingFee;
	}

	public void setProcessingFee(Double processingFee) {
		this.processingFee = processingFee;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public Integer getParticipatedId() {
		return participatedId;
	}

	public void setParticipatedId(Integer participatedId) {
		this.participatedId = participatedId;
	}

	public String getFeeStatus() {
		return feeStatus;
	}

	public void setFeeStatus(String feeStatus) {
		this.feeStatus = feeStatus;
	}

}
