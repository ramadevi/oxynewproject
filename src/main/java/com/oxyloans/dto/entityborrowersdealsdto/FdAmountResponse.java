package com.oxyloans.dto.entityborrowersdealsdto;

public class FdAmountResponse {
	private Integer borrowerId;
	private String borrowerName;
	private Double amount;
	private String panNumber;
	private String lenderReturnType;
	private String address;
	public Integer getBorrowerId() {
		return borrowerId;
	}
	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}
	public String getBorrowerName() {
		return borrowerName;
	}
	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getPanNumber() {
		return panNumber;
	}
	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
	public String getLenderReturnType() {
		return lenderReturnType;
	}
	public void setLenderReturnType(String lenderReturnType) {
		this.lenderReturnType = lenderReturnType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	

}
