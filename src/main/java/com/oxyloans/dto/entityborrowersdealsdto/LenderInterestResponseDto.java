package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;

public class LenderInterestResponseDto {
	private Integer sno;

	private BigInteger amountReturned;

	private String returnedDate;

	private String status;

	private String amountType;

	private String remarks;

	private Integer dealId;

	private String repaymentType;

	private Double rateOfInterest;

	private String participatedDate;

	private String dealClosedDate;

	private String fundStartDate;
	
	private String dealClosedToLender;

	private BigInteger amount;
	
	

	public BigInteger getAmount() {
		return amount;
	}

	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}

	public Integer getSno() {
		return sno;
	}

	public BigInteger getAmountReturned() {
		return amountReturned;
	}

	public String getReturnedDate() {
		return returnedDate;
	}

	public String getStatus() {
		return status;
	}

	public String getAmountType() {
		return amountType;
	}

	public String getRemarks() {
		return remarks;
	}

	public Integer getDealId() {
		return dealId;
	}

	public String getRepaymentType() {
		return repaymentType;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public String getParticipatedDate() {
		return participatedDate;
	}

	public String getDealClosedDate() {
		return dealClosedDate;
	}

	public String getFundStartDate() {
		return fundStartDate;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

	public void setAmountReturned(BigInteger amountReturned) {
		this.amountReturned = amountReturned;
	}

	public void setReturnedDate(String returnedDate) {
		this.returnedDate = returnedDate;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public void setRepaymentType(String repaymentType) {
		this.repaymentType = repaymentType;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public void setParticipatedDate(String participatedDate) {
		this.participatedDate = participatedDate;
	}

	public void setDealClosedDate(String dealClosedDate) {
		this.dealClosedDate = dealClosedDate;
	}

	public void setFundStartDate(String fundStartDate) {
		this.fundStartDate = fundStartDate;
	}

	public String getDealClosedToLender() {
		return dealClosedToLender;
	}

	public void setDealClosedToLender(String dealClosedToLender) {
		this.dealClosedToLender = dealClosedToLender;
	}

}
