package com.oxyloans.dto.entityborrowersdealsdto;

public class UnmatchedInterestsDto {

	private int dealId;
	private int userId;
	private double amount;
	private String paymentDate;

	public UnmatchedInterestsDto(int dealId, int userId, double amount) {
		super();
		this.dealId = dealId;
		this.userId = userId;
		this.amount = amount;
	}

	public UnmatchedInterestsDto() {
		super();
	}

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

}
