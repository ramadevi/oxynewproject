package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.ArrayList;

public class ChatBotResponse {
	
	private String id;
	private String from;
	private String to;
	private String author;
	private String ack;
	private String body;
	private boolean fromMe;
	private String type;
	private long timestamp;
	private boolean isForwarded;
	private boolean isMentioned;
	private ArrayList<String> mentionedIds;
	private QuotedMsgDto quotedMsg;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getAck() {
		return ack;
	}
	public void setAck(String ack) {
		this.ack = ack;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public boolean isFromMe() {
		return fromMe;
	}
	public void setFromMe(boolean fromMe) {
		this.fromMe = fromMe;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public boolean isForwarded() {
		return isForwarded;
	}
	public void setForwarded(boolean isForwarded) {
		this.isForwarded = isForwarded;
	}
	public boolean isMentioned() {
		return isMentioned;
	}
	public void setMentioned(boolean isMentioned) {
		this.isMentioned = isMentioned;
	}
	public ArrayList<String> getMentionedIds() {
		return mentionedIds;
	}
	public void setMentionedIds(ArrayList<String> mentionedIds) {
		this.mentionedIds = mentionedIds;
	}
	public QuotedMsgDto getQuotedMsg() {
		return quotedMsg;
	}
	public void setQuotedMsg(QuotedMsgDto quotedMsg) {
		this.quotedMsg = quotedMsg;
	}

}
