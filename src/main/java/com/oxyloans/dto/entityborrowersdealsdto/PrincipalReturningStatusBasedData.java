package com.oxyloans.dto.entityborrowersdealsdto;

import java.math.BigInteger;

public class PrincipalReturningStatusBasedData {

	private String userId;

	private Double amountReturned;

	private String date;

	private BigInteger paticipatedAmount;

	private String lenderName;

	private Double roi;

	private String subStatus;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Double getAmountReturned() {
		return amountReturned;
	}

	public void setAmountReturned(Double amountReturned) {
		this.amountReturned = amountReturned;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public BigInteger getPaticipatedAmount() {
		return paticipatedAmount;
	}

	public void setPaticipatedAmount(BigInteger paticipatedAmount) {
		this.paticipatedAmount = paticipatedAmount;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public Double getRoi() {
		return roi;
	}

	public void setRoi(Double roi) {
		this.roi = roi;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public PrincipalReturningStatusBasedData(String userId, Double amountReturned, String date,
			BigInteger paticipatedAmount, String lenderName, Double roi, String subStatus) {
		super();
		this.userId = userId;
		this.amountReturned = amountReturned;
		this.date = date;
		this.paticipatedAmount = paticipatedAmount;
		this.lenderName = lenderName;
		this.roi = roi;
		this.subStatus = subStatus;
	}

}
