package com.oxyloans.dto.entityborrowersdealsdto;
import java.util.Date;

public class TenureExtendsResponse {
	
	private int newTenure;
	
	private int oldTenure;
	
	private String  newEmiEndDate;
	
	private Date oldEmiEndDate;
	
	private int dealId;

	public int getNewTenure() {
		return newTenure;
	}

	public void setNewTenure(int newTenure) {
		this.newTenure = newTenure;
	}

	public int getOldTenure() {
		return oldTenure;
	}

	public void setOldTenure(int oldTenure) {
		this.oldTenure = oldTenure;
	}

	

	public String getNewEmiEndDate() {
		return newEmiEndDate;
	}

	public void setNewEmiEndDate(String newEmiEndDate) {
		this.newEmiEndDate = newEmiEndDate;
	}

	public Date getOldEmiEndDate() {
		return oldEmiEndDate;
	}

	public void setOldEmiEndDate(Date oldEmiEndDate) {
		this.oldEmiEndDate = oldEmiEndDate;
	}

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}
	
	
	
	

}
