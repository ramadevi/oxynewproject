package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

public class LenderClosedDealsInformation {

	private List<LenderInterestResponseDto> lenderReturns;

	private String downloadStatement;

	private String dealFirstInterestDate;

	private String dealName;

	public List<LenderInterestResponseDto> getLenderReturns() {
		return lenderReturns;
	}

	public void setLenderReturns(List<LenderInterestResponseDto> lenderReturns) {
		this.lenderReturns = lenderReturns;
	}

	public String getDownloadStatement() {
		return downloadStatement;
	}

	public void setDownloadStatement(String downloadStatement) {
		this.downloadStatement = downloadStatement;
	}

	public String getDealFirstInterestDate() {
		return dealFirstInterestDate;
	}

	public void setDealFirstInterestDate(String dealFirstInterestDate) {
		this.dealFirstInterestDate = dealFirstInterestDate;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

}
