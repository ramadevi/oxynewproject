package com.oxyloans.dto.entityborrowersdealsdto;

public class DealInformationResponseDto {

	private Integer dealId;

	private String dealName;

	private Double dealAmount;

	private Double paticipatedAmount;

	private Double remainingAmount;

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public Double getDealAmount() {
		return dealAmount;
	}

	public void setDealAmount(Double dealAmount) {
		this.dealAmount = dealAmount;
	}

	public Double getPaticipatedAmount() {
		return paticipatedAmount;
	}

	public void setPaticipatedAmount(Double paticipatedAmount) {
		this.paticipatedAmount = paticipatedAmount;
	}

	public Double getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(Double remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

}
