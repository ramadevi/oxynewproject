package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

import com.oxyloans.response.user.DealsInfoDto;

public class BorrowersDealsList {

	private Integer count;

	private List<BorrowersDealsResponseDto> listOfBorrowersDealsResponseDto;

	private List<DealsInfoDto> dealsInfoDto;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<BorrowersDealsResponseDto> getListOfBorrowersDealsResponseDto() {
		return listOfBorrowersDealsResponseDto;
	}

	public void setListOfBorrowersDealsResponseDto(List<BorrowersDealsResponseDto> listOfBorrowersDealsResponseDto) {
		this.listOfBorrowersDealsResponseDto = listOfBorrowersDealsResponseDto;
	}

	public List<DealsInfoDto> getDealsInfoDto() {
		return dealsInfoDto;
	}

	public void setDealsInfoDto(List<DealsInfoDto> dealsInfoDto) {
		this.dealsInfoDto = dealsInfoDto;
	}

}
