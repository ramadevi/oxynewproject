package com.oxyloans.dto.entityborrowersdealsdto;

import java.util.List;

public class LenderTotalParticipationInterestDetails {

	private Double InterestAmount;

	private List<LenderParticipationUpdatedInfo> listOfParticipationUpdated;

	public Double getInterestAmount() {
		return InterestAmount;
	}

	public void setInterestAmount(Double interestAmount) {
		InterestAmount = interestAmount;
	}

	public List<LenderParticipationUpdatedInfo> getListOfParticipationUpdated() {
		return listOfParticipationUpdated;
	}

	public void setListOfParticipationUpdated(List<LenderParticipationUpdatedInfo> listOfParticipationUpdated) {
		this.listOfParticipationUpdated = listOfParticipationUpdated;
	}
}
