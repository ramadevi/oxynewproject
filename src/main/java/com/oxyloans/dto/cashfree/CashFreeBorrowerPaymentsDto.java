package com.oxyloans.dto.cashfree;

public class CashFreeBorrowerPaymentsDto {

	private int loanId;
	private int emiNumber;
	private Double partPayment;
	private int userId;

	public int getLoanId() {
		return loanId;
	}

	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}

	public int getEmiNumber() {
		return emiNumber;
	}

	public void setEmiNumber(int emiNumber) {
		this.emiNumber = emiNumber;
	}

	public Double getPartPayment() {
		return partPayment;
	}

	public void setPartPayment(Double partPayment) {
		this.partPayment = partPayment;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
