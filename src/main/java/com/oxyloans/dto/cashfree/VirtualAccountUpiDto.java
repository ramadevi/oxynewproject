package com.oxyloans.dto.cashfree;

public class VirtualAccountUpiDto {

	private String virtualVpaId;
	private String name;
	private String phone;
	private String email;

	public String getVirtualVpaId() {
		return virtualVpaId;
	}

	public void setVirtualVpaId(String virtualVpaId) {
		this.virtualVpaId = virtualVpaId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "VirtualAccountUpiDto [virtualVpaId=" + virtualVpaId + ", name=" + name + ", phone=" + phone + ", email="
				+ email + ", getVirtualVpaId()=" + getVirtualVpaId() + ", getName()=" + getName() + ", getPhone()="
				+ getPhone() + ", getEmail()=" + getEmail() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}

}
