package com.oxyloans.dto.cashfree;

public class CashFreeData {

	private String token;
	private String expiry;

	private String nameAtBank;
	private String amountDeposited;
	private String bankName;
	private String utr;
	private String city;
	private String branch;
	private String micr;
	private String accountExists;

	private String accountNumber;
	private String ifsc;

	private String vpa;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public String getNameAtBank() {
		return nameAtBank;
	}

	public void setNameAtBank(String nameAtBank) {
		this.nameAtBank = nameAtBank;
	}

	public String getAmountDeposited() {
		return amountDeposited;
	}

	public void setAmountDeposited(String amountDeposited) {
		this.amountDeposited = amountDeposited;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getUtr() {
		return utr;
	}

	public void setUtr(String utr) {
		this.utr = utr;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getMicr() {
		return micr;
	}

	public void setMicr(String micr) {
		this.micr = micr;
	}

	public String getAccountExists() {
		return accountExists;
	}

	public void setAccountExists(String accountExists) {
		this.accountExists = accountExists;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getVpa() {
		return vpa;
	}

	public void setVpa(String vpa) {
		this.vpa = vpa;
	}

}
