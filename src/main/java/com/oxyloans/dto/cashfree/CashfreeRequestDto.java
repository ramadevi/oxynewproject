package com.oxyloans.dto.cashfree;

public class CashfreeRequestDto {

	private Integer dealId;

	private String amount;

	private String lenderFeePayments;

	private String paymentType;

	private String paidFrom;

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getLenderFeePayments() {
		return lenderFeePayments;
	}

	public void setLenderFeePayments(String lenderFeePayments) {
		this.lenderFeePayments = lenderFeePayments;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaidFrom() {
		return paidFrom;
	}

	public void setPaidFrom(String paidFrom) {
		this.paidFrom = paidFrom;
	}

}
