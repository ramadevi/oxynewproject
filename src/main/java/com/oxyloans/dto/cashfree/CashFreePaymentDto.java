package com.oxyloans.dto.cashfree;

public class CashFreePaymentDto {

	private CashFreeUpiObjectDto upi;

	public CashFreeUpiObjectDto getUpi() {
		return upi;
	}

	public void setUpi(CashFreeUpiObjectDto upi) {
		this.upi = upi;
	}

	@Override
	public String toString() {
		return "CashFreePaymentMethodDto [upi=" + upi + "]";
	}

	
}
