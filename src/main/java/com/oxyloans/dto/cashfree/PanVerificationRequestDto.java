package com.oxyloans.dto.cashfree;

public class PanVerificationRequestDto {

	private String pan;
	private String type;
	private String reference_id;
	private String registered_name;
	private String father_name;
	private String valid;
	private String message;
	private String name_match_score;
	private String name_provided;
	private String name;
	private String aadhaarNumber;

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReference_id() {
		return reference_id;
	}

	public void setReference_id(String reference_id) {
		this.reference_id = reference_id;
	}

	public String getRegistered_name() {
		return registered_name;
	}

	public void setRegistered_name(String registered_name) {
		this.registered_name = registered_name;
	}

	public String getFather_name() {
		return father_name;
	}

	public void setFather_name(String father_name) {
		this.father_name = father_name;
	}

	public String getValid() {
		return valid;
	}

	public void setValid(String valid) {
		this.valid = valid;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getName_match_score() {
		return name_match_score;
	}

	public void setName_match_score(String name_match_score) {
		this.name_match_score = name_match_score;
	}

	public String getName_provided() {
		return name_provided;
	}

	public void setName_provided(String name_provided) {
		this.name_provided = name_provided;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAadhaarNumber() {
		return aadhaarNumber;
	}

	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}

}
