package com.oxyloans.dto.cashfree;

public class CashFreeOrderDetailsReponseDto {

	private long cf_order_id;
	private String order_id;
	private String entity;
	private String order_currency;
	private double order_amount;
	private String order_status;
	private String order_token;
	private String order_expiry_time;
	private String order_note;
	private String payment_link;
	private String channel;
	private String message;
	private String code;
	private String type;
	private String payment_session_id;
	private String created_at;

private CashFreeOrderMetaDto order_meta;

private CashFreeCustomerDetailObject customer_details;

	private CashFreeOrderPayResponseDtos qrResponse;

	public long getCf_order_id() {
		return cf_order_id;
	}

	public void setCf_order_id(long cf_order_id) {
		this.cf_order_id = cf_order_id;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getOrder_currency() {
		return order_currency;
	}

	public void setOrder_currency(String order_currency) {
		this.order_currency = order_currency;
	}

	public double getOrder_amount() {
		return order_amount;
	}

	public void setOrder_amount(double order_amount) {
		this.order_amount = order_amount;
	}

	public String getOrder_status() {
		return order_status;
	}

	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}

	public String getOrder_token() {
		return order_token;
	}

	public void setOrder_token(String order_token) {
		this.order_token = order_token;
	}

	public String getOrder_expiry_time() {
		return order_expiry_time;
	}

	public void setOrder_expiry_time(String order_expiry_time) {
		this.order_expiry_time = order_expiry_time;
	}

	public String getOrder_note() {
		return order_note;
	}

	public void setOrder_note(String order_note) {
		this.order_note = order_note;
	}

	public String getPayment_link() {
		return payment_link;
	}

	public void setPayment_link(String payment_link) {
		this.payment_link = payment_link;
	}


	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPayment_session_id() {
		return payment_session_id;
	}

	public void setPayment_session_id(String payment_session_id) {
		this.payment_session_id = payment_session_id;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public CashFreeOrderMetaDto getOrder_meta() {
		return order_meta;
	}

	public void setOrder_meta(CashFreeOrderMetaDto order_meta) {
		this.order_meta = order_meta;
	}

	public CashFreeCustomerDetailObject getCustomer_details() {
		return customer_details;
	}

	public void setCustomer_details(CashFreeCustomerDetailObject customer_details) {
		this.customer_details = customer_details;
	}

	public CashFreeOrderPayResponseDtos getQrResponse() {
		return qrResponse;
	}

	public void setQrResponse(CashFreeOrderPayResponseDtos qrResponse) {
		this.qrResponse = qrResponse;
	}

	
	
}
