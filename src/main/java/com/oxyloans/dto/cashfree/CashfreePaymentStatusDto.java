package com.oxyloans.dto.cashfree;

public class CashfreePaymentStatusDto {
	
	private String payment_status;
	
	private int payuId;
	
	private String txnId;

	public String getPayment_status() {
		return payment_status;
	}

	public void setPayment_status(String payment_status) {
		this.payment_status = payment_status;
	}

	public int getPayuId() {
		return payuId;
	}

	public void setPayuId(int payuId) {
		this.payuId = payuId;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	

}
