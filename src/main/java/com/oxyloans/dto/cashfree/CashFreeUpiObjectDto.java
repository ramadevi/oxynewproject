package com.oxyloans.dto.cashfree;

public class CashFreeUpiObjectDto {

	private String channel;
	private String upi_id;

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getUpi_id() {
		return upi_id;
	}

	public void setUpi_id(String upi_id) {
		this.upi_id = upi_id;
	}

	@Override
	public String toString() {
		return "CashFreeUpiObject [channel=" + channel + ", upi_id=" + upi_id + "]";
	}

	
}
