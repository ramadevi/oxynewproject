package com.oxyloans.dto.cashfree;

public class CashfreeVerificationRequestDto {

	private String bankAccount;
	private String ifscCode;

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

}
