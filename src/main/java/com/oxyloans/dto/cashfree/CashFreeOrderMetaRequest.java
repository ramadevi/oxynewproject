package com.oxyloans.dto.cashfree;

public class CashFreeOrderMetaRequest {
	
	private String return_url;
	private String notify_url;
	public String getReturn_url() {
		return return_url;
	}
	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}
	public String getNotify_url() {
		return notify_url;
	}
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}
	
	

}
