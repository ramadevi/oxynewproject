package com.oxyloans.dto.cashfree;

public class VirtualAccountRequestDto {

	private String vAccountId;
	private String name;
	private String phone;
	private String email;

	public String getvAccountId() {
		return vAccountId;
	}

	public void setvAccountId(String vAccountId) {
		this.vAccountId = vAccountId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "VirtualAccountRequestDto [vAccountId=" + vAccountId + ", name=" + name + ", phone=" + phone + ", email="
				+ email + ", getvAccountId()=" + getvAccountId() + ", getName()=" + getName() + ", getPhone()="
				+ getPhone() + ", getEmail()=" + getEmail() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}

}
