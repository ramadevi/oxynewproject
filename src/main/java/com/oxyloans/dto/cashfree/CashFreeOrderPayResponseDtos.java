package com.oxyloans.dto.cashfree;

public class CashFreeOrderPayResponseDtos {

	private long cf_payment_id;
	private String payment_method;
	private String channel;
	private String action;
	private CashFreeOrderDataDto data;
	private String message;
	private String code;
	private String type;

	public long getCf_payment_id() {
		return cf_payment_id;
	}

	public void setCf_payment_id(long cf_payment_id) {
		this.cf_payment_id = cf_payment_id;
	}

	public String getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public CashFreeOrderDataDto getData() {
		return data;
	}

	public void setData(CashFreeOrderDataDto data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
