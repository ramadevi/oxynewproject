package com.oxyloans.dto.cashfree;

public class CashfreeVerificationResponseDto {

	private String status;
	private String message;
	private String subCode;
	private String accountStatus;
	private String accountStatusCode;
	private CashFreeData data;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSubCode() {
		return subCode;
	}

	public void setSubCode(String subCode) {
		this.subCode = subCode;
	}

	public CashFreeData getData() {
		return data;
	}

	public void setData(CashFreeData data) {
		this.data = data;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getAccountStatusCode() {
		return accountStatusCode;
	}

	public void setAccountStatusCode(String accountStatusCode) {
		this.accountStatusCode = accountStatusCode;
	}

}
