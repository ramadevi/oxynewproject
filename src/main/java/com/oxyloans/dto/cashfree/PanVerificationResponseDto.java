package com.oxyloans.dto.cashfree;

public class PanVerificationResponseDto {

	private String pan;
	private String type;
	private String referenceId;
	private String registered_name;
	private String fatherName;
	private String valid;
	private String message;
	private String name_match_score;

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getRegistered_name() {
		return registered_name;
	}

	public void setRegistered_name(String registered_name) {
		this.registered_name = registered_name;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getValid() {
		return valid;
	}

	public void setValid(String valid) {
		this.valid = valid;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getName_match_score() {
		return name_match_score;
	}

	public void setName_match_score(String name_match_score) {
		this.name_match_score = name_match_score;
	}

}
