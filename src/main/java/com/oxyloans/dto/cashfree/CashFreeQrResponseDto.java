package com.oxyloans.dto.cashfree;

public class CashFreeQrResponseDto {

	private String status;
	private String subCode;
	private String message;
	private String qrCode;
	private String virtualVPA;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubCode() {
		return subCode;
	}

	public void setSubCode(String subCode) {
		this.subCode = subCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public String getVirtualVPA() {
		return virtualVPA;
	}

	public void setVirtualVPA(String virtualVPA) {
		this.virtualVPA = virtualVPA;
	}

}
