package com.oxyloans.dto.cashfree;

public class CashFreeOrdePayRequest {
	private String order_token;
	private CashFreePaymentDto payment_method;
	private String upi_id;
	private String channel;

	public String getOrder_token() {
		return order_token;
	}

	public void setOrder_token(String order_token) {
		this.order_token = order_token;
	}

	public CashFreePaymentDto getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(CashFreePaymentDto payment_method) {
		this.payment_method = payment_method;
	}
	

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	
	public String getUpi_id() {
		return upi_id;
	}

	public void setUpi_id(String upi_id) {
		this.upi_id = upi_id;
	}

	@Override
	public String toString() {
		return "CashFreeOrdePayRequestDto [order_token=" + order_token + ", payment_method=" + payment_method + "]";
	}

	
}
