package com.oxyloans.dto.cashfree;

public class CashFreeOrderCreationsDto {

	private String order_id;
	private String custom_id;
	private double order_amount;
	private String order_currency;
	private CashFreeCustomerDetailObject customer_details;
	private CashFreeOrderMetaRequest order_meta;

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public double getOrder_amount() {
		return order_amount;
	}

	public void setOrder_amount(double order_amount) {
		this.order_amount = order_amount;
	}

	public String getOrder_currency() {
		return order_currency;
	}

	public void setOrder_currency(String order_currency) {
		this.order_currency = order_currency;
	}

	public CashFreeCustomerDetailObject getCustomer_details() {
		return customer_details;
	}

	public void setCustomer_details(CashFreeCustomerDetailObject customer_details) {
		this.customer_details = customer_details;
	}

	public CashFreeOrderMetaRequest getOrder_meta() {
		return order_meta;
	}

	public void setOrder_meta(CashFreeOrderMetaRequest order_meta) {
		this.order_meta = order_meta;
	}

	public String getCustom_id() {
		return custom_id;
	}

	public void setCustom_id(String custom_id) {
		this.custom_id = custom_id;
	}

}
