package com.oxyloans.dto.cashfree;

public class CashFreePayLoadRequest {

	private String qrcode;
	private String bhim;
	private String phonepe;
	private String paytm;
	private String gpay;

	public String getQrcode() {
		return qrcode;
	}

	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}

	public String getBhim() {
		return bhim;
	}

	public void setBhim(String bhim) {
		this.bhim = bhim;
	}

	public String getPhonepe() {
		return phonepe;
	}

	public void setPhonepe(String phonepe) {
		this.phonepe = phonepe;
	}

	public String getPaytm() {
		return paytm;
	}

	public void setPaytm(String paytm) {
		this.paytm = paytm;
	}

	public String getGpay() {
		return gpay;
	}

	public void setGpay(String gpay) {
		this.gpay = gpay;
	}

}
