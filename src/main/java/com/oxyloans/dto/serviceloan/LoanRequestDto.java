package com.oxyloans.dto.serviceloan;

import java.math.BigInteger;

public class LoanRequestDto {

	private Double loanRequestAmount;

	private Double rateOfInterest;

	private String expectedDate;

	private Integer duration;

	private String repaymentMethod;

	private String loanPurpose;

	private Integer parentRequestId;

	private String comments;

	private double feePaid;

	private Integer id;

	private String status;

	private double outStandingAmount;

	private String LoanRequestedId;

	private Double loanOfferedAmount;

	private Double fee;

	private String loanProcessType;

	private String durationType;

	private Double salaryForUser;

	private Double rateOfInterestToBorrower;

	private Double rateOfInterestToLender;

	private String repaymentTypeToBorrower;

	private String repaymentTypeToLender;

	private boolean partnerEsign;

	private boolean partnerEnach;

	private boolean offerAcceptedStatus;

	private boolean walletCalculationToLender = false;

	private BigInteger walletAmountToLender;

	public String getLoanProcessType() {
		return loanProcessType;
	}

	public void setLoanProcessType(String loanProcessType) {
		this.loanProcessType = loanProcessType;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public Double getLoanOfferedAmount() {
		return loanOfferedAmount;
	}

	public void setLoanOfferedAmount(Double loanOfferedAmount) {
		this.loanOfferedAmount = loanOfferedAmount;
	}

	public String getLoanRequestedId() {
		return LoanRequestedId;
	}

	public void setLoanRequestedId(String loanRequestedId) {
		LoanRequestedId = loanRequestedId;
	}

	public double getOutStandingAmount() {
		return outStandingAmount;
	}

	public void setOutStandingAmount(double outStandingAmount) {
		this.outStandingAmount = outStandingAmount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getLoanRequestAmount() {
		return loanRequestAmount;
	}

	public void setLoanRequestAmount(Double loanRequestAmount) {
		this.loanRequestAmount = loanRequestAmount;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public String getExpectedDate() {
		return expectedDate;
	}

	public void setExpectedDate(String expectedDate) {
		this.expectedDate = expectedDate;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getRepaymentMethod() {
		return repaymentMethod;
	}

	public void setRepaymentMethod(String repaymentMethod) {
		this.repaymentMethod = repaymentMethod;
	}

	public String getLoanPurpose() {
		return loanPurpose;
	}

	public void setLoanPurpose(String loanPurpose) {
		this.loanPurpose = loanPurpose;
	}

	public Integer getParentRequestId() {
		return parentRequestId;
	}

	public void setParentRequestId(Integer parentRequestId) {
		this.parentRequestId = parentRequestId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public double getFeePaid() {
		return feePaid;
	}

	public void setFeePaid(double feePaid) {
		this.feePaid = feePaid;
	}

	public String getDurationType() {
		return durationType;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public Double getSalaryForUser() {
		return salaryForUser;
	}

	public void setSalaryForUser(Double salaryForUser) {
		this.salaryForUser = salaryForUser;
	}

	public Double getRateOfInterestToBorrower() {
		return rateOfInterestToBorrower;
	}

	public void setRateOfInterestToBorrower(Double rateOfInterestToBorrower) {
		this.rateOfInterestToBorrower = rateOfInterestToBorrower;
	}

	public Double getRateOfInterestToLender() {
		return rateOfInterestToLender;
	}

	public void setRateOfInterestToLender(Double rateOfInterestToLender) {
		this.rateOfInterestToLender = rateOfInterestToLender;
	}

	public String getRepaymentTypeToBorrower() {
		return repaymentTypeToBorrower;
	}

	public void setRepaymentTypeToBorrower(String repaymentTypeToBorrower) {
		this.repaymentTypeToBorrower = repaymentTypeToBorrower;
	}

	public String getRepaymentTypeToLender() {
		return repaymentTypeToLender;
	}

	public void setRepaymentTypeToLender(String repaymentTypeToLender) {
		this.repaymentTypeToLender = repaymentTypeToLender;
	}

	public boolean isPartnerEsign() {
		return partnerEsign;
	}

	public void setPartnerEsign(boolean partnerEsign) {
		this.partnerEsign = partnerEsign;
	}

	public boolean isPartnerEnach() {
		return partnerEnach;
	}

	public void setPartnerEnach(boolean partnerEnach) {
		this.partnerEnach = partnerEnach;
	}

	public boolean isOfferAcceptedStatus() {
		return offerAcceptedStatus;
	}

	public void setOfferAcceptedStatus(boolean offerAcceptedStatus) {
		this.offerAcceptedStatus = offerAcceptedStatus;
	}

	public boolean isWalletCalculationToLender() {
		return walletCalculationToLender;
	}

	public void setWalletCalculationToLender(boolean walletCalculationToLender) {
		this.walletCalculationToLender = walletCalculationToLender;
	}

	public BigInteger getWalletAmountToLender() {
		return walletAmountToLender;
	}

	public void setWalletAmountToLender(BigInteger walletAmountToLender) {
		this.walletAmountToLender = walletAmountToLender;
	}

}
