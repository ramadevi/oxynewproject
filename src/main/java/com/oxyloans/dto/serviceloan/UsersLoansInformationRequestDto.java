package com.oxyloans.dto.serviceloan;

import com.oxyloans.request.PageDto;

public class UsersLoansInformationRequestDto {
	private String primaryType;
	private PageDto page;

	public String getPrimaryType() {
		return primaryType;
	}

	public void setPrimaryType(String primaryType) {
		this.primaryType = primaryType;
	}

	public PageDto getPage() {
		return page;
	}

	public void setPage(PageDto page) {
		this.page = page;
	}

}
