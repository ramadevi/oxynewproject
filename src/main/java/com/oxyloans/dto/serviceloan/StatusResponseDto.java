package com.oxyloans.dto.serviceloan;

public class StatusResponseDto {

	private String disbursementStatus;

	private String eSignStatus;

	private String eNachStatus;

	private Integer numberOfLoans;

	private Integer numberOfEsignsCompleted;

	private Integer numberOfLoansDisbursed;

	private Integer numberOfENachCompleted;

	public String getDisbursementStatus() {
		return disbursementStatus;
	}

	public void setDisbursementStatus(String disbursementStatus) {
		this.disbursementStatus = disbursementStatus;
	}

	public String geteSignStatus() {
		return eSignStatus;
	}

	public void seteSignStatus(String eSignStatus) {
		this.eSignStatus = eSignStatus;
	}

	public String geteNachStatus() {
		return eNachStatus;
	}

	public void seteNachStatus(String eNachStatus) {
		this.eNachStatus = eNachStatus;
	}

	public Integer getNumberOfLoans() {
		return numberOfLoans;
	}

	public void setNumberOfLoans(Integer numberOfLoans) {
		this.numberOfLoans = numberOfLoans;
	}

	public Integer getNumberOfEsignsCompleted() {
		return numberOfEsignsCompleted;
	}

	public void setNumberOfEsignsCompleted(Integer numberOfEsignsCompleted) {
		this.numberOfEsignsCompleted = numberOfEsignsCompleted;
	}

	public Integer getNumberOfLoansDisbursed() {
		return numberOfLoansDisbursed;
	}

	public void setNumberOfLoansDisbursed(Integer numberOfLoansDisbursed) {
		this.numberOfLoansDisbursed = numberOfLoansDisbursed;
	}

	public Integer getNumberOfENachCompleted() {
		return numberOfENachCompleted;
	}

	public void setNumberOfENachCompleted(Integer numberOfENachCompleted) {
		this.numberOfENachCompleted = numberOfENachCompleted;
	}

}
