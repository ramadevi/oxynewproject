package com.oxyloans.dto.serviceloan;

import java.util.Date;

public class StatementPdfRequestDto {

	private Integer loanId;

	private Date disbursmentDate;

	private double disbursmentAmount;

	private String accountNumber;

	private Integer rateOfInterest;

	private Integer installmentPaidNumber;

	private double installmentPaidAmount;

	private Integer installmentPendingNumber;

	private double installmentPendingAmount;

	private Integer futureInstallmentNumber;

	private Integer futureInstallmentAmount;

	public Integer getLoanId() {
		return loanId;
	}

	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}

	public Date getDisbursmentDate() {
		return disbursmentDate;
	}

	public void setDisbursmentDate(Date disbursmentDate) {
		this.disbursmentDate = disbursmentDate;
	}

	public double getDisbursmentAmount() {
		return disbursmentAmount;
	}

	public void setDisbursmentAmount(double disbursmentAmount) {
		this.disbursmentAmount = disbursmentAmount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Integer getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Integer rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public Integer getInstallmentPaidNumber() {
		return installmentPaidNumber;
	}

	public void setInstallmentPaidNumber(Integer installmentPaidNumber) {
		this.installmentPaidNumber = installmentPaidNumber;
	}

	public double getInstallmentPaidAmount() {
		return installmentPaidAmount;
	}

	public void setInstallmentPaidAmount(double installmentPaidAmount) {
		this.installmentPaidAmount = installmentPaidAmount;
	}

	public Integer getInstallmentPendingNumber() {
		return installmentPendingNumber;
	}

	public void setInstallmentPendingNumber(Integer installmentPendingNumber) {
		this.installmentPendingNumber = installmentPendingNumber;
	}

	public double getInstallmentPendingAmount() {
		return installmentPendingAmount;
	}

	public void setInstallmentPendingAmount(double installmentPendingAmount) {
		this.installmentPendingAmount = installmentPendingAmount;
	}

	public Integer getFutureInstallmentNumber() {
		return futureInstallmentNumber;
	}

	public void setFutureInstallmentNumber(Integer futureInstallmentNumber) {
		this.futureInstallmentNumber = futureInstallmentNumber;
	}

	public Integer getFutureInstallmentAmount() {
		return futureInstallmentAmount;
	}

	public void setFutureInstallmentAmount(Integer futureInstallmentAmount) {
		this.futureInstallmentAmount = futureInstallmentAmount;
	}

}
