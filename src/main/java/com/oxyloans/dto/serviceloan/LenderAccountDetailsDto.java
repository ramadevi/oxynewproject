package com.oxyloans.dto.serviceloan;

public class LenderAccountDetailsDto extends OxyAccountDetailsDto {
	
	private Double availableForInvestment = 0.0d;

	public Double getAvailableForInvestment() {
		return availableForInvestment;
	}

	public void setAvailableForInvestment(Double availableForInvestment) {
		this.availableForInvestment = availableForInvestment;
	}
	
}
