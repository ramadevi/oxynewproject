package com.oxyloans.dto.serviceloan;

public class NotificationCountResponseDtoForBorrowerAndLender {
	private int totalCount;
	private int loanResponsePendingCount;
	private int esignPendingCount;
	private int disbursedmentPendingCount;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getLoanResponsePendingCount() {
		return loanResponsePendingCount;
	}

	public void setLoanResponsePendingCount(int loanResponsePendingCount) {
		this.loanResponsePendingCount = loanResponsePendingCount;
	}

	public int getEsignPendingCount() {
		return esignPendingCount;
	}

	public void setEsignPendingCount(int esignPendingCount) {
		this.esignPendingCount = esignPendingCount;
	}

	public int getDisbursedmentPendingCount() {
		return disbursedmentPendingCount;
	}

	public void setDisbursedmentPendingCount(int disbursedmentPendingCount) {
		this.disbursedmentPendingCount = disbursedmentPendingCount;
	}

}
