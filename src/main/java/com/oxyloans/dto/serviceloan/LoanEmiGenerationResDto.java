package com.oxyloans.dto.serviceloan;

public class LoanEmiGenerationResDto {

	private String returnMessage;

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

}
