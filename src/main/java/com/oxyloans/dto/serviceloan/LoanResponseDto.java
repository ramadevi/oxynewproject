package com.oxyloans.dto.serviceloan;

import java.util.List;

import com.oxyloans.request.user.KycFileResponse;
import com.oxyloans.response.user.BorrowerDetailsRequestDto;
import com.oxyloans.response.user.CommentsRequestDto;
import com.oxyloans.response.user.EnachMandateResponseForEmi;
import com.oxyloans.response.user.EnachMandateResponseForPrincipal;
import com.oxyloans.response.user.LenderFavouriteResponseDto;
import com.oxyloans.response.user.UserResponse;

public class LoanResponseDto extends LoanRequestDto {

	// private int id;

	private int userId;

	private int loanRequestId;

	private String loanRequest;

	private String loanRequestedDate;

	private UserResponse user;

	private OfferSentDetails offerSentDetails;

	private String userDisplayId;

	private Double loanDisbursedAmount;

	private String loanStatus;

	private String loanId;

	private String lenderDisplayId;

	private String borrowerDisplayId;

	private UserResponse lenderUser;

	private UserResponse borrowerUser;

	private double profit;

	private boolean alreadyOffered;

	private boolean alreadyActive;

	private int loanDisbursedPercentage;

	private Boolean borrowerFeePaid;

	private Boolean lenderFeePaid;

	private String agreementUrl;

	private int noOfEmisPaid;

	private Double emiAmount;

	private Double dueEmiAmount;

	private Integer lenderEsignId;

	private Integer borrowerEsignId;

	private String emiStartDate;

	private Integer noRemaingEmis;

	private Double principalAmountPending;

	private Double totalAmountPaid;

	private Double totalAmountPending;

	private String borrowerDisbursementDate;

	private String loanAdminComments;

	private Integer lenderUserId;

	private Integer borrowerUserId;

	private List<KycFileResponse> lenderKycDocuments;

	private List<KycFileResponse> borrowerKycDocuments;

	private Boolean isECSActivated;

	private int enachMandateId;

	private String companyName;

	private String workExperience;

	private Integer salary;

	private List<LenderFavouriteResponseDto> favouriteBorrowers;

	private String downloadUrl;

	private String fileName;

	private boolean riskStatus;

	private boolean sendofferstatus;

	private double acceptedAmount;

	private Integer durationGivenByAdmin;

	private double rateOfInterestGivenByAdmin;

	private boolean beforeEnach = false;

	private boolean enachMandateResponce = false;

	private boolean aftreEnach = false;

	private String adminComments;

	private String loanEmiCardStatus;

	private String enachType;

	private String oxyLoanAdminComments;

	private String applicationNumber;

	private String city;

	private String email;

	private String mobileNumber;

	private String borrowerName;

	private double disburmentAmount;

	private int emiNumber;

	private String emiDueOnDate;

	private double penality;

	private String loanProcessType;

	private List<LoanEmiCardResponseDto> emiDto;

	private List<LoanEmiCardPaymentDetailsResponseDto> loanEmiCardPaymentDetailsResponseDtos;

	private double totalWalletAmount;

	private double inProcessAmountWallet;

	private double walletAmount;

	private double interestPendingTillDate;

	private BorrowerDetailsRequestDto BorrowerCommentsDetails;

	private String commentsByRadhaSir;

	private Integer ratingByRadhaSir;

	private Integer lendersRatingValue;

	private CommentsRequestDto commentsRequestDto;

	private boolean enachInitiated;

	private boolean enachSuccess;

	private EnachMandateResponseForEmi enachMandateResponseForEmi;
	private EnachMandateResponseForPrincipal enachMandateResponseForPrincipal;

	private Double sumOfDealAmount;

	private Double equityValue;

	private Double holdAmount;

	private Double dealValueWithAgreements;

	private Integer groupId;

	private String groupName;

	private String referredBy;

	private String driveLink;

	private String cifNumber;

	private String finoEmployeeMobileNumber;

	private Double rateOfInterestToBorrower;

	private Double rateOfInterestToLender;

	private String repaymentTypeToBorrower;

	private String repaymentTypeToLender;

	private boolean partnerEsign;

	private boolean partnerEnach;

	private boolean offerAcceptedStatus;

	public Double getSumOfDealAmount() {
		return sumOfDealAmount;
	}

	public void setSumOfDealAmount(Double sumOfDealAmount) {
		this.sumOfDealAmount = sumOfDealAmount;
	}

	public Double getEquityValue() {
		return equityValue;
	}

	public void setEquityValue(Double equityValue) {
		this.equityValue = equityValue;
	}

	public Double getHoldAmount() {
		return holdAmount;
	}

	public void setHoldAmount(Double holdAmount) {
		this.holdAmount = holdAmount;
	}

	public Double getDealValueWithAgreements() {
		return dealValueWithAgreements;
	}

	public void setDealValueWithAgreements(Double dealValueWithAgreements) {
		this.dealValueWithAgreements = dealValueWithAgreements;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getCommentsByRadhaSir() {
		return commentsByRadhaSir;
	}

	public void setCommentsByRadhaSir(String commentsByRadhaSir) {
		this.commentsByRadhaSir = commentsByRadhaSir;
	}

	public Integer getRatingByRadhaSir() {
		return ratingByRadhaSir;
	}

	public void setRatingByRadhaSir(Integer ratingByRadhaSir) {
		this.ratingByRadhaSir = ratingByRadhaSir;
	}

	public Integer getLendersRatingValue() {
		return lendersRatingValue;
	}

	public void setLendersRatingValue(Integer lendersRatingValue) {
		this.lendersRatingValue = lendersRatingValue;
	}

	public double getInterestPendingTillDate() {
		return interestPendingTillDate;
	}

	public void setInterestPendingTillDate(double interestPendingTillDate) {
		this.interestPendingTillDate = interestPendingTillDate;
	}

	public List<LoanEmiCardResponseDto> getEmiDto() {
		return emiDto;
	}

	public void setEmiDto(List<LoanEmiCardResponseDto> emiDto) {
		this.emiDto = emiDto;
	}

	public List<LoanEmiCardPaymentDetailsResponseDto> getLoanEmiCardPaymentDetailsResponseDtos() {
		return loanEmiCardPaymentDetailsResponseDtos;
	}

	public void setLoanEmiCardPaymentDetailsResponseDtos(
			List<LoanEmiCardPaymentDetailsResponseDto> loanEmiCardPaymentDetailsResponseDtos) {
		this.loanEmiCardPaymentDetailsResponseDtos = loanEmiCardPaymentDetailsResponseDtos;
	}

	public double getTotalWalletAmount() {
		return totalWalletAmount;
	}

	public void setTotalWalletAmount(double totalWalletAmount) {
		this.totalWalletAmount = totalWalletAmount;
	}

	public double getInProcessAmountWallet() {
		return inProcessAmountWallet;
	}

	public void setInProcessAmountWallet(double inProcessAmountWallet) {
		this.inProcessAmountWallet = inProcessAmountWallet;
	}

	public double getWalletAmount() {
		return walletAmount;
	}

	public void setWalletAmount(double walletAmount) {
		this.walletAmount = walletAmount;
	}

	public String getLoanProcessType() {
		return loanProcessType;
	}

	public void setLoanProcessType(String loanProcessType) {
		this.loanProcessType = loanProcessType;
	}

	public double getPenality() {
		return penality;
	}

	public void setPenality(double penality) {
		this.penality = penality;
	}

	public String getEmiDueOnDate() {
		return emiDueOnDate;
	}

	public void setEmiDueOnDate(String emiDueOnDate) {
		this.emiDueOnDate = emiDueOnDate;
	}

	public int getEmiNumber() {
		return emiNumber;
	}

	public void setEmiNumber(int emiNumber) {
		this.emiNumber = emiNumber;
	}

	public double getDisburmentAmount() {
		return disburmentAmount;
	}

	public void setDisburmentAmount(double disburmentAmount) {
		this.disburmentAmount = disburmentAmount;
	}

	public String getApplicationNumber() {
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public String getOxyLoanAdminComments() {
		return oxyLoanAdminComments;
	}

	public void setOxyLoanAdminComments(String oxyLoanAdminComments) {
		this.oxyLoanAdminComments = oxyLoanAdminComments;
	}

	public String getEnachType() {
		return enachType;
	}

	public void setEnachType(String enachType) {
		this.enachType = enachType;
	}

	public boolean isBeforeEnach() {
		return beforeEnach;
	}

	public boolean isAftreEnach() {
		return aftreEnach;
	}

	public void setBeforeEnach(boolean beforeEnach) {
		this.beforeEnach = beforeEnach;
	}

	public void setAftreEnach(boolean aftreEnach) {
		this.aftreEnach = aftreEnach;
	}

	public boolean isEnachMandateResponce() {
		return enachMandateResponce;
	}

	public void setEnachMandateResponce(boolean enachMandateResponce) {
		this.enachMandateResponce = enachMandateResponce;
	}

	public Integer getDurationGivenByAdmin() {
		return durationGivenByAdmin;
	}

	public void setDurationGivenByAdmin(Integer durationGivenByAdmin) {
		this.durationGivenByAdmin = durationGivenByAdmin;
	}

	public double getRateOfInterestGivenByAdmin() {
		return rateOfInterestGivenByAdmin;
	}

	public void setRateOfInterestGivenByAdmin(double rateOfInterestGivenByAdmin) {
		this.rateOfInterestGivenByAdmin = rateOfInterestGivenByAdmin;
	}

	public double getAcceptedAmount() {
		return acceptedAmount;
	}

	public void setAcceptedAmount(double acceptedAmount) {
		this.acceptedAmount = acceptedAmount;
	}

	public boolean isRiskStatus() {
		return riskStatus;
	}

	public boolean isSendofferstatus() {
		return sendofferstatus;
	}

	public void setRiskStatus(boolean riskStatus) {
		this.riskStatus = riskStatus;
	}

	public void setSendofferstatus(boolean sendofferstatus) {
		this.sendofferstatus = sendofferstatus;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<LenderFavouriteResponseDto> getFavouriteBorrowers() {
		return favouriteBorrowers;
	}

	public void setFavouriteBorrowers(List<LenderFavouriteResponseDto> favouriteBorrowers) {
		this.favouriteBorrowers = favouriteBorrowers;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getWorkExperience() {
		return workExperience;
	}

	public void setWorkExperience(String workExperience) {
		this.workExperience = workExperience;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public Boolean getIsECSActivated() {
		return isECSActivated;
	}

	public void setIsECSActivated(Boolean isECSActivated) {
		this.isECSActivated = isECSActivated;
	}

	public int getEnachMandateId() {
		return enachMandateId;
	}

	public void setEnachMandateId(int enachMandateId) {
		this.enachMandateId = enachMandateId;
	}

	public List<KycFileResponse> getLenderKycDocuments() {
		return lenderKycDocuments;
	}

	public void setLenderKycDocuments(List<KycFileResponse> lenderKycDocuments) {
		this.lenderKycDocuments = lenderKycDocuments;
	}

	public List<KycFileResponse> getBorrowerKycDocuments() {
		return borrowerKycDocuments;
	}

	public void setBorrowerKycDocuments(List<KycFileResponse> borrowerKycDocuments) {
		this.borrowerKycDocuments = borrowerKycDocuments;
	}

	public Integer getLenderUserId() {
		return lenderUserId;
	}

	public void setLenderUserId(Integer lenderUserId) {
		this.lenderUserId = lenderUserId;
	}

	public Integer getBorrowerUserId() {
		return borrowerUserId;
	}

	public void setBorrowerUserId(Integer borrowerUserId) {
		this.borrowerUserId = borrowerUserId;
	}

	public String getLoanAdminComments() {
		return loanAdminComments;
	}

	public void setLoanAdminComments(String loanAdminComments) {
		this.loanAdminComments = loanAdminComments;
	}

	public String getBorrowerDisbursementDate() {
		return borrowerDisbursementDate;
	}

	public void setBorrowerDisbursementDate(String borrowerDisbursementDate) {
		this.borrowerDisbursementDate = borrowerDisbursementDate;
	}

	public Double getTotalAmountPending() {
		return totalAmountPending;
	}

	public void setTotalAmountPending(Double totalAmountPending) {
		this.totalAmountPending = totalAmountPending;
	}

	public Double getTotalAmountPaid() {
		return totalAmountPaid;
	}

	public void setTotalAmountPaid(Double totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}

	public Double getPrincipalAmountPending() {
		return principalAmountPending;
	}

	public void setPrincipalAmountPending(Double principalAmountPending) {
		this.principalAmountPending = principalAmountPending;
	}

	public Integer getNoRemaingEmis() {
		return noRemaingEmis;
	}

	public void setNoRemaingEmis(Integer noRemaingEmis) {
		this.noRemaingEmis = noRemaingEmis;
	}

	public String getEmiStartDate() {
		return emiStartDate;
	}

	public void setEmiStartDate(String emiStartDate) {
		this.emiStartDate = emiStartDate;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getLoanRequestId() {
		return loanRequestId;
	}

	public void setLoanRequestId(int loanRequestId) {
		this.loanRequestId = loanRequestId;
	}

	public String getLoanRequest() {
		return loanRequest;
	}

	public void setLoanRequest(String loanRequest) {
		this.loanRequest = loanRequest;
	}

	public String getLoanRequestedDate() {
		return loanRequestedDate;
	}

	public void setLoanRequestedDate(String loanRequestedDate) {
		this.loanRequestedDate = loanRequestedDate;
	}

	public UserResponse getUser() {
		return user;
	}

	public void setUser(UserResponse user) {
		this.user = user;
	}

	public String getUserDisplayId() {
		return userDisplayId;
	}

	public void setUserDisplayId(String userDisplayId) {
		this.userDisplayId = userDisplayId;
	}

	public Double getLoanDisbursedAmount() {
		return loanDisbursedAmount;
	}

	public void setLoanDisbursedAmount(Double loanDisbursedAmount) {
		this.loanDisbursedAmount = loanDisbursedAmount;
	}

	public String getLoanStatus() {
		return loanStatus;
	}

	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getLenderDisplayId() {
		return lenderDisplayId;
	}

	public void setLenderDisplayId(String lenderDisplayId) {
		this.lenderDisplayId = lenderDisplayId;
	}

	public String getBorrowerDisplayId() {
		return borrowerDisplayId;
	}

	public void setBorrowerDisplayId(String borrowerDisplayId) {
		this.borrowerDisplayId = borrowerDisplayId;
	}

	public UserResponse getLenderUser() {
		return lenderUser;
	}

	public void setLenderUser(UserResponse lenderUser) {
		this.lenderUser = lenderUser;
	}

	public UserResponse getBorrowerUser() {
		return borrowerUser;
	}

	public void setBorrowerUser(UserResponse borrowerUser) {
		this.borrowerUser = borrowerUser;
	}

	public double getProfit() {
		return profit;
	}

	public void setProfit(double profit) {
		this.profit = profit;
	}

	public boolean isAlreadyOffered() {
		return alreadyOffered;
	}

	public void setAlreadyOffered(boolean alreadyOffered) {
		this.alreadyOffered = alreadyOffered;
	}

	public boolean isAlreadyActive() {
		return alreadyActive;
	}

	public void setAlreadyActive(boolean alreadyActive) {
		this.alreadyActive = alreadyActive;
	}

	public int getLoanDisbursedPercentage() {
		return loanDisbursedPercentage;
	}

	public void setLoanDisbursedPercentage(int loanDisbursedPercentage) {
		this.loanDisbursedPercentage = loanDisbursedPercentage;
	}

	public Boolean getBorrowerFeePaid() {
		return borrowerFeePaid;
	}

	public void setBorrowerFeePaid(Boolean borrowerFeePaid) {
		this.borrowerFeePaid = borrowerFeePaid;
	}

	public Boolean getLenderFeePaid() {
		return lenderFeePaid;
	}

	public void setLenderFeePaid(Boolean lenderFeePaid) {
		this.lenderFeePaid = lenderFeePaid;
	}

	public String getAgreementUrl() {
		return agreementUrl;
	}

	public void setAgreementUrl(String agreementUrl) {
		this.agreementUrl = agreementUrl;
	}

	public int getNoOfEmisPaid() {
		return noOfEmisPaid;
	}

	public void setNoOfEmisPaid(int noOfEmisPaid) {
		this.noOfEmisPaid = noOfEmisPaid;
	}

	public Double getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(Double emiAmount) {
		this.emiAmount = emiAmount;
	}

	public Double getDueEmiAmount() {
		return dueEmiAmount;
	}

	public void setDueEmiAmount(Double dueEmiAmount) {
		this.dueEmiAmount = dueEmiAmount;
	}

	public Integer getLenderEsignId() {
		return lenderEsignId;
	}

	public void setLenderEsignId(Integer lenderEsignId) {
		this.lenderEsignId = lenderEsignId;
	}

	public Integer getBorrowerEsignId() {
		return borrowerEsignId;
	}

	public void setBorrowerEsignId(Integer borrowerEsignId) {
		this.borrowerEsignId = borrowerEsignId;
	}

	public OfferSentDetails getOfferSentDetails() {
		return offerSentDetails;
	}

	public void setOfferSentDetails(OfferSentDetails offerSentDetails) {
		this.offerSentDetails = offerSentDetails;
	}

	public String getAdminComments() {
		return adminComments;
	}

	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}

	public String getLoanEmiCardStatus() {
		return loanEmiCardStatus;
	}

	public void setLoanEmiCardStatus(String loanEmiCardStatus) {
		this.loanEmiCardStatus = loanEmiCardStatus;
	}

	public BorrowerDetailsRequestDto getBorrowerCommentsDetails() {
		return BorrowerCommentsDetails;
	}

	public void setBorrowerCommentsDetails(BorrowerDetailsRequestDto borrowerCommentsDetails) {
		BorrowerCommentsDetails = borrowerCommentsDetails;
	}

	public CommentsRequestDto getCommentsRequestDto() {
		return commentsRequestDto;
	}

	public void setCommentsRequestDto(CommentsRequestDto commentsRequestDto) {
		this.commentsRequestDto = commentsRequestDto;
	}

	public boolean isEnachInitiated() {
		return enachInitiated;
	}

	public void setEnachInitiated(boolean enachInitiated) {
		this.enachInitiated = enachInitiated;
	}

	public boolean isEnachSuccess() {
		return enachSuccess;
	}

	public void setEnachSuccess(boolean enachSuccess) {
		this.enachSuccess = enachSuccess;
	}

	public EnachMandateResponseForEmi getEnachMandateResponseForEmi() {
		return enachMandateResponseForEmi;
	}

	public void setEnachMandateResponseForEmi(EnachMandateResponseForEmi enachMandateResponseForEmi) {
		this.enachMandateResponseForEmi = enachMandateResponseForEmi;
	}

	public EnachMandateResponseForPrincipal getEnachMandateResponseForPrincipal() {
		return enachMandateResponseForPrincipal;
	}

	public void setEnachMandateResponseForPrincipal(EnachMandateResponseForPrincipal enachMandateResponseForPrincipal) {
		this.enachMandateResponseForPrincipal = enachMandateResponseForPrincipal;
	}

	public String getReferredBy() {
		return referredBy;
	}

	public void setReferredBy(String referredBy) {
		this.referredBy = referredBy;
	}

	public String getDriveLink() {
		return driveLink;
	}

	public void setDriveLink(String driveLink) {
		this.driveLink = driveLink;
	}

	public String getCifNumber() {
		return cifNumber;
	}

	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}

	public String getFinoEmployeeMobileNumber() {
		return finoEmployeeMobileNumber;
	}

	public void setFinoEmployeeMobileNumber(String finoEmployeeMobileNumber) {
		this.finoEmployeeMobileNumber = finoEmployeeMobileNumber;
	}

	public Double getRateOfInterestToBorrower() {
		return rateOfInterestToBorrower;
	}

	public void setRateOfInterestToBorrower(Double rateOfInterestToBorrower) {
		this.rateOfInterestToBorrower = rateOfInterestToBorrower;
	}

	public Double getRateOfInterestToLender() {
		return rateOfInterestToLender;
	}

	public void setRateOfInterestToLender(Double rateOfInterestToLender) {
		this.rateOfInterestToLender = rateOfInterestToLender;
	}

	public String getRepaymentTypeToBorrower() {
		return repaymentTypeToBorrower;
	}

	public void setRepaymentTypeToBorrower(String repaymentTypeToBorrower) {
		this.repaymentTypeToBorrower = repaymentTypeToBorrower;
	}

	public String getRepaymentTypeToLender() {
		return repaymentTypeToLender;
	}

	public void setRepaymentTypeToLender(String repaymentTypeToLender) {
		this.repaymentTypeToLender = repaymentTypeToLender;
	}

	public boolean isPartnerEsign() {
		return partnerEsign;
	}

	public void setPartnerEsign(boolean partnerEsign) {
		this.partnerEsign = partnerEsign;
	}

	public boolean isPartnerEnach() {
		return partnerEnach;
	}

	public void setPartnerEnach(boolean partnerEnach) {
		this.partnerEnach = partnerEnach;
	}

	public boolean isOfferAcceptedStatus() {
		return offerAcceptedStatus;
	}

	public void setOfferAcceptedStatus(boolean offerAcceptedStatus) {
		this.offerAcceptedStatus = offerAcceptedStatus;
	}

}
