package com.oxyloans.dto.serviceloan;

import java.util.Date;

public class OfferSentDetails {
	private String LoanRequestId;

	private Integer id;

	private Date OfferSentOn;

	private Date OfferAccepeted;

	private Integer duaration;

	private Double rateOfInterest;

	private String comments;

	private String loanOfferdStatus;

	private Double loanOfferedAmount;

	private Double borrowerFee;

	private Double emiAmount;

	private Double netDisbursementAmount;

	private String PayUStatus;

	private String DurationType;

	public String getPayUStatus() {
		return PayUStatus;
	}

	public void setPayUStatus(String payUStatus) {
		PayUStatus = payUStatus;
	}

	public Double getNetDisbursementAmount() {
		return netDisbursementAmount;
	}

	public void setNetDisbursementAmount(Double netDisbursementAmount) {
		this.netDisbursementAmount = netDisbursementAmount;
	}

	public Double getBorrowerFee() {
		return borrowerFee;
	}

	public Double getEmiAmount() {
		return emiAmount;
	}

	public void setBorrowerFee(Double borrowerFee) {
		this.borrowerFee = borrowerFee;
	}

	public void setEmiAmount(Double emiAmount) {
		this.emiAmount = emiAmount;
	}

	public String getLoanRequestId() {
		return LoanRequestId;
	}

	public Integer getId() {
		return id;
	}

	public Date getOfferSentOn() {
		return OfferSentOn;
	}

	public Date getOfferAccepeted() {
		return OfferAccepeted;
	}

	public Integer getDuaration() {
		return duaration;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public String getComments() {
		return comments;
	}

	public String getLoanOfferdStatus() {
		return loanOfferdStatus;
	}

	public Double getLoanOfferedAmount() {
		return loanOfferedAmount;
	}

	public void setLoanRequestId(String loanRequestId) {
		LoanRequestId = loanRequestId;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setOfferSentOn(Date offerSentOn) {
		OfferSentOn = offerSentOn;
	}

	public void setOfferAccepeted(Date offerAccepeted) {
		OfferAccepeted = offerAccepeted;
	}

	public void setDuaration(Integer duaration) {
		this.duaration = duaration;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void setLoanOfferdStatus(String loanOfferdStatus) {
		this.loanOfferdStatus = loanOfferdStatus;
	}

	public void setLoanOfferedAmount(Double loanOfferedAmount) {
		this.loanOfferedAmount = loanOfferedAmount;
	}

	public String getDurationType() {
		return DurationType;
	}

	public void setDurationType(String durationType) {
		DurationType = durationType;
	}

}
