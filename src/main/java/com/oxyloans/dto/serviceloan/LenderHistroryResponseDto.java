package com.oxyloans.dto.serviceloan;

import java.math.BigInteger;
import java.util.Date;

public class LenderHistroryResponseDto {
	private String accountNumber;

	private double creditedAmount;

	private double debitedAmount;

	private String loanStatus;

	private String borrowerName;

	private String loanId;

	private String transactionDate;

	private String disbursedAmount;

	private double walletBalance;

	private String creditedStatus;

	private String debitedStatus;

	private Date sortingTransactionDate;

	private Double profitReceived;

	private String applicationNumber;

	private double rateOfInterest;

	private int duration;

	private double totalAmountDisbursed;

	private double amountPaidTillDate;

	private double profit;

	private Integer emisPaidByBorrower;
	
	private Double withdrawAmount;

	private String amountFrom;
	
	private String remarks;
	
	private BigInteger sumOfInterestAmountForDeal;

	private BigInteger sumOfPrincipalAmountForDeal;

	private String dealName;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getCreditedAmount() {
		return creditedAmount;
	}

	public void setCreditedAmount(double creditedAmount) {
		this.creditedAmount = creditedAmount;
	}

	public double getDebitedAmount() {
		return debitedAmount;
	}

	public void setDebitedAmount(double debitedAmount) {
		this.debitedAmount = debitedAmount;
	}

	public String getLoanStatus() {
		return loanStatus;
	}

	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getDisbursedAmount() {
		return disbursedAmount;
	}

	public void setDisbursedAmount(String disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}

	public double getWalletBalance() {
		return walletBalance;
	}

	public void setWalletBalance(double walletBalance) {
		this.walletBalance = walletBalance;
	}

	public String getCreditedStatus() {
		return creditedStatus;
	}

	public void setCreditedStatus(String creditedStatus) {
		this.creditedStatus = creditedStatus;
	}

	public String getDebitedStatus() {
		return debitedStatus;
	}

	public void setDebitedStatus(String debitedStatus) {
		this.debitedStatus = debitedStatus;
	}

	public Date getSortingTransactionDate() {
		return sortingTransactionDate;
	}

	public void setSortingTransactionDate(Date sortingTransactionDate) {
		this.sortingTransactionDate = sortingTransactionDate;
	}

	public Double getProfitReceived() {
		return profitReceived;
	}

	public void setProfitReceived(Double profitReceived) {
		this.profitReceived = profitReceived;
	}

	public String getApplicationNumber() {
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}

	public double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public double getTotalAmountDisbursed() {
		return totalAmountDisbursed;
	}

	public void setTotalAmountDisbursed(double totalAmountDisbursed) {
		this.totalAmountDisbursed = totalAmountDisbursed;
	}

	public double getAmountPaidTillDate() {
		return amountPaidTillDate;
	}

	public void setAmountPaidTillDate(double amountPaidTillDate) {
		this.amountPaidTillDate = amountPaidTillDate;
	}

	public double getProfit() {
		return profit;
	}

	public void setProfit(double profit) {
		this.profit = profit;
	}

	public Integer getEmisPaidByBorrower() {
		return emisPaidByBorrower;
	}

	public void setEmisPaidByBorrower(Integer emisPaidByBorrower) {
		this.emisPaidByBorrower = emisPaidByBorrower;
	}

	public Double getWithdrawAmount() {
		return withdrawAmount;
	}

	public void setWithdrawAmount(Double withdrawAmount) {
		this.withdrawAmount = withdrawAmount;
	}

	public String getAmountFrom() {
		return amountFrom;
	}

	public void setAmountFrom(String amountFrom) {
		this.amountFrom = amountFrom;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigInteger getSumOfInterestAmountForDeal() {
		return sumOfInterestAmountForDeal;
	}

	public void setSumOfInterestAmountForDeal(BigInteger sumOfInterestAmountForDeal) {
		this.sumOfInterestAmountForDeal = sumOfInterestAmountForDeal;
	}

	public BigInteger getSumOfPrincipalAmountForDeal() {
		return sumOfPrincipalAmountForDeal;
	}

	public void setSumOfPrincipalAmountForDeal(BigInteger sumOfPrincipalAmountForDeal) {
		this.sumOfPrincipalAmountForDeal = sumOfPrincipalAmountForDeal;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

}
