package com.oxyloans.dto.serviceloan;

public class LoanEmiCardPaymentDetailsResponseDto extends LoanEmiCardPaymentDetailsRequestDto {
	private Integer emiId;

	private String loanId;

	private String modeOfPayment;

	private String transactionReferenceNumber;

	private String paymentStatus;

	private double partialPaymentAmount;

	private double penalty;

	private double remainingEmiAmount;

	private String penaltyDate;

	private String amountPaidDate;

	private String partialAmountPaidDate;

	private String partParymentDate;

	private double remainingPenaltyAndRemainingEmi;
	
	private String mobileNumber;

	private String borrowerName;

	private Integer borrowerId;

	

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public Integer getEmiId() {
		return emiId;
	}

	public void setEmiId(Integer emiId) {
		this.emiId = emiId;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getTransactionReferenceNumber() {
		return transactionReferenceNumber;
	}

	public void setTransactionReferenceNumber(String transactionReferenceNumber) {
		this.transactionReferenceNumber = transactionReferenceNumber;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public double getPartialPaymentAmount() {
		return partialPaymentAmount;
	}

	public void setPartialPaymentAmount(double partialPaymentAmount) {
		this.partialPaymentAmount = partialPaymentAmount;
	}

	public double getPenalty() {
		return penalty;
	}

	public void setPenalty(double penalty) {
		this.penalty = penalty;
	}

	public double getRemainingEmiAmount() {
		return remainingEmiAmount;
	}

	public void setRemainingEmiAmount(double remainingEmiAmount) {
		this.remainingEmiAmount = remainingEmiAmount;
	}

	public String getPenaltyDate() {
		return penaltyDate;
	}

	public void setPenaltyDate(String penaltyDate) {
		this.penaltyDate = penaltyDate;
	}

	public String getAmountPaidDate() {
		return amountPaidDate;
	}

	public void setAmountPaidDate(String amountPaidDate) {
		this.amountPaidDate = amountPaidDate;
	}

	public String getPartialAmountPaidDate() {
		return partialAmountPaidDate;
	}

	public void setPartialAmountPaidDate(String partialAmountPaidDate) {
		this.partialAmountPaidDate = partialAmountPaidDate;
	}

	public String getPartParymentDate() {
		return partParymentDate;
	}

	public void setPartParymentDate(String partParymentDate) {
		this.partParymentDate = partParymentDate;
	}

	public double getRemainingPenaltyAndRemainingEmi() {
		return remainingPenaltyAndRemainingEmi;
	}

	public void setRemainingPenaltyAndRemainingEmi(double remainingPenaltyAndRemainingEmi) {
		this.remainingPenaltyAndRemainingEmi = remainingPenaltyAndRemainingEmi;
	}

}
