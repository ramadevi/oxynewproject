package com.oxyloans.dto.serviceloan;

public class LenderEmiDetailsResponseDto {

	private Integer borrowerId;

	private String borrowerName;

	private String loanId;

	private Integer numbersOfEmisPaid;

	private double EmisReceived;

	private double profit;

	private double disburmentAmount;

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public Integer getNumbersOfEmisPaid() {
		return numbersOfEmisPaid;
	}

	public void setNumbersOfEmisPaid(Integer numbersOfEmisPaid) {
		this.numbersOfEmisPaid = numbersOfEmisPaid;
	}

	public double getEmisReceived() {
		return EmisReceived;
	}

	public void setEmisReceived(double emisReceived) {
		EmisReceived = emisReceived;
	}

	public double getProfit() {
		return profit;
	}

	public void setProfit(double profit) {
		this.profit = profit;
	}

	public double getDisburmentAmount() {
		return disburmentAmount;
	}

	public void setDisburmentAmount(double disburmentAmount) {
		this.disburmentAmount = disburmentAmount;
	}

}
