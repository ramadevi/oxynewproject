package com.oxyloans.dto.serviceloan;

public class CreditScoreCalculationDto {

	private int creditScoreValue;
	private int referencesCount;
	public int getCreditScoreValue() {
		return creditScoreValue;
	}
	public void setCreditScoreValue(int creditScoreValue) {
		this.creditScoreValue = creditScoreValue;
	}
	public int getReferencesCount() {
		return referencesCount;
	}
	public void setReferencesCount(int referencesCount) {
		this.referencesCount = referencesCount;
	}

	

}
