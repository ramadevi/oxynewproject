package com.oxyloans.dto.serviceloan;

import java.util.List;

public class LenderTransactionHistoryResponseDto {
	private List<LenderHistroryResponseDto> lenderHistoryList;

	private String downloadUrl;

	public List<LenderHistroryResponseDto> getLenderHistoryList() {
		return lenderHistoryList;
	}

	public void setLenderHistoryList(List<LenderHistroryResponseDto> lenderHistoryList) {
		this.lenderHistoryList = lenderHistoryList;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

}
