package com.oxyloans.dto.serviceloan;

import java.util.Comparator;
import java.util.List;

public class DealLevelInterestPaymentsDto {

	private int sNo;
	private String paymentDate;
	private int dealId;
	private String DealName;
	private String principal;
	private int noOfLenders;
	private Double totalInterest;
	private String dealStatus;
	private String paymentStatus;
	private int tenure;
	private int paymentMonth;
	private String actualPaymentDate;
	private String fileName;
	private String downloadUrl;
	private Integer totalAchievedValue;
	private Integer participatedValue;
	private Integer totalDealValue;
	private Integer currentValue;
	private Integer withDrawlValue;
	private int noOfwithdrawlsFromWallet;
	private int noOfwithdrawlsFromDeal;
	private int totalNoOfWithDrawls;
	private int amountReturnedToWallet;
	private int amountReturned;
	private Double rateOfInterest;

	private List<InterestsApprovalDto> interestsApprovalDto;

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getDealName() {
		return DealName;
	}

	public void setDealName(String dealName) {
		DealName = dealName;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public int getNoOfLenders() {
		return noOfLenders;
	}

	public void setNoOfLenders(int noOfLenders) {
		this.noOfLenders = noOfLenders;
	}

	public String getDealStatus() {
		return dealStatus;
	}

	public void setDealStatus(String dealStatus) {
		this.dealStatus = dealStatus;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Double getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(Double totalInterest) {
		this.totalInterest = totalInterest;
	}

	public List<InterestsApprovalDto> getInterestsApprovalDto() {
		return interestsApprovalDto;
	}

	public void setInterestsApprovalDto(List<InterestsApprovalDto> interestsApprovalDto) {
		this.interestsApprovalDto = interestsApprovalDto;
	}

	public int getsNo() {
		return sNo;
	}

	public void setsNo(int sNo) {
		this.sNo = sNo;
	}

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public int getTenure() {
		return tenure;
	}

	public void setTenure(int tenure) {
		this.tenure = tenure;
	}

	public int getPaymentMonth() {
		return paymentMonth;
	}

	public void setPaymentMonth(int paymentMonth) {
		this.paymentMonth = paymentMonth;
	}

	public String getActualPaymentDate() {
		return actualPaymentDate;
	}

	public void setActualPaymentDate(String actualPaymentDate) {
		this.actualPaymentDate = actualPaymentDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public Integer getTotalAchievedValue() {
		return totalAchievedValue;
	}

	public void setTotalAchievedValue(Integer totalAchievedValue) {
		this.totalAchievedValue = totalAchievedValue;
	}

	public Integer getParticipatedValue() {
		return participatedValue;
	}

	public void setParticipatedValue(Integer participatedValue) {
		this.participatedValue = participatedValue;
	}

	public Integer getTotalDealValue() {
		return totalDealValue;
	}

	public void setTotalDealValue(Integer totalDealValue) {
		this.totalDealValue = totalDealValue;
	}

	public Integer getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(Integer currentValue) {
		this.currentValue = currentValue;
	}

	public Integer getWithDrawlValue() {
		return withDrawlValue;
	}

	public void setWithDrawlValue(Integer withDrawlValue) {
		this.withDrawlValue = withDrawlValue;
	}

	public int getNoOfwithdrawlsFromWallet() {
		return noOfwithdrawlsFromWallet;
	}

	public void setNoOfwithdrawlsFromWallet(int noOfwithdrawlsFromWallet) {
		this.noOfwithdrawlsFromWallet = noOfwithdrawlsFromWallet;
	}

	public int getNoOfwithdrawlsFromDeal() {
		return noOfwithdrawlsFromDeal;
	}

	public void setNoOfwithdrawlsFromDeal(int noOfwithdrawlsFromDeal) {
		this.noOfwithdrawlsFromDeal = noOfwithdrawlsFromDeal;
	}

	public int getTotalNoOfWithDrawls() {
		return totalNoOfWithDrawls;
	}

	public void setTotalNoOfWithDrawls(int totalNoOfWithDrawls) {
		this.totalNoOfWithDrawls = totalNoOfWithDrawls;
	}

	public int getAmountReturnedToWallet() {
		return amountReturnedToWallet;
	}

	public void setAmountReturnedToWallet(int amountReturnedToWallet) {
		this.amountReturnedToWallet = amountReturnedToWallet;
	}

	public int getAmountReturned() {
		return amountReturned;
	}

	public void setAmountReturned(int amountReturned) {
		this.amountReturned = amountReturned;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public Comparator<DealLevelInterestPaymentsDto> getComparator() {
		return comparator;
	}

	public void setComparator(Comparator<DealLevelInterestPaymentsDto> comparator) {
		this.comparator = comparator;
	}

	public Comparator<DealLevelInterestPaymentsDto> comparator = (c1, c2) -> {
		return (c1.getPaymentDate()).compareTo(c2.getPaymentDate());

	};

}
