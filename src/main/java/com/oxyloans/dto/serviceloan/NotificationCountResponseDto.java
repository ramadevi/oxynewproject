package com.oxyloans.dto.serviceloan;

public class NotificationCountResponseDto {
	private int totalCount;
	private int interestedPendingBorrowers;
	private int interestedPendingLenders;
	private int lenderTransactionUploadedPending;
	private int offerPendingFromAdminSide;
	private int countNumberOfLoansInAgreedState;
	private int countNumberPendingDisbursmentDate;
	
	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getInterestedPendingBorrowers() {
		return interestedPendingBorrowers;
	}

	public void setInterestedPendingBorrowers(int interestedPendingBorrowers) {
		this.interestedPendingBorrowers = interestedPendingBorrowers;
	}

	public int getInterestedPendingLenders() {
		return interestedPendingLenders;
	}

	public void setInterestedPendingLenders(int interestedPendingLenders) {
		this.interestedPendingLenders = interestedPendingLenders;
	}

	public int getLenderTransactionUploadedPending() {
		return lenderTransactionUploadedPending;
	}

	public void setLenderTransactionUploadedPending(int lenderTransactionUploadedPending) {
		this.lenderTransactionUploadedPending = lenderTransactionUploadedPending;
	}

	public int getOfferPendingFromAdminSide() {
		return offerPendingFromAdminSide;
	}

	public void setOfferPendingFromAdminSide(int offerPendingFromAdminSide) {
		this.offerPendingFromAdminSide = offerPendingFromAdminSide;
	}

	public int getCountNumberOfLoansInAgreedState() {
		return countNumberOfLoansInAgreedState;
	}

	public void setCountNumberOfLoansInAgreedState(int countNumberOfLoansInAgreedState) {
		this.countNumberOfLoansInAgreedState = countNumberOfLoansInAgreedState;
	}

	public int getCountNumberPendingDisbursmentDate() {
		return countNumberPendingDisbursmentDate;
	}

	public void setCountNumberPendingDisbursmentDate(int countNumberPendingDisbursmentDate) {
		this.countNumberPendingDisbursmentDate = countNumberPendingDisbursmentDate;
	}

	
}
