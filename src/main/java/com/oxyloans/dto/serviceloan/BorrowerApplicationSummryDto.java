package com.oxyloans.dto.serviceloan;

public class BorrowerApplicationSummryDto {
	private Integer id;

	private Double loanreRequestAmount;

	private Double loanOfferAmount;

	private String OfferStatus;

	private String applicationId;

	private Integer userId;

	private String offerSentDate;

	private String OfferAccpetedDate;

	private Double rateOfInterest;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOfferSentDate() {
		return offerSentDate;
	}

	public void setOfferSentDate(String offerSentDate) {
		this.offerSentDate = offerSentDate;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public String getOfferAccpetedDate() {
		return OfferAccpetedDate;
	}

	public void setOfferAccpetedDate(String offerAccpetedDate) {
		OfferAccpetedDate = offerAccpetedDate;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Double getLoanreRequestAmount() {
		return loanreRequestAmount;
	}

	public Double getLoanOfferAmount() {
		return loanOfferAmount;
	}

	public String getOfferStatus() {
		return OfferStatus;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setLoanreRequestAmount(Double loanreRequestAmount) {
		this.loanreRequestAmount = loanreRequestAmount;
	}

	public void setLoanOfferAmount(Double loanOfferAmount) {
		this.loanOfferAmount = loanOfferAmount;
	}

	public void setOfferStatus(String offerStatus) {
		OfferStatus = offerStatus;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

}
