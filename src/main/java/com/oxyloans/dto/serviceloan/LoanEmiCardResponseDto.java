package com.oxyloans.dto.serviceloan;

import java.util.List;

import com.oxyloans.response.user.LenderEmiDetails;
import com.oxyloans.response.user.UserResponse;

public class LoanEmiCardResponseDto {

	private int id;

	private Integer loanId;

	private Integer emiNumber;

	private double emiPrincipalAmount = 0.0;

	private double emiInterstAmount = 0.0;

	private double emiAmount = 0.0;

	private String emiDueOn;

	private String emiPaidOn;

	private double emiLateFeeCharges = 0.0;

	private boolean emiStatus = false;

	private Boolean paidOnTime;

	private String comments;

	private UserResponse lenderUser;

	private UserResponse borrowerUser;

	private String payuTransactionNumber;

	private String oxyLoansTransactionNumber;

	private String payuStatus;

	private List<LoanEmiCardPaymentDetailsResponseDto> loanPaymenHistory;

	private String emiStatusBasedOnPayment;

	private double EmiBalanceAmount;

	private double transferedPreviousEmiBalanceAmt;

	private String partPayAmount;

	private double remainingEmiAmount;

	private double totalPartialPaymentAmount;

	private double excessOfEmiAmount;

	private String modeOfPayment;

	private double emiAmountAfterExcessAmountRemoved;

	private double remainingPenaltyAndRemainingEmi;

	private String emiDueOnDate;

	private double interestAmount;

	private int differenceInDays;

	private double dueAmount;

	private double penalty;

	private double penaltyPaid;

	private double penaltyDue;

	private double dueAmountWithoutPenality;

	private double dueAmountWithPenality;

	private double emiAmountPaid;

	private double interestAmountPaid;

	private double totalAmountPaid;

	private double excessAmount;

	private double applicationLevelTotalAmount;

	private String emiPaidOnDate;

	private double totalApplicationLevelDue;

	private String mobileNumber;

	private String borrowerName;

	private String status;

	private int applicationId;

	private double penaltyWaiveOffValue;

	private String penalityWaiveOffDay;

	private String partPaymentDate;

	private double totalPenaltyWaiveToApplication;

	private Integer borrowerId;

	private List<LoanEmiCardResponseDto> getApplicationLevelPenalityWithEmi;

	private int lenderId;

	private String lenderName;

	private double applicationLevelPartPaid;

	private List<LoanEmiCardResponseDto> paymentHistory;

	private List<LoanEmiCardResponseDto> lenderDetails;

	private List<LoanEmiCardResponseDto> partPayment;

	private double ApplicationLevelDisbursedAmount;

	private double applicationlevelInterestAmount;

	private double applicationlevelPrincipalAmount;

	private double interestForAppllication;

	private double durationForApplication;

	private double rateOfInterest;

	private int duration;

	private String disbursedDate;

	private double firstEmiAmount;

	private double secondEmiAmount;

	private String emiStartDate;

	private int emisToBePaidTillDate;

	private int emisPaid;

	private int unpaidEmis;

	private int furtherEmis;

	private int emisPendingTillDate;

	private double preclose;

	private double outStandingAmount;

	private double amountPending;

	private double totalPenlty;

	private double interestPendingTillDate;

	private double interestWaiveOffAmount;

	private double emiAndInterest;

	private LenderEmiDetails lenderEmiDetails;

	public LenderEmiDetails getLenderEmiDetails() {
		return lenderEmiDetails;
	}

	public void setLenderEmiDetails(LenderEmiDetails lenderEmiDetails) {
		this.lenderEmiDetails = lenderEmiDetails;
	}

	public double getEmiAndInterest() {
		return emiAndInterest;
	}

	public void setEmiAndInterest(double emiAndInterest) {
		this.emiAndInterest = emiAndInterest;
	}

	public double getInterestPendingTillDate() {
		return interestPendingTillDate;
	}

	public void setInterestPendingTillDate(double interestPendingTillDate) {
		this.interestPendingTillDate = interestPendingTillDate;
	}

	public double getInterestWaiveOffAmount() {
		return interestWaiveOffAmount;
	}

	public void setInterestWaiveOffAmount(double interestWaiveOffAmount) {
		this.interestWaiveOffAmount = interestWaiveOffAmount;
	}

	public double getApplicationlevelInterestAmount() {
		return applicationlevelInterestAmount;
	}

	public void setApplicationlevelInterestAmount(double applicationlevelInterestAmount) {
		this.applicationlevelInterestAmount = applicationlevelInterestAmount;
	}

	public double getApplicationlevelPrincipalAmount() {
		return applicationlevelPrincipalAmount;
	}

	public void setApplicationlevelPrincipalAmount(double applicationlevelPrincipalAmount) {
		this.applicationlevelPrincipalAmount = applicationlevelPrincipalAmount;
	}

	public double getInterestForAppllication() {
		return interestForAppllication;
	}

	public void setInterestForAppllication(double interestForAppllication) {
		this.interestForAppllication = interestForAppllication;
	}

	public double getDurationForApplication() {
		return durationForApplication;
	}

	public void setDurationForApplication(double durationForApplication) {
		this.durationForApplication = durationForApplication;
	}

	public double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getDisbursedDate() {
		return disbursedDate;
	}

	public void setDisbursedDate(String disbursedDate) {
		this.disbursedDate = disbursedDate;
	}

	public double getFirstEmiAmount() {
		return firstEmiAmount;
	}

	public void setFirstEmiAmount(double firstEmiAmount) {
		this.firstEmiAmount = firstEmiAmount;
	}

	public double getSecondEmiAmount() {
		return secondEmiAmount;
	}

	public void setSecondEmiAmount(double secondEmiAmount) {
		this.secondEmiAmount = secondEmiAmount;
	}

	public String getEmiStartDate() {
		return emiStartDate;
	}

	public void setEmiStartDate(String emiStartDate) {
		this.emiStartDate = emiStartDate;
	}

	public int getEmisToBePaidTillDate() {
		return emisToBePaidTillDate;
	}

	public void setEmisToBePaidTillDate(int emisToBePaidTillDate) {
		this.emisToBePaidTillDate = emisToBePaidTillDate;
	}

	public int getEmisPaid() {
		return emisPaid;
	}

	public void setEmisPaid(int emisPaid) {
		this.emisPaid = emisPaid;
	}

	public int getUnpaidEmis() {
		return unpaidEmis;
	}

	public void setUnpaidEmis(int unpaidEmis) {
		this.unpaidEmis = unpaidEmis;
	}

	public int getFurtherEmis() {
		return furtherEmis;
	}

	public void setFurtherEmis(int furtherEmis) {
		this.furtherEmis = furtherEmis;
	}

	public int getEmisPendingTillDate() {
		return emisPendingTillDate;
	}

	public void setEmisPendingTillDate(int emisPendingTillDate) {
		this.emisPendingTillDate = emisPendingTillDate;
	}

	public double getPreclose() {
		return preclose;
	}

	public void setPreclose(double preclose) {
		this.preclose = preclose;
	}

	public double getOutStandingAmount() {
		return outStandingAmount;
	}

	public void setOutStandingAmount(double outStandingAmount) {
		this.outStandingAmount = outStandingAmount;
	}

	public double getAmountPending() {
		return amountPending;
	}

	public void setAmountPending(double amountPending) {
		this.amountPending = amountPending;
	}

	public double getTotalPenlty() {
		return totalPenlty;
	}

	public void setTotalPenlty(double totalPenlty) {
		this.totalPenlty = totalPenlty;
	}

	public int getLenderId() {
		return lenderId;
	}

	public void setLenderId(int lenderId) {
		this.lenderId = lenderId;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public double getApplicationLevelPartPaid() {
		return applicationLevelPartPaid;
	}

	public void setApplicationLevelPartPaid(double applicationLevelPartPaid) {
		this.applicationLevelPartPaid = applicationLevelPartPaid;
	}

	public List<LoanEmiCardResponseDto> getPaymentHistory() {
		return paymentHistory;
	}

	public void setPaymentHistory(List<LoanEmiCardResponseDto> paymentHistory) {
		this.paymentHistory = paymentHistory;
	}

	public List<LoanEmiCardResponseDto> getLenderDetails() {
		return lenderDetails;
	}

	public void setLenderDetails(List<LoanEmiCardResponseDto> lenderDetails) {
		this.lenderDetails = lenderDetails;
	}

	public List<LoanEmiCardResponseDto> getPartPayment() {
		return partPayment;
	}

	public void setPartPayment(List<LoanEmiCardResponseDto> partPayment) {
		this.partPayment = partPayment;
	}

	public double getApplicationLevelDisbursedAmount() {
		return ApplicationLevelDisbursedAmount;
	}

	public void setApplicationLevelDisbursedAmount(double applicationLevelDisbursedAmount) {
		ApplicationLevelDisbursedAmount = applicationLevelDisbursedAmount;
	}

	public double getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(double interestAmount) {
		this.interestAmount = interestAmount;
	}

	public int getDifferenceInDays() {
		return differenceInDays;
	}

	public void setDifferenceInDays(int differenceInDays) {
		this.differenceInDays = differenceInDays;
	}

	public double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(double dueAmount) {
		this.dueAmount = dueAmount;
	}

	public double getPenaltyPaid() {
		return penaltyPaid;
	}

	public void setPenaltyPaid(double penaltyPaid) {
		this.penaltyPaid = penaltyPaid;
	}

	public double getPenaltyDue() {
		return penaltyDue;
	}

	public void setPenaltyDue(double penaltyDue) {
		this.penaltyDue = penaltyDue;
	}

	public double getDueAmountWithoutPenality() {
		return dueAmountWithoutPenality;
	}

	public void setDueAmountWithoutPenality(double dueAmountWithoutPenality) {
		this.dueAmountWithoutPenality = dueAmountWithoutPenality;
	}

	public double getDueAmountWithPenality() {
		return dueAmountWithPenality;
	}

	public void setDueAmountWithPenality(double dueAmountWithPenality) {
		this.dueAmountWithPenality = dueAmountWithPenality;
	}

	public double getEmiAmountPaid() {
		return emiAmountPaid;
	}

	public void setEmiAmountPaid(double emiAmountPaid) {
		this.emiAmountPaid = emiAmountPaid;
	}

	public double getInterestAmountPaid() {
		return interestAmountPaid;
	}

	public void setInterestAmountPaid(double interestAmountPaid) {
		this.interestAmountPaid = interestAmountPaid;
	}

	public double getTotalAmountPaid() {
		return totalAmountPaid;
	}

	public void setTotalAmountPaid(double totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}

	public double getExcessAmount() {
		return excessAmount;
	}

	public void setExcessAmount(double excessAmount) {
		this.excessAmount = excessAmount;
	}

	public double getApplicationLevelTotalAmount() {
		return applicationLevelTotalAmount;
	}

	public void setApplicationLevelTotalAmount(double applicationLevelTotalAmount) {
		this.applicationLevelTotalAmount = applicationLevelTotalAmount;
	}

	public String getEmiPaidOnDate() {
		return emiPaidOnDate;
	}

	public void setEmiPaidOnDate(String emiPaidOnDate) {
		this.emiPaidOnDate = emiPaidOnDate;
	}

	public double getTotalApplicationLevelDue() {
		return totalApplicationLevelDue;
	}

	public void setTotalApplicationLevelDue(double totalApplicationLevelDue) {
		this.totalApplicationLevelDue = totalApplicationLevelDue;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public double getPenaltyWaiveOffValue() {
		return penaltyWaiveOffValue;
	}

	public void setPenaltyWaiveOffValue(double penaltyWaiveOffValue) {
		this.penaltyWaiveOffValue = penaltyWaiveOffValue;
	}

	public String getPenalityWaiveOffDay() {
		return penalityWaiveOffDay;
	}

	public void setPenalityWaiveOffDay(String penalityWaiveOffDay) {
		this.penalityWaiveOffDay = penalityWaiveOffDay;
	}

	public String getPartPaymentDate() {
		return partPaymentDate;
	}

	public void setPartPaymentDate(String partPaymentDate) {
		this.partPaymentDate = partPaymentDate;
	}

	public double getTotalPenaltyWaiveToApplication() {
		return totalPenaltyWaiveToApplication;
	}

	public void setTotalPenaltyWaiveToApplication(double totalPenaltyWaiveToApplication) {
		this.totalPenaltyWaiveToApplication = totalPenaltyWaiveToApplication;
	}

	public String getEmiDueOnDate() {
		return emiDueOnDate;
	}

	public void setEmiDueOnDate(String emiDueOnDate) {
		this.emiDueOnDate = emiDueOnDate;
	}

	public String getPartPayAmount() {
		return partPayAmount;
	}

	public void setPartPayAmount(String partPayAmount) {
		this.partPayAmount = partPayAmount;
	}

	public String getPayuStatus() {
		return payuStatus;
	}

	public void setPayuStatus(String payuStatus) {
		this.payuStatus = payuStatus;
	}

	public String getOxyLoansTransactionNumber() {
		return oxyLoansTransactionNumber;
	}

	public void setOxyLoansTransactionNumber(String oxyLoansTransactionNumber) {
		this.oxyLoansTransactionNumber = oxyLoansTransactionNumber;
	}

	public String getPayuTransactionNumber() {
		return payuTransactionNumber;
	}

	public void setPayuTransactionNumber(String payuTransactionNumber) {
		this.payuTransactionNumber = payuTransactionNumber;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getLoanId() {
		return loanId;
	}

	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}

	public Integer getEmiNumber() {
		return emiNumber;
	}

	public void setEmiNumber(Integer emiNumber) {
		this.emiNumber = emiNumber;
	}

	public double getEmiPrincipalAmount() {
		return emiPrincipalAmount;
	}

	public void setEmiPrincipalAmount(double emiPrincipalAmount) {
		this.emiPrincipalAmount = emiPrincipalAmount;
	}

	public double getEmiInterstAmount() {
		return emiInterstAmount;
	}

	public void setEmiInterstAmount(double emiInterstAmount) {
		this.emiInterstAmount = emiInterstAmount;
	}

	public double getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(double emiAmount) {
		this.emiAmount = emiAmount;
	}

	public String getEmiDueOn() {
		return emiDueOn;
	}

	public void setEmiDueOn(String emiDueOn) {
		this.emiDueOn = emiDueOn;
	}

	public String getEmiPaidOn() {
		return emiPaidOn;
	}

	public void setEmiPaidOn(String emiPaidOn) {
		this.emiPaidOn = emiPaidOn;
	}

	public double getEmiLateFeeCharges() {
		return emiLateFeeCharges;
	}

	public void setEmiLateFeeCharges(double emiLateFeeCharges) {
		this.emiLateFeeCharges = emiLateFeeCharges;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public boolean isEmiStatus() {
		return emiStatus;
	}

	public void setEmiStatus(boolean emiStatus) {
		this.emiStatus = emiStatus;
	}

	public Boolean getPaidOnTime() {
		return paidOnTime;
	}

	public void setPaidOnTime(Boolean paidOnTime) {
		this.paidOnTime = paidOnTime;
	}

	public UserResponse getLenderUser() {
		return lenderUser;
	}

	public void setLenderUser(UserResponse lenderUser) {
		this.lenderUser = lenderUser;
	}

	public UserResponse getBorrowerUser() {
		return borrowerUser;
	}

	public void setBorrowerUser(UserResponse borrowerUser) {
		this.borrowerUser = borrowerUser;
	}

	public double getPenalty() {
		return penalty;
	}

	public void setPenalty(double penalty) {
		this.penalty = penalty;
	}

	public List<LoanEmiCardPaymentDetailsResponseDto> getLoanPaymenHistory() {
		return loanPaymenHistory;
	}

	public void setLoanPaymenHistory(List<LoanEmiCardPaymentDetailsResponseDto> loanPaymenHistory) {
		this.loanPaymenHistory = loanPaymenHistory;
	}

	public String getEmiStatusBasedOnPayment() {
		return emiStatusBasedOnPayment;
	}

	public void setEmiStatusBasedOnPayment(String emiStatusBasedOnPayment) {
		this.emiStatusBasedOnPayment = emiStatusBasedOnPayment;
	}

	public double getEmiBalanceAmount() {
		return EmiBalanceAmount;
	}

	public void setEmiBalanceAmount(double emiBalanceAmount) {
		EmiBalanceAmount = emiBalanceAmount;
	}

	public double getTransferedPreviousEmiBalanceAmt() {
		return transferedPreviousEmiBalanceAmt;
	}

	public void setTransferedPreviousEmiBalanceAmt(double transferedPreviousEmiBalanceAmt) {
		this.transferedPreviousEmiBalanceAmt = transferedPreviousEmiBalanceAmt;
	}

	public double getRemainingEmiAmount() {
		return remainingEmiAmount;
	}

	public void setRemainingEmiAmount(double remainingEmiAmount) {
		this.remainingEmiAmount = remainingEmiAmount;
	}

	public double getTotalPartialPaymentAmount() {
		return totalPartialPaymentAmount;
	}

	public void setTotalPartialPaymentAmount(double totalPartialPaymentAmount) {
		this.totalPartialPaymentAmount = totalPartialPaymentAmount;
	}

	public double getExcessOfEmiAmount() {
		return excessOfEmiAmount;
	}

	public void setExcessOfEmiAmount(double excessOfEmiAmount) {
		this.excessOfEmiAmount = excessOfEmiAmount;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public double getEmiAmountAfterExcessAmountRemoved() {
		return emiAmountAfterExcessAmountRemoved;
	}

	public void setEmiAmountAfterExcessAmountRemoved(double emiAmountAfterExcessAmountRemoved) {
		this.emiAmountAfterExcessAmountRemoved = emiAmountAfterExcessAmountRemoved;
	}

	public double getRemainingPenaltyAndRemainingEmi() {
		return remainingPenaltyAndRemainingEmi;
	}

	public void setRemainingPenaltyAndRemainingEmi(double remainingPenaltyAndRemainingEmi) {
		this.remainingPenaltyAndRemainingEmi = remainingPenaltyAndRemainingEmi;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public List<LoanEmiCardResponseDto> getGetApplicationLevelPenalityWithEmi() {
		return getApplicationLevelPenalityWithEmi;
	}

	public void setGetApplicationLevelPenalityWithEmi(List<LoanEmiCardResponseDto> getApplicationLevelPenalityWithEmi) {
		this.getApplicationLevelPenalityWithEmi = getApplicationLevelPenalityWithEmi;
	}

}
