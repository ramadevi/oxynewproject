package com.oxyloans.dto.serviceloan;

import java.util.Date;

public class StatementPdfResponseDto extends StatementPdfRequestDto {

	private Integer loanId;

	private Date disbursmentDate;

	private double disbursmentAmount;

	private String accountNumber;

	private Integer rateOfInterest;

	private Integer installmentPaidNumber;

	private double installmentPaidAmount;

	private Integer installmentPendingNumber;

	private double installmentPendingAmount;

	private Integer futureInstallmentNumber;

	private Integer futureInstallmentAmount;

	private String disburmentAmountDetails;

	private String loanEmiCardDetails;

	private String emiStartDateAndEmiEndDate;

	private String emiTable;

	private String borrowerDealStructureEmiTable;

	private String borrowerDisburmentDate;

	private String lenderDealStructureEmiTable;

	public String getLenderDealStructureEmiTable() {
		return lenderDealStructureEmiTable;
	}

	public void setLenderDealStructureEmiTable(String lenderDealStructureEmiTable) {
		this.lenderDealStructureEmiTable = lenderDealStructureEmiTable;
	}

	public String getBorrowerDisburmentDate() {
		return borrowerDisburmentDate;
	}

	public void setBorrowerDisburmentDate(String borrowerDisburmentDate) {
		this.borrowerDisburmentDate = borrowerDisburmentDate;
	}

	public String getBorrowerDealStructureEmiTable() {
		return borrowerDealStructureEmiTable;
	}

	public void setBorrowerDealStructureEmiTable(String borrowerDealStructureEmiTable) {
		this.borrowerDealStructureEmiTable = borrowerDealStructureEmiTable;
	}

	public String getEmiTable() {
		return emiTable;
	}

	public void setEmiTable(String emiTable) {
		this.emiTable = emiTable;
	}

	public String getEmiStartDateAndEmiEndDate() {
		return emiStartDateAndEmiEndDate;
	}

	public void setEmiStartDateAndEmiEndDate(String emiStartDateAndEmiEndDate) {
		this.emiStartDateAndEmiEndDate = emiStartDateAndEmiEndDate;
	}

	public String getLoanEmiCardDetails() {
		return loanEmiCardDetails;
	}

	public void setLoanEmiCardDetails(String loanEmiCardDetails) {
		this.loanEmiCardDetails = loanEmiCardDetails;
	}

	public String getDisburmentAmountDetails() {
		return disburmentAmountDetails;
	}

	public void setDisburmentAmountDetails(String disburmentAmountDetails) {
		this.disburmentAmountDetails = disburmentAmountDetails;
	}

	public Integer getLoanId() {
		return loanId;
	}

	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}

	public Date getDisbursmentDate() {
		return disbursmentDate;
	}

	public void setDisbursmentDate(Date disbursmentDate) {
		this.disbursmentDate = disbursmentDate;
	}

	public double getDisbursmentAmount() {
		return disbursmentAmount;
	}

	public void setDisbursmentAmount(double disbursmentAmount) {
		this.disbursmentAmount = disbursmentAmount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Integer getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Integer rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public Integer getInstallmentPaidNumber() {
		return installmentPaidNumber;
	}

	public void setInstallmentPaidNumber(Integer installmentPaidNumber) {
		this.installmentPaidNumber = installmentPaidNumber;
	}

	public double getInstallmentPaidAmount() {
		return installmentPaidAmount;
	}

	public void setInstallmentPaidAmount(double installmentPaidAmount) {
		this.installmentPaidAmount = installmentPaidAmount;
	}

	public Integer getInstallmentPendingNumber() {
		return installmentPendingNumber;
	}

	public void setInstallmentPendingNumber(Integer installmentPendingNumber) {
		this.installmentPendingNumber = installmentPendingNumber;
	}

	public double getInstallmentPendingAmount() {
		return installmentPendingAmount;
	}

	public void setInstallmentPendingAmount(double installmentPendingAmount) {
		this.installmentPendingAmount = installmentPendingAmount;
	}

	public Integer getFutureInstallmentNumber() {
		return futureInstallmentNumber;
	}

	public void setFutureInstallmentNumber(Integer futureInstallmentNumber) {
		this.futureInstallmentNumber = futureInstallmentNumber;
	}

	public Integer getFutureInstallmentAmount() {
		return futureInstallmentAmount;
	}

	public void setFutureInstallmentAmount(Integer futureInstallmentAmount) {
		this.futureInstallmentAmount = futureInstallmentAmount;
	}

}
