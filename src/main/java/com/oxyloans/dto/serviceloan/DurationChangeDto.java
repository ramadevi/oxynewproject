package com.oxyloans.dto.serviceloan;

public class DurationChangeDto {
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
