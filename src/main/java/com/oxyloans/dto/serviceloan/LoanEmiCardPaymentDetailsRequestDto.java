package com.oxyloans.dto.serviceloan;

import java.util.Date;

public class LoanEmiCardPaymentDetailsRequestDto {

	private String applicationId;

	private Integer emiId;

	private String loanId;

	private String modeOfPayment;

	private String transactionReferenceNumber;

	private String paymentStatus;

	private double partialPaymentAmount;

	private double penalty;

	private double remainingEmiAmount;

	private String penaltyDate;

	private String amountPaidDate;

	private String updatedUserName;

	private Integer applicationIdNew;

	public String getUpdatedUserName() {
		return updatedUserName;
	}

	public void setUpdatedUserName(String updatedUserName) {
		this.updatedUserName = updatedUserName;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getEmiId() {
		return emiId;
	}

	public void setEmiId(Integer emiId) {
		this.emiId = emiId;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getTransactionReferenceNumber() {
		return transactionReferenceNumber;
	}

	public void setTransactionReferenceNumber(String transactionReferenceNumber) {
		this.transactionReferenceNumber = transactionReferenceNumber;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public double getPartialPaymentAmount() {
		return partialPaymentAmount;
	}

	public void setPartialPaymentAmount(double partialPaymentAmount) {
		this.partialPaymentAmount = partialPaymentAmount;
	}

	public double getPenalty() {
		return penalty;
	}

	public void setPenalty(double penalty) {
		this.penalty = penalty;
	}

	public double getRemainingEmiAmount() {
		return remainingEmiAmount;
	}

	public void setRemainingEmiAmount(double remainingEmiAmount) {
		this.remainingEmiAmount = remainingEmiAmount;
	}

	public String getPenaltyDate() {
		return penaltyDate;
	}

	public void setPenaltyDate(String penaltyDate) {
		this.penaltyDate = penaltyDate;
	}

	public String getAmountPaidDate() {
		return amountPaidDate;
	}

	public void setAmountPaidDate(String amountPaidDate) {
		this.amountPaidDate = amountPaidDate;
	}

	public Integer getApplicationIdNew() {
		return applicationIdNew;
	}

	public void setApplicationIdNew(Integer applicationIdNew) {
		this.applicationIdNew = applicationIdNew;
	}

}
