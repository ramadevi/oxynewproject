package com.oxyloans.dto.serviceloan;

import com.oxyloans.response.user.UserResponse;

public class ApplicationResponseDto {

	private int id;

	private double disbursmentAmount;

	private int borrowerUserId;

	private String disbursedDate;

	private String parentId;

	private String applicationId;

	private UserResponse borrowerUser;

	private double emiAmount;

	private double penaltyAmount;

	private double loanRequestAmount;

	private double loanOfferedAmount;

	private double currentApplicationAmountPaid;

	private double currentApplicationAmountNotPaid;

	private int currentemiNotPaid;

	private String emiStartDate;

	private double toatlOutStndingAmount;

	private double applicationAmountPaid;

	private double applicationAmountNotPaid;

	private int duration;

	private double featureMonthEMiAmount;

	private int noOfFeatureMOnth;

	private int emiNotPaid;

	public double getCurrentApplicationAmountPaid() {
		return currentApplicationAmountPaid;
	}

	public void setCurrentApplicationAmountPaid(double currentApplicationAmountPaid) {
		this.currentApplicationAmountPaid = currentApplicationAmountPaid;
	}

	public double getCurrentApplicationAmountNotPaid() {
		return currentApplicationAmountNotPaid;
	}

	public void setCurrentApplicationAmountNotPaid(double currentApplicationAmountNotPaid) {
		this.currentApplicationAmountNotPaid = currentApplicationAmountNotPaid;
	}

	public int getCurrentemiNotPaid() {
		return currentemiNotPaid;
	}

	public void setCurrentemiNotPaid(int currentemiNotPaid) {
		this.currentemiNotPaid = currentemiNotPaid;
	}

	public String getEmiStartDate() {
		return emiStartDate;
	}

	public void setEmiStartDate(String emiStartDate) {
		this.emiStartDate = emiStartDate;
	}

	public double getToatlOutStndingAmount() {
		return toatlOutStndingAmount;
	}

	public void setToatlOutStndingAmount(double toatlOutStndingAmount) {
		this.toatlOutStndingAmount = toatlOutStndingAmount;
	}

	public double getApplicationAmountPaid() {
		return applicationAmountPaid;
	}

	public void setApplicationAmountPaid(double applicationAmountPaid) {
		this.applicationAmountPaid = applicationAmountPaid;
	}

	public double getApplicationAmountNotPaid() {
		return applicationAmountNotPaid;
	}

	public void setApplicationAmountNotPaid(double applicationAmountNotPaid) {
		this.applicationAmountNotPaid = applicationAmountNotPaid;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public double getFeatureMonthEMiAmount() {
		return featureMonthEMiAmount;
	}

	public void setFeatureMonthEMiAmount(double featureMonthEMiAmount) {
		this.featureMonthEMiAmount = featureMonthEMiAmount;
	}

	public int getNoOfFeatureMOnth() {
		return noOfFeatureMOnth;
	}

	public void setNoOfFeatureMOnth(int noOfFeatureMOnth) {
		this.noOfFeatureMOnth = noOfFeatureMOnth;
	}

	public int getEmiNotPaid() {
		return emiNotPaid;
	}

	public void setEmiNotPaid(int emiNotPaid) {
		this.emiNotPaid = emiNotPaid;
	}

	public double getLoanOfferedAmount() {
		return loanOfferedAmount;
	}

	public void setLoanOfferedAmount(double loanOfferedAmount) {
		this.loanOfferedAmount = loanOfferedAmount;
	}

	public double getLoanRequestAmount() {
		return loanRequestAmount;
	}

	public void setLoanRequestAmount(double loanRequestAmount) {
		this.loanRequestAmount = loanRequestAmount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(double emiAmount) {
		this.emiAmount = emiAmount;
	}

	public double getPenaltyAmount() {
		return penaltyAmount;
	}

	public void setPenaltyAmount(double penaltyAmount) {
		this.penaltyAmount = penaltyAmount;
	}

	public double getDisbursmentAmount() {
		return disbursmentAmount;
	}

	public void setDisbursmentAmount(double disbursmentAmount) {
		this.disbursmentAmount = disbursmentAmount;
	}

	public int getBorrowerUserId() {
		return borrowerUserId;
	}

	public void setBorrowerUserId(int borrowerUserId) {
		this.borrowerUserId = borrowerUserId;
	}

	public String getDisbursedDate() {
		return disbursedDate;
	}

	public void setDisbursedDate(String disbursedDate) {
		this.disbursedDate = disbursedDate;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public UserResponse getBorrowerUser() {
		return borrowerUser;
	}

	public void setBorrowerUser(UserResponse borrowerUser) {
		this.borrowerUser = borrowerUser;
	}

}
