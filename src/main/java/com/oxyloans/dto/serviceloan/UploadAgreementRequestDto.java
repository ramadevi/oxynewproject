package com.oxyloans.dto.serviceloan;

public class UploadAgreementRequestDto {
	
	private String eSignType = "AADHAR";

	private String status;

	private String errorMessage;
	
	private String otpValue;

	public String geteSignType() {
		return eSignType;
	}

	public void seteSignType(String eSignType) {
		this.eSignType = eSignType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getOtpValue() {
		return otpValue;
	}

	public void setOtpValue(String otpValue) {
		this.otpValue = otpValue;
	}

}
