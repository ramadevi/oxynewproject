package com.oxyloans.dto.serviceloan;

import com.oxyloans.response.user.AddressDetailsResponseDto;

/**
 * @author damodharreddy
 *
 */
public class OxyAccountDetailsDto implements OxyDashboardDetailsDto {

	private Double commitmentAmount = 0.0d;

	private Double amountDisbursed = 0.0d;

	private Double inProcessAmount = 0.0d;

	private Double outstandingAmount = 0.0d;

	private Integer noOfLoanRequests = 0;

	private Integer noOfLoanResponses = 0;

	private Integer noOfActiveLoans = 0;

	private Integer noOfDisbursedLoans = 0;

	private Integer noOfClosedLoans = 0;

	private Double closedLoansAmount = 0.0d;

	private Double activeLoansAmount = 0.0d;

	private Double principalReceived = 0d;

	private Double interestPaid = 0d;

	private Double totalTransactionFee = 0d;

	private int noOfFailedEmis = 0;

	private Integer oxyScore = 0;

	private Double lenderWalletAmount = 0.0d;

	private StatusResponseDto myTrackingStatus;

	private String registractionStatus;

	private Integer noOfApplications;
	
	private Integer noOfActiveApplications = 0;

	private Integer noOfDisbursedApplications = 0;

	private Integer noOfClosedApplications = 0;
	
	private String referrerLink;
	
	private Double remainingAmountToParticipateInDeals;
	
	private Double maximumAmount;

	public Integer getNoOfActiveApplications() {
		return noOfActiveApplications;
	}

	public void setNoOfActiveApplications(Integer noOfActiveApplications) {
		this.noOfActiveApplications = noOfActiveApplications;
	}

	public Integer getNoOfDisbursedApplications() {
		return noOfDisbursedApplications;
	}

	public void setNoOfDisbursedApplications(Integer noOfDisbursedApplications) {
		this.noOfDisbursedApplications = noOfDisbursedApplications;
	}

	public Integer getNoOfClosedApplications() {
		return noOfClosedApplications;
	}

	public void setNoOfClosedApplications(Integer noOfClosedApplications) {
		this.noOfClosedApplications = noOfClosedApplications;
	}

	public Integer getNoOfApplications() {
		return noOfApplications;
	}

	public void setNoOfApplications(Integer noOfApplications) {
		this.noOfApplications = noOfApplications;
	}

	public Double getLenderWalletAmount() {
		return lenderWalletAmount;
	}

	public void setLenderWalletAmount(Double lenderWalletAmount) {
		this.lenderWalletAmount = lenderWalletAmount;
	}

	private Double loanOfferedAmount = 0.0d;

	public Double getLoanOfferedAmount() {
		return loanOfferedAmount;
	}

	public void setLoanOfferedAmount(Double loanOfferedAmount) {
		this.loanOfferedAmount = loanOfferedAmount;
	}

	public Integer getOxyScore() {
		return oxyScore;
	}

	public void setOxyScore(Integer oxyScore) {
		this.oxyScore = oxyScore;
	}

	public Double getCommitmentAmount() {
		return commitmentAmount;
	}

	public void setCommitmentAmount(Double commitmentAmount) {
		this.commitmentAmount = commitmentAmount;
	}

	public Double getAmountDisbursed() {
		return amountDisbursed;
	}

	public void setAmountDisbursed(Double amountDisbursed) {
		this.amountDisbursed = amountDisbursed;
	}

	public Double getOutstandingAmount() {
		return outstandingAmount;
	}

	public void setOutstandingAmount(Double outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}

	public Integer getNoOfLoanRequests() {
		return noOfLoanRequests;
	}

	public void setNoOfLoanRequests(Integer noOfLoanRequests) {
		this.noOfLoanRequests = noOfLoanRequests;
	}

	public Integer getNoOfActiveLoans() {
		return noOfActiveLoans;
	}

	public void setNoOfActiveLoans(Integer noOfActiveLoans) {
		this.noOfActiveLoans = noOfActiveLoans;
	}

	public Integer getNoOfDisbursedLoans() {
		return noOfDisbursedLoans;
	}

	public void setNoOfDisbursedLoans(Integer noOfDisbursedLoans) {
		this.noOfDisbursedLoans = noOfDisbursedLoans;
	}

	public Integer getNoOfClosedLoans() {
		return noOfClosedLoans;
	}

	public void setNoOfClosedLoans(Integer noOfClosedLoans) {
		this.noOfClosedLoans = noOfClosedLoans;
	}

	public Double getPrincipalReceived() {
		return principalReceived;
	}

	public void setPrincipalReceived(Double principalReceived) {
		this.principalReceived = principalReceived;
	}

	public Double getInterestPaid() {
		return interestPaid;
	}

	public void setInterestPaid(Double interestPaid) {
		this.interestPaid = interestPaid;
	}

	public Double getTotalTransactionFee() {
		return totalTransactionFee;
	}

	public void setTotalTransactionFee(Double totalTransactionFee) {
		this.totalTransactionFee = totalTransactionFee;
	}

	public int getNoOfFailedEmis() {
		return noOfFailedEmis;
	}

	public void setNoOfFailedEmis(int noOfFailedEmis) {
		this.noOfFailedEmis = noOfFailedEmis;
	}

	public Integer getNoOfLoanResponses() {
		return noOfLoanResponses;
	}

	public void setNoOfLoanResponses(Integer noOfLoanResponses) {
		this.noOfLoanResponses = noOfLoanResponses;
	}

	public Double getClosedLoansAmount() {
		return closedLoansAmount;
	}

	public void setClosedLoansAmount(Double closedLoansAmount) {
		this.closedLoansAmount = closedLoansAmount;
	}

	public Double getInProcessAmount() {
		return inProcessAmount;
	}

	public void setInProcessAmount(Double inProcessAmount) {
		this.inProcessAmount = inProcessAmount;
	}

	public Double getActiveLoansAmount() {
		return activeLoansAmount;
	}

	public void setActiveLoansAmount(Double activeLoansAmount) {
		this.activeLoansAmount = activeLoansAmount;
	}

	public StatusResponseDto getMyTrackingStatus() {
		return myTrackingStatus;
	}

	public void setMyTrackingStatus(StatusResponseDto myTrackingStatus) {
		this.myTrackingStatus = myTrackingStatus;
	}

	public String getRegistractionStatus() {
		return registractionStatus;
	}

	public void setRegistractionStatus(String registractionStatus) {
		this.registractionStatus = registractionStatus;
	}

	public String getReferrerLink() {
		return referrerLink;
	}

	public void setReferrerLink(String referrerLink) {
		this.referrerLink = referrerLink;
	}

	public Double getRemainingAmountToParticipateInDeals() {
		return remainingAmountToParticipateInDeals;
	}

	public void setRemainingAmountToParticipateInDeals(Double remainingAmountToParticipateInDeals) {
		this.remainingAmountToParticipateInDeals = remainingAmountToParticipateInDeals;
	}

	public Double getMaximumAmount() {
		return maximumAmount;
	}

	public void setMaximumAmount(Double maximumAmount) {
		this.maximumAmount = maximumAmount;
	}

}
