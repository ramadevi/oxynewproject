package com.oxyloans.dto.serviceloan;

import java.util.List;

public class TotalLendersInformationResponseDto {
	private List<UsersLoansInformationResponseDto> results;

	private Integer totalCount;

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public List<UsersLoansInformationResponseDto> getResults() {
		return results;
	}

	public void setResults(List<UsersLoansInformationResponseDto> results) {
		this.results = results;
	}

}
