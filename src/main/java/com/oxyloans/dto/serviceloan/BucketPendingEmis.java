package com.oxyloans.dto.serviceloan;

public class BucketPendingEmis {
	
//B.emi_amount,B.emi_due_on,loan_id,now()\\:\\:date - B.emi_due_on\\:\\:date as diff_days,B.emi_number
	
	private double emiAmount;
	
	private String emiDueOn;
	
	private int loanId;
	
	private int daysDiff;
	
	private int emiNumber;

	public double getEmiAmount() {
		return emiAmount;
	}

	public String getEmiDueOn() {
		return emiDueOn;
	}

	public int getLoanId() {
		return loanId;
	}

	public int getDaysDiff() {
		return daysDiff;
	}

	public int getEmiNumber() {
		return emiNumber;
	}

	public void setEmiAmount(double emiAmount) {
		this.emiAmount = emiAmount;
	}

	public void setEmiDueOn(String emiDueOn) {
		this.emiDueOn = emiDueOn;
	}

	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}

	public void setDaysDiff(int daysDiff) {
		this.daysDiff = daysDiff;
	}

	public void setEmiNumber(int emiNumber) {
		this.emiNumber = emiNumber;
	}
	
	
	
	

}
