package com.oxyloans.dto.serviceloan;

public class StatusResponseDtoForVanGeneration {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
