package com.oxyloans.dto.serviceloan;

public class ProfileRiskCalculationDto {

	private int creditScoreValue;
	private int salaryScore;
	private int companyScore;
	private int experienceScore;
	private char grade;

	public int getCreditScoreValue() {
		return creditScoreValue;
	}

	public void setCreditScoreValue(int creditScoreValue) {
		this.creditScoreValue = creditScoreValue;
	}

	public int getSalaryScore() {
		return salaryScore;
	}

	public void setSalaryScore(int salaryScore) {
		this.salaryScore = salaryScore;
	}

	public int getCompanyScore() {
		return companyScore;
	}

	public void setCompanyScore(int companyScore) {
		this.companyScore = companyScore;
	}

	public int getExperienceScore() {
		return experienceScore;
	}

	public void setExperienceScore(int experienceScore) {
		this.experienceScore = experienceScore;
	}

	public char getGrade() {
		return grade;
	}

	public void setGrade(char grade) {
		this.grade = grade;
	}

}
