package com.oxyloans.dto.serviceloan;

public class AdminDashboardDetails extends OxyAccountDetailsDto {

	private int registeredUsersCount;

	private int lendersCount;

	private int borrowersCount;

	private int todayRegisteredUsersCount;

	private double borrowersRequestedAmount;

	private double lendersCommitedAmount;

	private int noOfAggrements;

	private int noOfConversationRequests;

	private double totalOfferAmount;

	private double totalDisbursedAmount;

	private int todayRegisteredPartnersCount;

	private int totalRegisteredPartners;

	private int todayRegisteredBorrowers;

	private int todayRegisteredLenders;

	public double getTotalDisbursedAmount() {
		return totalDisbursedAmount;
	}

	public void setTotalDisbursedAmount(double totalDisbursedAmount) {
		this.totalDisbursedAmount = totalDisbursedAmount;
	}

	public double getTotalOfferAmount() {
		return totalOfferAmount;
	}

	public void setTotalOfferAmount(double totalOfferAmount) {
		this.totalOfferAmount = totalOfferAmount;
	}

	public int getRegisteredUsersCount() {
		return registeredUsersCount;
	}

	public void setRegisteredUsersCount(int registeredUsersCount) {
		this.registeredUsersCount = registeredUsersCount;
	}

	public int getLendersCount() {
		return lendersCount;
	}

	public void setLendersCount(int lendersCount) {
		this.lendersCount = lendersCount;
	}

	public int getBorrowersCount() {
		return borrowersCount;
	}

	public void setBorrowersCount(int borrowersCount) {
		this.borrowersCount = borrowersCount;
	}

	public int getTodayRegisteredUsersCount() {
		return todayRegisteredUsersCount;
	}

	public void setTodayRegisteredUsersCount(int todayRegisteredUsersCount) {
		this.todayRegisteredUsersCount = todayRegisteredUsersCount;
	}

	public double getBorrowersRequestedAmount() {
		return borrowersRequestedAmount;
	}

	public void setBorrowersRequestedAmount(double borrowersRequestedAmount) {
		this.borrowersRequestedAmount = borrowersRequestedAmount;
	}

	public double getLendersCommitedAmount() {
		return lendersCommitedAmount;
	}

	public void setLendersCommitedAmount(double lendersCommitedAmount) {
		this.lendersCommitedAmount = lendersCommitedAmount;
	}

	public int getNoOfAggrements() {
		return noOfAggrements;
	}

	public void setNoOfAggrements(int noOfAggrements) {
		this.noOfAggrements = noOfAggrements;
	}

	public int getNoOfConversationRequests() {
		return noOfConversationRequests;
	}

	public void setNoOfConversationRequests(int noOfConversationRequests) {
		this.noOfConversationRequests = noOfConversationRequests;
	}

	public int getTodayRegisteredPartnersCount() {
		return todayRegisteredPartnersCount;
	}

	public void setTodayRegisteredPartnersCount(int todayRegisteredPartnersCount) {
		this.todayRegisteredPartnersCount = todayRegisteredPartnersCount;
	}

	public int getTotalRegisteredPartners() {
		return totalRegisteredPartners;
	}

	public void setTotalRegisteredPartners(int totalRegisteredPartners) {
		this.totalRegisteredPartners = totalRegisteredPartners;
	}

	public int getTodayRegisteredBorrowers() {
		return todayRegisteredBorrowers;
	}

	public void setTodayRegisteredBorrowers(int todayRegisteredBorrowers) {
		this.todayRegisteredBorrowers = todayRegisteredBorrowers;
	}

	public int getTodayRegisteredLenders() {
		return todayRegisteredLenders;
	}

	public void setTodayRegisteredLenders(int todayRegisteredLenders) {
		this.todayRegisteredLenders = todayRegisteredLenders;
	}

}
