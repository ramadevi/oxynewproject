package com.oxyloans.dto.serviceloan;

import java.util.List;

import com.oxyloans.response.user.BorrowerLoansResponse;

public class BorrowerIndividualLoansCountResposeDto {

	private List<UsersLoansInformationResponseDto> userLoansInfoList;

	private int borrowerId;

	private int count;
	private int applicationId;
	private Double disbursmentAmount;
	private Double rateOfInterest;
	private int duration;
	private String durationType;
	private String borrowerName;
	private Double amountToBeDisbursed;
	private Double amountDisbursed;

	public List<BorrowerLoansResponse> borrowersLoansResponse;

	public List<BorrowerLoansResponse> getBorrowersLoansResponse() {
		return borrowersLoansResponse;
	}

	public void setBorrowersLoansResponse(List<BorrowerLoansResponse> borrowersLoansResponse) {
		this.borrowersLoansResponse = borrowersLoansResponse;
	}

	public List<UsersLoansInformationResponseDto> getUserLoansInfoList() {
		return userLoansInfoList;
	}

	public void setUserLoansInfoList(List<UsersLoansInformationResponseDto> userLoansInfoList) {
		this.userLoansInfoList = userLoansInfoList;
	}

	public int getCount() {
		return count;
	}

	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public Double getDisbursmentAmount() {
		return disbursmentAmount;
	}

	public void setDisbursmentAmount(Double disbursmentAmount) {
		this.disbursmentAmount = disbursmentAmount;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getDurationType() {
		return durationType;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public int getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(int borrowerId) {
		this.borrowerId = borrowerId;
	}

	public Double getAmountToBeDisbursed() {
		return amountToBeDisbursed;
	}

	public void setAmountToBeDisbursed(Double amountToBeDisbursed) {
		this.amountToBeDisbursed = amountToBeDisbursed;
	}

	public Double getAmountDisbursed() {
		return amountDisbursed;
	}

	public void setAmountDisbursed(Double amountDisbursed) {
		this.amountDisbursed = amountDisbursed;
	}

}
