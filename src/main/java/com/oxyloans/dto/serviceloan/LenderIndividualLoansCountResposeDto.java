package com.oxyloans.dto.serviceloan;

import java.util.List;

public class LenderIndividualLoansCountResposeDto {

	private List<UsersLoansInformationResponseDto> userLoansInfoList;

	private int count;

	public List<UsersLoansInformationResponseDto> getUserLoansInfoList() {
		return userLoansInfoList;
	}

	public void setUserLoansInfoList(List<UsersLoansInformationResponseDto> userLoansInfoList) {
		this.userLoansInfoList = userLoansInfoList;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
