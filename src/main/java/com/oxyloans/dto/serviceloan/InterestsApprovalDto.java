package com.oxyloans.dto.serviceloan;

public class InterestsApprovalDto {

	private int sno;
	private int userId;
	private String lenderName;
	private String participatedDate;
	private String groupName;
	private String paymentMethod;
	private Double participatedAmount;
	private int rateOfInterest;
	private Double interestAmount;
	private String paymentStatus;
	private String loanStatus;
	private String approvalStatus;
	private String bankAccountNumber;
	private String ifscCode;
	private String nameAsPerBank;
	private String bankName;
	private String branchName;
	private String remarks;
	private int daysDifference;
	private String paymentsAdminApproval = "";
	private String SubbuAdminApproval = "";
	private String superAdminApproval = "";
	private Double roi;
	private Double currentParticipationAmount;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public String getParticipatedDate() {
		return participatedDate;
	}

	public void setParticipatedDate(String participatedDate) {
		this.participatedDate = participatedDate;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Double getParticipatedAmount() {
		return participatedAmount;
	}

	public void setParticipatedAmount(Double participatedAmount) {
		this.participatedAmount = participatedAmount;
	}

	public Double getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(Double interestAmount) {
		this.interestAmount = interestAmount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getLoanStatus() {
		return loanStatus;
	}

	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}

	public String getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getNameAsPerBank() {
		return nameAsPerBank;
	}

	public void setNameAsPerBank(String nameAsPerBank) {
		this.nameAsPerBank = nameAsPerBank;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public int getSno() {
		return sno;
	}

	public void setSno(int sno) {
		this.sno = sno;
	}

	public int getDaysDifference() {
		return daysDifference;
	}

	public void setDaysDifference(int daysDifference) {
		this.daysDifference = daysDifference;
	}

	public String getPaymentsAdminApproval() {
		return paymentsAdminApproval;
	}

	public void setPaymentsAdminApproval(String paymentsAdminApproval) {
		this.paymentsAdminApproval = paymentsAdminApproval;
	}

	public String getSubbuAdminApproval() {
		return SubbuAdminApproval;
	}

	public void setSubbuAdminApproval(String subbuAdminApproval) {
		SubbuAdminApproval = subbuAdminApproval;
	}

	public String getSuperAdminApproval() {
		return superAdminApproval;
	}

	public void setSuperAdminApproval(String superAdminApproval) {
		this.superAdminApproval = superAdminApproval;
	}

	public int getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(int rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public Double getRoi() {
		return roi;
	}

	public void setRoi(Double roi) {
		this.roi = roi;
	}

	public Double getCurrentParticipationAmount() {
		return currentParticipationAmount;
	}

	public void setCurrentParticipationAmount(Double currentParticipationAmount) {
		this.currentParticipationAmount = currentParticipationAmount;
	}

}
