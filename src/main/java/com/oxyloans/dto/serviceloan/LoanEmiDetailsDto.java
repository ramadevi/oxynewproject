package com.oxyloans.dto.serviceloan;

public class LoanEmiDetailsDto {

	private Integer id;
	private Integer loanId;
	private Integer emiNumber;
	private Double emiPrincipalAmount;
	private Double emiInterstAmount;
	private Double emiAmount;
	private String emiDueOn;
	private String emiPaidOn;
	private Double emiLateFeeCharges;
	private String modeOfPayment;
	private String transactionNumber;
	private Boolean isEmiProcessed;
	private String payuTransactionNumber;
	private String payuStatus;
	private String enachStatus;
	private String clntTxnRef;
	private String enachScheduledDate;
	private Double remainingEmiAmount;
	private String status;
	private Double remainingPenaltyAndRemainingEmi;
	private Double excessOfEmiAmount;
	private Double disbursmentAmount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLoanId() {
		return loanId;
	}

	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}

	public Integer getEmiNumber() {
		return emiNumber;
	}

	public void setEmiNumber(Integer emiNumber) {
		this.emiNumber = emiNumber;
	}

	public Double getEmiPrincipalAmount() {
		return emiPrincipalAmount;
	}

	public void setEmiPrincipalAmount(Double emiPrincipalAmount) {
		this.emiPrincipalAmount = emiPrincipalAmount;
	}

	public Double getEmiInterstAmount() {
		return emiInterstAmount;
	}

	public void setEmiInterstAmount(Double emiInterstAmount) {
		this.emiInterstAmount = emiInterstAmount;
	}

	public Double getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(Double emiAmount) {
		this.emiAmount = emiAmount;
	}

	public String getEmiDueOn() {
		return emiDueOn;
	}

	public void setEmiDueOn(String emiDueOn) {
		this.emiDueOn = emiDueOn;
	}

	public String getEmiPaidOn() {
		return emiPaidOn;
	}

	public void setEmiPaidOn(String emiPaidOn) {
		this.emiPaidOn = emiPaidOn;
	}

	public Double getEmiLateFeeCharges() {
		return emiLateFeeCharges;
	}

	public void setEmiLateFeeCharges(Double emiLateFeeCharges) {
		this.emiLateFeeCharges = emiLateFeeCharges;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public Boolean getIsEmiProcessed() {
		return isEmiProcessed;
	}

	public void setIsEmiProcessed(Boolean isEmiProcessed) {
		this.isEmiProcessed = isEmiProcessed;
	}

	public String getPayuTransactionNumber() {
		return payuTransactionNumber;
	}

	public void setPayuTransactionNumber(String payuTransactionNumber) {
		this.payuTransactionNumber = payuTransactionNumber;
	}

	public String getPayuStatus() {
		return payuStatus;
	}

	public void setPayuStatus(String payuStatus) {
		this.payuStatus = payuStatus;
	}

	public String getEnachStatus() {
		return enachStatus;
	}

	public void setEnachStatus(String enachStatus) {
		this.enachStatus = enachStatus;
	}

	public String getClntTxnRef() {
		return clntTxnRef;
	}

	public void setClntTxnRef(String clntTxnRef) {
		this.clntTxnRef = clntTxnRef;
	}

	public String getEnachScheduledDate() {
		return enachScheduledDate;
	}

	public void setEnachScheduledDate(String enachScheduledDate) {
		this.enachScheduledDate = enachScheduledDate;
	}

	public Double getRemainingEmiAmount() {
		return remainingEmiAmount;
	}

	public void setRemainingEmiAmount(Double remainingEmiAmount) {
		this.remainingEmiAmount = remainingEmiAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getRemainingPenaltyAndRemainingEmi() {
		return remainingPenaltyAndRemainingEmi;
	}

	public void setRemainingPenaltyAndRemainingEmi(Double remainingPenaltyAndRemainingEmi) {
		this.remainingPenaltyAndRemainingEmi = remainingPenaltyAndRemainingEmi;
	}

	public Double getExcessOfEmiAmount() {
		return excessOfEmiAmount;
	}

	public void setExcessOfEmiAmount(Double excessOfEmiAmount) {
		this.excessOfEmiAmount = excessOfEmiAmount;
	}

	public Double getDisbursmentAmount() {
		return disbursmentAmount;
	}

	public void setDisbursmentAmount(Double disbursmentAmount) {
		this.disbursmentAmount = disbursmentAmount;
	}

}
