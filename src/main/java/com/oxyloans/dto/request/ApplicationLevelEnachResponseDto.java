package com.oxyloans.dto.request;

public class ApplicationLevelEnachResponseDto {

	private String name;
	
	private String applicationId;
	
	private Double amountToActivate;
	
	private Double rateOfInterest;
	
	private Integer duration;
	
	private String status;
	
	private String type;
	
	private String enachMandateStatus;
	
	private Integer id;
	
	private boolean enachActivatedStatus;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public Double getAmountToActivate() {
		return amountToActivate;
	}

	public void setAmountToActivate(Double amountToActivate) {
		this.amountToActivate = amountToActivate;
	}


	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getEnachMandateStatus() {
		return enachMandateStatus;
	}

	public void setEnachMandateStatus(String enachMandateStatus) {
		this.enachMandateStatus = enachMandateStatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isEnachActivatedStatus() {
		return enachActivatedStatus;
	}

	public void setEnachActivatedStatus(boolean enachActivatedStatus) {
		this.enachActivatedStatus = enachActivatedStatus;
	}
	
	
}
