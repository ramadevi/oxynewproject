package com.oxyloans.dto.request;

import java.util.List;

public class BorrowerUserDetails {

	private String name;

	private String email;

	private String mobileNumber;

	private List<String> applicationNumber;

	private Integer noOfLoans;

	private Double sumOfAmountDisbursed;

	private Integer userId;

	private boolean requestSubmitedStatus;

	private Double requestedAmount;

	private Integer duration;

	private Double rateOfInterest;

	private String repaymentMethod;

	private String durationType;

	private String accountNumber;

	private String bankName;

	private String branchName;

	private String ifscCode;

	private String bankAddress;

	private String nameAccordingToBank;

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public List<String> getApplicationNumber() {
		return applicationNumber;
	}

	public Integer getNoOfLoans() {
		return noOfLoans;
	}

	public Double getSumOfAmountDisbursed() {
		return sumOfAmountDisbursed;
	}

	public Integer getUserId() {
		return userId;
	}

	public boolean isRequestSubmitedStatus() {
		return requestSubmitedStatus;
	}

	public Double getRequestedAmount() {
		return requestedAmount;
	}

	public Integer getDuration() {
		return duration;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public String getRepaymentMethod() {
		return repaymentMethod;
	}

	public String getDurationType() {
		return durationType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public String getNameAccordingToBank() {
		return nameAccordingToBank;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public void setApplicationNumber(List<String> applicationNumber) {
		this.applicationNumber = applicationNumber;
	}

	public void setNoOfLoans(Integer noOfLoans) {
		this.noOfLoans = noOfLoans;
	}

	public void setSumOfAmountDisbursed(Double sumOfAmountDisbursed) {
		this.sumOfAmountDisbursed = sumOfAmountDisbursed;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setRequestSubmitedStatus(boolean requestSubmitedStatus) {
		this.requestSubmitedStatus = requestSubmitedStatus;
	}

	public void setRequestedAmount(Double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public void setRepaymentMethod(String repaymentMethod) {
		this.repaymentMethod = repaymentMethod;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public void setNameAccordingToBank(String nameAccordingToBank) {
		this.nameAccordingToBank = nameAccordingToBank;
	}

}
