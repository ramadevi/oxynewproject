package com.oxyloans.dto.request;

import java.util.List;

public class SearchRequestDto {
	
	public static enum LogicalOperator {
		AND, OR, NOT
	}
	
	public static enum Operator {
		EQUALS, NOT_EQUALS, NULL, NOT_NULL, LIKE, NOT_LIKE, STARTS_WITH, GT, LT, GTE, LTE, IN, NOT_IN,ILIKE
	}
	
	public static enum SortOrder {
		ASC, DESC
	}
	
	private String fieldName;
	
	private Object fieldValue;
	
	private List<Object> fieldValues;
	
	private Operator operator;
	
	private LogicalOperator logicalOperator;
	
	private String sortBy;
	
	private SortOrder sortOrder = SortOrder.DESC;
	
	private PageDto page = new PageDto();
	
	private SearchRequestDto leftOperand;
	
	private SearchRequestDto rightOperand;
	
	private String searchType=null;
	

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public LogicalOperator getLogicalOperator() {
		return logicalOperator;
	}

	public void setLogicalOperator(LogicalOperator logicalOperator) {
		this.logicalOperator = logicalOperator;
	}

	public PageDto getPage() {
		return page;
	}

	public void setPage(PageDto page) {
		this.page = page;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public SearchRequestDto getLeftOperand() {
		return leftOperand;
	}

	public void setLeftOperand(SearchRequestDto leftOperand) {
		this.leftOperand = leftOperand;
	}

	public SearchRequestDto getRightOperand() {
		return rightOperand;
	}

	public void setRightOperand(SearchRequestDto rightOperand) {
		this.rightOperand = rightOperand;
	}

	public List<Object> getFieldValues() {
		return fieldValues;
	}

	public void setFieldValues(List<Object> fieldValues) {
		this.fieldValues = fieldValues;
	}

	public void setFieldValue(Object fieldValue) {
		this.fieldValue = fieldValue;
	}

	public Object getFieldValue() {
		return fieldValue;
	}
}
