package com.oxyloans.dto.request;

public class PartnerLoanInfoResponse {

	
	private String name;
	
	private String loanId;
	
	private Double rateOfInterest;
	
	private Double disbursmentAmount;
	
	private Integer duration;
	
	private String loanStatus;
	
	private Double sumOfEmiAmountToBePaid;
	
	private Double sumOfEmiAmountPaid;
	
	private Double sumOfEmiAmountDue;
	
	private Integer totalNoOfEmisToBePaidInFuture;
	
	private Integer totalNoOfEmisPaid;
	
	private Double sumOfEmiAmountToBePaidInFuture;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public Double getDisbursmentAmount() {
		return disbursmentAmount;
	}

	public void setDisbursmentAmount(Double disbursmentAmount) {
		this.disbursmentAmount = disbursmentAmount;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getLoanStatus() {
		return loanStatus;
	}

	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}

	public Double getSumOfEmiAmountToBePaid() {
		return sumOfEmiAmountToBePaid;
	}

	public void setSumOfEmiAmountToBePaid(Double sumOfEmiAmountToBePaid) {
		this.sumOfEmiAmountToBePaid = sumOfEmiAmountToBePaid;
	}

	public Double getSumOfEmiAmountPaid() {
		return sumOfEmiAmountPaid;
	}

	public void setSumOfEmiAmountPaid(Double sumOfEmiAmountPaid) {
		this.sumOfEmiAmountPaid = sumOfEmiAmountPaid;
	}

	public Double getSumOfEmiAmountDue() {
		return sumOfEmiAmountDue;
	}

	public void setSumOfEmiAmountDue(Double sumOfEmiAmountDue) {
		this.sumOfEmiAmountDue = sumOfEmiAmountDue;
	}

	public Integer getTotalNoOfEmisPaid() {
		return totalNoOfEmisPaid;
	}

	public void setTotalNoOfEmisPaid(Integer totalNoOfEmisPaid) {
		this.totalNoOfEmisPaid = totalNoOfEmisPaid;
	}

	public Integer getTotalNoOfEmisToBePaidInFuture() {
		return totalNoOfEmisToBePaidInFuture;
	}

	public void setTotalNoOfEmisToBePaidInFuture(Integer totalNoOfEmisToBePaidInFuture) {
		this.totalNoOfEmisToBePaidInFuture = totalNoOfEmisToBePaidInFuture;
	}

	public Double getSumOfEmiAmountToBePaidInFuture() {
		return sumOfEmiAmountToBePaidInFuture;
	}

	public void setSumOfEmiAmountToBePaidInFuture(Double sumOfEmiAmountToBePaidInFuture) {
		this.sumOfEmiAmountToBePaidInFuture = sumOfEmiAmountToBePaidInFuture;
	}
	
	
}
