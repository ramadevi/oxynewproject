package com.oxyloans.dto.request;

import com.oxyloans.icici.upi.QRCallBackResponseDto;

public class LenderUserDetails {

	private String name;

	private String email;

	private String mobileNumber;

	private Double amountLentByLender;

	private Integer userId;

	private String paidOnDate;

	private Integer walletAmount;

	private QRCallBackResponseDto callBackResponseDto;

	private String accountNumber;

	private String bankName;

	private String branchName;

	private String ifscCode;

	private String bankAddress;

	private String nameAccordingToBank;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Double getAmountLentByLender() {
		return amountLentByLender;
	}

	public void setAmountLentByLender(Double amountLentByLender) {
		this.amountLentByLender = amountLentByLender;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public QRCallBackResponseDto getCallBackResponseDto() {
		return callBackResponseDto;
	}

	public void setCallBackResponseDto(QRCallBackResponseDto callBackResponseDto) {
		this.callBackResponseDto = callBackResponseDto;
	}

	public String getPaidOnDate() {
		return paidOnDate;
	}

	public void setPaidOnDate(String paidOnDate) {
		this.paidOnDate = paidOnDate;
	}

	public Integer getWalletAmount() {
		return walletAmount;
	}

	public void setWalletAmount(Integer walletAmount) {
		this.walletAmount = walletAmount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public String getNameAccordingToBank() {
		return nameAccordingToBank;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public void setNameAccordingToBank(String nameAccordingToBank) {
		this.nameAccordingToBank = nameAccordingToBank;
	}
}
