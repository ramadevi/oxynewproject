package com.oxyloans.dto.request.user;

public class UserRequest {

	public static enum City {
		Hyderabad, Bangalore
	}

	private String email;

	private String password;

	private String confirmPassword;

	private String mobileNumber;

	private String type;

	private String mobileOtpValue;

	private transient boolean create = false;

	private double amountInterested;

	private Double longitude;

	private Double latitude;

	private Integer oxyScore;

	private String city;

	private Integer id;

	private String adminComments;

	private String loanId;

	private String disbursedDate;

	private String status;

	private String firstName;

	private String lastName;

	private String dob;

	private String pinCode;

	private String state;

	private String MobileOtpSession;

	private String emailOtp;

	private String gender;

	private Integer paymentFeeRateOfInterest;

	private Double loanAmountApprovedByAdmin;

	private String utmSource;

	private Integer roi;

	private String workExperience;

	private int salaryOrIncomePoints;

	private int CompanyOrOrganizationPoints;

	private int experianceOrExistenceOfOrganizationPoints;

	private int cibilScorePoints;

	private String grade;

	private String loanRequestId;

	private String loanOfferStatus;

	private String locality;

	private String uniqueNumber;

	private String citizenship;

	private boolean gmailAccountVerified;

	private String socialLoginUserId;

	private String whatsappNumber;

	private String whatsappOtp;

	private Boolean userExpertise;

	private String primaryType;

	private String documentDriveLink;

	private String userType;

	private String partnerUtmName;

	private String partnerPassword;

	private String partnerEmail;

	private String partnerMobileNumber;

	private String uuid;

	private String nameAsPerBank;

	private String ifscCode;

	private String accountNumber;

	private int borrowerId;

	private int dealId;

	private boolean borrowerFeePaid;

	private boolean isMailsShouldSend;

	private String projectType;

	public String getWhatsappNumber() {
		return whatsappNumber;
	}

	public void setWhatsappNumber(String whatsappNumber) {
		this.whatsappNumber = whatsappNumber;
	}

	public String getWhatsappOtp() {
		return whatsappOtp;
	}

	public void setWhatsappOtp(String whatsappOtp) {
		this.whatsappOtp = whatsappOtp;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getLoanOfferStatus() {
		return loanOfferStatus;
	}

	public void setLoanOfferStatus(String loanOfferStatus) {
		this.loanOfferStatus = loanOfferStatus;
	}

	public String getLoanRequestId() {
		return loanRequestId;
	}

	public void setLoanRequestId(String loanRequestId) {
		this.loanRequestId = loanRequestId;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public String getType() {
		return type;
	}

	public String getMobileOtpValue() {
		return mobileOtpValue;
	}

	public boolean isCreate() {
		return create;
	}

	public double getAmountInterested() {
		return amountInterested;
	}

	public Double getLongitude() {
		return longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Integer getOxyScore() {
		return oxyScore;
	}

	public String getCity() {
		return city;
	}

	public Integer getId() {
		return id;
	}

	public String getAdminComments() {
		return adminComments;
	}

	public String getLoanId() {
		return loanId;
	}

	public String getDisbursedDate() {
		return disbursedDate;
	}

	public String getStatus() {
		return status;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getDob() {
		return dob;
	}

	public String getPinCode() {
		return pinCode;
	}

	public String getState() {
		return state;
	}

	public String getMobileOtpSession() {
		return MobileOtpSession;
	}

	public String getEmailOtp() {
		return emailOtp;
	}

	public String getGender() {
		return gender;
	}

	public Integer getPaymentFeeRateOfInterest() {
		return paymentFeeRateOfInterest;
	}

	public Double getLoanAmountApprovedByAdmin() {
		return loanAmountApprovedByAdmin;
	}

	public String getUtmSource() {
		return utmSource;
	}

	public Integer getRoi() {
		return roi;
	}

	public String getWorkExperience() {
		return workExperience;
	}

	public int getSalaryOrIncomePoints() {
		return salaryOrIncomePoints;
	}

	public int getCompanyOrOrganizationPoints() {
		return CompanyOrOrganizationPoints;
	}

	public int getExperianceOrExistenceOfOrganizationPoints() {
		return experianceOrExistenceOfOrganizationPoints;
	}

	public int getCibilScorePoints() {
		return cibilScorePoints;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setMobileOtpValue(String mobileOtpValue) {
		this.mobileOtpValue = mobileOtpValue;
	}

	public void setCreate(boolean create) {
		this.create = create;
	}

	public void setAmountInterested(double amountInterested) {
		this.amountInterested = amountInterested;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public void setOxyScore(Integer oxyScore) {
		this.oxyScore = oxyScore;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public void setDisbursedDate(String disbursedDate) {
		this.disbursedDate = disbursedDate;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setMobileOtpSession(String mobileOtpSession) {
		MobileOtpSession = mobileOtpSession;
	}

	public void setEmailOtp(String emailOtp) {
		this.emailOtp = emailOtp;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setPaymentFeeRateOfInterest(Integer paymentFeeRateOfInterest) {
		this.paymentFeeRateOfInterest = paymentFeeRateOfInterest;
	}

	public void setLoanAmountApprovedByAdmin(Double loanAmountApprovedByAdmin) {
		this.loanAmountApprovedByAdmin = loanAmountApprovedByAdmin;
	}

	public void setUtmSource(String utmSource) {
		this.utmSource = utmSource;
	}

	public void setRoi(Integer roi) {
		this.roi = roi;
	}

	public void setWorkExperience(String workExperience) {
		this.workExperience = workExperience;
	}

	public void setSalaryOrIncomePoints(int salaryOrIncomePoints) {
		this.salaryOrIncomePoints = salaryOrIncomePoints;
	}

	public void setCompanyOrOrganizationPoints(int companyOrOrganizationPoints) {
		CompanyOrOrganizationPoints = companyOrOrganizationPoints;
	}

	public void setExperianceOrExistenceOfOrganizationPoints(int experianceOrExistenceOfOrganizationPoints) {
		this.experianceOrExistenceOfOrganizationPoints = experianceOrExistenceOfOrganizationPoints;
	}

	public void setCibilScorePoints(int cibilScorePoints) {
		this.cibilScorePoints = cibilScorePoints;
	}

	public String getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(String uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public boolean isGmailAccountVerified() {
		return gmailAccountVerified;
	}

	public void setGmailAccountVerified(boolean gmailAccountVerified) {
		this.gmailAccountVerified = gmailAccountVerified;
	}

	public String getSocialLoginUserId() {
		return socialLoginUserId;
	}

	public void setSocialLoginUserId(String socialLoginUserId) {
		this.socialLoginUserId = socialLoginUserId;
	}

	public Boolean getUserExpertise() {
		return userExpertise;
	}

	public void setUserExpertise(Boolean userExpertise) {
		this.userExpertise = userExpertise;
	}

	public String getPrimaryType() {
		return primaryType;
	}

	public void setPrimaryType(String primaryType) {
		this.primaryType = primaryType;
	}

	public String getDocumentDriveLink() {
		return documentDriveLink;
	}

	public void setDocumentDriveLink(String documentDriveLink) {
		this.documentDriveLink = documentDriveLink;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getPartnerUtmName() {
		return partnerUtmName;
	}

	public void setPartnerUtmName(String partnerUtmName) {
		this.partnerUtmName = partnerUtmName;
	}

	public String getPartnerPassword() {
		return partnerPassword;
	}

	public void setPartnerPassword(String partnerPassword) {
		this.partnerPassword = partnerPassword;
	}

	public String getPartnerEmail() {
		return partnerEmail;
	}

	public void setPartnerEmail(String partnerEmail) {
		this.partnerEmail = partnerEmail;
	}

	public String getPartnerMobileNumber() {
		return partnerMobileNumber;
	}

	public void setPartnerMobileNumber(String partnerMobileNumber) {
		this.partnerMobileNumber = partnerMobileNumber;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getNameAsPerBank() {
		return nameAsPerBank;
	}

	public void setNameAsPerBank(String nameAsPerBank) {
		this.nameAsPerBank = nameAsPerBank;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(int borrowerId) {
		this.borrowerId = borrowerId;
	}

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public boolean isBorrowerFeePaid() {
		return borrowerFeePaid;
	}

	public void setBorrowerFeePaid(boolean borrowerFeePaid) {
		this.borrowerFeePaid = borrowerFeePaid;
	}

	public boolean isMailsShouldSend() {
		return isMailsShouldSend;
	}

	public void setMailsShouldSend(boolean isMailsShouldSend) {
		this.isMailsShouldSend = isMailsShouldSend;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

}
