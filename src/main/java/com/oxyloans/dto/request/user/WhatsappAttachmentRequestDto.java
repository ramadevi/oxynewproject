package com.oxyloans.dto.request.user;

import java.io.InputStream;

public class WhatsappAttachmentRequestDto {


	private InputStream inputSream;;
	private String phone;
	private String caption;
	private String fileName;
	private String chatId;

	
	public InputStream getInputSream() {
		return inputSream;
	}

	public void setInputSream(InputStream inputSream) {
		this.inputSream = inputSream;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}

}
