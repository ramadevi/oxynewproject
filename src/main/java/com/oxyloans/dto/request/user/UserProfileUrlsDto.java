package com.oxyloans.dto.request.user;

public class UserProfileUrlsDto {
	
	private String faceBookUrl;
	
	private String linkdinUrl;;
	
	private String twitterUrl;

	public String getFaceBookUrl() {
		return faceBookUrl;
	}

	public String getLinkdinUrl() {
		return linkdinUrl;
	}

	public String getTwitterUrl() {
		return twitterUrl;
	}

	public void setFaceBookUrl(String faceBookUrl) {
		this.faceBookUrl = faceBookUrl;
	}

	public void setLinkdinUrl(String linkdinUrl) {
		this.linkdinUrl = linkdinUrl;
	}

	public void setTwitterUrl(String twitterUrl) {
		this.twitterUrl = twitterUrl;
	}

}
