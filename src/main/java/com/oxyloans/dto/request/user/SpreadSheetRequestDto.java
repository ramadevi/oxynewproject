package com.oxyloans.dto.request.user;

public class SpreadSheetRequestDto {
	private String gmailCode;
	private String spreadSheetId;
	private String range;
	private String rows;
	private String type;

	private String date;

	private String message;

	private String lastMessageNumber;

	private String mailSubject;

	private String fromMail;

	private String senderName;
	private Integer lenderId;
	
	private Long lenderId1;
	
	private String password;
	
	

	public Long getLenderId1() {
		return lenderId1;
	}

	public void setLenderId1(Long lenderId1) {
		this.lenderId1 = lenderId1;
	}

	public String getRows() {
		return rows;
	}

	public void setRows(String rows) {
		this.rows = rows;
	}

	public String getGmailCode() {
		return gmailCode;
	}

	public void setGmailCode(String gmailCode) {
		this.gmailCode = gmailCode;
	}

	public String getSpreadSheetId() {
		return spreadSheetId;
	}

	public void setSpreadSheetId(String spreadSheetId) {
		this.spreadSheetId = spreadSheetId;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLastMessageNumber() {
		return lastMessageNumber;
	}

	public void setLastMessageNumber(String lastMessageNumber) {
		this.lastMessageNumber = lastMessageNumber;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getFromMail() {
		return fromMail;
	}

	public void setFromMail(String fromMail) {
		this.fromMail = fromMail;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	
}
