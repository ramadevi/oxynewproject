package com.oxyloans.dto.request.user;

public class ReferenceDetailsResponseDto {

	private Integer userId;

	private String status;

	private String reference1;

	private String reference2;

	private String reference3;

	private String reference4;

	private String reference5;

	private String reference6;

	private String reference7;

	private String reference8;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReference1() {
		return reference1;
	}

	public void setReference1(String reference1) {
		this.reference1 = reference1;
	}

	public String getReference2() {
		return reference2;
	}

	public void setReference2(String reference2) {
		this.reference2 = reference2;
	}

	public String getReference3() {
		return reference3;
	}

	public void setReference3(String reference3) {
		this.reference3 = reference3;
	}

	public String getReference4() {
		return reference4;
	}

	public void setReference4(String reference4) {
		this.reference4 = reference4;
	}

	public String getReference5() {
		return reference5;
	}

	public void setReference5(String reference5) {
		this.reference5 = reference5;
	}

	public String getReference6() {
		return reference6;
	}

	public void setReference6(String reference6) {
		this.reference6 = reference6;
	}

	public String getReference7() {
		return reference7;
	}

	public void setReference7(String reference7) {
		this.reference7 = reference7;
	}

	public String getReference8() {
		return reference8;
	}

	public void setReference8(String reference8) {
		this.reference8 = reference8;
	}

}
