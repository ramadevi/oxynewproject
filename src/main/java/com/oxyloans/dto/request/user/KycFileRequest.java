
package com.oxyloans.dto.request.user;

import java.io.InputStream;

public class KycFileRequest {

	public static enum KycType {
		PAN, AADHAR, PASSPORT, PROFILEPIC, CONTACTS, BANKSTATEMENT, PAYSLIPS, DRIVINGLICENCE, VOTERID, TRANSACTIONSS,
		CICREPORT, LENDERREPORT, EMISDETAILS, EXPERIAN, CIBILSCORE, REPAYMENTACCOUNTDETAILS, PAYMENTSCREENSHOT,
		IDFCPAYMENTS, DISBURSEDLOANS, NORMALEMISPENDING, CRITICALEMISPENDING, LENDERINTEREST, LENDEREMI,
		LENDERPRINCIPAL, LENDERWITHDRAW, LENDERRELEND, LENDERPOOLINGINTEREST, USERQUERYSCREENSHOT, CHEQUELEAF,
		BULKINVITE, COMPANYPAN, MEMORANDUMOFASSOCIATION, CERTIFICATEOFINSURANCE, GST, TAN, COMPANYBANKSTMT, AOI, MAOA,
		GOVTIDPROOF, FINANCIALS, DEALERSHIPAGREEMENT, INVENTORYDTLS, PURCHASEORDER, GSTREGCERT, GSTFILLINGREC,
		FUNDINGCOMPANYPROFILE, OSBOOKINGS, SALESINFO, LOANBOOKS, SALESPROJECTIONS, PF, MONTHREVENUE, AUDITEDFINANCIALS,
		FACILITYAGREEMENTS, OPERATIONALMETRICS, REVENUEANDEXPENSES, POOLINGLENDERS, PANVIEDOKYC, AADHARVIEDOKYC,
		PARTNERNDA, PARTNERMOU, LENDERREFERRALBONUS, WHATSAPP, TENTH, INTER, GRADUATION, UNIVERSITYOFFERLETTER, FEE,
		BORROWERHDFCPAYMENT, BORROWERICICIPAYMENT;
	}

	private String fileName;

	private InputStream fileInputStream;

	private String base64EncodedStream;

	private KycType kycType;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public KycType getKycType() {
		return kycType;
	}

	public void setKycType(KycType kycType) {
		this.kycType = kycType;
	}

	public String getBase64EncodedStream() {
		return base64EncodedStream;
	}

	public void setBase64EncodedStream(String base64EncodedStream) {
		this.base64EncodedStream = base64EncodedStream;
	}

}
