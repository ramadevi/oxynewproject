package com.oxyloans.dto.request.user;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)

public class QueriesCountsResponse {
	
	@JsonProperty
	private int allQueriesCount;
	
	@JsonProperty
	private int resolvedCount;
	
	@JsonProperty
	private int cancelledCount;
	
	@JsonProperty
	private int pendingCount;

	public QueriesCountsResponse(int allQueriesCount, int resolvedCount, int cancelledCount, int pendingCount) {
		
		this.allQueriesCount = allQueriesCount;
		this.resolvedCount = resolvedCount;
		this.cancelledCount = cancelledCount;
		this.pendingCount = pendingCount;
	}

	@Override
	public String toString() {
		return "QueriesCountsResponse [allQueriesCount=" + allQueriesCount + ", resolvedCount=" + resolvedCount
				+ ", cancelledCount=" + cancelledCount + ", pendingCount=" + pendingCount + "]";
	}
	
	

}
