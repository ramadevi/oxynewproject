package com.oxyloans.dto.request.user;

public class WhatsappCampaignDto {

	private String message;

	private String whatsappGroupNames;

	private String messageStatus;

	private String phoneNumber;

	private String messageType;

	public String getWhatsappGroupNames() {
		return whatsappGroupNames;
	}

	public void setWhatsappGroupNames(String whatsappGroupNames) {
		this.whatsappGroupNames = whatsappGroupNames;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

}
