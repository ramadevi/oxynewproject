package com.oxyloans.dto.request.user;

import java.util.Date;

public class LenderReferenceRequestDto {

	private String email;

	private String mobileNumber;

	private Integer id;

	private Integer referrerId;

	private Integer refereeId;

	private String name;

	private String mailContent;

	private String mailSubject;

	private String comments;

	private String transferredOn;

	private String primaryType;

	private Date referredOn;

	private Double amount;

	private String status;

	private int seekerRequestedId;

	private String citizenType;

	private String userType;

	private String inviteType;
	private String userInvite;
	
	
	
	public String getUserInvite() {
		return userInvite;
	}

	public void setUserInvite(String userInvite) {
		this.userInvite = userInvite;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Integer getReferrerId() {
		return referrerId;
	}

	public void setReferrerId(Integer referrerId) {
		this.referrerId = referrerId;
	}

	public Integer getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(Integer refereeId) {
		this.refereeId = refereeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMailContent() {
		return mailContent;
	}

	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getTransferredOn() {
		return transferredOn;
	}

	public void setTransferredOn(String transferredOn) {
		this.transferredOn = transferredOn;
	}

	public String getPrimaryType() {
		return primaryType;
	}

	public void setPrimaryType(String primaryType) {
		this.primaryType = primaryType;
	}

	public Date getReferredOn() {
		return referredOn;
	}

	public void setReferredOn(Date referredOn) {
		this.referredOn = referredOn;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getSeekerRequestedId() {
		return seekerRequestedId;
	}

	public void setSeekerRequestedId(int seekerRequestedId) {
		this.seekerRequestedId = seekerRequestedId;
	}

	public String getCitizenType() {
		return citizenType;
	}

	public void setCitizenType(String citizenType) {
		this.citizenType = citizenType;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getInviteType() {
		return inviteType;
	}

	public void setInviteType(String inviteType) {
		this.inviteType = inviteType;
	}

}
