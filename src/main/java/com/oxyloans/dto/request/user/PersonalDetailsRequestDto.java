package com.oxyloans.dto.request.user;

import java.math.BigInteger;
import java.util.Date;

public class PersonalDetailsRequestDto {

	private Integer id;

	private String firstName;

	private String lastName;

	private String fatherName;

	private String dob;

	private String gender;

	private String maritalStatus;

	private String nationality;

	private String address;

	private Date createdAt;

	private Date modifiedAt;

	private String companyName;

	private Integer salary;

	private String panNumber;

	private String middleName;

	private String mobileNumber;

	private String pinCode;

	private String state;

	private String city;

	private String permanentAddress;

	private String userName;

	private String accountNumber;

	private String ifscCode;

	private String bankName;

	private String branchName;

	private String bankAddress;

	private Double loanRequestAmount;

	private String employment;

	private String workExperience;

	private String modeOfTransactions;

	private String email;

	private String facebookUrl;

	private String twitterUrl;

	private String locality;

	private String whatsAppNumber;

	private String confirmAccountNumber;

	private String modifyOption;

	private String whatsappOtp;

	private boolean walletCalculationToLender = false;

	private BigInteger walletAmountToLender;

	private String loanStatus;

	private String esignStatus;

	private String mobileOtpSession;

	private String mobileOtp;

	private boolean updateBankDetails;

	private boolean studentOrNot;

	private String aadharNumber;

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getFacebookUrl() {
		return facebookUrl;
	}

	public String getTwitterUrl() {
		return twitterUrl;
	}

	public String getLinkedinUrl() {
		return linkedinUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public void setTwitterUrl(String twitterUrl) {
		this.twitterUrl = twitterUrl;
	}

	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}

	private String linkedinUrl;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getModeOfTransactions() {
		return modeOfTransactions;
	}

	public void setModeOfTransactions(String modeOfTransactions) {
		this.modeOfTransactions = modeOfTransactions;
	}

	public String getEmployment() {
		return employment;
	}

	public void setEmployment(String employment) {
		this.employment = employment;
	}

	public String getWorkExperience() {
		return workExperience;
	}

	public void setWorkExperience(String workExperience) {
		this.workExperience = workExperience;
	}

	public Double getLoanRequestAmount() {
		return loanRequestAmount;
	}

	public void setLoanRequestAmount(Double loanRequestAmount) {
		this.loanRequestAmount = loanRequestAmount;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getWhatsAppNumber() {
		return whatsAppNumber;
	}

	public void setWhatsAppNumber(String whatsAppNumber) {
		this.whatsAppNumber = whatsAppNumber;
	}

	public String getConfirmAccountNumber() {
		return confirmAccountNumber;
	}

	public void setConfirmAccountNumber(String confirmAccountNumber) {
		this.confirmAccountNumber = confirmAccountNumber;
	}

	public String getModifyOption() {
		return modifyOption;
	}

	public void setModifyOption(String modifyOption) {
		this.modifyOption = modifyOption;
	}

	public String getWhatsappOtp() {
		return whatsappOtp;
	}

	public void setWhatsappOtp(String whatsappOtp) {
		this.whatsappOtp = whatsappOtp;
	}

	public boolean isWalletCalculationToLender() {
		return walletCalculationToLender;
	}

	public void setWalletCalculationToLender(boolean walletCalculationToLender) {
		this.walletCalculationToLender = walletCalculationToLender;
	}

	public BigInteger getWalletAmountToLender() {
		return walletAmountToLender;
	}

	public void setWalletAmountToLender(BigInteger walletAmountToLender) {
		this.walletAmountToLender = walletAmountToLender;
	}

	public String getLoanStatus() {
		return loanStatus;
	}

	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}

	public String getEsignStatus() {
		return esignStatus;
	}

	public void setEsignStatus(String esignStatus) {
		this.esignStatus = esignStatus;
	}

	public String getMobileOtpSession() {
		return mobileOtpSession;
	}

	public void setMobileOtpSession(String mobileOtpSession) {
		this.mobileOtpSession = mobileOtpSession;
	}

	public String getMobileOtp() {
		return mobileOtp;
	}

	public void setMobileOtp(String mobileOtp) {
		this.mobileOtp = mobileOtp;
	}

	public boolean isUpdateBankDetails() {
		return updateBankDetails;
	}

	public void setUpdateBankDetails(boolean updateBankDetails) {
		this.updateBankDetails = updateBankDetails;
	}

	public boolean isStudentOrNot() {
		return studentOrNot;
	}

	public void setStudentOrNot(boolean studentOrNot) {
		this.studentOrNot = studentOrNot;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

}
