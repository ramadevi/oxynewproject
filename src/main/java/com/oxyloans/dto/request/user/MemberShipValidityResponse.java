package com.oxyloans.dto.request.user;

public class MemberShipValidityResponse {
	
	
	
	private int userId;
	
	private String validityDate;
	
	private String renewedStatus;
	
	private double previousAmount;
	
	private String paymentDate;
	
	private String lenderName;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}

	public String getRenewedStatus() {
		return renewedStatus;
	}

	public void setRenewedStatus(String renewedStatus) {
		this.renewedStatus = renewedStatus;
	}

	
	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public double getPreviousAmount() {
		return previousAmount;
	}

	public void setPreviousAmount(double previousAmount) {
		this.previousAmount = previousAmount;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	
	
	

}
