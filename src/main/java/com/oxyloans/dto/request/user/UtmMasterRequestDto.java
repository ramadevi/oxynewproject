package com.oxyloans.dto.request.user;

public class UtmMasterRequestDto {

	private int id;

	private String utmName;

	private String utmDate;

	public String getUtmName() {
		return utmName;
	}

	public void setUtmName(String utmName) {
		this.utmName = utmName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUtmDate() {
		return utmDate;
	}

	public void setUtmDate(String utmDate) {
		this.utmDate = utmDate;
	}

}
