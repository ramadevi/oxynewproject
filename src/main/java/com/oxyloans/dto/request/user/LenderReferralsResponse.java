package com.oxyloans.dto.request.user;

public class LenderReferralsResponse {

	private Integer refereeId;

	private Integer referrerId;
	
	private Double amount;
	
	private String transferredOn;
	
	private String userName; 
	
    private String remarks;
	
	private String totalPaidEarnings;
    
	private String refereeDealIdPair;
	
	private Integer dealId;
	
	private String paymentStatus;
    
	private String refferalBonusId;
	
	public Integer getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(Integer refereeId) {
		this.refereeId = refereeId;
	}

	public Integer getReferrerId() {
		return referrerId;
	}

	public void setReferrerId(Integer referrerId) {
		this.referrerId = referrerId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getTransferredOn() {
		return transferredOn;
	}

	public void setTransferredOn(String transferredOn) {
		this.transferredOn = transferredOn;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getTotalPaidEarnings() {
		return totalPaidEarnings;
	}

	public void setTotalPaidEarnings(String totalPaidEarnings) {
		this.totalPaidEarnings = totalPaidEarnings;
	}

	public String getRefereeDealIdPair() {
		return refereeDealIdPair;
	}

	public void setRefereeDealIdPair(String refereeDealIdPair) {
		this.refereeDealIdPair = refereeDealIdPair;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getRefferalBonusId() {
		return refferalBonusId;
	}

	public void setRefferalBonusId(String refferalBonusId) {
		this.refferalBonusId = refferalBonusId;
	}

	public Double getParticipationAmt() {
		return participationAmt;
	}

	public void setParticipationAmt(Double participationAmt) {
		this.participationAmt = participationAmt;
	}

	private Double participationAmt;
	
}
