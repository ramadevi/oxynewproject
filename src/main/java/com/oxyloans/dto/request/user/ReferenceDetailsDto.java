package com.oxyloans.dto.request.user;

public class ReferenceDetailsDto {

	private String referenceNumber;

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	@Override
	public String toString() {
		return "ReferenceDetailsDto [referenceNumber=" + referenceNumber + "]";
	}

}
