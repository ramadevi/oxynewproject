package com.oxyloans.dto.request.user;

public class BulkInviteRequest {
	
	private String email;
	private String fileName;
	private String mobileNumber;
	private String fromMail;
	private String name;
	private String message;
private String mailSubject;
private String inviteType;
private String emailCampignTemplateName;
private String whatsappImage;
private String mailDispalyName;
private String logo;
private String projectType;

	
	public String getProjectType() {
	return projectType;
}

public void setProjectType(String projectType) {
	this.projectType = projectType;
}

	public String getMailSubject() {
	return mailSubject;
}

public void setMailSubject(String mailSubject) {
	this.mailSubject = mailSubject;
}

public String getEmailCampignTemplateName() {
	return emailCampignTemplateName;
}

public void setEmailCampignTemplateName(String emailCampignTemplateName) {
	this.emailCampignTemplateName = emailCampignTemplateName;
}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getFromMail() {
		return fromMail;
	}

	public void setFromMail(String fromMail) {
		this.fromMail = fromMail;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getInviteType() {
		return inviteType;
	}

	public void setInviteType(String inviteType) {
		this.inviteType = inviteType;
	}

	public String getWhatsappImage() {
		return whatsappImage;
	}

	public void setWhatsappImage(String whatsappImage) {
		this.whatsappImage = whatsappImage;
	}

	public String getMailDispalyName() {
		return mailDispalyName;
	}

	public void setMailDispalyName(String mailDispalyName) {
		this.mailDispalyName = mailDispalyName;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}
	
	


}
