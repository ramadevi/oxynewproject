package com.oxyloans.dto.request.user;

public class ProfessionalDetailsRequestDto {
	
	private String employment;
	
	private String companyName;
   
	private String designation;

	private String description;

	private Integer noOfJobsChanged;

	private String workExperience;

	private Integer officeAddressId;

	private String officeContactNumber;

	private String highestQualification;

	private String fieldOfStudy;

	private String college;

	private Integer yearOfPassing;

	public String getEmployment() {
		return employment;
	}

	public void setEmployment(String employment) {
		this.employment = employment;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getNoOfJobsChanged() {
		return noOfJobsChanged;
	}

	public void setNoOfJobsChanged(Integer noOfJobsChanged) {
		this.noOfJobsChanged = noOfJobsChanged;
	}

	public String getWorkExperience() {
		return workExperience;
	}

	public void setWorkExperience(String workExperience) {
		this.workExperience = workExperience;
	}

	public Integer getOfficeAddressId() {
		return officeAddressId;
	}

	public void setOfficeAddressId(Integer officeAddressId) {
		this.officeAddressId = officeAddressId;
	}

	public String getOfficeContactNumber() {
		return officeContactNumber;
	}

	public void setOfficeContactNumber(String officeContactNumber) {
		this.officeContactNumber = officeContactNumber;
	}

	public String getHighestQualification() {
		return highestQualification;
	}

	public void setHighestQualification(String highestQualification) {
		this.highestQualification = highestQualification;
	}

	public String getFieldOfStudy() {
		return fieldOfStudy;
	}

	public void setFieldOfStudy(String fieldOfStudy) {
		this.fieldOfStudy = fieldOfStudy;
	}

	public String getCollege() {
		return college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public Integer getYearOfPassing() {
		return yearOfPassing;
	}

	public void setYearOfPassing(Integer yearOfPassing) {
		this.yearOfPassing = yearOfPassing;
	}

	

}
