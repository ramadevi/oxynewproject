package com.oxyloans.dto.request.user;

import java.util.List;

public class MonthlyValidityResponse {
	
	private int count;
	
	private List<MemberShipValidityResponse> memberShipValidityResponse;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<MemberShipValidityResponse> getMemberShipValidityResponse() {
		return memberShipValidityResponse;
	}

	public void setMemberShipValidityResponse(List<MemberShipValidityResponse> memberShipValidityResponse) {
		this.memberShipValidityResponse = memberShipValidityResponse;
	}
	
	

}
