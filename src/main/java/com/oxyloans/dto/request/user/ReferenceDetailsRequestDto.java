package com.oxyloans.dto.request.user;

import java.util.List;

public class ReferenceDetailsRequestDto {

	private Integer userId;

	private boolean updateReferenceDetails;

	List<ReferenceDetailsDto> referenceDto;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public boolean isUpdateReferenceDetails() {
		return updateReferenceDetails;
	}

	public void setUpdateReferenceDetails(boolean updateReferenceDetails) {
		this.updateReferenceDetails = updateReferenceDetails;
	}

	public List<ReferenceDetailsDto> getReferenceDto() {
		return referenceDto;
	}

	public void setReferenceDto(List<ReferenceDetailsDto> referenceDto) {
		this.referenceDto = referenceDto;
	}

}
