
package com.oxyloans.dto.request.user;

public class KycFileResponse {

	private Integer userId;

	private String documentType;

	private String documentSubType;

	private String status;

	private String fileName;

	private KycFileRequest.KycType kycType;

	private String downloadUrl;

	private String downloadUrlForLender;

	private String downloadUrlForBorrower;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentSubType() {
		return documentSubType;
	}

	public void setDocumentSubType(String documentSubType) {
		this.documentSubType = documentSubType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public KycFileRequest.KycType getKycType() {
		return kycType;
	}

	public void setKycType(KycFileRequest.KycType kycType) {
		this.kycType = kycType;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getDownloadUrlForLender() {
		return downloadUrlForLender;
	}

	public void setDownloadUrlForLender(String downloadUrlForLender) {
		this.downloadUrlForLender = downloadUrlForLender;
	}

	public String getDownloadUrlForBorrower() {
		return downloadUrlForBorrower;
	}

	public void setDownloadUrlForBorrower(String downloadUrlForBorrower) {
		this.downloadUrlForBorrower = downloadUrlForBorrower;
	}

}
