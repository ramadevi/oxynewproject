package com.oxyloans.dto.request.user;

public class BulkinviteResponse {
	
	    private Integer count;
	    private String fileName;
	    private String status;
	    private int totalCount;	    
	    private int uniqueEmailCount;
	    private int uniqueMobileCount;
	    
		public Integer getCount() {
			return count;
		}
		public void setCount(Integer count) {
			this.count = count;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public int getTotalCount() {
			return totalCount;
		}
		public void setTotalCount(int totalCount) {
			this.totalCount = totalCount;
		}
		public int getUniqueEmailCount() {
			return uniqueEmailCount;
		}
		public void setUniqueEmailCount(int uniqueEmailCount) {
			this.uniqueEmailCount = uniqueEmailCount;
		}
		public int getUniqueMobileCount() {
			return uniqueMobileCount;
		}
		public void setUniqueMobileCount(int uniqueMobileCount) {
			this.uniqueMobileCount = uniqueMobileCount;
		}
		
	    
}
