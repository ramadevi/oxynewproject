package com.oxyloans.dto.request;

public class PointOfContact {
	
	private String pocEmail;
	
	private String pocMobileNumber;

	public String getPocEmail() {
		return pocEmail;
	}

	public void setPocEmail(String pocEmail) {
		this.pocEmail = pocEmail;
	}

	public String getPocMobileNumber() {
		return pocMobileNumber;
	}

	public void setPocMobileNumber(String pocMobileNumber) {
		this.pocMobileNumber = pocMobileNumber;
	}
	
	
}
