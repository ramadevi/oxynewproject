package com.oxyloans.dto.request;

import java.util.List;

public class PartnerRequestDto {

	private String partnerName;

	private String partnerEmail;

	private String partnermobileNumber;

	/*
	 * private List<PointOfContact> listOfPoCEmail;
	 * 
	 * private List<PointOfContact> listOfPoCMobileNumber;
	 */
	private String pocName;

	private String listOfPoCEmail;

	private String listOfPoCMobileNumber;

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerEmail() {
		return partnerEmail;
	}

	public void setPartnerEmail(String partnerEmail) {
		this.partnerEmail = partnerEmail;
	}

	public String getPartnermobileNumber() {
		return partnermobileNumber;
	}

	public void setPartnermobileNumber(String partnermobileNumber) {
		this.partnermobileNumber = partnermobileNumber;
	}

	public String getPocName() {
		return pocName;
	}

	public void setPocName(String pocName) {
		this.pocName = pocName;
	}

	public String getListOfPoCEmail() {
		return listOfPoCEmail;
	}

	public void setListOfPoCEmail(String listOfPoCEmail) {
		this.listOfPoCEmail = listOfPoCEmail;
	}

	public String getListOfPoCMobileNumber() {
		return listOfPoCMobileNumber;
	}

	public void setListOfPoCMobileNumber(String listOfPoCMobileNumber) {
		this.listOfPoCMobileNumber = listOfPoCMobileNumber;
	}

}
