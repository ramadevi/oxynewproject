package com.oxyloans.dto.poolingaccount;

import java.math.BigInteger;
import java.util.List;

public class PoolingAccountResponseDto {

	private BigInteger totalCurrentAmount;

	private BigInteger totalMonthlyInterest;

	private List<ListOfPoolingAccountsDetails> listOfPoolingAccountsDetails;

	private Integer totalCount;

	public BigInteger getTotalCurrentAmount() {
		return totalCurrentAmount;
	}

	public void setTotalCurrentAmount(BigInteger totalCurrentAmount) {
		this.totalCurrentAmount = totalCurrentAmount;
	}

	public BigInteger getTotalMonthlyInterest() {
		return totalMonthlyInterest;
	}

	public void setTotalMonthlyInterest(BigInteger totalMonthlyInterest) {
		this.totalMonthlyInterest = totalMonthlyInterest;
	}

	public List<ListOfPoolingAccountsDetails> getListOfPoolingAccountsDetails() {
		return listOfPoolingAccountsDetails;
	}

	public void setListOfPoolingAccountsDetails(List<ListOfPoolingAccountsDetails> listOfPoolingAccountsDetails) {
		this.listOfPoolingAccountsDetails = listOfPoolingAccountsDetails;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

}
