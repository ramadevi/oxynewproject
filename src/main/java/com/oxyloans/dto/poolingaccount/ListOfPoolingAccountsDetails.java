package com.oxyloans.dto.poolingaccount;

public class ListOfPoolingAccountsDetails {

	private String name;

	private Integer interestDateForEveryMonth;

	private double currentAmount;

	private double monthlyInterest;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getInterestDateForEveryMonth() {
		return interestDateForEveryMonth;
	}

	public void setInterestDateForEveryMonth(Integer interestDateForEveryMonth) {
		this.interestDateForEveryMonth = interestDateForEveryMonth;
	}

	public double getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(double currentAmount) {
		this.currentAmount = currentAmount;
	}

	public double getMonthlyInterest() {
		return monthlyInterest;
	}

	public void setMonthlyInterest(double monthlyInterest) {
		this.monthlyInterest = monthlyInterest;
	}

}
