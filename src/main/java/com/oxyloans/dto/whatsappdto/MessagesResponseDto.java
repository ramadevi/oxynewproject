package com.oxyloans.dto.whatsappdto;

public class MessagesResponseDto {

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
