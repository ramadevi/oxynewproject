package com.oxyloans.dto.whatsappdto;

public class Messages {

	private String id;

	private String body;


	private boolean fromMe;
	private int self;
	private int isForwarded;
	private String author;
	private String time;
	private String chatId;
	private String messageNumber;
	private String type;
	private String senderName;
	private String quotedMsgBody;
	private String quotedMsgId;
	private String quotedMsgType;
	private String chatName;
	private String caption;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public boolean isFromMe() {
		return fromMe;
	}

	public void setFromMe(boolean fromMe) {
		this.fromMe = fromMe;
	}

	public int getSelf() {
		return self;
	}

	public void setSelf(int self) {
		this.self = self;
	}

	public int getIsForwarded() {
		return isForwarded;
	}

	public void setIsForwarded(int isForwarded) {
		this.isForwarded = isForwarded;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}

	public String getMessageNumber() {
		return messageNumber;
	}

	public void setMessageNumber(String messageNumber) {
		this.messageNumber = messageNumber;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getQuotedMsgBody() {
		return quotedMsgBody;
	}

	public void setQuotedMsgBody(String quotedMsgBody) {
		this.quotedMsgBody = quotedMsgBody;
	}

	public String getQuotedMsgType() {
		return quotedMsgType;
	}

	public void setQuotedMsgType(String quotedMsgType) {
		this.quotedMsgType = quotedMsgType;
	}

	public String getChatName() {
		return chatName;
	}

	public void setChatName(String chatName) {
		this.chatName = chatName;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getQuotedMsgId() {
		return quotedMsgId;
	}

	public void setQuotedMsgId(String quotedMsgId) {
		this.quotedMsgId = quotedMsgId;
	}

}
