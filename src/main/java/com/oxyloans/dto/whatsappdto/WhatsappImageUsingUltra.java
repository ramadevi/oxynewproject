package com.oxyloans.dto.whatsappdto;

public class WhatsappImageUsingUltra {

	private String to;

	private String token;

	private String image;

	private String caption;

	public String getTo() {
		return to;
	}

	public String getToken() {
		return token;
	}

	public String getImage() {
		return image;
	}

	public String getCaption() {
		return caption;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}
}
