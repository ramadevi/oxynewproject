package com.oxyloans.dto.response.admin;

public class AdminEmiDetailsResponseDto {
	private Integer noOfEMIProcessed;
	private Integer noOfEmiNotProcessed;
	private Double amountReceived;
	private Double amountNotReceived;
	private Double earnedAmount;
	private Integer noOFEMISpending;
	private Integer currentMonthTotalEmis;
	private Double currnentMonthTotalAmount;
	private Double thirtyDayAmount;
	private Integer thirtyDayCount;
	private Double sixtyDayAmount;
	private Integer sixtyDayCount;
	private Double npaDayAmount;
	private Integer npaDayCount;
	private Double nintyDayAmount;
	private Integer nintyDayCount;
	private Integer totalPendingEMis;
	

	
	
	public Integer getTotalPendingEMis() {
		return totalPendingEMis;
	}

	public void setTotalPendingEMis(Integer totalPendingEMis) {
		this.totalPendingEMis = totalPendingEMis;
	}

	public Double getCurrnentMonthTotalAmount() {
		return currnentMonthTotalAmount;
	}

	public void setCurrnentMonthTotalAmount(Double currnentMonthTotalAmount) {
		this.currnentMonthTotalAmount = currnentMonthTotalAmount;
	}

	public Double getThirtyDayAmount() {
		return thirtyDayAmount;
	}

	public Integer getThirtyDayCount() {
		return thirtyDayCount;
	}

	public Double getSixtyDayAmount() {
		return sixtyDayAmount;
	}

	public Integer getSixtyDayCount() {
		return sixtyDayCount;
	}

	public Double getNpaDayAmount() {
		return npaDayAmount;
	}

	public Integer getNpaDayCount() {
		return npaDayCount;
	}

	public Double getNintyDayAmount() {
		return nintyDayAmount;
	}

	public Integer getNintyDayCount() {
		return nintyDayCount;
	}

	public void setThirtyDayAmount(Double thirtyDayAmount) {
		this.thirtyDayAmount = thirtyDayAmount;
	}

	public void setThirtyDayCount(Integer thirtyDayCount) {
		this.thirtyDayCount = thirtyDayCount;
	}

	public void setSixtyDayAmount(Double sixtyDayAmount) {
		this.sixtyDayAmount = sixtyDayAmount;
	}

	public void setSixtyDayCount(Integer sixtyDayCount) {
		this.sixtyDayCount = sixtyDayCount;
	}

	public void setNpaDayAmount(Double npaDayAmount) {
		this.npaDayAmount = npaDayAmount;
	}

	public void setNpaDayCount(Integer npaDayCount) {
		this.npaDayCount = npaDayCount;
	}

	public void setNintyDayAmount(Double nintyDayAmount) {
		this.nintyDayAmount = nintyDayAmount;
	}

	public void setNintyDayCount(Integer nintyDayCount) {
		this.nintyDayCount = nintyDayCount;
	}

	

	public Integer getCurrentMonthTotalEmis() {
		return currentMonthTotalEmis;
	}


	public void setCurrentMonthTotalEmis(Integer currentMonthTotalEmis) {
		this.currentMonthTotalEmis = currentMonthTotalEmis;
	}


	public Integer getNoOfEMIProcessed() {
		return noOfEMIProcessed;
	}

	public Integer getNoOfEmiNotProcessed() {
		return noOfEmiNotProcessed;
	}

	public Double getAmountReceived() {
		return amountReceived;
	}

	public Double getAmountNotReceived() {
		return amountNotReceived;
	}

	public Double getEarnedAmount() {
		return earnedAmount;
	}

	public Integer getNoOFEMISpending() {
		return noOFEMISpending;
	}

	public void setNoOfEMIProcessed(Integer noOfEMIProcessed) {
		this.noOfEMIProcessed = noOfEMIProcessed;
	}

	public void setNoOfEmiNotProcessed(Integer noOfEmiNotProcessed) {
		this.noOfEmiNotProcessed = noOfEmiNotProcessed;
	}

	public void setAmountReceived(Double amountReceived) {
		this.amountReceived = amountReceived;
	}

	public void setAmountNotReceived(Double amountNotReceived) {
		this.amountNotReceived = amountNotReceived;
	}

	public void setEarnedAmount(Double earnedAmount) {
		this.earnedAmount = earnedAmount;
	}

	public void setNoOFEMISpending(Integer noOFEMISpending) {
		this.noOFEMISpending = noOFEMISpending;
	}

}
