package com.oxyloans.dto.response.admin;

import java.util.List;

public class RBIStatsResponse {

	private List<StatsResponseDto> listOfStatsResponseDto;
	
	private String primaryType;
	
	private String url;

	public List<StatsResponseDto> getListOfStatsResponseDto() {
		return listOfStatsResponseDto;
	}

	public void setListOfStatsResponseDto(List<StatsResponseDto> listOfStatsResponseDto) {
		this.listOfStatsResponseDto = listOfStatsResponseDto;
	}

	public String getPrimaryType() {
		return primaryType;
	}

	public void setPrimaryType(String primaryType) {
		this.primaryType = primaryType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
