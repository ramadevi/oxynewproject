package com.oxyloans.dto.response.admin;

import java.util.List;

public class DisbursmentPendingResponseDto {

	public Integer countForDisbursmentPending;

	public List<UserInformationResponseDto> disbursmentPendingInformation;

	public Integer getCountForDisbursmentPending() {
		return countForDisbursmentPending;
	}

	public void setCountForDisbursmentPending(Integer countForDisbursmentPending) {
		this.countForDisbursmentPending = countForDisbursmentPending;
	}

	public List<UserInformationResponseDto> getDisbursmentPendingInformation() {
		return disbursmentPendingInformation;
	}

	public void setDisbursmentPendingInformation(List<UserInformationResponseDto> disbursmentPendingInformation) {
		this.disbursmentPendingInformation = disbursmentPendingInformation;
	}

}
