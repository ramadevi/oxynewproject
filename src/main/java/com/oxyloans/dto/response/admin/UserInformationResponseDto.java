package com.oxyloans.dto.response.admin;

import java.util.List;

import com.oxyloans.request.user.KycFileResponse;
import com.oxyloans.dto.response.user.CommentsRequestDto;

public class UserInformationResponseDto {

	private Integer lenderId;

	private String lenderName;

	private String lenderCity;

	private String lenderbankName;

	private String lenderAccountNumber;

	private String LenderBranchName;

	private String LenderIfscCode;

	private String loanId;

	private String durationType;

	private String loanRequestId;

	private Integer borrowerId;

	private String borrowerName;

	private String borrowerCity;

	private String borrowerbankName;

	private String borrowerAccountNumber;

	private String borrowerBranchName;

	private String borrowerIfscCode;

	private String enachType;

	private double disbursedAmount;

	private Integer duration;

	private List<KycFileResponse> borrowerKycDocuments;

	private CommentsRequestDto commentsRequestDto;

	private Integer oxyScore;

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public String getLenderCity() {
		return lenderCity;
	}

	public void setLenderCity(String lenderCity) {
		this.lenderCity = lenderCity;
	}

	public String getLenderbankName() {
		return lenderbankName;
	}

	public void setLenderbankName(String lenderbankName) {
		this.lenderbankName = lenderbankName;
	}

	public String getLenderAccountNumber() {
		return lenderAccountNumber;
	}

	public void setLenderAccountNumber(String lenderAccountNumber) {
		this.lenderAccountNumber = lenderAccountNumber;
	}

	public String getLenderBranchName() {
		return LenderBranchName;
	}

	public void setLenderBranchName(String lenderBranchName) {
		LenderBranchName = lenderBranchName;
	}

	public String getLenderIfscCode() {
		return LenderIfscCode;
	}

	public void setLenderIfscCode(String lenderIfscCode) {
		LenderIfscCode = lenderIfscCode;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public Integer getOxyScore() {
		return oxyScore;
	}

	public void setOxyScore(Integer oxyScore) {
		this.oxyScore = oxyScore;
	}

	public String getDurationType() {
		return durationType;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public String getLoanRequestId() {
		return loanRequestId;
	}

	public void setLoanRequestId(String loanRequestId) {
		this.loanRequestId = loanRequestId;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public String getBorrowerCity() {
		return borrowerCity;
	}

	public void setBorrowerCity(String borrowerCity) {
		this.borrowerCity = borrowerCity;
	}

	public String getBorrowerbankName() {
		return borrowerbankName;
	}

	public void setBorrowerbankName(String borrowerbankName) {
		this.borrowerbankName = borrowerbankName;
	}

	public String getBorrowerAccountNumber() {
		return borrowerAccountNumber;
	}

	public void setBorrowerAccountNumber(String borrowerAccountNumber) {
		this.borrowerAccountNumber = borrowerAccountNumber;
	}

	public String getBorrowerBranchName() {
		return borrowerBranchName;
	}

	public void setBorrowerBranchName(String borrowerBranchName) {
		this.borrowerBranchName = borrowerBranchName;
	}

	public String getBorrowerIfscCode() {
		return borrowerIfscCode;
	}

	public void setBorrowerIfscCode(String borrowerIfscCode) {
		this.borrowerIfscCode = borrowerIfscCode;
	}

	public String getEnachType() {
		return enachType;
	}

	public void setEnachType(String enachType) {
		this.enachType = enachType;
	}

	public double getDisbursedAmount() {
		return disbursedAmount;
	}

	public void setDisbursedAmount(double disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public List<KycFileResponse> getBorrowerKycDocuments() {
		return borrowerKycDocuments;
	}

	public void setBorrowerKycDocuments(List<KycFileResponse> borrowerKycDocuments) {
		this.borrowerKycDocuments = borrowerKycDocuments;
	}

	public CommentsRequestDto getCommentsRequestDto() {
		return commentsRequestDto;
	}

	public void setCommentsRequestDto(CommentsRequestDto commentsRequestDto) {
		this.commentsRequestDto = commentsRequestDto;
	}

}
