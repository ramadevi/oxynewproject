package com.oxyloans.dto.response.admin;

public class AdminResponse {
	
	private Long totalNumberOfUsers;

	private Long totalNumberOfLenders;

	private Long totalNumberOfBorrowers;

	public Long getTotalNumberOfUsers() {
		return totalNumberOfUsers;
	}

	public void setTotalNumberOfUsers(Long totalNumberOfUsers) {
		this.totalNumberOfUsers = totalNumberOfUsers;
	}

	public Long getTotalNumberOfLenders() {
		return totalNumberOfLenders;
	}

	public void setTotalNumberOfLenders(Long totalNumberOfLenders) {
		this.totalNumberOfLenders = totalNumberOfLenders;
	}

	public Long getTotalNumberOfBorrowers() {
		return totalNumberOfBorrowers;
	}

	public void setTotalNumberOfBorrowers(Long totalNumberOfBorrowers) {
		this.totalNumberOfBorrowers = totalNumberOfBorrowers;
	}

}
