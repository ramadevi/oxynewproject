package com.oxyloans.dto.response.admin;

public class StatsResponseDto {

	private String userName;

	private String userPAN;

	private String registeredDate;

	private String grade;

	private String userMappedPAN;

	private Double avgPercentage;

	private Double totalAmount;

	private Integer duration;

	private String userMappedName;

	private Integer userId;

	private Double totalParticipatedAmount;

	private Double totalClosedAmount;

	private Double totalWithdrawalAmount;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPAN() {
		return userPAN;
	}

	public void setUserPAN(String userPAN) {
		this.userPAN = userPAN;
	}

	public String getRegisteredDate() {
		return registeredDate;
	}

	public void setRegisteredDate(String registeredDate) {
		this.registeredDate = registeredDate;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getUserMappedPAN() {
		return userMappedPAN;
	}

	public void setUserMappedPAN(String userMappedPAN) {
		this.userMappedPAN = userMappedPAN;
	}

	public Double getAvgPercentage() {
		return avgPercentage;
	}

	public void setAvgPercentage(Double avgPercentage) {
		this.avgPercentage = avgPercentage;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getUserMappedName() {
		return userMappedName;
	}

	public void setUserMappedName(String userMappedName) {
		this.userMappedName = userMappedName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Double getTotalParticipatedAmount() {
		return totalParticipatedAmount;
	}

	public void setTotalParticipatedAmount(Double totalParticipatedAmount) {
		this.totalParticipatedAmount = totalParticipatedAmount;
	}

	public Double getTotalClosedAmount() {
		return totalClosedAmount;
	}

	public void setTotalClosedAmount(Double totalClosedAmount) {
		this.totalClosedAmount = totalClosedAmount;
	}

	public Double getTotalWithdrawalAmount() {
		return totalWithdrawalAmount;
	}

	public void setTotalWithdrawalAmount(Double totalWithdrawalAmount) {
		this.totalWithdrawalAmount = totalWithdrawalAmount;
	}

}