package com.oxyloans.dto.response.user;

import java.util.List;

import com.oxyloans.request.user.LenderReferralsResponse;

public class LenderReferenceAmountResponse {
	
	private String disbursmentDate;
	
	private Double disbursmentAmount;
	
	private Double amount;
	
	private String paymentStatus;
	
	private String transferredOn;
	
	private Integer refereeId;
	
	private Integer referrerId;
	
	private String participatedOn;
	
	private Integer dealId;

	private Double participatedAmount;
	
	private String userName;
	
	private String refereeNewId;
	
	private String remarks;
	
    private String refferalBonusUpdatedId;
    
    private List<LenderReferralsResponse> lenderReferralsResponse;

	
	public List<LenderReferralsResponse> getLenderReferralsResponse() {
		return lenderReferralsResponse;
	}

	public void setLenderReferralsResponse(List<LenderReferralsResponse> lenderReferralsResponse) {
		this.lenderReferralsResponse = lenderReferralsResponse;
	}

	public String getRefferalBonusUpdatedId() {
		return refferalBonusUpdatedId;
	}

	public void setRefferalBonusUpdatedId(String refferalBonusUpdatedId) {
		this.refferalBonusUpdatedId = refferalBonusUpdatedId;
	}

	public String getRefereeDealIdPair() {
		return refereeDealIdPair;
	}

	public void setRefereeDealIdPair(String refereeDealIdPair) {
		this.refereeDealIdPair = refereeDealIdPair;
	}

	private String refereeDealIdPair;  

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getTransferredOn() {
		return transferredOn;
	}

	public void setTransferredOn(String transferredOn) {
		this.transferredOn = transferredOn;
	}

	public Integer getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(Integer refereeId) {
		this.refereeId = refereeId;
	}

	public Integer getReferrerId() {
		return referrerId;
	}

	public void setReferrerId(Integer referrerId) {
		this.referrerId = referrerId;
	}

	public String getDisbursmentDate() {
		return disbursmentDate;
	}

	public void setDisbursmentDate(String disbursmentDate) {
		this.disbursmentDate = disbursmentDate;
	}

	public Double getDisbursmentAmount() {
		return disbursmentAmount;
	}

	public void setDisbursmentAmount(Double disbursmentAmount) {
		this.disbursmentAmount = disbursmentAmount;
	}

	public String getParticipatedOn() {
		return participatedOn;
	}

	public void setParticipatedOn(String participatedOn) {
		this.participatedOn = participatedOn;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Double getParticipatedAmount() {
		return participatedAmount;
	}

	public void setParticipatedAmount(Double participatedAmount) {
		this.participatedAmount = participatedAmount;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRefereeNewId() {
		return refereeNewId;
	}

	public void setRefereeNewId(String refereeNewId) {
		this.refereeNewId = refereeNewId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
