package com.oxyloans.dto.response.user;

public class LenderDetails {
	private Integer lenderId;

	private String lenderName;

	private String lenderAccountNumber;

	private Double sumOfEmiAmount;

	private String DueOnDate;

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public String getLenderAccountNumber() {
		return lenderAccountNumber;
	}

	public void setLenderAccountNumber(String lenderAccountNumber) {
		this.lenderAccountNumber = lenderAccountNumber;
	}

	public Double getSumOfEmiAmount() {
		return sumOfEmiAmount;
	}

	public void setSumOfEmiAmount(Double sumOfEmiAmount) {
		this.sumOfEmiAmount = sumOfEmiAmount;
	}

	public String getDueOnDate() {
		return DueOnDate;
	}

	public void setDueOnDate(String dueOnDate) {
		DueOnDate = dueOnDate;
	}

}
