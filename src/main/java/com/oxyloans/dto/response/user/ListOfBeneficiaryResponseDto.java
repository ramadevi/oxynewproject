package com.oxyloans.dto.response.user;

import java.util.List;

public class ListOfBeneficiaryResponseDto {
	private List<BeneficiaryResponseDto> listOfBeneficiaries;

	private Integer count;

	public List<BeneficiaryResponseDto> getListOfBeneficiaries() {
		return listOfBeneficiaries;
	}

	public void setListOfBeneficiaries(List<BeneficiaryResponseDto> listOfBeneficiaries) {
		this.listOfBeneficiaries = listOfBeneficiaries;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
