package com.oxyloans.dto.response.user;

import java.util.List;

public class MaximumAmountInfo {

	private List<MaximumAmountResponse> listOfResponse;
	
	private String downloadUrl;

	public List<MaximumAmountResponse> getListOfResponse() {
		return listOfResponse;
	}

	public void setListOfResponse(List<MaximumAmountResponse> listOfResponse) {
		this.listOfResponse = listOfResponse;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	
	
}
