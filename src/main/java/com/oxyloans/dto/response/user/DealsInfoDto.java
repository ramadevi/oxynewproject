package com.oxyloans.dto.response.user;

public class DealsInfoDto {

	private int dealId;
	private String dealName;

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

}
