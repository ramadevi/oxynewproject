package com.oxyloans.dto.response.user;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class PaymentUploadHistoryResponseDto extends PaymentUploadHistoryRequestDto {

	private String borrowerUniqueNumber;

	private String borrowerName;

	private Integer documentUploadId;

	private Double amount;

	private String updatedName;

	private String paidDate;

	private Integer userId;

	private String fileName;

	private String filePath;

	private Date updatedOn;

	private Integer countOfPaymentScreenShots;

	private Integer id;

	private BigInteger countByOxyMembers;

	private String status;

	private String paymentUrl;

	private String comments;

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getPaymentUrl() {
		return paymentUrl;
	}

	public void setPaymentUrl(String paymentUrl) {
		this.paymentUrl = paymentUrl;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private List<PaymentUploadHistoryResponseDto> listOfPaymentUploaded;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getBorrowerUniqueNumber() {
		return borrowerUniqueNumber;
	}

	public void setBorrowerUniqueNumber(String borrowerUniqueNumber) {
		this.borrowerUniqueNumber = borrowerUniqueNumber;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public Integer getDocumentUploadId() {
		return documentUploadId;
	}

	public void setDocumentUploadId(Integer documentUploadId) {
		this.documentUploadId = documentUploadId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getUpdatedName() {
		return updatedName;
	}

	public void setUpdatedName(String updatedName) {
		this.updatedName = updatedName;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getCountOfPaymentScreenShots() {
		return countOfPaymentScreenShots;
	}

	public void setCountOfPaymentScreenShots(Integer countOfPaymentScreenShots) {
		this.countOfPaymentScreenShots = countOfPaymentScreenShots;
	}

	public List<PaymentUploadHistoryResponseDto> getListOfPaymentUploaded() {
		return listOfPaymentUploaded;
	}

	public void setListOfPaymentUploaded(List<PaymentUploadHistoryResponseDto> listOfPaymentUploaded) {
		this.listOfPaymentUploaded = listOfPaymentUploaded;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigInteger getCountByOxyMembers() {
		return countByOxyMembers;
	}

	public void setCountByOxyMembers(BigInteger countByOxyMembers) {
		this.countByOxyMembers = countByOxyMembers;
	}

}
