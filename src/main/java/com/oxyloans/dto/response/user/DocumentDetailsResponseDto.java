package com.oxyloans.dto.response.user;

public class DocumentDetailsResponseDto {
	private String panUrl;

	private String bankstatementsUrl;

	private String payslipsUrl;

	private String aadharUrl;

	private String drivinglicenceUrl;

	private String voteridUrl;

	private String passportUrl;

	private String experianUrl;

	private String cibilscoreUrl;

	public String getPanUrl() {
		return panUrl;
	}

	public void setPanUrl(String panUrl) {
		this.panUrl = panUrl;
	}

	public String getBankstatementsUrl() {
		return bankstatementsUrl;
	}

	public void setBankstatementsUrl(String bankstatementsUrl) {
		this.bankstatementsUrl = bankstatementsUrl;
	}

	public String getPayslipsUrl() {
		return payslipsUrl;
	}

	public void setPayslipsUrl(String payslipsUrl) {
		this.payslipsUrl = payslipsUrl;
	}

	public String getAadharUrl() {
		return aadharUrl;
	}

	public void setAadharUrl(String aadharUrl) {
		this.aadharUrl = aadharUrl;
	}

	public String getDrivinglicenceUrl() {
		return drivinglicenceUrl;
	}

	public void setDrivinglicenceUrl(String drivinglicenceUrl) {
		this.drivinglicenceUrl = drivinglicenceUrl;
	}

	public String getVoteridUrl() {
		return voteridUrl;
	}

	public void setVoteridUrl(String voteridUrl) {
		this.voteridUrl = voteridUrl;
	}

	public String getPassportUrl() {
		return passportUrl;
	}

	public void setPassportUrl(String passportUrl) {
		this.passportUrl = passportUrl;
	}

	public String getExperianUrl() {
		return experianUrl;
	}

	public void setExperianUrl(String experianUrl) {
		this.experianUrl = experianUrl;
	}

	public String getCibilscoreUrl() {
		return cibilscoreUrl;
	}

	public void setCibilscoreUrl(String cibilscoreUrl) {
		this.cibilscoreUrl = cibilscoreUrl;
	}

}
