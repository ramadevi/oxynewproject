package com.oxyloans.dto.response.user;

public class CommentsRequestDto {

	private Integer loanRequestId;

	private String comments;

	private Integer rating;

	private double rateOfInterestToBorrower;

	private double rateOfInterestToLender;

	private Integer durationToTheApplication;

	private String repaymentMethodForBorrower;

	private String repaymentMethodForLender;

	private String durationType;

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public double getRateOfInterestToBorrower() {
		return rateOfInterestToBorrower;
	}

	public void setRateOfInterestToBorrower(double rateOfInterestToBorrower) {
		this.rateOfInterestToBorrower = rateOfInterestToBorrower;
	}

	public double getRateOfInterestToLender() {
		return rateOfInterestToLender;
	}

	public void setRateOfInterestToLender(double rateOfInterestToLender) {
		this.rateOfInterestToLender = rateOfInterestToLender;
	}

	public Integer getDurationToTheApplication() {
		return durationToTheApplication;
	}

	public void setDurationToTheApplication(Integer durationToTheApplication) {
		this.durationToTheApplication = durationToTheApplication;
	}

	public String getRepaymentMethodForBorrower() {
		return repaymentMethodForBorrower;
	}

	public void setRepaymentMethodForBorrower(String repaymentMethodForBorrower) {
		this.repaymentMethodForBorrower = repaymentMethodForBorrower;
	}

	public String getRepaymentMethodForLender() {
		return repaymentMethodForLender;
	}

	public void setRepaymentMethodForLender(String repaymentMethodForLender) {
		this.repaymentMethodForLender = repaymentMethodForLender;
	}

	public String getDurationType() {
		return durationType;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public Integer getLoanRequestId() {
		return loanRequestId;
	}

	public void setLoanRequestId(Integer loanRequestId) {
		this.loanRequestId = loanRequestId;
	}

}
