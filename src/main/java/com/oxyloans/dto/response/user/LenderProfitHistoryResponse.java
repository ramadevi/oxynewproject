package com.oxyloans.dto.response.user;

public class LenderProfitHistoryResponse {
	private String borrowerName;

	private String loanId;

	private Double disbursedAmount;

	private Double profitReceived;

	private int duration;

	private double rateOfInterest;

	private String loanStatus;

	private Integer countOfEmiPaid;

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public Double getDisbursedAmount() {
		return disbursedAmount;
	}

	public void setDisbursedAmount(Double disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}

	public Double getProfitReceived() {
		return profitReceived;
	}

	public void setProfitReceived(Double profitReceived) {
		this.profitReceived = profitReceived;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public String getLoanStatus() {
		return loanStatus;
	}

	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}

	public Integer getCountOfEmiPaid() {
		return countOfEmiPaid;
	}

	public void setCountOfEmiPaid(Integer countOfEmiPaid) {
		this.countOfEmiPaid = countOfEmiPaid;
	}

}
