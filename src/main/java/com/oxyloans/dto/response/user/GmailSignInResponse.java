package com.oxyloans.dto.response.user;

public class GmailSignInResponse {
	private String signInUrl;

	private String gmailCode;

	private boolean userExists;

	private String gmailRedirectUri;

	private String userType;
	
	private String projectType;
	
	private String phoneNumberRequiredOrNot;

	public String getSignInUrl() {
		return signInUrl;
	}

	public void setSignInUrl(String signInUrl) {
		this.signInUrl = signInUrl;
	}

	public String getGmailCode() {
		return gmailCode;
	}

	public void setGmailCode(String gmailCode) {
		this.gmailCode = gmailCode;
	}

	public boolean isUserExists() {
		return userExists;
	}

	public void setUserExists(boolean userExists) {
		this.userExists = userExists;
	}

	public String getGmailRedirectUri() {
		return gmailRedirectUri;
	}

	public void setGmailRedirectUri(String gmailRedirectUri) {
		this.gmailRedirectUri = gmailRedirectUri;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getPhoneNumberRequiredOrNot() {
		return phoneNumberRequiredOrNot;
	}

	public void setPhoneNumberRequiredOrNot(String phoneNumberRequiredOrNot) {
		this.phoneNumberRequiredOrNot = phoneNumberRequiredOrNot;
	}

	
	
}
