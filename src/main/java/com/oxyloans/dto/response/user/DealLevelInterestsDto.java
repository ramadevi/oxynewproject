package com.oxyloans.dto.response.user;

import java.util.List;

import com.oxyloans.service.loan.dto.DealLevelInterestPaymentsDto;

public class DealLevelInterestsDto {
	private List<DealLevelInterestPaymentsDto> listOfDealLevelInterestPayments;
	private Integer countOfDeals;

	public List<DealLevelInterestPaymentsDto> getListOfDealLevelInterestPayments() {
		return listOfDealLevelInterestPayments;
	}

	public void setListOfDealLevelInterestPayments(List<DealLevelInterestPaymentsDto> listOfDealLevelInterestPayments) {
		this.listOfDealLevelInterestPayments = listOfDealLevelInterestPayments;
	}

	public Integer getCountOfDeals() {
		return countOfDeals;
	}

	public void setCountOfDeals(Integer countOfDeals) {
		this.countOfDeals = countOfDeals;
	}

}
