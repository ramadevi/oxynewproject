package com.oxyloans.dto.response.user;

public class BorrowerDetailsRequestDto {
	private String location;

	private String locationResidence;

	private String companyName;

	private String companyResidence;

	private String role;

	private Double loanRequirement;

	private Double emi;

	private Double salary;

	private Double eligibility;

	private String cibilPassword;

	private String aadharPassword;

	private String panPassword;

	private String bankPassword;

	private String payslipsPassword;

	private String comments;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocationResidence() {
		return locationResidence;
	}

	public void setLocationResidence(String locationResidence) {
		this.locationResidence = locationResidence;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyResidence() {
		return companyResidence;
	}

	public void setCompanyResidence(String companyResidence) {
		this.companyResidence = companyResidence;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Double getLoanRequirement() {
		return loanRequirement;
	}

	public void setLoanRequirement(Double loanRequirement) {
		this.loanRequirement = loanRequirement;
	}

	public Double getEmi() {
		return emi;
	}

	public void setEmi(Double emi) {
		this.emi = emi;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public Double getEligibility() {
		return eligibility;
	}

	public void setEligibility(Double eligibility) {
		this.eligibility = eligibility;
	}

	public String getCibilPassword() {
		return cibilPassword;
	}

	public void setCibilPassword(String cibilPassword) {
		this.cibilPassword = cibilPassword;
	}

	public String getAadharPassword() {
		return aadharPassword;
	}

	public void setAadharPassword(String aadharPassword) {
		this.aadharPassword = aadharPassword;
	}

	public String getPanPassword() {
		return panPassword;
	}

	public void setPanPassword(String panPassword) {
		this.panPassword = panPassword;
	}

	public String getBankPassword() {
		return bankPassword;
	}

	public void setBankPassword(String bankPassword) {
		this.bankPassword = bankPassword;
	}

	public String getPayslipsPassword() {
		return payslipsPassword;
	}

	public void setPayslipsPassword(String payslipsPassword) {
		this.payslipsPassword = payslipsPassword;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
