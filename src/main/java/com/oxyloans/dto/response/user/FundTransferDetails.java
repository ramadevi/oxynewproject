package com.oxyloans.dto.response.user;

public class FundTransferDetails {
	private Integer userId;

	private String beneficiaryName;

	private String beneficiaryAccountNumber;

	private String transactionType;

	private String beneficiaryIfsc;

	private String channel;

	private Double beneficiaryAmount;

	private String transactionRequestId;

	private String updatedOn;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryAccountNumber() {
		return beneficiaryAccountNumber;
	}

	public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getBeneficiaryIfsc() {
		return beneficiaryIfsc;
	}

	public void setBeneficiaryIfsc(String beneficiaryIfsc) {
		this.beneficiaryIfsc = beneficiaryIfsc;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public Double getBeneficiaryAmount() {
		return beneficiaryAmount;
	}

	public void setBeneficiaryAmount(Double beneficiaryAmount) {
		this.beneficiaryAmount = beneficiaryAmount;
	}

	public String getTransactionRequestId() {
		return transactionRequestId;
	}

	public void setTransactionRequestId(String transactionRequestId) {
		this.transactionRequestId = transactionRequestId;
	}

	public String getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

}
