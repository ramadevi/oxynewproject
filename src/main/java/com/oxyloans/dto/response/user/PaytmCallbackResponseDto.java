package com.oxyloans.dto.response.user;

public class PaytmCallbackResponseDto {

	private String event_tracking_id;

	private String response_code;

	private Data data = new Data();

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public String getResponse_code() {
		return response_code;
	}

	public void setResponse_code(String response_code) {
		this.response_code = response_code;
	}

	public String getEvent_tracking_id() {
		return event_tracking_id;
	}

	public void setEvent_tracking_id(String event_tracking_id) {
		this.event_tracking_id = event_tracking_id;
	}

}
