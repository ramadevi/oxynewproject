package com.oxyloans.dto.response.user;

public class BorrowersLoanInfo {

	private Integer borrowerId;

	private String firstName;

	private String lastName;

	private Double remainingLoanOfferedAmount;

	private Double disbursmentAmount;

	private String disbursmentDate;

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Double getRemainingLoanOfferedAmount() {
		return remainingLoanOfferedAmount;
	}

	public void setRemainingLoanOfferedAmount(Double remainingLoanOfferedAmount) {
		this.remainingLoanOfferedAmount = remainingLoanOfferedAmount;
	}

	public Double getDisbursmentAmount() {
		return disbursmentAmount;
	}

	public void setDisbursmentAmount(Double disbursmentAmount) {
		this.disbursmentAmount = disbursmentAmount;
	}

	public String getDisbursmentDate() {
		return disbursmentDate;
	}

	public void setDisbursmentDate(String disbursmentDate) {
		this.disbursmentDate = disbursmentDate;
	}

}
