package com.oxyloans.dto.response.user;

public class UserPendingQuereisResponseDto {
	
	private String pendingQuereis;
	
	private String respondedOn;
	
	private String resolvedBy;
	
	private String adminFileName;

	private String adminFilePath;
	
	private String adminScreenshotUrl;
	
	private Integer adminDocumentId;
	
	private String respondedBy;

	public String getPendingQuereis() {
		return pendingQuereis;
	}

	public void setPendingQuereis(String pendingQuereis) {
		this.pendingQuereis = pendingQuereis;
	}

	public String getRespondedOn() {
		return respondedOn;
	}

	public void setRespondedOn(String respondedOn) {
		this.respondedOn = respondedOn;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public String getAdminFileName() {
		return adminFileName;
	}

	public void setAdminFileName(String adminFileName) {
		this.adminFileName = adminFileName;
	}

	public String getAdminFilePath() {
		return adminFilePath;
	}

	public void setAdminFilePath(String adminFilePath) {
		this.adminFilePath = adminFilePath;
	}

	public String getAdminScreenshotUrl() {
		return adminScreenshotUrl;
	}

	public void setAdminScreenshotUrl(String adminScreenshotUrl) {
		this.adminScreenshotUrl = adminScreenshotUrl;
	}

	public Integer getAdminDocumentId() {
		return adminDocumentId;
	}

	public void setAdminDocumentId(Integer adminDocumentId) {
		this.adminDocumentId = adminDocumentId;
	}

	public String getRespondedBy() {
		return respondedBy;
	}

	public void setRespondedBy(String respondedBy) {
		this.respondedBy = respondedBy;
	}
	
	

}
