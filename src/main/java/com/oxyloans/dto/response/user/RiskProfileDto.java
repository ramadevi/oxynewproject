package com.oxyloans.dto.response.user;

public class RiskProfileDto {
private Integer salaryOrIncome;

private Integer CompanyOrOrganization;

private Integer experianceOrExistenceOfOrganization;

private Integer cibilScore;

private Integer totalScore;

private String grade;

private Integer userId;

public Integer getSalaryOrIncome() {
	return salaryOrIncome;
}

public Integer getCompanyOrOrganization() {
	return CompanyOrOrganization;
}

public Integer getExperianceOrExistenceOfOrganization() {
	return experianceOrExistenceOfOrganization;
}

public Integer getCibilScore() {
	return cibilScore;
}

public Integer getTotalScore() {
	return totalScore;
}

public String getGrade() {
	return grade;
}

public Integer getUserId() {
	return userId;
}

public void setSalaryOrIncome(Integer salaryOrIncome) {
	this.salaryOrIncome = salaryOrIncome;
}

public void setCompanyOrOrganization(Integer companyOrOrganization) {
	CompanyOrOrganization = companyOrOrganization;
}

public void setExperianceOrExistenceOfOrganization(Integer experianceOrExistenceOfOrganization) {
	this.experianceOrExistenceOfOrganization = experianceOrExistenceOfOrganization;
}

public void setCibilScore(Integer cibilScore) {
	this.cibilScore = cibilScore;
}

public void setTotalScore(Integer totalScore) {
	this.totalScore = totalScore;
}

public void setGrade(String grade) {
	this.grade = grade;
}

public void setUserId(Integer userId) {
	this.userId = userId;
}

}
