package com.oxyloans.dto.response.user;

import java.util.List;

public class ActiveLenderResponseDto {

	private Integer totalCount;

	private List<LenderWalletInfo> lenderWalletInfo;

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public List<LenderWalletInfo> getLenderWalletInfo() {
		return lenderWalletInfo;
	}

	public void setLenderWalletInfo(List<LenderWalletInfo> lenderWalletInfo) {
		this.lenderWalletInfo = lenderWalletInfo;
	}
}
