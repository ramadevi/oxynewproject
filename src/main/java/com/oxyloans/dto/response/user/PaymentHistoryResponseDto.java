package com.oxyloans.dto.response.user;

import java.math.BigInteger;
import java.util.List;

import com.oxyloans.request.LenderPayuDto;

public class PaymentHistoryResponseDto {

	private String applicationId;

	private double amount;

	private String amountPaidDate;

	private String currentDate;

	private Double amountThroughEcs;

	private Double amountThroughPaymentScreenShot;

	private Double amountThroughPayU;

	private Double partPayment;

	private Double fullPayment;

	private Double amountThroughIdfc;

	private Double amountOnFifthEcs;

	private Double amountOnTwentyEcs;

	private Double IdfcAmountOnFifth;

	private Double IdfcAmountOnTenth;

	private Double paymentScreenshotFullPayment;

	private Double paymentScreenshotPartPayment;

	private Double totalAmount;

	private Double totalEcsAmount;

	private Double totalProcessingFee;

	private Double totalLenderFeeThroughPayu;

	private Double lenderFeeThroughPayu;

	private List<BorrowerProcessingFeeResponseDto> listOfBorrowersIncludingFee;

	private List<PaymentHistoryRequestDto> amountFromSreedeviHyundai;

	private List<PaymentHistoryRequestDto> amountFromLuckyHyundai;

	private List<PaymentHistoryRequestDto> amountFromTollGates;

	private List<PaymentHistoryRequestDto> amountFromPadiminiBollineni;

	private List<PaymentHistoryRequestDto> amountFromSharanyaLodge;

	private List<PaymentHistoryRequestDto> amountFromGongineniAdvertising;

	private List<PaymentHistoryRequestDto> amountFromCitly;

	private List<PaymentHistoryRequestDto> amountFromLakshminrusimhamVenkata;

	private List<PaymentHistoryRequestDto> amountFromSaralTalwar;

	private List<PaymentHistoryRequestDto> amountFromMahenderReddy;

	private List<LenderPayuDto> lenderPayuDetails;

	private BigInteger sumOfPaytmAmount;

	public Double getAmountOnFifthEcs() {
		return amountOnFifthEcs;
	}

	public void setAmountOnFifthEcs(Double amountOnFifthEcs) {
		this.amountOnFifthEcs = amountOnFifthEcs;
	}

	public Double getAmountOnTwentyEcs() {
		return amountOnTwentyEcs;
	}

	public void setAmountOnTwentyEcs(Double amountOnTwentyEcs) {
		this.amountOnTwentyEcs = amountOnTwentyEcs;
	}

	public Double getIdfcAmountOnFifth() {
		return IdfcAmountOnFifth;
	}

	public void setIdfcAmountOnFifth(Double idfcAmountOnFifth) {
		IdfcAmountOnFifth = idfcAmountOnFifth;
	}

	public Double getIdfcAmountOnTenth() {
		return IdfcAmountOnTenth;
	}

	public void setIdfcAmountOnTenth(Double idfcAmountOnTenth) {
		IdfcAmountOnTenth = idfcAmountOnTenth;
	}

	public Double getPaymentScreenshotFullPayment() {
		return paymentScreenshotFullPayment;
	}

	public void setPaymentScreenshotFullPayment(Double paymentScreenshotFullPayment) {
		this.paymentScreenshotFullPayment = paymentScreenshotFullPayment;
	}

	public Double getPaymentScreenshotPartPayment() {
		return paymentScreenshotPartPayment;
	}

	public void setPaymentScreenshotPartPayment(Double paymentScreenshotPartPayment) {
		this.paymentScreenshotPartPayment = paymentScreenshotPartPayment;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getTotalEcsAmount() {
		return totalEcsAmount;
	}

	public void setTotalEcsAmount(Double totalEcsAmount) {
		this.totalEcsAmount = totalEcsAmount;
	}

	public Double getAmountThroughIdfc() {
		return amountThroughIdfc;
	}

	public void setAmountThroughIdfc(Double amountThroughIdfc) {
		this.amountThroughIdfc = amountThroughIdfc;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getAmountPaidDate() {
		return amountPaidDate;
	}

	public void setAmountPaidDate(String amountPaidDate) {
		this.amountPaidDate = amountPaidDate;
	}

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public Double getAmountThroughEcs() {
		return amountThroughEcs;
	}

	public void setAmountThroughEcs(Double amountThroughEcs) {
		this.amountThroughEcs = amountThroughEcs;
	}

	public Double getAmountThroughPaymentScreenShot() {
		return amountThroughPaymentScreenShot;
	}

	public void setAmountThroughPaymentScreenShot(Double amountThroughPaymentScreenShot) {
		this.amountThroughPaymentScreenShot = amountThroughPaymentScreenShot;
	}

	public Double getAmountThroughPayU() {
		return amountThroughPayU;
	}

	public void setAmountThroughPayU(Double amountThroughPayU) {
		this.amountThroughPayU = amountThroughPayU;
	}

	public Double getPartPayment() {
		return partPayment;
	}

	public void setPartPayment(Double partPayment) {
		this.partPayment = partPayment;
	}

	public Double getFullPayment() {
		return fullPayment;
	}

	public void setFullPayment(Double fullPayment) {
		this.fullPayment = fullPayment;
	}

	public Double getTotalProcessingFee() {
		return totalProcessingFee;
	}

	public void setTotalProcessingFee(Double totalProcessingFee) {
		this.totalProcessingFee = totalProcessingFee;
	}

	public List<BorrowerProcessingFeeResponseDto> getListOfBorrowersIncludingFee() {
		return listOfBorrowersIncludingFee;
	}

	public void setListOfBorrowersIncludingFee(List<BorrowerProcessingFeeResponseDto> listOfBorrowersIncludingFee) {
		this.listOfBorrowersIncludingFee = listOfBorrowersIncludingFee;
	}

	public List<PaymentHistoryRequestDto> getAmountFromSreedeviHyundai() {
		return amountFromSreedeviHyundai;
	}

	public void setAmountFromSreedeviHyundai(List<PaymentHistoryRequestDto> amountFromSreedeviHyundai) {
		this.amountFromSreedeviHyundai = amountFromSreedeviHyundai;
	}

	public List<PaymentHistoryRequestDto> getAmountFromLuckyHyundai() {
		return amountFromLuckyHyundai;
	}

	public void setAmountFromLuckyHyundai(List<PaymentHistoryRequestDto> amountFromLuckyHyundai) {
		this.amountFromLuckyHyundai = amountFromLuckyHyundai;
	}

	public List<PaymentHistoryRequestDto> getAmountFromTollGates() {
		return amountFromTollGates;
	}

	public void setAmountFromTollGates(List<PaymentHistoryRequestDto> amountFromTollGates) {
		this.amountFromTollGates = amountFromTollGates;
	}

	public List<PaymentHistoryRequestDto> getAmountFromPadiminiBollineni() {
		return amountFromPadiminiBollineni;
	}

	public void setAmountFromPadiminiBollineni(List<PaymentHistoryRequestDto> amountFromPadiminiBollineni) {
		this.amountFromPadiminiBollineni = amountFromPadiminiBollineni;
	}

	public List<PaymentHistoryRequestDto> getAmountFromSharanyaLodge() {
		return amountFromSharanyaLodge;
	}

	public void setAmountFromSharanyaLodge(List<PaymentHistoryRequestDto> amountFromSharanyaLodge) {
		this.amountFromSharanyaLodge = amountFromSharanyaLodge;
	}

	public List<PaymentHistoryRequestDto> getAmountFromGongineniAdvertising() {
		return amountFromGongineniAdvertising;
	}

	public void setAmountFromGongineniAdvertising(List<PaymentHistoryRequestDto> amountFromGongineniAdvertising) {
		this.amountFromGongineniAdvertising = amountFromGongineniAdvertising;
	}

	public List<PaymentHistoryRequestDto> getAmountFromCitly() {
		return amountFromCitly;
	}

	public void setAmountFromCitly(List<PaymentHistoryRequestDto> amountFromCitly) {
		this.amountFromCitly = amountFromCitly;
	}

	public List<PaymentHistoryRequestDto> getAmountFromLakshminrusimhamVenkata() {
		return amountFromLakshminrusimhamVenkata;
	}

	public void setAmountFromLakshminrusimhamVenkata(List<PaymentHistoryRequestDto> amountFromLakshminrusimhamVenkata) {
		this.amountFromLakshminrusimhamVenkata = amountFromLakshminrusimhamVenkata;
	}

	public List<PaymentHistoryRequestDto> getAmountFromSaralTalwar() {
		return amountFromSaralTalwar;
	}

	public void setAmountFromSaralTalwar(List<PaymentHistoryRequestDto> amountFromSaralTalwar) {
		this.amountFromSaralTalwar = amountFromSaralTalwar;
	}

	public List<PaymentHistoryRequestDto> getAmountFromMahenderReddy() {
		return amountFromMahenderReddy;
	}

	public void setAmountFromMahenderReddy(List<PaymentHistoryRequestDto> amountFromMahenderReddy) {
		this.amountFromMahenderReddy = amountFromMahenderReddy;
	}

	public Double getTotalLenderFeeThroughPayu() {
		return totalLenderFeeThroughPayu;
	}

	public void setTotalLenderFeeThroughPayu(Double totalLenderFeeThroughPayu) {
		this.totalLenderFeeThroughPayu = totalLenderFeeThroughPayu;
	}

	public Double getLenderFeeThroughPayu() {
		return lenderFeeThroughPayu;
	}

	public void setLenderFeeThroughPayu(Double lenderFeeThroughPayu) {
		this.lenderFeeThroughPayu = lenderFeeThroughPayu;
	}

	public List<LenderPayuDto> getLenderPayuDetails() {
		return lenderPayuDetails;
	}

	public void setLenderPayuDetails(List<LenderPayuDto> lenderPayuDetails) {
		this.lenderPayuDetails = lenderPayuDetails;
	}

	public BigInteger getSumOfPaytmAmount() {
		return sumOfPaytmAmount;
	}

	public void setSumOfPaytmAmount(BigInteger sumOfPaytmAmount) {
		this.sumOfPaytmAmount = sumOfPaytmAmount;
	}

}
