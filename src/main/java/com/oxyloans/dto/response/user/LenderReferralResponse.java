package com.oxyloans.dto.response.user;

import java.util.List;

public class LenderReferralResponse {

	private Integer refereeId;

	private Double amount;

	private Integer id;
	
    private String refereeIds;
	
	private String dealIds;
	
	private String ids;
	
	private List<String> dealAndRefereePairs;

	
	
	public List<String> getDealAndRefereePairs() {
		return dealAndRefereePairs;
	}

	public void setDealAndRefereePairs(List<String> dealAndRefereePairs) {
		this.dealAndRefereePairs = dealAndRefereePairs;
	}

	public String getRefereeIds() {
		return refereeIds;
	}

	public void setRefereeIds(String refereeIds) {
		this.refereeIds = refereeIds;
	}

	public String getDealIds() {
		return dealIds;
	}

	public void setDealIds(String dealIds) {
		this.dealIds = dealIds;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public Integer getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(Integer refereeId) {
		this.refereeId = refereeId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
