package com.oxyloans.dto.response.user;

import java.util.Date;

public class UserPersonalDetailsResponse {

	private Integer id;

	private String firstName;

	private Integer groupId;

	private String groupName;

	private boolean lenderValidityStatus;

	private double lenderWalletAmount = 0.0;

	private Date validityDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public boolean isLenderValidityStatus() {
		return lenderValidityStatus;
	}

	public void setLenderValidityStatus(boolean lenderValidityStatus) {
		this.lenderValidityStatus = lenderValidityStatus;
	}

	public double getLenderWalletAmount() {
		return lenderWalletAmount;
	}

	public void setLenderWalletAmount(double lenderWalletAmount) {
		this.lenderWalletAmount = lenderWalletAmount;
	}

	public Date getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(java.util.Date date) {
		this.validityDate = date;
	}

}
