package com.oxyloans.dto.response.user;

import java.util.List;

import com.oxyloans.request.user.LenderReferralsResponse;

public class LenderReferenceAmountResponseDto {

	private List<LenderReferenceAmountResponse> lenderReferenceAmountResponse;
	
	private List<LenderReferralsResponse> lenderReferralsResponse;

	private Double sumOfDisbursementAmount;

	private Integer countOfDisbursments;

	private List<LenderReferenceResponseDto> listOfLenderReferenceResponseDto;

	private Double sumOfEarnedAmount;

	private String downloadLink;

	private Integer count;
	
	private Integer totalAmountEarned;

	private Integer sumOfPaidAmount;

	private Integer sumOfUnpaidAmount;
	
	
	public List<LenderReferralsResponse> getLenderReferralsResponse() {
		return lenderReferralsResponse;
	}

	public void setLenderReferralsResponse(List<LenderReferralsResponse> lenderReferralsResponse) {
		this.lenderReferralsResponse = lenderReferralsResponse;
	}

	public List<LenderReferenceAmountResponse> getLenderReferenceAmountResponse() {
		return lenderReferenceAmountResponse;
	}

	public void setLenderReferenceAmountResponse(List<LenderReferenceAmountResponse> lenderReferenceAmountResponse) {
		this.lenderReferenceAmountResponse = lenderReferenceAmountResponse;
	}

	public Double getSumOfDisbursementAmount() {
		return sumOfDisbursementAmount;
	}

	public void setSumOfDisbursementAmount(Double sumOfDisbursementAmount) {
		this.sumOfDisbursementAmount = sumOfDisbursementAmount;
	}

	public Integer getCountOfDisbursments() {
		return countOfDisbursments;
	}

	public void setCountOfDisbursments(Integer countOfDisbursments) {
		this.countOfDisbursments = countOfDisbursments;
	}

	public List<LenderReferenceResponseDto> getListOfLenderReferenceResponseDto() {
		return listOfLenderReferenceResponseDto;
	}

	public void setListOfLenderReferenceResponseDto(List<LenderReferenceResponseDto> listOfLenderReferenceResponseDto) {
		this.listOfLenderReferenceResponseDto = listOfLenderReferenceResponseDto;
	}

	public Double getSumOfEarnedAmount() {
		return sumOfEarnedAmount;
	}

	public void setSumOfEarnedAmount(Double sumOfEarnedAmount) {
		this.sumOfEarnedAmount = sumOfEarnedAmount;
	}

	public String getDownloadLink() {
		return downloadLink;
	}

	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getTotalAmountEarned() {
		return totalAmountEarned;
	}

	public void setTotalAmountEarned(Integer totalAmountEarned) {
		this.totalAmountEarned = totalAmountEarned;
	}

	public Integer getSumOfPaidAmount() {
		return sumOfPaidAmount;
	}

	public void setSumOfPaidAmount(Integer sumOfPaidAmount) {
		this.sumOfPaidAmount = sumOfPaidAmount;
	}

	public Integer getSumOfUnpaidAmount() {
		return sumOfUnpaidAmount;
	}

	public void setSumOfUnpaidAmount(Integer sumOfUnpaidAmount) {
		this.sumOfUnpaidAmount = sumOfUnpaidAmount;
	}

}
