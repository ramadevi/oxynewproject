package com.oxyloans.dto.response.user;


import java.util.List;

import com.oxyloans.request.user.AddressDetailsRequestDto;

public class AddressDetailsResponseDto  extends AddressDetailsRequestDto   {
	
	private Integer id;

	private Integer UserId;

	private String type;
	
	private String houseNumber;

	private String street;

	private String area;

	private String city;

	public Integer getUserId() {
		return UserId;
	}

	public void setUserId(Integer userId) {
		UserId = userId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void addObject(String string, List<String> list) {
		// TODO Auto-generated method stub
		
	}

	
	


}
