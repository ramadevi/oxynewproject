package com.oxyloans.dto.response.user;

public class PaymentUploadHistoryRequestDto {

	private String borrowerUniqueNumber;

	private String borrowerName;

	private Integer documentUploadId;

	private Double amount;

	private String updatedName;

	private String paidDate;

	private String paymentType;

	private Integer userId;

	private String status;

	private String comments;
	
	private String messageNumber;

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getBorrowerUniqueNumber() {
		return borrowerUniqueNumber;
	}

	public void setBorrowerUniqueNumber(String borrowerUniqueNumber) {
		this.borrowerUniqueNumber = borrowerUniqueNumber;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public Integer getDocumentUploadId() {
		return documentUploadId;
	}

	public void setDocumentUploadId(Integer documentUploadId) {
		this.documentUploadId = documentUploadId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getUpdatedName() {
		return updatedName;
	}

	public void setUpdatedName(String updatedName) {
		this.updatedName = updatedName;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessageNumber() {
		return messageNumber;
	}

	public void setMessageNumber(String messageNumber) {
		this.messageNumber = messageNumber;
	}

}
