package com.oxyloans.dto.response.user;

import java.util.Date;

public class EmisPaidInformation {

	private Double sNo;

	private String borrowerID;

	private Date disbursedDate;

	private String borrowerName;

	private Double loanAmount;

	private Double rateOfInaterest;

	private Integer duration;

	private Date emiStartDate;

	private Integer emisToBepaidTillDate;

	private Integer emisPending;

	private Integer featureEmis;

	private Double penalityperEmi;

	private Double amountPending;

	private Double totalPenality;

	private Double partPayment;

	private Double totalAmountPendingWithpenality;

	private Double totalAmountPendingWithpartPayment;

	private Integer noOfeEmisPaid;

	private Double emiAmount;

	public Double getsNo() {
		return sNo;
	}

	public void setsNo(Double sNo) {
		this.sNo = sNo;
	}

	public String getBorrowerID() {
		return borrowerID;
	}

	public void setBorrowerID(String borrowerID) {
		this.borrowerID = borrowerID;
	}

	public Date getDisbursedDate() {
		return disbursedDate;
	}

	public void setDisbursedDate(Date disbursedDate) {
		this.disbursedDate = disbursedDate;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public Double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public Double getRateOfInaterest() {
		return rateOfInaterest;
	}

	public void setRateOfInaterest(Double rateOfInaterest) {
		this.rateOfInaterest = rateOfInaterest;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Date getEmiStartDate() {
		return emiStartDate;
	}

	public void setEmiStartDate(Date emiStartDate) {
		this.emiStartDate = emiStartDate;
	}

	public Integer getEmisToBepaidTillDate() {
		return emisToBepaidTillDate;
	}

	public void setEmisToBepaidTillDate(Integer emisToBepaidTillDate) {
		this.emisToBepaidTillDate = emisToBepaidTillDate;
	}

	public Integer getEmisPending() {
		return emisPending;
	}

	public void setEmisPending(Integer emisPending) {
		this.emisPending = emisPending;
	}

	public Integer getFeatureEmis() {
		return featureEmis;
	}

	public void setFeatureEmis(Integer featureEmis) {
		this.featureEmis = featureEmis;
	}

	public Double getPenalityperEmi() {
		return penalityperEmi;
	}

	public void setPenalityperEmi(Double penalityperEmi) {
		this.penalityperEmi = penalityperEmi;
	}

	public Double getAmountPending() {
		return amountPending;
	}

	public void setAmountPending(Double amountPending) {
		this.amountPending = amountPending;
	}

	public Double getTotalPenality() {
		return totalPenality;
	}

	public void setTotalPenality(Double totalPenality) {
		this.totalPenality = totalPenality;
	}

	public Double getPartPayment() {
		return partPayment;
	}

	public void setPartPayment(Double partPayment) {
		this.partPayment = partPayment;
	}

	public Double getTotalAmountPendingWithpenality() {
		return totalAmountPendingWithpenality;
	}

	public void setTotalAmountPendingWithpenality(Double totalAmountPendingWithpenality) {
		this.totalAmountPendingWithpenality = totalAmountPendingWithpenality;
	}

	public Double getTotalAmountPendingWithpartPayment() {
		return totalAmountPendingWithpartPayment;
	}

	public void setTotalAmountPendingWithpartPayment(Double totalAmountPendingWithpartPayment) {
		this.totalAmountPendingWithpartPayment = totalAmountPendingWithpartPayment;
	}

	public Integer getNoOfeEmisPaid() {
		return noOfeEmisPaid;
	}

	public void setNoOfeEmisPaid(Integer noOfeEmisPaid) {
		this.noOfeEmisPaid = noOfeEmisPaid;
	}

	public Double getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(Double emiAmount) {
		this.emiAmount = emiAmount;
	}

}
