package com.oxyloans.dto.response.user;

public class PaytmReversalApiRequestDto {

	private String bankTxnIdentifier;

	private String remitterIfsc;

	private String transactionType;

	private String transactionRequestId;

	private double amount;

	public String getBankTxnIdentifier() {
		return bankTxnIdentifier;
	}

	public void setBankTxnIdentifier(String bankTxnIdentifier) {
		this.bankTxnIdentifier = bankTxnIdentifier;
	}

	public String getRemitterIfsc() {
		return remitterIfsc;
	}

	public void setRemitterIfsc(String remitterIfsc) {
		this.remitterIfsc = remitterIfsc;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionRequestId() {
		return transactionRequestId;
	}

	public void setTransactionRequestId(String transactionRequestId) {
		this.transactionRequestId = transactionRequestId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
