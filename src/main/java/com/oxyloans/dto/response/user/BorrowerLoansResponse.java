package com.oxyloans.dto.response.user;

public class BorrowerLoansResponse {

	private String loanid;
	private int lender_id;
	private String lender_name;
	private double disbursment_amount;

	public String getLoanid() {
		return loanid;
	}

	public void setLoanid(String loanid) {
		this.loanid = loanid;
	}

	public int getLender_id() {
		return lender_id;
	}

	public void setLender_id(int lender_id) {
		this.lender_id = lender_id;
	}

	public String getLender_name() {
		return lender_name;
	}

	public void setLender_name(String lender_name) {
		this.lender_name = lender_name;
	}

	public double getDisbursment_amount() {
		return disbursment_amount;
	}

	public void setDisbursment_amount(double disbursment_amount) {
		this.disbursment_amount = disbursment_amount;
	}
}
