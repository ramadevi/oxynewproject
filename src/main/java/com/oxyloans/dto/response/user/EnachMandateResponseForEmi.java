package com.oxyloans.dto.response.user;

import java.util.Date;

public class EnachMandateResponseForEmi {
	private int mandateId;
	private Double amount;
	private Date startDate;
	private Date endDate;
	private String mandateStatus;

	public int getMandateId() {
		return mandateId;
	}

	public void setMandateId(int mandateId) {
		this.mandateId = mandateId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getMandateStatus() {
		return mandateStatus;
	}

	public void setMandateStatus(String mandateStatus) {
		this.mandateStatus = mandateStatus;
	}

}
