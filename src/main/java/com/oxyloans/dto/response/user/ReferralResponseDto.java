package com.oxyloans.dto.response.user;

public class ReferralResponseDto {

	private Integer userId;
	
	private Double totalEarnedAmount;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Double getTotalEarnedAmount() {
		return totalEarnedAmount;
	}

	public void setTotalEarnedAmount(Double totalEarnedAmount) {
		this.totalEarnedAmount = totalEarnedAmount;
	}
	
	
}
