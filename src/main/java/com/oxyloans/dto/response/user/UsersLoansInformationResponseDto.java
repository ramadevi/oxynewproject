package com.oxyloans.dto.response.user;

public class UsersLoansInformationResponseDto {

	private Integer lenderId;

	private String lenderName;

	private double totalDisbursementAmount;

	private double totalPrincipalRecevied;

	private double totalProfit;

	private double totalOutstandingAmount;

	private String individualLoansInformation;

	private String borrowerName;

	private String loanId;

	private double individualDisburmentAmount;

	private double individualPrincipalPaid;

	private double individualProfitPaid;

	private double individualOutstanding;

	private String fileName;

	private String downloadUrl;

	private Integer countOfIndividualLoans;

	private Integer borrowerId;

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public double getTotalDisbursementAmount() {
		return totalDisbursementAmount;
	}

	public void setTotalDisbursementAmount(double totalDisbursementAmount) {
		this.totalDisbursementAmount = totalDisbursementAmount;
	}

	public double getTotalPrincipalRecevied() {
		return totalPrincipalRecevied;
	}

	public void setTotalPrincipalRecevied(double totalPrincipalRecevied) {
		this.totalPrincipalRecevied = totalPrincipalRecevied;
	}

	public double getTotalProfit() {
		return totalProfit;
	}

	public void setTotalProfit(double totalProfit) {
		this.totalProfit = totalProfit;
	}

	public double getTotalOutstandingAmount() {
		return totalOutstandingAmount;
	}

	public void setTotalOutstandingAmount(double totalOutstandingAmount) {
		this.totalOutstandingAmount = totalOutstandingAmount;
	}

	public String getIndividualLoansInformation() {
		return individualLoansInformation;
	}

	public void setIndividualLoansInformation(String individualLoansInformation) {
		this.individualLoansInformation = individualLoansInformation;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public double getIndividualDisburmentAmount() {
		return individualDisburmentAmount;
	}

	public void setIndividualDisburmentAmount(double individualDisburmentAmount) {
		this.individualDisburmentAmount = individualDisburmentAmount;
	}

	public double getIndividualPrincipalPaid() {
		return individualPrincipalPaid;
	}

	public void setIndividualPrincipalPaid(double individualPrincipalPaid) {
		this.individualPrincipalPaid = individualPrincipalPaid;
	}

	public double getIndividualProfitPaid() {
		return individualProfitPaid;
	}

	public void setIndividualProfitPaid(double individualProfitPaid) {
		this.individualProfitPaid = individualProfitPaid;
	}

	public double getIndividualOutstanding() {
		return individualOutstanding;
	}

	public void setIndividualOutstanding(double individualOutstanding) {
		this.individualOutstanding = individualOutstanding;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public Integer getCountOfIndividualLoans() {
		return countOfIndividualLoans;
	}

	public void setCountOfIndividualLoans(Integer countOfIndividualLoans) {
		this.countOfIndividualLoans = countOfIndividualLoans;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}
}
