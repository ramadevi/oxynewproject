package com.oxyloans.dto.response.user;

public class EquityNewLendersDto {

	private int lenderId;

	private String lenderName;

	private int groupId;

	private String groupName;
	
	private int dealId;
	
	private String dealName;
	
	private Double participatedAmount;

	public int getLenderId() {
		return lenderId;
	}

	public void setLenderId(int lenderId) {
		this.lenderId = lenderId;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public Double getParticipatedAmount() {
		return participatedAmount;
	}

	public void setParticipatedAmount(Double participatedAmount) {
		this.participatedAmount = participatedAmount;
	}

	
	
}
