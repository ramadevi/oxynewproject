package com.oxyloans.dto.response.user;

public class LenderRatingResponseDto {
	private Integer id;

	private Integer rating;

	private Integer lenderId;

	private Integer parentRequestId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public Integer getParentRequestId() {
		return parentRequestId;
	}

	public void setParentRequestId(Integer parentRequestId) {
		this.parentRequestId = parentRequestId;
	}
}
