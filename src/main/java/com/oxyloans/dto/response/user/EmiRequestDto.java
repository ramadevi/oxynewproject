package com.oxyloans.dto.response.user;

public class EmiRequestDto {
	private String emiTable;

	private double totalInterestAmount;

	private double payableAmount;

	private double diminishingAmount;

	private double oneInterestAmount;

	private double emiAmount;

	public String getEmiTable() {
		return emiTable;
	}

	public void setEmiTable(String emiTable) {
		this.emiTable = emiTable;
	}

	public double getTotalInterestAmount() {
		return totalInterestAmount;
	}

	public void setTotalInterestAmount(double totalInterestAmount) {
		this.totalInterestAmount = totalInterestAmount;
	}

	public double getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(double payableAmount) {
		this.payableAmount = payableAmount;
	}

	public double getDiminishingAmount() {
		return diminishingAmount;
	}

	public void setDiminishingAmount(double diminishingAmount) {
		this.diminishingAmount = diminishingAmount;
	}

	public double getOneInterestAmount() {
		return oneInterestAmount;
	}

	public void setOneInterestAmount(double oneInterestAmount) {
		this.oneInterestAmount = oneInterestAmount;
	}

	public double getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(double emiAmount) {
		this.emiAmount = emiAmount;
	}

}
