package com.oxyloans.dto.response.user;

public class PaytmRequestDto {

	private String iss = "PAYTM";

	private String timestamp;

	private String partnerId = "VAN_OXY_000212";

	private String requestReferenceId = "REQOXTTRADE";

	public String getIss() {
		return iss;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public String getRequestReferenceId() {
		return requestReferenceId;
	}

	public void setIss(String iss) {
		this.iss = iss;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public void setRequestReferenceId(String requestReferenceId) {
		this.requestReferenceId = requestReferenceId;
	}

}
