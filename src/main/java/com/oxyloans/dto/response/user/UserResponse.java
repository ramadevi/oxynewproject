package com.oxyloans.dto.response.user;

import java.util.Date;
import java.util.List;

import com.oxyloans.request.user.UserRequest;

public class UserResponse extends UserRequest {

	private Integer id;

	private boolean emailSent;

	private boolean emailVerified;

	private String accessToken;

	private String name;

	private String email;

	private String contactNumber;

	private String companyName;

	private String remarks;

	private Date profileCreatedOn;

	private String primaryType;

	private String firstName;

	private String lastName;

	private boolean personalDetailsFilled = false;

	private boolean kycUploaded = false;

	private String profilePicUrl;

	private String aadharFileUrl;

	private String panUrl;

	private String passportUrl;

	private String address;

	private String status;

	private String adminComments;

	private String mobileOtpSession;

	private boolean mobileverified;

	private String displayId;

	private double loanAmountRequest;

	private double borrowerFee;

	private Integer paymentFeeRateOfInterest;

	private double gst;

	private double amountForGivenInterest;

	private String employment;

	private String workExperience;

	private Integer Salary;

	private int experinaScore;

	private String experinace;

	private Integer netIncome;

	private String organization;

	private String organizationFound;

	private String employementType;

	private Integer documentId;

	private String accountNumber;

	private String bankName;

	private String branchName;

	private String ifscCode;

	private String bankAddress;

	private RiskProfileDto riskProfileDto;

	private Integer creditScoreByPaisabazaar;

	private String facebookUrl;

	private String twitterUrl;

	private String linkedinUrl;

	private List<String> utm;

	// private String enachType;

	private NomineeResponseDto nomineeResponseDto;

	private BankDetailsResponseDto bankDetailsResponseDto;

	private String uniqueNumber;

	private CommentsRequestDto commentsRequestDto;

	private String panNumber;

	private String dob;

	private String vanNumber;

	private String whatsappHash;

	private String emailOtpSession;

	private String adviseSeekersSalt;

	private String chequeLeafUrl;

	private String documentDriveLink;

	private String registeredOn;

	private long tokenGeneratedTime;

	private boolean validityCheck;
	
	private boolean mobileNumberVerified;
	
	private String originalDob;
	
	
	public String getOriginalDob() {
		return originalDob;
	}

	public void setOriginalDob(String originalDob) {
		this.originalDob = originalDob;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(String uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public BankDetailsResponseDto getBankDetailsResponseDto() {
		return bankDetailsResponseDto;
	}

	public void setBankDetailsResponseDto(BankDetailsResponseDto bankDetailsResponseDto) {
		this.bankDetailsResponseDto = bankDetailsResponseDto;
	}

	public NomineeResponseDto getNomineeResponseDto() {
		return nomineeResponseDto;
	}

	public void setNomineeResponseDto(NomineeResponseDto nomineeResponseDto) {
		this.nomineeResponseDto = nomineeResponseDto;
	}

	public List<String> getUtm() {
		return utm;
	}

	public void setUtm(List<String> utm) {
		this.utm = utm;
	}

	public String getFacebookUrl() {
		return facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getTwitterUrl() {
		return twitterUrl;
	}

	public void setTwitterUrl(String twitterUrl) {
		this.twitterUrl = twitterUrl;
	}

	public String getLinkedinUrl() {
		return linkedinUrl;
	}

	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}

	public Integer getCreditScoreByPaisabazaar() {
		return creditScoreByPaisabazaar;
	}

	public void setCreditScoreByPaisabazaar(Integer creditScoreByPaisabazaar) {
		this.creditScoreByPaisabazaar = creditScoreByPaisabazaar;
	}

	public RiskProfileDto getRiskProfileDto() {
		return riskProfileDto;
	}

	public void setRiskProfileDto(RiskProfileDto riskProfileDto) {
		this.riskProfileDto = riskProfileDto;
	}

	private String userNameAccordingToBank;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getUserNameAccordingToBank() {
		return userNameAccordingToBank;
	}

	public void setUserNameAccordingToBank(String userNameAccordingToBank) {
		this.userNameAccordingToBank = userNameAccordingToBank;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public String getEmployementType() {
		return employementType;
	}

	public void setEmployementType(String employementType) {
		this.employementType = employementType;
	}

	public Integer getNetIncome() {
		return netIncome;
	}

	public void setNetIncome(Integer netIncome) {
		this.netIncome = netIncome;
	}

	public String getOrganization() {
		return organization;
	}

	public String getOrganizationFound() {
		return organizationFound;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public void setOrganizationFound(String organizationFound) {
		this.organizationFound = organizationFound;
	}

	public String getExperinace() {
		return experinace;
	}

	public void setExperinace(String experinace) {
		this.experinace = experinace;
	}

	public int getExperinaScore() {
		return experinaScore;
	}

	public void setExperinaScore(int experinaScore) {
		this.experinaScore = experinaScore;
	}

	public String getEmployment() {
		return employment;
	}

	public String getWorkExperience() {
		return workExperience;
	}

	public Integer getSalary() {
		return Salary;
	}

	public void setSalary(Integer salary) {
		Salary = salary;
	}

	public void setEmployment(String employment) {
		this.employment = employment;
	}

	public void setWorkExperience(String workExperience) {
		this.workExperience = workExperience;
	}

	public double getGst() {
		return gst;
	}

	public void setGst(double gst) {
		this.gst = gst;
	}

	public double getAmountForGivenInterest() {
		return amountForGivenInterest;
	}

	public void setAmountForGivenInterest(double amountForGivenInterest) {
		this.amountForGivenInterest = amountForGivenInterest;
	}

	public double getBorrowerFee() {
		return borrowerFee;
	}

	public void setBorrowerFee(double borrowerFee) {
		this.borrowerFee = borrowerFee;
	}

	public double getLoanAmountRequest() {
		return loanAmountRequest;
	}

	public void setLoanAmountRequest(double loanAmountRequest) {
		this.loanAmountRequest = loanAmountRequest;
	}

	public Integer getPaymentFeeRateOfInterest() {
		return paymentFeeRateOfInterest;
	}

	public void setPaymentFeeRateOfInterest(Integer paymentFeeRateOfInterest) {
		this.paymentFeeRateOfInterest = paymentFeeRateOfInterest;
	}

	public String getDisplayId() {
		return displayId;
	}

	public void setDisplayId(String displayId) {
		this.displayId = displayId;
	}

	public boolean isMobileverified() {
		return mobileverified;
	}

	public void setMobileverified(boolean mobileverified) {
		this.mobileverified = mobileverified;
	}

	public String getMobileOtpSession() {
		return mobileOtpSession;
	}

	public void setMobileOtpSession(String mobileOtpSession) {
		this.mobileOtpSession = mobileOtpSession;
	}

	public String getAdminComments() {
		return adminComments;
	}

	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isEmailSent() {
		return emailSent;
	}

	public void setEmailSent(boolean emailSent) {
		this.emailSent = emailSent;
	}

	public boolean isEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getProfileCreatedOn() {
		return profileCreatedOn;
	}

	public void setProfileCreatedOn(Date profileCreatedOn) {
		this.profileCreatedOn = profileCreatedOn;
	}

	public String getPrimaryType() {
		return primaryType;
	}

	public void setPrimaryType(String primaryType) {
		this.primaryType = primaryType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isPersonalDetailsFilled() {
		return personalDetailsFilled;
	}

	public void setPersonalDetailsFilled(boolean personalDetailsFilled) {
		this.personalDetailsFilled = personalDetailsFilled;
	}

	public boolean isKycUploaded() {
		return kycUploaded;
	}

	public void setKycUploaded(boolean kycUploaded) {
		this.kycUploaded = kycUploaded;
	}

	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAadharFileUrl() {
		return aadharFileUrl;
	}

	public void setAadharFileUrl(String aadharFileUrl) {
		this.aadharFileUrl = aadharFileUrl;
	}

	public String getPanUrl() {
		return panUrl;
	}

	public void setPanUrl(String panUrl) {
		this.panUrl = panUrl;
	}

	public String getPassportUrl() {
		return passportUrl;
	}

	public void setPassportUrl(String passportUrl) {
		this.passportUrl = passportUrl;
	}

	public CommentsRequestDto getCommentsRequestDto() {
		return commentsRequestDto;
	}

	public void setCommentsRequestDto(CommentsRequestDto commentsRequestDto) {
		this.commentsRequestDto = commentsRequestDto;
	}

	public String getVanNumber() {
		return vanNumber;
	}

	public void setVanNumber(String vanNumber) {
		this.vanNumber = vanNumber;
	}

	public String getWhatsappHash() {
		return whatsappHash;
	}

	public void setWhatsappHash(String whatsappHash) {
		this.whatsappHash = whatsappHash;
	}

	public String getEmailOtpSession() {
		return emailOtpSession;
	}

	public void setEmailOtpSession(String emailOtpSession) {
		this.emailOtpSession = emailOtpSession;
	}

	public String getAdviseSeekersSalt() {
		return adviseSeekersSalt;
	}

	public void setAdviseSeekersSalt(String adviseSeekersSalt) {
		this.adviseSeekersSalt = adviseSeekersSalt;
	}

	public String getChequeLeafUrl() {
		return chequeLeafUrl;
	}

	public void setChequeLeafUrl(String chequeLeafUrl) {
		this.chequeLeafUrl = chequeLeafUrl;
	}

	public String getDocumentDriveLink() {
		return documentDriveLink;
	}

	public void setDocumentDriveLink(String documentDriveLink) {
		this.documentDriveLink = documentDriveLink;
	}

	public String getRegisteredOn() {
		return registeredOn;
	}

	public void setRegisteredOn(String registeredOn) {
		this.registeredOn = registeredOn;
	}

	public long getTokenGeneratedTime() {
		return tokenGeneratedTime;
	}

	public void setTokenGeneratedTime(long tokenGeneratedTime) {
		this.tokenGeneratedTime = tokenGeneratedTime;
	}

	public boolean isValidityCheck() {
		return validityCheck;
	}

	public void setValidityCheck(boolean validityCheck) {
		this.validityCheck = validityCheck;
	}

	public boolean isMobileNumberVerified() {
		return mobileNumberVerified;
	}

	public void setMobileNumberVerified(boolean mobileNumberVerified) {
		this.mobileNumberVerified = mobileNumberVerified;
	}

}
