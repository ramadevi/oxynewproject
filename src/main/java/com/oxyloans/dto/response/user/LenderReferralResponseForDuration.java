package com.oxyloans.dto.response.user;

public class LenderReferralResponseForDuration {

	private Integer refereeId;

	private Double amount;

	private Integer id;
	
	private Integer dealId;
	
	 private String ids1;
	
    public String getIds1() {
		return ids1;
	}

	public void setIds1(String ids1) {
		this.ids1 = ids1;
	}

	public String getDealIds() {
		return dealIds;
	}

	public void setDealIds(String dealIds) {
		this.dealIds = dealIds;
	}

	public String getRefereeIds() {
		return refereeIds;
	}

	public void setRefereeIds(String refereeIds) {
		this.refereeIds = refereeIds;
	}

	private String dealIds;
	
    private String refereeIds;

	public Integer getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(Integer refereeId) {
		this.refereeId = refereeId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}
	
	
	
}
