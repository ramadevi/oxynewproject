package com.oxyloans.dto.response.user;

public class BorrowersLoanOwnerNames {

	private String loanOwner;

	public String getLoanOwner() {
		return loanOwner;
	}

	public void setLoanOwner(String loanOwner) {
		this.loanOwner = loanOwner;
	}
}
