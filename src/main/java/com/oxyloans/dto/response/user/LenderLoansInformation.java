package com.oxyloans.dto.response.user;

public class LenderLoansInformation {

	private String lenderName;
	private String borrowerName;
	private String loanId;
	private String disbursementDate;
	private Double emiAmount;
	private String status;
	private Integer lenderId;
	private Integer borrowerId;
	private String lenderAccountNumber;
	private String emiDueOn;
	private String emiPaidOn;

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getDisbursementDate() {
		return disbursementDate;
	}

	public void setDisbursementDate(String disbursementDate) {
		this.disbursementDate = disbursementDate;
	}

	public Double getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(Double emiAmount) {
		this.emiAmount = emiAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public String getLenderAccountNumber() {
		return lenderAccountNumber;
	}

	public void setLenderAccountNumber(String lenderAccountNumber) {
		this.lenderAccountNumber = lenderAccountNumber;
	}

	public String getEmiDueOn() {
		return emiDueOn;
	}

	public void setEmiDueOn(String emiDueOn) {
		this.emiDueOn = emiDueOn;
	}

	public String getEmiPaidOn() {
		return emiPaidOn;
	}

	public void setEmiPaidOn(String emiPaidOn) {
		this.emiPaidOn = emiPaidOn;
	}

}
