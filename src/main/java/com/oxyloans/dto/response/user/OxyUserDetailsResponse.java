package com.oxyloans.dto.response.user;

import java.util.List;

import com.oxyloans.request.BorrowerUserDetails;
import com.oxyloans.request.LenderUserDetails;

public class OxyUserDetailsResponse {

	private List<LenderUserDetails> listOfLenders;
	
	private List<BorrowerUserDetails> listOfBorrowers;
	
	private Integer countOfLenders;
	
	private Integer countOfBorrowers;
	
	private String utmName;

	public List<LenderUserDetails> getListOfLenders() {
		return listOfLenders;
	}

	public void setListOfLenders(List<LenderUserDetails> listOfLenders) {
		this.listOfLenders = listOfLenders;
	}

	public List<BorrowerUserDetails> getListOfBorrowers() {
		return listOfBorrowers;
	}

	public void setListOfBorrowers(List<BorrowerUserDetails> listOfBorrowers) {
		this.listOfBorrowers = listOfBorrowers;
	}

	public Integer getCountOfLenders() {
		return countOfLenders;
	}

	public void setCountOfLenders(Integer countOfLenders) {
		this.countOfLenders = countOfLenders;
	}

	public Integer getCountOfBorrowers() {
		return countOfBorrowers;
	}

	public void setCountOfBorrowers(Integer countOfBorrowers) {
		this.countOfBorrowers = countOfBorrowers;
	}

	public String getUtmName() {
		return utmName;
	}

	public void setUtmName(String utmName) {
		this.utmName = utmName;
	}
	
	
}
