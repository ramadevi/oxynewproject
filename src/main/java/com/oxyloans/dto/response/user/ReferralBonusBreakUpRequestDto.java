package com.oxyloans.dto.response.user;

public class ReferralBonusBreakUpRequestDto {

	private int dealId;

	private int refereeUserId;

	private int referrerUserId;

	public int getDealId() {
		return dealId;
	}

	public int getRefereeUserId() {
		return refereeUserId;
	}

	public int getReferrerUserId() {
		return referrerUserId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public void setRefereeUserId(int refereeUserId) {
		this.refereeUserId = refereeUserId;
	}

	public void setReferrerUserId(int referrerUserId) {
		this.referrerUserId = referrerUserId;
	}

}
