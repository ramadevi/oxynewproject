package com.oxyloans.dto.response.user;

import com.oxyloans.request.LenderFavouriteRequestDto;

public class LenderFavouriteResponseDto extends LenderFavouriteRequestDto {

	private Integer lenderUserId;

	private Integer borrowerUserId;

	private String type;

	public Integer getLenderUserId() {
		return lenderUserId;
	}

	public void setLenderUserId(Integer lenderUserId) {
		this.lenderUserId = lenderUserId;
	}

	public Integer getBorrowerUserId() {
		return borrowerUserId;
	}

	public void setBorrowerUserId(Integer borrowerUserId) {
		this.borrowerUserId = borrowerUserId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
