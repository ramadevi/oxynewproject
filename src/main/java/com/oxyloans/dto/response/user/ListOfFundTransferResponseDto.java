package com.oxyloans.dto.response.user;

import java.util.List;

public class ListOfFundTransferResponseDto {
	private List<FundTransferDetails> listOfFundTransferResponseDto;

	private Integer count;

	public List<FundTransferDetails> getListOfFundTransferResponseDto() {
		return listOfFundTransferResponseDto;
	}

	public void setListOfFundTransferResponseDto(List<FundTransferDetails> listOfFundTransferResponseDto) {
		this.listOfFundTransferResponseDto = listOfFundTransferResponseDto;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
