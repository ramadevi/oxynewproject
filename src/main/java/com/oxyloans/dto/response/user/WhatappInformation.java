package com.oxyloans.dto.response.user;

import java.util.List;

public class WhatappInformation {

	private Integer totalWhatappGroupsCount;

	private List<ListOfWhatappGroupNames> listOfWhatappGroups;

	public Integer getTotalWhatappGroupsCount() {
		return totalWhatappGroupsCount;
	}

	public void setTotalWhatappGroupsCount(Integer totalWhatappGroupsCount) {
		this.totalWhatappGroupsCount = totalWhatappGroupsCount;
	}

	public List<ListOfWhatappGroupNames> getListOfWhatappGroups() {
		return listOfWhatappGroups;
	}

	public void setListOfWhatappGroups(List<ListOfWhatappGroupNames> listOfWhatappGroups) {
		this.listOfWhatappGroups = listOfWhatappGroups;
	}

}
