package com.oxyloans.dto.response.user;

public class WtappGroupRequestDto {

	private String groupName;

	private String linkDescription;

	private String linkTitle;

	private String linkUrl;

	private boolean linkflag = true;

	private String message;

	private String phoneNumber;

	private String previewBase64;
	
	private String chatId;

	private boolean history;

	private int lastMessageNumber;

	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}

	public boolean isHistory() {
		return history;
	}

	public void setHistory(boolean history) {
		this.history = history;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getLinkDescription() {
		return linkDescription;
	}

	public void setLinkDescription(String linkDescription) {
		this.linkDescription = linkDescription;
	}

	public String getLinkTitle() {
		return linkTitle;
	}

	public void setLinkTitle(String linkTitle) {
		this.linkTitle = linkTitle;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public boolean isLinkflag() {
		return linkflag;
	}

	public void setLinkflag(boolean linkflag) {
		this.linkflag = linkflag;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPreviewBase64() {
		return previewBase64;
	}

	public void setPreviewBase64(String previewBase64) {
		this.previewBase64 = previewBase64;
	}

	public int getLastMessageNumber() {
		return lastMessageNumber;
	}

	public void setLastMessageNumber(int lastMessageNumber) {
		this.lastMessageNumber = lastMessageNumber;
	}

}
