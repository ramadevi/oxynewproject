package com.oxyloans.dto.response.user;

public class BorrowerReferenceResponse {

private Double loanEligibility;
	
	private Double salary;
	
	private Integer countOfReferees;
	
	private Double loanEligibilityBasedOnReferring;

	public Double getLoanEligibility() {
		return loanEligibility;
	}

	public void setLoanEligibility(Double loanEligibility) {
		this.loanEligibility = loanEligibility;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public Integer getCountOfReferees() {
		return countOfReferees;
	}

	public void setCountOfReferees(Integer countOfReferees) {
		this.countOfReferees = countOfReferees;
	}

	public Double getLoanEligibilityBasedOnReferring() {
		return loanEligibilityBasedOnReferring;
	}

	public void setLoanEligibilityBasedOnReferring(Double loanEligibilityBasedOnReferring) {
		this.loanEligibilityBasedOnReferring = loanEligibilityBasedOnReferring;
	}
	
	
	
	
}
