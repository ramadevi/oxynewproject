package com.oxyloans.dto.response.user;

public class UpdatePaytmProductMappingRequest {

	private String id;
	private String status;
	private String type;

	public String getId() {
		return id;
	}

	public String getStatus() {
		return status;
	}

	public String getType() {
		return type;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setType(String type) {
		this.type = type;
	}

}
