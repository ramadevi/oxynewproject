package com.oxyloans.dto.response.user;

import java.util.List;

public class BorrowersLoanOwnerInformation {
	List<BorrowersLoanOwnerNames> borrowersLoanOwnerNames;

	List<BorrowersLoanInfo> listOfBorrowersMappedToLoanOwner;

	private Integer loanOwnersCount;

	public List<BorrowersLoanOwnerNames> getBorrowersLoanOwnerNames() {
		return borrowersLoanOwnerNames;
	}

	public void setBorrowersLoanOwnerNames(List<BorrowersLoanOwnerNames> borrowersLoanOwnerNames) {
		this.borrowersLoanOwnerNames = borrowersLoanOwnerNames;
	}

	public Integer getLoanOwnersCount() {
		return loanOwnersCount;
	}

	public void setLoanOwnersCount(Integer loanOwnersCount) {
		this.loanOwnersCount = loanOwnersCount;
	}

	public List<BorrowersLoanInfo> getListOfBorrowersMappedToLoanOwner() {
		return listOfBorrowersMappedToLoanOwner;
	}

	public void setListOfBorrowersMappedToLoanOwner(List<BorrowersLoanInfo> listOfBorrowersMappedToLoanOwner) {
		this.listOfBorrowersMappedToLoanOwner = listOfBorrowersMappedToLoanOwner;
	}
}
