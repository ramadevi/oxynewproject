package com.oxyloans.dto.response.user;

import java.util.List;

public class ListOfBeneficiarysAddedBorrowers {
	private Integer borrowerid;

	private String uniqueNumber;

	private String borrowerName;

	private Double loanOfferedAmount;

	private Double currentAmount;

	private String vanNumber;

	private List<BeneficiaryResponseDto> listOfFundTransactionsCompleted;

	private List<BeneficiaryResponseDto> listOfFundTransactionsPending;

	public Integer getBorrowerid() {
		return borrowerid;
	}

	public void setBorrowerid(Integer borrowerid) {
		this.borrowerid = borrowerid;
	}

	public String getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(String uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public Double getLoanOfferedAmount() {
		return loanOfferedAmount;
	}

	public void setLoanOfferedAmount(Double loanOfferedAmount) {
		this.loanOfferedAmount = loanOfferedAmount;
	}

	public Double getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(Double currentAmount) {
		this.currentAmount = currentAmount;
	}

	public String getVanNumber() {
		return vanNumber;
	}

	public void setVanNumber(String vanNumber) {
		this.vanNumber = vanNumber;
	}

	public List<BeneficiaryResponseDto> getListOfFundTransactionsCompleted() {
		return listOfFundTransactionsCompleted;
	}

	public void setListOfFundTransactionsCompleted(List<BeneficiaryResponseDto> listOfFundTransactionsCompleted) {
		this.listOfFundTransactionsCompleted = listOfFundTransactionsCompleted;
	}

	public List<BeneficiaryResponseDto> getListOfFundTransactionsPending() {
		return listOfFundTransactionsPending;
	}

	public void setListOfFundTransactionsPending(List<BeneficiaryResponseDto> listOfFundTransactionsPending) {
		this.listOfFundTransactionsPending = listOfFundTransactionsPending;
	}

}
