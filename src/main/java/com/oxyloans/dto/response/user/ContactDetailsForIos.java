package com.oxyloans.dto.response.user;

public class ContactDetailsForIos {
	private String contactDetails;

	public String getContactDetails() {
		return contactDetails;
	}

	public void setContactDetails(String contactDetails) {
		this.contactDetails = contactDetails;
	}

}
