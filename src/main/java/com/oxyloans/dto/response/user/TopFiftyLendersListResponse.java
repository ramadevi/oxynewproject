package com.oxyloans.dto.response.user;

public class TopFiftyLendersListResponse {

private Integer lenderId;
	
	private String lenderName;
	
	private Double totalParticipationAmount;
	
	private String mobileNumber;
	
	private String email;
	
	private String city;
	
	private String state;
	
	private String pincode;
	
	private String address;
	
	
    private String registeredOn;
	
	private String receivedOn;
	
	
	

	public String getRegisteredOn() {
		return registeredOn;
	}

	public void setRegisteredOn(String registeredOn) {
		this.registeredOn = registeredOn;
	}

	public String getReceivedOn() {
		return receivedOn;
	}

	public void setReceivedOn(String receivedOn) {
		this.receivedOn = receivedOn;
	}

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public Double getTotalParticipationAmount() {
		return totalParticipationAmount;
	}

	public void setTotalParticipationAmount(Double totalParticipationAmount) {
		this.totalParticipationAmount = totalParticipationAmount;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	
}
