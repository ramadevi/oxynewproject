package com.oxyloans.dto.response.user;

import java.util.List;
import java.util.Map;

public class ReadingCommitmentAmountDto {

	private Double commitmentAmount;
	private Map<String, Double> map;
	private int count;
	private Map<String, Double> participationAmountMap;

	private List<String> list;

	public Double getCommitmentAmount() {
		return commitmentAmount;
	}

	public void setCommitmentAmount(Double commitmentAmount) {
		this.commitmentAmount = commitmentAmount;
	}

	public Map<String, Double> getMap() {
		return map;
	}

	public void setMap(Map<String, Double> map) {
		this.map = map;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public Map<String, Double> getParticipationAmountMap() {
		return participationAmountMap;
	}

	public void setParticipationAmountMap(Map<String, Double> participationAmountMap) {
		this.participationAmountMap = participationAmountMap;
	}
}
