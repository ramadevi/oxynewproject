package com.oxyloans.dto.response.user;

import java.util.List;

public class EquityLendersListsDto {

	private int newLendersCount;

	private int lenderGroupMembersCount;

	private int equityDealsCount;

	private List<EquityNewLendersDto> equityNewLendersDto;

	private List<EquityFoundingLendersDto> equityFoundingLendersDto;

	public int getNewLendersCount() {
		return newLendersCount;
	}

	public void setNewLendersCount(int newLendersCount) {
		this.newLendersCount = newLendersCount;
	}

	public int getLenderGroupMembersCount() {
		return lenderGroupMembersCount;
	}

	public void setLenderGroupMembersCount(int lenderGroupMembersCount) {
		this.lenderGroupMembersCount = lenderGroupMembersCount;
	}

	public int getEquityDealsCount() {
		return equityDealsCount;
	}

	public void setEquityDealsCount(int equityDealsCount) {
		this.equityDealsCount = equityDealsCount;
	}

	public List<EquityNewLendersDto> getEquityNewLendersDto() {
		return equityNewLendersDto;
	}

	public void setEquityNewLendersDto(List<EquityNewLendersDto> equityNewLendersDto) {
		this.equityNewLendersDto = equityNewLendersDto;
	}

	public List<EquityFoundingLendersDto> getEquityFoundingLendersDto() {
		return equityFoundingLendersDto;
	}

	public void setEquityFoundingLendersDto(List<EquityFoundingLendersDto> equityFoundingLendersDto) {
		this.equityFoundingLendersDto = equityFoundingLendersDto;
	}

}
