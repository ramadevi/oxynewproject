package com.oxyloans.dto.response.user;

import java.math.BigInteger;
import java.util.List;

public class LendersWithdrawResponse {

	private List<LendersReturnResponse>  listOfWithdrawRequests;
	
	private List<LendersReturnResponse>  listOfWithdrawInitiated;
	
	private BigInteger totalAmountPArticipatedIndeal;
	
	private BigInteger dealValue;
	
	private BigInteger totalAmountReturned;
	
	private BigInteger remainingAmount;


	public List<LendersReturnResponse> getListOfWithdrawRequests() {
		return listOfWithdrawRequests;
	}

	public void setListOfWithdrawRequests(List<LendersReturnResponse> listOfWithdrawRequests) {
		this.listOfWithdrawRequests = listOfWithdrawRequests;
	}

	public List<LendersReturnResponse> getListOfWithdrawInitiated() {
		return listOfWithdrawInitiated;
	}

	public void setListOfWithdrawInitiated(List<LendersReturnResponse> listOfWithdrawInitiated) {
		this.listOfWithdrawInitiated = listOfWithdrawInitiated;
	}

	public BigInteger getTotalAmountPArticipatedIndeal() {
		return totalAmountPArticipatedIndeal;
	}

	public void setTotalAmountPArticipatedIndeal(BigInteger totalAmountPArticipatedIndeal) {
		this.totalAmountPArticipatedIndeal = totalAmountPArticipatedIndeal;
	}

	public BigInteger getDealValue() {
		return dealValue;
	}

	public void setDealValue(BigInteger dealValue) {
		this.dealValue = dealValue;
	}

	public BigInteger getTotalAmountReturned() {
		return totalAmountReturned;
	}

	public void setTotalAmountReturned(BigInteger totalAmountReturned) {
		this.totalAmountReturned = totalAmountReturned;
	}

	public BigInteger getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(BigInteger remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	
	
}
