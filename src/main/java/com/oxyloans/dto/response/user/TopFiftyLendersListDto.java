package com.oxyloans.dto.response.user;

import java.util.List;

public class TopFiftyLendersListDto {
	
	private String downloadLink;
	
	private List<TopFiftyLendersListResponse> listResponse;

	public String getDownloadLink() {
		return downloadLink;
	}

	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}

	public List<TopFiftyLendersListResponse> getListResponse() {
		return listResponse;
	}

	public void setListResponse(List<TopFiftyLendersListResponse> listResponse) {
		this.listResponse = listResponse;
	}
	
	
	

}
