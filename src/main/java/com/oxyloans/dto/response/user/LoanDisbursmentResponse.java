package com.oxyloans.dto.response.user;

public class LoanDisbursmentResponse {

	private String status;

	private int countForUpdatedBorrowers;

	private int countForNotUpdatedBorrowers;

	public int countOfApprovedLoans;

	public int getCountForNotUpdatedBorrowers() {
		return countForNotUpdatedBorrowers;
	}

	public void setCountForNotUpdatedBorrowers(int countForNotUpdatedBorrowers) {
		this.countForNotUpdatedBorrowers = countForNotUpdatedBorrowers;
	}

	public int getCountForUpdatedBorrowers() {
		return countForUpdatedBorrowers;
	}

	public void setCountForUpdatedBorrowers(int countForUpdatedBorrowers) {
		this.countForUpdatedBorrowers = countForUpdatedBorrowers;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCountOfApprovedLoans() {
		return countOfApprovedLoans;
	}

	public void setCountOfApprovedLoans(int countOfApprovedLoans) {
		this.countOfApprovedLoans = countOfApprovedLoans;
	}

 



}
