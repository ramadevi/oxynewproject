package com.oxyloans.dto.response.user;

import java.util.Date;

public class LenderBonusAmountStatus {

	private String downloadUrl;
	
	private Integer amountEarned;
	
	private Integer userId;

	private String paymentStatus;

	private Integer pageNo;

	private Integer pageSize;
	
    private int  month;
	
	private int year;
	
	private String remarks;
	
    private Date transferredOn;
	
    public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getTransferredOn() {
		return transferredOn;
	}

	public void setTransferredOn(Date transferredOn) {
		this.transferredOn = transferredOn;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public Integer getAmountEarned() {
		return amountEarned;
	}

	public void setAmountEarned(Integer amountEarned) {
		this.amountEarned = amountEarned;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
	
}
