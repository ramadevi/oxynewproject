package com.oxyloans.dto.response.user;

public class FinancialDetailsResponseDto {
	private Integer userId;

	private Integer monthlyEmi;

	private Integer creditAmount;

	private Integer existingLoanAmount;

	private Integer creditCardsRepaymentAmount;

	private Integer otherSourcesIncome;

	private Integer netMonthlyIncome;

	private Integer avgMonthlyExpenses;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getMonthlyEmi() {
		return monthlyEmi;
	}

	public void setMonthlyEmi(Integer monthlyEmi) {
		this.monthlyEmi = monthlyEmi;
	}

	public Integer getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Integer creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Integer getExistingLoanAmount() {
		return existingLoanAmount;
	}

	public void setExistingLoanAmount(Integer existingLoanAmount) {
		this.existingLoanAmount = existingLoanAmount;
	}

	public Integer getCreditCardsRepaymentAmount() {
		return creditCardsRepaymentAmount;
	}

	public void setCreditCardsRepaymentAmount(Integer creditCardsRepaymentAmount) {
		this.creditCardsRepaymentAmount = creditCardsRepaymentAmount;
	}

	public Integer getOtherSourcesIncome() {
		return otherSourcesIncome;
	}

	public void setOtherSourcesIncome(Integer otherSourcesIncome) {
		this.otherSourcesIncome = otherSourcesIncome;
	}

	public Integer getNetMonthlyIncome() {
		return netMonthlyIncome;
	}

	public void setNetMonthlyIncome(Integer netMonthlyIncome) {
		this.netMonthlyIncome = netMonthlyIncome;
	}

	public Integer getAvgMonthlyExpenses() {
		return avgMonthlyExpenses;
	}

	public void setAvgMonthlyExpenses(Integer avgMonthlyExpenses) {
		this.avgMonthlyExpenses = avgMonthlyExpenses;
	}

}
