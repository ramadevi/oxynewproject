package com.oxyloans.dto.response.user;

public class BorrowerFreeDetailsResponseDto {
	private Integer applictionId;

	private Double offeredAmount;

	private String borrowerName;

	private Integer borrowerId;

	private Double applicationLevelFreeHaveToPay;

	private Double applicationLevelFreePaid;

	private Integer percentage;

	public Integer getApplictionId() {
		return applictionId;
	}

	public void setApplictionId(Integer applictionId) {
		this.applictionId = applictionId;
	}

	public Double getOfferedAmount() {
		return offeredAmount;
	}

	public void setOfferedAmount(Double offeredAmount) {
		this.offeredAmount = offeredAmount;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public Double getApplicationLevelFreeHaveToPay() {
		return applicationLevelFreeHaveToPay;
	}

	public void setApplicationLevelFreeHaveToPay(Double applicationLevelFreeHaveToPay) {
		this.applicationLevelFreeHaveToPay = applicationLevelFreeHaveToPay;
	}

	public Double getApplicationLevelFreePaid() {
		return applicationLevelFreePaid;
	}

	public void setApplicationLevelFreePaid(Double applicationLevelFreePaid) {
		this.applicationLevelFreePaid = applicationLevelFreePaid;
	}

	public Integer getPercentage() {
		return percentage;
	}

	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}
}
