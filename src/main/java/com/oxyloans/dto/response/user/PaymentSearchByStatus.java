package com.oxyloans.dto.response.user;

import java.util.List;

public class PaymentSearchByStatus {

	private List<PaymentUploadHistoryResponseDto> paymentUploadHistoryResponseDto;

	private Integer totalCount;

	public List<PaymentUploadHistoryResponseDto> getPaymentUploadHistoryResponseDto() {
		return paymentUploadHistoryResponseDto;
	}

	public void setPaymentUploadHistoryResponseDto(
			List<PaymentUploadHistoryResponseDto> paymentUploadHistoryResponseDto) {
		this.paymentUploadHistoryResponseDto = paymentUploadHistoryResponseDto;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

}
