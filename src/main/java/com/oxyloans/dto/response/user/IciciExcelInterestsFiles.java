package com.oxyloans.dto.response.user;

public class IciciExcelInterestsFiles {

	private String fileName;
	private String accountNumber;
	private double amount;
	private String amountpaidDate;
	private String remarks;
	private String lenderId;

	private int dealId;
	private String debitAccount;
	private String lenderName;
	private String payMode;
	private String amountPaidBorrower;
	private String status;

	private String amountType;

	private String ifscCode;

	private String creditedOn;

	private String addDetails2;
	private int addDetails1;

	private int addDetails5;

	private String daysDifference;

	private String originalPaymentDate;
	
    private String addDetailsI5;
	
	private String addDetailsI1;
	
	private String dealIds;

	public String getAddDetailsI5() {
		return addDetailsI5;
	}

	public void setAddDetailsI5(String addDetailsI5) {
		this.addDetailsI5 = addDetailsI5;
	}

	public String getAddDetailsI1() {
		return addDetailsI1;
	}

	public void setAddDetailsI1(String addDetailsI1) {
		this.addDetailsI1 = addDetailsI1;
	}

	public String getDealIds() {
		return dealIds;
	}

	public void setDealIds(String dealIds) {
		this.dealIds = dealIds;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getAmountpaidDate() {
		return amountpaidDate;
	}

	public void setAmountpaidDate(String amountpaidDate) {
		this.amountpaidDate = amountpaidDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getLenderId() {
		return lenderId;
	}

	public void setLenderId(String lenderId) {
		this.lenderId = lenderId;
	}

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public String getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public String getPayMode() {
		return payMode;
	}

	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getAmountPaidBorrower() {
		return amountPaidBorrower;
	}

	public void setAmountPaidBorrower(String amountPaidBorrower) {
		this.amountPaidBorrower = amountPaidBorrower;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreditedOn() {
		return creditedOn;
	}

	public void setCreditedOn(String creditedOn) {
		this.creditedOn = creditedOn;
	}

	@Override
	public String toString() {
		return "IciciExcelInterestsFiles [fileName=" + fileName + ", accountNumber=" + accountNumber + ", amount="
				+ amount + ", amountpaidDate=" + amountpaidDate + ", remarks=" + remarks + ", lenderId=" + lenderId
				+ ", dealId=" + dealId + ", debitAccount=" + debitAccount + ", lenderName=" + lenderName + ", payMode="
				+ payMode + ", amountPaidBorrower=" + amountPaidBorrower + ", status=" + status + ", ifscCode="
				+ ifscCode + ", creditedOn=" + creditedOn + "]";
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public String getAddDetails2() {
		return addDetails2;
	}

	public void setAddDetails2(String addDetails2) {
		this.addDetails2 = addDetails2;
	}

	public int getAddDetails5() {
		return addDetails5;
	}

	public void setAddDetails5(int addDetails5) {
		this.addDetails5 = addDetails5;
	}

	public String getDaysDifference() {
		return daysDifference;
	}

	public void setDaysDifference(String daysDifference) {
		this.daysDifference = daysDifference;
	}

	public String getOriginalPaymentDate() {
		return originalPaymentDate;
	}

	public void setOriginalPaymentDate(String originalPaymentDate) {
		this.originalPaymentDate = originalPaymentDate;
	}

	public int getAddDetails1() {
		return addDetails1;
	}

	public void setAddDetails1(int addDetails1) {
		this.addDetails1 = addDetails1;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
