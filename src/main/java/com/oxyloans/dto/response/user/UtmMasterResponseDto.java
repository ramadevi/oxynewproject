package com.oxyloans.dto.response.user;

import com.oxyloans.request.user.UtmMasterRequestDto;

public class UtmMasterResponseDto extends UtmMasterRequestDto {
	private String utmName;

	public String getUtmName() {
		return utmName;
	}

	public void setUtmName(String utmName) {
		this.utmName = utmName;
	}

}
