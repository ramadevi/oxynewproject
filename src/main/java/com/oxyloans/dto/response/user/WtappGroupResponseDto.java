package com.oxyloans.dto.response.user;

public class WtappGroupResponseDto {

	private String group_invitation_link;

	private String converstaion_id;

	public String getGroup_invitation_link() {
		return group_invitation_link;
	}

	public void setGroup_invitation_link(String group_invitation_link) {
		this.group_invitation_link = group_invitation_link;
	}

	public String getConverstaion_id() {
		return converstaion_id;
	}

	public void setConverstaion_id(String converstaion_id) {
		this.converstaion_id = converstaion_id;
	}

}
