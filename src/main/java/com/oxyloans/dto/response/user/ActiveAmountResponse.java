package com.oxyloans.dto.response.user;

public class ActiveAmountResponse {

	private Integer userId;
	
	private String userName;
	
	private Double totalActiveAmount;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Double getTotalActiveAmount() {
		return totalActiveAmount;
	}

	public void setTotalActiveAmount(Double totalActiveAmount) {
		this.totalActiveAmount = totalActiveAmount;
	}
	
	
}
