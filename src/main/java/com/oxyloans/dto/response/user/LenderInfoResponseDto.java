package com.oxyloans.dto.response.user;

public class LenderInfoResponseDto {
	private String borrowerName;

	private double disbursedAmount;

	private double emiAmountRecevied;

	private String emiStartDate;

	private Integer emisRecevied;

	private Integer emiNotRecevied;

	private String loanStatus;

	private String repaymentMethod;

	private double firstEmiAmount;

	private double profitRecevied;

	private Integer duration;

	private Integer emisToRecevied;

	private Integer borrowerId;

	private String loanId;

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public Integer getEmisToRecevied() {
		return emisToRecevied;
	}

	public void setEmisToRecevied(Integer emisToRecevied) {
		this.emisToRecevied = emisToRecevied;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public double getDisbursedAmount() {
		return disbursedAmount;
	}

	public void setDisbursedAmount(double disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}

	public double getEmiAmountRecevied() {
		return emiAmountRecevied;
	}

	public void setEmiAmountRecevied(double emiAmountRecevied) {
		this.emiAmountRecevied = emiAmountRecevied;
	}

	public String getEmiStartDate() {
		return emiStartDate;
	}

	public void setEmiStartDate(String emiStartDate) {
		this.emiStartDate = emiStartDate;
	}

	public Integer getEmisRecevied() {
		return emisRecevied;
	}

	public void setEmisRecevied(Integer emisRecevied) {
		this.emisRecevied = emisRecevied;
	}

	public Integer getEmiNotRecevied() {
		return emiNotRecevied;
	}

	public void setEmiNotRecevied(Integer emiNotRecevied) {
		this.emiNotRecevied = emiNotRecevied;
	}

	public String getLoanStatus() {
		return loanStatus;
	}

	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}

	public String getRepaymentMethod() {
		return repaymentMethod;
	}

	public void setRepaymentMethod(String repaymentMethod) {
		this.repaymentMethod = repaymentMethod;
	}

	public double getFirstEmiAmount() {
		return firstEmiAmount;
	}

	public void setFirstEmiAmount(double firstEmiAmount) {
		this.firstEmiAmount = firstEmiAmount;
	}

	public double getProfitRecevied() {
		return profitRecevied;
	}

	public void setProfitRecevied(double profitRecevied) {
		this.profitRecevied = profitRecevied;
	}

}
