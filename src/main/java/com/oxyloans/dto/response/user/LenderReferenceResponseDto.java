package com.oxyloans.dto.response.user;

import java.util.List;

public class LenderReferenceResponseDto {

	private String refereeEmail;

	private String refereeMobileNumber;

	private String referredOn;

	private Integer refereeId;

	private Integer referrerId;

	private String refereeName;

	private String status;

	private Double amount;

	private String paymentStatus;

	private String radhaSirComments;

	private String refereeNewId;

	private String transferredOn;

	private List<LenderReferenceAmountResponse> lenderReferenceAmountResponse;
	
	private Double totalAmountEarned;
	
	private String primaryType;
	
	private String referrerNewId;
	

	public String getRefereeEmail() {
		return refereeEmail;
	}

	public void setRefereeEmail(String refereeEmail) {
		this.refereeEmail = refereeEmail;
	}

	public String getRefereeMobileNumber() {
		return refereeMobileNumber;
	}

	public void setRefereeMobileNumber(String refereeMobileNumber) {
		this.refereeMobileNumber = refereeMobileNumber;
	}

	public String getReferredOn() {
		return referredOn;
	}

	public void setReferredOn(String referredOn) {
		this.referredOn = referredOn;
	}

	public Integer getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(Integer refereeId) {
		this.refereeId = refereeId;
	}

	public Integer getReferrerId() {
		return referrerId;
	}

	public void setReferrerId(Integer referrerId) {
		this.referrerId = referrerId;
	}

	public String getRefereeName() {
		return refereeName;
	}

	public void setRefereeName(String refereeName) {
		this.refereeName = refereeName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getRadhaSirComments() {
		return radhaSirComments;
	}

	public void setRadhaSirComments(String radhaSirComments) {
		this.radhaSirComments = radhaSirComments;
	}

	public String getRefereeNewId() {
		return refereeNewId;
	}

	public void setRefereeNewId(String refereeNewId) {
		this.refereeNewId = refereeNewId;
	}

	public String getTransferredOn() {
		return transferredOn;
	}

	public void setTransferredOn(String transferredOn) {
		this.transferredOn = transferredOn;
	}

	public List<LenderReferenceAmountResponse> getLenderReferenceAmountResponse() {
		return lenderReferenceAmountResponse;
	}

	public void setLenderReferenceAmountResponse(List<LenderReferenceAmountResponse> lenderReferenceAmountResponse) {
		this.lenderReferenceAmountResponse = lenderReferenceAmountResponse;
	}

	public Double getTotalAmountEarned() {
		return totalAmountEarned;
	}

	public void setTotalAmountEarned(Double totalAmountEarned) {
		this.totalAmountEarned = totalAmountEarned;
	}

	public String getPrimaryType() {
		return primaryType;
	}

	public void setPrimaryType(String primaryType) {
		this.primaryType = primaryType;
	}

	public String getReferrerNewId() {
		return referrerNewId;
	}

	public void setReferrerNewId(String referrerNewId) {
		this.referrerNewId = referrerNewId;
	}

}
