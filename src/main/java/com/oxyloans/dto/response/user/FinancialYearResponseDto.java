package com.oxyloans.dto.response.user;

import java.math.BigInteger;

public class FinancialYearResponseDto {
	
	private Integer sNo;
	
	private String startDate;
	
	private String endDate;
	
	private BigInteger incomeEarned;
	
	private String financialYear;
	

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public BigInteger getIncomeEarned() {
		return incomeEarned;
	}

	public void setIncomeEarned(BigInteger incomeEarned) {
		this.incomeEarned = incomeEarned;
	}

	public String getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}

	public Integer getsNo() {
		return sNo;
	}

	public void setsNo(Integer sNo) {
		this.sNo = sNo;
	}


}
