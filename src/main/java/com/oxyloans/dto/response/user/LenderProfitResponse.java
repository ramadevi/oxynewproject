package com.oxyloans.dto.response.user;

import java.util.List;

import com.oxyloans.service.loan.dto.LenderHistroryResponseDto;

public class LenderProfitResponse {

	private List<LenderHistroryResponseDto> lenderHistoryResponseDto;

	private double totalProfit;

	private Integer totalEmisReceived;

	private List<LenderProfitHistoryResponse> lenderProfitHistoryResponse;

	public List<LenderHistroryResponseDto> getLenderHistoryResponseDto() {
		return lenderHistoryResponseDto;
	}

	public void setLenderHistoryResponseDto(List<LenderHistroryResponseDto> lenderHistoryResponseDto) {
		this.lenderHistoryResponseDto = lenderHistoryResponseDto;
	}

	public double getTotalProfit() {
		return totalProfit;
	}

	public void setTotalProfit(double totalProfit) {
		this.totalProfit = totalProfit;
	}

	public Integer getTotalEmisReceived() {
		return totalEmisReceived;
	}

	public void setTotalEmisReceived(Integer totalEmisReceived) {
		this.totalEmisReceived = totalEmisReceived;
	}

	public List<LenderProfitHistoryResponse> getLenderProfitHistoryResponse() {
		return lenderProfitHistoryResponse;
	}

	public void setLenderProfitHistoryResponse(List<LenderProfitHistoryResponse> lenderProfitHistoryResponse) {
		this.lenderProfitHistoryResponse = lenderProfitHistoryResponse;
	}
}
