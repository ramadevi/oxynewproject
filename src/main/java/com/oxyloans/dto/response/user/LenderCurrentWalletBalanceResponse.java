package com.oxyloans.dto.response.user;

import java.math.BigInteger;

public class LenderCurrentWalletBalanceResponse {

	private Integer userId;

	private String name;

	private BigInteger credit;

	private BigInteger debit;

	private BigInteger totalLoanAmount;

	private BigInteger loanAmountToDeals;

	private BigInteger firstPaticipation;

	private BigInteger secondPaticipation;

	private BigInteger currentWalletAmount;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigInteger getCredit() {
		return credit;
	}

	public void setCredit(BigInteger credit) {
		this.credit = credit;
	}

	public BigInteger getDebit() {
		return debit;
	}

	public void setDebit(BigInteger debit) {
		this.debit = debit;
	}

	public BigInteger getTotalLoanAmount() {
		return totalLoanAmount;
	}

	public void setTotalLoanAmount(BigInteger totalLoanAmount) {
		this.totalLoanAmount = totalLoanAmount;
	}

	public BigInteger getLoanAmountToDeals() {
		return loanAmountToDeals;
	}

	public void setLoanAmountToDeals(BigInteger loanAmountToDeals) {
		this.loanAmountToDeals = loanAmountToDeals;
	}

	public BigInteger getFirstPaticipation() {
		return firstPaticipation;
	}

	public void setFirstPaticipation(BigInteger firstPaticipation) {
		this.firstPaticipation = firstPaticipation;
	}

	public BigInteger getSecondPaticipation() {
		return secondPaticipation;
	}

	public void setSecondPaticipation(BigInteger secondPaticipation) {
		this.secondPaticipation = secondPaticipation;
	}

	public BigInteger getCurrentWalletAmount() {
		return currentWalletAmount;
	}

	public void setCurrentWalletAmount(BigInteger currentWalletAmount) {
		this.currentWalletAmount = currentWalletAmount;
	}

}
