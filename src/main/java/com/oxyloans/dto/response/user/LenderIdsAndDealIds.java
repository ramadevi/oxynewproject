package com.oxyloans.dto.response.user;

public class LenderIdsAndDealIds {

	private Integer refereeId;
	
	private Integer dealId;
	
	private Double amountForDeal;
	
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(Integer refereeId) {
		this.refereeId = refereeId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Double getAmountForDeal() {
		return amountForDeal;
	}

	public void setAmountForDeal(Double amountForDeal) {
		this.amountForDeal = amountForDeal;
	}
	
	
}
