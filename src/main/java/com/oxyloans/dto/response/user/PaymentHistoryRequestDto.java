package com.oxyloans.dto.response.user;

import java.util.List;

public class PaymentHistoryRequestDto {
	private String applicationId;

	private double amount;

	private String amountPaidDate;

	private String currentDate;

	private String loanOwnerName;

	private List<PaymentUploadHistoryTableResponseDto> indvidualPaymentsByOwner;

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getAmountPaidDate() {
		return amountPaidDate;
	}

	public void setAmountPaidDate(String amountPaidDate) {
		this.amountPaidDate = amountPaidDate;
	}

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public String getLoanOwnerName() {
		return loanOwnerName;
	}

	public void setLoanOwnerName(String loanOwnerName) {
		this.loanOwnerName = loanOwnerName;
	}

	public List<PaymentUploadHistoryTableResponseDto> getIndvidualPaymentsByOwner() {
		return indvidualPaymentsByOwner;
	}

	public void setIndvidualPaymentsByOwner(List<PaymentUploadHistoryTableResponseDto> indvidualPaymentsByOwner) {
		this.indvidualPaymentsByOwner = indvidualPaymentsByOwner;
	}
}
