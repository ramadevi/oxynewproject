package com.oxyloans.dto.response.user;

public class HighestReferralBonusResponse {
	
	private Integer referrerId;
	
	private String referrerName;
	
	private Double sumOfAmountEarned;
	
    private String mobileNumber;
	
	private String email;

	public Integer getReferrerId() {
		return referrerId;
	}

	public void setReferrerId(Integer referrerId) {
		this.referrerId = referrerId;
	}

	public String getReferrerName() {
		return referrerName;
	}

	public void setReferrerName(String referrerName) {
		this.referrerName = referrerName;
	}

	public Double getSumOfAmountEarned() {
		return sumOfAmountEarned;
	}

	public void setSumOfAmountEarned(Double sumOfAmountEarned) {
		this.sumOfAmountEarned = sumOfAmountEarned;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
