package com.oxyloans.dto.response.user;

public class LenderRatingRequestDto {
	private Integer lenderId;

	private Integer rating;

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}
}
