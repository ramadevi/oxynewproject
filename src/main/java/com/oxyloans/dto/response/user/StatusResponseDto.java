package com.oxyloans.dto.response.user;

public class StatusResponseDto {
	private String status;

	private String message;

	private String downloadUrl;

	private int count;

	private Double totalAmount;

	private String fileName;
	
	private int sentMailsCount;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getSentMailsCount() {
		return sentMailsCount;
	}

	public void setSentMailsCount(int sentMailsCount) {
		this.sentMailsCount = sentMailsCount;
	}

}
