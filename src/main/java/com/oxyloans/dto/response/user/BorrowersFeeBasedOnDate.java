package com.oxyloans.dto.response.user;

import java.util.List;

public class BorrowersFeeBasedOnDate {
	private List<BorrowerFreeDetailsResponseDto> borrowerFreeDetailsResponseDto;

	private double totalFreeBasedOndate;

	private double totalFreeTillDate;

	public List<BorrowerFreeDetailsResponseDto> getBorrowerFreeDetailsResponseDto() {
		return borrowerFreeDetailsResponseDto;
	}

	public void setBorrowerFreeDetailsResponseDto(List<BorrowerFreeDetailsResponseDto> borrowerFreeDetailsResponseDto) {
		this.borrowerFreeDetailsResponseDto = borrowerFreeDetailsResponseDto;
	}

	public double getTotalFreeBasedOndate() {
		return totalFreeBasedOndate;
	}

	public void setTotalFreeBasedOndate(double totalFreeBasedOndate) {
		this.totalFreeBasedOndate = totalFreeBasedOndate;
	}

	public double getTotalFreeTillDate() {
		return totalFreeTillDate;
	}

	public void setTotalFreeTillDate(double totalFreeTillDate) {
		this.totalFreeTillDate = totalFreeTillDate;
	}

}
