package com.oxyloans.dto.response.user;

public class LenderReferralPaymentStatus {

	private Double amount;

	private String paymentStatus;

	private String transferredOn;

	private Integer refereeId;

	private Integer dealId;

	private Double participatedAmount;

	private String participatedOn;

	private String dealName;

	private Integer sNo;
	
	private String dealType;
	
	private Integer referrerId;
	
	private String refereeNewId;
	
	private Double updatedBonus;
	
	private String remarks;


	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getTransferredOn() {
		return transferredOn;
	}

	public void setTransferredOn(String transferredOn) {
		this.transferredOn = transferredOn;
	}

	public Integer getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(Integer refereeId) {
		this.refereeId = refereeId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Double getParticipatedAmount() {
		return participatedAmount;
	}

	public void setParticipatedAmount(Double participatedAmount) {
		this.participatedAmount = participatedAmount;
	}

	public String getParticipatedOn() {
		return participatedOn;
	}

	public void setParticipatedOn(String participatedOn) {
		this.participatedOn = participatedOn;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public Integer getsNo() {
		return sNo;
	}

	public void setsNo(Integer sNo) {
		this.sNo = sNo;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public Integer getReferrerId() {
		return referrerId;
	}

	public void setReferrerId(Integer referrerId) {
		this.referrerId = referrerId;
	}

	public String getRefereeNewId() {
		return refereeNewId;
	}

	public void setRefereeNewId(String refereeNewId) {
		this.refereeNewId = refereeNewId;
	}

	public Double getUpdatedBonus() {
		return updatedBonus;
	}

	public void setUpdatedBonus(Double updatedBonus) {
		this.updatedBonus = updatedBonus;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
