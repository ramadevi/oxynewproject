package com.oxyloans.dto.response.user;

public class WtappIndividualGroupRequestDto {

	private String to;

	private String body;

	private String token;

	public String getTo() {
		return to;
	}

	public String getBody() {
		return body;
	}

	public String getToken() {
		return token;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
