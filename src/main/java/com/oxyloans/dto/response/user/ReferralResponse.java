package com.oxyloans.dto.response.user;

import java.util.List;

public class ReferralResponse {
	
	private List<ReferralResponseDto> listOfResponse;
	
	private String downloadUrl;

	public List<ReferralResponseDto> getListOfResponse() {
		return listOfResponse;
	}

	public void setListOfResponse(List<ReferralResponseDto> listOfResponse) {
		this.listOfResponse = listOfResponse;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	
	

}
