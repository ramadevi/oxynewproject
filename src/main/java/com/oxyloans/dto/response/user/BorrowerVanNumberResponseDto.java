package com.oxyloans.dto.response.user;

public class BorrowerVanNumberResponseDto {

	private String vanNumber;

	private double amountCreditedToVan;

	private double amountdebitedAmount;

	private double currentAmountInVan;

	public String getVanNumber() {
		return vanNumber;
	}

	public void setVanNumber(String vanNumber) {
		this.vanNumber = vanNumber;
	}

	public double getAmountCreditedToVan() {
		return amountCreditedToVan;
	}

	public void setAmountCreditedToVan(double amountCreditedToVan) {
		this.amountCreditedToVan = amountCreditedToVan;
	}

	public double getAmountdebitedAmount() {
		return amountdebitedAmount;
	}

	public void setAmountdebitedAmount(double amountdebitedAmount) {
		this.amountdebitedAmount = amountdebitedAmount;
	}

	public double getCurrentAmountInVan() {
		return currentAmountInVan;
	}

	public void setCurrentAmountInVan(double currentAmountInVan) {
		this.currentAmountInVan = currentAmountInVan;
	}

}
