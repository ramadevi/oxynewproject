package com.oxyloans.dto.response.user;

public class LenderEmiDetails {
	private double interestDetails;

	private double principalDetails;

	private double emiDetails;

	public double getInterestDetails() {
		return interestDetails;
	}

	public void setInterestDetails(double interestDetails) {
		this.interestDetails = interestDetails;
	}

	public double getPrincipalDetails() {
		return principalDetails;
	}

	public void setPrincipalDetails(double principalDetails) {
		this.principalDetails = principalDetails;
	}

	public double getEmiDetails() {
		return emiDetails;
	}

	public void setEmiDetails(double emiDetails) {
		this.emiDetails = emiDetails;
	}
}
