package com.oxyloans.dto.response.user;

import java.util.List;

public class BankTransactionsDto {

	private String totalTransactionAmount;

	private Double currentMonthTotalTransaction;

	private String totalTransactionAmountInGivenDates;

	private List<BankTransactionsBreakUpDto> bankTransactionsBreakUpDtoList;

	private String downloadUrl;

	public String getTotalTransactionAmount() {
		return totalTransactionAmount;
	}

	public void setTotalTransactionAmount(String totalTransactionAmount) {
		this.totalTransactionAmount = totalTransactionAmount;
	}

	public Double getCurrentMonthTotalTransaction() {
		return currentMonthTotalTransaction;
	}

	public void setCurrentMonthTotalTransaction(Double currentMonthTotalTransaction) {
		this.currentMonthTotalTransaction = currentMonthTotalTransaction;
	}

	public List<BankTransactionsBreakUpDto> getBankTransactionsBreakUpDtoList() {
		return bankTransactionsBreakUpDtoList;
	}

	public void setBankTransactionsBreakUpDtoList(List<BankTransactionsBreakUpDto> bankTransactionsBreakUpDtoList) {
		this.bankTransactionsBreakUpDtoList = bankTransactionsBreakUpDtoList;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getTotalTransactionAmountInGivenDates() {
		return totalTransactionAmountInGivenDates;
	}

	public void setTotalTransactionAmountInGivenDates(String totalTransactionAmountInGivenDates) {
		this.totalTransactionAmountInGivenDates = totalTransactionAmountInGivenDates;
	}

}
