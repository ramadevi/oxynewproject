package com.oxyloans.dto.response.user;

public class PaymentUploadHistoryTableResponseDto {
	private String paid_date;
	private Double individualAmountByOwner;

	public String getPaid_date() {
		return paid_date;
	}

	public void setPaid_date(String paid_date) {
		this.paid_date = paid_date;
	}

	public Double getIndividualAmountByOwner() {
		return individualAmountByOwner;
	}

	public void setIndividualAmountByOwner(Double individualAmountByOwner) {
		this.individualAmountByOwner = individualAmountByOwner;
	}
}
