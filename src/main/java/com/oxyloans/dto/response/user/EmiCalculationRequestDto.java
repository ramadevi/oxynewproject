package com.oxyloans.dto.response.user;

public class EmiCalculationRequestDto {
	private double totalEmiAmount;

	private double totalInterestAmount;

	private double firstEmiAmount;

	public double getTotalEmiAmount() {
		return totalEmiAmount;
	}

	public void setTotalEmiAmount(double totalEmiAmount) {
		this.totalEmiAmount = totalEmiAmount;
	}

	public double getTotalInterestAmount() {
		return totalInterestAmount;
	}

	public void setTotalInterestAmount(double totalInterestAmount) {
		this.totalInterestAmount = totalInterestAmount;
	}

	public double getFirstEmiAmount() {
		return firstEmiAmount;
	}

	public void setFirstEmiAmount(double firstEmiAmount) {
		this.firstEmiAmount = firstEmiAmount;
	}
}
