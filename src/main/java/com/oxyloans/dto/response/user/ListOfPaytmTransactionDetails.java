package com.oxyloans.dto.response.user;

public class ListOfPaytmTransactionDetails {

	private Integer userId;

	private String status;

	private String amount;

	private String vanNumber;

	private String beneficiaryAccountNumber;

	private String beneficiaryIfsc;

	private String remitterAccountNumber;

	private String remitterIfsc;

	private String remitterName;

	private String bankTxnIdentifier;

	private String transactionRequestId;

	private String transferMode;

	private String responseCode;

	private String transactionDate;

	private String updatedOn;

	private String beneficiaryName;

	private String beneficiaryNickName;

	private String beneficiaryAccountType;

	private String beneficiaryUserType;

	private String createdOn;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getVanNumber() {
		return vanNumber;
	}

	public void setVanNumber(String vanNumber) {
		this.vanNumber = vanNumber;
	}

	public String getBeneficiaryAccountNumber() {
		return beneficiaryAccountNumber;
	}

	public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}

	public String getBeneficiaryIfsc() {
		return beneficiaryIfsc;
	}

	public void setBeneficiaryIfsc(String beneficiaryIfsc) {
		this.beneficiaryIfsc = beneficiaryIfsc;
	}

	public String getRemitterAccountNumber() {
		return remitterAccountNumber;
	}

	public void setRemitterAccountNumber(String remitterAccountNumber) {
		this.remitterAccountNumber = remitterAccountNumber;
	}

	public String getRemitterIfsc() {
		return remitterIfsc;
	}

	public void setRemitterIfsc(String remitterIfsc) {
		this.remitterIfsc = remitterIfsc;
	}

	public String getRemitterName() {
		return remitterName;
	}

	public void setRemitterName(String remitterName) {
		this.remitterName = remitterName;
	}

	public String getBankTxnIdentifier() {
		return bankTxnIdentifier;
	}

	public void setBankTxnIdentifier(String bankTxnIdentifier) {
		this.bankTxnIdentifier = bankTxnIdentifier;
	}

	public String getTransactionRequestId() {
		return transactionRequestId;
	}

	public void setTransactionRequestId(String transactionRequestId) {
		this.transactionRequestId = transactionRequestId;
	}

	public String getTransferMode() {
		return transferMode;
	}

	public void setTransferMode(String transferMode) {
		this.transferMode = transferMode;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryNickName() {
		return beneficiaryNickName;
	}

	public void setBeneficiaryNickName(String beneficiaryNickName) {
		this.beneficiaryNickName = beneficiaryNickName;
	}

	public String getBeneficiaryAccountType() {
		return beneficiaryAccountType;
	}

	public void setBeneficiaryAccountType(String beneficiaryAccountType) {
		this.beneficiaryAccountType = beneficiaryAccountType;
	}

	public String getBeneficiaryUserType() {
		return beneficiaryUserType;
	}

	public void setBeneficiaryUserType(String beneficiaryUserType) {
		this.beneficiaryUserType = beneficiaryUserType;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

}
