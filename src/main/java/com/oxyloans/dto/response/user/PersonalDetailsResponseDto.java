package com.oxyloans.dto.response.user;

import java.math.BigInteger;
import java.util.Date;

import com.oxyloans.request.user.PersonalDetailsRequestDto;
import com.oxyloans.request.user.ReferenceDetailsResponseDto;
import com.oxyloans.request.user.UserProfileUrlsDto;
import com.oxyloans.service.loan.dto.NotificationCountResponseDtoForBorrowerAndLender;
import com.oxyloans.user.type.dto.OxyUserTypeResponseDto;

public class PersonalDetailsResponseDto extends PersonalDetailsRequestDto {

	private Integer id;

	private Integer userId;

	private String userDisplayId;

	private String firstName;

	private String lastName;

	private String fatherName;

	private String gender;

	private String maritalStatus;

	private String nationality;

	private Date createdAt; // dd/MM/yyyy HH:mm:ss

	private Date modifiedAt;

	private Integer loanRequestId = null;

	private boolean emailVerified = false;

	private String email;

	private boolean rateOfInterestUpdated = false;

	private String primaryType;

	private String city;

	private Boolean kycStatus = false;

	private String userStatus;

	private Boolean experianStatus = false;

	private boolean personalDetailsInfo = true;

	private boolean bankDetailsInfo = true;

	private boolean LoanOfferedStatus = false;

	private Double lenderWalletAmount;

	private UserProfileUrlsDto urlsDto;

	private NotificationCountResponseDtoForBorrowerAndLender notificationDetails;

	private String multiChainAddress;

	private String vanNumber;

	private boolean durationIncrement = false;

	private String citizenship;

	private Integer groupId;

	private String groupName;

	private boolean whatsAppNumberUpdated;

	private Double holdAmountInDealParticipation;

	private Double equityAmount;

	private boolean whatsappVerified;

	private boolean userExpertise;

	private boolean bankDetailsVerifiedStatus;

	private String utm;

	private Boolean esignedStatus = false;

	private boolean loansExists = false;

	private Boolean enachStatus = false;

	private String typeOfUser;

	private OxyUserTypeResponseDto oxyUserTypeResponseDto;

	private boolean referenceStatus = false;

	private ReferenceDetailsResponseDto referenceDetailsResponseDto;

	private String documentDriveLink;

	private Double loanEligibility;

	private Double salaryNew;

	private Integer countOfReferees;

	private Double loanEligibilityBasedOnReferring;

	private boolean lenderValidityStatus;

	private boolean walletCalculationToLender = false;

	private BigInteger walletAmountToLender;

	private String loanStatus;

	private String esignStatus;

	private Boolean isNotificationSendAboutValidity;

	private int cibilScore;

	private int profileScore;

	private int referencesCount;

	private Boolean creditReportAcquired;

	private String universityName;
	private String country;
	private String location;

	private Boolean siteTool;

	private Boolean testUser;
	
	private Boolean holdStatus = false;

	private String aadharNumber;

	private Boolean feeStatus;

	private Boolean autoLendingStatus = false;

	public boolean isDurationIncrement() {
		return durationIncrement;
	}

	public void setDurationIncrement(boolean durationIncrement) {
		this.durationIncrement = durationIncrement;
	}

	public String getVanNumber() {
		return vanNumber;
	}

	public void setVanNumber(String vanNumber) {
		this.vanNumber = vanNumber;
	}

	public String getMultiChainAddress() {
		return multiChainAddress;
	}

	public void setMultiChainAddress(String multiChainAddress) {
		this.multiChainAddress = multiChainAddress;
	}

	public UserProfileUrlsDto getUrlsDto() {
		return urlsDto;
	}

	public void setUrlsDto(UserProfileUrlsDto urlsDto) {
		this.urlsDto = urlsDto;
	}

	public Double getLenderWalletAmount() {
		return lenderWalletAmount;
	}

	public void setLenderWalletAmount(Double lenderWalletAmount) {
		this.lenderWalletAmount = lenderWalletAmount;
	}

	public boolean isLoanOfferedStatus() {
		return LoanOfferedStatus;
	}

	public void setLoanOfferedStatus(boolean loanOfferedStatus) {
		LoanOfferedStatus = loanOfferedStatus;
	}

	public boolean isBankDetailsInfo() {
		return bankDetailsInfo;
	}

	public void setBankDetailsInfo(boolean bankDetailsInfo) {
		this.bankDetailsInfo = bankDetailsInfo;
	}

	public boolean isPersonalDetailsInfo() {
		return personalDetailsInfo;
	}

	public void setPersonalDetailsInfo(boolean personalDetailsInfo) {
		this.personalDetailsInfo = personalDetailsInfo;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public Boolean getExperianStatus() {
		return experianStatus;
	}

	public void setExperianStatus(Boolean experianStatus) {
		this.experianStatus = experianStatus;
	}

	public Boolean getKycStatus() {
		return kycStatus;
	}

	public void setKycStatus(Boolean kycStatus) {
		this.kycStatus = kycStatus;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPrimaryType() {
		return primaryType;
	}

	public void setPrimaryType(String primaryType) {
		this.primaryType = primaryType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLoanRequestId() {
		return loanRequestId;
	}

	public void setLoanRequestId(Integer loanRequestId) {
		this.loanRequestId = loanRequestId;
	}

	public boolean isEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserDisplayId() {
		return userDisplayId;
	}

	public void setUserDisplayId(String userDisplayId) {
		this.userDisplayId = userDisplayId;
	}

	public boolean isRateOfInterestUpdated() {
		return rateOfInterestUpdated;
	}

	public void setRateOfInterestUpdated(boolean rateOfInterestUpdated) {
		this.rateOfInterestUpdated = rateOfInterestUpdated;
	}

	public NotificationCountResponseDtoForBorrowerAndLender getNotificationDetails() {
		return notificationDetails;
	}

	public void setNotificationDetails(NotificationCountResponseDtoForBorrowerAndLender notificationDetails) {
		this.notificationDetails = notificationDetails;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public boolean isWhatsAppNumberUpdated() {
		return whatsAppNumberUpdated;
	}

	public void setWhatsAppNumberUpdated(boolean whatsAppNumberUpdated) {
		this.whatsAppNumberUpdated = whatsAppNumberUpdated;
	}

	public Double getHoldAmountInDealParticipation() {
		return holdAmountInDealParticipation;
	}

	public void setHoldAmountInDealParticipation(Double holdAmountInDealParticipation) {
		this.holdAmountInDealParticipation = holdAmountInDealParticipation;
	}

	public Double getEquityAmount() {
		return equityAmount;
	}

	public void setEquityAmount(Double equityAmount) {
		this.equityAmount = equityAmount;
	}

	public boolean isWhatsappVerified() {
		return whatsappVerified;
	}

	public void setWhatsappVerified(boolean whatsappVerified) {
		this.whatsappVerified = whatsappVerified;
	}

	public boolean isUserExpertise() {
		return userExpertise;
	}

	public void setUserExpertise(boolean userExpertise) {
		this.userExpertise = userExpertise;
	}

	public boolean isBankDetailsVerifiedStatus() {
		return bankDetailsVerifiedStatus;
	}

	public void setBankDetailsVerifiedStatus(boolean bankDetailsVerifiedStatus) {
		this.bankDetailsVerifiedStatus = bankDetailsVerifiedStatus;
	}

	public String getUtm() {
		return utm;
	}

	public void setUtm(String utm) {
		this.utm = utm;
	}

	public Boolean getEsignedStatus() {
		return esignedStatus;
	}

	public void setEsignedStatus(Boolean esignedStatus) {
		this.esignedStatus = esignedStatus;
	}

	public boolean isLoansExists() {
		return loansExists;
	}

	public void setLoansExists(boolean loansExists) {
		this.loansExists = loansExists;
	}

	public Boolean getEnachStatus() {
		return enachStatus;
	}

	public void setEnachStatus(Boolean enachStatus) {
		this.enachStatus = enachStatus;
	}

	public String getTypeOfUser() {
		return typeOfUser;
	}

	public void setTypeOfUser(String typeOfUser) {
		this.typeOfUser = typeOfUser;
	}

	public OxyUserTypeResponseDto getOxyUserTypeResponseDto() {
		return oxyUserTypeResponseDto;
	}

	public void setOxyUserTypeResponseDto(OxyUserTypeResponseDto oxyUserTypeResponseDto) {
		this.oxyUserTypeResponseDto = oxyUserTypeResponseDto;
	}

	public boolean isReferenceStatus() {
		return referenceStatus;
	}

	public void setReferenceStatus(boolean referenceStatus) {
		this.referenceStatus = referenceStatus;
	}

	public ReferenceDetailsResponseDto getReferenceDetailsResponseDto() {
		return referenceDetailsResponseDto;
	}

	public void setReferenceDetailsResponseDto(ReferenceDetailsResponseDto referenceDetailsResponseDto) {
		this.referenceDetailsResponseDto = referenceDetailsResponseDto;
	}

	public String getDocumentDriveLink() {
		return documentDriveLink;
	}

	public void setDocumentDriveLink(String documentDriveLink) {
		this.documentDriveLink = documentDriveLink;
	}

	public Double getLoanEligibility() {
		return loanEligibility;
	}

	public void setLoanEligibility(Double loanEligibility) {
		this.loanEligibility = loanEligibility;
	}

	public Double getSalaryNew() {
		return salaryNew;
	}

	public void setSalaryNew(Double salaryNew) {
		this.salaryNew = salaryNew;
	}

	public Integer getCountOfReferees() {
		return countOfReferees;
	}

	public void setCountOfReferees(Integer countOfReferees) {
		this.countOfReferees = countOfReferees;
	}

	public Double getLoanEligibilityBasedOnReferring() {
		return loanEligibilityBasedOnReferring;
	}

	public void setLoanEligibilityBasedOnReferring(Double loanEligibilityBasedOnReferring) {
		this.loanEligibilityBasedOnReferring = loanEligibilityBasedOnReferring;
	}

	public boolean isLenderValidityStatus() {
		return lenderValidityStatus;
	}

	public void setLenderValidityStatus(boolean lenderValidityStatus) {
		this.lenderValidityStatus = lenderValidityStatus;
	}

	public boolean isWalletCalculationToLender() {
		return walletCalculationToLender;
	}

	public void setWalletCalculationToLender(boolean walletCalculationToLender) {
		this.walletCalculationToLender = walletCalculationToLender;
	}

	public BigInteger getWalletAmountToLender() {
		return walletAmountToLender;
	}

	public void setWalletAmountToLender(BigInteger walletAmountToLender) {
		this.walletAmountToLender = walletAmountToLender;
	}

	public String getLoanStatus() {
		return loanStatus;
	}

	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}

	public String getEsignStatus() {
		return esignStatus;
	}

	public void setEsignStatus(String esignStatus) {
		this.esignStatus = esignStatus;
	}

	public Boolean getIsNotificationSendAboutValidity() {
		return isNotificationSendAboutValidity;
	}

	public void setIsNotificationSendAboutValidity(Boolean isNotificationSendAboutValidity) {
		this.isNotificationSendAboutValidity = isNotificationSendAboutValidity;
	}

	public int getCibilScore() {
		return cibilScore;
	}

	public void setCibilScore(int cibilScore) {
		this.cibilScore = cibilScore;
	}

	public int getProfileScore() {
		return profileScore;
	}

	public void setProfileScore(int profileScore) {
		this.profileScore = profileScore;
	}

	public int getReferencesCount() {
		return referencesCount;
	}

	public void setReferencesCount(int referencesCount) {
		this.referencesCount = referencesCount;
	}

	public Boolean getCreditReportAcquired() {
		return creditReportAcquired;
	}

	public void setCreditReportAcquired(Boolean creditReportAcquired) {
		this.creditReportAcquired = creditReportAcquired;
	}

	public String getUniversityName() {
		return universityName;
	}

	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Boolean getSiteTool() {
		return siteTool;
	}

	public void setSiteTool(Boolean siteTool) {
		this.siteTool = siteTool;
	}

	public Boolean getHoldStatus() {
		return holdStatus;
	}

	public void setHoldStatus(Boolean holdStatus) {
		this.holdStatus = holdStatus;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public Boolean getFeeStatus() {
		return feeStatus;
	}

	public void setFeeStatus(Boolean feeStatus) {
		this.feeStatus = feeStatus;
	}

	public Boolean getAutoLendingStatus() {
		return autoLendingStatus;
	}

	public void setAutoLendingStatus(Boolean autoLendingStatus) {
		this.autoLendingStatus = autoLendingStatus;
	}

	public Boolean getTestUser() {
		return testUser;
	}

	public void setTestUser(Boolean testUser) {
		this.testUser = testUser;
	}

}
