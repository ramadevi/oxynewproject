package com.oxyloans.dto.response.user;

import java.util.List;

public class LenderRenewalResponse {

	private List<ValidityResponse> listOfLenders;

	private String message;
	
	private Integer userId;

	public List<ValidityResponse> getListOfLenders() {
		return listOfLenders;
	}

	public void setListOfLenders(List<ValidityResponse> listOfLenders) {
		this.listOfLenders = listOfLenders;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
