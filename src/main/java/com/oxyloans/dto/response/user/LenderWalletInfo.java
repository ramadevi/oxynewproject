package com.oxyloans.dto.response.user;

public class LenderWalletInfo {
	private Integer id;

	private String scrowAccountNumber;

	private Integer transactionAmount;

	private String transactionDate;

	private String lenderName;

	private double walletAmount;

	private String status;

	private Integer documentId;

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getScrowAccountNumber() {
		return scrowAccountNumber;
	}

	public void setScrowAccountNumber(String scrowAccountNumber) {
		this.scrowAccountNumber = scrowAccountNumber;
	}

	public Integer getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(Integer transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public double getWalletAmount() {
		return walletAmount;
	}

	public void setWalletAmount(double walletAmount) {
		this.walletAmount = walletAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
