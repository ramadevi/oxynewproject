package com.oxyloans.dto.response.user;

import java.util.List;

public class BeneficiaryAddedBorrowerInfo {
	private List<ListOfBeneficiarysAddedBorrowers> listOfBeneficiarysAddedBorrowers;

	private Integer listOfBeneficiarysAddedBorrowersCount;

	public List<ListOfBeneficiarysAddedBorrowers> getListOfBeneficiarysAddedBorrowers() {
		return listOfBeneficiarysAddedBorrowers;
	}

	public void setListOfBeneficiarysAddedBorrowers(
			List<ListOfBeneficiarysAddedBorrowers> listOfBeneficiarysAddedBorrowers) {
		this.listOfBeneficiarysAddedBorrowers = listOfBeneficiarysAddedBorrowers;
	}

	public Integer getListOfBeneficiarysAddedBorrowersCount() {
		return listOfBeneficiarysAddedBorrowersCount;
	}

	public void setListOfBeneficiarysAddedBorrowersCount(Integer listOfBeneficiarysAddedBorrowersCount) {
		this.listOfBeneficiarysAddedBorrowersCount = listOfBeneficiarysAddedBorrowersCount;
	}
}
