package com.oxyloans.dto.response.user;

public class ListOfWhatappGroupNames {

	private String GroupName;

	private String dealName;

	private String sentToWhatsappGorups;

	private String chatId;

	public String getGroupName() {
		return GroupName;
	}

	public void setGroupName(String groupName) {
		GroupName = groupName;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getSentToWhatsappGorups() {
		return sentToWhatsappGorups;
	}

	public void setSentToWhatsappGorups(String sentToWhatsappGorups) {
		this.sentToWhatsappGorups = sentToWhatsappGorups;
	}

	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}

}
