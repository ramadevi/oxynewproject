package com.oxyloans.dto.response.user;

import java.util.List;

public class ActiveLendersCountResponse {
	
    private Integer totalCount;
	
	private List<ActiveLendersResponse> activeLendersResponse;

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public List<ActiveLendersResponse> getActiveLendersResponse() {
		return activeLendersResponse;
	}

	public void setActiveLendersResponse(List<ActiveLendersResponse> activeLendersResponse) {
		this.activeLendersResponse = activeLendersResponse;
	}
	   

	
}
