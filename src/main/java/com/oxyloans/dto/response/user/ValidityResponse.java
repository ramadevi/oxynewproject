package com.oxyloans.dto.response.user;

public class ValidityResponse {
	
	private Integer userId;
	
	private String validityDate;
	
	private String name;
	
	private String status;
	
	private Boolean validityStatus;
	
	private String modifiedValidityDate;

	private String modifiedParticipatedDate;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getValidityStatus() {
		return validityStatus;
	}

	public void setValidityStatus(Boolean validityStatus) {
		this.validityStatus = validityStatus;
	}

	public String getModifiedValidityDate() {
		return modifiedValidityDate;
	}

	public void setModifiedValidityDate(String modifiedValidityDate) {
		this.modifiedValidityDate = modifiedValidityDate;
	}

	public String getModifiedParticipatedDate() {
		return modifiedParticipatedDate;
	}

	public void setModifiedParticipatedDate(String modifiedParticipatedDate) {
		this.modifiedParticipatedDate = modifiedParticipatedDate;
	}
	
	

}
