package com.oxyloans.dto.response.user;

import java.util.Date;

public class ActiveLendersResponse {
	
	private Integer lenderId;
	
	private String lenderName;
	
	private Double totalParticipationAmount;
	
	private String mobileNumber;
	
	private String email;
	
	private String city;
	
	private String state;
	
	private String pincode;
	
	private String address;
	
    private String panNumber;
	
	private Date dob;
	
	private Integer lenderGroupId;
	
	private String lenderGroup;
	
	private Date userRegisterDate;
	
	private String aadharNumber;
	
	private String gender;
	
	private Double currentWalletAmount;
	
	private String utm;
	
	private String bankAccNumber;
	
	private String ifsc;
	
	private String bankName;
	
	private String branchName;
	
	private String whastappNumber;

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public Double getTotalParticipationAmount() {
		return totalParticipationAmount;
	}

	public void setTotalParticipationAmount(Double totalParticipationAmount) {
		this.totalParticipationAmount = totalParticipationAmount;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Integer getLenderGroupId() {
		return lenderGroupId;
	}

	public void setLenderGroupId(Integer lenderGroupId) {
		this.lenderGroupId = lenderGroupId;
	}

	public String getLenderGroup() {
		return lenderGroup;
	}

	public void setLenderGroup(String lenderGroup) {
		this.lenderGroup = lenderGroup;
	}

	public Date getUserRegisterDate() {
		return userRegisterDate;
	}

	public void setUserRegisterDate(Date userRegisterDate) {
		this.userRegisterDate = userRegisterDate;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Double getCurrentWalletAmount() {
		return currentWalletAmount;
	}

	public void setCurrentWalletAmount(Double currentWalletAmount) {
		this.currentWalletAmount = currentWalletAmount;
	}

	public String getUtm() {
		return utm;
	}

	public void setUtm(String utm) {
		this.utm = utm;
	}

	public String getBankAccNumber() {
		return bankAccNumber;
	}

	public void setBankAccNumber(String bankAccNumber) {
		this.bankAccNumber = bankAccNumber;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getWhastappNumber() {
		return whastappNumber;
	}

	public void setWhastappNumber(String whastappNumber) {
		this.whastappNumber = whastappNumber;
	}
	
	
	
}
