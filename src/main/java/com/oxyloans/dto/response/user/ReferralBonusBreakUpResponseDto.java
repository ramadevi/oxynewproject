package com.oxyloans.dto.response.user;

import java.math.BigInteger;

public class ReferralBonusBreakUpResponseDto {

	private BigInteger bonusAmount;

	private String participatedDate;

	private String returnedDate;

	private BigInteger participatedAmount;

	private String remarks;

	public BigInteger getBonusAmount() {
		return bonusAmount;
	}

	public String getParticipatedDate() {
		return participatedDate;
	}

	public String getReturnedDate() {
		return returnedDate;
	}

	public BigInteger getParticipatedAmount() {
		return participatedAmount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setBonusAmount(BigInteger bonusAmount) {
		this.bonusAmount = bonusAmount;
	}

	public void setParticipatedDate(String participatedDate) {
		this.participatedDate = participatedDate;
	}

	public void setReturnedDate(String returnedDate) {
		this.returnedDate = returnedDate;
	}

	public void setParticipatedAmount(BigInteger participatedAmount) {
		this.participatedAmount = participatedAmount;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
