package com.oxyloans.dto.response.user;

import java.util.ArrayList;
import java.util.List;

public class DownLoadLinkResponse {
	
	private String downloadLink;

    List<ActiveLendersResponse> activeUserList = new ArrayList<>();
	
	List<HighestReferralBonusResponse> highestBonusList = new ArrayList<>();

	public String getDownloadLink() {
		return downloadLink;
	}

	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}

	public List<ActiveLendersResponse> getActiveUserList() {
		return activeUserList;
	}

	public void setActiveUserList(List<ActiveLendersResponse> activeUserList) {
		this.activeUserList = activeUserList;
	}

	public List<HighestReferralBonusResponse> getHighestBonusList() {
		return highestBonusList;
	}

	public void setHighestBonusList(List<HighestReferralBonusResponse> highestBonusList) {
		this.highestBonusList = highestBonusList;
	}
	
	

}
