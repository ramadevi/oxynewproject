package com.oxyloans.dto.response.user;

public class NodalAccountRequestDto {
	private String iss = "PAYTM";

	private String timestamp;

	private String partnerId = "NODAL_API_SUITE_OXY_000213";

	private String requestReferenceId = "OXTTRADE";

	public String getIss() {
		return iss;
	}

	public void setIss(String iss) {
		this.iss = iss;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getRequestReferenceId() {
		return requestReferenceId;
	}

	public void setRequestReferenceId(String requestReferenceId) {
		this.requestReferenceId = requestReferenceId;
	}

}
