package com.oxyloans.dto.response.user;

import java.util.List;

public class UserQueryDetailsResponse {

	private List<UserQueryDetailsResponseDto> listOfUserQueryDetailsResponseDto;
	
	private Integer count;
	
private Integer lenderCount;
	
	private Integer borrowerCount;
	

	public List<UserQueryDetailsResponseDto> getListOfUserQueryDetailsResponseDto() {
		return listOfUserQueryDetailsResponseDto;
	}

	public void setListOfUserQueryDetailsResponseDto(List<UserQueryDetailsResponseDto> listOfUserQueryDetailsResponseDto) {
		this.listOfUserQueryDetailsResponseDto = listOfUserQueryDetailsResponseDto;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getLenderCount() {
		return lenderCount;
	}

	public void setLenderCount(Integer lenderCount) {
		this.lenderCount = lenderCount;
	}

	public Integer getBorrowerCount() {
		return borrowerCount;
	}

	public void setBorrowerCount(Integer borrowerCount) {
		this.borrowerCount = borrowerCount;
	}
	
	
}
