package com.oxyloans.dto.response.user;

import java.math.BigInteger;

public class LenderNocResponseDto {
	private String status;

	private double profitValue;

	private String lenderUniqueNumber;

	private String lenderProfitUrl;

	private String lenderProfit;

	private String lenderIndividualProfit;
	
	private BigInteger totalInterestEarned;

	public double getProfitValue() {
		return profitValue;
	}

	public void setProfitValue(double profitValue) {
		this.profitValue = profitValue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLenderUniqueNumber() {
		return lenderUniqueNumber;
	}

	public void setLenderUniqueNumber(String lenderUniqueNumber) {
		this.lenderUniqueNumber = lenderUniqueNumber;
	}

	public String getLenderProfitUrl() {
		return lenderProfitUrl;
	}

	public void setLenderProfitUrl(String lenderProfitUrl) {
		this.lenderProfitUrl = lenderProfitUrl;
	}

	public String getLenderProfit() {
		return lenderProfit;
	}

	public void setLenderProfit(String lenderProfit) {
		this.lenderProfit = lenderProfit;
	}

	public String getLenderIndividualProfit() {
		return lenderIndividualProfit;
	}

	public void setLenderIndividualProfit(String lenderIndividualProfit) {
		this.lenderIndividualProfit = lenderIndividualProfit;
	}

	public BigInteger getTotalInterestEarned() {
		return totalInterestEarned;
	}

	public void setTotalInterestEarned(BigInteger totalInterestEarned) {
		this.totalInterestEarned = totalInterestEarned;
	}

}
