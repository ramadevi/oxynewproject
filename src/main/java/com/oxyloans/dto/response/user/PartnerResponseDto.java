package com.oxyloans.dto.response.user;

import java.math.BigInteger;

public class PartnerResponseDto {

	private String name;

	private String email;

	private String mobileNumber;

	private String utmName;

	private Integer id;

	private String mobileOtpSession;

	private boolean mobileverified;

	private boolean emailVerified;

	private BigInteger todayRegisteredLenders;

	private BigInteger totalLenders;

	private BigInteger todayRegisteredBorrowers;

	private BigInteger totalBorrowers;

	private BigInteger runningLoansCount;

	private BigInteger runningLoansAmount;

	private BigInteger closedLoansCount;

	private BigInteger totalDisbursedAmount;

	private BigInteger totalFeePaid;

	private String accountNumber;

	private String bankName;

	private String branchName;

	private String ifscCode;

	private String userName;

	private String status;

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public String getUtmName() {
		return utmName;
	}

	public Integer getId() {
		return id;
	}

	public String getMobileOtpSession() {
		return mobileOtpSession;
	}

	public boolean isMobileverified() {
		return mobileverified;
	}

	public boolean isEmailVerified() {
		return emailVerified;
	}

	public BigInteger getTodayRegisteredLenders() {
		return todayRegisteredLenders;
	}

	public BigInteger getTotalLenders() {
		return totalLenders;
	}

	public BigInteger getTodayRegisteredBorrowers() {
		return todayRegisteredBorrowers;
	}

	public BigInteger getTotalBorrowers() {
		return totalBorrowers;
	}

	public BigInteger getRunningLoansCount() {
		return runningLoansCount;
	}

	public BigInteger getRunningLoansAmount() {
		return runningLoansAmount;
	}

	public BigInteger getClosedLoansCount() {
		return closedLoansCount;
	}

	public BigInteger getTotalDisbursedAmount() {
		return totalDisbursedAmount;
	}

	public BigInteger getTotalFeePaid() {
		return totalFeePaid;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public String getUserName() {
		return userName;
	}

	public String getStatus() {
		return status;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public void setUtmName(String utmName) {
		this.utmName = utmName;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setMobileOtpSession(String mobileOtpSession) {
		this.mobileOtpSession = mobileOtpSession;
	}

	public void setMobileverified(boolean mobileverified) {
		this.mobileverified = mobileverified;
	}

	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	public void setTodayRegisteredLenders(BigInteger todayRegisteredLenders) {
		this.todayRegisteredLenders = todayRegisteredLenders;
	}

	public void setTotalLenders(BigInteger totalLenders) {
		this.totalLenders = totalLenders;
	}

	public void setTodayRegisteredBorrowers(BigInteger todayRegisteredBorrowers) {
		this.todayRegisteredBorrowers = todayRegisteredBorrowers;
	}

	public void setTotalBorrowers(BigInteger totalBorrowers) {
		this.totalBorrowers = totalBorrowers;
	}

	public void setRunningLoansCount(BigInteger runningLoansCount) {
		this.runningLoansCount = runningLoansCount;
	}

	public void setRunningLoansAmount(BigInteger runningLoansAmount) {
		this.runningLoansAmount = runningLoansAmount;
	}

	public void setClosedLoansCount(BigInteger closedLoansCount) {
		this.closedLoansCount = closedLoansCount;
	}

	public void setTotalDisbursedAmount(BigInteger totalDisbursedAmount) {
		this.totalDisbursedAmount = totalDisbursedAmount;
	}

	public void setTotalFeePaid(BigInteger totalFeePaid) {
		this.totalFeePaid = totalFeePaid;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
