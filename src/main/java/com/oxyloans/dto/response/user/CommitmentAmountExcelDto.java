package com.oxyloans.dto.response.user;

public class CommitmentAmountExcelDto {

	private String virtualAccountnumber;
	private int userId;
	private String lenderName;
	private Double commitmentAmountPromised;

	private Double commitmentAmountConsidered;

	public String getVirtualAccountnumber() {
		return virtualAccountnumber;
	}

	public void setVirtualAccountnumber(String virtualAccountnumber) {
		this.virtualAccountnumber = virtualAccountnumber;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public Double getCommitmentAmountPromised() {
		return commitmentAmountPromised;
	}

	public void setCommitmentAmountPromised(Double commitmentAmountPromised) {
		this.commitmentAmountPromised = commitmentAmountPromised;
	}

	public Double getCommitmentAmountConsidered() {
		return commitmentAmountConsidered;
	}

	public void setCommitmentAmountConsidered(Double commitmentAmountConsidered) {
		this.commitmentAmountConsidered = commitmentAmountConsidered;
	}

}
