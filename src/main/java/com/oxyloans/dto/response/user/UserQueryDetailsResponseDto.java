package com.oxyloans.dto.response.user;

import java.util.List;

public class UserQueryDetailsResponseDto {

	private Integer id;

	private Integer userId;

	private String query;

	private String status;

	private Integer documentId;

	private String screenshotUrl;

	private String mobileNumber;

	private String email;

	private String fileName;

	private String filePath;

	private String userNewId;

	private String ticketId;

	private String comments;

	private String resolvedBy;
	
	private String name;
	
	private String receivedOn;

	private String respondedOn;

	private List<UserPendingQuereisResponseDto> listOfPendingQueries;

	private Integer sNo;

	private String adminFileName;

	private String adminFilePath;

	private String adminScreenshotUrl;

	private Integer adminDocumentId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public String getScreenshotUrl() {
		return screenshotUrl;
	}

	public void setScreenshotUrl(String screenshotUrl) {
		this.screenshotUrl = screenshotUrl;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getUserNewId() {
		return userNewId;
	}

	public void setUserNewId(String userNewId) {
		this.userNewId = userNewId;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReceivedOn() {
		return receivedOn;
	}

	public void setReceivedOn(String receivedOn) {
		this.receivedOn = receivedOn;
	}

	public String getRespondedOn() {
		return respondedOn;
	}

	public void setRespondedOn(String respondedOn) {
		this.respondedOn = respondedOn;
	}

	public List<UserPendingQuereisResponseDto> getListOfPendingQueries() {
		return listOfPendingQueries;
	}

	public void setListOfPendingQueries(List<UserPendingQuereisResponseDto> listOfPendingQueries) {
		this.listOfPendingQueries = listOfPendingQueries;
	}

	public Integer getsNo() {
		return sNo;
	}

	public void setsNo(Integer sNo) {
		this.sNo = sNo;
	}

	public String getAdminFileName() {
		return adminFileName;
	}

	public void setAdminFileName(String adminFileName) {
		this.adminFileName = adminFileName;
	}

	public String getAdminFilePath() {
		return adminFilePath;
	}

	public void setAdminFilePath(String adminFilePath) {
		this.adminFilePath = adminFilePath;
	}

	public String getAdminScreenshotUrl() {
		return adminScreenshotUrl;
	}

	public void setAdminScreenshotUrl(String adminScreenshotUrl) {
		this.adminScreenshotUrl = adminScreenshotUrl;
	}

	public Integer getAdminDocumentId() {
		return adminDocumentId;
	}

	public void setAdminDocumentId(Integer adminDocumentId) {
		this.adminDocumentId = adminDocumentId;
	}

}
