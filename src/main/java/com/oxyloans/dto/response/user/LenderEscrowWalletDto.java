package com.oxyloans.dto.response.user;

public class LenderEscrowWalletDto {
	private String fileStatus;

	private String emailStatus;

	public String getFileStatus() {
		return fileStatus;
	}

	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}

	public String getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}
}
