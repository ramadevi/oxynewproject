package com.oxyloans.dto.response.user;

public class AccountBalanceResponseDto {
	private String accountNumber;

	private double effectiveBalance;

	private double slfdBalance;

	private String status;

	private String txn_id;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getEffectiveBalance() {
		return effectiveBalance;
	}

	public void setEffectiveBalance(double effectiveBalance) {
		this.effectiveBalance = effectiveBalance;
	}

	public double getSlfdBalance() {
		return slfdBalance;
	}

	public void setSlfdBalance(double slfdBalance) {
		this.slfdBalance = slfdBalance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTxn_id() {
		return txn_id;
	}

	public void setTxn_id(String txn_id) {
		this.txn_id = txn_id;
	}

}
