package com.oxyloans.dto.response.user;

import java.util.List;

public class LendersLoanInfo {
	private List<LenderDetails> listOfLenderDetails;

	private Integer totalCountOfLenders;

	private List<LenderLoansInformation> listOfLenderLoansInformation;

	public List<LenderDetails> getListOfLenderDetails() {
		return listOfLenderDetails;
	}

	public void setListOfLenderDetails(List<LenderDetails> listOfLenderDetails) {
		this.listOfLenderDetails = listOfLenderDetails;
	}

	public Integer getTotalCountOfLenders() {
		return totalCountOfLenders;
	}

	public void setTotalCountOfLenders(Integer totalCountOfLenders) {
		this.totalCountOfLenders = totalCountOfLenders;
	}

	public List<LenderLoansInformation> getListOfLenderLoansInformation() {
		return listOfLenderLoansInformation;
	}

	public void setListOfLenderLoansInformation(List<LenderLoansInformation> listOfLenderLoansInformation) {
		this.listOfLenderLoansInformation = listOfLenderLoansInformation;
	}

}
