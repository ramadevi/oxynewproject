package com.oxyloans.dto.response.user;

public class BeneficiaryResponseDto extends BeneficiaryRequestDto {

	private Integer userId;

	private String beneficiaryAccountNumber;

	private String beneficiaryAccountType;

	private String beneficiaryIfsc;

	private String beneficiaryName;

	private String beneficiaryNickName;

	private String beneficiaryUserType;

	private String createdOn;

	private String status;

	private Integer borrowerParentRequestId;

	public Integer getBorrowerParentRequestId() {
		return borrowerParentRequestId;
	}

	public void setBorrowerParentRequestId(Integer borrowerParentRequestId) {
		this.borrowerParentRequestId = borrowerParentRequestId;
	}

	public String getBeneficiaryAccountNumber() {
		return beneficiaryAccountNumber;
	}

	public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}

	public String getBeneficiaryAccountType() {
		return beneficiaryAccountType;
	}

	public void setBeneficiaryAccountType(String beneficiaryAccountType) {
		this.beneficiaryAccountType = beneficiaryAccountType;
	}

	public String getBeneficiaryIfsc() {
		return beneficiaryIfsc;
	}

	public void setBeneficiaryIfsc(String beneficiaryIfsc) {
		this.beneficiaryIfsc = beneficiaryIfsc;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryNickName() {
		return beneficiaryNickName;
	}

	public void setBeneficiaryNickName(String beneficiaryNickName) {
		this.beneficiaryNickName = beneficiaryNickName;
	}

	public String getBeneficiaryUserType() {
		return beneficiaryUserType;
	}

	public void setBeneficiaryUserType(String beneficiaryUserType) {
		this.beneficiaryUserType = beneficiaryUserType;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
