package com.oxyloans.dto.lenderspendingamountdto;

import java.util.List;

public class OxyStatusBasedPendingLendersResponse {

	private int totalCount;

	private List<OxyLendersPendingAmountResponseDto> oxyLendersPendingAmountResponseDto;

	public List<OxyLendersPendingAmountResponseDto> getOxyLendersPendingAmountResponseDto() {
		return oxyLendersPendingAmountResponseDto;
	}

	public void setOxyLendersPendingAmountResponseDto(
			List<OxyLendersPendingAmountResponseDto> oxyLendersPendingAmountResponseDto) {
		this.oxyLendersPendingAmountResponseDto = oxyLendersPendingAmountResponseDto;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
