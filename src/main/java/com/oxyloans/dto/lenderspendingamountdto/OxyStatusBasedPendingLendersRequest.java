package com.oxyloans.dto.lenderspendingamountdto;

public class OxyStatusBasedPendingLendersRequest {

	private Integer pageNo;

	private Integer pageSize;

	public Integer getPageNo() {
		return pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

}
