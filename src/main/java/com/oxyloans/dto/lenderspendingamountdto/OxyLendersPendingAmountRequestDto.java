package com.oxyloans.dto.lenderspendingamountdto;

public class OxyLendersPendingAmountRequestDto {

	private Integer id;

	private Integer userId;

	private Double amount;

	private Integer dealId;

	private String reason;

	private String amountType;

	private String transactionType;

	private Integer noOfDays;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public Double getAmount() {
		return amount;
	}

	public Integer getDealId() {
		return dealId;
	}

	public String getReason() {
		return reason;
	}

	public String getAmountType() {
		return amountType;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

}
