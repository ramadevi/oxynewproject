package com.oxyloans.dto.lenderspendingamountdto;

public class OxyLendersPendingAmountResponseDto {

	private Integer id;

	private Integer userId;

	private Double amount;

	private Integer dealId;

	private String dealName;

	private String reason;

	private String amountType;

	private String transactionType;

	private Integer noOfDays;

	private String status;

	private String createdDate;

	private String approvedDate;

	private String h2hFile;

	public Integer getId() {
		return id;
	}

	public Integer getUserId() {
		return userId;
	}

	public Double getAmount() {
		return amount;
	}

	public Integer getDealId() {
		return dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public String getReason() {
		return reason;
	}

	public String getAmountType() {
		return amountType;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public String getStatus() {
		return status;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getApprovedDate() {
		return approvedDate;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getH2hFile() {
		return h2hFile;
	}

	public void setH2hFile(String h2hFile) {
		this.h2hFile = h2hFile;
	}

}
