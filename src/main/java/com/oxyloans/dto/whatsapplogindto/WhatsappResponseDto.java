package com.oxyloans.dto.whatsapplogindto;

import java.util.List;

import com.oxyloans.whatsappdto.WhatsappLoginResponse;

public class WhatsappResponseDto {

	private String whatsappNumber;

	private String session;

	private String otp;

	private Integer id;

	private String otpGeneratedTime;
	
	private List<WhatsappLoginResponse> whatsappLoginResponse ;
	
	private String status;
	
	private String message;
	
	public String getWhatsappNumber() {
		return whatsappNumber;
	}

	public void setWhatsappNumber(String whatsappNumber) {
		this.whatsappNumber = whatsappNumber;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOtpGeneratedTime() {
		return otpGeneratedTime;
	}

	public void setOtpGeneratedTime(String otpGeneratedTime) {
		this.otpGeneratedTime = otpGeneratedTime;
	}

	

	public List<WhatsappLoginResponse> getWhatsappLoginResponse() {
		return whatsappLoginResponse;
	}

	public void setWhatsappLoginResponse(List<WhatsappLoginResponse> whatsappLoginResponse) {
		this.whatsappLoginResponse = whatsappLoginResponse;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	

}
