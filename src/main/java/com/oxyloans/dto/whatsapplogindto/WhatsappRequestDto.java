package com.oxyloans.dto.whatsapplogindto;

public class WhatsappRequestDto {

	private String whatsappNumber;

	private String session;

	private String otp;

	private Integer id;

	private String otpGeneratedTime;

	public String getWhatsappNumber() {
		return whatsappNumber;
	}

	public void setWhatsappNumber(String whatsappNumber) {
		this.whatsappNumber = whatsappNumber;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOtpGeneratedTime() {
		return otpGeneratedTime;
	}

	public void setOtpGeneratedTime(String otpGeneratedTime) {
		this.otpGeneratedTime = otpGeneratedTime;
	}

}
