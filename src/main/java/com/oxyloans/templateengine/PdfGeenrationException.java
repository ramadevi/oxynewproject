package com.oxyloans.templateengine;

public class PdfGeenrationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PdfGeenrationException() {
		super();
	}

	public PdfGeenrationException(String message) {
		super(message);
	}

	public PdfGeenrationException(String message, Throwable cause) {
		super(message, cause);
	}

	public PdfGeenrationException(Throwable cause) {
		super(cause);
	}

}
