package com.oxyloans.templateengine.velocity;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import com.oxyloans.engine.template.ITemplateEngine;
import com.oxyloans.engine.template.TemplateContext;

public class VelocityTemplateEngine implements ITemplateEngine {

	private static final Logger logger = LogManager.getLogger(VelocityTemplateEngine.class);
	
	private String fileEncoding;
	
	private VelocityEngine engine;
	
	public VelocityTemplateEngine(String velocityPropertiesFileName) throws Exception {
		InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(velocityPropertiesFileName);
		Properties properties = new Properties();
		properties.load(resourceAsStream);
		this.engine = new VelocityEngine(properties);
		this.fileEncoding = "UTF-8";
	}

	@Override
	public String generateContent(String templateName, TemplateContext templateContext) {
		try {
			logger.info("Generating template with templateName {} and context {}", templateName, templateContext);
			Template template = this.engine.getTemplate(templateName, this.fileEncoding);
			VelocityContext context = new VelocityContext(templateContext);
			StringWriter writer = new StringWriter();
			template.merge(context, writer);
			return writer.toString();
		} catch (Exception e) {
			logger.error("Exception : ", e);
		}
		return null;
	}
	
	public static void main(String[] args) {
		System.out.println(ClasspathResourceLoader.class.getName());
	}

}
