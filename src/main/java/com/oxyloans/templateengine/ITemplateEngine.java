package com.oxyloans.templateengine;

public interface ITemplateEngine {
	
	public String generateContent(String templateName, TemplateContext templateContext);

}
