package com.oxyloans.templateengine;

public interface IPdfEngine {
	
	/**
	 * 
	 * @param templateName
	 * @param templateContext
	 * @return the temp file path with file name
	 */
	public String generatePdf(String templateName, TemplateContext templateContext, String outputFileName) throws PdfGeenrationException;
	
	public String esignPdf(String pdfFilePath, TemplateContext templateContext);

}
