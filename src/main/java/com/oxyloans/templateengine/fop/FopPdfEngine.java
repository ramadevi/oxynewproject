package com.oxyloans.templateengine.fop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.StringReader;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

import com.oxyloans.engine.template.IPdfEngine;
import com.oxyloans.engine.template.ITemplateEngine;
import com.oxyloans.engine.template.PdfGeenrationException;
import com.oxyloans.engine.template.TemplateContext;

public class FopPdfEngine implements IPdfEngine {
	
	private ITemplateEngine templateEngine;
	
	private FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
	
	public FopPdfEngine(ITemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}

	@Override
	public String generatePdf(String templateName, TemplateContext templateContext, String outputFileName) throws PdfGeenrationException {
		
		String sourceXml = templateEngine.generateContent(templateName, templateContext);
		// Prepare output stream to which PDF will be written
		File resFile = new File(outputFileName);
		try {
			Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, new FileOutputStream(resFile));
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setParameter("versionParam", "2.0");
			Source src = new StreamSource(new StringReader(sourceXml));
			Result res = new SAXResult(fop.getDefaultHandler());
			transformer.transform(src, res);
		} catch (Exception e) {
			throw new PdfGeenrationException(e);
		}
		return resFile.getAbsolutePath();
	}

	@Override
	public String esignPdf(String pdfFilePath, TemplateContext templateContext) {
		return null;
	}

}
