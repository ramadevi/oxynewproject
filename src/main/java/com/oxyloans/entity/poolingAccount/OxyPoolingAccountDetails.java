package com.oxyloans.entity.poolingAccount;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

@Entity
@Table(name = "oxy_pooling_account_details")
public class OxyPoolingAccountDetails {

	@javax.persistence.Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer Id;

	@Column(name = "name", nullable = true)
	private String name;

	@Column(name = "current_amount", nullable = true)
	private double currentAmount;

	@Column(name = "interest_amount", nullable = true)
	private double interestAmount;

	@Column(name = "interest_date", nullable = true)
	private Integer interestDate;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(double currentAmount) {
		this.currentAmount = currentAmount;
	}

	public double getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(double interestAmount) {
		this.interestAmount = interestAmount;
	}

	public Integer getInterestDate() {
		return interestDate;
	}

	public void setInterestDate(Integer interestDate) {
		this.interestDate = interestDate;
	}

}
