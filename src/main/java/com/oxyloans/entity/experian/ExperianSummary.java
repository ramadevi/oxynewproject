package com.oxyloans.entity.experian;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

import com.oxyloans.entity.user.User;

@Entity(name="experian_summary")
public class ExperianSummary {
	
	public String getExperianFilePath() {
		return experianFilePath;
	}

	public void setExperianFilePath(String experianFilePath) {
		this.experianFilePath = experianFilePath;
	}

	@Id
	@Column(name = "user_id", unique = true)
	private Integer userId;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	private User user;
	
	@Column(name="score",nullable=false)
	private Integer score;
	
	@Column(name="credit_account_total",nullable=false)
	private Integer creditAccountTotal;
	
	@Column(name="credit_account_active",nullable=false)
	private Integer creditAccountActive;
	
	@Column(name="credit_account_closed",nullable=false)
	private Integer creditAccountClosed;
	
	@Column(name="outstanding_balance_all",nullable=false)
	private Integer outstandingBalanceAll;
	
	@Column(name="outstanding_balance_secured",nullable=false)
	private Integer outstandingBalanceSecured;
	
	@Column(name="outstanding_balance_unsecured",nullable=false)
	private Integer outstandingBalanceUnSecured;
	
	@Column(name="experian_file_path",nullable=false)
	private String  experianFilePath;
	
	@Column(name="file_name",nullable=false)
	private String fileName;
	
	@Column(name="oxy_score",nullable=true)
	private Integer oxyScore;

	@Column(name="credit_score_by_paisabazaar",nullable=true)
	private Integer creditScoreByPaisabazaar;
	
	
	public Integer getCreditScoreByPaisabazaar() {
		return creditScoreByPaisabazaar;
	}

	public void setCreditScoreByPaisabazaar(Integer creditScoreByPaisabazaar) {
		this.creditScoreByPaisabazaar = creditScoreByPaisabazaar;
	}

	public Integer getOxyScore() {
		return oxyScore;
	}

	public void setOxyScore(Integer oxyScore) {
		this.oxyScore = oxyScore;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Integer getCreditAccountTotal() {
		return creditAccountTotal;
	}

	public void setCreditAccountTotal(Integer creditAccountTotal) {
		this.creditAccountTotal = creditAccountTotal;
	}

	public Integer getCreditAccountActive() {
		return creditAccountActive;
	}

	public void setCreditAccountActive(Integer creditAccountActive) {
		this.creditAccountActive = creditAccountActive;
	}

	public Integer getCreditAccountClosed() {
		return creditAccountClosed;
	}

	public void setCreditAccountClosed(Integer creditAccountClosed) {
		this.creditAccountClosed = creditAccountClosed;
	}

	public Integer getOutstandingBalanceAll() {
		return outstandingBalanceAll;
	}

	public void setOutstandingBalanceAll(Integer outstandingBalanceAll) {
		this.outstandingBalanceAll = outstandingBalanceAll;
	}

	public Integer getOutstandingBalanceSecured() {
		return outstandingBalanceSecured;
	}

	public void setOutstandingBalanceSecured(Integer outstandingBalanceSecured) {
		this.outstandingBalanceSecured = outstandingBalanceSecured;
	}

	public Integer getOutstandingBalanceUnSecured() {
		return outstandingBalanceUnSecured;
	}

	public void setOutstandingBalanceUnSecured(Integer outstandingBalanceUnSecured) {
		this.outstandingBalanceUnSecured = outstandingBalanceUnSecured;
	}
	
	
	
	

}
