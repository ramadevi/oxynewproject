package com.oxyloans.entity.fd;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "oxy_fd_creation")
public class OxyFdCreation {

	public static enum TypeOfDeposit {
		CNP, TOQ, TOM, CWP, TPA, TQP, TMP, TRB
	}

	public static enum FdType {
		NPC, WPC
	}

	public static enum Status {
		INITIATED, CREATED, CLOSED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = false)
	private Integer userId;

	@Column(name = "unique_number", nullable = false)
	private String uniqueNumber;

	@Column(name = "account_number", nullable = false)
	private String accountNumber;

	@Column(name = "days_only", nullable = false)
	private Integer daysOnly;

	@Column(name = "fd_amount", nullable = false)
	private Integer fdAmount;

	@Column(name = "type_of_deposit", nullable = false)
	@Enumerated(EnumType.STRING)
	private TypeOfDeposit typeOfDeposit = TypeOfDeposit.TPA;

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status = Status.INITIATED;

	@Column(name = "days", nullable = false)
	private Integer days;

	@Column(name = "months", nullable = false)
	private Integer months;

	@Column(name = "years", nullable = false)
	private Integer years;

	@Column(name = "fd_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private FdType fdType = FdType.WPC;

	@Column(name = "maturity_proceeds", nullable = false)
	private String maturityProceeds;

	@Column(name = "created_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "fd_response", nullable = true)
	private String fdResponse;

	@Column(name = "fd_response_received_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fdResponseReceivedOn;

	@Column(name = "fd_closed_response", nullable = true)
	private String fdClosedResponse;

	@Column(name = "fd_closed_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fdClosedOn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(String uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Integer getDaysOnly() {
		return daysOnly;
	}

	public void setDaysOnly(Integer daysOnly) {
		this.daysOnly = daysOnly;
	}

	public Integer getFdAmount() {
		return fdAmount;
	}

	public void setFdAmount(Integer fdAmount) {
		this.fdAmount = fdAmount;
	}

	public TypeOfDeposit getTypeOfDeposit() {
		return typeOfDeposit;
	}

	public void setTypeOfDeposit(TypeOfDeposit typeOfDeposit) {
		this.typeOfDeposit = typeOfDeposit;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}

	public Integer getMonths() {
		return months;
	}

	public void setMonths(Integer months) {
		this.months = months;
	}

	public Integer getYears() {
		return years;
	}

	public void setYears(Integer years) {
		this.years = years;
	}

	public FdType getFdType() {
		return fdType;
	}

	public void setFdType(FdType fdType) {
		this.fdType = fdType;
	}

	public String getMaturityProceeds() {
		return maturityProceeds;
	}

	public void setMaturityProceeds(String maturityProceeds) {
		this.maturityProceeds = maturityProceeds;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getFdResponse() {
		return fdResponse;
	}

	public void setFdResponse(String fdResponse) {
		this.fdResponse = fdResponse;
	}

	public Date getFdResponseReceivedOn() {
		return fdResponseReceivedOn;
	}

	public void setFdResponseReceivedOn(Date fdResponseReceivedOn) {
		this.fdResponseReceivedOn = fdResponseReceivedOn;
	}

	public String getFdClosedResponse() {
		return fdClosedResponse;
	}

	public void setFdClosedResponse(String fdClosedResponse) {
		this.fdClosedResponse = fdClosedResponse;
	}

	public Date getFdClosedOn() {
		return fdClosedOn;
	}

	public void setFdClosedOn(Date fdClosedOn) {
		this.fdClosedOn = fdClosedOn;
	}

}
