package com.oxyloans.entity.esign;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "esign_transactions")
public class EsignTransactions implements IEntity {
	
	public static enum TransactonStatus {
		INITIATED, SUCCESS, DOWNLOADED, FAILED
	}
	
	public static enum EsignType {
		AADHAR, MOBILE
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "co_ordinate_x", nullable = false)
	private Integer coOrdinateX;

	@Column(name = "co_ordinate_y", nullable = false)
	private Integer coOrdinateY;

	//This is actual Loan Id
	@Column(name = "transaction_id", unique = true, nullable = false)
	private String transactionId;

	@Column(name = "transacton_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private TransactonStatus transactonStatus;

	@Column(name = "esigned_doc_downloaded", nullable = false)
	private Boolean esignedDocDownloaded = false;

	@Column(name = "transacton_error_message", nullable = true)
	private String transactonErrorMessage;

	@Column(name = "esign_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private EsignType esignType = EsignType.AADHAR;

	public Integer getCoOrdinateX() {
		return coOrdinateX;
	}

	public void setCoOrdinateX(Integer coOrdinateX) {
		this.coOrdinateX = coOrdinateX;
	}

	public Integer getCoOrdinateY() {
		return coOrdinateY;
	}

	public void setCoOrdinateY(Integer coOrdinateY) {
		this.coOrdinateY = coOrdinateY;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public TransactonStatus getTransactonStatus() {
		return transactonStatus;
	}

	public void setTransactonStatus(TransactonStatus transactonStatus) {
		this.transactonStatus = transactonStatus;
	}

	public Boolean getEsignedDocDownloaded() {
		return esignedDocDownloaded;
	}

	public void setEsignedDocDownloaded(Boolean esignedDocDownloaded) {
		this.esignedDocDownloaded = esignedDocDownloaded;
	}

	public String getTransactonErrorMessage() {
		return transactonErrorMessage;
	}

	public void setTransactonErrorMessage(String transactonErrorMessage) {
		this.transactonErrorMessage = transactonErrorMessage;
	}

	public Integer getId() {
		return id;
	}

	public EsignType getEsignType() {
		return esignType;
	}

	public void setEsignType(EsignType esignType) {
		this.esignType = esignType;
	}

}
