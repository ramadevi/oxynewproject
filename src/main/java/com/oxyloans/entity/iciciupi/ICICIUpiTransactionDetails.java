package com.oxyloans.entity.iciciupi;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "icici_upi_transaction_details")
public class ICICIUpiTransactionDetails {

	public static enum TransactionStatus {
		INITIATED, PENDING, SUCCESS, FAILURE, APPROVED, REJECTED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "payer_va", nullable = true)
	private String payerVa;

	@Column(name = "amount", nullable = true)
	private String amount;

	@Column(name = "note", nullable = true)
	private String note;

	@Column(name = "transaction_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private TransactionStatus transactionStatus = TransactionStatus.INITIATED;

	@Column(name = "collect_by_date", nullable = true)
	private String collectByDate;

	@Column(name = "sub_merchant_id", nullable = true)
	private String subMerchantId;

	@Column(name = "sub_merchant_name", nullable = true)
	private String subMerchantName;

	@Column(name = "merchant_tran_id", nullable = true)
	private String merchantTranId;

	@Column(name = "bill_number", nullable = true)
	private String billNumber;

	@Column(name = "transaction_initiation_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date transactionInitiationDate;

	@Column(name = "transaction_initiation_timestamp", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionInitiationDateTimestamp;

	@Column(name = "bank_rrn", nullable = true)
	private String bankRrn;

	@Column(name = "transaction_response", nullable = true)
	private String transactionResponse;

	@Column(name = "transaction_status_response", nullable = true)
	private String transactionStatusResponse;

	@Column(name = "transaction_status_response_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date transactionStatusResponseDate;

	@Column(name = "transaction_status_response_timestamp", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionStatusResponsTimestamp;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPayerVa() {
		return payerVa;
	}

	public void setPayerVa(String payerVa) {
		this.payerVa = payerVa;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public TransactionStatus getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(TransactionStatus transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getCollectByDate() {
		return collectByDate;
	}

	public void setCollectByDate(String collectByDate) {
		this.collectByDate = collectByDate;
	}

	public String getSubMerchantId() {
		return subMerchantId;
	}

	public void setSubMerchantId(String subMerchantId) {
		this.subMerchantId = subMerchantId;
	}

	public String getSubMerchantName() {
		return subMerchantName;
	}

	public void setSubMerchantName(String subMerchantName) {
		this.subMerchantName = subMerchantName;
	}

	public String getMerchantTranId() {
		return merchantTranId;
	}

	public void setMerchantTranId(String merchantTranId) {
		this.merchantTranId = merchantTranId;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public Date getTransactionInitiationDate() {
		return transactionInitiationDate;
	}

	public void setTransactionInitiationDate(Date transactionInitiationDate) {
		this.transactionInitiationDate = transactionInitiationDate;
	}

	public Date getTransactionInitiationDateTimestamp() {
		return transactionInitiationDateTimestamp;
	}

	public void setTransactionInitiationDateTimestamp(Date transactionInitiationDateTimestamp) {
		this.transactionInitiationDateTimestamp = transactionInitiationDateTimestamp;
	}

	public String getBankRrn() {
		return bankRrn;
	}

	public void setBankRrn(String bankRrn) {
		this.bankRrn = bankRrn;
	}

	public String getTransactionResponse() {
		return transactionResponse;
	}

	public void setTransactionResponse(String transactionResponse) {
		this.transactionResponse = transactionResponse;
	}

	public String getTransactionStatusResponse() {
		return transactionStatusResponse;
	}

	public void setTransactionStatusResponse(String transactionStatusResponse) {
		this.transactionStatusResponse = transactionStatusResponse;
	}

	public Date getTransactionStatusResponseDate() {
		return transactionStatusResponseDate;
	}

	public void setTransactionStatusResponseDate(Date transactionStatusResponseDate) {
		this.transactionStatusResponseDate = transactionStatusResponseDate;
	}

	public Date getTransactionStatusResponsTimestamp() {
		return transactionStatusResponsTimestamp;
	}

	public void setTransactionStatusResponsTimestamp(Date transactionStatusResponsTimestamp) {
		this.transactionStatusResponsTimestamp = transactionStatusResponsTimestamp;
	}

}
