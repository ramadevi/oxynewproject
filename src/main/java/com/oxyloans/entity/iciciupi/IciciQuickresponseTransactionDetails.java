package com.oxyloans.entity.iciciupi;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Id;

@Entity
@Table(name = "icici_quickresponse_transaction_details")
public class IciciQuickresponseTransactionDetails {
	public static enum TransactionStatus {
		INITIATED, SUCCESS, FAILURE, NOTYETSCANNED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer Id;

	@Column(name = "amount", nullable = true)
	private String amount;

	@Column(name = "merchant_transaction_id", nullable = true)
	private String merchantTransactionId;

	@Column(name = "bill_number", nullable = true)
	private String billNumber;

	@Column(name = "ref_id", nullable = true)
	private String refId;

	@Column(name = "ref_id_initiated_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date refIdInitiatedOn;

	@Column(name = "user_id", nullable = true)
	private Integer userId;

	@Column(name = "transaction_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private TransactionStatus transactionStatus = TransactionStatus.INITIATED;

	@Column(name = "sub_merchant_id", nullable = true)
	private String subMerchantId;

	@Column(name = "bank_rrn", nullable = true)
	private String bankRRN;

	@Column(name = "call_back_recevied_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date callBackReceviedOn;
	

	@Column(name = "call_back_response", nullable = true)
	private String callBackResponse;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMerchantTransactionId() {
		return merchantTransactionId;
	}

	public void setMerchantTransactionId(String merchantTransactionId) {
		this.merchantTransactionId = merchantTransactionId;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public Date getRefIdInitiatedOn() {
		return refIdInitiatedOn;
	}

	public void setRefIdInitiatedOn(Date refIdInitiatedOn) {
		this.refIdInitiatedOn = refIdInitiatedOn;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public TransactionStatus getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(TransactionStatus transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getSubMerchantId() {
		return subMerchantId;
	}

	public void setSubMerchantId(String subMerchantId) {
		this.subMerchantId = subMerchantId;
	}

	public String getBankRRN() {
		return bankRRN;
	}

	public void setBankRRN(String bankRRN) {
		this.bankRRN = bankRRN;
	}

	public Date getCallBackReceviedOn() {
		return callBackReceviedOn;
	}

	public void setCallBackReceviedOn(Date callBackReceviedOn) {
		this.callBackReceviedOn = callBackReceviedOn;
	}

	public String getCallBackResponse() {
		return callBackResponse;
	}

	public void setCallBackResponse(String callBackResponse) {
		this.callBackResponse = callBackResponse;
	}

}
