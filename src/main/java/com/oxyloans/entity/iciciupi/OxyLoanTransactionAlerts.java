package com.oxyloans.entity.iciciupi;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "oxy_loan_transaction_alerts")
public class OxyLoanTransactionAlerts {

	public static enum TransactionType {
		DEBITED, CREDITED
	}

	public static enum TransactionSubType {
		CI, BI, I, O, IC, IP, SI, BS, SC
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "transaction_initiated_date", nullable = true)
	private String transactionInitiatedDate;

	@Column(name = "bank_transaction_id", nullable = true)
	private String bankTransactionId;

	@Column(name = "transaction_completed_date", nullable = true)
	private String transactionCompletedDate;

	@Column(name = "balance", nullable = true)
	private String balance;

	@Column(name = "amount", nullable = true)
	private String amount;

	@Column(name = "branch_id", nullable = true)
	private String branchId;

	@Column(name = "narration", nullable = true)
	private String narration;

	@Column(name = "transaction_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private TransactionType transactionType = TransactionType.DEBITED;

	@Column(name = "transaction_sub_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private TransactionSubType transactionSubType = TransactionSubType.CI;

	@Column(name = "alert_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date alertOn = new Date();

	@Column(name = "account_number", nullable = true)
	private String accountNumber;

	@Column(name = "transation_alert_response", nullable = true)
	private String transationAlertResponse;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTransactionInitiatedDate() {
		return transactionInitiatedDate;
	}

	public void setTransactionInitiatedDate(String transactionInitiatedDate) {
		this.transactionInitiatedDate = transactionInitiatedDate;
	}

	public String getBankTransactionId() {
		return bankTransactionId;
	}

	public void setBankTransactionId(String bankTransactionId) {
		this.bankTransactionId = bankTransactionId;
	}

	public String getTransactionCompletedDate() {
		return transactionCompletedDate;
	}

	public void setTransactionCompletedDate(String transactionCompletedDate) {
		this.transactionCompletedDate = transactionCompletedDate;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public TransactionSubType getTransactionSubType() {
		return transactionSubType;
	}

	public void setTransactionSubType(TransactionSubType transactionSubType) {
		this.transactionSubType = transactionSubType;
	}

	public Date getAlertOn() {
		return alertOn;
	}

	public void setAlertOn(Date alertOn) {
		this.alertOn = alertOn;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getTransationAlertResponse() {
		return transationAlertResponse;
	}

	public void setTransationAlertResponse(String transationAlertResponse) {
		this.transationAlertResponse = transationAlertResponse;
	}

}
