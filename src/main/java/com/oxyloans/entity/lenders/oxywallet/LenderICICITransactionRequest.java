package com.oxyloans.entity.lenders.oxywallet;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lender_icici_transaction_request")
public class LenderICICITransactionRequest {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "encrypted_key", nullable = true)
	private String encryptedKey;

	@Column(name = "encrypted_data", nullable = true)
	private String encryptedData;

	@Column(name = "received_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date receivedOn = new Date();

	@Column(name = "icici_decrypted_data", nullable = true)
	private String iciciDecryptedData;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEncryptedKey() {
		return encryptedKey;
	}

	public void setEncryptedKey(String encryptedKey) {
		this.encryptedKey = encryptedKey;
	}

	public String getEncryptedData() {
		return encryptedData;
	}

	public void setEncryptedData(String encryptedData) {
		this.encryptedData = encryptedData;
	}

	public Date getReceivedOn() {
		return receivedOn;
	}

	public void setReceivedOn(Date receivedOn) {
		this.receivedOn = receivedOn;
	}

	public String getIciciDecryptedData() {
		return iciciDecryptedData;
	}

	public void setIciciDecryptedData(String iciciDecryptedData) {
		this.iciciDecryptedData = iciciDecryptedData;
	}

}
