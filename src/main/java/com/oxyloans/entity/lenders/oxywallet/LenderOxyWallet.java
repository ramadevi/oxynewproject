package com.oxyloans.entity.lenders.oxywallet;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lender_scrow_wallet")
public class LenderOxyWallet {

	public static enum Status {
		UPLOADED, APPROVED, REJECTED
	}

	public static enum PrincipalToWallet {
		REQUESTED, PRINCIPALRETURNED, INTERESTRETURNED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", unique = true)
	private Integer userId;

	@Column(name = "scrow_account_number", nullable = false)
	private String scrowAccountNumber;

	@Column(name = "transaction_amount", nullable = false)
	private Integer transactionAmount;

	@Column(name = "transaction_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionDate;

	@Column(name = "document_uploaded_id", nullable = false)
	private Integer documentUploadedId;

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status = Status.UPLOADED;

	@Column(name = "created_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate = new Date();

	@Column(name = "created_by", nullable = true)
	private String createdBy;

	@Column(name = "comments", nullable = false)
	private String comments;

	@Column(name = "transactiontype", nullable = false)
	private String transactionType;

	@Column(name = "withdrawfundsid", nullable = false)
	private Integer withdrawFundsId;

	@Column(name = "unique_transaction_reference", nullable = true)
	private String uniqueTransactionReference;

	@Column(name = "icici_decrypted_data", nullable = true)
	private String iciciDecryptedData;

	@Column(name = "remarks", nullable = true)
	private String remarks;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId = 0;

	@Column(name = "principal_to_wallet", nullable = true)
	@Enumerated(EnumType.STRING)
	private PrincipalToWallet principalToWallet = PrincipalToWallet.REQUESTED;

	@Column(name = "interest_to_principal_moved", nullable = true)
	private double interestToPrincipalMoved;

	@Column(name = "wallet_transfer_id", unique = true)
	private Integer walletTransferId = 0;

	@Column(name = "difference_in_days", nullable = true)
	private Integer differenceInDays;

	public Integer getWithdrawFundsId() {
		return withdrawFundsId;
	}

	public void setWithdrawFundsId(Integer withdrawFundsId) {
		this.withdrawFundsId = withdrawFundsId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getScrowAccountNumber() {
		return scrowAccountNumber;
	}

	public void setScrowAccountNumber(String scrowAccountNumber) {
		this.scrowAccountNumber = scrowAccountNumber;
	}

	public Integer getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(Integer transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Integer getDocumentUploadedId() {
		return documentUploadedId;
	}

	public void setDocumentUploadedId(Integer documentUploadedId) {
		this.documentUploadedId = documentUploadedId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUniqueTransactionReference() {
		return uniqueTransactionReference;
	}

	public void setUniqueTransactionReference(String uniqueTransactionReference) {
		this.uniqueTransactionReference = uniqueTransactionReference;
	}

	public String getIciciDecryptedData() {
		return iciciDecryptedData;
	}

	public void setIciciDecryptedData(String iciciDecryptedData) {
		this.iciciDecryptedData = iciciDecryptedData;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public PrincipalToWallet getPrincipalToWallet() {
		return principalToWallet;
	}

	public void setPrincipalToWallet(PrincipalToWallet principalToWallet) {
		this.principalToWallet = principalToWallet;
	}

	public double getInterestToPrincipalMoved() {
		return interestToPrincipalMoved;
	}

	public void setInterestToPrincipalMoved(double interestToPrincipalMoved) {
		this.interestToPrincipalMoved = interestToPrincipalMoved;
	}

	public Integer getWalletTransferId() {
		return walletTransferId;
	}

	public void setWalletTransferId(Integer walletTransferId) {
		this.walletTransferId = walletTransferId;
	}

	public Integer getDifferenceInDays() {
		return differenceInDays;
	}

	public void setDifferenceInDays(Integer differenceInDays) {
		this.differenceInDays = differenceInDays;
	}

}
