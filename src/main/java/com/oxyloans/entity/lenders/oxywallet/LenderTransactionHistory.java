package com.oxyloans.entity.lenders.oxywallet;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lender_transaction_history")
public class LenderTransactionHistory {

	public static enum AmountType {
		CREDIT, DEBIT
	}

	public static enum AmountSubType {
		MW, EC, QR, MPR, SPR, P, PU, WW, DW, WTW, FFW// ec(ecollection),qr(scanner),mpr(manual principal
														// return),spr(system
		// principal
		// return),p(paticipation),pu(paticipation Updation),ww(wallet
		// withdraw),mw(manuval wallet),DW(deal
		// withdraw),(walletToWallet),(freeFromWallet)
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer Id;

	@Column(name = "user_id", nullable = true)
	private Integer userId;

	@Enumerated(EnumType.STRING)
	@Column(name = "amount_type", nullable = true)
	private AmountType amountType = AmountType.CREDIT;

	@Enumerated(EnumType.STRING)
	@Column(name = "amount_sub_type", nullable = true)
	private AmountSubType amountSubType = AmountSubType.EC;

	@Column(name = "amount", nullable = true)
	private Double amount = 0.0;

	@Column(name = "previous_balance", nullable = true)
	private Double previousBalance = 0.0;

	@Column(name = "current_amount", nullable = true)
	private Double currentAmount = 0.0;

	@Column(name = "paid_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date paidDate;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId = 0;

	@Column(name = "created_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	@Column(name = "is_current", nullable = true)
	private Boolean isCurrent = false;

	@Column(name = "remarks", nullable = true)
	private String remarks = "";

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public AmountType getAmountType() {
		return amountType;
	}

	public void setAmountType(AmountType amountType) {
		this.amountType = amountType;
	}

	public AmountSubType getAmountSubType() {
		return amountSubType;
	}

	public void setAmountSubType(AmountSubType amountSubType) {
		this.amountSubType = amountSubType;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getPreviousBalance() {
		return previousBalance;
	}

	public void setPreviousBalance(Double previousBalance) {
		this.previousBalance = previousBalance;
	}

	public Double getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(Double currentAmount) {
		this.currentAmount = currentAmount;
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsCurrent() {
		return isCurrent;
	}

	public void setIsCurrent(Boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
