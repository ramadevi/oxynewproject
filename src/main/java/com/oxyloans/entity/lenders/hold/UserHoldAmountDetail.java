package com.oxyloans.entity.lenders.hold;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name = "user_hold_amount_details")
@Entity
public class UserHoldAmountDetail {

	public static enum Status {
		OPEN, CLOSE, DELETE
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id")
	private Integer userId;

	@Column(name = "deal_id")
	private Integer dealId;

	@Column(name = "hold_amount")
	private Double holdAmount;

	@Column(name = "comments")
	private String comments;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status = Status.OPEN;

	@Column(name = "created_on", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	@OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	@JoinColumn(name = "user_hold_amount_details_id")
	private List<UserHoldAmountMappedToDeal> userHoldAmountMappedToDeal;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Double getHoldAmount() {
		return holdAmount;
	}

	public void setHoldAmount(Double holdAmount) {
		this.holdAmount = holdAmount;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public List<UserHoldAmountMappedToDeal> getUserHoldAmountMappedToDeal() {
		return userHoldAmountMappedToDeal;
	}

	public void setUserHoldAmountMappedToDeal(List<UserHoldAmountMappedToDeal> userHoldAmountMappedToDeal) {
		this.userHoldAmountMappedToDeal = userHoldAmountMappedToDeal;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

}
