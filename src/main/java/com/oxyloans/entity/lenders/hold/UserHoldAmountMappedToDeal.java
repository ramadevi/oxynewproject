package com.oxyloans.entity.lenders.hold;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.user.User;

@Table(name = "user_hold_amount_mapped_to_deals")
@Entity
public class UserHoldAmountMappedToDeal {

	public static enum Status {
		OPEN, CLOSE
	}

	public static enum AmountType {
		INTEREST, PRINCIPAL
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_hold_amount_details_id")
	private Integer userHoldAmountDetailsId;

	@Column(name = "user_id")
	private Integer userId;

	@Column(name = "deal_id")
	private Integer dealId;

	@Column(name = "hold_amount")
	private Double holdAmount;

	@Column(name = "amount_type")
	@Enumerated(EnumType.STRING)
	private AmountType amountType = AmountType.INTEREST;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status = Status.OPEN;

	@Column(name = "created_on", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	@JoinColumn(name = "user_hold_amount_details_id", insertable = false, updatable = false)
	private UserHoldAmountDetail userHoldAmountDetail;

	@Column(name = "amount_returned_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date amountReturnedOn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserHoldAmountDetailsId() {
		return userHoldAmountDetailsId;
	}

	public void setUserHoldAmountDetailsId(Integer userHoldAmountDetailsId) {
		this.userHoldAmountDetailsId = userHoldAmountDetailsId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Double getHoldAmount() {
		return holdAmount;
	}

	public void setHoldAmount(Double holdAmount) {
		this.holdAmount = holdAmount;
	}

	public AmountType getAmountType() {
		return amountType;
	}

	public void setAmountType(AmountType amountType) {
		this.amountType = amountType;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public UserHoldAmountDetail getUserHoldAmountDetail() {
		return userHoldAmountDetail;
	}

	public void setUserHoldAmountDetail(UserHoldAmountDetail userHoldAmountDetail) {
		this.userHoldAmountDetail = userHoldAmountDetail;
	}

	public Date getAmountReturnedOn() {
		return amountReturnedOn;
	}

	public void setAmountReturnedOn(Date amountReturnedOn) {
		this.amountReturnedOn = amountReturnedOn;
	}

}
