package com.oxyloans.entity.lenders.principal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "oxy_principal_return")
public class OxyPrincipalReturn {
	public static enum Status {
		INITIATED, PRINCIPALRETURNED, INTERESTRETURNED, REJECTED
	}

	public static enum AmountType {
		PRINCIPALTOBANK, PRINCIPALTOWALLET
	}

	public static enum PrincipalStatus {
		INITIATED, BEFORE, AFTER, EXECUTED, APPROVED
	}

	public static enum InterestStatus {
		INITIATED, BEFORE, AFTER, EXECUTED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer Id;

	@Column(name = "user_id", nullable = true)
	private Integer userId = 0;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId = 0;

	@Column(name = "principal_amount", nullable = true)
	private Double principalAmount = 0.0;

	@Column(name = "interest_amount", nullable = true)
	private Double interestAmount = 0.0;

	@Column(name = "status", nullable = true)
	@Enumerated(EnumType.STRING)
	private Status status = Status.INITIATED;

	@Column(name = "principal_approved_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date principalApprovedOn;

	@Column(name = "principal_modified_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date principalModifiedOn;

	@Column(name = "interest_approved_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date interestApprovedOn;

	@Column(name = "amount_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private AmountType amountType = AmountType.PRINCIPALTOBANK;

	@Column(name = "difference_in_days", nullable = true)
	private Integer differenceInDays;

	@Column(name = "remarks", nullable = true)
	private String remarks;

	@Column(name = "principal_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private PrincipalStatus principalStatus = PrincipalStatus.INITIATED;

	@Column(name = "interest_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private InterestStatus interestStatus = InterestStatus.INITIATED;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Double getPrincipalAmount() {
		return principalAmount;
	}

	public void setPrincipalAmount(Double principalAmount) {
		this.principalAmount = principalAmount;
	}

	public Double getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(Double interestAmount) {
		this.interestAmount = interestAmount;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getPrincipalApprovedOn() {
		return principalApprovedOn;
	}

	public void setPrincipalApprovedOn(Date principalApprovedOn) {
		this.principalApprovedOn = principalApprovedOn;
	}

	public Date getPrincipalModifiedOn() {
		return principalModifiedOn;
	}

	public void setPrincipalModifiedOn(Date principalModifiedOn) {
		this.principalModifiedOn = principalModifiedOn;
	}

	public Date getInterestApprovedOn() {
		return interestApprovedOn;
	}

	public void setInterestApprovedOn(Date interestApprovedOn) {
		this.interestApprovedOn = interestApprovedOn;
	}

	public AmountType getAmountType() {
		return amountType;
	}

	public void setAmountType(AmountType amountType) {
		this.amountType = amountType;
	}

	public Integer getDifferenceInDays() {
		return differenceInDays;
	}

	public void setDifferenceInDays(Integer differenceInDays) {
		this.differenceInDays = differenceInDays;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public PrincipalStatus getPrincipalStatus() {
		return principalStatus;
	}

	public void setPrincipalStatus(PrincipalStatus principalStatus) {
		this.principalStatus = principalStatus;
	}

	public InterestStatus getInterestStatus() {
		return interestStatus;
	}

	public void setInterestStatus(InterestStatus interestStatus) {
		this.interestStatus = interestStatus;
	}

}
