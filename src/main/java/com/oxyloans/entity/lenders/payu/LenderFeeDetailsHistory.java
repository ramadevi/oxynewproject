package com.oxyloans.entity.lenders.payu;

import javax.persistence.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lender_fee_details_history")
public class LenderFeeDetailsHistory {
	public static enum LenderFeePayments {
		MONTHLY, QUARTERLY, HALFYEARLY, PERYEAR, FIVEYEARS, TENYEARS, LIFETIME, PERDEAL
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "fee_amount", nullable = true)
	private Double feeAmount;

	@Column(name = "fee_amoun_with_gst", nullable = true)
	private Double feeAmountWithGst;

	@Column(name = "is_current", nullable = true)
	private boolean isCurrent = false;

	@Column(name = "lender_fee_payments", nullable = true)
	@Enumerated(EnumType.STRING)
	private LenderFeePayments lenderFeePayments;

	@Column(name = "current_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date currentOn = new Date();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(Double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public Double getFeeAmountWithGst() {
		return feeAmountWithGst;
	}

	public void setFeeAmountWithGst(Double feeAmountWithGst) {
		this.feeAmountWithGst = feeAmountWithGst;
	}

	public boolean isCurrent() {
		return isCurrent;
	}

	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public LenderFeePayments getLenderFeePayments() {
		return lenderFeePayments;
	}

	public void setLenderFeePayments(LenderFeePayments lenderFeePayments) {
		this.lenderFeePayments = lenderFeePayments;
	}

	public Date getCurrentOn() {
		return currentOn;
	}

	public void setCurrentOn(Date currentOn) {
		this.currentOn = currentOn;
	}

}
