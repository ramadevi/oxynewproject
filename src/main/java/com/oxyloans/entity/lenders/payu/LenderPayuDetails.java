package com.oxyloans.entity.lenders.payu;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.enach.EnachTransactions.Status;

@Entity
@Table(name = "lender_payu_details")
public class LenderPayuDetails {

	public static enum PayuStatus {
		COMPLETED, PENDING, TOKEN_INTIATED, NOT_ATTEMPTED, USER_DROPPED,ACTIVE,PAID,EXPIRED,TERMINATED,TERMINATION_REQUESTED
	}

	public static enum LenderFeePayments {
		MONTHLY, QUARTERLY, HALFYEARLY, PERYEAR, FIVEYEARS, TENYEARS, LIFETIME, PERDEAL
	}

	public static enum UserStatus {
		PARTICIPATION, UPDATION, RENEWAL, NOTYETRENEWAL
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "lender_user_id", nullable = true)
	private Integer lenderUserId;

	@Column(name = "lender_name", nullable = true)
	private String lenderName;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId;

	@Column(name = "amount", nullable = false)
	private Double amount = 0.0;

	@Column(name = "transaction_number", nullable = true)
	private String transactionNumber;

	@Column(name = "payment_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date paymentDate = new Date();

	@Column(name = "status", nullable = true)
	@Enumerated(EnumType.STRING)
	private PayuStatus status = PayuStatus.PENDING;

	@Column(name = "lender_fee_payments", nullable = true)
	@Enumerated(EnumType.STRING)
	private LenderFeePayments lenderFeePayments;

	@Column(name = "payu_transaction_id", nullable = true)
	private String payuTransactionNumber;

	@Column(name = "order_id", nullable = true)
	private String OrderId;

	@Column(name = "order_token", nullable = true)
	private String OrderToken;

	@Column(name = "user_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private UserStatus userStatus;

	@Column(name = "paid_from", nullable = true)
	private String paidFrom;

	@Column(name = "participated_table_id", nullable = true)
	private Integer participatedTableId;
	
	@Column(name = "payment_session_id", nullable = true)
	private String paymentSessionId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLenderUserId() {
		return lenderUserId;
	}

	public void setLenderUserId(Integer lenderUserId) {
		this.lenderUserId = lenderUserId;
	}

	public String getLenderName() {
		return lenderName;
	}

	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public PayuStatus getStatus() {
		return status;
	}

	public void setStatus(PayuStatus status) {
		this.status = status;
	}

	public String getPayuTransactionNumber() {
		return payuTransactionNumber;
	}

	public void setPayuTransactionNumber(String payuTransactionNumber) {
		this.payuTransactionNumber = payuTransactionNumber;
	}

	public String getOrderId() {
		return OrderId;
	}

	public void setOrderId(String orderId) {
		OrderId = orderId;
	}

	public String getOrderToken() {
		return OrderToken;
	}

	public void setOrderToken(String orderToken) {
		OrderToken = orderToken;
	}

	public LenderFeePayments getLenderFeePayments() {
		return lenderFeePayments;
	}

	public void setLenderFeePayments(LenderFeePayments lenderFeePayments) {
		this.lenderFeePayments = lenderFeePayments;
	}

	public UserStatus getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	}

	public String getPaidFrom() {
		return paidFrom;
	}

	public void setPaidFrom(String paidFrom) {
		this.paidFrom = paidFrom;
	}

	public Integer getParticipatedTableId() {
		return participatedTableId;
	}

	public void setParticipatedTableId(Integer participatedTableId) {
		this.participatedTableId = participatedTableId;
	}

	public String getPaymentSessionId() {
		return paymentSessionId;
	}

	public void setPaymentSessionId(String paymentSessionId) {
		this.paymentSessionId = paymentSessionId;
	}
	
	

}
