package com.oxyloans.entity.lenders.payu;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lender_renewal_details")
public class LenderRenewalDetails {
	
	public static enum RenewalStatus {
		Renewed,NotYetRenewed,REDUNDANT
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "user_id", nullable = true)
	private Integer user_id = 0;

	@Column(name = "validity_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date validityDate;
	
	@Column(name = "renewal_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private RenewalStatus renewalStatus = RenewalStatus.NotYetRenewed;
	
	@Column(name = "amount", nullable = true)
	private Double amount = 0.0;
	
	@Column(name = "payment_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date paymentDate;
	
	@Column(name = "message_sent", nullable = true)
	private boolean messageSent = true;
	
	@Column(name = "old_validity_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date oldValidityDate;

	@Column(name="payment_received_on",nullable = true)
	private Date paymentReceivedOn;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Date getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(Date validityDate) {
		this.validityDate = validityDate;
	}

	public RenewalStatus getRenewalStatus() {
		return renewalStatus;
	}

	public void setRenewalStatus(RenewalStatus renewalStatus) {
		this.renewalStatus = renewalStatus;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public boolean isMessageSent() {
		return messageSent;
	}

	public void setMessageSent(boolean messageSent) {
		this.messageSent = messageSent;
	}

	public Date getOldValidityDate() {
		return oldValidityDate;
	}

	public void setOldValidityDate(Date oldValidityDate) {
		this.oldValidityDate = oldValidityDate;
	}

	public Date getPaymentReceivedOn() {
		return paymentReceivedOn;
	}

	public void setPaymentReceivedOn(Date paymentReceivedOn) {
		this.paymentReceivedOn = paymentReceivedOn;
	}

	
}
