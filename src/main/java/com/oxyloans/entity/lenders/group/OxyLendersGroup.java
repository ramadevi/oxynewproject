package com.oxyloans.entity.lenders.group;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "oxy_lenders_group")
public class OxyLendersGroup {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "lender_group_name", nullable = true)
	private String lenderGroupName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLenderGroupName() {
		return lenderGroupName;
	}

	public void setLenderGroupName(String lenderGroupName) {
		this.lenderGroupName = lenderGroupName;
	}

}
