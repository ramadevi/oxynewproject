package com.oxyloans.entity.lenders.outstanding;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "oxy_lenders_outstandingamount")
public class OxyLendersOutstandingamount {

	public static enum AmountType {
		CREDIT, DEBIT
	}

	public static enum AmountSubType {

		P, PU, PRA, PRW, MPRW, DW // p(paticipation)pu(paticipationUpdation)pra(principlareturnaccount)prw(principalreturnwallet)mprw(manuvalprincipalreturntowallet)dw(withdraw)

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = true)
	private Integer userId = 0;

	@Column(name = "outstanding_amount", nullable = true)
	private Double outstandingAmount;

	@Column(name = "created_on", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	@Column(name = "is_current", nullable = true)
	private Boolean isCurrent = false;

	@Enumerated(EnumType.STRING)
	@Column(name = "amount_type", nullable = true)
	private AmountType amountType = AmountType.CREDIT;

	@Enumerated(EnumType.STRING)
	@Column(name = "amount_sub_type", nullable = true)
	private AmountSubType amountSubType = AmountSubType.P;

	@Column(name = "amount", nullable = true)
	private Double amount = 0.0;

	@Column(name = "previous_balance", nullable = true)
	private Double previousBalance = 0.0;

	public Integer getId() {
		return id;
	}

	public Integer getUserId() {
		return userId;
	}

	public Double getOutstandingAmount() {
		return outstandingAmount;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setOutstandingAmount(Double outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsCurrent() {
		return isCurrent;
	}

	public void setIsCurrent(Boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public AmountType getAmountType() {
		return amountType;
	}

	public AmountSubType getAmountSubType() {
		return amountSubType;
	}

	public Double getAmount() {
		return amount;
	}

	public Double getPreviousBalance() {
		return previousBalance;
	}

	public void setAmountType(AmountType amountType) {
		this.amountType = amountType;
	}

	public void setAmountSubType(AmountSubType amountSubType) {
		this.amountSubType = amountSubType;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setPreviousBalance(Double previousBalance) {
		this.previousBalance = previousBalance;
	}

}
