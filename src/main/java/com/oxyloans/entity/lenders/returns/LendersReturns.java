package com.oxyloans.entity.lenders.returns;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.user.User;

@Entity
@Table(name = "lenders_returns")
public class LendersReturns {

	public static enum Status {
		INITIATED, REQUESTED, APPROVED, REJECTED, H2HAPPROVAL, CANCELLED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = true)
	private Integer userId;

	@Column(name = "amount", nullable = true)
	private double amount = 0.0;

	@Column(name = "paid_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date paidDate;

	@Column(name = "debit_account", nullable = true)
	private String debitAccount;

	@Column(name = "borrower_name", nullable = true)
	private String borrowerName;

	@Column(name = "remarks", nullable = true)
	private String remarks;

	@Column(name = "amount_type", nullable = true)
	private String amountType;

	@Column(name = "lender_infomation_excel_sheet", nullable = true)
	private String lenderInfomationExcelSheet;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId;

	@Column(name = "status", nullable = true)
	@Enumerated(EnumType.STRING)
	private Status status = Status.INITIATED;

	@Column(name = "requested_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date requestedOn;

	@Column(name = "interestamount_for_withdrawalfunds", nullable = true)
	private double interestamountForWithdrawalfunds = 0.0;

	@Column(name = "h2h_approved_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date h2hApprovedOn;

	@Column(name = "file_name_for_principal_withdrawal", nullable = true)
	private String fileNameForPrincipalWithdrawal;

	@Column(name = "file_name_for_interest", nullable = true)
	private String fileNameForInterest;

	@Column(name = "days_difference", nullable = true)
	private Integer days_difference;

	@Column(name = "actual_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date actualDate;

	@Column(name = "created_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public String getLenderInfomationExcelSheet() {
		return lenderInfomationExcelSheet;
	}

	public void setLenderInfomationExcelSheet(String lenderInfomationExcelSheet) {
		this.lenderInfomationExcelSheet = lenderInfomationExcelSheet;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getRequestedOn() {
		return requestedOn;
	}

	public void setRequestedOn(Date requestedOn) {
		this.requestedOn = requestedOn;
	}

	public double getInterestamountForWithdrawalfunds() {
		return interestamountForWithdrawalfunds;
	}

	public void setInterestamountForWithdrawalfunds(double interestamountForWithdrawalfunds) {
		this.interestamountForWithdrawalfunds = interestamountForWithdrawalfunds;
	}

	public Date getH2hApprovedOn() {
		return h2hApprovedOn;
	}

	public void setH2hApprovedOn(Date h2hApprovedOn) {
		this.h2hApprovedOn = h2hApprovedOn;
	}

	public String getFileNameForPrincipalWithdrawal() {
		return fileNameForPrincipalWithdrawal;
	}

	public void setFileNameForPrincipalWithdrawal(String fileNameForPrincipalWithdrawal) {
		this.fileNameForPrincipalWithdrawal = fileNameForPrincipalWithdrawal;
	}

	public String getFileNameForInterest() {
		return fileNameForInterest;
	}

	public void setFileNameForInterest(String fileNameForInterest) {
		this.fileNameForInterest = fileNameForInterest;
	}

	public String getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	public Integer getDays_difference() {
		return days_difference;
	}

	public void setDays_difference(Integer days_difference) {
		this.days_difference = days_difference;
	}

	public Date getActualDate() {
		return actualDate;
	}

	public void setActualDate(Date actualDate) {
		this.actualDate = actualDate;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
