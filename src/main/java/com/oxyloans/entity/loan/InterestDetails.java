package com.oxyloans.entity.loan;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "interest_details")
public class InterestDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "loan_id", nullable = true)
	private Integer loanId;

	@Column(name = "emi_number", nullable = true)
	private Integer emiNumber;

	@Column(name = "difference_in_days", nullable = true)
	private Integer differenceInDays;

	@Column(name = "interest ", nullable = true)
	private double interest;

	@Column(name = "interest_paid", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date interestPaid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLoanId() {
		return loanId;
	}

	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}

	public Integer getEmiNumber() {
		return emiNumber;
	}

	public void setEmiNumber(Integer emiNumber) {
		this.emiNumber = emiNumber;
	}

	public Integer getDifferenceInDays() {
		return differenceInDays;
	}

	public void setDifferenceInDays(Integer differenceInDays) {
		this.differenceInDays = differenceInDays;
	}

	public double getInterest() {
		return interest;
	}

	public void setInterest(double interest) {
		this.interest = interest;
	}

	public Date getInterestPaid() {
		return interestPaid;
	}

	public void setInterestPaid(Date interestPaid) {
		this.interestPaid = interestPaid;
	}

}
