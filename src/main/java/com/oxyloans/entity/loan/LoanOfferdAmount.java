package com.oxyloans.entity.loan;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.loan.LoanRequest.DurationType;

@Entity
@Table(name = "loan_offerd_amount")
public class LoanOfferdAmount {

	public static enum LoanOfferdStatus {
		INITIATED, LOANOFFERACCEPTED, LOANOFFERREJECTED, LOANOFFEREXPIRED, LOANOFFERCOMPLETED, ADMINREJECTED
	}

	@Id
	@Column(name = "id", unique = true)
	private Integer id;

	@Column(name = "loan_offered_amount", nullable = true)
	private Double loanOfferedAmount;

	@Column(name = "comments", nullable = true)
	private String comments;

	@Column(name = "offer_sent_on", nullable = true, insertable = true, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date offerSentOn = new Date();

	@Column(name = "email_sent", nullable = true)
	private Boolean emailSent;

	@Column(name = "user_id", nullable = true)
	private Integer userId;

	@Column(name = "accepeted_on", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date accepetedOn;

	@Column(name = "loan_request_offered_id", nullable = true)
	private String loanofferedId;

	@Column(name = "loan_offerd_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private LoanOfferdStatus loanOfferdStatus = LoanOfferdStatus.INITIATED;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id", insertable = false, updatable = false)
	private LoanRequest loanRequest;

	@Column(name = "email_token", nullable = false)
	private String emailToken;

	@Column(name = "rate_of_interest", nullable = false)
	private Double rateOfInterest;

	@Column(name = "duration", nullable = false)
	private Integer duration;

	@Column(name = "borrower_fee", nullable = true)
	private Double borrowerFee;

	@Column(name = "emi_amount", nullable = true)
	private Double emiAmount;

	@Column(name = "net_disbursement_amount", nullable = true)
	private Double netDisbursementAmount;

	@Column(name = "payu_transaction_number", nullable = true)
	private String payuTransactionNumber;

	@Column(name = "payu_status", nullable = true)
	private String payuStatus;

	@Column(name = "txn_numaber", nullable = true)
	private String txnNumber;

	@Column(name = "duration_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private DurationType durationType = DurationType.Months;

	public String getTxnNumber() {
		return txnNumber;
	}

	public void setTxnNumber(String txnNumber) {
		this.txnNumber = txnNumber;
	}

	public String getPayuTransactionNumber() {
		return payuTransactionNumber;
	}

	public String getPayuStatus() {
		return payuStatus;
	}

	public void setPayuTransactionNumber(String payuTransactionNumber) {
		this.payuTransactionNumber = payuTransactionNumber;
	}

	public void setPayuStatus(String payuStatus) {
		this.payuStatus = payuStatus;
	}

	public Double getNetDisbursementAmount() {
		return netDisbursementAmount;
	}

	public void setNetDisbursementAmount(Double netDisbursementAmount) {
		this.netDisbursementAmount = netDisbursementAmount;
	}

	public Integer getId() {
		return id;
	}

	public Double getLoanOfferedAmount() {
		return loanOfferedAmount;
	}

	public String getComments() {
		return comments;
	}

	public Date getOfferSentOn() {
		return offerSentOn;
	}

	public Boolean getEmailSent() {
		return emailSent;
	}

	public Integer getUserId() {
		return userId;
	}

	public Date getAccepetedOn() {
		return accepetedOn;
	}

	public String getLoanofferedId() {
		return loanofferedId;
	}

	public LoanOfferdStatus getLoanOfferdStatus() {
		return loanOfferdStatus;
	}

	public LoanRequest getLoanRequest() {
		return loanRequest;
	}

	public String getEmailToken() {
		return emailToken;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public Integer getDuration() {
		return duration;
	}

	public Double getBorrowerFee() {
		return borrowerFee;
	}

	public Double getEmiAmount() {
		return emiAmount;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setLoanOfferedAmount(Double loanOfferedAmount) {
		this.loanOfferedAmount = loanOfferedAmount;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void setOfferSentOn(Date offerSentOn) {
		this.offerSentOn = offerSentOn;
	}

	public void setEmailSent(Boolean emailSent) {
		this.emailSent = emailSent;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setAccepetedOn(Date accepetedOn) {
		this.accepetedOn = accepetedOn;
	}

	public void setLoanofferedId(String loanofferedId) {
		this.loanofferedId = loanofferedId;
	}

	public void setLoanOfferdStatus(LoanOfferdStatus loanOfferdStatus) {
		this.loanOfferdStatus = loanOfferdStatus;
	}

	public void setLoanRequest(LoanRequest loanRequest) {
		this.loanRequest = loanRequest;
	}

	public void setEmailToken(String emailToken) {
		this.emailToken = emailToken;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public void setBorrowerFee(Double borrowerFee) {
		this.borrowerFee = borrowerFee;
	}

	public void setEmiAmount(Double emiAmount) {
		this.emiAmount = emiAmount;
	}

	public DurationType getDurationType() {
		return durationType;
	}

	public void setDurationType(DurationType durationType) {
		this.durationType = durationType;
	}

}
