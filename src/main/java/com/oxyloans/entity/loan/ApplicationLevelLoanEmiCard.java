package com.oxyloans.entity.loan;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.IEntity;

@Entity
@Table(name = "applicationlevel_loan_emicard")
public class ApplicationLevelLoanEmiCard implements IEntity{

	public static enum Status {
		Paid, Unpaid
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "application_id", nullable = true)
	private Integer applicationId;

	@Column(name = "borrower_id", nullable = true)
	private Integer borrowerId;

	@Column(name = "emi_number", nullable = true)
	private Integer emiNumber;

	@Column(name = "principal_amount", nullable = true)
	private double principalAmount = 0.0;

	@Column(name = "interst_amount", nullable = true)
	private double interstAmount = 0.0;

	@Column(name = "emi_amount", nullable = true)
	private double emiAmount = 0.0;

	@Column(name = "status", nullable = true)
	@Enumerated(EnumType.STRING)
	private Status status = Status.Unpaid;
	
	@Column(name = "emi_due_on", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date emiDueOn;
	
	@Column(name = "emi_paid_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date emiPaidOn;
	
	@Column(name="comments",nullable = true)
	private String comments;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public double getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(double emiAmount) {
		this.emiAmount = emiAmount;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Integer getEmiNumber() {
		return emiNumber;
	}

	public void setEmiNumber(Integer emiNumber) {
		this.emiNumber = emiNumber;
	}

	public double getPrincipalAmount() {
		return principalAmount;
	}

	public void setPrincipalAmount(double principalAmount) {
		this.principalAmount = principalAmount;
	}

	public double getInterstAmount() {
		return interstAmount;
	}

	public void setInterstAmount(double interstAmount) {
		this.interstAmount = interstAmount;
	}

	public Date getEmiDueOn() {
		return emiDueOn;
	}

	public void setEmiDueOn(Date emiDueOn) {
		this.emiDueOn = emiDueOn;
	}

	public Date getEmiPaidOn() {
		return emiPaidOn;
	}

	public void setEmiPaidOn(Date emiPaidOn) {
		this.emiPaidOn = emiPaidOn;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}