package com.oxyloans.entity.loan;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lender_rating_to_borrower_application")
public class LenderRatingToBorrowerApplication {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "parent_request_id", nullable = true)
	private Integer parentRequestId;

	@Column(name = "lender_id", nullable = true)
	private Integer lenderId;

	@Column(name = "rating", nullable = true)
	private Integer rating;

	@Column(name = "updated_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedOn = new Date();

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	@JoinColumn(name = "id", insertable = false, updatable = false) // oxy_loan_request table id mapped to
																	// parent_request_id
	private LoanRequest loanRequest;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentRequestId() {
		return parentRequestId;
	}

	public void setParentRequestId(Integer parentRequestId) {
		this.parentRequestId = parentRequestId;
	}

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

}
