package com.oxyloans.entity.loan;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.IEntity;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.User.PrimaryType;

@Entity
@Table(name = "oxy_loan_request")
public class LoanRequest implements IEntity {

	public static enum LoanStatus {
		Requested, Conversation, Rejected, Accepted, Agreed, Active, Closed, ADMINREJECTED, CLOSEDBYPLATFORM, Hold,
		Edit, PartnerApproved, PartnerReject, PartnerShortList, PartnerEdited
	}

	public static enum DurationType {
		Months, Days
	}

	public static enum RepaymentMethod {

		PI("Principal Interest Monthly Flat EMI"), I("Interest Monthly Flat EMI");

		private String displayValue;

		RepaymentMethod(String displayValue) {
			this.displayValue = displayValue;
		}

		public String getDisplayValue() {
			return displayValue;
		}

		public void setDisplayValue(String displayValue) {
			this.displayValue = displayValue;
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = false)
	private Integer userId;

	@Column(name = "user_primary_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private PrimaryType userPrimaryType;

	@Column(name = "loan_request_amount", nullable = false)
	private double loanRequestAmount = 0.0;

	@Column(name = "disbursment_amount", nullable = false)
	private double disbursmentAmount = 0.0;

	@Column(name = "loan_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private LoanStatus loanStatus = LoanStatus.Requested;

	@Column(name = "loan_requested_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date loanRequestedDate = new Date();

	@Column(name = "loan_interest_paid", nullable = false)
	private double loanInterestPaid = 0.0;

	@Column(name = "principal_repaid", nullable = false)
	private double principalRepaid = 0.0;

	@Column(name = "no_of_failed_emis", nullable = false)
	private int noOfFailedEmis = 0;

	@Column(name = "rate_of_interest", nullable = false)
	private Double rateOfInterest;

	@Column(name = "expected_date", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date expectedDate;

	@Column(name = "loan_request_id", nullable = false)
	private String loanRequestId;

	@Column(name = "Duration", nullable = false)
	private Integer duration;

	@Column(name = "repayment_method", nullable = false)
	@Enumerated(EnumType.STRING)
	private RepaymentMethod repaymentMethod;

	@Column(name = "loan_purpose ", nullable = false)
	private String loanPurpose;

	@Column(name = "parent_request_id", nullable = false)
	private Integer parentRequestId;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	@JoinColumn(name = "parent_request_id", insertable = false, updatable = false)
	private LoanRequest parentRequest;

	@Column(name = "loan_id", nullable = true)
	private String loanId;

	@Column(name = "comments", nullable = true)
	private String comments;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	@JoinColumn(name = "user_id", insertable = false, updatable = false)
	private User user;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "loanRequest")
	private LoanOfferdAmount loanOfferedAmount;

	@Column(name = "admin_comments", nullable = false)
	private String adminComments;

	@Column(name = "loan_amount_approved_by_admin", nullable = true)
	private Double loanAmountApprovedByAdmin;

	@Column(name = "payment_fee_rateofinterest", nullable = true)
	private Integer paymentFeeRateOfInterest = 4;

	@Column(name = "is_ecs_activated", nullable = true)
	private Boolean isECSActivated;

	@Column(name = "txn_numaber", nullable = true)
	private String txnNumber;

	@Column(name = "enachtype", nullable = true)
	private String enachType;

	@Column(name = "loanprocesstype", nullable = true)
	private String loanProcessType;

	@Column(name = "comments_by_sir", nullable = true)
	private String commentsBySir;

	@Column(name = "rating_by_sir", nullable = true)
	private Integer ratingBySir;

	@Column(name = "rate_of_interest_to_borrower", nullable = true)
	private double rateOfInterestToBorrower = 0.0;

	@Column(name = "rate_of_interest_to_lender", nullable = true)
	private double rateOfInterestToLender = 0.0;

	@Column(name = "duration_by_sir", nullable = true)
	private Integer durationBySir = 0;

	@Column(name = "repayment_method_for_borrower", nullable = true)
	private String repaymentMethodForBorrower;

	@Column(name = "repayment_method_for_lender", nullable = true)
	private String repaymentMethodForLender;

	@Column(name = "duration_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private DurationType durationType = DurationType.Months;

	@Column(name = "application_id", nullable = true)
	private String application_id;

	public String getLoanProcessType() {
		return loanProcessType;
	}

	public void setLoanProcessType(String loanProcessType) {
		this.loanProcessType = loanProcessType;
	}

	public String getEnachType() {
		return enachType;
	}

	public void setEnachType(String enachType) {
		this.enachType = enachType;
	}

	public String getTxnNumber() {
		return txnNumber;
	}

	public void setTxnNumber(String txnNumber) {
		this.txnNumber = txnNumber;
	}

	public Boolean getIsECSActivated() {
		return isECSActivated;
	}

	public void setIsECSActivated(Boolean isECSActivated) {
		this.isECSActivated = isECSActivated;
	}

	public LoanOfferdAmount getLoanOfferedAmount() {
		return loanOfferedAmount;
	}

	public void setLoanOfferedAmount(LoanOfferdAmount loanOfferedAmount) {
		this.loanOfferedAmount = loanOfferedAmount;
	}

	public Integer getPaymentFeeRateOfInterest() {
		return paymentFeeRateOfInterest;
	}

	public void setPaymentFeeRateOfInterest(Integer paymentFeeRateOfInterest) {
		this.paymentFeeRateOfInterest = paymentFeeRateOfInterest;
	}

	public Double getLoanAmountApprovedByAdmin() {
		return loanAmountApprovedByAdmin;
	}

	public void setLoanAmountApprovedByAdmin(Double loanAmountApprovedByAdmin) {
		this.loanAmountApprovedByAdmin = loanAmountApprovedByAdmin;
	}

	public String getAdminComments() {
		return adminComments;
	}

	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public PrimaryType getUserPrimaryType() {
		return userPrimaryType;
	}

	public void setUserPrimaryType(PrimaryType userPrimaryType) {
		this.userPrimaryType = userPrimaryType;
	}

	public Double getLoanRequestAmount() {
		return loanRequestAmount;
	}

	public void setLoanRequestAmount(Double loanRequestAmount) {
		this.loanRequestAmount = loanRequestAmount;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public Date getExpectedDate() {
		return expectedDate;
	}

	public void setExpectedDate(Date expectedDate) {
		this.expectedDate = expectedDate;
	}

	public Double getDisbursmentAmount() {
		return disbursmentAmount;
	}

	public void setDisbursmentAmount(Double disbursmentAmount) {
		this.disbursmentAmount = disbursmentAmount;
	}

	public LoanStatus getLoanStatus() {
		return loanStatus;
	}

	public void setLoanStatus(LoanStatus loanStatus) {
		this.loanStatus = loanStatus;
	}

	public Date getLoanRequestedDate() {
		return loanRequestedDate;
	}

	public void setLoanRequestedDate(Date loanRequestedDate) {
		this.loanRequestedDate = loanRequestedDate;
	}

	public Double getLoanInterestPaid() {
		return loanInterestPaid;
	}

	public void setLoanInterestPaid(Double loanInterestPaid) {
		this.loanInterestPaid = loanInterestPaid;
	}

	public Double getPrincipalRepaid() {
		return principalRepaid;
	}

	public void setPrincipalRepaid(Double principalRepaid) {
		this.principalRepaid = principalRepaid;
	}

	public Integer getNoOfFailedEmis() {
		return noOfFailedEmis;
	}

	public void setNoOfFailedEmis(Integer noOfFailedEmis) {
		this.noOfFailedEmis = noOfFailedEmis;
	}

	public String getLoanRequestId() {
		return loanRequestId;
	}

	public void setLoanRequestId(String loanRequestId) {
		this.loanRequestId = loanRequestId;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public RepaymentMethod getRepaymentMethod() {
		return repaymentMethod;
	}

	public void setRepaymentMethod(RepaymentMethod repaymentMethod) {
		this.repaymentMethod = repaymentMethod;
	}

	public String getLoanPurpose() {
		return loanPurpose;
	}

	public void setLoanPurpose(String loanPurpose) {
		this.loanPurpose = loanPurpose;
	}

	public Integer getParentRequestId() {
		return parentRequestId;
	}

	public void setParentRequestId(Integer parentRequestId) {
		this.parentRequestId = parentRequestId;
	}

	public LoanRequest getParentRequest() {
		return parentRequest;
	}

	public void setParentRequest(LoanRequest parentRequest) {
		this.parentRequest = parentRequest;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getCommentsBySir() {
		return commentsBySir;
	}

	public void setCommentsBySir(String commentsBySir) {
		this.commentsBySir = commentsBySir;
	}

	public Integer getRatingBySir() {
		return ratingBySir;
	}

	public void setRatingBySir(Integer ratingBySir) {
		this.ratingBySir = ratingBySir;
	}

	public double getRateOfInterestToBorrower() {
		return rateOfInterestToBorrower;
	}

	public void setRateOfInterestToBorrower(double rateOfInterestToBorrower) {
		this.rateOfInterestToBorrower = rateOfInterestToBorrower;
	}

	public double getRateOfInterestToLender() {
		return rateOfInterestToLender;
	}

	public void setRateOfInterestToLender(double rateOfInterestToLender) {
		this.rateOfInterestToLender = rateOfInterestToLender;
	}

	public Integer getDurationBySir() {
		return durationBySir;
	}

	public void setDurationBySir(Integer durationBySir) {
		this.durationBySir = durationBySir;
	}

	public String getRepaymentMethodForBorrower() {
		return repaymentMethodForBorrower;
	}

	public void setRepaymentMethodForBorrower(String repaymentMethodForBorrower) {
		this.repaymentMethodForBorrower = repaymentMethodForBorrower;
	}

	public String getRepaymentMethodForLender() {
		return repaymentMethodForLender;
	}

	public void setRepaymentMethodForLender(String repaymentMethodForLender) {
		this.repaymentMethodForLender = repaymentMethodForLender;
	}

	public DurationType getDurationType() {
		return durationType;
	}

	public void setDurationType(DurationType durationType) {
		this.durationType = durationType;
	}

	public String getApplication_id() {
		return application_id;
	}

	public void setApplication_id(String application_id) {
		this.application_id = application_id;
	}

	
}