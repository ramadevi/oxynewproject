package com.oxyloans.entity.loan;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.user.User;

@Entity
@Table(name = "paytm_transaction_details")
public class PaytmTransactionDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "van_number", nullable = true)
	private String vanNumber;

	@Column(name = "status", nullable = true)
	private String status;

	@Column(name = "amount", nullable = true)
	private String amount;

	@Column(name = "beneficiary_accountnumber", nullable = true)
	private String beneficiaryAccountnumber;

	@Column(name = "beneficiary_ifsc", nullable = true)
	private String beneficiaryIfsc;

	@Column(name = "remitter_accountnumber", nullable = true)
	private String remitterAccountnumber;

	@Column(name = "remitter_ifsc", nullable = true)
	private String remitterIfsc;

	@Column(name = "remitter_name", nullable = true)
	private String remitterName;

	@Column(name = "bank_txn_identifier", nullable = true)
	private String bankTxnIdentifier;

	@Column(name = "transactionRequestId", nullable = true)
	private String transactionRequestId;

	@Column(name = "transfer_mode", nullable = true)
	private String transferMode;

	@Column(name = "responseCode", nullable = true)
	private String responseCode;

	@Column(name = "transaction_date", nullable = true)
	private String transactionDate;

	@Column(name = "updated_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedOn = new Date();

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	@JoinColumn(name = "van_number", insertable = false, updatable = false)
	private User user;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVanNumber() {
		return vanNumber;
	}

	public void setVanNumber(String vanNumber) {
		this.vanNumber = vanNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBeneficiaryAccountnumber() {
		return beneficiaryAccountnumber;
	}

	public void setBeneficiaryAccountnumber(String beneficiaryAccountnumber) {
		this.beneficiaryAccountnumber = beneficiaryAccountnumber;
	}

	public String getBeneficiaryIfsc() {
		return beneficiaryIfsc;
	}

	public void setBeneficiaryIfsc(String beneficiaryIfsc) {
		this.beneficiaryIfsc = beneficiaryIfsc;
	}

	public String getRemitterAccountnumber() {
		return remitterAccountnumber;
	}

	public void setRemitterAccountnumber(String remitterAccountnumber) {
		this.remitterAccountnumber = remitterAccountnumber;
	}

	public String getRemitterIfsc() {
		return remitterIfsc;
	}

	public void setRemitterIfsc(String remitterIfsc) {
		this.remitterIfsc = remitterIfsc;
	}

	public String getRemitterName() {
		return remitterName;
	}

	public void setRemitterName(String remitterName) {
		this.remitterName = remitterName;
	}

	public String getBankTxnIdentifier() {
		return bankTxnIdentifier;
	}

	public void setBankTxnIdentifier(String bankTxnIdentifier) {
		this.bankTxnIdentifier = bankTxnIdentifier;
	}

	public String getTransactionRequestId() {
		return transactionRequestId;
	}

	public void setTransactionRequestId(String transactionRequestId) {
		this.transactionRequestId = transactionRequestId;
	}

	public String getTransferMode() {
		return transferMode;
	}

	public void setTransferMode(String transferMode) {
		this.transferMode = transferMode;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

}
