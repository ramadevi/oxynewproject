package com.oxyloans.entity.loan;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "penalty_details")
public class PenaltyDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "borrower_parent_requestid", nullable = true)
	private Integer borrowerParentRequestid;

	@Column(name = "emi_number", nullable = true)
	private Integer emiNumber;

	@Column(name = "penalty", nullable = true)
	private double penalty;

	@Column(name = "penalty_paid", nullable = true)
	private double penaltyPaid;

	@Column(name = "penalty_paid_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date penaltyPaidOn;

	@Column(name = "penalty_due", nullable = true)
	private double penaltyDue;

	@Column(name = "penalty_waived_off", nullable = true)
	private double penaltyWaivedOff = 0.0;

	@Column(name = "penalty_waived_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date penaltyWaivedDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBorrowerParentRequestid() {
		return borrowerParentRequestid;
	}

	public void setBorrowerParentRequestid(Integer borrowerParentRequestid) {
		this.borrowerParentRequestid = borrowerParentRequestid;
	}

	public Integer getEmiNumber() {
		return emiNumber;
	}

	public void setEmiNumber(Integer emiNumber) {
		this.emiNumber = emiNumber;
	}

	public double getPenalty() {
		return penalty;
	}

	public void setPenalty(double penalty) {
		this.penalty = penalty;
	}

	public double getPenaltyPaid() {
		return penaltyPaid;
	}

	public void setPenaltyPaid(double penaltyPaid) {
		this.penaltyPaid = penaltyPaid;
	}

	public Date getPenaltyPaidOn() {
		return penaltyPaidOn;
	}

	public void setPenaltyPaidOn(Date penaltyPaidOn) {
		this.penaltyPaidOn = penaltyPaidOn;
	}

	public double getPenaltyDue() {
		return penaltyDue;
	}

	public void setPenaltyDue(double penaltyDue) {
		this.penaltyDue = penaltyDue;
	}

	public double getPenaltyWaivedOff() {
		return penaltyWaivedOff;
	}

	public void setPenaltyWaivedOff(double penaltyWaivedOff) {
		this.penaltyWaivedOff = penaltyWaivedOff;
	}

	public Date getPenaltyWaivedDate() {
		return penaltyWaivedDate;
	}

	public void setPenaltyWaivedDate(Date penaltyWaivedDate) {
		this.penaltyWaivedDate = penaltyWaivedDate;
	}

}
