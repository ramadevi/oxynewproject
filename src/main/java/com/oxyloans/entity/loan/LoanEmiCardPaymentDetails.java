package com.oxyloans.entity.loan;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "loan_emi_card_payment_details")
public class LoanEmiCardPaymentDetails {
	public static enum PaymentStatus {
		NOTYETPAID, FULLYPAID, PARTPAID

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "emi_id")
	private Integer emiId;

	@Column(name = "loan_id", nullable = true)
	private String loanId;

	@Column(name = "mode_of_payment", nullable = true)
	private String modeOfPayment;

	@Column(name = "transaction_reference_number", nullable = true)
	private String transactionRefereceNumber;

	@Column(name = "payment_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private PaymentStatus paymentStatus = PaymentStatus.NOTYETPAID;

	@Column(name = "partial_payment_amount", nullable = true)
	private double partialPaymentAmount;

	@Column(name = "penalty", nullable = true)
	private double penalty = 0.0;

	@Column(name = "remaining_emi_amount", nullable = true)
	private double remainingEmiAmount;

	@Column(name = "partpay_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date partpayDate;

	@Column(name = "amount_paid_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date amountPaidDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEmiId() {
		return emiId;
	}

	public void setEmiId(Integer emiId) {
		this.emiId = emiId;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getTransactionRefereceNumber() {
		return transactionRefereceNumber;
	}

	public void setTransactionRefereceNumber(String transactionRefereceNumber) {
		this.transactionRefereceNumber = transactionRefereceNumber;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public double getPartialPaymentAmount() {
		return partialPaymentAmount;
	}

	public void setPartialPaymentAmount(double partialPaymentAmount) {
		this.partialPaymentAmount = partialPaymentAmount;
	}

	public double getPenalty() {
		return penalty;
	}

	public void setPenalty(double penalty) {
		this.penalty = penalty;
	}

	public double getRemainingEmiAmount() {
		return remainingEmiAmount;
	}

	public void setRemainingEmiAmount(double remainingEmiAmount) {
		this.remainingEmiAmount = remainingEmiAmount;
	}

	public Date getPartpayDate() {
		return partpayDate;
	}

	public void setPartpayDate(Date partpayDate) {
		this.partpayDate = partpayDate;
	}

	public Date getAmountPaidDate() {
		return amountPaidDate;
	}

	public void setAmountPaidDate(Date amountPaidDate) {
		this.amountPaidDate = amountPaidDate;
	}

}
