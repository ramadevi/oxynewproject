package com.oxyloans.entity.loan;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lender_borrower_conversation")
public class LenderBorrowerConversation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "lender_user_id")
	private Integer lenderUserId;
	
	@Column(name = "borrower_user_id")
	private Integer borrowerUserId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLenderUserId() {
		return lenderUserId;
	}

	public void setLenderUserId(Integer lenderUserId) {
		this.lenderUserId = lenderUserId;
	}

	public Integer getBorrowerUserId() {
		return borrowerUserId;
	}

	public void setBorrowerUserId(Integer borrowerUserId) {
		this.borrowerUserId = borrowerUserId;
	}
}
