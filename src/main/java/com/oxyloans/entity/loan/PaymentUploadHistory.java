package com.oxyloans.entity.loan;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "payment_upload_history")
public class PaymentUploadHistory {

	public static enum PaymentType {
		FULLPAYMENT, PARTPAYMENT
	}

	public static enum Status {
		UPLOADED, APPROVED, NOTYETREFLECTED, REJECTED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "borrower_unique_number", nullable = true)
	private String borrowerUniqueNumber;

	@Column(name = "borrower_name", nullable = true)
	private String borrowerName;

	@Column(name = "document_uploaded_id", nullable = true)
	private Integer documentUploadedId;

	@Column(name = "amount", nullable = true)
	private double amount;

	@Column(name = "updated_name", nullable = true)
	private String updatedName;

	@Column(name = "paid_date", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date paidDate;

	@Column(name = "updated_on", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedOn = new Date();

	@Column(name = "payment_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private PaymentType paymentType = PaymentType.FULLPAYMENT;

	@Column(name = "user_id", nullable = true)
	private Integer userId;

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status = Status.UPLOADED;

	@Column(name = "approved_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date approvedOn;

	@Column(name = "comments", nullable = true)
	private String comments = "";
	
	@Column(name = "whats_app_message_no", nullable = true)
	private String whatsAppMessageNo;


	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBorrowerUniqueNumber() {
		return borrowerUniqueNumber;
	}

	public void setBorrowerUniqueNumber(String borrowerUniqueNumber) {
		this.borrowerUniqueNumber = borrowerUniqueNumber;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public Integer getDocumentUploadedId() {
		return documentUploadedId;
	}

	public void setDocumentUploadedId(Integer documentUploadedId) {
		this.documentUploadedId = documentUploadedId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getUpdatedName() {
		return updatedName;
	}

	public void setUpdatedName(String updatedName) {
		this.updatedName = updatedName;
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getApprovedOn() {
		return approvedOn;
	}

	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getWhatsAppMessageNo() {
		return whatsAppMessageNo;
	}

	public void setWhatsAppMessageNo(String whatsAppMessageNo) {
		this.whatsAppMessageNo = whatsAppMessageNo;
	}
	

}
