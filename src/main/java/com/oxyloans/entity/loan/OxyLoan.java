package com.oxyloans.entity.loan;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.IEntity;
import com.oxyloans.entity.enach.EnachMandate;
import com.oxyloans.entity.loan.LoanRequest.DurationType;
import com.oxyloans.entity.loan.LoanRequest.LoanStatus;
import com.oxyloans.entity.loan.LoanRequest.RepaymentMethod;
import com.oxyloans.entity.user.User;

@Entity
@Table(name = "oxy_loan")
public class OxyLoan implements IEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "oxy_loan_request_id", nullable = false)
	private Integer loanRequestId;

	@Column(name = "oxy_loan_respond_id", nullable = false)
	private Integer loanRespondId;

	// This is actual Loan Id
	@Column(name = "loan_id", unique = true)
	private String loanId;

	@Column(name = "lender_user_id", nullable = true)
	private Integer lenderUserId;

	@Column(name = "borrower_user_id", nullable = true)
	private Integer borrowerUserId;

	@Column(name = "disbursment_amount", nullable = false)
	private double disbursmentAmount = 0.0;

	@Column(name = "loan_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private LoanStatus loanStatus = LoanStatus.Agreed;

	@Column(name = "lender_transaction_fee", nullable = false)
	private double lenderTransactionFee = 0.0;

	@Column(name = "borrower_transaction_fee", nullable = false)
	private double borrowerTransactionFee = 0.0;

	@Column(name = "loan_accepted_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date loanAcceptedDate;

	@Column(name = "lender_disbursed_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date lenderDisbursedDate;

	@Column(name = "borrower_disbursed_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date borrowerDisbursedDate;

	@Column(name = "loan_active_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date loanActiveDate;

	@Column(name = "loan_interest_paid", nullable = false)
	private double loanInterestPaid = 0.0;

	@Column(name = "principal_repaid", nullable = false)
	private double principalRepaid = 0.0;

	@Column(name = "no_of_failed_emis", nullable = false)
	private int noOfFailedEmis = 0;

	@Column(name = "lender_esigned", nullable = false)
	private boolean lenderEsigned = false;

	@Column(name = "borrower_esigned", nullable = false)
	private boolean borrowerEsigned = false;

	@Column(name = "duration", nullable = false)
	private Integer duration;

	@Column(name = "rate_of_interest", nullable = false)
	private Double rateOfInterest;

	@Column(name = "repayment_method", nullable = false)
	@Enumerated(EnumType.STRING)
	private RepaymentMethod repaymentMethod;

	@Column(name = "lender_fee_paid", nullable = false)
	private Boolean lenderFeePaid = false;

	@Column(name = "borrower_fee_paid", nullable = false)
	private Boolean borrowerFeePaid = false;

	@OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	@JoinColumn(name = "loan_id")
	private List<LoanEmiCard> loanEmiCards;

	@Column(name = "lender_esign_id", nullable = true)
	private Integer lenderEsignId;

	@Column(name = "borrower_esign_id", nullable = true)
	private Integer borrowerEsignId;

	@Column(name = "admin_comments", nullable = false)
	private String adminComments;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	@JoinColumn(name = "borrower_user_id", insertable = false, updatable = false)
	private User user;

	@Column(name = "is_ecs_activated", nullable = true)
	private Boolean isECSActivated;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "oxyLoan")
	private List<EnachMandate> enachMandate;

	@Column(name = "borrower_parent_requestid", nullable = true)
	private Integer borrowerParentRequestId;

	@Column(name = "lender_parent_requestid", nullable = true)
	private Integer lenderParentRequestId;

	@Column(name = "fee_percentage", nullable = true)
	private Integer feePercentage;

	@Column(name = "new_duration", nullable = true)
	private Integer newDuration;

	@Column(name = "duration_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private DurationType durationType = DurationType.Months;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId;

	@Column(name = "lender_return_type_for_deal", nullable = true)
	private String lenderReturnTypeForDeal;

	@Column(name = "deal_type", nullable = true)
	private String dealType;

	@Column(name = "application_id", nullable = true)
	private String application_id;

	public Integer getNewDuration() {
		return newDuration;
	}

	public void setNewDuration(Integer newDuration) {
		this.newDuration = newDuration;
	}

	public Integer getBorrowerParentRequestId() {
		return borrowerParentRequestId;
	}

	public Integer getLenderParentRequestId() {
		return lenderParentRequestId;
	}

	public void setBorrowerParentRequestId(Integer borrowerParentRequestId) {
		this.borrowerParentRequestId = borrowerParentRequestId;
	}

	public void setLenderParentRequestId(Integer lenderParentRequestId) {
		this.lenderParentRequestId = lenderParentRequestId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getIsECSActivated() {
		return isECSActivated;
	}

	public void setIsECSActivated(Boolean isECSActivated) {
		this.isECSActivated = isECSActivated;
	}

	public List<EnachMandate> getEnachMandate() {
		return enachMandate;
	}

	public void setEnachMandate(List<EnachMandate> enachMandate) {
		this.enachMandate = enachMandate;
	}

	public String getAdminComments() {
		return adminComments;
	}

	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}

	public Integer getId() {
		return id;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public Integer getLenderUserId() {
		return lenderUserId;
	}

	public void setLenderUserId(Integer lenderUserId) {
		this.lenderUserId = lenderUserId;
	}

	public Integer getBorrowerUserId() {
		return borrowerUserId;
	}

	public void setBorrowerUserId(Integer borrowerUserId) {
		this.borrowerUserId = borrowerUserId;
	}

	public double getDisbursmentAmount() {
		return disbursmentAmount;
	}

	public void setDisbursmentAmount(double disbursmentAmount) {
		this.disbursmentAmount = disbursmentAmount;
	}

	public LoanStatus getLoanStatus() {
		return loanStatus;
	}

	public void setLoanStatus(LoanStatus loanStatus) {
		this.loanStatus = loanStatus;
	}

	public double getLenderTransactionFee() {
		return lenderTransactionFee;
	}

	public void setLenderTransactionFee(double lenderTransactionFee) {
		this.lenderTransactionFee = lenderTransactionFee;
	}

	public double getBorrowerTransactionFee() {
		return borrowerTransactionFee;
	}

	public void setBorrowerTransactionFee(double borrowerTransactionFee) {
		this.borrowerTransactionFee = borrowerTransactionFee;
	}

	public Date getLoanAcceptedDate() {
		return loanAcceptedDate;
	}

	public void setLoanAcceptedDate(Date loanAcceptedDate) {
		this.loanAcceptedDate = loanAcceptedDate;
	}

	public Date getLenderDisbursedDate() {
		return lenderDisbursedDate;
	}

	public void setLenderDisbursedDate(Date lenderDisbursedDate) {
		this.lenderDisbursedDate = lenderDisbursedDate;
	}

	public Date getBorrowerDisbursedDate() {
		return borrowerDisbursedDate;
	}

	public void setBorrowerDisbursedDate(Date borrowerDisbursedDate) {
		this.borrowerDisbursedDate = borrowerDisbursedDate;
	}

	public Date getLoanActiveDate() {
		return loanActiveDate;
	}

	public void setLoanActiveDate(Date loanActiveDate) {
		this.loanActiveDate = loanActiveDate;
	}

	public double getLoanInterestPaid() {
		return loanInterestPaid;
	}

	public void setLoanInterestPaid(double loanInterestPaid) {
		this.loanInterestPaid = loanInterestPaid;
	}

	public double getPrincipalRepaid() {
		return principalRepaid;
	}

	public void setPrincipalRepaid(double principalRepaid) {
		this.principalRepaid = principalRepaid;
	}

	public int getNoOfFailedEmis() {
		return noOfFailedEmis;
	}

	public void setNoOfFailedEmis(int noOfFailedEmis) {
		this.noOfFailedEmis = noOfFailedEmis;
	}

	public Integer getLoanRequestId() {
		return loanRequestId;
	}

	public void setLoanRequestId(Integer loanRequestId) {
		this.loanRequestId = loanRequestId;
	}

	public Integer getLoanRespondId() {
		return loanRespondId;
	}

	public void setLoanRespondId(Integer loanRespondId) {
		this.loanRespondId = loanRespondId;
	}

	public boolean isLenderEsigned() {
		return lenderEsigned;
	}

	public void setLenderEsigned(boolean lenderEsigned) {
		this.lenderEsigned = lenderEsigned;
	}

	public boolean isBorrowerEsigned() {
		return borrowerEsigned;
	}

	public void setBorrowerEsigned(boolean borrowerEsigned) {
		this.borrowerEsigned = borrowerEsigned;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(Double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public RepaymentMethod getRepaymentMethod() {
		return repaymentMethod;
	}

	public void setRepaymentMethod(RepaymentMethod repaymentMethod) {
		this.repaymentMethod = repaymentMethod;
	}

	public Boolean isLenderFeePaid() {
		return lenderFeePaid;
	}

	public void setLenderFeePaid(Boolean lenderFeePaid) {
		this.lenderFeePaid = lenderFeePaid;
	}

	public Boolean isBorrowerFeePaid() {
		return borrowerFeePaid;
	}

	public void setBorrowerFeePaid(Boolean borrowerFeePaid) {
		this.borrowerFeePaid = borrowerFeePaid;
	}

	public List<LoanEmiCard> getLoanEmiCards() {
		return loanEmiCards;
	}

	public void setLoanEmiCards(List<LoanEmiCard> loanEmiCards) {
		this.loanEmiCards = loanEmiCards;
	}

	public Integer getLenderEsignId() {
		return lenderEsignId;
	}

	public void setLenderEsignId(Integer lenderEsignId) {
		this.lenderEsignId = lenderEsignId;
	}

	public Integer getBorrowerEsignId() {
		return borrowerEsignId;
	}

	public void setBorrowerEsignId(Integer borrowerEsignId) {
		this.borrowerEsignId = borrowerEsignId;
	}

	public Integer getFeePercentage() {
		return feePercentage;
	}

	public void setFeePercentage(Integer feePercentage) {
		this.feePercentage = feePercentage;
	}

	public DurationType getDurationType() {
		return durationType;
	}

	public void setDurationType(DurationType durationType) {
		this.durationType = durationType;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getLenderReturnTypeForDeal() {
		return lenderReturnTypeForDeal;
	}

	public void setLenderReturnTypeForDeal(String lenderReturnTypeForDeal) {
		this.lenderReturnTypeForDeal = lenderReturnTypeForDeal;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getApplication_id() {
		return application_id;
	}

	public void setApplication_id(String application_id) {
		this.application_id = application_id;
	}

}
