package com.oxyloans.entity.loan;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.IEntity;

@Entity
@Table(name = "loan_emi_card")
public class LoanEmiCard implements IEntity {
	public static enum Status {
		INITIATED, INPROCESS, COMPLETED, ADMINREJECTED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "loan_id", unique = false)
	private Integer loanId;

	@Column(name = "emi_number", nullable = false)
	private Integer emiNumber;

	@Column(name = "emi_principal_amount", nullable = false)
	private double emiPrincipalAmount = 0.0;

	@Column(name = "emi_interst_amount", nullable = false)
	private double emiInterstAmount = 0.0;

	@Column(name = "emi_amount", nullable = false)
	private double emiAmount = 0.0;

	@Column(name = "emi_due_on", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date emiDueOn;

	@Column(name = "emi_paid_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date emiPaidOn;

	@Column(name = "emiLateFeeCharges", nullable = false)
	private double emiLateFeeCharges = 0.0;

	@Column(name = "comments", nullable = true)
	private String comments;

	@Column(name = "paid_on_time", nullable = false)
	private boolean paidOnTime = true;

	@Column(name = "mode_of_payment", nullable = true)
	private String modeOfPayment;

	@Column(name = "transaction_number", nullable = true)
	private String transactionNumber;

	@Column(name = "is_emi_processed", nullable = true)
	private Boolean isEmiProcessed;

	@Column(name = "payu_transaction_number", nullable = true)
	private String payuTransactionNumber;

	@Column(name = "payu_status", nullable = true)
	private String payuStatus;

	@Column(name = "transactionschedulingresponsejson", nullable = true)
	private String transactionSchedulingResponseJson;

	@Column(name = "transactionverificationresponsejson", nullable = true)
	private String transactionVerificationResponseJson;

	@Column(name = "enachstatus", nullable = true)
	private String enachStatus;

	@Column(name = "statuscode", nullable = true)
	private String statusCode;

	@Column(name = "statusmessagedescription", nullable = true)
	private String statusMessageDescription;

	@Column(name = "clnt_txn_ref", nullable = true)
	private String clntTxnRef;

	@Column(name = "enachscheduleddate", nullable = true)
	private String enachScheduledDate;

	@Column(name = "statusvericationmessagedescription", nullable = true)
	private String statusVericationMessageDescription;

	@Column(name = "remaining_emi_amount", nullable = true)
	private double remainingEmiAmount = 0.0;

	@Column(name = "status", nullable = true)
	@Enumerated(EnumType.STRING)
	private Status status = Status.INITIATED;

	@Column(name = "remaining_penalty_and_remaining_emi", nullable = true)
	private double remainingPenaltyAndRemainingEmi = 0.0;

	@Column(name = "excess_of_emi_amount", nullable = true)
	private double excessOfEmiAmount;

	@Column(name = "escrowtranferflag", nullable = true)
	private Integer escrowTranferFlag = 0;

	@Column(name = "penality", nullable = true)
	private double penality = 0;

	@Column(name = "escrowtransferreddate", nullable = true, insertable = true, updatable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date escrowTransferredDate;

	@Column(name = "difference_in_days", nullable = true)
	private Integer differenceInDays;

	@Column(name = "updated_user_name", nullable = true)
	private String updatedUserName = "";

	@Column(name = "interest_waived_off", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date interest_waived_off;

	@Column(name = "lender_emi_principal_amount", nullable = true)
	private double lenderEmiPrincipalAmount;

	@Column(name = "lender_emi_interest_amount", nullable = true)
	private double lenderEmiInterestAmount;

	@Column(name = "lender_emi_amount", nullable = true)
	private double lenderEmiAmount;

	public Date getInterest_waived_off() {
		return interest_waived_off;
	}

	public void setInterest_waived_off(Date interest_waived_off) {
		this.interest_waived_off = interest_waived_off;
	}

	public String getUpdatedUserName() {
		return updatedUserName;
	}

	public void setUpdatedUserName(String updatedUserName) {
		this.updatedUserName = updatedUserName;
	}

	public Integer getEscrowTranferFlag() {
		return escrowTranferFlag;
	}

	public void setEscrowTranferFlag(Integer escrowTranferFlag) {
		this.escrowTranferFlag = escrowTranferFlag;
	}

	public Date getEscrowTransferredDate() {
		return escrowTransferredDate;
	}

	public void setEscrowTransferredDate(Date escrowTransferredDate) {
		this.escrowTransferredDate = escrowTransferredDate;
	}

	public String getStatusVericationMessageDescription() {
		return statusVericationMessageDescription;
	}

	public void setStatusVericationMessageDescription(String statusVericationMessageDescription) {
		this.statusVericationMessageDescription = statusVericationMessageDescription;
	}

	public String getClntTxnRef() {
		return clntTxnRef;
	}

	public void setClntTxnRef(String clntTxnRef) {
		this.clntTxnRef = clntTxnRef;
	}

	public String getEnachScheduledDate() {
		return enachScheduledDate;
	}

	public void setEnachScheduledDate(String enachScheduledDate) {
		this.enachScheduledDate = enachScheduledDate;
	}

	public String getStatusMessageDescription() {
		return statusMessageDescription;
	}

	public void setStatusMessageDescription(String statusMessageDescription) {
		this.statusMessageDescription = statusMessageDescription;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getTransactionSchedulingResponseJson() {
		return transactionSchedulingResponseJson;
	}

	public void setTransactionSchedulingResponseJson(String transactionSchedulingResponseJson) {
		this.transactionSchedulingResponseJson = transactionSchedulingResponseJson;
	}

	public String getTransactionVerificationResponseJson() {
		return transactionVerificationResponseJson;
	}

	public void setTransactionVerificationResponseJson(String transactionVerificationResponseJson) {
		this.transactionVerificationResponseJson = transactionVerificationResponseJson;
	}

	public String getEnachStatus() {
		return enachStatus;
	}

	public void setEnachStatus(String enachStatus) {
		this.enachStatus = enachStatus;
	}

	public String getPayuStatus() {
		return payuStatus;
	}

	public void setPayuStatus(String payuStatus) {
		this.payuStatus = payuStatus;
	}

	public String getPayuTransactionNumber() {
		return payuTransactionNumber;
	}

	public void setPayuTransactionNumber(String payuTransactionNumber) {
		this.payuTransactionNumber = payuTransactionNumber;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public Boolean getIsEmiProcessed() {
		return isEmiProcessed;
	}

	public void setIsEmiProcessed(Boolean isEmiProcessed) {
		this.isEmiProcessed = isEmiProcessed;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public Integer getLoanId() {
		return loanId;
	}

	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}

	public Integer getEmiNumber() {
		return emiNumber;
	}

	public void setEmiNumber(Integer emiNumber) {
		this.emiNumber = emiNumber;
	}

	public double getEmiPrincipalAmount() {
		return emiPrincipalAmount;
	}

	public void setEmiPrincipalAmount(double emiPrincipalAmount) {
		this.emiPrincipalAmount = emiPrincipalAmount;
	}

	public double getEmiInterstAmount() {
		return emiInterstAmount;
	}

	public void setEmiInterstAmount(double emiInterstAmount) {
		this.emiInterstAmount = emiInterstAmount;
	}

	public double getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(double emiAmount) {
		this.emiAmount = emiAmount;
	}

	public Date getEmiDueOn() {
		return emiDueOn;
	}

	public void setEmiDueOn(Date emiDueOn) {
		this.emiDueOn = emiDueOn;
	}

	public Date getEmiPaidOn() {
		return emiPaidOn;
	}

	public void setEmiPaidOn(Date emiPaidOn) {
		this.emiPaidOn = emiPaidOn;
	}

	public double getEmiLateFeeCharges() {
		return emiLateFeeCharges;
	}

	public void setEmiLateFeeCharges(double emiLateFeeCharges) {
		this.emiLateFeeCharges = emiLateFeeCharges;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public boolean isPaidOnTime() {
		return paidOnTime;
	}

	public void setPaidOnTime(boolean paidOnTime) {
		this.paidOnTime = paidOnTime;
	}

	public double getRemainingEmiAmount() {
		return remainingEmiAmount;
	}

	public void setRemainingEmiAmount(double remainingEmiAmount) {
		this.remainingEmiAmount = remainingEmiAmount;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public double getRemainingPenaltyAndRemainingEmi() {
		return remainingPenaltyAndRemainingEmi;
	}

	public void setRemainingPenaltyAndRemainingEmi(double remainingPenaltyAndRemainingEmi) {
		this.remainingPenaltyAndRemainingEmi = remainingPenaltyAndRemainingEmi;
	}

	public double getExcessOfEmiAmount() {
		return excessOfEmiAmount;
	}

	public void setExcessOfEmiAmount(double excessOfEmiAmount) {
		this.excessOfEmiAmount = excessOfEmiAmount;
	}

	public double getPenality() {
		return penality;
	}

	public void setPenality(double penality) {
		this.penality = penality;
	}

	public Integer getDifferenceInDays() {
		return differenceInDays;
	}

	public void setDifferenceInDays(Integer differenceInDays) {
		this.differenceInDays = differenceInDays;
	}

	public double getLenderEmiPrincipalAmount() {
		return lenderEmiPrincipalAmount;
	}

	public void setLenderEmiPrincipalAmount(double lenderEmiPrincipalAmount) {
		this.lenderEmiPrincipalAmount = lenderEmiPrincipalAmount;
	}

	public double getLenderEmiInterestAmount() {
		return lenderEmiInterestAmount;
	}

	public void setLenderEmiInterestAmount(double lenderEmiInterestAmount) {
		this.lenderEmiInterestAmount = lenderEmiInterestAmount;
	}

	public double getLenderEmiAmount() {
		return lenderEmiAmount;
	}

	public void setLenderEmiAmount(double lenderEmiAmount) {
		this.lenderEmiAmount = lenderEmiAmount;
	}

}
