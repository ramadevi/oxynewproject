package com.oxyloans.entity.nodalAccount;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.user.User;

@Entity
@Table(name = "fund_transfer")
public class FundTransfer {

	public static enum Transactiontype {
		CORPORATE_NODAL_IMPS, CORPORATE_NODAL_NEFT, CORPORATE_NODAL_RTGS, CORPORATE_NODAL_XFER, CORPORATE_NODAL_AUTO

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = true)
	private Integer userId;

	@Column(name = "beneficiary_name", nullable = true)
	private String beneficiaryName;

	@Column(name = "beneficiary_accountnumber", nullable = true)
	private String beneficiaryAccountnumber;

	@Column(name = "transactiontype", nullable = true)
	@Enumerated(EnumType.STRING)
	private Transactiontype transactiontype = Transactiontype.CORPORATE_NODAL_IMPS;

	@Column(name = "beneficiary_ifsc", nullable = true)
	private String beneficiaryIfsc;

	@Column(name = "channel", nullable = true)
	private String channel;

	@Column(name = "amount", nullable = true)
	private double amount;

	@Column(name = "transaction_request_id", nullable = true)
	private String transactionRequestId;

	@Column(name = "updated_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedOn = new Date();

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	@JoinColumn(name = "user_id", insertable = false, updatable = false)
	private User user;

	@Column(name = "updated_basedon_ist", nullable = true) // ist is indian standed time
	private String updatedBasedonIst;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryAccountnumber() {
		return beneficiaryAccountnumber;
	}

	public void setBeneficiaryAccountnumber(String beneficiaryAccountnumber) {
		this.beneficiaryAccountnumber = beneficiaryAccountnumber;
	}

	public Transactiontype getTransactiontype() {
		return transactiontype;
	}

	public void setTransactiontype(Transactiontype transactiontype) {
		this.transactiontype = transactiontype;
	}

	public String getBeneficiaryIfsc() {
		return beneficiaryIfsc;
	}

	public void setBeneficiaryIfsc(String beneficiaryIfsc) {
		this.beneficiaryIfsc = beneficiaryIfsc;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTransactionRequestId() {
		return transactionRequestId;
	}

	public void setTransactionRequestId(String transactionRequestId) {
		this.transactionRequestId = transactionRequestId;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUpdatedBasedonIst() {
		return updatedBasedonIst;
	}

	public void setUpdatedBasedonIst(String updatedBasedonIst) {
		this.updatedBasedonIst = updatedBasedonIst;
	}

}
