package com.oxyloans.entity.nodalAccount;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.user.User;

@Entity
@Table(name = "add_beneficiary_account")
public class AddBeneficiaryAccount {

	public static enum BeneficiaryUsertype {
		CUSTOMER, MERCHANT
	}

	public static enum Status {
		ADDED, EDITED, COMPLETED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = true)
	private Integer userId;

	@Column(name = "beneficiary_name", nullable = true)
	private String beneficiaryName;

	@Column(name = "beneficiary_nickname", nullable = true)
	private String beneficiaryNickname;

	@Column(name = "beneficiary_accountnumber", nullable = true)
	private String beneficiaryAccountnumber;

	@Column(name = "beneficiary_account_type", nullable = true)
	private String beneficiaryAccountType;

	@Column(name = "beneficiary_ifsc", nullable = true)
	private String beneficiaryIfsc;

	@Column(name = "beneficiary_usertype", nullable = true)
	@Enumerated(EnumType.STRING)
	private BeneficiaryUsertype beneficiaryUsertype = BeneficiaryUsertype.CUSTOMER;

	@Column(name = "created_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	@JoinColumn(name = "user_id", insertable = false, updatable = false)
	private User user;

	@Column(name = "beneficiary_account_already_exits", nullable = true)
	private boolean beneficiaryAccountAlreadyExits = false;

	@Column(name = "beneficiary_email", nullable = true)
	private String beneficiaryEmail;

	@Column(name = "beneficiary_mobile_number", nullable = true)
	private String beneficiaryMobileNumber;

	@Column(name = "amount", nullable = true)
	private double amount;

	@Column(name = "status", nullable = true)
	@Enumerated(EnumType.STRING)
	private Status status = Status.ADDED;

	@Column(name = "updated_basedon_ist", nullable = true) // ist is indian standed time
	private String updatedBasedonIst;

	@Column(name = "borrower_parent_requestid", nullable = true)
	private Integer borrowerParentRequestid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryNickname() {
		return beneficiaryNickname;
	}

	public void setBeneficiaryNickname(String beneficiaryNickname) {
		this.beneficiaryNickname = beneficiaryNickname;
	}

	public String getBeneficiaryAccountnumber() {
		return beneficiaryAccountnumber;
	}

	public void setBeneficiaryAccountnumber(String beneficiaryAccountnumber) {
		this.beneficiaryAccountnumber = beneficiaryAccountnumber;
	}

	public String getBeneficiaryAccountType() {
		return beneficiaryAccountType;
	}

	public void setBeneficiaryAccountType(String beneficiaryAccountType) {
		this.beneficiaryAccountType = beneficiaryAccountType;
	}

	public String getBeneficiaryIfsc() {
		return beneficiaryIfsc;
	}

	public void setBeneficiaryIfsc(String beneficiaryIfsc) {
		this.beneficiaryIfsc = beneficiaryIfsc;
	}

	public BeneficiaryUsertype getBeneficiaryUsertype() {
		return beneficiaryUsertype;
	}

	public void setBeneficiaryUsertype(BeneficiaryUsertype beneficiaryUsertype) {
		this.beneficiaryUsertype = beneficiaryUsertype;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isBeneficiaryAccountAlreadyExits() {
		return beneficiaryAccountAlreadyExits;
	}

	public void setBeneficiaryAccountAlreadyExits(boolean beneficiaryAccountAlreadyExits) {
		this.beneficiaryAccountAlreadyExits = beneficiaryAccountAlreadyExits;
	}

	public String getBeneficiaryEmail() {
		return beneficiaryEmail;
	}

	public void setBeneficiaryEmail(String beneficiaryEmail) {
		this.beneficiaryEmail = beneficiaryEmail;
	}

	public String getBeneficiaryMobileNumber() {
		return beneficiaryMobileNumber;
	}

	public void setBeneficiaryMobileNumber(String beneficiaryMobileNumber) {
		this.beneficiaryMobileNumber = beneficiaryMobileNumber;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getUpdatedBasedonIst() {
		return updatedBasedonIst;
	}

	public void setUpdatedBasedonIst(String updatedBasedonIst) {
		this.updatedBasedonIst = updatedBasedonIst;
	}

	public Integer getBorrowerParentRequestid() {
		return borrowerParentRequestid;
	}

	public void setBorrowerParentRequestid(Integer borrowerParentRequestid) {
		this.borrowerParentRequestid = borrowerParentRequestid;
	}

}
