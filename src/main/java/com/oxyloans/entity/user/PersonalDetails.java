package com.oxyloans.entity.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "user_personal_details")
public class PersonalDetails {

	@Id
	@Column(name = "user_id", unique = true)
	private Integer userId;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	private User user;

	@Column(name = "first_name", nullable = false)
	private String firstName;

	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Column(name = "father_name", nullable = false)
	private String fatherName = "";

	@Column(name = "dob", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dob;

	@Column(name = "gender", nullable = false)
	private String gender = "";

	@Column(name = "marital_status", nullable = true)
	private String maritalStatus;

	@Column(name = "nationality", nullable = true)
	private String nationality;

	@Column(name = "address", nullable = false)
	private String address = "";

	@Column(name = "created_at", nullable = false)
	private Date createdAt;

	@Column(name = "modified_at", nullable = false)
	private Date modifiedAt;

	@Column(name = "pan_number", nullable = false)
	private String panNumber = "";

	@Column(name = "middle_name", nullable = true)
	private String middleName;

	@Column(name = "permanent_address", nullable = true)
	private String permanentAddress;

	@Column(name = "facebook_url", nullable = true)
	private String facebookUrl;

	@Column(name = "twitter_url", nullable = true)
	private String twitterUrl;

	@Column(name = "linkedin_url", nullable = true)
	private String linkedinUrl;

	@Column(name = "whatsapp_number", nullable = true)
	private String whatsAppNumber;

	@Column(name = "user_expertise", nullable = true)
	private Boolean userExpertise = false;

	@Column(name = "whatsapp_verified_on", nullable = false)
	private Date whatsappVerifiedOn;

	@Column(name = "cif_number", nullable = true)
	private String cifNumber;

	@Column(name = "fino_employee_mobilenumber", nullable = true)
	private String finoEmployeeMobileNumber;

	@Column(name = "user_type", nullable = true)
	private String userType;

	@Column(name = "education", nullable = true)
	private String Education;

	@Column(name = "passion", nullable = true)
	private String Passion;

	@Column(name = "is_student", nullable = true)
	private Boolean isStudent = false;

	@Column(name = "site_tool", nullable = true)
	private Boolean siteTool = false;

	@Column(name = "aadhar_number", nullable = true)
	private String aadharNumber;
	
	@Column(name = "orginal_dob", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date orginalDob;

	public String getFacebookUrl() {
		return facebookUrl;
	}

	public String getTwitterUrl() {
		return twitterUrl;
	}

	public String getLinkedinUrl() {
		return linkedinUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public void setTwitterUrl(String twitterUrl) {
		this.twitterUrl = twitterUrl;
	}

	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getWhatsAppNumber() {
		return whatsAppNumber;
	}

	public void setWhatsAppNumber(String whatsAppNumber) {
		this.whatsAppNumber = whatsAppNumber;
	}

	public Boolean getUserExpertise() {
		return userExpertise;
	}

	public void setUserExpertise(Boolean userExpertise) {
		this.userExpertise = userExpertise;
	}

	public Date getWhatsappVerifiedOn() {
		return whatsappVerifiedOn;
	}

	public void setWhatsappVerifiedOn(Date whatsappVerifiedOn) {
		this.whatsappVerifiedOn = whatsappVerifiedOn;
	}

	public String getCifNumber() {
		return cifNumber;
	}

	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}

	public String getFinoEmployeeMobileNumber() {
		return finoEmployeeMobileNumber;
	}

	public void setFinoEmployeeMobileNumber(String finoEmployeeMobileNumber) {
		this.finoEmployeeMobileNumber = finoEmployeeMobileNumber;
	}

	public Boolean getIsStudent() {
		return isStudent;
	}

	public void setIsStudent(Boolean isStudent) {
		this.isStudent = isStudent;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getEducation() {
		return Education;
	}

	public void setEducation(String education) {
		Education = education;
	}

	public String getPassion() {
		return Passion;
	}

	public void setPassion(String passion) {
		Passion = passion;
	}

	public Boolean getSiteTool() {
		return siteTool;
	}

	public void setSiteTool(Boolean siteTool) {
		this.siteTool = siteTool;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public Date getOrginalDob() {
		return orginalDob;
	}

	public void setOrginalDob(Date orginalDob) {
		this.orginalDob = orginalDob;
	}
	

}