package com.oxyloans.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_address_details")
public class AddressDetails {
	
	private static final String SEPARATOR = ", ";

	public static enum Type {
		PRESENT, PERMANENT, OFFICE
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = false)
	private Integer userId;

	@Column(name = "type", nullable = false)
	@Enumerated(EnumType.STRING)
	private Type type = Type.PERMANENT;

	@Column(name = "house_number", nullable = false)
	private String houseNumber;

	@Column(name = "street", nullable = false)
	private String street;

	@Column(name = "area", nullable = true)
	private String area;

	@Column(name = "city", nullable = true)
	private String city;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getFullAdress() {
		return this.houseNumber +  SEPARATOR + this.getStreet() + SEPARATOR + this.getArea() + SEPARATOR + this.getCity();
	}

}
