package com.oxyloans.entity.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "utm_master")
public class UtmMaster {

	public static enum UtmName {
		Web, WN22_1, PV29_1, WN28_1, WN_1702
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "utm_name",insertable = true, updatable = true, nullable = true)
	@Enumerated(EnumType.STRING)
	private UtmName utmName = UtmName.Web;

	@Column(name = "utm_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date utmDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UtmName getUtmName() {
		return utmName;
	}

	public void setUtmName(UtmName utmName) {
		this.utmName = utmName;
	}

	public Date getUtmDate() {
		return utmDate;
	}

	public void setUtmDate(Date utmDate) {
		this.utmDate = utmDate;
	}

}
