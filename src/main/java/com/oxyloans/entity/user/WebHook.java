package com.oxyloans.entity.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="web_hook")
public class WebHook {
	public static enum NotificationTo{
		DAILY,WEEKLY,ADMINSTATS,MONTHLY,EVERYMONTHLY
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "message_id", nullable = true,unique = true)
	private String messageId;
	@Column(name = "notification_to", nullable = true)
	@Enumerated(EnumType.STRING)
	private NotificationTo notificationTo;
	@Column(name = "time_stamp_from", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date timeStampFrom;
	@Column(name = "current_stamp", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date currentStamp;
	
	@Column(name = "is_currnt", nullable = true)
	private boolean isCurrnt=false;
	
	@Column(name = "old_message_id", nullable = true)
	private String oldMessageId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public NotificationTo getNotificationTo() {
		return notificationTo;
	}
	public void setNotificationTo(NotificationTo notificationTo) {
		this.notificationTo = notificationTo;
	}
	public Date getTimeStampFrom() {
		return timeStampFrom;
	}
	public void setTimeStampFrom(Date timeStampFrom) {
		this.timeStampFrom = timeStampFrom;
	}
	public Date getCurrentStamp() {
		return currentStamp;
	}
	public void setCurrentStamp(Date currentStamp) {
		this.currentStamp = currentStamp;
	}
	public boolean isCurrnt() {
		return isCurrnt;
	}
	public void setCurrnt(boolean isCurrnt) {
		this.isCurrnt = isCurrnt;
	}
	public String getOldMessageId() {
		return oldMessageId;
	}
	public void setOldMessageId(String oldMessageId) {
		this.oldMessageId = oldMessageId;
	}

	
	

}
