package com.oxyloans.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_bank_details")
public class BankDetails {
	@Id
	@Column(name = "user_id", unique = true)
	private Integer userId;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	private User user;

	@Column(name = "account_number", nullable = false)
	private String accountNumber = "";

	@Column(name = "bank_name", nullable = false)
	private String bankName = "";

	@Column(name = "branch_name", nullable = false)
	private String branchName = "";

	@Column(name = "account_type", nullable = false)
	private String accountType = "";

	@Column(name = "ifsc_code", nullable = false)
	private String ifscCode = "";

	@Column(name = "address", nullable = false)
	private String address = "";

	@Column(name = "user_name", nullable = true)
	private String userName = "";

	@Column(name = "mode_of_transactions", nullable = true)
	private String modeOfTransactions = "";

	@Column(name = "bank_details_verified_status", nullable = true)
	private Boolean bankDetailsVerifiedstatus = false;

	@Column(name = "fy_income", nullable = true)
	private double fyIncome = 0.0;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getModeOfTransactions() {
		return modeOfTransactions;
	}

	public void setModeOfTransactions(String modeOfTransactions) {
		this.modeOfTransactions = modeOfTransactions;
	}

	public Boolean getBankDetailsVerifiedstatus() {
		return bankDetailsVerifiedstatus;
	}

	public void setBankDetailsVerifiedstatus(Boolean bankDetailsVerifiedstatus) {
		this.bankDetailsVerifiedstatus = bankDetailsVerifiedstatus;
	}

	public double getFyIncome() {
		return fyIncome;
	}

	public void setFyIncome(double fyIncome) {
		this.fyIncome = fyIncome;
	}

}
