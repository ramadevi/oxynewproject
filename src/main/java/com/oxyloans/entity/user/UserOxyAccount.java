package com.oxyloans.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_oxy_account")
public class UserOxyAccount {

	@Id
	@Column(name = "user_id", unique = true)
	private Integer userId;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	private User user;

	@Column(name = "oxy_account_number", nullable = false, unique = true)
	private String oxyAccountNumber;

	@Column(name = "total_amount", nullable = false)
	private Double totalAmount = 0.0;

	@Column(name = "disbursed_amount", nullable = false)
	private Double disbursedAmount = 0.0;

	@Column(name = "remainig_amount", nullable = false)
	private Double remainigAmount = 0.0;

	@Column(name = "no_of_active_loan_offers", nullable = false)
	private int noOfActiveLoanOffers = 0;

	@Column(name = "no_of_active_loan_requests", nullable = false)
	private int noOfActiveLoanRequests = 0;

	@Column(name = "no_of_active_approved_loans", nullable = false)
	private int noOfActiveApprovedLoans = 0;

	@Column(name = "active_participant_lenders", nullable = false)
	private int activeParticipantLenders = 0;

	@Column(name = "active_participant_borrowers", nullable = false)
	private int active_participant_borrowers = 0;

	@Column(name = "principal_received", nullable = false)
	private Double principalReceived = 0.0;

	@Column(name = "interest_earned", nullable = false)
	private Double interestEarned = 0.0;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getOxyAccountNumber() {
		return oxyAccountNumber;
	}

	public void setOxyAccountNumber(String oxyAccountNumber) {
		this.oxyAccountNumber = oxyAccountNumber;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getDisbursedAmount() {
		return disbursedAmount;
	}

	public void setDisbursedAmount(Double disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}

	public Double getRemainigAmount() {
		return remainigAmount;
	}

	public void setRemainigAmount(Double remainigAmount) {
		this.remainigAmount = remainigAmount;
	}

	public int getNoOfActiveLoanOffers() {
		return noOfActiveLoanOffers;
	}

	public void setNoOfActiveLoanOffers(int noOfActiveLoanOffers) {
		this.noOfActiveLoanOffers = noOfActiveLoanOffers;
	}

	public int getNoOfActiveLoanRequests() {
		return noOfActiveLoanRequests;
	}

	public void setNoOfActiveLoanRequests(int noOfActiveLoanRequests) {
		this.noOfActiveLoanRequests = noOfActiveLoanRequests;
	}

	public int getNoOfActiveApprovedLoans() {
		return noOfActiveApprovedLoans;
	}

	public void setNoOfActiveApprovedLoans(int noOfActiveApprovedLoans) {
		this.noOfActiveApprovedLoans = noOfActiveApprovedLoans;
	}

	public int getActiveParticipantLenders() {
		return activeParticipantLenders;
	}

	public void setActiveParticipantLenders(int activeParticipantLenders) {
		this.activeParticipantLenders = activeParticipantLenders;
	}

	public int getActive_participant_borrowers() {
		return active_participant_borrowers;
	}

	public void setActive_participant_borrowers(int active_participant_borrowers) {
		this.active_participant_borrowers = active_participant_borrowers;
	}

	public Double getPrincipalReceived() {
		return principalReceived;
	}

	public void setPrincipalReceived(Double principalReceived) {
		this.principalReceived = principalReceived;
	}

	public Double getInterestEarned() {
		return interestEarned;
	}

	public void setInterestEarned(Double interestEarned) {
		this.interestEarned = interestEarned;
	}

}
