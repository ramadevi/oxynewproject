package com.oxyloans.entity.user.LenderFavouriteUsers;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lender_favourite_users")
public class LenderFavouriteUsers {

	public static enum FavouriteType {
		FAVOURITE, UNFAVOURITE
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "lender_user_id", nullable = false)
	private Integer lenderUserId;

	@Column(name = "borrower_user_id", nullable = false)
	private Integer borrowerUserId;

	@Column(name = "borrower_favourite_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date borrowerFavouriteDate = new Date();

	@Column(name = "favourite_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private FavouriteType favouriteType = FavouriteType.FAVOURITE;

	@Column(name = "lender_gmail_contacts", nullable = true)
	private String lenderGmailContacts;
	
	@Column(name = "gmail_size", nullable = true)
	private Integer gmailSize; 

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLenderUserId() {
		return lenderUserId;
	}

	public void setLenderUserId(Integer lenderUserId) {
		this.lenderUserId = lenderUserId;
	}

	public Integer getBorrowerUserId() {
		return borrowerUserId;
	}

	public void setBorrowerUserId(Integer borrowerUserId) {
		this.borrowerUserId = borrowerUserId;
	}

	public Date getBorrowerFavouriteDate() {
		return borrowerFavouriteDate;
	}

	public void setBorrowerFavouriteDate(Date borrowerFavouriteDate) {
		this.borrowerFavouriteDate = borrowerFavouriteDate;
	}

	public FavouriteType getFavouriteType() {
		return favouriteType;
	}

	public void setFavouriteType(FavouriteType favouriteType) {
		this.favouriteType = favouriteType;
	}

	public String getLenderGmailContacts() {
		return lenderGmailContacts;
	}

	public void setLenderGmailContacts(String lenderGmailContacts) {
		this.lenderGmailContacts = lenderGmailContacts;
	}

	public Integer getGmailSize() {
		return gmailSize;
	}

	public void setGmailSize(Integer gmailSize) {
		this.gmailSize = gmailSize;
	}

}
