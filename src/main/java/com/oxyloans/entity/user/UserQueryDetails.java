package com.oxyloans.entity.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="user_query_details")
public class UserQueryDetails {
	
	public static enum Status {
		Pending,Completed,Cancelled
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "user_id", nullable = true)
	private Integer userId = 0;
	
	@Column(name = "query", nullable = true)
	private String query;
	
	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status = Status.Pending;
	
	@Column(name = "document_id", nullable = true)
	private Integer documentId = 0;
	
	@Column(name = "email", nullable = true)
	private String email;

	@Column(name = "mobile_number", nullable = true)
	private String mobileNumber;
	
	@Column(name = "comments", nullable = true)
	private String comments;
	
	@Column(name = "resolved_by", nullable = true)
	private String resolvedBy;
	
	@Column(name = "received_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date receivedOn;
	
	@Column(name = "responded_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date respondedOn;
	
	@Column(name = "admin_document_id", nullable = true)
	private Integer adminDocumentId = 0;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public Date getReceivedOn() {
		return receivedOn;
	}

	public void setReceivedOn(Date receivedOn) {
		this.receivedOn = receivedOn;
	}

	public Date getRespondedOn() {
		return respondedOn;
	}

	public void setRespondedOn(Date respondedOn) {
		this.respondedOn = respondedOn;
	}

	public Integer getAdminDocumentId() {
		return adminDocumentId;
	}

	public void setAdminDocumentId(Integer adminDocumentId) {
		this.adminDocumentId = adminDocumentId;
	}

}
