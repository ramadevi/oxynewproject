package com.oxyloans.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_professional_details")
public class ProfessionalDetails implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 1624719460526402027L;

	public static enum Employment {
		PUBLIC, PRIVATE, GOVERMENT,SALARIED,SELFEMPLOYED
	}

	@Id
	@Column(name = "user_id", unique = true)
	private Integer userId;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	private User user;

	@Column(name = "employment", nullable = false)
	@Enumerated(EnumType.STRING)
	private Employment employment = Employment.PRIVATE;

	@Column(name = "company_name", nullable = false)
	private String companyName="";

	@Column(name = "designation", nullable = false)
	private String designation="";

	@Column(name = "description", nullable = true)
	private String description="";

	@Column(name = "no_of_jobs_changed", nullable = false)
	private Integer noOfJobsChanged=0;

	@Column(name = "work_experience", nullable = false)
	private String workExperience="";

	@Column(name = "office_address_id", nullable = false)
	private Integer officeAddressId=0;

	@Column(name = "office_contact_number", nullable = false)
	private String officeContactNumber="";

	@Column(name = "highest_qualification", nullable = true)
	private String highestQualification="";

	@Column(name = "field_of_study", nullable = false)
	private String fieldOfStudy="";

	@Column(name = "college", nullable = false)
	private String college="";

	@Column(name = "year_of_passing", nullable = false)
	private Integer yearOfPassing=0;
	
	@Column(name = "document_drive_link", nullable = true)
	private String documentDriveLink;


	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Employment getEmployment() {
		return employment;
	}

	public void setEmployment(Employment employment) {
		this.employment = employment;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getNoOfJobsChanged() {
		return noOfJobsChanged;
	}

	public void setNoOfJobsChanged(Integer noOfJobsChanged) {
		this.noOfJobsChanged = noOfJobsChanged;
	}

	public String getWorkExperience() {
		return workExperience;
	}

	public void setWorkExperience(String workExperience) {
		this.workExperience = workExperience;
	}

	public Integer getOfficeAddressId() {
		return officeAddressId;
	}

	public void setOfficeAddressId(Integer officeAddressId) {
		this.officeAddressId = officeAddressId;
	}

	public String getOfficeContactNumber() {
		return officeContactNumber;
	}

	public void setOfficeContactNumber(String officeContactNumber) {
		this.officeContactNumber = officeContactNumber;
	}

	public String getHighestQualification() {
		return highestQualification;
	}

	public void setHighestQualification(String highestQualification) {
		this.highestQualification = highestQualification;
	}

	public String getFieldOfStudy() {
		return fieldOfStudy;
	}

	public void setFieldOfStudy(String fieldOfStudy) {
		this.fieldOfStudy = fieldOfStudy;
	}

	public String getCollege() {
		return college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public Integer getYearOfPassing() {
		return yearOfPassing;
	}

	public void setYearOfPassing(Integer yearOfPassing) {
		this.yearOfPassing = yearOfPassing;
	}

	public String getDocumentDriveLink() {
		return documentDriveLink;
	}

	public void setDocumentDriveLink(String documentDriveLink) {
		this.documentDriveLink = documentDriveLink;
	}

}