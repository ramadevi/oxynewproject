package com.oxyloans.entity.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="user_pending_queries")
public class UserPendingQueries {
	
	
	public static enum RespondedBy {
		USER, ADMIN
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "table_id", nullable = true)
	private Integer tableId = 0;
	
	@Column(name = "pending_comments", nullable = true)
	private String pendingComments;
	
	@Column(name = "responded_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date respondedOn;
	
	@Column(name = "resolved_by", nullable = true)
	private String resolvedBy;
	
	@Column(name = "admin_document_id", nullable = true)
	private Integer adminDocumentId = 0;

	
	  @Column(name = "responded_by", nullable = true)
	  @Enumerated(EnumType.STRING)
	  private RespondedBy respondedBy;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public String getPendingComments() {
		return pendingComments;
	}

	public void setPendingComments(String pendingComments) {
		this.pendingComments = pendingComments;
	}

	public Date getRespondedOn() {
		return respondedOn;
	}

	public void setRespondedOn(Date respondedOn) {
		this.respondedOn = respondedOn;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public Integer getAdminDocumentId() {
		return adminDocumentId;
	}

	public void setAdminDocumentId(Integer adminDocumentId) {
		this.adminDocumentId = adminDocumentId;
	}

	public RespondedBy getRespondedBy() {
		return respondedBy;
	}

	public void setRespondedBy(RespondedBy respondedBy) {
		this.respondedBy = respondedBy;
	}

	

}
