package com.oxyloans.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_profile_risk")
public class UserProfileRisk {
@Id
@Column(name = "user_id", unique = true)
private Integer userId;

@OneToOne(fetch = FetchType.LAZY)
@MapsId
private User user;

@Column(name = "salary_or_income", nullable = true)
private Integer salaryOrIncome;

@Column(name = "company_or_organization", nullable = true)
private Integer CompanyOrOrganization;

@Column(name = "experiance_or_existence_of_organization", nullable = true)
private Integer experianceOrExistenceOfOrganization;

@Column(name = "cibil_score", nullable = true)
private Integer cibilScore;

@Column(name = "total_score", nullable = true)
private Integer totalScore;

@Column(name = "grade", nullable = true)
private String  grade;

public String getGrade() {
	return grade;
}
public void setGrade(String grade) {
	this.grade = grade;
}
public Integer getTotalScore() {
	return totalScore;
}
public void setTotalScore(Integer totalScore) {
	this.totalScore = totalScore;
}
public Integer getUserId() {
	return userId;
}
public User getUser() {
	return user;
}
public Integer getSalaryOrIncome() {
	return salaryOrIncome;
}
public Integer getCompanyOrOrganization() {
	return CompanyOrOrganization;
}
public Integer getExperianceOrExistenceOfOrganization() {
	return experianceOrExistenceOfOrganization;
}
public Integer getCibilScore() {
	return cibilScore;
}
public void setUserId(Integer userId) {
	this.userId = userId;
}
public void setUser(User user) {
	this.user = user;
}
public void setSalaryOrIncome(Integer salaryOrIncome) {
	this.salaryOrIncome = salaryOrIncome;
}
public void setCompanyOrOrganization(Integer companyOrOrganization) {
	CompanyOrOrganization = companyOrOrganization;
}
public void setExperianceOrExistenceOfOrganization(Integer experianceOrExistenceOfOrganization) {
	this.experianceOrExistenceOfOrganization = experianceOrExistenceOfOrganization;
}
public void setCibilScore(Integer cibilScore) {
	this.cibilScore = cibilScore;
}

}
