package com.oxyloans.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_financial_details")
public class FinancialDetails {

	@Id
	@Column(name = "user_id", unique = true)
	private Integer userId;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	private User user;

	@Column(name = "monthly_emi", nullable = false)
	private Integer monthlyEmi=0;

	@Column(name = "credit_amount", nullable = false)
	private Integer creditAmount=0;

	@Column(name = "existing_loan_amount", nullable = false)
	private Integer existingLoanAmount=0;

	@Column(name = "credit_cards_repayment_amount", nullable = false)
	private Integer creditCardsRepaymentAmount=0;

	@Column(name = "other_sources_income", nullable = false)
	private Integer otherSourcesIncome=0;

	@Column(name = "net_monthly_income", nullable = false)
	private Integer netMonthlyIncome=0;

	@Column(name = "avg_monthly_expenses", nullable = false)
	private Integer avgMonthlyExpenses=0;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getMonthlyEmi() {
		return monthlyEmi;
	}

	public void setMonthlyEmi(Integer monthlyEmi) {
		this.monthlyEmi = monthlyEmi;
	}

	public Integer getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Integer creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Integer getExistingLoanAmount() {
		return existingLoanAmount;
	}

	public void setExistingLoanAmount(Integer existingLoanAmount) {
		this.existingLoanAmount = existingLoanAmount;
	}

	public Integer getCreditCardsRepaymentAmount() {
		return creditCardsRepaymentAmount;
	}

	public void setCreditCardsRepaymentAmount(Integer creditCardsRepaymentAmount) {
		this.creditCardsRepaymentAmount = creditCardsRepaymentAmount;
	}

	public Integer getOtherSourcesIncome() {
		return otherSourcesIncome;
	}

	public void setOtherSourcesIncome(Integer otherSourcesIncome) {
		this.otherSourcesIncome = otherSourcesIncome;
	}

	public Integer getNetMonthlyIncome() {
		return netMonthlyIncome;
	}

	public void setNetMonthlyIncome(Integer netMonthlyIncome) {
		this.netMonthlyIncome = netMonthlyIncome;
	}

	public Integer getAvgMonthlyExpenses() {
		return avgMonthlyExpenses;
	}

	public void setAvgMonthlyExpenses(Integer avgMonthlyExpenses) {
		this.avgMonthlyExpenses = avgMonthlyExpenses;
	}

}
