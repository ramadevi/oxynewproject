package com.oxyloans.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "lender_nominee_details")
public class Nominee {
	@Id
	@Column(name = "user_id", unique = true)
	private Integer userId;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	private User user;
	
	@Column(name = "email", nullable = true)
	private String email;

	@Column(name = "mobile_numer", nullable = true)
	private String mobileNumber;

	@Column(name = "name", nullable = true)
	private String name;

	@Column(name = "relation", nullable = true)
	private String relation;

	@Column(name = "account_number", nullable = true)
	private String accountNumber;

	@Column(name = "bank_name", nullable = true)
	private String bankName;

	@Column(name = "branch_name", nullable = true)
	private String branchName;

	@Column(name = "ifsc_code", nullable = true)
	private String ifscCode;

	@Column(name = "city", nullable = true)
	private String city;

	
	public String getEmail() {
		return email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Integer getUserId() {
		return userId;
	}

	public User getUser() {
		return user;
	}

	public String getName() {
		return name;
	}

	public String getRelation() {
		return relation;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	
	public String getCity() {
		return city;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	
	public void setCity(String city) {
		this.city = city;
	}

}
