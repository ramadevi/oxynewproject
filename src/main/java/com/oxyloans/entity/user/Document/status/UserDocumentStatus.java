package com.oxyloans.entity.user.Document.status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_document_status")
public class UserDocumentStatus {

	public static enum DocumentType {
		Kyc,ScrowScreenShot,PartnerAgreementType
	}
	public static enum Status {
		UPLOADED, ACCEPTED, REJECTED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Override
	public String toString() {
		return "UserDocumentStatus [id=" + id + ", userId=" + userId + ", documentType=" + documentType
				+ ", DocumentSubType=" + documentSubType + ", status=" + status + ", filePath=" + filePath
				+ ", fileName=" + fileName + "]";
	}

	@Column(name = "user_id", nullable = false)
	private Integer userId;

	@Column(name = "document_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private DocumentType documentType = DocumentType.Kyc;

	@Column(name = " document_Sub_Type", nullable = false)
	private String documentSubType;
	
	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;

	@Column(name = "file_Path", nullable = false)
	private String filePath;

	@Column(name = "file_name", nullable = false)
	private String fileName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public String getDocumentSubType() {
		return documentSubType;
	}

	public void setDocumentSubType(String documentSubType) {
		this.documentSubType = documentSubType;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
	
}
