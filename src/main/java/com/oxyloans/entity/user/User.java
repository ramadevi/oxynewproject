package com.oxyloans.entity.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.experian.ExperianSummary;
import com.oxyloans.entity.userlogin.UserLoginHistory;

@Entity
@Table(name = "`user`")
public class User {

	public static enum Status {
		REGISTERED, EMAIL_VERIFIED, ACTIVE, INACTIVE, INTERESTED, APPROVED, DISBURSED, VERIFIED, OXYRECOMMENDS,
		ADMINREJECTED, STUDENTADMIN
	}

	public static enum PrimaryType {

		LENDER("LR"), BORROWER("BR"), ADMIN("AD"), NONE("N"), OXYWHEELSADMIN("OAD"), PAYMENTSADMIN("PAD"),
		HELPDESKADMIN("HA"), SUBBUADMIN("SA"), SUPERADMIN("RA"), PARTNERADMIN("PA"), RADHAADMIN("RRA"), TESTADMIN("TA"),
		MASTERADMIN("MA"), BORROWERADMIN("BA"), STUDENTADMIN("OSA"); // ("oxyloansStudentAdmin")

		private String shortCode;

		private PrimaryType(String shortCode) {
			this.shortCode = shortCode;
		}

		public String getShortCode() {
			return shortCode;
		}
	}

	public static enum Citizenship {
		NRI, NONNRI
	}

	public static enum EmailApproval {
		ACTIVE, INACTIVE
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private ProfessionalDetails ProfessionalDetails;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private ReferenceDetails ReferenceDetails;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private PersonalDetails personalDetails;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private FinancialDetails financialDetails;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private BankDetails bankDetails;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private ExperianSummary experianSummary;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private UserProfileRisk userProfileRisk;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private Nominee nominee;

	@Column(name = "email", insertable = true, updatable = true, nullable = true)
	private String email;

	@Column(name = "mobile_number", nullable = false)
	private String mobileNumber;

	@Column(name = "password", nullable = true)
	private String password;

	@Column(name = "salt", nullable = true, insertable = true, updatable = false)
	private String salt;

	@Column(name = "email_token", nullable = true)
	private String emailToken;

	@Column(name = "email_sent", nullable = false)
	private boolean emailSent = false;

	@Column(name = "email_verified", nullable = false)
	private Boolean emailVerified = false;

	@Column(name = "email_sent_on", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date emailSentOn = new Date();

	@Column(name = "email_verified_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date emailVerifiedOn;

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status = Status.REGISTERED;

	@Column(name = "primary_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private PrimaryType primaryType = PrimaryType.BORROWER;

	@Column(name = "country_id")
	private Integer countryId;

	@Column(name = "registered_on", nullable = false, insertable = true, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date registeredOn = new Date();

	@Column(name = "modified_on", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn = new Date();

	@Column(name = "mobile_otp_session", nullable = false)
	private String mobileOtpSession;

	@Column(name = "mobile_otp_sent", nullable = false)
	private boolean mobileOtpSent = true;

	@Column(name = "mobile_number_verified", nullable = false)
	private boolean mobileNumberVerified = false;

	@Column(name = "kyc_file_path", nullable = true)
	private String kycFilePath;

	@Column(name = "longitude", nullable = true)
	private Double longitude;

	@Column(name = "latitude", nullable = true)
	private Double latitude;

	@Column(name = "unique_number", nullable = false)
	private String uniqueNumber;

	@Column(name = "oxy_score")
	private Integer oxyScore = 0;

	@Column(name = "old_applicatoin_user", insertable = true, updatable = true)
	private boolean oldApplicatoinUser = false;

	@Column(name = "city", nullable = false)
	private String city;

	@Column(name = "admin_comments", nullable = false)
	private String adminComments;

	@Column(name = "pincode", nullable = false)
	private String pinCode;

	@Column(name = "state", nullable = false)
	private String state;

	@Column(name = "email_otp_session", nullable = false)
	private String emailOtpSession;

	@Column(name = "urchin_tracking_module", nullable = true)
	private String urchinTrackingModule;

	@Column(name = "borrower_fee", nullable = true)
	private Double BorrowerFee;

	@Column(name = "locality", nullable = true)
	private String locality;

	@Column(name = "enachtype", nullable = true)
	private String enachType;

	@Column(name = "van_number", nullable = true)
	private String vanNumber;

	@Column(name = "loan_owner", nullable = true)
	private String loanOwner;

	@Column(name = "citizenship", nullable = true)
	@Enumerated(EnumType.STRING)
	private Citizenship citizenship = Citizenship.NONNRI;

	@Column(name = "lender_group_id")
	private Integer lenderGroupId = 0;

	@Column(name = "whatsapp_hash", nullable = true)
	private String whatsappHash;

	@Column(name = "whastapp_verified", nullable = true)
	private boolean whatsappVerified = false;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private OxyUsersuuidFromMobiledevice oxyUsersuuidFromMobiledevice;

	@Column(name = "email_approval", nullable = true)
	@Enumerated(EnumType.STRING)
	private EmailApproval emailApproval = EmailApproval.ACTIVE;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<UserLoginHistory> userLoginHistory = new ArrayList<>();

	@Column(name = "fee_status", nullable = true)
	private boolean feeStatus = false;

	@Column(name = "test_user", nullable = true)
	private boolean testUser = false;

	@Column(name = "participation_count")
	private Integer participationCount = 0;

	public String getVanNumber() {
		return vanNumber;
	}

	public void setVanNumber(String vanNumber) {
		this.vanNumber = vanNumber;
	}

	public Nominee getNominee() {
		return nominee;
	}

	public void setNominee(Nominee nominee) {
		this.nominee = nominee;
	}

	public String getEnachType() {
		return enachType;
	}

	public void setEnachType(String enachType) {
		this.enachType = enachType;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public ExperianSummary getExperianSummary() {
		return experianSummary;
	}

	public UserProfileRisk getUserProfileRisk() {
		return userProfileRisk;
	}

	public void setUserProfileRisk(UserProfileRisk userProfileRisk) {
		this.userProfileRisk = userProfileRisk;
	}

	public void setExperianSummary(ExperianSummary experianSummary) {
		this.experianSummary = experianSummary;
	}

	public Double getBorrowerFee() {
		return BorrowerFee;
	}

	public void setBorrowerFee(Double borrowerFee) {
		BorrowerFee = borrowerFee;
	}

	public String getUrchinTrackingModule() {
		return urchinTrackingModule;
	}

	public void setUrchinTrackingModule(String urchinTrackingModule) {
		this.urchinTrackingModule = urchinTrackingModule;
	}

	public Boolean getEmailVerified() {
		return emailVerified;
	}

	public String getEmailOtpSession() {
		return emailOtpSession;
	}

	public void setEmailOtpSession(String emailOtpSession) {
		this.emailOtpSession = emailOtpSession;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public String getAdminComments() {
		return adminComments;
	}

	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ProfessionalDetails getProfessionalDetails() {
		return ProfessionalDetails;
	}

	public void setProfessionalDetails(ProfessionalDetails professionalDetails) {
		ProfessionalDetails = professionalDetails;
	}

	public ReferenceDetails getReferenceDetails() {
		return ReferenceDetails;
	}

	public void setReferenceDetails(ReferenceDetails referenceDetails) {
		ReferenceDetails = referenceDetails;
	}

	public PersonalDetails getPersonalDetails() {
		return personalDetails;
	}

	public void setPersonalDetails(PersonalDetails personalDetails) {
		this.personalDetails = personalDetails;
	}

	public FinancialDetails getFinancialDetails() {
		return financialDetails;
	}

	public void setFinancialDetails(FinancialDetails financialDetails) {
		this.financialDetails = financialDetails;
	}

	public BankDetails getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(BankDetails bankDetails) {
		this.bankDetails = bankDetails;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getEmailToken() {
		return emailToken;
	}

	public void setEmailToken(String emailToken) {
		this.emailToken = emailToken;
	}

	public boolean isEmailSent() {
		return emailSent;
	}

	public void setEmailSent(boolean emailSent) {
		this.emailSent = emailSent;
	}

	public Boolean isEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(Boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	public Date getEmailSentOn() {
		return emailSentOn;
	}

	public void setEmailSentOn(Date emailSentOn) {
		this.emailSentOn = emailSentOn;
	}

	public Date getEmailVerifiedOn() {
		return emailVerifiedOn;
	}

	public void setEmailVerifiedOn(Date emailVerifiedOn) {
		this.emailVerifiedOn = emailVerifiedOn;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public PrimaryType getPrimaryType() {
		return primaryType;
	}

	public void setPrimaryType(PrimaryType primaryType) {
		this.primaryType = primaryType;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public Date getRegisteredOn() {
		return registeredOn;
	}

	public void setRegisteredOn(Date registeredOn) {
		this.registeredOn = registeredOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getMobileOtpSession() {
		return mobileOtpSession;
	}

	public void setMobileOtpSession(String mobileOtpSession) {
		this.mobileOtpSession = mobileOtpSession;
	}

	public boolean isMobileOtpSent() {
		return mobileOtpSent;
	}

	public void setMobileOtpSent(boolean mobileOtpSent) {
		this.mobileOtpSent = mobileOtpSent;
	}

	public boolean isMobileNumberVerified() {
		return mobileNumberVerified;
	}

	public void setMobileNumberVerified(boolean mobileNumberVerified) {
		this.mobileNumberVerified = mobileNumberVerified;
	}

	public String getKycFilePath() {
		return kycFilePath;
	}

	public void setKycFilePath(String kycFilePath) {
		this.kycFilePath = kycFilePath;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(String uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public Integer getOxyScore() {
		return oxyScore;
	}

	public void setOxyScore(Integer oxyScore) {
		this.oxyScore = oxyScore;
	}

	public boolean isOldApplicatoinUser() {
		return oldApplicatoinUser;
	}

	public void setOldApplicatoinUser(boolean oldApplicatoinUser) {
		this.oldApplicatoinUser = oldApplicatoinUser;
	}

	public String getLoanOwner() {
		return loanOwner;
	}

	public void setLoanOwner(String loanOwner) {
		this.loanOwner = loanOwner;
	}

	public Citizenship getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(Citizenship citizenship) {
		this.citizenship = citizenship;
	}

	public Integer getLenderGroupId() {
		return lenderGroupId;
	}

	public void setLenderGroupId(Integer lenderGroupId) {
		this.lenderGroupId = lenderGroupId;
	}

	public String getWhatsappHash() {
		return whatsappHash;
	}

	public void setWhatsappHash(String whatsappHash) {
		this.whatsappHash = whatsappHash;
	}

	public boolean isWhatsappVerified() {
		return whatsappVerified;
	}

	public void setWhatsappVerified(boolean whatsappVerified) {
		this.whatsappVerified = whatsappVerified;
	}

	public OxyUsersuuidFromMobiledevice getOxyUsersuuidFromMobiledevice() {
		return oxyUsersuuidFromMobiledevice;
	}

	public void setOxyUsersuuidFromMobiledevice(OxyUsersuuidFromMobiledevice oxyUsersuuidFromMobiledevice) {
		this.oxyUsersuuidFromMobiledevice = oxyUsersuuidFromMobiledevice;
	}

	public List<UserLoginHistory> getUserLoginHistory() {
		return userLoginHistory;
	}

	public void setUserLoginHistory(List<UserLoginHistory> userLoginHistory) {
		this.userLoginHistory = userLoginHistory;
	}

	public boolean isFeeStatus() {
		return feeStatus;
	}

	public void setFeeStatus(boolean feeStatus) {
		this.feeStatus = feeStatus;
	}

	public boolean isTestUser() {
		return testUser;
	}

	public void setTestUser(boolean testUser) {
		this.testUser = testUser;
	}

	public Integer getParticipationCount() {
		return participationCount;
	}

	public void setParticipationCount(Integer participationCount) {
		this.participationCount = participationCount;
	}

}
