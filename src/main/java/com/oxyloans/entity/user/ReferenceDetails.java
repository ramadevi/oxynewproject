package com.oxyloans.entity.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_reference_details")
public class ReferenceDetails {

	@Id
	@Column(name = "user_id", unique = true)
	private Integer userId;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	private User user;

	@Column(name = "reference_1", nullable = true)
	private String reference1;

	@Column(name = "reference_2", nullable = true)
	private String reference2;

	@Column(name = "reference_3", nullable = true)
	private String reference3;

	@Column(name = "reference_4", nullable = true)
	private String reference4;

	@Column(name = "reference_5", nullable = true)
	private String reference5;

	@Column(name = "reference_6", nullable = true)
	private String reference6;

	@Column(name = "reference_7", nullable = true)
	private String reference7;

	@Column(name = "reference_8", nullable = true)
	private String reference8;

	@Column(name = "created_on", nullable = true)
	private Date created_on;

	@Column(name = "modified_on", nullable = true)
	private Date modified_on;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getReference1() {
		return reference1;
	}

	public void setReference1(String reference1) {
		this.reference1 = reference1;
	}

	public String getReference2() {
		return reference2;
	}

	public void setReference2(String reference2) {
		this.reference2 = reference2;
	}

	public String getReference3() {
		return reference3;
	}

	public void setReference3(String reference3) {
		this.reference3 = reference3;
	}

	public String getReference4() {
		return reference4;
	}

	public void setReference4(String reference4) {
		this.reference4 = reference4;
	}

	public String getReference5() {
		return reference5;
	}

	public void setReference5(String reference5) {
		this.reference5 = reference5;
	}

	public String getReference6() {
		return reference6;
	}

	public void setReference6(String reference6) {
		this.reference6 = reference6;
	}

	public String getReference7() {
		return reference7;
	}

	public void setReference7(String reference7) {
		this.reference7 = reference7;
	}

	public String getReference8() {
		return reference8;
	}

	public void setReference8(String reference8) {
		this.reference8 = reference8;
	}

	public Date getCreated_on() {
		return created_on;
	}

	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}

	public Date getModified_on() {
		return modified_on;
	}

	public void setModified_on(Date modified_on) {
		this.modified_on = modified_on;
	}

}
