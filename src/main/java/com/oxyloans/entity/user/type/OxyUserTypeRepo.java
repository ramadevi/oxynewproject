package com.oxyloans.entity.user.type;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface OxyUserTypeRepo
		extends PagingAndSortingRepository<OxyUserType, Integer>, JpaSpecificationExecutor<OxyUserType> {

	@Query(value = "SELECT * FROM public.oxy_partner_details where upper(utm_name)=upper(:utm) and user_type='PARTNER'", nativeQuery = true)
	public OxyUserType getDetailsByUtmNameAndType(@Param("utm") String utm);

	@Query(value = "SELECT * FROM public.oxy_partner_details where user_type='PARTNER' order by id desc limit :pageSize  offset :pageNo", nativeQuery = true)
	public List<OxyUserType> getUsersBasedOnType(@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "SELECT count(id) FROM public.oxy_partner_details where user_type='PARTNER'", nativeQuery = true)
	public Integer getUsersBasedOnTypeCount();

	@Query(value = "select * from public.oxy_partner_details where upper(utm_name)=upper(:urchinTrackingModule) and type='SALARIED'\r\n"
			+ "", nativeQuery = true)
	public OxyUserType findingUserUtm(@Param("urchinTrackingModule") String urchinTrackingModule);

	@Query(value = "select * from public.oxy_partner_details where upper(utm_name)=upper(:utm)", nativeQuery = true)
	public OxyUserType findingUtm(@Param("utm") String utm);

	@Query(value = "select * from public.oxy_partner_details", nativeQuery = true)
	public List<OxyUserType> findingAllUtms();

	@Query(value = "select * from public.oxy_partner_details where id=:referrerId", nativeQuery = true)
	public OxyUserType findingPartnerDetails(@Param("referrerId") Integer referrerId);

	public int countByCreatedOnGreaterThanEqual(Date createdOn);

	@Query(value = "select count(*) from public.oxy_partner_details", nativeQuery = true)
	public int countOfPartnersRegistered();

	@Query(value = "select * from public.oxy_partner_details where id=:referrerId", nativeQuery = true)
	public OxyUserType findByIdNum(@Param("referrerId") Integer referrerId);

	@Query(value = "select u.id,(select CAST(SUM(A.transaction_amount)  AS DOUBLE PRECISION) from public.lender_scrow_wallet A where u.id = A.user_id AND A.status='APPROVED' AND transactiontype='credit') as \"credit\",\r\n"
			+ "			(select CAST(SUM(A.transaction_amount)  AS DOUBLE PRECISION)from public.lender_scrow_wallet A where u.id = A.user_id AND A.status='APPROVED' AND transactiontype='debit') as \"debit\",\r\n"
			+ "			(SELECT sum(O.disbursment_amount) FROM public.oxy_loan O where u.id =O.lender_user_id and (O.loan_status='Agreed' OR O.loan_status='Active' OR O.loan_status='Closed' OR O.loan_status='Hold') )\r\n"
			+ "			as \"agrements\" from public.user u where u.urchin_tracking_module=:urchinTrackingModule and u.primary_type='LENDER' order by u.id ASC", nativeQuery = true)
	public List<Object[]> lendersMappedToPartner(@Param("urchinTrackingModule") String urchinTrackingModule);

	@Query(value = "select count(*) as \"todayLenders\",(select count(*) from public.user where upper(urchin_tracking_module)=upper(:urchinTrackingModule) and primary_type='LENDER') as \"totalLenders\",(select count(*) from public.user where upper(urchin_tracking_module)=upper(:urchinTrackingModule) and TO_CHAR(registered_on,'yyyy-MM-dd')=:currentDate and primary_type='BORROWER') as \"todayBorrowers\",(select count(*) from public.user where upper(urchin_tracking_module)=upper(:urchinTrackingModule) and primary_type='BORROWER') as \"totalBorrowers\",(\r\n"
			+ "select count(b.id) from public.user a join public.oxy_loan b on a.id=b.borrower_user_id where upper(a.urchin_tracking_module)=upper(:urchinTrackingModule) and a.primary_type='BORROWER' and b.loan_status='Active' and b.borrower_disbursed_date is not null) as \"runningLoansCount\",(select sum(b.disbursment_amount) from public.user a join public.oxy_loan b on a.id=b.borrower_user_id where upper(a.urchin_tracking_module)=upper(:urchinTrackingModule) and a.primary_type='BORROWER' and b.loan_status='Active' and b.borrower_disbursed_date is not null) as \"runningLoansAmount\",(\r\n"
			+ "select count(b.id) from public.user a join public.oxy_loan b on a.id=b.borrower_user_id where upper(a.urchin_tracking_module)=upper(:urchinTrackingModule) and a.primary_type='BORROWER' and b.loan_status='Closed' and b.borrower_disbursed_date is not null) as \"closedLoansCount\",(select sum(b.disbursment_amount) from public.user a join public.oxy_loan b on a.id=b.borrower_user_id where upper(a.urchin_tracking_module)=upper(:urchinTrackingModule) and a.primary_type='BORROWER' and (b.loan_status='Active' OR b.loan_status='Closed') and b.borrower_disbursed_date is not null) as \"disbursedAmount\",(\r\n"
			+ "select sum(b.borrower_fee) from public.user a join public.oxy_cms_loans b on a.id=b.user_id where upper(a.urchin_tracking_module)=upper(:urchinTrackingModule) and a.primary_type='BORROWER' and b.transaction_status='EXECUTED' and b.borrower_came_from='PARTNER') as \"totalFeePaid\" from public.user where upper(urchin_tracking_module)=upper(:urchinTrackingModule) and TO_CHAR(registered_on,'yyyy-MM-dd')=:currentDate and primary_type='LENDER'", nativeQuery = true)
	public List<Object[]> partnerDashboard(@Param("urchinTrackingModule") String urchinTrackingModule,
			@Param("currentDate") String currentDate);

}
