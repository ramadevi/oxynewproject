package com.oxyloans.entity.user.type;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

@Entity
@Table(name = "oxy_partner_details")
public class OxyUserType {

	public static enum UserType {
		PARTNER, DEALER
	}

	public static enum RepaymentMethod {
		PI, I, BOTH
	}

	public static enum Type {
		SALARIED, SELFEMPLOYED
	}

	public static enum AdditionalFieldsToUser {
		NO, YES
	}
	
	public static enum ReferralCheck{
		YES,NO
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer Id;

	@Column(name = "utm_name", nullable = true)
	private String utmName;

	@Column(name = "user_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private UserType userType = UserType.PARTNER;

	@Column(name = "repayment_method", nullable = true)
	@Enumerated(EnumType.STRING)
	private RepaymentMethod repaymentMethod = RepaymentMethod.PI;

	@Column(name = "loan_amount_calculation", nullable = true)
	private double loanAmountCalculation = 0.0;

	@Column(name = "duration", nullable = true)
	private Integer duration;

	@Column(name = "first_cibil_score", nullable = true)
	private double firstCibilScore = 0.0;

	@Column(name = "second_cibil_score", nullable = true)
	private double secondCibilScore = 0.0;

	@Column(name = "third_cibil_score", nullable = true)
	private double thirdCibilScore = 0.0;

	@Column(name = "fourth_cibil_score", nullable = true)
	private double fourthCibilScore = 0.0;

	@Column(name = "fifth_cibil_score", nullable = true)
	private double fifthCibilScore = 0.0;

	@Column(name = "type", nullable = true)
	@Enumerated(EnumType.STRING)
	private Type type = Type.SALARIED;

	@Column(name = "created_on", nullable = true)
	private Date createdOn;

	@Column(name = "modified_on", nullable = true)
	private Date modifiedOn;

	@Column(name = "email", nullable = true)
	private String email;

	@Column(name = "mobile_number", nullable = true)
	private String mobileNumber;

	@Column(name = "password", nullable = true)
	private String password;

	@Column(name = "salt", nullable = true, updatable = true)
	private String salt;

	@Column(name = "point_of_contact", nullable = true)
	private String pointOfContact;

	@Column(name = "point_of_contact_mobilenumber", nullable = true)
	private String pointOfContactMobileNumbers;

	@Column(name = "partner_name", nullable = true)
	private String partnerName;

	@Column(name = "point_of_contact_name", nullable = true)
	private String pointOfContactName;

	@Column(name = "gmail_contacts", nullable = true)
	private String gmailContacts;

	@Column(name = "mobile_number_verified", nullable = false)
	private boolean mobileNumberVerified = false;

	@Column(name = "email_verified", nullable = false)
	private Boolean emailVerified = false;

	@Column(name = "email_otp_session", nullable = false)
	private String emailOtpSession;

	@Column(name = "account_number", nullable = true)
	private String accountNumber = "";

	@Column(name = "bank_name", nullable = true)
	private String bankName = "";

	@Column(name = "branch_name", nullable = true)
	private String branchName = "";

	@Column(name = "ifsc_code", nullable = true)
	private String ifscCode = "";

	@Column(name = "user_name", nullable = true)
	private String userName = "";

	@Column(name = "additional_fields_to_user", nullable = true)
	@Enumerated(EnumType.STRING)
	private AdditionalFieldsToUser additionalFieldsToUser = AdditionalFieldsToUser.NO;

	@Column(name = "lender_processing_fee", nullable = true)
	private double lenderProcessingFee = 0.0;

	@Column(name = "borrower_processing_fee", nullable = true)
	private double borrowerProcessingFee = 0.0;
	
	@Column(name="referral_check",nullable = true)
	@Enumerated(EnumType.STRING)
	private ReferralCheck referralCheck=ReferralCheck.NO;


	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getUtmName() {
		return utmName;
	}

	public void setUtmName(String utmName) {
		this.utmName = utmName;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public RepaymentMethod getRepaymentMethod() {
		return repaymentMethod;
	}

	public void setRepaymentMethod(RepaymentMethod repaymentMethod) {
		this.repaymentMethod = repaymentMethod;
	}

	public double getLoanAmountCalculation() {
		return loanAmountCalculation;
	}

	public void setLoanAmountCalculation(double loanAmountCalculation) {
		this.loanAmountCalculation = loanAmountCalculation;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public double getFirstCibilScore() {
		return firstCibilScore;
	}

	public void setFirstCibilScore(double firstCibilScore) {
		this.firstCibilScore = firstCibilScore;
	}

	public double getSecondCibilScore() {
		return secondCibilScore;
	}

	public void setSecondCibilScore(double secondCibilScore) {
		this.secondCibilScore = secondCibilScore;
	}

	public double getThirdCibilScore() {
		return thirdCibilScore;
	}

	public void setThirdCibilScore(double thirdCibilScore) {
		this.thirdCibilScore = thirdCibilScore;
	}

	public double getFourthCibilScore() {
		return fourthCibilScore;
	}

	public void setFourthCibilScore(double fourthCibilScore) {
		this.fourthCibilScore = fourthCibilScore;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public double getFifthCibilScore() {
		return fifthCibilScore;
	}

	public void setFifthCibilScore(double fifthCibilScore) {
		this.fifthCibilScore = fifthCibilScore;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getPointOfContact() {
		return pointOfContact;
	}

	public void setPointOfContact(String pointOfContact) {
		this.pointOfContact = pointOfContact;
	}

	public String getPointOfContactMobileNumbers() {
		return pointOfContactMobileNumbers;
	}

	public void setPointOfContactMobileNumbers(String pointOfContactMobileNumbers) {
		this.pointOfContactMobileNumbers = pointOfContactMobileNumbers;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPointOfContactName() {
		return pointOfContactName;
	}

	public void setPointOfContactName(String pointOfContactName) {
		this.pointOfContactName = pointOfContactName;
	}

	public String getGmailContacts() {
		return gmailContacts;
	}

	public void setGmailContacts(String gmailContacts) {
		this.gmailContacts = gmailContacts;
	}

	public boolean isMobileNumberVerified() {
		return mobileNumberVerified;
	}

	public void setMobileNumberVerified(boolean mobileNumberVerified) {
		this.mobileNumberVerified = mobileNumberVerified;
	}

	public Boolean isEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(Boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	public String getEmailOtpSession() {
		return emailOtpSession;
	}

	public void setEmailOtpSession(String emailOtpSession) {
		this.emailOtpSession = emailOtpSession;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public AdditionalFieldsToUser getAdditionalFieldsToUser() {
		return additionalFieldsToUser;
	}

	public void setAdditionalFieldsToUser(AdditionalFieldsToUser additionalFieldsToUser) {
		this.additionalFieldsToUser = additionalFieldsToUser;
	}

	public double getLenderProcessingFee() {
		return lenderProcessingFee;
	}

	public void setLenderProcessingFee(double lenderProcessingFee) {
		this.lenderProcessingFee = lenderProcessingFee;
	}

	public double getBorrowerProcessingFee() {
		return borrowerProcessingFee;
	}

	public void setBorrowerProcessingFee(double borrowerProcessingFee) {
		this.borrowerProcessingFee = borrowerProcessingFee;
	}

	public ReferralCheck getReferralCheck() {
		return referralCheck;
	}

	public void setReferralCheck(ReferralCheck referralCheck) {
		this.referralCheck = referralCheck;
	}

	public Boolean getEmailVerified() {
		return emailVerified;
	}

}
