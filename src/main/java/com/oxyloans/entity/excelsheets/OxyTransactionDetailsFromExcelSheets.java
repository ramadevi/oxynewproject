package com.oxyloans.entity.excelsheets;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name = "oxy_transaction_details_from_excelsheets")
@Entity
public class OxyTransactionDetailsFromExcelSheets {

	public static enum DebitedAccountType {
		IDFCREPAYMENT, ICICIREPAYMENT, ICICIDISBURSMENT
	}

	public static enum DebitedTowords {
		LENDERINTEREST, LENDEREMI, LENDERPRINCIPAL, LENDERWITHDRAW, DISBURSEDLOANS, LENDERRELEND, LENDERPOOLINGINTEREST,
		IDFCPAYMENTS, PRINCIPALINTEREST, WITHDRAWALINTEREST, POOLINGLENDERS
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "debited_account_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private DebitedAccountType debitedAccountType = DebitedAccountType.IDFCREPAYMENT;

	@Column(name = "debited_towords", nullable = true)
	@Enumerated(EnumType.STRING)
	private DebitedTowords debitedTowords = DebitedTowords.LENDEREMI;

	@Column(name = "transferred_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date transferredDate;

	@Column(name = "remarks", nullable = true)
	private String remarks;

	@Column(name = "file_name", nullable = true)
	private String fileName;

	@Column(name = "updated_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedOn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DebitedAccountType getDebitedAccountType() {
		return debitedAccountType;
	}

	public void setDebitedAccountType(DebitedAccountType debitedAccountType) {
		this.debitedAccountType = debitedAccountType;
	}

	public DebitedTowords getDebitedTowords() {
		return debitedTowords;
	}

	public void setDebitedTowords(DebitedTowords debitedTowords) {
		this.debitedTowords = debitedTowords;
	}

	public Date getTransferredDate() {
		return transferredDate;
	}

	public void setTransferredDate(Date transferredDate) {
		this.transferredDate = transferredDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

}
