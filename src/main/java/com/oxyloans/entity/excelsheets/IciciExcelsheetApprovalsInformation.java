package com.oxyloans.entity.excelsheets;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "icici_excelsheet_approvals_information")
public class IciciExcelsheetApprovalsInformation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId;

	@Column(name = "month_name_and_year", nullable = true)
	private String monthNameAndYear;

	@Column(name = "returns_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date returnsDate;

	@Column(name = "actual_payment_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date actualPaymentDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Date getReturnsDate() {
		return returnsDate;
	}

	public void setReturnsDate(Date returnsDate) {
		this.returnsDate = returnsDate;
	}

	public String getMonthNameAndYear() {
		return monthNameAndYear;
	}

	public void setMonthNameAndYear(String monthNameAndYear) {
		this.monthNameAndYear = monthNameAndYear;
	}

	public Date getActualPaymentDate() {
		return actualPaymentDate;
	}

	public void setActualPaymentDate(Date actualPaymentDate) {
		this.actualPaymentDate = actualPaymentDate;
	}

}
