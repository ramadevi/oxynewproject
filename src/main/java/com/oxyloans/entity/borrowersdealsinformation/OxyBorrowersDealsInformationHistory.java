package com.oxyloans.entity.borrowersdealsinformation;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name = "oxy_borrowers_deals_information_history")
public class OxyBorrowersDealsInformationHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId;

	@Column(name = "deal_name", nullable = true)
	private String dealName;

	@Column(name = "borrower_name", nullable = true)
	private String borrowerName;

	@Column(name = "created_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	@Column(name = "deal_amount", nullable = true)
	private Double dealAmount = 0.0;

	@Column(name = "duration", nullable = true)
	private Integer duration = 0;
	@Column(name = "funds_acceptance_start_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date fundsAcceptanceStartDate;
	@Column(name = "funds_acceptance_end_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date fundsAcceptanceEndDate;
	@Column(name = "borrower_rateofinterest", nullable = true)
	private Double borrowerRateofinterest = 0.0;

	@Column(name = "fee_collected_from_borrower", nullable = true)
	private Double feeCollectedFromBorrower = 0.0;
	
	@Column(name = "fee_rateofinterest_borrower", nullable = true)
	private Double feeROIForBorrower = 0.0;
	
	@Column(name = "roi_for_withdrawal", nullable = true)
	private Double roiForWithdrawal = 0.0;
	
	@Column(name = "minimum_paticipation_amount", nullable = true)
	private Double minimumPaticipationAmount = 0.0;
	
	
	@Column(name = "life_time_waiver", nullable = true)
	private Boolean lifeTimeWaiver = false;
	@Column(name = "life_time_waiver_limit", nullable = true)
	private Double lifeTimeWaiverLimit;
	


	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "deal_id", nullable = false, insertable = false, updatable = false)
	private OxyBorrowersDealsInformation oxyBorrowersDealsInformation;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public Double getDealAmount() {
		return dealAmount;
	}

	public void setDealAmount(Double dealAmount) {
		this.dealAmount = dealAmount;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public OxyBorrowersDealsInformation getOxyBorrowersDealsInformation() {
		return oxyBorrowersDealsInformation;
	}

	public void setOxyBorrowersDealsInformation(OxyBorrowersDealsInformation oxyBorrowersDealsInformation) {
		this.oxyBorrowersDealsInformation = oxyBorrowersDealsInformation;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Date getFundsAcceptanceStartDate() {
		return fundsAcceptanceStartDate;
	}

	public void setFundsAcceptanceStartDate(Date fundsAcceptanceStartDate) {
		this.fundsAcceptanceStartDate = fundsAcceptanceStartDate;
	}

	public Date getFundsAcceptanceEndDate() {
		return fundsAcceptanceEndDate;
	}

	public void setFundsAcceptanceEndDate(Date fundsAcceptanceEndDate) {
		this.fundsAcceptanceEndDate = fundsAcceptanceEndDate;
	}

	public Double getBorrowerRateofinterest() {
		return borrowerRateofinterest;
	}

	public void setBorrowerRateofinterest(Double borrowerRateofinterest) {
		this.borrowerRateofinterest = borrowerRateofinterest;
	}

	public Double getFeeCollectedFromBorrower() {
		return feeCollectedFromBorrower;
	}

	public void setFeeCollectedFromBorrower(Double feeCollectedFromBorrower) {
		this.feeCollectedFromBorrower = feeCollectedFromBorrower;
	}

	public Double getFeeROIForBorrower() {
		return feeROIForBorrower;
	}

	public void setFeeROIForBorrower(Double feeROIForBorrower) {
		this.feeROIForBorrower = feeROIForBorrower;
	}

	public Double getRoiForWithdrawal() {
		return roiForWithdrawal;
	}

	public void setRoiForWithdrawal(Double roiForWithdrawal) {
		this.roiForWithdrawal = roiForWithdrawal;
	}

	public Double getMinimumPaticipationAmount() {
		return minimumPaticipationAmount;
	}

	public void setMinimumPaticipationAmount(Double minimumPaticipationAmount) {
		this.minimumPaticipationAmount = minimumPaticipationAmount;
	}

	public Boolean getLifeTimeWaiver() {
		return lifeTimeWaiver;
	}

	public void setLifeTimeWaiver(Boolean lifeTimeWaiver) {
		this.lifeTimeWaiver = lifeTimeWaiver;
	}

	public Double getLifeTimeWaiverLimit() {
		return lifeTimeWaiverLimit;
	}

	public void setLifeTimeWaiverLimit(Double lifeTimeWaiverLimit) {
		this.lifeTimeWaiverLimit = lifeTimeWaiverLimit;
	}




}
