package com.oxyloans.entity.borrowersdealsinformation;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "oxy_lenders_accepted_deals")
public class OxyLendersAcceptedDeals {

	public static enum FeeStatus {
		PENDING, COMPLETED
	}

	public static enum LenderReturnsType {
		MONTHLY, QUARTELY, HALFLY, YEARLY, ENDOFTHEDEAL
	}

	public static enum ParticipationState {
		PARTICIPATED, NOTPARTICIPATED
	}

	public static enum PrincipalReturningAccountType {
		WALLET, BANKACCOUNT
	}

	public static enum PaticipationState {
		LENDERPATICIPATED, AUTOLEND
	}

	public static enum LenderParticipationFrom {
		WEB, MOBILE
	}

	@Column(name = "principal_returning_account_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private PrincipalReturningAccountType principalReturningAccountType = PrincipalReturningAccountType.BANKACCOUNT;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = true)
	private Integer userId;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId;

	@Column(name = "group_id", nullable = true)
	private Integer groupId;

	@Column(name = "participated_amount", nullable = true)
	private Double participatedAmount = 0.0;

	@Column(name = "processing_fee", nullable = true)
	private Double processingFee = 0.0;

	@Column(name = "fee_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private FeeStatus feeStatus = FeeStatus.COMPLETED;

	@Column(name = "lender_returns_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private LenderReturnsType lenderReturnsType = LenderReturnsType.MONTHLY;

	@Column(name = "rateofinterest", nullable = true)
	private Double rateofinterest = 0.0;

	@Column(name = "received_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date receivedOn;

	@Column(name = "lender_participation_from", nullable = true)
	@Enumerated(EnumType.STRING)
	private LenderParticipationFrom lenderParticipationFrom = LenderParticipationFrom.WEB;

	@Column(name = "participation_state", nullable = false)
	@Enumerated(EnumType.STRING)
	private ParticipationState participationState = ParticipationState.NOTPARTICIPATED;

	@Column(name = "lender_fee_id", nullable = true)
	private Integer lenderFeeId;

	@Column(name = "shares", nullable = true)
	private Integer shares;

	@Column(name = "paticipation_state", nullable = true)
	@Enumerated(EnumType.STRING)
	private PaticipationState paticipationState = PaticipationState.LENDERPATICIPATED;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Double getParticipatedAmount() {
		return participatedAmount;
	}

	public void setParticipatedAmount(Double participatedAmount) {
		this.participatedAmount = participatedAmount;
	}

	public Double getProcessingFee() {
		return processingFee;
	}

	public void setProcessingFee(Double processingFee) {
		this.processingFee = processingFee;
	}

	public FeeStatus getFeeStatus() {
		return feeStatus;
	}

	public void setFeeStatus(FeeStatus feeStatus) {
		this.feeStatus = feeStatus;
	}

	public Date getReceivedOn() {
		return receivedOn;
	}

	public void setReceivedOn(Date receivedOn) {
		this.receivedOn = receivedOn;
	}

	public LenderReturnsType getLenderReturnsType() {
		return lenderReturnsType;
	}

	public void setLenderReturnsType(LenderReturnsType lenderReturnsType) {
		this.lenderReturnsType = lenderReturnsType;
	}

	public Double getRateofinterest() {
		return rateofinterest;
	}

	public void setRateofinterest(Double rateofinterest) {
		this.rateofinterest = rateofinterest;
	}

	public ParticipationState getParticipationState() {
		return participationState;
	}

	public void setParticipationState(ParticipationState participationState) {
		this.participationState = participationState;
	}

	public Integer getLenderFeeId() {
		return lenderFeeId;
	}

	public void setLenderFeeId(Integer lenderFeeId) {
		this.lenderFeeId = lenderFeeId;
	}

	public PrincipalReturningAccountType getPrincipalReturningAccountType() {
		return principalReturningAccountType;
	}

	public void setPrincipalReturningAccountType(PrincipalReturningAccountType principalReturningAccountType) {
		this.principalReturningAccountType = principalReturningAccountType;
	}

	public Integer getShares() {
		return shares;
	}

	public void setShares(Integer shares) {
		this.shares = shares;
	}

	public PaticipationState getPaticipationState() {
		return paticipationState;
	}

	public void setPaticipationState(PaticipationState paticipationState) {
		this.paticipationState = paticipationState;
	}

	public LenderParticipationFrom getLenderParticipationFrom() {
		return lenderParticipationFrom;
	}

	public void setLenderParticipationFrom(LenderParticipationFrom lenderParticipationFrom) {
		this.lenderParticipationFrom = lenderParticipationFrom;
	}

}
