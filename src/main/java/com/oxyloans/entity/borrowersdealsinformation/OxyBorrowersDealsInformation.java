package com.oxyloans.entity.borrowersdealsinformation;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "oxy_borrowers_deals_information")
public class OxyBorrowersDealsInformation {

	public static enum DealStatus {
		NOTATACHIEVED, ACHIEVED
	}

	public static enum DealType {
		NORMAL, EQUITY, ESCROW, SALARIED, PERSONAL, TEST
	}

	public static enum DealParticipationStatus {
		NOTATACHIEVED, ACHIEVED, OPENINFUTURE
	}

	public static enum BorrowerClosingStatus {
		NOTYETCLOSED, CLOSED, UNUSED
	}

	public static enum AnyTimeWithdrawal {
		YES, NO
	}

	public static enum ParticipationLenderType {
		NEW, ANY
	}

	public static enum DealOpenStatus {
		NOW, FUTURE
	}

	public static enum DealSubtype {
		STUDENT, ASSET
	}

	public static enum AgreementsStatus {
		NOTYETGENERATED, GENERATED
	}

	public static enum FeeStatusToParticipate {
		MANDATORY, OPTIONAL
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "deal_name", nullable = true)
	private String dealName;

	@Column(name = "borrower_name", nullable = true)
	private String borrowerName;

	@Column(name = "deal_amount", nullable = true)
	private Double dealAmount = 0.0;

	@Column(name = "funds_acceptance_start_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date fundsAcceptanceStartDate;

	@Column(name = "funds_acceptance_end_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date fundsAcceptanceEndDate;

	@Column(name = "received_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date receivedOn;

	@Column(name = "borrower_rateofinterest", nullable = true)
	private Double borrowerRateofinterest = 0.0;

	@Column(name = "deal_link", nullable = true)
	private String dealLink;

	@Column(name = "duration", nullable = true)
	private Integer duration = 0;

	@Column(name = "whatapp_reponse_link", nullable = true)
	private String whatappReponseLink;

	@Column(name = "link_sent_to_lenders_group", nullable = true)
	private String linkSentToLendersGroup;

	@Column(name = "loan_active_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date loanActiveDate;

	@Column(name = "whatapp_message_to_lenders", nullable = true)
	private String whatappMessageToLenders;

	@Column(name = "paticipation_limit_to_lenders", nullable = true)
	private Double paticipationLimitToLenders = 0.0;

	@Column(name = "whatapp_chatid", nullable = true)
	private String whatappChatid;

	@Column(name = "deal_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private DealStatus dealStatus = DealStatus.NOTATACHIEVED;

	@Column(name = "lenders_commitment_amount", nullable = true)
	private String lendersCommitmentAmount;

	@Column(name = "deal_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private DealType dealType = DealType.NORMAL;

	@Column(name = "fee_collected_from_borrower", nullable = true)
	private Double feeCollectedFromBorrower = 0.0;

	@Column(name = "fee_rateofinterest_borrower", nullable = true)
	private Double feeROIForBorrower = 0.0;

	@Column(name = "deal_paticipation_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private DealParticipationStatus dealParticipationStatus = DealParticipationStatus.NOTATACHIEVED;

	@Column(name = "borrower_closing_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private BorrowerClosingStatus borrowerclosingstatus = BorrowerClosingStatus.NOTYETCLOSED;

	@Column(name = "borrower_closed_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date borrowerClosedDate;

	@Column(name = "any_time_withdrawal", nullable = true)
	@Enumerated(EnumType.STRING)
	private AnyTimeWithdrawal anyTimeWithdrawal = AnyTimeWithdrawal.NO;

	@Column(name = "deal_open_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private DealOpenStatus dealOpenStatus = DealOpenStatus.NOW;

	@Column(name = "roi_for_withdrawal", nullable = true)
	private Double roiForWithdrawal = 0.0;

	@Column(name = "participation_lender_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private ParticipationLenderType participationLenderType = ParticipationLenderType.ANY;

	@Column(name = "oxy_loan_request_id", nullable = true)
	private Integer oxyLoanRequestId = 0;

	@Column(name = "borrowers_ids_mapped_to_deal", nullable = true)
	private String borrowersIdsMappedToDeal = "";

	@Column(name = "enach_status", nullable = true)
	private Boolean enachStatus = false;

	@Column(name = "minimum_paticipation_amount", nullable = true)
	private Double minimumPaticipationAmount = 0.0;

	@Column(name = "deal_subtype", nullable = true)
	@Enumerated(EnumType.STRING)
	private DealSubtype dealSubType = DealSubtype.STUDENT;

	@Column(name = "agreements_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private AgreementsStatus agreementsStatus = AgreementsStatus.NOTYETGENERATED;

	@Column(name = "borrowers_amount_mapped_todeal", nullable = true)
	private String borrowersAmountMappedTodeal;

	@Column(name = "deal_achieved_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dealAcheivedDate;

	@Column(name = "deal_future_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date dealFutureDate;

	@Column(name = "deal_launch_hour", nullable = true)
	private String dealLaunchHoure;

	@Column(name = "new_deal_launch_hour", nullable = true)
	private String newDealLunchHoure;

	@Column(name = "life_time_waiver", nullable = true)
	private Boolean lifeTimeWaiver = false;

	@Column(name = "life_time_waiver_limit", nullable = true)
	private Double lifeTimeWaiverLimit;

	@Column(name = "modified_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	@Column(name = "fee_status_to_participate", nullable = true)
	@Enumerated(EnumType.STRING)
	private FeeStatusToParticipate feeStatusToParticipate = FeeStatusToParticipate.MANDATORY;

	@Column(name = "emi_end_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date emiEndDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public Double getDealAmount() {
		return dealAmount;
	}

	public void setDealAmount(Double dealAmount) {
		this.dealAmount = dealAmount;
	}

	public Date getFundsAcceptanceStartDate() {
		return fundsAcceptanceStartDate;
	}

	public void setFundsAcceptanceStartDate(Date fundsAcceptanceStartDate) {
		this.fundsAcceptanceStartDate = fundsAcceptanceStartDate;
	}

	public Date getFundsAcceptanceEndDate() {
		return fundsAcceptanceEndDate;
	}

	public void setFundsAcceptanceEndDate(Date fundsAcceptanceEndDate) {
		this.fundsAcceptanceEndDate = fundsAcceptanceEndDate;
	}

	public Date getReceivedOn() {
		return receivedOn;
	}

	public void setReceivedOn(Date receivedOn) {
		this.receivedOn = receivedOn;
	}

	public Double getBorrowerRateofinterest() {
		return borrowerRateofinterest;
	}

	public void setBorrowerRateofinterest(Double borrowerRateofinterest) {
		this.borrowerRateofinterest = borrowerRateofinterest;
	}

	public String getDealLink() {
		return dealLink;
	}

	public void setDealLink(String dealLink) {
		this.dealLink = dealLink;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getWhatappReponseLink() {
		return whatappReponseLink;
	}

	public void setWhatappReponseLink(String whatappReponseLink) {
		this.whatappReponseLink = whatappReponseLink;
	}

	public String getLinkSentToLendersGroup() {
		return linkSentToLendersGroup;
	}

	public void setLinkSentToLendersGroup(String linkSentToLendersGroup) {
		this.linkSentToLendersGroup = linkSentToLendersGroup;
	}

	public Date getLoanActiveDate() {
		return loanActiveDate;
	}

	public void setLoanActiveDate(Date loanActiveDate) {
		this.loanActiveDate = loanActiveDate;
	}

	public String getWhatappMessageToLenders() {
		return whatappMessageToLenders;
	}

	public void setWhatappMessageToLenders(String whatappMessageToLenders) {
		this.whatappMessageToLenders = whatappMessageToLenders;
	}

	public Double getPaticipationLimitToLenders() {
		return paticipationLimitToLenders;
	}

	public void setPaticipationLimitToLenders(Double paticipationLimitToLenders) {
		this.paticipationLimitToLenders = paticipationLimitToLenders;
	}

	public String getWhatappChatid() {
		return whatappChatid;
	}

	public void setWhatappChatid(String whatappChatid) {
		this.whatappChatid = whatappChatid;
	}

	public DealStatus getDealStatus() {
		return dealStatus;
	}

	public void setDealStatus(DealStatus dealStatus) {
		this.dealStatus = dealStatus;
	}

	public String getLendersCommitmentAmount() {
		return lendersCommitmentAmount;
	}

	public void setLendersCommitmentAmount(String lendersCommitmentAmount) {
		this.lendersCommitmentAmount = lendersCommitmentAmount;
	}

	public DealType getDealType() {
		return dealType;
	}

	public void setDealType(DealType dealType) {
		this.dealType = dealType;
	}

	public Double getFeeCollectedFromBorrower() {
		return feeCollectedFromBorrower;
	}

	public void setFeeCollectedFromBorrower(Double feeCollectedFromBorrower) {
		this.feeCollectedFromBorrower = feeCollectedFromBorrower;
	}

	public Double getFeeROIForBorrower() {
		return feeROIForBorrower;
	}

	public void setFeeROIForBorrower(Double feeROIForBorrower) {
		this.feeROIForBorrower = feeROIForBorrower;
	}

	public DealParticipationStatus getDealParticipationStatus() {
		return dealParticipationStatus;
	}

	public void setDealParticipationStatus(DealParticipationStatus dealParticipationStatus) {
		this.dealParticipationStatus = dealParticipationStatus;
	}

	public BorrowerClosingStatus getBorrowerclosingstatus() {
		return borrowerclosingstatus;
	}

	public void setBorrowerclosingstatus(BorrowerClosingStatus borrowerclosingstatus) {
		this.borrowerclosingstatus = borrowerclosingstatus;
	}

	public Date getBorrowerClosedDate() {
		return borrowerClosedDate;
	}

	public void setBorrowerClosedDate(Date borrowerClosedDate) {
		this.borrowerClosedDate = borrowerClosedDate;
	}

	public AnyTimeWithdrawal getAnyTimeWithdrawal() {
		return anyTimeWithdrawal;
	}

	public void setAnyTimeWithdrawal(AnyTimeWithdrawal anyTimeWithdrawal) {
		this.anyTimeWithdrawal = anyTimeWithdrawal;
	}

	public Double getRoiForWithdrawal() {
		return roiForWithdrawal;
	}

	public void setRoiForWithdrawal(Double roiForWithdrawal) {
		this.roiForWithdrawal = roiForWithdrawal;
	}

	public ParticipationLenderType getParticipationLenderType() {
		return participationLenderType;
	}

	public void setParticipationLenderType(ParticipationLenderType participationLenderType) {
		this.participationLenderType = participationLenderType;
	}

	public Integer getOxyLoanRequestId() {
		return oxyLoanRequestId;
	}

	public void setOxyLoanRequestId(Integer oxyLoanRequestId) {
		this.oxyLoanRequestId = oxyLoanRequestId;
	}

	public String getBorrowersIdsMappedToDeal() {
		return borrowersIdsMappedToDeal;
	}

	public void setBorrowersIdsMappedToDeal(String borrowersIdsMappedToDeal) {
		this.borrowersIdsMappedToDeal = borrowersIdsMappedToDeal;
	}

	public Boolean getEnachStatus() {
		return enachStatus;
	}

	public void setEnachStatus(Boolean enachStatus) {
		this.enachStatus = enachStatus;
	}

	public Double getMinimumPaticipationAmount() {
		return minimumPaticipationAmount;
	}

	public void setMinimumPaticipationAmount(Double minimumPaticipationAmount) {
		this.minimumPaticipationAmount = minimumPaticipationAmount;
	}

	public DealSubtype getDealSubType() {
		return dealSubType;
	}

	public void setDealSubType(DealSubtype dealSubType) {
		this.dealSubType = dealSubType;
	}

	public AgreementsStatus getAgreementsStatus() {
		return agreementsStatus;
	}

	public void setAgreementsStatus(AgreementsStatus agreementsStatus) {
		this.agreementsStatus = agreementsStatus;
	}

	public String getBorrowersAmountMappedTodeal() {
		return borrowersAmountMappedTodeal;
	}

	public void setBorrowersAmountMappedTodeal(String borrowersAmountMappedTodeal) {
		this.borrowersAmountMappedTodeal = borrowersAmountMappedTodeal;
	}

	public Date getDealAcheivedDate() {
		return dealAcheivedDate;
	}

	public void setDealAcheivedDate(Date dealAcheivedDate) {
		this.dealAcheivedDate = dealAcheivedDate;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public FeeStatusToParticipate getFeeStatusToParticipate() {
		return feeStatusToParticipate;
	}

	public void setFeeStatusToParticipate(FeeStatusToParticipate feeStatusToParticipate) {
		this.feeStatusToParticipate = feeStatusToParticipate;
	}

	public Date getEmiEndDate() {
		return emiEndDate;
	}

	public void setEmiEndDate(Date emiEndDate) {
		this.emiEndDate = emiEndDate;
	}

	public DealOpenStatus getDealOpenStatus() {
		return dealOpenStatus;
	}

	public void setDealOpenStatus(DealOpenStatus dealOpenStatus) {
		this.dealOpenStatus = dealOpenStatus;
	}

	public Date getDealFutureDate() {
		return dealFutureDate;
	}

	public void setDealFutureDate(Date dealFutureDate) {
		this.dealFutureDate = dealFutureDate;
	}

	public String getDealLaunchHoure() {
		return dealLaunchHoure;
	}

	public void setDealLaunchHoure(String dealLaunchHoure) {
		this.dealLaunchHoure = dealLaunchHoure;
	}

	public String getNewDealLunchHoure() {
		return newDealLunchHoure;
	}

	public void setNewDealLunchHoure(String newDealLunchHoure) {
		this.newDealLunchHoure = newDealLunchHoure;
	}

	public Boolean getLifeTimeWaiver() {
		return lifeTimeWaiver;
	}

	public void setLifeTimeWaiver(Boolean lifeTimeWaiver) {
		this.lifeTimeWaiver = lifeTimeWaiver;
	}

	public Double getLifeTimeWaiverLimit() {
		return lifeTimeWaiverLimit;
	}

	public void setLifeTimeWaiverLimit(Double lifeTimeWaiverLimit) {
		this.lifeTimeWaiverLimit = lifeTimeWaiverLimit;
	}

}
