package com.oxyloans.entity.borrowersdealsinformation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "oxy_deals_rateofinterest_to_lendersgroup")
public class OxyDealsRateofinterestToLendersgroup {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "deal_id", nullable = true)
	private Integer dealId = 0;

	@Column(name = "group_id", nullable = true)
	private Integer groupId = 0;

	@Column(name = "monthly_interest", nullable = true)
	private Double monthlyInterest = 0.0;

	@Column(name = "quartely_interest", nullable = true)
	private Double quartelyInterest = 0.0;

	@Column(name = "half_interest", nullable = true)
	private Double halfInterest = 0.0;

	@Column(name = "yearly_interest", nullable = true)
	private Double yearlyInterest = 0.0;

	@Column(name = "endofthedealInterest", nullable = true)
	private Double endofthedealInterest = 0.0;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Double getMonthlyInterest() {
		return monthlyInterest;
	}

	public void setMonthlyInterest(Double monthlyInterest) {
		this.monthlyInterest = monthlyInterest;
	}

	public Double getQuartelyInterest() {
		return quartelyInterest;
	}

	public void setQuartelyInterest(Double quartelyInterest) {
		this.quartelyInterest = quartelyInterest;
	}

	public Double getHalfInterest() {
		return halfInterest;
	}

	public void setHalfInterest(Double halfInterest) {
		this.halfInterest = halfInterest;
	}

	public Double getYearlyInterest() {
		return yearlyInterest;
	}

	public void setYearlyInterest(Double yearlyInterest) {
		this.yearlyInterest = yearlyInterest;
	}

	public Double getEndofthedealInterest() {
		return endofthedealInterest;
	}

	public void setEndofthedealInterest(Double endofthedealInterest) {
		this.endofthedealInterest = endofthedealInterest;
	}

}
