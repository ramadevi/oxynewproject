package com.oxyloans.entity.borrowersdealsinformation;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lenders_paticipation_updation")
public class LendersPaticipationUpdation {
	public static enum Type {
		ADD, UPDATE
	}

	public static enum DealType {
		NORMAL, EQUITY, ESCROW, SALARIED, SELFEMPLOYED, PERSONAL, TEST
	}

	public static enum FeeStatus {
		PENDING, COMPLETED
	}

	public static enum LenderParticipationFrom {
		WEB, MOBILE
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = true)
	private Integer userId;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId;

	@Column(name = "updation_amount", nullable = true)
	private Double updationAmount;

	@Column(name = "type", nullable = true)
	@Enumerated(EnumType.STRING)
	private Type type = Type.ADD;

	@Column(name = "updated_on", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date updatedOn;

	@Column(name = "deal_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private DealType dealType = DealType.NORMAL;

	@Column(name = "lender_fee_id", nullable = true)
	private Integer lenderFeeId;

	@Column(name = "processing_fee", nullable = true)
	private Double processingFee = 0.0;

	@Column(name = "created_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	@Column(name = "fee_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private FeeStatus feeStatus = FeeStatus.COMPLETED;

	@Column(name = "lender_participation_from", nullable = true)
	@Enumerated(EnumType.STRING)
	private LenderParticipationFrom lenderParticipationFrom = LenderParticipationFrom.WEB;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Double getUpdationAmount() {
		return updationAmount;
	}

	public void setUpdationAmount(Double updationAmount) {
		this.updationAmount = updationAmount;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public DealType getDealType() {
		return dealType;
	}

	public void setDealType(DealType dealType) {
		this.dealType = dealType;
	}

	public Integer getLenderFeeId() {
		return lenderFeeId;
	}

	public void setLenderFeeId(Integer lenderFeeId) {
		this.lenderFeeId = lenderFeeId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public FeeStatus getFeeStatus() {
		return feeStatus;
	}

	public void setFeeStatus(FeeStatus feeStatus) {
		this.feeStatus = feeStatus;
	}

	public Double getProcessingFee() {
		return processingFee;
	}

	public void setProcessingFee(Double processingFee) {
		this.processingFee = processingFee;
	}

	public LenderParticipationFrom getLenderParticipationFrom() {
		return lenderParticipationFrom;
	}

	public void setLenderParticipationFrom(LenderParticipationFrom lenderParticipationFrom) {
		this.lenderParticipationFrom = lenderParticipationFrom;
	}

}
