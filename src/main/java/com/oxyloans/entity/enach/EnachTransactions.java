package com.oxyloans.entity.enach;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.IEntity;

@Entity
@Table(name = "enach_transactions")
public class EnachTransactions implements IEntity {
	
	public static enum Status {
		SCHEDULED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "transaction_id", nullable = false, unique = true)
	private String transactionId;

	@Column(name = "mandate_id", nullable = false)
	private Integer mandateId;

	@Column(name = "initiation_date", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date initiationDate = new Date();

	@Column(name = "scheduled_date", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date scheduledDate;

	@Column(name = "scheduled_amount", nullable = false)
	private Double scheduledAmount;

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status = Status.SCHEDULED;

	@Column(name = "modified_on", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn = new Date();

	@Column(name = "executed_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date executedDate;

	public Integer getId() {
		return id;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Integer getMandateId() {
		return mandateId;
	}

	public void setMandateId(Integer mandateId) {
		this.mandateId = mandateId;
	}

	public Date getInitiationDate() {
		return initiationDate;
	}

	public void setInitiationDate(Date initiationDate) {
		this.initiationDate = initiationDate;
	}

	public Date getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	public Double getScheduledAmount() {
		return scheduledAmount;
	}

	public void setScheduledAmount(Double scheduledAmount) {
		this.scheduledAmount = scheduledAmount;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Date getExecutedDate() {
		return executedDate;
	}

	public void setExecutedDate(Date executedDate) {
		this.executedDate = executedDate;
	}
}
