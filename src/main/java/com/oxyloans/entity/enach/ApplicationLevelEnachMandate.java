package com.oxyloans.entity.enach;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.IEntity;

@Entity
@Table(name = "applicationlevel_enach_mandate")
public class ApplicationLevelEnachMandate implements IEntity{

	
	public static enum Frequency {   
        MNTH,DAY,DAIL
    }

    public static enum AmountType {
        M,D
    }

    public static enum MandateStatus {
        INITIATED, SUCCESS, FAILED,ADMINREJECTED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column(name = "application_id", nullable = true)
	private Integer applicationId;
    
    @Column(name = "is_principal_or_interest", nullable = true)
    private String isPrincialOrInterest;

    @Column(name = "transaction_id", nullable = true, unique = true)
    private String mandateTransactionId;

    @Column(name = "mandate_type", nullable = true)
    private String mandateType = "NETBANKING";

    @Column(name = "max_amount", nullable = true)
    private double maxAmount;

    @Column(name = "amount_type", nullable = true)
    @Enumerated(EnumType.STRING)
    private AmountType amountType = AmountType.M;

    @Column(name = "frequency", nullable = true)
    @Enumerated(EnumType.STRING)
    private Frequency frequency = Frequency.MNTH;

    @Column(name = "debit_start_date", nullable = true)
    @Temporal(TemporalType.DATE)
    private Date debitStartDate;

    @Column(name = "debit_end_date", nullable = true)
    @Temporal(TemporalType.DATE)
    private Date debitEndDate;

    @Column(name = "mandate_status", nullable = true)
    @Enumerated(EnumType.STRING)
    private MandateStatus mandateStatus;

    @Column(name = "modified_on", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn = new Date();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public String getMandateTransactionId() {
		return mandateTransactionId;
	}

	public void setMandateTransactionId(String mandateTransactionId) {
		this.mandateTransactionId = mandateTransactionId;
	}

	public String getMandateType() {
		return mandateType;
	}

	public void setMandateType(String mandateType) {
		this.mandateType = mandateType;
	}

	public double getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(double maxAmount) {
		this.maxAmount = maxAmount;
	}

	public AmountType getAmountType() {
		return amountType;
	}

	public void setAmountType(AmountType amountType) {
		this.amountType = amountType;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public Date getDebitStartDate() {
		return debitStartDate;
	}

	public void setDebitStartDate(Date debitStartDate) {
		this.debitStartDate = debitStartDate;
	}

	public Date getDebitEndDate() {
		return debitEndDate;
	}

	public void setDebitEndDate(Date debitEndDate) {
		this.debitEndDate = debitEndDate;
	}

	public MandateStatus getMandateStatus() {
		return mandateStatus;
	}

	public void setMandateStatus(MandateStatus mandateStatus) {
		this.mandateStatus = mandateStatus;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getIsPrincialOrInterest() {
		return isPrincialOrInterest;
	}

	public void setIsPrincialOrInterest(String isPrincialOrInterest) {
		this.isPrincialOrInterest = isPrincialOrInterest;
	}

   
    
}