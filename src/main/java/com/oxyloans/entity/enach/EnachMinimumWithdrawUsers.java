package com.oxyloans.entity.enach;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "enach_minimum_withdraw_users")
public class EnachMinimumWithdrawUsers {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "applicationid", nullable = false)
	private String applicationId;
	@Column(name = "borrowerid", nullable = false)
	private Integer borrowerId;
	@Column(name = "tenure", nullable = true)
	private Integer tenure;
	@Column(name = "amount", nullable = true)
	private Double amount;
	@Column(name = "emidate", nullable = true)
	private String emiDate;
	@Column(name = "no_of_runs", nullable = true)
	private Integer noOfRuns;
	@Column(name = "created_on", nullable = false, insertable = true, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();
	@Column(name = "created_by", nullable = true)
	private String createdBy;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public Integer getTenure() {
		return tenure;
	}

	public void setTenure(Integer tenure) {
		this.tenure = tenure;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getEmiDate() {
		return emiDate;
	}

	public void setEmiDate(String emiDate) {
		this.emiDate = emiDate;
	}

	public Integer getNoOfRuns() {
		return noOfRuns;
	}

	public void setNoOfRuns(Integer noOfRuns) {
		this.noOfRuns = noOfRuns;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

}
