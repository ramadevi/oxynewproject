package com.oxyloans.entity.enach;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "enach_transaction_verification_response_applevel")
public class TransactionVerificationResponseAppLevel {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "application_id", nullable = false)
	private Integer applicationId;
	@Column(name = "emi_number", nullable = false)
	private Integer emiNumber;
	@Column(name = "enachtype", nullable = true)
	private String enachType;
	@Column(name = "enachstatus", nullable = true)
	private String enachStatus;
	@Column(name = "statuscode", nullable = true)
	private String statusCode;
	@Column(name = "statusdescription", nullable = true)
	private String statusDescription;
	@Column(name = "transactiondate", nullable = true)
	private String transactionDate;
	@Column(name = "clnttxnref", nullable = true)
	private String clntTxnRef;
	@Column(name = "enachjson", nullable = true)
	private String enachJson;
	@Column(name = "created_on", nullable = false, insertable = true, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();
	@Column(name = "txn_scheduling_ref_id", nullable = true)
	private String txnScheduligRefId;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
	public Integer getEmiNumber() {
		return emiNumber;
	}
	public void setEmiNumber(Integer emiNumber) {
		this.emiNumber = emiNumber;
	}
	public String getEnachType() {
		return enachType;
	}
	public void setEnachType(String enachType) {
		this.enachType = enachType;
	}
	public String getEnachStatus() {
		return enachStatus;
	}
	public void setEnachStatus(String enachStatus) {
		this.enachStatus = enachStatus;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getClntTxnRef() {
		return clntTxnRef;
	}
	public void setClntTxnRef(String clntTxnRef) {
		this.clntTxnRef = clntTxnRef;
	}
	public String getEnachJson() {
		return enachJson;
	}
	public void setEnachJson(String enachJson) {
		this.enachJson = enachJson;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getTxnScheduligRefId() {
		return txnScheduligRefId;
	}
	public void setTxnScheduligRefId(String txnScheduligRefId) {
		this.txnScheduligRefId = txnScheduligRefId;
	}

	
}
