package com.oxyloans.entity.enach;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.IEntity;
import com.oxyloans.entity.loan.OxyLoan;

@Entity
@Table(name = "enach_mandate")
public class EnachMandate implements IEntity {

    public static enum Frequency {
        MNTH,DAIL
    }

    public static enum AmountType {
        M
    }

    public static enum MandateStatus {
        INITIATED, SUCCESS, FAILED,ADMINREJECTED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "transaction_id", nullable = false, unique = true)
    private String mandateTransactionId;

    @Column(name = "mandate_type", nullable = false)
    private String mandateType = "NETBANKING";

    //This is actual Loan Id
    @Column(name = "max_amount", nullable = false)
    private double maxAmount;

    @Column(name = "amount_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private AmountType amountType = AmountType.M;

    @Column(name = "frequency", nullable = false)
    @Enumerated(EnumType.STRING)
    private Frequency frequency = Frequency.MNTH;

    @Column(name = "debit_start_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date debitStartDate;

    @Column(name = "debit_end_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date debitEndDate;

    @Column(name = "mandate_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private MandateStatus mandateStatus;

    @Column(name = "modified_on", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn = new Date();

    @Column(name = "oxy_loan_id", nullable = false)
    private Integer oxyLoanId;

    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "oxy_loan_id", insertable = false, updatable = false)
	private OxyLoan oxyLoan;

    public Integer getId() {
        return id;
    }

    public String getMandateTransactionId() {
        return mandateTransactionId;
    }

    public void setMandateTransactionId(String mandateTransactionId) {
        this.mandateTransactionId = mandateTransactionId;
    }

    public String getMandateType() {
        return mandateType;
    }

    public void setMandateType(String mandateType) {
        this.mandateType = mandateType;
    }

    public double getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(double maxAmount) {
        this.maxAmount = maxAmount;
    }

    public AmountType getAmountType() {
        return amountType;
    }

    public void setAmountType(AmountType amountType) {
        this.amountType = amountType;
    }

    public Frequency getFrequency() {
        return frequency;
    }

    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }

    public Date getDebitStartDate() {
        return debitStartDate;
    }

    public void setDebitStartDate(Date debitStartDate) {
        this.debitStartDate = debitStartDate;
    }

    public Date getDebitEndDate() {
        return debitEndDate;
    }

    public void setDebitEndDate(Date debitEndDate) {
        this.debitEndDate = debitEndDate;
    }

    public MandateStatus getMandateStatus() {
        return mandateStatus;
    }

    public void setMandateStatus(MandateStatus mandateStatus) {
        this.mandateStatus = mandateStatus;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Integer getOxyLoanId() {
        return oxyLoanId;
    }

    public void setOxyLoanId(Integer oxyLoanId) {
        this.oxyLoanId = oxyLoanId;
    }

    public OxyLoan getOxyLoan() {
        return oxyLoan;
    }

    public void setOxyLoan(OxyLoan oxyLoan) {
        this.oxyLoan = oxyLoan;
    }

}
