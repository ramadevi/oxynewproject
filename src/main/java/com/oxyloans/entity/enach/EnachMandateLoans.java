package com.oxyloans.entity.enach;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.oxyloans.entity.IEntity;

@Entity
@Table(name = "enach_mandate_loans")
public class EnachMandateLoans implements IEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "enach_mandate_id", nullable = false)
	private Integer enachMandateId;

	@Column(name = "borrower_id", nullable = false)
	private Integer borrowerId;

	//This is actual Loan Id
	@Column(name = "oxy_loan_id", nullable = false)
	private Integer oxyLoanId;

	public Integer getId() {
		return id;
	}

	public Integer getEnachMandateId() {
		return enachMandateId;
	}

	public void setEnachMandateId(Integer enachMandateId) {
		this.enachMandateId = enachMandateId;
	}

	public Integer getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}

	public Integer getOxyLoanId() {
		return oxyLoanId;
	}

	public void setOxyLoanId(Integer oxyLoanId) {
		this.oxyLoanId = oxyLoanId;
	}

}
