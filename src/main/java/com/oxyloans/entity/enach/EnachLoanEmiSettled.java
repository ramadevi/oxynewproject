package com.oxyloans.entity.enach;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.oxyloans.entity.IEntity;

@Entity
@Table(name = "enach_loans_emi_settled")
public class EnachLoanEmiSettled implements IEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "enach_transaction_id", nullable = false, unique = true)
	private String enachTransactionId;

	@Column(name = "loan_id", nullable = false)
	private Integer loanId;

	@Column(name = "emiId", nullable = false)
	private Integer emi_id;

	@Column(name = "emi_amount", nullable = false)
	private Double emiAmount;

	public Integer getId() {
		return id;
	}

	public String getEnachTransactionId() {
		return enachTransactionId;
	}

	public void setEnachTransactionId(String enachTransactionId) {
		this.enachTransactionId = enachTransactionId;
	}

	public Integer getLoanId() {
		return loanId;
	}

	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}

	public Integer getEmi_id() {
		return emi_id;
	}

	public void setEmi_id(Integer emi_id) {
		this.emi_id = emi_id;
	}

	public Double getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(Double emiAmount) {
		this.emiAmount = emiAmount;
	}
	

}
