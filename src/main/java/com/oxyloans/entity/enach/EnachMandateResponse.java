package com.oxyloans.entity.enach;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "enach_mandate_response")
public class EnachMandateResponse {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "enach_mandate_id", nullable = false, unique = true)
	private Integer enachMandateId;

	@Column(name = "txn_status", nullable = true)
	private String txnStatus;
	@Column(name = "txn_msg", nullable = true)
	private String txnMsg;
	@Column(name = "txn_err_msg", nullable = true)
	private String txnErrMsg;
	@Column(name = "clnt_txn_ref", nullable = true)
	private String clntTxnRef;
	@Column(name = "tpsl_bank_cd", nullable = true)
	private String tpslBankCd;
	@Column(name = "tpsl_txn_id", nullable = true)
	private String tpslTxnId;
	@Column(name = "txn_amt", nullable = true)
	private Double txnAmt;
	@Column(name = "clnt_rqst_meta", nullable = true)
	private String clntRqstMeta;
	@Column(name = "tpsl_txn_time", nullable = true)
	private String tpslTxnTime;
	@Column(name = "bal_amt", nullable = true)
	private Double balAmt;
	@Column(name = "card_id", nullable = true)
	private String cardId;
	@Column(name = "alias_name", nullable = true)
	private String aliasName;
	@Column(name = "bank_transaction_id", nullable = true)
	private String bankTransactionId;
	@Column(name = "mandate_reg_no", nullable = true)
	private String mandateRegNo;
	@Column(name = "token", nullable = true)
	private String token;
	@Column(name = "hash", nullable = true)
	private String hash;
	@Column(name = "consumerid", nullable = true)
	private String consumerId;

	@Column(name = "bankreferenceidentifier", nullable = true)
	private String bankReferenceIdentifier;

	@Column(name = "mandateverificationresponsejson", nullable = true)
	private String mandateVerificationResponseJson;

	@Column(name = "created_on", nullable = false, insertable = true, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	public String getBankReferenceIdentifier() {
		return bankReferenceIdentifier;
	}

	public void setBankReferenceIdentifier(String bankReferenceIdentifier) {
		this.bankReferenceIdentifier = bankReferenceIdentifier;
	}

	public String getMandateVerificationResponseJson() {
		return mandateVerificationResponseJson;
	}

	public void setMandateVerificationResponseJson(String mandateVerificationResponseJson) {
		this.mandateVerificationResponseJson = mandateVerificationResponseJson;
	}

	public String getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(String consumerId) {
		this.consumerId = consumerId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEnachMandateId() {
		return enachMandateId;
	}

	public void setEnachMandateId(Integer enachMandateId) {
		this.enachMandateId = enachMandateId;
	}

	public String getTxnStatus() {
		return txnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}

	public String getTxnMsg() {
		return txnMsg;
	}

	public void setTxnMsg(String txnMsg) {
		this.txnMsg = txnMsg;
	}

	public String getTxnErrMsg() {
		return txnErrMsg;
	}

	public void setTxnErrMsg(String txnErrMsg) {
		this.txnErrMsg = txnErrMsg;
	}

	public String getClntTxnRef() {
		return clntTxnRef;
	}

	public void setClntTxnRef(String clntTxnRef) {
		this.clntTxnRef = clntTxnRef;
	}

	public String getTpslBankCd() {
		return tpslBankCd;
	}

	public void setTpslBankCd(String tpslBankCd) {
		this.tpslBankCd = tpslBankCd;
	}

	public String getTpslTxnId() {
		return tpslTxnId;
	}

	public void setTpslTxnId(String tpslTxnId) {
		this.tpslTxnId = tpslTxnId;
	}

	public Double getTxnAmt() {
		return txnAmt;
	}

	public void setTxnAmt(Double txnAmt) {
		this.txnAmt = txnAmt;
	}

	public String getClntRqstMeta() {
		return clntRqstMeta;
	}

	public void setClntRqstMeta(String clntRqstMeta) {
		this.clntRqstMeta = clntRqstMeta;
	}

	public String getTpslTxnTime() {
		return tpslTxnTime;
	}

	public void setTpslTxnTime(String tpslTxnTime) {
		this.tpslTxnTime = tpslTxnTime;
	}

	public Double getBalAmt() {
		return balAmt;
	}

	public void setBalAmt(Double balAmt) {
		this.balAmt = balAmt;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	public String getBankTransactionId() {
		return bankTransactionId;
	}

	public void setBankTransactionId(String bankTransactionId) {
		this.bankTransactionId = bankTransactionId;
	}

	public String getMandateRegNo() {
		return mandateRegNo;
	}

	public void setMandateRegNo(String mandateRegNo) {
		this.mandateRegNo = mandateRegNo;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
