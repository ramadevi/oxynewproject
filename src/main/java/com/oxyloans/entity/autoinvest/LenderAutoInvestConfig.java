package com.oxyloans.entity.autoinvest;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lender_auto_invest_config")
public class LenderAutoInvestConfig {

	public static enum DealType {
		NORMAL, ESCROW, BOTH
	}

	public static enum PrincipalReturningAccountType {
		WALLET, BANKACCOUNT
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = false)
	private Integer userId;
	@Column(name = "riskcategory", nullable = true)
	private String riskCategory;
	@Column(name = "gender", nullable = true)
	private String gender;
	@Column(name = "oxyscore", nullable = true)
	private String oxyScore;
	@Column(name = "city", nullable = true)
	private String city;
	@Column(name = "employmenttype", nullable = true)
	private String employmentType;
	@Column(name = "salaryrange", nullable = true)
	private String salaryRange;
	@Column(name = "maxamount", nullable = true)
	private Double maxAmount;
	@Column(name = "duration", nullable = true)
	private String duration;
	@Column(name = "rateofinterest", nullable = true)
	private String rateOfInterest;
	@Column(name = "auto_esign", nullable = true)
	private Boolean autoEsigin = false;
	@Column(name = "termsandconditions", nullable = true)
	private Boolean termsAndConditions = false;
	@Column(name = "comments", nullable = true)
	private String comments;
	@Column(name = "created_on", nullable = false, insertable = true, updatable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;
	@Column(name = "status", nullable = true)
	private String status;

	@Column(name = "deal_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private DealType dealType = DealType.NORMAL;

	@Column(name = "principal_returning_account_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private PrincipalReturningAccountType principalReturningAccountType = PrincipalReturningAccountType.BANKACCOUNT;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getRiskCategory() {
		return riskCategory;
	}

	public void setRiskCategory(String riskCategory) {
		this.riskCategory = riskCategory;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	public String getSalaryRange() {
		return salaryRange;
	}

	public void setSalaryRange(String salaryRange) {
		this.salaryRange = salaryRange;
	}

	public Double getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(Double maxAmount) {
		this.maxAmount = maxAmount;
	}

	public String getOxyScore() {
		return oxyScore;
	}

	public void setOxyScore(String oxyScore) {
		this.oxyScore = oxyScore;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(String rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public Boolean getAutoEsigin() {
		return autoEsigin;
	}

	public void setAutoEsigin(Boolean autoEsigin) {
		this.autoEsigin = autoEsigin;
	}

	public Boolean getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(Boolean termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public DealType getDealType() {
		return dealType;
	}

	public void setDealType(DealType dealType) {
		this.dealType = dealType;
	}

	public PrincipalReturningAccountType getPrincipalReturningAccountType() {
		return principalReturningAccountType;
	}

	public void setPrincipalReturningAccountType(PrincipalReturningAccountType principalReturningAccountType) {
		this.principalReturningAccountType = principalReturningAccountType;
	}

}
