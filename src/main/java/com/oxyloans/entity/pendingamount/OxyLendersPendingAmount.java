package com.oxyloans.entity.pendingamount;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "oxy_lenders_pending_amount")
public class OxyLendersPendingAmount {

	public static enum AmountType {
		LENDERINTEREST, LENDERPRINCIPAL, REFERRALBONUS
	}

	public static enum Status {
		CREATED, INITIATED, EXECUTED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = true)
	private Integer userId;

	@Column(name = "amount", nullable = true)
	private Double amount;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId;

	@Column(name = "deal_name", nullable = true)
	private String dealName;

	@Column(name = "reason", nullable = true)
	private String reason;

	@Column(name = "transaction_type", nullable = true)
	private String transactionType;

	@Column(name = "number_of_days", nullable = true)
	private Integer numberOfDays;

	@Column(name = "file_name", nullable = true)
	private String fileName;

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status = Status.CREATED;

	@Column(name = "amount_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private AmountType amountType = AmountType.LENDERINTEREST;

	@Column(name = "created_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "approved_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date approvedDate;

	public Integer getId() {
		return id;
	}

	public Integer getUserId() {
		return userId;
	}

	public Double getAmount() {
		return amount;
	}

	public Integer getDealId() {
		return dealId;
	}

	public String getDealName() {
		return dealName;
	}

	public String getReason() {
		return reason;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public Integer getNumberOfDays() {
		return numberOfDays;
	}

	public String getFileName() {
		return fileName;
	}

	public Status getStatus() {
		return status;
	}

	public AmountType getAmountType() {
		return amountType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public void setNumberOfDays(Integer numberOfDays) {
		this.numberOfDays = numberOfDays;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public void setAmountType(AmountType amountType) {
		this.amountType = amountType;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

}
