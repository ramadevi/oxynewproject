package com.oxyloans.entity.whatapp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "whatapp_groups_information")
public class WhatappGroupsInformation {

	public static enum ChatType {
		GROUP, BROADCAST
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "chat_id", nullable = true)
	private String chatId;

	@Column(name = "groupName", nullable = true)
	private String groupName;

	@Column(name = "chat_type", nullable = true)
	private String chatType = ChatType.GROUP.toString();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getChatType() {
		return chatType;
	}

	public void setChatType(String chatType) {
		this.chatType = chatType;
	}

}
