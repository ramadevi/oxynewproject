package com.oxyloans.entity.whatapp;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "oxy_free_whatsapp")
public class OxyFreeWhatsapp {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer Id;

	@Column(name = "mobile_number", nullable = true)
	private String mobileNumber;

	@Column(name = "messages_count", nullable = true)
	private int messagesCount;

	@Column(name = "messages_sent_on", nullable = true)
	private Date messagesSentOn;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public int getMessagesCount() {
		return messagesCount;
	}

	public void setMessagesCount(int messagesCount) {
		this.messagesCount = messagesCount;
	}

	public Date getMessagesSentOn() {
		return messagesSentOn;
	}

	public void setMessagesSentOn(Date messagesSentOn) {
		this.messagesSentOn = messagesSentOn;
	}

}
