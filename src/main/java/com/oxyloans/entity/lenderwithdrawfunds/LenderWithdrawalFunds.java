package com.oxyloans.entity.lenderwithdrawfunds;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lender_withdrawal_funds")
public class LenderWithdrawalFunds {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = false)
	private Integer userId;

	@Column(name = "amount", nullable = true)
	private Double amount;

	@Column(name = "amount_expected_date", nullable = true)
	private String amountExpectedDate;

	@Column(name = "withdrawalreason", nullable = true)
	private String withdrawalReason;

	@Column(name = "rating", nullable = true)
	private String rating;

	@Column(name = "feedback", nullable = true)
	private String feedBack;

	@Column(name = "admincomments", nullable = true)
	private String adminComments;

	@Column(name = "status", nullable = true)
	private String status;

	@Column(name = "created_on", nullable = false, insertable = true, updatable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "approved_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date approvedOn;

	@Column(name = "receiver_user_id", unique = true)
	private Integer receiverUserId = 0;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getWithdrawalReason() {
		return withdrawalReason;
	}

	public void setWithdrawalReason(String withdrawalReason) {
		this.withdrawalReason = withdrawalReason;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getFeedBack() {
		return feedBack;
	}

	public void setFeedBack(String feedBack) {
		this.feedBack = feedBack;
	}

	public String getAdminComments() {
		return adminComments;
	}

	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getReceiverUserId() {
		return receiverUserId;
	}

	public void setReceiverUserId(Integer receiverUserId) {
		this.receiverUserId = receiverUserId;
	}

	public String getAmountExpectedDate() {
		return amountExpectedDate;
	}

	public void setAmountExpectedDate(String amountExpectedDate) {
		this.amountExpectedDate = amountExpectedDate;
	}

	public Date getApprovedOn() {
		return approvedOn;
	}

	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

}
