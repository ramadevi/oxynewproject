package com.oxyloans.entity.LenderReferenceDetails;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.cms.LenderCmsPayments;

@Entity
@Table(name = "referral_bonus_reading")
public class ReferralBonusReading {
	
	public static enum Status {
		GENERATED, INITIATED , EXCUTED
		}

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Integer id;
		
		@Column(name = "user_id", nullable = true)
		private Integer userId;

		@Column(name = "refferal_bonus_updated_id", nullable = true)
		private String refferalBonusUpdatedId;
		
		@Column(name = "referee_dealid_pair", nullable = true)
		private String refereeDealIdPair;
		
		@Column(name = "total_amount", nullable = true)
		private Double totalAmount = 0.0;
		
		@Column(name = "status", nullable = true)
		private String status;
		
		@Column(name = "sheet_generated_on", nullable = true)
		@Temporal(TemporalType.TIMESTAMP)
		private Date sheetGeneratedOn;
		
		@Column(name = "initiated_date", nullable = true)
		@Temporal(TemporalType.DATE)
		private Date initiatedDate;

		@Column(name = "excuted_date", nullable = true)
		@Temporal(TemporalType.DATE)
		private Date excutedDate;
		
		
		@Column(name = "lender_cms_payments_id")
		private Integer lendercmspaymentsid;
		  
		  
	    @ManyToOne(cascade = CascadeType.ALL)
	    @JoinColumn(name = "lender_cms_payments_id", nullable = true, insertable = false, updatable = false)
	    private LenderCmsPayments lenderCmsPayments;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getUserId() {
			return userId;
		}

		public void setUserId(Integer userId) {
			this.userId = userId;
		}

		public String getRefferalBonusUpdatedId() {
			return refferalBonusUpdatedId;
		}

		public void setRefferalBonusUpdatedId(String refferalBonusUpdatedId) {
			this.refferalBonusUpdatedId = refferalBonusUpdatedId;
		}

		public String getRefereeDealIdPair() {
			return refereeDealIdPair;
		}

		public void setRefereeDealIdPair(String refereeDealIdPair) {
			this.refereeDealIdPair = refereeDealIdPair;
		}

		public Double getTotalAmount() {
			return totalAmount;
		}

		public void setTotalAmount(Double totalAmount) {
			this.totalAmount = totalAmount;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Date getSheetGeneratedOn() {
			return sheetGeneratedOn;
		}

		public void setSheetGeneratedOn(Date sheetGeneratedOn) {
			this.sheetGeneratedOn = sheetGeneratedOn;
		}

		public Date getInitiatedDate() {
			return initiatedDate;
		}

		public void setInitiatedDate(Date initiatedDate) {
			this.initiatedDate = initiatedDate;
		}

		public Date getExcutedDate() {
			return excutedDate;
		}

		public void setExcutedDate(Date excutedDate) {
			this.excutedDate = excutedDate;
		}

		public Integer getLendercmspaymentsid() {
			return lendercmspaymentsid;
		}

		public void setLendercmspaymentsid(Integer lendercmspaymentsid) {
			this.lendercmspaymentsid = lendercmspaymentsid;
		}

		public LenderCmsPayments getLenderCmsPayments() {
			return lenderCmsPayments;
		}

		public void setLenderCmsPayments(LenderCmsPayments lenderCmsPayments) {
			this.lenderCmsPayments = lenderCmsPayments;
		}
	
	
	

}
