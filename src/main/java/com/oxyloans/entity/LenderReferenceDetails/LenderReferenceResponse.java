package com.oxyloans.entity.LenderReferenceDetails;

import java.util.List;

import com.oxyloans.response.user.LenderReferenceResponseDto;
import com.oxyloans.response.user.LenderReferralResponse;
import com.oxyloans.response.user.LenderReferralResponseForDuration;

public class LenderReferenceResponse {

	private List<LenderReferenceResponseDto> listOfLenderReferenceDetails;

	private Integer countOfReferees;
	
	private String status;
	
	private Integer totalAmountEarned;
	
	private List<LenderReferralResponse> listOfLenderReferralResponse;
	
	private List<LenderReferralResponseForDuration> listOfNewReferralResponse;
	

	public Integer getCountOfReferees() {
		return countOfReferees;
	}

	public void setCountOfReferees(Integer countOfReferees) {
		this.countOfReferees = countOfReferees;
	}

	public List<LenderReferenceResponseDto> getListOfLenderReferenceDetails() {
		return listOfLenderReferenceDetails;
	}

	public void setListOfLenderReferenceDetails(List<LenderReferenceResponseDto> listOfLenderReferenceDetails) {
		this.listOfLenderReferenceDetails = listOfLenderReferenceDetails;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getTotalAmountEarned() {
		return totalAmountEarned;
	}

	public void setTotalAmountEarned(Integer totalAmountEarned) {
		this.totalAmountEarned = totalAmountEarned;
	}

	public List<LenderReferralResponse> getListOfLenderReferralResponse() {
		return listOfLenderReferralResponse;
	}

	public void setListOfLenderReferralResponse(List<LenderReferralResponse> listOfLenderReferralResponse) {
		this.listOfLenderReferralResponse = listOfLenderReferralResponse;
	}

	public List<LenderReferralResponseForDuration> getListOfNewReferralResponse() {
		return listOfNewReferralResponse;
	}

	public void setListOfNewReferralResponse(List<LenderReferralResponseForDuration> listOfNewReferralResponse) {
		this.listOfNewReferralResponse = listOfNewReferralResponse;
	}

}
