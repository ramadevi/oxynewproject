package com.oxyloans.entity.LenderReferenceDetails;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lender_reference_details")
public class LenderReferenceDetails {

	public static enum Status {
		Invited, Registered, Lent, Disbursed
	}

	public static enum CitizenType {
		NONNRI, NRI
	}

	public static enum Source {
		ReferralLink, BulkInvite,Partner
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "referrer_id", nullable = true)
	private Integer referrerId = 0;

	@Column(name = "referee_email", nullable = true)
	private String refereeEmail;

	@Column(name = "referee_mobilenumber", nullable = true)
	private String refereeMobileNumber;

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status = Status.Invited;

	@Column(name = "referee_id", nullable = true)
	private Integer refereeId = 0;

	@Column(name = "amount", nullable = false)
	private Double amount = 0.0;

	@Column(name = "referred_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date referredOn = new Date();

	@Column(name = "referee_name", nullable = true)
	private String refereeName;

	@Column(name = "mail_content", nullable = true)
	private String mailContent;

	@Column(name = "radhasir_comments", nullable = true)
	private String radhaSirComments;

	@Column(name = "user_primarytype", nullable = true)
	private String userPrimaryType;

	@Column(name = "citizen_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private CitizenType citizenType = CitizenType.NONNRI;

	@Column(name = "source", nullable = false)
	@Enumerated(EnumType.STRING)
	private Source source = Source.ReferralLink;

	public Integer getId() {
		return id;
	}

	public Integer getReferrerId() {
		return referrerId;
	}

	public void setReferrerId(Integer referrerId) {
		this.referrerId = referrerId;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Double getAmount() {
		return amount;
	}

	public Integer getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(Integer refereeId) {
		this.refereeId = refereeId;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Date getReferredOn() {
		return referredOn;
	}

	public void setReferredOn(Date referredOn) {
		this.referredOn = referredOn;
	}

	public String getRefereeEmail() {
		return refereeEmail;
	}

	public void setRefereeEmail(String refereeEmail) {
		this.refereeEmail = refereeEmail;
	}

	public String getRefereeMobileNumber() {
		return refereeMobileNumber;
	}

	public void setRefereeMobileNumber(String refereeMobileNumber) {
		this.refereeMobileNumber = refereeMobileNumber;
	}

	public String getRefereeName() {
		return refereeName;
	}

	public void setRefereeName(String refereeName) {
		this.refereeName = refereeName;
	}

	public String getMailContent() {
		return mailContent;
	}

	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}

	public String getRadhaSirComments() {
		return radhaSirComments;
	}

	public void setRadhaSirComments(String radhaSirComments) {
		this.radhaSirComments = radhaSirComments;
	}

	public String getUserPrimaryType() {
		return userPrimaryType;
	}

	public void setUserPrimaryType(String userPrimaryType) {
		this.userPrimaryType = userPrimaryType;
	}

	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}

	public CitizenType getCitizenType() {
		return citizenType;
	}

	public void setCitizenType(CitizenType citizenType) {
		this.citizenType = citizenType;
	}

}
