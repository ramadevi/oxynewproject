package com.oxyloans.entity.LenderReferenceDetails;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lender_referral_bonus_updated")
public class LenderReferralBonusUpdated {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "referrer_user_id", nullable = true)
	private Integer referrerUserId = 0;
	
	@Column(name = "referee_user_id", nullable = true)
	private Integer refereeUserId = 0;
	
	@Column(name = "amount", nullable = false)
	private Double amount = 0.0;
	
	@Column(name = "payment_status", nullable = false)
	private String paymentStatus = "Unpaid";

	@Column(name = "transferred_on", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date transferredOn;
	
	@Column(name = "radhasir_comments_bonus", nullable = true)
	private String radhaSirCommentsForBonus;
	
	@Column(name = "deal_id", nullable = true)
	private Integer dealId;
	
	@Column(name = "participated_on", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date participatedOn;
	
	@Column(name = "participated_amount", nullable = false)
	private Double participatedAmount = 0.0;
	
	@Column(name = "updated_bonus", nullable = true)
	private Double updatedBonus = 0.0;
	
	@Column(name = "remarks", nullable = true)
	private String remarks;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Date getTransferredOn() {
		return transferredOn;
	}

	public void setTransferredOn(Date transferredOn) {
		this.transferredOn = transferredOn;
	}

	public Integer getReferrerUserId() {
		return referrerUserId;
	}

	public void setReferrerUserId(Integer referrerUserId) {
		this.referrerUserId = referrerUserId;
	}

	public Integer getRefereeUserId() {
		return refereeUserId;
	}

	public void setRefereeUserId(Integer refereeUserId) {
		this.refereeUserId = refereeUserId;
	}

	public String getRadhaSirCommentsForBonus() {
		return radhaSirCommentsForBonus;
	}

	public void setRadhaSirCommentsForBonus(String radhaSirCommentsForBonus) {
		this.radhaSirCommentsForBonus = radhaSirCommentsForBonus;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Date getParticipatedOn() {
		return participatedOn;
	}

	public void setParticipatedOn(Date participatedOn) {
		this.participatedOn = participatedOn;
	}

	public Double getParticipatedAmount() {
		return participatedAmount;
	}

	public void setParticipatedAmount(Double participatedAmount) {
		this.participatedAmount = participatedAmount;
	}

	public Double getUpdatedBonus() {
		return updatedBonus;
	}

	public void setUpdatedBonus(Double updatedBonus) {
		this.updatedBonus = updatedBonus;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	


}
