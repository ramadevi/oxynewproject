package com.oxyloans.entity.borrowerpayments;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;

@Entity
@Table(name = "borrower_payments")
public class BorrowerPayments {

	public static enum PayStatus {
		INITIATED, EDITED, APPROVED, SUCCESS
	}

	public static enum FundingType {
		FD, SAVINGS, DAYS
	}

	public static enum FdStatus {
		CREATED, SAVED, CLOSED
	}

	public static enum LoanType {
		STUDENT, ASSET
	}

	public static enum PaymentsCollection {
		STUDENT, CONSULTANCY
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = true)
	private Integer userId;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId;

	@Column(name = "amount", nullable = true)
	private double amount = 0.0;

	@Column(name = "payment_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date paymentDate;

	@Column(name = "remarks", nullable = true)
	private String remarks;

	@Column(name = "borrower_payment_type", nullable = true)
	private String borrowerPaymentType;

	@Column(name = "fd_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private FdStatus fdStatus = FdStatus.CREATED;

	@Column(name = "account_number", nullable = true)
	private String accountNumber;

	@Column(name = "name", nullable = true)
	private String name = "";

	@Column(name = "ifsc", nullable = true)
	private String ifsc = "";

	@Column(name = "city", nullable = true)
	private String city = "";

	@Column(name = "branch", nullable = true)
	private String branch = "";

	@Column(name = "bank_name", nullable = true)
	private String bankName = "";

	@Column(name = "lead_by", nullable = true)
	private String leadBy = "";

	@Column(name = "consultancy", nullable = true)
	private String consultancy = "";

	@Column(name = "country", nullable = true)
	private String country = "";

	@Column(name = "university", nullable = true)
	private String university = "";

	@Column(name = "student_mobile_number", nullable = true)
	private String studentMobileNumber = "";

	@Column(name = "roi", nullable = true)
	private Double roi = 0.0;

	@Column(name = "fd_amount", nullable = true)
	private Double fdAmount = 0.0;

	@Column(name = "funding_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private FundingType fundingType = FundingType.FD;

	@Column(name = "bank_details_verified_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date bankDetailsVerifiedOn;

	@Column(name = "pay_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private PayStatus payStatus = PayStatus.APPROVED;

	@Column(name = "created_on", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	@Column(name = "fd_created", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date fdCreated;

	@Column(name = "fd_validity", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date fdValidity;

	@Column(name = "bank_verified_status", nullable = true)
	private Boolean bankVerifiedStatus = false;

	@Column(name = "bank_choosen", nullable = true)
	private String bankChoosen;

	@Column(name = "per_day_interest", nullable = true)
	private Double perDayInterest = 0.0;

	@Column(name = "interest_earned_on_fd", nullable = true)
	private Double interestEarnedOnFd = 0.0;

	@Column(name = "fd_closed_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date fdClosedDate;

	@ManyToOne
	@JoinColumn(name = "deal_id", insertable = false, updatable = false)
	private OxyBorrowersDealsInformation oxyBorrowersDealsInformation;

	@Column(name = "fee_invoice_url", nullable = true)
	private String feeInvoiceUrl;

	@Column(name = "amount_returned_to_repayment", nullable = true)
	private Double amountReturnedToRepayment;

	@Column(name = "amount_returned_to_another", nullable = true)
	private Double amountReturnedToAnother;

	@Column(name = "comments", nullable = true)
	private String comments;

	@Column(name = "loan_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private LoanType loanType = LoanType.STUDENT;

	@Column(name = "payments_collection", nullable = true)
	@Enumerated(EnumType.STRING)
	private PaymentsCollection paymentsCollection = PaymentsCollection.STUDENT;

	@Column(name = "borrower_fee", nullable = true)
	private Double borrowerFee = 0.0;
	
	@Column(name = "fd_fund_transfer_remarks", nullable = true)
	private String fdFundTransferRemarks;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getBorrowerPaymentType() {
		return borrowerPaymentType;
	}

	public void setBorrowerPaymentType(String borrowerPaymentType) {
		this.borrowerPaymentType = borrowerPaymentType;
	}

	public FdStatus getFdStatus() {
		return fdStatus;
	}

	public void setFdStatus(FdStatus fdStatus) {
		this.fdStatus = fdStatus;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getLeadBy() {
		return leadBy;
	}

	public void setLeadBy(String leadBy) {
		this.leadBy = leadBy;
	}

	public String getConsultancy() {
		return consultancy;
	}

	public void setConsultancy(String consultancy) {
		this.consultancy = consultancy;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getUniversity() {
		return university;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public String getStudentMobileNumber() {
		return studentMobileNumber;
	}

	public void setStudentMobileNumber(String studentMobileNumber) {
		this.studentMobileNumber = studentMobileNumber;
	}

	public Double getRoi() {
		return roi;
	}

	public void setRoi(Double roi) {
		this.roi = roi;
	}

	public Double getFdAmount() {
		return fdAmount;
	}

	public void setFdAmount(Double fdAmount) {
		this.fdAmount = fdAmount;
	}

	public FundingType getFundingType() {
		return fundingType;
	}

	public void setFundingType(FundingType fundingType) {
		this.fundingType = fundingType;
	}

	public Date getBankDetailsVerifiedOn() {
		return bankDetailsVerifiedOn;
	}

	public void setBankDetailsVerifiedOn(Date bankDetailsVerifiedOn) {
		this.bankDetailsVerifiedOn = bankDetailsVerifiedOn;
	}

	public PayStatus getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(PayStatus payStatus) {
		this.payStatus = payStatus;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getFdCreated() {
		return fdCreated;
	}

	public void setFdCreated(Date fdCreated) {
		this.fdCreated = fdCreated;
	}

	public Date getFdValidity() {
		return fdValidity;
	}

	public void setFdValidity(Date fdValidity) {
		this.fdValidity = fdValidity;
	}

	public Boolean getBankVerifiedStatus() {
		return bankVerifiedStatus;
	}

	public void setBankVerifiedStatus(Boolean bankVerifiedStatus) {
		this.bankVerifiedStatus = bankVerifiedStatus;
	}

	public String getBankChoosen() {
		return bankChoosen;
	}

	public void setBankChoosen(String bankChoosen) {
		this.bankChoosen = bankChoosen;
	}

	public Double getPerDayInterest() {
		return perDayInterest;
	}

	public void setPerDayInterest(Double perDayInterest) {
		this.perDayInterest = perDayInterest;
	}

	public Double getInterestEarnedOnFd() {
		return interestEarnedOnFd;
	}

	public void setInterestEarnedOnFd(Double interestEarnedOnFd) {
		this.interestEarnedOnFd = interestEarnedOnFd;
	}

	public Date getFdClosedDate() {
		return fdClosedDate;
	}

	public void setFdClosedDate(Date fdClosedDate) {
		this.fdClosedDate = fdClosedDate;
	}

	public OxyBorrowersDealsInformation getOxyBorrowersDealsInformation() {
		return oxyBorrowersDealsInformation;
	}

	public void setOxyBorrowersDealsInformation(OxyBorrowersDealsInformation oxyBorrowersDealsInformation) {
		this.oxyBorrowersDealsInformation = oxyBorrowersDealsInformation;
	}

	public Double getAmountReturnedToRepayment() {
		return amountReturnedToRepayment;
	}

	public void setAmountReturnedToRepayment(Double amountReturnedToRepayment) {
		this.amountReturnedToRepayment = amountReturnedToRepayment;
	}

	public Double getAmountReturnedToAnother() {
		return amountReturnedToAnother;
	}

	public void setAmountReturnedToAnother(Double amountReturnedToAnother) {
		this.amountReturnedToAnother = amountReturnedToAnother;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getFeeInvoiceUrl() {
		return feeInvoiceUrl;
	}

	public void setFeeInvoiceUrl(String feeInvoiceUrl) {
		this.feeInvoiceUrl = feeInvoiceUrl;
	}

	public LoanType getLoanType() {
		return loanType;
	}

	public void setLoanType(LoanType loanType) {
		this.loanType = loanType;
	}

	public PaymentsCollection getPaymentsCollection() {
		return paymentsCollection;
	}

	public void setPaymentsCollection(PaymentsCollection paymentsCollection) {
		this.paymentsCollection = paymentsCollection;
	}

	public Double getBorrowerFee() {
		return borrowerFee;
	}

	public void setBorrowerFee(Double borrowerFee) {
		this.borrowerFee = borrowerFee;
	}

	public String getFdFundTransferRemarks() {
		return fdFundTransferRemarks;
	}

	public void setFdFundTransferRemarks(String fdFundTransferRemarks) {
		this.fdFundTransferRemarks = fdFundTransferRemarks;
	}

}
