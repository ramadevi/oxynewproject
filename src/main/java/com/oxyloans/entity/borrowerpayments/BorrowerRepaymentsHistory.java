package com.oxyloans.entity.borrowerpayments;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity()
@Table(name = "borrower_repayments_history")
public class BorrowerRepaymentsHistory {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "borrower_payments_id", nullable = true)
	private Integer borrowerPaymentsId = 0;

	@Column(name = "amount_returned_to_repayment", nullable = true)
	private Double amountReturnedToRepayment;

	@Column(name = "created_on", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	@ManyToOne
	@JoinColumn(name = "borrower_payments_id", insertable = false, updatable = false)
	private BorrowerPayments borrowerPayments;

	public Integer getId() {
		return id;
	}

	public Integer getBorrowerPaymentsId() {
		return borrowerPaymentsId;
	}

	public Double getAmountReturnedToRepayment() {
		return amountReturnedToRepayment;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public BorrowerPayments getBorrowerPayments() {
		return borrowerPayments;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setBorrowerPaymentsId(Integer borrowerPaymentsId) {
		this.borrowerPaymentsId = borrowerPaymentsId;
	}

	public void setAmountReturnedToRepayment(Double amountReturnedToRepayment) {
		this.amountReturnedToRepayment = amountReturnedToRepayment;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public void setBorrowerPayments(BorrowerPayments borrowerPayments) {
		this.borrowerPayments = borrowerPayments;
	}

	
}
