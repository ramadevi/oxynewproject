package com.oxyloans.entity.borrowerpayments;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.user.User;

@Entity
@Table(name = "borrower_bank_details_history")
public class BorrowerBankDetailsHistory {

	public static enum FundingType {
		FD, SAVINGS, DAYS
	}
	
	public static enum PaymentsCollection {
		STUDENT, CONSULTANCY
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer Id;

	@Column(name = "user_id", nullable = true)
	public Integer userId;

	@Column(name = "bank_choosen", nullable = true)
	private String bankChoosen;

	@Column(name = "account_number", nullable = true)
	private String accountNumber;

	@Column(name = "name", nullable = true)
	private String name = "";

	@Column(name = "ifsc", nullable = true)
	private String ifsc = "";

	@Column(name = "city", nullable = true)
	private String city = "";

	@Column(name = "branch", nullable = true)
	private String branch = "";

	@Column(name = "bank_name", nullable = true)
	private String bankName = "";

	@Column(name = "lead_by", nullable = true)
	private String leadBy = "";

	@Column(name = "consultancy", nullable = true)
	private String consultancy = "";

	@Column(name = "country", nullable = true)
	private String country = "";

	@Column(name = "university", nullable = true)
	private String university = "";

	@Column(name = "student_mobile_number", nullable = true)
	private String studentMobileNumber = "";

	@Column(name = "roi", nullable = true)
	private Double roi = 0.0;

	@Column(name = "fd_amount", nullable = true)
	private Double fdAmount = 0.0;

	@Column(name = "funding_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private FundingType fundingType = FundingType.FD;

	@Column(name = "created_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	@Column(name = "payments_collection", nullable = true)
	@Enumerated(EnumType.STRING)
	private PaymentsCollection paymentsCollection = PaymentsCollection.STUDENT;

	@ManyToOne
	@JoinColumn(name = "user_id", insertable = false, updatable = false)
	private User user;

	public Integer getId() {
		return Id;
	}

	public Integer getUserId() {
		return userId;
	}

	public String getBankChoosen() {
		return bankChoosen;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getName() {
		return name;
	}

	public String getIfsc() {
		return ifsc;
	}

	public String getCity() {
		return city;
	}

	public String getBranch() {
		return branch;
	}

	public String getBankName() {
		return bankName;
	}

	public String getLeadBy() {
		return leadBy;
	}

	public String getConsultancy() {
		return consultancy;
	}

	public String getCountry() {
		return country;
	}

	public String getUniversity() {
		return university;
	}

	public String getStudentMobileNumber() {
		return studentMobileNumber;
	}

	public Double getRoi() {
		return roi;
	}

	public Double getFdAmount() {
		return fdAmount;
	}

	public FundingType getFundingType() {
		return fundingType;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public User getUser() {
		return user;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setBankChoosen(String bankChoosen) {
		this.bankChoosen = bankChoosen;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public void setLeadBy(String leadBy) {
		this.leadBy = leadBy;
	}

	public void setConsultancy(String consultancy) {
		this.consultancy = consultancy;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public void setStudentMobileNumber(String studentMobileNumber) {
		this.studentMobileNumber = studentMobileNumber;
	}

	public void setRoi(Double roi) {
		this.roi = roi;
	}

	public void setFdAmount(Double fdAmount) {
		this.fdAmount = fdAmount;
	}

	public void setFundingType(FundingType fundingType) {
		this.fundingType = fundingType;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public PaymentsCollection getPaymentsCollection() {
		return paymentsCollection;
	}

	public void setPaymentsCollection(PaymentsCollection paymentsCollection) {
		this.paymentsCollection = paymentsCollection;
	}

}
