package com.oxyloans.entity.borrowerpayments;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "borrower_payments_from_system")
public class BorrowerPaymentsFromSystem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "borrower_payments_id")
	private Integer borrowerPaymentsId;

	@Column(name = "amount")
	private Double amount;

	@Column(name = "created_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@ManyToOne
	@JoinColumn(name = "borrower_payments_id", insertable = false, updatable = false)
	private BorrowerPayments borrowerPayments;

	public Integer getId() {
		return id;
	}

	public Integer getBorrowerPaymentsId() {
		return borrowerPaymentsId;
	}

	public Double getAmount() {
		return amount;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setBorrowerPaymentsId(Integer borrowerPaymentsId) {
		this.borrowerPaymentsId = borrowerPaymentsId;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
