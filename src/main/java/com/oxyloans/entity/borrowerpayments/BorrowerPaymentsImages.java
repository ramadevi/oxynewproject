package com.oxyloans.entity.borrowerpayments;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "borrower_payments_images")
public class BorrowerPaymentsImages {

	public static enum DocumentStatus {
		UPLOADED, APPROVED, REJECTED
	}

	public static enum AccountType {
		HDFC, ICICI
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "borrower_payments_id", nullable = true)
	private Integer borrowerPaymentsId;

	@Column(name = "amount_paid", nullable = true)
	private Double amountPaid;

	@Column(name = "account_type", nullable = true)
	@Enumerated(EnumType.STRING)
	private AccountType accountType = AccountType.HDFC;

	@Column(name = "document_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private DocumentStatus documentStatus = DocumentStatus.UPLOADED;

	@Column(name = "document_upload_url", nullable = true)
	private String documentUploadUrl;

	@Column(name = "number_of_days_paid", nullable = true)
	private Integer numberOfDaysPaid=0;

	@Column(name = "approved_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date approvedOn;

	@Column(name = "created_on", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn = new Date();

	@Column(name = "invoice_url", nullable = true)
	private String invoiceUrl;

	@ManyToOne
	@JoinColumn(name = "borrower_payments_id", insertable = false, updatable = false)
	private BorrowerPayments borrowerPayments;

	public Integer getId() {
		return id;
	}

	public Integer getBorrowerPaymentsId() {
		return borrowerPaymentsId;
	}

	public Double getAmountPaid() {
		return amountPaid;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public DocumentStatus getDocumentStatus() {
		return documentStatus;
	}

	public String getDocumentUploadUrl() {
		return documentUploadUrl;
	}

	public Integer getNumberOfDaysPaid() {
		return numberOfDaysPaid;
	}

	public Date getApprovedOn() {
		return approvedOn;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public BorrowerPayments getBorrowerPayments() {
		return borrowerPayments;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setBorrowerPaymentsId(Integer borrowerPaymentsId) {
		this.borrowerPaymentsId = borrowerPaymentsId;
	}

	public void setAmountPaid(Double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public void setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus;
	}

	public void setDocumentUploadUrl(String documentUploadUrl) {
		this.documentUploadUrl = documentUploadUrl;
	}

	public void setNumberOfDaysPaid(Integer numberOfDaysPaid) {
		this.numberOfDaysPaid = numberOfDaysPaid;
	}

	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public void setBorrowerPayments(BorrowerPayments borrowerPayments) {
		this.borrowerPayments = borrowerPayments;
	}

	public String getInvoiceUrl() {
		return invoiceUrl;
	}

	public void setInvoiceUrl(String invoiceUrl) {
		this.invoiceUrl = invoiceUrl;
	}

}
