package com.oxyloans.entity.socialLogin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "social_login_details")
public class SocialLoginDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = true)
	private Integer userId;

	@Column(name = "google_user_id", nullable = true)
	private String googleUserId;

	@Column(name = "gmail_verified", nullable = false)
	private boolean gmailVerified = false;

	@Column(name = "facebook_user_id", nullable = true)
	private String facebookUserId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getGoogleUserId() {
		return googleUserId;
	}

	public void setGoogleUserId(String googleUserId) {
		this.googleUserId = googleUserId;
	}

	public boolean isGmailVerified() {
		return gmailVerified;
	}

	public void setGmailVerified(boolean gmailVerified) {
		this.gmailVerified = gmailVerified;
	}

	public String getFacebookUserId() {
		return facebookUserId;
	}

	public void setFacebookUserId(String facebookUserId) {
		this.facebookUserId = facebookUserId;
	}

}
