package com.oxyloans.entity.expertSeekers;

public class ExpertiseRequestDto {

	private String name;
	private String email;
	private String emailOtp;
	private String emailOtpSession;
	private String mobileNumber;
	private String mobileOtp;
	private String mobileOtpSession;
	private int advisorId;
	private String advisorMail;
	private String message;
	private String socialMediaUrl;
	private String adviseSeekersSalt;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailOtp() {
		return emailOtp;
	}

	public void setEmailOtp(String emailOtp) {
		this.emailOtp = emailOtp;
	}

	public String getEmailOtpSession() {
		return emailOtpSession;
	}

	public void setEmailOtpSession(String emailOtpSession) {
		this.emailOtpSession = emailOtpSession;
	}

	public String getMobileOtp() {
		return mobileOtp;
	}

	public void setMobileOtp(String mobileOtp) {
		this.mobileOtp = mobileOtp;
	}

	public String getMobileOtpSession() {
		return mobileOtpSession;
	}

	public void setMobileOtpSession(String mobileOtpSession) {
		this.mobileOtpSession = mobileOtpSession;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public int getAdvisorId() {
		return advisorId;
	}

	public void setAdvisorId(int advisorId) {
		this.advisorId = advisorId;
	}

	public String getAdvisorMail() {
		return advisorMail;
	}

	public void setAdvisorMail(String advisorMail) {
		this.advisorMail = advisorMail;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSocialMediaUrl() {
		return socialMediaUrl;
	}

	public void setSocialMediaUrl(String socialMediaUrl) {
		this.socialMediaUrl = socialMediaUrl;
	}

	public String getAdviseSeekersSalt() {
		return adviseSeekersSalt;
	}

	public void setAdviseSeekersSalt(String adviseSeekersSalt) {
		this.adviseSeekersSalt = adviseSeekersSalt;
	}

}
