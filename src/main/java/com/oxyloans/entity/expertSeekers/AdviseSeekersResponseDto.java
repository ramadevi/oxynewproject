package com.oxyloans.entity.expertSeekers;

import java.util.List;

import com.oxyloans.response.user.UniqueLendersResponseDto;

public class AdviseSeekersResponseDto {

	private int id;
	private String name;
	private String email;
	private String message;
	private String mobileNumber;
	private String contentAndFaqs;
	
	private List<UniqueLendersResponseDto> uniqueLendersResponse;
	private int countOfUniqueLenders;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getContentAndFaqs() {
		return contentAndFaqs;
	}

	public void setContentAndFaqs(String contentAndFaqs) {
		this.contentAndFaqs = contentAndFaqs;
	}

	public List<UniqueLendersResponseDto> getUniqueLendersResponse() {
		return uniqueLendersResponse;
	}

	public void setUniqueLendersResponse(List<UniqueLendersResponseDto> uniqueLendersResponse) {
		this.uniqueLendersResponse = uniqueLendersResponse;
	}

	public int getCountOfUniqueLenders() {
		return countOfUniqueLenders;
	}

	public void setCountOfUniqueLenders(int countOfUniqueLenders) {
		this.countOfUniqueLenders = countOfUniqueLenders;
	}

}
