package com.oxyloans.entity.expertSeekers;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "expert_advise_seekers")
public class ExpertAdviseSeekers {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "name", nullable = true)
	private String name;

	@Column(name = "mobile_number", nullable = false)
	private String mobileNumber;

	@Column(name = "mail_id", nullable = true)
	private String mailId;

	@Column(name = "advisor_id", nullable = true)
	private Integer advisorId;

	@Column(name = "advise_requested_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date adviseRequestedDate = new Date();

	@Column(name = "social_media_url", nullable = true)
	private String socialMediaUrl;

	@Column(name = "mobile_verified", nullable = true)
	private Boolean mobileVerified = false;

	@Column(name = "email_id_verified", nullable = true)
	private Boolean emailIdVerified = false;

	@Column(name = "lender_reference_id", nullable = true)
	private Integer lenderReferenceId;

	@Column(name = "seekers_faqs", nullable = true)
	private String seekersFaqs;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String isMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

	public Integer getAdvisorId() {
		return advisorId;
	}

	public void setAdvisorId(Integer advisorId) {
		this.advisorId = advisorId;
	}

	public Date getAdviseRequestedDate() {
		return adviseRequestedDate;
	}

	public void setAdviseRequestedDate(Date adviseRequestedDate) {
		this.adviseRequestedDate = adviseRequestedDate;
	}

	public Integer getLenderReferenceId() {
		return lenderReferenceId;
	}

	public void setLenderReferenceId(Integer lenderReferenceId) {
		this.lenderReferenceId = lenderReferenceId;
	}

	public String getSocialMediaUrl() {
		return socialMediaUrl;
	}

	public void setSocialMediaUrl(String socialMediaUrl) {
		this.socialMediaUrl = socialMediaUrl;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public String getSeekersFaqs() {
		return seekersFaqs;
	}

	public void setSeekersFaqs(String seekersFaqs) {
		this.seekersFaqs = seekersFaqs;
	}

	public Boolean getMobileVerified() {
		return mobileVerified;
	}

	public void setMobileVerified(Boolean mobileVerified) {
		this.mobileVerified = mobileVerified;
	}

	public Boolean getEmailIdVerified() {
		return emailIdVerified;
	}

	public void setEmailIdVerified(Boolean emailIdVerified) {
		this.emailIdVerified = emailIdVerified;
	}

}
