package com.oxyloans.entity.jpaspecification;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.oxyloans.entity.IEntity;
import com.oxyloans.entity.loan.OxyLoan;
import com.oxyloans.request.SearchRequestDto;
import com.oxyloans.request.SearchRequestDto.LogicalOperator;
import com.oxyloans.request.SearchRequestDto.Operator;

@Service("specificationProvider")
public class SpecificationProvider implements ISpecificationProvider {

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	private static final Logger logger = LoggerFactory.getLogger(SpecificationProvider.class);

	@Override
	public <T extends IEntity> Specification<T> construct(Class<T> type, SearchRequestDto criteria)
			throws ClassNotFoundException, IllegalAccessException, InvocationTargetException, NoSuchMethodException,
			InstantiationException {
		Specification<T> specs;
		try {
			specs = generate(type, criteria);
		} catch (NoSuchFieldException | SecurityException e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
		return specs;
	}

	public <T extends IEntity> Specification<T> generate(Class<T> type, SearchRequestDto filter)
			throws ClassNotFoundException, IllegalAccessException, InvocationTargetException, NoSuchMethodException,
			InstantiationException, NoSuchFieldException, SecurityException {
		LogicalOperator logicalOperator = filter.getLogicalOperator();
		if (logicalOperator != null) {
			Specification<T> leftSpec = generate(type, filter.getLeftOperand());
			Specification<T> rightSpec = generate(type, filter.getRightOperand());
			Specification<T> specs = null;
			switch (logicalOperator) {
			case AND:
				specs = Specification.where(leftSpec).and(rightSpec);
				break;
			case OR:
				specs = Specification.where(leftSpec).or(rightSpec);
				break;
			default:
				break;
			}

			return specs;
		} else {
			Specification<T> spec = identifySpecification(type, filter);
			return Specification.where(spec);
		}
	}

	@SuppressWarnings("unchecked")
	private <T extends IEntity> Specification<T> identifySpecification(Class<T> type, SearchRequestDto filter)
			throws ClassNotFoundException, IllegalAccessException, InvocationTargetException, NoSuchMethodException,
			InstantiationException, NoSuchFieldException, SecurityException {
		Specification<T> spec = null;
		Class<?> clazz = Class.forName(type.getName());
		boolean isJoin = false;
		int indexOf = StringUtils.indexOf(filter.getFieldName(), ".");
		Class<?> propertyType = getPropertyType(clazz, filter.getFieldName());
		if (indexOf != -1) {
			Class<?> innerPropertyType = PropertyUtils.getPropertyType(clazz.newInstance(),
					StringUtils.substring(filter.getFieldName(), 0, indexOf));
			if (innerPropertyType.isAssignableFrom(java.util.List.class)) {
				isJoin = true;
			}
		}

		if (String.class.getName().equals(propertyType.getName())) {

			if (filter.getFieldValue().getClass().isArray()) {
				spec = new StringLikeSpecification<T>(filter.getFieldName(),
						Arrays.asList((String[]) filter.getFieldValue()), filter.getOperator(), isJoin);
			} else if (filter.getFieldValue() instanceof Collection) {
				spec = new StringLikeSpecification<T>(filter.getFieldName(),
						(Collection<String>) filter.getFieldValue(), filter.getOperator(), isJoin);
			} else {
				spec = new StringLikeSpecification<T>(filter.getFieldName(), (String) filter.getFieldValue(),
						filter.getOperator(), isJoin);
			}
		} else if (Boolean.class.getName().equalsIgnoreCase(propertyType.getName())) {
			if (filter.getFieldValue() instanceof String) {
				if (filter.getFieldValue().toString().startsWith("T")
						|| filter.getFieldValue().toString().startsWith("t")) {
					filter.setFieldValue("true");
				} else if (filter.getFieldValue().toString().startsWith("F")
						|| filter.getFieldValue().toString().startsWith("f")) {
					filter.setFieldValue("false");
				}
				String bool = filter.getFieldValue().toString();
				if (bool.equals("true") || bool.equals("false"))
					spec = new BooleanSpecification<T>(filter.getFieldName(),
							Boolean.valueOf(filter.getFieldValue().toString()), Operator.EQUALS, isJoin);
			} else {
				spec = new BooleanSpecification<T>(filter.getFieldName(),
						Boolean.valueOf((boolean) filter.getFieldValue()), filter.getOperator(), isJoin);
			}
		} else if (Number.class.getName().equals(propertyType.getSuperclass().getName())) {

			if (filter.getFieldValue() instanceof Collection) {
				spec = indentifyNumberSpecification(filter, spec, propertyType,
						(Collection<Number>) filter.getFieldValue(), isJoin);
			} else {

				Double value = null;

				if (filter.getFieldValue() instanceof String) {
					value = Double.valueOf((String) filter.getFieldValue());
				} else {

					if (filter.getFieldValue() instanceof Integer) {
						value = ((Integer) filter.getFieldValue()).doubleValue();
					} else if (filter.getFieldValue() instanceof Long) {
						value = ((Long) filter.getFieldValue()).doubleValue();
					} else {
						value = (Double) filter.getFieldValue();
					}
				}
				spec = indentifyNumberSpecification(filter, spec, propertyType, value, isJoin);
			}
		} else if (Date.class.getName().equals(propertyType.getName())) {
			try {
				Date date = simpleDateFormat.parse((String) filter.getFieldValue());
				System.out.println(date);
				Date value = (Date) date;
				spec = new ComparableSpecification<T, Date>(filter.getFieldName(), value, filter.getOperator(), isJoin);
			} catch (Exception e) {
			}

		} else if (propertyType.isEnum()) {
			if (filter.getFieldValues() != null) {
				List<Object> elements = filter.getFieldValues();
				List<Enum<?>> enumValues = new ArrayList<>(elements.size());
				elements.forEach(x -> enumValues.add(Enum.valueOf((Class<? extends Enum>) propertyType, x.toString())));
				spec = new EnumSpecification<T>(filter.getFieldName(), enumValues, filter.getOperator(), isJoin);
			} else {
				Object value = filter.getFieldValue();
				if (value instanceof String) {
					value = (Enum<?>) getNewClassInstanceWithData(propertyType, (String) value);
				}
				spec = new EnumSpecification<T>(filter.getFieldName(), (Enum<?>) value, filter.getOperator(), isJoin);
			}
		}
		return spec;
	}

	private Class<?> getPropertyType(Class<?> clazz, String filterName)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, InstantiationException,
			NoSuchFieldException, SecurityException {

		Class<?> propertyType = null;
		int indexOf = StringUtils.indexOf(filterName, ".");

		if (indexOf == -1) {
			propertyType = PropertyUtils.getPropertyType(clazz.newInstance(), filterName);
		} else {
			String innerPropertyName = StringUtils.substring(filterName, 0, indexOf);
			Class<?> innerPropertyType = PropertyUtils.getPropertyType(clazz.newInstance(), innerPropertyName);
			logger.info("Found the class name for inner property {} as {} in class {}", innerPropertyName,
					innerPropertyType, clazz);

			if (innerPropertyType.isAssignableFrom(java.util.List.class)) {
				Field listField = clazz.getDeclaredField(innerPropertyName);
				ParameterizedType listType = (ParameterizedType) listField.getGenericType();
				innerPropertyType = (Class<?>) listType.getActualTypeArguments()[0];
			}
			propertyType = getPropertyType(innerPropertyType, StringUtils.substring(filterName, indexOf + 1));
		}

		return propertyType;
	}

	private <T extends IEntity> Specification<T> indentifyNumberSpecification(SearchRequestDto filter,
			Specification<T> spec, Class<?> propertyType, Double value, boolean isJoin) {

		if (Long.class.getName().equals(propertyType.getName())) {
			spec = new NumberSpecification<T, Long>(filter.getFieldName(), value == null ? null : value.longValue(),
					filter.getOperator(), isJoin);
		} else if (Integer.class.getName().equals(propertyType.getName())) {
			spec = new NumberSpecification<T, Integer>(filter.getFieldName(), value == null ? null : value.intValue(),
					filter.getOperator(), isJoin);
		} else if (Float.class.getName().equals(propertyType.getName())) {
			spec = new NumberSpecification<T, Float>(filter.getFieldName(), value == null ? null : value.floatValue(),
					filter.getOperator(), isJoin);
		} else if (Double.class.getName().equals(propertyType.getName())) {
			spec = new NumberSpecification<T, Double>(filter.getFieldName(), value, filter.getOperator(), isJoin);
		} else if (BigDecimal.class.getName().equals(propertyType.getName())) {
			spec = new NumberSpecification<T, BigDecimal>(filter.getFieldName(), value, filter.getOperator(), isJoin);
		}

		return spec;
	}

	private <T extends IEntity> Specification<T> indentifyNumberSpecification(SearchRequestDto filter,
			Specification<T> spec, Class<?> propertyType, Collection<Number> values, boolean isJoin) {

		if (Long.class.getName().equals(propertyType.getName())) {
			spec = new NumberSpecification<T, Long>(filter.getFieldName(), values, filter.getOperator(), isJoin);
		} else if (Integer.class.getName().equals(propertyType.getName())) {
			spec = new NumberSpecification<T, Integer>(filter.getFieldName(), values, filter.getOperator(), isJoin);
		} else if (Float.class.getName().equals(propertyType.getName())) {
			spec = new NumberSpecification<T, Float>(filter.getFieldName(), values, filter.getOperator(), isJoin);
		} else if (Double.class.getName().equals(propertyType.getName())) {
			spec = new NumberSpecification<T, Double>(filter.getFieldName(), values, filter.getOperator(), isJoin);
		} else if (BigDecimal.class.getName().equals(propertyType.getName())) {
			spec = new NumberSpecification<T, BigDecimal>(filter.getFieldName(), values, filter.getOperator(), isJoin);
		}

		return spec;
	}

	public static <T> T getNewClassInstanceWithData(Class<T> clazz, String data) {
		T value = null;
		Gson gson = new Gson();
		try {
			value = gson.fromJson(data, clazz);
		} catch (Exception jsonException) {
			logger.debug("Json Data Parsing Problem", jsonException);
			throw new RuntimeException("Internal Server Error : Json Data Parsing Problem");
		}
		return value;
	}

	public static void main(String[] args) throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, InstantiationException, NoSuchFieldException, SecurityException {
		SpecificationProvider s = new SpecificationProvider();
		Class<?> propertyType = s.getPropertyType(OxyLoan.class, "lenderFeePaid");
		System.out.println(propertyType.getName());
		System.out.println(propertyType.getSuperclass().getName());
	}

}