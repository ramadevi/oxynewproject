package com.oxyloans.entity.jpaspecification;

import java.lang.reflect.InvocationTargetException;

import org.springframework.data.jpa.domain.Specification;

import com.oxyloans.entity.IEntity;
import com.oxyloans.request.SearchRequestDto;

public interface ISpecificationProvider {

	public <T extends IEntity> Specification<T> construct(Class<T> type, SearchRequestDto citeria) throws ClassNotFoundException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, InstantiationException;

}
