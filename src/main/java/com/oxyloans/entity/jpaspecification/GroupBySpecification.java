package com.oxyloans.entity.jpaspecification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import com.oxyloans.entity.IEntity;

public class GroupBySpecification<T extends IEntity> extends AbstractSpecification implements Specification<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String attributeName;
	
	private boolean isJoin;
	
	private boolean multiGroup;
	
	private List<String> attributeNames;

	public GroupBySpecification(String attributeName,boolean isJoin) {
		this.attributeName = attributeName;
		this.isJoin=isJoin;
	}
	
	public GroupBySpecification(List<String> attributeNames,boolean multiGroup,boolean isJoin) {
		this.attributeNames = attributeNames;
		this.multiGroup =  multiGroup;
		this.isJoin=isJoin;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		if (StringUtils.isNotEmpty(attributeName)) {
			Path<T> path = null;
			if(isJoin) {
				int index = StringUtils.indexOf(attributeName, ".");
				Join join = root.join(attributeName.substring(0, index));
				path = getPath(join, attributeName.substring(index+1, attributeName.length()));
			} else {
				path = getPath(root, this.attributeName);
			}
//			getPath(join, attributeName.substring(index+1, attributeName.length()));
//			query.groupBy(root.<String>get(this.attributeName));
			query.groupBy(path);
		} else if(!attributeNames.isEmpty()&&multiGroup) {
			
//			List<Expression<T>> pathList = new ArrayList<Expression<T>>();
//			Expression[] array = new Path[pathList.size()];
//			
//			for(int i=0;i<array.length;i++)
//			{
//				array[i] = pathList.get(i);
//			}
//			/*for(String attributeName :  attributeNames) {
//				Path<T> path = getPath(root, attributeName);
//				array[path];
//				pathList.add(path);
//			}
//			Object[] array = pathList.toArray();*/
//			query.groupBy(array);
			List<Expression<T>> pathList = new ArrayList<Expression<T>>();
			for(String attributeName :  attributeNames) {
				Path<T> path = getPath(root, attributeName);
				pathList.add(path);
			}
			Expression[] array = new Path[pathList.size()];
			for(int i=0;i<array.length;i++)
			{
				array[i] = pathList.get(i);
			}
			query.groupBy(array);
		}
		
		return null;
	}
	
	private Path<T> getPath(Path<T> root, String propertyName) {
		int indexOf = StringUtils.indexOf(propertyName, ".");
		if(indexOf == -1) {
			return root.<T>get(propertyName);
		} else {
			Path<? extends IEntity> innerPath = traversePath(root, propertyName);
			return innerPath.<T>get(StringUtils.substring(propertyName, StringUtils.lastIndexOf(propertyName, ".") + 1));
		}
	}
}