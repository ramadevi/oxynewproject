package com.oxyloans.entity.jpaspecification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import com.oxyloans.entity.IEntity;
import com.oxyloans.request.SearchRequestDto.Operator;

public class ComparableSpecification<T extends IEntity, N extends Comparable<N>> extends AbstractSpecification implements Specification<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String attributeName;
	
	private N attributeValue;
	
	private Operator operation;
	
	private boolean isJoin;

	public ComparableSpecification(String attributeName, N value) {
		this(attributeName, value, Operator.EQUALS,false);
	}

	public ComparableSpecification(String attributeName, N value, Operator operation, boolean isJoin) {
		this.attributeName = attributeName;
		this.operation = operation;
		this.attributeValue = value;
		this.isJoin = isJoin;
	}
	
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		if (StringUtils.isNotEmpty(attributeName)) {
			Path<N> path = null;
			if(isJoin) {
				int index = StringUtils.indexOf(attributeName, ".");
				Join join = root.join(attributeName.substring(0, index));
				path = getPath(join, attributeName.substring(index+1, attributeName.length()));
			} else {
				path = getPath(root, this.attributeName);
			}
			switch (this.operation) {
			case EQUALS:
				return cb.equal(path, attributeValue);
			case NOT_EQUALS:
				return cb.notEqual(path, attributeValue);
			case GT:
				return cb.greaterThan(path, attributeValue);
			case GTE:
				return cb.greaterThanOrEqualTo(path, attributeValue);
			case LT:
				return cb.lessThan(path, attributeValue);
			case LTE:
				return cb.lessThanOrEqualTo(path, attributeValue);
			//TODO need to implement BETWEEN
			case NULL:
				return cb.isNull(path);
			case NOT_NULL:
				return cb.isNotNull(path);	
			default:
				break;
			}
		}
		return null;
	}
	
	private Path<N> getPath(Path<T> root, String propertyName) {
		int indexOf = StringUtils.indexOf(propertyName, ".");
		if(indexOf == -1) {
			return root.<N>get(propertyName);
		} else {
			Path<? extends IEntity> innerPath = traversePath(root, propertyName);
			return innerPath.<N>get(StringUtils.substring(propertyName, StringUtils.lastIndexOf(propertyName, ".") + 1));
		}
	}

}