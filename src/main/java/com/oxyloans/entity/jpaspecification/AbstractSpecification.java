package com.oxyloans.entity.jpaspecification;

import javax.persistence.criteria.Path;

import org.apache.commons.lang3.StringUtils;

import com.oxyloans.entity.IEntity;


public class AbstractSpecification {
	
	protected Path<? extends IEntity> traversePath(Path<? extends IEntity> root, String propertyName) {
		
		int indexOf = StringUtils.indexOf(propertyName, ".");
		
		if (indexOf == -1) {
			return root;
		} else {
			return traversePath(root.<IEntity>get(StringUtils.substring(propertyName, 0, indexOf)), StringUtils.substring(propertyName, indexOf + 1));
		}
	}
}