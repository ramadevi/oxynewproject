package com.oxyloans.entity.jpaspecification;

import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import com.oxyloans.entity.IEntity;
import com.oxyloans.request.SearchRequestDto.Operator;

public class StringLikeSpecification<T extends IEntity> extends AbstractSpecification implements Specification<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String attributeName;
	
	private String attributeValue;
	
	private Collection<String> attributeValues;
	
	private Operator operator;
	
	private boolean isJoin;

	public StringLikeSpecification(String attributeName, Object value) {
		this(attributeName, (String) value, Operator.LIKE,false);
	}

	public StringLikeSpecification(String attributeName, String value, Operator operation, boolean isJoin) {
		this.attributeName = attributeName;
		this.operator = operation;
		this.isJoin = isJoin;
		switch (operation) {
		case LIKE:
		case ILIKE:
		case NOT_LIKE:
			this.attributeValue = "%" + value + "%";
			break;
		case EQUALS:
		case NOT_EQUALS:
			this.attributeValue = value;
			break;
		case STARTS_WITH :
			this.attributeValue = value + "%";
			break;
		case NULL:
		case NOT_NULL:
			break;
		default:
			break;
		}
	}
	
	public StringLikeSpecification(String attributeName, Collection<String> values, Operator operation, boolean isJoin) {
		this.attributeName = attributeName;
		this.operator = operation;
		this.isJoin = isJoin;
		switch (operation) {
		case IN:
		case NOT_IN:
			this.attributeValues = values;
			break;
		default:
			break;
		}
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		if (StringUtils.isNotEmpty(attributeName)) {
			Path<String> path = null;
			if(isJoin) {
				int index = StringUtils.indexOf(attributeName, ".");
				Join join = root.join(attributeName.substring(0, index));
				path = getPath(join, attributeName.substring(index+1, attributeName.length()));
			} else {
				path = getPath(root, this.attributeName);
			}
			switch (this.operator) {
			case LIKE:
				return cb.like(path, attributeValue);
			case ILIKE:
				return cb.like(path, attributeValue);
			case NOT_LIKE:
				return cb.notLike(path, attributeValue);
			case EQUALS:
				return cb.equal(path, attributeValue);
			case NOT_EQUALS:
				return cb.notEqual(path, attributeValue);
			case NULL:
				return cb.isNull(path);
			case NOT_NULL:
				return cb.isNotNull(path);
			case IN:
				return path.in(attributeValues);
			case NOT_IN:
				return path.in(attributeValues).not();
			case STARTS_WITH:
				return cb.like(path, attributeValue);
			default:
				break;
			}
		}
		return null;
	}
	
	private Path<String> getPath(Path<T> root, String propertyName) {
		int indexOf = StringUtils.indexOf(propertyName, ".");
		if(indexOf == -1) {
			return root.<String>get(propertyName);
		} else {
			Path<? extends IEntity> innerPath = traversePath(root, propertyName);
			return innerPath.<String>get(StringUtils.substring(propertyName, StringUtils.lastIndexOf(propertyName, ".") + 1));
		}
	}

}