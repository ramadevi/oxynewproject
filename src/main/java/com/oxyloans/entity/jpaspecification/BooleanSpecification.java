package com.oxyloans.entity.jpaspecification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import com.oxyloans.entity.IEntity;
import com.oxyloans.request.SearchRequestDto;
import com.oxyloans.request.SearchRequestDto.Operator;

public class BooleanSpecification<T extends IEntity> extends AbstractSpecification implements Specification<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String attributeName;
	
	private Boolean attributeValue;
	
	private Operator operator;
	
	private boolean isJoin;

	public BooleanSpecification(String attributeName, Boolean value) {
		this(attributeName, value, Operator.EQUALS,false);
	}

	public BooleanSpecification(String attributeName, Boolean value, Operator operation, boolean isJoin) {
		this.attributeName = attributeName;
		this.operator = operation;
		this.attributeValue = value;
		this.isJoin = isJoin;
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		if (StringUtils.isNotEmpty(attributeName)) {
			Path<Boolean> path = null;
			if(isJoin) {
				int index = StringUtils.indexOf(attributeName, ".");
				Join join = root.join(attributeName.substring(0, index));
				path = getPath(join, attributeName.substring(index+1, attributeName.length()));
			} else {
				path = getPath(root, this.attributeName);
			}
			switch (this.operator) {
			case EQUALS:
				return cb.equal(path, attributeValue);
			case NOT_EQUALS:
				return cb.notEqual(path, attributeValue);
			case NULL:
				return cb.isNull(path);
			case NOT_NULL:
				return cb.isNotNull(path);	
			default:
				break;
			}
		}
		return null;
	}
	
	private Path<Boolean> getPath(Path<T> root, String propertyName) {
		int indexOf = StringUtils.indexOf(propertyName, ".");
		if(indexOf == -1) {
			return root.<Boolean>get(propertyName);
		} else {
			Path<? extends IEntity> innerPath = traversePath(root, propertyName);
			return innerPath.<Boolean>get(StringUtils.substring(propertyName, StringUtils.lastIndexOf(propertyName, ".") + 1));
		}
	}
}