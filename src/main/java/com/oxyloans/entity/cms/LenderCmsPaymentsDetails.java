package com.oxyloans.entity.cms;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.icici.upi.ICICIUpiTransactionDetails.TransactionStatus;

@Entity
@Table(name = "lender_cms_payments_details")
public class LenderCmsPaymentsDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = " lender_cms_payments_id", nullable = true)
	private Integer lenderCmsPaymentsId;

	@Column(name = "lender_id", nullable = true)
	private Integer lenderId;

	@Column(name = "amount", nullable = true)
	private double amount = 0.0;

	@Column(name = "transaction_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private TransactionStatus transactionStatus = TransactionStatus.INITIATED;

	@Column(name = "days_difference", nullable = true)
	private Integer daysDifference;

	@Column(name = "remarks", nullable = true)
	private String remarks;

	@Column(name = "read_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date readOn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLenderId() {
		return lenderId;
	}

	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public TransactionStatus getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(TransactionStatus transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public Integer getLenderCmsPaymentsId() {
		return lenderCmsPaymentsId;
	}

	public void setLenderCmsPaymentsId(Integer lenderCmsPaymentsId) {
		this.lenderCmsPaymentsId = lenderCmsPaymentsId;
	}

	public Integer getDaysDifference() {
		return daysDifference;
	}

	public void setDaysDifference(Integer daysDifference) {
		this.daysDifference = daysDifference;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getReadOn() {
		return readOn;
	}

	public void setReadOn(Date readOn) {
		this.readOn = readOn;
	}

}
