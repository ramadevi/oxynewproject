package com.oxyloans.entity.cms;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "oxy_cms_loans")
public class OxyCmsLoans {

	public enum FileExecutionStatus {
		INITIATED, EXECUTED, FAILED, MOVEDTOS3,ONHOLD,OLD
	}

	public enum BorrowerCameFrom {
		OXYLOANS, PARTNER
	}

	public enum TransactionStatusBorrowerFee {
		INITIATED, EXECUTED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "user_id", nullable = true)
	private Integer userId;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId;

	@Column(name = "transaction_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionDate;

	@Column(name = "amount", nullable = true)
	private double amount = 0.0;

	@Column(name = "file_name", nullable = true)
	private String fileName;

	@Column(name = "transaction_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private FileExecutionStatus transactionStatus = FileExecutionStatus.INITIATED;

	@Column(name = "borrower_came_from", nullable = true)
	@Enumerated(EnumType.STRING)
	private BorrowerCameFrom borrowerCameFrom = BorrowerCameFrom.OXYLOANS;

	@Column(name = "borrower_fee", nullable = true)
	private Double borrowerFee = 0.0;

	@Column(name = "file_name_borrowerfee", nullable = true)
	private String fileNameBorrowerfee;

	@Column(name = "transaction_status_borrower_fee", nullable = true)
	@Enumerated(EnumType.STRING)
	private TransactionStatusBorrowerFee transactionStatusBorrowerFee = TransactionStatusBorrowerFee.INITIATED;

	@Column(name = "loan_executed_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date loanExecutedOn;

	@Column(name = "fee_executed_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date feeExecutedOn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public FileExecutionStatus getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(FileExecutionStatus transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public BorrowerCameFrom getBorrowerCameFrom() {
		return borrowerCameFrom;
	}

	public void setBorrowerCameFrom(BorrowerCameFrom borrowerCameFrom) {
		this.borrowerCameFrom = borrowerCameFrom;
	}

	public Double getBorrowerFee() {
		return borrowerFee;
	}

	public void setBorrowerFee(Double borrowerFee) {
		this.borrowerFee = borrowerFee;
	}

	public String getFileNameBorrowerfee() {
		return fileNameBorrowerfee;
	}

	public void setFileNameBorrowerfee(String fileNameBorrowerfee) {
		this.fileNameBorrowerfee = fileNameBorrowerfee;
	}

	public TransactionStatusBorrowerFee getTransactionStatusBorrowerFee() {
		return transactionStatusBorrowerFee;
	}

	public void setTransactionStatusBorrowerFee(TransactionStatusBorrowerFee transactionStatusBorrowerFee) {
		this.transactionStatusBorrowerFee = transactionStatusBorrowerFee;
	}

	public Date getLoanExecutedOn() {
		return loanExecutedOn;
	}

	public void setLoanExecutedOn(Date loanExecutedOn) {
		this.loanExecutedOn = loanExecutedOn;
	}

	public Date getFeeExecutedOn() {
		return feeExecutedOn;
	}

	public void setFeeExecutedOn(Date feeExecutedOn) {
		this.feeExecutedOn = feeExecutedOn;
	}

}
