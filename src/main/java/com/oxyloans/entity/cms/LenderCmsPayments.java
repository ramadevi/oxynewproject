package com.oxyloans.entity.cms;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.oxyloans.entity.LenderReferenceDetails.ReferralBonusReading;
import com.oxyloans.entity.cms.OxyCmsLoans.FileExecutionStatus;

@Entity
@Table(name = "lender_cms_payments")
public class LenderCmsPayments {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "deal_id", nullable = true)
	private Integer dealId;

	@Column(name = "payment_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date paymentDate;

	@Column(name = "total_amount", nullable = true)
	private double totalAmount = 0.0;

	@Column(name = "file_name", nullable = true)
	private String fileName;

	@Column(name = "file_execution_status", nullable = true)
	@Enumerated(EnumType.STRING)
	private FileExecutionStatus fileExecutionsSatus = FileExecutionStatus.INITIATED;

	@Column(name = "s3_download_link", nullable = true)
	private String s3_download_link;

	@Column(name = "lender_returns_type", nullable = true)
	private String lenderReturnsType;

	@Column(name = "user_id", nullable = true)
	private Integer userId = 0;

	@Column(name = "borrower_payments_id", nullable = true)
	private Integer borrowerPaymentsId = 0;

	@Column(name = "created_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "lender_scrow_id", nullable = true)
	private Integer lenderScrowId;
	
	@OneToMany(mappedBy = "lenderCmsPayments", cascade = CascadeType.ALL)
	private List<ReferralBonusReading> referralBonusReading = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public FileExecutionStatus getFileExecutionsSatus() {
		return fileExecutionsSatus;
	}

	public void setFileExecutionsSatus(FileExecutionStatus fileExecutionsSatus) {
		this.fileExecutionsSatus = fileExecutionsSatus;
	}

	public String getS3_download_link() {
		return s3_download_link;
	}

	public void setS3_download_link(String s3_download_link) {
		this.s3_download_link = s3_download_link;
	}

	public String getLenderReturnsType() {
		return lenderReturnsType;
	}

	public void setLenderReturnsType(String lenderReturnsType) {
		this.lenderReturnsType = lenderReturnsType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getBorrowerPaymentsId() {
		return borrowerPaymentsId;
	}

	public void setBorrowerPaymentsId(Integer borrowerPaymentsId) {
		this.borrowerPaymentsId = borrowerPaymentsId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getLenderScrowId() {
		return lenderScrowId;
	}

	public void setLenderScrowId(Integer lenderScrowId) {
		this.lenderScrowId = lenderScrowId;
	}

	public List<ReferralBonusReading> getReferralBonusReading() {
		return referralBonusReading;
	}

	public void setReferralBonusReading(List<ReferralBonusReading> referralBonusReading) {
		this.referralBonusReading = referralBonusReading;
	}

}
