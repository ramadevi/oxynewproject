package com.oxyloans.util.pincodeutil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Component;

@Component
public class PinCodeUtil {

	private Client client = ClientBuilder.newClient().register(MultiPartFeature.class).register(MultiPartWriter.class);

	public PincodeResultes getDetails(String pincode) throws ParseException {

		WebTarget webTarget = client.target("https://api.postalpincode.in");
		webTarget = webTarget.path("/pincode/" + pincode);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		JSONArray pinResponce = response.readEntity(JSONArray.class);
		
		List<PinCodeResponce> ListofPinCodeResponce=new ArrayList<PinCodeResponce>();
		
		LinkedHashMap<String,ArrayList> totalResponce=(LinkedHashMap<String,ArrayList>) pinResponce.get(0);
	    ArrayList<LinkedHashMap<String,String>> listOfPincodes=totalResponce.get("PostOffice");
	   
	    if(listOfPincodes!=null) {
	    for (int i = 0; i < listOfPincodes.size(); i++) {
		  LinkedHashMap<String,String> onePinResponce=listOfPincodes.get(i);
		    PinCodeResponce pinCodeResponce=new PinCodeResponce();
		    
		    pinCodeResponce.setBlock(onePinResponce.get("Block"));
		    pinCodeResponce.setPincode(onePinResponce.get("Pincode"));
		    ListofPinCodeResponce.add(pinCodeResponce);
		      	
         }
	    
	    PincodeResultes results=new PincodeResultes();
	    results.setPinresults(ListofPinCodeResponce);
	    results.setCity(listOfPincodes.get(0).get("District"));
	    results.setState(listOfPincodes.get(0).get("State"));
	    results.setSize(ListofPinCodeResponce.size());
		return results;
	    }else {
	    	PincodeResultes results=new PincodeResultes();
		    results.setSize(0);
		    results.setCity("");
		    results.setState("");
			return results;
	    }
	}

}
