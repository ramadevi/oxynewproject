/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxyloans.util.dateutil;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Component;

/**
 *
 * @author USER
 */
@Component
public class DateUtil {

    public Timestamp getTimestamp() {
        DateFormat df = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
        DateFormat df1 = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
        //df.setTimeZone(TimeZone.getTimeZone("EST5EDT"));
        //df1.setTimeZone(TimeZone.getTimeZone("EST5EDT"));
        Timestamp date = null;
        try {
            date = new Timestamp(df1.parse(df.format(new java.util.Date())).getTime());
        } catch (Exception e) {

            e.printStackTrace();
        }
        return date;
    }

    public String getHHmmMMddyyyyDateFormat() {
        String date = null;
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HHmmMMddyyyy");
            LocalDateTime now = LocalDateTime.now();
            date = dtf.format(now);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     *
     * @return
     */
    public Date getTodayDate() {
        SimpleDateFormat formatterOut = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        //dateFormat.setTimeZone(TimeZone.getTimeZone("EST5EDT"));
        Calendar cal = Calendar.getInstance();
        String todayDateString = dateFormat.format(cal.getTime());
        System.out.println(todayDateString); //2014/08/06 16:00:22
        Date todayDate = parseDate(formatStringDate(todayDateString), formatterOut);
        System.out.println("todayDate:" + todayDate); //2014/08/06 16:00:22
        return todayDate;
    }

    /**
     *
     * @return
     */
    public Date getYesterdayDate() {
        SimpleDateFormat formatterOut = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        //dateFormat.setTimeZone(TimeZone.getTimeZone("EST5EDT"));
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String todayDateString = dateFormat.format(cal.getTime());
        System.out.println(todayDateString); //2014/08/06 16:00:22
        Date todayDate = parseDate(formatStringDate(todayDateString), formatterOut);
        System.out.println("todayDate:" + todayDate); //2014/08/06 16:00:22
        return todayDate;
    }

    /**
     *
     * @param sDate
     * @return
     */
    public String formatStringDate(String sDate) {
        SimpleDateFormat formatterIn = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatterOut = new SimpleDateFormat("yyyy-MM-dd");

        String newformat = parseDate(sDate, formatterIn, formatterOut);

        return (newformat);
    }

    private String parseDate(String sDate, SimpleDateFormat formatterIn, SimpleDateFormat formatterOut) {
        String newformat = "";
        try {
            java.util.Date oDate = formatterIn.parse(sDate); //parse the string to a date
            newformat = formatterOut.format(oDate); //create new String format
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (newformat);
    }

    private java.sql.Date parseDate(String sDate, SimpleDateFormat formatter) {
        java.sql.Date sqlDate = null;

        try {
            java.util.Date oDate = formatter.parse(sDate); //parse the string to a date
            long time = oDate.getTime(); //get the time
            sqlDate = new java.sql.Date(time); //set the sql date
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return (sqlDate);
    }

    /**
     *
     * @param inputDate
     * @return
     */
    public LocalDate formatLongDateStringToLocalDate(String inputDate) {
        System.out.println("formatLongDateStringToLocalDate:inputDate:" + inputDate);
        DateTimeFormatter f = DateTimeFormatter.ofPattern("E MMM dd HH:mm:ss z uuuu")
                .withLocale(Locale.US);
        ZonedDateTime zdt = ZonedDateTime.parse(inputDate, f);
        LocalDate ld = zdt.toLocalDate();
//        DateTimeFormatter fLocalDate = DateTimeFormatter.ofPattern("dd-/dd/uuuu");
//        String output = ld.format(fLocalDate);
        return ld;
    }

    public LocalDate formatDateStringToLocalDate(String inputDate) {
        DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate ld = LocalDate.parse(inputDate, f);
        return ld;
    }

    public LocalDate getTodayWithLocalDate() {
        return LocalDate.now();
    }

    public String getCurrentTimeHoursAndMinutesUsingDate() {
        java.util.Date date = new java.util.Date();
        String strDateFormat = "HH_mm";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
        String formattedDate = dateFormat.format(date);
        System.out.println("Current time of the day using Date - 24 hour format: " + formattedDate);
        return formattedDate;
    }

    public String getCurrentTimeUsingDate() {
        java.util.Date date = new java.util.Date();
        String strDateFormat = "hh:mm:ss a";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
        String formattedDate = dateFormat.format(date);
        System.out.println("Current time of the day using Date - 12 hour format: " + formattedDate);
        return formattedDate;
    }

    public String getCurrentTimeUsingCalendar() {
        Calendar cal = Calendar.getInstance();
        java.util.Date date = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String formattedDate = dateFormat.format(date);
        System.out.println("Current time of the day using Calendar - 24 hour format: " + formattedDate);
        return formattedDate;
    }

    public String getDateFormat2() {
        String date = null;
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            LocalDateTime now = LocalDateTime.now();
            date = dtf.format(now);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     *
     * @param dateInString
     * @return
     */
    public Date convertStringToDate(String dateInString) {
        java.sql.Date dtResult = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date = formatter.parse(dateInString);
            dtResult = new java.sql.Date(date.getTime());
        } catch (ParseException ex) {
            Logger.getLogger(DateUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dtResult;
    }

    public Date convertUSDateStringToSqlDate(String inputDate) {
        java.sql.Date convertedDate = null;
        String chngdDate = "";
        try {
            java.util.Date parsedDt = new java.util.Date();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

            parsedDt = sdf.parse(inputDate);

            sdf.applyPattern("yyyy-MM-dd");
            chngdDate = sdf.format(parsedDt);
            //System.out.println("Changed Date: " + chngdDate);

            SimpleDateFormat sdfOut = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date = sdfOut.parse(chngdDate);

            //System.out.println("Util Date: " + date);
            convertedDate = new java.sql.Date(date.getTime());
        } catch (ParseException ex) {
            Logger.getLogger(DateUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        //System.out.println("convertedDate: " + convertedDate);
        return convertedDate;
    }

    public Timestamp convertLongDateStringToSqlTimestamp(String dateInString) {
        java.sql.Timestamp dtResult = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
            java.util.Date date = formatter.parse(dateInString);
            dtResult = new java.sql.Timestamp(date.getTime());
        } catch (ParseException ex) {
            Logger.getLogger(DateUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dtResult;
    }

    public java.util.Date convertStringToLongDate(String dateInString) {
        java.util.Date dtResult = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date = formatter.parse(dateInString);
//            dtResult = new java.sql.Date(date.getTime());

            SimpleDateFormat formatterOut = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
            dtResult = formatterOut.parse(formatterOut.format(date));
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return dtResult;
    }

    public String convertStringToUnitedStatesDateFormat(String dateInString) {
        String newformat = "";
        try {
            SimpleDateFormat formatterIn = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatterOut = new SimpleDateFormat("MM/dd/yyyy");

            java.util.Date oDate = formatterIn.parse(dateInString); //parse the string to a date
            newformat = formatterOut.format(oDate); //create new String format

        } catch (ParseException ex) {
            Logger.getLogger(DateUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return newformat;
    }
    public String convertStringToUnitedStatesDateTimeFormat(String dateInString) {
        String newformat = "";
        try {
            SimpleDateFormat formatterIn = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat formatterOut = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

            java.util.Date oDate = formatterIn.parse(dateInString); //parse the string to a date
            newformat = formatterOut.format(oDate); //create new String format

        } catch (ParseException ex) {
            Logger.getLogger(DateUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return newformat;
    }

    public LocalDate convertStringToUnitedStatesDate(String dateInString) {
        DateTimeFormatter f = DateTimeFormatter.ofPattern("MM/dd/yyyy")
                .withLocale(Locale.US);
        LocalDate ld = LocalDate.parse(dateInString, f);
        return ld;
    }

    public LocalDate formatLongToDateTime(long time) {
//        DateFormat sdf = new SimpleDateFormat("E MMM dd HH:mm:ss z uuuu");

        DateTimeFormatter f = DateTimeFormatter.ofPattern("E MMM dd HH:mm:ss z uuuu")
                .withLocale(Locale.US);
        ZonedDateTime zdt = ZonedDateTime.parse(new Date(time).toString(), f);
        LocalDate ld = zdt.toLocalDate();
        return ld;
//        return sdf.format(new Date(time));
    }

    /**
     *
     * @return
     */
    public void formatDate(final String inputDate) throws Exception {
        SimpleDateFormat ipSdf = new SimpleDateFormat("MM/dd/yyyy");
        DateFormat opSdf = new SimpleDateFormat("yyyy-MM-dd");

        java.util.Date ipDate = ipSdf.parse(inputDate);

        String opDate = opSdf.format(ipDate);

        System.out.println("Current time of the day using Calendar - 24 hour format: " + opDate);

    }
    
    public static String convertddmmyyyyhhmmsstoddmmyyyy(String str) {
		SimpleDateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

		DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		java.util.Date date = null;
		String returnString=null;
		try {
			date = inputFormat.parse(str);
			returnString=sdf.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnString;
	}
    
    public static java.util.Date convertStrYYYYMMDDToDate(String str) {
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");

		java.util.Date date = null;
		try {
			date = inputFormat.parse(str);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}
    
    public static void wait(int ms){
        try
        {
            Thread.sleep(ms);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }
    
    public static String convertyyyymmddhhmmsstoddmmyyyy(String str) {
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date date = null;
		String returnString=null;
		try {
			date = inputFormat.parse(str);
			returnString=sdf.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnString;
	}


}
