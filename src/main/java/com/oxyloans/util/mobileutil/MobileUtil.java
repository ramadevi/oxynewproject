package com.oxyloans.util.mobileutil;

import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.mobile.twoFactor.MobileOtpSendResponse;
import com.oxyloans.mobile.twoFactor.MobileOtpSentFailedException;
import com.oxyloans.mobile.twoFactor.MobileOtpVerifyResponse;

@Component
public class MobileUtil {

	private static final Logger logger = LogManager.getLogger(MobileUtil.class);

	@Value("${twoFactorHost}")
	private String twoFactorHost;

	@Value("${twoFactorAPIKey}")
	private String twoFactorAPIKey;

	@Value("${twoFactorVerifyPath}")
	private String twoFactorVerifyPath;

	@Value("${twoFactorTempalteName}")
	private String twoFactorTempalteName;

	private String sendOtpUrl;

	private String verifyOtpUrl;

	private Client client = ClientBuilder.newClient().register(MultiPartFeature.class).register(MultiPartWriter.class);

	public MobileUtil() {
	}

	@PostConstruct
	public void smsUrls() {
		this.sendOtpUrl = twoFactorHost + twoFactorAPIKey + "/SMS/{phoneNumber}";
		this.verifyOtpUrl = twoFactorHost + twoFactorAPIKey + twoFactorVerifyPath;
	}

	public String sendOtp(String mobileNumber) {
		return sendOtp(mobileNumber, this.twoFactorTempalteName);
	}

	public String sendOtp(String mobileNumber, String templateName) {
		String urlToHit = sendOtpUrl.replace("{phoneNumber}", mobileNumber);
		logger.info(urlToHit);
		WebTarget webTarget = client.target(urlToHit);
		webTarget = webTarget.path("AUTOGEN/" + templateName);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);
		Response response = invocationBuilder.get();
		MobileOtpSendResponse mobileOtpSendResponse = response.readEntity(MobileOtpSendResponse.class);

		if (response.getStatus() == 200 && mobileOtpSendResponse != null
				&& "Success".equalsIgnoreCase(mobileOtpSendResponse.getStatus())) {
			logger.info(mobileOtpSendResponse.getDetails());
			return mobileOtpSendResponse.getDetails();
		} else {
			logger.info(mobileOtpSendResponse.getStatus());
			logger.info(mobileOtpSendResponse.getDetails());
			throw new MobileOtpSentFailedException(mobileOtpSendResponse.getDetails(), ErrorCodes.UNKNOWN_ERROR);
		}
	}

	public String sendCustomOtp(String mobileNumber, String otpValue, String templateName) {
		String urlToHit = sendOtpUrl.replace("{phoneNumber}", mobileNumber);
		logger.info(urlToHit);
		WebTarget webTarget = client.target(urlToHit);
		webTarget = webTarget.path(otpValue + "/" + templateName);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);
		Response response = invocationBuilder.get();
		MobileOtpSendResponse mobileOtpSendResponse = response.readEntity(MobileOtpSendResponse.class);

		if (response.getStatus() == 200 && mobileOtpSendResponse != null
				&& "Success".equalsIgnoreCase(mobileOtpSendResponse.getStatus())) {
			logger.info(mobileOtpSendResponse.getDetails());
			return mobileOtpSendResponse.getDetails();
		} else {
			logger.info(mobileOtpSendResponse.getStatus());
			logger.info(mobileOtpSendResponse.getDetails());
			throw new MobileOtpSentFailedException(mobileOtpSendResponse.getDetails(), ErrorCodes.UNKNOWN_ERROR);
		}
	}

	public boolean verifyOtp(String mobileNumber, String sessionId, String optValue) {
		logger.info(this.verifyOtpUrl);
		WebTarget webTarget = client.target(this.verifyOtpUrl);
		webTarget = webTarget.path(sessionId).path(optValue);
		logger.info(webTarget.getUri().toString());
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);
		Response response = invocationBuilder.get();
		MobileOtpVerifyResponse mobileOtpVerifyResponse = null;
		if (response.getStatus() == 200) {
			mobileOtpVerifyResponse = response.readEntity(MobileOtpVerifyResponse.class);

		}
		if (mobileOtpVerifyResponse != null && "Success".equalsIgnoreCase(mobileOtpVerifyResponse.getStatus())) {
			return "OTP Matched".equalsIgnoreCase(mobileOtpVerifyResponse.getDetails());
		}
		return false;
	}

	public String sendTransactionalMessage(MobileRequest mobileRequest) {
		WebTarget webTarget = client.target(twoFactorHost + twoFactorAPIKey);
		webTarget = webTarget.path("/ADDON_SERVICES/SEND/TSMS");
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		FormDataMultiPart mp = new FormDataMultiPart();
		FormDataBodyPart fromPart = new FormDataBodyPart("From", mobileRequest.getSenderId());
		mp.bodyPart(fromPart);
		FormDataBodyPart toPart = new FormDataBodyPart("To", mobileRequest.getMobileNumber());
		mp.bodyPart(toPart);
		FormDataBodyPart templateNamePart = new FormDataBodyPart("TemplateName", mobileRequest.getTemplateName());
		mp.bodyPart(templateNamePart);
		if (mobileRequest.getVariblesMap() != null) {
			for (Entry<String, String> entry : mobileRequest.getVariblesMap().entrySet()) {
				FormDataBodyPart part = new FormDataBodyPart(entry.getKey(), entry.getValue());
				mp.bodyPart(part);
			}
		}
		Entity<FormDataMultiPart> entity = Entity.entity(mp, MediaType.MULTIPART_FORM_DATA);
		Response response = invocationBuilder.post(entity);
		MobileOtpSendResponse mobileOtpSendResponse = response.readEntity(MobileOtpSendResponse.class);

		if (response.getStatus() == 200 && mobileOtpSendResponse != null
				&& "Success".equalsIgnoreCase(mobileOtpSendResponse.getStatus())) {
			logger.info(mobileOtpSendResponse.getDetails());
			return mobileOtpSendResponse.getDetails();
		} else {
			logger.info(mobileOtpSendResponse.getStatus());
			logger.info(mobileOtpSendResponse.getDetails());
			throw new MobileOtpSentFailedException(mobileOtpSendResponse.getDetails(), ErrorCodes.UNKNOWN_ERROR);
		}
	}

}
