package com.oxyloans.security;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

@Provider
public class FilterBinding implements DynamicFeature {

	private static final Logger logger = LogManager.getLogger(FilterBinding.class);

	@Autowired
	private CORSFilter corsFilter;

	@Autowired
	private AuthenticationFilter authenticationFilter;

	@Autowired
	private ThreadLocalClearFilter threadLocalClearFilter;

	@Override
	public void configure(ResourceInfo resourceInfo, FeatureContext context) {
		if (!resourceInfo.getResourceMethod().getName().startsWith("registration")
				&& !resourceInfo.getResourceMethod().getName().startsWith("login")
				&& !resourceInfo.getResourceMethod().getName().startsWith("newRegistration")
				&& !resourceInfo.getResourceMethod().getName().startsWith("pincode")
				&& !resourceInfo.getResourceMethod().getName().startsWith("registractionForOxyfarms")
				&& !resourceInfo.getResourceMethod().getName().startsWith("callBackApi")
				&& !resourceInfo.getResourceMethod().getName().startsWith("paytmReversalApi")
				&& !resourceInfo.getResourceMethod().getName().startsWith("addingBeneficiary")
				// && !resourceInfo.getResourceMethod().getName().startsWith("fundTransfer")
				&& !resourceInfo.getResourceMethod().getName().startsWith("toCheckAccountBalance")
				&& !resourceInfo.getResourceMethod().getName().startsWith("decryptingTheDataByPriavteKey")
				&& !resourceInfo.getResourceMethod().getName().startsWith("veifyExistenceOfSocialLoginUser")
				&& !resourceInfo.getResourceMethod().getName().startsWith("newRegistationWithSocialLogin")
				&& !resourceInfo.getResourceMethod().getName().startsWith("transactionInitiationForUpi")
				&& !resourceInfo.getResourceMethod().getName().startsWith("toCheckTransactionStatusCheck")
				&& !resourceInfo.getResourceMethod().getName().startsWith("iciciCallBackForUPITransaction")
				&& !resourceInfo.getResourceMethod().getName().startsWith("getListOfUniqueLenders")
				&& !resourceInfo.getResourceMethod().getName().startsWith("seekingSuggestionFromLenders")
				&& !resourceInfo.getResourceMethod().getName().startsWith("getUniqueLenderDetails")
				&& !resourceInfo.getResourceMethod().getName().startsWith("sendMobileOtpForAdviseSeekers")
				&& !resourceInfo.getResourceMethod().getName().startsWith("sendEmailOtp")
				&& !resourceInfo.getResourceMethod().getName().startsWith("transactionAlert")
				&& !resourceInfo.getResourceMethod().getName().startsWith("quickResponseTransactionDetails")
				&& !resourceInfo.getResourceMethod().getName().startsWith("callResponseForQr")
				&& !resourceInfo.getResourceMethod().getName().startsWith("transactionStatusCheck")
				&& !resourceInfo.getResourceMethod().getName().startsWith("UserNewRegistration")
				&& !resourceInfo.getResourceMethod().getName().startsWith("userEmailVerification")
				&& !resourceInfo.getResourceMethod().getName().startsWith("sendingEmailActivationLink")
				&& !resourceInfo.getResourceMethod().getName().startsWith("oldUsersRegistrationStep2Pending")
				&& !resourceInfo.getResourceMethod().getName().startsWith("partnerRegistrationFlow")
				&& !resourceInfo.getResourceMethod().getName().startsWith("resetpasswordForPartners")
				&& !resourceInfo.getResourceMethod().getName().startsWith("borrowerEmiDetails")
				&& !resourceInfo.getResourceMethod().getName().startsWith("accessToken")
				&& !resourceInfo.getResourceMethod().getName().startsWith("freeWhatsappMessages")
				&& !resourceInfo.getResourceMethod().getName().startsWith("freeWhatsappMessagesThroughExcel")
				&& !resourceInfo.getResourceMethod().getName().startsWith("verifyEndPoint")
				&& !resourceInfo.getResourceMethod().getName().startsWith("sampleNotification")
				&& !resourceInfo.getResourceMethod().getName().startsWith("cibRegistration")
				&& !resourceInfo.getResourceMethod().getName().startsWith("cibRegistrationStatusCheck")
				&& !resourceInfo.getResourceMethod().getName().startsWith("fixedDeposit")
				&& !resourceInfo.getResourceMethod().getName().startsWith("fdFetch")
				&& !resourceInfo.getResourceMethod().getName().startsWith("fdLiquidation")
				&& !resourceInfo.getResourceMethod().getName().startsWith("callbackForGetePayment")
				&& !resourceInfo.getResourceMethod().getName().startsWith("forgotPasswordForMobileApp")
				&& !resourceInfo.getResourceMethod().getName().startsWith("forgotPasswordForMobileAppVerification")
				&& !resourceInfo.getResourceMethod().getName().startsWith("otplessLogin")
				&& !resourceInfo.getResourceMethod().getName().startsWith("getUserUniqueNumber")
				&& !resourceInfo.getResourceMethod().getName().startsWith("whatsappLoginOtp")
				&& !resourceInfo.getResourceMethod().getName().startsWith("whatsappLoginOtpVerification")
				&& !resourceInfo.getResourceMethod().getName().startsWith("ondcCallBack")
				&& !resourceInfo.getResourceMethod().getName().startsWith("getHtmlPage")
				&& !resourceInfo.getResourceMethod().getName().startsWith("callBackForOndc")
				&& !resourceInfo.getResourceMethod().getName().startsWith("whatsappLoginById")
				&& !resourceInfo.getResourceMethod().getName().startsWith("buyerSearchForSeller")
				&& !resourceInfo.getResourceMethod().getName().startsWith("generateSalt")
				&& !resourceInfo.getResourceMethod().getName().startsWith("googleSpreadSheets")
				&& !resourceInfo.getResourceMethod().getName().startsWith("spreadSheetOxyloansUserData")
				&& !resourceInfo.getResourceMethod().getName().startsWith("spreadSheetOxyloansAmountUserData")
				&& !resourceInfo.getResourceMethod().getName().startsWith("spreadSheetClosedDealsCount")
				&& !resourceInfo.getResourceMethod().getName().startsWith("generatePassword")) {
			logger.info("Adding {} to Auth Filter", resourceInfo.getResourceMethod());
			context.register(authenticationFilter, 2);
		} else {
			logger.info("Not Adding {} to Auth Filter", resourceInfo.getResourceMethod().getName());
		}
		context.register(corsFilter, 1000);
		context.register(threadLocalClearFilter, 2100);
	}

}
