package com.oxyloans.security.mobile.twoFactor;

import com.oxyloans.service.OxyRuntimeException;

public class MobileOtpSentFailedException extends OxyRuntimeException {
	
	private String details;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MobileOtpSentFailedException(String message, String errorCode) {
		super(message, errorCode);
		this.details = message;
	}

	public String getDetails() {
		return details;
	}

}
