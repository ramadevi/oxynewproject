package com.oxyloans.security.mobile.twoFactor;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MobileOtpVerifyResponse {

	@JsonProperty(value = "Status")
	private String Status;

	@JsonProperty(value = "Details")
	private String Details;

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getDetails() {
		return Details;
	}

	public void setDetails(String details) {
		Details = details;
	}

}