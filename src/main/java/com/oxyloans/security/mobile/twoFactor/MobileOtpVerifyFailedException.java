package com.oxyloans.security.mobile.twoFactor;

import com.oxyloans.service.OxyRuntimeException;

public class MobileOtpVerifyFailedException extends OxyRuntimeException {
	
	private String details;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MobileOtpVerifyFailedException(String message, String errorCode) {
		super(message, errorCode);
		this.details = message;
	}

	public String getDetails() {
		return details;
	}

}
