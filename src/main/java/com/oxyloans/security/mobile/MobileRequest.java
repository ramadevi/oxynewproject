package com.oxyloans.security.mobile;

import java.util.Map;

public class MobileRequest {
	
	private String mobileNumber;
	
	private String templateName;
	
	private String senderId = "OXYLON";
	
	private Map<String, String> variblesMap;
	
	public MobileRequest(String mobileNumber, String templateName) {
		this.mobileNumber = mobileNumber;
		this.templateName = templateName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public String getTemplateName() {
		return templateName;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public Map<String, String> getVariblesMap() {
		return variblesMap;
	}

	public void setVariblesMap(Map<String, String> variblesMap) {
		this.variblesMap = variblesMap;
	}
	
	

}
