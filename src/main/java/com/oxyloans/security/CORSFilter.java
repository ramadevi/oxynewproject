package com.oxyloans.security;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

import org.springframework.stereotype.Component;

@Component
public class CORSFilter implements ContainerResponseFilter {

	public static final String ACCESS_CONTROL_ALLOW_ORIGIN_HEADER = "Access-Control-Allow-Origin";
	public static final String ACCESS_CONTROL_ALLOW_METHODS_HEADER = "Access-Control-Allow-Methods";
	public static final String ACCESS_CONTROL_ALLOW_HEADERS_HEADER = "Access-Control-Allow-Headers";
	public static final String ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER = "Access-Control-Allow-Credentials";
	public static final String ACCESS_CONTROL_EXPOSE_HEADERS_HEADER = "Access-Control-Expose-Headers";

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
		responseContext.getHeaders().remove(ACCESS_CONTROL_ALLOW_ORIGIN_HEADER);
		responseContext.getHeaders().remove(ACCESS_CONTROL_ALLOW_HEADERS_HEADER);
		responseContext.getHeaders().remove(ACCESS_CONTROL_ALLOW_METHODS_HEADER);
		responseContext.getHeaders().remove(ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER);

		String originHeaderFromRequest = requestContext.getHeaderString("Origin");

		responseContext.getHeaders().putSingle(ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
		responseContext.getHeaders().putSingle(ACCESS_CONTROL_ALLOW_HEADERS_HEADER, "X-Requested-With,Content-Type,Accept,Origin,accessToken");
		responseContext.getHeaders().putSingle(ACCESS_CONTROL_EXPOSE_HEADERS_HEADER, "X-Requested-With,Content-Type,Accept,Origin,accessToken");
		responseContext.getHeaders().putSingle(ACCESS_CONTROL_ALLOW_METHODS_HEADER, "GET,POST,HEAD,DELETE,OPTIONS,PUT,HEAD,PATCH");
		responseContext.getHeaders().putSingle(ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER, "true");
		return;

	}

}
