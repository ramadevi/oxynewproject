package com.oxyloans.security;

import com.oxyloans.coreauthdto.GenericAccessToken;

public class ThreadLocalSecurityProvider {
	
	private static final ThreadLocal<GenericAccessToken> ACCESSTOKEN = new ThreadLocal<GenericAccessToken>();
	
	public static void setAccessToken(GenericAccessToken token) {
		ACCESSTOKEN.set(token);
	}
	
	public static void removeAccessToken() {
		ACCESSTOKEN.remove();
	}
	
	public static GenericAccessToken getAccessToken() {
		return ACCESSTOKEN.get();
	}

}
