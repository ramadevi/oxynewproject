package com.oxyloans.security;

import java.io.IOException;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang.StringUtils;

@Provider
@Priority(value = 1)
public class OptionsFilter implements ContainerRequestFilter {

	@Context
	private HttpServletResponse response;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		String method = requestContext.getMethod();
		String origin = requestContext.getHeaderString("Origin");
		if ("OPTIONS".equalsIgnoreCase(method)) {
			response.addHeader("Access-Control-Allow-Origin", StringUtils.isBlank(origin) ? "*" : origin);
			response.addHeader("Access-Control-Allow-Headers",
					"origin, Content-Type, accept, authorization, accessToken, pageNo, pageSize, totalResults");
			response.addHeader("Access-Control-Allow-Credentials", "true");
			response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD, PATCH");
			requestContext.abortWith(Response.status(Status.OK).build());
			return;
		}
	}
}
