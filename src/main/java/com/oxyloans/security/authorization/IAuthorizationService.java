package com.oxyloans.security.authorization;

import com.oxyloans.entity.user.User;

public interface IAuthorizationService {
	
	public void hasPermission(String api, String method);
	
	public void hasPermission(User user);
	
	public Integer getCurrentUser();
	
	public Integer getOnBehalfUser();

}
