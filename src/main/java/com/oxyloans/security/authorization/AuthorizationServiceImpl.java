package com.oxyloans.security.authorization;

import org.springframework.stereotype.Service;

import com.amazonaws.services.lightsail.model.AccessDeniedException;
import com.oxyloans.coreauthdto.GenericAccessToken;
import com.oxyloans.coreauthdto.PasswordGrantAccessToken;
import com.oxyloans.coreauthdto.GenericAccessToken.GrantType;
import com.oxyloans.entity.user.User;
import com.oxyloans.security.ThreadLocalSecurityProvider;

@Service
public class AuthorizationServiceImpl implements IAuthorizationService {

	@Override
	public void hasPermission(String api, String method) {
	}

	@Override
	public void hasPermission(User user) {
		if (user == null) {
			throw new AccessDeniedException("Not Permitted on give object");
		}
		GenericAccessToken accessToken = ThreadLocalSecurityProvider.getAccessToken();
		if (accessToken.getGrantType() == GrantType.PWD) {
			PasswordGrantAccessToken passwordGrantAccessToken = (PasswordGrantAccessToken) accessToken;
			boolean permissionGranted = user.getId().equals(Integer.parseInt(passwordGrantAccessToken.getUserId()));
			if (!permissionGranted) {
				throw new AccessDeniedException("Not Permitted on give object");
			}
		}
	}

	@Override
	public Integer getCurrentUser() {
		GenericAccessToken accessToken = ThreadLocalSecurityProvider.getAccessToken();
		if (accessToken.getGrantType() == GrantType.PWD) {
			PasswordGrantAccessToken passwordGrantAccessToken = (PasswordGrantAccessToken) accessToken;
			return Integer.parseInt(passwordGrantAccessToken.getUserId());
		}
		return null;
	}

	@Override
	public Integer getOnBehalfUser() {
		GenericAccessToken accessToken = ThreadLocalSecurityProvider.getAccessToken();
		if (accessToken.getGrantType() == GrantType.PWD) {
			PasswordGrantAccessToken passwordGrantAccessToken = (PasswordGrantAccessToken) accessToken;
			return Integer.parseInt(passwordGrantAccessToken.getUserId());
		}
		return null;
	}

}
