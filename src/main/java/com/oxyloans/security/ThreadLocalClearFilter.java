package com.oxyloans.security;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

import org.springframework.stereotype.Component;

import com.oxyloans.security.ThreadLocalSecurityProvider;

@Component
public class ThreadLocalClearFilter implements ContainerResponseFilter {

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
		ThreadLocalSecurityProvider.removeAccessToken();
	}

}
