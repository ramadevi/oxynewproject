package com.oxyloans.security;

import java.io.IOException;
import java.util.Date;
import java.util.Base64;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.oxyloans.authentication.exception.AccessTokenMissingException;
import com.oxyloans.core.auth.dto.GenericAccessToken;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.security.ThreadLocalSecurityProvider;
import com.oxyloans.security.signature.ISignatureService;
import com.oxyloans.service.user.LoginServiceFactory;

@Component
public class AuthenticationFilter implements ContainerRequestFilter {

	private static final Logger logger = LogManager.getLogger(AuthenticationFilter.class);

	public static final String ACCESS_TOKEN = "accessToken";

	@Autowired
	private LoginServiceFactory loginServiceFactory;

	@Value("${accessTokenTtl}")
	private String accessTokenTtl;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		MultivaluedMap<String, String> headers = requestContext.getHeaders();
		String accessToken = headers.getFirst(ACCESS_TOKEN);
		if (StringUtils.isEmpty(accessToken)) {
			throw new AccessTokenMissingException("accessToken not passing", ErrorCodes.TOKEN_NOT_VALID); // Create new
																											// Exception
																											// and throw
		}
		String[] split = accessToken.split("\\" + ISignatureService.SEPARATOR);
		String decodeToken = new String(Base64.getDecoder().decode(split[0]));
		GenericAccessToken accessTokenValue = new Gson().fromJson(decodeToken, GenericAccessToken.class);

		long generatedTime = accessTokenValue.getIat(); // while generating token time in milli seconds
		Date d = new Date(generatedTime);
		long currentTime = System.currentTimeMillis();
		Date d1 = new Date(currentTime);
		logger.info("difference in time", d1.getTime() - d.getTime());
		if (d1.getTime() - d.getTime() > Long.parseLong(accessTokenTtl)) {
			throw new AccessTokenMissingException("your session has expired. please login again",
					ErrorCodes.TOKEN_NOT_VALID);
		}
		accessTokenValue.getTtl();
		accessTokenValue.getIat();
		System.currentTimeMillis();

		GenericAccessToken convertedToken = loginServiceFactory
				.getService(accessTokenValue.getGrantType().name().toUpperCase()).verifyToken(accessToken, decodeToken);
		ThreadLocalSecurityProvider.setAccessToken(convertedToken);
		logger.info("Request Headers : {} ", requestContext.getHeaders());
	}

}
