package com.oxyloans.exception.Exceptionmapperemail;



import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Component;

import com.oxyloans.errormessages.EmailMessagingExceptionErrormessages;
@Provider
@Component
public class EmailMessagingException implements ExceptionMapper<MailSendException>{

@Value("${statuscode}")
private int code;
	
	@Override
	public Response toResponse(MailSendException exception) {
		EmailMessagingExceptionErrormessages messageingException=new EmailMessagingExceptionErrormessages(exception.getMessage(),code);
		
		return Response.status(Status.BAD_REQUEST).entity(messageingException).build();
	}

}
