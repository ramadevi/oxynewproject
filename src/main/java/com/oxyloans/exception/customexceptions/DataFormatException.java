package com.oxyloans.exception.customexceptions;

import com.oxyloans.service.OxyRuntimeException;

public class DataFormatException extends OxyRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1612311064088258934L;

	public DataFormatException(String message, String errorCode) {
		super(message, errorCode);

	}

}
