package com.oxyloans.exception.customexceptions;

import com.oxyloans.service.OxyRuntimeException;

public class UploadException extends OxyRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UploadException(String message, String errorCode) {
		super(message, errorCode);
	}

}
