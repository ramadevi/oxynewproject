package com.oxyloans.exception.customexceptions;

import com.oxyloans.service.OxyRuntimeException;

public class AgeValidationException extends OxyRuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AgeValidationException(String message, String errorCode) {
		super(message, errorCode);
	}

}
