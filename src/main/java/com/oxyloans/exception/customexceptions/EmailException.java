package com.oxyloans.exception.customexceptions;

public class EmailException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1612311064088258934L;

	public EmailException(String message) {
		super(message);

	}

}
