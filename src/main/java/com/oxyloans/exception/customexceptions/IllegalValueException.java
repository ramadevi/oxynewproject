package com.oxyloans.exception.customexceptions;

import com.oxyloans.service.OxyRuntimeException;

public class IllegalValueException extends OxyRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1612311064088258934L;

	public IllegalValueException(String message, String errorCode) {
		super(message, errorCode);

	}

}
