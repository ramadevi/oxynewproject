package com.oxyloans.exception.customexceptions;

import com.oxyloans.service.OxyRuntimeException;

public class EntityAlreadyExistsException extends OxyRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EntityAlreadyExistsException(String message, String errorCode) {
		super(message, errorCode);
	}

}
