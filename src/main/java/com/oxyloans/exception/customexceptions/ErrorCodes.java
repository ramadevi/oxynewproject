package com.oxyloans.exception.customexceptions;

public class ErrorCodes {

	public static final String INVALID_DATE_FORMAT = "104"; // Status Code - 400

	public static final String ENTITY_NOT_ACTIVE = "106"; // Status Code - 400

	public static final String BORROWER_NOT_PAID = "107"; // Status Code - 400

	public static final String ACTION_ALREDY_DONE = "108"; // Status Code - 400

	public static final String ALREDY_IN_PROGRESS = "109"; // Status Code - 400

	public static final String LIMIT_REACHED = "110"; // Status Code - 400

	public static final String LENDER_NOT_RESPONDED = "111"; // Status Code - 400

	public static final String EMPTY_REQUEST = "112"; // Status Code - 400

	public static final String EMAIL_NOT_VERIFIED = "113"; // Status Code - 400

	public static final String INVALID_OPERATION = "114"; // Status Code - 400

	public static final String TOKEN_NOT_VALID = "100"; // Status Code - 401

	public static final String USER_NOT_FOUND = "101"; // Status Code - 401

	public static final String INVALID_PASSWORD = "102"; // Status Code - 401

	public static final String INVALID_OTP = "103"; // Status Code - 401

	public static final String PERMISSION_DENIED = "403"; // Status Code - 403

	public static final String ENITITY_NOT_FOUND = "404"; // Status Code - 404

	public static final String ENTITY_ALREDY_EXISTS = "105"; // Status Code - 409

	public static final String UNKNOWN_ERROR = "500"; // Status Code - 500

	public static final String LOAN_REQUEST_NULL = "115";// Status Code - 115

	public static final String CITY_ERROR = "116";

	public static final String EXPERIAN_SCORE_NOT_NULL = "116";

	public static final String PAYU_TransactionNumber_NOT_EQUALS = "117";

	public static final String BORROWER_FEE_PAID = "118";

	public static final String PROFESSIONSALDETAILS_NOT_FOUND = "119";

	public static final String SCORE_ERROR = "120";

	public static final String DATA_PERSIST_ERROR = "121";

	public static final String DATA_FETCH_ERROR = "121";

	public static final String NO_LOAN_FOUND_FOR_BORROWER = "122";

	public static final String AMOUNT_EXCEEDED = "123";

	public static final String AMOUNT_LIMIT = "124";

	public static final String PATICIPATION_LIMIT = "125";

	public static final String LOWER_AMOUNT = "126";
	
	public static final String EMAIL_IS_NULL="128";
	
	public static final String INCOMPLETED_PROFILE = "130";

}
