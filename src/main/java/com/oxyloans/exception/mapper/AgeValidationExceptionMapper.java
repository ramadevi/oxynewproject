package com.oxyloans.exception.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.oxyloans.customexceptions.AgeValidationException;
import com.oxyloans.errormessages.ErrorMessage;

@Provider
public class AgeValidationExceptionMapper implements ExceptionMapper<AgeValidationException> {

	private static final Logger logger = LogManager.getLogger(OperationNotAllowedExceptionMapper.class);

	@Override
	public Response toResponse(AgeValidationException exception) {
	
		logger.error(exception);
		ErrorMessage errorMessages = new ErrorMessage(exception.getMessage(), exception.getErrorCode());
		return Response.status(Status.BAD_REQUEST).entity(errorMessages).build();

	}
	
	

}
