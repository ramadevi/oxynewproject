package com.oxyloans.exception.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.oxyloans.customexceptions.EntityAlreadyExistsException;
import com.oxyloans.errormessages.ErrorMessage;

@Provider
public class EntityAlreadyExistsExceptionMapper implements ExceptionMapper<EntityAlreadyExistsException> {

	private static final Logger logger = LogManager.getLogger(EntityAlreadyExistsExceptionMapper.class);

	@Override
	public Response toResponse(EntityAlreadyExistsException exception) {
		logger.error(exception);
		ErrorMessage errorMessages = new ErrorMessage(exception.getMessage(), exception.getErrorCode());
		return Response.status(Status.CONFLICT).entity(errorMessages).build();
	}
}
