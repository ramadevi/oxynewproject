package com.oxyloans.exception.mapper;

import java.util.NoSuchElementException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.errormessages.ErrorMessage;


@Provider
public class NoSuchElementExceptionMapper implements ExceptionMapper<NoSuchElementException> {

	private static final Logger logger = LogManager.getLogger(NoSuchElementExceptionMapper.class);

	@Override
	public Response toResponse(NoSuchElementException exception) {
		logger.error("", exception);
		ErrorMessage errorMessages = new ErrorMessage(exception.getMessage(), ErrorCodes.ENITITY_NOT_FOUND);
		return Response.status(Status.BAD_REQUEST).entity(errorMessages).build();
	}
}
