package com.oxyloans.exception.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.oxyloans.customexceptions.IllegalValueException;
import com.oxyloans.errormessages.ErrorMessage;

@Provider
public class IllegalValueExceptionMapper implements ExceptionMapper<IllegalValueException> {

	private static final Logger logger = LogManager.getLogger(IllegalValueExceptionMapper.class);

	@Override
	public Response toResponse(IllegalValueException exception) {
		logger.error(exception);
		ErrorMessage errorMessages = new ErrorMessage(exception.getMessage(), exception.getErrorCode());
		return Response.status(Status.BAD_REQUEST).entity(errorMessages).build();
	}
}
