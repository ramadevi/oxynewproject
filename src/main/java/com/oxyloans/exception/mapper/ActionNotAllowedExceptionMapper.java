package com.oxyloans.exception.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.oxyloans.errormessages.ErrorMessage;
import com.oxyloans.service.loan.ActionNotAllowedException;

@Provider
public class ActionNotAllowedExceptionMapper implements ExceptionMapper<ActionNotAllowedException> {

	private static final Logger logger = LogManager.getLogger(ActionNotAllowedExceptionMapper.class);

	@Override
	public Response toResponse(ActionNotAllowedException exception) {
		logger.error(exception);
		ErrorMessage errorMessages = new ErrorMessage(exception.getMessage(), exception.getErrorCode());
		return Response.status(Status.BAD_REQUEST).entity(errorMessages).build();
	}
}
