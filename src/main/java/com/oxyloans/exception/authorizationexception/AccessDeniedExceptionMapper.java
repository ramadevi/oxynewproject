package com.oxyloans.exception.authorizationexception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.oxyloans.errormessages.ErrorMessage;

@Provider
public class AccessDeniedExceptionMapper implements ExceptionMapper<AccessDeniedException> {

	private static final Logger logger = LogManager.getLogger(AccessDeniedExceptionMapper.class);

	@Override
	public Response toResponse(AccessDeniedException exception) {
		logger.error(exception);
		ErrorMessage errorMessages = new ErrorMessage("Not Allowed", exception.getErrorCode());
		return Response.status(Status.FORBIDDEN).entity(errorMessages).build();
	}
}
