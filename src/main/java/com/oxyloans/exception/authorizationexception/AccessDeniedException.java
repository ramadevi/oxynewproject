package com.oxyloans.exception.authorizationexception;

import com.oxyloans.service.OxyRuntimeException;

public class AccessDeniedException extends OxyRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public AccessDeniedException(String message, String errorCode) {
		super(message, errorCode);
	}

}
