package com.oxyloans.exception.oxyruntimeexception;

public class OperationNotAllowedException extends OxyRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public OperationNotAllowedException(String message, String errorCode) {
		super(message, errorCode);
	}

}
