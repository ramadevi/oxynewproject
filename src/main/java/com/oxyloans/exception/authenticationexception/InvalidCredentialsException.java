package com.oxyloans.exception.authenticationexception;

import com.oxyloans.service.OxyRuntimeException;

public class InvalidCredentialsException extends OxyRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1612311064088258934L;

	public InvalidCredentialsException(String message, String errorCode) {
		super(message, errorCode);
	}

}
