package com.oxyloans.exception.authenticationexception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.oxyloans.errormessages.ErrorMessage;

@Provider
public class InvalidAccessTokenExceptionMapper implements ExceptionMapper<InvalidAccessTokenException> {

	private static final Logger logger = LogManager.getLogger(InvalidAccessTokenExceptionMapper.class);

	@Override
	public Response toResponse(InvalidAccessTokenException exception) {
		logger.error(exception);
		ErrorMessage errorMessages = new ErrorMessage("Invalid username or password.", exception.getErrorCode());
		return Response.status(Status.UNAUTHORIZED).entity(errorMessages).build();
	}
}
