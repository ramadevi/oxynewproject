package com.oxyloans.rest.autoinvest;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.oxyloans.autoinvest.dto.AutoInvestConfigResponse;
import com.oxyloans.autoinvest.dto.AutoInvestResponseDto;
import com.oxyloans.autoinvest.dto.LenderAutoInvestConfigRequest;
import com.oxyloans.autoinvest.dto.LenderAutoInvestConfigResponse;
import com.oxyloans.autoinvest.dto.LenderAutoInvestConfigSearchRequest;
import com.oxyloans.autoinvest.service.LenderAutoInvestConfigServive;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.service.loan.dto.LoanResponseDto;

@Component
@Path("/v1/user")
public class AutoInvestConfigResource {

	private static final Logger logger = LogManager.getLogger(AutoInvestConfigResource.class);

	@PostConstruct
	public void print() {
		logger.info("AutoInvestConfigResource rest initia......");
	}

	@Autowired
	private LenderAutoInvestConfigServive lenderAutoInvestConfigServive;

	@POST
	@Path("/saveautoinvestconfig")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderAutoInvestConfigResponse saveAutoInvestConfig(LenderAutoInvestConfigRequest request)
			throws MessagingException {
		logger.info("saveAutoInvestConfig in rest......" + request.getUserId());
		return lenderAutoInvestConfigServive.saveAutoInvestConfig(request);
	}

	@GET
	@Path("/{userId}/getautoinvestconfigbylenderid")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderAutoInvestConfigResponse getAutoInvestConfigByLenderId(@PathParam("userId") Integer userId)
			throws MessagingException {
		logger.info("getAutoInvestConfigByLenderLd in rest......" + userId);
		return lenderAutoInvestConfigServive.getAutoInvestConfigByLenderId(userId);
	}

	@POST
	@Path("/updateautoinvestconfig")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderAutoInvestConfigResponse updAutoInvestConfig(LenderAutoInvestConfigRequest request)
			throws MessagingException {
		logger.info("updAutoInvestConfig in rest......" + request.getUserId());
		return lenderAutoInvestConfigServive.updAutoInvestConfig(request);
	}

	@POST
	@Path("/inactiveautoinvestconfig")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderAutoInvestConfigResponse updAutoInvestConfigInActive(LenderAutoInvestConfigRequest request)
			throws MessagingException {
		logger.info("updAutoInvestConfigInActive in rest......" + request.getUserId());
		return lenderAutoInvestConfigServive.updAutoInvestConfigInActive(request);
	}

	@POST
	@Path("/autoinvestsearch")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<LenderAutoInvestConfigResponse> autoInvestSearch(
			LenderAutoInvestConfigSearchRequest request) throws MessagingException {
		logger.info("autoInvestSearch in rest......" + request.getUserId());
		return lenderAutoInvestConfigServive.autoInvestSearch(request);
	}

	@POST
	@Path("/getlenderautoinvesthistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AutoInvestConfigResponse getLenderAutoInvestHistory(LenderAutoInvestConfigSearchRequest request)
			throws MessagingException {
		logger.info("getLenderAutoInvestHistory in rest......" + request.getUserId());
		return lenderAutoInvestConfigServive.getLenderAutoInvestHistory(request);
	}

	// @Scheduled(cron = "0 */2 * ? * *")
	@Scheduled(cron = "0 0 0/2 ? * *")
	public void scheduleAutoInvest() throws MessagingException {
		logger.info("scheduleAutoInvest in rest......");
		SearchResultsDto<LenderAutoInvestConfigResponse> response = lenderAutoInvestConfigServive
				.getAllAutoInvestLendersInfo();
		List<LenderAutoInvestConfigResponse> autoInvestLendersList = response.getResults();
		lenderAutoInvestConfigServive.scheduleAutoInvest(autoInvestLendersList);
		logger.info("AutoInvest Lender List Size!!!!!!!!" + autoInvestLendersList.size());
	}

	@Scheduled(cron = "0 0 1/2 ? * *")
	public void scheduleLenderAutoEsign() throws MessagingException {
		logger.info("scheduleLenderAutoEsign in rest......");
		List<String> response = lenderAutoInvestConfigServive.getAllAutoInvestAgreedLoanIds();
		if (response != null && !response.isEmpty()) {
			for (String loanId : response) {
				LoanResponseDto resp = lenderAutoInvestConfigServive.autoInvestLenderEsign(loanId);
				logger.info("generated lender esign id......" + resp.getLenderEsignId());
			}
		}
	}

	@GET
	@Path("/{userId}/auto-lending-enable")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AutoInvestResponseDto autoLendEnableData(@PathParam("userId") Integer userId) {
		return lenderAutoInvestConfigServive.autoLendEnableData(userId);
	}

	@PATCH
	@Path("/{id}/{status}/auto-lending-enable-or-disable")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AutoInvestResponseDto closingAutoLending(@PathParam("id") Integer id, @PathParam("status") String status) {
		return lenderAutoInvestConfigServive.closingAutoLending(id, status);
	}
}
