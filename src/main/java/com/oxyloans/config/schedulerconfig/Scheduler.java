package com.oxyloans.schedulerconfig;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.service.loan.LoanServiceFactory;

public class Scheduler implements Job {
	private final Logger logger = LogManager.getLogger(getClass());
	
	@Autowired
	private LoanServiceFactory loanServiceFactory;
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		loanServiceFactory.getService(PrimaryType.BORROWER.name()).loanApplicationScheduler();
	}
	
	
	}
