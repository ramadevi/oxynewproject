package com.oxyloans.rest.jersey.config;

import java.util.Properties;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import com.oxyloans.authentication.exception.AccessTokenMissingExceptionMapper;
import com.oxyloans.authentication.exception.InvalidAccessTokenExceptionMapper;
import com.oxyloans.authentication.exception.InvalidCredentialsException;
import com.oxyloans.authorization.exception.AccessDeniedExceptionMapper;
import com.oxyloans.response.admin.AdminResponse;
import com.oxyloans.security.FilterBinding;
import com.oxyloans.exception.mapper.ActionNotAllowedExceptionMapper;
import com.oxyloans.exception.mapper.DataFormatExceptionMapper;
import com.oxyloans.exception.mapper.EntityAlreadyExistsExceptionMapper;
import com.oxyloans.exception.mapper.IllegalValueExceptionMapper;
import com.oxyloans.exception.mapper.LimitExceededExceptionMapper;
import com.oxyloans.exception.mapper.MobileOtpSentFailedExceptionMapper;
import com.oxyloans.exception.mapper.NoSuchElementExceptionMapper;
import com.oxyloans.exception.mapper.UploadExceptionMapper;
import com.oxyloans.rest.loan.DashboardResource;
import com.oxyloans.rest.user.UserResource;
import com.oxyloans.service.loan.LoanServiceFactory;
import com.oxyloans.service.user.LoginServiceFactory;

@Configuration
@ImportResource(value = {"classpath:aws-ses-context.xml", "classpath:velocity-context.xml", "classpath:fop-context.xml", "classpath:redis-template.xml"})
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		packages("com.oxyloans");
		register(UserResource.class);
		register(DashboardResource.class);
		register(MultiPartFeature.class);
		register(FilterBinding.class);
		register(MobileOtpSentFailedExceptionMapper.class);
		register(InvalidCredentialsException.class);
		register(InvalidAccessTokenExceptionMapper.class);
		register(AccessTokenMissingExceptionMapper.class);
		register(AccessDeniedExceptionMapper.class);
		register(NoSuchElementExceptionMapper.class);
		register(UploadExceptionMapper.class);
		register(EntityAlreadyExistsExceptionMapper.class);
		register(DataFormatExceptionMapper.class);
		register(ActionNotAllowedExceptionMapper.class);
		register(LimitExceededExceptionMapper.class);
		register(IllegalValueExceptionMapper.class);
		register(AdminResponse.class);
		property("jersey.config.server.provider.classnames", "org.glassfish.jersey.media.multipart.MultiPartFeature");
	}
	
	@Bean
    public ServiceLocatorFactoryBean serviceLocatorBean(){
        ServiceLocatorFactoryBean bean = new ServiceLocatorFactoryBean();
        bean.setServiceLocatorInterface(LoginServiceFactory.class);
        Properties serviceMappings = new Properties();
        bean.setServiceMappings(serviceMappings);
        return bean;
    }
	
	@Bean
    public ServiceLocatorFactoryBean loanServiceLocatorBean(){
        ServiceLocatorFactoryBean bean = new ServiceLocatorFactoryBean();
        bean.setServiceLocatorInterface(LoanServiceFactory.class);
        Properties serviceMappings = new Properties();
        bean.setServiceMappings(serviceMappings);
        return bean;
    }
}
