package com.oxyloans.rest.holdamount;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxloans.lender.holdamount.dto.HoldAmountRequestDto;
import com.oxloans.lender.holdamount.dto.HoldAmountResponseDto;
import com.oxloans.lender.holdamount.dto.ListOfHoldAmountsRequestDto;
import com.oxloans.lender.holdamount.dto.UserHoldAmountMappedToDealRequestDto;
import com.oxloans.lender.holdamount.dto.UserHoldAmountMappedToDealResponseDto;
import com.oxyloans.entity.lenders.hold.UserHoldAmountMappedToDeal.AmountType;
import com.oxyloans.lender.holdamount.service.HoldAmountService;

@Component
@Path("v1/user")
public class HoldAmountController {

	@Autowired
	private HoldAmountService holdAmountService;

	@POST
	@Path("/holdamount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public HoldAmountResponseDto saveingHoldAmount(HoldAmountRequestDto holdAmountRequestDto) {
		return holdAmountService.saveingHoldAmount(holdAmountRequestDto);

	}

	@GET
	@Path("/holdamount-list")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<HoldAmountResponseDto> listOfHoldAmounts() {
		return holdAmountService.listOfHoldAmounts();
	}

	@POST
	@Path("/{userHoldAmountDetailId}/holdamount-mapping-todeals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserHoldAmountMappedToDealResponseDto savingHoldAmountMappedToDeal(
			@PathParam("userHoldAmountDetailId") Integer userHoldAmountDetailId,
			ListOfHoldAmountsRequestDto listOfHoldAmountsRequestDto) {
		return holdAmountService.savingHoldAmountMappedToDeal(userHoldAmountDetailId, listOfHoldAmountsRequestDto);
	}

	@PATCH
	@Path("/{userHoldAmountDetailId}/deleting-hold-request")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public HoldAmountResponseDto deletingTheHoldAmountRequest(
			@PathParam("userHoldAmountDetailId") Integer userHoldAmountDetailId) {
		return holdAmountService.deletingTheHoldAmountRequest(userHoldAmountDetailId);
	}

	@PATCH
	@Path("{userId}/{dealId}/{amountType}/{amount}/sample")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public double remainingAmountAfterRemovingHoldAmount(@PathParam("userId") int userId,
			@PathParam("dealId") int dealId, @PathParam("amountType") AmountType amountType,
			@PathParam("amount") double amount) {
		return holdAmountService.remainingAmountAfterRemovingHoldAmount(userId, dealId, amountType, amount);
	}

	@GET
	@Path("/{userId}/hold-amount-details")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<HoldAmountResponseDto> holdInformation(@PathParam("userId") int userId) {
		return holdAmountService.holdInformation(userId);
	}
}
