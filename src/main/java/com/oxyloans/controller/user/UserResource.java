package com.oxyloans.rest.user;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.oxyloans.authentication.exception.AccessTokenMissingException;
import com.oxyloans.core.auth.dto.GenericAccessToken;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.customexceptions.UploadException;
import com.oxyloans.enach.scheduler.EnachMinimunWithdrawUsersDto;
import com.oxyloans.enach.scheduler.EnachMiniumWithDrwUserRequest;
import com.oxyloans.enach.scheduler.EnachTypeRequestDto;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceResponse;
import com.oxyloans.entity.borrowers.deals.dto.DealAndParticipationCount;
import com.oxyloans.entity.borrowers.deals.dto.LenderInterestRequestDto;
import com.oxyloans.entity.borrowers.deals.dto.LenderInterestResponseDto;
import com.oxyloans.entity.expertSeekers.AdviseSeekersResponseDto;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.entity.user.Document.status.UserDocumentStatus;
import com.oxyloans.experian.dto.ExperianRequestDto;
import com.oxyloans.experian.dto.ExperianResponceDto;
import com.oxyloans.experin.service.ExperianService;
import com.oxyloans.file.FileResponse;
import com.oxyloans.icici.ServiceForICICI;
import com.oxyloans.icici.dto.RequestFromICICI;
import com.oxyloans.icici.upi.GetepayRequestDto;
import com.oxyloans.lender.wallet.ScrowLenderTransactionResponse;
import com.oxyloans.lender.wallet.ScrowTransactionRequest;
import com.oxyloans.lender.wallet.ScrowWalletResponse;
import com.oxyloans.pincodeutil.PincodeResultes;
import com.oxyloans.request.PageDto;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.request.SocialLoginRequestDto;
import com.oxyloans.request.user.BulkinviteResponse;
import com.oxyloans.request.user.KycFileRequest;
import com.oxyloans.request.user.KycFileRequest.KycType;
import com.oxyloans.request.user.KycFileResponse;
import com.oxyloans.request.user.LenderReferenceRequestDto;
import com.oxyloans.request.user.NomineeRequestDto;
import com.oxyloans.request.user.PersonalDetailsRequestDto;
import com.oxyloans.request.user.QueriesCountsResponse;
import com.oxyloans.request.user.TestUserResponse;
import com.oxyloans.request.user.UserQueryDetailsRequestDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.BorrowersLoanOwnerInformation;
import com.oxyloans.response.user.ContactDetailsForIos;
import com.oxyloans.response.user.EmailAddressResponse;
import com.oxyloans.response.user.GmailSignInResponse;
import com.oxyloans.response.user.LenderMailResponse;
import com.oxyloans.response.user.LenderProfitResponse;
import com.oxyloans.response.user.LenderRatingRequestDto;
import com.oxyloans.response.user.LenderRatingResponseDto;
import com.oxyloans.response.user.LenderReferenceAmountResponseDto;
import com.oxyloans.response.user.LenderReferralPaymentStatus;
import com.oxyloans.response.user.NomineeResponseDto;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.response.user.PaymentSearchByStatus;
import com.oxyloans.response.user.PaymentUploadHistoryRequestDto;
import com.oxyloans.response.user.PaymentUploadHistoryResponseDto;
import com.oxyloans.response.user.PersonalDetailsResponseDto;
import com.oxyloans.response.user.StatusResponseDto;
import com.oxyloans.response.user.UserPersonalDetailsResponse;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.response.user.UserVanNumberResponseDto;
import com.oxyloans.response.user.ValidityResponse;
import com.oxyloans.security.AuthenticationFilter;
import com.oxyloans.security.signature.ISignatureService;
import com.oxyloans.service.loan.dto.ApplicationResponseDto;
import com.oxyloans.service.loan.dto.LoanResponseDto;
import com.oxyloans.service.socialLogin.SocialLoginService;
import com.oxyloans.service.user.LoginServiceFactory;
import com.oxyloans.service.user.PwdLoginService;
import com.oxyloans.service.user.UserService;
import com.oxyloans.transactionalert.dto.TransactionAlertResponseDto;

@Component
@Path("/v1/user")
public class UserResource {

	private static final Logger logger = LogManager.getLogger(UserResource.class);

	@PostConstruct
	public void print() {
		logger.info("REST initia......");
	}

	@Autowired
	private UserService userService;

	@Autowired
	private LoginServiceFactory loginServiceFactory;

	@Autowired
	private ExperianService experianService;

	@Autowired
	private ServiceForICICI serviceForICICI;

	@Autowired
	private SocialLoginService socialLoginService;

	@Autowired
	private PwdLoginService abstractLoginService;

	@POST
	@Path("/register/{emailToken}/verify")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse registrationEmailVerify(UserRequest userRequest, @PathParam("emailToken") String emailToken) {
		logger.info("mailToken : " + emailToken);
		return userService.verifyEmail(userRequest, emailToken);
	}

	@POST
	@Path("/resetPassword/{emailToken}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse registrationResetPassword(UserRequest userRequest, @PathParam("emailToken") String emailToken) {
		logger.info("mailToken : " + emailToken);
		return userService.verifyEmail(userRequest, emailToken);
	}

	@DELETE
	@Path("/delete/{mobileNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse deleteByMobileNumber(@PathParam("mobileNumber") String mobileNumber) {
		userService.deleteByMobileNumber(mobileNumber);
		return null;
	}

	@POST
	@Path("/{userId}/upload/kyc")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public UserResponse uploadKyc(FormDataMultiPart multiPart, @PathParam("userId") Integer userId)
			throws MessagingException, FileNotFoundException, IOException {
		List<KycFileRequest> files = new ArrayList<KycFileRequest>();
		extracted(files, multiPart);
		if (files.isEmpty()) {
			throw new UploadException("No Files are uploaded", ErrorCodes.EMPTY_REQUEST);
		}
		return userService.uploadKyc(userId, files);
	}

	private void extracted(List<KycFileRequest> files, FormDataMultiPart multipart) {
		if (multipart != null) {
			for (BodyPart part : multipart.getBodyParts()) {
				InputStream is = part.getEntityAs(InputStream.class);
				ContentDisposition meta = part.getContentDisposition();
				KycFileRequest fileRequest = new KycFileRequest();
				fileRequest.setFileInputStream(is);
				fileRequest.setFileName(meta.getFileName());
				fileRequest.setKycType(
						KycType.valueOf(part.getContentDisposition().getParameters().get("name").toUpperCase()));
				files.add(fileRequest);
			}
		}
	}

	private KycFileRequest extracted(FormDataMultiPart multipart, KycType type) {
		if (multipart != null) {
			for (BodyPart part : multipart.getBodyParts()) {
				KycType uploadedType = KycType
						.valueOf(part.getContentDisposition().getParameters().get("name").toUpperCase());
				if (uploadedType != type) {
					continue;
				}
				InputStream is = part.getEntityAs(InputStream.class);
				ContentDisposition meta = part.getContentDisposition();
				KycFileRequest fileRequest = new KycFileRequest();
				fileRequest.setFileInputStream(is);
				fileRequest.setFileName(meta.getFileName());
				fileRequest.setKycType(uploadedType);
				return fileRequest;
			}
		}
		return null;
	}

	@POST
	@Path("/{userId}/upload/uploadProfilePic")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public UserResponse uploadProfilePic(FormDataMultiPart multiPart, @PathParam("userId") Integer userId)
			throws MessagingException, FileNotFoundException, IOException {
		KycFileRequest profilePic = extracted(multiPart, KycType.PROFILEPIC);
		if (profilePic == null) {
			throw new UploadException("No Files are uploaded", ErrorCodes.EMPTY_REQUEST);
		}
		return userService.uploadProfilePic(userId, profilePic);
	}

	@POST
	@Path("/{userId}/uploadbase64/uploadProfilePic")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse uploadBase64ProfilePic(@PathParam("userId") Integer userId, KycFileRequest profilePic)
			throws MessagingException, FileNotFoundException, IOException {
		if (profilePic == null) {
			throw new UploadException("No Files are uploaded", ErrorCodes.EMPTY_REQUEST);
		}
		return userService.uploadProfilePic(userId, profilePic);
	}

	@POST
	@Path("/{userId}/uploadbase64/kyc")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse uploadBase64Kyc(@PathParam("userId") Integer userId, List<KycFileRequest> kycFiles)
			throws MessagingException, FileNotFoundException, IOException {
		if (kycFiles == null || kycFiles.isEmpty()) {
			throw new UploadException("No Files are uploaded", ErrorCodes.EMPTY_REQUEST);
		}
		return userService.uploadKyc(userId, kycFiles);
	}

	@POST
	@Path("/{userId}/upload/uploadContacts")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public UserResponse uploadContacts(FormDataMultiPart multiPart, @PathParam("userId") Integer userId)
			throws MessagingException, FileNotFoundException, IOException {
		KycFileRequest profilePic = extracted(multiPart, KycType.CONTACTS);
		if (profilePic == null) {
			throw new UploadException("No Files are uploaded", ErrorCodes.EMPTY_REQUEST);
		}
		return userService.uploadContacts(userId, profilePic);
	}

	@GET
	@Path("/{userId}/download/{kycType}")
	@Produces(MediaType.APPLICATION_JSON)
	public KycFileResponse downloadKycDocument(@PathParam("userId") Integer userId,
			@PathParam("kycType") String kycType) throws MessagingException, FileNotFoundException, IOException {
		return userService.downloadFile(userId, KycType.valueOf(kycType));
	}

	@PATCH
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PersonalDetailsResponseDto get(@PathParam("id") int userId,
			PersonalDetailsRequestDto PersonalDetailsRequestDto) {
		return userService.updatePersonalDetails(userId, PersonalDetailsRequestDto);
	}

	// Documentation not done
	@GET
	@Path("/{type}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<UserResponse> getNewUsersData(@PathParam("type") String type) {
		return userService.getNewUsersData(type);
	}

	@POST
	@Path("/sendOtp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse loginSendOtp(UserRequest userRequest) throws MessagingException {
		return userService.sendOtp(userRequest);
	}

	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(UserRequest userRequest, @QueryParam("grantType") String grantType) {
		UserResponse login = loginServiceFactory.getService(grantType.toUpperCase()).login(userRequest);
		ResponseBuilder response = Response.ok(login).header(AuthenticationFilter.ACCESS_TOKEN, login.getAccessToken());
		login.setAccessToken(null);
		return response.build();
	}

	@POST
	@Path("/resetpassword")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse registrationResetPassword(UserRequest userRequest) throws MessagingException {
		return userService.resetPasswordSendEmail(userRequest);
	}

	@POST
	@Path("/resetpassword/{emailToken}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse registrationResetPasswordEmailVerify(UserRequest userRequest,
			@PathParam("emailToken") String emailToken) {
		logger.info("mailToken : " + emailToken);
		return userService.verifyEmail(userRequest, emailToken);
	}

	@GET
	@Path("/personal/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PersonalDetailsResponseDto getPersonal(@PathParam("id") Integer id) {
		logger.info("get" + id);
		return userService.getPersonalDetails(id);
	}

	@POST
	@Path("/logout")
	@Produces(MediaType.APPLICATION_JSON)
	public String logout(@HeaderParam(value = "accessToken") String accessToken) throws MessagingException {
		if (StringUtils.isEmpty(accessToken)) {
			throw new AccessTokenMissingException("accessToken not passing", ErrorCodes.TOKEN_NOT_VALID); // Create new
																											// Exception
																											// and throw
		}
		String[] split = accessToken.split("\\" + ISignatureService.SEPARATOR);
		String decodeToken = new String(Base64.getDecoder().decode(split[0]));
		GenericAccessToken accessTokenValue = new Gson().fromJson(decodeToken, GenericAccessToken.class);
		loginServiceFactory.getService(accessTokenValue.getGrantType().name().toUpperCase()).logout(accessToken);
		return "Success";
	}

	@PATCH
	@Path("/{userId}/oxyscore")
	@Produces(MediaType.APPLICATION_JSON)
	public UserResponse addOxyScore(@PathParam("userId") Integer userId, UserRequest userRequest)
			throws MessagingException, FileNotFoundException, IOException {
		return userService.updateOxyScore(userId, userRequest);
	}

	@PATCH
	@Path("/{userId}/changeprimarytype/{primaryType}")
	@Produces(MediaType.APPLICATION_JSON)
	public UserResponse changePrimaryType(@PathParam("userId") Integer userId,
			@PathParam("primaryType") String primaryType, @QueryParam("deleteOldData") boolean deleteOldData)
			throws MessagingException, FileNotFoundException, IOException {
		PrimaryType newPrimaryType = PrimaryType.valueOf(primaryType);
		return userService.migrate(userId, newPrimaryType, deleteOldData);
	}

	@POST
	@Path("/{id}/city")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse saveCity(@PathParam(value = "id") Integer id, UserRequest userRequest) {
		return userService.saveCity(id, userRequest);
	}

	@GET
	@Path("/{userId}/document")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<UserDocumentStatus> getDetails(@PathParam("userId") Integer userId) {
		return userService.getDetails(userId);
	}

	@GET
	@Path("/{pincode}/pincode")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PincodeResultes pincode(@PathParam("pincode") String pinCode) throws ParseException {
		return userService.saveCity1(pinCode);
	}

	@PATCH
	@Path("/personal/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PersonalDetailsResponseDto updateProfilePage(@PathParam("id") int userId,
			PersonalDetailsRequestDto PersonalDetailsRequestDto) {
		return userService.updateProfilePage(userId, PersonalDetailsRequestDto);
	}

	@POST
	@Path("/{id}/experian")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ExperianResponceDto experian(@PathParam("id") Integer userId, ExperianRequestDto experianRequestDto)
			throws Exception {
		System.out.println(userId);
		return experianService.singileAction(userId, experianRequestDto);
	}

	@GET
	@Path("/{id}/getexperian")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ExperianResponceDto getExperian(@PathParam("id") Integer userId) {
		System.out.println(userId);
		return experianService.getExperian(userId);
	}

	@PATCH
	@Path("/profile/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PersonalDetailsResponseDto ProfilePageSubmissionAtATime(@PathParam("id") int userId,
			PersonalDetailsRequestDto PersonalDetailsRequestDto) {
		return userService.ProfilePageSubmissionAtATime(userId, PersonalDetailsRequestDto);
	}

	@GET
	@Path("/{userId}/loanrequestedamount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse getLoanRequestedAmountAndPaymentFeeRoi(@PathParam("userId") Integer userId) {
		System.out.println(userId);
		return userService.getLoanRequestedAmountAndPaymentFeeRoi(userId);
	}

	@PATCH
	@Path("{userId}/adminloanrequestedamount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse updateLoanRequestedAmountAndPaymentFeeRoi(@PathParam("userId") int userId,
			UserRequest userRequest) {
		return userService.updateLoanRequestedAmountAndPaymentFeeRoi(userId, userRequest);
	}

	@GET
	@Path("/{userId}/borrowerfee")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse calculatingFeeValue(@PathParam("userId") Integer userId) {
		System.out.println(userId);
		return userService.calculatingFeeValue(userId);
	}

	@POST
	@Path("/{userId}/upload/lendertransactiondetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public UserResponse lenderTransactionDetails(FormDataMultiPart multiPart, @PathParam("userId") Integer userId)
			throws MessagingException, FileNotFoundException, IOException {
		List<KycFileRequest> files = new ArrayList<KycFileRequest>();
		extracted(files, multiPart);
		if (files.isEmpty()) {
			throw new UploadException("No Files are uploaded", ErrorCodes.EMPTY_REQUEST);
		}
		return userService.uploadScowTransactionDetails(userId, files);
	}

	@POST
	@Path("/{id}/savelendertransaction")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ScrowLenderTransactionResponse saveLenderScrowTransactionDetails(@PathParam("id") Integer userId,
			ScrowTransactionRequest scrowTransactionRequest) throws Exception {
		logger.info("saveLenderScrowTransactionDetails  ........... " + userId);
		return userService.saveScrowTransactionDetails(userId, scrowTransactionRequest);
	}

	@GET
	@Path("/{userId}/download/{kycType}/{documnetId}")
	@Produces(MediaType.APPLICATION_JSON)
	public KycFileResponse downloadScrowTransactionScreenShot(@PathParam("userId") Integer userId,
			@PathParam("kycType") String kycType, @PathParam("documnetId") Integer documnetId)
			throws MessagingException, FileNotFoundException, IOException {
		return userService.downloadTransactionScreenshot(documnetId, userId, KycType.valueOf(kycType));
	}

	@POST
	@Path("/{userId}/getlenderwallettrns")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ScrowWalletResponse getLenderWalletDetails(@PathParam("userId") Integer userId, PageDto pageDto) {
		logger.info("getLenderWalletDetails  ........... " + userId);
		ScrowWalletResponse response = userService.getLenderWalletDetails(userId, pageDto);
		return response;
	}

	@POST
	@Path("/loanOfferAccepetence")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse loanOfferAccepetence(UserRequest userRequest) {
		return userService.loanOfferAccepetence(userRequest);
	}

	@POST
	@Path("/approvelenderwallettransaction/{documnetId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ScrowLenderTransactionResponse approveLenderWalletTransaction(@PathParam("documnetId") Integer documnetId)
			throws Exception {
		logger.info("approveLenderWalletTransaction  ........... " + documnetId);
		return userService.approveLenderWalletTransaction(documnetId);
	}

	@POST
	@Path("/searchlenderwallet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ScrowWalletResponse searchlenderwallet(ScrowTransactionRequest scrowTransactionRequest) {
		logger.info("searchlenderwallet  ........... " + scrowTransactionRequest.getFirstName()
				+ " Page Number --------" + scrowTransactionRequest.getPage().getPageNo());
		ScrowWalletResponse response = userService.searchlenderwallet(scrowTransactionRequest,
				scrowTransactionRequest.getPage());
		return response;
	}

	@POST
	@Path("/getcicreport")
	@Produces(MediaType.APPLICATION_JSON)
	public KycFileResponse generateCICReport() {
		logger.info("--------------------generateCICReport------------------------------");
		return userService.generateCICReport();
	}

	@POST
	@Path("/updateescrowwallettransaction/{documnetId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ScrowLenderTransactionResponse updateEscrowWalletTransaction(@PathParam("documnetId") Integer documnetId,
			ScrowTransactionRequest scrowTransactionRequest) {
		logger.info("updateEscrowWalletTransaction  ........... " + documnetId + " Status --------"
				+ scrowTransactionRequest.getStatus());
		return userService.updateEscrowWalletTransaction(documnetId, scrowTransactionRequest);
	}

	@POST
	@Path("/{userId}/application/loansbyapplication")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<ApplicationResponseDto> loansByApplication(@PathParam(value = "userId") int userId,
			ScrowTransactionRequest dto) throws MessagingException {
		logger.info("loansByApplication  ........... " + userId);
		if (dto.getUserId() == 0) {
			dto.setUserId(userId);
		}
		return userService.loansByApplication(userId, dto);
	}

	@POST
	@Path("/{userId}/updtnachtype")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto updEnachType(@PathParam(value = "userId") int userId,
			EnachTypeRequestDto enachTypeRequestDto) throws MessagingException {
		logger.info("updEnachType  ........... " + userId);
		return userService.updEnachType(userId, enachTypeRequestDto);
	}

	@POST
	@Path("/saveeanchminimumwtdrwusers")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse saveEanchMinimumWtdrwUsers(EnachMiniumWithDrwUserRequest request) throws MessagingException {
		logger.info("---------- saveEanchMinimumWtdrwUsers  ........... ");
		return userService.saveEanchMinimumWtdrwUsers(request);
	}

	@POST
	@Path("/getenachminimumwithdrawusers")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<EnachMinimunWithdrawUsersDto> getEnachMinimumWithdrawUsers(PageDto pageDto)
			throws MessagingException {
		logger.info("----------getEnachMinimumWithdrawUsers  ........... ");
		return userService.getEnachMinimumWithdrawUsers(pageDto);
	}

	@POST
	@Path("/searchenachminimumwithdrawusers")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<EnachMinimunWithdrawUsersDto> searchEnachMinimumWithdrawUsers(ScrowTransactionRequest dto)
			throws MessagingException {
		logger.info("----------searchEnachMinimumWithdrawUsers  ........... ");
		return userService.searchEnachMinimumWithdrawUsers(dto);
	}

	@POST
	@Path("/nominee")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NomineeResponseDto nomineeDetails(NomineeRequestDto nomineeRequestDto) throws MessagingException {
		logger.info("----------searchEnachMinimumWithdrawUsers  ........... ");
		return userService.nomineeDetails(nomineeRequestDto);
	}

	@GET
	@Path("/nominee/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NomineeResponseDto getNominee(@PathParam("id") Integer id) {
		return userService.getNominee(id);

	}

	@Scheduled(cron = "0 0 */2 * * ?")
	public int kycPendingSchedular() {
		return userService.kycPendingSchedular();

	}

	@PATCH
	@Path("/oxyfarms")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse registractionForOxyfarms(UserRequest userRequest) {
		return userService.registractionForOxyfarms(userRequest);

	}

	@POST
	@Path("/{userId}/contactDetailsFromIos")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KycFileResponse uploadContactByIos(@PathParam("userId") Integer userId,
			ContactDetailsForIos contactDetailsForIos) throws IOException {
		return userService.uploadContactByIos(userId, contactDetailsForIos);
	}

	@GET
	@Path("/{userId}/getIosContactDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KycFileResponse getIosContactDetails(@PathParam("userId") int userId) {
		return userService.getIosContactDetails(userId);
	}

	@POST
	@Path("/{uniqueNumber}/paymentScreenshot")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public UserResponse uploadPaymentScreenshot(FormDataMultiPart multiPart,
			@PathParam("uniqueNumber") String uniqueNumber)
			throws MessagingException, FileNotFoundException, IOException {
		List<KycFileRequest> files = new ArrayList<KycFileRequest>();
		extracted(files, multiPart);
		if (files.isEmpty()) {
			throw new UploadException("No Files are uploaded", ErrorCodes.EMPTY_REQUEST);
		}
		return userService.uploadPaymentScreenshot(uniqueNumber, files);
	}

	@POST
	@Path("/paymentUpload")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PaymentUploadHistoryResponseDto uploadPaymentHistory(
			PaymentUploadHistoryRequestDto paymentUploadHistoryRequestDto) {
		return userService.uploadPaymentHistory(paymentUploadHistoryRequestDto);
	}

	@POST
	@Path("/{parentRequestId}/lenderRatingToBorrowerApplication")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderRatingResponseDto lenderRatingToBorrowerApplication(
			@PathParam(value = "parentRequestId") Integer parentRequestId,
			LenderRatingRequestDto lenderRatingRequestDto) {
		logger.info("Lender Rating....");

		return userService.lenderRatingToBorrowerApplication(parentRequestId, lenderRatingRequestDto);
	}

	@POST
	@Path("/{documentId}/updatePaymentUploadHistoryStatus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PaymentUploadHistoryResponseDto updatePaymentUploadHistoryStatus(@PathParam("documentId") int documentId,
			PaymentUploadHistoryRequestDto paymentUploadHistoryRequestDto) {
	
		return userService.updatePaymentUploadHistoryStatus(documentId, paymentUploadHistoryRequestDto);
	}

	@GET
	@Path("/{uniqueNumber}/borrowerInformation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<UserResponse> borrowerInformation(@PathParam("uniqueNumber") String uniqueNumber) {
		System.out.println(uniqueNumber);
		return userService.borrowerInformation(uniqueNumber);

	}

	@GET
	@Path("/{userId}/countOfPaymentScreenshot")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PaymentUploadHistoryResponseDto countOfpaymentScreenshot(@PathParam("userId") Integer userId) {
		return userService.countOfPaymentScreenshot(userId);
	}

	@POST
	@Path("{status}/getPaymentUploadedListAndCount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PaymentSearchByStatus getPaymentUploadedList(@PathParam("status") String status,
			PaginationRequestDto paginationRequestDto) {
		return userService.getPaymentUploadedListAndCount(status, paginationRequestDto);
	}

	@GET
	@Path("/getUpdatedInfoByOxyMembers/{updatedName}/{paymentStatus}") // performed by admin
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<PaymentUploadHistoryResponseDto> getUpdatedInfoByOxyMembers(
			@PathParam("updatedName") String updatedName, @PathParam("paymentStatus") String paymentStatus) {
		return userService.getUpdatedInfoByOxyMembers(updatedName, paymentStatus);

	}

	@GET
	@Path("/{startDate}/{endDate}/lenderProfitInformation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderProfitResponse getLenderProfitInformation(@PathParam("startDate") String startDate,
			@PathParam("endDate") String endDate) {
		return userService.getLenderProfitInformation(startDate, endDate);
	}

	@GET
	@Path("/sendingMailToApprovePayments")
	// @Scheduled(cron = "0 30 12 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String sendingMailToApprovePayments() {
		return userService.sendingMailToApprovePayments();
	}

	@GET
	@Path("/{userId}/getLenderProfitDetails/{month}") // performed by lender
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderProfitResponse getLenderProfitDetails(@PathParam("userId") int userId, @PathParam("month") int month) {
		return userService.getLenderProfitDetails(userId, month);
	}

	@GET
	@Path("/fetchingBorrowerVanNumber/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserVanNumberResponseDto fetchBorrowerVanNumber(@PathParam("userId") Integer userId) {
		return userService.fetchBorrowerVanNumber(userId);
	}

	@POST
	@Path("{days}/{paymentStatus}/getPaymentStatistics")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PaymentSearchByStatus getPaymentStatistics(@PathParam("days") int days,
			@PathParam("paymentStatus") String paymentStatus, PaginationRequestDto paginationRequestDto) {
		return userService.getPaymentStatistics(days, paymentStatus, paginationRequestDto);
	}

	@POST
	@Path("/lenderReferring")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceResponse lenderReferenceDetails(LenderReferenceRequestDto lenderRefernceDto)
			throws MessagingException {
		return userService.lenderReferenceDetails(lenderRefernceDto);
	}

	@POST
	@Path("/{lenderId}/displayingReferrerInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceResponse displayingRefereesInfo(@PathParam("lenderId") Integer lenderId,
			PaginationRequestDto pageRequestDto) {
		return userService.displayingRefereesInfo(lenderId, pageRequestDto);

	}

	@POST
	@Path("/{lenderId}/refereeRegisteredInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceResponse gettingLenderRefereesRegisteredInfo(@PathParam("lenderId") Integer lenderId,
			PaginationRequestDto pageRequestDto) {
		return userService.gettingLenderRefereesRegisteredInfo(lenderId, pageRequestDto);
	}

	@POST
	@Path("/iciciCallBack")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String decryptingTheDataByPriavteKey(RequestFromICICI requestFromICICI) {
		return serviceForICICI.decryptingTheDataByPriavteKey(requestFromICICI);
	}

	@GET
	@Path("/mailContentShowingToLender")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderMailResponse showingMailContentToLender() {
		return userService.showingMailContentToLender();
	}

	@POST
	@Path("/uploadExcel/{date}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public FileResponse uploadExcelSheets(FormDataMultiPart multipart, @PathParam("date") String date) {
		List<KycFileRequest> files = new ArrayList<KycFileRequest>();
		extracted(files, multipart);
		if (files.isEmpty()) {
			throw new UploadException("No Files are uploaded", ErrorCodes.EMPTY_REQUEST);
		}
		return userService.uploadExcelSheets(files, date);

	}

	@GET
	@Path("/downloadExcel/{date}/{kyc}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public FileResponse downloadExcelSheets(@PathParam("date") String date, @PathParam("kyc") String kyc) {
		return userService.downloadExcelSheets(date, kyc);
	}

	@POST
	@Path("/getContactsFromGmailAccount/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public List<EmailAddressResponse> getContactsFromGmailAccount(@PathParam("userId") Integer userId,
			GmailSignInResponse response) throws IOException, GeneralSecurityException, InterruptedException {
		logger.info("getContactsFromGmailAccount Api started");
		return userService.getContactsFromGmailAccount(userId, response);
	}

	@GET
	@Path("/getGmailAuthorization/{type}/{userType}/{projectType}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public GmailSignInResponse getGmailAuthorization(@PathParam("type") String type,
			@PathParam("userType") String userType, @PathParam("projectType") String projectType) throws IOException, GeneralSecurityException, InterruptedException {
		logger.info("getGmailAuthorization Api started");
		return userService.getGmailAuthorization(type, userType, projectType);
	}

	@GET
	@Path("/getLenderStoredEmailContacts/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public List<EmailAddressResponse> getLenderStoredEmailContacts(@PathParam("userId") int userId)
			throws JsonParseException, JsonMappingException, IOException {
		return userService.getLenderStoredEmailContacts(userId);
	}

	@POST
	@Path("/{refereeId}/gettingAmountDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceAmountResponseDto gettingLenderReferenceAmountDetails(
			@PathParam("refereeId") String refereeId1, PaginationRequestDto pageRequestDto) {
		return userService.gettingLenderReferenceAmountDetails(refereeId1, pageRequestDto);
	}

	@GET
	@Path("/{refereeId}/displayingRefereePaymentStatus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<LenderReferralPaymentStatus> displayingRefereePaymentStatus(@PathParam("refereeId") String refereeId1) {
		return userService.displayingRefereePaymentStatus(refereeId1);

	}

	@POST
	@Path("/groupingOfBorrowers")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersLoanOwnerInformation displayingLoanOwnerNames(PaginationRequestDto pageRequestDto) {
		return userService.displayingLoanOwnerNames(pageRequestDto);
	}

	@POST
	@Path("/{loanOwner}/gettingLenderBorrowerOwner")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersLoanOwnerInformation gettingLenderBorrowerOwner(@PathParam("loanOwner") String loanOwnerName,
			PaginationRequestDto pageRequestDto) {
		return userService.gettingLenderBorrowerOwner(loanOwnerName, pageRequestDto);
	}

	@PATCH
	@Path("{id}/updatingWhatsAppNumber")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PersonalDetailsResponseDto updatingWhatsAppNumberForExistingUser(@PathParam("id") Integer id,
			PersonalDetailsRequestDto userRequest) {
		return userService.updatingWhatsAppNumberForExistingUser(id, userRequest);
	}

	@POST
	@Path("/veifyExistenceOfSocialLoginUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public GmailSignInResponse veifyExistenceOfSocialLoginUser(SocialLoginRequestDto request)
			throws GeneralSecurityException, IOException {
		return socialLoginService.veifyExistenceOfSocialLoginUser(request);
	}

	@POST
	@Path("/newRegistationWithSocialLogin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public UserResponse newRegistationWithSocialLogin(UserRequest request)
			throws GeneralSecurityException, IOException {
		request.setCreate(true);
		return socialLoginService.newRegistationWithSocialLogin(request);
	}

	@POST
	@Path("/{userId}/readingQueriesFromUsers")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public StatusResponseDto readingQueriesFromUsers(@PathParam("userId") Integer userId,
			UserQueryDetailsRequestDto queryRequestDto) {
		return userService.readingQueriesFromUsers(userId, queryRequestDto);
	}

	@POST
	@Path("/{userId}/userQueryScreenshot")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public UserResponse uploadUserQueryScreenshot(@PathParam("userId") Integer userId, FormDataMultiPart multiPart)
			throws MessagingException, FileNotFoundException, IOException {
		List<KycFileRequest> files = new ArrayList<KycFileRequest>();
		extracted(files, multiPart);
		return userService.uploadUserQueryScreenshot(userId, files);
	}

	@POST
	@Path("/sendWhatsappOtp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse sendWhatsappOtp(UserRequest userRequest) throws MessagingException {
		return userService.sendWhatsappOtp(userRequest);
	}

	@POST
	@Path("/verifyWhatsappOtp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse verifyWhatsappOtp(UserRequest userRequest) throws MessagingException {
		return userService.verifyWhatsappOtp(userRequest);
	}

	@POST
	@Path("/assignUserExpertise")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public StatusResponseDto assignUserExpertise(UserRequest request) {
		return userService.assignUserExpertise(request);
	}

	@GET
	@Path("/fetchAdviseSeekers/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public List<AdviseSeekersResponseDto> fetchAdviseSeekers(@PathParam("userId") int userId) {
		return userService.fetchAdviseSeekers(userId);

	}

	@GET
	@Path("/validityDetailsOfMembership")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 30 6 * * ?")
	public List<ValidityResponse> gettingValidityDetails() {
		return userService.gettingValidityDetails();
	}

	@POST
	@Path("/sendBulkInviteThroughExcel/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public LenderReferenceResponse sendBulkInviteThroughExcel(FormDataMultiPart multiPart,
			@PathParam("userId") int userId, @FormDataParam("content") String content) throws IOException {
		KycFileRequest fileReqest = extracted(multiPart, KycType.BULKINVITE);
		return userService.sendBulkInviteThroughExcel(fileReqest, userId, content);

	}

	@POST
	@Path("/tralert")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransactionAlertResponseDto transactionAlert(RequestFromICICI requestFromICICI) {
		return serviceForICICI.transactionAlert(requestFromICICI);
	}

	@PATCH
	@Path("/bankDetailsVerification")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderInterestResponseDto lenderBankDetailsVerification(LenderInterestRequestDto lenderInterestRequestDto) {
		return userService.lenderBankDetailsVerification(lenderInterestRequestDto);
	}

	@PATCH
	@Path("/savingBonusPaymentStatusFromExcel")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public LenderReferenceResponse savingBonusPaymentStatusFromExcel(FormDataMultiPart multiPart)
			throws MessagingException, FileNotFoundException, IOException {

		List<KycFileRequest> files = new ArrayList<KycFileRequest>();
		extracted(files, multiPart);
		if (multiPart != null) {
			for (BodyPart part : multiPart.getBodyParts()) {
				InputStream is = part.getEntityAs(InputStream.class);
				ContentDisposition meta = part.getContentDisposition();
				KycFileRequest fileRequest = new KycFileRequest();
				fileRequest.setFileInputStream(is);
				fileRequest.setFileName(meta.getFileName());
				fileRequest.setKycType(
						KycType.valueOf(part.getContentDisposition().getParameters().get("name").toUpperCase()));
				files.add(fileRequest);
			}
		}

		if (files.isEmpty()) {
			throw new UploadException("No Files are uploaded", ErrorCodes.EMPTY_REQUEST);
		}
		return userService.savingBonusPaymentStatusFromExcel(files);
	}

	@PATCH
	@Path("/savingBonusPaymentStatusFromExcelForDealIds")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public LenderReferenceResponse savingBonusPaymentStatusFromExcelForDealIds(FormDataMultiPart multiPart)
			throws MessagingException, FileNotFoundException, IOException {

		List<KycFileRequest> files = new ArrayList<KycFileRequest>();
		extracted(files, multiPart);
		if (multiPart != null) {
			for (BodyPart part : multiPart.getBodyParts()) {
				InputStream is = part.getEntityAs(InputStream.class);
				ContentDisposition meta = part.getContentDisposition();
				KycFileRequest fileRequest = new KycFileRequest();
				fileRequest.setFileInputStream(is);
				fileRequest.setFileName(meta.getFileName());
				fileRequest.setKycType(
						KycType.valueOf(part.getContentDisposition().getParameters().get("name").toUpperCase()));
				files.add(fileRequest);
			}
		}

		if (files.isEmpty()) {
			throw new UploadException("No Files are uploaded", ErrorCodes.EMPTY_REQUEST);
		}
		return userService.savingBonusPaymentStatusFromExcelForDealIds(files);
	}

	@POST
	@Path("/newregister")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse newRegistrationCreate(UserRequest userRequest) throws MessagingException {
		userRequest.setCreate(true);
		return userService.newRegister(userRequest);
	}

	@PATCH
	@Path("/newregister")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse newRegistrationUpdate(UserRequest userRequest) throws MessagingException {
		return userService.newRegister(userRequest);
	}

	@POST
	@Path("/{partnerId}/partnerAgreementType")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public UserResponse uploadPartnerAgreementType(@PathParam("partnerId") Integer partnerId,
			FormDataMultiPart multiPart) throws MessagingException, FileNotFoundException, IOException {
		List<KycFileRequest> files = new ArrayList<KycFileRequest>();
		extracted(files, multiPart);
		return userService.uploadPartnerAgreementType(partnerId, files);
	}

	@GET
	@Path("/{partnerId}/downloadPartnerAgreementType/{kycType}")
	@Produces(MediaType.APPLICATION_JSON)
	public KycFileResponse downloadPartnerAgreementType(@PathParam("partnerId") Integer partnerId,
			@PathParam("kycType") String kycType) throws MessagingException, FileNotFoundException, IOException {
		return userService.downloadPartnerAgreementType(partnerId, KycType.valueOf(kycType));
	}

	@GET
	@Path("/{userId}/{userType}/accessTokenGeneration")
	@Produces(MediaType.APPLICATION_JSON)
	public Response accessToken(@PathParam("userId") int userId, @PathParam("userType") String userType) {
		UserResponse tokenResponse = abstractLoginService.accessToken(userId, userType);
		ResponseBuilder response = Response.ok(tokenResponse).header(AuthenticationFilter.ACCESS_TOKEN,
				tokenResponse.getAccessToken());
		tokenResponse.setAccessToken(null);
		return response.build();
	}

	@GET
	@Path("/{startDate}/{endDate}/getcicreportBasedOnDates")
	@Produces(MediaType.APPLICATION_JSON)
	public KycFileResponse generateCICReportBasedOnDates(@PathParam("startDate") String startDate,
			@PathParam("endDate") String endDate) {
		logger.info("--------------------generateCICReport------------------------------");
		return userService.generateCICReportBasedOnDates(startDate, endDate);
	}

	@PATCH
	@Path("/updatingRenewalMessageStatus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse updatingRenewalMessageStatus(UserRequest request) {
		return userService.updatingRenewalMessageStatus(request);
	}

	@POST
	@Path("/getepay")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String callbackForGetePayment(GetepayRequestDto getepayRequestDto) {
		return userService.callbackForGetePayment(getepayRequestDto);
	}

	@GET
	@Path("/birthdayInvitation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Scheduled(cron = "0 30 4 * * ?")
	public String birthDayInvitaionsTask() {
		// This method will be automatically invoked based on the specified cron
		// expression
		return userService.sendBirthdayInvitationtoRegisterLender();
	}

	@POST
	@Path("/sentCongrats")
	@Scheduled(cron = "0 30 12 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String sentCongratsnotificationToLenders() {

		return userService.sendCongrastNotificationToParticipatedLenders();

	}

	@PATCH
	@Path("/{userId}/testUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateTestUser(@PathParam("userId") Integer userId) {
		return userService.updateTestUsers(userId);
	}

	@GET
	@Path("/getTestUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<TestUserResponse> getListOfTestUser() {

		return userService.getTestUserDetails();
	}

	@POST
	@Path("/ondc/onboarding/on_subscribe")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String ondcCallBack() {
		logger.info("ondc : ");
		return "Success";

	}

	@GET
	@Path("/lender/personal/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserPersonalDetailsResponse getLenderPersonal(@PathParam("id") Integer id) {
		return userService.getLenderDetails(id);
	}
	@POST
	@Path("/{userId}/dealAndParticipationCount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON) 
	public DealAndParticipationCount countForDealAndParticipation(@PathParam("userId") int userId) {
		return userService.countForDealAndParticipation(userId);
	}


	@GET
	@Path("/{userId}/allQueriesCount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public QueriesCountsResponse userAllQueriesCount(@PathParam(value = "userId") int userId) {
		
		return userService.countUserRaiseQueries(userId);
		}
	
	@POST
	@Path("/inviteCampaignThroughExcel")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public BulkinviteResponse inviteCampaignThroughExcel(FormDataMultiPart multiPart,
			@FormDataParam("message") String message, @FormDataParam("inviteType") String inviteType,
			@FormDataParam("imageUrl") String whatsappImage, @FormDataParam("mailSubject") String mailSubject,
			@FormDataParam("mailDispalyName") String mailDispalyName, @FormDataParam("projectType") String projectType,
			@FormDataParam("sampleEmail") String sampleEmail, @FormDataParam("sampleMobile") String sampleMobile,@FormDataParam("userType") String userType,
			@FormDataParam("userid") String userid

	) throws FileNotFoundException,IOException {
		
		KycFileRequest fileReqest = extracted(multiPart, KycType.BULKINVITE);
	   
	    return userService.sendCampaignThroughExcel(fileReqest,message,inviteType,whatsappImage,mailSubject,mailDispalyName,projectType,sampleEmail,
	    		sampleMobile,userType,userid);

	}
	
	@POST
	@Path("/downloadCampaignUrl")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String downloandCampaignUrl(FormDataMultiPart multiPart)throws FileNotFoundException,IOException {
		KycFileRequest fileReqest = extracted(multiPart, KycType.BULKINVITE);
		
		return userService.convertUrltoPgnimage(fileReqest);
	}

	@POST
	@Path("/getAllContactsFromGmailAccount/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public List<EmailAddressResponse> getAllContactsFromGmailAccount(@PathParam("userId") int userId,
			GmailSignInResponse response) throws IOException, GeneralSecurityException, InterruptedException {
		logger.info("getContactsFromGmailAccount Api started");
		return userService.getAllContactsFromGmailAccount(userId, response);
	}
	
	
}