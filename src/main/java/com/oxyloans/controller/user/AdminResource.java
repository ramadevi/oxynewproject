package com.oxyloans.admin.user;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.response.admin.AdminResponse;
import com.oxyloans.service.user.UserService;

@Component
@Path("v2/admin")
public class AdminResource {
	@PostConstruct
	public void print() {
		System.out.println("REST initia......");
	}
	
	@Autowired
	private UserService userService;
	
	@GET
	@Path("/stats")
	@Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public AdminResponse totalNumberOfUsers() {
		return userService.totalNumberOfUsers();
		
	}
	
    

}
