package com.oxyloans.rest.user;

import java.util.List;

import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.partner.dto.PartnerRegistrationRequestDto;
import com.oxyloans.partner.dto.PartnerRegistrationResponseDto;
import com.oxyloans.registration.dto.UserRegistrationRequestDto;
import com.oxyloans.registration.dto.UserRegistrationResponseDto;
import com.oxyloans.request.PartnerLoanInfoResponse;
import com.oxyloans.request.PartnerNDArequestDto;
import com.oxyloans.request.PartnerRequestDto;
import com.oxyloans.request.user.ReferenceDetailsRequestDto;
import com.oxyloans.request.user.ReferenceDetailsResponseDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.LenderMailResponse;
import com.oxyloans.response.user.LendersWithdrawResponse;
import com.oxyloans.response.user.OxyUserDetailsResponse;
import com.oxyloans.response.user.PartnerResponseDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.service.user.NewUserService;
import com.oxyloans.user.type.dto.OxyUserTypeRequestDto;
import com.oxyloans.user.type.dto.OxyUserTypeResponseDto;

@Component
@Path("v1/user")
public class NewUserResource {

	@Autowired
	private NewUserService newUserService;

	@POST
	@Path("/newUserRegistration")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserRegistrationResponseDto UserNewRegistration(UserRegistrationRequestDto userRegistrationRequestDto) {
		return newUserService.UserNewRegistration(userRegistrationRequestDto);

	}

	@PATCH
	@Path("/emailVerification")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserRegistrationResponseDto userEmailVerification(UserRegistrationRequestDto userRegistrationRequestDto) {
		return newUserService.userEmailVerification(userRegistrationRequestDto);
	}

	@POST
	@Path("/sendingEmailActivationLink")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserRegistrationResponseDto sendingEmailActivationLink(
			UserRegistrationRequestDto userRegistrationRequestDto) {
		return newUserService.sendingEmailActivationLink(userRegistrationRequestDto);
	}

	@PATCH
	@Path("/borrowerReferenceDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ReferenceDetailsResponseDto updateReferenceDetailsToBorrower(
			ReferenceDetailsRequestDto referenceDetailsRequestDto) {
		return newUserService.updateReferenceDetailsToBorrower(referenceDetailsRequestDto);
	}

	@PATCH
	@Path("/emailIdUpdationToOldUsers")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserRegistrationResponseDto oldUsersRegistrationStep2Pending(
			UserRegistrationRequestDto userRegistrationRequestDto) {
		return newUserService.oldUsersRegistrationStep2Pending(userRegistrationRequestDto);
	}

	@GET
	@Path("/generatingPasswordForPartners")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyUserTypeResponseDto generatingPasswordForPartners() {
		return newUserService.generatingPasswordForPartners();
	}

	@PATCH
	@Path("/savingPartnerDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse savingPartnerDetails(UserRequest userRequest) {
		return newUserService.savingPartnerDetails(userRequest);
	}

	@POST
	@Path("/resetpasswordForPartners")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse resetpasswordForPartners(UserRequest userRequest) throws MessagingException {
		return newUserService.resetpasswordForPartners(userRequest);
	}

	@GET
	@Path("/{utmName}/gettingPartnerDetailsBasedOnUtm")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyUserDetailsResponse gettingPartnerDetailsBasedOnUtm(@PathParam(value = "utmName") String utmName) {
		return newUserService.gettingPartnerDetailsBasedOnUtm(utmName);
	}

	@POST
	@Path("/partnerRegistrationFlow")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PartnerResponseDto partnerRegistrationFlow(PartnerRequestDto requestDto) {

		return newUserService.partnerRegistrationFlow(requestDto);
	}

	@GET
	@Path("/{partnerId}/mailContentShowingToPartner")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderMailResponse showingMailContentToPartner(@PathParam("partnerId") Integer partnerId) {
		return newUserService.showingMailContentToPartner(partnerId);
	}

	@GET
	@Path("/{dealId}/withdrawalInfoBasedOnId")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LendersWithdrawResponse withdrawalInformationBasedOnId(@PathParam("dealId") Integer dealId) {
		return newUserService.withdrawalInformationBasedOnId(dealId);
	}

	@PATCH
	@Path("/verifyingMobileAndEmailForPartner")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PartnerResponseDto verifyingMobileAndEmailForPartner(OxyUserTypeRequestDto requestDto)
			throws MessagingException {
		return newUserService.verifyingMobileAndEmailForPartner(requestDto);
	}

	@GET
	@Path("/{utmName}/countOfLRAndBRforPartner")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PartnerResponseDto gettingCountOfLendersAndBorrowersForPartner(@PathParam("utmName") String utmName) {
		return newUserService.gettingCountOfLendersAndBorrowersForPartner(utmName);
	}

	@PATCH
	@Path("/{utmName}/bankDetailsForPartner")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PartnerResponseDto bankDetailsForPartner(@PathParam("utmName") String utmName,
			OxyUserTypeRequestDto requestDto) throws MessagingException {
		return newUserService.bankDetailsForPartner(utmName, requestDto);
	}

	@GET
	@Path("/{utmName}/partnerProfileDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PartnerResponseDto partnerProfileDetails(@PathParam("utmName") String utmName) throws MessagingException {
		return newUserService.partnerProfileDetails(utmName);
	}

	@POST
	@Path("/{utmName}/partnerNDA")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PartnerResponseDto preparingPartnerNDA(@PathParam("utmName") String utmName, PartnerNDArequestDto requestDto)
			throws MessagingException {
		return newUserService.preparingPartnerNDA(utmName, requestDto);
	}

	@PATCH
	@Path("/{id}/additionalFieldsToUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PartnerRegistrationResponseDto additionalFieldsApprovalFromAdmin(@PathParam("id") Integer id,
			PartnerRegistrationRequestDto partnerRegistrationRequestDto) {
		return newUserService.additionalFieldsApprovalFromAdmin(id, partnerRegistrationRequestDto);
	}

	@POST
	@Path("/{utmName}/partnerBorrowersLoanInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<PartnerLoanInfoResponse> partnerBorrowersLoanInfo(@PathParam("utmName") String utmName) {
		return newUserService.partnerBorrowersLoanInfo(utmName);
	}

	@POST
	@Path("/sendMobileOtp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse sendMobileOtp(UserRequest userRequest) throws MessagingException {
		return newUserService.sendMobileOtp(userRequest);
	}

	@POST
	@Path("/verifyMobileOtpValue")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse verifyMobileOtpValue(UserRequest request) {
		return newUserService.verifyMobileOtpValue(request);
	}

	@POST
	@Path("/forgot-passsword")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserRegistrationResponseDto forgotPasswordForMobileApp(
			UserRegistrationRequestDto userRegistrationRequestDto) {
		return newUserService.forgotPasswordForMobileApp(userRegistrationRequestDto);
	}

	@PATCH
	@Path("/forgot-passsword-verification")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserRegistrationResponseDto forgotPasswordForMobileAppVerification(
			UserRegistrationRequestDto userRegistrationRequestDto) {
		return newUserService.forgotPasswordForMobileAppVerification(userRegistrationRequestDto);
	}

	@PATCH
	@Path("/{userId}/site-tool")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserRegistrationResponseDto updateSiteTool(@PathParam("userId") int userId) {
		return newUserService.updateSiteTool(userId);
	}

	@GET
	@Path("/{userId}/user-uniquenumber")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse getUserUniqueNumber(@PathParam("userId") int userId) {
		return newUserService.getUserUniqueNumber(userId);
	}

	@PATCH
	@Path("/invalidEmailUpdaion")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserRegistrationResponseDto updatecredentialwheninvalidemailenter(
			UserRegistrationRequestDto userRegistrationRequestDto) {
		return newUserService.updateCredentialByInvalidEmailEnterAtTimeOfRegistration(userRegistrationRequestDto);
	}

	@GET
	@Path("/generate_salt")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String generateSalt() {
		return newUserService.generateSalt();
	}

	@GET
	@Path("/generate_password/{password}/{salt}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String generatePassword(@PathParam("password") String password, @PathParam("salt") String salt) {
		return newUserService.generatePassword(password, salt);
	}

}
