package com.oxyloans.rest.principal;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.lender.withdrawalfunds.dto.LenderPrincipal;
import com.oxyloans.lender.withdrawalfunds.dto.LenderPrincipalResponseDto;
import com.oxyloans.principal.dto.LenderPayementDetails;
import com.oxyloans.principal.dto.LenderPaymentsResponse;
import com.oxyloans.principalReturn.service.PrincipalReturnServiceRepo;

@Component
@Path("v1/user")
public class PrincipalReturn {

	@Autowired
	private PrincipalReturnServiceRepo principalReturnService;

	@POST
	@Path("/{dealId}/principalReturn")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPrincipalResponseDto pricipalReturnToLender(@PathParam("dealId") Integer dealId,
			LenderPrincipal lenderPrincipal) {
		return principalReturnService.pricipalReturnToLender(dealId, lenderPrincipal);
	}

	@GET
	@Path("/{dealId}/{status}/principalReturnInitiated")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPrincipal getPrincipalInitiated(@PathParam("dealId") Integer dealId,
			@PathParam("status") String status) {
		return principalReturnService.getPrincipalInitiated(dealId, status);
	}

	@PATCH
	@Path("/{dealId}/pricipalReturnToH2H")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPrincipalResponseDto sendingPricipalReturnToH2H(@PathParam("dealId") Integer dealId,
			LenderPrincipal lenderPrincipal) {
		return principalReturnService.sendingPricipalReturnToH2H(dealId, lenderPrincipal);
	}
	
	@GET
	@Path("/deal-principal-history")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaymentsResponse getDealPrincipalHistory(@QueryParam("dealId") Integer dealId) {
		return principalReturnService.getDealPrincipalHistory(dealId);
	}
	
	@GET
	@Path("/deal-summary")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<LenderPayementDetails> bothPrincipalAndInteresReturnedDetailSummary(
			@QueryParam("dealId") Integer dealId, @QueryParam("amountType") String amountType) {
		return principalReturnService.bothPrincipalAndInteresReturnedDetailSummary(dealId, amountType);
	}
}
