package com.oxyloans.rest.loan;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.oxyloans.deallevel.disbursment.DealLevelInformationRepo;
import com.oxyloans.engine.template.PdfGeenrationException;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceDetails;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceResponse;
import com.oxyloans.entity.borrowers.deals.dto.BorrowerAutoLendingResponse;
import com.oxyloans.entity.borrowers.deals.dto.BorrowersDealsList;
import com.oxyloans.entity.borrowers.deals.dto.BorrowersDealsRequestDto;
import com.oxyloans.entity.borrowers.deals.dto.BorrowersDealsResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.ClosedDealsInformation;
import com.oxyloans.entity.borrowers.deals.dto.DealDisbursmentInformation;
import com.oxyloans.entity.borrowers.deals.dto.DealInformationRoiToLender;
import com.oxyloans.entity.borrowers.deals.dto.DealLevelLoanEmiCardInformation;
import com.oxyloans.entity.borrowers.deals.dto.DealLevelRequestDto;
import com.oxyloans.entity.borrowers.deals.dto.DealLevelResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.LenderGroupNamesRequestDto;
import com.oxyloans.entity.borrowers.deals.dto.LenderMappedToGroupIdResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.LenderPaticipatedDeal;
import com.oxyloans.entity.borrowers.deals.dto.LendersDealsInformation;
import com.oxyloans.entity.borrowers.deals.dto.LendersDealsResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.OxyLendersAcceptedDealsRequestDto;
import com.oxyloans.entity.borrowers.deals.dto.OxyLendersAcceptedDealsResponseDto;
import com.oxyloans.entity.lenders.group.OxyLendersGroup;
import com.oxyloans.entity.loan.LoanEmiCard;
import com.oxyloans.entity.loan.LoanRequest.LoanStatus;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.excelsheets.OxyTransactionDetailsFromExcelSheetsRequest;
import com.oxyloans.excelsheets.OxyTransactionDetailsFromExcelSheetsResponse;
import com.oxyloans.experian.dto.ExperianRequestDto;
import com.oxyloans.lender.participationToWallet.LenderIncomeRequestDto;
import com.oxyloans.lender.wallet.LenderReturnsResponseDto;
import com.oxyloans.lender.wallet.ScrowWalletResponse;
import com.oxyloans.loan.dto.LenderInvestmentDataDtoResponse;
import com.oxyloans.notifications.NotificationsRepo;
import com.oxyloans.payumoney.PayuMoneyPaymentDetailsRequest;
import com.oxyloans.payumoney.PayuMoneyPaymentDetaisResponce;
import com.oxyloans.payumoney.PayuMoneyService;
import com.oxyloans.request.ApplicationLevelEnachResponseDto;
import com.oxyloans.request.LenderFavouriteRequestDto;
import com.oxyloans.request.PageDto;
import com.oxyloans.request.SearchRequestDto;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.request.user.KycFileResponse;
import com.oxyloans.request.user.LenderReferenceRequestDto;
import com.oxyloans.request.user.SpreadSheetRequestDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.request.user.WhatsappCampaignDto;
import com.oxyloans.response.admin.AdminEmiDetailsResponseDto;
import com.oxyloans.response.admin.DisbursmentPendingResponseDto;
import com.oxyloans.response.admin.UserInformationResponseDto;
import com.oxyloans.response.user.ActiveLenderResponseDto;
import com.oxyloans.response.user.BorrowerDetailsRequestDto;
import com.oxyloans.response.user.BorrowersFeeBasedOnDate;
import com.oxyloans.response.user.BorrowersLoanOwnerInformation;
import com.oxyloans.response.user.BorrowersLoanOwnerNames;
import com.oxyloans.response.user.CommentsRequestDto;
import com.oxyloans.response.user.CommentsResponseDto;
import com.oxyloans.response.user.EnachScheduledMailResponseDto;
import com.oxyloans.response.user.EnachTransactionResponseDto;
import com.oxyloans.response.user.EquityLendersListsDto;
import com.oxyloans.response.user.LenderEscrowWalletDto;
import com.oxyloans.response.user.LenderFavouriteResponseDto;
import com.oxyloans.response.user.LenderLoansInformation;
import com.oxyloans.response.user.LenderNocResponseDto;
import com.oxyloans.response.user.LendersLoanInfo;
import com.oxyloans.response.user.ListOfWhatappGroupNames;
import com.oxyloans.response.user.LoanDisbursmentResponse;
import com.oxyloans.response.user.OxyLendersGroups;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.response.user.PaymentHistoryResponseDto;
import com.oxyloans.response.user.PaymentSearchByStatus;
import com.oxyloans.response.user.PaymentUploadHistoryResponseDto;
import com.oxyloans.response.user.ReadingCommitmentAmountDto;
import com.oxyloans.response.user.StatusResponseDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.response.user.WhatappInformation;
import com.oxyloans.service.enach.ENachMandateResponse;
import com.oxyloans.service.enach.EnachMandateReponseRequestDto;
import com.oxyloans.service.enach.IEnachService;
import com.oxyloans.service.esign.EsignInitResponse;
import com.oxyloans.service.loan.BucketPdfresponse;
import com.oxyloans.service.loan.LenderLoanService;
import com.oxyloans.service.loan.LoanServiceFactory;
import com.oxyloans.service.loan.dto.ApplicationResponseDto;
import com.oxyloans.service.loan.dto.BorrowerApplicationSummryDto;
import com.oxyloans.service.loan.dto.BorrowerIndividualLoansCountResposeDto;
import com.oxyloans.service.loan.dto.BucketPendingEmis;
import com.oxyloans.service.loan.dto.DurationChangeDto;
import com.oxyloans.service.loan.dto.LenderEmiDetailsResponseDto;
import com.oxyloans.service.loan.dto.LenderHistroryResponseDto;
import com.oxyloans.service.loan.dto.LenderIndividualLoansCountResposeDto;
import com.oxyloans.service.loan.dto.LenderTransactionHistoryResponseDto;
import com.oxyloans.service.loan.dto.LoanEmiCardPaymentDetailsRequestDto;
import com.oxyloans.service.loan.dto.LoanEmiCardPaymentDetailsResponseDto;
import com.oxyloans.service.loan.dto.LoanEmiCardResponseDto;
import com.oxyloans.service.loan.dto.LoanEmiGenerationResDto;
import com.oxyloans.service.loan.dto.LoanRequestDto;
import com.oxyloans.service.loan.dto.LoanResponseDto;
import com.oxyloans.service.loan.dto.NotificationCountResponseDto;
import com.oxyloans.service.loan.dto.OfferSentDetails;
import com.oxyloans.service.loan.dto.TotalLendersInformationResponseDto;
import com.oxyloans.service.loan.dto.UploadAgreementRequestDto;
import com.oxyloans.service.loan.dto.UsersLoansInformationResponseDto;
import com.oxyloans.whatsapp.dto.WhatsappMessagesDto;

@Component
@Path("/v1/user")
public class LoanResource {

	private static final Logger logger = LogManager.getLogger(LoanResource.class);

	@Autowired
	private LoanServiceFactory loanServiceFactory;

	@Autowired
	private IEnachService enachService;

	@Autowired
	private PayuMoneyService payuMoneyService;

	@Autowired
	private DealLevelInformationRepo dealLevelInformationRepo;

	@Autowired
	private NotificationsRepo notificationsRepo;

	@Autowired
	private LenderLoanService lenderLoanService;

	@PostConstruct
	public void postConstruct() {
		logger.info("dashboard Rest Resource");
	}

	@POST
	@Path("/{userId}/loan/{primaryType}/request")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto raiseLoanRequest(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, LoanRequestDto loanRequestDto) throws MessagingException {
		logger.info("Input : " + userId);
		loanRequestDto.setLoanProcessType("manual");
		return loanServiceFactory.getService(primaryType.toUpperCase()).loanRequest(userId, loanRequestDto);
	}

	@GET
	@Path("/{userId}/loan/{primaryType}/request/{loanRequestId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto getLoanRequest(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, @PathParam(value = "loanRequestId") int loanRequestId)
			throws MessagingException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).getLoanRequest(userId, loanRequestId);
	}

	@PATCH
	@Path("/{userId}/loan/{primaryType}/request/{loanRequestId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto updateRespondedLoanRequest(@PathParam(value = "userId") int userId,
			@PathParam(value = "loanRequestId") int loanRequestId, @PathParam("primaryType") String primaryType,
			LoanRequestDto loanRequestDto) throws MessagingException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).updateRespondedLoanRequest(userId,
				loanRequestId, loanRequestDto);
	}

	@PATCH
	@Path("/{userId}/loan/{primaryType}/request")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto updateLoanRequest(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, LoanRequestDto loanRequestDto) throws MessagingException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).updateLoanRequest(userId, loanRequestDto);
	}

	@PATCH
	@Path("/{userId}/loan/{primaryType}/request/{loanRequestId}/comment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto updateLoanRequestWithComment(@PathParam(value = "userId") int userId,
			@PathParam(value = "loanRequestId") int loanRequestId, @PathParam("primaryType") String primaryType,
			BorrowerDetailsRequestDto borrowerDetailsRequestDto) throws MessagingException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).updateLoanRequest(userId, loanRequestId,
				borrowerDetailsRequestDto);
	}

	@POST
	@Path("/{userId}/loan/{primaryType}/request/search")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<LoanResponseDto> searchLoanRequests(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, @QueryParam("includeUser") boolean includeUser,
			SearchRequestDto searchRequestDto) throws MessagingException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).searchRequests(userId, searchRequestDto);
	}

	@PATCH
	@Path("/{userId}/loan/{primaryType}/request/{loanRequestId}/{action}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto actOnLoanRequest(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, @PathParam("loanRequestId") Integer loanRequestId,
			@PathParam("action") String action)
			throws MessagingException, PdfGeenrationException, FileNotFoundException, IOException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).actOnRequest(userId, loanRequestId,
				LoanStatus.valueOf(action));
	}

	@GET
	@Path("/{userId}/loan/{primaryType}/request/{loanRequestId}/download")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KycFileResponse downloadLoanAgreement(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, @PathParam("loanRequestId") Integer loanRequestId)
			throws MessagingException, PdfGeenrationException, FileNotFoundException, IOException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).downloadLoanAgrement(userId, loanRequestId);
	}

	@POST
	@Path("/{userId}/loan/{primaryType}/{loanRequestId}/esign")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public EsignInitResponse esignAgreement(@PathParam("userId") Integer userId,
			@PathParam("loanRequestId") Integer loanRequestId, @PathParam("primaryType") String primaryType,
			UploadAgreementRequestDto agreementFileRequest) {
		return loanServiceFactory.getService(primaryType.toUpperCase()).esign(userId, loanRequestId,
				agreementFileRequest);
	}

	@POST
	@Path("/{userId}/loan/{primaryType}/{loanRequestId}/uploadAgreement")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto uploadSignedLoanAgreement(@PathParam("userId") Integer userId,
			@PathParam("loanRequestId") Integer loanRequestId, @PathParam("primaryType") String primaryType,
			UploadAgreementRequestDto agreementFileRequest) {
		return loanServiceFactory.getService(primaryType.toUpperCase()).uploadEsignedAgreementPdf(userId, loanRequestId,
				agreementFileRequest);
	}

	@POST
	@Path("/{userId}/loan/{primaryType}/search")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<LoanResponseDto> searchLoans(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, @QueryParam("includeUser") boolean includeUser,
			SearchRequestDto searchRequestDto) throws MessagingException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).searchLoans(userId, searchRequestDto);
	}

	@POST
	@Path("/loan/{loanId}/feepaid")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto feePaid(@PathParam("loanId") int loanId, @QueryParam("primaryType") PrimaryType primaryType,
			LoanRequestDto loanRequestDto) throws MessagingException {
		logger.info("Input : " + loanId);
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).feePaid(loanId, primaryType, loanRequestDto);
	}

	@POST
	@Path("/loan/{loanId}/{emiNumber}/emipaid")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanEmiCardResponseDto emiPaid(@PathParam("loanId") int loanId, @PathParam("emiNumber") int emiNumber,
			LoanEmiCardResponseDto request) {
		logger.info("Input : " + loanId);
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).emiPaid(loanId, emiNumber, request);
	}

	@PATCH
	@Path("/loan/{loanId}/{emiNumber}/eminotreceived")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanEmiCardResponseDto emiFailed(@PathParam("loanId") int loanId, @PathParam("emiNumber") int emiNumber) {
		logger.info("Input : " + loanId);
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).markEmiFailed(loanId, emiNumber);
	}

	@GET
	@Path("/{userId}/loan/{primaryType}/{loanId}/emicard")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<LoanEmiCardResponseDto> getEmiCard(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, @PathParam("loanId") Integer loanId) {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).getEmiCard(loanId);
	}

	@GET
	@Path("/{userId}/loan/{primaryType}/migrate")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String migrateOldSystemToNewSystem(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, @PathParam("loanId") Integer loanId) {
		logger.info("Input : " + userId);
		loanServiceFactory.getService(primaryType.toUpperCase()).migrateOldSystemToNewSystem();
		return "success";
	}

	@POST
	@Path("/loan/{loanId}/loanpreclose")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanEmiCardResponseDto loanPreClose(@PathParam("loanId") int loanId) {
		logger.info("Input : " + loanId);
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).loanPreClose(loanId);
	}

	@GET
	@Path("/loan/pending/{year}/{month}/{type}/{pageno}/{pagesize}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<LoanResponseDto> emiPaid(@PathParam("year") int year, @PathParam("month") int month,
			@PathParam("type") String type, @PathParam("pageno") int pageNo, @PathParam("pagesize") int pageSize) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).pendingEmis(year, month, type, pageNo, pageSize);
	}

	@PATCH
	@Path("/{userId}/loan/{primaryType}/updateuserstatus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse updateUserStatus(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, LoanRequestDto loanRequestDto) throws MessagingException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).updateUserStatus(userId, loanRequestDto);
	}

	@PATCH
	@Path("/{userId}/loan/{primaryType}/report")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse sendReport(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, UserRequest userRequest)
			throws MessagingException, PdfGeenrationException, IOException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).sendReport(userId, userRequest);

	}

//  @POST
//  @Path("/{userId}/loan/{primaryType}/enach")
//  @Produces(MediaType.APPLICATION_JSON)
//  public ENachMandateResponse generateEnachMandate(@PathParam(value = "userId") int userId, @PathParam("primaryType") String primaryType,
//          ENachMandateRequest eNachMandateRequest) {
//      logger.info("Input : " + eNachMandateRequest);
//      return enachService.generateMandateToken(userId, eNachMandateRequest);
//  }
	@POST
	@Path("/{userId}/loan/{primaryType}/enach/{mandateId}")
	@Produces(MediaType.APPLICATION_JSON)
	public ENachMandateResponse generateEnachMandate(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, @PathParam("mandateId") int mandateId) {
		return enachService.generateMandateToken(userId, mandateId);
	}

	@POST
	@Path("/{userId}/loan/enach/response")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean EnachMandateReponse(@PathParam(value = "userId") int userId,
			EnachMandateReponseRequestDto enachMandateReponseRequestDto) {
		return enachService.updateEnachMandateReponse(userId, enachMandateReponseRequestDto);
	}

	@POST
	@Path("/{userId}/{paymenttype}/fetchPaymentDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public PayuMoneyPaymentDetaisResponce fetchPayUMoneyPaymentDetails(@PathParam(value = "userId") int userId,
			PayuMoneyPaymentDetailsRequest payuMoneyPaymentDetailsRequest,
			@PathParam(value = "paymenttype") String paymentType) {
		return payuMoneyService.fetchPayUMoneyPaymentDetails(userId, payuMoneyPaymentDetailsRequest, paymentType);
	}

	@POST
	@Path("/loan/{oxyLoansTransactionNumber}/emipaidbyborrowerusingpaymentgateway")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanEmiCardResponseDto emiPaidByBorrowerUsingPaymentGateway(
			@PathParam("oxyLoansTransactionNumber") String oxyLoansTransactionNumber, LoanEmiCardResponseDto request) {
		logger.info("Input : " + oxyLoansTransactionNumber);
		return loanServiceFactory.getService(PrimaryType.BORROWER.name())
				.emiPaidByBorrowerUsingPaymentGateway(oxyLoansTransactionNumber, request);
	}

	@PATCH
	@Path("/favourite")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderFavouriteResponseDto selectFavouriteBorrower(LenderFavouriteRequestDto lenderFavouriteRequestDto) {

		return loanServiceFactory.getService(PrimaryType.LENDER.name())
				.selectFavouriteBorrower(lenderFavouriteRequestDto);

	}

	@POST
	@Path("/{userId}/{primaryType}/borrowerfee")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse BorrowerFee(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, @QueryParam("includeUser") boolean includeUser,
			UserRequest userRequest) throws MessagingException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).BorrowerFee(userId, userRequest);
	}

	@POST
	@Path("/{userId}/loan/{primaryType}/calculateprofilerisk")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse calculateprofileRisk(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, UserRequest userRequest) {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).calculateprofileRisk(userId, userRequest);
	}

	@GET
	@Path("/{userId}/getprofileandexperian")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse getExperianAndProfile(@PathParam("primaryType") String primaryType,
			@PathParam(value = "userId") int userId) {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getExperianAndProfile(userId);
	}

	@POST
	@Path("/{userId}/loan/sendOffer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto sendOffer(@PathParam(value = "userId") int userId, LoanRequestDto loanRequestDto)
			throws MessagingException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).sendOffer(userId, loanRequestDto);
	}

	@GET
	@Path("/{id}/pdf/{type}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto getBorrowerStatements(@PathParam(value = "id") int id,
			@PathParam(value = "type") String type) throws PdfGeenrationException, IOException {
		return loanServiceFactory.getService(PrimaryType.BORROWER.name()).getBorrowerStatements(id, type);

	}

	// @Scheduled(cron="0 0,20 0,19 ? * *")
	public void loanApplicationScheduler() {
		logger.info("Input :loanApplicationScheduler is starting");
		loanServiceFactory.getService(PrimaryType.BORROWER.name()).loanApplicationScheduler();
	}

	@POST
	@Path("/{userId}/loan/{primaryType}/newrequest")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto raiseNewLoanRequest(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, LoanRequestDto loanRequestDto) throws MessagingException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).newLoanRequest(userId, loanRequestDto);
	}

	@GET
	@Path("/{loanId}/borrowerDealStructure")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto borrowerDealStructure(@PathParam(value = "loanId") String loanId)
			throws PdfGeenrationException, IOException {
		return loanServiceFactory.getService(PrimaryType.BORROWER.name()).borrowerDealStructure(loanId);

	}

	@GET
	@Path("/{id}/{loanrequestid}/loanofferdetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OfferSentDetails getOfferSendDetails(@PathParam(value = "id") int id,
			@PathParam(value = "loanrequestid") int loanRequestId) {
		return loanServiceFactory.getService(PrimaryType.BORROWER.name()).getOfferSendDetails(id, loanRequestId);

	}

	@GET
	@Path("/{loanId}/lenderDealStructure")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto lenderDealStructure(@PathParam(value = "loanId") String loanId)
			throws PdfGeenrationException, IOException {
		return loanServiceFactory.getService(PrimaryType.LENDER.name()).lenderDealStructure(loanId);

	}

	@PATCH
	@Path("/{id}/creditScoreByPaisabazaar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse addingCreditScoreByPaisaBazaar(@PathParam(value = "id") int userId,
			ExperianRequestDto experianRequestDto) {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(PrimaryType.BORROWER.name()).addingCreditScoreByPaisaBazaar(userId,
				experianRequestDto);
	}

	@GET
	@Path("/getUtmFields")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse getAllUtmFieldValues() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getAllUtmFieldValues();
	}

	@PATCH
	@Path("/{oxyLoansTransactionNumber}/payuborrowerfee")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PayuMoneyPaymentDetaisResponce borrowerFeeUpdation(
			@PathParam("oxyLoansTransactionNumber") String oxyLoansTransactionNumber, LoanEmiCardResponseDto request) {
		logger.info("Input : " + oxyLoansTransactionNumber);
		return loanServiceFactory.getService(PrimaryType.BORROWER.name()).borrowerFeeUpdation(oxyLoansTransactionNumber,
				request);
	}

	@PATCH
	@Path("/{id}/paymentDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanEmiCardPaymentDetailsResponseDto updatePaymentDetails(@PathParam("id") int emiId,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).updatePaymentDetails(emiId,
				loanEmiCardPaymentDetailsRequestDto);
	}

	@GET
	@Path("/{id}/lenderEmiDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<LenderEmiDetailsResponseDto> getLenderEmiDetails(@PathParam("id") int userId) {
		return loanServiceFactory.getService(PrimaryType.LENDER.name()).getLenderEmiDetails(userId);
	}

	@Scheduled(cron = "0 0,30 0,15 ? * *")
	public void disbursedLoans() {
		logger.info("Input :loanApplicationScheduler is starting");
		loanServiceFactory.getService(PrimaryType.BORROWER.name()).disbursedLoans();
	}

	@GET
	@Path("/{id}/lenderHistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<LenderHistroryResponseDto> getLenderHistory(@PathParam("id") int userId) {
		return loanServiceFactory.getService(PrimaryType.LENDER.name()).getLenderHistory(userId);
	}

	@POST
	@Path("/lendersLoanInformation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TotalLendersInformationResponseDto getLendersLoansInformation(PageDto pageDto) {

		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getLendersLoansInformation(pageDto);

	}

	@POST
	@Path("/{lenderUserId}/individualLoans")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderIndividualLoansCountResposeDto getloanDetails(@PathParam("lenderUserId") int lenderUserId,
			PageDto pageDto) throws PdfGeenrationException, IOException {

		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getloanDetails(lenderUserId, pageDto);

	}

	@POST
	@Path("/{userId}/upd/updemisbyapplication")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanEmiCardPaymentDetailsResponseDto updEmisByApplication(@PathParam("userId") int userId,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto) {
		logger.info("updemisbyapplication!!!!!" + loanEmiCardPaymentDetailsRequestDto.getApplicationId());
		return loanServiceFactory.getService(PrimaryType.BORROWER.name())
				.updEmisByApplication(loanEmiCardPaymentDetailsRequestDto);
	}

	@GET
	@Path("/{userId}/loansInformation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UsersLoansInformationResponseDto getLenderLoansInformationPdf(@PathParam("userId") int userId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getLenderLoansInformationPdf(userId);
	}

	@POST
	@Path("/loan/featurepending/{year}/{month}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto featurePending(@PathParam("year") int year, @PathParam("month") int month,
			SearchRequestDto searchRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).featurePending(year, month, searchRequestDto);
	}

	@POST
	@Path("/{borrowerUserId}/individualLoansForBorrower")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerIndividualLoansCountResposeDto getloanDetailsForBorrower(
			@PathParam("borrowerUserId") int borrowerUserId, PageDto pageDto)
			throws PdfGeenrationException, IOException {

		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getloanDetailsForBorrower(borrowerUserId,
				pageDto);

	}

	@GET
	@Path("/{userId}/individualLoansForBorrower")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UsersLoansInformationResponseDto getBorrowerLoansInformationPdf(@PathParam("userId") int userId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getBorrowerLoansInformationPdf(userId);
	}

	@GET
	@Path("/{loanId}/manualEsignProcess")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto manualEsignProcess(@PathParam("loanId") String loanId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).manualEsignProcess(loanId);

	}

	@GET
	@Path("/loan/bucketpending/{start}/{end}/{type}/{pageno}/{pagesize}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<LoanResponseDto> bucketPendingEmis(@PathParam("start") int start, @PathParam("end") int end,
			@PathParam("type") String type, @PathParam("pageno") int pageNo, @PathParam("pagesize") int pageSize) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).bucketPendingEmis(start, end, type, pageNo,
				pageSize);
	}

	@GET
	@Path("/notifications/admin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NotificationCountResponseDto getAdminPendingActionsDetails() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getAdminPendingActionsDetails();
	}

	@POST
	@Path("/{id}/rejectloanoffer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto rejectLoanOffer(@PathParam("id") int id) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).rejectLoanOffer(id);

	}

	// @Scheduled(cron = "0 0 0 * * ?") // schedular penality
	public void calculatingPenalityUsingSchedular() {
		loanServiceFactory.getService(PrimaryType.ADMIN.name()).calculatingPenalityUsingSchedular();
	}

	@PATCH
	@Path("/{loanId}/loanLevelUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public int getUnpaidLoanEmiDetails(@PathParam("loanId") int loanId,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getUnpaidLoanEmiDetails(loanId,
				loanEmiCardPaymentDetailsRequestDto);
	}

	// @Scheduled(cron = "0 0 12 2 * ?")
	// @Scheduled(cron = "0 0 12 27 * ?")
	public LoanResponseDto sendingEmailsBeforeEmiDate() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).sendingEmailsBeforeEmiDate();

	}

	@GET
	@Path("/adminemidetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<AdminEmiDetailsResponseDto> getEmiDetails() {

		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getEmiDetails();

	}

	@GET
	@Path("/{id}/lenderHistoryPdf")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderTransactionHistoryResponseDto generateLenderHistoryPdf(@PathParam("id") int id)
			throws PdfGeenrationException, IOException {
		return loanServiceFactory.getService(PrimaryType.LENDER.name()).generateLenderHistoryPdf(id);
	}

	@GET
	@Path("/thirtydaysbucketdownload")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BucketPdfresponse thirtyDaysBucketDownload() throws PdfGeenrationException, IOException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).thirtyDaysBucketDownload();
	}

	@GET
	@Path("/sixtydaysbucketdownload")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BucketPdfresponse sixtyDaysBucketDownload() throws PdfGeenrationException, IOException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).sixtyDaysBucketDownload();
	}

	@GET
	@Path("/ninetydaysbucketdownload")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BucketPdfresponse ninetyDaysBucketDownload() throws PdfGeenrationException, IOException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).ninetyDaysBucketDownload();
	}

	@GET
	@Path("/loan/bucketpending/{start}/{end}/{userid}/{type}/{pageno}/{pagesize}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<LoanResponseDto> PendingEmisWithUserId(@PathParam("start") int start,
			@PathParam("end") int end, @PathParam("userid") int userId, @PathParam("type") String type,
			@PathParam("pageno") int pageNo, @PathParam("pagesize") int pageSize) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).PendingEmisWithUserId(start, end, type, userId,
				pageNo, pageSize);
	}

	@GET
	@Path("/loan/pending/{year}/{month}/{type}/{userid}/{pageno}/{pagesize}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<LoanResponseDto> emiPaidwithUserId(@PathParam("year") int year,
			@PathParam("month") int month, @PathParam("type") String type, @PathParam("userid") int userId,
			@PathParam("pageno") int pageNo, @PathParam("pagesize") int pageSize) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).emiPaidwithUserId(year, month, type, userId,
				pageNo, pageSize);
	}

	@Scheduled(cron = "0 0,30 0,15 ? * *")
	public int getAdminPendingActionsDetailsSchedular() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getAdminPendingActionsDetailsSchedular();
	}

	@GET
	@Path("/npabucketdownload")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BucketPdfresponse npaBucketDownload() throws PdfGeenrationException, IOException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).npaBucketDownload();
	}

	@GET
	@Path("/{userid}/lenderAndBorrowerInformation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UsersLoansInformationResponseDto searchForLenderAndBorrowerInfo(@PathParam("userid") int userId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).searchForLenderAndBorrowerInfo(userId);
	}

	@POST
	@Path("/borrowersLoanInformation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TotalLendersInformationResponseDto getBorrowersLoansInformation(PageDto pageDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getBorrowersLoansInformation(pageDto);
	}

	@GET
	@Path("/loanapplication/bucketpending/{start}/{end}/{type}/{pageno}/{pagesize}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<ApplicationResponseDto> bucketsByApplication(@PathParam("start") int start,
			@PathParam("end") int end, @PathParam("type") String type, @PathParam("pageno") int pageNo,
			@PathParam("pagesize") int pageSize) {

		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).bucketsByApplication(start, end, type, pageNo,
				pageSize);
	}

	@GET
	@Path("/loanapplication/bucketpendingbyuserid/{start}/{end}/{type}/{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<ApplicationResponseDto> bucketsByApplicationByUserId(@PathParam("start") int start,
			@PathParam("end") int end, @PathParam("type") String type, @PathParam("userid") int userId) {

		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).bucketsByApplicationByUserId(start, end, type,
				userId);
	}

	@GET
	@Path("/emi/bucketpending/{start}/{end}/{type}/{loanId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<BucketPendingEmis> bucketsEmiPending(@PathParam("start") int start,
			@PathParam("end") int end, @PathParam("type") String type, @PathParam("loanId") int loanId) {

		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).bucketsEmiPending(start, end, type, loanId);
	}

	@GET
	@Path("/loan/pending/application/{year}/{month}/{type}/{pageno}/{pagesize}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<LoanResponseDto> pendingEmisApplicationLevel(@PathParam("year") int year,
			@PathParam("month") int month, @PathParam("type") String type, @PathParam("pageno") int pageNo,
			@PathParam("pagesize") int pageSize) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).pendingEmisApplicationLevel(year, month, type,
				pageNo, pageSize);
	}

	@GET
	@Path("/borrowerParentRequestId/{borrowerParentRequestId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto SearchByParentRequestId(
			@PathParam("borrowerParentRequestId") String borrowerParentRequestId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).SearchByParentRequestId(borrowerParentRequestId);
	}

	@GET
	@Path("/borrowerId/{borrowerId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto SearchByBorrowerId(@PathParam("borrowerId") int borrowerId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).SearchByBorrowerId(borrowerId);
	}

	@GET
	@Path("/loan/pending/{year}/{month}/{type}/{loanId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<LoanResponseDto> pendingEmisBasedOnLoanLevel(@PathParam("year") int year,
			@PathParam("month") int month, @PathParam("type") String type, @PathParam("loanId") int loanId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).pendingEmisBasedOnLoanLevel(year, month, type,
				loanId);
	}

	@PATCH
	@Path("/{loanId}/generateloanemicard")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanEmiGenerationResDto getLoanEmiCard(@PathParam("loanId") int loanId,
			LoanEmiCardResponseDto loanEmiCardResponseDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getLoanEmiCard(loanId, loanEmiCardResponseDto);
	}

	@PATCH
	@Path("/{loanId}/{emiNumber}/penalityToZero")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String makingPenalityValueZero(@PathParam("loanId") int loanId, @PathParam("emiNumber") int emiNumber) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).makingPenalityValueZero(loanId, emiNumber);
	}

	@PATCH
	@Path("/{loanId}/{emiNumber}/statusUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updatingEmiPaidOnDate(@PathParam("loanId") int loanId, @PathParam("emiNumber") int emiNumber,
			LoanEmiCard loanEmiCard) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).updatingEmiPaidOnDate(loanId, emiNumber,
				loanEmiCard);
	}

	@PATCH
	@Path("/{loanId}/{emiNumber}/removingEmiPaidDate")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String deleteExtraPaidEmiDetails(@PathParam("loanId") int loanId, @PathParam("emiNumber") int emiNumber,
			LoanEmiCard loanEmiCard) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).deleteExtraPaidEmiDetails(loanId, emiNumber);
	}

	@GET
	@Path("/oxyloans")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Scheduled(cron = "0 0 0 * * ?")
	public String calculatingInterestAmount() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).calculatingInterestAmount();

	}

	@GET
	@Path("/{borrowerApplicationId}/applicationDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanEmiCardResponseDto calculatePendingPenaltyTillDate(
			@PathParam("borrowerApplicationId") int borrowerApplicationId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.calculatePendingPenaltyTillDate(borrowerApplicationId);

	}

	@PATCH
	@Path("/{loanId}/updateStatus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public int countValue(@PathParam("loanId") int loanId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).countValue(loanId);
	}

	@GET
	@Path("/{lenderParentRequestId}/lenderreport")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KycFileResponse lenderReport(@PathParam("lenderParentRequestId") Integer lenderParentRequestId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).lenderReport(lenderParentRequestId);
	}

	@GET
	@Path("/{borrowerApplicationId}/borrowerApplicationLevel")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanEmiCardResponseDto getBorrowerApplicationDetails(
			@PathParam("borrowerApplicationId") int borrowerApplicationId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.getBorrowerApplicationDetails(borrowerApplicationId);

	}

	@GET
	@Path("/{userid}/borrowerapplications")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<BorrowerApplicationSummryDto> borrowerApplictionSummary(@PathParam("userid") int userId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).borrowerApplictionSummary(userId);

	}

	@GET
	@Path("/{parentrequestid}/emidetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ApplicationResponseDto emiDetails(@PathParam("parentrequestid") int parentRequestID) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).emiDetails(parentRequestID);
	}

	// @Scheduled(cron = "0 0 12 6 * ?")
	// @Scheduled(cron = "0 0 12 11 * ?")
	// @Scheduled(cron = "0 0 12 21 * ?")
	public String sendingNotificationToLender() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).sendingNotificationToLender();
	}

	@GET
	@Path("/getexcel")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KycFileResponse getDataFromExcel() {
		return loanServiceFactory.getService(PrimaryType.BORROWER.name()).getDataFromExcel();
	}

	@GET
	@Path("/{loanId}/InterestAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanEmiCardResponseDto getInterestPendingTillDate(@PathParam("loanId") int loanId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getInterestPendingTillDate(loanId);
	}

	@PATCH
	@Path("/{loanId}/InterestWaiveOff")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanEmiCardResponseDto interestWaiveOff(@PathParam("loanId") int loanId,
			LoanEmiCardResponseDto loanEmiCardResponseDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).interestWaiveOff(loanId, loanEmiCardResponseDto);
	}

	@POST
	@Path("/{loanId}/updatingEnachForOldBorrowers")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String enachSchedulingToOldBorrowers(@PathParam("loanId") int loanId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).enachSchedulingToOldBorrowers(loanId);

	}

	@GET
	@Path("/{userId}/borrowerComments")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerDetailsRequestDto getBorrowerComments(@PathParam("userId") int userId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getBorrowerComments(userId);
	}

	@GET
	@Path("/updatingEnachSuccessReponceToEmiCard")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateingEnachBasedOnEnachResponce() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).updateingEnachBasedOnEnachResponce();
	}

	@POST
	@Path("/{id}/commentsByRadhaSir")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CommentsResponseDto uploadCommentsFromRadhaSir(@PathParam("id") int id,
			CommentsRequestDto commentsRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).uploadCommentsFromRadhaSir(id,
				commentsRequestDto);

	}

	@GET
	@Path("/getBorrowerPaymentList/{borrowerUniqueNumber}/{paymentStatus}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PaymentSearchByStatus getBorrowerPaymentList(@PathParam("borrowerUniqueNumber") String borrowerUniqueNumber,
			@PathParam("paymentStatus") String paymentStatus) {

		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getBorrowerPaymentList(borrowerUniqueNumber,
				paymentStatus);
	}

	@GET
	@Path("/{month}/borrowersFeeDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersFeeBasedOnDate getBorrowersFreeBasedOnGivenDate(@PathParam("month") int month) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getBorrowersFreeBasedOnGivenDate(month);
	}

	@GET
	@Path("/{applicationId}/loanLevelFreeForOldApplications")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String calculatingLoanLevelFeeForOldBorrower(@PathParam("applicationId") int applicationId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.calculatingLoanLevelFeeForOldBorrower(applicationId);
	}

	@GET
	@Path("/sendEnachScheduleEmailExcel")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	//@Scheduled(cron = "0 30 5 2 * ?")
	// @Scheduled(cron = "0 30 5 17 * ?")
	public EnachScheduledMailResponseDto sendEnachScheduleEmailExcel() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).sendEnachScheduleEmailExcel();
	}

	@GET
	@Path("/enachVerificationResponse")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Scheduled(cron = "0 50 9 6 * ?")
	// @Scheduled(cron = "0 50 9 21 * ?")
	public EnachTransactionResponseDto enachSuccessVerificationList() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).enachSuccessVerificationList();
	}

	@POST
	@Path("/{userId}/lender-income")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderNocResponseDto getLendersInformation(@PathParam("userId") int userId,
			LenderIncomeRequestDto lenderIncomeRequestDto) throws FileNotFoundException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getLendersInformation(userId,
				lenderIncomeRequestDto);
	}

	@GET
	@Path("/getEcsAndNonEcsPaymentHistory/{monthName}/{year}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PaymentHistoryResponseDto getEcsAndNonEcsPaymentHistory(@PathParam("monthName") String monthName,
			@PathParam("year") String year) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getEcsAndNonEcsPaymentHistory(monthName, year);
	}

	@GET
	@Path("/{date1}/{date2}/getLenderWalletDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderEscrowWalletDto getLenderScrowWalletDetails(@PathParam("date1") String date1,
			@PathParam("date2") String date2) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getLenderScrowWalletDetails(date1, date2);

	}

	@PATCH
	@Path("/{userId}/updatingDuration")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DurationChangeDto incresingDurationToExistingBorrower(@PathParam("userId") int userId) {
		return loanServiceFactory.getService(PrimaryType.LENDER.name()).incresingDurationToExistingBorrower(userId);

	}

	@POST
	@Path("/getAllLendersWalletInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ActiveLenderResponseDto getAllLendersWalletInfo(PaginationRequestDto paginationRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getAllLenderWalletInfo(paginationRequestDto);
	}

	@GET
	@Path("/{currentMonth}/{year}/lenderLoansInformation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderLoansInformation gettingLenderLoanInformation(@PathParam("currentMonth") Integer currentMonth,
			@PathParam("year") Integer year) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).gettingLenderLoanInformation(currentMonth, year);
	}

	@POST
	@Path("/{currentMonth}/{year}/lendersInfoOfLoans")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LendersLoanInfo gettingLendersLoanInfoBasedOnMonthAndYear(@PathParam("currentMonth") Integer currentMonth,
			@PathParam("year") Integer year, PaginationRequestDto paginationRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.gettingLendersLoanInfoBasedOnMonthAndYear(currentMonth, year, paginationRequestDto);
	}

	@GET
	@Path("/{lenderId}/getLendersLoanInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<LenderLoansInformation> getLendersDetailedLoanInfo(@PathParam("lenderId") Integer lenderId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getLendersDetailedLoanInfo(lenderId);
	}

	@POST
	@Path("/{lenderId}/getLenderInfoBasedOnId")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LendersLoanInfo getLenderInfoBasedOnId(@PathParam("lenderId") Integer lenderId,
			PaginationRequestDto paginationRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getLenderInfoBasedOnId(lenderId,
				paginationRequestDto);
	}

	@GET
	@Path("/{loanId}/rejectLoanByLoanId")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public StatusResponseDto rejectingLoanByLoanId(@PathParam("loanId") int loanId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).rejectingLoanByLoanId(loanId);
	}

	@GET
	@Path("/{userId}/getLenderProfit")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderNocResponseDto getLenderProfitValue(@PathParam("userId") int userId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getLenderProfitValue(userId);
	}

	@GET
	@Path("/{userId}/getLenderProfitPdf")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderNocResponseDto generateLenderProfitPdf(@PathParam("userId") int userId) throws FileNotFoundException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).generateLenderProfitPdf(userId);
	}

	@GET
	@Path("/{excelUploadedDate}/updateDisbursedLoans")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanDisbursmentResponse updateingDisbursedDateBasedOnExcelSheet(
			@PathParam("excelUploadedDate") String excelUploadedDate) throws PdfGeenrationException, IOException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.updateingDisbursedDateBasedOnExcelSheet(excelUploadedDate);
	}

	@POST
	@Path("/displayPendingLoansInformation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DisbursmentPendingResponseDto getDisbursmentPendingLoansDetails(PaginationRequestDto paginationRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.getDisbursmentPendingLoansDetails(paginationRequestDto);
	}

	@POST
	@Path("/displayingAllLenderReferenceDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceResponse displayingAllLenderReferenceDetails(PaginationRequestDto pageRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.displayingAllLenderReferenceDetailsInfo(pageRequestDto);
	}

	@POST
	@Path("{userId}/approvingAllLoansInApplicationLevel")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public LoanDisbursmentResponse approvingAllLoansInApplicationLevel(@PathParam("userId") int userId,
			UserRequest userRequest) throws PdfGeenrationException, IOException {

		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).approvingAllLoansInApplicationLevel(userId,
				userRequest);
	}

	@GET
	@Path("/{loanId}/searchByLoanIdAndDisbursementPending")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserInformationResponseDto searchByLoanIdDisbursmentPending(@PathParam("loanId") String loanId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).searchByLoanIdDisbursmentPending(loanId);
	}

	@GET
	@Path("/{userId}/searchByUserIdAndDisbursementPending")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DisbursmentPendingResponseDto searchByLenderIdAndBorrowerId(@PathParam("userId") Integer userId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).searchByLenderIdAndBorrowerId(userId);
	}

	@GET
	@Path("/sendingMailToNormalBorrowersFromAdmin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 30 6 * * ?")
	public String sendingMailsToNormalEmisPendingFromAdmin() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).sendingMailsToNormalEmisPendingFromAdmin();
	}

	@GET
	@Path("/sendingMailToCriticalBorrowers")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 30 6 * * ?")
	public String sendingMailsToCriticalEmisPending() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).sendingMailsToCriticalEmisPending();
	}

	@GET
	@Path("/sendingMailToNormalBorrowersFromCRM")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 30 6 * * ?")
	public String sendingMailsToNormalEmisPendingFromCRM() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).sendingMailsToNormalEmisPendingFromCRM();
	}

	@POST
	@Path("{userId}/settingDisbursmentDatesForAllLoans")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanDisbursmentResponse settingDisbursmentDatesForAllLoans(@PathParam("userId") int userId,
			UserRequest useerRequest) throws PdfGeenrationException, IOException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).settingDisbursmentDatesForAllLoans(userId,
				useerRequest);
	}

	@GET
	@Path("loansForBorrowerApplication/{borrowerId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerIndividualLoansCountResposeDto loansForBorrowerApplication(@PathParam("borrowerId") int borrowerId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).loansForBorrowerApplication(borrowerId);
	}

	@GET
	@Path("/getLoanOwners")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersLoanOwnerInformation getLoanOwners() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getLoanOwners();
	}

	@GET
	@Path("/{loanOwner}/getBorrowersLoanInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersLoanOwnerInformation getBorrowerDetailedLoanInfo(@PathParam("loanOwner") String loanOwner) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).gettingBorrowerDetailedLoanInfo(loanOwner);
	}

	@GET
	@Path("/{date}/{deditedType}/lenderOutGoingAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyTransactionDetailsFromExcelSheetsResponse readingLendersOutGoingAmount(@PathParam("date") String date,
			@PathParam("deditedType") String deditedType) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).readingLendersOutGoingAmount(date, deditedType);
	}

	@GET
	@Path("/{userId}/lenderDashboard")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReturnsResponseDto getLenderNewDashBoard(@PathParam("userId") Integer userId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getLenderNewDashBoard(userId);
	}

	@POST
	@Path("/uploadingExcelSheet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyTransactionDetailsFromExcelSheetsResponse updatingExcelSheet(
			OxyTransactionDetailsFromExcelSheetsRequest oxyTransactionDetailsFromExcelSheetsRequest) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.updatingExcelSheet(oxyTransactionDetailsFromExcelSheetsRequest);

	}

	@PATCH
	@Path("/{userId}/updateLoanOwnerName")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CommentsResponseDto updatingLoanOwnerToBorrower(@PathParam("userId") Integer userId,
			BorrowersLoanOwnerNames borrowersLoanOwnerNames) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).updatingLoanOwnerToBorrower(userId,
				borrowersLoanOwnerNames);

	}

	@GET
	@Path("/readingLenderReferenceInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	//@Scheduled(cron = "0 30 5 28 * ?")
	public EnachScheduledMailResponseDto readingLenderReferenceInfoToExcelSheet() throws ParseException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).readingLenderReferenceInfoToExcelSheet();
	}

	@GET
	@Path("/lenderReferenceToSheetNew")
	@Scheduled(cron = "0 30 12 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceResponse readingLenderReferenceToSheet() throws ParseException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).readingLenderReferenceToSheet();
	}

	@PATCH
	@Path("/readingCommentsOfRadhaSir")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceResponse readingCommentsOfRadhaSir(LenderReferenceRequestDto lenderReferenceRequestDto)
			throws ParseException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.readingCommentsOfRadhaSir(lenderReferenceRequestDto);
	}

	@PATCH
	@Path("/responseForReferenceBonus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceResponse readingResponseForReferenceBonus(LenderReferenceRequestDto lenderReferenceRequestDto)
			throws ParseException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.readingResponseForReferenceBonus(lenderReferenceRequestDto);
	}

	@GET
	@Path("/lenderReferralBonusDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceResponse gettingLenderReferralBonusDetails() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).gettingLenderReferralBonusDetails();

	}

	@PATCH
	@Path("/updateDealsInformation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersDealsResponseDto updateBorrowerDealsInformation(BorrowersDealsRequestDto borrowersDealsRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.updateBorrowerDealsInformation(borrowersDealsRequestDto);

	}

	@POST
	@Path("/listOfDealsInformationForNormalDeals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersDealsList getListOfDealsInformation(PaginationRequestDto pageRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getListOfDealsInformation(pageRequestDto);

	}

	@GET
	@Path("{dealId}/dealInformation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersDealsResponseDto getSingleDeal(@PathParam(value = "dealId") int dealId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getSingleDeal(dealId);

	}

	@GET
	@Path("{groupId}/groupOfLenders")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderMappedToGroupIdResponseDto getListOfLendersMappedToGroupId(@PathParam(value = "groupId") int groupId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getListOfLendersMappedToGroupId(groupId);

	}

	@POST
	@Path("/{userId}/listOfDealsInformationToLender")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LendersDealsResponseDto listOfDealsInformationDispayingToLender(@PathParam("userId") Integer userId,
			PaginationRequestDto pageRequestDto) {
		return loanServiceFactory.getService(PrimaryType.LENDER.name()).listOfDealsInformationDispayingToLender(userId,
				pageRequestDto);
	}

	@PATCH
	@Path("/updatingLenderDeal")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyLendersAcceptedDealsResponseDto updateLenderDealInformation(
			OxyLendersAcceptedDealsRequestDto oxyLendersAcceptedDealsRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.updateLenderDealInformation(oxyLendersAcceptedDealsRequestDto);
	}

	@GET
	@Path("/{userId}/{dealId}/singleDeal")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealInformationRoiToLender getSigleDealInformation(@PathParam(value = "userId") int userId,
			@PathParam(value = "dealId") int dealId) {
		return loanServiceFactory.getService(PrimaryType.LENDER.name()).getSigleDealInformation(userId, dealId);

	}

	@POST
	@Path("{userId}/listOfDealPaticipation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaticipatedDeal getLenderPaticipatedDealsInformation(@PathParam(value = "userId") int userId,
			PaginationRequestDto pageRequestDto) {
		return loanServiceFactory.getService(PrimaryType.LENDER.name()).getLenderPaticipatedDealsInformation(userId,
				pageRequestDto);

	}

	@GET
	@Path("{dealId}/listOfLenders")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaticipatedDeal getListOfLendersByDealId(@PathParam(value = "dealId") int dealId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getListOfLendersByDealId(dealId);

	}

	@POST
	@Path("/fetchLenderPauyUPaymentDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public PayuMoneyPaymentDetaisResponce fetchLenderPauyUPaymentDetails(
			PayuMoneyPaymentDetailsRequest payuMoneyPaymentDetailsRequest) {
		return payuMoneyService.fetchLenderPauyUPaymentDetails(payuMoneyPaymentDetailsRequest);
	}

	@PATCH
	@Path("/{transactionNumber}/lenderFeeUpdation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PayuMoneyPaymentDetaisResponce lenderFeeUpdation(@PathParam("transactionNumber") String transactionNumber,
			PayuMoneyPaymentDetailsRequest request) {
		logger.info("Input : " + transactionNumber);
		return loanServiceFactory.getService(PrimaryType.LENDER.name()).lenderFeeUpdation(transactionNumber, request);
	}

	@POST
	@Path("{dealId}/dealLevelAgreements")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelResponseDto dealIdBasedAgreementsGeneration(@PathParam(value = "dealId") int dealId,
			DealLevelRequestDto dealLevelRequestDto) throws PdfGeenrationException, IOException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).dealIdBasedAgreementsGeneration(dealId,
				dealLevelRequestDto);

	}

	@GET
	@Path("/listOfWhatappGroupNames/{type}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WhatappInformation getWhatappGroupsInformation(@PathParam("type") String type) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getWhatappGroupsInformation(type);

	}

	@POST
	@Path("/{id}/lenderWalletTransactions")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ScrowWalletResponse lenderWalletTransactionsBasedOnId(@PathParam("id") int id,
			PaginationRequestDto pageRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).gettingLenderSrowWalletTransactions(id,
				pageRequestDto);
	}

	@PATCH
	@Path("/{refereeId}/{referrerId}/editingLenderReferenceDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceDetails toEditTheLenderReferenceInformation(@PathParam("refereeId") Integer refereeId,
			@PathParam("referrerId") Integer referrerId, LenderReferenceRequestDto requestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).toEditTheLenderReferenceInformation(refereeId,
				referrerId, requestDto);

	}

	@GET
	@Path("/listOfOxyLendersGroup")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<OxyLendersGroup> gettingListOfOxyLendersGroups() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).gettingListOfOxyLendersGroups();
	}

	/*
	 * @POST
	 * 
	 * @Path("/{userId}/updateLenderToGroup")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON)
	 * 
	 * @Consumes(MediaType.APPLICATION_JSON) public CommentsResponseDto
	 * updatingLenderToLendersGroup(@PathParam("userId") Integer userId,
	 * OxyLendersGroups oxyLenderGroup) { return
	 * loanServiceFactory.getService(PrimaryType.ADMIN.name()).
	 * updatingLenderToLendersGroup(userId, oxyLenderGroup); }
	 */

	@POST
	@Path("/listOfLendersMappedToGroup")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderMappedToGroupIdResponseDto gettingListOfLendersMappedToGroupName(OxyLendersGroups groupName) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).gettingListOfLendersMappedToGroupName(groupName);
	}

	@POST
	@Path("/loan/{loanId}/loanPrecloseByPlatform")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanEmiCardResponseDto loanPrecloseByPlatform(@PathParam("loanId") int loanId) {
		logger.info("Input : " + loanId);
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).loanPreCloseByPlatForm(loanId);
	}

	@POST
	@Path("/{userId}/loan/{primaryType}/searchEnachMandate")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<LoanResponseDto> searchEnachMandate(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, @QueryParam("includeUser") boolean includeUser,
			SearchRequestDto searchRequestDto) throws MessagingException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).searchEnachMandate(userId, searchRequestDto);
	}

	@GET
	@Path("/{dealId}/participationAndDisbursmentPendingAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealDisbursmentInformation getDealParticipationAndDisbursmentPendingInfo(
			@PathParam("dealId") Integer dealId) {
		return dealLevelInformationRepo.getDealParticipationAndDisbursmentPendingInfo(dealId);
	}

	@PATCH
	@Path("/{dealId}/dealLevelDisbursment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelResponseDto dealLevelDisbursment(@PathParam("dealId") Integer dealId,
			DealLevelRequestDto dealLevelRequestDto) {
		return dealLevelInformationRepo.dealLevelDisbursment(dealId, dealLevelRequestDto);
	}

	@POST
	@Path("/readingWhatsAppPaymentScreenshots")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<PaymentUploadHistoryResponseDto> readingWhatsAppPaymentScreenshots(WhatsappMessagesDto request)
			throws IOException, ParseException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).readingWhatsAppPaymentScreenshots(request);
	}

	@GET
	@Path("/{userId}/{dealId}/dealLevelLoanEmiCard")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelLoanEmiCardInformation getDealBasedLoanEmiDetails(@PathParam("userId") Integer userId,
			@PathParam("dealId") Integer dealId) {
		return dealLevelInformationRepo.getDealBasedLoanEmiDetails(userId, dealId);
	}

	@POST
	@Path("/readingCommittmentAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 0/15 * * * ?")
	public ReadingCommitmentAmountDto readingCommittmentAmount(BorrowersDealsRequestDto borrowersDealsRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.readingCommittmentAmount(borrowersDealsRequestDto);
	}

	@POST
	@Path("/notificationsToLendersForPendingAction")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 0/5 * * * ?")
	public StatusResponseDto sendingNotificationsToLendersInDealPending(ListOfWhatappGroupNames dealName)
			throws IOException {
		return notificationsRepo.sendingNotificationsToLendersInDealPending(dealName);
	}

	@POST
	@Path("/checking-current-wallet-balance")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderMappedToGroupIdResponseDto gettingLenderCurrentWalletBalance(OxyLendersGroups oxyLendersGroups) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.gettingLenderCurrentWalletBalanceInfo(oxyLendersGroups);
	}

	@POST
	@Path("/currentWalletBalanceExcelSheet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderMappedToGroupIdResponseDto gettingLenderCurrentWalletBalanceExcelSheet(
			OxyLendersGroups oxyLendersGroups) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.gettingLenderCurrentWalletBalanceExcelSheet(oxyLendersGroups);
	}

	@POST
	@Path("/{userId}/listOfDealsInformationForEquityDeals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersDealsList getListOfDealsInformationForEquity(@PathParam("userId") int userId,
			PaginationRequestDto pageRequestDto) {
		return dealLevelInformationRepo.getListOfDealsInformationForEquity(userId, pageRequestDto);

	}

	@POST
	@Path("/sumOfDealsAmountByLendersGroup")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LendersDealsInformation getLendersGroupBasedDealAmounts(
			LenderGroupNamesRequestDto lenderGroupNamesRequestDto) {
		return dealLevelInformationRepo.getLendersGroupBasedDealAmounts(lenderGroupNamesRequestDto);
	}

	@GET
	@Path("/listOfDealNames")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WhatappInformation gettingListOfDealNames() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).gettingListOfDealsNames();
	}

	@POST
	@Path("/sendwhatsAppCampaignMessage")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WhatsappCampaignDto sendwhatsAppCampaignMessage(WhatsappCampaignDto whatsappCampaignDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).sendwhatsAppCampaignMessage(whatsappCampaignDto);
	}

	@GET
	@Path("/sendingReferenceInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public EnachScheduledMailResponseDto sendingMailToReferrer1() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).sendingMailToReferrerAboutReference1();
	}

	@GET
	@Path("/lendersInAllEquityDeals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public EquityLendersListsDto lendersInAllEquityDeals() {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).lendersInAllEquityDeals();
	}

	@GET
	@Path("/getBorrowersMsgContent/{type}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public StatusResponseDto getBorrowersMsgContent(@PathParam("type") String type) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getBorrowersMsgContent(type);
	}

	@POST
	@Path("/sendNotoficationsToBorrowerFromAdmin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public StatusResponseDto sendNotoficationsToBorrowerFromAdmin(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).sendNotoficationsToBorrowerFromAdmin(request);
	}

	@POST
	@Path("/getUnsentBorrowerNotifications")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public StatusResponseDto getUnsentBorrowerNotifications(SpreadSheetRequestDto request)
			throws GeneralSecurityException, IOException, InterruptedException {
		logger.info("unsent");
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getUnsentBorrowerNotifications(request);

	}

	@POST
	@Path("/writeNotificationsResponseMessages")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public StatusResponseDto writeNotificationsResponseMessages(SpreadSheetRequestDto request)
			throws GeneralSecurityException, IOException, InterruptedException, ParseException {
		logger.info("unsent");
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).writeNotificationsResponseMessages(request);

	}

	@GET
	@Path("/{groupName}/whatsAppGroupNames")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WhatappInformation gettingListOfWhatsAppGroupNamesBasedOnGroupName(
			@PathParam("groupName") String groupName) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.gettingListOfWhatsAppGroupNamesBasedOnGroupName(groupName);
	}

	@POST
	@Path("/{userId}/lenderClosedDeals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ClosedDealsInformation getLenderClosedDealsInfo(@PathParam("userId") Integer userId,
			PaginationRequestDto pageRequestDto) {
		return lenderLoanService.getLenderClosedDealsInfo(userId, pageRequestDto);

	}

	@GET
	@Path("{dealId}/listOfLendersForExcel")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaticipatedDeal getListOfLendersByDealIdForExcelSheet(@PathParam(value = "dealId") int dealId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).getListOfLendersByDealIdForExcelSheet(dealId);

	}

	@POST
	@Path("{dealId}/dealIdBasedAgreementsGenerationForZaggle")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelResponseDto dealIdBasedAgreementsGenerationForZaggle(@PathParam(value = "dealId") int dealId,
			DealLevelRequestDto dealLevelRequestDto) throws PdfGeenrationException, IOException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).dealIdBasedAgreementsGenerationForZaggle(dealId,
				dealLevelRequestDto);

	}

	@POST
	@Path("/{userId}/loan/{primaryType}/{loanRequestId}/uploadEsignedAgreementPdfforDeal")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto uploadEsignedAgreementPdfforDeal(@PathParam("userId") int userId,
			@PathParam("loanRequestId") Integer loanRequestId, @PathParam("primaryType") String primaryType,
			UploadAgreementRequestDto agreementFileRequest) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).uploadEsignedAgreementPdfforDeal(userId,
				loanRequestId, agreementFileRequest);
	}

	@PATCH
	@Path("/{userId}/loan/{primaryType}/updateLoanRequest")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto updateLoanRequestNewMethod(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, LoanRequestDto loanRequestDto) throws MessagingException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).updateLoanRequestNewMethod(userId,
				loanRequestDto);
	}

	@POST
	@Path("/currentWalletBalanceExcelSheetNew")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderMappedToGroupIdResponseDto gettingLenderCurrentWalletBalanceExcelSheetTwo(
			OxyLendersGroups oxyLendersGroups) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.gettingLenderCurrentWalletBalanceExcelSheetTwo(oxyLendersGroups);
	}

	@POST
	@Path("/{userId}/loan/{primaryType}/{applicationId}/esignForApplicationLevel")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public EsignInitResponse esignForApplicationLevel(@PathParam("userId") Integer userId,
			@PathParam("applicationId") Integer applicationId, @PathParam("primaryType") String primaryType,
			UploadAgreementRequestDto agreementFileRequest) {
		return loanServiceFactory.getService(primaryType.toUpperCase()).esignForApplicationLevel(userId, applicationId,
				agreementFileRequest);
	}

	@POST
	@Path("/{userId}/loan/{primaryType}/{applicationId}/uploadEsignedAgreementPdfforDealInApplicationLevel")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto uploadEsignedAgreementPdfforDealInApplicationLevel(@PathParam("userId") int userId,

			@PathParam("applicationId") Integer applicationId, @PathParam("primaryType") String primaryType,
			UploadAgreementRequestDto agreementFileRequest) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.uploadEsignedAgreementPdfforDealInApplicationLevel(userId, applicationId, agreementFileRequest);
	}

	@POST
	@Path("/{userId}/loan/{primaryType}/searchEnachMandateApplicationLevel")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<ApplicationLevelEnachResponseDto> searchEnachMandateApplicationLevel(
			@PathParam(value = "userId") int userId, @PathParam("primaryType") String primaryType)
			throws MessagingException {
		logger.info("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).searchEnachMandateApplicationLevel(userId);
	}

	@POST
	@Path("/{userId}/loan/{primaryType}/enachForApplication/{mandateId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ENachMandateResponse generateEnachMandateForApplicationLevel(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, @PathParam("mandateId") int mandateId) {
		return enachService.generateEnachMandateForApplicationLevel(userId, mandateId);
	}

	@POST
	@Path("/{userId}/loan/enachForApplicationLevel/response")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean EnachMandateReponseForApplicationLevel(@PathParam(value = "userId") int userId,
			EnachMandateReponseRequestDto enachMandateReponseRequestDto) {
		return enachService.updateEnachMandateReponseForApplicationLevel(userId, enachMandateReponseRequestDto);
	}

	@PATCH
	@Path("/{userId}/dealLevelDisbursmentForAppLevel")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelResponseDto dealLevelDisbursmentForAppLevel(@PathParam("userId") Integer userId,
			UserRequest userRequest) {
		return dealLevelInformationRepo.dealLevelDisbursmentForAppLevel(userId, userRequest);
	}

	@POST
	@Path("/{applicationId}/amountDistributionBasedOnDisbursment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updatingAmountBasedOnDisbursment(@PathParam("applicationId") Integer applicationId,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).updatingAmountBasedOnDisbursment(applicationId,
				loanEmiCardPaymentDetailsRequestDto);
	}

	@GET
	@Path("/{dealId}/notification-after-deal-creation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String sendingEmailNotifiaction(@PathParam("dealId") Integer dealId) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).sendingEmailNotifiaction(dealId);
	}

	@POST
	@Path("/{dealId}/autolending")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String autoPaticiaption(@PathParam("dealId") Integer dealId) {
		return loanServiceFactory.getService(PrimaryType.LENDER.name()).autoPaticiaption(dealId);
	}

	@POST
	@Path("/sending-loan-offer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelRequestDto automaticSendingOffer(BorrowersDealsRequestDto borrowersDealsRequestDto) {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).automaticSendingOffer(borrowersDealsRequestDto);

	}

	@PATCH
	@Path("/{transactionNumber}/lenderFeePayments")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PayuMoneyPaymentDetaisResponce lenderFeePaymentsUpdation(
			@PathParam("transactionNumber") String transactionNumber, PayuMoneyPaymentDetailsRequest request) {
		logger.info("Input : " + transactionNumber);
		return loanServiceFactory.getService(PrimaryType.LENDER.name()).lenderFeePayments(transactionNumber, request);
	}

	@GET
	@Path("/readingLenderReferenceInfo1")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 30 5 28 * ?")
	public EnachScheduledMailResponseDto readingLenderReferenceInfoToExcelSheet1() throws ParseException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).readingLenderReferenceInfoToExcelSheet1();
	}

	@GET
	@Path("/lenderReferenceToSheetNew1")
	// @Scheduled(cron = "0 30 12 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceResponse readingLenderReferenceToSheet1() throws ParseException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name()).readingLenderReferenceToSheet1();
	}

	@PATCH
	@Path("/readingCommentsOfRadhaSir1")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceResponse readingCommentsOfRadhaSir1(LenderReferenceRequestDto lenderReferenceRequestDto)
			throws ParseException {
		return loanServiceFactory.getService(PrimaryType.ADMIN.name())
				.readingCommentsOfRadhaSir1(lenderReferenceRequestDto);
	}

	@GET
	@Path("/{userId}/lendertotalInvestmentData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderInvestmentDataDtoResponse getLenderTotalInvestments(@PathParam(value = "userId") int userId) {
		return loanServiceFactory.getService(PrimaryType.LENDER.name()).lenderTotalInvestmentData(userId);

	}
	
	@GET
	@Path("/lendertotalInAutoLending")
	//@Scheduled(cron="0 */30 * * * *")
	//@Scheduled(cron="0 */5 * * * *")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerAutoLendingResponse getLendersInAutoLending() {
		return loanServiceFactory.getService(PrimaryType.LENDER.name()).getLendersUsingAutoLending();
		
	}
	
	
	
	
	
	
}
