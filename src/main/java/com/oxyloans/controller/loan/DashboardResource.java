package com.oxyloans.rest.loan;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.service.loan.LoanServiceFactory;
import com.oxyloans.service.loan.dto.OxyDashboardDetailsDto;

@Component
@Path("/v1/user")
public class DashboardResource {
	
	private static final Logger logger = LogManager.getLogger(DashboardResource.class);
	
	@Autowired
	private LoanServiceFactory loanServiceFactory;

	@PostConstruct
	public void print() {
		logger.info("dashboard Rest Resource");
	}

	@GET
	@Path("/{userId}/dashboard/{primaryType}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyDashboardDetailsDto userDashboardStats(@PathParam(value = "userId") int userId, @PathParam("primaryType") String primaryType, @QueryParam(value = "current") boolean current) throws MessagingException {
		System.out.println("Input : " + userId);
		return loanServiceFactory.getService(primaryType.toUpperCase()).getDashboard(userId, current);
	}
	
	
	@GET
	@Path("/{userId}/borrowerdashboard/{primaryType}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyDashboardDetailsDto borrowerDashBordDetails(@PathParam(value = "userId") int userId, @PathParam("primaryType") String primaryType) {
		return loanServiceFactory.getService(primaryType.toUpperCase()).borrowerDashBordDetails(userId);
		
	}
	

}