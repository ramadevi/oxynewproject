package com.oxyloans.rest.loan;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.borrower.loan.service.LoanService;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.customexceptions.UploadException;
import com.oxyloans.entity.borrowers.deals.dto.BorrowerPaymentsStatusDto;
import com.oxyloans.entity.borrowers.deals.dto.BorrowersTotalFdsResponse;
import com.oxyloans.loan.dto.BorrowerListOfPaymentsUploadedResponse;
import com.oxyloans.loan.dto.BorrowerLoanResponseDto;
import com.oxyloans.loan.dto.BorrowerNewBankDetailsRequestDto;
import com.oxyloans.loan.dto.BorrowerNewBankDetailsResponseDto;
import com.oxyloans.loan.dto.BorrowersMappedToDealRequestDto;
import com.oxyloans.loan.dto.BorrowersMappedToDealResponseDto;
import com.oxyloans.loan.dto.ClosedFdsWithRepaymentRequestDto;
import com.oxyloans.loan.dto.FdCalculationResponseDto;
import com.oxyloans.loan.dto.FdCloseingRequestDto;
import com.oxyloans.loan.dto.FdSearchDownloadRequest;
import com.oxyloans.loan.dto.FdSearchDownloadResponse;
import com.oxyloans.loan.dto.FdStatisticsRequestDto;
import com.oxyloans.loan.dto.FdStatisticsResponseDto;
import com.oxyloans.loan.dto.InvoiceRequestDto;
import com.oxyloans.loan.dto.InvoiceResponceDto;
import com.oxyloans.loan.dto.ListOfBorrowerNewBankDetailsResponse;
import com.oxyloans.loan.dto.ListOfPaymentsDetailsRequestDto;
import com.oxyloans.loan.dto.ListOfPaymentsDetailsResponseDto;
import com.oxyloans.loan.dto.MonthWiseBorrowersFdRequestDto;
import com.oxyloans.loan.dto.MonthWiseBorrowersFdResponseDto;
import com.oxyloans.loan.dto.PaymentUploadedRequestDto;
import com.oxyloans.loan.dto.PaymentUploadedResponseDto;
import com.oxyloans.loan.dto.SearchTypes;
import com.oxyloans.request.user.KycFileRequest;
import com.oxyloans.request.user.KycFileRequest.KycType;

@Component
@Path("v1/user")
public class BorrowerResource {

	@Autowired
	private LoanService loanService;

	@PATCH
	@Path("/borrower-account-details")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerNewBankDetailsResponseDto savingBorrowerNewBankDetails(
			BorrowerNewBankDetailsRequestDto borrowerNewBankDetailsRequestDto) {
		return loanService.savingBankDetails(borrowerNewBankDetailsRequestDto);

	}

	@GET
	@Path("/{userId}/new-account-details")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerNewBankDetailsResponseDto getNewBankDetailsOfuser(@PathParam("userId") int userId) {
		return loanService.getNewBankDetailsOfuser(userId);
	}

	@PATCH
	@Path("/update-fdcreated-date-amount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerNewBankDetailsResponseDto addingCreatedDateAndAmount(
			BorrowerNewBankDetailsRequestDto borrowerNewBankDetailsRequestDto) {
		return loanService.addingCreatedDateAndAmount(borrowerNewBankDetailsRequestDto);
	}

	@GET
	@Path("/{userId}/fee-calculation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public FdCalculationResponseDto fdFreeCalculation(@PathParam("userId") int UserId) {
		return loanService.fdFreeCalculation(UserId);
	}

	@PATCH
	@Path("/funds-transfer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersMappedToDealResponseDto fdTransferFromSystem(
			BorrowersMappedToDealRequestDto borrowersMappedToDealRequestDto) {
		return loanService.fdTransferFromSystem(borrowersMappedToDealRequestDto);
	}

	@POST
	@Path("/{userId}/payment-upload")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public BorrowerLoanResponseDto borrowerPayment(FormDataMultiPart multiPart, @PathParam("userId") Integer userId)
			throws MessagingException, FileNotFoundException, IOException {
		List<KycFileRequest> files = new ArrayList<KycFileRequest>();

		extracted(files, multiPart);

		if (files.isEmpty()) {
			throw new UploadException("No Files are uploaded", ErrorCodes.EMPTY_REQUEST);
		}

		return loanService.uploadsPaymentScreenshot(userId, files);

	}

	private void extracted(List<KycFileRequest> files, FormDataMultiPart multipart) {
		if (multipart != null) {
			for (BodyPart part : multipart.getBodyParts()) {
				InputStream is = part.getEntityAs(InputStream.class);
				ContentDisposition meta = part.getContentDisposition();
				KycFileRequest fileRequest = new KycFileRequest();
				fileRequest.setFileInputStream(is);
				fileRequest.setFileName(meta.getFileName());
				fileRequest.setKycType(
						KycType.valueOf(part.getContentDisposition().getParameters().get("name").toUpperCase()));
				files.add(fileRequest);
			}
		}
	}

	@POST
	@Path("/payment-upload")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PaymentUploadedResponseDto paymentUploaded(PaymentUploadedRequestDto paymentUploadedRequestDto) {
		return loanService.paymentUploaded(paymentUploadedRequestDto);

	}

	@PATCH
	@Path("/{paymentId}/{status}/payment-status-updation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PaymentUploadedResponseDto updatePaymentStatus(@PathParam("paymentId") int paymentId,
			@PathParam("status") String status) {
		return loanService.updatePaymentStatus(paymentId, status);

	}

	@POST
	@Path("/{pageNumber}/{pageSize}/{type}/bank-details")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ListOfBorrowerNewBankDetailsResponse bankDetailsBeforeFdCreatedDateAndAfter(
			@PathParam("pageNumber") int pageNumber, @PathParam("pageSize") int pageSize,
			@PathParam("type") String type) {
		return loanService.bankDetailsBeforeFdCreatedAndAfter(pageNumber, pageSize, type);

	}

	@POST
	@Path("/payment-details")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ListOfPaymentsDetailsResponseDto getPaymentsBasedonBankTypeAndStatus(
			ListOfPaymentsDetailsRequestDto listOfPaymentsDetailsRequestDto) {
		return loanService.getPaymentsBasedonBankTypeAndStatus(listOfPaymentsDetailsRequestDto);

	}

	@PATCH
	@Path("/fd-closing")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PaymentUploadedResponseDto closingFd(FdCloseingRequestDto fdCloseingRequestDto) {
		return loanService.closingFd(fdCloseingRequestDto);

	}

	@POST
	@Path("/fd-details-basedon-status")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ListOfBorrowerNewBankDetailsResponse fdStatusBasedSearch(
			ListOfPaymentsDetailsRequestDto listOfPaymentsDetailsRequestDto) {
		return loanService.fdStatusBasedSearch(listOfPaymentsDetailsRequestDto);

	}

	@GET
	@Path("/{userId}/payments")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<BorrowerListOfPaymentsUploadedResponse> getUserPayments(@PathParam("userId") int userId) {
		return loanService.getUserPayments(userId);
	}

	@GET
	@Path("/{userId}/fd-search")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerNewBankDetailsResponseDto searchByUserId(@PathParam("userId") int userId) {
		return loanService.searchByUserId(userId);
	}

	@POST
	@Path("/fd-statistics")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public FdStatisticsResponseDto fdStatistics(FdStatisticsRequestDto fdStatisticsRequestDto) {
		return loanService.fdStatistics(fdStatisticsRequestDto);
	}

	@POST
	@Path("/payments-invoice")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<InvoiceResponceDto> generatedInvoives(InvoiceRequestDto invoiceRequestDto) {
		return loanService.generatedInvoives(invoiceRequestDto);
	}

	@POST
	@Path("/fd-monthly-details")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MonthWiseBorrowersFdResponseDto monthlyFdInformation(
			MonthWiseBorrowersFdRequestDto monthWiseBorrowersFdRequestDto) {
		return loanService.monthlyFdInformation(monthWiseBorrowersFdRequestDto);

	}

	@POST
	@Path("/search-individual-types")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchTypes listUniqueSearchNames(FdSearchDownloadRequest fdSearchDownloadRequest) {
		return loanService.listUniqueSearchNames(fdSearchDownloadRequest);
	}

	@POST
	@Path("/download-fd-details")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public FdSearchDownloadResponse searchByType(FdSearchDownloadRequest fdSearchDownloadRequest) {
		return loanService.searchByType(fdSearchDownloadRequest);

	}

	@POST
	@Path("/active_loan_excelsheet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String borrowerLoanActiveExcelSheet() {
		return loanService.excelSheetForActiveBorrower();

	}

	@POST
	@Path("/fd-closed-details")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MonthWiseBorrowersFdResponseDto getClosedFdsWithRepaymentDetails(
			ClosedFdsWithRepaymentRequestDto closedFdsWithRepaymentRequestDto) {
		return loanService.getClosedFdsWithRepaymentDetails(closedFdsWithRepaymentRequestDto);

	}

	@POST
	@Path("/fds-executed-from-system")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerPaymentsStatusDto getTotalCreatedFdsDetails(
			ListOfPaymentsDetailsRequestDto listOfPaymentsDetailsRequestDto) {
		return loanService.getTotalCreatedFdsDetails(listOfPaymentsDetailsRequestDto);

	}

	@PATCH
	@Path("/fd-status-updated")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersTotalFdsResponse updateFdStatus(FdCloseingRequestDto fdCloseingRequestDto) {
		return loanService.updateFdRepaymentStatus(fdCloseingRequestDto);

	}
}
