package com.oxyloans.rest.loan;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.cashfree.AadhaarVerificationResponseDto;
import com.oxyloans.cashfree.CashFreeOrdePayRequest;
import com.oxyloans.cashfree.CashFreeOrderCreationsRequest;
import com.oxyloans.cashfree.CashFreeOrderDetailsReponseDto;
import com.oxyloans.cashfree.CashFreeOrderDetailsReponseDtos;
import com.oxyloans.cashfree.CashFreeOrderPayResponseDtos;
import com.oxyloans.cashfree.CashFreeQrResponseDto;
import com.oxyloans.cashfree.CashfreePaymentStatusDto;
import com.oxyloans.cashfree.CashfreeVerificationRequestDto;
import com.oxyloans.cashfree.CashfreeVerificationResponseDto;
import com.oxyloans.cashfree.PanVerificationResponseDto;
import com.oxyloans.cashfree.payment.CashfreeServiceRepo;
import com.oxyloans.request.user.UserRequest;

@Component
@Path("v1/user")
public class CashFreeResource {

	@Autowired
	private CashfreeServiceRepo cashFreeServiceRepo;

	private final Logger logger = LogManager.getLogger(CashFreeResource.class);

	/*
	 * @GET
	 * 
	 * @Path("/generateCashfreeToken")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON)
	 * 
	 * @Consumes(MediaType.APPLICATION_JSON) public String generateCashfreeToken() {
	 * return cashFreeServiceRepo.generateCashfreeToken(); }
	 */

	@POST
	@Path("/verifyBankAccountAndIfsc")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CashfreeVerificationResponseDto verifyBankAccountAndIfsc(CashfreeVerificationRequestDto request) {
		return cashFreeServiceRepo.verifyBankAccountAndIfsc(request);
	}

	@GET
	@Path("/verifyBankUpi")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CashfreeVerificationResponseDto verifyBankUpi(@QueryParam("vpa") String upi) {

		return cashFreeServiceRepo.verifyBankUpi(upi);
	}

	@GET
	@Path("/verifyPan")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PanVerificationResponseDto verifyPan(@QueryParam("name") String name, @QueryParam("pan") String pan) {
		return cashFreeServiceRepo.verifyPan(name, pan); // name is not mandatory
	}

	@GET
	@Path("/verifyAadhaar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AadhaarVerificationResponseDto verifyAadhaar(@QueryParam("aadhaarNumber") String aadhaarNumber) {
		return cashFreeServiceRepo.verifyAadhaar(aadhaarNumber);
	}

	@POST
	@Path("/createVA")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CashfreeVerificationResponseDto createVirtualAccount(UserRequest userRequest) {

		return cashFreeServiceRepo.createVirtualAccount(userRequest);
	}

	@POST
	@Path("/createVAUPI")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CashfreeVerificationResponseDto createVirtualAccountUpi(UserRequest userRequest,
			@QueryParam("upi") String upi) {

		return cashFreeServiceRepo.createVirtualAccountUpi(userRequest, upi);
	}

	@GET
	@Path("/createQrCodeCashFree")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CashFreeQrResponseDto createQrCodeCashFree(@QueryParam("upi") String upi) {

		return cashFreeServiceRepo.createQrCodeCashFree(upi);
	}

	@GET
	@Path("/createDynamicQrCodeCashFree")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CashFreeQrResponseDto createDynamicQrCodeCashFree(@QueryParam("upi") String upi,
			@QueryParam("amount") Double amount) {
		return cashFreeServiceRepo.createDynamicQrCodeCashFree(upi, amount);
	}

	
	  @POST
	  @Path("/cashfreeForOrder")
	  @Produces(MediaType.APPLICATION_JSON)
	 @Consumes(MediaType.APPLICATION_JSON)
	  public CashFreeOrderDetailsReponseDtos lenderFeeUsingCashFreeOrder(CashFreeOrderCreationsRequest cashfreeRequestDto)
	  {
		  return  cashFreeServiceRepo.userFeeUsingCashFreeForMobile(cashfreeRequestDto); 
		  }
	  
	  @POST
	  @Path("/cashfreeOrderPay")
	  @Produces(MediaType.APPLICATION_JSON)
	  @Consumes(MediaType.APPLICATION_JSON)
	  
public CashFreeOrderPayResponseDtos getOrderPay(CashFreeOrdePayRequest request) {
	  
	 return cashFreeServiceRepo.cashfreeOrderPay(request);
	  }
	  
	  @POST
	  @Path("/{order_id}/getOrderbyId")
	  @Produces(MediaType.APPLICATION_JSON)
	  @Consumes(MediaType.APPLICATION_JSON)
	  public CashfreePaymentStatusDto getOrderById(@PathParam("order_id") String order_id) { 
		  
		  return cashFreeServiceRepo.getCashfeePaymentDetails(order_id);
	  
	  }
	 
	  @POST
		@Path("/cashfreeOrderForReact")
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
	public CashFreeOrderDetailsReponseDto  lenderReactFeeUsingCashFreeOrder(
			CashFreeOrderCreationsRequest cashfreeRequestDto) {
					return cashFreeServiceRepo.lenderReactFeeUsingCashFreeOrder( cashfreeRequestDto);
		}
		
		
		@GET
		@Path("/{order_id}/getOrderbyIdForReact")
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		public CashFreeOrderDetailsReponseDto getOrderByOrderId(@PathParam("order_id") String order_id) {
			return cashFreeServiceRepo.getCashfeePaymentDetailsStatus(order_id);
			
		}
}
