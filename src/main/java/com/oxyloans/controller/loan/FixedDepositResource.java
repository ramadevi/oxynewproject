package com.oxyloans.rest.loan;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.fd.CIBRegistrationRequestDto;
import com.oxyloans.fd.FDCreationRequestDto;
import com.oxyloans.fd.FDFetchRequestDto;
import com.oxyloans.fd.FDLiquidationRequestDto;
import com.oxyloans.fd.service.FixedDepositImpl;
import com.oxyloans.fd.service.FixedDepositService;

@Component
@Path("/v1/user")
public class FixedDepositResource {

	@Autowired
	private FixedDepositService fixedDepositImpl;

	@POST
	@Path("/cib-registration")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String cibRegistration(CIBRegistrationRequestDto cibRegistrationRequestDto) {
		return fixedDepositImpl.cibRegistration(cibRegistrationRequestDto);
	}

	@POST
	@Path("/fd-status")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String cibRegistrationStatusCheck(CIBRegistrationRequestDto cibRegistrationRequestDto) {
		return fixedDepositImpl.cibRegistrationStatusCheck(cibRegistrationRequestDto);
	}

	@POST
	@Path("/{userId}/fd-creation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String fixedDeposit(@PathParam("userId") Integer userId, FDCreationRequestDto fdCreationRequestDto) {
		return fixedDepositImpl.fixedDeposit(userId, fdCreationRequestDto);
	}

	@POST
	@Path("/fd-fetch")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String fdFetch(FDFetchRequestDto fdFetchRequestDto) {
		return fixedDepositImpl.fdFetch(fdFetchRequestDto);
	}

	@POST
	@Path("/fd-liquidation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String fdLiquidation(FDLiquidationRequestDto fdLiquidationRequestDto) {
		return fixedDepositImpl.fdLiquidation(fdLiquidationRequestDto);
	}
}
