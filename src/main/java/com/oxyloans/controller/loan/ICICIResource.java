package com.oxyloans.rest.loan;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.icici.upi.CallBackRequestDto;
import com.oxyloans.icici.upi.QRSuccessTransactionsDetails;
import com.oxyloans.icici.upi.QRTransactionCallBackDto;
import com.oxyloans.icici.upi.QRTransactionDetailsRequestDto;
import com.oxyloans.icici.upi.TransactionResponseDto;
import com.oxyloans.icici.upi.TransactionStatusCheckRequestDto;
import com.oxyloans.icici.upi.TransactionStatusCheckResponseDto;
import com.oxyloans.icici.upi.UnifiedPaymentsInterfaceRequestDto;
import com.oxyloans.icici.upi.UnifiedPaymentsInterfaceService;
import com.oxyloans.icici.upi.UnifiedPaymentsInterfaceServiceRepo;
import com.oxyloans.response.user.PaginationRequestDto;

@Component
@Path("v1/user")
public class ICICIResource {

	@Autowired
	private UnifiedPaymentsInterfaceServiceRepo unifiedPaymentsInterfaceService;

	@POST
	@Path("/QRTransactionInitiation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransactionResponseDto quickResponseTransactionDetails(
			QRTransactionDetailsRequestDto transactionDetailsRequestDto) {
		return unifiedPaymentsInterfaceService.quickResponseTransactionDetails(transactionDetailsRequestDto);
	}

	@POST
	@Path("/upicredit")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.TEXT_PLAIN)
	public String callResponseForQr(String encryptedCallBack) {
		return unifiedPaymentsInterfaceService.callResponseForQr(encryptedCallBack);
	}

	@POST
	@Path("/transactionStatus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransactionStatusCheckResponseDto transactionStatusCheck(
			TransactionStatusCheckRequestDto transactionStatusCheckRequestDto) {
		return unifiedPaymentsInterfaceService.transactionStatusCheck(transactionStatusCheckRequestDto);
	}

	@PATCH
	@Path("/{id}/qrStatusCheck")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransactionResponseDto qrStatusBasedOnId(@PathParam(value = "id") Integer id) {
		return unifiedPaymentsInterfaceService.qrStatusBasedOnId(id);
	}

	@POST
	@Path("/{userId}/successTransactions")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public QRSuccessTransactionsDetails getSuccessTransactionsFromQR(@PathParam("userId") Integer userId,
			PaginationRequestDto pageRequestDto) {
		return unifiedPaymentsInterfaceService.getSuccessTransactionsFromQR(userId, pageRequestDto);
	}
}
