package com.oxyloans.rest.loan;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.disbursment.BorrowerDisbusmentResponseDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.service.loan.BorrowerDisbursmentService;
import com.oxyloans.service.loan.BorrowerDisbursmentServiceRepo;

@Component
@Path("/v1/user")
public class LoanDisbursmentResource {

	@Autowired
	private BorrowerDisbursmentServiceRepo borrowerDisbursmentService;

	@POST
	@Path("/applicationLevelFundsTransfer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerDisbusmentResponseDto applicationLevelFundsTransfer(UserRequest request) {
		return borrowerDisbursmentService.applicationLevelFundsTransfer(request);
	}

}
