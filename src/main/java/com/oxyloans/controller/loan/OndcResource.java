package com.oxyloans.rest.loan;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyAgreement;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.ondc.SellerMessageRequestDto;
import com.oxyloans.ondc.SellerMessageResponseDto;
import com.oxyloans.ondc.SellerService;
import com.oxyloans.ondc.SubscriberUrlRequestDto;
import com.oxyloans.ondc.SubscriberUrlResponseDto;

@Component
@Path("/")
public class OndcResource {

	private final Logger logger = LogManager.getLogger(AdminLoanResource.class);

	@Autowired
	private SellerService sellerService;

	@GET
	@Path("ondc-site-verification.html")
	public String getHtmlPage() {

		return "<html>\n" + "    <head>\n"
				+ "        <meta name='ondc-site-verification' content='ZzzfZAYhrp0xTbdIJF/zMKmMMQrCI+lNH5Hzhgo02o1WJNKHxqT2kcC/rrvjzfNPkoyJCMmmR2lrguy7qVxUAg==' />\n"
				+ "    </head>\n" + "    <body>\n" + "        ONDC Site Verification Page\n" + "    </body>\n"
				+ "</html>";

	}

	@POST
	@Path("subscriber_url/on_subscribe")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SubscriberUrlResponseDto callBackForOndc(SubscriberUrlRequestDto subscriberUrlRequestDto) {
		SubscriberUrlResponseDto subscriberUrlResponseDto = new SubscriberUrlResponseDto();
		try {
			String privateKeyString = "MFECAQEwBQYDK2VuBCIEIPhaAP8oBvay2ssASIZHjKioI2CyLmQzXpbZrvx/U4NsgSEAGK2u0PE3tsEhhmhdwXwE3BZJZC9/TNlmyZ4On8Ifk3I=";
			String publicKeyString = "MCowBQYDK2VuAyEAduMuZgmtpjdCuxv+Nc49K0cB6tL/Dj3HZetvVN7ZekM=";
			String challengeString = subscriberUrlRequestDto.getChallenge();

			byte[] privateKeyBytes = Base64.getDecoder().decode(privateKeyString);
			byte[] publicKeyBytes = Base64.getDecoder().decode(publicKeyString);
			byte[] challengeBytes = Base64.getDecoder().decode(challengeString); // Decode challengeString directly

			byte[] decryptedBytes = encryptDecrypt(Cipher.DECRYPT_MODE, challengeBytes, privateKeyBytes,
					publicKeyBytes);

			// Assuming decryptedBytes represent a string, convert it to a string
			String decryptedString = new String(decryptedBytes);
			logger.info("decryption of ondc" + decryptedString);
			subscriberUrlResponseDto.setAnswer(decryptedString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return subscriberUrlResponseDto;
	}

	public byte[] encryptDecrypt(int mode, byte[] data, byte[] privateKey, byte[] publicKey)
			throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException, InvalidKeyException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

		if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
			Security.addProvider(new BouncyCastleProvider());
		}

		KeyAgreement keyAgreement = KeyAgreement.getInstance("X25519", BouncyCastleProvider.PROVIDER_NAME);
		X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(publicKey);
		PublicKey publickey = KeyFactory.getInstance("X25519", BouncyCastleProvider.PROVIDER_NAME)
				.generatePublic(x509EncodedKeySpec);

		PrivateKey privatekey = KeyFactory.getInstance("X25519", BouncyCastleProvider.PROVIDER_NAME)
				.generatePrivate(new PKCS8EncodedKeySpec(privateKey));

		keyAgreement.init(privatekey);
		keyAgreement.doPhase(publickey, true);
		byte[] secret = keyAgreement.generateSecret();
		SecretKey originalKey = new SecretKeySpec(secret, 0, secret.length, "AES");

		Cipher cipher = Cipher.getInstance("AES", BouncyCastleProvider.PROVIDER_NAME);
		cipher.init(mode, originalKey);

		return cipher.doFinal(data);
	}

	@POST
	@Path("/sellerAppl")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SellerMessageResponseDto buyerSearchForSeller(SellerMessageRequestDto sellerMessageRequestDto) {
		return sellerService.buyerSearchForSeller(sellerMessageRequestDto);

	}
}
