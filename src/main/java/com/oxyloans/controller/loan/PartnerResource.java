package com.oxyloans.rest.loan;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.engine.template.PdfGeenrationException;
import com.oxyloans.entity.borrowers.deals.dto.DealLevelResponseDto;
import com.oxyloans.partner.dto.LoanRequestStatusUpdationRequestDto;
import com.oxyloans.partner.dto.LoanResponseStatusUpdationRequestDto;
import com.oxyloans.partner.dto.PartnerRegistrationResponseDto;
import com.oxyloans.partner.service.PartnerService;
import com.oxyloans.partner.service.PartnerServiceRepo;
import com.oxyloans.service.loan.dto.LoanRequestDto;
import com.oxyloans.service.loan.dto.LoanResponseDto;

@Component
@Path("/v1/user")
public class PartnerResource {

	@Autowired
	private PartnerServiceRepo partnerService;

	@PATCH
	@Path("/{userId}/loan/{primaryType}/updateLoanRequestByPartner")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseDto updateLoanRequestByPartner(@PathParam(value = "userId") int userId,
			@PathParam("primaryType") String primaryType, LoanRequestDto loanRequestDto) throws MessagingException {
		return partnerService.updateLoanRequestByPartner(userId, loanRequestDto);
	}

	@PATCH
	@Path("/{userId}/approveLoanRequestByPartner")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseStatusUpdationRequestDto partnerApprovingLoanRequest(@PathParam(value = "userId") int userId,
			LoanRequestStatusUpdationRequestDto loanRequestStatusUpdationRequestDto) {
		return partnerService.partnerApprovingLoanRequest(userId, loanRequestStatusUpdationRequestDto);
	}

	@POST
	@Path("/{borrowerId}/partnerAgreements")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelResponseDto partnerBasedAgreementsGeneration(@PathParam(value = "borrowerId") Integer borrowerId)
			throws PdfGeenrationException, IOException {
		return partnerService.partnerBasedAgreementsGeneration(borrowerId);

	}

	@POST
	@Path("/{borrowerId}/fundsTransfer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PartnerRegistrationResponseDto partnerFundsTransfer(@PathParam(value = "borrowerId") Integer borrowerId) {
		return partnerService.partnerFundsTransfer(borrowerId);
	}

	@GET
	@Path("/{partnerUtm}/totalLendersWallet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanResponseStatusUpdationRequestDto lendersWalletAmount(
			@PathParam(value = "partnerUtm") String partnerUtm) {
		return partnerService.lendersWalletAmount(partnerUtm);
	}

}
