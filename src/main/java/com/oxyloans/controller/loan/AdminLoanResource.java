package com.oxyloans.rest.loan;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.oxyloans.autoinvest.dto.LenderParticipationUsingAutoLendingResponse;
import com.oxyloans.borrower.noc.BorrowerDealNotificationRequestfordeals;
import com.oxyloans.borrower.noc.BorrowerNocRequestDto;
import com.oxyloans.borrower.noc.BorrowerNocResponseDto;
import com.oxyloans.cashfree.CashfreeRequestDto;
import com.oxyloans.cashfree.CashfreeResponseDto;
import com.oxyloans.cashfree.payment.CashfreeServiceRepo;
import com.oxyloans.cms.dto.AfterApprovalFilesDto;
import com.oxyloans.cms.dto.BeforeApprovalFilesDto;
import com.oxyloans.cms.dto.CmsPaymentsStatisticsDto;
import com.oxyloans.cms.dto.IntiatedFileDto;
import com.oxyloans.cms.dto.NotificationsDto;
import com.oxyloans.cms.dto.PaymentDetailDto;
import com.oxyloans.cms.dto.RejectedFilesRequest;
import com.oxyloans.cms.dto.TotalDealLevelInterestsDto;
import com.oxyloans.dashboard.LenderInterestBasedOnDeals;
import com.oxyloans.dashboard.NewLenderDashboardRequestDto;
import com.oxyloans.dashboard.NewLenderDashboardResponseDto;
import com.oxyloans.deallevel.disbursment.DealLevelInformationRepo;
import com.oxyloans.emi.dto.BorrowerLoanRequestDto;
import com.oxyloans.emi.dto.BorrowerLoanResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.BorrowerDealNotificationRequest;
import com.oxyloans.entity.borrowers.deals.dto.BorrowerDealNotificationResponse;
import com.oxyloans.entity.borrowers.deals.dto.BorrowerFdDataDtoResponse;
import com.oxyloans.entity.borrowers.deals.dto.BorrowerOfferAcceptedStatusResponse;
import com.oxyloans.entity.borrowers.deals.dto.BorrowerPaymentsResponse;
import com.oxyloans.entity.borrowers.deals.dto.BorrowersDealsList;
import com.oxyloans.entity.borrowers.deals.dto.BorrowersDealsRequestDto;
import com.oxyloans.entity.borrowers.deals.dto.BorrowersDealsResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.ChatBotRequest;
import com.oxyloans.entity.borrowers.deals.dto.ChatBotResponse;
import com.oxyloans.entity.borrowers.deals.dto.ClosedDealsInformation;
import com.oxyloans.entity.borrowers.deals.dto.DealInformationResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.DealInformationRoiToLender;
import com.oxyloans.entity.borrowers.deals.dto.DealLevelLoanEmiCardInformation;
import com.oxyloans.entity.borrowers.deals.dto.DealLevelRequestDto;
import com.oxyloans.entity.borrowers.deals.dto.DealLevelResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.DealLoanActiveDateCalculationRequest;
import com.oxyloans.entity.borrowers.deals.dto.DealLoanActiveDateCalculationResponse;
import com.oxyloans.entity.borrowers.deals.dto.DealQuarterlyReportsRequest;
import com.oxyloans.entity.borrowers.deals.dto.DealQuarterlyReportsResponse;
import com.oxyloans.entity.borrowers.deals.dto.DealTenureUpdationResponse;
import com.oxyloans.entity.borrowers.deals.dto.DealsResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.FdAmountResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.GoogleSheetClosedDealsResponse;
import com.oxyloans.entity.borrowers.deals.dto.GoogleSheetResponse;
import com.oxyloans.entity.borrowers.deals.dto.GoogleSheetUserPersonalResponse;
import com.oxyloans.entity.borrowers.deals.dto.InterestReadRequest;
import com.oxyloans.entity.borrowers.deals.dto.LenderClosedDealsInformation;
import com.oxyloans.entity.borrowers.deals.dto.LenderDealsStatisticsInformation;
import com.oxyloans.entity.borrowers.deals.dto.LenderFurtherPaymentDetails;
import com.oxyloans.entity.borrowers.deals.dto.LenderInterestRequestDto;
import com.oxyloans.entity.borrowers.deals.dto.LenderInterestResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.LenderPaidAmountRequestDto;
import com.oxyloans.entity.borrowers.deals.dto.LenderPaidAmountResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.LenderParticipationUpdatedInfo;
import com.oxyloans.entity.borrowers.deals.dto.LenderPaticipatedDeal;
import com.oxyloans.entity.borrowers.deals.dto.LenderPaticipatedResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.LendersRunningAmountInfomation;
import com.oxyloans.entity.borrowers.deals.dto.LendersWalletRequestDto;
import com.oxyloans.entity.borrowers.deals.dto.LendersWalletResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.ListOfDealsInformationToLender;
import com.oxyloans.entity.borrowers.deals.dto.ListOfLenderInterestDetails;
import com.oxyloans.entity.borrowers.deals.dto.NewLenderExcelSheetGenerateResponse;
import com.oxyloans.entity.borrowers.deals.dto.NotificationGrapicalImageResponse;
import com.oxyloans.entity.borrowers.deals.dto.OxyLendersAcceptedDealsRequestDto;
import com.oxyloans.entity.borrowers.deals.dto.OxyLendersAcceptedDealsResponseDto;
import com.oxyloans.entity.borrowers.deals.dto.PendingInterestsDto;
import com.oxyloans.entity.borrowers.deals.dto.PerDealUsersCountResponse;
import com.oxyloans.entity.borrowers.deals.dto.PrincipalReturningStatusBasedData;
import com.oxyloans.entity.borrowers.deals.dto.PrincipalReturningStatusRequestDto;
import com.oxyloans.entity.borrowers.deals.dto.RemainderForInterestPaying;
import com.oxyloans.entity.borrowers.deals.dto.RunningDealInformation;
import com.oxyloans.entity.borrowers.deals.dto.TenureExtendsResponse;
import com.oxyloans.entity.borrowers.deals.dto.UserAmountGoogleSheetResponse;
import com.oxyloans.entity.borrowers.deals.dto.UserPanNumberRequest;
import com.oxyloans.entity.borrowers.deals.dto.UserPanNumberResponse;
import com.oxyloans.entity.borrowers.deals.dto.WebHookRequest;
import com.oxyloans.entity.borrowers.deals.dto.WebHookResponse;
import com.oxyloans.entity.expertSeekers.AdviseSeekersResponseDto;
import com.oxyloans.entity.expertSeekers.ExpertiseRequestDto;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.equity.deals.info.EquityDealsResponse;
import com.oxyloans.file.FileResponse;
import com.oxyloans.icici.dto.BeforeApprovalFiles;
import com.oxyloans.icici.dto.IciciExcelsheetApprovalRequestDto;
import com.oxyloans.icici.dto.LenderInterestPaymentsDto;
import com.oxyloans.icici.upi.TransactionDetailsFromQrCode;
import com.oxyloans.lender.participationToWallet.LenderPaticipationResponseDto;
import com.oxyloans.lender.participationToWallet.ListOfLendersMovingToWallet;
import com.oxyloans.lender.wallet.DealLevelInterestsDto;
import com.oxyloans.lender.wallet.LenderFeeDetailsDto;
import com.oxyloans.lender.wallet.LenderFeeDetailsHistoryDto;
import com.oxyloans.lender.wallet.LenderFeeDetailsResponse;
import com.oxyloans.lender.wallet.LenderFeeFilesResponse;
import com.oxyloans.lender.wallet.LenderFeeInWalletDto;
import com.oxyloans.lender.wallet.LenderFeeTransactions;
import com.oxyloans.lender.wallet.LenderMemberShipDetails;
import com.oxyloans.lender.wallet.PrincpleReturnsResponseDto;
import com.oxyloans.lender.wallet.ScrowLenderTransactionResponse;
import com.oxyloans.lender.wallet.UpdateLenderFeeData;
import com.oxyloans.lender.wallet.WalletRequestDto;
import com.oxyloans.lender.wallet.transfer.dto.ActionOnRequestedTransaction;
import com.oxyloans.lender.wallet.transfer.dto.ListOfWalletToWalletTransactionsResponse;
import com.oxyloans.lender.wallet.transfer.dto.WalletTransferLenderToLenderRequestDto;
import com.oxyloans.lender.wallet.transfer.dto.WalletTransferLenderToLenderResponseDto;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsFromDealsRequestDto;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsFromDealsResponseDto;
import com.oxyloans.paymentstats.dto.PaymentsStatsResponseDto;
import com.oxyloans.paytm.ListOfPaytmTransferedUsersDto;
import com.oxyloans.poolingaccount.dto.PoolingAccountRequestDto;
import com.oxyloans.poolingaccount.dto.PoolingAccountResponseDto;
import com.oxyloans.request.CampaignRequestDto;
import com.oxyloans.request.LenderReferralRequestDto;
import com.oxyloans.request.ReferralRequestDto;
import com.oxyloans.request.StudentRequestDto;
import com.oxyloans.request.TransactionAlertsDto;
import com.oxyloans.request.user.KycFileRequest;
import com.oxyloans.request.user.KycFileRequest.KycType;
import com.oxyloans.request.user.KycFileResponse;
import com.oxyloans.request.user.MemberShipValidityRequest;
import com.oxyloans.request.user.MonthlyValidityResponse;
import com.oxyloans.request.user.SpreadSheetRequestDto;
import com.oxyloans.request.user.TopFiftyLendersListRequsetDto;
import com.oxyloans.request.user.UserQueryDetailsRequestDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.admin.RBIStatsResponse;
import com.oxyloans.response.user.ActiveAmountResponse;
import com.oxyloans.response.user.ActiveLendersCountResponse;
import com.oxyloans.response.user.BankTransactionsDto;
import com.oxyloans.response.user.BorrowerReferenceResponse;
import com.oxyloans.response.user.CommentsResponseDto;
import com.oxyloans.response.user.DownLoadLinkResponse;
import com.oxyloans.response.user.EquityInvestorsExcelDto;
import com.oxyloans.response.user.FinancialYearResponseDto;
import com.oxyloans.response.user.HighestReferralBonusResponse;
import com.oxyloans.response.user.IciciExcelInterestsFiles;
import com.oxyloans.response.user.LenderBonusAmountStatus;
import com.oxyloans.response.user.LenderReferenceAmountResponseDto;
import com.oxyloans.response.user.LenderRenewalResponse;
import com.oxyloans.response.user.MaximumAmountInfo;
import com.oxyloans.response.user.OxyLendersGroups;
import com.oxyloans.response.user.PageRequestDto;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.response.user.ReadingCommitmentAmountDto;
import com.oxyloans.response.user.ReferralBonusBreakUpRequestDto;
import com.oxyloans.response.user.ReferralBonusBreakUpResponseDto;
import com.oxyloans.response.user.ReferralResponse;
import com.oxyloans.response.user.StatusResponseDto;
import com.oxyloans.response.user.TopFiftyLendersListDto;
import com.oxyloans.response.user.UniqueLendersResponseDto;
import com.oxyloans.response.user.UserPendingQuereisResponseDto;
import com.oxyloans.response.user.UserQueryDetailsResponse;
import com.oxyloans.response.user.UserQueryDetailsResponseDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.service.loan.NewAdminLoanServiceRepo;
import com.oxyloans.service.loan.dto.AdminDashboardDetails;
import com.oxyloans.service.loan.dto.DealLevelInterestPaymentsDto;
import com.oxyloans.service.loan.dto.InterestsApprovalDto;
import com.oxyloans.stats.dto.InterestAmountResponse;
import com.oxyloans.stats.dto.LenderDetailsResponseDto;
import com.oxyloans.stats.dto.StatsRequestDto;
import com.oxyloans.user.type.dto.ListOfBorrowersBasedOnType;
import com.oxyloans.user.type.dto.OxyUserTypeRequestDto;
import com.oxyloans.user.type.dto.OxyUserTypeResponseDto;
import com.oxyloans.wallettowallet.transfer.service.WalletToWalletTransferServiceRepo;
import com.oxyloans.webhooks.dto.WebhookChangesDto;
import com.oxyloans.whatapp.service.WhatappServiceRepo;
import com.oxyloans.whatsapp.dto.MessagesResponseDto;
import com.oxyloans.whatsapp.dto.StatusCheckDto;

@Component
@EnableScheduling
@Path("v1/user")
public class AdminLoanResource {

	@Autowired
	private NewAdminLoanServiceRepo newAdminLoanServiceRepo;

	@Autowired
	private DealLevelInformationRepo dealLevelInformationRepo;

	private final Logger logger = LogManager.getLogger(AdminLoanResource.class);

	@Autowired
	private CashfreeServiceRepo cashfreeService;

	@Autowired
	private WhatappServiceRepo whatappService;

	@Autowired
	private WalletToWalletTransferServiceRepo walletToWalletTransferServiceRepo;

	@POST
	@Path("/getPaytmDetails/{month}/{year}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ListOfPaytmTransferedUsersDto getListOfPaytmBasedOnMonthAndYear(@PathParam("month") String month,
			@PathParam("year") String year, PaginationRequestDto pageRequestDto) {
		return newAdminLoanServiceRepo.getListOfPaytmBasedOnMonthAndYear(month, year, pageRequestDto);

	}

	@POST
	@Path("/getCurrentWalletBalance")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LendersWalletResponseDto getCurrentWalletBalance(LendersWalletRequestDto lendersWalletRequestDto) {
		return newAdminLoanServiceRepo.getCurrentWalletBalance(lendersWalletRequestDto);

	}

	@GET
	@Path("/{dealId}/{month}/{year}/interestdetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ListOfLenderInterestDetails getListOfLendersInterestDetails(@PathParam("dealId") int dealId,
			@PathParam("month") String month, @PathParam("year") String year) {
		return newAdminLoanServiceRepo.getListOfLendersInterestDetails(dealId, month, year);

	}

	@POST
	@Path("/{dealId}/updateInterestAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderInterestResponseDto updateLenderInterestAmount(@PathParam("dealId") int dealId,
			LenderInterestRequestDto lenderInterestRequestDto) {
		return newAdminLoanServiceRepo.updateLenderInterestAmount(dealId, lenderInterestRequestDto);

	}

	@POST
	@Path("/googleSheetWhatsappCampaign")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public StatusResponseDto googleSheetWhatsappCampaign(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException {
		return newAdminLoanServiceRepo.googleSheetWhatsappCampaign(request);

	}

	@POST
	@Path("/listOfUserQueryDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserQueryDetailsResponse gettingListOfUserQueryDetails(PaginationRequestDto pageRequestDto) {
		return newAdminLoanServiceRepo.gettingListOfUserQueryDetails(pageRequestDto);
	}

	@PATCH
	@Path("/resolvingUserQuery")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public StatusResponseDto resolvingUSerQueryDetails(UserQueryDetailsRequestDto userQueryDetailsRequestDto) {
		return newAdminLoanServiceRepo.resolvingUSerQueryDetails(userQueryDetailsRequestDto);
	}

	@GET
	@Path("/{userId}/gettingMobileAndEmail")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse gettingMobileAndEmailOfUser(@PathParam("userId") Integer userId) {
		return newAdminLoanServiceRepo.gettingMobileAndEmailOfUser(userId);
	}

	@PATCH
	@Path("/dealLevelClosing")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelResponseDto dealLevelClosing(DealLevelRequestDto dealLevelRequestDto) {
		return newAdminLoanServiceRepo.dealLevelClosing(dealLevelRequestDto);
	}

	@GET
	@Path("/equityInvestorsExcelDownload/{dealId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public EquityInvestorsExcelDto getEquityInvestorsExcel(@PathParam("dealId") int dealId) {
		return newAdminLoanServiceRepo.getEquityInvestorsExcel(dealId);
	}

	@GET
	@Path("/listOfBorrowerDeals/{type}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public BorrowersDealsList getAllBorrowerDeals(@PathParam("type") String type) {
		return newAdminLoanServiceRepo.getAllBorrowerDeals(type);
	}

	@GET
	@Path("/{userId}/{dealId}/paticipationChanges")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderParticipationUpdatedInfo getLenderParticipationUpdatedDetails(@PathParam("userId") Integer userId,
			@PathParam("dealId") Integer dealId) {
		return newAdminLoanServiceRepo.getLenderParticipationUpdatedDetails(userId, dealId);
	}

	@POST
	@Path("/sendWhatsappAndEmailCampaign")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public StatusResponseDto whatsappAndEmailCampaign(SpreadSheetRequestDto request)
			throws GeneralSecurityException, IOException, InterruptedException, ParseException {

		return newAdminLoanServiceRepo.whatsappAndEmailCampaign(request);

	}

	@POST
	@Path("/listOfUniqueLenders")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public AdviseSeekersResponseDto getListOfUniqueLenders(PaginationRequestDto request) {
		return newAdminLoanServiceRepo.getListOfUniqueLenders(request);
	}

	@POST
	@Path("/seekingSuggestionFromLenders")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public StatusResponseDto seekingSuggestionFromLenders(ExpertiseRequestDto request) {
		return newAdminLoanServiceRepo.seekingSuggestionFromLenders(request);
	}

	@GET
	@Path("/uniqueLendersSearchCall/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public UniqueLendersResponseDto getUniqueLenderDetails(@PathParam("userId") String userId) {
		return newAdminLoanServiceRepo.getUniqueLenderDetails(userId);
	}

	@POST
	@Path("/sendMobileOtpForAdviseSeekers")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public UserResponse sendMobileOtpForAdviseSeekers(UserRequest userRequest) {
		return newAdminLoanServiceRepo.sendMobileOtpForAdviseSeekers(userRequest);
	}

	@POST
	@Path("/sendEmailOtp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public UserResponse sendEmailOtp(UserRequest userRequest) throws MessagingException {
		return newAdminLoanServiceRepo.sendEmailOtp(userRequest);
	}

	@PATCH
	@Path("/updateMobileNumberAndEmail")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public StatusResponseDto updateMobileNumberAndEmail(UserRequest request) {
		return newAdminLoanServiceRepo.updateMobileNumberAndEmail(request);
	}

	@GET
	@Path("/{dealType}/allRunningDeals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RunningDealInformation getRunningDeals(@PathParam("dealType") String dealType) {
		return newAdminLoanServiceRepo.getAllRunningDeals(dealType);

	}

	@GET
	@Path("/{userId}/dealsStatistics")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderDealsStatisticsInformation getTotalDealsStatistics(@PathParam("userId") Integer userId) {
		return newAdminLoanServiceRepo.getTotalDealsStatistics(userId);
	}

	@PATCH
	@Path("/{dealId}/dealPaticipationStatusUpdation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelResponseDto updatingDealParticipationStatus(@PathParam("dealId") Integer dealId,
			DealLevelRequestDto dealLevelRequestDto) {
		return newAdminLoanServiceRepo.updatingDealParticipationStatus(dealId, dealLevelRequestDto);
	}

	@GET
	@Path("/remainderForPayingInterest")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Scheduled(cron = "0 30 3 * * ?")
	public List<RemainderForInterestPaying> sendingRemainderForPayingInterestToLenders()
			throws FileNotFoundException, IOException {
		return newAdminLoanServiceRepo.sendingRemainderForPayingInterestToLenders();
	}

	@GET
	@Path("/validityDetailsOfMembershipToExcel")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderRenewalResponse validityDetailsOfMembershipToGroup() {
		return newAdminLoanServiceRepo.validityDetailsOfMembershipToGroup();
	}

	@POST
	@Path("/closedDealsInformation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ClosedDealsInformation getClosedDealInformation(PaginationRequestDto pageRequestDto) {
		return newAdminLoanServiceRepo.getClosedDealInformation(pageRequestDto);
	}

	@POST
	@Path("/{userId}/allLenderReferenceDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceAmountResponseDto gettingLenderReferenceInfoBasedOnId(@PathParam("userId") Integer userId,
			LenderReferralRequestDto lenderReferralRequestDto) {
		return newAdminLoanServiceRepo.gettingLenderReferenceInfoBasedOnId(userId, lenderReferralRequestDto);
	}

	@POST
	@Path("/newLenderDashboard")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NewLenderDashboardResponseDto lenderNewDashboard(NewLenderDashboardRequestDto newLenderDashboardRequestDto) {
		return newAdminLoanServiceRepo.lenderNewDashboard(newLenderDashboardRequestDto);
	}

	@PATCH
	@Path("/paticipationForOldDeals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyLendersAcceptedDealsResponseDto insertingPaticipationForOldDeals(
			OxyLendersAcceptedDealsRequestDto oxyLendersAcceptedDealsRequestDto) {
		return newAdminLoanServiceRepo.insertingPaticipationForOldDeals(oxyLendersAcceptedDealsRequestDto);
	}

	@POST
	@Path("/excelsForNewLenderDashboard")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NewLenderDashboardResponseDto excelsForNewLenderDashboard(
			NewLenderDashboardRequestDto newLenderDashboardRequestDto) {
		return newAdminLoanServiceRepo.excelsForNewLenderDashboard(newLenderDashboardRequestDto);
	}

	@POST
	@Path("/{userId}/totalLenderParticipatedDeals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaticipatedDeal getLenderPaticipatedAllDealsInfo(@PathParam("userId") Integer userId,
			PaginationRequestDto pageRequestDto) {
		return newAdminLoanServiceRepo.getLenderPaticipatedAllDealsInfo(userId, pageRequestDto);
	}

	@GET
	@Path("/{userId}/referralBonusAmountLink")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderBonusAmountStatus gettingReferralBonusAmountStatus(@PathParam("userId") Integer userId) {
		return newAdminLoanServiceRepo.gettingReferralBonusAmountStatus(userId);
	}

	@GET
	@Path("/{userId}/referralStatusInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderBonusAmountStatus gettingReferralStatusInfo(@PathParam("userId") Integer userId) {
		return newAdminLoanServiceRepo.gettingReferralStatusInfo(userId);
	}

	@POST
	@Path("/queryDetailsBasedOnPrimaryType")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserQueryDetailsResponse gettingListOfUserQueryDetailsBasedOnPrimaryType(
			UserQueryDetailsRequestDto userQueryDetailsRequestDto) {
		return newAdminLoanServiceRepo.gettingListOfUserQueryDetailsBasedOnPrimaryType(userQueryDetailsRequestDto);
	}

	@POST
	@Path("/countOfQueriesBasedOnStatus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserQueryDetailsResponse gettingCountOfQueriesBasedOnStatus(
			UserQueryDetailsRequestDto userQueryDetailsRequestDto) {
		return newAdminLoanServiceRepo.gettingCountOfQueriesBasedOnStatus(userQueryDetailsRequestDto);
	}

	@GET
	@Path("/currentDateDeals/{monthName}/{year}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<DealLevelInterestPaymentsDto> totalDealsCurrentDate(@PathParam("monthName") String monthName,
			@PathParam("year") String year, @QueryParam("status") String status, @QueryParam("startDate") int startDate,
			@QueryParam("endDate") int endDate) {
		return newAdminLoanServiceRepo.currentDateDealsInfo(monthName, year, status, startDate, endDate);
	}

	@POST
	@Path("/dealsInterestAmountTotal")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<TotalDealLevelInterestsDto> dealLevelInterestsNew(DealLevelInterestsDto dealLevelInterestsDto) {
		return newAdminLoanServiceRepo.newdealLevelInterestPayments(dealLevelInterestsDto);
	}

	@GET
	@Path("/dealLevelInterestPaymentsInfo/{monthName}/{year}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<TotalDealLevelInterestsDto> dealLevelInterestPaymentsInfo(@PathParam("monthName") String monthName,
			@PathParam("year") String year, @QueryParam("status") String status, @QueryParam("startDate") int startDate,
			@QueryParam("endDate") int endDate) {
		return newAdminLoanServiceRepo.dealLevelInterestPaymentsInfo(monthName, year, status, startDate, endDate);
	}

	@POST
	@Path("/dealinformation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersDealsResponseDto dealDataByName(DealLevelRequestDto dealLevelRequestDto) {
		return newAdminLoanServiceRepo.getAllDealDataByName(dealLevelRequestDto);
	}

	@POST
	@Path("/interestDetailsForDeal")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelInterestPaymentsDto interestDetailsForDeal(IciciExcelsheetApprovalRequestDto request) {
		return newAdminLoanServiceRepo.interestDetailsForDeal(request);
	}

	@POST
	@Path("/interestsApprovals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelInterestPaymentsDto approvingInterestsForLenders(IciciExcelsheetApprovalRequestDto request) {
		return newAdminLoanServiceRepo.approvingInterestsForLenders(request);
	}

	@PATCH
	@Path("/updateInterestsInfoExcelSheet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelInterestPaymentsDto updateInterestsInfoExcelSheet(IciciExcelsheetApprovalRequestDto request) {
		return newAdminLoanServiceRepo.updateInterestsInfoExcelSheet(request);
	}

	@POST
	@Path("/interestsInfoBreakUp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<InterestsApprovalDto> interestsInfoBreakUp(IciciExcelsheetApprovalRequestDto request) {
		return newAdminLoanServiceRepo.interestsInfoBreakUp(request);
	}

	@POST
	@Path("/referralBonusAmountBasedOnStatus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceAmountResponseDto gettingPaidAndUnpaidBonusAmount(
			LenderBonusAmountStatus lenderBonusAmountStatus) {
		return newAdminLoanServiceRepo.gettingPaidAndUnpaidBonusAmount(lenderBonusAmountStatus);
	}

	@POST
	@Path("/downLoadLinkForBonusAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceAmountResponseDto gettingDownloadLinkForreferralAmountBasedOnStatus(
			LenderBonusAmountStatus lenderBonusAmountStatus) {
		return newAdminLoanServiceRepo.gettingDownloadLinkForreferralAmountBasedOnStatus(lenderBonusAmountStatus);
	}

	@POST
	@Path("/queryDetailsBasedOnUserId")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserQueryDetailsResponse gettingListOfUserQueryDetailsBasedOnId(
			UserQueryDetailsRequestDto userQueryDetailsRequestDto) {
		return newAdminLoanServiceRepo.gettingListOfUserQueryDetailsBasedOnId(userQueryDetailsRequestDto);
	}

	@GET
	@Path("/{id}/pendingQueriesBasedOnId")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<UserPendingQuereisResponseDto> gettingListOfUserPendingQueriesBasedOnId(@PathParam("id") Integer id) {
		return newAdminLoanServiceRepo.gettingListOfUserPendingQueriesBasedOnId(id);
	}

	@GET
	@Path("/{userId}/{dealId}/loanStatementBasedOnCurrentValue")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelLoanEmiCardInformation loanEmiCardBasedOnCurrentValue(@PathParam("userId") Integer userId,
			@PathParam("dealId") Integer dealId) {
		return dealLevelInformationRepo.loanEmiCardBasedOnCurrentValue(userId, dealId);
	}

	@POST
	@Path("/getLenderIndividualReturns")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderInterestBasedOnDeals getLenderReturnsInfo(NewLenderDashboardRequestDto newLenderDashboardRequestDto) {
		return newAdminLoanServiceRepo.getLenderReturnsInfo(newLenderDashboardRequestDto);
	}

	@POST
	@Path("/updationFromICICIFiles") // reading reports with a post call
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<IciciExcelInterestsFiles> updationFromICICIFiles(IciciExcelsheetApprovalRequestDto request) {
		return newAdminLoanServiceRepo.updatingFromICICIResponseFiles(request);
	}

	@GET
	@Path("/updationFromICICIFiles")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Scheduled(cron = "0 30 5 * * ?")
	@Scheduled(cron = "0 0 9 * * ?")
	public List<IciciExcelInterestsFiles> updationFromICICIFiles() {

		return newAdminLoanServiceRepo.updatingFromICICIResponseFiles();
	}

	@GET
	@Path("/updating-icici-files")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Scheduled(cron = "0 30 14 * * ?")
	@Scheduled(cron = "0 0  17 * * ?")
	public List<IciciExcelInterestsFiles> updatingICICIFilesOnCurrentDate() {
		return newAdminLoanServiceRepo.updatingICICIFilesOnCurrentDate();
	}

	@POST
	@Path("/getBankTransactions")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BankTransactionsDto getBankTransactions(TransactionAlertsDto request) {
		return newAdminLoanServiceRepo.getBankTransactions(request);
	}

	@PATCH
	@Path("/updatingTypesOfBorrowersAndInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyUserTypeResponseDto typesOfBorrowersAndDetails(OxyUserTypeRequestDto OxyUserTypeRequestDto) {
		return newAdminLoanServiceRepo.typesOfBorrowersAndDetails(OxyUserTypeRequestDto);

	}

	@POST
	@Path("/borrowersBasedOnType")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ListOfBorrowersBasedOnType getListOfUsersBasedOnType(PaginationRequestDto pageRequestDto) {
		return newAdminLoanServiceRepo.getListOfUsersBasedOnType(pageRequestDto);
	}

	@GET
	@Path("/{userTypeId}/getSingleUserType")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyUserTypeResponseDto getSingleUserType(@PathParam("userTypeId") Integer userTypeId) {
		return newAdminLoanServiceRepo.getSingleUserType(userTypeId);
	}

	@PATCH
	@Path("/{userId}/uploadingDocDriveLink")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse uploadingCompanyDocDriveLink(@PathParam("userId") Integer userId, UserRequest request) {
		return newAdminLoanServiceRepo.uploadingCompanyDocDriveLink(userId, request);
	}

	@GET
	@Path("/highestReferralBonus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<HighestReferralBonusResponse> gettingHighestReferralBonus() {
		return newAdminLoanServiceRepo.gettingHighestReferralBonus();
	}

	@GET
	@Path("/sendingMessageAboutValidity")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 30 12 * * TUE")
	public List<LenderRenewalResponse> sendingValidityDetailsOfMembershipToLender() {
		return newAdminLoanServiceRepo.sendingValidityDetailsOfMembershipToLender();
	}

	@GET
	@Path("/gettingStatsBasedOnDates")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RBIStatsResponse gettingStatsBasedOnDates(LendersWalletRequestDto requestDto) {
		return newAdminLoanServiceRepo.gettingStatsBasedOnDates(requestDto);
	}

	@GET
	@Path("/gettingStatsBasedOnDatesToExcelSheet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RBIStatsResponse gettingStatsBasedOnDatesToExcelSheet(LendersWalletRequestDto requestDto) {
		return newAdminLoanServiceRepo.gettingStatsBasedOnDatesToExcelSheet(requestDto);
	}

	@GET
	@Path("/{userId}/borrowerEligibilityDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerReferenceResponse borrowerEligibilityDetails(@PathParam("userId") Integer userId) {
		return newAdminLoanServiceRepo.borrowerEligibilityDetails(userId);
	}

	@GET
	@Path("/utmBasedExcelDownload/{primaryType}/{utm}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public StatusResponseDto utmBasedExcelDownload(@PathParam("primaryType") String primaryType,
			@PathParam("utm") String utm) {
		return newAdminLoanServiceRepo.utmBasedExcelDownload(primaryType, utm);
	}

	@POST
	@Path("/borrowerAndLenderEmailCampign")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public StatusResponseDto emailCampaignToLenderAndBorrower(CampaignRequestDto request) {
		return newAdminLoanServiceRepo.emailCampaignToLenderAndBorrower(request);
	}

	@POST
	@Path("/moveFileToApprovalFolder")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public StatusResponseDto moveFileToApprovalFolder(IciciExcelsheetApprovalRequestDto request) {
		return newAdminLoanServiceRepo.moveFileToApprovalFolder(request);
	}

	@POST
	@Path("/listOfFilesInBeforeApproval")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<BeforeApprovalFilesDto> getListOfFilesFromBeforeApproval(IciciExcelsheetApprovalRequestDto request) {
		return newAdminLoanServiceRepo.getListOfFilesFromBeforeApproval(request);
	}

	@POST
	@Path("/{dealId}/movingPrincipalToWallet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaticipationResponseDto movingPrincipalToWallet(@PathParam("dealId") Integer dealId,
			ListOfLendersMovingToWallet lenderPaticipationRequest) {
		return newAdminLoanServiceRepo.movingPrincipalToWallet(dealId, lenderPaticipationRequest);
	}

	@POST
	@Path("/poolingAccountDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PoolingAccountResponseDto getPoolingAccountDetails(PoolingAccountRequestDto poolingAccountRequestDto) {
		return newAdminLoanServiceRepo.getPoolingAccountDetails(poolingAccountRequestDto);
	}

	@POST
	@Path("/readingParticipationAmounts")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 0/15 * * * ?")
	public ReadingCommitmentAmountDto readingCommittmentAmount(BorrowersDealsRequestDto borrowersDealsRequestDto) {
		return newAdminLoanServiceRepo.readingParticipationAmounts(borrowersDealsRequestDto);
	}

	@POST
	@Path("/qrSuccessTransactionDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransactionDetailsFromQrCode qrTransactionDetails(PaginationRequestDto pageRequestDto) {
		return newAdminLoanServiceRepo.qrTransactionDetails(pageRequestDto);
	}

	@GET
	@Path("/getCurrentDayBankTransactions")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BankTransactionsDto getCurrentDayBankTransactions() {
		return newAdminLoanServiceRepo.getCurrentDayBankTransactions();

	}

	@POST
	@Path("/{userId}/cashfree")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CashfreeResponseDto lenderFeeUsingCashFree(@PathParam("userId") Integer userId,
			CashfreeRequestDto cashfreeRequestDto) {
		return cashfreeService.lenderFeeUsingCashFree(userId, cashfreeRequestDto);
	}

	@POST
	@Path("/lendersCurrentPrincipalAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LendersRunningAmountInfomation LendersRunningAmountInfomation(PaginationRequestDto pageRequestDto) {
		return newAdminLoanServiceRepo.LendersRunningAmountInfomation(pageRequestDto);
	}

	@POST
	@Path("/borrowerEmiDetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerLoanResponseDto borrowerEmiDetails(BorrowerLoanRequestDto borrowerLoanRequestDto) {
		return newAdminLoanServiceRepo.borrowerEmiDetails(borrowerLoanRequestDto);
	}

	@GET
	@Path("/{loanRequestId}/loanOfferStatusToBorrower")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerOfferAcceptedStatusResponse borrowerOfferStatusCheck(@PathParam("loanRequestId") int loanRequestId) {
		return newAdminLoanServiceRepo.borrowerOfferStatusCheck(loanRequestId);
	}

	@POST
	@Path("/firstInterestStartDate")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLoanActiveDateCalculationResponse firstInterestCalculation(
			DealLoanActiveDateCalculationRequest dealLoanActiveDateCalculationRequest) {
		return newAdminLoanServiceRepo.firstInterestCalculation(dealLoanActiveDateCalculationRequest);
	}

	@GET
	@Path("/{loanRequestId}/commentsFromAdmin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerOfferAcceptedStatusResponse commentsGivenByAdmin(@PathParam("loanRequestId") int loanRequestId) {
		return newAdminLoanServiceRepo.commentsGivenByAdmin(loanRequestId);
	}

	@POST
	@Path("/interestDetailsForDeall")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelInterestPaymentsDto interestDetailsForDeall(IciciExcelsheetApprovalRequestDto request) {
		return newAdminLoanServiceRepo.interestDetailsForDeall(request);
	}

	@PATCH
	@Path("/interestsApprovalss")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelInterestPaymentsDto approvingInterestsForLenderss(IciciExcelsheetApprovalRequestDto request) {
		return newAdminLoanServiceRepo.approvingInterestsForLenderss(request);
	}

	@GET
	@Path("/dealLevelInterestCmsInfo/{monthName}/{year}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<DealLevelInterestPaymentsDto> dealLevelInterestCmsInfo(@PathParam("monthName") String monthName,
			@PathParam("year") String year) {
		return newAdminLoanServiceRepo.dealLevelInterestCmsInfo(monthName, year);
	}

	@GET
	@Path("/{userId}/{dealId}/closedDealsInterest")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderClosedDealsInformation lenderClosedDealLoanStatement(@PathParam(value = "userId") int userId,
			@PathParam(value = "dealId") int dealId) {
		return newAdminLoanServiceRepo.lenderClosedDealLoanStatement(userId, dealId);
	}

	@GET
	@Path("/{userId}/closedDealsDownload")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ClosedDealsInformation lenderClosedDealsDownload(@PathParam(value = "userId") int userId) {
		return newAdminLoanServiceRepo.lenderClosedDealsDownload(userId);
	}

	@GET
	@Path("/pendingH2HNotifications")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
//	@Scheduled(cron = "*0 0 10 * * ?")
	public List<PendingInterestsDto> pendingH2HNotifications() {
		return newAdminLoanServiceRepo.pendingH2HNotifications();
	}

	@POST
	@Path("/oxyWhatsappThroughExcel")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Map<String, Boolean> freeWhatsappMessagesThroughExcel(FormDataMultiPart multiPart,
			@FormDataParam("mobileNumber") String mobileNumber, @FormDataParam("message") String message) {

		KycFileRequest fileReqest = extracted(multiPart, KycType.WHATSAPP);

		return newAdminLoanServiceRepo.freeWhatsappMessagesThroughExcel(fileReqest, mobileNumber, message);
	}

	@POST
	@Path("/oxyWhatsapp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String, Boolean> freeWhatsappMessages(StatusCheckDto request) {

		return newAdminLoanServiceRepo.freeWhatsappMessages(request);
	}

	private KycFileRequest extracted(FormDataMultiPart multipart, KycType type) {
		if (multipart != null) {
			for (BodyPart part : multipart.getBodyParts()) {
				KycType uploadedType = KycType
						.valueOf(part.getContentDisposition().getParameters().get("name").toUpperCase());
				if (uploadedType != type) {
					continue;
				}
				InputStream is = part.getEntityAs(InputStream.class);
				ContentDisposition meta = part.getContentDisposition();
				KycFileRequest fileRequest = new KycFileRequest();
				fileRequest.setFileInputStream(is);
				fileRequest.setFileName(meta.getFileName());
				fileRequest.setKycType(uploadedType);
				return fileRequest;
			}
		}
		return null;
	}

	@GET
	@Path("/monthlyPaymentStats")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PaymentsStatsResponseDto paymentStats(@QueryParam("month") String month, @QueryParam("year") String year) {
		return newAdminLoanServiceRepo.getMonthlyPaymentStats(month, year);
	}

	@GET
	@Path("/lenderPaticipationClosing")
	@Scheduled(cron = "0 30 15 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String closingLendersPaticipationStatus() {
		return newAdminLoanServiceRepo.closingLendersPaticipationStatus();
	}

	@PATCH
	@Path("/borrowerNoc")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerNocResponseDto generateNocForBorrower(BorrowerNocRequestDto borrowerNocRequestDto)
			throws FileNotFoundException {
		return newAdminLoanServiceRepo.generateNocForBorrower(borrowerNocRequestDto);
	}

	@POST
	@Path("/{dealId}/movingPrincipalToH2h")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaticipationResponseDto movingPrincipalToH2hForWallet(@PathParam("dealId") Integer dealId,
			ListOfLendersMovingToWallet lenderPaticipationRequest) {
		return newAdminLoanServiceRepo.movingPrincipalToH2hForWallet(dealId, lenderPaticipationRequest);
	}

	@POST
	@Path("/assert_based_closed_deals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelResponseDto displayingAssertDeals(PaginationRequestDto pageRequestDto) {
		return newAdminLoanServiceRepo.displayingAssertDeals(pageRequestDto);
	}

	@POST
	@Path("/student_agreements")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Scheduled(cron = "0 30 15 * * ?")
	public String agreementsForStudentDeals() {
		return dealLevelInformationRepo.agreementsForStudentDeals();
	}

	@PATCH
	@Path("/principal_return_account_type")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaticipatedResponseDto principalReturnAccountType(
			OxyLendersAcceptedDealsRequestDto oxyLendersAcceptedDealsRequestDto) {
		return newAdminLoanServiceRepo.principalReturnAccountType(oxyLendersAcceptedDealsRequestDto);
	}

	@GET
	@Path("/webhooks")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String verifyEndPoint(@QueryParam("hub.mode") String mode, @QueryParam("hub.challenge") String challenge,
			@QueryParam("hub.verify_token") String verifytoken) {
		System.out.println("verify end point starts!!!!!!!!!!!!");

		logger.info("mode :" + mode);
		logger.info("challenge :" + challenge);
		logger.info("verify_token :" + verifytoken);

		return newAdminLoanServiceRepo.verifyEndPoint(mode, challenge, verifytoken);
	}

	@POST
	@Path("/sendingEmailWhileInitiatingAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<DealLevelResponseDto> initiatingEmailWhileInitiatingIntOrPrincipal(DealLevelRequestDto requestDto) {
		return newAdminLoanServiceRepo.initiatingEmailWhileInitiatingIntOrPrincipal(requestDto);
	}

	@POST
	@Path("/webhooks")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WebhookChangesDto sampleNotification(@HeaderParam(value = "X-Hub-Signature-256") String signature,
			WebhookChangesDto request) {

		return newAdminLoanServiceRepo.sampleNotification(signature, request);
	}

	@GET
	@Path("/{dealName}/group_chatid")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MessagesResponseDto whatsappGroupChatid(@PathParam(value = "dealName") String dealName) {
		return whatappService.whatsappGroupChatid(dealName);
	}

	@GET
	@Path("/{userId}/downloadRunningDeals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KycFileResponse downloadingRunningDealsInfo(@PathParam(value = "userId") Integer userId) {
		return newAdminLoanServiceRepo.downloadingRunningDealsInfo(userId);
	}

	@GET
	@Path("/readRejectedFilesInCms")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<FileResponse> readRejectedFilesInCms() {
		return newAdminLoanServiceRepo.readRejectedFilesInCms();
	}

	@GET
	@Path("/rejectedFilesInCms")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RejectedFilesRequest showRejectedFilesInCms() {
		return newAdminLoanServiceRepo.showRejectedFilesInCms();
	}

	@POST
	@Path("/{chatId}/{message}/group")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String whatsappMessageToGroupUsingUltra(@PathParam(value = "chatId") String chatId,
			@PathParam(value = "message") String message) {
		return whatappService.whatsappMessageToGroupUsingUltra(chatId, message);
	}

	@POST
	@Path("/{chatId}/{message}/individual")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String sendingIndividualMessageUsingUltraApi(@PathParam(value = "chatId") String chatId,
			@PathParam(value = "message") String message) {
		return whatappService.sendingIndividualMessageUsingUltraApi(chatId, message);
	}

	@GET
	@Path("/highestLendingActiveAmountOfLender")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<ActiveAmountResponse> highestLendingActiveAmountOfLenders() {
		return newAdminLoanServiceRepo.highestLendingActiveAmountOfLenders();
	}

	@GET
	@Path("/{pageNo}/{pageSize}/downloadLinkForActiveAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KycFileResponse downloadLinkForGettingActiveAmount(@PathParam("pageNo") Integer pageNo,
			@PathParam("pageSize") Integer pageSize) {
		return newAdminLoanServiceRepo.downloadLinkForGettingActiveAmount(pageNo, pageSize);
	}

	@POST
	@Path("/{userId}/updateLenderToGroup")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CommentsResponseDto updatingLendersValidityStatus(@PathParam("userId") Integer userId,
			OxyLendersGroups oxyLenderGroup) {
		return newAdminLoanServiceRepo.updatingLendersValidityStatus(userId, oxyLenderGroup);
	}

	@POST
	@Path("/downloadLinkForNotParticipatedInDeal")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KycFileResponse downloadLinkForNotParticipatedInDeal(StatsRequestDto requestDto) {
		return newAdminLoanServiceRepo.downloadLinkForNotParticipatedInDeal(requestDto);
	}

	@GET
	@Path("/{userId}/refereeDetials")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<LenderDetailsResponseDto> refereeDetailsInformation(@PathParam("userId") Integer userId) {

		return newAdminLoanServiceRepo.refereeDetailsInformation(userId);

	}

	@POST
	@Path("/wallet_amount_transfer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletTransferLenderToLenderResponseDto TransferWalletAmountFromLenderToLender(
			WalletTransferLenderToLenderRequestDto walletTransferLenderToLenderRequestDto) {
		logger.info("sender" + walletTransferLenderToLenderRequestDto.getSenderId());
		return walletToWalletTransferServiceRepo
				.TransferWalletAmountFromLenderToLender(walletTransferLenderToLenderRequestDto);
	}

	@POST
	@Path("/wallet_to_wallet_initiated_transfer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ListOfWalletToWalletTransactionsResponse walletToWalletRequests(PaginationRequestDto pageRequestDto) {
		return walletToWalletTransferServiceRepo.WalletToWalletRequests(pageRequestDto);
	}

	@POST
	@Path("/action_on_wallet_to_wallet_transfer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletTransferLenderToLenderResponseDto adminActionOnWalletToWallet(
			ActionOnRequestedTransaction actionOnRequestedTransaction) {
		return walletToWalletTransferServiceRepo.AdminActionOnWalletToWallet(actionOnRequestedTransaction);
	}

	@POST
	@Path("/notifications-before-deal-creation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelResponseDto beforeDealCreationNotification(DealLevelRequestDto dealLevelRequestDto) {
		return newAdminLoanServiceRepo.notificationsBeforedealCreation(dealLevelRequestDto);

	}

	@POST
	@Path("/listOfQueriesHisory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<UserQueryDetailsResponseDto> listOfQueryHistoryRaisedByUser(
			UserQueryDetailsRequestDto userQueryDetailsRequestDto) {
		return newAdminLoanServiceRepo.listOfQueryHistoryRaisedByUser(userQueryDetailsRequestDto);
	}

	@GET
	@Path("/principalEndNotifications")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<DealsResponseDto> principalEndNotifications() {
		return newAdminLoanServiceRepo.principalEndNotifications();
	}

	@GET
	@Path("/{userId}/partiallyClosedDealsForUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaticipatedDeal partiallyClosedDealsOfUser(@PathParam("userId") Integer userId) {
		return newAdminLoanServiceRepo.partiallyClosedDealsOfUser(userId);
	}

	@POST
	@Path("/{userId}/partiallyClosedDealsBasedOnPagination")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaticipatedDeal partiallyClosedDealsBasedOnPagination(@PathParam("userId") Integer userId,
			PageRequestDto pageRequestDto) {
		return newAdminLoanServiceRepo.partiallyClosedDealsBasedOnPagination(userId, pageRequestDto);
	}

	@POST
	@Path("/closedDealsForUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NewLenderDashboardResponseDto closedDealsForUser(NewLenderDashboardRequestDto newLenderDashboardRequestDto) {
		return newAdminLoanServiceRepo.closedDealsForUser(newLenderDashboardRequestDto);
	}

	@POST
	@Path("/closedDealsForUserBasedOnPagination")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NewLenderDashboardResponseDto closedDealsForUserBasedOnPagination(
			NewLenderDashboardRequestDto newLenderDashboardRequestDto) {
		return newAdminLoanServiceRepo.closedDealsForUserBasedOnPagination(newLenderDashboardRequestDto);
	}

	@GET
	@Path("/{userId}/runningDealsInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaticipatedDeal runningDealsInfo(@PathParam("userId") Integer userId) {
		return newAdminLoanServiceRepo.runningDealsInfo(userId);
	}

	@POST
	@Path("/{userId}/runningDealsInfoBasedOnPagination")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaticipatedDeal runningDealsInfoBasedOnPagination(@PathParam("userId") Integer userId,
			PageRequestDto pageRequestDto) {
		return newAdminLoanServiceRepo.runningDealsInfoBasedOnPagination(userId, pageRequestDto);
	}

	@GET
	@Path("/{userId}/closedDealsDownloadForUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ClosedDealsInformation lenderClosedDealsDownloadForUser(@PathParam(value = "userId") Integer userId) {
		return newAdminLoanServiceRepo.lenderClosedDealsDownloadForUser(userId);
	}

	@GET
	@Path("/{userId}/partiallyClosedDealsDownload")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public KycFileResponse partiallyClosedDealsDownload(@PathParam(value = "userId") Integer userId) {
		return newAdminLoanServiceRepo.partiallyClosedDealsDownload(userId);
	}

	@GET
	@Path("/dealEndNotifications")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// s@Scheduled(cron = "*0 0 10 * * ?")
	public String getDealEndNotifications() {
		return newAdminLoanServiceRepo.getDealEndNotifications();
	}

	@POST
	@Path("/deducting_fee_from_wallet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ScrowLenderTransactionResponse deductingFeeFromWallet(WalletRequestDto walletRequestDto) {
		return newAdminLoanServiceRepo.deductingFeeFromWallet(walletRequestDto);
	}

	@POST
	@Path("/deducting_lender_fee_from_wallet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ScrowLenderTransactionResponse lenderDeductingFeeFromWallet(WalletRequestDto walletRequestDto) {
		return newAdminLoanServiceRepo.deductingLenderFeeFromWallet(walletRequestDto);
	}

	@PATCH
	@Path("/lenders_fee_details_upadate")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderFeeDetailsResponse updateFeeAmount(LenderFeeDetailsHistoryDto lenderFeeDetailsHistoryDto) {
		return newAdminLoanServiceRepo.lenderFeesInAdmin(lenderFeeDetailsHistoryDto);
	}

	@GET
	@Path("/lender_fee_amount_details")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<LenderFeeDetailsResponse> lendersfeeSttausData() {
		return newAdminLoanServiceRepo.getAllFeesDetails();
	}

	@POST
	@Path("/principal-status")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<PrincipalReturningStatusBasedData> statusBasedPrincipalReturned(
			PrincipalReturningStatusRequestDto principalReturningStatusRequestDto) {
		return newAdminLoanServiceRepo.statusBasedPrincipalReturned(principalReturningStatusRequestDto);
	}

	@GET
	@Path("/cms-files")
	// @Scheduled(cron = "30 17 * * *")
	@Scheduled(cron = "0 30 14 * * ?")
	@Scheduled(cron = "0 30 16 * * ?")
	public List<AfterApprovalFilesDto> filesInApprovalFolder() {
		return newAdminLoanServiceRepo.filesInApprovalFolder();
	}

	@GET
	@Path("/before-cms-files")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Scheduled(cron = "0 0 */2 * * *")
	public List<BeforeApprovalFiles> beforeFiles() {
		return newAdminLoanServiceRepo.filesBeforeApproval();
	}

	@POST
	@Path("/cms-files")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<AfterApprovalFilesDto> filesInApprovalFolder(NotificationsDto request) {

		return newAdminLoanServiceRepo.filesInApprovalFolder(request);
	}

	@GET
	@Path("/lender-fee-before-cms-files")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 0 * ? * *")
	public LenderFeeFilesResponse lenderFeePaidInWalletbeforeFiles(LenderFeeInWalletDto lenderFeeInWalletDto) {
		return newAdminLoanServiceRepo.listOfLenderFeePaymentDetailsInWallet(lenderFeeInWalletDto);
	}

	@GET
	@Path("/approval-files-not-reading")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 0 * ? * *")
	public LenderInterestPaymentsDto afterFilesExecutedandNotReading() {
		return newAdminLoanServiceRepo.filesAfterApprovalNotReading();
	}

	@GET
	@Path("/approval-files-not-reading-admin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 0 * ? * *")
	public LenderInterestPaymentsDto afterFilesExecutedandNotReadingInAdmin() {
		return newAdminLoanServiceRepo.filesAfterApprovalNotReadingInAdmin();
	}

	@GET
	@Path("/cms-stats")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CmsPaymentsStatisticsDto getcmsStats(@QueryParam("date") String date) {

		return newAdminLoanServiceRepo.getCmsStats(date);
	}

	@POST
	@Path("/lender_fee_payment_details")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderFeeTransactions lenderFeePaymentDetails(WalletRequestDto walletRequestDto) {
		return newAdminLoanServiceRepo.lenderFeePaymentDetails(walletRequestDto);
	}

	@POST
	@Path("/{userId}/fee_details_for_lender")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderFeeTransactions feeDetailsForLenderBasedOnUserId(@PathParam(value = "userId") Integer userId,
			WalletRequestDto requestDto) {
		return newAdminLoanServiceRepo.feeDetailsForLenderBasedOnUserId(userId, requestDto);
	}

	@GET
	@Path("/{userId}/financial_year_data")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<FinancialYearResponseDto> listOfFinancialYearsEarningData(@PathParam(value = "userId") Integer userId) {
		return newAdminLoanServiceRepo.listOfFinancialYearsEarningData(userId);
	}

	@PATCH
	@Path("/to-cancel-ticket")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserQueryDetailsResponseDto toCancelTheRequestRaisedByTheUser(UserQueryDetailsRequestDto requestDto) {
		return newAdminLoanServiceRepo.toCancelTheRequestRaisedByTheUser(requestDto);
	}

	@GET
	@Path("/to-get-maximum-limit")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MaximumAmountInfo toGetMaximumLimit() {
		return newAdminLoanServiceRepo.toGetMaximumLimit();
	}

	@PATCH
	@Path("/to-cancel-withdrawal-request")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderWithdrawalFundsFromDealsResponseDto toCancelTheWithdrawRequestRaisedByTheUser(
			LenderWithdrawalFundsFromDealsRequestDto requestDto) {
		return newAdminLoanServiceRepo.toCancelTheWithdrawRequestRaisedByTheUser(requestDto);
	}

	@POST
	@Path("/generating_fee_wallet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String generatingFileForFeeFromWallet(WalletRequestDto requestDto) {
		return newAdminLoanServiceRepo.generatingFileForFeeFromWallet(requestDto);
	}

	@POST
	@Path("/monthly_interest_earnings")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public InterestAmountResponse monthlyInterestEarningsBasedOnDates(StatsRequestDto requestDto) {
		return newAdminLoanServiceRepo.monthlyInterestEarningsBasedOnDates(requestDto);
	}

	@GET
	@Path("/{userId}/wallet-to-wallet-debit-history")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ListOfWalletToWalletTransactionsResponse WalletToWalletDebitedHistory(@PathParam("userId") Integer userId) {
		return walletToWalletTransferServiceRepo.WalletToWalletDebitedHistory(userId);
	}

	@POST
	@Path("/deals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersDealsList getDealsInfo(PageRequestDto request) {
		return newAdminLoanServiceRepo.getDealsInfo(request);
	}

	@GET
	@Path("/{userId}/list_of_equity_deals_of_lender")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public EquityDealsResponse listOfEquityDealsOfLender(@PathParam("userId") Integer userId) {
		return newAdminLoanServiceRepo.listOfEquityDealsOfLender(userId);
	}

	@POST
	@Path("quarterly-reports/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseEntity<?> userQaurterlyReport(@RequestBody DealQuarterlyReportsRequest request,
			@PathParam("id") int id) {

		DealQuarterlyReportsResponse response = newAdminLoanServiceRepo.userQaurterlyReport(request, id);

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@POST
	@Path("quarterly-reports")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseEntity<?> usersQaurterlyReports(@RequestBody DealQuarterlyReportsRequest request) {

		String downloadUrl = newAdminLoanServiceRepo.usersQaurterlyReports(request);

		return new ResponseEntity<>(downloadUrl, new HttpHeaders(), HttpStatus.OK);
	}

	@POST
	@Path("/referral_bonus_info_financial_year")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ReferralResponse listOfReferralBonusInfo(ReferralRequestDto requestDto) {
		return newAdminLoanServiceRepo.listOfReferralBonusInfo(requestDto);
	}

	@GET
	@Path("/{userId}/borrower-payment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<BorrowerPaymentsResponse> paymentToBorrower(@PathParam(value = "userId") int userId) {

		return newAdminLoanServiceRepo.dealPaymentToBorrower(userId);

	}

	@PATCH
	@Path("/student_info")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse takingInfoFromStudent(StudentRequestDto requestDto) {
		return newAdminLoanServiceRepo.takingInfoFromStudent(requestDto);
	}

	@GET
	@Path("/files-output")
	@Scheduled(cron = "0 30 11 * * ?")
	@Scheduled(cron = "0 30 13 * * ?")
	public String FilesNotMovedOutput() {
		return newAdminLoanServiceRepo.FilesNotMovedOutput();
	}

	@POST
	@Path("/lenderParticipatedAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String lenderTotalAmountParticipatedIntoDeal() {
		return newAdminLoanServiceRepo.lenderparticapatedAmount();
	}

	@GET
	@Path("/{userId}/{dealId}/interest-info")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderFurtherPaymentDetails getfurtherMonthInterestDate(@PathParam("userId") Integer userId,
			@PathParam("dealId") Integer dealId) {
		return newAdminLoanServiceRepo.getfurtherMonthInterestDate(userId, dealId);
	}

	@PATCH
	@Path("/deal-reopen")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DealLevelResponseDto dealReopen(BorrowersDealsRequestDto borrowersDealsRequestDto) {
		return newAdminLoanServiceRepo.dealReopen(borrowersDealsRequestDto);
	}

	@GET
	@Path("/notachieved-deals")
	@Scheduled(cron = "0 30 12 * * ?")
	@Scheduled(cron = "0 30 4 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<DealInformationResponseDto> paticipationNotAchivedDealsInfo() {
		return dealLevelInformationRepo.paticipationNotAchivedDealsInfo();

	}

	@PATCH
	@Path("/extendsDuration")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TenureExtendsResponse dealDurationExtension(BorrowersDealsRequestDto borrowersDealsRequestDto) {

		return newAdminLoanServiceRepo.extendsDealDuration(borrowersDealsRequestDto);
	}

	@POST
	@Path("/remainderToLenders")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void remainderToLendersParticipatedAmount() {

		newAdminLoanServiceRepo.sendRemainderToLenderParticipationAmountReachesToLimit();

	}

	@GET
	@Path("/users-registered-count")
	// @Scheduled(cron = "0 30 4 * * ?")
	@Scheduled(cron = "0 30 14 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AdminDashboardDetails adminDashboard() {
		return newAdminLoanServiceRepo.adminDashboard();
	}

	@GET
	@Path("/{userId}/{pageNumber}/{pageSize}/auto-lending")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderParticipationUsingAutoLendingResponse lendersParticipatedUsingAutoLending(
			@PathParam("userId") Integer userId, @PathParam("pageNumber") Integer pageNumber,
			@PathParam("pageSize") Integer pageSize) {
		return newAdminLoanServiceRepo.lendersParticipatedUsingAutoLending(userId, pageNumber, pageSize);

	}

	@GET
	@Scheduled(cron = "0 30 10 * * ?")
	@Path("/principal_last_interest_missed_payments")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String principalAndLastInterestMissedPaymentsReminder() {
		return newAdminLoanServiceRepo.principalAndLastInterestMissedPaymentsReminder();
	}

	@GET
	@Path("/remainderDealTenure")
	@Scheduled(cron = "0 00 4 * * ?")
	@Scheduled(cron = "0 00 11 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<DealTenureUpdationResponse> remainderBeforeDealTenure() throws ParseException {
		return newAdminLoanServiceRepo.enableRemainderBeforeDealClouse();

	}

	@POST
	@Path("/futuredealsopen")
	@Scheduled(cron = "0 0/30 * * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowersDealsResponseDto dealsOpenInFuture() {
		return newAdminLoanServiceRepo.dealOpenInfuture();
	}

	@GET
	@Path("/missingPrinciple")
	@Scheduled(cron = "0 40 4 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String missingReminders() throws ParseException {
		return newAdminLoanServiceRepo.missingPaymentReminder();

	}

	// new methods
	@GET
	@Path("/withDrawnRequestReminder")
	@Scheduled(cron = "0 45 4 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String sendWithDrawnReminder() throws ParseException {
		return newAdminLoanServiceRepo.sendWithDrawnAmountReminder();
	}

	@GET
	@Path("/walletTowalletRemainder")
	@Scheduled(cron = "0 0 17 * * ?")
	@Scheduled(cron = "0 40 11 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String checkWalletToWallet() throws ParseException {
		return newAdminLoanServiceRepo.checkAndSendWalletToWalletRequestsRemainder();
	}

	@POST
	@Path("/referee-deals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<ReferralBonusBreakUpResponseDto> referralDealBasedBreakUp(
			ReferralBonusBreakUpRequestDto referralBonusBreakUpRequestDto) {
		return newAdminLoanServiceRepo.referralDealBasedBreakUp(referralBonusBreakUpRequestDto);
	}

	@GET
	@Path("/{userId}/wallet_to_wallet_request_list")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<WalletTransferLenderToLenderResponseDto> walletToWalletRequestsList(
			@PathParam("userId") Integer userId) {
		return walletToWalletTransferServiceRepo.WalletToWalletRequestsList(userId);
	}

	@PATCH
	@Path("/wallet_to_wallet_request")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletTransferLenderToLenderResponseDto walletToWalletStatusChange(
			WalletTransferLenderToLenderRequestDto walletTransferLenderToLenderRequestDto) {
		return walletToWalletTransferServiceRepo.walletToWalletStatusChange(walletTransferLenderToLenderRequestDto);
	}

	@GET
	@Path("/notification_to_dev_team")
	// @Scheduled(cron = "0 30 23 * * ?")
	@Scheduled(cron = "0 30 12 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerDealNotificationResponse notificationTodevTeam() {
		return newAdminLoanServiceRepo.notificationTodevTeam();

	}

	@GET
	@Path("/notification_to_dev_team_weekly")
	// @Scheduled(cron = "0 30 23 * * ?")
	// @Scheduled(cron = "0 31 12 * * 1")
	@Scheduled(cron = "0 30 12 * * SAT")

	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerDealNotificationResponse notificationTodevTeamWeekly() {
		return newAdminLoanServiceRepo.notificationTodevTeamWeekly();

	}

	@GET
	@Path("/notification_to_dev_team_monthlyDev")
	// @Scheduled(cron = "0 30 23 * * ?")
	// @Scheduled(cron = "0 30 12 L * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerDealNotificationResponse notificationTodevTeamMonthlyDev(
			BorrowerDealNotificationRequest borrowerDealNotificationRequest) {
		return newAdminLoanServiceRepo.notificationTodevTeamMonthlyDev(borrowerDealNotificationRequest);

	}

	@POST
	@Path("/newLenders_excel_sheet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NewLenderExcelSheetGenerateResponse excelSheetGenerateForNewLenders1(
			BorrowerDealNotificationRequest borrowerDealNotificationRequest) {
		return newAdminLoanServiceRepo.excelSheetGenerateForNewLenders1(borrowerDealNotificationRequest);

	}

	@GET
	@Path("/lenders_fee_status")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderFeeDetailsDto getPeningFeeLenders() {
		return newAdminLoanServiceRepo.getPendingFeeStatusLenders();
	}

	@POST
	@Path("/updationFromICICIFiles1") // reading reports with a post call
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<IciciExcelInterestsFiles> updationFromICICIFiles1(IciciExcelsheetApprovalRequestDto request) {
		return newAdminLoanServiceRepo.updatingFromICICIResponseFiles1(request);
	}

	@GET
	@Path("/updationFromICICIFiles1")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
//	@Scheduled(cron = "0 0 5 * * ?")
//	@Scheduled(cron = "0 30 8 * * ?")
	public List<IciciExcelInterestsFiles> updationFromICICIFiles1() {
		return newAdminLoanServiceRepo.updatingFromICICIResponseFiles1();
	}

	@GET
	@Path("/updating-icici-files1")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 0 14 * * ?")
	// @Scheduled(cron = "0 30 16 * * ?")
	public List<IciciExcelInterestsFiles> updatingICICIFilesOnCurrentDate1() {
		return newAdminLoanServiceRepo.updatingICICIFilesOnCurrentDate1();
	}

	@GET
	@Path("/calculating-outstanding-amount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String outstandingCalculation() {
		return newAdminLoanServiceRepo.outstandingCalculation();
	}

	@POST
	@Path("/{dealId}/lender_life_time_waiver")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String lenderLifeTimeWaiverStatus(@PathParam(value = "dealId") Integer dealId) {
		return newAdminLoanServiceRepo.updateLendersLifetimeStatus(dealId);
	}

	@GET
	@Path("/send-offer")
	@Scheduled(cron = "0 30 14 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String sendOfferToborrowers() {
		return dealLevelInformationRepo.sendOfferToborrowers();

	}

	@GET
	@Path("/notification_to_dev_team_monthly")
	@Scheduled(cron = "0 35 12 28-31 * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerDealNotificationResponse notificationTodevTeamMonthly() {
		return newAdminLoanServiceRepo.notificationTodevTeamMonthly();

	}

	@POST
	@Path("/{userId}/lender_fee_paid_type")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderFeeTransactions lenderFeeDetails(@PathParam(value = "userId") Integer userId,
			WalletRequestDto requestDto) {
		return newAdminLoanServiceRepo.lenderFeeDetaislOnUserId(userId, requestDto);
	}

	@POST
	@Path("/grapical_image_admin_module")

	// @Scheduled(cron = "0 30 12 28-31 * ?")

	@Produces(MediaType.APPLICATION_JSON)

	@Consumes(MediaType.APPLICATION_JSON)
	public List<NotificationGrapicalImageResponse> grapicalImageAdminModule(
			BorrowerDealNotificationRequest borrowerDealNotificationRequest) {
		return newAdminLoanServiceRepo.grapicalImageAdminModule(borrowerDealNotificationRequest);

	}

	@GET
	@Path("/getMessageId")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ChatBotResponse getLastMessageIds(ChatBotRequest chatBotRequest) {
		return newAdminLoanServiceRepo.getMessageIdForParticularMsg(chatBotRequest);
	}

	@POST
	@Path("/web_hook")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WebHookResponse saveWebHook(WebHookRequest webHookRequest) {
		return newAdminLoanServiceRepo.saveWebHook(webHookRequest);
	}

	@POST
	@Path("/before-intiated-file")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<IntiatedFileDto> getIntiatedFilesFromBeforeFolder(PaymentDetailDto paymentDetailDto) {
		return newAdminLoanServiceRepo.getIntiatedFilesBeforeFolderWithBetweenDates(paymentDetailDto);

	}

	@GET
	@Path("/9oclockColsedDealReminder")
	@Scheduled(cron = "0 15 4 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String closingLendersPaticipationBy9oclock() {
		return newAdminLoanServiceRepo.closingLendersPaticipationBy9oclock();
	}

	// Admin Show Bonus Monthly
	@POST
	@Path("/displayMonthlyAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceAmountResponseDto displayPaidAmount(LenderBonusAmountStatus lenderBonusAmountStatus) {
		return newAdminLoanServiceRepo.displayPaidAmount(lenderBonusAmountStatus);
	}

	// Lender Show Monthly call
	@POST
	@Path("/displayMonthlyReferrersAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReferenceAmountResponseDto displayMonthlyReferrersAmount(
			LenderBonusAmountStatus lenderBonusAmountStatus) {
		return newAdminLoanServiceRepo.displayMonthlyReferrersAmount(lenderBonusAmountStatus);
	}

	@POST
	@Path("/googleSheetOxyloans")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<GoogleSheetResponse> googleSheetOxyloans(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException {
		return newAdminLoanServiceRepo.googleSheetOxyloans(request);
	}

	@GET
	@Path("/agreementReminder")
	@Scheduled(cron = "0 15 4 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String reminderForMissedAgrrements() {
		return newAdminLoanServiceRepo.missedAgreementsReminders();
	}

	@GET
	@Path("/perDeal_users")
	// @Scheduled(cron = "0 32 12 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PerDealUsersCountResponse perDealUsersCountAndAmount() {
		return newAdminLoanServiceRepo.perDealUsersCountAndAmount();
	}

	@GET
	@Path("/reminderforWithoutBankDetailParticipated")
	// @Scheduled(cron = "0 15 4 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String usersWithotBankdetailUpdation() {
		return newAdminLoanServiceRepo.usersWithotBankdetailUpdationParticipatedInDeals();
	}

	@POST
	@Path("/validityExpireBetweenDates")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MonthlyValidityResponse validityExpirebetweenStartAndEndDates(
			MemberShipValidityRequest memberShipValidityRequest) {
		return newAdminLoanServiceRepo.userMemberShipExpireBetweenDates(memberShipValidityRequest);
	}

	@POST
	@Path("/TopFiftyLenders_excel_sheet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TopFiftyLendersListDto excleSheetForTopFiftyLenders(
			TopFiftyLendersListRequsetDto topFiftyLendersListRequsetDto) {
		return newAdminLoanServiceRepo.getTopFiftyLendersList(topFiftyLendersListRequsetDto);

	}

	@GET
	@Path("/{number}/highestTop3ReferralBonusLenders")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DownLoadLinkResponse gettingHighestReferralBonusLenders(@PathParam("number") Integer number) {
		return newAdminLoanServiceRepo.gettingHighestReferralBonusLenders(number);
	}

	@GET
	@Path("/activLendersParicipationAmount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DownLoadLinkResponse activLendersParicipationAmount() {
		return newAdminLoanServiceRepo.activLendersParicipationAmount();
	}

	@POST
	@Path("/getUserDetailsInGoogleSheet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GoogleSheetUserPersonalResponse googleSheetOxyloansUserData(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException {
		return newAdminLoanServiceRepo.googleSheetOxyloansUserData(request);
	}

	@POST
	@Path("/getUserAmountFromInGoogleSheet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<UserAmountGoogleSheetResponse> googleSheetOxyloansAmountUserData(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException {
		return newAdminLoanServiceRepo.googleSheetOxyloansAmountUserData(request);
	}

	@POST
	@Path("/DealsCount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GoogleSheetClosedDealsResponse googleSheetOxyloansClosedDealsCount(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException {
		return newAdminLoanServiceRepo.googleSheetOxyloansClosedDealsCount(request);
	}

	@POST
	@Path("/lenderFee_excel_sheet")
	// @Scheduled(cron = "0 30 23 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaidAmountResponseDto lenderFeePaidAmount(LenderPaidAmountRequestDto request) {
		return newAdminLoanServiceRepo.lenderFeePaidAmount(request);

	}

	// Scheduler run every month end
	@GET
	@Path("/Top3BonusLendersInfoToWtspGroupMessage")
	@Scheduled(cron = "0 55 12 30-31 * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 35 12 30 * ?")
	public String TopBonusEarnedUsersToWhatsmessage() {
		return newAdminLoanServiceRepo.topBonusEarnedUsersToWhatsmessage();
	}

	@GET
	@Path("/fd_amount_excel_sheet")
	// @Scheduled(cron = "0 30 23 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public FdAmountResponseDto fdAmount() {
		return newAdminLoanServiceRepo.fdAmount();

	}

	@GET
	@Path("/RunningDeals_excel_sheet/{year}/{month}/{dealpaticipationstatus}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NewLenderExcelSheetGenerateResponse excelSheetGenerateForRunningdeals(@PathParam("year") int year,
			@PathParam("month") int month, @PathParam("dealpaticipationstatus") String dealpaticipationstatus,

			BorrowerDealNotificationRequest borrowerDealNotificationRequest) {

		return newAdminLoanServiceRepo.excelSheetGenerateForRunningDeals(year, month, dealpaticipationstatus,
				borrowerDealNotificationRequest);
	}

	@GET
	@Path("/ClosedDeals_excel_sheet/{year}/{month}/{borrowerclosingstatus}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NewLenderExcelSheetGenerateResponse excelSheetGenerateForcloseddeals(@PathParam("year") int year,
			@PathParam("month") int month, @PathParam("borrowerclosingstatus") String borrowerclosingstatus,

			BorrowerDealNotificationRequest borrowerDealNotificationRequest) {

		return newAdminLoanServiceRepo.excelSheetGenerateForclosedDeals(year, month, borrowerclosingstatus,
				borrowerDealNotificationRequest);

	}

	// google SpreadSheet apis start
	@POST
	@Path("/googleSpreadSheetRead")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<GoogleSheetResponse> googleSpreadSheets(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException {
		return newAdminLoanServiceRepo.googleSpreadSheets(request);
	}

	@POST
	@Path("/getUserDetailsfromSpreadSheet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GoogleSheetUserPersonalResponse spreadSheetOxyloansUserData(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException {
		return newAdminLoanServiceRepo.spreadSheetOxyloansUserData(request);
	}

	@POST
	@Path("/getUserAmountFromSpreadSheet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<UserAmountGoogleSheetResponse> spreadSheetOxyloansAmountUserData(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException {
		return newAdminLoanServiceRepo.spreadSheetOxyloansAmountUserData(request);
	}

	@POST
	@Path("/closedAndTotaldealsCount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GoogleSheetClosedDealsResponse spreadSheetClosedDealsCount(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException {
		return newAdminLoanServiceRepo.spreadSheetClosedDealsCount(request);
	}
	// Google spread sheet apis end

	@PATCH
	@Path("/user_dob")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserPanNumberResponse updatePanDob(UserPanNumberRequest request) {
		return newAdminLoanServiceRepo.updatePanDob(request);

	}

	@POST
	@Path("/Deals_excel_sheet/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NewLenderExcelSheetGenerateResponse excelSheetGenerateFordeals(
			BorrowerDealNotificationRequestfordeals borrowerDealNotificationRequestForDeals) {
		return newAdminLoanServiceRepo.excelSheetGenerateforDeals(borrowerDealNotificationRequestForDeals);
	}

	@POST
	@Path("/ClosedDeals_excel_sheet/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NewLenderExcelSheetGenerateResponse excelSheetGenerateForcloseddeals(
			BorrowerDealNotificationRequestfordeals borrowerDealNotificationRequestForDeals) {
		return newAdminLoanServiceRepo.excelSheetGenerateforClosedindateDeals(borrowerDealNotificationRequestForDeals);
	}

	@GET
	@Path("/listOfMembershipPaidLenders")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<LenderMemberShipDetails> lendersValidityDetails() {
		return newAdminLoanServiceRepo.getLendersValidityStatus();
	}

	@PATCH
	@Path("/userValidityUpdation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String upadetValidity(UpdateLenderFeeData updateLenderFeeData) {
		return newAdminLoanServiceRepo.updateLenderFeeInformation(updateLenderFeeData);

	}

	@POST
	@Path("/activLendersParicipationAmountAndCount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ActiveLendersCountResponse activLendersParicipationAmountAndCount(
			PaginationRequestDto paginationRequestDto) {
		return newAdminLoanServiceRepo.activLendersParicipationAmountAndCount(paginationRequestDto);
	}

	@POST
	@Path("/getNotAchievedDeals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<ListOfDealsInformationToLender> notAchievedDealsData(PaginationRequestDto paginationRequestDto) {
		return newAdminLoanServiceRepo.getCurentDateDeals(paginationRequestDto);

	}
	@POST
	@Path("/intersetMissing")
	@Scheduled(cron = "0 40 15 * * ?")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String interestMissing() {
		 return newAdminLoanServiceRepo.sendReminderForInterestMissing();
	}
	
	@GET
	@Path("/lender-fee-before-cms-files1")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 0 * ? * *")
	public LenderFeeFilesResponse lenderFeePaidInWalletbeforeFiles1(LenderFeeInWalletDto lenderFeeInWalletDto) {
		return newAdminLoanServiceRepo.listOfLenderFeePaymentDetailsInWallet1(lenderFeeInWalletDto);
	}
	
	@POST
	@Path("/approval-files-reading-admin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Scheduled(cron = "0 0 * ? * *")
	public LenderInterestPaymentsDto afterFilesExecutedReadingInAdmin(InterestReadRequest request) {
		return newAdminLoanServiceRepo.filesAfterApprovalReadingInAdmin(request);
	}
	
	@GET
	@Path("/borrower_fd_data")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BorrowerFdDataDtoResponse borrowerFdDetails() {
		return newAdminLoanServiceRepo.borrowerFdDetailsData();

	}
	
	@POST
	@Path("/generating_fee_wallet1")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String generatingNewFileForFeeFromWallet(WalletRequestDto requestDto) {
		return newAdminLoanServiceRepo.generatingNewFileForFeeFromWallet(requestDto);
	}
	
}
