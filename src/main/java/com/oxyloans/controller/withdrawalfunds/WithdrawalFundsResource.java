package com.oxyloans.rest.withdrawalfunds;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.entity.borrowers.deals.dto.LenderPaticipatedDeal;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsFromDealsRequestDto;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsFromDealsResponseDto;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsRequest;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsResponse;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsSearchRequest;
import com.oxyloans.lender.withdrawalfunds.dto.ListOfLendersWithdrawalFundsInfo;
import com.oxyloans.lender.withdrawalfunds.service.LenderWithdrawalFundsService;
import com.oxyloans.lender.withdrawfunds.entity.LenderWithdrawalFunds;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.response.user.PaginationRequestDto;

@Component
@Path("/v1/user")
public class WithdrawalFundsResource {

	private static final Logger logger = LogManager.getLogger(WithdrawalFundsResource.class);

	@PostConstruct
	public void print() {
		logger.info("WithdrawalFundsResource rest initia......");
	}

	@Autowired
	private LenderWithdrawalFundsService lenderWithdrawalFundsService;

	@POST
	@Path("/savewithdrawalfundsinfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderWithdrawalFundsResponse saveWithDwalFundsInfo(LenderWithdrawalFundsRequest request)
			throws MessagingException {
		logger.info("saveWithDwalFundsInfo in rest......" + request.getUserId());
		return lenderWithdrawalFundsService.saveWithDwalFundsInfo(request);
	}

	@POST
	@Path("/lenderwithdrawalfundssearch")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<LenderWithdrawalFundsResponse> lenderWithdrawalFundsSearch(
			LenderWithdrawalFundsSearchRequest request) throws MessagingException {
		logger.info("lenderWithdrawalFundsSearch in rest......" + request.getUserId());
		return lenderWithdrawalFundsService.withdrawalFundsSearch(request);
	}

	@POST
	@Path("/updatewithdrawalfundsstatus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderWithdrawalFundsResponse updateWithdrawalFundsStatus(LenderWithdrawalFundsRequest request)
			throws MessagingException {
		logger.info("updateWithdrawalFundsStatus in rest......" + request.getId());
		return lenderWithdrawalFundsService.updateWithdrawalFundsStatus(request);
	}

	@GET
	@Path("/withdrawalRequestsScheduler")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<LenderWithdrawalFunds> withdrawalRequestsScheduler() {
		return lenderWithdrawalFundsService.withdrawalRequestsScheduler();
	}

	@GET
	@Path("/{userId}/lenderWithdrawFundsInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderWithdrawalFundsResponse lenderWithdrawFundsInfo(@PathParam("userId") int userId) {
		return lenderWithdrawalFundsService.lenderWithdrawFundsInfo(userId);

	}

	@POST
	@Path("/{userId}/lenderPaticipatedDealBasedOnDealType")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderPaticipatedDeal getLenderPaticipatedDealsBasedOnDealType(@PathParam(value = "userId") Integer userId,
			PaginationRequestDto pageRequestDto) {
		return lenderWithdrawalFundsService.getLenderPaticipatedDealsBasedOnDealType(userId, pageRequestDto);
	}

	@POST
	@Path("/withdrawalFundsFromDeals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderWithdrawalFundsFromDealsResponseDto withdrawalFundsFromDeals(
			LenderWithdrawalFundsFromDealsRequestDto lenderWithdrawalFundsFromDealsRequestDto) {
		return lenderWithdrawalFundsService.withdrawalFundsFromDeals(lenderWithdrawalFundsFromDealsRequestDto);
	}

	@POST
	@Path("/listOfWithdrawalsRequestedByLenders")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ListOfLendersWithdrawalFundsInfo listOfLendersWithdrawalInfo(PaginationRequestDto pageRequestDto) {
		return lenderWithdrawalFundsService.listOfLendersWithdrawalInfo(pageRequestDto);

	}

	@PATCH
	@Path("/approvingWithdrawalRequest")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderWithdrawalFundsFromDealsResponseDto approvingWithdrawalFund(
			LenderWithdrawalFundsFromDealsRequestDto lenderWithdrawalFundsFromDealsRequestDto) {
		return lenderWithdrawalFundsService.approvingWithdrawalFund(lenderWithdrawalFundsFromDealsRequestDto);

	}

	@GET
	@Path("/{userId}/{dealId}/displayingWithdrawalRequests")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ListOfLendersWithdrawalFundsInfo getWithdrawalRequestedInfo(@PathParam(value = "userId") Integer userId,
			@PathParam(value = "dealId") Integer dealId) {
		return lenderWithdrawalFundsService.getWithdrawalRequestedInfo(userId, dealId);

	}

	@GET
	@Path("/{id}/deal-withdrawal-interest")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderWithdrawalFundsFromDealsResponseDto getWithDrawalApprovedByAdmin(@PathParam(value = "id") Integer id) {
		return lenderWithdrawalFundsService.getWithDrawalApprovedByAdmin(id);

	}

	@PATCH
	@Path("/h2happroval")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ListOfLendersWithdrawalFundsInfo h2hApprovalForWithDrawalInterestAmount(
			ListOfLendersWithdrawalFundsInfo listOfLendersWithdrawalFundsInfo) {
		return lenderWithdrawalFundsService.h2hApprovalForWithDrawalInterestAmount(listOfLendersWithdrawalFundsInfo);
	}
}
