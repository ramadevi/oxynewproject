package com.oxyloans.rest.lenderpending.amount;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.oxyloans.entity.pending.amount.OxyLendersPendingAmount;
import com.oxyloans.lenders.pending.amount.dto.OxyLendersPendingAmountRequestDto;
import com.oxyloans.lenders.pending.amount.dto.OxyLendersPendingAmountResponseDto;
import com.oxyloans.lenders.pending.amount.dto.OxyStatusBasedPendingLendersRequest;
import com.oxyloans.lenders.pending.amount.dto.OxyStatusBasedPendingLendersResponse;
import com.oxyloans.lenders.pending.amount.service.LendersPendingAmountServiceRepo;

@Controller
@Path("/v1/user")
public class LenderPendingAmount {

	@Autowired
	private LendersPendingAmountServiceRepo lendersPendingAmountServiceRepo;

	@PATCH
	@Path("/lender-pending-amount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyLendersPendingAmount lenderPendingAmountReturning(
			OxyLendersPendingAmountRequestDto oxyLendersPendingAmountRequestDto) {
		return lendersPendingAmountServiceRepo.createUserPendingAmount(oxyLendersPendingAmountRequestDto);
	}

	@GET
	@Path("/{id}/single-lender-pending-amount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyLendersPendingAmountResponseDto singleLenderPendingAmount(@PathParam("id") Integer id) {
		return lendersPendingAmountServiceRepo.getLenderPendingAmount(id);
	}

	@POST
	@Path("/lender-pending-amount-search")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyStatusBasedPendingLendersResponse searchPendingAmountLendersByStatus(
			OxyStatusBasedPendingLendersRequest oxyStatusBasedPendingLendersRequest) {
		return lendersPendingAmountServiceRepo.searchPendingAmountLendersByStatus(oxyStatusBasedPendingLendersRequest);
	}

	@PATCH
	@Path("/{id}/pending-amount-excelsheet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OxyLendersPendingAmountResponseDto generateH2HFilePendingAmount(@PathParam("id") Integer id) {
		return lendersPendingAmountServiceRepo.generateH2HFilePendingAmount(id);
	}

}
