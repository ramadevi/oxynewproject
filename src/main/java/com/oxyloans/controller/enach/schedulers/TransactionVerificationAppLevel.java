package com.oxyloans.enach.schedulers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.oxyloans.common.util.DateUtil;
import com.oxyloans.enach.scheduler.EnachMandateVerificationResponse;
import com.oxyloans.enach.scheduler.TransactionVerificationJsonRequest;
import com.oxyloans.enach.scheduler.TransactionVerificationTempMapDto;
import com.oxyloans.repository.enach.EnachTransactionScedulingResponseRepoAppLevel;
import com.oxyloans.entity.enach.TransactionSchedulingResponseAppLevel;
import com.oxyloans.entity.enach.TransactionVerificationResponseAppLevel;
import com.oxyloans.repo.enach.EnachTransactionVerificationRepoAppLevel;
import com.oxyloans.service.loan.AdminLoanService;
import com.oxyloans.service.loan.dto.LoanEmiCardPaymentDetailsRequestDto;

@EnableScheduling
@Component
@RestController
public class TransactionVerificationAppLevel {

	private final Logger logger = LogManager.getLogger(getClass());

	@Value("${enachurl}")
	private String enachUrl;

	@Value("${merchantId}")
	private String merchantId;

	@Autowired
	private EnachTransactionScedulingResponseRepoAppLevel enachTransactionScedulingResponseRepoAppLevel;

	@Autowired
	private EnachTransactionVerificationRepoAppLevel enachTransactionVerificationRepoAppLevel;

	@Autowired
	private AdminLoanService adminLoanService;

	SimpleDateFormat format1 = new SimpleDateFormat("ddMMyyyy");
	SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat format3 = new SimpleDateFormat("dd-MM-yyyy");

	@Scheduled(cron = "0 30 9 6 * ?")
	@Scheduled(cron = "0 30 9 7 * ?")
	public void transactionVerificationForAppLevel() {
		logger.info("transactionVerification method start!!!! 0 30 6 5,6,7,8 * ?");
		LinkedHashMap<String, TransactionVerificationTempMapDto> loanIdMap = new LinkedHashMap<String, TransactionVerificationTempMapDto>();
		List<TransactionVerificationJsonRequest> transactionVerificationList = new ArrayList<TransactionVerificationJsonRequest>();
		ResponseEntity<EnachMandateVerificationResponse> responseEntity = null;
		List<EnachMandateVerificationResponse> transactionVerificationResponseList = new ArrayList<EnachMandateVerificationResponse>();
		LinkedHashMap<String, String> jsonMap = new LinkedHashMap<String, String>();
		List<TransactionSchedulingResponseAppLevel> loanEmiCardDetailsList = enachTransactionScedulingResponseRepoAppLevel
				.getEnachDetailsForTransactionVerification();
		try {
			if (loanEmiCardDetailsList != null && !loanEmiCardDetailsList.isEmpty()) {
				for (TransactionSchedulingResponseAppLevel dto : loanEmiCardDetailsList) {
					TransactionVerificationTempMapDto tempDto = new TransactionVerificationTempMapDto();
					tempDto.setId(dto.getId());
					//tempDto.setLoanId(dto.getApplicationId());
					tempDto.setApplicationId(dto.getApplicationId());
					tempDto.setEmiNumber(dto.getEmiNumber());
					tempDto.setEmiCardRefId(dto.getEmiCardRefId());
					TransactionVerificationJsonRequest jsonRequest = new TransactionVerificationJsonRequest();
					jsonRequest.getTransaction().setIdentifier(dto.getClntTxnRef());
					Date date = format1.parse(dto.getEnachScheduledDate());
					jsonRequest.getTransaction().setDateTime(format3.format(date));
					loanIdMap.put(jsonRequest.getTransaction().getIdentifier(), tempDto);
					transactionVerificationList.add(jsonRequest);
				}
			}

			if (transactionVerificationList != null && !transactionVerificationList.isEmpty()) {
				logger.info("Transaction Verification List Size  !!!!" + transactionVerificationList.size());
				HttpHeaders headers = new HttpHeaders();
				RestTemplate restTemplate = new RestTemplate();
				headers.set("Content-Type", "application/json");
				for (TransactionVerificationJsonRequest jsonRequest : transactionVerificationList) {
					jsonRequest.getMerchant().setIdentifier(merchantId);
					Gson gson = new Gson();
					String json = gson.toJson(jsonRequest);
					HttpEntity<TransactionVerificationJsonRequest> entity = new HttpEntity<TransactionVerificationJsonRequest>(
							jsonRequest, headers);
					responseEntity = restTemplate.exchange(enachUrl, HttpMethod.POST, entity,
							EnachMandateVerificationResponse.class, HttpStatus.OK);
					int statusCode = responseEntity.getStatusCodeValue();
					if (statusCode == 200) {
						EnachMandateVerificationResponse response = responseEntity.getBody();
						json = gson.toJson(response);
						transactionVerificationResponseList.add(response);
						jsonMap.put(response.getMerchantTransactionIdentifier(), json);
					}
				}
			}

			saveTransactionVerificationResponseForAppLevel(transactionVerificationResponseList, jsonMap, loanIdMap);

		} catch (Exception e) {
			logger.info("Exception occured in transactionVerification", e);
		}

		logger.info("transactionVerification method end!!!!");
	}

	@Transactional
	public void saveTransactionVerificationResponseForAppLevel(List<EnachMandateVerificationResponse> transactionResponseList,
			LinkedHashMap<String, String> jsonMap, LinkedHashMap<String, TransactionVerificationTempMapDto> loanIdMap) {
		logger.info("saveTransactionVerificationResponse method start");

		if (transactionResponseList != null && !transactionResponseList.isEmpty()) {
			for (EnachMandateVerificationResponse response : transactionResponseList) {
				try {
					DateUtil.wait(100);
					TransactionVerificationTempMapDto dto = loanIdMap.get(response.getMerchantTransactionIdentifier());
					TransactionSchedulingResponseAppLevel schedulingres = enachTransactionScedulingResponseRepoAppLevel
							.findById(dto.getId()).get();
					DateUtil.wait(16000);
					String status = response.getPaymentMethod().getPaymentTransaction().getStatusMessage();
					TransactionVerificationResponseAppLevel tvnresponse = new TransactionVerificationResponseAppLevel();
					tvnresponse.setApplicationId(dto.getApplicationId());
					tvnresponse.setEmiNumber(dto.getEmiNumber());
					tvnresponse.setTxnScheduligRefId("" + dto.getId());
					if (status.equalsIgnoreCase("I")) {
						tvnresponse.setEnachStatus("INITIATED");
					} else if (status.equalsIgnoreCase("S")) {
						tvnresponse.setEnachStatus("SUCCESS");
					} else if (status.equalsIgnoreCase("F")) {
						tvnresponse.setEnachStatus("FAILED");
					} else if (status.equalsIgnoreCase("A")) {
						tvnresponse.setEnachStatus("FAILED");
					}
					tvnresponse.setClntTxnRef(response.getMerchantTransactionIdentifier());
					tvnresponse.setTransactionDate(response.getPaymentMethod().getPaymentTransaction().getDateTime());
					if (!response.getPaymentMethod().getPaymentTransaction().getStatusCode().equalsIgnoreCase("0300")) {
						tvnresponse.setStatusDescription(response.getPaymentMethod().getError().getDesc());
					} else {
						tvnresponse.setStatusDescription(
								response.getPaymentMethod().getPaymentTransaction().getErrorMessage());
					}
					tvnresponse.setStatusCode(response.getPaymentMethod().getPaymentTransaction().getStatusCode());
					tvnresponse.setEnachJson(jsonMap.get(response.getMerchantTransactionIdentifier()));
					tvnresponse.setEnachType(schedulingres.getEnachType());
					enachTransactionVerificationRepoAppLevel.save(tvnresponse);
					DateUtil.wait(1000);
					schedulingres.setIsVerified(true);
					enachTransactionScedulingResponseRepoAppLevel.save(schedulingres);
					
					logger.info(tvnresponse.getStatusCode()+" STATUS CODE");
					if (tvnresponse.getStatusCode().equalsIgnoreCase("0300")) {
						LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto = new LoanEmiCardPaymentDetailsRequestDto();
						Date date = format1.parse(schedulingres.getEnachScheduledDate());
						loanEmiCardPaymentDetailsRequestDto.setAmountPaidDate(format2.format(date));
						loanEmiCardPaymentDetailsRequestDto.setEmiId(dto.getEmiNumber());
						
						loanEmiCardPaymentDetailsRequestDto.setApplicationIdNew(dto.getApplicationId());
						loanEmiCardPaymentDetailsRequestDto.setModeOfPayment("ENACH");
						loanEmiCardPaymentDetailsRequestDto
								.setTransactionReferenceNumber(schedulingres.getTxnRefNumber());
						loanEmiCardPaymentDetailsRequestDto.setPartialPaymentAmount(schedulingres.getTxnAmount());
						loanEmiCardPaymentDetailsRequestDto.setPaymentStatus("PARTPAID");
						DateUtil.wait(1000);
						 adminLoanService.updatingAmountBasedOnDisbursment(dto.getApplicationId(),
								 loanEmiCardPaymentDetailsRequestDto);
					}

				} catch (Exception e) {
					logger.info("Exception occured in saveTransactionVerificationResponse", e);
				}
			}

		}
		logger.info("saveTransactionVerificationResponse method end");
	}

}
