package com.oxyloans.enach.schedulers;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.oxyloans.common.util.DateUtil;
import com.oxyloans.enach.scheduler.EnachMandateVerificationResponse;
import com.oxyloans.enach.scheduler.TransactionSchedulingJsonRequest;
import com.oxyloans.entity.enach.EnachMinimumWithdrawUsers;
import com.oxyloans.repository.enach.EnachTransactionScedulingResponseRepo;
import com.oxyloans.entity.enach.TransactionSchedulingResponse;
import com.oxyloans.entity.loan.LoanEmiCard;
import com.oxyloans.repo.enach.EnachMinimumWithdrawUsersRepo;
import com.oxyloans.repo.loan.LoanEmiCardRepo;

@EnableScheduling
@Component
@RestController
public class EnachMinimumDebitScheduler {
	
	private final Logger logger = LogManager.getLogger(getClass());

	@Value("${enachurl}")
	private String enachUrl;

	@Value("${merchantId}")
	private String merchantId;
	
	private String day = "05";

	@Autowired
	private LoanEmiCardRepo loanEmiCardRepo;
	
	@Autowired
	private EnachTransactionScedulingResponseRepo enachTransactionScedulingResponseRepo;
	
	@Autowired
	private EnachMinimumWithdrawUsersRepo enachMinimumWithdrawUsersRepo;

	DecimalFormat df = new DecimalFormat("#.##");
	
	@Scheduled(cron = "0 30 3 2 * ?")
	//@Scheduled(cron = "0 */2 * ? * *")
	public void transactionScheduling() {
		logger.info("transactionScheduling method in  EnachMinimumDebitScheduler start!!!!");
		try {
			LinkedHashMap<String, String> loanIdMap = new LinkedHashMap<String, String>();
			List<TransactionSchedulingJsonRequest> transactionList = new ArrayList<TransactionSchedulingJsonRequest>();
			ResponseEntity<EnachMandateVerificationResponse> responseEntity = null;
			List<EnachMandateVerificationResponse> transactionResponseList = new ArrayList<EnachMandateVerificationResponse>();
			LinkedHashMap<String, String> jsonMap = new LinkedHashMap<String, String>();
			Date today = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(today);
			String emiDueDate = "";			
			int month = cal.get(Calendar.MONTH);
			int year = cal.get(Calendar.YEAR);
			month = month + 1;
			emiDueDate = year + "-" + (month < 10 ? ("0" + month) : (month)) + "-" + day;
			logger.info("Due Date!!!!" + emiDueDate);
			Iterable<EnachMinimumWithdrawUsers> minumdebitUsers= enachMinimumWithdrawUsersRepo.findAll();
			if(minumdebitUsers!=null) {
			for(EnachMinimumWithdrawUsers dto: minumdebitUsers) {				
				List<Object[]> emaiDeailsList = loanEmiCardRepo
						.getEMIDetailsForMiniumEnachDebit(DateUtil.convertStrYYYYMMDDToDate(emiDueDate),dto.getApplicationId());
				String endDateTime = day + (month < 10 ? ("0" + month) : (month)) + year;
				if (emaiDeailsList != null && !emaiDeailsList.isEmpty()) {
					emaiDeailsList.forEach(e -> {
						TransactionSchedulingJsonRequest jsonRequest = new TransactionSchedulingJsonRequest();
						df.setRoundingMode(RoundingMode.DOWN);
						jsonRequest.getTransaction().setIdentifier(e[0] == null ? "" : e[0].toString());
						jsonRequest.getPayment().getInstruction().setIdentifier(e[1] == null ? "" : e[1].toString());
						jsonRequest.getPayment().getInstruction().setAmount(dto.getAmount()==null ?"0":""+(dto.getAmount()/emaiDeailsList.size()));
						jsonRequest.getPayment().getInstruction().setEndDateTime(endDateTime);
						loanIdMap.put(jsonRequest.getTransaction().getIdentifier(), e[6] == null ? "" : e[6].toString());
						transactionList.add(jsonRequest);
					});
				}
			}			

			if (transactionList != null && !transactionList.isEmpty()) {
				logger.info("Transaction List Size in   EnachMinimumDebitScheduler !!!!" + transactionList.size());
				HttpHeaders headers = new HttpHeaders();
				RestTemplate restTemplate = new RestTemplate();
				headers.set("Content-Type", "application/json");
				for (TransactionSchedulingJsonRequest jsonRequest : transactionList) {
					jsonRequest.getMerchant().setIdentifier(merchantId);
					Gson gson = new Gson();
					String json = gson.toJson(jsonRequest);
					HttpEntity<TransactionSchedulingJsonRequest> entity = new HttpEntity<TransactionSchedulingJsonRequest>(
							jsonRequest, headers);
					responseEntity = restTemplate.exchange(enachUrl, HttpMethod.POST, entity,
							EnachMandateVerificationResponse.class, HttpStatus.OK);
					int statusCode = responseEntity.getStatusCodeValue();
					if (statusCode == 200) {
						EnachMandateVerificationResponse response = responseEntity.getBody();
						json = gson.toJson(response);
						transactionResponseList.add(response);
						jsonMap.put(response.getMerchantTransactionIdentifier(), json);
					}
				}
			}

			saveTransactionSchedulingResponse(transactionResponseList, jsonMap, loanIdMap);

		} 
		}catch (Exception e) {
			logger.info("Exception occured in transactionScheduling in  EnachMinimumDebitScheduler start", e);
		}
		

		logger.info("transactionScheduling method in  EnachMinimumDebitScheduler start end!!!!");

	}

	@Transactional
	public void saveTransactionSchedulingResponse(List<EnachMandateVerificationResponse> transactionResponseList,
			LinkedHashMap<String, String> jsonMap, LinkedHashMap<String, String> loanIdMap) {
		logger.info("saveTransactionVerificationResponse method in  EnachMinimumDebitScheduler start start");
		try {

			if (transactionResponseList != null && !transactionResponseList.isEmpty()) {
				for (EnachMandateVerificationResponse response : transactionResponseList) {
					DateUtil.wait(16000);
					if (!response.getPaymentMethod().getPaymentTransaction().getErrorMessage()
							.equalsIgnoreCase("Duplicate Merchant Request No")) {
						Integer id = Integer
								.parseInt(loanIdMap.get(response.getMerchantTransactionIdentifier() == null ? "0"
										: response.getMerchantTransactionIdentifier()));
						LoanEmiCard emiCard = loanEmiCardRepo.findById(id).get();
						
						TransactionSchedulingResponse txnSchedulingResponse = new TransactionSchedulingResponse();
						txnSchedulingResponse.setLoanId(emiCard.getLoanId());
						txnSchedulingResponse.setEmiNumber(emiCard.getEmiNumber());						
						txnSchedulingResponse.setTxnRefNumber(
								response.getPaymentMethod().getPaymentTransaction().getIdentifier());
						String status = response.getPaymentMethod().getPaymentTransaction().getStatusMessage();
						if (status.equalsIgnoreCase("I")) {
							txnSchedulingResponse.setEnachStatus("INITIATED");
						} else if (status.equalsIgnoreCase("S")) {
							txnSchedulingResponse.setEnachStatus("SUCCESS");
						} else if (status.equalsIgnoreCase("F")) {
							txnSchedulingResponse.setEnachStatus("FAILED");
						}
						txnSchedulingResponse.setClntTxnRef(response.getMerchantTransactionIdentifier());
						txnSchedulingResponse.setEnachScheduledDate(response.getPaymentMethod().getPaymentTransaction().getDateTime());
						txnSchedulingResponse.setStatusDescription(response.getPaymentMethod().getPaymentTransaction().getErrorMessage());
						txnSchedulingResponse.setStatusCode(response.getPaymentMethod().getPaymentTransaction().getStatusCode());
						txnSchedulingResponse.setEnachJson(
								jsonMap.get(response.getMerchantTransactionIdentifier()));
						txnSchedulingResponse.setEnachType("MANUALENACH");
						txnSchedulingResponse.setTxnAmount(Double.parseDouble(response.getPaymentMethod().getPaymentTransaction().getAmount()));
						txnSchedulingResponse.setEmiCardRefId(id);
						enachTransactionScedulingResponseRepo.save(txnSchedulingResponse);
						
					}
				}
			}

		} catch (Exception e) {
			logger.info("Exception occured in saveTransactionVerificationResponse in  EnachMinimumDebitScheduler start", e);
		}
		logger.info("saveTransactionVerificationResponse method in  EnachMinimumDebitScheduler start end");
	}

}
