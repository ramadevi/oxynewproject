package com.oxyloans.enach.schedulers;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.oxyloans.common.util.DateUtil;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.enach.scheduler.EnachMandateVerificationResponse;
import com.oxyloans.enach.scheduler.TransactionSchedulingJsonRequest;
import com.oxyloans.repository.enach.EnachTransactionScedulingResponseRepo;
import com.oxyloans.entity.enach.TransactionSchedulingResponse;
import com.oxyloans.entity.loan.LoanEmiCard;
import com.oxyloans.repo.loan.LoanEmiCardRepo;
import com.oxyloans.service.OperationNotAllowedException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@EnableScheduling
@Component
@RestController
public class TransactionScheduling {

	private final Logger logger = LogManager.getLogger(getClass());

	@Value("${enachurl}")
	private String enachUrl;

	@Value("${merchantId}")
	private String merchantId;

	private String interesrDay = "05"; // for interest amount
	private String principalDay = "06"; // for principal amount

	@Autowired
	private LoanEmiCardRepo loanEmiCardRepo;

	@Autowired
	private EnachTransactionScedulingResponseRepo enachTransactionScedulingResponseRepo;

	DecimalFormat df = new DecimalFormat("#.##");

	protected final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	// @Scheduled(cron = "0 */2 * ? * *")

	//@Scheduled(cron = "0 10 14 3 * ?")
	//@Scheduled(cron = "0 10 14 4 * ?")
	public void transactionScheduling() {

		logger.info("transactionScheduling method start!!!!");
		try {
			LinkedHashMap<String, String> loanIdMap = new LinkedHashMap<String, String>();
			List<TransactionSchedulingJsonRequest> transactionList = new ArrayList<TransactionSchedulingJsonRequest>();
			ResponseEntity<EnachMandateVerificationResponse> responseEntity = null;
			List<EnachMandateVerificationResponse> transactionResponseList = new ArrayList<EnachMandateVerificationResponse>();
			LinkedHashMap<String, String> jsonMap = new LinkedHashMap<String, String>();
			Date today = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(today);
			String emiDueDate = "";
			int month = cal.get(Calendar.MONTH);
			int year = cal.get(Calendar.YEAR);
			month = month + 1;
			String day = "";
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd");
			LocalDateTime now = LocalDateTime.now();
			String curretDate = dtf.format(now);
			if (curretDate.equals("03")) {
				day = interesrDay;
			} else {
				day = principalDay;
			}

			emiDueDate = year + "-" + (month < 10 ? ("0" + month) : (month)) + "-" + day;
			logger.info("Due Date!!!!" + emiDueDate);
			List<Object[]> emaiDeailsList = loanEmiCardRepo
					.getEMIDetailsByEMIDate(DateUtil.convertStrYYYYMMDDToDate(emiDueDate));
			// String endDateTime = "10" + (month < 10 ? ("0" + month) : (month)) + year;
			String endDateTime = day + (month < 10 ? ("0" + month) : (month)) + year;
			if (emaiDeailsList != null && !emaiDeailsList.isEmpty()) {
				emaiDeailsList.forEach(e -> {
					TransactionSchedulingJsonRequest jsonRequest = new TransactionSchedulingJsonRequest();
					df.setRoundingMode(RoundingMode.DOWN);
					String transactionIdentifier = e[0] == null ? "" : e[0].toString() + endDateTime;

					jsonRequest.getTransaction().setIdentifier(transactionIdentifier);
					logger.info("TransactionIdentifier!!!!" + transactionIdentifier);

					jsonRequest.getPayment().getInstruction().setIdentifier(e[1] == null ? "" : e[1].toString());
					jsonRequest.getPayment().getInstruction().setAmount(e[3] == null ? "" : e[3].toString());
					jsonRequest.getPayment().getInstruction().setEndDateTime(endDateTime);
					loanIdMap.put(jsonRequest.getTransaction().getIdentifier(), e[6] == null ? "" : e[6].toString());
					transactionList.add(jsonRequest);
				});
			}

			if (transactionList != null && !transactionList.isEmpty()) {
				logger.info("Transaction List Size  !!!!" + transactionList.size());
				HttpHeaders headers = new HttpHeaders();
				RestTemplate restTemplate = new RestTemplate();
				headers.set("Content-Type", "application/json");
				for (TransactionSchedulingJsonRequest jsonRequest : transactionList) {
					jsonRequest.getMerchant().setIdentifier(merchantId);
					Gson gson = new Gson();
					String json = gson.toJson(jsonRequest);
					HttpEntity<TransactionSchedulingJsonRequest> entity = new HttpEntity<TransactionSchedulingJsonRequest>(
							jsonRequest, headers);
					responseEntity = restTemplate.exchange(enachUrl, HttpMethod.POST, entity,
							EnachMandateVerificationResponse.class, HttpStatus.OK);
					int statusCode = responseEntity.getStatusCodeValue();
					if (statusCode == 200) {
						EnachMandateVerificationResponse response = responseEntity.getBody();
						json = gson.toJson(response);
						transactionResponseList.add(response);
						jsonMap.put(response.getMerchantTransactionIdentifier(), json);
					}
				}
			}

			saveTransactionSchedulingResponse(transactionResponseList, jsonMap, loanIdMap);

		} catch (Exception e) {
			logger.info("Exception occured in transactionScheduling", e);
		}

		logger.info("transactionScheduling method end!!!!");

	}

	@Transactional
	public void saveTransactionSchedulingResponse(List<EnachMandateVerificationResponse> transactionResponseList,
			LinkedHashMap<String, String> jsonMap, LinkedHashMap<String, String> loanIdMap) {
		logger.info("saveTransactionVerificationResponse method start");

		if (transactionResponseList != null && !transactionResponseList.isEmpty()) {
			for (EnachMandateVerificationResponse response : transactionResponseList) {
				try {
					DateUtil.wait(16000);
//					if (!response.getPaymentMethod().getPaymentTransaction().getErrorMessage()
//							.equalsIgnoreCase("Duplicate Merchant Request No")) {
					Integer id = Integer
							.parseInt(loanIdMap.get(response.getMerchantTransactionIdentifier() == null ? "0"
									: response.getMerchantTransactionIdentifier()));
					LoanEmiCard emiCard = loanEmiCardRepo.findById(id).get();

					TransactionSchedulingResponse txnSchedulingResponse = new TransactionSchedulingResponse();
					txnSchedulingResponse.setLoanId(emiCard.getLoanId());
					txnSchedulingResponse.setEmiNumber(emiCard.getEmiNumber());
					txnSchedulingResponse
							.setTxnRefNumber(response.getPaymentMethod().getPaymentTransaction().getIdentifier());
					String status = response.getPaymentMethod().getPaymentTransaction().getStatusMessage();
					if (status.equalsIgnoreCase("I")) {
						txnSchedulingResponse.setEnachStatus("INITIATED");
					} else if (status.equalsIgnoreCase("S")) {
						txnSchedulingResponse.setEnachStatus("SUCCESS");
					} else if (status.equalsIgnoreCase("F")) {
						txnSchedulingResponse.setEnachStatus("FAILED");
						/*
						 * throw new
						 * OperationNotAllowedException("TransactionScheduling is already Scheduled",
						 * ErrorCodes.ENTITY_ALREDY_EXISTS);
						 */
					}
					txnSchedulingResponse.setClntTxnRef(response.getMerchantTransactionIdentifier());
					txnSchedulingResponse
							.setEnachScheduledDate(response.getPaymentMethod().getPaymentTransaction().getDateTime());
					txnSchedulingResponse.setStatusDescription(
							response.getPaymentMethod().getPaymentTransaction().getErrorMessage());
					txnSchedulingResponse
							.setStatusCode(response.getPaymentMethod().getPaymentTransaction().getStatusCode());
					txnSchedulingResponse.setEnachJson(jsonMap.get(response.getMerchantTransactionIdentifier()));
					txnSchedulingResponse.setEnachType("DIGITALENACH");
					txnSchedulingResponse.setTxnAmount(
							Double.parseDouble(response.getPaymentMethod().getPaymentTransaction().getAmount()));
					txnSchedulingResponse.setEmiCardRefId(id);
					enachTransactionScedulingResponseRepo.save(txnSchedulingResponse);

					// }
				} catch (Exception e) {
					logger.info("Exception occured in saveTransactionVerificationResponse", e);
				}
			}

		}
		logger.info("saveTransactionVerificationResponse method end");
	}

}
