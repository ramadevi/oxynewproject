package com.oxyloans.enach.schedulers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.oxyloans.common.util.DateUtil;
import com.oxyloans.enach.scheduler.EnachMandateJsonRequest;
import com.oxyloans.enach.scheduler.EnachMandateRequest;
import com.oxyloans.enach.scheduler.EnachMandateVerificationResponse;
import com.oxyloans.repo.enach.EnachNativeRepo;
import com.oxyloans.repo.loan.OxyLoanRequestRepo;

@EnableScheduling
@Component
@RestController
public class EnachMandateVerification {

	@Value("${enachurl}")
	private String enachUrl;

	@Value("${merchantId}")
	private String merchantId;

	// private String loanIdConstatnt = "LN";

	@Autowired
	private EnachNativeRepo enachNativeRepo;

	@Autowired
	protected OxyLoanRequestRepo loanRequestRepo;

	private final Logger logger = LogManager.getLogger(getClass());

	@Scheduled(cron = "0 0 0 * * ?")
	//@Scheduled(cron = "0 */2 * ? * *")
	public void enachMandateVerification() {
		logger.info("enachMandateVerification method start!!!!");
		List<EnachMandateRequest> mandateList = new ArrayList<EnachMandateRequest>();
		List<EnachMandateVerificationResponse> verificationList = new ArrayList<EnachMandateVerificationResponse>();
		LinkedHashMap<String, String> jsonMap = new LinkedHashMap<String, String>();
		ResponseEntity<EnachMandateVerificationResponse> responseEntity = null;
		try {
			List<Object[]> objList = enachNativeRepo.getInitiatedMandateResponses();

			if (objList != null && !objList.isEmpty()) {

				objList.forEach(e -> {
					EnachMandateRequest mandateRequest = new EnachMandateRequest();
					if (e[2] != null) {
						mandateRequest.setConsumerIdentifier(e[2].toString());
					} else {
						if (e[0] != null) {
							mandateRequest.setConsumerIdentifier(e[0].toString());
						} else {
							mandateRequest.setConsumerIdentifier("");
						}
					}
					mandateRequest.setTransactionIdentifier(e[1] == null ? "" : e[1].toString());
					mandateRequest
							.setDateTime(e[3] == null ? "" : DateUtil.convertddmmyyyyhhmmsstoddmmyyyy(e[3].toString()));
					mandateList.add(mandateRequest);
				});

			}
			if (mandateList != null && !mandateList.isEmpty()) {
				logger.info("List Size  !!!!" + mandateList.size());
				HttpHeaders headers = new HttpHeaders();
				RestTemplate restTemplate = new RestTemplate();
				headers.set("Content-Type", "application/json");
				EnachMandateJsonRequest jsonRequest = new EnachMandateJsonRequest();
				for (EnachMandateRequest mandateRequest : mandateList) {
					jsonRequest.getMerchant().setIdentifier(merchantId);
					jsonRequest.getTransaction().setIdentifier(mandateRequest.getTransactionIdentifier());
					jsonRequest.getTransaction().setDateTime(mandateRequest.getDateTime());
					jsonRequest.getConsumer().setIdentifier(mandateRequest.getConsumerIdentifier());
					HttpEntity<EnachMandateJsonRequest> entity = new HttpEntity<EnachMandateJsonRequest>(jsonRequest,
							headers);
					responseEntity = restTemplate.exchange(enachUrl, HttpMethod.POST, entity,
							EnachMandateVerificationResponse.class, HttpStatus.OK);
					int statusCode = responseEntity.getStatusCodeValue();
					if (statusCode == 200) {
						EnachMandateVerificationResponse response = responseEntity.getBody();
						Gson gson = new Gson();
						String json = gson.toJson(response);
						verificationList.add(response);
						jsonMap.put(response.getMerchantTransactionIdentifier(), json);
						logger.info(
								"Merchant Trasaction Identifier !!!! " + response.getMerchantTransactionIdentifier());
					}
				}
			}
			saveMandateVerificationResponse(verificationList, jsonMap);

		} catch (Exception e) {
			logger.info("Exception occured in enachMandateVerification", e);
		}
		logger.info("enachMandateVerification method end");
	}

	@Transactional
	public void saveMandateVerificationResponse(List<EnachMandateVerificationResponse> verificationList,
			LinkedHashMap<String, String> jsonMap) {
		logger.info("saveMandateVerificationResponse method start");
		try {

			if (verificationList != null && !verificationList.isEmpty()) {
				for (EnachMandateVerificationResponse response : verificationList) {
					if (response.getPaymentMethod().getPaymentTransaction().getStatusCode().equalsIgnoreCase("0300")) {
						DateUtil.wait(16000);
						if (response.getPaymentMethod().getPaymentTransaction().getStatusMessage()
								.equalsIgnoreCase("Mandate Verification Successfull")) {

							enachNativeRepo.updateMandateVerificationResponses(
									response.getPaymentMethod().getPaymentTransaction().getBankReferenceIdentifier(),
									jsonMap.get(response.getMerchantTransactionIdentifier()),
									response.getMerchantTransactionIdentifier(), "SUCCESS", Boolean.TRUE);
						} else if (response.getPaymentMethod().getPaymentTransaction().getStatusMessage()
								.equalsIgnoreCase("Mandate has been De-activated")) {
							try {
								enachNativeRepo.updateMandateVerificationResponses(
										response.getPaymentMethod().getPaymentTransaction()
												.getBankReferenceIdentifier(),
										jsonMap.get(response.getMerchantTransactionIdentifier()),
										response.getMerchantTransactionIdentifier(), "DeActivated", Boolean.FALSE);
							} catch (Exception e) {
								logger.info("Exception occured in else block enachMandateVerification", e);
							}
						}
					}
				}
			}

		} catch (Exception e) {
			logger.info("Exception occured in saveMandateVerificationResponse", e);
		}
		logger.info("saveMandateVerificationResponse method end");
	}

	/*
	 * public void
	 * saveMandateVerificationResponse(List<EnachMandateVerificationResponse>
	 * verificationList,LinkedHashMap<String,String> jsonMap) {
	 * logger.info("saveMandateVerificationResponse method start"); try {
	 * 
	 * if(verificationList!=null && !verificationList.isEmpty()) {
	 * for(EnachMandateVerificationResponse response:verificationList) { if
	 * (response.getPaymentMethod().getPaymentTransaction().getStatusCode()
	 * .equalsIgnoreCase("0300")) { EnachMandateResponse enachMandateResponseData =
	 * emandateResponseRepo.findByTpslTxnId(
	 * response.getPaymentMethod().getPaymentTransaction().getIdentifier());
	 * 
	 * EnachMandate enachMandateData =
	 * enachMandateRepo1.findBymerchantTransactionIdentifier(response.
	 * getMerchantTransactionIdentifier()); int loanId =
	 * enachMandateData.getOxyLoanId(); logger.info("Loan Id  !!!!"+loanId); OxyLoan
	 * oxyLoandData = oxyLoanRepo.findById(loanId).get(); LoanRequest loanRequest =
	 * loanRequestRepo.findByLoanId(loanIdConstatnt + loanId); if
	 * (response.getPaymentMethod().getPaymentTransaction().getStatusMessage()
	 * .equalsIgnoreCase("Mandate Verification Successfull")) {
	 * 
	 * if(enachMandateData!=null) {
	 * enachMandateData.setMandateStatus(EnachMandate.MandateStatus.SUCCESS);
	 * enachMandateRepo1.save(enachMandateData); }
	 * 
	 * enachMandateResponseData.setTxnStatus(
	 * response.getPaymentMethod().getPaymentTransaction().getStatusCode());
	 * enachMandateResponseData.setTxnMsg("SUCCESS");
	 * enachMandateResponseData.setBankReferenceIdentifier(response.getPaymentMethod
	 * ().getPaymentTransaction().getBankReferenceIdentifier());
	 * enachMandateResponseData.setMandateVerificationResponseJson(jsonMap.get(
	 * response.getMerchantTransactionIdentifier()));
	 * emandateResponseRepo.save(enachMandateResponseData);
	 * oxyLoandData.setIsECSActivated(Boolean.TRUE);
	 * loanRequest.setIsECSActivated(Boolean.TRUE);
	 * loanRequestRepo.save(loanRequest); oxyLoanRepo.save(oxyLoandData); } else if
	 * (response.getPaymentMethod().getPaymentTransaction().getStatusMessage()
	 * .equalsIgnoreCase("Mandate has been De-activated")) { try {
	 * if(enachMandateData!=null) {
	 * enachMandateData.setMandateStatus(EnachMandate.MandateStatus.SUCCESS);
	 * enachMandateRepo1.save(enachMandateData); }
	 * 
	 * emandateResponseRepo.delete(enachMandateResponseData);
	 * oxyLoandData.setIsECSActivated(Boolean.FALSE);
	 * loanRequest.setIsECSActivated(Boolean.FALSE);
	 * loanRequestRepo.save(loanRequest); oxyLoanRepo.save(oxyLoandData); }catch
	 * (Exception e) {
	 * logger.info("Exception occured in else block enachMandateVerification", e); }
	 * } } } }
	 * 
	 * 
	 * } catch (Exception e) {
	 * logger.info("Exception occured in saveMandateVerificationResponse", e); }
	 * logger.info("saveMandateVerificationResponse method end"); }
	 */

}
