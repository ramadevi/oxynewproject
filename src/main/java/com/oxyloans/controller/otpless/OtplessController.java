package com.oxyloans.rest.otpless;

import javax.ws.rs.Consumes;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.otpless.dto.OtplessRequestDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.security.AuthenticationFilter;
import com.oxyloans.userlogin.history.OtplessLoginService;
import com.oxyloans.whatsapplogin.dto.WhatsappRequestDto;
import com.oxyloans.whatsapplogin.dto.WhatsappResponseDto;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

@Component
@Path("/v1/user")
public class OtplessController {

	@Autowired
	private OtplessLoginService otplessLoginService;

	@PATCH
	@Path("/otpless-login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse otplessLogin(OtplessRequestDto otplessRequestDto) {
		return otplessLoginService.otplessLogin(otplessRequestDto);
	}

	@POST
	@Path("/whatsapp-login-otp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WhatsappResponseDto whatsappLoginOtp(WhatsappRequestDto whatsappRequestDto) {
		return otplessLoginService.whatsappLogin(whatsappRequestDto);
	}

	@POST
	@Path("/whatsapp-login-otp-verification")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response whatsappLoginOtpVerification(WhatsappRequestDto whatsappRequestDto) {
	    WhatsappResponseDto whatsappResponseDto = otplessLoginService.whatsappLoginOtpVerification(whatsappRequestDto);

	    if (whatsappResponseDto.getMessage() == null) {
	        UserResponse userToken = otplessLoginService.whatsappLoginBasedOnId(whatsappResponseDto.getId());
	        ResponseBuilder response = Response.ok(userToken).header(AuthenticationFilter.ACCESS_TOKEN, userToken.getAccessToken());
			userToken.setAccessToken(null);
			return response.build();
	    } else {
	        return Response.ok(whatsappResponseDto).build();
	    }
	}
	@POST
	@Path("/whatsapp-login-after-otp-verification/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response whatsappLoginById(@PathParam("id") Integer id) {
		UserResponse userToken = otplessLoginService.whatsappLoginBasedOnId(id);
		ResponseBuilder response = Response.ok(userToken).header(AuthenticationFilter.ACCESS_TOKEN, userToken.getAccessToken());
		userToken.setAccessToken(null);
		return response.build();

		
	}
}
