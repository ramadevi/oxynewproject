package com.oxyloans.rest.relend;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oxyloans.lender.relend.service.LenderReLendService;
import com.oxyloans.relend.dto.LenderReLendRequest;
import com.oxyloans.relend.dto.LenderReLendResponse;
import com.oxyloans.relend.dto.LenderReLendSearchRequest;
import com.oxyloans.request.SearchResultsDto;

@Component
@Path("/v1/user")
public class LenderReLendResource {
	
	private static final Logger logger = LogManager.getLogger(LenderReLendResource.class);
	
	@PostConstruct
	public void print() {
		logger.info("LenderReLendResource rest initia......");
	}
	
	@Autowired
	private LenderReLendService lenderReLendService;
	
	@POST
	@Path("/saverelendconfig")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReLendResponse saveReLendConfig(LenderReLendRequest request) throws MessagingException {	
		logger.info("saveReLendConfig in rest......"+request.getUserId());
		return lenderReLendService.saveReLendConfig(request);
	}
	
	@GET
	@Path("/{userId}/getrelendconfigbylenderid")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReLendResponse getReLendConfigByLenderId(@PathParam("userId") Integer userId) throws MessagingException {	
		logger.info("getReLendConfigByLenderId in rest......"+userId);
		return lenderReLendService.getReLendConfigByLenderId(userId);
	}
	
	@POST
	@Path("/updaterelendconfig")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReLendResponse updRelendConfig(LenderReLendRequest request) throws MessagingException {	
		logger.info("updRelendConfig in rest......"+request.getUserId());
		return lenderReLendService.updReLendConfig(request);
	}
	
	@POST
	@Path("/relendsearch")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SearchResultsDto<LenderReLendResponse> searchReLendLenders(LenderReLendSearchRequest request) throws MessagingException {	
		logger.info("searchReLendLenders in rest......"+request.getUserId());
		return lenderReLendService.searchReLendLenders(request);
	}
	
	@POST
	@Path("/movefundsemitoescrow")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LenderReLendResponse transferFundsToLenderWallet(LenderReLendSearchRequest request) throws MessagingException {	
		logger.info("transferFundsToLenderWallet in rest......"+request.getUserId());
		return lenderReLendService.transferFundsToLenderWallet(request);
	}

}
