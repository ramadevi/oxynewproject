package com.oxyloans.service.cashfreepayment;

import java.text.DecimalFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.oxyloans.cashfree.AadhaarVerificationResponseDto;
import com.oxyloans.cashfree.CashFreeCustomerDetailObject;
import com.oxyloans.cashfree.CashFreeOrdePayRequest;
import com.oxyloans.cashfree.CashFreeOrderCreationsDto;
import com.oxyloans.cashfree.CashFreeOrderCreationsRequest;
import com.oxyloans.cashfree.CashFreeOrderDetailsReponseDto;
import com.oxyloans.cashfree.CashFreeOrderDetailsReponseDtos;
import com.oxyloans.cashfree.CashFreeOrderMetaRequest;
import com.oxyloans.cashfree.CashFreeOrderPayResponseDtos;
import com.oxyloans.cashfree.CashFreePaymentDto;
import com.oxyloans.cashfree.CashFreeQrResponseDto;
import com.oxyloans.cashfree.CashFreeUpiObjectDto;
import com.oxyloans.cashfree.CashfreePaymentStatusDto;
import com.oxyloans.cashfree.CashfreeRequestDto;
import com.oxyloans.cashfree.CashfreeResponseDto;
import com.oxyloans.cashfree.CashfreeVerificationRequestDto;
import com.oxyloans.cashfree.CashfreeVerificationResponseDto;
import com.oxyloans.cashfree.PanVerificationRequestDto;
import com.oxyloans.cashfree.PanVerificationResponseDto;
import com.oxyloans.cashfree.VirtualAccountRequestDto;
import com.oxyloans.cashfree.VirtualAccountUpiDto;
import com.oxyloans.common.util.DateUtil;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation.ParticipationLenderType;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDeals;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDealsRepo;
import com.oxyloans.entity.lender.payu.LenderPayuDetails;
import com.oxyloans.entity.lender.payu.LenderPayuDetails.PayuStatus;
import com.oxyloans.entity.lender.payu.LenderPayuDetailsRepo;
import com.oxyloans.entity.user.User;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.service.OperationNotAllowedException;

@Service
public class CashfreeService implements CashfreeServiceRepo {

	private final Logger logger = LogManager.getLogger(CashfreeService.class);

	@Autowired
	protected DateUtil dateUtil;

	@Autowired
	private UserRepo userRepo;

	@Value("${lenderFeeSuccess}")
	private String lenderFeeSuccess;

	@Value("${lenderFeeFailure}")
	private String lenderFeeFailure;

	@Value("${lenderAfterPartcipationFeeSuccess}")
	private String lenderAfterPartcipationFeeSuccess;

	@Value("${lenderAfterPartcipationFeeFailure}")
	private String lenderAfterPartcipationFeeFailure;

	@Value("${cashfeeAppId}")
	private String cashfeeAppId;

	@Value("${cashfeeSecretKey}")
	private String cashfeeSecretKey;

	@Value("${verificationSuiteClientId}")
	private String verificationSuiteClientId;

	@Value("${verificationSuiteClientSecret}")
	private String verificationSuiteClientSecret;

	@Value("${notifyUrl}")
	private String notifyUrl;

	@Value("${cashfeeTokenUrl}")
	private String cashfeeTokenUrl;

	@Value("${cashfeeOrderPayUrl}")
	private String cashfeeOrderPayUrl;

	@Autowired
	private LenderPayuDetailsRepo lenderPayuDetailsRepo;

	@Autowired
	private OxyLendersAcceptedDealsRepo oxyLendersAcceptedDealsRepo;

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	private Client client = ClientBuilder.newClient().register(MultiPartFeature.class).register(MultiPartWriter.class);

	String autocollectAuthorizeurlLive = "https://cac-api.cashfree.com/cac/v1/authorize";

	String autoCollectClientIdLive = "CF191095NEJ3FI01KL2EA6E";
	String autoCollectClientSecretLive = "da37af692651991972915411fcfd6fc69d37b1bf";

	String autocollectAuthorizeurlQA = "https://cac-gamma.cashfree.com/cac/v1/authorize";

	String autoCollectClientIdQA = "CF141947FJ8BPG96W5YU2YA";
	String autoCollectClientSecretQA = "a7e14c7755d60ebfad3d8cc300fccdd4324d8d33";

	private final String cashfreeVerificationSuiteVerificatinUrl = "https://payout-api.cashfree.com/payout/v1/authorize";

	@Value("${verificationSuccessUrl}")
	private String verificationSuccessUrl;

	@Override
	public CashfreeResponseDto lenderFeeUsingCashFree(int userId, CashfreeRequestDto cashfreeRequestDto) {
		String orderId = "orderOXY" + dateUtil.getHHmmMMddyyyyDateFormat() + userId;
		User userDetails = userRepo.findById(userId).get();
		CashfreeResponseDto cashfeeObject = new CashfreeResponseDto();
		Calendar currentDate = Calendar.getInstance();

		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
				.findDealAlreadyGiven(cashfreeRequestDto.getDealId());

		if (oxyBorrowersDealsInformation != null) {

			if (oxyBorrowersDealsInformation.getParticipationLenderType().equals(ParticipationLenderType.NEW)) {

				List<OxyLendersAcceptedDeals> oxylendersAcceptedDealsList = oxyLendersAcceptedDealsRepo
						.findbyUserId(userId);

				if (oxylendersAcceptedDealsList != null && !oxylendersAcceptedDealsList.isEmpty()) {

					OxyLendersAcceptedDeals oxylendersAcceptedDeals = oxyLendersAcceptedDealsRepo
							.toCheckUserAlreadyInvoledInDeal(userId, cashfreeRequestDto.getDealId());

					if (oxylendersAcceptedDeals == null) {
						throw new OperationNotAllowedException(
								"Newly registered users or Existing users who haven't participated yet in any deal "
										+ "can only participate in this deal.",
								ErrorCodes.PERMISSION_DENIED);
					}
				}

			}
		}

		if (userDetails != null) {

			Float litersOfPetrol = Float.parseFloat(cashfreeRequestDto.getAmount());
			DecimalFormat df = new DecimalFormat("0.00");
			df.setMaximumFractionDigits(2);
			String currentAmount = df.format(litersOfPetrol);

			cashfeeObject.setAppId(cashfeeAppId);
			cashfeeObject.setOrderId(orderId);
			cashfeeObject.setAmount(currentAmount);
			cashfeeObject.setEmail(userDetails.getEmail());
			cashfeeObject.setMobileNumber(userDetails.getMobileNumber());
			if (cashfreeRequestDto.getDealId() == 0) {
				cashfeeObject.setReturnUrl(verificationSuccessUrl);
			} else {
				if (cashfreeRequestDto.getPaymentType().equals("BEFORE")) {
					cashfeeObject.setReturnUrl(lenderFeeSuccess);
				} else {
					cashfeeObject.setReturnUrl(lenderAfterPartcipationFeeSuccess);
				}
			}
			if (cashfreeRequestDto.getPaymentType().equals("BEFORE")) {
				cashfeeObject.setNotifyUrl(lenderFeeFailure);
			} else {
				cashfeeObject.setNotifyUrl(lenderAfterPartcipationFeeFailure);

			}
			cashfeeObject.setOrderCurrency("INR");
			cashfeeObject.setOrderNote("LENDERFEE");
			if (userDetails.getPersonalDetails() != null) {
				cashfeeObject.setName(userDetails.getPersonalDetails().getFirstName());
			}
			cashfeeObject.setSignature(generatingSignature(cashfeeObject));

			LenderPayuDetails lenderPayuDetails = new LenderPayuDetails();
			lenderPayuDetails.setLenderUserId(userId);
			if (userDetails.getPersonalDetails() != null) {
				lenderPayuDetails.setLenderName(userDetails.getPersonalDetails().getFirstName());

			}

			lenderPayuDetails.setTransactionNumber(orderId);
			lenderPayuDetails.setAmount(Double.parseDouble(cashfreeRequestDto.getAmount()));
			lenderPayuDetails.setDealId(cashfreeRequestDto.getDealId());

			if (cashfreeRequestDto.getLenderFeePayments() != null) {

				lenderPayuDetails.setLenderFeePayments(
						LenderPayuDetails.LenderFeePayments.valueOf(cashfreeRequestDto.getLenderFeePayments()));
			}
			if (cashfreeRequestDto.getPaidFrom() != null) {
				lenderPayuDetails.setPaidFrom(cashfreeRequestDto.getPaidFrom());
			}

			lenderPayuDetails.setPaymentDate(currentDate.getTime());

			lenderPayuDetailsRepo.save(lenderPayuDetails);
			LenderPayuDetails transactionDetails = lenderPayuDetailsRepo.findByTransactionNumber(orderId);
			if (transactionDetails != null) {
				cashfeeObject.setTransactionId(transactionDetails.getId());
			}
		}

		return cashfeeObject;

	}

	public String generatingSignature(CashfreeResponseDto cashfeeObject) {
		String signature = null;
		Map<String, String> postData = new HashMap<String, String>();

		postData.put("appId", cashfeeObject.getAppId());
		postData.put("orderId", cashfeeObject.getOrderId());
		postData.put("orderAmount", cashfeeObject.getAmount());
		postData.put("orderCurrency", "INR");
		postData.put("orderNote", "LENDERFEE");
		postData.put("customerName", cashfeeObject.getName());
		postData.put("customerPhone", cashfeeObject.getMobileNumber());
		postData.put("customerEmail", cashfeeObject.getEmail());
		postData.put("returnUrl", cashfeeObject.getReturnUrl());
		postData.put("notifyUrl", cashfeeObject.getNotifyUrl());
		String data = "";
		SortedSet<String> keys = new TreeSet<String>(postData.keySet());
		for (String key : keys) {
			data = data + key + postData.get(key);
		}

		try {
			signature = encode(cashfeeSecretKey, data);
		} catch (Exception e) {

			e.printStackTrace();
		}

		return signature;

	}

	public String encode(String key, String data) throws Exception {
		String hash = null;
		Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
		SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
		sha256_HMAC.init(secret_key);

		hash = Base64.getEncoder().encodeToString(sha256_HMAC.doFinal(data.getBytes()));
		return hash;
	}

	public String generateCashfreeToken(String url, String clientId, String clientSecret) {

		logger.info("generating cashfree token starts");

		WebTarget webTarget = client.target(url);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");
		map.add("X-Client-Id", clientId);
		map.add("X-Client-Secret", clientSecret);

		invocationBuilder.headers(map);

		logger.info(invocationBuilder);

		Response response = invocationBuilder.post(Entity.json(null));

		String cashfreeResponse = response.readEntity(String.class);

		Gson gson = new Gson();
		CashfreeVerificationResponseDto cashfreeValidationResponseDto = gson.fromJson(cashfreeResponse,
				CashfreeVerificationResponseDto.class);

		String generatedToken = "";
		if (cashfreeValidationResponseDto.getSubCode().equals("200")) {
			generatedToken = cashfreeValidationResponseDto.getData().getToken();
		} else {
			throw new OperationNotAllowedException(cashfreeValidationResponseDto.getMessage(),
					ErrorCodes.PERMISSION_DENIED);
		}

		logger.info("generating cashfree token ends");
		return generatedToken;

	}

	@Override
	public CashfreeVerificationResponseDto verifyBankAccountAndIfsc(CashfreeVerificationRequestDto request) {

		if (request.getBankAccount() == null) {
			throw new OperationNotAllowedException("Please enter Bank Account No.", ErrorCodes.PERMISSION_DENIED);
		}

		if (request.getIfscCode() == null) {
			throw new OperationNotAllowedException("Please enter Ifsc Account No.", ErrorCodes.PERMISSION_DENIED);
		}

		String authorizationToken = "Bearer ";

		authorizationToken += generateCashfreeToken(cashfreeVerificationSuiteVerificatinUrl, verificationSuiteClientId,
				verificationSuiteClientSecret);

		WebTarget webTarget = client.target("https://payout-api.cashfree.com/payout/v1.2/validation/bankDetails");
		webTarget = webTarget.queryParam("bankAccount", request.getBankAccount()).queryParam("ifsc",
				request.getIfscCode());
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");
		map.add("Authorization", authorizationToken);

		invocationBuilder.headers(map);

		Response response = invocationBuilder.get();

		String cashfreeResponse = response.readEntity(String.class);

		Gson gson = new Gson();
		CashfreeVerificationResponseDto cashfreeValidationResponseDto = gson.fromJson(cashfreeResponse,
				CashfreeVerificationResponseDto.class);
		return cashfreeValidationResponseDto;

	}

	@Override
	public CashfreeVerificationResponseDto verifyBankUpi(String upi) {

		if (upi == null || upi.length() == 0) {
			throw new OperationNotAllowedException("Please enter Upi", ErrorCodes.PERMISSION_DENIED);
		}

		String authorizationToken = "Bearer ";

		authorizationToken += generateCashfreeToken(cashfreeVerificationSuiteVerificatinUrl, verificationSuiteClientId,
				verificationSuiteClientSecret);

		WebTarget webTarget = client.target("https://payout-api.cashfree.com/payout/v1/validation/upiDetails");

		webTarget = webTarget.queryParam("vpa", upi);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");
		map.add("Authorization", authorizationToken);

		invocationBuilder.headers(map);

		Response response = invocationBuilder.get();

		String cashfreeResponse = response.readEntity(String.class);

		Gson gson = new Gson();
		CashfreeVerificationResponseDto cashfreeValidationResponseDto = gson.fromJson(cashfreeResponse,
				CashfreeVerificationResponseDto.class);
		return cashfreeValidationResponseDto;

	}

	@Override
	public PanVerificationResponseDto verifyPan(String name, String pan) {

		if (pan == null || pan.length() == 0) {
			throw new OperationNotAllowedException("Please enter pan", ErrorCodes.PERMISSION_DENIED);
		}

		String authorizationToken = "Bearer ";

		authorizationToken += generateCashfreeToken(cashfreeVerificationSuiteVerificatinUrl, verificationSuiteClientId,
				verificationSuiteClientSecret);

		WebTarget webTarget = client.target("https://api.cashfree.com/verification/pan");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");
		map.add("X-Client-Id", verificationSuiteClientId);
		map.add("X-Client-Secret", verificationSuiteClientSecret);
		map.add("X-api-version", "v1");

		invocationBuilder.headers(map);

		PanVerificationRequestDto request = new PanVerificationRequestDto();

		logger.info(pan);

		request.setPan(pan);
		request.setName(name);

		Response response = invocationBuilder.post(Entity.json(request));

		String panResponse = response.readEntity(String.class);

		Gson gson = new Gson();
		PanVerificationResponseDto identityVerificationResponse = gson.fromJson(panResponse,
				PanVerificationResponseDto.class);
		return identityVerificationResponse;

	}

	@Override
	public AadhaarVerificationResponseDto verifyAadhaar(String aadhaar) {

		if (aadhaar == null || aadhaar.length() == 0) {
			throw new OperationNotAllowedException("Please enter aadhaar number", ErrorCodes.PERMISSION_DENIED);
		}

		String authorizationToken = "Bearer ";

		authorizationToken += generateCashfreeToken(cashfreeVerificationSuiteVerificatinUrl, verificationSuiteClientId,
				verificationSuiteClientSecret);

		WebTarget webTarget = client.target("https://api.cashfree.com/verification/aadhaar");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");
		map.add("X-Client-Id", verificationSuiteClientId);
		map.add("X-Client-Secret", verificationSuiteClientSecret);
		map.add("X-api-version", "v1");

		invocationBuilder.headers(map);

		PanVerificationRequestDto request = new PanVerificationRequestDto();
		request.setAadhaarNumber(aadhaar);

		Response response = invocationBuilder.post(Entity.json(request));

		String aadhaarResponse = response.readEntity(String.class);

		Gson gson = new Gson();
		AadhaarVerificationResponseDto identityVerificationResponse = gson.fromJson(aadhaarResponse,
				AadhaarVerificationResponseDto.class);
		return identityVerificationResponse;

	}

	@Override
	public CashfreeVerificationResponseDto createVirtualAccount(UserRequest userRequest) {

		String authorizationToken = "Bearer ";

		authorizationToken += generateCashfreeToken(autocollectAuthorizeurlLive, autoCollectClientIdLive,
				autoCollectClientSecretLive);

		WebTarget webTarget = client.target("https://cac-api.cashfree.com/cac/v1/createVA");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");

		map.add("Authorization", authorizationToken);

		logger.info(authorizationToken);

		invocationBuilder.headers(map);

		VirtualAccountRequestDto request = new VirtualAccountRequestDto();
		request.setEmail(userRequest.getEmail());
		request.setName(userRequest.getFirstName() + " " + userRequest.getLastName());
		request.setPhone(userRequest.getMobileNumber());
		/*
		 * request.setvAccountId(String.valueOf(Calendar.getInstance().getTimeInMillis()
		 * ).substring(6, 10) + "CFR" + userRequest.getId());
		 */

		request.setvAccountId("OXYCFR" + userRequest.getId());

		logger.info(request.toString());

		Response response = invocationBuilder.post(Entity.json(request));

		String virtualAccountResponse = response.readEntity(String.class);

		Gson gson = new Gson();
		CashfreeVerificationResponseDto cashfreeVerificationResponseDto = gson.fromJson(virtualAccountResponse,
				CashfreeVerificationResponseDto.class);

		logger.info(cashfreeVerificationResponseDto.toString());

		return cashfreeVerificationResponseDto;

	}

	@Override
	public CashfreeVerificationResponseDto createVirtualAccountUpi(UserRequest userRequest, String virtualAccountId) {

		String authorizationToken = "Bearer ";

		authorizationToken += generateCashfreeToken(autocollectAuthorizeurlLive, autoCollectClientIdLive,
				autoCollectClientSecretLive);

		WebTarget webTarget = client.target("https://cac-api.cashfree.com/cac/v1/createVA");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");

		map.add("Authorization", authorizationToken);

		invocationBuilder.headers(map);

		VirtualAccountUpiDto request = new VirtualAccountUpiDto();
		request.setEmail(userRequest.getEmail());
		request.setName(userRequest.getFirstName() + " " + userRequest.getLastName());
		request.setPhone(userRequest.getMobileNumber());
		request.setVirtualVpaId(virtualAccountId);

		logger.info(request.toString());

		Response response = invocationBuilder.post(Entity.json(request));

		String virtualAccountResponse = response.readEntity(String.class);

		Gson gson = new Gson();
		CashfreeVerificationResponseDto cashfreeVerificationResponseDto = gson.fromJson(virtualAccountResponse,
				CashfreeVerificationResponseDto.class);

		logger.info(cashfreeVerificationResponseDto.toString());

		return cashfreeVerificationResponseDto;

	}

	@Override
	public CashFreeQrResponseDto createQrCodeCashFree(String upi) {

		if (upi == null || upi.length() == 0) {
			throw new OperationNotAllowedException("Please enter upi", ErrorCodes.PERMISSION_DENIED);
		}

		String authorizationToken = "Bearer ";

		authorizationToken += generateCashfreeToken(autocollectAuthorizeurlQA, autoCollectClientIdQA,
				autoCollectClientSecretQA);

		WebTarget webTarget = client.target("https://cac-gamma.cashfree.com/cac/v1/createQRCode");

		webTarget = webTarget.queryParam("virtualVPA", upi);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");

		map.add("Authorization", authorizationToken);

		invocationBuilder.headers(map);

		Response response = invocationBuilder.get();

		String qrResponse = response.readEntity(String.class);

		Gson gson = new Gson();
		CashFreeQrResponseDto cashFreeQrResponseDto = gson.fromJson(qrResponse, CashFreeQrResponseDto.class);

		logger.info(cashFreeQrResponseDto.toString());

		return cashFreeQrResponseDto;

	}

	@Override
	public CashFreeQrResponseDto createDynamicQrCodeCashFree(String upi, Double amount) {

		if (upi == null || upi.length() == 0) {
			throw new OperationNotAllowedException("Please enter upi", ErrorCodes.PERMISSION_DENIED);
		}

		String authorizationToken = "Bearer ";

		authorizationToken += generateCashfreeToken(this.autocollectAuthorizeurlLive, this.autoCollectClientIdLive,
				this.autoCollectClientSecretLive);

		WebTarget webTarget = client.target("https://cac-api.cashfree.com/cac/v1/createDynamicQRCode");

		webTarget = webTarget.queryParam("virtualVPA", upi).queryParam("amount", amount).queryParam("remarks",
				"testing");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");

		map.add("Authorization", authorizationToken);

		invocationBuilder.headers(map);

		Response response = invocationBuilder.get();

		String qrResponse = response.readEntity(String.class);

		Gson gson = new Gson();
		CashFreeQrResponseDto cashFreeQrResponseDto = gson.fromJson(qrResponse, CashFreeQrResponseDto.class);

		logger.info(cashFreeQrResponseDto.toString());

		return cashFreeQrResponseDto;

	}

	@Override
	public CashFreeOrderDetailsReponseDtos userFeeUsingCashFreeForMobile(
			CashFreeOrderCreationsRequest cashfreeRequestDto) {

		CashFreeOrderDetailsReponseDtos response = new CashFreeOrderDetailsReponseDtos();

		String customId = "OXYCFR" + dateUtil.getHHmmMMddyyyyDateFormat() + cashfreeRequestDto.getUserId();

		Calendar currentDate = Calendar.getInstance();

		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
				.findDealAlreadyGiven(cashfreeRequestDto.getDealId());

		if (oxyBorrowersDealsInformation != null) {

			if (oxyBorrowersDealsInformation.getParticipationLenderType().equals(ParticipationLenderType.NEW)) {

				List<OxyLendersAcceptedDeals> oxylendersAcceptedDealsList = oxyLendersAcceptedDealsRepo
						.findbyUserId(cashfreeRequestDto.getUserId());

				if (oxylendersAcceptedDealsList != null && !oxylendersAcceptedDealsList.isEmpty()) {

					OxyLendersAcceptedDeals oxylendersAcceptedDeals = oxyLendersAcceptedDealsRepo
							.toCheckUserAlreadyInvoledInDeal(cashfreeRequestDto.getDealId(),
									cashfreeRequestDto.getUserId());

					if (oxylendersAcceptedDeals == null) {
						throw new OperationNotAllowedException(
								"Newly registered users or Existing users who haven't participated yet in any deal "
										+ "can only participate in this deal.",
								ErrorCodes.PERMISSION_DENIED);
					}
				}

			}
		}

		User user = userRepo.findById(cashfreeRequestDto.getUserId()).get();

		String email = "";
		String phone = "";
		if (user != null) {
			email = user.getEmail();
			phone = user.getMobileNumber();
		}

		CashFreeOrderCreationsDto obj = new CashFreeOrderCreationsDto();
		obj.setCustom_id(customId);
		obj.setOrder_amount(cashfreeRequestDto.getOrderAmount());
		obj.setOrder_currency("INR");
		CashFreeCustomerDetailObject cashFreeObject = new CashFreeCustomerDetailObject();

		cashFreeObject.setCustomer_id(obj.getCustom_id());
		cashFreeObject.setCustomer_email(email);
		cashFreeObject.setCustomer_phone(phone);

		CashFreeOrderMetaRequest cashFreeOrderMetaDto = new CashFreeOrderMetaRequest();
		cashFreeOrderMetaDto.setNotify_url(notifyUrl);
		obj.setCustomer_details(cashFreeObject);
		obj.setOrder_meta(cashFreeOrderMetaDto);

		CashFreeOrderDetailsReponseDtos cashFreeOrderDetailsReponseDto = generatingCashFreeOrder(obj);
		if (cashFreeOrderDetailsReponseDto != null) {
			LenderPayuDetails lenderPayuDetails = new LenderPayuDetails();
			lenderPayuDetails.setLenderUserId(cashfreeRequestDto.getUserId());
			if (user.getPersonalDetails() != null) {
				lenderPayuDetails.setLenderName(user.getPersonalDetails().getFirstName());
			}
			logger.info("orderId.." + cashFreeOrderDetailsReponseDto.getOrder_id());

			logger.info("orderToken.." + cashFreeOrderDetailsReponseDto.getOrder_token());
			if (cashFreeOrderDetailsReponseDto != null)
				lenderPayuDetails.setAmount(cashfreeRequestDto.getOrderAmount());
			lenderPayuDetails.setDealId(cashfreeRequestDto.getDealId());
			lenderPayuDetails.setPaymentDate(currentDate.getTime());
			logger.info("order_id..." + cashFreeOrderDetailsReponseDto.getCf_order_id());
			lenderPayuDetails.setOrderId(cashFreeOrderDetailsReponseDto.getOrder_id());
			lenderPayuDetails.setOrderToken(cashFreeOrderDetailsReponseDto.getOrder_token());
			lenderPayuDetails.setStatus(PayuStatus.TOKEN_INTIATED);
			lenderPayuDetails.setTransactionNumber(customId);
			lenderPayuDetailsRepo.save(lenderPayuDetails);

			response.setOrder_id(lenderPayuDetails.getOrderId());
			response.setOrder_token(response.getOrder_token());
		}
		return cashFreeOrderDetailsReponseDto;

	}

	private CashFreeOrderDetailsReponseDtos generatingCashFreeOrder(CashFreeOrderCreationsDto obj) {

		CashFreeOrderDetailsReponseDtos response = new CashFreeOrderDetailsReponseDtos();

		WebTarget webTarget = client.target(cashfeeTokenUrl);

		logger.info("cashfeeTokenUrl..." + cashfeeTokenUrl);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
		map.add("Content-Type", "application/json");
		map.add("x-client-id", cashfeeAppId);
		map.add("x-client-secret", cashfeeSecretKey);
		map.add("x-api-version", "2022-01-01");

		invocationBuilder.headers(map);

		Response resp = invocationBuilder.post(Entity.json(obj));

		String paymentResponse = resp.readEntity(String.class);

		logger.info(paymentResponse);

		Gson gson = new Gson();
		response = gson.fromJson(paymentResponse, CashFreeOrderDetailsReponseDtos.class);

		return response;
	}

	@Override
	public CashFreeOrderPayResponseDtos cashfreeOrderPay(CashFreeOrdePayRequest request) {

		CashFreeOrderPayResponseDtos response = new CashFreeOrderPayResponseDtos();
		CashFreeOrdePayRequest obj = new CashFreeOrdePayRequest();

		obj.setOrder_token(request.getOrder_token());

		CashFreePaymentDto cashFreePaymentMethodDto = new CashFreePaymentDto();

		CashFreeUpiObjectDto cashFreeUpiObject = new CashFreeUpiObjectDto();

		cashFreeUpiObject.setChannel("link");
		cashFreeUpiObject.setUpi_id("success@upi");

		cashFreePaymentMethodDto.setUpi(cashFreeUpiObject);

		obj.setPayment_method(cashFreePaymentMethodDto);
		// https://sandbox.cashfree.com/pg/orders/pay"
		WebTarget webTarget = client.target(cashfeeOrderPayUrl);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
		map.add("Content-Type", "application/json");
		map.add("x-client-id", cashfeeAppId);
		map.add("x-client-secret", cashfeeSecretKey);
		map.add("x-api-version", "2022-01-01");

		invocationBuilder.headers(map);

		Response resp = invocationBuilder.post(Entity.json(obj));

		String paymentResponse = resp.readEntity(String.class);
		logger.info(paymentResponse);

		Gson gson = new Gson();
		response = gson.fromJson(paymentResponse, CashFreeOrderPayResponseDtos.class);
		logger.info("response..." + response.getCf_payment_id());
		return response;

	}

	@Override
	public CashfreePaymentStatusDto getCashfeePaymentDetails(String order_id) {

		LenderPayuDetails lenderPayuDetails = lenderPayuDetailsRepo.findByOrderId(order_id);
		CashfreePaymentStatusDto cashfreePaymentStatusListDto = new CashfreePaymentStatusDto();
		WebTarget webTarget = client.target("https://api.cashfree.com/pg/orders/" + order_id + "/payments");
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<>();
		map.add("Content-Type", "application/json");
		map.add("x-client-id", cashfeeAppId);
		map.add("x-client-secret", cashfeeSecretKey);
		map.add("x-api-version", "2022-01-01");

		invocationBuilder.headers(map);

		Response response = invocationBuilder.post(Entity.entity("{\"my\":\"json\"}", MediaType.APPLICATION_JSON));

		if (response.getStatus() == 200) {
			String cashfeeOrder = response.readEntity(String.class);

			JsonParser jsonParser = new JsonParser();
			JsonArray jsonArray = jsonParser.parse(cashfeeOrder).getAsJsonArray();

			if (jsonArray.size() > 0) {
				JsonElement paymentStatusElement = jsonArray.get(0).getAsJsonObject().get("payment_status");
				if (paymentStatusElement != null) {
					String paymentStatus = paymentStatusElement.getAsString();
					if ("SUCCESS".equals(paymentStatus)) {
						lenderPayuDetails.setStatus(PayuStatus.COMPLETED);

						if (lenderPayuDetails != null) {
							cashfreePaymentStatusListDto.setPayuId(lenderPayuDetails.getId());

							cashfreePaymentStatusListDto.setTxnId(lenderPayuDetails.getTransactionNumber());
						}

					}
					if ("NOT_ATTEMPTED".equals(paymentStatus)) {
						lenderPayuDetails.setStatus(PayuStatus.NOT_ATTEMPTED);

					}
					if ("PENDING".equals(paymentStatus)) {
						lenderPayuDetails.setStatus(PayuStatus.PENDING);

					}
					if ("USER_DROPPED".equals(paymentStatus)) {
						lenderPayuDetails.setStatus(PayuStatus.USER_DROPPED);
						lenderPayuDetailsRepo.save(lenderPayuDetails);

					}
					cashfreePaymentStatusListDto.setPayment_status(paymentStatus);
				}

			}
			lenderPayuDetailsRepo.save(lenderPayuDetails);

		}
		return cashfreePaymentStatusListDto;
	}

	@Override
	public CashFreeOrderDetailsReponseDto lenderReactFeeUsingCashFreeOrder(
			CashFreeOrderCreationsRequest cashfreeRequestDto) {

		logger.info("lenderReactFeeUsingCashFreeOrder method is started...");
		
		String customId = "OXYCFR" + dateUtil.getHHmmMMddyyyyDateFormat() + cashfreeRequestDto.getUserId();

		Calendar currentDate = Calendar.getInstance();
		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
				.findDealAlreadyGiven(cashfreeRequestDto.getDealId());

		if (oxyBorrowersDealsInformation != null) {

			if (oxyBorrowersDealsInformation.getParticipationLenderType().equals(ParticipationLenderType.NEW)) {

				List<OxyLendersAcceptedDeals> oxylendersAcceptedDealsList = oxyLendersAcceptedDealsRepo
						.findbyUserId(cashfreeRequestDto.getUserId());

				if (oxylendersAcceptedDealsList != null && !oxylendersAcceptedDealsList.isEmpty()) {

					OxyLendersAcceptedDeals oxylendersAcceptedDeals = oxyLendersAcceptedDealsRepo
							.toCheckUserAlreadyInvoledInDeal(cashfreeRequestDto.getDealId(),
									cashfreeRequestDto.getUserId());

					if (oxylendersAcceptedDeals == null) {
						throw new OperationNotAllowedException(
								"Newly registered users or Existing users who haven't participated yet in any deal "
										+ "can only participate in this deal.",
								ErrorCodes.PERMISSION_DENIED);
					}
				}

			}
		}
		
		
		User user = userRepo.findById(cashfreeRequestDto.getUserId()).get();

		String email = "";
		String phone = "";
		if (user != null) {
			email = user.getEmail();
			phone = user.getMobileNumber();
		}

		CashFreeOrderCreationsDto obj = new CashFreeOrderCreationsDto();
		obj.setCustom_id(customId);
		obj.setOrder_amount(cashfreeRequestDto.getOrderAmount());
		obj.setOrder_currency("INR");
		CashFreeCustomerDetailObject cashFreeObject = new CashFreeCustomerDetailObject();

		cashFreeObject.setCustomer_id(obj.getCustom_id());
		cashFreeObject.setCustomer_email(email);
		cashFreeObject.setCustomer_phone(phone);

		CashFreeOrderMetaRequest cashFreeOrderMetaDto = new CashFreeOrderMetaRequest();
		cashFreeOrderMetaDto.setNotify_url(notifyUrl);
		cashFreeOrderMetaDto.setReturn_url(cashfreeRequestDto.getReturn_url());
		obj.setCustomer_details(cashFreeObject);
		obj.setOrder_meta(cashFreeOrderMetaDto);

		CashFreeOrderDetailsReponseDto cashFreeOrderDetailsReponseDto = generatingCashFreeOrder1(obj);
		
		if (cashFreeOrderDetailsReponseDto != null) {
			LenderPayuDetails lenderPayuDetails = new LenderPayuDetails();
			lenderPayuDetails.setLenderUserId(cashfreeRequestDto.getUserId());
			if (user.getPersonalDetails() != null) {
				lenderPayuDetails.setLenderName(user.getPersonalDetails().getFirstName());
			}
			logger.info("orderId.." + cashFreeOrderDetailsReponseDto.getOrder_id());

			//logger.info("orderToken.." + cashFreeOrderDetailsReponseDto.getOrder_token());
			
			logger.info("payment_session_id.." + cashFreeOrderDetailsReponseDto.getPayment_session_id());
			
			if (cashFreeOrderDetailsReponseDto != null)
				lenderPayuDetails.setAmount(cashFreeOrderDetailsReponseDto.getOrder_amount());
			lenderPayuDetails.setDealId(cashfreeRequestDto.getDealId());
			lenderPayuDetails.setPaymentDate(currentDate.getTime());
			logger.info("order_id..." + cashFreeOrderDetailsReponseDto.getCf_order_id());
			lenderPayuDetails.setOrderId(cashFreeOrderDetailsReponseDto.getOrder_id());
			lenderPayuDetails.setOrderToken(cashFreeOrderDetailsReponseDto.getOrder_token());
			lenderPayuDetails.setStatus(PayuStatus.valueOf(cashFreeOrderDetailsReponseDto.getOrder_status()));
			lenderPayuDetails.setTransactionNumber(customId);
			lenderPayuDetails.setPaymentSessionId(cashFreeOrderDetailsReponseDto.getPayment_session_id());
			lenderPayuDetailsRepo.save(lenderPayuDetails);

			
		}
		logger.info("lenderReactFeeUsingCashFreeOrder method is ended...");
		return cashFreeOrderDetailsReponseDto;

	}

	private CashFreeOrderDetailsReponseDto generatingCashFreeOrder1(CashFreeOrderCreationsDto obj) {

		logger.info("generatingCashFreeOrder1 method is started...");
		CashFreeOrderDetailsReponseDto response = new CashFreeOrderDetailsReponseDto();

		WebTarget webTarget = client.target("https://api.cashfree.com/pg/orders");

		logger.info("cashfeeTokenUrl..." + cashfeeTokenUrl);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
		map.add("Content-Type", "application/json");
		map.add("x-client-id", cashfeeAppId);
		map.add("x-client-secret", cashfeeSecretKey);
		map.add("x-api-version", "2023-08-01");

		invocationBuilder.headers(map);

		Response resp = invocationBuilder.post(Entity.json(obj));

		String paymentResponse = resp.readEntity(String.class);

		logger.info(paymentResponse);

		Gson gson = new Gson();
		response = gson.fromJson(paymentResponse, CashFreeOrderDetailsReponseDto.class);
		logger.info("generatingCashFreeOrder1 method is ended...");
		return response;
	}


	@Override
	public CashFreeOrderDetailsReponseDto getCashfeePaymentDetailsStatus(String order_id) {
		
		logger.info("getCashfeePaymentDetailsStatus method is started...");
		
		LenderPayuDetails lenderPayuDetails = lenderPayuDetailsRepo.findByOrderId(order_id);
		
		CashFreeOrderDetailsReponseDto cashfreePaymentStatusListDto = new CashFreeOrderDetailsReponseDto();
		
		if(lenderPayuDetails!=null ) {
			
	    WebTarget webTarget = client.target("https://api.cashfree.com/pg/orders/" + order_id);
	    Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
	            .accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
		map.add("Content-Type", "application/json");
		map.add("x-client-id", cashfeeAppId);
		map.add("x-client-secret", cashfeeSecretKey);
		map.add("x-api-version", "2023-08-01");

	    invocationBuilder.headers(map);

	    Response response = invocationBuilder.post(Entity.entity("{\"my\":\"json\"}", MediaType.APPLICATION_JSON));

	    String paymentResponse = response.readEntity(String.class);

		logger.info(paymentResponse);

		Gson gson = new Gson();
		cashfreePaymentStatusListDto = gson.fromJson(paymentResponse, CashFreeOrderDetailsReponseDto.class);

		lenderPayuDetails.setStatus(PayuStatus.valueOf(cashfreePaymentStatusListDto.getOrder_status()));
		lenderPayuDetailsRepo.save(lenderPayuDetails);
		}else {
			throw new OperationNotAllowedException("This Order Id is Not Present : " +order_id , ErrorCodes.PERMISSION_DENIED);
			
		}
		
		logger.info("getCashfeePaymentDetailsStatus method is ended...");
	    return cashfreePaymentStatusListDto;
	}

}
