package com.oxyloans.service.cashfreepayment;

import com.oxyloans.cashfree.AadhaarVerificationResponseDto;
import com.oxyloans.cashfree.CashFreeOrdePayRequest;
import com.oxyloans.cashfree.CashFreeOrderCreationsRequest;
import com.oxyloans.cashfree.CashFreeOrderDetailsReponseDto;
import com.oxyloans.cashfree.CashFreeOrderDetailsReponseDtos;
import com.oxyloans.cashfree.CashFreeOrderPayResponseDtos;
import com.oxyloans.cashfree.CashFreeQrResponseDto;
import com.oxyloans.cashfree.CashfreePaymentStatusDto;
import com.oxyloans.cashfree.CashfreeRequestDto;
import com.oxyloans.cashfree.CashfreeResponseDto;
import com.oxyloans.cashfree.CashfreeVerificationRequestDto;
import com.oxyloans.cashfree.CashfreeVerificationResponseDto;
import com.oxyloans.cashfree.PanVerificationResponseDto;
import com.oxyloans.request.user.UserRequest;

public interface CashfreeServiceRepo {

	public CashfreeResponseDto lenderFeeUsingCashFree(int userId, CashfreeRequestDto cashfreeRequestDto);

	public CashfreeVerificationResponseDto verifyBankAccountAndIfsc(CashfreeVerificationRequestDto request);

	public CashfreeVerificationResponseDto verifyBankUpi(String upi);

	public PanVerificationResponseDto verifyPan(String name, String pan);

	public AadhaarVerificationResponseDto verifyAadhaar(String aadhaarNumber);

	public CashfreeVerificationResponseDto createVirtualAccount(UserRequest userRequest);

	public CashFreeQrResponseDto createQrCodeCashFree(String upi);

	public CashFreeQrResponseDto createDynamicQrCodeCashFree(String upi, Double amount);

	public CashfreeVerificationResponseDto createVirtualAccountUpi(UserRequest userRequest, String upi);

	public CashFreeOrderDetailsReponseDtos userFeeUsingCashFreeForMobile( CashFreeOrderCreationsRequest cashfreeRequestDto);

	public CashFreeOrderPayResponseDtos cashfreeOrderPay(CashFreeOrdePayRequest request);

	public CashfreePaymentStatusDto getCashfeePaymentDetails( String order_id);

	public CashFreeOrderDetailsReponseDto lenderReactFeeUsingCashFreeOrder(
			CashFreeOrderCreationsRequest cashfreeRequestDto);

	public CashFreeOrderDetailsReponseDto getCashfeePaymentDetailsStatus(String order_id);
}
