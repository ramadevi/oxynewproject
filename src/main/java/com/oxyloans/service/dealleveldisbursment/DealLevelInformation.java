package com.oxyloans.service.dealleveldisbursment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.engine.template.PdfGeenrationException;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceDetails;
//import com.oxyloans.entity.LenderReferenceDetails.LenderReferralBonus;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferralBonusUpdated;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsList;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsRequestDto;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsResponseDto;
import com.oxyloans.entityborrowersdealsdto.DealDisbursmentInformation;
import com.oxyloans.entityborrowersdealsdto.DealInformationResponseDto;
import com.oxyloans.entityborrowersdealsdto.DealLevelLoanEmiCardInformation;
import com.oxyloans.entityborrowersdealsdto.DealLevelRequestDto;
import com.oxyloans.entityborrowersdealsdto.DealLevelResponseDto;
import com.oxyloans.entityborrowersdealsdto.IndividualDealsInformation;
import com.oxyloans.entityborrowersdealsdto.LenderDealsInformation;
import com.oxyloans.entityborrowersdealsdto.LenderGroupNamesRequestDto;
import com.oxyloans.entityborrowersdealsdto.LenderLoanDetailsResponseDto;
import com.oxyloans.entityborrowersdealsdto.LenderParticipationUpdatedInfo;
import com.oxyloans.entityborrowersdealsdto.LenderTotalParticipationInterestDetails;
import com.oxyloans.entityborrowersdealsdto.LendersDealsInformation;
import com.oxyloans.entityborrowersdealsdto.LoanResponseForDealsDto;
import com.oxyloans.entity.borrowers.deals.information.LendersPaticipationUpdation;
import com.oxyloans.entity.borrowers.deals.information.LendersPaticipationUpdationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyDealsRateofinterestToLendersgroup;
import com.oxyloans.entity.borrowers.deals.information.OxyDealsRateofinterestToLendersgroupRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDeals;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDealsRepo;
import com.oxyloans.entity.enach.ApplicationLevelEnachMandate;
import com.oxyloans.repository.enach.ApplicationLevelEnachMandateRepo;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWallet;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletNativeRepo;
import com.oxyloans.entity.lender.returns.LendersReturns;
import com.oxyloans.entity.lender.returns.LendersReturnsRepo;
import com.oxyloans.repository.lendergroup.OxyLendersGroupRepo;
import com.oxyloans.entity.loan.LoanEmiCard;
import com.oxyloans.entity.loan.LoanOfferdAmount;
import com.oxyloans.entity.loan.LoanOfferdAmount.LoanOfferdStatus;
import com.oxyloans.entity.loan.LoanRequest;
import com.oxyloans.entity.loan.LoanRequest.DurationType;
import com.oxyloans.entity.loan.OxyLoan;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.User.Status;
import com.oxyloans.entity.user.type.OxyUserType;
import com.oxyloans.entity.user.type.OxyUserTypeRepo;
import com.oxyloans.file.FileRequest;
import com.oxyloans.file.FileRequest.FileType;
import com.oxyloans.file.FileResponse;
import com.oxyloans.repo.loan.ApplicationLevelLoanEmiCard;
import com.oxyloans.repo.loan.ApplicationLevelLoanEmiCardRepo;
import com.oxyloans.repo.loan.LenderBorrowerConversationRepo;
import com.oxyloans.repo.loan.LoanEmiCardRepo;
import com.oxyloans.repo.loan.OxyLoanRepo;
import com.oxyloans.repo.loan.OxyLoanRequestRepo;
import com.oxyloans.repo.user.LenderReferenceDetailsRepo;
//import com.oxyloans.repo.user.LenderReferralBonusRepo;
import com.oxyloans.repo.user.LenderReferralBonusUpdatedRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.service.file.IFileManagementService;
import com.oxyloans.service.loan.ActionNotAllowedException;
import com.oxyloans.service.loan.AdminLoanService;
import com.oxyloans.serviceloan.LoanRequestDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;

@Service
public class DealLevelInformation implements DealLevelInformationRepo {

	private final Logger logger = LogManager.getLogger(DealLevelInformation.class);

	protected final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	protected final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-YYYY hh:mm:ss");

	protected final SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	@Autowired
	private OxyLendersAcceptedDealsRepo oxyLendersAcceptedDealsRepo;

	@Autowired
	private OxyLoanRepo oxyLoanRepo;

	@Autowired
	private OxyLoanRequestRepo loanRequestRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private LenderReferenceDetailsRepo lenderReferenceDetailsRepo;

	/*
	 * @Autowired private LenderReferralBonusRepo lenderReferralBonusRepo;
	 */
	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Autowired
	private LendersReturnsRepo lendersReturnsRepo;

	@Autowired
	private AdminLoanService adminLoanService;

	@Autowired
	private OxyDealsRateofinterestToLendersgroupRepo oxyDealsRateofinterestToLendersgroupRepo;

	@Autowired
	private OxyLendersGroupRepo oxyLendersGroupRepo;

	@Autowired
	private LendersPaticipationUpdationRepo lendersPaticipationUpdationRepo;

	@Value("${referralBonusByBorrower}")
	private Integer referralBonusByBorrower;

	@Autowired
	private LenderOxyWalletNativeRepo lenderOxyWalletNativeRepo;

	@Autowired
	private LenderReferralBonusUpdatedRepo lenderReferralBonusUpdatedRepo;

	@Autowired
	private ApplicationLevelLoanEmiCardRepo applicationLevelLoanEmiCardRepo;

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	private ApplicationLevelEnachMandateRepo applicationLevelEnachMandateRepo;

	@Autowired
	private LoanEmiCardRepo loanEmiCardRepo;

	@Autowired
	private OxyUserTypeRepo oxyUserTypeRepo;

	@Autowired
	private LenderBorrowerConversationRepo lenderBorrowerConversationRepo;

	@Autowired
	protected IFileManagementService fileManagementService;

	@Value("${iciciFilePathBeforeApproval}")
	private String iciciFilePathBeforeApproval;

	@Autowired
	private IEmailService emailService;

	@Value("${iciciReportsManualFolder}")
	private String iciciReportsManualFolder;

	@Override
	public DealDisbursmentInformation getDealParticipationAndDisbursmentPendingInfo(int dealId) {
		DealDisbursmentInformation dealDisbursmentInformation = new DealDisbursmentInformation();
		try {
			Double lendersParticipatedAmount = oxyLendersAcceptedDealsRepo.getListOfLendersParticipatedAmount(dealId);

			if (lendersParticipatedAmount != null) {
				dealDisbursmentInformation.setSumOfLendersParticipatedAmount(
						BigDecimal.valueOf(lendersParticipatedAmount).toBigInteger());
			}
			Double disbursmentAmountPending = oxyLoanRepo.getDealIdDisbursmentPendingAmount(dealId);
			if (disbursmentAmountPending != null) {
				dealDisbursmentInformation
						.setSumOfDisbursmentAmountPending(BigDecimal.valueOf(disbursmentAmountPending).toBigInteger());
			}

		} catch (Exception e) {
			logger.info("Exception in  getDealParticipationAndDisbursmentPendingInfo Method");
		}
		dealDisbursmentInformation.setDealId(dealId);
		return dealDisbursmentInformation;

	}

	@Override
	public DealLevelResponseDto dealLevelDisbursment(int dealId, DealLevelRequestDto dealLevelRequestDto) {
		DealLevelResponseDto dealLevelResponseDto = new DealLevelResponseDto();
		try {
			List<String> listOfLoanIds = oxyLoanRepo.getListOfDisbursmentPendingLoanIds(dealId);
			if (listOfLoanIds != null && !listOfLoanIds.isEmpty()) {
				for (int i = 0; i < listOfLoanIds.size(); i++) {
					String loanId = listOfLoanIds.get(i);
					OxyLoan loan = oxyLoanRepo.findByLoanId(loanId);
					if (loan.getLoanStatus().toString().equalsIgnoreCase(Status.ACTIVE.toString())) {
						if (dealLevelRequestDto.getDisbursedDate() != null) {
							loan.setBorrowerDisbursedDate(
									expectedDateFormat.parse(dealLevelRequestDto.getDisbursedDate()));
							Double disburmentAmount = loan.getDisbursmentAmount();
							int borrowerParentRequestId = loan.getBorrowerParentRequestId();
							LoanRequest loanRequestDetails = loanRequestRepo.findById(borrowerParentRequestId).get();
							if (loanRequestDetails != null && loanRequestDetails.getLoanOfferedAmount() != null) {
								logger.info("Application level fee percentage start");
								double applicationLevelAmount = loanRequestDetails.getLoanOfferedAmount()
										.getLoanOfferedAmount();
								double applicationLevelFree = loanRequestDetails.getLoanOfferedAmount()
										.getBorrowerFee();
								double free = 0.0;
								double Gst = 0.0;
								int percentageForFree = 1; // to get Percentage value
								free = (applicationLevelAmount * percentageForFree) / 100;
								Gst = (free * 18) / 100;
								double total = free + Gst;
								percentageForFree = (int) (applicationLevelFree / total);
								logger.info("Application level fee percentage end");
								logger.info("Loan level fee percentage start");

								double loanLevelFree = (disburmentAmount * percentageForFree) / 100;
								double loanLevelFreeWithGst = (loanLevelFree * 18) / 100;
								double totalFree = loanLevelFree + loanLevelFreeWithGst;
								loan.setBorrowerTransactionFee(totalFree);
								loan.setFeePercentage(percentageForFree);
								loan.setBorrowerFeePaid(true);
								oxyLoanRepo.save(loan);

								logger.info("Loan level fee percentage end");

							}
							loan.setAdminComments(Status.DISBURSED.toString());
							oxyLoanRepo.save(loan);

							LoanRequest loanRequest = loanRequestRepo
									.findByUserIdAndParentRequestIdIsNull(loan.getBorrowerUserId());
							LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();
							if (loanOfferdAmount != null) {

								int borrowerParentRequestid = loanOfferdAmount.getId();
								Double totalDisbursedAmount = oxyLoanRepo
										.getTotalActiveAmountByParentId(borrowerParentRequestid);
								if (totalDisbursedAmount != null
										&& loanOfferdAmount.getLoanOfferedAmount().equals(totalDisbursedAmount)) {
									loanRequest.setParentRequestId(0);
									loanOfferdAmount.setLoanOfferdStatus(LoanOfferdStatus.LOANOFFERCOMPLETED);
									loanOfferdAmount.setLoanRequest(loanRequest);
									loanRequest.setLoanOfferedAmount(loanOfferdAmount);
									loanRequestRepo.save(loanRequest);

									User userDetails = userRepo.findById(loan.getBorrowerUserId()).get();
									userDetails.setAdminComments(null);

									userRepo.save(userDetails);
									loanRequestRepo.save(loanRequest);

								}
							}

							logger.info("Reference Amount Starts................................");
							Integer refereeId = loan.getBorrowerUserId();

							LenderReferenceDetails refereeInfo = lenderReferenceDetailsRepo.findRefereeInfo(refereeId);

							if (refereeInfo != null) {

								User referee = userRepo.findById(refereeInfo.getRefereeId()).get();

								if (referee != null && referee.getPrimaryType().equals(User.PrimaryType.BORROWER)) {

									Double disbursmentAmount = loan.getDisbursmentAmount();

									logger.info("disbursmentAmount......................" + disbursmentAmount);

									logger.info(
											"referralBonusByBorrower......................" + referralBonusByBorrower);

									Double amount = 0.0;

									amount = disbursmentAmount / referralBonusByBorrower;

									refereeInfo.setStatus(LenderReferenceDetails.Status.Disbursed);

									LenderReferralBonusUpdated referralBonusDetails = new LenderReferralBonusUpdated();
									referralBonusDetails.setReferrerUserId(refereeInfo.getReferrerId());
									referralBonusDetails.setAmount(amount);
									referralBonusDetails.setRefereeUserId(refereeId);
									referralBonusDetails.setDealId(dealId);
									referralBonusDetails.setParticipatedOn(new Date());
									referralBonusDetails.setParticipatedAmount(disbursmentAmount);

									lenderReferralBonusUpdatedRepo.save(referralBonusDetails);

									Double sumOfAmountUnpaid = lenderReferenceDetailsRepo
											.findingAmountForUnpaid(refereeInfo.getRefereeId());
									if (sumOfAmountUnpaid != null) {
										refereeInfo.setAmount(sumOfAmountUnpaid);
										lenderReferenceDetailsRepo.save(refereeInfo);
									} else {
										refereeInfo.setAmount(0.0);
										lenderReferenceDetailsRepo.save(refereeInfo);
									}

								}

							}
							logger.info("Reference Amount Ends................................");

						}

					}

				}

			}

		} catch (Exception e) {
			logger.info("Exception in  dealLevelDisbursment Method");
		}
		dealLevelResponseDto.setDealId(dealId);
		dealLevelResponseDto.setStatus("updated");
		return dealLevelResponseDto;

	}

	@Override
	public DealLevelLoanEmiCardInformation getDealBasedLoanEmiDetails(int userId, int dealId) {
		DealLevelLoanEmiCardInformation dealLevelLoanEmiCardInformation = new DealLevelLoanEmiCardInformation();
		OxyBorrowersDealsInformation dealInfomation = oxyBorrowersDealsInformationRepo.findById(dealId).get();
		if (dealInfomation != null) {
			OxyLendersAcceptedDeals lendersAcceptedDeals = oxyLendersAcceptedDealsRepo
					.toCheckUserAlreadyInvoledInDeal(userId, dealId);
			if (lendersAcceptedDeals != null) {
				Double paticipatedAmount = lendersAcceptedDeals.getParticipatedAmount();
				Double rateOfInterest = lendersAcceptedDeals.getRateofinterest();
				Integer duration = dealInfomation.getDuration();
				if (dealInfomation.getLoanActiveDate() == null) {
					throw new OperationNotAllowedException("Interest start date not given to this deal",
							ErrorCodes.USER_NOT_FOUND);
				}
				String loanActiveDate = dealInfomation.getLoanActiveDate().toString();

				String lenderReturnType = lendersAcceptedDeals.getLenderReturnsType().toString();
				String remarks = null;
				if (dealId == 1) {
					remarks = "DEAL NADERGUL%";
				} else {
					remarks = dealInfomation.getDealName() + "%";
				}
				String participatedDate = expectedDateFormat.format(lendersAcceptedDeals.getReceivedOn());
				String participatedDateRequired = null;
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
				try {
					participatedDateRequired = sdf2.format(sdf.parse(participatedDate));

				} catch (java.text.ParseException e) {

					e.printStackTrace();
				}
				dealLevelLoanEmiCardInformation.setDealLevelLoanEmiCard(getLenderDealBasedCalcualtion(loanActiveDate,
						duration, dealId, lenderReturnType, paticipatedAmount, rateOfInterest, remarks, userId,
						participatedDateRequired, participatedDate));

			}

			dealLevelLoanEmiCardInformation
					.setInterestStartDate(expectedDateFormat.format(dealInfomation.getLoanActiveDate()));
		}
		return dealLevelLoanEmiCardInformation;

	}

	public List<LenderLoanDetailsResponseDto> getLenderDealBasedCalcualtion(String loanActiveDate, int duration,
			int dealId, String lenderReturnType, double paticipatedAmount, double rateOfInterest, String remarks,
			int userId, String participatedDateRequired, String participatedDate) {
		String nextDate1 = loanActiveDate;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = formatter.parse(loanActiveDate);

		} catch (ParseException e) {
			e.printStackTrace();

		}
		List<LenderLoanDetailsResponseDto> listOfLoanEmiCard = new ArrayList<LenderLoanDetailsResponseDto>();
		double interestAmount = 0;
		OxyBorrowersDealsInformation dealInfomation = oxyBorrowersDealsInformationRepo.findById(dealId).get();
		// String fundsStartDate =
		// expectedDateFormat.format(dealInfomation.getFundsAcceptanceStartDate());
		String fundsStartDate = expectedDateFormat.format(dealInfomation.getLoanActiveDate());
		String fundsStartDateRequired = null;
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		try {
			fundsStartDateRequired = sdf2.format(sdf1.parse(fundsStartDate));

		} catch (java.text.ParseException e) {

			e.printStackTrace();
		}
		LocalDate dateBefore = LocalDate.parse(fundsStartDateRequired);
		LocalDate dateAfter = LocalDate.parse(participatedDateRequired);
		long noOfDaysBetween = ChronoUnit.DAYS.between(dateAfter, dateBefore);

		int firstInterestCalculationDays = 0;
		int days = (int) noOfDaysBetween;

		String dateType1[] = participatedDateRequired.split("-");
		int yearForPaticipationStart = Integer.parseInt(dateType1[0]);
		int monthForPaticipationStart = Integer.parseInt(dateType1[1]);
		YearMonth yearMonth = YearMonth.of(yearForPaticipationStart, monthForPaticipationStart);
		int daysInMonthForParticipationDate = yearMonth.lengthOfMonth();

		if (days > 0) {
			if (daysInMonthForParticipationDate > 30) {
				firstInterestCalculationDays = days - 2; // previous -1
			} else if (daysInMonthForParticipationDate == 29) {
				firstInterestCalculationDays = days; // previous +1
			} else if (daysInMonthForParticipationDate == 28) {
				firstInterestCalculationDays = days + 1; // previous +2
			} else {
				if (lenderReturnType.equalsIgnoreCase("YEARLY")) {

					firstInterestCalculationDays = days - 2;

				} else {
					firstInterestCalculationDays = days - 1;
				}
			}
		}
		double singleDayInterest = 0.0;
		OxyBorrowersDealsInformation oxyBorrowersDeals = oxyBorrowersDealsInformationRepo.toGetCalculationType(dealId);
		if (dealInfomation.getDealType().equals(OxyBorrowersDealsInformation.DealType.NORMAL)) {
			if (oxyBorrowersDeals != null) {
				interestAmount = Math.round((paticipatedAmount * rateOfInterest) / 100d);
				// singleDayInterest = interestAmount / daysInMonth;
				singleDayInterest = interestAmount / 30;
			} else {

				interestAmount = Math.round((paticipatedAmount * (rateOfInterest / 12)) / 100d);
				// singleDayInterest = interestAmount / daysInMonth;
				singleDayInterest = interestAmount / 30;
			}
		} else {
			if (oxyBorrowersDeals != null) {
				interestAmount = Math.round((paticipatedAmount * rateOfInterest) / 100d);
				// singleDayInterest = interestAmount / daysInMonth;
				singleDayInterest = interestAmount / 30;

			} else {
				if (lenderReturnType.equals(OxyLendersAcceptedDeals.LenderReturnsType.MONTHLY.toString())) {
					interestAmount = Math.round(paticipatedAmount * (rateOfInterest / 12)) / 100d;
					// singleDayInterest = interestAmount / daysInMonth;
					singleDayInterest = interestAmount / 30;
				} else {
					interestAmount = Math.round(paticipatedAmount * (rateOfInterest / 12) / 100d);
					// singleDayInterest = interestAmount / daysInMonth;
					singleDayInterest = interestAmount / 30;
				}
			}
		}
		Integer quartlyValue = 0;
		Integer halflyValue = 0;
		Integer yearlyValue = 0;
		Integer endofdealValue = 0;
		Integer countValueForQuartely = 0;
		Integer countValueForHalfly = 0;
		Integer countValueForYearly = 0;
		Integer countValueForEnd = 0;
		Double lenderParticipationUpdatedInfo = lendersPaticipationUpdationRepo
				.getSumOfLenderUpdatedAmountToDeal(userId, dealId);
		Date currentDate = null;
		try {
			currentDate = formatter.parse(loanActiveDate);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Calendar c2 = Calendar.getInstance();
		c2.setTime(currentDate);
		for (int i = 1; i <= duration; i++) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c1 = Calendar.getInstance();
			int year = 0;
			int month = 0;
			int date1 = 0;
			if (lenderReturnType == "ENDOFTHEDEAL") {

				String dateParts[] = nextDate1.split("-");
				year = Integer.parseInt(dateParts[0]);

				month = Integer.parseInt(dateParts[1]);

				date1 = Integer.parseInt(dateParts[2]);
			}

			LenderLoanDetailsResponseDto lenderLoanDetailsResponseDto = new LenderLoanDetailsResponseDto();
			List<LenderParticipationUpdatedInfo> totalUpdatedInfo = new ArrayList<LenderParticipationUpdatedInfo>();
			if (lenderReturnType == "MONTHLY") {
				Double pricipalAmountForMonthly = 0.0;
				logger.info(date);

				String emiReceivingDate = sdf1.format(c2.getTime());
				// String emiReceivingDate = expectedDateFormat.format(date);

				String s1[] = emiReceivingDate.split("/");
				String monthAndYear = s1[1] + "/" + s1[2];
				logger.info(monthAndYear);
				lenderLoanDetailsResponseDto.setDate(sdf.format(c2.getTime()));
				List<LendersPaticipationUpdation> pativipatedAddInfo = lendersPaticipationUpdationRepo
						.getLenderAddedAreUpdate(userId, dealId, monthAndYear);

				Double totalInterest = 0.0;

				if (i == 1) {
					totalInterest = firstInterestCalculationDays * singleDayInterest;
					if (pativipatedAddInfo.isEmpty()) {

						lenderLoanDetailsResponseDto
								.setFirstParticipationInterest(firstInterestCalculationDays * singleDayInterest);
						lenderLoanDetailsResponseDto.setFirstParticipationAmount(paticipatedAmount);
						lenderLoanDetailsResponseDto
								.setDifferenceInDaysForFirstParticipation(firstInterestCalculationDays);
						lenderLoanDetailsResponseDto.setInterestAmount((double) Math.round(totalInterest));
					} else {
						LenderTotalParticipationInterestDetails lenderTotalParticipationInterestDetails = getPaticipationUpdatedInfo(
								monthAndYear, emiReceivingDate, dealId, rateOfInterest, lenderReturnType, totalInterest,
								userId, i);
						if (lenderTotalParticipationInterestDetails != null) {
							List<LenderParticipationUpdatedInfo> paticipatedUpdatedInfo = lenderTotalParticipationInterestDetails
									.getListOfParticipationUpdated();
							LenderParticipationUpdatedInfo lenderFirstParticipation = new LenderParticipationUpdatedInfo();
							lenderFirstParticipation.setDifferenceInDays(firstInterestCalculationDays);
							lenderFirstParticipation.setAmount(paticipatedAmount);
							lenderFirstParticipation.setInterestAmount((double) Math.round(totalInterest));
							lenderFirstParticipation.setUpatedDate(participatedDate);
							paticipatedUpdatedInfo.add(lenderFirstParticipation);

							lenderLoanDetailsResponseDto.setListOfPaticipatedInfo(paticipatedUpdatedInfo);
							if (lenderTotalParticipationInterestDetails.getInterestAmount() > totalInterest) {
								lenderLoanDetailsResponseDto.setInterestAmount((double) Math
										.round(lenderTotalParticipationInterestDetails.getInterestAmount()));
							} else {
								lenderLoanDetailsResponseDto.setInterestAmount((double) Math.round(totalInterest));
							}
						}
						lenderLoanDetailsResponseDto.setFirstParticipationAmount(paticipatedAmount);
						lenderLoanDetailsResponseDto
								.setDifferenceInDaysForFirstParticipation(firstInterestCalculationDays);
						lenderLoanDetailsResponseDto.setFirstParticipationInterest(totalInterest);
					}
				} else {
					LenderTotalParticipationInterestDetails lenderTotalParticipationInterestDetails = getPaticipationUpdatedInfo(
							monthAndYear, emiReceivingDate, dealId, rateOfInterest, lenderReturnType, totalInterest,
							userId, i);
					if (lenderTotalParticipationInterestDetails != null) {
						List<LenderParticipationUpdatedInfo> paticipatedUpdatedInfo = lenderTotalParticipationInterestDetails
								.getListOfParticipationUpdated();
						LenderParticipationUpdatedInfo lenderFirstParticipation = new LenderParticipationUpdatedInfo();
						lenderFirstParticipation.setDifferenceInDays(30);
						lenderFirstParticipation.setAmount(paticipatedAmount);
						lenderFirstParticipation.setInterestAmount((double) Math.round(interestAmount));
						lenderFirstParticipation.setUpatedDate(participatedDate);
						paticipatedUpdatedInfo.add(lenderFirstParticipation);

						lenderLoanDetailsResponseDto.setListOfPaticipatedInfo(paticipatedUpdatedInfo);

						lenderLoanDetailsResponseDto.setInterestAmount((double) Math
								.round(lenderTotalParticipationInterestDetails.getInterestAmount() + interestAmount));

					}

					lenderLoanDetailsResponseDto.setFirstParticipationAmount(paticipatedAmount);
					lenderLoanDetailsResponseDto.setDifferenceInDaysForFirstParticipation(30);
					lenderLoanDetailsResponseDto.setFirstParticipationInterest(interestAmount);

				}

				if (i == duration) {
					if (lenderParticipationUpdatedInfo != null) {
						pricipalAmountForMonthly = paticipatedAmount + lenderParticipationUpdatedInfo;
					} else {

						pricipalAmountForMonthly = paticipatedAmount;
					}
				}

				LenderLoanDetailsResponseDto statusAndAmountResponse = lenderInterestStatusAndAmount(emiReceivingDate,
						participatedDate, userId, dealId, monthAndYear);
				if (statusAndAmountResponse != null) {
					lenderLoanDetailsResponseDto.setAmountRecevied(statusAndAmountResponse.getAmountRecevied());
					lenderLoanDetailsResponseDto.setPaymentStatus(statusAndAmountResponse.getPaymentStatus());
					lenderLoanDetailsResponseDto.setInterestPaidDate(statusAndAmountResponse.getInterestPaidDate());
					if (statusAndAmountResponse.getPaymentStatus().equalsIgnoreCase("Future")) {
						lenderLoanDetailsResponseDto.setAutomaticInterestCalculation(true);
					}
				}

				if (pricipalAmountForMonthly > 0) {
					lenderLoanDetailsResponseDto.setPrincipalAmount(pricipalAmountForMonthly);
				}
				lenderLoanDetailsResponseDto.setSno(i);
				lenderLoanDetailsResponseDto.setLenderReturnType(lenderReturnType);
				lenderLoanDetailsResponseDto.setRateOfInterest(rateOfInterest);
				listOfLoanEmiCard.add(lenderLoanDetailsResponseDto);
			} else {

				if (lenderReturnType == "QUARTELY") {
					String monthAndYear = null;
					Double pricipalAmountForQuartely = 0.0;
					Double interestAmountToQuartely = 0.0;
					Double totalInterest = 0.0;

					if (i % 3 == 0) {

						countValueForQuartely = countValueForQuartely + 1;
						quartlyValue = quartlyValue + 3;
						if (countValueForQuartely == 1) {
							Double interestForTwoMonths = interestAmount * 2;
							interestAmountToQuartely = interestForTwoMonths
									+ (firstInterestCalculationDays * singleDayInterest);
							lenderLoanDetailsResponseDto
									.setDifferenceInDaysForFirstParticipation(firstInterestCalculationDays - 1);
						} else {
							interestAmountToQuartely = interestAmount * 3;
							lenderLoanDetailsResponseDto.setDifferenceInDaysForFirstParticipation(30 * 3);
						}

						if (i == duration) {
							if (lenderParticipationUpdatedInfo != null) {
								pricipalAmountForQuartely = paticipatedAmount + lenderParticipationUpdatedInfo;
							} else {

								pricipalAmountForQuartely = paticipatedAmount;
							}

						}

						String emiReceivingDate = expectedDateFormat.format(date);
						lenderLoanDetailsResponseDto.setDate(emiReceivingDate);
						String s1[] = emiReceivingDate.split("/");
						monthAndYear = s1[1] + "/" + s1[2];
						int yearlyYear = Integer.parseInt(s1[2]);
						int yearlyMonth = Integer.parseInt(s1[1]);
						int yearlyDate = Integer.parseInt(s1[0]);
						Calendar prevYear = Calendar.getInstance();

						prevYear.set(yearlyYear, yearlyMonth - 1, yearlyDate);

						prevYear.add(Calendar.MONTH, 3);
						List<LendersPaticipationUpdation> pativipatedAddInfo = lendersPaticipationUpdationRepo
								.getLenderAddedAreUpdate(userId, dealId, monthAndYear);
						if (pativipatedAddInfo.isEmpty()) {
							lenderLoanDetailsResponseDto
									.setInterestAmount((double) Math.round(interestAmountToQuartely));
							lenderLoanDetailsResponseDto.setFirstParticipationInterest(interestAmountToQuartely);
							lenderLoanDetailsResponseDto.setFirstParticipationAmount(paticipatedAmount);

						} else {
							LenderTotalParticipationInterestDetails lenderTotalParticipationInterestDetails = getPaticipationUpdatedInfo(
									monthAndYear, emiReceivingDate, dealId, rateOfInterest, lenderReturnType,
									totalInterest, userId, i);
							if (lenderTotalParticipationInterestDetails != null) {
								List<LenderParticipationUpdatedInfo> paticipatedUpdatedInfo = lenderTotalParticipationInterestDetails
										.getListOfParticipationUpdated();
								LenderParticipationUpdatedInfo lenderFirstParticipation = new LenderParticipationUpdatedInfo();
								lenderFirstParticipation.setDifferenceInDays(
										lenderLoanDetailsResponseDto.getDifferenceInDaysForFirstParticipation());
								lenderFirstParticipation.setAmount(paticipatedAmount);
								lenderFirstParticipation
										.setInterestAmount((double) Math.round(interestAmountToQuartely));
								lenderFirstParticipation.setUpatedDate(participatedDate);
								paticipatedUpdatedInfo.add(lenderFirstParticipation);

								lenderLoanDetailsResponseDto.setListOfPaticipatedInfo(
										lenderTotalParticipationInterestDetails.getListOfParticipationUpdated());
								lenderLoanDetailsResponseDto
										.setInterestAmount((double) Math.round(interestAmountToQuartely
												+ lenderTotalParticipationInterestDetails.getInterestAmount()));
							}
							lenderLoanDetailsResponseDto.setFirstParticipationAmount(paticipatedAmount);

							lenderLoanDetailsResponseDto.setFirstParticipationInterest(interestAmountToQuartely);
						}

						if (pricipalAmountForQuartely > 0) {
							lenderLoanDetailsResponseDto.setPrincipalAmount(pricipalAmountForQuartely);
						}
						LenderLoanDetailsResponseDto statusAndAmountResponse = lenderInterestStatusAndAmount(
								emiReceivingDate, participatedDate, userId, dealId, monthAndYear);
						if (statusAndAmountResponse != null) {
							lenderLoanDetailsResponseDto.setAmountRecevied(statusAndAmountResponse.getAmountRecevied());
							lenderLoanDetailsResponseDto.setPaymentStatus(statusAndAmountResponse.getPaymentStatus());
							lenderLoanDetailsResponseDto
									.setInterestPaidDate(statusAndAmountResponse.getInterestPaidDate());

							if (statusAndAmountResponse.getPaymentStatus().equalsIgnoreCase("Future")) {
								lenderLoanDetailsResponseDto.setAutomaticInterestCalculation(true);
							}
						}
						lenderLoanDetailsResponseDto.setSno(countValueForQuartely);
						listOfLoanEmiCard.add(lenderLoanDetailsResponseDto);
						nextDate1 = sdf.format(prevYear.getTime());

						try {
							date = sdf.parse(nextDate1);
						} catch (ParseException e) {

							e.printStackTrace();

						}
					} else {
						if (i == duration) {
							countValueForQuartely = countValueForQuartely + 1;
							int pendingEmiInterest = duration - quartlyValue;
							String emiReceivingDate = expectedDateFormat.format(date);
							lenderLoanDetailsResponseDto.setDate(emiReceivingDate);
							if (lenderParticipationUpdatedInfo != null) {
								pricipalAmountForQuartely = ((paticipatedAmount + lenderParticipationUpdatedInfo)
										+ (interestAmount * pendingEmiInterest));
							} else {

								pricipalAmountForQuartely = (paticipatedAmount + (interestAmount * pendingEmiInterest));
							}

							if (pricipalAmountForQuartely > 0) {
								lenderLoanDetailsResponseDto.setPrincipalAmount(pricipalAmountForQuartely);
							}

							LenderLoanDetailsResponseDto statusAndAmountResponse = lenderInterestStatusAndAmount(
									emiReceivingDate, participatedDate, userId, dealId, monthAndYear);
							if (statusAndAmountResponse != null) {
								lenderLoanDetailsResponseDto
										.setAmountRecevied(statusAndAmountResponse.getAmountRecevied());
								lenderLoanDetailsResponseDto
										.setPaymentStatus(statusAndAmountResponse.getPaymentStatus());
								lenderLoanDetailsResponseDto
										.setInterestPaidDate(statusAndAmountResponse.getInterestPaidDate());
								if (statusAndAmountResponse.getPaymentStatus().equalsIgnoreCase("Future")) {
									lenderLoanDetailsResponseDto.setAutomaticInterestCalculation(true);
								}
							}
							lenderLoanDetailsResponseDto.setSno(countValueForQuartely);
							listOfLoanEmiCard.add(lenderLoanDetailsResponseDto);
						}

					}
					lenderLoanDetailsResponseDto.setLenderReturnType(lenderReturnType);
					lenderLoanDetailsResponseDto.setRateOfInterest(rateOfInterest);
				}

				if (lenderReturnType == "HALFLY") {
					String monthAndYear = null;
					Double totalInterest = 0.0;
					Double pricipalAmountForHalfly = 0.0;
					Double interestAmountToHalfly = 0.0;
					if (i % 6 == 0) {

						countValueForHalfly = countValueForHalfly + 1;
						interestAmountToHalfly = interestAmount * 6;

						halflyValue = halflyValue + 6;
						if (i == duration) {
							if (lenderParticipationUpdatedInfo != null) {
								pricipalAmountForHalfly = paticipatedAmount + lenderParticipationUpdatedInfo;
							} else {

								pricipalAmountForHalfly = paticipatedAmount;
							}

						}
						String emiReceivingDate = expectedDateFormat.format(date);
						lenderLoanDetailsResponseDto.setDate(emiReceivingDate);

						String s1[] = emiReceivingDate.split("/");
						monthAndYear = s1[1] + "/" + s1[2];
						int yearlyYear = Integer.parseInt(s1[2]);
						int yearlyMonth = Integer.parseInt(s1[1]);
						int yearlyDate = Integer.parseInt(s1[0]);
						Calendar prevYear = Calendar.getInstance();

						prevYear.set(yearlyYear, yearlyMonth - 1, yearlyDate);

						prevYear.add(Calendar.MONTH, 6);

						lenderLoanDetailsResponseDto.setDate(emiReceivingDate);
						List<LendersPaticipationUpdation> pativipatedAddInfo = lendersPaticipationUpdationRepo
								.getLenderAddedAreUpdate(userId, dealId, monthAndYear);
						if (countValueForHalfly == 1) {
							Double interestForFiveMonths = interestAmount * 5;
							interestAmountToHalfly = interestForFiveMonths
									+ (firstInterestCalculationDays * singleDayInterest);
							lenderLoanDetailsResponseDto
									.setDifferenceInDaysForFirstParticipation(firstInterestCalculationDays - 1);
						} else {
							lenderLoanDetailsResponseDto.setDifferenceInDaysForFirstParticipation(30 * 6);
						}

						if (pativipatedAddInfo.isEmpty()) {
							lenderLoanDetailsResponseDto.setInterestAmount((double) Math.round(interestAmountToHalfly));
							lenderLoanDetailsResponseDto.setFirstParticipationInterest(interestAmountToHalfly);
							lenderLoanDetailsResponseDto.setFirstParticipationAmount(paticipatedAmount);

						} else {
							LenderTotalParticipationInterestDetails lenderTotalParticipationInterestDetails = getPaticipationUpdatedInfo(
									monthAndYear, emiReceivingDate, dealId, rateOfInterest, lenderReturnType,
									totalInterest, userId, i);
							if (lenderTotalParticipationInterestDetails != null) {
								List<LenderParticipationUpdatedInfo> paticipatedUpdatedInfo = lenderTotalParticipationInterestDetails
										.getListOfParticipationUpdated();
								LenderParticipationUpdatedInfo lenderFirstParticipation = new LenderParticipationUpdatedInfo();
								lenderFirstParticipation.setDifferenceInDays(
										lenderLoanDetailsResponseDto.getDifferenceInDaysForFirstParticipation());
								lenderFirstParticipation.setAmount(paticipatedAmount);
								lenderFirstParticipation.setInterestAmount((double) Math.round(interestAmountToHalfly));
								lenderFirstParticipation.setUpatedDate(participatedDate);
								paticipatedUpdatedInfo.add(lenderFirstParticipation);

								lenderLoanDetailsResponseDto.setListOfPaticipatedInfo(
										lenderTotalParticipationInterestDetails.getListOfParticipationUpdated());
								lenderLoanDetailsResponseDto
										.setInterestAmount((double) Math.round(interestAmountToHalfly
												+ lenderTotalParticipationInterestDetails.getInterestAmount()));
							}
							lenderLoanDetailsResponseDto.setFirstParticipationAmount(paticipatedAmount);

							lenderLoanDetailsResponseDto.setFirstParticipationInterest(interestAmountToHalfly);
						}

						if (pricipalAmountForHalfly > 0) {
							lenderLoanDetailsResponseDto.setPrincipalAmount(pricipalAmountForHalfly);
						}
						LenderLoanDetailsResponseDto statusAndAmountResponse = lenderInterestStatusAndAmount(
								emiReceivingDate, participatedDate, userId, dealId, monthAndYear);
						if (statusAndAmountResponse != null) {
							lenderLoanDetailsResponseDto.setAmountRecevied(statusAndAmountResponse.getAmountRecevied());
							lenderLoanDetailsResponseDto.setPaymentStatus(statusAndAmountResponse.getPaymentStatus());
							lenderLoanDetailsResponseDto
									.setInterestPaidDate(statusAndAmountResponse.getInterestPaidDate());
							if (statusAndAmountResponse.getPaymentStatus().equalsIgnoreCase("Future")) {
								lenderLoanDetailsResponseDto.setAutomaticInterestCalculation(true);
							}
						}
						lenderLoanDetailsResponseDto.setSno(countValueForHalfly);
						listOfLoanEmiCard.add(lenderLoanDetailsResponseDto);
						nextDate1 = sdf.format(prevYear.getTime());

						try {
							date = sdf.parse(nextDate1);
						} catch (ParseException e) {

							e.printStackTrace();

						}

					} else {
						if (i == duration) {
							countValueForHalfly = countValueForHalfly + 1;
							int pendingEmiInterest = duration - halflyValue;
							String emiReceivingDate = expectedDateFormat.format(date);
							lenderLoanDetailsResponseDto.setDate(emiReceivingDate);
							if (lenderParticipationUpdatedInfo != null) {
								pricipalAmountForHalfly = ((paticipatedAmount + lenderParticipationUpdatedInfo)
										+ (interestAmount * pendingEmiInterest));
							} else {

								pricipalAmountForHalfly = (paticipatedAmount + (interestAmount * pendingEmiInterest));
							}

							if (pricipalAmountForHalfly > 0) {
								lenderLoanDetailsResponseDto.setPrincipalAmount(pricipalAmountForHalfly);
							}
							LenderLoanDetailsResponseDto statusAndAmountResponse = lenderInterestStatusAndAmount(
									emiReceivingDate, participatedDate, userId, dealId, monthAndYear);
							if (statusAndAmountResponse != null) {
								lenderLoanDetailsResponseDto
										.setAmountRecevied(statusAndAmountResponse.getAmountRecevied());
								lenderLoanDetailsResponseDto
										.setPaymentStatus(statusAndAmountResponse.getPaymentStatus());
								lenderLoanDetailsResponseDto
										.setInterestPaidDate(statusAndAmountResponse.getInterestPaidDate());
								if (statusAndAmountResponse.getPaymentStatus().equalsIgnoreCase("Future")) {
									lenderLoanDetailsResponseDto.setAutomaticInterestCalculation(true);
								}
							}
							lenderLoanDetailsResponseDto.setSno(countValueForHalfly);
							listOfLoanEmiCard.add(lenderLoanDetailsResponseDto);
						}
					}
					lenderLoanDetailsResponseDto.setLenderReturnType(lenderReturnType);
					lenderLoanDetailsResponseDto.setRateOfInterest(rateOfInterest);
				}

				if (lenderReturnType == "YEARLY") {
					String monthAndYear = null;
					Double totalInterest = 0.0;
					Double pricipalAmountForYearly = 0.0;
					Double interestAmountForYearly = 0.0;
					if (i % 12 == 0) {
						countValueForYearly = countValueForYearly + 1;
						interestAmountForYearly = interestAmount * 12;

						yearlyValue = yearlyValue + 12;
						if (i == duration) {
							if (lenderParticipationUpdatedInfo != null) {
								pricipalAmountForYearly = paticipatedAmount + lenderParticipationUpdatedInfo;
							} else {

								pricipalAmountForYearly = paticipatedAmount;
							}

						}

						String emiReceivingDate = expectedDateFormat.format(date);

						lenderLoanDetailsResponseDto.setDate(emiReceivingDate);

						String s1[] = emiReceivingDate.split("/");
						monthAndYear = s1[1] + "/" + s1[2];
						int yearlyYear = Integer.parseInt(s1[2]);
						int yearlyMonth = Integer.parseInt(s1[1]);
						int yearlyDate = Integer.parseInt(s1[0]);
						Calendar prevYear = Calendar.getInstance();

						prevYear.set(yearlyYear, yearlyMonth - 1, yearlyDate);

						prevYear.add(Calendar.YEAR, 1);

						lenderLoanDetailsResponseDto.setDate(emiReceivingDate);
						List<LendersPaticipationUpdation> pativipatedAddInfo = lendersPaticipationUpdationRepo
								.getLenderAddedAreUpdate(userId, dealId, monthAndYear);
						if (countValueForYearly == 1) {

							int differenceInDaysCount = 0;
							int firstMonthCount = 0;
							String Paticipateddate[] = participatedDateRequired.split("-");
							String fundsStart = null;
							try {
								fundsStart = sdf2.format(sdf1.parse(
										expectedDateFormat.format(dealInfomation.getFundsAcceptanceStartDate())));

							} catch (java.text.ParseException e) {

								e.printStackTrace();
							}
							LocalDate before = LocalDate.parse(participatedDateRequired);
							LocalDate after = LocalDate.parse(fundsStart);
							long noOfDays = ChronoUnit.DAYS.between(after, before);

							Period diff = Period.between(LocalDate.parse(participatedDateRequired).withDayOfMonth(1),
									LocalDate.parse(fundsStartDateRequired).withDayOfMonth(1));
							if (noOfDays < 30) {
								firstMonthCount = 30 - (int) noOfDays;

							} else {
								firstMonthCount = 0;
							}
							// firstMonthCount = 30 - Integer.parseInt(Paticipateddate[2]);
							if (diff.getYears() == 1 && diff.getMonths() == 0) {
								if (firstMonthCount >= 1) {
									// differenceInDaysCount = firstMonthCount - 1;
									// differenceInDaysCount = differenceInDaysCount + (11 * 30);
									if (participatedDateRequired.equals(fundsStart)) {
										differenceInDaysCount = 359;
									} else {
										differenceInDaysCount = 360 - (int) noOfDays;
									}

								} else {
									differenceInDaysCount = differenceInDaysCount + (359);
								}
							} else if (diff.getYears() == 0 && diff.getMonths() >= 1) {
								if (firstMonthCount >= 1) {
									differenceInDaysCount = firstMonthCount - 1;
									differenceInDaysCount = differenceInDaysCount + ((diff.getMonths() - 1) * 30);
								} else {
									differenceInDaysCount = differenceInDaysCount + ((diff.getMonths() * 30) - 2);
								}

							} else if (diff.getYears() == 1 && diff.getMonths() >= 1) {
								if (firstMonthCount >= 1) {
									differenceInDaysCount = firstMonthCount - 1;
									differenceInDaysCount = differenceInDaysCount + (330);
								} else {
									differenceInDaysCount = differenceInDaysCount + (359); //
								}
							} else {
								differenceInDaysCount = 0;
							}

							interestAmountForYearly = (differenceInDaysCount * singleDayInterest);
							lenderLoanDetailsResponseDto
									.setDifferenceInDaysForFirstParticipation(differenceInDaysCount);
						} else {
							lenderLoanDetailsResponseDto.setDifferenceInDaysForFirstParticipation(30 * 12);
						}
						if (pativipatedAddInfo.isEmpty()) {
							lenderLoanDetailsResponseDto
									.setInterestAmount((double) Math.round(interestAmountForYearly));
							lenderLoanDetailsResponseDto.setFirstParticipationInterest(interestAmountForYearly);
							lenderLoanDetailsResponseDto.setFirstParticipationAmount(paticipatedAmount);

						} else {
							LenderTotalParticipationInterestDetails lenderTotalParticipationInterestDetails = getPaticipationUpdatedInfo(
									monthAndYear, emiReceivingDate, dealId, rateOfInterest, lenderReturnType,
									totalInterest, userId, i);
							if (lenderTotalParticipationInterestDetails != null) {
								List<LenderParticipationUpdatedInfo> paticipatedUpdatedInfo = lenderTotalParticipationInterestDetails
										.getListOfParticipationUpdated();
								LenderParticipationUpdatedInfo lenderFirstParticipation = new LenderParticipationUpdatedInfo();
								lenderFirstParticipation.setDifferenceInDays(
										lenderLoanDetailsResponseDto.getDifferenceInDaysForFirstParticipation());
								lenderFirstParticipation.setAmount(paticipatedAmount);
								lenderFirstParticipation
										.setInterestAmount((double) Math.round(interestAmountForYearly));
								lenderFirstParticipation.setUpatedDate(participatedDate);
								paticipatedUpdatedInfo.add(lenderFirstParticipation);

								lenderLoanDetailsResponseDto.setListOfPaticipatedInfo(
										lenderTotalParticipationInterestDetails.getListOfParticipationUpdated());
								lenderLoanDetailsResponseDto
										.setInterestAmount((double) Math.round(interestAmountForYearly
												+ lenderTotalParticipationInterestDetails.getInterestAmount()));
							}
							lenderLoanDetailsResponseDto.setFirstParticipationAmount(paticipatedAmount);

							lenderLoanDetailsResponseDto.setFirstParticipationInterest(interestAmountForYearly);
						}

						if (pricipalAmountForYearly > 0) {
							lenderLoanDetailsResponseDto.setPrincipalAmount(pricipalAmountForYearly);
						}
						LenderLoanDetailsResponseDto statusAndAmountResponse = lenderInterestStatusAndAmount(
								emiReceivingDate, participatedDate, userId, dealId, monthAndYear);
						if (statusAndAmountResponse != null) {
							lenderLoanDetailsResponseDto.setAmountRecevied(statusAndAmountResponse.getAmountRecevied());
							lenderLoanDetailsResponseDto
									.setInterestPaidDate(statusAndAmountResponse.getInterestPaidDate());
							lenderLoanDetailsResponseDto.setPaymentStatus(statusAndAmountResponse.getPaymentStatus());
							if (statusAndAmountResponse.getPaymentStatus().equalsIgnoreCase("Future")) {
								lenderLoanDetailsResponseDto.setAutomaticInterestCalculation(true);
							}
						}
						lenderLoanDetailsResponseDto.setSno(countValueForYearly);
						listOfLoanEmiCard.add(lenderLoanDetailsResponseDto);
						nextDate1 = sdf.format(prevYear.getTime());

						try {
							date = sdf.parse(nextDate1);
						} catch (ParseException e) {

							e.printStackTrace();

						}
					} else {
						if (i == duration) {
							countValueForYearly = countValueForYearly + 1;
							int pendingEmiInterest = duration - yearlyValue;
							String emiReceivingDate = expectedDateFormat.format(date);

							lenderLoanDetailsResponseDto.setDate(emiReceivingDate);
							if (lenderParticipationUpdatedInfo != null) {
								pricipalAmountForYearly = ((paticipatedAmount + lenderParticipationUpdatedInfo)
										+ (interestAmount * pendingEmiInterest));
							} else {

								pricipalAmountForYearly = (paticipatedAmount + (interestAmount * pendingEmiInterest));
							}

							if (pricipalAmountForYearly > 0) {
								lenderLoanDetailsResponseDto.setPrincipalAmount(pricipalAmountForYearly);
							}
							LenderLoanDetailsResponseDto statusAndAmountResponse = lenderInterestStatusAndAmount(
									emiReceivingDate, participatedDate, userId, dealId, monthAndYear);
							if (statusAndAmountResponse != null) {
								lenderLoanDetailsResponseDto
										.setAmountRecevied(statusAndAmountResponse.getAmountRecevied());
								lenderLoanDetailsResponseDto
										.setPaymentStatus(statusAndAmountResponse.getPaymentStatus());
								lenderLoanDetailsResponseDto
										.setInterestPaidDate(statusAndAmountResponse.getInterestPaidDate());
								if (statusAndAmountResponse.getPaymentStatus().equalsIgnoreCase("Future")) {
									lenderLoanDetailsResponseDto.setAutomaticInterestCalculation(true);
								}
							}
							lenderLoanDetailsResponseDto.setSno(countValueForYearly);
							listOfLoanEmiCard.add(lenderLoanDetailsResponseDto);

						}
					}
					lenderLoanDetailsResponseDto.setLenderReturnType(lenderReturnType);
					lenderLoanDetailsResponseDto.setRateOfInterest(rateOfInterest);

				}
				if (lenderReturnType == "ENDOFTHEDEAL") {
					String monthAndYear = null;
					Double totalInterest = 0.0;
					Double pricipalAmountForEndOfDeal = 0.0;
					Double interestAmountForEndOfDeal = 0.0;
					if (i % duration == 0) {
						countValueForEnd = countValueForEnd + 1;
						interestAmountForEndOfDeal = interestAmount * duration;

						endofdealValue = endofdealValue + duration;
						if (i == duration) {
							if (lenderParticipationUpdatedInfo != null) {
								pricipalAmountForEndOfDeal = paticipatedAmount + lenderParticipationUpdatedInfo;
							} else {

								pricipalAmountForEndOfDeal = paticipatedAmount;
							}

						}
						String emiReceivingDate = expectedDateFormat.format(date);
						String s1[] = emiReceivingDate.split("/");
						monthAndYear = s1[1] + "/" + s1[2];
						lenderLoanDetailsResponseDto.setDate(emiReceivingDate);
						List<LendersPaticipationUpdation> pativipatedAddInfo = lendersPaticipationUpdationRepo
								.getLenderAddedAreUpdate(userId, dealId, monthAndYear);

						lenderLoanDetailsResponseDto.setDate(emiReceivingDate);
						if (countValueForEnd == 1) {
							Double interestForEndOfDeal = interestAmount * (duration - 1);
							interestAmountForEndOfDeal = interestForEndOfDeal
									+ (firstInterestCalculationDays * singleDayInterest);
							lenderLoanDetailsResponseDto.setDifferenceInDaysForFirstParticipation(
									firstInterestCalculationDays + (30 * (duration - 1)));
						} else {
							lenderLoanDetailsResponseDto.setDifferenceInDaysForFirstParticipation(30 * (duration - 1));
						}
						if (pativipatedAddInfo.isEmpty()) {
							lenderLoanDetailsResponseDto
									.setInterestAmount((double) Math.round(interestAmountForEndOfDeal));
							lenderLoanDetailsResponseDto.setFirstParticipationInterest(interestAmountForEndOfDeal);
							lenderLoanDetailsResponseDto.setFirstParticipationAmount(paticipatedAmount);

						} else {
							LenderTotalParticipationInterestDetails lenderTotalParticipationInterestDetails = getPaticipationUpdatedInfo(
									monthAndYear, emiReceivingDate, dealId, rateOfInterest, lenderReturnType,
									totalInterest, userId, i);
							if (lenderTotalParticipationInterestDetails != null) {
								List<LenderParticipationUpdatedInfo> paticipatedUpdatedInfo = lenderTotalParticipationInterestDetails
										.getListOfParticipationUpdated();
								LenderParticipationUpdatedInfo lenderFirstParticipation = new LenderParticipationUpdatedInfo();
								lenderFirstParticipation.setDifferenceInDays(
										lenderLoanDetailsResponseDto.getDifferenceInDaysForFirstParticipation());
								lenderFirstParticipation.setAmount(paticipatedAmount);
								lenderFirstParticipation
										.setInterestAmount((double) Math.round(interestAmountForEndOfDeal));
								lenderFirstParticipation.setUpatedDate(participatedDate);
								paticipatedUpdatedInfo.add(lenderFirstParticipation);

								lenderLoanDetailsResponseDto.setListOfPaticipatedInfo(
										lenderTotalParticipationInterestDetails.getListOfParticipationUpdated());
								lenderLoanDetailsResponseDto
										.setInterestAmount((double) Math.round(interestAmountForEndOfDeal
												+ lenderTotalParticipationInterestDetails.getInterestAmount()));
							}
							lenderLoanDetailsResponseDto.setFirstParticipationAmount(paticipatedAmount);

							lenderLoanDetailsResponseDto.setFirstParticipationInterest(interestAmountForEndOfDeal);

						}
						if (pricipalAmountForEndOfDeal > 0) {
							lenderLoanDetailsResponseDto.setPrincipalAmount(pricipalAmountForEndOfDeal);
						}
						LenderLoanDetailsResponseDto statusAndAmountResponse = lenderInterestStatusAndAmount(
								emiReceivingDate, participatedDate, userId, dealId, monthAndYear);
						if (statusAndAmountResponse != null) {
							lenderLoanDetailsResponseDto.setAmountRecevied(statusAndAmountResponse.getAmountRecevied());
							lenderLoanDetailsResponseDto.setPaymentStatus(statusAndAmountResponse.getPaymentStatus());
							lenderLoanDetailsResponseDto
									.setInterestPaidDate(statusAndAmountResponse.getInterestPaidDate());
							if (statusAndAmountResponse.getPaymentStatus().equalsIgnoreCase("Future")) {
								lenderLoanDetailsResponseDto.setAutomaticInterestCalculation(true);
							}
						}
						lenderLoanDetailsResponseDto.setSno(countValueForEnd);
						listOfLoanEmiCard.add(lenderLoanDetailsResponseDto);

					} else {
						if (i == duration) {
							countValueForEnd = countValueForEnd + 1;
							int pendingEmiInterest = duration - endofdealValue;
							String emiReceivingDate = expectedDateFormat.format(date);
							lenderLoanDetailsResponseDto.setDate(emiReceivingDate);
							if (lenderParticipationUpdatedInfo != null) {
								pricipalAmountForEndOfDeal = ((paticipatedAmount + lenderParticipationUpdatedInfo)
										+ (interestAmount * pendingEmiInterest));
							} else {

								pricipalAmountForEndOfDeal = (paticipatedAmount
										+ (interestAmount * pendingEmiInterest));
							}

							if (pricipalAmountForEndOfDeal > 0) {
								lenderLoanDetailsResponseDto.setPrincipalAmount(pricipalAmountForEndOfDeal);
							}
							LenderLoanDetailsResponseDto statusAndAmountResponse = lenderInterestStatusAndAmount(
									emiReceivingDate, participatedDate, userId, dealId, monthAndYear);
							if (statusAndAmountResponse != null) {
								lenderLoanDetailsResponseDto
										.setAmountRecevied(statusAndAmountResponse.getAmountRecevied());
								lenderLoanDetailsResponseDto
										.setInterestPaidDate(statusAndAmountResponse.getInterestPaidDate());
								lenderLoanDetailsResponseDto
										.setPaymentStatus(statusAndAmountResponse.getPaymentStatus());
								if (statusAndAmountResponse.getPaymentStatus().equalsIgnoreCase("Future")) {
									lenderLoanDetailsResponseDto.setAutomaticInterestCalculation(true);
								}
							}
							lenderLoanDetailsResponseDto.setSno(countValueForEnd);
							listOfLoanEmiCard.add(lenderLoanDetailsResponseDto);
						}
					}
					lenderLoanDetailsResponseDto.setLenderReturnType(lenderReturnType);
					lenderLoanDetailsResponseDto.setRateOfInterest(rateOfInterest);
				}

			}
			if (lenderReturnType == "ENDOFTHEDEAL") {
				c1.set(year, month, date1);

				nextDate1 = sdf.format(c1.getTime());

				try {
					date = sdf.parse(nextDate1);
				} catch (ParseException e) {

					e.printStackTrace();

				}
			}
			if (lenderReturnType == "MONTHLY") {
				c2.add(Calendar.MONTH, 1);

			}

		}
		return listOfLoanEmiCard;

	}

	@Override
	public BorrowersDealsList getListOfDealsInformationForEquity(int userId, PaginationRequestDto pageRequestDto) {
		int pageNo = pageRequestDto.getPageNo();
		int pageSize = pageRequestDto.getPageSize();
		pageNo = (pageSize * (pageNo - 1));

		BorrowersDealsList borrowersDealsList = new BorrowersDealsList();
		String dealStatusForFrontEnd = null;
		String dealStatus = null;
		if (pageRequestDto.getDealType().equalsIgnoreCase("CLOSED")) {
			dealStatusForFrontEnd = "Achieved";
			dealStatus = "ACHIEVED";
		} else {
			dealStatusForFrontEnd = "Yet to be Achieved";
			dealStatus = "NOTATACHIEVED";
		}

		User user = userRepo.findById(userId).get();
		int groupId = 0;
		if (user.getLenderGroupId() == 0 || user.getLenderGroupId() == 7) {

			groupId = 0;
		} else {
			groupId = 3;

		}
		List<Object[]> listOfDeals = oxyBorrowersDealsInformationRepo.getListOfDealsInformationForEquityDeals(pageNo,
				pageSize, dealStatus, pageRequestDto.getDealName());

		Integer count = oxyBorrowersDealsInformationRepo.getListOfDealsInformationCountForEquityDeals(dealStatus,
				pageRequestDto.getDealName());
		Integer totalCount = 0;
		if (count != null) {
			totalCount = count;
		}
		List<BorrowersDealsResponseDto> borrowersDealsResponse = new ArrayList<BorrowersDealsResponseDto>();
		if (listOfDeals != null && !listOfDeals.isEmpty()) {
			Iterator it = listOfDeals.iterator();
			while (it.hasNext()) {
				Object[] e = (Object[]) it.next();
				BorrowersDealsResponseDto borrowersDealsInfo = new BorrowersDealsResponseDto();
				double dealAmount = Double.parseDouble(e[3] == null ? "0" : e[3].toString());
				int dealId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
				borrowersDealsInfo.setDealId(dealId);
				borrowersDealsInfo.setDealName(e[1] == null ? " " : e[1].toString());
				borrowersDealsInfo.setBorrowerName(e[2] == null ? " " : e[2].toString());
				borrowersDealsInfo.setBorrowerRateOfInterest(Double.parseDouble(e[6] == null ? "0" : e[6].toString()));
				borrowersDealsInfo.setFundsAcceptanceStartDate(e[4] == null ? " " : e[4].toString());
				borrowersDealsInfo.setFundsAcceptanceEndDate(e[5] == null ? " " : e[5].toString());
				borrowersDealsInfo.setDealAmount(dealAmount);
				borrowersDealsInfo.setDealLink(e[7] == null ? " " : e[7].toString());
				borrowersDealsInfo.setDuration(Integer.parseInt(e[8] == null ? "0" : e[8].toString()));
				borrowersDealsInfo.setWhatAppResponseUrl(e[9] == null ? " " : e[9].toString());
				borrowersDealsInfo.setWhatAppLinkSentToLendersGroup(e[10] == null ? " " : e[10].toString());
				borrowersDealsInfo.setLoanActiveDate(e[11] == null ? " " : e[11].toString());
				borrowersDealsInfo.setMessageSentToLenders(e[12] == null ? " " : e[12].toString());
				if (dealStatus.equalsIgnoreCase("ACHIEVED")) {
					borrowersDealsInfo.setLenderPaticipateStatus(false);
				} else {
					if (e[17].toString().equals(OxyBorrowersDealsInformation.ParticipationLenderType.ANY.toString())) {
						borrowersDealsInfo.setLenderPaticipateStatus(true);
					} else {
						List<OxyLendersAcceptedDeals> oxylendersAcceptedDealsList = oxyLendersAcceptedDealsRepo
								.findbyUserId(userId);

						if (oxylendersAcceptedDealsList != null && !oxylendersAcceptedDealsList.isEmpty()) {

							OxyLendersAcceptedDeals oxylendersAcceptedDeals = oxyLendersAcceptedDealsRepo
									.toCheckUserAlreadyInvoledInDeal(userId, dealId);

							if (oxylendersAcceptedDeals == null) {
								borrowersDealsInfo.setLenderPaticipateStatus(false);
							} else {
								borrowersDealsInfo.setLenderPaticipateStatus(true);
							}
						} else {
							borrowersDealsInfo.setLenderPaticipateStatus(true);
						}
					}

				}
				borrowersDealsInfo.setDealCreatedType(e[17].toString());
				borrowersDealsInfo
						.setPaticipationLimitToLenders(Double.parseDouble(e[13] == null ? " " : e[13].toString()));

				if (e[19] != null) {
					String lastParticipationDate = null;

					Date date = new Date();
					try {
						date = simpleDateFormat1.parse(e[19].toString());

						lastParticipationDate = adminLoanService.addfiveAndHalfHoursToTimeStamp(date);

						borrowersDealsInfo.setLastParticipationDate(lastParticipationDate);

					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}

				OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
						.getFirstParticipationDate(dealId);

				if (oxyLendersAcceptedDeals != null) {

					String firstParticipationDate = adminLoanService
							.addfiveAndHalfHoursToTimeStamp(oxyLendersAcceptedDeals.getReceivedOn());

					borrowersDealsInfo.setFirstParticipationDate(firstParticipationDate);
				}

				if (pageRequestDto.getDealType().equalsIgnoreCase("CLOSED")) {
					borrowersDealsInfo.setBorrowerDealClosingStatus(e[14] == null ? " " : e[14].toString());
					borrowersDealsInfo.setBorrowerDealClosedDate(e[15] == null ? "null" : e[15].toString());
				} else {
					borrowersDealsInfo.setBorrowerDealClosingStatus(null);
					borrowersDealsInfo.setBorrowerDealClosedDate(null);
				}

				borrowersDealsInfo
						.setGetIndividualDealsInformation(adminLoanService.getIndividualDealsInformation(dealId));
				Double lenderAverageValue = oxyDealsRateofinterestToLendersgroupRepo.getAverageValuForLender(dealId);
				borrowersDealsInfo.setAverageValueForLender(lenderAverageValue);
				borrowersDealsInfo.setFundingStatus(dealStatusForFrontEnd);
				Double particiaptionAmount = 0.0;
				Double amount = oxyLendersAcceptedDealsRepo.getListOfLendersParticipatedAmount(dealId);
				Double updatedAmount = lendersPaticipationUpdationRepo.getDealUpdatedSumValue(dealId);
				if (amount != null) {
					if (updatedAmount != null) {
						particiaptionAmount = amount + updatedAmount;
					} else {
						particiaptionAmount = amount;
					}
				}
				borrowersDealsInfo.setTotalPaticipatedAmount(BigDecimal.valueOf(particiaptionAmount).toBigInteger());
				double remainingAmount = 0.0;
				if (particiaptionAmount > 0) {
					remainingAmount = dealAmount - particiaptionAmount;
					borrowersDealsInfo
							.setRemainingAmountToPaticipateInDeal(BigDecimal.valueOf(remainingAmount).toBigInteger());
				} else {
					borrowersDealsInfo
							.setRemainingAmountToPaticipateInDeal(BigDecimal.valueOf(dealAmount).toBigInteger());
				}
				borrowersDealsInfo.setDealPaticipatedAmount(BigDecimal.valueOf(particiaptionAmount).toBigInteger());
				String closeStatus = e[14] == null ? " " : e[14].toString();

				Double returnedPrincipalAmount = lendersReturnsRepo.getSumValueReturnedToDeal(dealId);

				Double returnedToWallet = lenderOxyWalletNativeRepo.sumOfAmountReturnedFromDealToWallet(dealId);
				double currentAmount = 0.0;
				if (returnedPrincipalAmount == null && returnedToWallet == null) {
					currentAmount = particiaptionAmount;
				}
				if (returnedPrincipalAmount != null && returnedToWallet != null) {
					currentAmount = particiaptionAmount - (returnedPrincipalAmount + returnedToWallet);

				} else {
					if (returnedPrincipalAmount == null && returnedToWallet != null) {
						currentAmount = particiaptionAmount - returnedToWallet;
					}
					if (returnedPrincipalAmount != null && returnedToWallet == null) {
						currentAmount = particiaptionAmount - returnedPrincipalAmount;
					}
				}
				if (returnedPrincipalAmount != null) {
					borrowersDealsInfo.setWithdrawalAndPrincipalReturned(
							BigDecimal.valueOf(returnedPrincipalAmount).toBigInteger());
				}
				if (returnedToWallet != null) {
					borrowersDealsInfo
							.setDealAmountReturnedToWallet(BigDecimal.valueOf(returnedToWallet).toBigInteger());
				}

				borrowersDealsInfo.setDealCurrentAmount(BigDecimal.valueOf(currentAmount).toBigInteger());

				int loanRequestId = Integer.parseInt(e[16].toString());
				if (loanRequestId > 0) {
					LoanRequest loanRequest = loanRequestRepo.findById(loanRequestId).get();
					if (loanRequest != null) {
						borrowersDealsInfo.setDealMappedToBorrowerId(loanRequest.getUserId());
					}
				}
				User adminDetails = userRepo.getAdminDetails();
				if (adminDetails != null) {
					borrowersDealsInfo.setAdminId(adminDetails.getId());
				}
				borrowersDealsInfo.setMinimumPaticipationAmount(
						BigDecimal.valueOf(Double.parseDouble(e[18] == null ? "0" : e[18].toString())).toBigInteger());
				borrowersDealsInfo.setLenderPaticipationAmount(
						BigDecimal.valueOf(Double.parseDouble(e[13] == null ? "0" : e[13].toString())).toBigInteger());
				String listOfBorrowerIdsMappedTodeal = e[20] == null ? null : e[20].toString();

				List<Map<String, Integer>> listOfMaps = new ArrayList<>();

				if (listOfBorrowerIdsMappedTodeal != null && !listOfBorrowerIdsMappedTodeal.isEmpty()) {
					borrowersDealsInfo.setBorrowersMappedStatus(true);
					String[] ids = listOfBorrowerIdsMappedTodeal.split(",");
					for (String s : ids) {
						Map<String, Integer> listOfBorrowerIds = new HashMap<String, Integer>();
						listOfBorrowerIds.put("borrowerId", Integer.parseInt(s));
						listOfMaps.add(listOfBorrowerIds);
					}

				}

				borrowersDealsInfo.setListOfBorrowersMappedTodeal(listOfMaps);
				borrowersDealsInfo.setWithdrawStatus(e[21] == null ? null : e[21].toString());
				borrowersDealsInfo.setRoiForWithdraw(Double.parseDouble(e[22] == null ? "0" : e[22].toString()));
				Double roi = oxyDealsRateofinterestToLendersgroupRepo.getMaxRoiToDeal(dealId);
				if (roi != null) {
					borrowersDealsInfo.setRateOfInterest(roi);
				}
				OxyDealsRateofinterestToLendersgroup rateOfInterest = oxyDealsRateofinterestToLendersgroupRepo
						.findByDealIdAndGroupId(dealId, groupId);
				// displaying rate of interest monthly or yearly
				Double roi1 = 0.0;
				String type = null;
				if (rateOfInterest != null) {
					if (rateOfInterest.getYearlyInterest() > 0) {
						roi1 = rateOfInterest.getYearlyInterest();
						type = "YEARLY";

					} else if (rateOfInterest.getHalfInterest() > 0) {
						roi1 = rateOfInterest.getHalfInterest();
						type = "HALFYEARLY";

					} else if (rateOfInterest.getQuartelyInterest() > 0) {
						roi1 = rateOfInterest.getQuartelyInterest();
						type = "QUARTERLY";

					} else if (rateOfInterest.getEndofthedealInterest() > 0) {
						roi1 = rateOfInterest.getEndofthedealInterest();
						type = "ENDOFTHEDEALINTEREST";

					} else {
						roi1 = rateOfInterest.getMonthlyInterest();
						type = "MONTHLY";
					}
					logger.info(roi1 + "RATEOFINTERST");
				}
				borrowersDealsInfo.setRepaymentType(type);
				borrowersDealsResponse.add(borrowersDealsInfo);
			}

		}
		borrowersDealsList.setCount(totalCount);
		borrowersDealsList.setListOfBorrowersDealsResponseDto(borrowersDealsResponse);
		return borrowersDealsList;
	}

	@Override
	public LendersDealsInformation getLendersGroupBasedDealAmounts(
			LenderGroupNamesRequestDto lenderGroupNamesRequestDto) {
		int pageNo = lenderGroupNamesRequestDto.getPageNo();
		int pageSize = lenderGroupNamesRequestDto.getPageSize();

		pageNo = (pageSize * (pageNo - 1));
		LendersDealsInformation lendersDealsInformation = new LendersDealsInformation();
		int lenderGroupId = 0;
		try {

			Integer id = oxyLendersGroupRepo.gettingOxyLenderGroupId(lenderGroupNamesRequestDto.getGroupName());
			if (id != null) {
				lenderGroupId = id;
			} else {
				lenderGroupId = 0;
			}
			BigInteger sumOfDealsAmount = oxyLendersAcceptedDealsRepo
					.sumOfParticipatedAmountBasedOnLenderGroup(lenderGroupId);
			BigInteger sumOfUpdatedAmount = lendersPaticipationUpdationRepo
					.sumOfParticipationUpdatedBasedOnLenderGroup(lenderGroupId);
			if (sumOfDealsAmount != null) {
				if (sumOfUpdatedAmount != null) {
					Double sumOfAmounts = sumOfDealsAmount.doubleValue() + sumOfUpdatedAmount.doubleValue();
					lendersDealsInformation.setTotalDealsAmount(BigDecimal.valueOf(sumOfAmounts).toBigInteger());
				} else {

					lendersDealsInformation.setTotalDealsAmount(sumOfDealsAmount);
				}
			}
			StringBuilder sb = new StringBuilder();
			int y = 0;
			try {
				Date date = new SimpleDateFormat("MMMM").parse(lenderGroupNamesRequestDto.getMonth());
				y = date.getMonth() + 1;

				sb.append(String.format("%02d", y)).append('/').append(lenderGroupNamesRequestDto.getYear());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			String monthAndYear = sb.toString();

			BigInteger sumOfDealsAmountByMonthAndYear = oxyLendersAcceptedDealsRepo
					.sumOfParticipatedAmountBasedOnMonthAndYear(lenderGroupId, monthAndYear);
			BigInteger sumOfUpdatedDealsAmountByMonthAndYear = lendersPaticipationUpdationRepo
					.sumOfParticipatedUpdatedBasedOnMonthAndYear(lenderGroupId, monthAndYear);
			if (sumOfDealsAmountByMonthAndYear != null) {
				if (sumOfUpdatedDealsAmountByMonthAndYear != null) {
					Double sumOfAmounts = sumOfDealsAmountByMonthAndYear.doubleValue()
							+ sumOfUpdatedDealsAmountByMonthAndYear.doubleValue();
					lendersDealsInformation
							.setTotalDealAmountByMonthAndYear(BigDecimal.valueOf(sumOfAmounts).toBigInteger());
				} else {
					lendersDealsInformation.setTotalDealAmountByMonthAndYear(sumOfDealsAmountByMonthAndYear);
				}
			}
			Integer dealsCount = oxyBorrowersDealsInformationRepo.getListOfDealsInformationCount();
			if (dealsCount != null) {
				lendersDealsInformation.setTotalNumberOfDeals(dealsCount);
			}
			List<IndividualDealsInformation> individualDealsInformation = new ArrayList<IndividualDealsInformation>();
			List<OxyBorrowersDealsInformation> listOfDeals = oxyBorrowersDealsInformationRepo.getListOfDeals(pageNo,
					pageSize);
			if (listOfDeals != null && !listOfDeals.isEmpty()) {
				for (OxyBorrowersDealsInformation dealInfo : listOfDeals) {
					IndividualDealsInformation individualDeal = new IndividualDealsInformation();

					int dealId = dealInfo.getId();
					individualDeal.setDealId(dealInfo.getId());
					individualDeal.setDealName(dealInfo.getDealName());
					BigInteger sumOfAmountBasedOnDeal = oxyLendersAcceptedDealsRepo
							.sumOfParticipatedAmountBasedOnLenderGroupAndDealId(lenderGroupId, dealId);
					BigInteger sumOfUpdatedAmountInDeal = lendersPaticipationUpdationRepo
							.sumOfParticipatedUpdatedBasedOnLenderGroupAndDealId(lenderGroupId, dealId);
					if (sumOfAmountBasedOnDeal != null) {
						if (sumOfUpdatedAmountInDeal != null) {
							Double sumOfAmounts = sumOfAmountBasedOnDeal.doubleValue()
									+ sumOfUpdatedAmountInDeal.doubleValue();
							individualDeal.setSumOfDealAmount(BigDecimal.valueOf(sumOfAmounts).toBigInteger());
						} else {
							individualDeal.setSumOfDealAmount(sumOfAmountBasedOnDeal);
						}

					}

					BigInteger sumOfAmountBasedOnMonthAndYear = oxyLendersAcceptedDealsRepo
							.sumOfParticipatedAmountBasedOnLenderGroupAndDealIdByMonthAndYear(lenderGroupId, dealId,
									monthAndYear);

					BigInteger sumOfUpdatedAmountBasedOnMonthAndYear = lendersPaticipationUpdationRepo
							.sumOfParticipatedUpdatedBasedOnLenderGroupAndDealIdByMonthAndYear(lenderGroupId, dealId,
									monthAndYear);
					if (sumOfAmountBasedOnMonthAndYear != null) {
						if (sumOfUpdatedAmountBasedOnMonthAndYear != null) {
							Double sumOfAmounts = sumOfAmountBasedOnMonthAndYear.doubleValue()
									+ sumOfUpdatedAmountBasedOnMonthAndYear.doubleValue();
							individualDeal
									.setSumOfDealAmountByMonthAndYear(BigDecimal.valueOf(sumOfAmounts).toBigInteger());
						} else {
							individualDeal.setSumOfDealAmountByMonthAndYear(sumOfDealsAmountByMonthAndYear);
						}
					}
					individualDealsInformation.add(individualDeal);
				}
				lendersDealsInformation.setIndividualDealsInformation(individualDealsInformation);
			}

		} catch (Exception e) {
			logger.info("Exception in getLendersGroupBasedDealAmounts Method");
		}
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		lendersDealsInformation.setCurrentDate(formatter.format(date));
		lendersDealsInformation.setGivenMonthAndYear(
				lenderGroupNamesRequestDto.getMonth() + " " + lenderGroupNamesRequestDto.getYear());
		return lendersDealsInformation;
	}

	public Integer getDifferenceInDays(String paticipationUpdateDate, String interestDate) {
		String startDate = null;
		String endDate = null;
		int days = 0;
		SimpleDateFormat exitingFormate = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat requiedFormate = new SimpleDateFormat("yyyy-MM-dd");
		try {
			startDate = requiedFormate.format(exitingFormate.parse(paticipationUpdateDate));
			endDate = requiedFormate.format(exitingFormate.parse(interestDate));

		} catch (java.text.ParseException e) {

		}
		LocalDate before = LocalDate.parse(startDate);
		LocalDate after = LocalDate.parse(endDate);
		long noOfDays = ChronoUnit.DAYS.between(before, after);

		days = (int) noOfDays;

		return days;

	}

	public Double getSingleDayInterestForlenderAddedValue(String emiReceivingDate, Integer dealId,
			double lenderAddedAmount, double rateOfInterest, String lenderReturnType) {
		SimpleDateFormat exitingFormate = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat requiedFormate = new SimpleDateFormat("yyyy-MM-dd");
		String requiredFormate = null;
		try {
			requiredFormate = requiedFormate.format(exitingFormate.parse(emiReceivingDate));

		} catch (java.text.ParseException e) {

		}
		String dateValue[] = requiredFormate.split("-");
		int year1 = Integer.parseInt(dateValue[0]);
		int month1 = Integer.parseInt(dateValue[1]);
		YearMonth yearMonth = YearMonth.of(year1, month1);
		int daysInMonthName = yearMonth.lengthOfMonth();
		OxyBorrowersDealsInformation dealInfomation = oxyBorrowersDealsInformationRepo.findById(dealId).get();
		double singleDealAddedValue = 0.0;
		double singleDayInterestValue = 0.0;
		OxyBorrowersDealsInformation oxyBorrowersDeals = oxyBorrowersDealsInformationRepo.toGetCalculationType(dealId);

		if (dealInfomation.getDealType().equals(OxyBorrowersDealsInformation.DealType.NORMAL)) {
			if (oxyBorrowersDeals != null) {
				singleDealAddedValue = Math.round((lenderAddedAmount * rateOfInterest) / 100d);
				singleDayInterestValue = singleDealAddedValue / 30;
			} else {
				singleDealAddedValue = Math.round((lenderAddedAmount * (rateOfInterest / 12)) / 100d);
				singleDayInterestValue = singleDealAddedValue / 30;
			}
		} else {
			if (oxyBorrowersDeals != null) {
				singleDealAddedValue = Math.round((lenderAddedAmount * rateOfInterest) / 100d);
				singleDayInterestValue = singleDealAddedValue / 30;
			} else {
				if (lenderReturnType.equals(OxyLendersAcceptedDeals.LenderReturnsType.MONTHLY.toString())) {
					singleDealAddedValue = Math.round(lenderAddedAmount * (rateOfInterest / 12)) / 100d;
					singleDayInterestValue = singleDealAddedValue / 30;
				} else {
					singleDealAddedValue = Math.round(lenderAddedAmount * (rateOfInterest / 12)) / 100d;
					singleDayInterestValue = singleDealAddedValue / 30;
				}
			}
		}
		return singleDayInterestValue;
	}

	@Override
	public LenderTotalParticipationInterestDetails getPaticipationUpdatedInfo(String monthAndYear,
			String emiReceivingDate, Integer dealId, double rateOfInterest, String lenderReturnType,
			double totalInterest, Integer userId, Integer emiNumber) {
		List<LendersPaticipationUpdation> pativipatedAddInfo = lendersPaticipationUpdationRepo
				.getLenderAddedAreUpdate(userId, dealId, monthAndYear);
		OxyBorrowersDealsInformation oxyBorrowersDeals = oxyBorrowersDealsInformationRepo.findDealAlreadyGiven(dealId);
		int endOfthedealdays = 30 * oxyBorrowersDeals.getDuration();
		List<LenderParticipationUpdatedInfo> totalUpdatedInfo = new ArrayList<LenderParticipationUpdatedInfo>();
		for (LendersPaticipationUpdation lenderUpdation : pativipatedAddInfo) {
			String emiDate[] = emiReceivingDate.split("/");
			int emiStartDate = Integer.parseInt(emiDate[0]);
			int month = Integer.parseInt(emiDate[1]);
			int yearly = Integer.parseInt(emiDate[2]);
			StringBuffer stringBuffer = new StringBuffer();
			stringBuffer.append(yearly + "-" + month + "-" + emiStartDate);
			String loanActiveDate = stringBuffer.toString();
			String updatedDate = lenderUpdation.getUpdatedOn().toString();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = null;
			Date date2 = null;
			try {
				date = formatter.parse(loanActiveDate);
				date2 = formatter.parse(updatedDate);
			} catch (ParseException e) {
				e.printStackTrace();

			}
			if (date2.compareTo(date) < 0) {
				LenderParticipationUpdatedInfo lenderParticipationUpdatedInfo = new LenderParticipationUpdatedInfo();
				lenderParticipationUpdatedInfo.setAmount(lenderUpdation.getUpdationAmount());
				lenderParticipationUpdatedInfo.setUpatedDate(expectedDateFormat.format(lenderUpdation.getUpdatedOn()));
				lenderParticipationUpdatedInfo.setUserId(userId);
				lenderParticipationUpdatedInfo.setRoi(rateOfInterest);
				lenderParticipationUpdatedInfo.setLenderReturnType(lenderReturnType);
				lenderParticipationUpdatedInfo.setTableId(lenderUpdation.getId());
				lenderParticipationUpdatedInfo.setTableType("SECOND");
				String paticipationUpdateDate = expectedDateFormat.format(lenderUpdation.getUpdatedOn());
				String participatedDateRequired = null;
				String newDateRequired = null;
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

				try {
					participatedDateRequired = sdf2.format(sdf.parse(paticipationUpdateDate));
					newDateRequired = sdf2.format(sdf.parse(emiReceivingDate));

				} catch (java.text.ParseException e) {

					e.printStackTrace();
				}
				LocalDate dateBefore = LocalDate.parse(lenderUpdation.getUpdatedOn().toString());
				LocalDate dateAfter = LocalDate.parse(newDateRequired.toString());

				long noOfDaysBetween = ChronoUnit.DAYS.between(dateBefore, dateAfter);
				int firstInterestCalculationDays = 0;
				int differenceIndays = (int) noOfDaysBetween;
				// Integer differenceIndays = 0;
				// differenceIndays = getDifferenceInDays(paticipationUpdateDate,
				// emiReceivingDate);
				String dateType1[] = participatedDateRequired.split("-");
				int yearForPaticipationStart = Integer.parseInt(dateType1[0]);
				int monthForPaticipationStart = Integer.parseInt(dateType1[1]);
				YearMonth yearMonth = YearMonth.of(yearForPaticipationStart, monthForPaticipationStart);
				int daysInMonthForParticipationDate = yearMonth.lengthOfMonth();

				if (differenceIndays > 0) {
					if (daysInMonthForParticipationDate > 30) {
						differenceIndays = differenceIndays - 2;
					} else if (daysInMonthForParticipationDate == 29) {
						differenceIndays = differenceIndays + 1;
					} else if (daysInMonthForParticipationDate == 28) {
						differenceIndays = differenceIndays + 2;
					} else {
						if (lenderReturnType.equalsIgnoreCase("YEARLY")) {

							differenceIndays = differenceIndays;

						} else {
							differenceIndays = differenceIndays - 1;
						}

					}
				}

				String s2[] = paticipationUpdateDate.split("/");
				int participatedDate = Integer.parseInt(s2[0]);

				int firstMonthDaysInterest = 0;
				if (participatedDate < 30) {
					firstMonthDaysInterest = 30 - participatedDate;
				}

				String s1[] = emiReceivingDate.split("/");

				int yearForFundsStart = Integer.parseInt(s1[2]);
				int monthForFundsStart = Integer.parseInt(s1[1]);
				int emiReceivingDateValue = Integer.parseInt(s1[0]);
				int lastMonthDaysInterest = 0;
				if (emiReceivingDateValue < 30) {
					lastMonthDaysInterest = emiReceivingDateValue;
				}
				DecimalFormat dateFormate = new DecimalFormat("0.00");

				Double singleDayInterestValue = 0.0;
				LendersReturns lenderReturns = lendersReturnsRepo.getLenderLatestInterestReturnedDate(userId, dealId);
				if (differenceIndays > 0) {
					if (lenderReturnType.equalsIgnoreCase("MONTHLY")) {
						if (differenceIndays < 30) {
							// if (lenderReturns == null) {
							// differenceIndays = differenceIndays - 1;
							// }
							singleDayInterestValue = getSingleDayInterestForlenderAddedValue(emiReceivingDate, dealId,
									lenderUpdation.getUpdationAmount(), rateOfInterest, lenderReturnType);

							totalInterest = totalInterest + (differenceIndays * singleDayInterestValue);
							lenderParticipationUpdatedInfo
									.setInterestAmount((double) Math.round(differenceIndays * singleDayInterestValue));
							lenderParticipationUpdatedInfo.setDifferenceInDays(differenceIndays);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);
						} else if (differenceIndays >= 30 && emiNumber == 1) {
							// differenceIndays = differenceIndays - 1;

							singleDayInterestValue = getSingleDayInterestForlenderAddedValue(emiReceivingDate, dealId,
									lenderUpdation.getUpdationAmount(), rateOfInterest, lenderReturnType);

							totalInterest = totalInterest + (differenceIndays * singleDayInterestValue);
							lenderParticipationUpdatedInfo
									.setInterestAmount((double) Math.round(differenceIndays * singleDayInterestValue));
							lenderParticipationUpdatedInfo.setDifferenceInDays(differenceIndays);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);
						} else {

							differenceIndays = 30;
							// if (lenderReturns == null) {
							// differenceIndays = differenceIndays - 1;
							// }
							singleDayInterestValue = getSingleDayInterestForlenderAddedValue(emiReceivingDate, dealId,
									lenderUpdation.getUpdationAmount(), rateOfInterest, lenderReturnType);

							totalInterest = totalInterest + (differenceIndays * singleDayInterestValue);
							lenderParticipationUpdatedInfo
									.setInterestAmount((double) Math.round(differenceIndays * singleDayInterestValue));
							lenderParticipationUpdatedInfo.setDifferenceInDays(differenceIndays);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);
						}
					} else if (lenderReturnType.equalsIgnoreCase("QUARTELY")) {
						LendersReturns lenderInterest = lendersReturnsRepo.getLenderLatestInterestReturnedDate(userId,
								dealId);

						singleDayInterestValue = getSingleDayInterestForlenderAddedValue(emiReceivingDate, dealId,
								lenderUpdation.getUpdationAmount(), rateOfInterest, lenderReturnType);
						singleDayInterestValue = Double.parseDouble(dateFormate.format(singleDayInterestValue));
						if (lenderInterest == null) {

							Date firstDate = null;
							Date secondDate = null;
							try {
								firstDate = formatter.parse(participatedDateRequired); // participatedDate
								secondDate = formatter.parse(newDateRequired); // quarterly fastDate
							} catch (ParseException e) {

								e.printStackTrace();
							}
							int differenceInDaysCount = 0;

							if (firstDate.before(secondDate)) {
								String paticipatedMonth = participatedDateRequired;
								Date nextEmiDatee = null;
								try {
									nextEmiDatee = formatter.parse(paticipatedMonth);
								} catch (ParseException e) {

									e.printStackTrace();
								}
								for (int i1 = 1; i1 <= 4; i1++) {
									SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
									Calendar c = Calendar.getInstance();
									int paticipatedYear = 0;
									int paticipatedMonthNumber = 0;
									int paticipatedDate = 0;
									String dateParts[] = paticipatedMonth.split("-");
									paticipatedYear = Integer.parseInt(dateParts[0]);

									paticipatedMonthNumber = Integer.parseInt(dateParts[1]);

									if (Integer.parseInt(dateParts[2]) == 31) {
										paticipatedDate = 30;

									} else {
										paticipatedDate = Integer.parseInt(dateParts[2]);
									}
									String requiredDate = expectedDateFormat.format(nextEmiDatee);
									String emiMonth[] = requiredDate.split("/");

									int monthNumber = Integer.parseInt(emiMonth[1]); // new
									int dateNumber = Integer.parseInt(emiMonth[0]); // new

									String paticipated[] = newDateRequired.split("-");
									int quartlyMonth = Integer.parseInt(paticipated[1]);
									int quartlyDate = Integer.parseInt(paticipated[2]);
									if (monthNumber != quartlyMonth) {
										if (i1 == 1) {
											if ((dateNumber + 1) <= 30) {
												differenceInDaysCount = differenceInDaysCount + (30 - (dateNumber));
											} else {
												differenceInDaysCount = 30;
											}
										} else {
											differenceInDaysCount = differenceInDaysCount + 30;
										}
									} else {
										if (i1 == 1) {
											if (quartlyDate > dateNumber) {
												differenceInDaysCount = differenceInDaysCount + quartlyDate
														- (dateNumber);
											}

										} else {
											/*
											 * if (i1 > 3) { differenceInDaysCount = differenceInDaysCount + 30; }
											 */
											differenceInDaysCount = differenceInDaysCount + emiStartDate - 1;
										}
										break;
									}

									c.set(paticipatedYear, paticipatedMonthNumber, paticipatedDate);

									paticipatedMonth = sdf1.format(c.getTime());

									try {
										nextEmiDatee = sdf1.parse(paticipatedMonth);
									} catch (ParseException e) {

										e.printStackTrace();

									}
								}

							}
							totalInterest = totalInterest + (differenceInDaysCount * singleDayInterestValue);
							lenderParticipationUpdatedInfo.setInterestAmount(
									(double) Math.round(differenceInDaysCount * singleDayInterestValue));
							lenderParticipationUpdatedInfo.setDifferenceInDays(differenceInDaysCount);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);
						} else {
							totalInterest = totalInterest + ((90) * singleDayInterestValue);
							lenderParticipationUpdatedInfo
									.setInterestAmount((double) Math.round(90 * singleDayInterestValue));
							lenderParticipationUpdatedInfo.setDifferenceInDays(90);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);
						}

					} else if (lenderReturnType.equalsIgnoreCase("HALFLY")) {
						LendersReturns lenderInterest = lendersReturnsRepo.getLenderLatestInterestReturnedDate(userId,
								dealId);

						int differenceInDaysCount = 0;
						singleDayInterestValue = getSingleDayInterestForlenderAddedValue(emiReceivingDate, dealId,
								lenderUpdation.getUpdationAmount(), rateOfInterest, lenderReturnType);
						singleDayInterestValue = Double.parseDouble(dateFormate.format(singleDayInterestValue));
						if (lenderInterest == null) {
							String Paticipateddate[] = participatedDateRequired.split("-");

							Date firstDate = null;
							Date secondDate = null;
							try {
								firstDate = formatter.parse(participatedDateRequired); // participatedDate
								secondDate = formatter.parse(newDateRequired); // halfly startDate
							} catch (ParseException e) {

								e.printStackTrace();
							}
							if (firstDate.before(secondDate)) {
								String paticipatedMonth = participatedDateRequired;
								Date nextEmiDatee = null;
								try {
									nextEmiDatee = formatter.parse(paticipatedMonth);
								} catch (ParseException e) {

									e.printStackTrace();
								}
								for (int i1 = 1; i1 <= 7; i1++) {
									SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
									Calendar c = Calendar.getInstance();
									int paticipatedYear = 0;
									int paticipatedMonthNumber = 0;
									int paticipatedDate = 0;
									String dateParts[] = paticipatedMonth.split("-");
									paticipatedYear = Integer.parseInt(dateParts[0]);

									paticipatedMonthNumber = Integer.parseInt(dateParts[1]);

									if (Integer.parseInt(dateParts[2]) == 31) {
										paticipatedDate = 30;

									} else {
										paticipatedDate = Integer.parseInt(dateParts[2]);
									}
									String requiredDate = expectedDateFormat.format(nextEmiDatee);
									String emiMonth[] = requiredDate.split("/");

									int monthNumber = Integer.parseInt(emiMonth[1]); // new
									int dateNumber = Integer.parseInt(emiMonth[0]); // new

									String paticipated[] = newDateRequired.split("-");
									int quartlyMonth = Integer.parseInt(paticipated[1]);
									int quartlyDate = Integer.parseInt(paticipated[2]);
									if (monthNumber != quartlyMonth) {
										if (i1 == 1) {
											if ((dateNumber + 1) <= 30) {
												differenceInDaysCount = differenceInDaysCount + (30 - (dateNumber));
											} else {
												differenceInDaysCount = 30;
											}
										} else {
											differenceInDaysCount = differenceInDaysCount + 30;
										}
									} else {
										if (i1 == 1) {
											if (quartlyDate > dateNumber) {
												differenceInDaysCount = differenceInDaysCount + quartlyDate
														- (dateNumber);
											}

										} else {
											/*
											 * if (i1 > 6) { differenceInDaysCount = differenceInDaysCount + 30; }
											 */
											differenceInDaysCount = differenceInDaysCount + emiStartDate - 1;
										}
										break;
									}

									c.set(paticipatedYear, paticipatedMonthNumber, paticipatedDate);

									paticipatedMonth = sdf1.format(c.getTime());

									try {
										nextEmiDatee = sdf1.parse(paticipatedMonth);
									} catch (ParseException e) {

										e.printStackTrace();

									}
								}

							} else {
								differenceInDaysCount = 0;
							}

							totalInterest = totalInterest + (differenceInDaysCount * singleDayInterestValue);
							lenderParticipationUpdatedInfo.setInterestAmount(
									(double) Math.round(differenceInDaysCount * singleDayInterestValue));
							lenderParticipationUpdatedInfo.setDifferenceInDays(differenceInDaysCount);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);
						} else {
							totalInterest = totalInterest + ((180) * singleDayInterestValue);
							lenderParticipationUpdatedInfo
									.setInterestAmount((double) Math.round(180 * singleDayInterestValue));
							lenderParticipationUpdatedInfo.setDifferenceInDays(180);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);
						}

					} else if (lenderReturnType.equalsIgnoreCase("YEARLY")) {
						singleDayInterestValue = getSingleDayInterestForlenderAddedValue(emiReceivingDate, dealId,
								lenderUpdation.getUpdationAmount(), rateOfInterest, lenderReturnType);
						singleDayInterestValue = Double.parseDouble(dateFormate.format(singleDayInterestValue));
						LendersReturns lenderInterest = lendersReturnsRepo.getLenderLatestInterestReturnedDate(userId,
								dealId);

						String monthAndYear1 = s1[1] + "/" + s1[2];

						int firstMonthCount = 0;
						int differenceInDaysCount = 0;

						String nextDate = null;
						if (lenderReturns == null) {
							String Paticipateddate[] = participatedDateRequired.split("-");
							nextDate = s1[2] + "-" + s1[1] + "-" + s1[0];
							String fundsStart = null;
							try {
								fundsStart = sdf2.format(sdf.parse(
										expectedDateFormat.format(oxyBorrowersDeals.getFundsAcceptanceStartDate())));

							} catch (java.text.ParseException e) {

								e.printStackTrace();
							}
							LocalDate before = LocalDate.parse(participatedDateRequired);
							LocalDate after = LocalDate.parse(fundsStart);
							long noOfDays = ChronoUnit.DAYS.between(after, before);

							if (noOfDays < 30) {
								firstMonthCount = 30 - (int) noOfDays;

							} else {
								firstMonthCount = 0;
							}

							Period diff = Period.between(LocalDate.parse(participatedDateRequired).withDayOfMonth(1),
									LocalDate.parse(nextDate).withDayOfMonth(1));
							// firstMonthCount = 30 - Integer.parseInt(Paticipateddate[2]);
							if (diff.getYears() == 1 && diff.getMonths() == 0) {
								if (firstMonthCount >= 1) {
									if (participatedDateRequired.equals(fundsStart)) {
										differenceInDaysCount = 359;
									} else {
										differenceInDaysCount = 360 - (int) noOfDays;
									}

								} else {
									differenceInDaysCount = differenceInDaysCount + (359);
								}
							} else if (diff.getYears() == 0 && diff.getMonths() >= 1) {
								if (firstMonthCount >= 1) {
									differenceInDaysCount = firstMonthCount - 1;
									differenceInDaysCount = differenceInDaysCount + ((diff.getMonths() - 1) * 30);
								} else {
									differenceInDaysCount = differenceInDaysCount + ((diff.getMonths() * 30) - 2);
								}

							} else if (diff.getYears() == 1 && diff.getMonths() >= 1) {
								if (firstMonthCount >= 1) {
									differenceInDaysCount = firstMonthCount - 1;
									differenceInDaysCount = differenceInDaysCount + (330);
								} else {
									differenceInDaysCount = differenceInDaysCount + (359); //
								}
							} else {
								// differenceInDaysCount = 0;
								differenceInDaysCount = 360;
							}

							totalInterest = totalInterest + (differenceInDaysCount * singleDayInterestValue);

							lenderParticipationUpdatedInfo.setInterestAmount(
									(double) Math.round(differenceInDaysCount * singleDayInterestValue));
							lenderParticipationUpdatedInfo.setDifferenceInDays(differenceInDaysCount);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);

						} else {
							totalInterest = totalInterest + (360 * singleDayInterestValue);

							lenderParticipationUpdatedInfo
									.setInterestAmount((double) Math.round(360 * singleDayInterestValue));
							lenderParticipationUpdatedInfo.setDifferenceInDays(360);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);

						}

					} else if (lenderReturnType.equalsIgnoreCase("ENDOFTHEDEAL")) {

						int firstMonthCount = 0;
						int differenceInDaysCount = 0;
						singleDayInterestValue = getSingleDayInterestForlenderAddedValue(emiReceivingDate, dealId,
								lenderUpdation.getUpdationAmount(), rateOfInterest, lenderReturnType);
						singleDayInterestValue = Double.parseDouble(dateFormate.format(singleDayInterestValue));

						String Paticipateddate[] = participatedDateRequired.split("-");

						Period diff = Period.between(LocalDate.parse(participatedDateRequired).withDayOfMonth(1),
								LocalDate.parse(newDateRequired).withDayOfMonth(1));

						firstMonthCount = 30 - Integer.parseInt(Paticipateddate[2]);

						if (diff.getYears() != 0 && diff.getMonths() == 0) {
							if (firstMonthCount >= 1) {
								differenceInDaysCount = firstMonthCount - 1;
								differenceInDaysCount = differenceInDaysCount + ((diff.getYears() * 12) * 30);
							} else {
								differenceInDaysCount = differenceInDaysCount + ((((diff.getYears() * 12) * 30) - 2));
							}

						} else if (diff.getYears() == 0 && diff.getMonths() >= 1) {
							if (firstMonthCount >= 1) {
								differenceInDaysCount = firstMonthCount - 1;
								differenceInDaysCount = differenceInDaysCount + ((diff.getMonths() - 1) * 30);
							} else {
								differenceInDaysCount = differenceInDaysCount + ((diff.getMonths() * 30) - 2);
							}

						} else if (diff.getYears() != 0 && diff.getMonths() >= 1) {
							if (firstMonthCount >= 1) {
								differenceInDaysCount = firstMonthCount - 1;
								differenceInDaysCount = differenceInDaysCount
										+ (((diff.getMonths() - 1) * 30) + ((diff.getYears() * 12) * 30));
							} else {
								differenceInDaysCount = differenceInDaysCount
										+ ((((diff.getMonths()) * 30) - 2) + ((diff.getYears() * 12) * 30)); //
							}

						} else {
							differenceInDaysCount = 0;
						}

						totalInterest = totalInterest + (differenceInDaysCount * singleDayInterestValue);
						lenderParticipationUpdatedInfo
								.setInterestAmount((double) Math.round(differenceInDaysCount * singleDayInterestValue));
						lenderParticipationUpdatedInfo.setDifferenceInDays(differenceInDaysCount);
						totalUpdatedInfo.add(lenderParticipationUpdatedInfo);

					}

				}
			}
		}
		LenderTotalParticipationInterestDetails lenderTotalParticipationInterestDetails = new LenderTotalParticipationInterestDetails();
		lenderTotalParticipationInterestDetails.setListOfParticipationUpdated(totalUpdatedInfo);
		lenderTotalParticipationInterestDetails.setInterestAmount(totalInterest);
		return lenderTotalParticipationInterestDetails;

	}

	@Override
	public DealLevelLoanEmiCardInformation loanEmiCardBasedOnCurrentValue(int userId, int dealId) {
		DealLevelLoanEmiCardInformation dealLevelLoanEmiCardInformation = new DealLevelLoanEmiCardInformation();
		try {
			OxyBorrowersDealsInformation dealInfomation = oxyBorrowersDealsInformationRepo.findById(dealId).get();
			OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
					.toCheckUserAlreadyInvoledInDeal(userId, dealId);
			Double participationUpdated = lendersPaticipationUpdationRepo.getSumOfLenderUpdatedAmountToDeal(userId,
					dealId);
			Double participatedAmount = 0.0;
			Double participationCurrentValue = 0.0;
			Double currentValue = 0.0;
			Double withdrawalAmount = 0.0;
			if (oxyLendersAcceptedDeals != null) {
				if (participationUpdated != null) {
					participatedAmount = oxyLendersAcceptedDeals.getParticipatedAmount() + participationUpdated;
				} else {
					participatedAmount = oxyLendersAcceptedDeals.getParticipatedAmount();
				}

			}
			Double sumPricipalReturnedAmount = lendersReturnsRepo.getSumOfAmountReturned(userId, "LENDERPRINCIPAL",
					dealId);
			if (sumPricipalReturnedAmount != null) {
				participationCurrentValue = participatedAmount - sumPricipalReturnedAmount;
			} else {
				participationCurrentValue = participatedAmount;
			}
			Double sumOfWithDramAmount = lendersReturnsRepo.getLenderWithdrawSumValue(userId, dealId);
			if (sumOfWithDramAmount != null) {
				withdrawalAmount = participationCurrentValue - sumOfWithDramAmount;
			} else {
				withdrawalAmount = participationCurrentValue;
			}

			Double sumOfWalletAmount = lenderOxyWalletNativeRepo.paticipatedAmountMovedToWalletSum(userId, dealId);
			if (sumOfWalletAmount != null) {
				currentValue = withdrawalAmount - sumOfWalletAmount;
			} else {
				currentValue = withdrawalAmount;
			}

			LendersReturns lenderReturns = lendersReturnsRepo.getLatestPricipalReturnedDate(userId, dealId);

			LenderOxyWallet lenderOxyWallet = lenderOxyWalletNativeRepo.getLatestPrincipalReturn(userId, dealId);
			String latestPricipalDate = null;
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			if (lenderReturns != null && lenderOxyWallet == null) {
				latestPricipalDate = lenderReturns.getPaidDate().toString();
			} else if (lenderReturns == null && lenderOxyWallet != null) {
				latestPricipalDate = lenderOxyWallet.getTransactionDate().toString();
			} else if (lenderReturns != null && lenderOxyWallet != null) {
				String interestReturned = lenderReturns.getPaidDate().toString();
				String walletLoaded = lenderOxyWallet.getTransactionDate().toString();
				Date interestReturnedDate = null;
				Date walletLoadedFundsDate = null;
				try {
					interestReturnedDate = formatter.parse(interestReturned);
					walletLoadedFundsDate = formatter.parse(walletLoaded);
				} catch (ParseException e) {
					e.printStackTrace();

				}

				if (interestReturnedDate.compareTo(walletLoadedFundsDate) > 0) {
					latestPricipalDate = lenderReturns.getPaidDate().toString();
				} else {
					latestPricipalDate = lenderOxyWallet.getTransactionDate().toString();
				}

			}

			String loanActiveDate = dealInfomation.getLoanActiveDate().toString();
			String nextDate1 = loanActiveDate;

			Date date = null;
			Date date2 = null;
			try {
				date = formatter.parse(loanActiveDate);
				date2 = formatter.parse(latestPricipalDate);
			} catch (ParseException e) {
				e.printStackTrace();

			}
			List<LenderParticipationUpdatedInfo> totalUpdatedInfo = new ArrayList<LenderParticipationUpdatedInfo>();
			int endOfthedealdays = 30 * dealInfomation.getDuration();
			Integer countValueForMonthly = 0;
			Integer countValueForQuartely = 0;
			Integer countValueForHalfly = 0;
			Integer countValueForYearly = 0;
			Integer countValueForEnd = 0;
			for (int i = 1; i <= dealInfomation.getDuration(); i++) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Calendar c1 = Calendar.getInstance();
				String dateParts[] = nextDate1.split("-");
				int year = Integer.parseInt(dateParts[0]);

				int month = Integer.parseInt(dateParts[1]);

				int date1 = Integer.parseInt(dateParts[2]);

				String emiReceivingDate = expectedDateFormat.format(date);

				double singleDealAddedValue = 0.0;
				double singleDayInterestValue = 0.0;
				OxyBorrowersDealsInformation oxyBorrowersDeals = oxyBorrowersDealsInformationRepo
						.toGetCalculationType(dealId);

				if (dealInfomation.getDealType().equals(OxyBorrowersDealsInformation.DealType.NORMAL)) {
					if (oxyBorrowersDeals != null) {
						singleDealAddedValue = Math
								.round((currentValue * oxyLendersAcceptedDeals.getRateofinterest()) / 100d);
						singleDayInterestValue = singleDealAddedValue / 30;
					} else {
						singleDealAddedValue = Math
								.round((currentValue * (oxyLendersAcceptedDeals.getRateofinterest() / 12)) / 100d);
						singleDayInterestValue = singleDealAddedValue / 30;
					}
				} else {
					if (oxyBorrowersDeals != null) {
						singleDealAddedValue = Math
								.round((currentValue * oxyLendersAcceptedDeals.getRateofinterest()) / 100d);
						singleDayInterestValue = singleDealAddedValue / 30;
					} else {
						if (oxyLendersAcceptedDeals.getLenderReturnsType()
								.equals(OxyLendersAcceptedDeals.LenderReturnsType.MONTHLY)) {
							singleDealAddedValue = Math
									.round(currentValue * oxyLendersAcceptedDeals.getRateofinterest()) / 100d;
							singleDayInterestValue = singleDealAddedValue / 30;
						} else {
							singleDealAddedValue = Math
									.round(currentValue * (oxyLendersAcceptedDeals.getRateofinterest() / 12)) / 100d;
							singleDayInterestValue = singleDealAddedValue / 30;
						}
					}
				}
				Integer differenceIndays = 0;
				LenderParticipationUpdatedInfo lenderParticipationUpdatedInfo = new LenderParticipationUpdatedInfo();
				if (oxyLendersAcceptedDeals.getLenderReturnsType()
						.equals(OxyLendersAcceptedDeals.LenderReturnsType.MONTHLY)) {
					if (date.compareTo(date2) > 0) {
						differenceIndays = 30;
						countValueForMonthly = countValueForMonthly + 1;
						lenderParticipationUpdatedInfo
								.setInterestAmount((double) Math.round(differenceIndays * singleDayInterestValue));
						lenderParticipationUpdatedInfo.setDifferenceInDays(differenceIndays);
						lenderParticipationUpdatedInfo.setEmiStartDate(emiReceivingDate);
						if (i == dealInfomation.getDuration()) {
							lenderParticipationUpdatedInfo
									.setPrincipalAmount(BigDecimal.valueOf(currentValue).toBigInteger());
						} else {
							lenderParticipationUpdatedInfo.setPrincipalAmount(BigDecimal.valueOf(0.0).toBigInteger());
						}
						lenderParticipationUpdatedInfo.setSno(countValueForMonthly);
						totalUpdatedInfo.add(lenderParticipationUpdatedInfo);
					}
				} else if (oxyLendersAcceptedDeals.getLenderReturnsType()
						.equals(OxyLendersAcceptedDeals.LenderReturnsType.QUARTELY)) {
					if (date.compareTo(date2) > 0) {
						if (i % 3 == 0) {
							countValueForQuartely = countValueForQuartely + 1;
							differenceIndays = 90;

							lenderParticipationUpdatedInfo
									.setInterestAmount((double) Math.round(differenceIndays * singleDayInterestValue));
							lenderParticipationUpdatedInfo.setDifferenceInDays(differenceIndays);
							lenderParticipationUpdatedInfo.setEmiStartDate(emiReceivingDate);
							if (i == dealInfomation.getDuration()) {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(currentValue).toBigInteger());
							} else {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(0.0).toBigInteger());
							}
							lenderParticipationUpdatedInfo.setSno(countValueForQuartely);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);

						} else {
							countValueForQuartely = countValueForQuartely + 1;
							if (i == dealInfomation.getDuration()) {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(currentValue).toBigInteger());
							} else {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(0.0).toBigInteger());
							}
							lenderParticipationUpdatedInfo.setSno(countValueForQuartely);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);
						}
					}

				} else if (oxyLendersAcceptedDeals.getLenderReturnsType()
						.equals(OxyLendersAcceptedDeals.LenderReturnsType.HALFLY)) {
					if (date.compareTo(date2) > 0) {
						if (i % 6 == 0) {
							countValueForHalfly = countValueForHalfly + 1;
							differenceIndays = 180;

							lenderParticipationUpdatedInfo
									.setInterestAmount((double) Math.round(differenceIndays * singleDayInterestValue));
							lenderParticipationUpdatedInfo.setDifferenceInDays(differenceIndays);
							lenderParticipationUpdatedInfo.setEmiStartDate(emiReceivingDate);
							if (i == dealInfomation.getDuration()) {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(currentValue).toBigInteger());
							} else {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(0.0).toBigInteger());
							}
							lenderParticipationUpdatedInfo.setSno(countValueForHalfly);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);

						} else {
							countValueForHalfly = countValueForHalfly + 1;
							if (i == dealInfomation.getDuration()) {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(currentValue).toBigInteger());
							} else {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(0.0).toBigInteger());
							}
							lenderParticipationUpdatedInfo.setSno(countValueForHalfly);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);
						}
					}

				} else if (oxyLendersAcceptedDeals.getLenderReturnsType()
						.equals(OxyLendersAcceptedDeals.LenderReturnsType.YEARLY)) {
					if (date.compareTo(date2) > 0) {
						if (i % 12 == 0) {
							countValueForYearly = countValueForYearly + 1;
							differenceIndays = 360;

							lenderParticipationUpdatedInfo
									.setInterestAmount((double) Math.round(differenceIndays * singleDayInterestValue));
							lenderParticipationUpdatedInfo.setDifferenceInDays(differenceIndays);
							lenderParticipationUpdatedInfo.setEmiStartDate(emiReceivingDate);
							if (i == dealInfomation.getDuration()) {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(currentValue).toBigInteger());
							} else {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(0.0).toBigInteger());
							}
							lenderParticipationUpdatedInfo.setSno(countValueForYearly);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);

						} else {
							countValueForYearly = countValueForYearly + 1;
							if (i == dealInfomation.getDuration()) {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(currentValue).toBigInteger());
							} else {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(0.0).toBigInteger());
							}
							lenderParticipationUpdatedInfo.setSno(countValueForYearly);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);
						}
					}
				} else if (oxyLendersAcceptedDeals.getLenderReturnsType()
						.equals(OxyLendersAcceptedDeals.LenderReturnsType.ENDOFTHEDEAL)) {
					if (date.compareTo(date2) > 0) {
						if (i % dealInfomation.getDuration() == 0) {
							countValueForEnd = countValueForEnd + 1;
							differenceIndays = endOfthedealdays;

							lenderParticipationUpdatedInfo
									.setInterestAmount((double) Math.round(differenceIndays * singleDayInterestValue));
							lenderParticipationUpdatedInfo.setDifferenceInDays(differenceIndays);
							lenderParticipationUpdatedInfo.setEmiStartDate(emiReceivingDate);
							if (i == dealInfomation.getDuration()) {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(currentValue).toBigInteger());
							} else {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(0.0).toBigInteger());
							}
							lenderParticipationUpdatedInfo.setSno(countValueForEnd);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);

						} else {
							countValueForEnd = countValueForEnd + 1;
							if (i == dealInfomation.getDuration()) {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(currentValue).toBigInteger());
							} else {
								lenderParticipationUpdatedInfo
										.setPrincipalAmount(BigDecimal.valueOf(0.0).toBigInteger());
							}
							lenderParticipationUpdatedInfo.setSno(countValueForEnd);
							totalUpdatedInfo.add(lenderParticipationUpdatedInfo);
						}
					}
				}
				c1.set(year, month, date1);

				nextDate1 = sdf.format(c1.getTime());

				try {
					date = sdf.parse(nextDate1);
				} catch (ParseException e) {

					e.printStackTrace();

				}
			}
			dealLevelLoanEmiCardInformation.setParticipationUpdatedInfoList(totalUpdatedInfo);
			dealLevelLoanEmiCardInformation
					.setFirstInterestStartDate(expectedDateFormat.format(dealInfomation.getLoanActiveDate()));
		} catch (Exception e) {
			logger.info("Exception in loanEmiCardBasedOnCurrentValue method");
		}
		return dealLevelLoanEmiCardInformation;

	}

	@Override
	public DealLevelResponseDto dealLevelDisbursmentForAppLevel(Integer userId, UserRequest userRequest) {

		DealLevelResponseDto response = new DealLevelResponseDto();
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);

		if (loanRequest != null) {
			if (loanRequest.getLoanOfferedAmount() != null) {
				if (loanRequest.getLoanOfferedAmount().getLoanOfferedAmount() >= 50000) {

					ApplicationLevelEnachMandate enachForPI = null;
					ApplicationLevelEnachMandate enachForP = null;
					ApplicationLevelEnachMandate enachForI = null;
					if (loanRequest.getRepaymentMethodForBorrower().equalsIgnoreCase("PI")) {
						enachForPI = applicationLevelEnachMandateRepo.findingIsEnachActivatedForPI(loanRequest.getId());
					} else {
						enachForP = applicationLevelEnachMandateRepo.findingIsEnachActivatedForP(loanRequest.getId());
						enachForI = applicationLevelEnachMandateRepo.findingIsEnachActivatedForI(loanRequest.getId());
					}

					if (enachForPI != null || (enachForP != null && enachForI != null)) {

						Integer countOfLoans = oxyLoanRepo
								.findingCountOfLoansBasedOnParentRequestId(loanRequest.getId());
						Integer noOfLoansDisbursed = oxyLoanRepo.findingNoOfLoansDisbursed(loanRequest.getId());

						if (countOfLoans != noOfLoansDisbursed) {

							ApplicationLevelLoanEmiCard loanEmiCard = applicationLevelLoanEmiCardRepo
									.findFirstEmiBasedOnApplicationId(loanRequest.getId());

							if (loanEmiCard != null) {

								if (userRequest.getAdminComments().equals(Status.DISBURSED.toString())) {

									if (loanRequest.getDurationType() == DurationType.Months) {
										String firstEmiDueDate = null;
										int differenceInDays = 0;
										firstEmiDueDate = expectedDateFormat.format(loanEmiCard.getEmiDueOn());
										Date dateBefore = null;
										Date dateAfter = null;
										try {
											dateBefore = expectedDateFormat.parse(userRequest.getDisbursedDate());
											dateAfter = expectedDateFormat.parse(firstEmiDueDate);
										} catch (ParseException e) {

											e.printStackTrace();
										}

										long difference = dateAfter.getTime() - dateBefore.getTime();
										float daysBetween = (difference / (1000 * 60 * 60 * 24));
										differenceInDays = (int) daysBetween;

										Double interestPerDay = loanEmiCard.getInterstAmount() / 30;

										Double interestAmountForFirstEmi = differenceInDays * interestPerDay;

										logger.info(interestAmountForFirstEmi);
										loanEmiCard.setInterstAmount(interestAmountForFirstEmi);

										Double emiAmount = loanEmiCard.getPrincipalAmount() + interestAmountForFirstEmi;

										loanEmiCard.setEmiAmount(emiAmount);
										applicationLevelLoanEmiCardRepo.save(loanEmiCard);

										List<OxyLoan> listOfLoans = oxyLoanRepo
												.findingListOfLoansByAppId(loanRequest.getLoanRequestId());

										if (listOfLoans != null && !listOfLoans.isEmpty()) {
											for (OxyLoan loan : listOfLoans) {
												loan.setAdminComments("DISBURSED");
												loan.setBorrowerDisbursedDate(dateBefore);
												oxyLoanRepo.save(loan);

												logger.info("Referral bonus for borrower starts................");
												Integer borrowerRefereeId = loan.getBorrowerUserId();

												LenderReferenceDetails borrowerRefereeInfo = lenderReferenceDetailsRepo
														.findRefereeInfo(borrowerRefereeId);

												if (borrowerRefereeInfo != null) {

													OxyUserType oxyUser = oxyUserTypeRepo
															.findByIdNum(borrowerRefereeInfo.getReferrerId());

													User borrowerReferee = userRepo
															.findByIdNum(borrowerRefereeInfo.getRefereeId());
													if (oxyUser != null || borrowerReferee != null) {
														if ((oxyUser
																.getReferralCheck() == OxyUserType.ReferralCheck.YES)
																|| borrowerReferee.getUrchinTrackingModule()
																		.equalsIgnoreCase("WEB")) {

															if (borrowerReferee.getPrimaryType()
																	.equals(User.PrimaryType.BORROWER)) {

																Double disbursmentAmount = loan.getDisbursmentAmount();

																Double amount = 0.0;

																amount = disbursmentAmount / referralBonusByBorrower;

																borrowerRefereeInfo.setStatus(
																		LenderReferenceDetails.Status.Disbursed);

																LenderReferralBonusUpdated referralBonusDetails = new LenderReferralBonusUpdated();

																if (borrowerReferee.getLoanOwner() != null
																		|| !borrowerReferee.getLoanOwner().isEmpty()) {
																	OxyBorrowersDealsInformation dealInfo = oxyBorrowersDealsInformationRepo
																			.findByDealName(
																					borrowerReferee.getLoanOwner());
																	if (dealInfo != null) {
																		referralBonusDetails
																				.setDealId(dealInfo.getId());
																		oxyBorrowersDealsInformationRepo.save(dealInfo);

																	} else {
																		referralBonusDetails.setDealId(0);
																	}
																} else {
																	referralBonusDetails.setDealId(0);
																}

																referralBonusDetails.setReferrerUserId(
																		borrowerRefereeInfo.getReferrerId());
																referralBonusDetails.setAmount(amount);
																referralBonusDetails
																		.setRefereeUserId(borrowerRefereeId);
																referralBonusDetails.setParticipatedOn(new Date());
																referralBonusDetails
																		.setParticipatedAmount(disbursmentAmount);

																lenderReferralBonusUpdatedRepo
																		.save(referralBonusDetails);

																Double sumOfAmountUnpaid = lenderReferenceDetailsRepo
																		.findingAmountForUnpaid(
																				borrowerRefereeInfo.getRefereeId());
																if (sumOfAmountUnpaid != null) {
																	borrowerRefereeInfo.setAmount(sumOfAmountUnpaid);
																	lenderReferenceDetailsRepo
																			.save(borrowerRefereeInfo);
																} else {
																	borrowerRefereeInfo.setAmount(0.0);
																	lenderReferenceDetailsRepo
																			.save(borrowerRefereeInfo);
																}
															}
														}

													}
												}

												logger.info("Referral bonus for borrower ends................");

												logger.info("Referral bonus for lender starts................");

												Integer lenderRefereeId = loan.getLenderUserId();

												LenderReferenceDetails refereeInfo = lenderReferenceDetailsRepo
														.findRefereeInfo(lenderRefereeId);

												if (refereeInfo != null) {

													OxyUserType oxyUser = oxyUserTypeRepo
															.findByIdNum(refereeInfo.getReferrerId());

													User referee = userRepo.findById(refereeInfo.getRefereeId()).get();

													if (oxyUser.getReferralCheck() == OxyUserType.ReferralCheck.YES
															&& referee.getPrimaryType() == User.PrimaryType.LENDER
															&& !referee.getUrchinTrackingModule()
																	.equalsIgnoreCase("WEB")) {

														Double sumOfAmount = lenderReferralBonusUpdatedRepo
																.findingSumOfAmount(lenderRefereeId);

														Double disbursmentAmount = loan.getDisbursmentAmount();

														Double amount = 0.0;

														if (sumOfAmount != null) {
															amount = (disbursmentAmount / 1000);
														} else {

															Double amountForReferee = lenderReferralBonusUpdatedRepo
																	.getAmountForReferee(refereeInfo.getRefereeId());
															if (amountForReferee != null) {
																if (amountForReferee < 1000) {
																	amount = (disbursmentAmount / 100);
																} else if (amountForReferee >= 1000) {
																	amount = (disbursmentAmount / 1000);
																}
															} else {
																amount = (disbursmentAmount / 100);
															}

														}
														refereeInfo.setStatus(LenderReferenceDetails.Status.Disbursed);

														LenderReferralBonusUpdated referralBonusDetails = new LenderReferralBonusUpdated();
														referralBonusDetails
																.setReferrerUserId(refereeInfo.getReferrerId());
														referralBonusDetails.setAmount(amount);
														referralBonusDetails.setRefereeUserId(lenderRefereeId);
														referralBonusDetails.setDealId(0);
														referralBonusDetails.setParticipatedOn(new Date());
														referralBonusDetails.setParticipatedAmount(disbursmentAmount);

														lenderReferralBonusUpdatedRepo.save(referralBonusDetails);

														Double sumOfAmountUnpaid = lenderReferenceDetailsRepo
																.findingAmountForUnpaid(refereeInfo.getRefereeId());
														if (sumOfAmountUnpaid != null) {
															refereeInfo.setAmount(sumOfAmountUnpaid);
															lenderReferenceDetailsRepo.save(refereeInfo);
														} else {
															refereeInfo.setAmount(0.0);
															lenderReferenceDetailsRepo.save(refereeInfo);
														}
													}
												}

												logger.info("Referral bonus for lender ends................");
											}
										}

										Double sumOfDisbursmentAmount = oxyLoanRepo
												.findingSumOfActiveDisbursmentAmount(userId);

										if (sumOfDisbursmentAmount == null) {
											sumOfDisbursmentAmount = 0.0;
										}

										LoanOfferdAmount loanOfferedAmount = loanRequest.getLoanOfferedAmount();

										if (loanOfferedAmount != null) {
											if (Double.compare(sumOfDisbursmentAmount,
													loanOfferedAmount.getLoanOfferedAmount()) == 0) {
												loanOfferedAmount
														.setLoanOfferdStatus(LoanOfferdStatus.LOANOFFERCOMPLETED);
												loanOfferedAmount.setLoanRequest(loanRequest);
												loanRequest.setLoanOfferedAmount(loanOfferedAmount);
												loanRequestRepo.save(loanRequest);
											}

										}

									}
									response.setStatus("Successfully Updated");
								}
							}
						} else {
							throw new ActionNotAllowedException(
									"Already disbursment done for this application APBR" + loanRequest.getId(),
									ErrorCodes.INVALID_OPERATION);
						}
						// }
					} else {
						throw new ActionNotAllowedException(
								"Enach is not activated for this application APBR" + loanRequest.getId(),
								ErrorCodes.INVALID_OPERATION);
					}
				} else {

					List<String> loanIds = oxyLoanRepo.disbursementpending(loanRequest.getId());
					if (loanIds != null && !loanIds.isEmpty()) {
						LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();
						if (Double.compare((loanRequest.getDisbursmentAmount()),
								loanOfferdAmount.getLoanOfferedAmount()) == 0) {
							loanOfferdAmount.setLoanOfferdStatus(LoanOfferdStatus.LOANOFFERCOMPLETED);
							loanOfferdAmount.setLoanRequest(loanRequest);
							loanRequest.setLoanOfferedAmount(loanOfferdAmount);
							loanRequestRepo.save(loanRequest);

						}
						for (int i = 0; i < loanIds.size(); i++) {

							OxyLoan loan = oxyLoanRepo.findByLoanId(loanIds.get(i));

							try {
								loan.setBorrowerDisbursedDate(expectedDateFormat.parse(userRequest.getDisbursedDate()));
							} catch (ParseException e1) {

								e1.printStackTrace();
							}
							loan.setAdminComments(Status.DISBURSED.toString());
							oxyLoanRepo.save(loan);

							logger.info("Referral bonus for borrower starts................");
							Integer borrowerRefereeId = loan.getBorrowerUserId();

							LenderReferenceDetails borrowerRefereeInfo = lenderReferenceDetailsRepo
									.findRefereeInfo(borrowerRefereeId);

							if (borrowerRefereeInfo != null) {

								OxyUserType oxyUser = oxyUserTypeRepo.findByIdNum(borrowerRefereeInfo.getReferrerId());

								User borrowerReferee = userRepo.findByIdNum(borrowerRefereeInfo.getRefereeId());
								if (oxyUser != null || borrowerReferee != null) {
									if ((oxyUser.getReferralCheck() == OxyUserType.ReferralCheck.YES)
											|| borrowerReferee.getUrchinTrackingModule().equalsIgnoreCase("WEB")) {

										if (borrowerReferee.getPrimaryType().equals(User.PrimaryType.BORROWER)) {

											Double disbursmentAmount = loan.getDisbursmentAmount();

											Double amount = 0.0;

											amount = disbursmentAmount / referralBonusByBorrower;

											borrowerRefereeInfo.setStatus(LenderReferenceDetails.Status.Disbursed);

											LenderReferralBonusUpdated referralBonusDetails = new LenderReferralBonusUpdated();

											if (borrowerReferee.getLoanOwner() != null
													|| !borrowerReferee.getLoanOwner().isEmpty()) {
												OxyBorrowersDealsInformation dealInfo = oxyBorrowersDealsInformationRepo
														.findByDealName(borrowerReferee.getLoanOwner());
												if (dealInfo != null) {
													referralBonusDetails.setDealId(dealInfo.getId());
													oxyBorrowersDealsInformationRepo.save(dealInfo);

												} else {
													referralBonusDetails.setDealId(0);
												}
											} else {
												referralBonusDetails.setDealId(0);
											}

											referralBonusDetails.setReferrerUserId(borrowerRefereeInfo.getReferrerId());
											referralBonusDetails.setAmount(amount);
											referralBonusDetails.setRefereeUserId(borrowerRefereeId);
											referralBonusDetails.setParticipatedOn(new Date());
											referralBonusDetails.setParticipatedAmount(disbursmentAmount);

											lenderReferralBonusUpdatedRepo.save(referralBonusDetails);

											Double sumOfAmountUnpaid = lenderReferenceDetailsRepo
													.findingAmountForUnpaid(borrowerRefereeInfo.getRefereeId());
											if (sumOfAmountUnpaid != null) {
												borrowerRefereeInfo.setAmount(sumOfAmountUnpaid);
												lenderReferenceDetailsRepo.save(borrowerRefereeInfo);
											} else {
												borrowerRefereeInfo.setAmount(0.0);
												lenderReferenceDetailsRepo.save(borrowerRefereeInfo);
											}
										}
									}

								}
							}
							logger.info("Referral bonus for borrower ends................");

							logger.info("Referral bonus for lender starts................");

							Integer lenderRefereeId = loan.getLenderUserId();

							LenderReferenceDetails refereeInfo = lenderReferenceDetailsRepo
									.findRefereeInfo(lenderRefereeId);

							if (refereeInfo != null) {

								OxyUserType oxyUser = oxyUserTypeRepo.findByIdNum(refereeInfo.getReferrerId());

								User referee = userRepo.findById(refereeInfo.getRefereeId()).get();

								if (oxyUser.getReferralCheck() == OxyUserType.ReferralCheck.YES
										&& referee.getPrimaryType() == User.PrimaryType.LENDER
										&& !referee.getUrchinTrackingModule().equalsIgnoreCase("WEB")) {

									Double sumOfAmount = lenderReferralBonusUpdatedRepo
											.findingSumOfAmount(lenderRefereeId);

									Double disbursmentAmount = loan.getDisbursmentAmount();

									Double amount = 0.0;

									if (sumOfAmount != null) {
										amount = (disbursmentAmount / 1000);
									} else {

										Double amountForReferee = lenderReferralBonusUpdatedRepo
												.getAmountForReferee(refereeInfo.getRefereeId());
										if (amountForReferee != null) {
											if (amountForReferee < 1000) {
												amount = (disbursmentAmount / 100);
											} else if (amountForReferee >= 1000) {
												amount = (disbursmentAmount / 1000);
											}
										} else {
											amount = (disbursmentAmount / 100);
										}

									}
									refereeInfo.setStatus(LenderReferenceDetails.Status.Disbursed);

									LenderReferralBonusUpdated referralBonusDetails = new LenderReferralBonusUpdated();
									referralBonusDetails.setReferrerUserId(refereeInfo.getReferrerId());
									referralBonusDetails.setAmount(amount);
									referralBonusDetails.setRefereeUserId(lenderRefereeId);
									referralBonusDetails.setDealId(0);
									referralBonusDetails.setParticipatedOn(new Date());
									referralBonusDetails.setParticipatedAmount(disbursmentAmount);

									lenderReferralBonusUpdatedRepo.save(referralBonusDetails);

									Double sumOfAmountUnpaid = lenderReferenceDetailsRepo
											.findingAmountForUnpaid(refereeInfo.getRefereeId());
									if (sumOfAmountUnpaid != null) {
										refereeInfo.setAmount(sumOfAmountUnpaid);
										lenderReferenceDetailsRepo.save(refereeInfo);
									} else {
										refereeInfo.setAmount(0.0);
										lenderReferenceDetailsRepo.save(refereeInfo);
									}
								}
							}

							logger.info("Referral bonus for lender ends................");

							double interestAmountForBorrower = 0.0;
							double interestAmountForLender = 0.0;

							String disbursedDate = userRequest.getDisbursedDate();
							String dateParts[] = disbursedDate.split("/");
							String emiDate = dateParts[0];
							Integer disbursedmentDate = Integer.parseInt(emiDate);
							Calendar c = Calendar.getInstance();

							int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
							int remainingDays = monthMaxDays - disbursedmentDate;

							int id = loan.getId();
							remainingDays = remainingDays + 5;
							List<LoanEmiCard> loanEmiCradDetails = loanEmiCardRepo.findByLoanId(id);
							int differenceInDays = 0;
							String firstEmiDueDate = null;
							if (loan.getDurationType().equals(DurationType.Months)) {
								for (LoanEmiCard firstEmiInformation : loanEmiCradDetails) {
									if (firstEmiInformation.getEmiNumber() == 1) {
										interestAmountForBorrower = firstEmiInformation.getEmiInterstAmount();
										interestAmountForLender = firstEmiInformation.getLenderEmiInterestAmount();

										firstEmiDueDate = expectedDateFormat.format(firstEmiInformation.getEmiDueOn());
										Date dateBefore = null;
										Date dateAfter = null;
										try {
											dateBefore = expectedDateFormat.parse(disbursedDate);
											dateAfter = expectedDateFormat.parse(firstEmiDueDate);
										} catch (ParseException e) {

											e.printStackTrace();
										}

										long difference = dateAfter.getTime() - dateBefore.getTime();
										float daysBetween = (difference / (1000 * 60 * 60 * 24));
										differenceInDays = (int) daysBetween;
									}

								}
								double interestPerDayToBorrower = (interestAmountForBorrower / 30);
								double firstEmiInterestToBorrower = (differenceInDays * interestPerDayToBorrower);

								double interestPerDayToLender = (interestAmountForLender / 30);
								double firstEmiInterestToLender = (differenceInDays * interestPerDayToLender);

								for (LoanEmiCard firstEmiInformation : loanEmiCradDetails) {
									if (firstEmiInformation.getEmiNumber() == 1) {
										firstEmiInformation.setEmiInterstAmount(Math.round(firstEmiInterestToBorrower));
										firstEmiInformation
												.setEmiAmount(Math.round(firstEmiInformation.getEmiPrincipalAmount()
														+ firstEmiInterestToBorrower));

										firstEmiInformation
												.setLenderEmiInterestAmount(Math.round(firstEmiInterestToLender));
										firstEmiInformation.setLenderEmiAmount(
												Math.round(firstEmiInformation.getLenderEmiPrincipalAmount()
														+ firstEmiInterestToLender));

									}
									loanEmiCardRepo.save(firstEmiInformation);
								}

							}

						}
					} else {
						throw new ActionNotAllowedException("no loans to disburse please check" + loanRequest.getId(),
								ErrorCodes.INVALID_OPERATION);
					}

				}
			}
		}
		return response;
	}

	public LenderLoanDetailsResponseDto lenderInterestStatusAndAmount(String emiReceivingDate, String participatedDate,
			Integer userId, Integer dealId, String monthAndYear) {
		LenderLoanDetailsResponseDto lenderLoanDetailsResponseDto = new LenderLoanDetailsResponseDto();
		Date interestDate = null;
		Date paticipated = null;
		try {
			interestDate = new SimpleDateFormat("dd/MM/yyyy").parse(emiReceivingDate);
			paticipated = new SimpleDateFormat("dd/MM/yyyy").parse(participatedDate);
		} catch (ParseException e) {

			e.printStackTrace();
		}
		if (interestDate.after(paticipated)) {
			/*
			 * Double amount = lendersReturnsRepo.interestReturnedBasedOnPaidDate(userId,
			 * dealId, monthAndYear); if (amount != null) {
			 * lenderLoanDetailsResponseDto.setPaymentStatus("Paid");
			 * lenderLoanDetailsResponseDto.setAmountRecevied(amount);
			 * 
			 * LendersReturns lenderReturns =
			 * lendersReturnsRepo.interestReturnedBasedOnPaidDateList(userId, dealId,
			 * monthAndYear); if (lenderReturns != null) { lenderLoanDetailsResponseDto
			 * .setInterestPaidDate(expectedDateFormat.format(lenderReturns.getPaidDate()));
			 * }
			 * 
			 * } else {
			 */
			LendersReturns lendersReturns = lendersReturnsRepo.interestReturnedBasedOnActuvalDate(userId, dealId,
					monthAndYear);
			if (lendersReturns != null) {
				lenderLoanDetailsResponseDto.setPaymentStatus("Paid");
				lenderLoanDetailsResponseDto.setAmountRecevied(lendersReturns.getAmount());
				lenderLoanDetailsResponseDto
						.setInterestPaidDate(expectedDateFormat.format(lendersReturns.getPaidDate()));

			} else {
				LendersReturns lendersAmountReturned = lendersReturnsRepo.uniqueLenderReturns(userId, dealId,
						monthAndYear);
				if (lendersAmountReturned != null) {
					lenderLoanDetailsResponseDto.setPaymentStatus("Paid");
					lenderLoanDetailsResponseDto.setAmountRecevied(lendersAmountReturned.getAmount());
					lenderLoanDetailsResponseDto
							.setInterestPaidDate(expectedDateFormat.format(lendersAmountReturned.getPaidDate()));
				} else {
					lenderLoanDetailsResponseDto.setPaymentStatus("Future");
					lenderLoanDetailsResponseDto.setAmountRecevied(0.0);
				}
			}

			// }
		} else {
			lenderLoanDetailsResponseDto.setPaymentStatus("NotParticipated");
			lenderLoanDetailsResponseDto.setAmountRecevied(0.0);
		}
		return lenderLoanDetailsResponseDto;
	}

	@Override
	@Transactional
	public String agreementsForStudentDeals() {
		DealLevelRequestDto dealLevelRequestDto = new DealLevelRequestDto();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, 1);
		Date nextMonthFirstDay = calendar.getTime();

		String format = dateFormat.format(nextMonthFirstDay);
		String firstStartDate[] = format.split("-");
		StringBuilder date = new StringBuilder();
		date.append(firstStartDate[0]).append("-").append(firstStartDate[1]).append("-").append("05");
		dealLevelRequestDto.setDisbursedDate(date.toString());

		List<Object[]> aggrementsPending = oxyBorrowersDealsInformationRepo.agreementPendingForStudentDeals();
		if (aggrementsPending != null && !aggrementsPending.isEmpty()) {
			for (Object[] deal : aggrementsPending) {
				int dealId = Integer.parseInt(deal[0] == null ? "0" : deal[0].toString());
				OxyBorrowersDealsInformation dealinfo = oxyBorrowersDealsInformationRepo.findDealAlreadyGiven(dealId);
				Double dealAmount = Double.parseDouble(deal[1] == null ? "0.0" : deal[1].toString());
				String dealName = deal[2] == null ? "null" : deal[2].toString();
				Double participatedAmount = oxyBorrowersDealsInformationRepo.paticipatedDealAmount(dealId);
				if (participatedAmount != null) {
					if (dealAmount.equals(participatedAmount)) {
						List<LenderDealsInformation> lendersParticipatedAmount = lendersWithTotalParticipation(dealId);
						if (lendersParticipatedAmount != null && !lendersParticipatedAmount.isEmpty()) {
							Double particiationAmount = 0.0;
							Double loanAmount = 0.0;
							Double lenderAmount = 0.0;
							for (LenderDealsInformation lenderInfo : lendersParticipatedAmount) {

								Integer userId = lenderInfo.getUserId();
								LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
								lenderAmount = lenderInfo.getTotalParticipatedAmount();

								Double amountForAggrments = oxyLoanRepo.checkAlreadyAgreementsAgeneratedToLender(userId,
										dealId);
								if (amountForAggrments != null) {
									particiationAmount = lenderAmount - amountForAggrments;
								} else {
									particiationAmount = lenderAmount;
								}

								List<Integer> listOfBorrersMappedTodeal = userRepo.borroweridsMappedToDeal(dealName);
								int countValueForPaticipation = 0;
								Double remainingAmount = 0.0;
								if (listOfBorrersMappedTodeal != null && !listOfBorrersMappedTodeal.isEmpty()) {
									for (int i = 0; i < listOfBorrersMappedTodeal.size(); i++) {
										if (particiationAmount != null && particiationAmount > 0) {

											int borrowerId = listOfBorrersMappedTodeal.get(i);

											if (dealinfo != null) {
												Double currentApplicationAmount = loanRequestRepo
														.checkCurrentApplication(borrowerId);
												LoanRequest loanRequestForBorrower = loanRequestRepo
														.findByUserIdAndParentRequestIdIsNull(borrowerId);
												if (currentApplicationAmount != null) {
													Double amountForAgreementsGrenerated = oxyLoanRepo
															.checkAlreadyAgreementsAgenerated(borrowerId, dealId);

													int count = lenderBorrowerConversationRepo
															.countByLenderUserIdAndBorrowerUserId(userId, borrowerId);
													Double amount = 0.0;
													if (amountForAgreementsGrenerated != null) {
														amount = amountForAgreementsGrenerated;
													}
													remainingAmount = currentApplicationAmount - amount;
													if (remainingAmount > 0) {
														if (count == 0) {
															LoanRequestDto loanRequestDto = new LoanRequestDto();
															if (particiationAmount >= 50000) {
																loanAmount = 50000.0;
															} else {
																loanAmount = particiationAmount;
															}
															Double totalAmountDisbursed = oxyLoanRepo
																	.getTotalAmountDisbursedForParticularrUsers(userId,
																			borrowerId);
															if (totalAmountDisbursed == null
																	|| totalAmountDisbursed < 50000) {
																loanRequestDto.setLoanRequestAmount(loanAmount);
																loanRequestDto.setRateOfInterest(loanRequestForBorrower
																		.getRateOfInterestToBorrower());
																loanRequestDto.setDuration(
																		loanRequestForBorrower.getDurationBySir());
																loanRequestDto.setDurationType(loanRequestForBorrower
																		.getDurationType().toString());
																loanRequestDto.setRepaymentMethod(loanRequestForBorrower
																		.getRepaymentMethodForBorrower());
																loanRequestDto.setLoanPurpose("personal loan");
																loanRequestDto.setExpectedDate(expectedDateFormat
																		.format(dealinfo.getFundsAcceptanceEndDate()));
																loanRequestDto.setParentRequestId(loanRequest.getId());
																LoanResponseForDealsDto loanRequestForDeals = adminLoanService
																		.loanRequestForDeals(borrowerId,
																				loanRequestDto);
																Integer loanRequestId = loanRequestForDeals
																		.getLoanRequestId();

																try {
																	adminLoanService.actOnRequestForDeals(userId,
																			loanRequestId, "Accepted", dealId,
																			dealLevelRequestDto);
																} catch (PdfGeenrationException e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																} catch (IOException e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}

																particiationAmount = particiationAmount - (50000);
																if (particiationAmount == 0 || particiationAmount < 0) {
																	OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
																			.toCheckUserAlreadyInvoledInDeal(userId,
																					dealId);
																	oxyLendersAcceptedDeals.setParticipationState(
																			OxyLendersAcceptedDeals.ParticipationState.PARTICIPATED);
																	oxyLendersAcceptedDealsRepo
																			.save(oxyLendersAcceptedDeals);
																}
															}
														}
													}
												}
											}
										}

									}

								}

							}
						}

					}
				}
				Double amountInActiveStatus = oxyLoanRepo.agreementsGeneratedToDealAndActiveStatus(dealId);
				if (amountInActiveStatus != null) {
					if (dealAmount.equals(amountInActiveStatus)) {
						dealinfo.setAgreementsStatus(OxyBorrowersDealsInformation.AgreementsStatus.GENERATED);
						oxyBorrowersDealsInformationRepo.save(dealinfo);
					}
				}

			}

		}
		return "Success";

	}

	public List<LenderDealsInformation> lendersWithTotalParticipation(int dealId) {
		List<LenderDealsInformation> lendersParticiated = new ArrayList<LenderDealsInformation>();
		List<Object[]> object = oxyLendersAcceptedDealsRepo.getListOfLendersByDealId(dealId);
		if (object != null && !object.isEmpty()) {
			for (Object[] e : object) {
				LenderDealsInformation lenderDealsInformation = new LenderDealsInformation();
				int userId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
				double firstparticipation = Double.parseDouble(e[1] == null ? "0.0" : e[1].toString());
				Double secondParticipationUpdation = lendersPaticipationUpdationRepo
						.getSumOfLenderUpdatedAmountToDeal(userId, dealId);
				if (secondParticipationUpdation != null) {
					lenderDealsInformation.setTotalParticipatedAmount(firstparticipation + secondParticipationUpdation);
				} else {
					lenderDealsInformation.setTotalParticipatedAmount(firstparticipation);
				}
				lenderDealsInformation.setUserId(userId);
				lendersParticiated.add(lenderDealsInformation);
			}

		}
		Collections.sort(lendersParticiated, new Comparator<LenderDealsInformation>() {
			@Override
			public int compare(LenderDealsInformation L1, LenderDealsInformation L2) {
				int result = L1.getTotalParticipatedAmount().compareTo(L2.getTotalParticipatedAmount());
				return result;
			}
		});
		return lendersParticiated;

	}

	@Override
	public List<DealInformationResponseDto> paticipationNotAchivedDealsInfo() {

		List<DealInformationResponseDto> openDeals = new ArrayList<DealInformationResponseDto>();

		List<OxyBorrowersDealsInformation> openDealsInfo = oxyBorrowersDealsInformationRepo
				.findByDealParticipationStatus(OxyBorrowersDealsInformation.DealParticipationStatus.NOTATACHIEVED);

		if (openDealsInfo != null && !openDealsInfo.isEmpty()) {

			openDealsInfo.stream()
					.filter(dealsInfo -> !dealsInfo.getDealType().equals(OxyBorrowersDealsInformation.DealType.TEST))
					.forEach(deals -> {
						double totalPaticipation = 0.0;
						List<OxyLendersAcceptedDeals> oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
								.findByDealId(deals.getId());
						if (oxyLendersAcceptedDeals != null && !oxyLendersAcceptedDeals.isEmpty()) {
							totalPaticipation += oxyLendersAcceptedDeals.stream()
									.mapToDouble(OxyLendersAcceptedDeals::getParticipatedAmount).sum();
						}
						List<LendersPaticipationUpdation> lendersPaticipationUpdation = lendersPaticipationUpdationRepo
								.findByDealId(deals.getId());
						if (lendersPaticipationUpdation != null && !lendersPaticipationUpdation.isEmpty()) {
							totalPaticipation += lendersPaticipationUpdation.stream()
									.mapToDouble(LendersPaticipationUpdation::getUpdationAmount).sum();
						}

						DealInformationResponseDto dealInformationResponseDto = new DealInformationResponseDto();
						dealInformationResponseDto.setDealId(deals.getId());
						dealInformationResponseDto.setDealName(deals.getDealName());
						dealInformationResponseDto.setDealAmount(deals.getDealAmount());
						dealInformationResponseDto.setPaticipatedAmount(totalPaticipation);
						dealInformationResponseDto.setRemainingAmount(deals.getDealAmount() - totalPaticipation);
						openDeals.add(dealInformationResponseDto);
					});

			FileOutputStream outFile = null;

			HSSFWorkbook workBook = new HSSFWorkbook();
			HSSFSheet spreadSheet = workBook.createSheet("OPENDEALS");
			HSSFFont headerFont = workBook.createFont();

			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 15);
			headerFont.setColor(IndexedColors.BLACK.index);

			spreadSheet.createFreezePane(0, 1);

			Row row0 = spreadSheet.createRow(0);

			CellStyle style = workBook.createCellStyle();
			style.setFont(headerFont);
			style.setFillForegroundColor(IndexedColors.RED.getIndex());

			Font font = workBook.createFont();// Create font
			font.setBold(true);
			style.setFont(font);

			Cell cell = row0.createCell(0);
			cell.setCellValue("Deal Id");
			cell.setCellStyle(style);

			cell = row0.createCell(1);
			cell.setCellValue("Deal Name");
			cell.setCellStyle(style);

			cell = row0.createCell(2);
			cell.setCellValue("Deal Amount");
			cell.setCellStyle(style);

			cell = row0.createCell(3);
			cell.setCellValue("Deal Paticipated Amount");
			cell.setCellStyle(style);

			cell = row0.createCell(4);
			cell.setCellValue("Deal Remaining");
			cell.setCellStyle(style);

			for (int i = 0; i <= 10; i++) {
				spreadSheet.autoSizeColumn(i);
			}
			int rowCount = 0;

			for (DealInformationResponseDto deals : openDeals) {
				Row row1 = spreadSheet.createRow(++rowCount);
				Cell cell1 = row1.createCell(0);
				cell1.setCellValue(deals.getDealId());

				cell1 = row1.createCell(1);
				cell1.setCellValue(deals.getDealName());

				cell1 = row1.createCell(2);
				cell1.setCellValue(deals.getDealAmount());

				cell1 = row1.createCell(3);
				cell1.setCellValue(deals.getPaticipatedAmount());

				cell1 = row1.createCell(4);
				cell1.setCellValue(deals.getRemainingAmount());
			}

			File f = new File(iciciReportsManualFolder + "OPENDEALS" + ".xls");

			try {
				outFile = new FileOutputStream(f);

				workBook.write(outFile);

				workBook.close();
				outFile.close();

				FileInputStream targetStream = new FileInputStream(f);

				FileRequest fileRequest = new FileRequest();

				fileRequest.setFilePrifix(FileType.OPENDEALS.name());
				fileRequest.setInputStream(targetStream);
				fileRequest.setFileName(f.getName());
				FileResponse putFile = fileManagementService.putFile(fileRequest);

				FileResponse getFile = fileManagementService.getFile(fileRequest);

				Calendar current = Calendar.getInstance();
				TemplateContext templateContext = new TemplateContext();
				String expectedCurrentDate1 = expectedDateFormat.format(current.getTime());
				templateContext.put("currentDate", expectedCurrentDate1);
				templateContext.put("$message", "");
				String tableBody = openDealstableBody(openDeals);
				templateContext.put("tableBody", tableBody);

				String mailSubject = "Open deals";

				EmailRequest emailRequest = new EmailRequest(
						new String[] { "radhakrishna.t@oxyloans.com", "archana.n@oxyloans.com", "narendra@oxyloans.com",
								"ramadevi@oxyloans.com" },
						"open-deals.template", templateContext, mailSubject, new String[] { f.getAbsolutePath() });

				EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
				if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
					try {
						throw new MessagingException(emailResponseFromService.getErrorMessage());
					} catch (MessagingException e2) {

						e2.printStackTrace();
					}
				}

				// sending mail ends

			} catch (Exception ex) {
				ex.getMessage();
			}
		}
		return openDeals;

	}

	public String openDealstableBody(List<DealInformationResponseDto> openDeals) {
		String body = "";
		for (DealInformationResponseDto dealInformationResponseDto : openDeals) {
			body = body + " <tr>\r\n" + " <td style=\"border: 1px solid black;\r\n" + "  border-collapse: collapse;\" >"
					+ dealInformationResponseDto.getDealId() + "</td>\r\n" + " <td style=\"border: 1px solid black;\r\n"
					+ "  border-collapse: collapse;\" >" + dealInformationResponseDto.getDealName() + "</td>\r\n"
					+ " <td style=\"border: 1px solid black;\r\n" + "  border-collapse: collapse;\" >"
					+ BigDecimal.valueOf(dealInformationResponseDto.getDealAmount()).toBigInteger() + "</td>\r\n"
					+ " <td style=\"border: 1px solid black;\r\n" + "  border-collapse: collapse;\" >"
					+ BigDecimal.valueOf(dealInformationResponseDto.getPaticipatedAmount()).toBigInteger() + "</td>\r\n"
					+ " <td style=\"border: 1px solid black;\r\n" + "  border-collapse: collapse;\" >"
					+ BigDecimal.valueOf(dealInformationResponseDto.getRemainingAmount()).toBigInteger() + "</td>\r\n"
					+ "  </tr>\r\n" + "";
		}
		return body;

	}

	@Override
	@Transactional
	public String sendOfferToborrowers() {
		List<Object[]> aggrementsPending = oxyBorrowersDealsInformationRepo.agreementPendingForStudentDeals();
		if (aggrementsPending != null && !aggrementsPending.isEmpty()) {
			for (Object[] deal : aggrementsPending) {
				int dealId = Integer.parseInt(deal[0] == null ? "0" : deal[0].toString());
				OxyBorrowersDealsInformation dealinfo = oxyBorrowersDealsInformationRepo.findDealAlreadyGiven(dealId);

				if (dealinfo != null) {
					Double participatedAmount = oxyBorrowersDealsInformationRepo.paticipatedDealAmount(dealId);
					if (participatedAmount != null) {
						if (dealinfo.getDealAmount().equals(participatedAmount)) {

							String userIdsInput = dealinfo.getBorrowersIdsMappedToDeal();
							String amountsInput = dealinfo.getBorrowersIdsMappedToDeal();
							if (userIdsInput != null && amountsInput != null) {
								BorrowersDealsRequestDto borrowersDealsRequestDto = new BorrowersDealsRequestDto();
								borrowersDealsRequestDto.setDealSubtype(dealinfo.getDealSubType().toString());
								borrowersDealsRequestDto.setDealName(dealinfo.getDealName());
								OxyDealsRateofinterestToLendersgroup groupZero = oxyDealsRateofinterestToLendersgroupRepo
										.getDealMappedToSingleGroup(dealId, 0);
								if (groupZero != null) {
									borrowersDealsRequestDto
											.setOxyPremiumMonthlyInterest(groupZero.getMonthlyInterest());
								}
								OxyDealsRateofinterestToLendersgroup groupThird = oxyDealsRateofinterestToLendersgroupRepo
										.getDealMappedToSingleGroup(dealId, 3);
								if (groupThird != null) {
									borrowersDealsRequestDto
											.setNewLendersMonthlyInterest(groupThird.getMonthlyInterest());
								}
								borrowersDealsRequestDto.setDuration(dealinfo.getDuration());
								borrowersDealsRequestDto
										.setBorrowerRateOfInterest(dealinfo.getBorrowerRateofinterest());
								borrowersDealsRequestDto
										.setParticipationLimitToLenders(dealinfo.getPaticipationLimitToLenders());
								borrowersDealsRequestDto
										.setIdsWithLoanAmount(mapUserIdsToAmounts(userIdsInput, amountsInput));
								adminLoanService.automaticSendingOffer(borrowersDealsRequestDto);
							}
						}
					}
				}
			}
		}
		return "update";
	}

	private List<DealLevelRequestDto> mapUserIdsToAmounts(String userIdsInput, String amountsInput) {
		List<DealLevelRequestDto> listOfLoanAmount = new ArrayList<DealLevelRequestDto>();
		String[] userIds = userIdsInput.split(",");
		String[] amounts = amountsInput.split(",");

		for (int i = 0; i < userIds.length && i < amounts.length; i++) {
			DealLevelRequestDto dealLevelRequestDto = new DealLevelRequestDto();
			dealLevelRequestDto.setBorrowerId(Integer.parseInt(userIds[i]));
			dealLevelRequestDto.setLoanAmount(Double.parseDouble(amounts[i]));

			listOfLoanAmount.add(dealLevelRequestDto);
		}

		return listOfLoanAmount;
	}
}
