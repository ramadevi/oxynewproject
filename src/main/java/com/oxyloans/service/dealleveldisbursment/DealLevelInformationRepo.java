package com.oxyloans.service.dealleveldisbursment;

import java.util.List;

import com.oxyloans.entityborrowersdealsdto.BorrowersDealsList;
import com.oxyloans.entityborrowersdealsdto.DealDisbursmentInformation;
import com.oxyloans.entityborrowersdealsdto.DealInformationResponseDto;
import com.oxyloans.entityborrowersdealsdto.DealLevelLoanEmiCardInformation;
import com.oxyloans.entityborrowersdealsdto.DealLevelRequestDto;
import com.oxyloans.entityborrowersdealsdto.DealLevelResponseDto;
import com.oxyloans.entityborrowersdealsdto.LenderGroupNamesRequestDto;
import com.oxyloans.entityborrowersdealsdto.LenderTotalParticipationInterestDetails;
import com.oxyloans.entityborrowersdealsdto.LendersDealsInformation;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.PaginationRequestDto;

public interface DealLevelInformationRepo {

	public DealDisbursmentInformation getDealParticipationAndDisbursmentPendingInfo(int dealId);

	public DealLevelResponseDto dealLevelDisbursment(int dealId, DealLevelRequestDto dealLevelRequestDto);

	public DealLevelLoanEmiCardInformation getDealBasedLoanEmiDetails(int userId, int dealId);

	public BorrowersDealsList getListOfDealsInformationForEquity(int userId, PaginationRequestDto pageRequestDto);

	public LendersDealsInformation getLendersGroupBasedDealAmounts(
			LenderGroupNamesRequestDto lenderGroupNamesRequestDto);

	public LenderTotalParticipationInterestDetails getPaticipationUpdatedInfo(String monthAndYear,
			String emiReceivingDate, Integer dealId, double rateOfInterest, String lenderReturnType,
			double totalInterest, Integer userId, Integer emiNumber);

	public DealLevelLoanEmiCardInformation loanEmiCardBasedOnCurrentValue(int userId, int dealId);

	public DealLevelResponseDto dealLevelDisbursmentForAppLevel(Integer userId, UserRequest userRequest);

	public String agreementsForStudentDeals();

	public List<DealInformationResponseDto> paticipationNotAchivedDealsInfo();

	public String sendOfferToborrowers();

}
