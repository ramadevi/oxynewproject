package com.oxyloans.service.experinservice;

import com.oxyloans.experian.ExperianRequestDto;
import com.oxyloans.experian.ExperianResponceDto;

public interface ExperianService {

public ExperianResponceDto singileAction(Integer userId,ExperianRequestDto experianRequestDto) throws Exception ;

public ExperianResponceDto getExperian(Integer userId);
}
