package com.oxyloans.service.experinservice;

import com.oxyloans.service.OxyRuntimeException;

public class ExperianException extends OxyRuntimeException {

	private static final long serialVersionUID = 1L;

	public ExperianException(String message, String errorCode) {
		super(message, errorCode);
		// TODO Auto-generated constructor stub
	}

}
