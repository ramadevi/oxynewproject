package com.oxyloans.service.experinservice;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.entity.experian.ExperianSummary;
import com.oxyloans.entity.user.User;
import com.oxyloans.experian.ExperianRequestDto;
import com.oxyloans.experian.ExperianResponceDto;
import com.oxyloans.file.FileRequest;
import com.oxyloans.file.FileRequest.FileType;
import com.oxyloans.file.FileResponse;
import com.oxyloans.repo.loan.OxyLoanRequestRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.service.file.IFileManagementService;
import com.oxyloans.service.loan.ActionNotAllowedException;
import com.oxyloans.service.user.UserServiceImpl;

@Service
public class ExperianServiceImpl implements ExperianService {

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private OxyLoanRequestRepo loanRequestRepo;

	@Autowired
	protected IFileManagementService fileManagementService;

	@Value("${singleActionUrl}")
	private String singleActionUrl;

	@Value("${voucherCode}")
	private String voucherCode;

	@Value("${clientName}")
	private String clientName;

	private SimpleDateFormat expectedDate = new SimpleDateFormat("dd/MM/yyyy");
	private SimpleDateFormat requiredDate = new SimpleDateFormat("dd-MMM-yyyy");

	
	@Override
	@Transactional
	public ExperianResponceDto singileAction(Integer userId, ExperianRequestDto experianRequestDto) throws Exception {

		User user = userRepo.findById(userId).get();

		if (user == null) {
			logger.info("User is not Exist", userId);
			throw new ActionNotAllowedException("User is not Exist", ErrorCodes.PERMISSION_DENIED);

		}
		if (loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId()) == null) {
			logger.info("Loan request Could not be null", user.getId());
			throw new OperationNotAllowedException("Loan request Could not be null", ErrorCodes.LOAN_REQUEST_NULL);

		}
		if (user.getExperianSummary() == null) {
			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("clientName", clientName));
			params.add(new BasicNameValuePair("allowInput", "1"));
			params.add(new BasicNameValuePair("allowEdit", "1"));
			params.add(new BasicNameValuePair("allowCaptcha", "1"));
			params.add(new BasicNameValuePair("allowConsent", "1"));
			params.add(new BasicNameValuePair("allowEmailVerify", "1"));
			params.add(new BasicNameValuePair("allowVoucher", "1"));
			params.add(new BasicNameValuePair("voucherCode", voucherCode));
			params.add(new BasicNameValuePair("firstName", experianRequestDto.getFirstName()));
			params.add(new BasicNameValuePair("surName", experianRequestDto.getSurName()));
			params.add(new BasicNameValuePair("dateOfBirth", experianRequestDto.getDateOfBirth()));
			params.add(new BasicNameValuePair("gender", experianRequestDto.getGender()));
			params.add(new BasicNameValuePair("mobileNo", experianRequestDto.getMobileNo()));
			params.add(new BasicNameValuePair("email", experianRequestDto.getEmail()));
			params.add(new BasicNameValuePair("flatno", experianRequestDto.getFlatno()));
			params.add(new BasicNameValuePair("city", experianRequestDto.getCity()));
			params.add(new BasicNameValuePair("state", experianRequestDto.getState()));
			params.add(new BasicNameValuePair("pincode", experianRequestDto.getPincode()));
			params.add(new BasicNameValuePair("pan", experianRequestDto.getPan()));
			params.add(new BasicNameValuePair("reason", "test"));
			params.add(new BasicNameValuePair("middleName", ""));
			params.add(new BasicNameValuePair("telephoneNo", experianRequestDto.getTelephoneNo()));
			params.add(new BasicNameValuePair("telephoneType", "1"));
			params.add(new BasicNameValuePair("passport", ""));
			params.add(new BasicNameValuePair("voterid", ""));
			params.add(new BasicNameValuePair("aadhaar", ""));
			params.add(new BasicNameValuePair("driverlicense", ""));
			params.add(new BasicNameValuePair("noValidationByPass", "0"));
			params.add(new BasicNameValuePair("emailConditionalByPass", "1"));
			String request = getQuery(params);
			System.out.println("Request" + request);
			ExperianResponceDto experianResponceDto = actionUrlExecution(user, request, "OXYLOANS_FM");
			return experianResponceDto;
		}

		else {
			throw new ExperianException("user alredy has experina score", ErrorCodes.EXPERIAN_SCORE_NOT_NULL);
		}
	}

	private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for (NameValuePair pair : params) {
			if (first)
				first = false;
			else
				result.append("&");
			if (pair.getName() != null) {
				result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
			}
			result.append("=");
			if (pair.getValue() != null) {
				result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
			}
		}
		return result.toString();
	}

	public ExperianResponceDto actionUrlExecution(User user, String request, String clientName) throws Exception {

		logger.info("User is hitting SingleAction", user.getId());
		String url = singleActionUrl;
		System.out.println("Call url : " + url);
		URL u = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) u.openConnection();
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setConnectTimeout(3000);
		conn.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
		conn.setRequestProperty("Referer", clientName);
		OutputStream os = conn.getOutputStream();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
		writer.write(request);
		writer.flush();
		writer.close();
		os.close();
		InputStream in = conn.getInputStream();
		String encoding = conn.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		String body = IOUtils.toString(in, encoding);
		ObjectMapper ObjectMapper = new ObjectMapper();
		Map<String, String> map = ObjectMapper.readValue(body, Map.class);
		if (map.get("errorString") != null) {
			throw new ActionNotAllowedException(map.get("errorString"), ErrorCodes.PERMISSION_DENIED);
		}

		String showHtmlReportForCreditReport = map.get("showHtmlReportForCreditReport").toString();

		System.out.println("showHtmlReportForCreditReport" + showHtmlReportForCreditReport);
		String converingTopureXml = showHtmlReportForCreditReport.replace("&lt;", "<");
		String converingTopureXml1 = converingTopureXml.replace("&quot;", "\"");
		String finalXml = converingTopureXml1.replace("&gt;", ">");

		System.out.println("finalXml" + finalXml);

		JSONObject convertedjsonObject = XML.toJSONObject(finalXml);

		Integer score = (Integer) convertedjsonObject.getJSONObject("INProfileResponse").getJSONObject("SCORE")
				.get("BureauScore");
		JSONObject cAISSummary = convertedjsonObject.getJSONObject("INProfileResponse").getJSONObject("CAIS_Account")
				.getJSONObject("CAIS_Summary");
		JSONObject creditAccount = cAISSummary.getJSONObject("Credit_Account");
		Integer creditAccountTotal = (Integer) creditAccount.get("CreditAccountTotal");
		Integer creditAccountActive = (Integer) creditAccount.get("CreditAccountActive");
		Integer creditAccountClosed = (Integer) creditAccount.get("CreditAccountClosed");

		JSONObject totalOutstandingBalance = cAISSummary.getJSONObject("Total_Outstanding_Balance");
		Integer outstandingBalanceAll = (Integer) totalOutstandingBalance.get("Outstanding_Balance_All");
		Integer outstandingBalanceSecured = (Integer) totalOutstandingBalance.get("Outstanding_Balance_Secured");
		Integer outstandingBalanceUnSecured = (Integer) totalOutstandingBalance.get("Outstanding_Balance_UnSecured");

		Integer oxyScore = (1000 * score) / 900;

		System.out.println("oxyScore" + oxyScore);

		String fileName = user.getId() + ".txt";

		FileRequest fileRequest = new FileRequest();
		fileRequest.setInputStream(new ByteArrayInputStream(convertedjsonObject.toString().getBytes()));
		fileRequest.setFileName(fileName);
		fileRequest.setFileType(FileType.EXPERIAN);
		fileRequest.setFilePrifix(FileType.EXPERIAN.toString());
		fileRequest.setUserId(user.getId());

		try {
			logger.info("experian Report is saving in s3", user.getId());
			FileResponse fileResponse = fileManagementService.putFile(fileRequest);
			ExperianSummary experianSummary = user.getExperianSummary();
			if (experianSummary == null) {
				experianSummary = new ExperianSummary();
				experianSummary.setUserId(user.getId());
				experianSummary.setExperianFilePath(fileResponse.getFilePath());
				experianSummary.setCreditAccountActive(creditAccountActive);
				experianSummary.setScore(score);
				experianSummary.setFileName(fileResponse.getFileName());
				experianSummary.setCreditAccountClosed(creditAccountClosed);
				experianSummary.setCreditAccountTotal(creditAccountTotal);
				experianSummary.setOutstandingBalanceAll(outstandingBalanceAll);
				experianSummary.setOutstandingBalanceSecured(outstandingBalanceSecured);
				experianSummary.setOutstandingBalanceUnSecured(outstandingBalanceUnSecured);
				experianSummary.setOxyScore(oxyScore);
				experianSummary.setUser(user);
				user.setExperianSummary(experianSummary);
				user = userRepo.save(user);

			} else {
				experianSummary.setExperianFilePath(fileResponse.getFilePath());
				experianSummary.setCreditAccountActive(creditAccountActive);
				experianSummary.setScore(score);
				experianSummary.setFileName(fileResponse.getFileName());
				experianSummary.setCreditAccountClosed(creditAccountClosed);
				experianSummary.setCreditAccountTotal(creditAccountTotal);
				experianSummary.setOutstandingBalanceAll(outstandingBalanceAll);
				experianSummary.setOutstandingBalanceSecured(outstandingBalanceSecured);
				experianSummary.setOutstandingBalanceUnSecured(outstandingBalanceUnSecured);
				experianSummary.setOxyScore(oxyScore);
				user.setExperianSummary(experianSummary);

			}

		} catch (Exception e) {

		}
		ExperianResponceDto experianResponceDto = new ExperianResponceDto();
		experianResponceDto.setScore(score);
		experianResponceDto.setCreditAccountTotal(creditAccountTotal);
		experianResponceDto.setCreditAccountActive(creditAccountActive);
		experianResponceDto.setCreditAccountClosed(creditAccountClosed);
		experianResponceDto.setOutstandingBalanceAll(outstandingBalanceAll);
		experianResponceDto.setOutstandingBalanceSecured(outstandingBalanceSecured);
		experianResponceDto.setOutstandingBalanceUnSecured(outstandingBalanceUnSecured);
		FileRequest fileRequest2 = new FileRequest();
		fileRequest2.setFileName(fileName);
		fileRequest2.setFileType(FileType.EXPERIAN);
		fileRequest2.setFilePrifix(FileType.EXPERIAN.toString());
		fileRequest2.setUserId(user.getId());
		int status = conn.getResponseCode();
		if (status == HttpURLConnection.HTTP_OK) {
			Map<String, List<String>> rHeaders = conn.getHeaderFields();
			List<String> cookies = rHeaders.get("Set-Cookie");

			for (String cookie : cookies) {
				if (cookie.indexOf("JSESSIONID") >= 0) {
					return experianResponceDto;
				}
			}
		}

		return experianResponceDto;
	}

	@Override
	public ExperianResponceDto getExperian(Integer userId) {

		User user = userRepo.findById(userId).get();

		if (user == null) {
			throw new OperationNotAllowedException("User is Null", ErrorCodes.USER_NOT_FOUND);
		}
		ExperianSummary experianSummary = user.getExperianSummary();
		if (experianSummary != null) {
			ExperianResponceDto experianResponceDto = new ExperianResponceDto();
			experianResponceDto.setScore(experianSummary.getScore());
			experianResponceDto.setCreditAccountActive(experianSummary.getCreditAccountActive());
			experianResponceDto.setCreditAccountClosed(experianSummary.getCreditAccountClosed());
			experianResponceDto.setCreditAccountTotal(experianSummary.getCreditAccountTotal());
			experianResponceDto.setOutstandingBalanceAll(experianSummary.getOutstandingBalanceAll());
			experianResponceDto.setOutstandingBalanceSecured(experianSummary.getOutstandingBalanceSecured());
			experianResponceDto.setOutstandingBalanceUnSecured(experianSummary.getOutstandingBalanceUnSecured());

			return experianResponceDto;
		} else {
			throw new OperationNotAllowedException("User don't have experian", ErrorCodes.ENITITY_NOT_FOUND);
		}
	}

}
