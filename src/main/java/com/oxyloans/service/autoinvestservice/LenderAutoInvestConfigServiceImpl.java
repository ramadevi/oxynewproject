package com.oxyloans.service.autoinvestservice;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oxyloans.autoinvest.AutoInvestBorrowersList;
import com.oxyloans.autoinvest.AutoInvestConfigResponse;
import com.oxyloans.autoinvest.AutoInvestResponseDto;
import com.oxyloans.autoinvest.LenderAutoInvestConfigRequest;
import com.oxyloans.autoinvest.LenderAutoInvestConfigResponse;
import com.oxyloans.autoinvest.LenderAutoInvestConfigSearchRequest;
import com.oxyloans.autoinvest.entity.LenderAutoInvestConfig;
import com.oxyloans.autoinvest.entity.LenderAutoInvestConfigArchive;
import com.oxyloans.autoinvest.repo.LenderAutoInvestConfigArchiveRepo;
import com.oxyloans.autoinvest.repo.LenderAutoInvestConfigNativeRepo;
import com.oxyloans.autoinvest.repo.LenderAutoInvestConfigRepo;
import com.oxyloans.common.util.DateUtil;
import com.oxyloans.customexceptions.DataFormatException;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.dashboard.LenderWalletHistoryResponseDto;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletNativeRepo;
import com.oxyloans.entity.lender.oxywallet.LenderTransactionHistory;
import com.oxyloans.entity.lender.oxywallet.LenderTransactionHistoryRepo;
import com.oxyloans.entity.loan.LoanRequest;
import com.oxyloans.entity.loan.OxyLoan;
import com.oxyloans.entity.loan.LoanRequest.LoanStatus;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.UserProfileRisk;
import com.oxyloans.repo.loan.LenderBorrowerConversationRepo;
import com.oxyloans.repo.loan.OxyLoanRepo;
import com.oxyloans.repo.loan.OxyLoanRequestRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.response.user.RiskProfileDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.service.loan.ActionNotAllowedException;
import com.oxyloans.service.loan.LoanServiceFactory;
import com.oxyloans.serviceloan.LoanRequestDto;
import com.oxyloans.serviceloan.LoanResponseDto;
import com.oxyloans.service.user.UserServiceImpl;
import com.oxyloans.whatapp.service.WhatappService;

@Service
public class LenderAutoInvestConfigServiceImpl implements LenderAutoInvestConfigServive {

	private static final Logger logger = LogManager.getLogger(LenderAutoInvestConfigServiceImpl.class);

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Autowired
	private LenderAutoInvestConfigRepo lenderAutoInvestConfigRepo;

	@Autowired
	private LenderAutoInvestConfigArchiveRepo lenderAutoInvestConfigRepoArchive;

	@Autowired
	private LenderAutoInvestConfigNativeRepo lenderAutoInvestConfigNativeRepo;

	@Autowired
	private LenderOxyWalletNativeRepo lenderOxyWalletNativeRepo;

	@Autowired
	private LoanServiceFactory loanServiceFactory;

	@Autowired
	protected OxyLoanRequestRepo loanRequestRepo;

	@Autowired
	protected OxyLoanRepo oxyLoanRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	protected LenderBorrowerConversationRepo lenderBorrowerConversationRepo;

	@Autowired
	private LenderAutoInvestConfigArchiveRepo lenderAutoInvestConfigArchiveRepo;

	@Autowired
	private LenderTransactionHistoryRepo lenderTransactionHistoryRepo;

	DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Autowired
	private UserServiceImpl userServiceImpl;
	
	@Autowired
	private WhatappService whatappService;

	@Override
	@Transactional
	public LenderAutoInvestConfigResponse saveAutoInvestConfig(LenderAutoInvestConfigRequest request) {
		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl saveAutoInvestConfig method start!!!!!!!!!");
		LenderAutoInvestConfigResponse lenderAutoInvestConfigResponse = new LenderAutoInvestConfigResponse();
		LenderAutoInvestConfig lenderAutoInvestConfig = lenderAutoInvestConfigRepo
				.autoLenderAlreadyEnabled(request.getUserId());
		if (lenderAutoInvestConfig != null) {
			lenderAutoInvestConfig.setDealType(LenderAutoInvestConfig.DealType.valueOf(request.getDealType()));
			lenderAutoInvestConfig.setPrincipalReturningAccountType(LenderAutoInvestConfig.PrincipalReturningAccountType
					.valueOf(request.getPrincipalReturningAccountType()));
			lenderAutoInvestConfigRepo.save(lenderAutoInvestConfig);
			LenderAutoInvestConfigArchive lenderAutoInvestHistory = new LenderAutoInvestConfigArchive();
			lenderAutoInvestHistory.setUserId(request.getUserId());
			lenderAutoInvestHistory.setDealType(LenderAutoInvestConfigArchive.DealType.valueOf(request.getDealType()));
			lenderAutoInvestHistory.setCreatedOn(new Date());
			lenderAutoInvestHistory.setStatus("ACTIVE");
			lenderAutoInvestHistory
					.setPrincipalReturningAccountType(LenderAutoInvestConfigArchive.PrincipalReturningAccountType
							.valueOf(request.getPrincipalReturningAccountType()));
			lenderAutoInvestConfigArchiveRepo.save(lenderAutoInvestHistory);
			lenderAutoInvestConfigResponse.setId(lenderAutoInvestConfig.getId());
		} else {
			LenderAutoInvestConfig saveConfig = new LenderAutoInvestConfig();
			saveConfig.setUserId(request.getUserId());
			saveConfig.setDealType(LenderAutoInvestConfig.DealType.valueOf(request.getDealType()));
			saveConfig.setCreatedOn(new Date());
			saveConfig.setStatus("ACTIVE");
			saveConfig.setPrincipalReturningAccountType(LenderAutoInvestConfig.PrincipalReturningAccountType
					.valueOf(request.getPrincipalReturningAccountType()));
			lenderAutoInvestConfigRepo.save(saveConfig);
			LenderAutoInvestConfigArchive lenderAutoInvestHistory = new LenderAutoInvestConfigArchive();
			lenderAutoInvestHistory.setUserId(request.getUserId());
			lenderAutoInvestHistory.setDealType(LenderAutoInvestConfigArchive.DealType.valueOf(request.getDealType()));
			lenderAutoInvestHistory.setCreatedOn(new Date());
			lenderAutoInvestHistory.setStatus("ACTIVE");
			lenderAutoInvestHistory
					.setPrincipalReturningAccountType(LenderAutoInvestConfigArchive.PrincipalReturningAccountType
							.valueOf(request.getPrincipalReturningAccountType()));
			lenderAutoInvestConfigArchiveRepo.save(lenderAutoInvestHistory);
			lenderAutoInvestConfigResponse.setId(saveConfig.getId());
		}

		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl saveAutoInvestConfig method end!!!!!!!!!");
		return lenderAutoInvestConfigResponse;
	}

	@Override
	public LenderAutoInvestConfigResponse getAutoInvestConfigByLenderId(Integer userId) {

		logger.info(" !!!!!!!! getAutoInvestConfigByLenderId  method start!!!!!!!!!");
		if (userId == null || userId == 0) {
			throw new ActionNotAllowedException("No user id present in the request.", ErrorCodes.INVALID_OPERATION);
		}
		LenderAutoInvestConfig respconfig = lenderAutoInvestConfigRepo.findByUserId(userId);
		if (respconfig == null) {
			throw new ActionNotAllowedException("Auto-Invest config not found this user.",
					ErrorCodes.INVALID_OPERATION);
		}

		LenderAutoInvestConfigResponse response = new LenderAutoInvestConfigResponse();

		response.setMessage("Success");
		response.setId(respconfig.getId());
		response.setUserId(respconfig.getUserId());
		response.setRiskCategory(respconfig.getRiskCategory());
		response.setGender(respconfig.getGender());
		response.setOxyScore(respconfig.getOxyScore());
		response.setCity(respconfig.getCity());
		response.setEmploymentType(respconfig.getEmploymentType());
		response.setSalaryRange(respconfig.getSalaryRange());
		response.setDuration(respconfig.getDuration());
		response.setMaxAmount(respconfig.getMaxAmount());
		response.setRateOfInterest(respconfig.getRateOfInterest());
		response.setAutoEsigin(respconfig.getAutoEsigin());
		response.setTermsAndConditions(respconfig.getTermsAndConditions());
		response.setComments(respconfig.getComments());
		response.setStatus(respconfig.getStatus());
		response.setCreatedOn(DateUtil.convertyyyymmddhhmmsstoddmmyyyy(dateFormat.format(respconfig.getCreatedOn())));
		User lenderUser = userRepo.findById(userId).get();
		if (lenderUser != null) {
			UserResponse user = new UserResponse();
			user.setId(lenderUser.getId());
			userDetails(lenderUser, user);
			response.setUser(user);
		}

		logger.info(" !!!!!!!! getAutoInvestConfigByLenderId  method end!!!!!!!!!");
		return response;

	}

	protected void userDetails(User findById, UserResponse user) {
		if (findById.getPersonalDetails() != null) {
			user.setFirstName(findById.getPersonalDetails().getFirstName());
			user.setLastName(findById.getPersonalDetails().getLastName());
			user.setAddress(findById.getPersonalDetails().getAddress());

		}
		if (findById.getPersonalDetails().getFacebookUrl() != null) {
			user.setFacebookUrl(findById.getPersonalDetails().getFacebookUrl());
		}
		if (findById.getPersonalDetails().getTwitterUrl() != null) {
			user.setTwitterUrl(findById.getPersonalDetails().getTwitterUrl());
		}
		if (findById.getPersonalDetails().getLinkedinUrl() != null) {
			user.setLinkedinUrl(findById.getPersonalDetails().getLinkedinUrl());
		}
		user.setEmail(findById.getEmail());
		user.setMobileNumber(findById.getMobileNumber());
		user.setCity(findById.getCity());
		user.setPinCode(findById.getPinCode());
		user.setUtmSource(findById.getUrchinTrackingModule());
		user.setState(findById.getState());
		// user.setEnachType(findById.getEnachType());
		if (findById.getExperianSummary() != null && findById.getUserProfileRisk() != null) {
			user.setOxyScore(findById.getExperianSummary().getScore() + findById.getUserProfileRisk().getTotalScore());

		}
		if (findById.getProfessionalDetails() != null) {
			user.setCompanyName(findById.getProfessionalDetails().getCompanyName());
			user.setWorkExperience(findById.getProfessionalDetails().getWorkExperience());
		}
		if (findById.getFinancialDetails() != null) {
			user.setSalary(findById.getFinancialDetails().getNetMonthlyIncome());
		}
		if (findById.getBankDetails() != null) {
			user.setAccountNumber(findById.getBankDetails().getAccountNumber());
			user.setBankName(findById.getBankDetails().getBankName());
			user.setBranchName(findById.getBankDetails().getBranchName());
			user.setIfscCode(findById.getBankDetails().getIfscCode());
			user.setBankAddress(findById.getBankDetails().getAddress());
			user.setUserNameAccordingToBank(findById.getBankDetails().getUserName());

		}
		RiskProfileDto riskProfileDto = getReskProfileCalculation(findById.getId());
		user.setRiskProfileDto(riskProfileDto);
	}

	private RiskProfileDto getReskProfileCalculation(Integer userId) {

		RiskProfileDto riskProfileDto = new RiskProfileDto();
		User user = userRepo.findById(userId).get();
		UserProfileRisk userProfileRisk = user.getUserProfileRisk();
		if (ObjectUtils.allNotNull(user, userProfileRisk)) {
			riskProfileDto.setUserId(user.getId());
			riskProfileDto.setCibilScore(userProfileRisk.getCibilScore());
			riskProfileDto.setCompanyOrOrganization(userProfileRisk.getCompanyOrOrganization());
			riskProfileDto
					.setExperianceOrExistenceOfOrganization(userProfileRisk.getExperianceOrExistenceOfOrganization());
			riskProfileDto.setGrade(userProfileRisk.getGrade());
			riskProfileDto.setSalaryOrIncome(userProfileRisk.getSalaryOrIncome());
			return riskProfileDto;
		}
		return null;

	}

	@Override
	public LenderAutoInvestConfigResponse updAutoInvestConfig(LenderAutoInvestConfigRequest request) {

		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl updAutoInvestConfig method end!!!!!!!!!");
		return null;

	}

	@Override
	public LenderAutoInvestConfigResponse updAutoInvestConfigInActive(LenderAutoInvestConfigRequest request) {
		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl updAutoInvestConfigInActive method start!!!!!!!!!");

		return null;

	}

	@Override
	public SearchResultsDto<LenderAutoInvestConfigResponse> autoInvestSearch(
			LenderAutoInvestConfigSearchRequest request) {
		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl autoInvestSearch method start!!!!!!!!!");
		List<Object[]> objList = lenderAutoInvestConfigNativeRepo.loansByApplication(request.getFirstName(),
				request.getLastName(), request.getUserId(), request.getPage().getPageSize(),
				request.getPage().getPageNo());
		Integer totalCount1 = 0;
		totalCount1 = lenderAutoInvestConfigNativeRepo.countTranRecords(request.getFirstName(), request.getLastName(),
				request.getUserId(), request.getPage().getPageSize(), request.getPage().getPageNo());
		SearchResultsDto<LenderAutoInvestConfigResponse> results = new SearchResultsDto<LenderAutoInvestConfigResponse>();
		List<LenderAutoInvestConfigResponse> applicationResponses = new ArrayList<LenderAutoInvestConfigResponse>();
		try {
			if (objList != null && !objList.isEmpty()) {
				results.setTotalCount(totalCount1);
				results.setPageNo(request.getPage().getPageNo());
				results.setPageCount(objList.size());
				objList.forEach(e -> {
					try {
						LenderAutoInvestConfigResponse dto = new LenderAutoInvestConfigResponse();
						dto.setId(e[0] == null ? 0 : Integer.parseInt(e[0].toString()));
						dto.setUserId(e[1] == null ? 0 : Integer.parseInt(e[1].toString()));
						dto.setRiskCategory(e[2] == null ? "" : e[2].toString());
						dto.setGender(e[3] == null ? "" : e[3].toString());
						dto.setOxyScore(e[4] == null ? "" : e[4].toString());
						dto.setCity(e[5] == null ? "" : e[5].toString());
						dto.setEmploymentType(e[6] == null ? "" : e[6].toString());
						dto.setSalaryRange(e[7] == null ? "" : e[7].toString());
						dto.setMaxAmount(Double.parseDouble(e[8] == null ? "0" : e[8].toString()));
						dto.setDuration(e[9] == null ? "" : e[9].toString());
						dto.setRateOfInterest(e[10] == null ? "" : e[10].toString());
						dto.setAutoEsigin(Boolean.parseBoolean(e[11] == null ? "false" : e[11].toString()));
						dto.setTermsAndConditions(Boolean.parseBoolean(e[12] == null ? "false" : e[12].toString()));
						dto.setComments(e[13] == null ? "" : e[13].toString());
						SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
						DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");
						dto.setCreatedOn(expectedDateFormat.format(dateFormat1.parse(e[14].toString())));
						dto.setStatus(e[15] == null ? "" : e[15].toString());
						dto.setFirstName(e[16] == null ? "" : e[16].toString());
						dto.setLastName(e[17] == null ? "" : e[17].toString());
						dto.setPanNumber(e[18] == null ? "" : e[18].toString());
						dto.setAddress(e[19] == null ? "" : e[19].toString());
						dto.setEmail(e[20] == null ? "" : e[20].toString());
						dto.setMobileNumber(e[21] == null ? "" : e[21].toString());
						dto.setLenderCity(e[22] == null ? "0" : e[22].toString());
						Double lenderWalletCreditAmount = lenderOxyWalletNativeRepo
								.lenderWalletCreditAmount(dto.getUserId());
						Double lenderWalletdebitAmount = lenderOxyWalletNativeRepo
								.lenderWalletDebitAmount(dto.getUserId());
						Double lenderWalletInprocessAmount = lenderOxyWalletNativeRepo
								.lenderWalletInprocessAmount(dto.getUserId());
						if (!(lenderWalletInprocessAmount != null && lenderWalletInprocessAmount > 0)) {
							lenderWalletInprocessAmount = 0d;
						}
						if (!(lenderWalletCreditAmount != null && lenderWalletCreditAmount > 0)) {
							lenderWalletCreditAmount = 0d;
						}
						if (!(lenderWalletdebitAmount != null && lenderWalletdebitAmount > 0)) {
							lenderWalletdebitAmount = 0d;
						}
						Double lenderWalletAmount = lenderWalletCreditAmount - lenderWalletdebitAmount;
						if (lenderWalletAmount != null && lenderWalletAmount > 0) {
							lenderWalletAmount = lenderWalletAmount - lenderWalletInprocessAmount;
						} else {
							lenderWalletAmount = 0.0;
						}
						dto.setLenderWalletAmount(lenderWalletAmount);
						applicationResponses.add(dto);
					} catch (Exception e1) {
						logger.info("Exception occured in autoInvestSearch", e1);
						throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
					}
				});
			}
		} catch (Exception e) {
			logger.info("Exception occured in autoInvestSearch", e);
			throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
		}
		results.setResults(applicationResponses);
		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl autoInvestSearch method end!!!!!!!!!");
		return results;
	}

	public AutoInvestConfigResponse getLenderAutoInvestHistory(LenderAutoInvestConfigSearchRequest request) {

		logger.info(" !!!!!!!! getLenderAutoInvestHistory  method start!!!!!!!!!");
		if (request.getUserId() == null || request.getUserId() == 0) {
			throw new ActionNotAllowedException("No user id present in the request.", ErrorCodes.INVALID_OPERATION);
		}
		List<LenderAutoInvestConfigArchive> respconfigList = lenderAutoInvestConfigRepoArchive
				.findByUserIdOrderByCreatedOnDesc(request.getUserId());
		if (respconfigList == null || respconfigList.isEmpty()) {
			throw new ActionNotAllowedException("No Auto-Invest config history found for this user.",
					ErrorCodes.INVALID_OPERATION);
		}
		List<LenderAutoInvestConfigResponse> autoInvestHistoryList = new ArrayList<LenderAutoInvestConfigResponse>();
		AutoInvestConfigResponse returnResponse = new AutoInvestConfigResponse();
		for (LenderAutoInvestConfigArchive respconfig : respconfigList) {
			LenderAutoInvestConfigResponse response = new LenderAutoInvestConfigResponse();
			response.setId(respconfig.getId());
			response.setUserId(respconfig.getUserId());
			response.setRiskCategory(respconfig.getRiskCategory());
			response.setGender(respconfig.getGender());
			response.setOxyScore(respconfig.getOxyScore());
			response.setCity(respconfig.getCity());
			response.setEmploymentType(respconfig.getEmploymentType());
			response.setSalaryRange(respconfig.getSalaryRange());
			response.setDuration(respconfig.getDuration());
			response.setMaxAmount(respconfig.getMaxAmount());
			response.setRateOfInterest(respconfig.getRateOfInterest());
			response.setAutoEsigin(respconfig.getAutoEsigin());
			response.setTermsAndConditions(respconfig.getTermsAndConditions());
			response.setComments(respconfig.getComments());
			response.setStatus(respconfig.getStatus());
			response.setCreatedOn(
					DateUtil.convertyyyymmddhhmmsstoddmmyyyy(dateFormat.format(respconfig.getCreatedOn())));
			autoInvestHistoryList.add(response);
		}
		User lenderUser = userRepo.findById(request.getUserId()).get();
		if (lenderUser != null) {
			UserResponse user = new UserResponse();
			user.setId(lenderUser.getId());
			userDetails(lenderUser, user);
			returnResponse.setUser(user);
		}
		returnResponse.setConfigHistory(autoInvestHistoryList);
		logger.info(" !!!!!!!! getAutoInvestConfigByLenderId  method `end!!!!!!!!!");
		return returnResponse;

	}

	@Override
	public SearchResultsDto<LenderAutoInvestConfigResponse> getAllAutoInvestLendersInfo() {
		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl getAllAutoInvestLendersInfo method start!!!!!!!!!");
		List<Object[]> objList = lenderAutoInvestConfigNativeRepo.allAutoInvestLenders();
		SearchResultsDto<LenderAutoInvestConfigResponse> results = new SearchResultsDto<LenderAutoInvestConfigResponse>();
		List<LenderAutoInvestConfigResponse> applicationResponses = new ArrayList<LenderAutoInvestConfigResponse>();
		try {
			if (objList != null && !objList.isEmpty()) {
				objList.forEach(e -> {
					try {
						LenderAutoInvestConfigResponse dto = new LenderAutoInvestConfigResponse();
						dto.setId(e[0] == null ? 0 : Integer.parseInt(e[0].toString()));
						dto.setUserId(e[1] == null ? 0 : Integer.parseInt(e[1].toString()));
						dto.setRiskCategory(e[2] == null ? "" : e[2].toString());
						dto.setGender(e[3] == null ? "" : e[3].toString());
						dto.setOxyScore(e[4] == null ? "" : e[4].toString());
						dto.setCity(e[5] == null ? "" : e[5].toString());
						dto.setEmploymentType(e[6] == null ? "" : e[6].toString());
						dto.setSalaryRange(e[7] == null ? "" : e[7].toString());
						dto.setMaxAmount(Double.parseDouble(e[8] == null ? "0" : e[8].toString()));
						dto.setDuration(e[9] == null ? "" : e[9].toString());
						dto.setRateOfInterest(e[10] == null ? "" : e[10].toString());
						dto.setAutoEsigin(Boolean.parseBoolean(e[11] == null ? "false" : e[11].toString()));
						dto.setTermsAndConditions(Boolean.parseBoolean(e[12] == null ? "false" : e[12].toString()));
						dto.setComments(e[13] == null ? "" : e[13].toString());
						SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
						DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");
						dto.setCreatedOn(expectedDateFormat.format(dateFormat1.parse(e[14].toString())));
						dto.setStatus(e[15] == null ? "" : e[15].toString());
						dto.setFirstName(e[16] == null ? "" : e[16].toString());
						dto.setLastName(e[17] == null ? "" : e[17].toString());
						dto.setPanNumber(e[18] == null ? "" : e[18].toString());
						dto.setAddress(e[19] == null ? "" : e[19].toString());
						dto.setEmail(e[20] == null ? "" : e[20].toString());
						dto.setMobileNumber(e[21] == null ? "" : e[21].toString());
						dto.setLenderCity(e[22] == null ? "0" : e[22].toString());
						Double lenderWalletCreditAmount = lenderOxyWalletNativeRepo
								.lenderWalletCreditAmount(dto.getUserId());
						Double lenderWalletdebitAmount = lenderOxyWalletNativeRepo
								.lenderWalletDebitAmount(dto.getUserId());
						Double lenderWalletInprocessAmount = lenderOxyWalletNativeRepo
								.lenderWalletInprocessAmount(dto.getUserId());
						if (!(lenderWalletInprocessAmount != null && lenderWalletInprocessAmount > 0)) {
							lenderWalletInprocessAmount = 0d;
						}
						if (!(lenderWalletCreditAmount != null && lenderWalletCreditAmount > 0)) {
							lenderWalletCreditAmount = 0d;
						}
						if (!(lenderWalletdebitAmount != null && lenderWalletdebitAmount > 0)) {
							lenderWalletdebitAmount = 0d;
						}
						Double lenderWalletAmount = lenderWalletCreditAmount - lenderWalletdebitAmount;
						if (lenderWalletAmount != null && lenderWalletAmount > 0) {
							lenderWalletAmount = lenderWalletAmount - lenderWalletInprocessAmount;
						} else {
							lenderWalletAmount = 0.0;
						}
						dto.setLenderWalletAmount(lenderWalletAmount);
						applicationResponses.add(dto);
					} catch (Exception e1) {
						logger.info("Exception occured in getAllAutoInvestLendersInfo", e1);
						throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
					}
				});
			}
		} catch (Exception e) {
			logger.info("Exception occured in getAllAutoInvestLendersInfo", e);
			throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
		}
		results.setResults(applicationResponses);
		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl getAllAutoInvestLendersInfo method end!!!!!!!!!");
		return results;
	}

	@Override
	public void scheduleAutoInvest(List<LenderAutoInvestConfigResponse> autoInvestLendersList) {
		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl scheduleAutoInvest method start!!!!!!!!!");
		LinkedHashMap<Integer, LenderAutoInvestConfigResponse> lenderConfigMap = new LinkedHashMap<Integer, LenderAutoInvestConfigResponse>();
		for (LenderAutoInvestConfigResponse autoInvestDto : autoInvestLendersList) {
			lenderConfigMap.put(autoInvestDto.getUserId(), autoInvestDto);
		}
		List<Object[]> objList = lenderAutoInvestConfigNativeRepo.allborrowerForAutoInvest();
		List<AutoInvestBorrowersList> borrowersList = new ArrayList<AutoInvestBorrowersList>();
		if (objList != null && !objList.isEmpty()) {
			objList.forEach(e -> {
				try {
					AutoInvestBorrowersList dto = new AutoInvestBorrowersList();
					dto.setFirstName(e[0] == null ? "" : e[0].toString());
					dto.setLastName(e[1] == null ? "" : e[1].toString());
					dto.setGender(e[2] == null ? "" : e[2].toString());
					dto.setMobileNumber(e[3] == null ? "" : e[3].toString());
					dto.setEmail(e[4] == null ? "" : e[4].toString());
					dto.setCity(e[5] == null ? "" : e[5].toString());
					dto.setGrade(e[6] == null ? "" : e[6].toString());
					dto.setScore(Integer.parseInt(e[7] == null ? "0" : e[7].toString()));
					dto.setEmployment(e[8] == null ? "" : e[8].toString());
					dto.setNet_monthly_income(Integer.parseInt(e[9] == null ? "0" : e[9].toString()));
					dto.setRate_of_interest(Double.parseDouble(e[10] == null ? "0" : e[10].toString()));
					dto.setDuration(Integer.parseInt(e[11] == null ? "0" : e[11].toString()));
					dto.setUserId(Integer.parseInt(e[12] == null ? "0" : e[12].toString()));
					dto.setLoanRequestAmount(Double.parseDouble(e[13] == null ? "0" : e[13].toString()));
					borrowersList.add(dto);
				} catch (Exception e1) {
					logger.info("Exception occured in getAllAutoInvestLendersInfo", e1);
					throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
				}
			});
		}

		for (LenderAutoInvestConfigResponse autoInvestLrDto : autoInvestLendersList) {
			for (AutoInvestBorrowersList autoInvestBrDto : borrowersList) {
				char borrowerGrade = autoInvestBrDto.getGrade().toUpperCase().charAt(0);
				char configuredGrade = autoInvestLrDto.getRiskCategory().toUpperCase().charAt(0);
				char configuredGender = autoInvestLrDto.getGender().toUpperCase().charAt(0);
				char borrowerGender = autoInvestBrDto.getGender().toUpperCase().charAt(0);
				String salRange = autoInvestLrDto.getSalaryRange();
				Double lowSal = 0d;
				Double highSal = 0d;
				Double sal = 0d;
				if (salRange.contains("-")) {
					String arr[] = salRange.split("-");
					lowSal = Double.parseDouble(arr[0]);
					highSal = Double.parseDouble(arr[1]);
				} else if (salRange.equalsIgnoreCase("more than 100000")) {
					sal = 100001d;
				}

				if (borrowerGrade <= configuredGrade && configuredGender == borrowerGender
						&& autoInvestBrDto.getScore() <= Integer.parseInt(autoInvestLrDto.getOxyScore())
						&& autoInvestBrDto.getCity().equalsIgnoreCase(autoInvestLrDto.getCity())
						&& autoInvestBrDto.getEmployment().equalsIgnoreCase(autoInvestLrDto.getEmploymentType())
						&& Double.parseDouble("" + autoInvestBrDto.getRate_of_interest()) >= Double
								.parseDouble(autoInvestLrDto.getRateOfInterest())
						&& autoInvestBrDto.getDuration() <= Integer.parseInt(autoInvestLrDto.getDuration())
						&& ((autoInvestBrDto.getNet_monthly_income().doubleValue() >= lowSal
								&& autoInvestBrDto.getNet_monthly_income().doubleValue() <= highSal)
								|| sal >= autoInvestBrDto.getNet_monthly_income().doubleValue())
						&& autoInvestBrDto.getLoanRequestAmount() >= autoInvestLrDto.getMaxAmount()) {
					logger.info("Elgible for AutoInvest");
					LoanRequestDto loanRequestDto = new LoanRequestDto();
					loanRequestDto.setLoanProcessType("autoinvest");
					loanRequestDto.setLoanRequestAmount(autoInvestLrDto.getMaxAmount());
					loanRequestDto.setRateOfInterest(Double.parseDouble(autoInvestLrDto.getRateOfInterest()));
					loanRequestDto.setDuration(Integer.parseInt(autoInvestLrDto.getDuration()));
					loanRequestDto.setRepaymentMethod("PI");
					loanRequestDto.setLoanPurpose("personal");
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.DATE, +7);
					Date date = cal.getTime();
					SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
					loanRequestDto.setExpectedDate(format1.format(date));
					LoanRequest oxyLoanRequest = loanRequestRepo
							.findByUserIdAndParentRequestIdIsNull(autoInvestBrDto.getUserId());
					loanRequestDto.setParentRequestId(oxyLoanRequest.getId());
					try {
						LoanResponseDto resp = loanServiceFactory.getService("LENDER")
								.loanRequest(autoInvestLrDto.getUserId(), loanRequestDto);
					} catch (Exception e) {
						logger.info("Exception occured in LenderAutoInvestConfigServiceImpl scheduleAutoInvest", e);
					}

				}
			}

		}

		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl scheduleAutoInvest method end!!!!!!!!!");

	}

	@Override
	public List<String> getAllAutoInvestAgreedLoanIds() {
		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl getAllAutoInvestAgreedLoanIds method start!!!!!!!!!");
		List<String> loanIdsList = lenderAutoInvestConfigNativeRepo.getAllAutoInvestAgreedLoanIds();
		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl scheduleAutoInvest method end!!!!!!!!!");
		return loanIdsList;
	}

	@Override
	@Transactional
	public LoanResponseDto autoInvestLenderEsign(String loanId) {
		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl autoInvestLenderEsign method start!!!!!!!!!" + loanId);
		OxyLoan oxyLoan = oxyLoanRepo.findByLoanId(loanId);

		if (oxyLoan == null) {
			throw new NoSuchElementException("No Loan present to esign against");
		}

		LoanRequest loanRequest = loanRequestRepo.findByLoanId(loanId);
		if (loanRequest == null) {
			throw new NoSuchElementException("No Loan present to esign against");
		}

		if (oxyLoan.getLoanStatus() == LoanStatus.Agreed && oxyLoan.getBorrowerDisbursedDate() == null) {

			oxyLoan.setLenderEsignId(2222);
			oxyLoan.setLenderEsigned(true);
			loanRequest.setAdminComments("APPROVED");

			oxyLoan = oxyLoanRepo.save(oxyLoan);
			loanRequest = loanRequestRepo.save(loanRequest);

		}

		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setLoanId(oxyLoan.getLoanId());
		loanResponseDto.setLenderEsignId(oxyLoan.getLenderEsignId());
		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl autoInvestLenderEsign method end!!!!!!!!!");
		return loanResponseDto;

	}

	@Override
	public List<LenderWalletHistoryResponseDto> autoPaticipation(String dealType, double dealMinimumAmount,
			String feeStatus) {

		logger.info("autoPaticipation method2 start");
		List<LenderAutoInvestConfig> autoLendingEnabledUsers = lenderAutoInvestConfigRepo
				.listOfLendersAutoLendingEnabled(dealType);
		List<LenderWalletHistoryResponseDto> lenderWalletHistory = new ArrayList<LenderWalletHistoryResponseDto>();
		if (autoLendingEnabledUsers != null && !autoLendingEnabledUsers.isEmpty()) {
			// autoLendingEnabledUsers.forEach(lenderAuto -> {
			for (LenderAutoInvestConfig lenderAuto : autoLendingEnabledUsers) {
				LenderTransactionHistory lenderTransactionHistory = lenderTransactionHistoryRepo
						.lastTransactionInfo(lenderAuto.getUserId());
				if (lenderTransactionHistory != null) {
					if (lenderTransactionHistory.getCurrentAmount() >= dealMinimumAmount) {
						LenderWalletHistoryResponseDto lenderWalletHistoryResponseDto = new LenderWalletHistoryResponseDto();
						lenderWalletHistoryResponseDto.setUserId(lenderAuto.getUserId());
						lenderWalletHistoryResponseDto.setPrincipalReturnAccountType(
								lenderAuto.getPrincipalReturningAccountType().toString());
						lenderWalletHistoryResponseDto
								.setWalletLoadedDate(expectedDateFormat.format(lenderTransactionHistory.getPaidDate()));
						if (feeStatus.equalsIgnoreCase(
								OxyBorrowersDealsInformation.FeeStatusToParticipate.MANDATORY.toString())) {
							boolean status = userServiceImpl.lenderValidityStatus(lenderAuto.getUserId());
							if (status == false) {
								lenderWalletHistory.add(lenderWalletHistoryResponseDto);
							}
						} else {
							lenderWalletHistory.add(lenderWalletHistoryResponseDto);
						}

					}
				}
			}

		}
		List<LenderWalletHistoryResponseDto> lenderWalletHistory2 = lenderWalletHistory.stream()
				.sorted((p1, p2) -> p1.getWalletLoadedDate().compareTo(p2.getWalletLoadedDate()))
				.collect(Collectors.toList());

		logger.info("autoPaticipation method2 ends");
		return lenderWalletHistory2;

	}

	@Override
	public AutoInvestResponseDto autoLendEnableData(int userId) {
		AutoInvestResponseDto autoInvestResponseDto = new AutoInvestResponseDto();
		try {
			LenderAutoInvestConfig lenderAutoInvest = lenderAutoInvestConfigRepo.autoLenderAlreadyEnabled(userId);
			if (lenderAutoInvest != null) {
				autoInvestResponseDto.setId(lenderAutoInvest.getId());
				autoInvestResponseDto.setCreatedDate(expectedDateFormat.format(lenderAutoInvest.getCreatedOn()));
				autoInvestResponseDto.setStatus(lenderAutoInvest.getStatus());
				autoInvestResponseDto.setDealType(lenderAutoInvest.getDealType().toString());
			}
		} catch (Exception e) {
			logger.info("autoLendEnableData method start");
		}
		return autoInvestResponseDto;

	}

	@Override
	public AutoInvestResponseDto closingAutoLending(int id,String status) {
		
		AutoInvestResponseDto autoInvestResponseDto = new AutoInvestResponseDto();

  try {
    LenderAutoInvestConfig lenderAutoInvest = lenderAutoInvestConfigRepo.findById(id).get();
    
    if (lenderAutoInvest != null) {
        lenderAutoInvest.setStatus(status);
        lenderAutoInvestConfigRepo.save(lenderAutoInvest);
        autoInvestResponseDto.setStatus("updated");
        
        LenderAutoInvestConfigArchive lenderAutoInvestHistory = new LenderAutoInvestConfigArchive();
        lenderAutoInvestHistory.setUserId(lenderAutoInvest.getUserId());
        lenderAutoInvestHistory.setCreatedOn(new Date());
        lenderAutoInvestHistory.setStatus("INACTIVE"); 
        
        lenderAutoInvestConfigArchiveRepo.save(lenderAutoInvestHistory);
        
        User user = userRepo.findById(lenderAutoInvest.getUserId()).get();

        if (user != null) {
            String mobileNumber = null;

            
            if (user.getPersonalDetails().getWhatsAppNumber() != null) {
                mobileNumber = user.getPersonalDetails().getWhatsAppNumber();
            } else {
                mobileNumber = "91" + user.getMobileNumber();
            }

          
            if ("Active".equalsIgnoreCase(status) || "Inactive".equalsIgnoreCase(status)) {
                String messageToWhatsApp = "Dear " + user.getPersonalDetails().getFirstName() + " (LR" + user.getId() + "), " +
                        "You have successfully " + ("Active".equalsIgnoreCase(status) ? "enabled" : "disabled") + " Auto-Lending.";
                whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, messageToWhatsApp);
            }
        }
    }
    
     } catch (Exception e) {
    logger.error("An error occurred in closingAutoLending method: " + e.getMessage());
    }
       autoInvestResponseDto.setStatus("updated");
        return autoInvestResponseDto;
      }

}
