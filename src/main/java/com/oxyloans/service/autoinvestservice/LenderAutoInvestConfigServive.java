package com.oxyloans.service.autoinvestservice;

import java.util.List;

import com.oxyloans.autoinvest.AutoInvestConfigResponse;
import com.oxyloans.autoinvest.AutoInvestResponseDto;
import com.oxyloans.autoinvest.LenderAutoInvestConfigRequest;
import com.oxyloans.autoinvest.LenderAutoInvestConfigResponse;
import com.oxyloans.autoinvest.LenderAutoInvestConfigSearchRequest;
import com.oxyloans.dashboard.LenderWalletHistoryResponseDto;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.serviceloan.LoanResponseDto;

public interface LenderAutoInvestConfigServive {

	public LenderAutoInvestConfigResponse saveAutoInvestConfig(LenderAutoInvestConfigRequest request);

	public LenderAutoInvestConfigResponse getAutoInvestConfigByLenderId(Integer userId);

	public LenderAutoInvestConfigResponse updAutoInvestConfig(LenderAutoInvestConfigRequest request);

	public LenderAutoInvestConfigResponse updAutoInvestConfigInActive(LenderAutoInvestConfigRequest request);

	public SearchResultsDto<LenderAutoInvestConfigResponse> autoInvestSearch(
			LenderAutoInvestConfigSearchRequest request);

	public AutoInvestConfigResponse getLenderAutoInvestHistory(LenderAutoInvestConfigSearchRequest request);

	public SearchResultsDto<LenderAutoInvestConfigResponse> getAllAutoInvestLendersInfo();

	public void scheduleAutoInvest(List<LenderAutoInvestConfigResponse> autoInvestLendersList);

	public LoanResponseDto autoInvestLenderEsign(String loanId);

	public List<String> getAllAutoInvestAgreedLoanIds();

	public List<LenderWalletHistoryResponseDto> autoPaticipation(String dealType, double dealMinimumAmount,
			String feeStatus);

	public AutoInvestResponseDto autoLendEnableData(int userId);

	public AutoInvestResponseDto closingAutoLending(int id, String status);

}
