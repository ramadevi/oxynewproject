package com.oxyloans.service.esign;

public class EsignInitRequest {
	
	public static class Document {
		
		private String data;
		
	    private String type = "pdf";
	    
	    private String info;
	    
	    private String xCoordinate;
	    
		private String yCoordinate;
		
		private String signPageNumber;

		public String getData() {
			return data;
		}

		public void setData(String data) {
			this.data = data;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}

		public String getxCoordinate() {
			return xCoordinate;
		}

		public void setxCoordinate(String xCoordinate) {
			this.xCoordinate = xCoordinate;
		}

		public String getyCoordinate() {
			return yCoordinate;
		}

		public void setyCoordinate(String yCoordinate) {
			this.yCoordinate = yCoordinate;
		}

		public String getSignPageNumber() {
			return signPageNumber;
		}

		public void setSignPageNumber(String signPageNumber) {
			this.signPageNumber = signPageNumber;
		}
		
	}
	
	private String signerName;
	
	private String signerCity;
	
	private String purpose;
	
	private String version ="2.1";
	
	private String responseURL;
	
	private Document document = new Document();

	public String getSignerName() {
		return signerName;
	}

	public void setSignerName(String signerName) {
		this.signerName = signerName;
	}

	public String getSignerCity() {
		return signerCity;
	}

	public void setSignerCity(String signerCity) {
		this.signerCity = signerCity;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getResponseURL() {
		return responseURL;
	}

	public void setResponseURL(String responseURL) {
		this.responseURL = responseURL;
	}

	public Document getDocument() {
		return document;
	}

	public String getData() {
		return this.getDocument().data;
	}

	public void setData(String data) {
		this.getDocument().data = data;
	}

	public String getType() {
		return this.getDocument().type;
	}

	public void setType(String type) {
		this.getDocument().type = type;
	}

	public String getInfo() {
		return this.getDocument().info;
	}

	public void setInfo(String info) {
		this.getDocument().info = info;
	}

	public String getxCoordinate() {
		return this.getDocument().xCoordinate;
	}

	public void setxCoordinate(String xCoordinate) {
		this.getDocument().xCoordinate = xCoordinate;
	}

	public String getyCoordinate() {
		return this.getDocument().yCoordinate;
	}

	public void setyCoordinate(String yCoordinate) {
		this.getDocument().yCoordinate = yCoordinate;
	}

	public String getSignPageNumber() {
		return this.getDocument().signPageNumber;
	}

	public void setSignPageNumber(String signPageNumber) {
		this.getDocument().signPageNumber = signPageNumber;
	}

}
