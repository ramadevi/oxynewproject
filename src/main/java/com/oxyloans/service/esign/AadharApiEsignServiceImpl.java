package com.oxyloans.service.esign;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AadharApiEsignServiceImpl implements IEsignService {

	private final Logger logger = LogManager.getLogger(AadharApiEsignServiceImpl.class);
	
	@Value("${qtApiKey}")
	private String qtApiKey;
	
	@Value("${qtAgencyId}")
	private String qtAgencyId;
	
	@Value("${aadhaarapiBaseurl}")
	private String aadhaarapiBaseurl;
	
	@Value("${aadhaarapiInitpath}")
	private String aadhaarapiInitpath;
	
	@Value("${aadhaarapiFetchpath}")
	private String aadhaarapiFetchpath;
	
	//@Value("${aadhaarapiFetchpath}")
	private String responseUrl = "https://oxyloans.com/esign/response";
	
	private String initUrl;
	
	private String fetchUrl;
	
	private Client client = ClientBuilder.newClient().register(MultiPartFeature.class).register(MultiPartWriter.class);
	
	public AadharApiEsignServiceImpl() {
	}
	
	@PostConstruct
	public void constructFullUrl() {
		this.initUrl = this.aadhaarapiBaseurl + this.aadhaarapiInitpath;
		this.fetchUrl = this.aadhaarapiBaseurl + this.aadhaarapiFetchpath;
		this.client.property(ClientProperties.CONNECT_TIMEOUT, 12000);
		this.client.property(ClientProperties.READ_TIMEOUT, 12000);
	}
	
	public EsignInitResponse initEsign(EsignInitRequest esignRequest) {
		logger.info(this.initUrl);
		esignRequest.setResponseURL(responseUrl);
		WebTarget webTarget = client.target(this.initUrl);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE);
		invocationBuilder.header("qt_api_key", this.qtApiKey);
		invocationBuilder.header("qt_agency_id", this.qtAgencyId);
		Response response = invocationBuilder.post(Entity.entity(esignRequest, MediaType.APPLICATION_JSON));
		EsignInitResponse eSignResponse = response.readEntity(EsignInitResponse.class);
		if (response.getStatus() == 200 && eSignResponse != null) {
			logger.info("Final Response {}, {}", eSignResponse.getId(), eSignResponse.getDocs().length);
			return eSignResponse;
		} else {
			logger.error("FAILED esign {}, {}, {}", response.getStatus(), eSignResponse.getStatusCode(), eSignResponse.getMessage());
			return null;
		}
	}
	
	public EsignFetchResponse downloadEsignedDocument (String transctionId) {
		String finalFetchUrl = this.fetchUrl.replace("{transactionId}", transctionId);
		logger.info(finalFetchUrl);
		WebTarget webTarget = client.target(finalFetchUrl);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE);
		invocationBuilder.header("qt_api_key", this.qtApiKey);
		invocationBuilder.header("qt_agency_id", this.qtAgencyId);
		Response response = invocationBuilder.get();
		EsignFetchResponse eSignResponse = response.readEntity(EsignFetchResponse.class);
		if (response.getStatus() == 200 && eSignResponse != null) {
			logger.info("Final Response {}, {}", response.getStatus());
			return eSignResponse;
		} else {
			logger.error("FAILED esign {}, {}, {}, {}", response.getStatus(), eSignResponse.getStatusCode(), eSignResponse.getMessage(), eSignResponse.getErrors());
			return null;
		}
	}

}
