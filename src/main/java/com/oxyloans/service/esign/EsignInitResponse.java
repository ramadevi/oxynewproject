package com.oxyloans.service.esign;

public class EsignInitResponse {
	
	private String id;
	
	private String request_version;
	
	private String createdAt;
	
	private String agreement;
	
	private String webhook_security_key;
	
	private String[] docs;
	
	private String statusCode;
	
	private String message;
	
	private String[] errors;
	
	private String error;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRequest_version() {
		return request_version;
	}

	public void setRequest_version(String request_version) {
		this.request_version = request_version;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getAgreement() {
		return agreement;
	}

	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}

	public String getWebhook_security_key() {
		return webhook_security_key;
	}

	public void setWebhook_security_key(String webhook_security_key) {
		this.webhook_security_key = webhook_security_key;
	}

	public String[] getDocs() {
		return docs;
	}

	public void setDocs(String[] docs) {
		this.docs = docs;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String[] getErrors() {
		return errors;
	}

	public void setErrors(String[] errors) {
		this.errors = errors;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
