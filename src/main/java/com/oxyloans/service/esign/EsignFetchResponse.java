package com.oxyloans.service.esign;

public class EsignFetchResponse {

	/**
	 * @author damodharreddy
	 *
	 */
	/**
	 * @author damodharreddy
	 *
	 */
	public static class ResponseDocument {

		private String id;

		private int index;

		private String doc_info;

		private String type;

		private String dynamic_url;

		private String sign_status; // Y - Yes, N - no

		private String auth_mode; // O/F/I - OTP/FINGERNPRINT/IRIS

		private String response_status;

		private String error_code;

		private String error_message;

		private String request_timestamp;

		private String signer_name_esp;

		private String signer_location_esp;

		private String name_match_score;
		
		private String esp_res_code;
		
		private String aadhaar_identifier;
		
		public String getAadhaar_identifier() {
			return aadhaar_identifier;
		}

		public void setAadhaar_identifier(String aadhaar_identifier) {
			this.aadhaar_identifier = aadhaar_identifier;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}

		public String getDoc_info() {
			return doc_info;
		}

		public void setDoc_info(String doc_info) {
			this.doc_info = doc_info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getDynamic_url() {
			return dynamic_url;
		}

		public void setDynamic_url(String dynamic_url) {
			this.dynamic_url = dynamic_url;
		}

		public String getSign_status() {
			return sign_status;
		}

		public void setSign_status(String sign_status) {
			this.sign_status = sign_status;
		}

		public String getAuth_mode() {
			return auth_mode;
		}

		public void setAuth_mode(String auth_mode) {
			this.auth_mode = auth_mode;
		}

		public String getResponse_status() {
			return response_status;
		}

		public void setResponse_status(String response_status) {
			this.response_status = response_status;
		}

		public String getError_code() {
			return error_code;
		}

		public void setError_code(String error_code) {
			this.error_code = error_code;
		}

		public String getError_message() {
			return error_message;
		}

		public void setError_message(String error_message) {
			this.error_message = error_message;
		}

		public String getRequest_timestamp() {
			return request_timestamp;
		}

		public void setRequest_timestamp(String request_timestamp) {
			this.request_timestamp = request_timestamp;
		}

		public String getSigner_name_esp() {
			return signer_name_esp;
		}

		public void setSigner_name_esp(String signer_name_esp) {
			this.signer_name_esp = signer_name_esp;
		}

		public String getSigner_location_esp() {
			return signer_location_esp;
		}

		public void setSigner_location_esp(String signer_location_esp) {
			this.signer_location_esp = signer_location_esp;
		}

		public String getName_match_score() {
			return name_match_score;
		}

		public void setName_match_score(String name_match_score) {
			this.name_match_score = name_match_score;
		}

		public String getEsp_res_code() {
			return esp_res_code;
		}

		public void setEsp_res_code(String esp_res_code) {
			this.esp_res_code = esp_res_code;
		}
		
		@Override
		public String toString() {
			return "AuthMode : " + this.auth_mode + ", sign_status : " + this.sign_status + ", response_status : " + this.response_status;
		}

	}

	private String id;

	private int transaction_status;

	private String public_ip;

	private String signer_consent; // Y - Yes, N - No

	private String request_medium; // W - Web, M - Mobile

	private int current_document;

	private ResponseDocument[] documents;

	private int signed_document_count;

	private String request_version;

	private String response_url;

	private int env;

	private String signer_name;

	private String signer_city;

	private String purpose;

	private int transaction_attempts;

	private int otp_attempts;

	private int otp_failures;

	private String user_notified;

	private String response_to_agency;

	private String createdAt;
	
	private String statusCode;
	
	private String[] errors;
	
	private String message;
	
	private String webhook_security_key;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getTransaction_status() {
		return transaction_status;
	}

	public void setTransaction_status(int transaction_status) {
		this.transaction_status = transaction_status;
	}

	public String getPublic_ip() {
		return public_ip;
	}

	public void setPublic_ip(String public_ip) {
		this.public_ip = public_ip;
	}

	public String getSigner_consent() {
		return signer_consent;
	}

	public void setSigner_consent(String signer_consent) {
		this.signer_consent = signer_consent;
	}

	public String getRequest_medium() {
		return request_medium;
	}

	public void setRequest_medium(String request_medium) {
		this.request_medium = request_medium;
	}

	public int getCurrent_document() {
		return current_document;
	}

	public void setCurrent_document(int current_document) {
		this.current_document = current_document;
	}

	public ResponseDocument[] getDocuments() {
		return documents;
	}

	public void setDocuments(ResponseDocument[] documents) {
		this.documents = documents;
	}

	public int getSigned_document_count() {
		return signed_document_count;
	}

	public void setSigned_document_count(int signed_document_count) {
		this.signed_document_count = signed_document_count;
	}

	public String getRequest_version() {
		return request_version;
	}

	public void setRequest_version(String request_version) {
		this.request_version = request_version;
	}

	public String getResponse_url() {
		return response_url;
	}

	public void setResponse_url(String response_url) {
		this.response_url = response_url;
	}

	public int getEnv() {
		return env;
	}

	public void setEnv(int env) {
		this.env = env;
	}

	public String getSigner_name() {
		return signer_name;
	}

	public void setSigner_name(String signer_name) {
		this.signer_name = signer_name;
	}

	public String getSigner_city() {
		return signer_city;
	}

	public void setSigner_city(String signer_city) {
		this.signer_city = signer_city;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public int getTransaction_attempts() {
		return transaction_attempts;
	}

	public void setTransaction_attempts(int transaction_attempts) {
		this.transaction_attempts = transaction_attempts;
	}

	public int getOtp_attempts() {
		return otp_attempts;
	}

	public void setOtp_attempts(int otp_attempts) {
		this.otp_attempts = otp_attempts;
	}

	public int getOtp_failures() {
		return otp_failures;
	}

	public void setOtp_failures(int otp_failures) {
		this.otp_failures = otp_failures;
	}

	public String getUser_notified() {
		return user_notified;
	}

	public void setUser_notified(String user_notified) {
		this.user_notified = user_notified;
	}

	public String getResponse_to_agency() {
		return response_to_agency;
	}

	public void setResponse_to_agency(String response_to_agency) {
		this.response_to_agency = response_to_agency;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String[] getErrors() {
		return errors;
	}

	public void setErrors(String[] errors) {
		this.errors = errors;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getWebhook_security_key() {
		return webhook_security_key;
	}

	public void setWebhook_security_key(String webhook_security_key) {
		this.webhook_security_key = webhook_security_key;
	}
	
}
