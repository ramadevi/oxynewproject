package com.oxyloans.service.esign;

public interface IEsignService {
	
	public EsignInitResponse initEsign(EsignInitRequest esignRequest);
	
	public EsignFetchResponse downloadEsignedDocument(String transctionId);	//Returns download URL

}
