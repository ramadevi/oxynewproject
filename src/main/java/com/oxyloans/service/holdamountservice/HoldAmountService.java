package com.oxyloans.service.holdamountservice;

import java.util.List;

import com.oxloans.lenderholdamountdto.HoldAmountRequestDto;
import com.oxloans.lenderholdamountdto.HoldAmountResponseDto;
import com.oxloans.lenderholdamountdto.ListOfHoldAmountsRequestDto;
import com.oxloans.lenderholdamountdto.UserHoldAmountMappedToDealResponseDto;
import com.oxyloans.entity.lenders.hold.UserHoldAmountMappedToDeal.AmountType;

public interface HoldAmountService {

	public HoldAmountResponseDto saveingHoldAmount(HoldAmountRequestDto holdAmountRequestDto);

	public List<HoldAmountResponseDto> listOfHoldAmounts();

	public HoldAmountResponseDto deletingTheHoldAmountRequest(int holdAmountId);

	public UserHoldAmountMappedToDealResponseDto savingHoldAmountMappedToDeal(int userHoldAmountDetailId,
			ListOfHoldAmountsRequestDto listOfHoldAmountsRequestDto);

	public double remainingAmountAfterRemovingHoldAmount(int userId, int dealId, AmountType amountType, double amount);

	public List<HoldAmountResponseDto> holdInformation(int userId);

}
