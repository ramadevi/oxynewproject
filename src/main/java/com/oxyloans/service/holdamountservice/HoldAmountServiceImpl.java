package com.oxyloans.service.holdamountservice;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.persistence.EntityNotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oxloans.lenderholdamountdto.HoldAmountRequestDto;
import com.oxloans.lenderholdamountdto.HoldAmountResponseDto;
import com.oxloans.lenderholdamountdto.ListOfHoldAmountsRequestDto;
import com.oxloans.lenderholdamountdto.UserHoldAmountMappedToDealRequestDto;
import com.oxloans.lenderholdamountdto.UserHoldAmountMappedToDealResponseDto;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.lender.returns.LendersReturns;
import com.oxyloans.entity.lender.returns.LendersReturnsRepo;
import com.oxyloans.entity.lenders.hold.UserHoldAmountDetail;
import com.oxyloans.repository.lenderhold.UserHoldAmountDetailRepo;
import com.oxyloans.entity.lenders.hold.UserHoldAmountMappedToDeal;
import com.oxyloans.entity.lenders.hold.UserHoldAmountMappedToDeal.AmountType;
import com.oxyloans.entity.lenders.hold.UserHoldAmountMappedToDeal.Status;
import com.oxyloans.repository.lenderhold.UserHoldAmountMappedToDealRepo;
import com.oxyloans.entity.user.User;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.service.loan.ActionNotAllowedException;
import com.oxyloans.whatappservice.WhatappServiceRepo;

@Service
public class HoldAmountServiceImpl implements HoldAmountService {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private UserHoldAmountDetailRepo userHoldAmountDetailRepo;

	@Autowired
	private UserHoldAmountMappedToDealRepo userHoldAmountMappedToDealRepo;

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	private final Logger logger = LogManager.getLogger(getClass());

	@Autowired
	private WhatappServiceRepo whatappServiceRepo;

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Autowired
	private IEmailService emailService;

	@Autowired
	private LendersReturnsRepo lendersReturnsRepo;

	@Override
	public HoldAmountResponseDto saveingHoldAmount(HoldAmountRequestDto holdAmountRequestDto) {
		HoldAmountResponseDto holdAmountResponseDto = new HoldAmountResponseDto();
		User user = userRepo.findById(holdAmountRequestDto.getUserId()).get();
		if (user != null) {
			LendersReturns lendersReturns = lendersReturnsRepo
					.previousInterestAndPrincipalStatus(holdAmountRequestDto.getUserId());
			if (lendersReturns != null) {

				UserHoldAmountDetail userHoldAmountDetail = new UserHoldAmountDetail();
				userHoldAmountDetail.setUserId(holdAmountRequestDto.getUserId());
				userHoldAmountDetail.setDealId(holdAmountRequestDto.getDealId());
				userHoldAmountDetail.setHoldAmount(holdAmountRequestDto.getHoldAmount());
				userHoldAmountDetail.setComments(holdAmountRequestDto.getComments());
				userHoldAmountDetailRepo.save(userHoldAmountDetail);
				holdAmountResponseDto.setUserId(holdAmountRequestDto.getUserId());
			} else {
				throw new EntityNotFoundException("This is a lender who just participated less than month ago.");
			}
		} else {
			throw new EntityNotFoundException("user not found in database");
		}

		return holdAmountResponseDto;

	}

	@Override
	public List<HoldAmountResponseDto> listOfHoldAmounts() {
		List<HoldAmountResponseDto> holdAmounts = new ArrayList<HoldAmountResponseDto>();
		List<UserHoldAmountDetail> listOfHoldAmounts = userHoldAmountDetailRepo.findAll();
		if (listOfHoldAmounts != null && !listOfHoldAmounts.isEmpty()) {
			holdAmounts = listOfHoldAmounts.stream()
					.filter(h -> (h.getStatus().equals(UserHoldAmountDetail.Status.OPEN))
							|| (h.getStatus().equals(UserHoldAmountDetail.Status.CLOSE)))
					.sorted((s1, s2) -> s1.getStatus().compareTo(s2.getStatus())).map(holdamount -> {
						HoldAmountResponseDto holdAmountResponseDto = new HoldAmountResponseDto();

						List<UserHoldAmountMappedToDeal> listOFHoldUsers = userHoldAmountMappedToDealRepo
								.findByuserHoldAmountDetailsId(holdamount.getId());

						double totalHoldAmount = 0.0;
						if (listOFHoldUsers != null && !listOFHoldUsers.isEmpty()) {
							totalHoldAmount = listOFHoldUsers.stream()
									.mapToDouble(UserHoldAmountMappedToDeal::getHoldAmount).sum();
						}
						holdAmountResponseDto.setId(holdamount.getId());
						holdAmountResponseDto.setUserId(holdamount.getUserId());
						holdAmountResponseDto.setDealId(holdamount.getDealId());
						holdAmountResponseDto
								.setHoldAmount(BigDecimal.valueOf(holdamount.getHoldAmount()).toBigInteger());
						holdAmountResponseDto.setCurrentHoldAmount(
								BigDecimal.valueOf(holdamount.getHoldAmount() - totalHoldAmount).toBigInteger());

						holdAmountResponseDto.setStatus(holdamount.getStatus().toString());
						holdAmountResponseDto.setCreatedOn(dateFormat.format(holdamount.getCreatedOn()));
						holdAmountResponseDto.setComments(holdamount.getComments());
						return holdAmountResponseDto;
					}).collect(Collectors.toList());

		}
		return holdAmounts;

	}

	@Override
	public UserHoldAmountMappedToDealResponseDto savingHoldAmountMappedToDeal(int userHoldAmountDetailId,
			ListOfHoldAmountsRequestDto listOfHoldAmountsRequestDto) {

		UserHoldAmountMappedToDealResponseDto userHoldAmountMappedToDealResponseDto = new UserHoldAmountMappedToDealResponseDto();
		List<UserHoldAmountMappedToDeal> listOFHoldUsers = userHoldAmountMappedToDealRepo
				.findByuserHoldAmountDetailsId(userHoldAmountDetailId);

		double totalHoldAmount = 0.0;
		if (listOFHoldUsers != null && !listOFHoldUsers.isEmpty()) {
			totalHoldAmount = listOFHoldUsers.stream().mapToDouble(UserHoldAmountMappedToDeal::getHoldAmount).sum();
		}

		double amountMappingToDeals = 0.0;
		List<UserHoldAmountMappedToDealRequestDto> listOfHoldAmounts = listOfHoldAmountsRequestDto
				.getUserHoldAmountMappedToDealRequestDto();
		if (listOfHoldAmounts != null && !listOfHoldAmounts.isEmpty()) {
			amountMappingToDeals = listOfHoldAmounts.stream()
					.mapToDouble(UserHoldAmountMappedToDealRequestDto::getHoldAmount).sum();
		}

		UserHoldAmountDetail userHoldAmountDetail = userHoldAmountDetailRepo.findById(userHoldAmountDetailId).get();
		if (userHoldAmountDetail != null) {

			if ((totalHoldAmount + amountMappingToDeals) > userHoldAmountDetail.getHoldAmount()) {
				throw new ActionNotAllowedException("Please check your are giving more than the hold amount",
						ErrorCodes.ENTITY_ALREDY_EXISTS);
			} else {
				if (listOfHoldAmounts != null && !listOfHoldAmounts.isEmpty()) {
					listOfHoldAmounts.stream().forEach(userHoldAmountMappedToDealRequestDto -> {
						UserHoldAmountMappedToDeal userHoldAmountMappedToDeal = new UserHoldAmountMappedToDeal();
						userHoldAmountMappedToDeal.setUserHoldAmountDetailsId(userHoldAmountDetailId);
						userHoldAmountMappedToDeal.setUserId(userHoldAmountMappedToDealRequestDto.getUserId());
						userHoldAmountMappedToDeal.setDealId(userHoldAmountMappedToDealRequestDto.getDealId());
						userHoldAmountMappedToDeal.setAmountType(UserHoldAmountMappedToDeal.AmountType
								.valueOf(userHoldAmountMappedToDealRequestDto.getAmountType().toString()));
						userHoldAmountMappedToDeal.setHoldAmount(userHoldAmountMappedToDealRequestDto.getHoldAmount());
						userHoldAmountMappedToDealRepo.save(userHoldAmountMappedToDeal);
						userHoldAmountMappedToDealResponseDto.setStatus("successfullyUpdated");
					});
				}
			}
		}
		return userHoldAmountMappedToDealResponseDto;

	}

	@Override
	public HoldAmountResponseDto deletingTheHoldAmountRequest(int userHoldAmountDetailId) {
		HoldAmountResponseDto holdAmountResponseDto = new HoldAmountResponseDto();
		UserHoldAmountDetail userHoldAmountDetail = userHoldAmountDetailRepo.findById(userHoldAmountDetailId).get();
		if (userHoldAmountDetail != null) {
			if (userHoldAmountDetail.getStatus().equals(UserHoldAmountDetail.Status.CLOSE)) {
				throw new ActionNotAllowedException("Hold amount already collected", ErrorCodes.ENTITY_ALREDY_EXISTS);
			}
		}
		try {
			userHoldAmountDetail.setStatus(UserHoldAmountDetail.Status.DELETE);
			userHoldAmountDetailRepo.save(userHoldAmountDetail);

		} catch (Exception e) {
			logger.info("Exception in deleteingTheHoldAmountRequest method");
		}
		return holdAmountResponseDto;

	}

	@Override
	public double remainingAmountAfterRemovingHoldAmount(int userId, int dealId, AmountType amountType, double amount) {
		UserHoldAmountMappedToDeal userHoldAmountMappedToDeal = userHoldAmountMappedToDealRepo
				.findByUserIdAndDealIdAndAmountTypeAndStatus(userId, dealId, amountType, Status.OPEN);
		double remainingAmount = 0.0;
		remainingAmount = amount;
		if (userHoldAmountMappedToDeal != null) {
			remainingAmount = remainingAmount - userHoldAmountMappedToDeal.getHoldAmount();
			userHoldAmountMappedToDeal.setStatus(Status.CLOSE);
			userHoldAmountMappedToDeal.setAmountReturnedOn(new Date());
			userHoldAmountMappedToDealRepo.save(userHoldAmountMappedToDeal);
			List<UserHoldAmountMappedToDeal> listOFHoldUsers = userHoldAmountMappedToDealRepo
					.findByuserHoldAmountDetailsId(userHoldAmountMappedToDeal.getUserHoldAmountDetailsId());

			double totalHoldAmount = 0.0;
			if (listOFHoldUsers != null && !listOFHoldUsers.isEmpty()) {
				totalHoldAmount = listOFHoldUsers.stream()
						.filter(holdAmountCompleted -> holdAmountCompleted.getStatus()
								.equals(UserHoldAmountMappedToDeal.Status.CLOSE))
						.mapToDouble(UserHoldAmountMappedToDeal::getHoldAmount).sum();
			}
			UserHoldAmountDetail userHoldAmountDetail = userHoldAmountDetailRepo
					.findById(userHoldAmountMappedToDeal.getUserHoldAmountDetailsId()).get();
			double holdAmount = userHoldAmountDetail.getHoldAmount();
			if (totalHoldAmount == holdAmount) {
				userHoldAmountDetail.setStatus(UserHoldAmountDetail.Status.CLOSE);
				userHoldAmountDetailRepo.save(userHoldAmountDetail);
			}

			User user = userRepo.findById(userId).get();
			if (user != null) {
				LocalDate currentDate = LocalDate.now();
				OxyBorrowersDealsInformation extraAmountCreditedDealInfo = oxyBorrowersDealsInformationRepo
						.findById(userHoldAmountDetail.getDealId()).get();
				OxyBorrowersDealsInformation extraAmountDebitedDealInfo = oxyBorrowersDealsInformationRepo
						.findById(dealId).get();
				String messageToLender = " Dear " + user.getPersonalDetails().getFirstName() + "," + "\n" + "\n"
						+ "Greetings from Oxyloans! Kindly note that an extra amount of INR "
						+ BigDecimal.valueOf(userHoldAmountMappedToDeal.getHoldAmount()).toBigInteger()
						+ " is paid for " + extraAmountCreditedDealInfo.getDealName().toUpperCase() + "." + "\n"
						+ "So we have deducted the same from " + extraAmountDebitedDealInfo.getDealName().toUpperCase()
						+ " on " + currentDate + "." + "\n" + "Sorry for the inconvenience." + "\n" + "\n" + "Thanks,"
						+ "\n" + "OxyloansTeam.";
				String messageToMail = " Dear " + user.getPersonalDetails().getFirstName() + "," + "\n" + "\n"
						+ "Greetings from Oxyloans! Kindly note that an extra amount of INR "
						+ BigDecimal.valueOf(userHoldAmountMappedToDeal.getHoldAmount()).toBigInteger()
						+ " is paid for " + extraAmountCreditedDealInfo.getDealName().toUpperCase() + "." + "\n"
						+ "So we have deducted the same from " + extraAmountDebitedDealInfo.getDealName().toUpperCase()
						+ " on " + currentDate + "." + "\n" + "Sorry for the inconvenience." + "\n";
				String messageToAdmin = "Kindly note that an extra amount of INR "
						+ BigDecimal.valueOf(userHoldAmountMappedToDeal.getHoldAmount()).toBigInteger()
						+ " is paid for " + extraAmountCreditedDealInfo.getDealName().toUpperCase() + "." + "\n"
						+ "So we have deducted the same from " + extraAmountDebitedDealInfo.getDealName().toUpperCase()
						+ " on " + currentDate + ".";
				String mobileNumber = null;
				if (user.getPersonalDetails().getWhatsappVerifiedOn() != null) {
					mobileNumber = user.getPersonalDetails().getWhatsAppNumber();
				} else {
					mobileNumber = "91" + user.getMobileNumber();
				}

				TemplateContext templateContext = new TemplateContext();
				templateContext.put("messageToMail", messageToMail);
				templateContext.put("currentDate", currentDate);

				String mailsubject = "Information regarding the deduction for the extra amount paid";
				EmailRequest emailRequest = new EmailRequest(new String[] { user.getEmail() }, mailsubject,
						"hold-amount.template", templateContext);

				EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
				if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
					try {
						throw new MessagingException(emailResponseFromService.getErrorMessage());
					} catch (MessagingException e1) {

						e1.printStackTrace();
					}
				}
				whatappServiceRepo.sendingWhatsappMessageWithNewInstance(mobileNumber, messageToLender);
				whatappServiceRepo.sendingWhatsappMessageWithNewInstance("120363020098924954@g.us", messageToAdmin);
			}
		}

		logger.info("Hold Amount" + remainingAmount);
		return remainingAmount;
	}

	@Override
	public List<HoldAmountResponseDto> holdInformation(int userId) {
		List<HoldAmountResponseDto> listOfHoldAmounts = new ArrayList<HoldAmountResponseDto>();
		List<UserHoldAmountDetail> userHoldAmountDetail = userHoldAmountDetailRepo.findByUserId(userId);
		if (userHoldAmountDetail != null && !userHoldAmountDetail.isEmpty()) {
			userHoldAmountDetail.stream().forEach(holdAmount -> {
				HoldAmountResponseDto holdAmountResponseDto = new HoldAmountResponseDto();
				holdAmountResponseDto.setId(holdAmount.getId());
				holdAmountResponseDto.setUserId(holdAmount.getUserId());
				holdAmountResponseDto.setDealId(holdAmount.getDealId() == null ? 0 : holdAmount.getDealId());
				OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
						.findById(holdAmountResponseDto.getDealId()).get();
				if (oxyBorrowersDealsInformation != null) {
					holdAmountResponseDto.setHoldAmountGivenDealName(oxyBorrowersDealsInformation.getDealName());
				}
				holdAmountResponseDto.setStatus(holdAmount.getStatus().toString());
				holdAmountResponseDto.setComments(holdAmount.getComments());
				holdAmountResponseDto.setCreatedDate(dateFormat.format(holdAmount.getCreatedOn()));
				holdAmountResponseDto.setHoldAmount(BigDecimal.valueOf(holdAmount.getHoldAmount()).toBigInteger());
				List<UserHoldAmountMappedToDealRequestDto> mappedList = new ArrayList<UserHoldAmountMappedToDealRequestDto>();
				List<UserHoldAmountMappedToDeal> holdAmountMappedList = holdAmount.getUserHoldAmountMappedToDeal();
				if (holdAmountMappedList != null && !holdAmountMappedList.isEmpty()) {
					holdAmountMappedList.stream().forEach(holdMapped -> {
						UserHoldAmountMappedToDealRequestDto userHoldAmountMappedToDealRequestDto = new UserHoldAmountMappedToDealRequestDto();
						userHoldAmountMappedToDealRequestDto.setUserId(holdMapped.getUserId());
						userHoldAmountMappedToDealRequestDto.setDealId(holdMapped.getDealId());
						OxyBorrowersDealsInformation oxyBorrowersDeals = oxyBorrowersDealsInformationRepo
								.findById(holdMapped.getDealId()).get();
						if (oxyBorrowersDeals != null) {
							userHoldAmountMappedToDealRequestDto
									.setHoldAmountCollectedDealName(oxyBorrowersDeals.getDealName());
						}
						userHoldAmountMappedToDealRequestDto.setStatus(holdMapped.getStatus().toString());
						userHoldAmountMappedToDealRequestDto
								.setCreatedDate(dateFormat.format(holdMapped.getCreatedOn()));
						if (holdMapped.getAmountReturnedOn() != null) {
							userHoldAmountMappedToDealRequestDto
									.setClosedDate(dateFormat.format(holdMapped.getAmountReturnedOn()));
						}
						userHoldAmountMappedToDealRequestDto.setHoldAmount(holdMapped.getHoldAmount());
						userHoldAmountMappedToDealRequestDto.setAmountType(holdMapped.getAmountType().toString());
						mappedList.add(userHoldAmountMappedToDealRequestDto);
					});
				}
				holdAmountResponseDto.setUserHoldAmountMappedToDealRequestDto(mappedList);
				listOfHoldAmounts.add(holdAmountResponseDto);
			});
		}
		return listOfHoldAmounts;

	}
}
