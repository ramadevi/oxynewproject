package com.oxyloans.service.notifications;

import java.io.IOException;

import com.oxyloans.response.user.ListOfWhatappGroupNames;
import com.oxyloans.response.user.StatusResponseDto;

public interface NotificationsRepo {

	public  StatusResponseDto sendingNotificationsToLendersInDealPending(ListOfWhatappGroupNames dealName) throws IOException;

}
