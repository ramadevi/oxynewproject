package com.oxyloans.service.notifications;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.apache.logging.log4j.LogManager;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.oxyloans.entity.borrowers.deals.information.LendersPaticipationUpdationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDealsRepo;

import com.oxyloans.repository.whatsapprepo.WhatappGroupsInformationRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.response.user.ListOfWhatappGroupNames;
import com.oxyloans.response.user.PageRequestDto;
import com.oxyloans.response.user.StatusResponseDto;
import com.oxyloans.service.loan.AdminLoanService;
import com.oxyloans.service.user.UserServiceImpl;

import com.oxyloans.whatappservice.WhatappService;

@Service
public class Notifications implements NotificationsRepo {

	@Autowired
	private UserServiceImpl userServiceImpl;

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private WhatappService whatappService;

	private final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Notifications.class);

	@Autowired
	private OxyLendersAcceptedDealsRepo oxyLendersAcceptedDealsRepo;

	@Autowired
	private AdminLoanService adminLoanService;

	private Client client = ClientBuilder.newClient().register(MultiPartFeature.class).register(MultiPartWriter.class);

	@Value("${wtappApi}")
	private String wtappApi;

	@Autowired
	private WhatappGroupsInformationRepo whatappGroupsInformationRepo;

	@Autowired
	private LendersPaticipationUpdationRepo lendersPaticipationUpdationRepo;

	protected final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	public StatusResponseDto sendingNotificationsToLendersInDealPending(ListOfWhatappGroupNames dealName)
			throws IOException {
		String lineSpliter = System.lineSeparator();
		StatusResponseDto statusResponseDto = new StatusResponseDto();

		OxyBorrowersDealsInformation listOfDeals = oxyBorrowersDealsInformationRepo
				.findByDealName(dealName.getDealName());

		String responseStatus = null;
		String message = "";

		if (listOfDeals != null) {

			Integer id = listOfDeals.getId();

			String dealStartDate = listOfDeals.getFundsAcceptanceStartDate().toString();
			String dealEndDate = listOfDeals.getFundsAcceptanceEndDate().toString();
			BigInteger walletAmount = null;
			walletAmount = userRepo.findingSumOfAmountOFLendersTransfered(dealStartDate, dealEndDate);

			BigInteger dealAmount = oxyBorrowersDealsInformationRepo.getDealAmountForDeal(id);

			String whatsAppChatId = null;

			if (listOfDeals.getWhatappChatid() != null) {
				whatsAppChatId = listOfDeals.getWhatappChatid();

			}

			BigInteger sumOfParticipatedAmount = oxyLendersAcceptedDealsRepo.findingSumOfParticipatedAmount(id);
			double sumValue = 0.0;
			BigInteger sumValueInBigInt = null;
			Double sumOfUpdatedAmount = lendersPaticipationUpdationRepo.getDealUpdatedSumValue(id);
			if (sumOfParticipatedAmount != null) {
				if (sumOfUpdatedAmount != null) {
					sumValue = sumOfParticipatedAmount.doubleValue() + sumOfUpdatedAmount;
					sumValueInBigInt = BigDecimal.valueOf(sumValue).toBigInteger();
				} else {
					sumValue = sumOfParticipatedAmount.doubleValue();
					sumValueInBigInt = BigDecimal.valueOf(sumValue).toBigInteger();
				}
			}
			String statusForParticipationAmount = "";
			if (dealAmount.equals(sumValueInBigInt)) {
				statusForParticipationAmount += "Achieved";
			} else {
				statusForParticipationAmount += "Not yet achieved";
			}

			if (sumValueInBigInt == null) {
				sumValueInBigInt = BigDecimal.valueOf(0).toBigInteger();
			}

			BigInteger walletExcessAmount = null;
			if (walletAmount != null) {
				if (sumOfParticipatedAmount != null) {
					if (walletAmount.doubleValue() > sumOfParticipatedAmount.doubleValue()) {
						walletExcessAmount = BigDecimal
								.valueOf(walletAmount.doubleValue() - sumOfParticipatedAmount.doubleValue())
								.toBigInteger();
					} else {
						walletExcessAmount = BigDecimal.valueOf(0).toBigInteger();
					}
				}
			}
			if (walletExcessAmount == null) {
				walletExcessAmount = BigDecimal.valueOf(0).toBigInteger();
			}

			if (walletAmount == null) {
				walletAmount = BigDecimal.valueOf(0).toBigInteger();
			}

			message = "*Deal Info :*" + lineSpliter + "Deal Name : " + listOfDeals.getDealName() + lineSpliter
					+ "Deal value in INR : " + dealAmount + lineSpliter + "Participated amount in INR : "
					+ sumValueInBigInt + lineSpliter + "Target : " + statusForParticipationAmount + lineSpliter
					+ "*This is a system generated message.*";

			String linkSentToLendersGroup = dealName.getSentToWhatsappGorups();

			Set<String> listOfChatIds = new HashSet<String>();
			if (linkSentToLendersGroup != null && !linkSentToLendersGroup.isEmpty()) {
				String[] names = linkSentToLendersGroup.split(",");
				String chatid = null;

				for (int i = 0; i < names.length; i++) {
					chatid = names[i];
					if (chatid != null) {
						listOfChatIds.add(chatid);
					}
				}
			}
			listOfChatIds.add(whatsAppChatId);
			if (listOfChatIds != null && !listOfChatIds.isEmpty()) {
				for (String chatIdsOfGroup : listOfChatIds) {

					if (chatIdsOfGroup != null) {

						whatappService.whatsappMessageToGroupUsingUltra(chatIdsOfGroup, message);

					}
				}
			}

			if (statusForParticipationAmount.equals("Achieved")) {

				OxyBorrowersDealsInformation dealInfo = oxyBorrowersDealsInformationRepo.findById(id).get();
				dealInfo.setDealStatus(OxyBorrowersDealsInformation.DealStatus.ACHIEVED);
				oxyBorrowersDealsInformationRepo.save(dealInfo);
				String modifiedDealOpenedDate = adminLoanService
						.addfiveAndHalfHoursToTimeStamp(dealInfo.getReceivedOn());

				String modifiedDealAchievedDate = adminLoanService
						.addfiveAndHalfHoursToTimeStamp(dealInfo.getDealAcheivedDate());

				long diffInMillies;

				if (dealInfo.getReceivedOn().getTime() - dealInfo.getDealAcheivedDate().getTime() > 0) {
					diffInMillies = dealInfo.getReceivedOn().getTime() - dealInfo.getDealAcheivedDate().getTime();

				} else {
					diffInMillies = dealInfo.getDealAcheivedDate().getTime() - dealInfo.getReceivedOn().getTime();

				}

				int days = (int) TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

				int seconds = (int) (diffInMillies / 1000) % 60;
				int minutes = (int) ((diffInMillies / (1000 * 60)) % 60);
				int hours = (int) (diffInMillies / (1000 * 60 * 60));
                if (days > 0) {
					hours = hours - (days * 24);
				}

				Integer noOfLenders = oxyLendersAcceptedDealsRepo.findingNoOfLendersParticipatedInDeal(id);

				String commonMessageToGroup = "Hi All! Greetings from OxyLoans 	" + lineSpliter + lineSpliter
						+ "Deal Name : " + dealInfo.getDealName() + lineSpliter + "Deal Value : " + dealAmount
						+ lineSpliter + "Deal Opened : " + modifiedDealOpenedDate + lineSpliter + "Deal Achieved : "
						+ modifiedDealAchievedDate + lineSpliter + "No.Of Lenders Participated : " + noOfLenders
						+ lineSpliter + "Time taken to raise funds is : " + days + " : " + hours + " : " + minutes
						+ " : " + seconds + " (d:h:m:s) " + lineSpliter + lineSpliter
						+ "Thanks to all for the participation," + lineSpliter
						+ "Please find the participants details below" + lineSpliter + lineSpliter;

				List<Object[]> listOfLenders = oxyLendersAcceptedDealsRepo
						.totalLendersWithParticipatedAmount(listOfDeals.getId());
				String lenderWithAmount = "";

				if (listOfLenders != null && !listOfLenders.isEmpty()) {
					Iterator it = listOfLenders.iterator();
					while (it.hasNext()) {
						Object[] e = (Object[]) it.next();
						String lenderId = e[0] == null ? "0" : e[0].toString();
						Double amount = Double.parseDouble(e[1] == null ? "0.0" : e[1].toString());
						lenderWithAmount = lenderWithAmount + "LR" + lenderId + " " + " : " + " "
								+ BigDecimal.valueOf(amount).toBigInteger() + lineSpliter;
					}

				}
				StringBuilder whatsmessageToGroup = new StringBuilder();
				whatsmessageToGroup.append(commonMessageToGroup);
				whatsmessageToGroup.append(lenderWithAmount);
				whatsmessageToGroup.append(lineSpliter);
				whatsmessageToGroup.append("*This is a system generated message.*");

				whatappService.sendingIndividualMessageUsingUltraApi(listOfDeals.getWhatappChatid(),
						whatsmessageToGroup.toString());

			}
			responseStatus = "success";
		}

		statusResponseDto.setStatus(responseStatus);
		return statusResponseDto;

	}

}
