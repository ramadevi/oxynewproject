package com.oxyloans.service.user;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

//import com.amazonaws.services.directory.model.EntityAlreadyExistsException;
import com.oxyloans.coreauthdto.GenericAccessToken.EncodingAlgorithm;
import com.oxyloans.coreauthdto.PasswordGrantAccessToken;
import com.oxyloans.customexceptions.EntityAlreadyExistsException;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.engine.template.IPdfEngine;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceDetails;
import com.oxyloans.entity.borrowers.deals.information.LendersPaticipationUpdationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDealsRepo;
import com.oxyloans.repository.cms.OxyCmsLoansRepo;
import com.oxyloans.entity.lender.returns.LendersReturns;
import com.oxyloans.entity.lender.returns.LendersReturnsRepo;
import com.oxyloans.entity.loan.LoanEmiCard;
import com.oxyloans.entity.loan.LoanRequest;
import com.oxyloans.entity.loan.LoanRequest.RepaymentMethod;
import com.oxyloans.entity.user.OxyUsersuuidFromMobiledevice;
import com.oxyloans.entity.user.PersonalDetails;
import com.oxyloans.entity.user.ReferenceDetails;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.entity.user.type.OxyUserType;
import com.oxyloans.entity.user.type.OxyUserTypeRepo;
import com.oxyloans.file.FileRequest;
import com.oxyloans.file.FileRequest.FileType;
import com.oxyloans.file.FileResponse;
import com.oxyloans.mobile.MobileUtil;
import com.oxyloans.mobile.twoFactor.MobileOtpVerifyFailedException;
import com.oxyloans.mobileDevice.MobileDeviceResponseDto;
import com.oxyloans.partner.PartnerRegistrationRequestDto;
import com.oxyloans.partner.PartnerRegistrationResponseDto;
import com.oxyloans.registration.UserRegistrationRequestDto;
import com.oxyloans.registration.UserRegistrationResponseDto;
import com.oxyloans.repo.loan.ApplicationLevelLoanEmiCard;
import com.oxyloans.repo.loan.ApplicationLevelLoanEmiCardRepo;
import com.oxyloans.repo.loan.LoanEmiCardRepo;
import com.oxyloans.repo.loan.OxyLoanRepo;
import com.oxyloans.repo.loan.OxyLoanRequestRepo;
import com.oxyloans.repo.user.LenderReferenceDetailsRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.BorrowerUserDetails;
import com.oxyloans.request.LenderUserDetails;
import com.oxyloans.request.PartnerLoanInfoResponse;
import com.oxyloans.request.PartnerNDArequestDto;
import com.oxyloans.request.PartnerRequestDto;
import com.oxyloans.request.user.ReferenceDetailsDto;
import com.oxyloans.request.user.ReferenceDetailsRequestDto;
import com.oxyloans.request.user.ReferenceDetailsResponseDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.LenderMailResponse;
import com.oxyloans.response.user.LendersReturnResponse;
import com.oxyloans.response.user.LendersWithdrawResponse;
import com.oxyloans.response.user.OxyUserDetailsResponse;
import com.oxyloans.response.user.PartnerResponseDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.security.ThreadLocalSecurityProvider;
import com.oxyloans.security.signature.ISignatureService;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.service.file.IFileManagementService;
import com.oxyloans.service.loan.ActionNotAllowedException;
import com.oxyloans.service.loan.LoanServiceFactory;
import com.oxyloans.serviceloan.LoanRequestDto;
import com.oxyloans.serviceloan.LoanResponseDto;
import com.oxyloans.usertypedto.OxyUserTypeRequestDto;
import com.oxyloans.usertypedto.OxyUserTypeResponseDto;
import com.oxyloans.whatapp.service.WhatappService;

@Service
public class NewUserServiceImpl implements NewUserService {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private MobileUtil mobileUtil;

	@Autowired
	private ISignatureService signatureService;

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	@Value("${emailActiviationLink}")
	private String emailActiviationLink;

	@Value("${emailActivationReactLink}")
	private String emailActivationReactLink;

	@Autowired
	private IEmailService emailService;

	private final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Autowired
	private LoanServiceFactory loanServiceFactory;

	@Autowired
	private LenderReferenceDetailsRepo lenderReferenceDetailsRepo;

	@Value("${wtappApi}")
	private String wtappApi;

	@Autowired
	private WhatappService whatappService;

	@Autowired
	private OxyUserTypeRepo oxyUserTypeRepo;

	@Autowired
	private OxyLendersAcceptedDealsRepo oxyLendersAcceptedDealsRepo;

	@Autowired
	private LendersPaticipationUpdationRepo lendersPaticipationUpdationRepo;

	@Autowired
	private OxyLoanRepo oxyLoanRepo;

	@Value("${partnerLenderRegistrationLink}")
	private String partnerLenderRegistrationLink;

	@Value("${partnerBorrowerRegistrationLink}")
	private String partnerBorrowerRegistrationLink;

	@Value("${partnerLoginLink}")
	private String partnerLoginLink;

	@Autowired
	private LendersReturnsRepo lendersReturnsRepo;

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Value("${partnerToEditDetails}")
	private String partnerToEditDetails;

	@Autowired
	protected IPdfEngine pdfEngine;

	@Autowired
	protected IFileManagementService fileManagementService;

	@Value("${emailTtl}")
	private String emailTtl;

	@Autowired
	protected OxyLoanRequestRepo loanRequestRepo;

	@Autowired
	private OxyCmsLoansRepo oxyCmsLoansRepo;

	@Autowired
	private ApplicationLevelLoanEmiCardRepo applicationLevelLoanEmiCardRepo;

	@Autowired
	private LoanEmiCardRepo loanEmiCardRepo;

	@Override
	@Transactional
	public UserRegistrationResponseDto UserNewRegistration(UserRegistrationRequestDto userRegistrationRequestDto) {
		logger.info("UserNewRegistration method start");
		UserRegistrationResponseDto userRegistrationResponseDto = new UserRegistrationResponseDto();
		if (userRegistrationRequestDto.getMobileNumber() != null) {
			User mobileNumberCheck = userRepo.findByMobileNumber(userRegistrationRequestDto.getMobileNumber());

			if (mobileNumberCheck == null) {

				if (userRegistrationRequestDto.getMobileNumber() != null
						&& userRegistrationRequestDto.getMobileOtpValue() == null && userRegistrationRequestDto
								.getCitizenship().equalsIgnoreCase(User.Citizenship.NONNRI.toString())) {

					String otpSessionId = mobileUtil.sendOtp(userRegistrationRequestDto.getMobileNumber());
					userRegistrationResponseDto.setMobileNumber(userRegistrationRequestDto.getMobileNumber());
					userRegistrationResponseDto.setMobileOtpSession(otpSessionId);

				} else {
					User user = new User();
					if (userRegistrationRequestDto.getCitizenship()
							.equalsIgnoreCase(User.Citizenship.NONNRI.toString())) {
						boolean verifyOtp = false;
						verifyOtp = mobileUtil.verifyOtp(userRegistrationRequestDto.getMobileNumber(),
								userRegistrationRequestDto.getMobileOtpSession(),
								userRegistrationRequestDto.getMobileOtpValue());

						if (!verifyOtp) {
							throw new MobileOtpVerifyFailedException("Invalid OTP value please check.",
									ErrorCodes.INVALID_OTP);
						}
						user.setMobileNumberVerified(true);
						user.setMobileOtpSent(true);
						user.setCitizenship(User.Citizenship.NONNRI);
					} else {
						user.setMobileNumberVerified(true);
						user.setMobileOtpSent(true);
						user.setCitizenship(User.Citizenship.NRI);
					}
					Integer emailCheck = userRepo
							.findByEmailUsingEqualIgnoranceCase(userRegistrationRequestDto.getEmail());
					if (emailCheck > 0) {
						throw new ActionNotAllowedException("Registation already done using this email",
								ErrorCodes.ACTION_ALREDY_DONE);
					}

					user.setPrimaryType(User.PrimaryType.valueOf(userRegistrationRequestDto.getPrimaryType()));
					user.setMobileNumber(userRegistrationRequestDto.getMobileNumber());
					user.setMobileOtpSession("");
					user.setCity("");
					user.setAdminComments("");
					user.setPinCode("");
					user.setState("");
					user.setEmailOtpSession("");
					user.setEmail(userRegistrationRequestDto.getEmail());
					user.setEmailSent(true);
					user.setSalt(UUID.randomUUID().toString() + UUID.randomUUID().toString().substring(0, 10));
					if (userRegistrationRequestDto.getUtm() != null) {
						user.setUrchinTrackingModule(userRegistrationRequestDto.getUtm());
					}

					if (userRegistrationRequestDto.getUtm() != null
							&& userRegistrationRequestDto.getUtm().equalsIgnoreCase("TEST")) {
						user.setTestUser(true);
					}
					String hashedpassword = signatureService.hashPassword(userRegistrationRequestDto.getPassword(),
							user.getSalt());
					user.setPassword(hashedpassword);
					user.setUniqueNumber(new Long(System.currentTimeMillis()).toString().substring(0, 10));
					StringBuilder uniqueNumber = new StringBuilder();
					if (user.getPrimaryType() == PrimaryType.LENDER) {
						uniqueNumber.append("LR1");
					} else {
						uniqueNumber.append("BR1");
					}

					userRepo.save(user);

					if (userRegistrationRequestDto.getUuid() != null) {
						uuidFromMobileDevice(user.getId(), userRegistrationRequestDto.getUuid());
					}

					logger.info("User from partner starts.....");

					LenderReferenceDetails referenceDetails = lenderReferenceDetailsRepo
							.checkingRefereeAlreadyInvitedForNonNriForPartner(user.getEmail(), user.getMobileNumber());
					String messageToGroup = "";
					if (referenceDetails == null) {
						LenderReferenceDetails refereeInfo = new LenderReferenceDetails();
						refereeInfo.setRefereeEmail(user.getEmail());
						refereeInfo.setRefereeMobileNumber(user.getMobileNumber());
						refereeInfo.setStatus(LenderReferenceDetails.Status.Registered);
						OxyUserType oxyUserType = null;
						if (userRegistrationRequestDto.getUtm() != null
								&& !userRegistrationRequestDto.getUtm().equalsIgnoreCase("WEB")
								&& !userRegistrationRequestDto.getUtm().equalsIgnoreCase("MOBILE")) {
							oxyUserType = oxyUserTypeRepo.findingUtm(userRegistrationRequestDto.getUtm());
							refereeInfo.setSource(LenderReferenceDetails.Source.Partner);
							refereeInfo.setRefereeId(user.getId());
							refereeInfo.setReferrerId(oxyUserType.getId());
							if (user.getCitizenship().equals(User.Citizenship.NONNRI)) {
								refereeInfo.setCitizenType(LenderReferenceDetails.CitizenType.NONNRI);
							} else {
								refereeInfo.setCitizenType(LenderReferenceDetails.CitizenType.NRI);
							}
							if (user.getPrimaryType().equals(User.PrimaryType.LENDER)) {
								refereeInfo.setUserPrimaryType("LENDER");
							} else {
								refereeInfo.setUserPrimaryType("BORROWER");
							}

							refereeInfo.setRefereeName(userRegistrationRequestDto.getName());

							lenderReferenceDetailsRepo.save(refereeInfo);

							String primaryType = "";

							if (user.getPrimaryType() == User.PrimaryType.LENDER) {
								primaryType = "LR";
							} else {
								primaryType = "BR";
							}

							SimpleDateFormat sd = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");

							sd.setTimeZone(TimeZone.getTimeZone("IST"));

							String modified = sd.format(new Date());

							messageToGroup = "Referee: " + primaryType + user.getId() + " is registerd on " + modified
									+ " from Referrer: " + "PR" + oxyUserType.getId();
						} else {
							if (!userRegistrationRequestDto.getUniqueNumber().equals("0")) {
								User uniqueNumberSearch = userRepo
										.findByUniqueNumber(userRegistrationRequestDto.getUniqueNumber());
								refereeInfo.setSource(LenderReferenceDetails.Source.ReferralLink);
								refereeInfo.setRefereeId(user.getId());
								refereeInfo.setReferrerId(uniqueNumberSearch.getId());
								if (user.getCitizenship().equals(User.Citizenship.NONNRI)) {
									refereeInfo.setCitizenType(LenderReferenceDetails.CitizenType.NONNRI);
								} else {
									refereeInfo.setCitizenType(LenderReferenceDetails.CitizenType.NRI);
								}
								if (user.getPrimaryType().equals(User.PrimaryType.LENDER)) {
									refereeInfo.setUserPrimaryType("LENDER");
								} else {
									refereeInfo.setUserPrimaryType("BORROWER");
								}

								refereeInfo.setRefereeName(userRegistrationRequestDto.getName());

								lenderReferenceDetailsRepo.save(refereeInfo);

								String primaryType = "";

								if (user.getPrimaryType() == User.PrimaryType.LENDER) {
									primaryType = "LR";
								} else {
									primaryType = "BR";
								}

								String referrerPrimaryType = "";

								if (uniqueNumberSearch.getPrimaryType() == User.PrimaryType.LENDER) {
									referrerPrimaryType = "LR";
								} else {
									referrerPrimaryType = "BR";
								}

								SimpleDateFormat sd = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");

								sd.setTimeZone(TimeZone.getTimeZone("IST"));

								String modified = sd.format(new Date());

								messageToGroup = "Referee: " + primaryType + user.getId() + " is registerd on "
										+ modified + " from Referrer: " + referrerPrimaryType
										+ uniqueNumberSearch.getId();
							}
						}

					} else {
						referenceDetails.setRefereeId(user.getId());
						if (userRegistrationRequestDto.getUtm() != null
								&& !userRegistrationRequestDto.getUtm().equalsIgnoreCase("WEB")
								&& !userRegistrationRequestDto.getUtm().equalsIgnoreCase("MOBILE")) {
							referenceDetails.setSource(LenderReferenceDetails.Source.Partner);
						} else {
							referenceDetails.setSource(LenderReferenceDetails.Source.ReferralLink);
						}
						referenceDetails.setStatus(LenderReferenceDetails.Status.Registered);
						referenceDetails.setUserPrimaryType(userRegistrationRequestDto.getPrimaryType());

						lenderReferenceDetailsRepo.save(referenceDetails);

						User referrerInfo = userRepo.findByIdNum(referenceDetails.getReferrerId());

						String primaryType = "";

						if (user.getPrimaryType() == User.PrimaryType.LENDER) {
							primaryType = "LR";
						} else {
							primaryType = "BR";
						}

						String referrerPrimaryType = "";

						if (referrerInfo.getPrimaryType() == User.PrimaryType.LENDER) {
							referrerPrimaryType = "LR";
						} else {
							referrerPrimaryType = "BR";
						}

						SimpleDateFormat sd = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");

						sd.setTimeZone(TimeZone.getTimeZone("IST"));

						String modified = sd.format(new Date());

						messageToGroup = "Referee: " + primaryType + referenceDetails.getRefereeId()
								+ " is registerd on " + modified + " from Referrer: " + referrerPrimaryType
								+ referrerInfo.getId();

					}

					if (messageToGroup != null && !messageToGroup.isEmpty()) {
						whatappService.whatsappMessageToGroupUsingUltra("120363024766214462@g.us", messageToGroup);
					}

					logger.info("User from partner ends.....");

					if (userRegistrationRequestDto.getUtm() != null) {
						if (userRegistrationRequestDto.getUtm().equalsIgnoreCase("MOBILE")) {
							Random random = new Random();
							String id = String.format("%06d", random.nextInt(999999));
							TemplateContext templateContext = new TemplateContext();
							templateContext.put("otp", id);
							String hashedEmailOtpSession = signatureService.hashPassword(id, user.getSalt());
							String mailOtpSubject = "OTP From oxyloans";
							EmailRequest emailRequest = new EmailRequest(
									new String[] { userRegistrationRequestDto.getEmail() }, mailOtpSubject,
									"email-otp.template", templateContext);
							EmailResponse emailResponse = emailService.sendEmail(emailRequest);
							if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
								throw new RuntimeException(emailResponse.getErrorMessage()); // property
							}
							userRegistrationResponseDto.setEmailOtpSession(hashedEmailOtpSession);
							Long currentTimeInMilliSec = System.currentTimeMillis();
							userRegistrationResponseDto.setTimeInMilliSeconds(currentTimeInMilliSec.toString());
						} else {
							TemplateContext templateContext = new TemplateContext();
							Long currentTimeInMilliSec = System.currentTimeMillis();
							String verifyLink = null;
							if (userRegistrationRequestDto.getProjectType() != null
									&& userRegistrationRequestDto.getProjectType().equalsIgnoreCase("REACT")) {
								verifyLink = new StringBuffer()
										.append(this.emailActivationReactLink.replace("{id}", user.getId().toString())
												.replace("{time}", String.valueOf(currentTimeInMilliSec)))
										.toString();

							} else {
								verifyLink = new StringBuffer()
										.append(this.emailActiviationLink.replace("{id}", user.getId().toString())
												.replace("{time}", String.valueOf(currentTimeInMilliSec)))
										.toString();
							}

							/*
							 * String verifyLink = null; String emailActivationLink =
							 * userRegistrationRequestDto.getProjectType() .equalsIgnoreCase("REACT") ?
							 * emailActivationReactLink : emailActiviationLink;
							 * 
							 * verifyLink = new StringBuilder() .append(emailActivationLink.replace("{id}",
							 * String.valueOf(user.getId())) .replace("{time}",
							 * String.valueOf(currentTimeInMilliSec))) .toString();
							 */

							templateContext.put("verifyLink", verifyLink);
							String emailVerification = "Email Activation";
							String templateName = "email-verification.template";

							EmailRequest emailRequest = new EmailRequest(
									new String[] { userRegistrationRequestDto.getEmail() }, emailVerification,
									templateName, templateContext);
							EmailResponse emailResponse = emailService.sendEmail(emailRequest);
							if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
								throw new RuntimeException(emailResponse.getErrorMessage());
							}
						}
					}

					uniqueNumber.append(StringUtils.leftPad(user.getId().toString(), 6, "0"));
					user.setUniqueNumber(uniqueNumber.toString());
					PersonalDetails personalDetails = user.getPersonalDetails();
					if (personalDetails == null) {
						personalDetails = new PersonalDetails();
						personalDetails.setFirstName(userRegistrationRequestDto.getName());
						personalDetails.setLastName("");
						personalDetails.setFatherName("");
						personalDetails.setDob(null);
						personalDetails.setGender("");
						personalDetails.setAddress("");
						personalDetails.setCreatedAt(new Date());
						personalDetails.setModifiedAt(new Date());
						personalDetails.setPanNumber("");

						personalDetails.setCifNumber(userRegistrationRequestDto.getCifNumber() == null ? ""
								: userRegistrationRequestDto.getCifNumber());

						personalDetails.setFinoEmployeeMobileNumber(
								userRegistrationRequestDto.getFinoEmployeeMobileNumber() == null ? ""
										: userRegistrationRequestDto.getFinoEmployeeMobileNumber());

						personalDetails.setUser(user);
						user.setPersonalDetails(personalDetails);
						user = userRepo.save(user);
					}

					LoanRequestDto loanRequestDto = new LoanRequestDto();
					loanRequestDto.setDuration(6);
					loanRequestDto.setLoanPurpose("Initiated the Request");
					loanRequestDto.setLoanRequestAmount(5000.00);
					loanRequestDto.setRateOfInterest(0d);
					loanRequestDto.setRepaymentMethod(RepaymentMethod.I.name());
					Calendar expectedDate = Calendar.getInstance();
					expectedDate.add(Calendar.DATE, 1);
					loanRequestDto.setExpectedDate(expectedDateFormat.format(expectedDate.getTime()));

					PasswordGrantAccessToken accessToken = new PasswordGrantAccessToken(6000, EncodingAlgorithm.RSA);
					accessToken.setUserId(user.getId().toString());
					ThreadLocalSecurityProvider.setAccessToken(accessToken);
					LoanResponseDto loanRequest = loanServiceFactory
							.getService(user.getPrimaryType().name().toUpperCase())
							.loanRequest(user.getId(), loanRequestDto);
					logger.info("Created Loan Request for the user {} as request id {}", user.getId(),
							loanRequest.getLoanRequestId());

					userRegistrationResponseDto.setMobileNumber(userRegistrationRequestDto.getMobileNumber());
					userRegistrationResponseDto.setUserId(user.getId());
				}

			} else {
				PersonalDetails personalDetails = mobileNumberCheck.getPersonalDetails();
				if (personalDetails != null) {
					if (personalDetails.getDob() == null) {
						throw new ActionNotAllowedException(" User has not verified the email="
								+ mobileNumberCheck.getEmail() + " id=" + mobileNumberCheck.getId(),
								ErrorCodes.EMAIL_NOT_VERIFIED);
					} else {
						throw new ActionNotAllowedException("Registation already done using this mobile number",
								ErrorCodes.ACTION_ALREDY_DONE);
					}
				}
			}

		}
		logger.info("UserNewRegistration method end");
		return userRegistrationResponseDto;
	}

	@Override
	@Transactional
	public UserRegistrationResponseDto userEmailVerification(UserRegistrationRequestDto userRegistrationRequestDto) {
		logger.info("userEmailVerification method start");
		UserRegistrationResponseDto userRegistrationResponseDto = new UserRegistrationResponseDto();
		User user = userRepo.findById(userRegistrationRequestDto.getUserId()).get();
		if (user != null) {
			Long registerdTime = Long.parseLong(userRegistrationRequestDto.getTimeInMilliSeconds());
			Date registerdTimeDate = new Date(registerdTime);

			Long currentTime = System.currentTimeMillis();
			Date currentTimeDate = new Date(currentTime);

			if (currentTimeDate.getTime() - registerdTimeDate.getTime() > Long.parseLong(this.emailTtl)) {
				throw new ActionNotAllowedException("Email activation link expired", ErrorCodes.TOKEN_NOT_VALID);
			}
			if (userRegistrationRequestDto.getEmailOtpSession() != null
					&& userRegistrationRequestDto.getEmailOtp() != null) {
				String token = userRegistrationRequestDto.getEmailOtpSession();
				String hashedEmailOtpSession = signatureService.hashPassword(userRegistrationRequestDto.getEmailOtp(),
						user.getSalt());

				if (!hashedEmailOtpSession.equals(token)) {
					throw new ActionNotAllowedException("Invalid Email OTP please check.",
							ErrorCodes.EMAIL_NOT_VERIFIED);
				}

			}

			logger.info("userEmailVerification" + user.getEmailVerified());
			PersonalDetails personalDetails = user.getPersonalDetails();
			if (personalDetails != null) {
				if (user.getPersonalDetails().getDob() == null) {
					try {
						personalDetails.setDob(expectedDateFormat.parse(userRegistrationRequestDto.getDob()));
					} catch (ParseException e) {
						e.printStackTrace();
					}

					personalDetails.setAddress(userRegistrationRequestDto.getAddress());
					personalDetails.setPanNumber(userRegistrationRequestDto.getPanNumber());
					user.setPersonalDetails(personalDetails);
					personalDetails.setUser(user);
					user.setEmailOtpSession(" ");
					user.setEmailVerifiedOn(new Date());
					user.setEmailVerified(true);
					userRepo.save(user);
					userRegistrationResponseDto.setUserId(userRegistrationRequestDto.getUserId());
					userRegistrationResponseDto.setPrimaryType(user.getPrimaryType().toString());

					TemplateContext templateContext1 = new TemplateContext();
					templateContext1.put("userName", personalDetails.getFirstName());
					Date date = new Date();
					String date1 = expectedDateFormat.format(date);
					templateContext1.put("CurrentDate", date1);

					if (user.getPrimaryType() == PrimaryType.LENDER) {

						String welcomeMailToLender = "Message from CEO, OxyLoans – RBI Approved P2P Lending Platform";

						String emailTemplateName = "registraction-mail-lender.template";
						EmailRequest emailRequest1 = new EmailRequest(new String[] { user.getEmail() },
								welcomeMailToLender, emailTemplateName, templateContext1);
						EmailResponse emailResponse1 = emailService.sendEmail(emailRequest1);
						if (emailResponse1.getStatus() == EmailResponse.Status.FAILED) {
							throw new RuntimeException(emailResponse1.getErrorMessage());
						}
					} else {
						String welcomeMailToLender = "Message from CEO, OxyLoans – RBI Approved P2P Lending Platform";

						String emailTemplateName = "registraction-mail-borrower.template";
						EmailRequest emailRequest1 = new EmailRequest(new String[] { user.getEmail() },
								welcomeMailToLender, emailTemplateName, templateContext1);
						EmailResponse emailResponse1 = emailService.sendEmail(emailRequest1);
						if (emailResponse1.getStatus() == EmailResponse.Status.FAILED) {
							throw new RuntimeException(emailResponse1.getErrorMessage());
						}

					}

					if (user.getUrchinTrackingModule() != null
							&& user.getPrimaryType().equals(User.PrimaryType.BORROWER)
							&& !user.getUrchinTrackingModule().equalsIgnoreCase("web")
							&& !user.getUrchinTrackingModule().equalsIgnoreCase("MOBILE")) {
						templateContext1.put("uniqueNumber", user.getUniqueNumber());

						String referalTemplateName = "user-refer.template";
						String welcomeMail = "Welcome to OxyLoans";

						EmailRequest emailRequest1 = new EmailRequest(new String[] { user.getEmail() }, welcomeMail,
								referalTemplateName, templateContext1);
						EmailResponse emailResponse1 = emailService.sendEmail(emailRequest1);
						if (emailResponse1.getStatus() == EmailResponse.Status.FAILED) {
							throw new RuntimeException(emailResponse1.getErrorMessage());
						}

						String messageToBorrower = "Hi " + user.getPersonalDetails().getFirstName() + "\n\n"
								+ "Welcome to oxyloans! Your registration is successful" + "\n\n" + "*"
								+ "Note : If you are an EV dealer, please ask your customer to apply for vehicle loan using the below link only ."
								+ "*" + "\n\n" + "https://www.oxyloans.com/new/register_borrower?ref="
								+ user.getUniqueNumber();
						/*
						 * whatappService.sendingWhatsappMsgWithMobileNumber(91 +
						 * user.getMobileNumber(), messageToBorrower, wtappApi, null);
						 */
					}

				} else {
					throw new ActionNotAllowedException("you had already verified your email",
							ErrorCodes.ALREDY_IN_PROGRESS);
				}
			}

		} else {
			throw new ActionNotAllowedException("User not present database with given id",
					ErrorCodes.ENITITY_NOT_FOUND);
		}
		logger.info("userEmailVerification method end");
		return userRegistrationResponseDto;

	}

	@Override
	public UserRegistrationResponseDto sendingEmailActivationLink(
			UserRegistrationRequestDto userRegistrationRequestDto) {
		logger.info("sendingEmailActivationLink method start");
		UserRegistrationResponseDto userRegistrationResponseDto = new UserRegistrationResponseDto();
		User user = userRepo.findById(userRegistrationRequestDto.getUserId()).get();
		if (user != null) {
			if (user.getUrchinTrackingModule().equalsIgnoreCase("MOBILE")) {
				Random random = new Random();
				String id = String.format("%06d", random.nextInt(999999));
				TemplateContext templateContext = new TemplateContext();
				templateContext.put("otp", id);
				String hashedEmailOtpSession = signatureService.hashPassword(id, user.getSalt());
				String mailOtpSubject = "OTP From oxyloans";
				EmailRequest emailRequest = new EmailRequest(new String[] { user.getEmail() }, mailOtpSubject,
						"email-otp.template", templateContext);
				EmailResponse emailResponse = emailService.sendEmail(emailRequest);
				if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
					throw new RuntimeException(emailResponse.getErrorMessage()); // property
				}
				userRegistrationResponseDto.setEmailOtpSession(hashedEmailOtpSession);
				Long currentTimeInMilliSec = System.currentTimeMillis();
				userRegistrationResponseDto.setTimeInMilliSeconds(currentTimeInMilliSec.toString());
			} else {
				TemplateContext templateContext = new TemplateContext();
				Long currentTimeInMilliSec = System.currentTimeMillis();
				String verifyLink = new StringBuffer().append(this.emailActiviationLink
						.replace("{id}", user.getId().toString()).replace("{time}", currentTimeInMilliSec.toString()))
						.toString();
				templateContext.put("verifyLink", verifyLink);
				String emailVerification = "Email Activation";
				String templateName = "email-verification.template";

				EmailRequest emailRequest = new EmailRequest(new String[] { user.getEmail() }, emailVerification,
						templateName, templateContext);
				EmailResponse emailResponse = emailService.sendEmail(emailRequest);
				if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
					throw new RuntimeException(emailResponse.getErrorMessage());
				}
				userRegistrationResponseDto.setStatus("SuccessFully sent");
				userRegistrationResponseDto.setUserId(userRegistrationRequestDto.getUserId());
			}
		} else {
			throw new ActionNotAllowedException("User not present database with given id",
					ErrorCodes.ENITITY_NOT_FOUND);
		}
		logger.info("sendingEmailActivationLink method end");
		return userRegistrationResponseDto;

	}

	@Override
	@Transactional
	public ReferenceDetailsResponseDto updateReferenceDetailsToBorrower(
			ReferenceDetailsRequestDto referenceDetailsRequestDto) {
		ReferenceDetailsResponseDto referenceDetailsResponseDto = new ReferenceDetailsResponseDto();
		String message = null;

		User user = userRepo.findById(referenceDetailsRequestDto.getUserId()).get();

		if (user != null) {
			// if (user.getPinCode() == null) {

			ReferenceDetails referenceDetails = user.getReferenceDetails();

			if (referenceDetails == null || referenceDetailsRequestDto.isUpdateReferenceDetails()) {

				if (referenceDetails == null) {

					referenceDetails = new ReferenceDetails();
				}

				List<ReferenceDetailsDto> referenceList = referenceDetailsRequestDto.getReferenceDto();

				if (referenceList != null && !referenceList.isEmpty()) {

					Set<String> referenceSet = new HashSet<>();

					referenceList.forEach(n -> {
						referenceSet.add(n.getReferenceNumber());
					});

					if (referenceList.size() != referenceSet.size()) {
						throw new ActionNotAllowedException("Reference Numbers should not be same",
								ErrorCodes.INVALID_OPERATION);
					}

					if (referenceList.size() == 1) {

						referenceDetails.setReference1(referenceList.get(0).getReferenceNumber());

					} else {
						for (int i = 0; i < referenceList.size(); i++) {
							if (i == 0) {
								referenceDetails.setReference1(referenceList.get(i).getReferenceNumber());
							}
							if (i == 1) {

								referenceDetails.setReference2(referenceList.get(i).getReferenceNumber());
							}

							if (i == 2) {
								referenceDetails.setReference3(referenceList.get(i).getReferenceNumber());
							}

							if (i == 3) {
								referenceDetails.setReference4(referenceList.get(i).getReferenceNumber());
							}

							if (i == 4) {
								referenceDetails.setReference5(referenceList.get(i).getReferenceNumber());
							}

							if (i == 5) {
								referenceDetails.setReference6(referenceList.get(i).getReferenceNumber());
							}

							if (i == 6) {
								referenceDetails.setReference7(referenceList.get(i).getReferenceNumber());
							}

							if (i == 7) {
								referenceDetails.setReference8(referenceList.get(i).getReferenceNumber());
							}

						}
					}

					user.setReferenceDetails(referenceDetails);

				}

				referenceDetails.setUserId(referenceDetailsRequestDto.getUserId());

				referenceDetails.setCreated_on(new Date());
				message = "successfullyUpdated";
				referenceDetails.setUser(user);
				user.setReferenceDetails(referenceDetails);
				userRepo.save(user);
			}

		}

		referenceDetailsResponseDto.setStatus(message);
		referenceDetailsResponseDto.setUserId(referenceDetailsRequestDto.getUserId());
		return referenceDetailsResponseDto;

	}

	@Override
	@Transactional
	public UserRegistrationResponseDto oldUsersRegistrationStep2Pending(
			UserRegistrationRequestDto userRegistrationRequestDto) {

		User user = userRepo.findById(userRegistrationRequestDto.getUserId()).get();
		if (user != null) {
			Integer emailCheck = userRepo.findByEmailUsingEqualIgnoranceCase(userRegistrationRequestDto.getEmail());
			if (emailCheck > 0) {
				throw new ActionNotAllowedException("Registation already done using this email",
						ErrorCodes.ACTION_ALREDY_DONE);
			}
			user.setEmail(userRegistrationRequestDto.getEmail());
			userRepo.save(user);
			String name = null;
			if (user.getPersonalDetails() != null) {
				if (user.getPersonalDetails().getFirstName() != null
						&& user.getPersonalDetails().getLastName() != null) {
					name = user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName();
				}

			}
			TemplateContext templateContext1 = new TemplateContext();
			templateContext1.put("userName", name);
			Date date = new Date();
			String date1 = expectedDateFormat.format(date);
			templateContext1.put("CurrentDate", date1);
			/*
			 * if (user.getPrimaryType() == PrimaryType.LENDER) { LoanRequestDto
			 * loanRequestDto = new LoanRequestDto(); loanRequestDto.setDuration(6);
			 * loanRequestDto.setLoanPurpose("Initiated the Request");
			 * loanRequestDto.setLoanRequestAmount(5000.00);
			 * loanRequestDto.setRateOfInterest(0d);
			 * loanRequestDto.setRepaymentMethod(RepaymentMethod.I.name()); Calendar
			 * expectedDate = Calendar.getInstance(); expectedDate.add(Calendar.DATE, 1);
			 * loanRequestDto.setExpectedDate(expectedDateFormat.format(expectedDate.getTime
			 * ()));
			 * 
			 * PasswordGrantAccessToken accessToken = new PasswordGrantAccessToken(6000,
			 * EncodingAlgorithm.RSA); accessToken.setUserId(user.getId().toString());
			 * ThreadLocalSecurityProvider.setAccessToken(accessToken); LoanResponseDto
			 * loanRequest =
			 * loanServiceFactory.getService(user.getPrimaryType().name().toUpperCase())
			 * .loanRequest(user.getId(), loanRequestDto);
			 * logger.info("Created Loan Request for the user {} as request id {}",
			 * user.getId(), loanRequest.getLoanRequestId()); }
			 */

			if (user.getCitizenship().equals(user.getCitizenship().NONNRI)) {
				LenderReferenceDetails lenderReference = lenderReferenceDetailsRepo
						.findByRefereeMobileNumber(user.getMobileNumber());
				if (lenderReference != null) {
					lenderReference.setRefereeName(name);
					lenderReference.setSource(LenderReferenceDetails.Source.ReferralLink);
					lenderReference.setRefereeEmail(userRegistrationRequestDto.getEmail());
					lenderReference.setStatus(LenderReferenceDetails.Status.Registered);

					lenderReferenceDetailsRepo.save(lenderReference);
				}

			}
			TemplateContext templateContext = new TemplateContext();
			Long currentTimeInMilliSec = System.currentTimeMillis();
			String verifyLink = new StringBuffer().append(this.emailActiviationLink
					.replace("{id}", user.getId().toString()).replace("{time}", currentTimeInMilliSec.toString()))
					.toString();
			templateContext.put("verifyLink", verifyLink);
			String emailVerification = "Email Activation";
			String templateName = "email-verification.template";

			EmailRequest emailRequest = new EmailRequest(new String[] { userRegistrationRequestDto.getEmail() },
					emailVerification, templateName, templateContext);
			EmailResponse emailResponse = emailService.sendEmail(emailRequest);
			if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
				throw new RuntimeException(emailResponse.getErrorMessage());
			}
		}
		UserRegistrationResponseDto userRegistrationResponseDto = new UserRegistrationResponseDto();
		userRegistrationResponseDto.setUserId(user.getId());
		return userRegistrationResponseDto;

	}

	@Override
	public OxyUserTypeResponseDto generatingPasswordForPartners() {

		OxyUserTypeResponseDto response = new OxyUserTypeResponseDto();
		List<OxyUserType> listOfUtms = oxyUserTypeRepo.findingAllUtms();
		if (listOfUtms != null && !listOfUtms.isEmpty()) {
			for (OxyUserType utm : listOfUtms) {

				if (utm.getPassword() == null || utm.getPassword().isEmpty()) {
					String salt = UUID.randomUUID().toString() + UUID.randomUUID().toString().substring(0, 10);
					String password = "Test@123";
					String hashedpassword = signatureService.hashPassword(password, salt);

					utm.setPassword(hashedpassword);
					utm.setSalt(salt);

					oxyUserTypeRepo.save(utm);

					response.setStatus("Success");
				}

			}
		}
		return response;

	}

	@Override
	public UserResponse savingPartnerDetails(UserRequest userRequest) {

		UserResponse userResponse = new UserResponse();
		if (userRequest.getPartnerEmail() != null && userRequest.getPartnerMobileNumber() != null) {
			OxyUserType oxyUserType = oxyUserTypeRepo.findingUtm(userRequest.getPartnerUtmName());
			if (oxyUserType != null) {
				oxyUserType.setEmail(userRequest.getPartnerEmail());
				oxyUserType.setMobileNumber(userRequest.getPartnerMobileNumber());

				oxyUserTypeRepo.save(oxyUserType);

				userResponse.setPartnerEmail(oxyUserType.getEmail());
				userResponse.setPartnerMobileNumber(oxyUserType.getMobileNumber());
			} else {
				throw new ActionNotAllowedException("Partner is not present in database with given utm",
						ErrorCodes.ENITITY_NOT_FOUND);
			}
		}

		return userResponse;

	}

	@Override
	public UserResponse resetpasswordForPartners(UserRequest userRequest) {

		UserResponse response = new UserResponse();

		OxyUserType oxyUserType = oxyUserTypeRepo.findingUtm(userRequest.getPartnerUtmName());

		if (oxyUserType != null) {
			String salt = UUID.randomUUID().toString() + UUID.randomUUID().toString().substring(0, 10);

			String hashedpassword = signatureService.hashPassword(userRequest.getPartnerPassword(), salt);

			oxyUserType.setPassword(hashedpassword);
			oxyUserType.setSalt(salt);

			oxyUserTypeRepo.save(oxyUserType);

			response.setStatus("Success");
		} else {
			throw new ActionNotAllowedException("Partner is not present in database with given utm",
					ErrorCodes.ENITITY_NOT_FOUND);
		}

		return response;

	}

	@Override
	public OxyUserDetailsResponse gettingPartnerDetailsBasedOnUtm(String utmName) {

		OxyUserDetailsResponse response = new OxyUserDetailsResponse();

		OxyUserType oxyUserType = oxyUserTypeRepo.findingUtm(utmName);

		if (oxyUserType != null) {
			List<User> listOfLenders = userRepo.fingingListOfLendersBasedOnUtm(oxyUserType.getUtmName());

			Integer countOfLenders = userRepo.findingCountOfLenders(oxyUserType.getUtmName());

			response.setCountOfLenders(countOfLenders);

			List<LenderUserDetails> listOfLenderUserDetails = new ArrayList<LenderUserDetails>();

			if (listOfLenders != null && !listOfLenders.isEmpty()) {
				for (User user : listOfLenders) {
					LenderUserDetails lenderUser = new LenderUserDetails();

					lenderUser.setUserId(user.getId());
					lenderUser.setName((user.getPersonalDetails().getFirstName() + " "
							+ user.getPersonalDetails().getLastName()) == null ? ""
									: (user.getPersonalDetails().getFirstName() + " "
											+ user.getPersonalDetails().getLastName()));

					lenderUser.setEmail(user.getEmail() == null ? "" : user.getEmail());
					lenderUser.setMobileNumber(user.getMobileNumber() == null ? "" : user.getMobileNumber());
					if (user.getBankDetails() != null) {
						lenderUser.setAccountNumber(user.getBankDetails().getAccountNumber());
						lenderUser.setBankName(user.getBankDetails().getBankName());
						lenderUser.setBranchName(user.getBankDetails().getBranchName());
						lenderUser.setIfscCode(user.getBankDetails().getIfscCode());
						lenderUser.setBankAddress(user.getBankDetails().getAddress());
						lenderUser.setNameAccordingToBank(user.getBankDetails().getUserName());
					}
					/*
					 * Double sumOfAmountParticipated = oxyLendersAcceptedDealsRepo
					 * .findingSumOfAmountParticipatedByUser(user.getId());
					 * 
					 * if (sumOfAmountParticipated == null) { sumOfAmountParticipated = 0.0; }
					 * 
					 * Double sumOfAmountUpdated = lendersPaticipationUpdationRepo
					 * .findingSumOfAmountUpdatedByUser(user.getId());
					 * 
					 * if (sumOfAmountUpdated == null) { sumOfAmountUpdated = 0.0; }
					 * 
					 * lenderUser.setAmountLentByLender((sumOfAmountParticipated +
					 * sumOfAmountUpdated));
					 */
					listOfLenderUserDetails.add(lenderUser);
				}
			}

			response.setListOfLenders(listOfLenderUserDetails);

			List<User> listOfBorrowers = userRepo.findingListOfBorrowers(oxyUserType.getUtmName());

			List<BorrowerUserDetails> listOfBorrowerUserDetails = new ArrayList<BorrowerUserDetails>();
			Integer countOfBorrowers = userRepo.findingCountOfBorrowers(oxyUserType.getUtmName());

			response.setCountOfBorrowers(countOfBorrowers);

			if (listOfBorrowers != null && !listOfBorrowers.isEmpty()) {
				for (User user : listOfBorrowers) {
					BorrowerUserDetails borrowerUser = new BorrowerUserDetails();

					borrowerUser.setUserId(user.getId());
					borrowerUser.setName((user.getPersonalDetails().getFirstName() + " "
							+ user.getPersonalDetails().getLastName()) == null ? ""
									: (user.getPersonalDetails().getFirstName() + " "
											+ user.getPersonalDetails().getLastName()));

					borrowerUser.setEmail(user.getEmail() == null ? "" : user.getEmail());
					borrowerUser.setMobileNumber(user.getMobileNumber() == null ? "" : user.getMobileNumber());
					if (user.getBankDetails() != null) {
						borrowerUser.setAccountNumber(user.getBankDetails().getAccountNumber());
						borrowerUser.setBankName(user.getBankDetails().getBankName());
						borrowerUser.setBranchName(user.getBankDetails().getBranchName());
						borrowerUser.setIfscCode(user.getBankDetails().getIfscCode());
						borrowerUser.setBankAddress(user.getBankDetails().getAddress());
						borrowerUser.setNameAccordingToBank(user.getBankDetails().getUserName());
					}
					LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId());
					if (loanRequest != null) {
						if (loanRequest.getRateOfInterest() > 0) {
							borrowerUser.setRequestSubmitedStatus(true);
							borrowerUser.setRequestedAmount(loanRequest.getLoanRequestAmount());
							borrowerUser.setDuration(loanRequest.getDuration());
							borrowerUser.setRateOfInterest(loanRequest.getRateOfInterest());
							borrowerUser.setDurationType(loanRequest.getDurationType().toString());
							borrowerUser.setRepaymentMethod(loanRequest.getRepaymentMethod().toString());
						} else {
							borrowerUser.setRequestSubmitedStatus(false);
						}
					}
					/*
					 * Double sumOfAmount =
					 * oxyLoanRepo.findingSumOfAmountDisbursedForBorrower(user.getId());
					 * 
					 * borrowerUser.setSumOfAmountDisbursed(sumOfAmount); Integer countOfLoans =
					 * oxyLoanRepo.findingCountOfLoans(user.getId());
					 * 
					 * if (countOfLoans != null) { borrowerUser.setNoOfLoans(countOfLoans); }
					 * 
					 * List<String> listOfLoanRequests =
					 * oxyLoanRepo.findingListOfApplications(user.getId());
					 * 
					 * List<String> listOfApplications = new ArrayList<String>(); if
					 * (listOfLoanRequests != null && !listOfLoanRequests.isEmpty()) { for (String
					 * loanRequest : listOfLoanRequests) {
					 * 
					 * String applicationNumber = loanRequest;
					 * 
					 * listOfApplications.add(applicationNumber); } }
					 * 
					 * borrowerUser.setApplicationNumber(listOfApplications);
					 */

					listOfBorrowerUserDetails.add(borrowerUser);
				}

				response.setListOfBorrowers(listOfBorrowerUserDetails);

			}

			response.setUtmName(oxyUserType.getUtmName());
		}

		return response;

	}

	@Override
	public PartnerResponseDto partnerRegistrationFlow(PartnerRequestDto requestDto) {

		PartnerResponseDto response = new PartnerResponseDto();

		String utmName = requestDto.getPartnerName().replaceAll(" ", "");

		String parterUtm = "OXY_P" + utmName.substring(0, 5);

		if (requestDto.getPartnerName() != null) {
			OxyUserType oxyUserType = oxyUserTypeRepo.findingUtm(parterUtm);

			if (oxyUserType != null) {
				throw new ActionNotAllowedException("Utm name already exists.", ErrorCodes.ACTION_ALREDY_DONE);
			} else {
				OxyUserType userType = new OxyUserType();

				userType.setCreatedOn(new Date());
				userType.setPartnerName(requestDto.getPartnerName());
				userType.setUtmName(parterUtm);
				userType.setEmail(requestDto.getPartnerEmail());
				userType.setMobileNumber(requestDto.getPartnermobileNumber());

				String salt = UUID.randomUUID().toString() + UUID.randomUUID().toString().substring(0, 10);

				String hashedpassword = signatureService.hashPassword("Test@123", salt);
				userType.setSalt(salt);
				userType.setPassword(hashedpassword);
				userType.setUserType(OxyUserType.UserType.PARTNER);
				userType.setPointOfContact(requestDto.getListOfPoCEmail());
				userType.setPointOfContactMobileNumbers(requestDto.getListOfPoCMobileNumber());
				userType.setPointOfContactName(requestDto.getPocName());

				userType.setDuration(12);
				userType.setFirstCibilScore(5);
				userType.setSecondCibilScore(4.5);
				userType.setThirdCibilScore(4);
				userType.setFourthCibilScore(3.5);
				userType.setFifthCibilScore(3);
				userType.setLoanAmountCalculation(1);
				userType.setRepaymentMethod(OxyUserType.RepaymentMethod.I);

				oxyUserTypeRepo.save(userType);

				Calendar current = Calendar.getInstance();
				TemplateContext templateContext = new TemplateContext();

				String expectedCurrentDate = expectedDateFormat.format(current.getTime());
				templateContext.put("currentDate", expectedCurrentDate);

				String lenderLink = new StringBuffer()
						.append(this.partnerLenderRegistrationLink.replace("{utm_name}", parterUtm)).toString();

				String borrowerLink = new StringBuffer()
						.append(this.partnerBorrowerRegistrationLink.replace("{utm_name}", parterUtm)).toString();

				templateContext.put("lenderLink", lenderLink);
				templateContext.put("borrowerLink", borrowerLink);

				String displayerName = userType.getUtmName();
				String templateName = "partner-register.template";

				EmailRequest emailRequest = new EmailRequest(new String[] { requestDto.getPartnerEmail() },
						displayerName, templateName, templateContext);
				EmailResponse emailResponse = emailService.sendEmail(emailRequest);
				if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
					throw new RuntimeException(emailResponse.getErrorMessage());
				}

				TemplateContext templateContext1 = new TemplateContext();
				templateContext1.put("currentDate", expectedCurrentDate);
				templateContext1.put("userName", userType.getUtmName());
				templateContext1.put("password", "Test@123");
				templateContext1.put("login", partnerLoginLink);

				String displayerName1 = userType.getUtmName() + " Login Details";
				String templateName1 = "partner-login-details.template";

				EmailRequest emailRequest1 = new EmailRequest(new String[] { requestDto.getPartnerEmail() },
						displayerName1, templateName1, templateContext1);
				EmailResponse emailResponse1 = emailService.sendEmail(emailRequest1);
				if (emailResponse1.getStatus() == EmailResponse.Status.FAILED) {
					throw new RuntimeException(emailResponse1.getErrorMessage());
				}

				String toEditDetails = new StringBuffer()
						.append(this.partnerToEditDetails.replace("{id}", userType.getId().toString())).toString();

				SimpleDateFormat sd = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");

				sd.setTimeZone(TimeZone.getTimeZone("IST"));

				String modified = sd.format(new Date());

				String msgToPartner = "Hi " + "*" + requestDto.getPartnerName() + "*" + ",\n\n"
						+ "Welcome to oxyloans. Thanks for registering with us." + "\n\n"
						+ "*Our partnership will enable your users to Lend/Invest and borrow money.*" + "\n\n"
						+ "Enable your users to register as oxyloans lender/investor with the below link:" + "\n"
						+ lenderLink + "\n\n"
						+ "Enable your users to register as oxyloans borrower with the below link:" + "\n"
						+ borrowerLink + "\n\n" + "Thanks & Regards," + "\n" + "Oxyloans Team";

				/*
				 * whatappService.sendingWhatsappMsgWithMobileNumber(91 +
				 * requestDto.getPartnermobileNumber(), msgToPartner, wtappApi, null);
				 */

				whatappService.sendingIndividualMessageUsingUltraApi(91 + requestDto.getPartnermobileNumber(),
						msgToPartner);

				String msgToGroup = "*" + requestDto.getPartnerName().trim() + "*" + " has registered as a partner on "
						+ "*" + modified + "*" + "\nUTM: " + "*" + parterUtm + "*" + "\n"
						+ "Admin has to fill the details of repayment method and cibil score for the partner using the below link."
						+ "\n" + toEditDetails;

				/*
				 * whatappService.sendingWhatsappMsgWithMobileNumberToGroup(
				 * "120363024766214462@g.us", msgToGroup, wtappApi);
				 */

				whatappService.whatsappMessageToGroupUsingUltra("120363024766214462@g.us", msgToGroup);

				TemplateContext context = new TemplateContext();
				context.put("currentDate", expectedCurrentDate);
				context.put("editLink", toEditDetails);

				String displayerNameNew = userType.getUtmName() + " PARTNER";
				String templateNameNew = "partner-details.template";

				EmailRequest emailRequestNew = new EmailRequest(
						new String[] { "ramadevi@oxyloans.com", "narendra@oxyloans.com", "liveen@oxyloans.com",
								"radhakrishna.t@oxyloans.com", "sriram@oxyloans.com" },
						displayerNameNew, templateNameNew, context);
				EmailResponse emailResponseNew = emailService.sendEmail(emailRequestNew);
				if (emailResponseNew.getStatus() == EmailResponse.Status.FAILED) {
					throw new RuntimeException(emailResponse1.getErrorMessage());
				}

				response.setEmail(requestDto.getPartnerEmail());

				response.setMobileNumber(requestDto.getPartnermobileNumber());

				response.setUtmName(parterUtm);

			}
		}

		return response;

	}

	@Override
	public LenderMailResponse showingMailContentToPartner(Integer partnerId) {

		OxyUserType oxyUser = oxyUserTypeRepo.findingPartnerDetails(partnerId);

		LenderMailResponse response = new LenderMailResponse();
		if (oxyUser != null) {

			String referrerName = oxyUser.getPartnerName() == null ? "" : oxyUser.getPartnerName();

			String mailSubject = referrerName + " || OxyLoans - Interesting FinTech Platform";

			String mobileNumber = oxyUser.getMobileNumber() == null ? "" : oxyUser.getMobileNumber();

			String message = "Greetings from " + referrerName + "! I am an existing " + "partner"
					+ " in OxyLoans.com! OXYLOANS.com– RBI Approved Peer 2 Peer Lending Platform. OxyLoans enables every individual to Lend Money Like A Bank."
					+ "\n\n";

			String mailContent = message
					+ "I am Investing multiples of INR 50,000 in numerous deals, distributing risk, and maximizing monthly profit. I am earning a profit of on average 24% per year, an average 2% Pay-out. My friends from MNCs like TCS, Microsoft, IBM, Cap Gemini, Yash, etc have started their investment journey with small amounts (like 50k) and currently investing in lakhs. If this interests you, if you also want to earn like them then OxyLoans is the connection."
					+ "\n\n";

			String bottomOfTheMail = "I am sending this e-mail on my own interest, I have experienced good response directly from the founder and respective team. Please feel free to reach me on "
					+ mobileNumber;

			response.setMailSubject(mailSubject);
			response.setMailContent(mailContent);
			response.setBottomOfTheMail(bottomOfTheMail);

		}

		return response;
	}

	@Override
	public LendersWithdrawResponse withdrawalInformationBasedOnId(Integer dealId) {

		LendersWithdrawResponse lendersWithdrawResponse = new LendersWithdrawResponse();

		List<LendersReturns> listOfWithdrawalsInRequestedState = lendersReturnsRepo
				.findingWithdrawalsInRequestedState(dealId);

		List<LendersReturnResponse> listOfWithdrawRequests = new ArrayList<LendersReturnResponse>();

		if (listOfWithdrawalsInRequestedState != null && !listOfWithdrawalsInRequestedState.isEmpty()) {
			for (LendersReturns withdrawInfo : listOfWithdrawalsInRequestedState) {
				LendersReturnResponse lendersReturnResponse = new LendersReturnResponse();

				lendersReturnResponse.setUserId(withdrawInfo.getUserId() == null ? 0 : withdrawInfo.getUserId());
				lendersReturnResponse.setAmount(withdrawInfo.getAmount());

				if (withdrawInfo.getRequestedOn() != null) {

					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					String modified = format.format(withdrawInfo.getRequestedOn());

					lendersReturnResponse.setRequestedOn(modified);

				}
				lendersReturnResponse.setStatus(withdrawInfo.getStatus().toString());

				lendersReturnResponse.setAmountType(withdrawInfo.getAmountType());

				listOfWithdrawRequests.add(lendersReturnResponse);

			}
		}

		List<LendersReturnResponse> listOfWithdrawInitiated = new ArrayList<LendersReturnResponse>();

		List<LendersReturns> withdrawInitiatedState = lendersReturnsRepo.findingWithdrawalIntiatedList(dealId);

		if (withdrawInitiatedState != null && !withdrawInitiatedState.isEmpty()) {
			for (LendersReturns initiatedInfo : withdrawInitiatedState) {
				LendersReturnResponse response = new LendersReturnResponse();

				response.setAmount(initiatedInfo.getAmount());
				response.setUserId(initiatedInfo.getUserId());
				response.setAmountType(initiatedInfo.getAmountType());

				if (initiatedInfo.getPaidDate() != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					String modified = format.format(initiatedInfo.getPaidDate());

					response.setPaidDate(modified);
				}

				response.setStatus(initiatedInfo.getStatus().toString());

				listOfWithdrawInitiated.add(response);

			}
		}

		BigInteger amountParticipatedInDeal = oxyLendersAcceptedDealsRepo.findingSumOfAmountParticipatedInDeal1(dealId);

		BigInteger amountUpdatedInDeal = oxyLendersAcceptedDealsRepo.findingSumOfAmountUpdatedInDeal1(dealId);

		BigInteger totalAmountParticipatedInDeal = null;
		if (amountParticipatedInDeal == null) {
			amountParticipatedInDeal = BigInteger.ZERO;
		}

		if (amountUpdatedInDeal == null) {
			amountUpdatedInDeal = BigInteger.ZERO;
		}

		totalAmountParticipatedInDeal = amountParticipatedInDeal.add(amountUpdatedInDeal);

		if (totalAmountParticipatedInDeal == null) {
			totalAmountParticipatedInDeal = BigInteger.ZERO;
		}

		BigInteger sumOfAmountWithdrawInitiated = lendersReturnsRepo.findingSumOfPrincipalAmountReturned1(dealId);

		if (sumOfAmountWithdrawInitiated == null) {
			sumOfAmountWithdrawInitiated = BigInteger.valueOf(0);
		}

		BigInteger remainingAmount = totalAmountParticipatedInDeal.subtract(sumOfAmountWithdrawInitiated);

		OxyBorrowersDealsInformation dealInfo = oxyBorrowersDealsInformationRepo.findBydealId(dealId);
		if (dealInfo != null) {

			BigInteger dealValue = BigDecimal.valueOf(dealInfo.getDealAmount()).toBigInteger();

			lendersWithdrawResponse.setDealValue(dealValue);
		}

		lendersWithdrawResponse.setListOfWithdrawRequests(listOfWithdrawRequests);
		lendersWithdrawResponse.setListOfWithdrawInitiated(listOfWithdrawInitiated);
		lendersWithdrawResponse.setTotalAmountPArticipatedIndeal(totalAmountParticipatedInDeal);
		lendersWithdrawResponse.setRemainingAmount(remainingAmount);
		lendersWithdrawResponse.setTotalAmountReturned(sumOfAmountWithdrawInitiated);

		return lendersWithdrawResponse;

	}

	public String uuidFromMobileDevice(int userId, String uuid) {
		String message = null;
		User user = userRepo.findById(userId).get();
		if (user != null) {
			OxyUsersuuidFromMobiledevice oxyUsersuuidFromMobiledevice = user.getOxyUsersuuidFromMobiledevice();
			if (oxyUsersuuidFromMobiledevice == null) {
				oxyUsersuuidFromMobiledevice = new OxyUsersuuidFromMobiledevice();
				oxyUsersuuidFromMobiledevice.setUuid(uuid);
				oxyUsersuuidFromMobiledevice.setCreatedOn(new Date());
				oxyUsersuuidFromMobiledevice.setUser(user);
				user.setOxyUsersuuidFromMobiledevice(oxyUsersuuidFromMobiledevice);
				userRepo.save(user);
				message = "successfullyUpdated";
			} else {
				oxyUsersuuidFromMobiledevice.setUuid(uuid);
				oxyUsersuuidFromMobiledevice.setModifiedOn(new Date());
				oxyUsersuuidFromMobiledevice.setUser(user);
				user.setOxyUsersuuidFromMobiledevice(oxyUsersuuidFromMobiledevice);
				userRepo.save(user);
				message = "successfullyUpdated";
			}
		}
		return message;
	}

	public MobileDeviceResponseDto displayingIpAndUuid(int userId) {
		MobileDeviceResponseDto mobileDeviceResponseDto = new MobileDeviceResponseDto();
		User user = userRepo.findById(userId).get();
		if (user != null) {
			if (user.getOxyUsersuuidFromMobiledevice() != null) {
				mobileDeviceResponseDto.setUuid(user.getOxyUsersuuidFromMobiledevice().getUuid());
			}

		}
		return mobileDeviceResponseDto;

	}

	@Override
	public PartnerResponseDto verifyingMobileAndEmailForPartner(OxyUserTypeRequestDto requestDto)
			throws MessagingException {

		PartnerResponseDto partnerResponseDto = new PartnerResponseDto();

		OxyUserType oxyUser = oxyUserTypeRepo.findingUtm(requestDto.getUtmName());

		if (oxyUser != null) {

			if (requestDto.getUtmName() != null && requestDto.getPartnerMobileNumber() != null
					&& requestDto.getMobileOtpValue() == null && requestDto.getPartnerEmail() == null
					&& requestDto.getEmailOtpValue() == null) {

				if (oxyUser.isMobileNumberVerified() && oxyUser.isEmailVerified()) {
					throw new EntityAlreadyExistsException(
							"Partner " + requestDto.getPartnerMobileNumber() + " already verified",
							ErrorCodes.ENTITY_ALREDY_EXISTS);
				} else if (oxyUser.isMobileNumberVerified()) {

					partnerResponseDto = new PartnerResponseDto();

					partnerResponseDto.setId(oxyUser.getId());
					partnerResponseDto.setMobileNumber(oxyUser.getMobileNumber());
					partnerResponseDto.setMobileverified(oxyUser.isMobileNumberVerified());

					return partnerResponseDto;
				} else {

					partnerResponseDto = new PartnerResponseDto();

					String otpSessionId = mobileUtil.sendOtp(requestDto.getPartnerMobileNumber());

					partnerResponseDto.setMobileNumber(requestDto.getPartnerMobileNumber());
					partnerResponseDto.setMobileOtpSession(otpSessionId);

					return partnerResponseDto;
				}

			}
			if (requestDto.getUtmName() != null && requestDto.getPartnerMobileNumber() != null
					&& requestDto.getMobileOtpValue() != null) {

				boolean verifyOtp = false;

				verifyOtp = mobileUtil.verifyOtp(requestDto.getPartnerMobileNumber(), requestDto.getMobileOtpSession(),
						requestDto.getMobileOtpValue());

				if (!verifyOtp) {
					throw new MobileOtpVerifyFailedException("Invalid OTP value please check.", ErrorCodes.INVALID_OTP);
				}

				oxyUser.setMobileNumberVerified(true);

				oxyUser.setMobileNumber(requestDto.getPartnerMobileNumber());

				oxyUserTypeRepo.save(oxyUser);

				partnerResponseDto.setId(oxyUser.getId());
				partnerResponseDto.setMobileNumber(oxyUser.getMobileNumber());

				return partnerResponseDto;

			}

			if (requestDto.getUtmName() != null && requestDto.getPartnerMobileNumber() != null
					&& requestDto.getPartnerEmail() != null && requestDto.getEmailOtpValue() == null) {

				if (!oxyUser.isMobileNumberVerified()) {
					throw new ActionNotAllowedException("User mobile Not Verified", ErrorCodes.PERMISSION_DENIED);
				}

				Random random = new Random();
				String id = String.format("%06d", random.nextInt(999999));
				TemplateContext templateContext = new TemplateContext();
				templateContext.put("otp", id);
				String hashedEmailOtpSession = signatureService.hashPassword(id, oxyUser.getSalt());
				String mailOtpSubject = "OTP From oxyloans";
				EmailRequest emailRequest = new EmailRequest(new String[] { requestDto.getPartnerEmail() },
						mailOtpSubject, "email-otp-partner.template", templateContext);
				EmailResponse emailResponse = emailService.sendEmail(emailRequest);
				if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
					throw new MessagingException(emailResponse.getErrorMessage()); // TODO need to fill this
																					// property
				}

				oxyUser.setEmail(requestDto.getPartnerEmail());
				oxyUser.setEmailOtpSession(hashedEmailOtpSession);

				oxyUserTypeRepo.save(oxyUser);

				partnerResponseDto = new PartnerResponseDto();
				partnerResponseDto.setId(oxyUser.getId());
				partnerResponseDto.setMobileNumber(requestDto.getPartnerMobileNumber());
				partnerResponseDto.setEmail(requestDto.getPartnerEmail());

				return partnerResponseDto;

			}

			if (requestDto.getUtmName() != null && requestDto.getPartnerEmail() != null
					&& requestDto.getEmailOtpValue() != null) {

				String token = oxyUser.getEmailOtpSession();
				String hashedEmailOtpSession = signatureService.hashPassword(requestDto.getEmailOtpValue(),
						oxyUser.getSalt());
				if (!hashedEmailOtpSession.equals(token)) {
					throw new ActionNotAllowedException("Email not verified", ErrorCodes.EMAIL_NOT_VERIFIED);
				}
				oxyUser.setEmailVerified(true);

				oxyUserTypeRepo.save(oxyUser);

				partnerResponseDto = new PartnerResponseDto();
				partnerResponseDto.setId(oxyUser.getId());
				partnerResponseDto.setMobileNumber(oxyUser.getMobileNumber());
				partnerResponseDto.setEmail(oxyUser.getEmail());
				partnerResponseDto.setEmailVerified(oxyUser.isEmailVerified());

				return partnerResponseDto;

			}

		}

		return partnerResponseDto;

	}

	@Override
	public PartnerResponseDto gettingCountOfLendersAndBorrowersForPartner(String utmName) {

		PartnerResponseDto partnerResponseDto = new PartnerResponseDto();

		OxyUserType oxyUser = oxyUserTypeRepo.findingUtm(utmName);

		if (oxyUser != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

			Date date = new Date();

			String currentDate = format.format(date);

			List<Object[]> partnerDashboard = oxyUserTypeRepo.partnerDashboard(utmName, currentDate);
			if (partnerDashboard != null && !partnerDashboard.isEmpty()) {
				Iterator lt = partnerDashboard.iterator();
				while (lt.hasNext()) {
					Object[] e = (Object[]) lt.next();
					partnerResponseDto.setTodayRegisteredLenders(
							BigDecimal.valueOf(Double.valueOf(e[0] == null ? "0" : e[0].toString())).toBigInteger());
					partnerResponseDto.setTotalLenders(
							BigDecimal.valueOf(Double.valueOf(e[1] == null ? "0" : e[1].toString())).toBigInteger());
					partnerResponseDto.setTodayRegisteredBorrowers(
							BigDecimal.valueOf(Double.valueOf(e[2] == null ? "0" : e[2].toString())).toBigInteger());
					partnerResponseDto.setTotalBorrowers(
							BigDecimal.valueOf(Double.valueOf(e[3] == null ? "0" : e[3].toString())).toBigInteger());
					partnerResponseDto.setRunningLoansCount(
							BigDecimal.valueOf(Double.valueOf(e[4] == null ? "0" : e[4].toString())).toBigInteger());
					partnerResponseDto.setRunningLoansAmount(
							BigDecimal.valueOf(Double.valueOf(e[5] == null ? "0" : e[5].toString())).toBigInteger());
					partnerResponseDto.setClosedLoansCount(
							BigDecimal.valueOf(Double.valueOf(e[6] == null ? "0" : e[6].toString())).toBigInteger());
					partnerResponseDto.setTotalDisbursedAmount(
							BigDecimal.valueOf(Double.valueOf(e[7] == null ? "0" : e[7].toString())).toBigInteger());
					partnerResponseDto.setTotalFeePaid(
							BigDecimal.valueOf(Double.valueOf(e[8] == null ? "0" : e[8].toString())).toBigInteger());
				}

			}

			partnerResponseDto.setMobileverified(oxyUser.isMobileNumberVerified());
			partnerResponseDto.setEmailVerified(oxyUser.isEmailVerified());

		}

		return partnerResponseDto;

	}

	@Override
	public PartnerResponseDto bankDetailsForPartner(String utmName, OxyUserTypeRequestDto requestDto) {

		OxyUserType oxyUser = oxyUserTypeRepo.findingUtm(utmName);

		PartnerResponseDto response = new PartnerResponseDto();

		if (oxyUser != null) {

			if (requestDto.getAccountNumber() != null) {
				oxyUser.setAccountNumber(requestDto.getAccountNumber());
			}

			if (requestDto.getBankName() != null && requestDto.getBankName().length() != 0) {
				oxyUser.setBankName(requestDto.getBankName());
			}

			if (requestDto.getIfscCode() != null && requestDto.getIfscCode().length() != 0) {
				oxyUser.setIfscCode(requestDto.getIfscCode());
			}

			if (requestDto.getUserName() != null && requestDto.getUserName().length() != 0) {
				oxyUser.setUserName(requestDto.getUserName());
			}

			if (requestDto.getBranchName() != null && requestDto.getBranchName().length() != 0) {
				oxyUser.setBranchName(requestDto.getBranchName());
			}

			oxyUserTypeRepo.save(oxyUser);

			response.setId(oxyUser.getId());
			response.setUtmName(oxyUser.getUtmName());
		}

		return response;

	}

	@Override
	public PartnerResponseDto partnerProfileDetails(String utmName) {
		OxyUserType oxyUser = oxyUserTypeRepo.findingUtm(utmName);

		PartnerResponseDto response = new PartnerResponseDto();

		if (oxyUser != null) {
			response.setId(oxyUser.getId());
			response.setName(oxyUser.getPartnerName() == null ? "" : oxyUser.getPartnerName());
			response.setEmail(oxyUser.getEmail() == null ? "" : oxyUser.getEmail());
			response.setMobileNumber(oxyUser.getMobileNumber() == null ? "" : oxyUser.getMobileNumber());
			response.setUtmName(oxyUser.getUserName() == null ? "" : oxyUser.getUserName());
			response.setAccountNumber(oxyUser.getAccountNumber() == null ? "" : oxyUser.getAccountNumber());
			response.setBankName(oxyUser.getBankName() == null ? "" : oxyUser.getBankName());
			response.setBranchName(oxyUser.getBranchName() == null ? "" : oxyUser.getBranchName());
			response.setUserName(oxyUser.getUserName() == null ? "" : oxyUser.getUserName());
			response.setIfscCode(oxyUser.getIfscCode() == null ? "" : oxyUser.getIfscCode());
		}

		return response;
	}

	@Override
	public PartnerResponseDto preparingPartnerNDA(String utmName, PartnerNDArequestDto requestDto) {

		PartnerResponseDto response = new PartnerResponseDto();
		OxyUserType oxyUser = oxyUserTypeRepo.findingUtm(utmName);

		if (oxyUser != null) {

			SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
			String date = format.format(new Date());
			String dayWeekText = new SimpleDateFormat("EEEE").format(new Date());

			String partnerName = "";
			String partnerEmail = "";

			Calendar c = Calendar.getInstance();
			c.setTime(new Date());

			c.add(Calendar.YEAR, 3);
			c.add(Calendar.DATE, -1);

			Date currentDatePlusOne = c.getTime();

			String updatedDate = format.format(currentDatePlusOne);

			if (requestDto.getPartnerName() != null) {
				partnerName = requestDto.getPartnerName();
			} else {
				partnerName = oxyUser.getPartnerName() == null ? "" : oxyUser.getPartnerName();
			}
			if (requestDto.getEmailId() != null) {
				partnerEmail = requestDto.getEmailId();
			} else {
				partnerEmail = oxyUser.getEmail();
			}

			TemplateContext templateContext = new TemplateContext();

			templateContext.put("date", date);
			templateContext.put("dayWeekText", dayWeekText);
			templateContext.put("partnerName", partnerName.replaceAll("&", "And"));
			templateContext.put("companyName", requestDto.getCompanyName().replaceAll("&", "And"));
			templateContext.put("address", requestDto.getCompanyAddress().replaceAll("&", "And"));
			templateContext.put("partnerEmail", partnerEmail.replaceAll("&", "And"));
			templateContext.put("title", requestDto.getTitle().replaceAll("&", "And"));
			templateContext.put("city", requestDto.getCity().replaceAll("&", "And"));
			templateContext.put("updatedDate", updatedDate);

			if (requestDto.getServices() != null) {
				templateContext.put("services", requestDto.getServices().replaceAll("&", "And"));
			}

			String outputFileName = "";

			String partnerAgreementType = "";

			if (requestDto.getType().equalsIgnoreCase("NDA")) {
				outputFileName = "partnerNDA1" + ".pdf";
				partnerAgreementType = pdfEngine.generatePdf("agreement/partnerNDA1.xml", templateContext,
						outputFileName);
			} else if (requestDto.getType().equalsIgnoreCase("MOU")) {
				outputFileName = "partnerMOU" + ".pdf";
				partnerAgreementType = pdfEngine.generatePdf("agreement/partnerMOU.xml", templateContext,
						outputFileName);
			}

			FileRequest fileRequest = new FileRequest();
			try {
				fileRequest.setInputStream(new FileInputStream(partnerAgreementType));
				fileRequest.setFileName(outputFileName);
				fileRequest.setFileType(FileType.PartnerAgreementType);
				fileRequest.setFilePrifix(FileType.PartnerAgreementType.name());
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				FileResponse putFile = fileManagementService.putFile(fileRequest);
				FileResponse file1 = fileManagementService.getFile(fileRequest);
				logger.info("Agreement file {} uploaded to as {}", outputFileName, putFile.getFileName());

				logger.info(file1.getDownloadUrl());
				String[] url = file1.getDownloadUrl().split(Pattern.quote("?"));

				response.setStatus(url[0]);

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

		}
		return response;

	}

	@Override
	@Transactional
	public PartnerRegistrationResponseDto additionalFieldsApprovalFromAdmin(int id,
			PartnerRegistrationRequestDto partnerRegistrationRequestDto) {
		PartnerRegistrationResponseDto partnerRegistrationResponseDto = new PartnerRegistrationResponseDto();
		try {
			OxyUserType oxyUserType = oxyUserTypeRepo.findById(id).get();
			if (oxyUserType != null) {
				if (partnerRegistrationRequestDto.getAdditionalFiledsToUser() != null) {
					oxyUserType.setAdditionalFieldsToUser(OxyUserType.AdditionalFieldsToUser
							.valueOf(partnerRegistrationRequestDto.getAdditionalFiledsToUser()));
					oxyUserTypeRepo.save(oxyUserType);
					partnerRegistrationResponseDto.setStatus("successfully Updated");
				}
			}
		} catch (Exception e) {
			System.out.println("Exception in additionalFieldsApprovalFromAdmin method");
		}

		return partnerRegistrationResponseDto;

	}

	@Override
	public List<PartnerLoanInfoResponse> partnerBorrowersLoanInfo(String utmName) {

		List<PartnerLoanInfoResponse> listOfResponse = new ArrayList<PartnerLoanInfoResponse>();

		List<Integer> listOfUsers = oxyCmsLoansRepo.findingListOfUsersBasedOnUtm();

		if (listOfUsers != null && !listOfUsers.isEmpty()) {
			for (Integer userId : listOfUsers) {
				User user = userRepo.findByIdNum(userId);
				OxyUserType oxyUser = oxyUserTypeRepo.findingUtm(utmName);

				if (oxyUser != null) {
					if (user != null) {
						if (oxyUser.getUtmName().equalsIgnoreCase(user.getUrchinTrackingModule())
								&& user.getPrimaryType() == PrimaryType.BORROWER) {

							LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);

							if (loanRequest != null) {

								PartnerLoanInfoResponse response = new PartnerLoanInfoResponse();

								response.setName(user.getPersonalDetails().getFirstName() + " "
										+ user.getPersonalDetails().getLastName());
								response.setRateOfInterest(loanRequest.getRateOfInterestToBorrower());
								response.setDuration(loanRequest.getDurationBySir());

								Double sumOfDisbursmentAmount = oxyLoanRepo.findingSumOfDisbursmentAmount(userId,
										loanRequest.getId());

								response.setDisbursmentAmount(sumOfDisbursmentAmount);

								response.setLoanId("APBR" + loanRequest.getId());

								Double sumOfEmiAmountToBePaid = 0.0;
								Double sumOfEmiAmountPaid = 0.0;
								Integer noOfEmisInFutureToBePaidInFuture = 0;

								Double sumOfEmiAmountToBePaidInFuture = 0.0;
								Integer noOfEmisPaid = 0;

								String status = "";

								if (sumOfDisbursmentAmount >= 50000) {

									sumOfEmiAmountToBePaid = applicationLevelLoanEmiCardRepo
											.findingSumOfEmiAmountToBePaid(loanRequest.getId());

									if (sumOfEmiAmountToBePaid == null) {
										sumOfEmiAmountToBePaid = 0.0;
									}

									sumOfEmiAmountPaid = applicationLevelLoanEmiCardRepo
											.findingSumOfEmiAmountPaid(loanRequest.getId());

									if (sumOfEmiAmountPaid == null) {
										sumOfEmiAmountPaid = 0.0;
									}

									noOfEmisInFutureToBePaidInFuture = applicationLevelLoanEmiCardRepo
											.findingNoOfEmisToBePaidFuture(loanRequest.getId());

									if (loanRequest.getRepaymentMethodForBorrower().equalsIgnoreCase("I")) {
										noOfEmisInFutureToBePaidInFuture = noOfEmisInFutureToBePaidInFuture - 1;
									}

									sumOfEmiAmountToBePaidInFuture = applicationLevelLoanEmiCardRepo
											.findingSumOfEmiAmountToBePaidInFuture(loanRequest.getId());

									if (sumOfEmiAmountToBePaidInFuture == null) {
										sumOfEmiAmountToBePaidInFuture = 0.0;
									}

									noOfEmisPaid = applicationLevelLoanEmiCardRepo
											.findingTotalNoOfEmisPaid(loanRequest.getId());

									List<ApplicationLevelLoanEmiCard> listOfEmis = applicationLevelLoanEmiCardRepo
											.findingListOfEmis(loanRequest.getId());

									if (listOfEmis == null) {
										status = "CLOSED";
									} else {
										status = "RUNNING";
									}

								} else {
									sumOfEmiAmountToBePaid = loanEmiCardRepo
											.findingSumOfEmiAmountToBePaid(loanRequest.getId());

									if (sumOfEmiAmountToBePaid == null) {
										sumOfEmiAmountToBePaid = 0.0;
									}

									sumOfEmiAmountPaid = loanEmiCardRepo.findingSumOfEmiAmountPaid(loanRequest.getId());

									if (sumOfEmiAmountPaid == null) {
										sumOfEmiAmountPaid = 0.0;
									}

									noOfEmisInFutureToBePaidInFuture = loanEmiCardRepo
											.findingNoOfEmisToBePaidFuture(loanRequest.getId());

									sumOfEmiAmountToBePaidInFuture = loanEmiCardRepo
											.findingSumOfEmiAmountToBePaidInFuture(loanRequest.getId());

									if (sumOfEmiAmountToBePaidInFuture == null) {
										sumOfEmiAmountToBePaidInFuture = 0.0;
									}

									noOfEmisPaid = loanEmiCardRepo.findingTotalNoOfEmisPaid(loanRequest.getId());

									List<LoanEmiCard> listOfEmis = loanEmiCardRepo
											.findingListOfEmis(loanRequest.getId());
									if (listOfEmis.isEmpty()) {
										status = "CLOSED";
									} else {
										status = "RUNNING";
									}
								}

								response.setSumOfEmiAmountDue(sumOfEmiAmountToBePaid - sumOfEmiAmountPaid);
								response.setSumOfEmiAmountToBePaid(sumOfEmiAmountToBePaid);
								response.setSumOfEmiAmountPaid(sumOfEmiAmountPaid);
								response.setTotalNoOfEmisToBePaidInFuture(noOfEmisInFutureToBePaidInFuture);
								response.setSumOfEmiAmountToBePaidInFuture(sumOfEmiAmountToBePaidInFuture);
								response.setTotalNoOfEmisPaid(noOfEmisPaid);
								response.setLoanStatus(status);

								listOfResponse.add(response);

							}

						}
					}
				}
			}
		}

		return listOfResponse;

	}

	@Override
	public UserResponse sendMobileOtp(UserRequest userRequest) {

		UserResponse userResponse = new UserResponse();

		String otpSessionId = mobileUtil.sendOtp(userRequest.getMobileNumber());

		userResponse.setMobileNumber(userRequest.getMobileNumber());
		userResponse.setMobileOtpSession(otpSessionId);
		return userResponse;

	}

	@Override
	public UserResponse verifyMobileOtpValue(UserRequest request) {

		UserResponse response = new UserResponse();
		boolean verifyOtp = false;

		logger.info(request.getMobileNumber());
		logger.info(request.getMobileOtpValue());
		logger.info(request.getMobileOtpSession());

		verifyOtp = mobileUtil.verifyOtp(request.getMobileNumber(), request.getMobileOtpSession(),
				request.getMobileOtpValue());

		if (!verifyOtp) {
			throw new MobileOtpVerifyFailedException("Invalid OTP value please check.", ErrorCodes.INVALID_OTP);
		}

		response.setMobileNumberVerified(verifyOtp);

		return response;
	}

	@Override
	@Transactional
	public UserRegistrationResponseDto forgotPasswordForMobileApp(
			UserRegistrationRequestDto userRegistrationRequestDto) {

		UserRegistrationResponseDto userRegistrationResponseDto = new UserRegistrationResponseDto();

		if (userRegistrationRequestDto.getEmail() != null) {
			User user = userRepo.findByEmailIgnoreCase(userRegistrationRequestDto.getEmail());
			if (user != null) {
				if (user.getEmailVerified() == true) {
					userRegistrationResponseDto = emailOtpGeneration(user);
				} else {
					throw new ActionNotAllowedException(
							" User has not verified the email=" + userRegistrationRequestDto.getEmail(),
							ErrorCodes.EMAIL_NOT_VERIFIED);
				}
			} else {
				throw new ActionNotAllowedException("The email address you provided is not present in the database",
						ErrorCodes.ACTION_ALREDY_DONE);
			}
			userRegistrationResponseDto.setUserId(user.getId());
		}
		if (userRegistrationRequestDto.getMobileNumber() != null) {
			User user = userRepo.findByMobileNumber(userRegistrationRequestDto.getMobileNumber());
			if (user != null) {
				userRegistrationResponseDto = new UserRegistrationResponseDto();
				String otpSessionId = mobileUtil.sendOtp(userRegistrationRequestDto.getMobileNumber());
				userRegistrationResponseDto.setMobileNumber(userRegistrationRequestDto.getMobileNumber());
				userRegistrationResponseDto.setMobileOtpSession(otpSessionId);
			} else {
				throw new ActionNotAllowedException("The mobileNumber you provided is not present in the database",
						ErrorCodes.ACTION_ALREDY_DONE);
			}
			userRegistrationResponseDto.setUserId(user.getId());

		}

		return userRegistrationResponseDto;

	}

	public UserRegistrationResponseDto emailOtpGeneration(User user) {
		UserRegistrationResponseDto userRegistrationResponseDto = new UserRegistrationResponseDto();
		Random random = new Random();
		String id = String.format("%06d", random.nextInt(999999));
		TemplateContext templateContext = new TemplateContext();
		templateContext.put("otp", id);
		String hashedEmailOtpSession = signatureService.hashPassword(id, user.getSalt());
		String mailOtpSubject = "OTP From Password Reset";
		EmailRequest emailRequest = new EmailRequest(new String[] { user.getEmail() }, mailOtpSubject,
				"password-reset.template", templateContext);
		EmailResponse emailResponse = emailService.sendEmail(emailRequest);
		if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
			throw new RuntimeException(emailResponse.getErrorMessage()); // property
		}
		userRegistrationResponseDto.setEmailOtpSession(hashedEmailOtpSession);
		Long currentTimeInMilliSec = System.currentTimeMillis();
		userRegistrationResponseDto.setTimeInMilliSeconds(currentTimeInMilliSec.toString());
		userRegistrationResponseDto.setUserId(user.getId());
		return userRegistrationResponseDto;

	}

	@Override
	@Transactional
	public UserRegistrationResponseDto forgotPasswordForMobileAppVerification(
			UserRegistrationRequestDto userRegistrationRequestDto) {
		UserRegistrationResponseDto userRegistrationResponseDto = new UserRegistrationResponseDto();
		User user = userRepo.findById(userRegistrationRequestDto.getUserId()).get();
		if (user != null) {
			if (userRegistrationRequestDto.getEmailOtpSession() != null
					&& userRegistrationRequestDto.getEmailOtp() != null) {
				Long registerdTime = Long.parseLong(userRegistrationRequestDto.getTimeInMilliSeconds());
				Date registerdTimeDate = new Date(registerdTime);

				Long currentTime = System.currentTimeMillis();
				Date currentTimeDate = new Date(currentTime);

				if (currentTimeDate.getTime() - registerdTimeDate.getTime() > Long.parseLong(this.emailTtl)) {
					throw new ActionNotAllowedException("Email activation link expired", ErrorCodes.TOKEN_NOT_VALID);
				}
				if (userRegistrationRequestDto.getEmailOtpSession() != null
						&& userRegistrationRequestDto.getEmailOtp() != null) {
					String token = userRegistrationRequestDto.getEmailOtpSession();
					String hashedEmailOtpSession = signatureService
							.hashPassword(userRegistrationRequestDto.getEmailOtp(), user.getSalt());

					if (!hashedEmailOtpSession.equals(token)) {
						throw new ActionNotAllowedException("Please check invalid email otp.",
								ErrorCodes.EMAIL_NOT_VERIFIED);
					}
				}
				user.setPassword(
						signatureService.hashPassword(userRegistrationRequestDto.getPassword(), user.getSalt()));
				userRepo.save(user);
			} else {
				boolean verifyOtp = false;
				verifyOtp = mobileUtil.verifyOtp(userRegistrationRequestDto.getMobileNumber(),
						userRegistrationRequestDto.getMobileOtpSession(),
						userRegistrationRequestDto.getMobileOtpValue());

				if (!verifyOtp) {
					throw new MobileOtpVerifyFailedException("Invalid OTP value please check.", ErrorCodes.INVALID_OTP);
				}
				user.setPassword(
						signatureService.hashPassword(userRegistrationRequestDto.getPassword(), user.getSalt()));
				userRepo.save(user);
			}
		}
		userRegistrationResponseDto.setUserId(user.getId());
		return userRegistrationResponseDto;
	}

	@Override
	@Transactional
	public UserRegistrationResponseDto updateSiteTool(int userId) {
		UserRegistrationResponseDto userRegistrationResponseDto = new UserRegistrationResponseDto();
		User user = userRepo.findById(userId).get();
		if (user != null) {
			if (user.getPersonalDetails() != null) {
				PersonalDetails personalDetails = user.getPersonalDetails();
				personalDetails.setSiteTool(true);
				user.setPersonalDetails(personalDetails);
				personalDetails.setUser(user);
				userRepo.save(user);
				userRegistrationResponseDto.setUserId(userId);
			}
		}
		return userRegistrationResponseDto;

	}

	@Transactional
	@Override
	public UserResponse getUserUniqueNumber(int userId) {
		User user = userRepo.findByIdNum(userId);
		UserResponse userResponse = null;
		if (user == null) {
			throw new ActionNotAllowedException(
					"The user ID you are trying to retrieve actually not exists in the database",
					ErrorCodes.ENITITY_NOT_FOUND);
		}
		if (user != null) {
			userResponse = new UserResponse();
			userResponse.setUniqueNumber(user.getUniqueNumber());
		}
		return userResponse;

	}

	@Override
	public UserRegistrationResponseDto updateCredentialByInvalidEmailEnterAtTimeOfRegistration(
			UserRegistrationRequestDto userRegistrationRequestDto) {
		User updateUser = userRepo.findByMobileNumber(userRegistrationRequestDto.getMobileNumber());

		if (updateUser == null) {
			throw new EntityNotFoundException("User with mobile number " + userRegistrationRequestDto.getMobileNumber()
					+ " does not exist." + ErrorCodes.ENITITY_NOT_FOUND);
		}

		if (updateUser.getPrimaryType().equals(User.PrimaryType.LENDER)
				|| updateUser.getPrimaryType().equals(User.PrimaryType.BORROWER)) {
			updateUser.setEmail(userRegistrationRequestDto.getEmail());
			userRepo.save(updateUser);

			userRegistrationRequestDto.setUserId(updateUser.getId());
			UserRegistrationResponseDto response = sendingEmailActivationLink(userRegistrationRequestDto);

			return response;
		} else {
			UserRegistrationResponseDto response = new UserRegistrationResponseDto();
			response.setStatus("Primary type invalid. This operation is only permitted for lenders and borrowers.");
			return response;
		}
	}

	@Override
	public String generateSalt() {
		String salt = UUID.randomUUID().toString() + UUID.randomUUID().toString().substring(0, 10);
		return salt;

	}

	@Override
	public String generatePassword(String password, String salt) {
		String hashedpassword = signatureService.hashPassword(password, salt);
		return hashedpassword;

	}

}
