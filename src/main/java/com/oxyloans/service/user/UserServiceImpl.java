package com.oxyloans.service.user;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.security.GeneralSecurityException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import com.google.api.services.people.v1.model.PhoneNumber;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets.Details;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.people.v1.PeopleService;
import com.google.api.services.people.v1.PeopleServiceScopes;
import com.google.api.services.people.v1.model.EmailAddress;
import com.google.api.services.people.v1.model.ListConnectionsResponse;
import com.google.api.services.people.v1.model.ListOtherContactsResponse;
import com.google.api.services.people.v1.model.Name;
import com.google.api.services.people.v1.model.Person;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.oxyloans.authenticationexception.InvalidCredentialsException;
import com.oxyloans.autoinvest.entity.LenderAutoInvestConfig;
import com.oxyloans.autoinvest.repo.LenderAutoInvestConfigRepo;
import com.oxyloans.cicreport.CICReportConstants;
import com.oxyloans.coreauthdto.GenericAccessToken.EncodingAlgorithm;
import com.oxyloans.coreauthdto.PasswordGrantAccessToken;
import com.oxyloans.customexceptions.DataFormatException;
import com.oxyloans.customexceptions.EntityAlreadyExistsException;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.enach.scheduler.EnachMinimunWithdrawUsersDto;
import com.oxyloans.enach.scheduler.EnachMiniumWithDrwUserRequest;
import com.oxyloans.enach.scheduler.EnachTypeRequestDto;
import com.oxyloans.engine.template.PdfGeenrationException;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceDetails;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceDetails.CitizenType;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceResponse;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferralBonusUpdated;
import com.oxyloans.entityborrowersdealsdto.DealAndParticipationCount;
import com.oxyloans.entityborrowersdealsdto.LenderDealsStatisticsInformation;
//import com.oxyloans.entity.LenderReferenceDetails.LenderReferralBonus;
import com.oxyloans.entityborrowersdealsdto.LenderInterestRequestDto;
import com.oxyloans.entityborrowersdealsdto.LenderInterestResponseDto;
import com.oxyloans.entity.borrowers.deals.information.LendersPaticipationUpdationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDeals;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDealsRepo;
import com.oxyloans.entity.cms.LenderCmsPayments;
import com.oxyloans.repository.cms.LenderCmsPaymentsRepo;
import com.oxyloans.entity.cms.OxyCmsLoans.FileExecutionStatus;
import com.oxyloans.entity.enach.ApplicationLevelEnachMandate;
import com.oxyloans.repository.enach.ApplicationLevelEnachMandateRepo;
import com.oxyloans.entity.enach.EnachMandate;
import com.oxyloans.entity.enach.EnachMandate.MandateStatus;
import com.oxyloans.entity.enach.EnachMinimumWithdrawUsers;
import com.oxyloans.entity.expertSeekers.AdviseSeekersResponseDto;
import com.oxyloans.entity.expertSeekers.ExpertAdviseSeekers;
import com.oxyloans.repository.expertseekers.ExpertAdviseSeekersRepo;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWallet;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletNativeRepo;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletRepo;
import com.oxyloans.entity.lender.payu.LenderPayuDetailsRepo;
import com.oxyloans.entity.lender.payu.LenderRenewalDetails;
import com.oxyloans.entity.lender.payu.LenderRenewalDetailsRepo;
import com.oxyloans.repository.lendergroup.OxyLendersGroupRepo;
import com.oxyloans.entity.lenders.hold.UserHoldAmountDetail;
import com.oxyloans.repository.lenderhold.UserHoldAmountDetailRepo;
import com.oxyloans.entity.loan.InterestDetails;
import com.oxyloans.entity.loan.LenderRatingToBorrowerApplication;
import com.oxyloans.entity.loan.LoanEmiCard;
import com.oxyloans.entity.loan.LoanOfferdAmount;
import com.oxyloans.entity.loan.LoanOfferdAmount.LoanOfferdStatus;
import com.oxyloans.entity.loan.LoanRequest;
import com.oxyloans.entity.loan.LoanRequest.LoanStatus;
import com.oxyloans.entity.loan.LoanRequest.RepaymentMethod;
import com.oxyloans.entity.loan.OxyLoan;
import com.oxyloans.entity.loan.PaymentUploadHistory;
import com.oxyloans.entity.loan.PaymentUploadHistory.PaymentType;
import com.oxyloans.entity.loan.PenaltyDetails;
import com.oxyloans.entity.socialLogin.SocialLoginDetails;
import com.oxyloans.repository.socialloginrepo.SocialLoginDetailsRepo;
import com.oxyloans.entity.user.AddressDetails;
import com.oxyloans.entity.user.AddressDetails.Type;
import com.oxyloans.entity.user.BankDetails;
import com.oxyloans.entity.user.FinancialDetails;
import com.oxyloans.entity.user.Nominee;
import com.oxyloans.entity.user.PersonalDetails;
import com.oxyloans.entity.user.PersonalDetailsModifiedHistory;
import com.oxyloans.entity.user.ProfessionalDetails;
import com.oxyloans.entity.user.ProfessionalDetails.Employment;
import com.oxyloans.entity.user.ReferenceDetails;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.User.Citizenship;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.entity.user.UserPendingQueries;
import com.oxyloans.entity.user.UserProfileRisk;
import com.oxyloans.entity.user.UserQueryDetails;
import com.oxyloans.entity.user.Document.status.UserDocumentStatus;
import com.oxyloans.entity.user.Document.status.UserDocumentStatus.DocumentType;
import com.oxyloans.entity.user.Document.status.UserDocumentStatus.Status;
import com.oxyloans.repository.userdocumentstatus.UserDocumentStatusRepo;
import com.oxyloans.entity.user.LenderFavouriteUsers.LenderFavouriteUsers;
import com.oxyloans.repository.lenderfavouriteusersrepo.LenderFavouriteUsersRepo;
import com.oxyloans.entity.user.type.OxyUserType;
import com.oxyloans.entity.user.type.OxyUserTypeRepo;
import com.oxyloans.excelservice.ExcelServiceRepo;
import com.oxyloans.file.FileRequest;
import com.oxyloans.file.FileRequest.FileType;
import com.oxyloans.file.FileResponse;
import com.oxyloans.icici.upi.GetepayRequestDto;
import com.oxyloans.lender.wallet.ScrowLenderTransactionResponse;
import com.oxyloans.lender.wallet.ScrowTransactionRequest;
import com.oxyloans.lender.wallet.ScrowWalletResponse;
import com.oxyloans.mobile.MobileUtil;
import com.oxyloans.mobile.twoFactor.MobileOtpVerifyFailedException;
import com.oxyloans.partner.service.PartnerService;
import com.oxyloans.paytm.PaytmVanRequest;
import com.oxyloans.pincodeutil.PinCodeUtil;
import com.oxyloans.pincodeutil.PincodeResultes;
import com.oxyloans.repo.enach.EnachMandateRepo;
import com.oxyloans.repo.enach.EnachMinimumWithdrawUsersRepo;
import com.oxyloans.repo.loan.AddressDetailsRepo;
import com.oxyloans.repo.loan.InterestDetailsRepo;
import com.oxyloans.repo.loan.LenderRatingToBorrowerApplicationRepo;
import com.oxyloans.repo.loan.LoanEmiCardRepo;
import com.oxyloans.repo.loan.LoansByApplicationNativeRepo;
import com.oxyloans.repo.loan.OxyLoanRepo;
import com.oxyloans.repo.loan.OxyLoanRequestRepo;
import com.oxyloans.repo.loan.PaymentUploadHistoryRepo;
import com.oxyloans.repo.loan.PenaltyDetailsRepo;
import com.oxyloans.repo.user.LenderReferenceDetailsRepo;
//import com.oxyloans.repo.user.LenderReferralBonusRepo;
import com.oxyloans.repo.user.LenderReferralBonusUpdatedRepo;
import com.oxyloans.repo.user.PersonalDetailsModifiedHistoryRepo;
import com.oxyloans.repo.user.UserPendingQueriesRepo;
import com.oxyloans.repo.user.UserQueryDetailsRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.repo.user.UtmMasterRepo;
import com.oxyloans.request.PageDto;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.request.user.BulkInviteRequest;
import com.oxyloans.request.user.BulkinviteResponse;
import com.oxyloans.request.user.KycFileRequest;
import com.oxyloans.request.user.KycFileRequest.KycType;
import com.oxyloans.request.user.KycFileResponse;
import com.oxyloans.request.user.LenderReferenceRequestDto;
import com.oxyloans.request.user.NomineeRequestDto;
import com.oxyloans.request.user.PersonalDetailsRequestDto;
import com.oxyloans.request.user.ProfessionalDetailsRequestDto;
import com.oxyloans.request.user.QueriesCountsResponse;
import com.oxyloans.request.user.ReferenceDetailsResponseDto;
import com.oxyloans.request.user.TestUserResponse;
import com.oxyloans.request.user.UserProfileUrlsDto;
import com.oxyloans.request.user.UserQueryDetailsRequestDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.admin.AdminResponse;
import com.oxyloans.response.user.BorrowersLoanInfo;
import com.oxyloans.response.user.BorrowersLoanOwnerInformation;
import com.oxyloans.response.user.BorrowersLoanOwnerNames;
import com.oxyloans.response.user.ContactDetailsForIos;
import com.oxyloans.response.user.EmailAddressResponse;
import com.oxyloans.response.user.GmailSignInResponse;
import com.oxyloans.response.user.LenderMailResponse;
import com.oxyloans.response.user.LenderProfitHistoryResponse;
import com.oxyloans.response.user.LenderProfitResponse;
import com.oxyloans.response.user.LenderRatingRequestDto;
import com.oxyloans.response.user.LenderRatingResponseDto;
import com.oxyloans.response.user.LenderReferenceAmountResponse;
import com.oxyloans.response.user.LenderReferenceAmountResponseDto;
import com.oxyloans.response.user.LenderReferenceResponseDto;
import com.oxyloans.response.user.LenderReferralPaymentStatus;
import com.oxyloans.response.user.NomineeResponseDto;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.response.user.PaymentSearchByStatus;
import com.oxyloans.response.user.PaymentUploadHistoryRequestDto;
import com.oxyloans.response.user.PaymentUploadHistoryResponseDto;
import com.oxyloans.response.user.PersonalDetailsResponseDto;
import com.oxyloans.response.user.ProfessionalDetailsResponseDto;
import com.oxyloans.response.user.RiskProfileDto;
import com.oxyloans.response.user.StatusResponseDto;
import com.oxyloans.response.user.UserPersonalDetailsResponse;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.response.user.UserVanNumberResponseDto;
import com.oxyloans.response.user.ValidityResponse;
import com.oxyloans.security.ThreadLocalSecurityProvider;
import com.oxyloans.security.authorization.IAuthorizationService;
import com.oxyloans.security.signature.ISignatureService;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.service.file.IFileManagementService;
import com.oxyloans.service.loan.ActionNotAllowedException;
import com.oxyloans.service.loan.BorrowerLoanService;
import com.oxyloans.service.loan.LenderWalletHistoryServiceRepo;
import com.oxyloans.service.loan.LoanServiceFactory;
import com.oxyloans.service.loan.NewAdminLoanService;
import com.oxyloans.serviceloan.ApplicationResponseDto;
import com.oxyloans.serviceloan.LenderHistroryResponseDto;
import com.oxyloans.serviceloan.LoanEmiCardPaymentDetailsRequestDto;
import com.oxyloans.serviceloan.LoanEmiCardPaymentDetailsResponseDto;
import com.oxyloans.serviceloan.LoanRequestDto;
import com.oxyloans.serviceloan.LoanResponseDto;
import com.oxyloans.serviceloan.NotificationCountResponseDtoForBorrowerAndLender;
import com.oxyloans.usertypedto.OxyUserTypeResponseDto;
import com.oxyloans.whatapp.service.WhatappService;
import com.oxyloans.whatapp.service.WhatappServiceRepo;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");

	final static String APPLICATION_NAME = "Google People API Java Quickstart";
	final static JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	final static String TOKENS_DIRECTORY_PATH = "tokens";

	static List<String> SCOPES = Arrays.asList(PeopleServiceScopes.CONTACTS_OTHER_READONLY);

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private ExpertAdviseSeekersRepo expertAdviseSeekersRepo;

	@Autowired
	private PenaltyDetailsRepo penaltyDetailsRepo;

	@Value("${gmailPartnerRedirectUri}")
	private String gmailPartnerRedirectUri;

	@Value("${birthdayImage}")
	private String birthdayImage;

	@Autowired
	private InterestDetailsRepo interestDetailsRepo;

	@Autowired
	private BorrowerLoanService borrowerLoanService;

	@Autowired
	private IAuthorizationService authorizationService;

	@Autowired
	private WhatappServiceRepo whatappServiceRepo;

	/*
	 * @Value("${kycFolder}") private String kycFolder;
	 */
	@Value("${appUrl}")
	private String appUrl;

	@Value("${emailVerifyPath}")
	private String emailVerifyPath;

	@Value("${resetPasswordPath}")
	private String resetPasswordPath;

	@Value("${resetPasswordPathReact}")
	private String resetPasswordPathReact;

	@Value("${uploadExceptionMessage}")
	private String uploadExceptionMessage;

	@Value("${mailsubject}")
	private String mailsubject;

	@Value("${gmailClientId}")
	private String clientId;

	@Value("${gmailClientSecret}")
	private String clientSecret;

	@Value("${gmailRedirectUrl}")
	private String redirectUri;

	@Value("${gmailRedirectUri}")
	private String redirectUrl;

	@Value("${spreadSheetRedirectUrl}")
	private String spreadSheetRedirectUrl;

	@Value("${whatsaAppSendApiNewInstance}")
	private String whatsaAppSendApiNewInstance;

	@Value("${referralBonusFaq}")
	private String referralBonusFaq;

	@Value("${redirectionLinkReferrer}")
	private String redirectionLinkReferrer;

	@Autowired
	private ExcelServiceRepo excelServiceRepo;

	@Autowired
	private PersonalDetailsModifiedHistoryRepo personalDetailsModifiedHistoryRepo;

	@Autowired
	private MobileUtil mobileUtil;

	@Autowired
	private IFileManagementService fileManagementService;

	@Autowired
	private IEmailService emailService;

	@Autowired
	private ISignatureService signatureService;

	/*
	 * @Autowired private AddressDetailsRepo addressDetailsRepo;
	 */
	@Autowired
	private UserDocumentStatusRepo userDocumentStatusRepo;
	@Autowired
	private PinCodeUtil pinCodeUtil;

	@Autowired
	private AddressDetailsRepo addressDetailsRepo;

	@Autowired
	private LoanServiceFactory loanServiceFactory;

	@Autowired
	private OxyLoanRequestRepo loanRequestRepo;

	@Autowired
	protected LoanEmiCardRepo loanEmiCardRepo;

	@Autowired
	private LenderOxyWalletRepo lenderOxyWalletRepo;

	@Autowired
	private LenderOxyWalletNativeRepo lenderOxyWalletNativeRepo;

	@Autowired
	private UtmMasterRepo utmMasterRepo;

	@Autowired
	private LoansByApplicationNativeRepo loansByApplicationNativeRepo;

	@Autowired
	private EnachMinimumWithdrawUsersRepo enachMinimumWithdrawUsersRepo;

	@Autowired
	private SocialLoginDetailsRepo socialLoginDetailsRepo;

	@Autowired
	protected OxyLoanRepo oxyLoanRepo;

	@Value("${utm}")
	private String utm;

	private final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Autowired
	private PaymentUploadHistoryRepo paymentUploadHistoryRepo;

	@Autowired
	private LenderRatingToBorrowerApplicationRepo lenderRatingToBorrowerApplicationRepo;

	@Autowired
	private LenderFavouriteUsersRepo lenderFavouriteUsersRepo;

	@PostConstruct
	public void print() {
		System.out.println("service initia......");
	}

	@Autowired
	private LenderReferenceDetailsRepo lenderReferenceDetailsRepo;

	@Value("${referrerLink}")
	private String referrerLink;

	@Value("${redirectionLink}")
	private String redirectionLink;

	/*
	 * @Autowired private LenderReferralBonusRepo lenderReferralBonusRepo;
	 */
	@Autowired
	private OxyLendersGroupRepo oxyLendersGroupRepo;

	@Autowired
	private OxyLendersAcceptedDealsRepo oxyLendersAcceptedDealsRepo;

	@Autowired
	private UserQueryDetailsRepo userQueryDetailsRepo;

	@Value("${wtappApi}")
	private String wtappApi;

	@Autowired
	private WhatappService whatappService;

	@Autowired
	private LendersPaticipationUpdationRepo lendersPaticipationUpdationRepo;

	private Client client = ClientBuilder.newClient().register(MultiPartFeature.class).register(MultiPartWriter.class);

	@Autowired
	private LenderPayuDetailsRepo lenderPayuDetailsRepo;

	@Autowired
	private LenderRenewalDetailsRepo lenderRenewalDetailsRepo;

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Value("${linkForNRI}")
	private String linkForNRI;

	@Autowired
	private OxyUserTypeRepo oxyUserTypeRepo;

	@Value("${redirectionLinkForBorrower}")
	private String redirectionLinkForBorrower;

	@Value("${loanEligibilityForBorrower}")
	private Integer loanEligibilityForBorrower;

	@Value("${partnerLenderRegistrationLink}")
	private String partnerLenderRegistrationLink;

	@Value("${partnerBorrowerRegistrationLink}")
	private String partnerBorrowerRegistrationLink;

	/*
	 * @Value("${linkForNRIForPR}") private String linkForNRIForPR;
	 */

	@Autowired
	private NewUserServiceImpl newUserServiceImpl;

	@Autowired
	private LenderReferralBonusUpdatedRepo lenderReferralBonusUpdatedRepo;

	@Value("${iciciFilePathBeforeApproval}")
	private String iciciFilePathBeforeApproval;

	@Autowired
	private NewAdminLoanService newAdminLoanService;

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private ApplicationLevelEnachMandateRepo applicationLevelEnachMandateRepo;

	@Autowired
	private EnachMandateRepo enachMandateRepo;

	@Autowired
	private UserPendingQueriesRepo userPendingQueriesRepo;

	@Autowired
	private LenderCmsPaymentsRepo lenderCmsPaymentsRepo;

	@Autowired
	private UserHoldAmountDetailRepo userHoldAmountDetailRepo;

	@Autowired
	private LenderWalletHistoryServiceRepo lenderWalletHistoryServiceRepo;

	@Autowired
	private LenderAutoInvestConfigRepo lenderAutoInvestConfigRepo;

	@Transactional
	public UserResponse validateUser(UserRequest userRequest) {
		User user = null;
		UserResponse userResponse = null;
		if (userRequest.getPartnerUtmName() != null && userRequest.getPartnerPassword() != null
				|| (userRequest.getPartnerUtmName() != null
						&& userRequest.getPartnerPassword().equals(User.PrimaryType.SUPERADMIN.toString()))) {
			OxyUserType utm = oxyUserTypeRepo.findingUtm(userRequest.getPartnerUtmName());

			if (utm != null) {
				String hashedpassword = signatureService.hashPassword(userRequest.getPartnerPassword(), utm.getSalt());

				if (userRequest.getPartnerUtmName().equals(utm.getUtmName())) {
					if (utm.getPassword().equals(hashedpassword)
							|| userRequest.getPartnerPassword().equals(User.PrimaryType.SUPERADMIN.toString())) {
						userResponse = new UserResponse();
						userResponse.setId(utm.getId());
						userResponse.setPartnerEmail(utm.getEmail() == null ? "" : utm.getEmail());
						userResponse.setPartnerMobileNumber(utm.getMobileNumber() == null ? "" : utm.getMobileNumber());
						userResponse.setUserType(utm.getUserType().toString());
						userResponse.setPartnerUtmName(utm.getUtmName() == null ? "" : utm.getUtmName());
					} else {
						throw new InvalidCredentialsException("Invalid user name or password",
								ErrorCodes.USER_NOT_FOUND);
					}
				} else {
					throw new InvalidCredentialsException("Invalid user name or password", ErrorCodes.USER_NOT_FOUND);
				}

			} else {
				throw new InvalidCredentialsException("Invalid user name", ErrorCodes.USER_NOT_FOUND);
			}

		} else {
			if (StringUtils.isNotEmpty(userRequest.getEmail()) && userRequest.getPassword() != null
					&& userRequest.getSocialLoginUserId() == null && userRequest.getPrimaryType() == null) {
				user = userRepo.findByEmailIgnoreCase(userRequest.getEmail());
				if (user == null) {
					throw new InvalidCredentialsException("Invalid user name or password", ErrorCodes.USER_NOT_FOUND);
				}
				if (user.getPersonalDetails() != null) {
					OxyLendersAcceptedDeals lenderAccepeted = oxyLendersAcceptedDealsRepo
							.lenderParticipatedInDeals(user.getId());
					if (lenderAccepeted == null) {
						if (user.getPersonalDetails().getDob() == null) {
							throw new ActionNotAllowedException(
									"User Registration step 2 is pending =" + user.getId() + "=" + user.getEmail(),
									ErrorCodes.EMAIL_NOT_VERIFIED);
						}
					}
				}

				String hashedPassword = signatureService.hashPassword(userRequest.getPassword(), user.getSalt());

				if (!hashedPassword.equals(user.getPassword())) {
					throw new InvalidCredentialsException("Invalid user name or password", ErrorCodes.INVALID_PASSWORD);
				}
			}

			if (userRequest.getEmail() != null && userRequest.getSocialLoginUserId() != null
					&& userRequest.getPassword() == null && userRequest.getPrimaryType() == null) {
				user = userRepo.findByEmailIgnoreCase(userRequest.getEmail());
				if (user != null) {
					if (user.getPersonalDetails() != null) {
						OxyLendersAcceptedDeals lenderAccepeted = oxyLendersAcceptedDealsRepo
								.lenderParticipatedInDeals(user.getId());
						if (lenderAccepeted == null) {
							if (user.getPersonalDetails().getDob() == null) {
								throw new ActionNotAllowedException(
										"User Registration step 2 is pending =" + user.getId() + "=" + user.getEmail(),
										ErrorCodes.EMAIL_NOT_VERIFIED);
							}
						}
					}
					SocialLoginDetails details = socialLoginDetailsRepo.findByUserId(user.getId());
					if (details != null) {
						if (userRequest.getUtmSource().equalsIgnoreCase("gmail")) {
							if (!userRequest.getSocialLoginUserId().equals(details.getGoogleUserId())) {
								throw new InvalidCredentialsException("Invalid user name or password",
										ErrorCodes.INVALID_PASSWORD);
							}

						}

						if (userRequest.getUtmSource().equalsIgnoreCase("facebook")) {
							if (!userRequest.getSocialLoginUserId().equals(details.getFacebookUserId())) {
								throw new InvalidCredentialsException("Invalid user name or password",
										ErrorCodes.INVALID_PASSWORD);
							}
						}

					}
				} else {
					throw new InvalidCredentialsException("Invalid user name or password", ErrorCodes.INVALID_PASSWORD);
				}

			}

			if (userRequest.getEmail() != null && userRequest.getSocialLoginUserId() != null
					&& userRequest.getPassword() != null && userRequest.getPrimaryType() == null) {
				user = userRepo.findByEmailIgnoreCase(userRequest.getEmail());
				if (user != null) {
					if (user.getPersonalDetails() != null) {
						OxyLendersAcceptedDeals lenderAccepeted = oxyLendersAcceptedDealsRepo
								.lenderParticipatedInDeals(user.getId());
						if (lenderAccepeted == null) {
							if (user.getPersonalDetails().getDob() == null) {
								throw new ActionNotAllowedException(
										"User Registration step 2 is pending =" + user.getId() + "=" + user.getEmail(),
										ErrorCodes.EMAIL_NOT_VERIFIED);
							}
						}
					}
					String hashedPassword = signatureService.hashPassword(userRequest.getPassword(), user.getSalt());

					if (!hashedPassword.equals(user.getPassword())) {
						throw new InvalidCredentialsException("Invalid user name or password",
								ErrorCodes.INVALID_PASSWORD);
					}

					SocialLoginDetails details = new SocialLoginDetails();
					if (userRequest.getUtmSource().equalsIgnoreCase("gmail")) {
						details.setGoogleUserId(userRequest.getSocialLoginUserId());
					} else if (userRequest.getUtmSource().equalsIgnoreCase("facebook")) {
						details.setFacebookUserId(userRequest.getSocialLoginUserId());
					}
					details.setGmailVerified(true);

					details.setUserId(user.getId());
					socialLoginDetailsRepo.save(details);
				} else {
					throw new InvalidCredentialsException("Invalid user name or password", ErrorCodes.USER_NOT_FOUND);

				}
			}

			if (userRequest.getMobileNumber() != null && userRequest.getPassword() != null
					&& userRequest.getPrimaryType() == null) {
				user = userRepo.findByMobileNumber(userRequest.getMobileNumber());
				if (user == null) {
					throw new InvalidCredentialsException("Invalid user name or password", ErrorCodes.USER_NOT_FOUND);
				}
				if (user != null) {
					if (user.getPersonalDetails() != null) {
						OxyLendersAcceptedDeals lenderAccepeted = oxyLendersAcceptedDealsRepo
								.lenderParticipatedInDeals(user.getId());
						if (lenderAccepeted == null) {
							if (user.getPersonalDetails().getDob() == null) {
								throw new ActionNotAllowedException(
										"User Registration step 2 is pending =" + user.getId() + "=" + user.getEmail(),
										ErrorCodes.EMAIL_NOT_VERIFIED);
							}
						}
					}
				}
				String hashedPassword = signatureService.hashPassword(userRequest.getPassword(), user.getSalt());
				if (!hashedPassword.equals(user.getPassword())) {
					throw new InvalidCredentialsException("Invalid user name or password", ErrorCodes.INVALID_PASSWORD);
				}
			}

			if (userRequest.getMobileNumber() != null && userRequest.getPassword() == null
					&& userRequest.getPrimaryType() == null) {
				user = userRepo.findByMobileNumber(userRequest.getMobileNumber());
				if (user == null) {
					throw new InvalidCredentialsException("Invalid Mobile Number or password",
							ErrorCodes.USER_NOT_FOUND);
				}
				if (user.getPersonalDetails() != null) {
					OxyLendersAcceptedDeals lenderAccepeted = oxyLendersAcceptedDealsRepo
							.lenderParticipatedInDeals(user.getId());
					if (lenderAccepeted == null) {
						if (user.getPersonalDetails().getDob() == null) {
							throw new ActionNotAllowedException(
									"User Registration step 2 is pending =" + user.getId() + "=" + user.getEmail(),
									ErrorCodes.EMAIL_NOT_VERIFIED);
						}
					}
				}
				if (!user.isEmailVerified()) {
					userResponse = new UserResponse();
					userResponse.setId(user.getId());
					userResponse.setStatus(user.getStatus().toString());
					userResponse.setPrimaryType(user.getPrimaryType().name());
					if (user.getPersonalDetails() != null) {
						userResponse.setPersonalDetailsFilled(true);
						userResponse.setFirstName(user.getPersonalDetails().getFirstName());
						userResponse.setLastName(user.getPersonalDetails().getLastName());
					}
					List<UserDocumentStatus> uploadedDocs = userDocumentStatusRepo
							.findByUserIdAndDocumentType(user.getId(), DocumentType.Kyc);
					if (CollectionUtils.isNotEmpty(uploadedDocs)) {
						Set<KycType> kyc = new HashSet<KycType>();
						for (UserDocumentStatus statuses : uploadedDocs) {
							kyc.add(KycType.valueOf(statuses.getDocumentSubType().toUpperCase()));
						}
						userResponse.setKycUploaded(kyc.contains(KycType.AADHAR) && kyc.contains(KycType.PAN)
								&& kyc.contains(KycType.PASSPORT));
					}
					userResponse.setMobileNumber(user.getMobileNumber());
					userResponse.setEmail(user.getEmail());
					userResponse.setEmailVerified(user.isEmailVerified());
					userResponse.setMobileverified(user.isMobileNumberVerified());
					return userResponse;
				}
				if (user.isMobileNumberVerified() || user.isEmailVerified()) {

					boolean verified = mobileUtil.verifyOtp(user.getMobileNumber(), user.getMobileOtpSession(),
							userRequest.getMobileOtpValue());
					if (!verified) {
						throw new MobileOtpVerifyFailedException("Invalid OTP value please check.",
								ErrorCodes.INVALID_OTP);
					}

				}
			}
			if (userRequest.getPrimaryType() == null) {
				if (userRequest.getLongitude() != null && userRequest.getLatitude() != null) {
					user.setLongitude(userRequest.getLongitude());
					user.setLatitude(userRequest.getLatitude());
				} else {
					user.setLongitude(userRequest.getLongitude());
					user.setLatitude(userRequest.getLatitude());
				}
			}
			if (userRequest.getId() != null
					&& userRequest.getPrimaryType().equals(User.PrimaryType.SUPERADMIN.toString())) {
				user = userRepo.findById(userRequest.getId()).get();
			}
			user = userRepo.save(user);
			userResponse = new UserResponse();
			userResponse.setId(user.getId());
			userResponse.setStatus(user.getStatus().toString());
			userResponse.setPrimaryType(user.getPrimaryType().name());
			if (user.getPersonalDetails() != null) {
				userResponse.setPersonalDetailsFilled(true);
				userResponse.setFirstName(user.getPersonalDetails().getFirstName());
				userResponse.setLastName(user.getPersonalDetails().getLastName());
			}
			List<UserDocumentStatus> uploadedDocs = userDocumentStatusRepo.findByUserIdAndDocumentType(user.getId(),
					DocumentType.Kyc);
			if (CollectionUtils.isNotEmpty(uploadedDocs)) {
				Set<KycType> kyc = new HashSet<KycType>();
				for (UserDocumentStatus statuses : uploadedDocs) {
					kyc.add(KycType.valueOf(statuses.getDocumentSubType().toUpperCase()));
				}
				userResponse.setKycUploaded(
						kyc.contains(KycType.AADHAR) && kyc.contains(KycType.PAN) && kyc.contains(KycType.PASSPORT));
			}
			userResponse.setMobileNumber(user.getMobileNumber());
			userResponse.setEmail(user.getEmail());
			userResponse.setEmailVerified(user.isEmailVerified());
			userResponse.setMobileverified(user.isMobileNumberVerified());
			if (user.getUrchinTrackingModule() != null && !user.getUrchinTrackingModule().equalsIgnoreCase("web")) {
				userResponse.setUtmSource(user.getUrchinTrackingModule());
			}

			if (user.getPrimaryType() == PrimaryType.LENDER) {
				ValidityResponse validityResponse = newAdminLoanService.validityCheckForSingleUser(user.getId());

				if (validityResponse != null) {
					if (validityResponse.getUserId() != null) {
						if (validityResponse.getUserId() == user.getId()) {
							userResponse.setValidityCheck(true);
						} else {
							userResponse.setValidityCheck(false);
						}
					}
				}

			}
			if (userRequest.getUuid() != null) {
				newUserServiceImpl.uuidFromMobileDevice(user.getId(), userRequest.getUuid());
			}
		}
		return userResponse;
	}

	@Override
	@Transactional
	public UserResponse verifyEmail(UserRequest userRequest, String emailToken) {
		User user = userRepo.findByEmailToken(emailToken);
		if (user == null) {
			throw new EntityNotFoundException("No email token exists to verify..");
		}

		user.setEmailVerified(true);
		user.setEmailVerifiedOn(new Date());
		user.setPassword(signatureService.hashPassword(userRequest.getPassword(), user.getSalt()));
		if (user.getStatus() == User.Status.REGISTERED && user.getPrimaryType() == PrimaryType.BORROWER) {
			user.setStatus(User.Status.EMAIL_VERIFIED);
		}

		if (user.getStatus() == User.Status.ACTIVE) {
			user.setStatus(User.Status.ACTIVE);

		}

		else {
			user.setStatus(User.Status.REGISTERED);
		}
		user = userRepo.save(user);

		UserResponse response = new UserResponse();
		response.setId(user.getId());
		response.setMobileNumber(user.getMobileNumber());
		response.setEmail(user.getEmail());
		response.setEmailVerified(user.isEmailVerified());

		return response;

	}

	@Override
	public UserResponse uploadKyc(Integer userId, List<KycFileRequest> kycFiles)
			throws FileNotFoundException, IOException {

		User user = userRepo.findById(userId).get();
		List<UserDocumentStatus> userDocumentStatusList = userDocumentStatusRepo.findByUserIdAndDocumentType(userId,
				DocumentType.Kyc);
		if (userDocumentStatusList.isEmpty()) {
			for (KycFileRequest kyc : kycFiles) {
				FileRequest fileRequest = new FileRequest();

				InputStream fileStream = kyc.getFileInputStream();
				if (fileStream == null) {
					fileStream = decodeBase64Stream(kyc.getBase64EncodedStream());
				}
				fileRequest.setInputStream(fileStream);
				fileRequest.setUserId(userId);
				fileRequest.setFileName(kyc.getFileName());
				fileRequest.setFilePrifix(kyc.getKycType().name());
				FileResponse putFile = fileManagementService.putFile(fileRequest);
				UserDocumentStatus userDocumentStatus = new UserDocumentStatus();
				userDocumentStatus.setUserId(user.getId());
				userDocumentStatus.setDocumentType(DocumentType.Kyc);
				userDocumentStatus.setDocumentSubType(kyc.getKycType().toString());
				userDocumentStatus.setFilePath(putFile.getFilePath());
				userDocumentStatus.setFileName(kyc.getFileName());
				userDocumentStatus.setStatus(Status.UPLOADED);
				userDocumentStatusRepo.save(userDocumentStatus);
				user.setKycFilePath(putFile.getFilePath());
				userDocumentStatusList.add(userDocumentStatus);
			}
		} else {
			FileRequest fileRequest = null;
			Map<KycType, UserDocumentStatus> kycDocExistingMap = new HashMap<KycType, UserDocumentStatus>();
			for (UserDocumentStatus userDocumentStatus : userDocumentStatusList) {
				kycDocExistingMap.put(KycType.valueOf(userDocumentStatus.getDocumentSubType()), userDocumentStatus);
			}
			for (KycFileRequest kyc : kycFiles) {
				UserDocumentStatus userDocumentStatus = kycDocExistingMap.get(kyc.getKycType());
				boolean deleted = false;
				if (userDocumentStatus != null && (userDocumentStatus.getStatus().equals(Status.UPLOADED)
						|| userDocumentStatus.getStatus().equals(Status.REJECTED))) {
					fileRequest = new FileRequest();
					fileRequest.setFileName(userDocumentStatus.getFileName());
					fileRequest.setUserId(userDocumentStatus.getUserId());
					fileRequest.setFilePrifix(userDocumentStatus.getDocumentSubType());
					deleted = fileManagementService.deleteFile(fileRequest);

				}
				fileRequest = new FileRequest();
				InputStream fileStream = kyc.getFileInputStream();
				if (fileStream == null) {
					fileStream = decodeBase64Stream(kyc.getBase64EncodedStream());
				}
				fileRequest.setInputStream(fileStream);
				fileRequest.setUserId(userId);
				fileRequest.setFileName(kyc.getFileName());
				fileRequest.setFilePrifix(kyc.getKycType().name());
				FileResponse putFile = fileManagementService.putFile(fileRequest);
				if (userDocumentStatus == null) {
					userDocumentStatus = new UserDocumentStatus();
					userDocumentStatus.setUserId(user.getId());
					userDocumentStatus.setDocumentType(DocumentType.Kyc);
					userDocumentStatus.setDocumentSubType(kyc.getKycType().toString());
					userDocumentStatus.setFilePath(putFile.getFilePath());
					userDocumentStatus.setFileName(putFile.getFileName());
					userDocumentStatus.setStatus(Status.UPLOADED);
					userDocumentStatusList.add(userDocumentStatus);
				} else {
					userDocumentStatus.setFilePath(putFile.getFilePath());
					userDocumentStatus.setFileName(kyc.getFileName());
					userDocumentStatus.setStatus(Status.UPLOADED);
				}
				userDocumentStatus = userDocumentStatusRepo.save(userDocumentStatus);
			}
		}

		UserResponse response = new UserResponse();
		response.setId(user.getId());
		return response;
	}

	public InputStream decodeBase64Stream(String base64EncodedStream) {
		if (base64EncodedStream == null) {
			return null;
		}
		return new ByteArrayInputStream(Base64.getDecoder().decode(base64EncodedStream));
	}

	@Override
	public UserResponse uploadProfilePic(Integer userId, KycFileRequest profilePic)
			throws FileNotFoundException, IOException {
		User user = userRepo.findById(userId).get();
		UserDocumentStatus userDocumentStatus = userDocumentStatusRepo
				.findByUserIdAndDocumentTypeAndDocumentSubType(userId, DocumentType.Kyc, KycType.PROFILEPIC.name());

		FileRequest fileRequest = new FileRequest();
		if (userDocumentStatus != null) {
			fileRequest = new FileRequest();
			fileRequest.setFileName(userDocumentStatus.getFileName());
			fileRequest.setUserId(userDocumentStatus.getUserId());
			fileRequest.setFilePrifix(userDocumentStatus.getDocumentSubType());
			boolean deleted = fileManagementService.deleteFile(fileRequest);

		}
		fileRequest = new FileRequest();
		InputStream fileStream = profilePic.getFileInputStream();
		if (fileStream == null) {
			fileStream = decodeBase64Stream(profilePic.getBase64EncodedStream());
		}
		fileRequest.setInputStream(fileStream);
		fileRequest.setUserId(userId);
		fileRequest.setFileName(profilePic.getFileName());
		fileRequest.setFilePrifix(profilePic.getKycType().name());
		FileResponse putFile = fileManagementService.putFile(fileRequest);
		if (userDocumentStatus == null) {
			userDocumentStatus = new UserDocumentStatus();
			userDocumentStatus.setUserId(user.getId());
			userDocumentStatus.setDocumentType(DocumentType.Kyc);
			userDocumentStatus.setDocumentSubType(profilePic.getKycType().toString());
		}
		userDocumentStatus.setFilePath(putFile.getFilePath());
		userDocumentStatus.setFileName(profilePic.getFileName());
		userDocumentStatus.setStatus(Status.ACCEPTED);
		userDocumentStatus = userDocumentStatusRepo.save(userDocumentStatus);
		UserResponse response = new UserResponse();
		response.setId(user.getId());
		return response;
	}

	@Override
	public UserResponse uploadContacts(Integer userId, KycFileRequest contacts)
			throws FileNotFoundException, IOException {
		User user = userRepo.findById(userId).get();
		UserDocumentStatus userDocumentStatus = userDocumentStatusRepo
				.findByUserIdAndDocumentTypeAndDocumentSubType(userId, DocumentType.Kyc, KycType.CONTACTS.name());

		FileRequest fileRequest = new FileRequest();
		if (userDocumentStatus != null) {
			fileRequest = new FileRequest();
			fileRequest.setFileName(userDocumentStatus.getFileName());
			fileRequest.setUserId(userDocumentStatus.getUserId());
			fileRequest.setFilePrifix(userDocumentStatus.getDocumentSubType());
			boolean deleted = fileManagementService.deleteFile(fileRequest);
			logger.warn("Deleted the Contacts file : " + deleted);
		}
		fileRequest = new FileRequest();
		fileRequest.setInputStream(contacts.getFileInputStream());
		fileRequest.setUserId(userId);
		fileRequest.setFileName(contacts.getFileName());
		fileRequest.setFilePrifix(contacts.getKycType().name());
		FileResponse putFile = fileManagementService.putFile(fileRequest);
		if (userDocumentStatus == null) {
			userDocumentStatus = new UserDocumentStatus();
			userDocumentStatus.setUserId(user.getId());
			userDocumentStatus.setDocumentType(DocumentType.Kyc);
			userDocumentStatus.setDocumentSubType(contacts.getKycType().toString());
		}
		userDocumentStatus.setFilePath(putFile.getFilePath());
		userDocumentStatus.setFileName(contacts.getFileName());
		userDocumentStatus.setStatus(Status.ACCEPTED);
		userDocumentStatus = userDocumentStatusRepo.save(userDocumentStatus);

		// List<UserDocumentStatus> userDocumentStatusList =
		// userDocumentStatusRepo.findByUserIdAndDocumentType(userId,
		// DocumentType.Kyc);
		// boolean setToActive = canSetStatusToActive(user, userDocumentStatusList);
		// if (setToActive) {
		// user.setStatus(User.Status.ACTIVE);
		// user = userRepo.save(user);
		// }

		UserResponse response = new UserResponse();
		response.setId(user.getId());
		return response;
	}

	@Override
	public boolean deleteByMobileNumber(String mobileNumber) {
		User user = userRepo.findByMobileNumber(mobileNumber);
		if (user != null) {
			userRepo.deleteById(user.getId());
		}
		return user != null;
	}

	@Override
	public List<UserResponse> getNewUsersData(String type) {
		List<UserResponse> userResponseList = new ArrayList<UserResponse>();
		List<User> usersList = userRepo.findAll();

		usersList.forEach(user -> {

			if (user.getPrimaryType().toString().equalsIgnoreCase(type)) {
				UserResponse response = new UserResponse();
				response.setId(user.getId());
				response.setName(null);
				response.setEmail(user.getEmail());
				response.setContactNumber(user.getMobileNumber());
				response.setRemarks(null);
				response.setProfileCreatedOn(user.getRegisteredOn());
				response.setLongitude(user.getLongitude());
				response.setLatitude(user.getLatitude());
				userResponseList.add(response);
			}

		});

		return userResponseList;

	}

	@Override
	public AdminResponse totalNumberOfUsers() {
		Long lenderList = userRepo.countByPrimaryType(PrimaryType.LENDER);
		Long borrowerList = userRepo.countByPrimaryType(PrimaryType.BORROWER);
		Long totalUsers = userRepo.count();

		AdminResponse response = new AdminResponse();
		response.setTotalNumberOfLenders(lenderList);
		response.setTotalNumberOfBorrowers(borrowerList);
		response.setTotalNumberOfUsers(totalUsers);

		return response;
	}

	@Override
	@Transactional
	public ProfessionalDetailsResponseDto updateProfessionalDetails(int userId,
			ProfessionalDetailsRequestDto ProfessionalDetailsRequestDto) {
		User user = userRepo.findById(userId).get();
		ProfessionalDetails professionalDetails = user.getProfessionalDetails();
		if (professionalDetails == null) {
			professionalDetails = new ProfessionalDetails();
			professionalDetails.setEmployment(
					Employment.valueOf(ProfessionalDetailsRequestDto.getEmployment().toString().toUpperCase()));
			professionalDetails.setCompanyName(ProfessionalDetailsRequestDto.getCompanyName());
			professionalDetails.setDesignation(ProfessionalDetailsRequestDto.getDesignation());
			professionalDetails.setDescription(ProfessionalDetailsRequestDto.getDescription());
			professionalDetails.setNoOfJobsChanged(ProfessionalDetailsRequestDto.getNoOfJobsChanged());
			professionalDetails.setWorkExperience(ProfessionalDetailsRequestDto.getWorkExperience());
			AddressDetails officeAddress = addressDetailsRepo.findByUserIdAndType(user.getId(), Type.OFFICE);
			if (officeAddress != null) {
				professionalDetails.setOfficeAddressId(officeAddress.getId());
			}
			professionalDetails.setOfficeContactNumber(ProfessionalDetailsRequestDto.getOfficeContactNumber());
			professionalDetails.setHighestQualification(ProfessionalDetailsRequestDto.getHighestQualification());
			professionalDetails.setFieldOfStudy(ProfessionalDetailsRequestDto.getFieldOfStudy());
			professionalDetails.setCollege(ProfessionalDetailsRequestDto.getCollege());
			professionalDetails.setYearOfPassing(ProfessionalDetailsRequestDto.getYearOfPassing());

			user.setProfessionalDetails(professionalDetails);
			professionalDetails.setUser(user);
			user = userRepo.save(user);
		} else {
			if (professionalDetails.getEmployment() != null) {
				professionalDetails.setEmployment(
						Employment.valueOf(ProfessionalDetailsRequestDto.getEmployment().toString().toUpperCase()));
			}
			if (professionalDetails.getCompanyName() != null) {
				professionalDetails.setCompanyName(ProfessionalDetailsRequestDto.getCompanyName());
			}
			if (professionalDetails.getDesignation() != null) {
				professionalDetails.setDesignation(ProfessionalDetailsRequestDto.getDesignation());
			}
			if (professionalDetails.getDescription() != null) {
				professionalDetails.setDescription(ProfessionalDetailsRequestDto.getDescription());
			}
			if (professionalDetails.getNoOfJobsChanged() != null) {
				professionalDetails.setNoOfJobsChanged(ProfessionalDetailsRequestDto.getNoOfJobsChanged());
			}
			if (professionalDetails.getWorkExperience() != null) {
				professionalDetails.setWorkExperience(ProfessionalDetailsRequestDto.getWorkExperience());

			}
			if (professionalDetails.getOfficeContactNumber() != null) {
				professionalDetails.setOfficeContactNumber(ProfessionalDetailsRequestDto.getOfficeContactNumber());
			}
			if (professionalDetails.getHighestQualification() != null) {
				professionalDetails.setHighestQualification(ProfessionalDetailsRequestDto.getHighestQualification());
			}
			if (professionalDetails.getFieldOfStudy() != null) {
				professionalDetails.setFieldOfStudy(ProfessionalDetailsRequestDto.getFieldOfStudy());
			}
			if (professionalDetails.getCollege() != null) {
				professionalDetails.setCollege(ProfessionalDetailsRequestDto.getCollege());
			}

			if (professionalDetails.getYearOfPassing() != null) {
				professionalDetails.setYearOfPassing(ProfessionalDetailsRequestDto.getYearOfPassing());
			}
			user.setProfessionalDetails(professionalDetails);
		}

		// List<UserDocumentStatus> userDocumentStatusList =
		// userDocumentStatusRepo.findByUserIdAndDocumentType(userId,
		// DocumentType.Kyc);
		// boolean setToActive = canSetStatusToActive(user, userDocumentStatusList);
		// if (setToActive) {
		// user.setStatus(User.Status.ACTIVE);
		// user = userRepo.save(user);
		// }

		ProfessionalDetailsResponseDto professionalDetailsResponseDto = new ProfessionalDetailsResponseDto();
		professionalDetailsResponseDto.setId(professionalDetails.getUserId());
		professionalDetailsResponseDto.setUserId(user.getId());
		return professionalDetailsResponseDto;
	}

	@Override
	@Transactional
	public PersonalDetailsResponseDto updatePersonalDetails(int userId,
			PersonalDetailsRequestDto personalDetailsRequestDto) {
		User user = userRepo.findById(userId).get();
		ProfessionalDetails professionalDetails = user.getProfessionalDetails();
		FinancialDetails financialDetails = user.getFinancialDetails();
		PersonalDetails personalDetails = user.getPersonalDetails();
		if (personalDetails == null) {
			personalDetails = new PersonalDetails();
			financialDetails = new FinancialDetails();
			professionalDetails = new ProfessionalDetails();

			personalDetails.setFirstName(personalDetailsRequestDto.getFirstName());
			personalDetails.setLastName(personalDetailsRequestDto.getLastName());
			personalDetails.setFatherName(personalDetailsRequestDto.getFatherName());
			try {
				personalDetails.setDob(expectedDateFormat.parse(personalDetailsRequestDto.getDob()));
			} catch (ParseException e) {
				throw new DataFormatException("Illegal Date format. It should be dd/MM/yyyy",
						ErrorCodes.INVALID_DATE_FORMAT);
			}
			personalDetails.setGender(personalDetailsRequestDto.getGender());
			personalDetails.setMaritalStatus(personalDetailsRequestDto.getMaritalStatus());
			personalDetails.setNationality(personalDetailsRequestDto.getNationality());
			personalDetails.setModifiedAt(new Date());
			personalDetails.setCreatedAt(new Date());
			personalDetails.setAddress(personalDetailsRequestDto.getAddress());
			if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
				professionalDetails.setCompanyName(personalDetailsRequestDto.getCompanyName());
				user.setProfessionalDetails(professionalDetails);
				professionalDetails.setUser(user);
			}
			if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
				financialDetails.setNetMonthlyIncome(personalDetailsRequestDto.getSalary());
				user.setFinancialDetails(financialDetails);
				financialDetails.setUser(user);
			}
			personalDetails.setPanNumber(personalDetailsRequestDto.getPanNumber());
			personalDetails.setMiddleName(personalDetailsRequestDto.getMiddleName());

			user.setPersonalDetails(personalDetails);
			personalDetails.setUser(user);
			user = userRepo.save(user);
		} else {
			if (personalDetailsRequestDto.getFirstName() != null) {
				personalDetails.setFirstName(personalDetailsRequestDto.getFirstName());
			}
			if (personalDetailsRequestDto.getLastName() != null) {
				personalDetails.setLastName(personalDetailsRequestDto.getLastName());
			}
			if (personalDetailsRequestDto.getFatherName() != null) {
				personalDetails.setFatherName(personalDetailsRequestDto.getFatherName());
			}
			if (personalDetailsRequestDto.getDob() != null) {
				try {
					personalDetails.setDob(expectedDateFormat.parse(personalDetailsRequestDto.getDob()));
				} catch (ParseException e) {
					throw new DataFormatException("Illegal Date format. It should be dd/MM/yyyy",
							ErrorCodes.INVALID_DATE_FORMAT);
				}
			}
			if (personalDetailsRequestDto.getGender() != null) {
				personalDetails.setGender(personalDetailsRequestDto.getGender());
			}

			if (personalDetailsRequestDto.getMaritalStatus() != null) {
				personalDetails.setMaritalStatus(personalDetailsRequestDto.getMaritalStatus());
			}
			if (personalDetailsRequestDto.getNationality() != null) {
				personalDetails.setNationality(personalDetailsRequestDto.getNationality());
			}
			if (personalDetailsRequestDto.getModifiedAt() != null) {
				personalDetails.setModifiedAt(new Date());
			}

			if (personalDetailsRequestDto.getCreatedAt() != null) {
				personalDetails.setCreatedAt(new Date());
			}
			if (personalDetailsRequestDto.getAddress() != null) {
				personalDetails.setAddress(personalDetailsRequestDto.getAddress());
			}
			if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
				if (personalDetailsRequestDto.getCompanyName() != null) {
					if (professionalDetails == null) {
						professionalDetails = new ProfessionalDetails();
						professionalDetails.setCompanyName(personalDetailsRequestDto.getCompanyName());

						professionalDetails.setUser(user);
						user.setProfessionalDetails(professionalDetails);
						userRepo.save(user);

					} else {
						professionalDetails.setCompanyName(personalDetailsRequestDto.getCompanyName());
					}

				}
			}
			if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {

				if (personalDetailsRequestDto.getSalary() != null) {
					if (financialDetails == null) {
						financialDetails = new FinancialDetails();
						financialDetails.setNetMonthlyIncome(personalDetailsRequestDto.getSalary());

						financialDetails.setUser(user);
						user.setFinancialDetails(financialDetails);
						userRepo.save(user);
					}

					else {
						financialDetails.setNetMonthlyIncome(personalDetailsRequestDto.getSalary());

					}
				}

			}

			if (personalDetailsRequestDto.getPanNumber() != null) {
				personalDetails.setPanNumber(personalDetailsRequestDto.getPanNumber());
			}
			if (personalDetailsRequestDto.getMiddleName() != null
					|| personalDetailsRequestDto.getMiddleName() == null) {
				personalDetails.setMiddleName(personalDetailsRequestDto.getMiddleName());
			}

			user.setPersonalDetails(personalDetails);

		}

		// List<UserDocumentStatus> userDocumentStatusList =
		// userDocumentStatusRepo.findByUserIdAndDocumentType(userId,
		// DocumentType.Kyc);
		// boolean setToActive = canSetStatusToActive(user, userDocumentStatusList);
		// if (setToActive) {
		// user.setStatus(User.Status.ACTIVE);
		// user = userRepo.save(user);
		// }

		PersonalDetailsResponseDto personalDetailsResponseDto = new PersonalDetailsResponseDto();
		personalDetailsResponseDto.setId(personalDetails.getUserId());
		personalDetailsResponseDto.setUserId(user.getId());
		return personalDetailsResponseDto;

	}

	@Override
	@Transactional
	public UserResponse sendOtp(UserRequest userRequest) throws MessagingException {
		User user = userRepo.findByMobileNumber(userRequest.getMobileNumber());
		if (user == null) {
			throw new EntityNotFoundException("No User exists with mobile number " + userRequest.getMobileNumber());
		}
		String otpSessionId = mobileUtil.sendOtp(user.getMobileNumber());
		user.setMobileOtpSession(otpSessionId);
		user.setMobileOtpSent(true);
		user = userRepo.save(user);
		UserResponse response = new UserResponse();
		response.setId(user.getId());
		return response;
	}

	public UserResponse resetPasswordSendEmail(UserRequest userRequest) throws MessagingException {
		User user = userRepo.findByEmailIgnoreCase(userRequest.getEmail());
		if (user == null) {
			throw new EntityNotFoundException("No User exists with email " + userRequest.getEmail());
		}
		user.setEmail(userRequest.getEmail());
		String uuid = UUID.randomUUID().toString().replace("-", "");

		String verifyLink = null;

		if (userRequest.getProjectType().equalsIgnoreCase("OXYLOANS")) {
			verifyLink = new StringBuilder()
					.append(this.resetPasswordPath.replace("{email_token}", uuid).replace("{email}", user.getEmail()))
					.toString();
			logger.info("oxyloans project");

		} else if (userRequest.getProjectType().equalsIgnoreCase("REACT")) {
			verifyLink = new StringBuilder().append(
					this.resetPasswordPathReact.replace("{email_token}", uuid).replace("{email}", user.getEmail()))
					.toString();

			logger.info("react project");
		}

		TemplateContext templateContext = new TemplateContext();
		templateContext.put("verifyLink", verifyLink);
		EmailRequest emailRequest = new EmailRequest(new String[] { user.getEmail() }, mailsubject,
				"email-setpassword.template", templateContext);
		EmailResponse emailResponse = emailService.sendEmail(emailRequest);
		if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
			throw new MessagingException(emailResponse.getErrorMessage());
		}
		user.setEmailSentOn(new Date());
		user.setEmailSent(true);
		user.setEmailToken(uuid);
		user = userRepo.save(user);
		UserResponse userResponse = new UserResponse();
		userResponse.setId(user.getId());
		userResponse.setEmail(user.getEmail());
		return userResponse;

	}

	@Override
	@Transactional
	public PersonalDetailsResponseDto getPersonalDetails(Integer id) {

		User user = userRepo.findById(id).get();
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(id);
		Double lenderWalletCreditAmount = lenderOxyWalletNativeRepo.lenderWalletCreditAmount(id);
		Double lenderWalletdebitAmount = lenderOxyWalletNativeRepo.lenderWalletDebitAmount(id);
		Double lenderWalletInprocessAmount = lenderOxyWalletNativeRepo.lenderWalletInprocessAmount(id);
		PersonalDetailsResponseDto personalDetailsResponseDto = new PersonalDetailsResponseDto();
		UserProfileUrlsDto urlsDto = getUserUrlprofile(user.getId());
		personalDetailsResponseDto.setUrlsDto(urlsDto);

		String userInfo = userRepo.findingWhatsAppNumberForTheUser(user.getId());
		personalDetailsResponseDto.setWhatsAppNumber(userInfo == null ? null : userInfo);

		if (user.getPrimaryType().equals(User.PrimaryType.BORROWER)) {

			Integer details = lenderReferenceDetailsRepo.findingReferrerInformation(user.getId());

			if (user.getUrchinTrackingModule() != null) {

				if (user.getUrchinTrackingModule().equalsIgnoreCase("WEB")) {
					if (user.getFinancialDetails() != null) {
						Integer salary = user.getFinancialDetails().getNetMonthlyIncome();

						if (details != null) {
							Double loanEligibilityBasedOnSalary = (double) (salary / loanEligibilityForBorrower);

							Double loanEligibility = (details * loanEligibilityBasedOnSalary);

							if (salary > loanEligibility) {
								personalDetailsResponseDto.setLoanEligibility(loanEligibility);
							} else {
								personalDetailsResponseDto.setLoanEligibility((double) salary);
							}

							personalDetailsResponseDto.setSalaryNew((double) salary);
							personalDetailsResponseDto.setCountOfReferees(details);
							personalDetailsResponseDto.setLoanEligibilityBasedOnReferring(loanEligibility);

						} else {
							personalDetailsResponseDto.setLoanEligibility(0.0);
						}
					}

				} else {
					OxyUserType oxyUserType = oxyUserTypeRepo.findingUserUtm(user.getUrchinTrackingModule());

					if (oxyUserType != null) {
						if (user.getFinancialDetails() != null) {
							Integer salary = user.getFinancialDetails().getNetMonthlyIncome();

							Double totalSalary = oxyUserType.getLoanAmountCalculation() * salary;

							if (details != null) {
								Double loanEligibilityBasedOnSalary = (totalSalary / loanEligibilityForBorrower);

								Double loanEligibility = details * loanEligibilityBasedOnSalary;

								if (totalSalary > loanEligibility) {
									personalDetailsResponseDto.setLoanEligibility(loanEligibility);
								} else {
									personalDetailsResponseDto.setLoanEligibility(totalSalary);
								}

								personalDetailsResponseDto.setSalaryNew(totalSalary);
								personalDetailsResponseDto.setCountOfReferees(details);
								personalDetailsResponseDto.setLoanEligibilityBasedOnReferring(loanEligibility);

							} else {
								personalDetailsResponseDto.setLoanEligibility(0.0);
							}

						}
					}
				}
			}
			/*
			 * ExperianSummary experianSummary = user.getExperianSummary();
			 * 
			 * if (experianSummary != null && experianSummary.getScore() != null) {
			 * 
			 * personalDetailsResponseDto.setCreditReportAcquired(true);
			 * 
			 * personalDetailsResponseDto.setCibilScore(experianSummary.getScore());
			 * 
			 * CreditScoreCalculationDto calculationDto = newAdminLoanService
			 * .getCreditScoreValueAndReferenceCount(experianSummary.getScore());
			 * 
			 * if (calculationDto != null) {
			 * 
			 * personalDetailsResponseDto.setReferencesCount(calculationDto.
			 * getReferencesCount()); }
			 * 
			 * } else { personalDetailsResponseDto.setCreditReportAcquired(false); }
			 * 
			 * String newReferenceCodeChangesDateString = "2022-09-16 00:00:00.000";
			 * 
			 * Date newReferenceCodeChangesDate = new Date(); try {
			 * newReferenceCodeChangesDate =
			 * dateFormat.parse(newReferenceCodeChangesDateString);
			 * 
			 * } catch (ParseException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); }
			 * 
			 * if (user.getRegisteredOn().before(newReferenceCodeChangesDate)) {
			 * 
			 * personalDetailsResponseDto.setReferencesCount(8);
			 * 
			 * }
			 */
		}
		personalDetailsResponseDto.setSiteTool(user.getPersonalDetails().getSiteTool());
		personalDetailsResponseDto.setStudentOrNot(user.getPersonalDetails().getIsStudent());
		if (user != null) {
			if (user.getPrimaryType().equals(User.PrimaryType.BORROWER)) {
				personalDetailsResponseDto.setUniversityName(user.getPersonalDetails().getEducation() == null ? ""
						: user.getPersonalDetails().getEducation());
				personalDetailsResponseDto.setCountry(
						user.getPersonalDetails().getPassion() == null ? "" : user.getPersonalDetails().getPassion());
				personalDetailsResponseDto.setLocation(
						user.getPersonalDetails().getUserType() == null ? "" : user.getPersonalDetails().getUserType());
			}
		}

		if (user != null) {
			if (user.getPrimaryType().equals(User.PrimaryType.BORROWER)) {
				if (user.getProfessionalDetails() != null
						&& user.getProfessionalDetails().getDocumentDriveLink() != null) {
					personalDetailsResponseDto
							.setDocumentDriveLink(user.getProfessionalDetails().getDocumentDriveLink());
				}
			}
		}

		if (user != null) {

			LenderRenewalDetails renewalDetails = lenderRenewalDetailsRepo
					.findingRenewalDetailsBasedOnUserId(user.getId());
			if (renewalDetails != null) {
				personalDetailsResponseDto.setIsNotificationSendAboutValidity(renewalDetails.isMessageSent());
			}

		}

		if (userInfo != null) {
			personalDetailsResponseDto.setWhatsAppNumberUpdated(true);
		} else {
			personalDetailsResponseDto.setWhatsAppNumberUpdated(false);
		}

		if (!(lenderWalletInprocessAmount != null && lenderWalletInprocessAmount > 0)) {
			lenderWalletInprocessAmount = 0d;
		}
		if (!(lenderWalletCreditAmount != null && lenderWalletCreditAmount > 0)) {
			lenderWalletCreditAmount = 0d;
		}
		if (!(lenderWalletdebitAmount != null && lenderWalletdebitAmount > 0)) {
			lenderWalletdebitAmount = 0d;
		}
		Double lenderWalletAmount = lenderWalletCreditAmount - lenderWalletdebitAmount;
		if (lenderWalletAmount != null && lenderWalletAmount > 0) {
			personalDetailsResponseDto.setLenderWalletAmount(lenderWalletAmount - lenderWalletInprocessAmount);
		} else {
			personalDetailsResponseDto.setLenderWalletAmount(0.0);
		}
		if (user != null) {

			if (user.getPersonalDetails().getAadharNumber() != null) {
				personalDetailsResponseDto.setAadharNumber(user.getPersonalDetails().getAadharNumber());
			}
		}

		Double holdAmount = 0.0;
		Double dealValue = 0.0;
		Double dealValueWithAgreements = 0.0;
		Double equityAmount = 0.0;
		if (user.getPrimaryType() == PrimaryType.LENDER) {

			Double dealAmount = oxyLendersAcceptedDealsRepo
					.getDealParticipationAmountIncludingClosedDeals(user.getId());
			Double dealUpdatedAmount = lendersPaticipationUpdationRepo
					.getSumOfLenderUpdatedAmountButNotInEquityIncludingClosedDeals(user.getId());

			if (dealAmount != null) {
				if (dealUpdatedAmount != null) {
					dealValue = dealAmount + dealUpdatedAmount;
				} else {
					dealValue = dealAmount;
				}
			}

			Double dealAmountWithAgreements = oxyLendersAcceptedDealsRepo
					.getDealParticipationAmountAfterAgreements(user.getId());
			if (dealAmountWithAgreements != null) {
				dealValueWithAgreements = dealAmountWithAgreements;
			}
			holdAmount = dealValue - dealValueWithAgreements;

			Double equityValue = oxyLendersAcceptedDealsRepo.getDealParticipationAmountForEquity(user.getId());
			Double dealUpdatedAmountInEquity = lendersPaticipationUpdationRepo
					.getSumOfLenderUpdatedAmountInEquity(user.getId());
			if (equityValue != null) {
				if (dealUpdatedAmountInEquity != null) {
					equityAmount = equityValue + dealUpdatedAmountInEquity;
				} else {
					equityAmount = equityValue;
				}
			}

			personalDetailsResponseDto.setLenderValidityStatus(lenderValidityStatus(user.getId()));
			OxyUserType oxyUserType = oxyUserTypeRepo.getDetailsByUtmNameAndType(user.getUrchinTrackingModule());
			if (oxyUserType != null) {
				personalDetailsResponseDto.setWalletCalculationToLender(false);

				personalDetailsResponseDto.setWalletAmountToLender(
						BigDecimal.valueOf(partnerService.lenderFromPartnerWalletInfo(user.getId())).toBigInteger());

			} else {
				personalDetailsResponseDto.setWalletCalculationToLender(true);
				personalDetailsResponseDto.setWalletAmountToLender(null);
			}

			List<UserHoldAmountDetail> holdInfo = userHoldAmountDetailRepo.findByUserId(user.getId());
			if (holdInfo != null && !holdInfo.isEmpty()) {
				personalDetailsResponseDto.setHoldStatus(true);
			}
			personalDetailsResponseDto.setFeeStatus(user.isFeeStatus());

			LenderAutoInvestConfig lenderAutoInvest = lenderAutoInvestConfigRepo.autoLenderAlreadyEnabled(user.getId());
			if (lenderAutoInvest != null) {
				personalDetailsResponseDto.setAutoLendingStatus(true);
			}
		}
		personalDetailsResponseDto.setHoldAmountInDealParticipation(holdAmount);
		personalDetailsResponseDto.setEquityAmount(equityAmount);
		PersonalDetails personalDetails = user.getPersonalDetails();

		ProfessionalDetails professionalDetails = user.getProfessionalDetails();
		FinancialDetails financialDetails = null;

		if (user.getFinancialDetails() != null) {
			financialDetails = user.getFinancialDetails();
			logger.info("user id" + user.getFinancialDetails().getUserId());
		}
		BankDetails bankDetails = user.getBankDetails();
		if (bankDetails != null) {
			if (user.getPrimaryType().toString().equals(PrimaryType.LENDER.toString())) {
				personalDetailsResponseDto.setBankDetailsVerifiedStatus(bankDetails.getBankDetailsVerifiedstatus());
			} else {
				personalDetailsResponseDto.setBankDetailsVerifiedStatus(bankDetails.getBankDetailsVerifiedstatus());
			}
		}
		personalDetailsResponseDto.setEmail(user.getEmail());
		personalDetailsResponseDto.setUserStatus(user.getStatus().toString());
		personalDetailsResponseDto.setCity(user.getCity());
		if (user.getPinCode() != null) {
			personalDetailsResponseDto.setPinCode(user.getPinCode());
		}
		if (user.getState() != null) {
			personalDetailsResponseDto.setState(user.getState());
		}
		if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
			if (loanRequest != null) {
				personalDetailsResponseDto.setLoanRequestAmount(loanRequest.getLoanRequestAmount());
				LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();
				if (ObjectUtils.allNotNull(loanOfferdAmount)) {
					personalDetailsResponseDto.setLoanOfferedStatus(
							loanOfferdAmount.getLoanOfferdStatus() == LoanOfferdStatus.INITIATED ? true : false);
				}
				OxyUserType oxyUserType = oxyUserTypeRepo.getDetailsByUtmNameAndType(user.getUrchinTrackingModule());
				if (oxyUserType != null) {
					if (loanRequest.getLoanOfferedAmount() != null) {
						Double enachPendingAmount = oxyLoanRepo.getTotalActiveAmountByParentId(loanRequest.getId());
						if (enachPendingAmount != null) {
							if (loanRequest.getLoanOfferedAmount().getLoanOfferedAmount() >= 50000) {

								personalDetailsResponseDto.setLoanStatus("Applicationlevel");
							} else {
								personalDetailsResponseDto.setLoanStatus("Loanlevel");
							}

						} else {
							personalDetailsResponseDto.setLoanStatus("NA");
						}
					} else {
						personalDetailsResponseDto.setLoanStatus("NA");
					}

					Double holdAmountToBorrower = oxyLoanRepo.sumOfHoldAmountToBorrower(loanRequest.getId());
					if (holdAmountToBorrower != null) {
						if (loanRequest.getLoanOfferedAmount() != null) {

							personalDetailsResponseDto.setEsignStatus("ApplicationlevelEsign");

						}

					} else {
						personalDetailsResponseDto.setEsignStatus("NA");
					}
				} else {
					personalDetailsResponseDto.setLoanStatus("NA");
					personalDetailsResponseDto.setEsignStatus("NA");
				}
			}
		}
		if (user.getExperianSummary() != null) {
			personalDetailsResponseDto.setExperianStatus(true);

		}
		personalDetailsResponseDto.setMobileNumber(user.getMobileNumber());
		personalDetailsResponseDto.setEmailVerified(user.isEmailVerified());
		personalDetailsResponseDto.setWhatsappVerified(user.isWhatsappVerified());

		if (user.getCity().isEmpty()) {
			personalDetailsResponseDto.setCity(null);
		}

		if (personalDetails == null) {
			return personalDetailsResponseDto;
		}

		if (bankDetails == null) {
			personalDetailsResponseDto.setUserName(null);
			personalDetailsResponseDto.setAccountNumber(null);
			personalDetailsResponseDto.setIfscCode(null);
			personalDetailsResponseDto.setBankName(null);
			personalDetailsResponseDto.setBranchName(null);
			personalDetailsResponseDto.setBankAddress(null);
			personalDetailsResponseDto.setModeOfTransactions(null);
		} else {
			if (bankDetails.getUserName() != null && bankDetails.getUserName().length() == 0) {
				personalDetailsResponseDto.setUserName(null);
			} else {
				personalDetailsResponseDto.setUserName(bankDetails.getUserName());
			}
			if (bankDetails.getAccountNumber() != null && bankDetails.getAccountNumber().length() == 0) {
				personalDetailsResponseDto.setAccountNumber(null);
			} else {
				personalDetailsResponseDto.setAccountNumber(bankDetails.getAccountNumber());
			}
			if (bankDetails.getIfscCode() != null && bankDetails.getIfscCode().length() == 0) {
				personalDetailsResponseDto.setIfscCode(null);
			} else {
				personalDetailsResponseDto.setIfscCode(bankDetails.getIfscCode());
			}
			if (bankDetails.getBankName() != null && bankDetails.getBankName().length() == 0) {
				personalDetailsResponseDto.setBankName(null);
			} else {
				personalDetailsResponseDto.setBankName(bankDetails.getBankName());
			}
			if (bankDetails.getBranchName() != null && bankDetails.getBranchName().length() == 0) {
				personalDetailsResponseDto.setBranchName(null);
			} else {
				personalDetailsResponseDto.setBranchName(bankDetails.getBranchName());
			}
			if (bankDetails.getAddress() != null && bankDetails.getAddress().length() == 0) {
				personalDetailsResponseDto.setBankAddress(null);
			} else {
				personalDetailsResponseDto.setBankAddress(bankDetails.getAddress());
			}
			if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
				if (bankDetails.getModeOfTransactions() != null && bankDetails.getModeOfTransactions().length() == 0) {
					personalDetailsResponseDto.setModeOfTransactions(null);
				} else {
					personalDetailsResponseDto.setModeOfTransactions(bankDetails.getModeOfTransactions());
				}
			}
		}
		personalDetailsResponseDto.setUserId(personalDetails.getUserId());
		personalDetailsResponseDto.setFirstName(personalDetails.getFirstName());

		personalDetailsResponseDto.setLastName(personalDetails.getLastName());
		personalDetailsResponseDto.setFatherName(personalDetails.getFatherName());
		if (personalDetails.getDob() != null) {
			personalDetailsResponseDto.setDob(expectedDateFormat.format(personalDetails.getDob()));
		}
		if (user.isTestUser()) {
			personalDetailsResponseDto.setTestUser(true);
		} else {
			personalDetailsResponseDto.setTestUser(false);
		}
		personalDetailsResponseDto.setGender(personalDetails.getGender());
		personalDetailsResponseDto.setMaritalStatus(personalDetails.getMaritalStatus());
		personalDetailsResponseDto.setNationality(personalDetails.getNationality());
		personalDetailsResponseDto.setCreatedAt(personalDetails.getCreatedAt());
		personalDetailsResponseDto.setModifiedAt(personalDetails.getModifiedAt());
		personalDetailsResponseDto.setAddress(personalDetails.getAddress());
		personalDetailsResponseDto.setCitizenship(user.getCitizenship().toString());
		personalDetailsResponseDto.setUtm(user.getUrchinTrackingModule());

		if (user.getUrchinTrackingModule() != null) {

			OxyUserTypeResponseDto oxyUserTypeResponseDto = getSingleUserTypeInformation(
					user.getUrchinTrackingModule());
			if (oxyUserTypeResponseDto != null) {
				personalDetailsResponseDto.setOxyUserTypeResponseDto(oxyUserTypeResponseDto);
			}
		} else {
			OxyUserTypeResponseDto oxyUserTypeResponse = new OxyUserTypeResponseDto();
			oxyUserTypeResponse.setUserType("WEB");
			oxyUserTypeResponse.setType("WEB");
			personalDetailsResponseDto.setOxyUserTypeResponseDto(oxyUserTypeResponse);
		}
		String groupName = null;
		if (user.getLenderGroupId() == 0) {
			personalDetailsResponseDto.setGroupId(0);
			personalDetailsResponseDto.setGroupName("NewLender");
		} else {
			groupName = oxyLendersGroupRepo.getLenderGroupNameByid(user.getLenderGroupId());
			if (groupName != null) {
				if (groupName.equalsIgnoreCase("OXYMARCH09")) {
					personalDetailsResponseDto.setGroupId(user.getLenderGroupId());
					personalDetailsResponseDto.setGroupName(groupName);
				} else {
					personalDetailsResponseDto.setGroupId(3);
					personalDetailsResponseDto.setGroupName("OxyPremiuimLenders");
				}
			}

		}
		if (personalDetails.getPermanentAddress() != null) {
			personalDetailsResponseDto.setPermanentAddress(personalDetails.getPermanentAddress());
		}
		if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
			if (user.getReferenceDetails() != null && user.getReferenceDetails().getReference1() != null) {
				personalDetailsResponseDto.setReferenceStatus(true);

			} else {
				personalDetailsResponseDto.setReferenceStatus(false);
			}
			ReferenceDetailsResponseDto referenceDetailsResponseDto = getReferenceDetails(user.getId());
			if (referenceDetailsResponseDto != null) {
				personalDetailsResponseDto.setReferenceDetailsResponseDto(referenceDetailsResponseDto);
			}
		}
		if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
			if (professionalDetails == null) {
				personalDetailsResponseDto.setCompanyName(null);
				personalDetailsResponseDto.setWorkExperience(null);
				personalDetailsResponseDto.setEmployment(null);

			} else {
				if (professionalDetails.getEmployment() != null
						&& professionalDetails.getEmployment().toString().equals(Employment.SELFEMPLOYED.toString())) {
					personalDetailsResponseDto.setEmployment(professionalDetails.getEmployment().toString());
					if (professionalDetails.getCompanyName() == null) {
						personalDetailsResponseDto.setCompanyName(null);
					} else {
						if (professionalDetails.getCompanyName() != null
								&& professionalDetails.getCompanyName().toString().length() > 0) {
							personalDetailsResponseDto.setCompanyName(professionalDetails.getCompanyName());
						}
					}
					if (professionalDetails.getWorkExperience() == null) {
						personalDetailsResponseDto.setWorkExperience(null);
					} else {
						if (professionalDetails.getWorkExperience() != null
								&& professionalDetails.getWorkExperience().toString().length() > 0) {
							personalDetailsResponseDto.setWorkExperience(professionalDetails.getWorkExperience());
						}
					}

				}

				if (professionalDetails.getEmployment() != null
						&& professionalDetails.getEmployment().toString().equals(Employment.SALARIED.toString())) {
					personalDetailsResponseDto.setCompanyName(professionalDetails.getCompanyName());
					personalDetailsResponseDto.setWorkExperience(professionalDetails.getWorkExperience());
					personalDetailsResponseDto.setEmployment(professionalDetails.getEmployment().toString());
				}

				if (professionalDetails.getEmployment() != null && (professionalDetails.getEmployment().toString()
						.equals(Employment.PRIVATE.toString())
						|| professionalDetails.getEmployment().toString().equals(Employment.PUBLIC.toString())
						|| professionalDetails.getEmployment().toString().equals(Employment.GOVERMENT.toString()))) {
					personalDetailsResponseDto.setCompanyName(professionalDetails.getCompanyName());
					personalDetailsResponseDto.setWorkExperience(professionalDetails.getWorkExperience());
					personalDetailsResponseDto.setEmployment(professionalDetails.getEmployment().toString());
				}
			}
		}

		if (user.getPrimaryType().equals(PrimaryType.BORROWER)) {
			if (loanRequest != null) {

				List<OxyLoan> oxyLoanList = oxyLoanRepo.findByBorrowerParentRequestId(loanRequest.getId());
				int esignCount = 0;
				if (oxyLoanList != null && !oxyLoanList.isEmpty()) {
					personalDetailsResponseDto.setLoansExists(true);

					for (OxyLoan oxyLoan : oxyLoanList) {

						if (!oxyLoan.isBorrowerEsigned()) {
							esignCount++;
						}

						int enachMandateCount = 0;

						LoanRequest loanOffered = loanRequestRepo.findingLoanOfferedAcceptedDate(loanRequest.getId());

						if (loanRequest.getLoanOfferedAmount().getLoanOfferedAmount() >= 50000 && loanOffered != null) {

							List<ApplicationLevelEnachMandate> listOfEnachStatus = applicationLevelEnachMandateRepo
									.findingListByApplicationId(loanRequest.getId());
							if (listOfEnachStatus != null && !listOfEnachStatus.isEmpty()) {
								for (ApplicationLevelEnachMandate enach : listOfEnachStatus) {
									if (!enach.getMandateStatus()
											.equals(ApplicationLevelEnachMandate.MandateStatus.SUCCESS)) {
										enachMandateCount++;
									}
								}

								if (enachMandateCount > 0) {
									personalDetailsResponseDto.setEnachStatus(false);
								} else {
									personalDetailsResponseDto.setEnachStatus(true);
								}
							} else {
								personalDetailsResponseDto.setEnachStatus(false);
							}

						} else {
							List<EnachMandate> enachMandateList = enachMandateRepo.findByOxyLoanId(oxyLoan.getId());
							// int enachMandateCount = 0;
							if (enachMandateList != null && !enachMandateList.isEmpty()) {
								for (EnachMandate enachMandate : enachMandateList) {
									if (!enachMandate.getMandateStatus().equals(MandateStatus.SUCCESS)) {
										enachMandateCount++;
									}

								}

								if (enachMandateCount > 0) {
									personalDetailsResponseDto.setEnachStatus(false);
								} else {
									personalDetailsResponseDto.setEnachStatus(true);
								}

							} else {
								personalDetailsResponseDto.setEnachStatus(false);
							}
						}

					}
					if (esignCount > 0) {
						personalDetailsResponseDto.setEsignedStatus(false);
					} else {
						personalDetailsResponseDto.setEsignedStatus(true);
					}

				} else {
					personalDetailsResponseDto.setLoansExists(false);

				}

			}
		}

		if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
			if (financialDetails == null) {
				personalDetailsResponseDto.setSalary(null);

			} else {
				if (professionalDetails.getEmployment() != null
						&& professionalDetails.getEmployment().toString().equals(Employment.SELFEMPLOYED.toString())) {
					if (financialDetails.getNetMonthlyIncome() == null) {
						personalDetailsResponseDto.setSalary(null);
					} else {
						if (financialDetails.getNetMonthlyIncome() != null) {
							personalDetailsResponseDto.setSalary(financialDetails.getNetMonthlyIncome());
						}
					}
				}
				if (professionalDetails.getEmployment() != null
						&& professionalDetails.getEmployment().toString().equals(Employment.SALARIED.toString())) {
					personalDetailsResponseDto.setSalary(financialDetails.getNetMonthlyIncome());
				}
				if (professionalDetails.getEmployment() != null && (professionalDetails.getEmployment().toString()
						.equals(Employment.PRIVATE.toString())
						|| professionalDetails.getEmployment().toString().equals(Employment.PUBLIC.toString())
						|| professionalDetails.getEmployment().toString().equals(Employment.GOVERMENT.toString()))) {
					personalDetailsResponseDto.setSalary(financialDetails.getNetMonthlyIncome());
				}
			}
		}

		personalDetailsResponseDto.setPanNumber(personalDetails.getPanNumber());

		personalDetailsResponseDto.setMiddleName(personalDetails.getMiddleName());
		if (user.getPrimaryType().toString().equals(PrimaryType.LENDER.toString())) {
			if (user.getPersonalDetails().getPermanentAddress() == null) {
				personalDetailsResponseDto.setPersonalDetailsInfo(false);
			} else {
				if (user.getPersonalDetails().getPermanentAddress() != null
						&& user.getPersonalDetails().getPanNumber() != null) {
					personalDetailsResponseDto.setPersonalDetailsInfo(true);
				}
			}
		} else {
			if (professionalDetails == null) {
				personalDetailsResponseDto.setPersonalDetailsInfo(false);
			} else {
				if (user.getProfessionalDetails().getCompanyName() != null
						&& user.getProfessionalDetails().getCompanyName().toString().length() > 0) {
					personalDetailsResponseDto.setPersonalDetailsInfo(true);
				} else {
					personalDetailsResponseDto.setPersonalDetailsInfo(false);
				}
			}
		}

		if (bankDetails == null) {
			personalDetailsResponseDto.setBankDetailsInfo(false);
		} else {
			if (user.getBankDetails().getUserName() != null && user.getBankDetails().getUserName().length() > 0
					&& user.getBankDetails().getAccountNumber() != null
					&& user.getBankDetails().getAccountNumber().length() > 0) {
				personalDetailsResponseDto.setBankDetailsInfo(true);
			} else {
				personalDetailsResponseDto.setBankDetailsInfo(false);
			}
		}
		personalDetailsResponseDto.setNotificationDetails(notificationDetails(id));

		LoanResponseDto defaultLoanRequest = loanServiceFactory.getService(user.getPrimaryType().name().toUpperCase())
				.getDefaultLoanRequest(id);
		// LoanRequest defaultLoanRequest=loanRequestRepo.findByUserId(id)
		if (defaultLoanRequest != null) {

			personalDetailsResponseDto.setLoanRequestId(defaultLoanRequest.getLoanRequestId());
			personalDetailsResponseDto.setUserDisplayId(user.getUniqueNumber());
			personalDetailsResponseDto.setPrimaryType(user.getPrimaryType().name());
			personalDetailsResponseDto
					.setRateOfInterestUpdated(defaultLoanRequest.getRateOfInterest().doubleValue() > 0d);

		}
		if (user != null) {
			if (user.getLocality() != null) {
				personalDetailsResponseDto.setLocality(user.getLocality());
			}
			if (user.getVanNumber() != null) {
				personalDetailsResponseDto.setVanNumber(user.getVanNumber());
			}
		}
		Integer lenderMappingToBorrowers = oxyLoanRepo.getLoanIdsMappedToLenderIdCount(user.getId());
		if (lenderMappingToBorrowers > 0) {
			personalDetailsResponseDto.setDurationIncrement(true);
		}
		if (user != null) {
			if (user.getStatus().equals(com.oxyloans.entity.user.User.Status.REGISTERED)) {

				UserDocumentStatus userDocumentStatusPan = userDocumentStatusRepo
						.findByUserIdAndDocumentTypeAndDocumentSubType(user.getId(), DocumentType.Kyc,
								KycType.PAN.toString());
				UserDocumentStatus userDocumentStatusBankStatement = userDocumentStatusRepo
						.findByUserIdAndDocumentTypeAndDocumentSubType(user.getId(), DocumentType.Kyc,
								KycType.BANKSTATEMENT.toString());
				UserDocumentStatus userDocumentStatusPayslip = userDocumentStatusRepo
						.findByUserIdAndDocumentTypeAndDocumentSubType(user.getId(), DocumentType.Kyc,
								KycType.PAYSLIPS.toString());

				if (user.getPrimaryType().equals(PrimaryType.LENDER)) {
					if (userDocumentStatusPan != null) {
						List<UserDocumentStatus> userDocList = userDocumentStatusRepo
								.findByUserIdAndDocumentType(user.getId(), DocumentType.Kyc);

						for (UserDocumentStatus docs : userDocList) {

							if (docs != null) {
								if (docs.getDocumentSubType().equalsIgnoreCase(KycType.AADHAR.toString())
										|| docs.getDocumentSubType().equalsIgnoreCase(KycType.DRIVINGLICENCE.toString())
										|| docs.getDocumentSubType().equalsIgnoreCase(KycType.PASSPORT.toString())
										|| docs.getDocumentSubType().equalsIgnoreCase(KycType.VOTERID.toString())) {
									personalDetailsResponseDto.setKycStatus(true);
									return personalDetailsResponseDto;

								}
							}
						}
					}
				}

				if (user.getPrimaryType().equals(PrimaryType.BORROWER)) {
					if (userDocumentStatusPan != null && userDocumentStatusBankStatement != null
							&& userDocumentStatusPayslip != null) {
						List<UserDocumentStatus> userDocList = userDocumentStatusRepo
								.findByUserIdAndDocumentType(user.getId(), DocumentType.Kyc);
						if (user.getPersonalDetails().getIsStudent() == true) {
							if (userDocList != null && !userDocList.isEmpty()) {
								if (userDocList.size() >= 12) {
									personalDetailsResponseDto.setKycStatus(true);
									return personalDetailsResponseDto;
								}
							}
						} else {
							if (userDocList.size() >= 5) {
								personalDetailsResponseDto.setKycStatus(true);
								return personalDetailsResponseDto;
							}
						}
					}
				}
			}

		}

		if (user.getStatus().equals(com.oxyloans.entity.user.User.Status.ACTIVE)) {
			personalDetailsResponseDto.setKycStatus(true);
			return personalDetailsResponseDto;

		}
		return personalDetailsResponseDto;
	}

	@Override
	public KycFileResponse downloadFile(Integer userId, KycType kycFile) throws FileNotFoundException, IOException {
		User user = userRepo.findById(userId).get();
		UserDocumentStatus userDocumentStatus = userDocumentStatusRepo
				.findByUserIdAndDocumentTypeAndDocumentSubType(userId, DocumentType.Kyc, kycFile.name());
		if (userDocumentStatus == null) {
			throw new FileNotFoundException("File not found to download");
		}
		FileRequest fileRequest = new FileRequest();
		fileRequest.setFileName(userDocumentStatus.getFileName());
		fileRequest.setFileType(FileType.Kyc);
		fileRequest.setUserId(user.getId());
		fileRequest.setFilePrifix(kycFile.name());
		FileResponse file = fileManagementService.getFile(fileRequest);
		KycFileResponse fileResponse = new KycFileResponse();
		fileResponse.setDownloadUrl(file.getDownloadUrl());
		fileResponse.setFileName(userDocumentStatus.getFileName());
		fileResponse.setKycType(kycFile);
		return fileResponse;
	}

	private boolean canSetStatusToActive(User user, List<UserDocumentStatus> kycDocs) {
		if (user.getPrimaryType() == PrimaryType.LENDER) {

			return false;
		}
		if (user.getPersonalDetails() == null) {

			return false;
		}
		if (kycDocs == null || kycDocs.isEmpty()) {

			return false;
		}
		for (UserDocumentStatus kyc : kycDocs) {
			if (kyc.getStatus() == UserDocumentStatus.Status.REJECTED) {

				return false;
			}
		}
		return true;
	}

	// For now this is allowed only for Admin when scenario comes re visit this
	@Override
	@Transactional
	public UserResponse updateOxyScore(int userId, UserRequest userRequest) {
		int currentUser = authorizationService.getCurrentUser();
		User adminUser = userRepo.findById(currentUser).get();
		if (adminUser.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + adminUser.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}
		int oxyScore = userRequest.getOxyScore();
		User user = userRepo.findById(userId).get();
		user.setOxyScore(oxyScore);
		user = userRepo.save(user);
		UserResponse response = new UserResponse();
		response.setId(userId);
		return response;
	}

	@Override
	@Transactional
	public UserResponse migrate(int userId, PrimaryType toPrimaryType, boolean deleteOldData) {
		int currentUser = authorizationService.getCurrentUser();
		User adminUser = userRepo.findById(currentUser).get();
		if (adminUser.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + adminUser.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}
		User user = userRepo.findById(userId).get();
		if (user.getPrimaryType() == toPrimaryType) {

			return null;
		}
		loanServiceFactory.getService(PrimaryType.ADMIN.name()).deleteLoans(userId);

		// Changing in Loan Application as well
		loanServiceFactory.getService(PrimaryType.ADMIN.name()).changeDefaultLoanPrimaryType(user.getId(),
				toPrimaryType);

		user.setUniqueNumber(user.getUniqueNumber().replaceFirst(user.getPrimaryType().getShortCode(),
				toPrimaryType.getShortCode()));
		user.setPrimaryType(toPrimaryType);
		user = userRepo.save(user);
		UserResponse userResponse = new UserResponse();
		userResponse.setId(userId);
		userResponse.setPrimaryType(user.getPrimaryType().name());
		return userResponse;
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		byte[] byteArray = IOUtils.toByteArray(
				new BufferedInputStream(new FileInputStream(new File("/Users/damodharreddy/Downloads/IMG_6171.jpg"))));

	}

	@Override
	@Transactional
	public UserResponse saveCity(int id, UserRequest userRequest) {
		User user = userRepo.findById(id).get();
		if (user.getCity() != null) {
			user.setCity(userRequest.getCity());
			userRepo.save(user);
		}

		UserResponse response = new UserResponse();
		response.setId(user.getId());
		response.setCity(user.getCity());
		return response;
	}

	@Override
	public List<UserDocumentStatus> getDetails(int userId) {
		List<UserDocumentStatus> userDocumentStatus = userDocumentStatusRepo.findByUserId(userId);
		List<UserDocumentStatus> documentStatus = new ArrayList<UserDocumentStatus>();
		for (UserDocumentStatus userDocument : userDocumentStatus) {
			UserDocumentStatus document = new UserDocumentStatus();
			document.setUserId(userDocument.getUserId());
			document.setDocumentType(userDocument.getDocumentType());
			document.setDocumentSubType(userDocument.getDocumentSubType());
			document.setStatus(userDocument.getStatus());
			documentStatus.add(document);
		}
		return documentStatus;
	}

	@Override
	public PincodeResultes saveCity1(String pinCode) throws org.json.simple.parser.ParseException {

		PincodeResultes responce = pinCodeUtil.getDetails(pinCode);
		return responce;
	}

	@Override
	@Transactional
	public PersonalDetailsResponseDto updateProfilePage(int userId,
			PersonalDetailsRequestDto personalDetailsRequestDto) {
		User user = userRepo.findById(userId).get();
		PersonalDetails personalDetails = user.getPersonalDetails();
		ProfessionalDetails professionalDetails = user.getProfessionalDetails();
		FinancialDetails financialDetails = user.getFinancialDetails();
		BankDetails bankDetails = user.getBankDetails();
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);

		if (user.getPersonalDetails() == null || user.getPersonalDetails() != null) {
			if (personalDetails == null) {
				personalDetails = new PersonalDetails();
				personalDetails.setFatherName(personalDetailsRequestDto.getFatherName());
				personalDetails.setFacebookUrl(personalDetailsRequestDto.getFacebookUrl());
				personalDetails.setLinkedinUrl(personalDetailsRequestDto.getLinkedinUrl());
				personalDetails.setTwitterUrl(personalDetailsRequestDto.getTwitterUrl());
				personalDetails.setWhatsAppNumber(personalDetailsRequestDto.getWhatsAppNumber());

			} else {
				if (personalDetailsRequestDto.getFirstName() != null) {
					personalDetails.setFirstName(personalDetailsRequestDto.getFirstName());
				}
				if (personalDetailsRequestDto.getFacebookUrl() != null) {
					personalDetails.setFacebookUrl(personalDetailsRequestDto.getFacebookUrl());
				}

				if (personalDetailsRequestDto.getLinkedinUrl() != null) {
					personalDetails.setTwitterUrl(personalDetailsRequestDto.getLinkedinUrl());
				}

				if (personalDetailsRequestDto.getLinkedinUrl() != null) {
					personalDetails.setLinkedinUrl(personalDetailsRequestDto.getTwitterUrl());
				}

				if (personalDetailsRequestDto.getLastName() != null) {
					personalDetails.setLastName(personalDetailsRequestDto.getLastName());
				}
				if (personalDetailsRequestDto.getFatherName() != null) {
					personalDetails.setFatherName(personalDetailsRequestDto.getFatherName());
				}
				if (personalDetailsRequestDto.getDob() != null) {

					try {
						personalDetails.setDob(expectedDateFormat.parse(personalDetailsRequestDto.getDob()));
					} catch (ParseException e) {
						throw new DataFormatException("Illegal Date format. It should be dd/MM/yyyy",
								ErrorCodes.INVALID_DATE_FORMAT);
					}
				}

				if (personalDetailsRequestDto.getGender() != null) {
					personalDetails.setGender(personalDetailsRequestDto.getGender());
				}
				if (personalDetailsRequestDto.getAddress() != null) {

					personalDetails.setAddress(personalDetailsRequestDto.getAddress());
				}
				if (personalDetailsRequestDto.getPermanentAddress() != null) {
					personalDetails.setPermanentAddress(personalDetailsRequestDto.getPermanentAddress());
				}
				if (personalDetailsRequestDto.getPanNumber() != null) {
					personalDetails.setPanNumber(personalDetailsRequestDto.getPanNumber());
				}
				if (personalDetailsRequestDto.getMiddleName() != null) {
					personalDetails.setMiddleName(personalDetailsRequestDto.getMiddleName());
				}

				if (personalDetailsRequestDto.getWhatsAppNumber() != null) {
					personalDetails.setWhatsAppNumber(personalDetailsRequestDto.getWhatsAppNumber());
				}

				if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
					if (personalDetailsRequestDto.getLoanRequestAmount() != null) {
						loanRequest.setLoanRequestAmount(personalDetailsRequestDto.getLoanRequestAmount());

					}
				}
				if (personalDetailsRequestDto.getPinCode() != null) {
					user.setPinCode(personalDetailsRequestDto.getPinCode());

				}
				if (personalDetailsRequestDto.getCity() != null) {
					user.setCity(personalDetailsRequestDto.getCity());
				}
				if (personalDetailsRequestDto.getState() != null) {
					user.setState(personalDetailsRequestDto.getState());
				}
				if (personalDetailsRequestDto.getLocality() != null) {
					user.setLocality(personalDetailsRequestDto.getLocality());
				}

				if (personalDetailsRequestDto.getAadharNumber() != null) {
					personalDetails.setAadharNumber(personalDetailsRequestDto.getAadharNumber());
				}

				logger.info("Modify option for updating whatsapp number starts....................");
				if (personalDetailsRequestDto.getModifyOption() != null) {
					if (personalDetailsRequestDto.getModifyOption().equalsIgnoreCase("MODIFY")) {

						UserRequest userRequest = new UserRequest();

						userRequest.setWhatsappNumber(personalDetailsRequestDto.getWhatsAppNumber());

						userRequest.setWhatsappOtp(personalDetailsRequestDto.getWhatsappOtp());

						userRequest.setId(personalDetailsRequestDto.getId());

						if (userRequest.getWhatsappNumber() != null) {
							if (userRequest.getWhatsappNumber() != null && userRequest.getWhatsappOtp().equals("")) {
								UserResponse userResponse2 = null;

								try {

									userResponse2 = sendWhatsappOtp(userRequest);

								} catch (Exception ex) {
									throw new ActionNotAllowedException(ex.getMessage(), ErrorCodes.PERMISSION_DENIED);
								}
							}
						}

						if (userRequest.getWhatsappOtp() != null && !userRequest.getWhatsappOtp().equals("")) {
							String whatsappOtp = userRequest.getWhatsappOtp();

							String hash = signatureService.hashPassword(whatsappOtp, user.getSalt());

							if (!hash.equals(user.getWhatsappHash())) {
								throw new MobileOtpVerifyFailedException("Invalid OTP value please check.",
										ErrorCodes.INVALID_OTP);
							}

							personalDetails.setWhatsappVerifiedOn(new Date());
							user.setPersonalDetails(personalDetails);
							user.setWhatsappVerified(true);
							userRepo.save(user);
						}

						personalDetails.setWhatsAppNumber(personalDetailsRequestDto.getWhatsAppNumber());

						if (user.isWhatsappVerified()) {

							String message = "Hi *" + personalDetails.getFirstName() + ",* " + "\n\n"
									+ "Your WhatsApp number has been successfully verified. Thank you for confirming!."
									+ "\n\n" + "Thanks,\n" + "*Oxyloans Team*";

							String whatsappUpdate = whatappServiceRepo.sendingIndividualMessageUsingUltraApi(
									personalDetails.getWhatsAppNumber(), message);
						}

						logger.info("Modify option for updating whatsapp number ends....................");
					}

				}
				if (user.isWhatsappVerified()) {

					String message = "Hi *" + personalDetails.getFirstName() + ",* " + "\n\n"
							+ "Your profile details have been saved successfully." + "\n\n" + "Thanks,\n"
							+ "*Oxyloans Team*";

					// String whatsappUpdate =
					// whatappServiceRepo.sendingIndividualMessageUsingUltraApi(personalDetails.getWhatsAppNumber(),
					// message);
				}
				boolean studentOrNot = personalDetailsRequestDto.isStudentOrNot();
				if (studentOrNot == true) {
					personalDetails.setIsStudent(true);
				} else {
					personalDetails.setIsStudent(false);
				}

				savePersonalDetailsHistory(userId, personalDetailsRequestDto);
				user.setPersonalDetails(personalDetails);

			}
		}

		if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
			if (professionalDetails == null) {
				professionalDetails = new ProfessionalDetails();
				professionalDetails.setEmployment(
						Employment.valueOf(personalDetailsRequestDto.getEmployment().toString().toUpperCase()));

				professionalDetails.setCompanyName(
						personalDetailsRequestDto.getCompanyName() != null ? personalDetailsRequestDto.getCompanyName()
								: "");
				professionalDetails.setWorkExperience(personalDetailsRequestDto.getWorkExperience() != null
						? personalDetailsRequestDto.getWorkExperience()
						: " ");
				if (financialDetails == null) {
					financialDetails = new FinancialDetails();
					financialDetails.setNetMonthlyIncome(
							personalDetailsRequestDto.getSalary() != null ? personalDetailsRequestDto.getSalary() : 0);
					user.setFinancialDetails(financialDetails);
					financialDetails.setUser(user);
					user = userRepo.save(user);
				}

				user.setProfessionalDetails(professionalDetails);
				professionalDetails.setUser(user);
				user = userRepo.save(user);
			}

			else {

				if (personalDetailsRequestDto.getEmployment() != null) {

					professionalDetails.setEmployment(
							Employment.valueOf(personalDetailsRequestDto.getEmployment().toString().toUpperCase()));
					if (user.getProfessionalDetails().getCompanyName() != null
							|| user.getProfessionalDetails().getCompanyName() == null) {
						professionalDetails.setCompanyName(personalDetailsRequestDto.getCompanyName() != null
								? personalDetailsRequestDto.getCompanyName()
								: "");
					}
					if (user.getProfessionalDetails().getWorkExperience() != null
							|| user.getProfessionalDetails().getWorkExperience() == null) {
						professionalDetails.setWorkExperience(personalDetailsRequestDto.getWorkExperience() != null
								? personalDetailsRequestDto.getWorkExperience()
								: "");
					}

					user.setProfessionalDetails(professionalDetails);
				}

				if (personalDetailsRequestDto.getSalary() != null) {
					if (financialDetails == null) {
						financialDetails = new FinancialDetails();
						financialDetails.setNetMonthlyIncome(
								personalDetailsRequestDto.getSalary() != null ? personalDetailsRequestDto.getSalary()
										: 0);
						user.setFinancialDetails(financialDetails);
						financialDetails.setUser(user);
						user = userRepo.save(user);
					}
					financialDetails.setNetMonthlyIncome(
							personalDetailsRequestDto.getSalary() != null ? personalDetailsRequestDto.getSalary() : 0);
				}
			}
		}
		if (bankDetails == null) {
			bankDetails = new BankDetails();
			bankDetails.setUserName("");
			bankDetails.setAccountNumber("");
			bankDetails.setIfscCode("");
			bankDetails.setBankName("");
			bankDetails.setBranchName("");
			bankDetails.setAddress("");

			user.setBankDetails(bankDetails);
			bankDetails.setUser(user);
			user = userRepo.save(user);

		} else {
			/*
			 * if (user.getPrimaryType().equals(User.PrimaryType.BORROWER)) { if
			 * (user.getReferenceDetails() == null) { throw new
			 * ActionNotAllowedException(" Reference details not filled ",
			 * ErrorCodes.INCOMPLETED_PROFILE);
			 * 
			 * } } else {
			 */
			// if (user.getPinCode() == null) {
			if (user.getPinCode().length() <= 0) {
				throw new ActionNotAllowedException(" Profile details not filled ", ErrorCodes.INCOMPLETED_PROFILE);

			}
			// }

			/*
			 * if (personalDetailsRequestDto.isUpdateBankDetails() &&
			 * (personalDetailsRequestDto.getMobileOtp() != null &&
			 * personalDetailsRequestDto.getMobileOtpSession() != null)) {
			 * 
			 */
			if (personalDetailsRequestDto.getMobileOtp() != null
					&& personalDetailsRequestDto.getMobileOtpSession() != null) {
				boolean verifyOtp = mobileUtil.verifyOtp(personalDetailsRequestDto.getMobileNumber(),
						personalDetailsRequestDto.getMobileOtpSession(), personalDetailsRequestDto.getMobileOtp());

				if (!verifyOtp) {
					throw new MobileOtpVerifyFailedException("Invalid OTP value please check.", ErrorCodes.INVALID_OTP);
				}
			}

			if (personalDetailsRequestDto.getUserName() != null) {
				bankDetails.setUserName(personalDetailsRequestDto.getUserName());
			}
			if (personalDetailsRequestDto.getConfirmAccountNumber() != null) {
				if (personalDetailsRequestDto.getAccountNumber() != null) {
					if (personalDetailsRequestDto.getAccountNumber()
							.equals(personalDetailsRequestDto.getConfirmAccountNumber())) {
						logger.info("accno:" + bankDetails.getAccountNumber());

						if (user.getPrimaryType().equals(PrimaryType.LENDER)) {
							if (!bankDetails.getAccountNumber().equals("")) {
								if (personalDetailsRequestDto.getMobileOtp() != null
										&& personalDetailsRequestDto.getMobileOtpSession() != null) {

									String lastName = personalDetails.getLastName().trim();
									String firstName = personalDetails.getFirstName().trim();
									String message;

									if (lastName != null && !lastName.isEmpty()) {
										message = "Hello *" + firstName + "*  *" + lastName + "*, "
												+ "You have updated your bank details on *" + LocalDate.now() + "*.\r\n"
												+ "\r\n" + "Please find the details below\r\n" + "Name :"
												+ personalDetailsRequestDto.getUserName() + "\r\n" + "Account No.:"
												+ personalDetailsRequestDto.getAccountNumber() + "\r\n" + "Ifsc Code :"
												+ personalDetailsRequestDto.getIfscCode() + "\r\n" + "Bank Name :"
												+ personalDetailsRequestDto.getBankName() + "\r\n" + "\r\n"
												+ "*This is a system generated message*";
									} else {
										message = "Hello *" + firstName + "*, "
												+ "You have updated your bank details on *" + LocalDate.now() + "*.\r\n"
												+ "\r\n" + "Please find the details below\r\n" + "Name :"
												+ personalDetailsRequestDto.getUserName() + "\r\n" + "Account No.:"
												+ personalDetailsRequestDto.getAccountNumber() + "\r\n" + "Ifsc Code :"
												+ personalDetailsRequestDto.getIfscCode() + "\r\n" + "Bank Name :"
												+ personalDetailsRequestDto.getBankName() + "\r\n" + "\r\n"
												+ "*This is a system generated message*";
									}

									List<String> chatIds = new ArrayList<String>();
									chatIds.add("120363020098924954@g.us");
									chatIds.add(personalDetails.getWhatsAppNumber() != null
											? personalDetails.getWhatsAppNumber() + "@c.us"
											: "91" + user.getMobileNumber() + "@c.us");

									String whatsappNumber = personalDetails.getWhatsAppNumber() != null
											? personalDetails.getWhatsAppNumber() + "@c.us"
											: "91" + user.getMobileNumber() + "@c.us";

									whatappServiceRepo.sendingIndividualMessageUsingUltraApi(whatsappNumber, message);

									if (!user.isTestUser()) {
										whatappServiceRepo.sendingWhatsappMessageWithNewInstance(
												"120363020098924954@g.us", message);
									} else {
										whatappServiceRepo.sendingWhatsappMessageWithNewInstance(
												"917702795895-1630419500@g.us", message);
									}
								}
							}
							bankDetails.setBankDetailsVerifiedstatus(true);
							user.setBankDetails(bankDetails);
							bankDetails.setUser(user);
							user = userRepo.save(user);
						}
						bankDetails.setAccountNumber(personalDetailsRequestDto.getAccountNumber());

					} else {
						throw new OperationNotAllowedException("Bank Account number is not matching",
								ErrorCodes.ENITITY_NOT_FOUND);
					}
				}
			}
			if (personalDetailsRequestDto.getIfscCode() != null) {
				bankDetails.setIfscCode(personalDetailsRequestDto.getIfscCode());
			}
			if (personalDetailsRequestDto.getBankName() != null) {
				bankDetails.setBankName(personalDetailsRequestDto.getBankName());
			}
			if (personalDetailsRequestDto.getBranchName() != null) {
				bankDetails.setBranchName(personalDetailsRequestDto.getBranchName());
			}
			if (personalDetailsRequestDto.getBankAddress() != null) {
				bankDetails.setAddress(personalDetailsRequestDto.getBankAddress());
			}

			user.setBankDetails(bankDetails);
			// }
		}

		PersonalDetailsResponseDto personalDetailsResponseDto = new PersonalDetailsResponseDto();
		personalDetailsResponseDto.setId(personalDetails.getUserId());
		personalDetailsResponseDto.setUserId(user.getId());
		personalDetailsResponseDto.setWhatsAppNumber(personalDetailsRequestDto.getWhatsAppNumber());
		return personalDetailsResponseDto;

	}

	public String savePersonalDetailsHistory(int userId, PersonalDetailsRequestDto personalDetailsRequestDto) {

		PersonalDetailsModifiedHistory personalDetailsModifiedHistory = new PersonalDetailsModifiedHistory();

		if (personalDetailsRequestDto.getFirstName() != null) {
			personalDetailsModifiedHistory.setFirstName(personalDetailsRequestDto.getFirstName());
		}

		if (personalDetailsRequestDto.getLastName() != null) {
			personalDetailsModifiedHistory.setLastName(personalDetailsRequestDto.getLastName());
		}

		if (personalDetailsRequestDto.getAddress() != null) {

			personalDetailsModifiedHistory.setAddress(personalDetailsRequestDto.getAddress());
		}
		if (personalDetailsRequestDto.getPermanentAddress() != null) {
			personalDetailsModifiedHistory.setPermanentAddress(personalDetailsRequestDto.getPermanentAddress());
		}

		if (personalDetailsRequestDto.getWhatsAppNumber() != null) {
			personalDetailsModifiedHistory.setWhatsAppNumber(personalDetailsRequestDto.getWhatsAppNumber());
		}

		if (personalDetailsRequestDto.getMaritalStatus() != null) {
			personalDetailsModifiedHistory.setMartialStatus(personalDetailsRequestDto.getMaritalStatus());
		}

		if (personalDetailsRequestDto.getNationality() != null) {
			personalDetailsModifiedHistory.setNationality(personalDetailsRequestDto.getNationality());
		}

		if (personalDetailsRequestDto.getUserName() != null) {
			personalDetailsModifiedHistory.setNameAsPerBank(personalDetailsRequestDto.getUserName());
		}
		if (personalDetailsRequestDto.getAccountNumber() != null) {
			personalDetailsModifiedHistory.setAccountNumber(personalDetailsRequestDto.getAccountNumber());
		}
		if (personalDetailsRequestDto.getIfscCode() != null) {
			personalDetailsModifiedHistory.setIfscCode(personalDetailsRequestDto.getIfscCode());
		}
		if (personalDetailsRequestDto.getBankName() != null) {
			personalDetailsModifiedHistory.setBankName(personalDetailsRequestDto.getBankName());
		}
		if (personalDetailsRequestDto.getBranchName() != null) {
			personalDetailsModifiedHistory.setBranchName(personalDetailsRequestDto.getBranchName());
		}
		if (personalDetailsRequestDto.getBankAddress() != null) {
			personalDetailsModifiedHistory.setBankAddress(personalDetailsRequestDto.getBankAddress());
		}

		personalDetailsModifiedHistory.setModifiedOn(new Date());
		personalDetailsModifiedHistory.setUserId(userId);
		personalDetailsModifiedHistory = personalDetailsModifiedHistoryRepo.save(personalDetailsModifiedHistory);

		if (personalDetailsModifiedHistory != null) {

			return "saved";
		}

		return "not saved";

	}

	@Override
	@Transactional
	public PersonalDetailsResponseDto ProfilePageSubmissionAtATime(int userId,
			PersonalDetailsRequestDto personalDetailsRequestDto) {
		User user = userRepo.findById(userId).get();
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);

		ProfessionalDetails professionalDetails = user.getProfessionalDetails();
		FinancialDetails financialDetails = user.getFinancialDetails();
		BankDetails bankDetails = user.getBankDetails();
		PersonalDetails personalDetails = user.getPersonalDetails();
		if (personalDetails == null) {
			personalDetails = new PersonalDetails();

			user.setPersonalDetails(personalDetails);
			personalDetails.setUser(user);
			user = userRepo.save(user);
		} else {
			if (personalDetailsRequestDto.getFirstName() != null) {
				personalDetails.setFirstName(personalDetailsRequestDto.getFirstName());
			}
			if (personalDetailsRequestDto.getLastName() != null) {
				personalDetails.setLastName(personalDetailsRequestDto.getLastName());
			}
			if (personalDetailsRequestDto.getFatherName() != null) {
				personalDetails.setFatherName(personalDetailsRequestDto.getFatherName());
			}
			if (personalDetailsRequestDto.getDob() != null) {

				try {
					personalDetails.setDob(expectedDateFormat.parse(personalDetailsRequestDto.getDob()));
				} catch (ParseException e) {
					e.printStackTrace();
				}

			}
			if (personalDetailsRequestDto.getGender() != null) {
				personalDetails.setGender(personalDetailsRequestDto.getGender());
			}

			if (personalDetailsRequestDto.getModifiedAt() != null) {
				personalDetails.setModifiedAt(new Date());
			}

			if (personalDetailsRequestDto.getCreatedAt() != null) {
				personalDetails.setCreatedAt(new Date());
			}
			if (personalDetailsRequestDto.getAddress() != null) {
				personalDetails.setAddress(personalDetailsRequestDto.getAddress());
			}
			if (personalDetailsRequestDto.getPermanentAddress() != null) {
				personalDetails.setPermanentAddress(personalDetailsRequestDto.getPermanentAddress());
			}

			if (personalDetailsRequestDto.getPanNumber() != null) {
				personalDetails.setPanNumber(personalDetailsRequestDto.getPanNumber());
			}
			if (personalDetailsRequestDto.getMiddleName() != null) {
				personalDetails.setMiddleName(personalDetailsRequestDto.getMiddleName());
			}
			if (personalDetailsRequestDto.getPinCode() != null) {
				user.setPinCode(personalDetailsRequestDto.getPinCode());

			}
			if (personalDetailsRequestDto.getCity() != null) {
				user.setCity(personalDetailsRequestDto.getCity());
			}
			if (personalDetailsRequestDto.getState() != null) {
				user.setState(personalDetailsRequestDto.getState());
			}
			if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
				if (personalDetailsRequestDto.getLoanRequestAmount() != null) {
					loanRequest.setLoanRequestAmount(personalDetailsRequestDto.getLoanRequestAmount());
				}
			}
			user.setPersonalDetails(personalDetails);
		}

		if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
			if (professionalDetails == null) {
				professionalDetails = new ProfessionalDetails();
				professionalDetails.setEmployment(
						Employment.valueOf(personalDetailsRequestDto.getEmployment().toString().toUpperCase()));
				if (personalDetailsRequestDto.getEmployment().toString().equals(Employment.SALARIED.toString())) {
					professionalDetails.setCompanyName(personalDetailsRequestDto.getCompanyName());
					professionalDetails.setWorkExperience(personalDetailsRequestDto.getWorkExperience());
					if (financialDetails == null) {
						financialDetails = new FinancialDetails();
						financialDetails.setNetMonthlyIncome(personalDetailsRequestDto.getSalary());
						user.setFinancialDetails(financialDetails);
						financialDetails.setUser(user);
						user = userRepo.save(user);
					}

				}
				user.setProfessionalDetails(professionalDetails);
				professionalDetails.setUser(user);
				user = userRepo.save(user);

			} else {
				if (personalDetailsRequestDto.getEmployment() != null) {
					professionalDetails.setEmployment(
							Employment.valueOf(personalDetailsRequestDto.getEmployment().toString().toUpperCase()));
					if (personalDetailsRequestDto.getEmployment().toString().equals(Employment.SALARIED.toString())) {
						if (user.getProfessionalDetails().getCompanyName() != null
								&& user.getProfessionalDetails().getCompanyName().isEmpty()) {
							professionalDetails.setCompanyName(personalDetailsRequestDto.getCompanyName());
						}
						if (user.getProfessionalDetails().getWorkExperience() != null) {
							professionalDetails.setWorkExperience(personalDetailsRequestDto.getWorkExperience());
						}
					}
					user.setProfessionalDetails(professionalDetails);
				}
			}

			if (personalDetailsRequestDto.getSalary() != null) {
				if (financialDetails == null) {
					financialDetails = new FinancialDetails();
					financialDetails.setNetMonthlyIncome(personalDetailsRequestDto.getSalary());
					user.setFinancialDetails(financialDetails);
					financialDetails.setUser(user);
					user = userRepo.save(user);
				}
				financialDetails.setNetMonthlyIncome(personalDetailsRequestDto.getSalary());
			}

		}

		if (bankDetails == null) {
			bankDetails = new BankDetails();
			bankDetails.setUserName(personalDetailsRequestDto.getUserName());
			bankDetails.setAccountNumber(personalDetailsRequestDto.getAccountNumber());
			bankDetails.setIfscCode(personalDetailsRequestDto.getIfscCode());
			bankDetails.setBankName(personalDetailsRequestDto.getBankName());
			bankDetails.setBranchName(personalDetailsRequestDto.getBranchName());
			bankDetails.setAddress(personalDetailsRequestDto.getBankAddress());
			if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
				bankDetails.setModeOfTransactions(personalDetailsRequestDto.getModeOfTransactions());
			}
			user.setBankDetails(bankDetails);
			bankDetails.setUser(user);
			user = userRepo.save(user);

		} else {
			bankDetails.setUserName(personalDetailsRequestDto.getUserName());
			bankDetails.setAccountNumber(personalDetailsRequestDto.getAccountNumber());
			bankDetails.setIfscCode(personalDetailsRequestDto.getIfscCode());
			bankDetails.setBankName(personalDetailsRequestDto.getBankName());
			bankDetails.setBranchName(personalDetailsRequestDto.getBranchName());
			bankDetails.setAddress(personalDetailsRequestDto.getBankAddress());
			if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
				bankDetails.setModeOfTransactions(personalDetailsRequestDto.getModeOfTransactions());
			}
		}

		PersonalDetailsResponseDto personalDetailsResponseDto = new PersonalDetailsResponseDto();
		personalDetailsResponseDto.setId(personalDetails.getUserId());
		personalDetailsResponseDto.setUserId(user.getId());
		return personalDetailsResponseDto;
	}

	@Override
	@Transactional
	public UserResponse getLoanRequestedAmountAndPaymentFeeRoi(int userId) {
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
		UserResponse userResponse = new UserResponse();
		userResponse.setLoanAmountRequest(loanRequest.getLoanRequestAmount());
		userResponse.setPaymentFeeRateOfInterest(loanRequest.getPaymentFeeRateOfInterest());
		return userResponse;

	}

	@Override
	@Transactional
	public UserResponse updateLoanRequestedAmountAndPaymentFeeRoi(int userId, UserRequest userRequest) {

		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);

		if (loanRequest.getLoanRequestAmount() != null) {
			if (userRequest.getLoanAmountApprovedByAdmin() != null) {
				loanRequest.setLoanAmountApprovedByAdmin(userRequest.getLoanAmountApprovedByAdmin());
			} else {
				loanRequest.setLoanAmountApprovedByAdmin(loanRequest.getLoanRequestAmount());
			}
			loanRequestRepo.save(loanRequest);
			if (userRequest.getPaymentFeeRateOfInterest() != null) {
				loanRequest.setPaymentFeeRateOfInterest(userRequest.getPaymentFeeRateOfInterest());
			} else {
				loanRequest.setPaymentFeeRateOfInterest(4);
			}

			loanRequestRepo.save(loanRequest);

		}

		UserResponse response = new UserResponse();
		response.setLoanAmountApprovedByAdmin(loanRequest.getLoanAmountApprovedByAdmin());
		response.setPaymentFeeRateOfInterest(loanRequest.getPaymentFeeRateOfInterest());
		return response;
	}

	@Override
	@Transactional
	public UserResponse calculatingFeeValue(int userId) {

		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);

		int paymentFeeRateOfInterest = loanRequest.getPaymentFeeRateOfInterest();
		double loanAmountApprovedByAdmin = loanRequest.getLoanAmountApprovedByAdmin();

		double rateOfInterestForFee = (loanAmountApprovedByAdmin * paymentFeeRateOfInterest) / 100;

		double gst = (rateOfInterestForFee * 18) / 100;

		double borrowerFee = rateOfInterestForFee + gst;

		UserResponse response = new UserResponse();
		response.setPaymentFeeRateOfInterest(paymentFeeRateOfInterest);
		response.setLoanAmountApprovedByAdmin(loanAmountApprovedByAdmin);
		response.setAmountForGivenInterest(rateOfInterestForFee);
		response.setGst(gst);
		response.setBorrowerFee(borrowerFee);
		return response;

	}

	@Override
	public UserResponse uploadScowTransactionDetails(Integer userId, List<KycFileRequest> kycFiles)
			throws FileNotFoundException, IOException {

		User user = userRepo.findById(userId).get();
		UserDocumentStatus docStatus = new UserDocumentStatus();
		for (KycFileRequest kyc : kycFiles) {
			FileRequest fileRequest = new FileRequest();

			InputStream fileStream = kyc.getFileInputStream();
			if (fileStream == null) {
				fileStream = decodeBase64Stream(kyc.getBase64EncodedStream());
			}
			fileRequest.setInputStream(fileStream);
			fileRequest.setUserId(userId);
			fileRequest.setFileName(kyc.getFileName());
			fileRequest.setFilePrifix(kyc.getKycType().name());
			FileResponse putFile = fileManagementService.putFile(fileRequest);
			UserDocumentStatus userDocumentStatus = new UserDocumentStatus();
			userDocumentStatus.setUserId(user.getId());
			userDocumentStatus.setDocumentType(DocumentType.ScrowScreenShot);
			userDocumentStatus.setDocumentSubType(kyc.getKycType().toString());
			userDocumentStatus.setFilePath(putFile.getFilePath());
			userDocumentStatus.setFileName(kyc.getFileName());
			userDocumentStatus.setStatus(Status.UPLOADED);
			docStatus = userDocumentStatusRepo.save(userDocumentStatus);
		}

		UserResponse response = new UserResponse();
		response.setId(user.getId());
		response.setDocumentId(docStatus.getId());
		return response;
	}

	@Override
	public KycFileResponse downloadTransactionScreenshot(Integer documentId, Integer userId, KycType kycFile)
			throws FileNotFoundException, IOException {
		User user = userRepo.findById(userId).get();
		UserDocumentStatus userDocumentStatus = userDocumentStatusRepo.findByIdAndUserId(documentId, userId);
		if (userDocumentStatus == null) {
			throw new FileNotFoundException("File not found to download");
		}
		FileRequest fileRequest = new FileRequest();
		fileRequest.setFileName(userDocumentStatus.getFileName());
		fileRequest.setFileType(FileType.Kyc);
		fileRequest.setUserId(user.getId());
		fileRequest.setFilePrifix(kycFile.name());
		FileResponse file = fileManagementService.getFile(fileRequest);
		KycFileResponse fileResponse = new KycFileResponse();
		fileResponse.setDownloadUrl(file.getDownloadUrl());
		fileResponse.setFileName(userDocumentStatus.getFileName());
		fileResponse.setKycType(kycFile);
		return fileResponse;
	}

	public ScrowLenderTransactionResponse saveScrowTransactionDetails(int userId,
			ScrowTransactionRequest scrowTransactionRequest) {
		logger.info("---------------------saveScrowTransactionDetails method start ...............");
		LenderOxyWallet oxyWalletresponse = new LenderOxyWallet();
		Double amount = scrowTransactionRequest.getTransactionAmount();
		Integer transactionAmount = 0;
		try {
			LenderOxyWallet oxyWallet = new LenderOxyWallet();
			oxyWallet.setUserId(userId);
			oxyWallet.setScrowAccountNumber(scrowTransactionRequest.getScrowAccountNumber());

			transactionAmount = amount.intValue();

			oxyWallet.setTransactionAmount(transactionAmount);
			oxyWallet.setDocumentUploadedId(scrowTransactionRequest.getDocumentUploadedId());
			oxyWallet.setStatus(LenderOxyWallet.Status.UPLOADED);
			oxyWallet.setTransactionType("credit");
			oxyWallet.setTransactionDate(expectedDateFormat.parse(scrowTransactionRequest.getTransactionDate()));
			oxyWallet.setCreatedBy("" + userId);
			oxyWalletresponse = lenderOxyWalletRepo.save(oxyWallet);

		} catch (Exception e) {

			throw new DataFormatException("Error While Saving the Data", ErrorCodes.DATA_PERSIST_ERROR);
		}
		ScrowLenderTransactionResponse response = new ScrowLenderTransactionResponse();
		response.setUserId(oxyWalletresponse.getUserId());
		response.setScrowAccountNumber(oxyWalletresponse.getScrowAccountNumber());
		response.setTransactionAmount(transactionAmount);
		// response.setDocumentUploadedId(oxyWalletresponse.getDocumentUploadedId());
		response.setStatus("" + oxyWalletresponse.getStatus());
		response.setTransactionDate(expectedDateFormat.format(oxyWalletresponse.getTransactionDate()));
		response.setCreatedBy("" + userId);
		logger.info("---------------------saveScrowTransactionDetails method end ...............");
		return response;

	}

	@Override
	public ScrowWalletResponse getLenderWalletDetails(int userId, PageDto pageDto) {
		logger.info("---------------------getLenderWalletDetails method start ...............");
		ScrowWalletResponse walletResponse = new ScrowWalletResponse();
		List<Object[]> objList = lenderOxyWalletNativeRepo.getLenderWalletDetails(userId, pageDto.getPageSize(),
				pageDto.getPageNo());
		Integer totalCount = 0;
		Long totalCount1 = 0l;
		if (userId == 0) {
			totalCount1 = lenderOxyWalletRepo.count();
			walletResponse.setTotalCount(totalCount1.intValue());
		} else {
			totalCount = lenderOxyWalletRepo.countByUserId(userId);
			walletResponse.setTotalCount(totalCount);
		}
		List<ScrowLenderTransactionResponse> response = new ArrayList<ScrowLenderTransactionResponse>();
		try {
			if (objList != null && !objList.isEmpty()) {

				objList.forEach(e -> {

					try {
						ScrowLenderTransactionResponse oxyWalletresponse = new ScrowLenderTransactionResponse();
						oxyWalletresponse.setId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
						oxyWalletresponse.setUserId(Integer.parseInt(e[1] == null ? "0" : e[1].toString()));
						oxyWalletresponse.setScrowAccountNumber(e[2] == null ? "" : e[2].toString());
						oxyWalletresponse.setTransactionAmount(Integer.parseInt(e[3] == null ? "0" : e[3].toString()));
						oxyWalletresponse
								.setTransactionDate(expectedDateFormat.format(dateFormat.parse(e[4].toString())));
						oxyWalletresponse.setDocumentUploadedId(Integer.parseInt(e[5] == null ? "0" : e[5].toString()));
						oxyWalletresponse.setStatus(e[6] == null ? "" : e[6].toString());
						oxyWalletresponse.setFileName(e[7] == null ? "Image Not Uploaded" : e[7].toString());
						oxyWalletresponse.setFilePath(e[8] == null ? "" : e[8].toString());
						oxyWalletresponse.setDocumentType(e[9] == null ? "" : e[9].toString());
						oxyWalletresponse.setDocumentSubType(e[10] == null ? "" : e[10].toString());
						oxyWalletresponse.setFirstName(e[11] == null ? "" : e[11].toString());
						oxyWalletresponse.setLastName(e[12] == null ? "" : e[12].toString());
						oxyWalletresponse.setComments(e[13] == null ? "" : e[13].toString());

						response.add(oxyWalletresponse);

					} catch (Exception e1) {

						throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
					}
				});
			}
		} catch (Exception e) {

			throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
		}
		walletResponse.setResults(response);

		logger.info("---------------------getLenderWalletDetails method end ...............");
		return walletResponse;

	}

	@Override
	public UserResponse loanOfferAccepetence(UserRequest userRequest) {
		User user = userRepo.findById(userRequest.getId()).get();
		UserResponse userResponse = null;
		if (user != null) {
			LoanRequest loanRequest = loanRequestRepo.findByUserIdAndLoanRequestId(userRequest.getId(),
					userRequest.getLoanRequestId());
			if (loanRequest != null) {
				LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();
				if (loanOfferdAmount != null) {

					if (loanOfferdAmount.getLoanOfferdStatus() == LoanOfferdStatus.LOANOFFERACCEPTED
							|| loanOfferdAmount.getLoanOfferdStatus() == LoanOfferdStatus.LOANOFFERREJECTED) {
						throw new ActionNotAllowedException(
								"This Loan Offer Action already accepeted   " + loanOfferdAmount.getLoanofferedId(),
								ErrorCodes.ACTION_ALREDY_DONE);
					}
					if (userRequest.getLoanOfferStatus()
							.equalsIgnoreCase(LoanOfferdStatus.LOANOFFERACCEPTED.toString())) {

						if (user.getStatus() != com.oxyloans.entity.user.User.Status.ACTIVE) {
							user.setStatus(com.oxyloans.entity.user.User.Status.ACTIVE);
							userRepo.save(user);
						}
						loanOfferdAmount.setAccepetedOn(new Date());
						loanOfferdAmount.setLoanOfferdStatus(LoanOfferdStatus.LOANOFFERACCEPTED);
						loanRequest.setLoanOfferedAmount(loanOfferdAmount);
						loanRequestRepo.save(loanRequest);
						OxyUserType oxyUserType = oxyUserTypeRepo
								.getDetailsByUtmNameAndType(user.getUrchinTrackingModule());
						if (oxyUserType != null) {
							Date currentDate = new Date();
							String lineSeparator = System.lineSeparator();

							String firstMessage = "Dear Team," + lineSeparator + "Please note that "
									+ user.getPersonalDetails().getFirstName() + "( BR" + user.getId() + " )"
									+ " has accepted the offer " + lineSeparator + "sent by Partner "
									+ oxyUserType.getUtmName() + "on " + expectedDateFormat.format(currentDate)
									+ ".The application id is " + loanRequest.getLoanRequestId() + lineSeparator
									+ "Thanks," + lineSeparator + "Oxyloans Team";
							String secondMessage = "Dear" + oxyUserType.getUtmName() + "," + lineSeparator
									+ "Please note that " + user.getPersonalDetails().getFirstName() + "( BR"
									+ user.getId() + " )" + " has accepted the offer " + lineSeparator
									+ "sent by  you on " + expectedDateFormat.format(currentDate)
									+ ".The application id is " + loanRequest.getLoanRequestId() + lineSeparator
									+ "Thanks," + lineSeparator + "Oxyloans Team";
							if (oxyUserType.getMobileNumber() != null) {

								whatappService.partnerActionUsingUltra("919492902990", firstMessage,
										"91" + oxyUserType.getMobileNumber(), secondMessage);
							} else {
								whatappService.sendingIndividualMessageUsingUltraApi("919492902990", firstMessage);
							}
						}
						OxyUserType userType = oxyUserTypeRepo
								.getDetailsByUtmNameAndType(user.getUrchinTrackingModule());
						if (userType != null) {
							try {
								partnerService.partnerBasedAgreementsGeneration(userRequest.getId());
							} catch (PdfGeenrationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						userResponse = new UserResponse();
						userResponse.setId(user.getId());
						return userResponse;

					} else {
						if (user.getStatus() != com.oxyloans.entity.user.User.Status.ACTIVE) {
							user.setStatus(com.oxyloans.entity.user.User.Status.ACTIVE);
							user.setAdminComments(null);
							userRepo.save(user);
						}
						loanRequest.setParentRequestId(0);
						loanOfferdAmount.setAccepetedOn(new Date());
						loanOfferdAmount.setLoanOfferdStatus(LoanOfferdStatus.LOANOFFERREJECTED);
						loanRequest.setLoanOfferedAmount(loanOfferdAmount);
						user.setAdminComments(null);
						userRepo.save(user);
						loanRequestRepo.save(loanRequest);
						OxyUserType oxyUserType = oxyUserTypeRepo
								.getDetailsByUtmNameAndType(user.getUrchinTrackingModule());
						if (oxyUserType != null) {
							Date currentDate = new Date();
							String lineSeparator = System.lineSeparator();

							String firstMessage = "Dear Team," + lineSeparator + "Please note that "
									+ user.getPersonalDetails().getFirstName() + "( BR" + user.getId() + " )"
									+ " has rejected the offer " + lineSeparator + "sent by Partner "
									+ oxyUserType.getUtmName() + "on " + expectedDateFormat.format(currentDate)
									+ ".The application id is " + loanRequest.getLoanRequestId() + lineSeparator
									+ "Thanks," + lineSeparator + "Oxyloans Team";
							String secondMessage = "Dear" + oxyUserType.getUtmName() + "," + lineSeparator
									+ "Please note that " + user.getPersonalDetails().getFirstName() + "( BR"
									+ user.getId() + " )" + " has rejected the offer " + lineSeparator
									+ "sent by  you on " + expectedDateFormat.format(currentDate)
									+ ".The application id is " + loanRequest.getLoanRequestId() + lineSeparator
									+ "Thanks," + lineSeparator + "Oxyloans Team";
							if (oxyUserType.getMobileNumber() != null) {
								whatappService.partnerActionUsingUltra("919492902990", firstMessage,
										"91" + oxyUserType.getMobileNumber(), secondMessage);
							} else {
								whatappService.sendingIndividualMessageUsingUltraApi("919492902990", firstMessage);
							}
						}
						userResponse = new UserResponse();
						userResponse.setId(user.getId());
						return userResponse;

					}

				} else {
					throw new OperationNotAllowedException("offer not sent", ErrorCodes.ENITITY_NOT_FOUND);
				}
			} else {
				throw new OperationNotAllowedException("User is not rised loanrequest ", ErrorCodes.LOAN_REQUEST_NULL);
			}
		} else {
			throw new OperationNotAllowedException("User is not therer", ErrorCodes.USER_NOT_FOUND);

		}

	}

	public ScrowLenderTransactionResponse approveLenderWalletTransaction(int documnetId) {
		logger.info("---------------------approveLenderWalletTransaction method start ...............");
		LenderOxyWallet oxyWallet = lenderOxyWalletRepo.findById(documnetId);
		LenderOxyWallet oxyWalletresponse = new LenderOxyWallet();
		try {
			oxyWallet.setStatus(LenderOxyWallet.Status.APPROVED);
			oxyWallet.setCreatedBy("Admin");
			oxyWalletresponse = lenderOxyWalletRepo.save(oxyWallet);
			if (oxyWalletresponse.getUserId() != 0) {
				User user = userRepo.findById(oxyWalletresponse.getUserId()).get();
				;
				TemplateContext templateContext = new TemplateContext();
				EmailRequest emailRequest = new EmailRequest(new String[] { user.getEmail() }, mailsubject,
						"lenderwalletinfotolender.template", templateContext);
				EmailResponse emailResponse = emailService.sendEmail(emailRequest);
				if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
					throw new MessagingException(emailResponse.getErrorMessage()); // TODO need to fill this property
				}
			}

		} catch (Exception e) {

			throw new DataFormatException("Error While Saving the Data", ErrorCodes.DATA_PERSIST_ERROR);
		}
		ScrowLenderTransactionResponse response = new ScrowLenderTransactionResponse();
		response.setUserId(oxyWalletresponse.getUserId());
		response.setScrowAccountNumber(oxyWalletresponse.getScrowAccountNumber());
		response.setTransactionAmount(oxyWalletresponse.getTransactionAmount());
		// response.setDocumentUploadedId(oxyWalletresponse.getDocumentUploadedId());
		response.setStatus("" + oxyWalletresponse.getStatus());
		response.setTransactionDate(expectedDateFormat.format(oxyWalletresponse.getTransactionDate()));
		response.setCreatedBy("" + oxyWalletresponse.getUserId());
		logger.info("---------------------approveLenderWalletTransaction method end ...............");
		return response;

	}

	@Override
	public ScrowWalletResponse searchlenderwallet(ScrowTransactionRequest scrowTransactionRequest, PageDto pageDto) {
		logger.info("---------------------searchlenderwallet method start ...............");
		ScrowWalletResponse walletResponse = new ScrowWalletResponse();
		List<Object[]> objList = lenderOxyWalletNativeRepo.searchLenderWalletDetails(
				scrowTransactionRequest.getFirstName(), scrowTransactionRequest.getLastName(), pageDto.getPageSize(),
				pageDto.getPageNo());
		Integer totalCount1 = 0;
		totalCount1 = lenderOxyWalletNativeRepo.countTranRecords(scrowTransactionRequest.getFirstName(),
				scrowTransactionRequest.getLastName());
		walletResponse.setTotalCount(totalCount1.intValue());
		List<ScrowLenderTransactionResponse> response = new ArrayList<ScrowLenderTransactionResponse>();
		try {
			if (objList != null && !objList.isEmpty()) {

				objList.forEach(e -> {

					try {
						ScrowLenderTransactionResponse oxyWalletresponse = new ScrowLenderTransactionResponse();
						oxyWalletresponse.setId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
						oxyWalletresponse.setUserId(Integer.parseInt(e[1] == null ? "0" : e[1].toString()));
						oxyWalletresponse.setScrowAccountNumber(e[2] == null ? "" : e[2].toString());
						oxyWalletresponse.setTransactionAmount(Integer.parseInt(e[3] == null ? "0" : e[3].toString()));
						oxyWalletresponse
								.setTransactionDate(expectedDateFormat.format(dateFormat.parse(e[4].toString())));
						oxyWalletresponse.setDocumentUploadedId(Integer.parseInt(e[5] == null ? "0" : e[5].toString()));
						oxyWalletresponse.setStatus(e[6] == null ? "" : e[6].toString());
						oxyWalletresponse.setFileName(e[7] == null ? "Image Not Uploaded" : e[7].toString());
						oxyWalletresponse.setFilePath(e[8] == null ? "" : e[8].toString());
						oxyWalletresponse.setDocumentType(e[9] == null ? "" : e[9].toString());
						oxyWalletresponse.setDocumentSubType(e[10] == null ? "" : e[10].toString());
						oxyWalletresponse.setFirstName(e[11] == null ? "" : e[11].toString());
						oxyWalletresponse.setLastName(e[12] == null ? "" : e[12].toString());
						oxyWalletresponse.setComments(e[13] == null ? "" : e[13].toString());

						response.add(oxyWalletresponse);

					} catch (Exception e1) {

						throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
					}
				});
			}
		} catch (Exception e) {

			throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
		}
		walletResponse.setResults(response);

		logger.info("---------------------searchlenderwallet method end ...............");
		return walletResponse;

	}

	public KycFileResponse generateCICReport() {

		logger.info("---------------------generateCICReport method start ...............");
		List<Object[]> objList = lenderOxyWalletNativeRepo.generateCICReport();

		KycFileResponse fileResponse = new KycFileResponse();
		ByteArrayOutputStream output = null;
		FileOutputStream outFile = null;
		try {
			output = new ByteArrayOutputStream();
			HSSFRow row = null;
			HSSFCellStyle yellowStyle = null;
			HSSFFont headerFont = null;
			HSSFCell cell0 = null;
			HSSFCell cell1 = null;
			HSSFWorkbook wb = new HSSFWorkbook();
			HSSFSheet sheet = wb.createSheet("CIC");
			int index = 0;
			row = sheet.createRow((short) index);
			headerFont = wb.createFont();
			headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			headerFont.setFontHeightInPoints((short) 10);
			headerFont.setColor(HSSFColor.BLACK.index);

			yellowStyle = wb.createCellStyle();
			yellowStyle.setFont(headerFont);
			yellowStyle.setBorderTop((short) 1); // double lines border
			yellowStyle.setBorderRight((short) 1);
			yellowStyle.setBorderBottom((short) 1);
			yellowStyle.setBorderLeft((short) 1);
			yellowStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
			yellowStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
			yellowStyle.setWrapText(true);

			cell0 = row.createCell(0);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CONSUMER_NAME));
			cell0 = row.createCell(1);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DATE_OF_BIRTH));
			cell0 = row.createCell(2);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.GENDER));
			cell0 = row.createCell(3);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.INCOME_TAX_ID_NUMBER));
			cell0 = row.createCell(4);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PASSPORT_NUMBER));
			cell0 = row.createCell(5);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PASSPORT_ISSUE_DATE));
			cell0 = row.createCell(6);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PASSPORT_EXPIRY_DATE));
			cell0 = row.createCell(7);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.VOTER_ID_NUMBER));
			cell0 = row.createCell(8);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DRIVING_LICENSE_NUMBER));
			cell0 = row.createCell(9);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DRIVING_LICENSE_ISSUE_DATE));
			cell0 = row.createCell(10);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DRIVING_LICENSE_EXPIRY_DATE));
			cell0 = row.createCell(11);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.RATION_CARD_NUMBER));
			cell0 = row.createCell(12);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.UNIVERSAL_ID_NUMBER));
			cell0 = row.createCell(13);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ADDITIONAL_ID_1));
			cell0 = row.createCell(14);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ADDITIONAL_ID_2));
			cell0 = row.createCell(15);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.TELEPHONE_NUMBER_MOBILE));
			cell0 = row.createCell(16);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.TELEPHONE_NUMBER_RESIDENCE));
			cell0 = row.createCell(17);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.TELEPHONE_NUMBER_OFFICE));
			cell0 = row.createCell(18);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.EXTENSION_OFFICE));
			cell0 = row.createCell(19);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.TELEPHONE_NUMBER_OTHER));
			cell0 = row.createCell(20);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.EXTENSION_OTHER));
			cell0 = row.createCell(21);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.EMAIL_ID_1));
			cell0 = row.createCell(22);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.EMAIL_ID_2));
			cell0 = row.createCell(23);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PERMANANT_ADD));

			cell0 = row.createCell(24);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.STATE_CODE_1));

			cell0 = row.createCell(25);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PIN_CODE_1));

			cell0 = row.createCell(26);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ADDRESS_CATEGORY_1));

			cell0 = row.createCell(27);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.RESIDENCE_CODE_1));

			cell0 = row.createCell(28);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CORRESPONDING_ADD));

			cell0 = row.createCell(29);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.STATE_CODE_2));

			cell0 = row.createCell(30);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PIN_CODE_2));

			cell0 = row.createCell(31);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ADDRESS_CATEGORY_2));

			cell0 = row.createCell(32);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.RESIDENCE_CATEGORY_2));

			cell0 = row.createCell(33);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CURRENT_NEW_MEMBER_CODE));

			cell0 = row.createCell(34);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CURRENT_NEW_MEMBER_SHORT_NAME));

			cell0 = row.createCell(35);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CURRENT_NEW_ACCOUNT_NUMBER));

			cell0 = row.createCell(36);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ACCOUNT_TYPE));

			cell0 = row.createCell(37);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OWNERSHIP_INDICATOR));

			cell0 = row.createCell(38);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DATE_OPENDED_DISBURSED));

			cell1 = row.createCell(39);
			cell1.setCellStyle(yellowStyle);
			cell1.setCellValue(new HSSFRichTextString(CICReportConstants.DATE_OF_LAST_PAYMENT));

			cell0 = row.createCell(40);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DATE_CLOSED));

			cell0 = row.createCell(41);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DATE_REPORTED));

			cell0 = row.createCell(42);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.HIGH_CREDIT_SANCTIONED_AMOUNT));

			cell0 = row.createCell(43);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CURRENT_BALANCE));

			cell0 = row.createCell(44);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.AMOUNT_OVERDUE));

			cell0 = row.createCell(45);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.NUMBER_OF_DAYS_PAST_DUE));

			cell0 = row.createCell(46);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OLD_MBR_CODE));

			cell0 = row.createCell(47);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OLD_MBR_SHORT_NAME));

			cell0 = row.createCell(48);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OLD_ACCOUNT_NUMBER));

			cell0 = row.createCell(49);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OLD_ACCOUNT_TYPE));

			cell0 = row.createCell(50);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OLD_OWNERSHIP_INDICATOR));

			cell0 = row.createCell(51);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.SUIT_FILED_WILFUL_DEFAULT));

			cell0 = row.createCell(52);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.WRITTEN_OFF_AND_SETTLED_STATUS));

			cell0 = row.createCell(53);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ASSET_CLASSIFICATION));

			cell0 = row.createCell(54);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.VALUE_OF_COLLATERAL));

			cell0 = row.createCell(55);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.TYPE_OF_COLLATERAL));

			cell0 = row.createCell(56);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CREDIT_LIMIT));

			cell0 = row.createCell(57);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CASH_LIMIT));

			cell0 = row.createCell(58);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.RATE_OF_INTEREST));

			cell0 = row.createCell(59);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.REPAYMENTTENURE));

			cell0 = row.createCell(60);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.EMI_AMOUNT));

			cell0 = row.createCell(61);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.WRITTEN_OFF_AMOUNT_TOTAL));

			cell0 = row.createCell(62);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.WRITTEN_OFF_PRINICIPAL_AMOUNT));

			cell0 = row.createCell(63);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.SETTLEMENT_AMOUNT));

			cell0 = row.createCell(64);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PAYMENT_FREEQUENCY));

			cell0 = row.createCell(65);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ACTUAL_PAYMENT_AMOUNT));

			cell0 = row.createCell(66);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OCCUPATION_CODE));

			cell0 = row.createCell(67);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.INCOME));

			cell0 = row.createCell(68);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.NET_GROSS_INCOME_INDICATOR));

			cell0 = row.createCell(69);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.MONTHLY_ANNUAL_INCOME_INDICATOR));

			if (objList != null && !objList.isEmpty()) {
				index++;
				for (Object[] obj : objList) {

					try {
						HSSFCell cell01 = null;
						HSSFCell cell11 = null;
						HSSFRow rows = sheet.createRow((short) index);
						cell01 = rows.createCell(0);
						cell01.setCellValue(new HSSFRichTextString(obj[2] == null ? "" : obj[2].toString()));
						cell01 = rows.createCell(1);
						cell01.setCellValue(new HSSFRichTextString(obj[3] == null ? "" : obj[3].toString()));
						cell01 = rows.createCell(2);
						cell01.setCellValue(new HSSFRichTextString(obj[4] == null ? "" : obj[4].toString()));
						cell01 = rows.createCell(3);
						cell01.setCellValue(new HSSFRichTextString(obj[5] == null ? "" : obj[5].toString()));
						cell01 = rows.createCell(4);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(5);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(6);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(7);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(8);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(9);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(10);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(11);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(12);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(13);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(14);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(15);
						cell01.setCellValue(new HSSFRichTextString(obj[6] == null ? "" : obj[6].toString()));
						cell01 = rows.createCell(16);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(17);
						cell01.setCellValue(new HSSFRichTextString(obj[7] == null ? "" : obj[7].toString()));
						cell01 = rows.createCell(18);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(19);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(20);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(21);
						cell01.setCellValue(new HSSFRichTextString(obj[8] == null ? "" : obj[8].toString()));
						cell01 = rows.createCell(22);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(23);
						cell01.setCellValue(new HSSFRichTextString(obj[9] == null ? "" : obj[9].toString()));

						cell01 = rows.createCell(24);
						cell01.setCellValue(new HSSFRichTextString(obj[10] == null ? "" : obj[10].toString()));

						cell01 = rows.createCell(25);
						cell01.setCellValue(new HSSFRichTextString(obj[11] == null ? "" : obj[11].toString()));

						cell01 = rows.createCell(26);
						cell01.setCellValue(new HSSFRichTextString(obj[12] == null ? "" : obj[12].toString()));

						cell01 = rows.createCell(27);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(28);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(29);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(30);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(31);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(32);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(33);
						cell01.setCellValue(new HSSFRichTextString("036FP00239"));

						cell01 = rows.createCell(34);
						cell01.setCellValue(new HSSFRichTextString("OXYLOANS"));

						cell01 = rows.createCell(35);
						cell01.setCellValue(new HSSFRichTextString(obj[13] == null ? "" : obj[13].toString()));

						cell01 = rows.createCell(36);
						cell01.setCellValue(new HSSFRichTextString("05"));

						cell01 = rows.createCell(37);
						cell01.setCellValue(new HSSFRichTextString("1"));

						cell01 = rows.createCell(38);
						cell01.setCellValue(new HSSFRichTextString(obj[14] == null ? "" : obj[14].toString()));

						cell11 = rows.createCell(39);
						cell11.setCellValue(new HSSFRichTextString(obj[15] == null ? "" : obj[15].toString()));

						cell01 = rows.createCell(40);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(41);
						cell01.setCellValue(new HSSFRichTextString(obj[16] == null ? "" : obj[16].toString()));

						cell01 = rows.createCell(42);
						cell01.setCellValue(new HSSFRichTextString(obj[17] == null ? "" : obj[17].toString()));

						cell01 = rows.createCell(43);
						cell01.setCellValue(new HSSFRichTextString(
								obj[18] == null ? obj[17] == null ? "" : obj[17].toString() : obj[18].toString()));

						cell01 = rows.createCell(44);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(45);
						cell01.setCellValue(new HSSFRichTextString(obj[21] == null ? "" : obj[21].toString()));

						cell01 = rows.createCell(46);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(47);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(48);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(49);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(50);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(51);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(52);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(53);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(54);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(55);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(56);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(57);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(58);
						cell01.setCellValue(new HSSFRichTextString(obj[19] == null ? "" : obj[19].toString()));

						cell01 = rows.createCell(59);
						cell01.setCellValue(new HSSFRichTextString(obj[20] == null ? "" : obj[20].toString()));

						cell01 = rows.createCell(60);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(61);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(62);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(63);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(64);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(65);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(66);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(67);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(68);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(69);
						cell01.setCellValue(new HSSFRichTextString(""));

						index++;
					} catch (Exception e1) {

						throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
					}
				}
			}

			for (int i = 0; i <= 69; i++) {
				sheet.autoSizeColumn(i);
			}
			wb.write(output);

			FileRequest fileRequest = new FileRequest();
			KycFileRequest kyc = new KycFileRequest();
			InputStream fileStream = new ByteArrayInputStream(output.toByteArray());

			fileRequest.setInputStream(fileStream);
			kyc.setFileName("cic_report.xlsx");
			fileRequest.setUserId(01234);
			fileRequest.setFileName(kyc.getFileName());
			fileRequest.setFileType(FileType.Agreement);
			fileRequest.setFilePrifix(FileType.Agreement.name());
			fileManagementService.putFile(fileRequest);
			FileResponse file = fileManagementService.getFile(fileRequest);

			fileResponse.setDownloadUrl(file.getDownloadUrl());
			fileResponse.setFileName("cic_report.xlsx");
			fileResponse.setKycType(KycType.CICREPORT);

			boolean deleted = fileManagementService.deleteFile(fileRequest);

		} catch (Exception e) {
			logger.error("Exxception occured in generateCICReport CIC Report", e);

		} finally {
			try {
				if (outFile != null) {
					outFile.close();
				}

			} catch (Exception e) {
				logger.error("Exxception occured in generateCICReport CIC Report", e);

			}
		}
		return fileResponse;
	}

	public ScrowLenderTransactionResponse updateEscrowWalletTransaction(int documnetId,
			ScrowTransactionRequest scrowTransactionRequest) {
		logger.info("---------------------updateEscrowWalletTransaction method start ...............");
		LenderOxyWallet oxyWallet = lenderOxyWalletRepo.findById(documnetId);
		LenderOxyWallet oxyWalletresponse = new LenderOxyWallet();
		if (oxyWallet != null) {
			try {
				if (scrowTransactionRequest.getAdminName() != null
						&& !scrowTransactionRequest.getAdminName().isEmpty()) {

					if (scrowTransactionRequest.getStatus() != null && !scrowTransactionRequest.getStatus().isEmpty()) {
						if (scrowTransactionRequest.getStatus().equalsIgnoreCase("approved")) {
							oxyWallet.setStatus(LenderOxyWallet.Status.APPROVED);
						} else {
							oxyWallet.setStatus(LenderOxyWallet.Status.REJECTED);
						}
						oxyWallet.setComments(scrowTransactionRequest.getComments());
						oxyWallet.setRemarks(scrowTransactionRequest.getComments());
						oxyWallet.setCreatedBy("Admin");
						oxyWalletresponse = lenderOxyWalletRepo.save(oxyWallet);
						if (scrowTransactionRequest.getStatus().equalsIgnoreCase("approved")) {
							lenderWalletHistoryServiceRepo.lenderTransactionHistory(oxyWallet.getUserId(),
									oxyWallet.getTransactionAmount(), "CREDIT", "MW", 0);
						}
						if (oxyWalletresponse.getUserId() != 0
								&& scrowTransactionRequest.getStatus().equalsIgnoreCase("approved")) {

							User user = userRepo.findById(oxyWalletresponse.getUserId()).get();

							TemplateContext templateContext = new TemplateContext();
							templateContext.put("lenderName", user.getPersonalDetails().getFirstName() + " "
									+ user.getPersonalDetails().getLastName());
							templateContext.put("lenderId", user.getUniqueNumber());
							templateContext.put("lendingamount",
									BigDecimal.valueOf(oxyWallet.getTransactionAmount()).toBigInteger());

							EmailRequest emailRequest = new EmailRequest(
									new String[] { user.getEmail(), "archana.n@oxyloans.com" }, "Wallet Transaction",
									"lenderwalletinfotoadmin.template", templateContext);
							EmailResponse emailResponse = emailService.sendEmail(emailRequest);
							if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
								throw new MessagingException(emailResponse.getErrorMessage()); // TODO need to fill this
																								// property
							}

							DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

							String walletLoadDate = dateFormat.format(oxyWallet.getTransactionDate());

							String date = dateFormat.format(new Date());

							String messageToWtappTest = null;

							if (user.isTestUser()) {
								messageToWtappTest = " *This is a test user* ";
							}

							if (!user.isTestUser()) {
								String message = (messageToWtappTest != null ? messageToWtappTest : "")
										+ "This is to inform you that an amount of " + " Rs. "
										+ oxyWallet.getTransactionAmount() + " has been loaded to "
										+ oxyWallet.getScrowAccountNumber() + " wallet manually on " + walletLoadDate
										+ " and approved by admin " + scrowTransactionRequest.getAdminName() + " on "
										+ date + "." + "\n\n" + "*This is a system generated message.*";

								String groupChatId = "917702795895-1630419144@g.us";
								whatappService.sendingIndividualMessageUsingUltraApi(groupChatId, message);
							}
						}
					} else {
						throw new DataFormatException("Error While Saving the Data", ErrorCodes.DATA_PERSIST_ERROR);
					}
				} else {

					throw new ActionNotAllowedException("Admin Name is Mandatory.", ErrorCodes.USER_NOT_FOUND);
				}
			} catch (Exception e) {

				throw new DataFormatException("Error While Saving the Data", ErrorCodes.DATA_PERSIST_ERROR);
			}
		} else {
			throw new DataFormatException("Document Id " + documnetId + " not Found", ErrorCodes.DATA_PERSIST_ERROR);
		}
		ScrowLenderTransactionResponse response = new ScrowLenderTransactionResponse();
		response.setUserId(oxyWalletresponse.getUserId());
		response.setScrowAccountNumber(oxyWalletresponse.getScrowAccountNumber());
		response.setTransactionAmount(oxyWalletresponse.getTransactionAmount());
		response.setStatus("" + oxyWalletresponse.getStatus());
		response.setTransactionDate(expectedDateFormat.format(oxyWalletresponse.getTransactionDate()));
		response.setCreatedBy("" + oxyWalletresponse.getUserId());
		logger.info("---------------------updateEscrowWalletTransaction method end ...............");
		return response;

	}

	private UserProfileUrlsDto getUserUrlprofile(Integer userId) {
		User user = userRepo.findById(userId).get();
		if (user != null) {
			PersonalDetails personalDetails = user.getPersonalDetails();
			if (personalDetails != null) {
				UserProfileUrlsDto urlsDto = new UserProfileUrlsDto();
				urlsDto.setFaceBookUrl(personalDetails.getFacebookUrl());
				urlsDto.setLinkdinUrl(personalDetails.getLinkedinUrl());
				urlsDto.setTwitterUrl(personalDetails.getTwitterUrl());
				return urlsDto;
			}

		} else {
			throw new ActionNotAllowedException("User Not Exist", ErrorCodes.USER_NOT_FOUND);
		}
		return null;

	}

	@Override
	public SearchResultsDto<ApplicationResponseDto> loansByApplication(int userId, ScrowTransactionRequest dto1) {
		logger.info("---------------------loansByApplication method start ...............");
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.DOWN);
		List<Object[]> objList = loansByApplicationNativeRepo.loansByApplication(dto1.getFirstName(),
				dto1.getLastName(), dto1.getUserId(), dto1.getPage().getPageSize(), dto1.getPage().getPageNo());
		Integer totalCount1 = 0;
		totalCount1 = loansByApplicationNativeRepo.countTranRecords(dto1.getFirstName(), dto1.getLastName(),
				dto1.getUserId());
		SearchResultsDto<ApplicationResponseDto> results = new SearchResultsDto<ApplicationResponseDto>();
		List<ApplicationResponseDto> applicationResponses = new ArrayList<ApplicationResponseDto>();
		try {
			if (objList != null && !objList.isEmpty()) {

				results.setTotalCount(totalCount1);
				results.setPageNo(dto1.getPage().getPageNo());
				results.setPageCount(objList.size());

				objList.forEach(e -> {

					try {
						ApplicationResponseDto dto = new ApplicationResponseDto();
						dto.setApplicationId(e[0] == null ? "" : e[0].toString());
						dto.setDisbursmentAmount(Double.parseDouble(e[1] == null ? "0" : e[1].toString()));
						dto.setBorrowerUserId(Integer.parseInt(e[2] == null ? "0" : e[2].toString()));
						dto.setDisbursedDate(expectedDateFormat.format(dateFormat1.parse(e[3].toString())));
						dto.setParentId(e[4] == null ? "0" : e[4].toString());
						User borrowerUser = userRepo.findById(dto.getBorrowerUserId()).get();

						List<Object[]> objList1 = loansByApplicationNativeRepo
								.emiAndPenaltyDetails(Integer.parseInt(dto.getParentId()));
						if (objList != null && !objList.isEmpty()) {
							double emiAmount = 0.0;
							double penaltyAmount = 0.0;
							for (Object[] obj1 : objList1) {
								emiAmount = emiAmount + Double.parseDouble(obj1[1] == null ? "0" : obj1[1].toString());
								penaltyAmount = penaltyAmount
										+ Double.parseDouble(obj1[2] == null ? "0" : obj1[2].toString());
							}

							dto.setEmiAmount(Double.parseDouble(df.format(emiAmount)));
							dto.setPenaltyAmount(Double.parseDouble(df.format(penaltyAmount)));
						}

						UserResponse user = new UserResponse();
						dto.setBorrowerUser(user);
						user.setId(borrowerUser.getId());
						userDetails(borrowerUser, user);
						applicationResponses.add(dto);

					} catch (Exception e1) {

						throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
					}
				});
			}
		} catch (Exception e) {

			throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
		}
		results.setResults(applicationResponses);

		logger.info("---------------------loansByApplication method end ...............");
		return results;

	}

	@Override
	public LoanResponseDto updEnachType(int userId, EnachTypeRequestDto enachTypeRequestDto) {
		logger.info("---------------------updEnachType method start ...............");
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		LoanRequest loanRequest = loanRequestRepo.findByLoanId(enachTypeRequestDto.getLoanId());
		if (loanRequest == null) {
			throw new NoSuchElementException("No Loan Id Present In the Data Base");
		} else {
			loanRequest.setEnachType(enachTypeRequestDto.getEnachType());
			LoanRequest request = loanRequestRepo.save(loanRequest);
			if (request == null) {
				throw new DataFormatException("Error While Saving the Data", ErrorCodes.DATA_PERSIST_ERROR);
			} else {

				loanResponseDto.setLoanId(request.getLoanId());
				loanResponseDto.setEnachType(request.getEnachType());
			}
		}
		logger.info("---------------------updEnachType method end ...............");
		return loanResponseDto;
	}

	protected void userDetails(User findById, UserResponse user) {
		if (findById.getPersonalDetails() != null) {
			user.setFirstName(findById.getPersonalDetails().getFirstName());
			user.setLastName(findById.getPersonalDetails().getLastName());
			user.setAddress(findById.getPersonalDetails().getAddress());

		}
		if (findById.getPersonalDetails().getFacebookUrl() != null) {
			user.setFacebookUrl(findById.getPersonalDetails().getFacebookUrl());
		}
		if (findById.getPersonalDetails().getTwitterUrl() != null) {
			user.setTwitterUrl(findById.getPersonalDetails().getTwitterUrl());
		}
		if (findById.getPersonalDetails().getLinkedinUrl() != null) {
			user.setLinkedinUrl(findById.getPersonalDetails().getLinkedinUrl());
		}
		user.setEmail(findById.getEmail());
		user.setMobileNumber(findById.getMobileNumber());
		user.setCity(findById.getCity());
		user.setPinCode(findById.getPinCode());
		user.setUtmSource(findById.getUrchinTrackingModule());
		user.setState(findById.getState());

		if (findById.getExperianSummary() != null && findById.getUserProfileRisk() != null) {
			user.setOxyScore(findById.getExperianSummary().getScore() + findById.getUserProfileRisk().getTotalScore());

		}
		if (findById.getProfessionalDetails() != null) {
			user.setCompanyName(findById.getProfessionalDetails().getCompanyName());
			user.setWorkExperience(findById.getProfessionalDetails().getWorkExperience());
		}
		if (findById.getFinancialDetails() != null) {
			user.setSalary(findById.getFinancialDetails().getNetMonthlyIncome());
		}
		if (findById.getBankDetails() != null) {
			user.setAccountNumber(findById.getBankDetails().getAccountNumber());
			user.setBankName(findById.getBankDetails().getBankName());
			user.setBranchName(findById.getBankDetails().getBranchName());
			user.setIfscCode(findById.getBankDetails().getIfscCode());
			user.setBankAddress(findById.getBankDetails().getAddress());
			user.setUserNameAccordingToBank(findById.getBankDetails().getUserName());

		}
		RiskProfileDto riskProfileDto = getReskProfileCalculation(findById.getId());
		user.setRiskProfileDto(riskProfileDto);
	}

	private RiskProfileDto getReskProfileCalculation(Integer userId) {

		RiskProfileDto riskProfileDto = new RiskProfileDto();
		User user = userRepo.findById(userId).get();
		UserProfileRisk userProfileRisk = user.getUserProfileRisk();
		if (ObjectUtils.allNotNull(user, userProfileRisk)) {
			riskProfileDto.setUserId(user.getId());
			riskProfileDto.setCibilScore(userProfileRisk.getCibilScore());
			riskProfileDto.setCompanyOrOrganization(userProfileRisk.getCompanyOrOrganization());
			riskProfileDto
					.setExperianceOrExistenceOfOrganization(userProfileRisk.getExperianceOrExistenceOfOrganization());
			riskProfileDto.setGrade(userProfileRisk.getGrade());
			riskProfileDto.setSalaryOrIncome(userProfileRisk.getSalaryOrIncome());
			return riskProfileDto;
		}
		return null;

	}

	@Override
	public UserResponse saveEanchMinimumWtdrwUsers(EnachMiniumWithDrwUserRequest request) {

		logger.info("---------------------saveEanchMinimumWtdrwUsers method start ...............");
		List<Object[]> objList = loansByApplicationNativeRepo.loansByApplication("", "", request.getBorrowerId(), 10,
				1);
		ArrayList<String> applicationList = new ArrayList<String>();
		try {
			if (objList != null && !objList.isEmpty()) {
				objList.forEach(e -> {
					applicationList.add(e[0] == null ? "" : e[0].toString());
				});

			}

		} catch (Exception e) {

			throw new DataFormatException("Error While getting the application details", ErrorCodes.DATA_PERSIST_ERROR);
		}

		if (applicationList != null && !applicationList.isEmpty()) {
			if (!applicationList.contains(request.getApplicationId())) {
				throw new DataFormatException("Apllication Id Not belongs to this borrower",
						ErrorCodes.NO_LOAN_FOUND_FOR_BORROWER);
			}
		} else {
			throw new DataFormatException("No Loans found for this borrower", ErrorCodes.NO_LOAN_FOUND_FOR_BORROWER);
		}

		EnachMinimumWithdrawUsers wthdrwUsers = enachMinimumWithdrawUsersRepo.findByBorrowerId(request.getBorrowerId());
		if (wthdrwUsers == null) {
			wthdrwUsers = new EnachMinimumWithdrawUsers();
			wthdrwUsers.setApplicationId(request.getApplicationId());
			wthdrwUsers.setBorrowerId(request.getBorrowerId());
			wthdrwUsers.setAmount(request.getAmount());
			wthdrwUsers.setCreatedBy("Admin");
			wthdrwUsers.setEmiDate(request.getEmiDate());
			wthdrwUsers.setTenure(request.getTenure());
			wthdrwUsers.setNoOfRuns(0);
		} else {
			wthdrwUsers.setApplicationId(request.getApplicationId());
			wthdrwUsers.setBorrowerId(request.getBorrowerId());
			wthdrwUsers.setAmount(request.getAmount());
			wthdrwUsers.setCreatedBy("Admin");
			wthdrwUsers.setEmiDate(request.getEmiDate());
			;
			wthdrwUsers.setTenure(request.getTenure());
			wthdrwUsers.setNoOfRuns(0);
		}
		UserResponse response = new UserResponse();

		try {
			wthdrwUsers = enachMinimumWithdrawUsersRepo.save(wthdrwUsers);
			if (wthdrwUsers == null) {
				throw new DataFormatException("Error While Saving the Data", ErrorCodes.DATA_PERSIST_ERROR);
			}

		} catch (Exception e) {

			throw new DataFormatException("Error While Saving the Data", ErrorCodes.DATA_PERSIST_ERROR);
		}
		logger.info("---------------------saveEanchMinimumWtdrwUsers method end ...............");
		response.setId(wthdrwUsers.getBorrowerId());
		return response;
	}

	@Override
	public SearchResultsDto<EnachMinimunWithdrawUsersDto> getEnachMinimumWithdrawUsers(PageDto pageDto) {

		logger.info("---------------------getEnachMinimumWithdrawUsers method start ...............");
		List<Object[]> objList = enachMinimumWithdrawUsersRepo.getEnachMinimumWithdrawUsers(pageDto.getPageSize(),
				pageDto.getPageNo());
		Integer totalCount1 = 0;
		totalCount1 = enachMinimumWithdrawUsersRepo.countTranRecords();
		SearchResultsDto<EnachMinimunWithdrawUsersDto> results = new SearchResultsDto<EnachMinimunWithdrawUsersDto>();
		List<EnachMinimunWithdrawUsersDto> applicationResponses = new ArrayList<EnachMinimunWithdrawUsersDto>();
		try {
			if (objList != null && !objList.isEmpty()) {

				results.setTotalCount(totalCount1);
				results.setPageNo(pageDto.getPageNo());
				results.setPageCount(objList.size());

				objList.forEach(e -> {

					try {
						EnachMinimunWithdrawUsersDto dto = new EnachMinimunWithdrawUsersDto();
						dto.setApplicationId(e[0] == null ? "" : e[0].toString());
						dto.setBorrowerId(Integer.parseInt(e[1] == null ? "0" : e[1].toString()));
						dto.setTenure(Integer.parseInt(e[2] == null ? "0" : e[2].toString()));
						dto.setAmount(Double.parseDouble(e[3] == null ? "0" : e[3].toString()));
						dto.setEmiDate(e[4] == null ? "0" : e[4].toString());
						dto.setCreatedOn(expectedDateFormat.format(dateFormat.parse(e[5].toString())));
						User borrowerUser = userRepo.findById(dto.getBorrowerId()).get();
						UserResponse user = new UserResponse();
						dto.setBorrowerUser(user);
						user.setId(borrowerUser.getId());
						userDetails(borrowerUser, user);
						applicationResponses.add(dto);

					} catch (Exception e1) {

						throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
					}
				});
			}
		} catch (Exception e) {

			throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
		}
		results.setResults(applicationResponses);

		logger.info("---------------------getEnachMinimumWithdrawUsers method end ...............");
		return results;

	}

	public SearchResultsDto<EnachMinimunWithdrawUsersDto> searchEnachMinimumWithdrawUsers(
			ScrowTransactionRequest searchDto) {

		logger.info("---------------------searchEnachMinimumWithdrawUsers method start ...............");
		List<Object[]> objList = enachMinimumWithdrawUsersRepo.searchEnachMinimumWithdrawUsers(searchDto.getFirstName(),
				searchDto.getLastName(), searchDto.getUserId(), searchDto.getPage().getPageSize(),
				searchDto.getPage().getPageNo());
		Integer totalCount1 = 0;
		totalCount1 = enachMinimumWithdrawUsersRepo.seacrhCountTranRecords(searchDto.getFirstName(),
				searchDto.getLastName(), searchDto.getUserId(), searchDto.getPage().getPageSize(),
				searchDto.getPage().getPageNo());
		SearchResultsDto<EnachMinimunWithdrawUsersDto> results = new SearchResultsDto<EnachMinimunWithdrawUsersDto>();
		List<EnachMinimunWithdrawUsersDto> applicationResponses = new ArrayList<EnachMinimunWithdrawUsersDto>();
		try {
			if (objList != null && !objList.isEmpty()) {

				results.setTotalCount(totalCount1);
				results.setPageNo(searchDto.getPage().getPageNo());
				results.setPageCount(objList.size());

				objList.forEach(e -> {

					try {
						EnachMinimunWithdrawUsersDto dto = new EnachMinimunWithdrawUsersDto();
						dto.setApplicationId(e[0] == null ? "" : e[0].toString());
						dto.setBorrowerId(Integer.parseInt(e[1] == null ? "0" : e[1].toString()));
						dto.setTenure(Integer.parseInt(e[2] == null ? "0" : e[2].toString()));
						dto.setAmount(Double.parseDouble(e[3] == null ? "0" : e[3].toString()));
						dto.setEmiDate(e[4] == null ? "0" : e[4].toString());
						dto.setCreatedOn(expectedDateFormat.format(dateFormat.parse(e[5].toString())));
						User borrowerUser = userRepo.findById(dto.getBorrowerId()).get();
						UserResponse user = new UserResponse();
						dto.setBorrowerUser(user);
						user.setId(borrowerUser.getId());
						userDetails(borrowerUser, user);
						applicationResponses.add(dto);

					} catch (Exception e1) {

						throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
					}
				});
			}
		} catch (Exception e) {

			throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
		}
		results.setResults(applicationResponses);

		logger.info("---------------------searchEnachMinimumWithdrawUsers method end ...............");
		return results;
	}

	@Override
	@Transactional
	public NomineeResponseDto nomineeDetails(NomineeRequestDto nomineeRequestDto) {

		User user = userRepo.findById(nomineeRequestDto.getUserId()).get();
		Nominee nominee = user.getNominee();
		if (nominee == null) {
			nominee = new Nominee();
			nominee.setUserId(nomineeRequestDto.getUserId());
			nominee.setEmail(nomineeRequestDto.getEmail());
			nominee.setMobileNumber(nomineeRequestDto.getMobileNumber());
			nominee.setName(nomineeRequestDto.getName());
			nominee.setRelation(nomineeRequestDto.getRelation());
			nominee.setAccountNumber(nomineeRequestDto.getAccountNumber());
			nominee.setBankName(nomineeRequestDto.getBankName());
			nominee.setBranchName(nomineeRequestDto.getBranchName());
			nominee.setIfscCode(nomineeRequestDto.getIfscCode());
			nominee.setCity(nomineeRequestDto.getCity());
			user.setNominee(nominee);
			nominee.setUser(user);
			user = userRepo.save(user);
		} else {
			if (nominee.getMobileNumber() != null || nominee.getMobileNumber() == null) {
				nominee.setMobileNumber(nomineeRequestDto.getMobileNumber());
			}

			if (nominee.getEmail() != null || nominee.getEmail() == null) {
				nominee.setEmail(nomineeRequestDto.getEmail());
			}
			if (nominee.getName() != null) {
				nominee.setName(nomineeRequestDto.getName());
			}
			if (nominee.getRelation() != null) {
				nominee.setRelation(nomineeRequestDto.getRelation());
			}

			if (nominee.getAccountNumber() != null) {
				nominee.setAccountNumber(nomineeRequestDto.getAccountNumber());
			}
			if (nominee.getBankName() != null) {
				nominee.setBankName(nomineeRequestDto.getBankName());
			}
			if (nominee.getBranchName() != null) {
				nominee.setBranchName(nomineeRequestDto.getBranchName());
			}
			if (nominee.getIfscCode() != null) {
				nominee.setIfscCode(nomineeRequestDto.getIfscCode());
			}
			if (nominee.getCity() != null) {
				nominee.setCity(nomineeRequestDto.getCity());
			}
			user.setNominee(nominee);
		}

		NomineeResponseDto nomineeResponseDto = new NomineeResponseDto();
		nomineeResponseDto.setUserId(nominee.getUserId());
		return nomineeResponseDto;
	}

	@Override
	public NomineeResponseDto getNominee(Integer id) {
		NomineeResponseDto nomineeResponseDto = new NomineeResponseDto();
		User user = userRepo.findById(id).get();
		Nominee nominee = user.getNominee();
		if (nominee == null) {
			throw new OperationNotAllowedException("user dont have nominee details", ErrorCodes.ENITITY_NOT_FOUND);
		} else {
			nomineeResponseDto.setEmial(nominee.getEmail());
			nomineeResponseDto.setMobileNumber(nominee.getMobileNumber());
			nomineeResponseDto.setUserId(nominee.getUserId());
			nomineeResponseDto.setName(nominee.getName());
			nomineeResponseDto.setRelation(nominee.getRelation());
			nomineeResponseDto.setAccountNumber(nominee.getAccountNumber());
			nomineeResponseDto.setBankName(nominee.getBankName());
			nomineeResponseDto.setBranchName(nominee.getBranchName());
			nomineeResponseDto.setIfscCode(nominee.getIfscCode());
			nomineeResponseDto.setCity(nominee.getCity());
			return nomineeResponseDto;
		}

	}

	public NotificationCountResponseDtoForBorrowerAndLender notificationDetails(int userId) {
		int totalCount = 0;
		int loanResponsePendingCount = 0;
		int esignPendingCount = 0;
		int disbursedmentPendingCount = 0;
		User user = userRepo.findById(userId).get();

		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
			if (loanRequest != null) {
				int borrowerId = loanRequest.getId();
				List<LoanRequest> loanRequest1 = loanRequestRepo.findByParentRequestId(borrowerId);
				for (LoanRequest loanRequestInformation : loanRequest1) {
					if (loanRequestInformation.getLoanStatus() == LoanStatus.Conversation) {
						loanResponsePendingCount = loanResponsePendingCount + 1;
					}
				}
			}
		} else {
			LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
			if (loanRequest != null) {
				int lenderId = loanRequest.getId();
				List<LoanRequest> loanRequest1 = loanRequestRepo.findByParentRequestId(lenderId);
				for (LoanRequest loanRequestInformation : loanRequest1) {
					if (loanRequestInformation.getLoanStatus() == LoanStatus.Conversation) {
						loanResponsePendingCount = loanResponsePendingCount + 1;
					}
				}
			}
		}

		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			List<OxyLoan> oxyLoan = oxyLoanRepo.findByBorrowerUserId(userId);
			for (OxyLoan oxyLoansInformation : oxyLoan) {
				if (oxyLoansInformation.getLoanStatus() == LoanStatus.Agreed) {
					esignPendingCount = esignPendingCount + 1;
				}
				if (oxyLoansInformation.getBorrowerDisbursedDate() == null) {
					disbursedmentPendingCount = disbursedmentPendingCount + 1;
				}
			}
		} else {
			List<OxyLoan> oxyLoan = oxyLoanRepo.findByLenderUserId(userId);
			for (OxyLoan oxyLoansInformation : oxyLoan) {
				if (oxyLoansInformation.getLoanStatus() == LoanStatus.Agreed) {
					esignPendingCount = esignPendingCount + 1;
				}
				if (oxyLoansInformation.getBorrowerDisbursedDate() == null) {
					disbursedmentPendingCount = disbursedmentPendingCount + 1;
				}
			}
		}

		totalCount = loanResponsePendingCount + esignPendingCount + disbursedmentPendingCount;
		NotificationCountResponseDtoForBorrowerAndLender notificationCountResponseDto = new NotificationCountResponseDtoForBorrowerAndLender();
		notificationCountResponseDto.setTotalCount(totalCount);
		notificationCountResponseDto.setLoanResponsePendingCount(loanResponsePendingCount);
		notificationCountResponseDto.setEsignPendingCount(esignPendingCount);
		notificationCountResponseDto.setDisbursedmentPendingCount(disbursedmentPendingCount);
		return notificationCountResponseDto;

	}

	@Override
	public int kycPendingSchedular() {
		Date currentDate = new Date();
		String dateFormate = expectedDateFormat.format(currentDate);
		String currentDatabaseFormate = new SimpleDateFormat("yyyy-MM-dd").format(currentDate);

		List<Object[]> borrowerDetails = userRepo.getkycPendingBorrowers();
		if (borrowerDetails != null && !borrowerDetails.isEmpty()) {

			Iterator it = borrowerDetails.iterator();
			while (it.hasNext()) {
				Object e[] = (Object[]) it.next();

				String registeredDate = (e[1] == null ? " " : e[1].toString());

				Date date = null;
				try {
					date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(registeredDate);
				} catch (ParseException e1) {

					e1.printStackTrace();
				}
				String newString = new SimpleDateFormat("h:mm").format(date); // 9:00
				String dateParts[] = newString.split(":");

				int registeredTime = Integer.parseInt(dateParts[0]);

				String splitingForDate[] = registeredDate.split(" ");
				String databaseFormate = (splitingForDate[0]);

				int differenceInTime = currentDate.getHours() - registeredTime;
				if (currentDatabaseFormate.equalsIgnoreCase(databaseFormate)) {
					if (differenceInTime == 2) {

						String borrowerFirstName = (e[2] == null ? " " : e[2].toString());
						String borrowerLastName = (e[3] == null ? " " : e[3].toString());
						String email = (e[0] == null ? " " : e[0].toString());
						int id = Integer.parseInt(e[4] == null ? "0" : e[4].toString());
						User user = userRepo.findById(id).get();
						TemplateContext context = new TemplateContext();
						context.put("BorrowerName", borrowerFirstName + " " + borrowerLastName);
						context.put("CurrentDate", dateFormate);

						String mailsubject = "Kyc Pending";
						String emailTemplateName = "borrower-kycpending.template";
						if (!utm.equals(user.getUrchinTrackingModule())) {
							EmailRequest emailRequest = new EmailRequest(new String[] { email }, mailsubject,
									emailTemplateName, context);
							EmailResponse emailResponse = emailService.sendEmail(emailRequest);
							if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
								throw new RuntimeException(emailResponse.getErrorMessage());
							}
						}

					}
				}
			}

		}
		List<Object[]> lenderDetails = userRepo.getkycPendingLenders();
		if (lenderDetails != null && !lenderDetails.isEmpty()) {
			Iterator it1 = lenderDetails.iterator();
			while (it1.hasNext()) {
				Object e[] = (Object[]) it1.next();
				String registeredDate1 = (e[1] == null ? " " : e[1].toString());

				Date date1 = null;
				try {
					date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(registeredDate1);
				} catch (ParseException e1) {

					e1.printStackTrace();
				}
				String newString1 = new SimpleDateFormat("h:mm").format(date1); // 9:00
				String dateParts1[] = newString1.split(":");
				int registeredTime1 = Integer.parseInt(dateParts1[0]);
				int differenceInTime = currentDate.getHours() - registeredTime1;

				String splitingForDate[] = registeredDate1.split(" ");
				String databaseFormate = (splitingForDate[0]);

				if (currentDatabaseFormate.equalsIgnoreCase(databaseFormate)) {
					if (differenceInTime == 2) {

						String email = (e[0] == null ? " " : e[0].toString());

						String lenderFirstName = (e[2] == null ? " " : e[2].toString());
						String lenderLastName = (e[3] == null ? " " : e[3].toString());

						TemplateContext context = new TemplateContext();
						context.put("LenderName", lenderFirstName + " " + lenderLastName);
						context.put("CurrentDate", dateFormate);

						String mailsubject = "Kyc Pending";
						String emailTemplateName = "lender-kycpending.template";

						EmailRequest emailRequest = new EmailRequest(new String[] { email }, mailsubject,
								emailTemplateName, context);
						EmailResponse emailResponse = emailService.sendEmail(emailRequest);
						if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
							throw new RuntimeException(emailResponse.getErrorMessage());
						}

					}
				}
			}

		}

		return 1;
	}

	@Override
	public UserResponse registractionForOxyfarms(UserRequest userRequest) {
		User user = new User();

		user.setMobileNumber(userRequest.getMobileNumber());
		User userVerificationByMobile = userRepo.findByMobileNumber(userRequest.getMobileNumber());
		if (userVerificationByMobile != null) {
			throw new EntityAlreadyExistsException("User " + userRequest.getMobileNumber() + " already register",
					ErrorCodes.ENTITY_ALREDY_EXISTS);
		}
		user.setUrchinTrackingModule("oxyfarms");
		user.setPrimaryType(PrimaryType.BORROWER);
		user.setMobileNumberVerified(userRequest.getMobileNumber() != null ? true : false);
		user.setSalt(UUID.randomUUID().toString() + UUID.randomUUID().toString().substring(0, 10));
		user.setEmail(userRequest.getEmail());
		User userVerificationByEmail = userRepo.findByEmailIgnoreCase(userRequest.getEmail());
		if (userVerificationByEmail != null) {
			throw new EntityAlreadyExistsException("User " + userRequest.getEmail() + " already register",
					ErrorCodes.ENTITY_ALREDY_EXISTS);
		}
		user.setEmailVerified(true);
		user.setEmailSent(true);
		String hashedpassword = signatureService.hashPassword("oxyfarms", user.getSalt());
		user.setPassword(hashedpassword);

		user.setPinCode(userRequest.getPinCode());
		user.setLocality(userRequest.getLocality());
		user.setCity(userRequest.getCity());
		user.setState(userRequest.getState());
		Random random = new Random();
		String id = String.format("%04d", random.nextInt(9999));
		user.setMobileOtpSession(id);

		user.setUniqueNumber(new Long(System.currentTimeMillis()).toString().substring(0, 10));
		StringBuilder uniqueNumber = new StringBuilder();
		uniqueNumber.append("BR1");

		user.setUniqueNumber(uniqueNumber.toString());
		userRepo.save(user);

		int borrowerId = user.getId();
		User userInfo = userRepo.findById(borrowerId).get();

		PersonalDetails personalDetails = user.getPersonalDetails();
		if (userInfo.getPersonalDetails() == null) {
			PersonalDetails personalDetailsDto = new PersonalDetails();

			personalDetailsDto.setFirstName(userRequest.getFirstName());
			personalDetailsDto.setLastName(userRequest.getLastName());
			try {
				personalDetailsDto.setDob(expectedDateFormat.parse(userRequest.getDob()));
			} catch (ParseException e) {

				e.printStackTrace();
			}
			personalDetailsDto.setCreatedAt(new Date());
			personalDetailsDto.setGender(userRequest.getGender());

			user.setPersonalDetails(personalDetailsDto);
			personalDetailsDto.setUser(user);
			user = userRepo.save(user);
		}

		LoanRequestDto loanRequestDto = new LoanRequestDto();
		loanRequestDto.setDuration(6);
		loanRequestDto.setLoanPurpose("Initiated the Request");
		loanRequestDto.setLoanRequestAmount(userRequest.getAmountInterested());
		loanRequestDto.setRateOfInterest(0d);
		loanRequestDto.setRepaymentMethod(RepaymentMethod.PI.name());
		Calendar expectedDate = Calendar.getInstance();
		expectedDate.add(Calendar.DATE, 1);
		loanRequestDto.setExpectedDate(expectedDateFormat.format(expectedDate.getTime()));

		PasswordGrantAccessToken accessToken = new PasswordGrantAccessToken(6000, EncodingAlgorithm.RSA);
		accessToken.setUserId(user.getId().toString());
		ThreadLocalSecurityProvider.setAccessToken(accessToken);
		LoanResponseDto loanRequest = loanServiceFactory.getService(user.getPrimaryType().name().toUpperCase())
				.loanRequest(user.getId(), loanRequestDto);

		user.setPersonalDetails(personalDetails);

		UserResponse userResponse = new UserResponse();
		userResponse.setMobileNumber(user.getMobileNumber());
		uniqueNumber.append(StringUtils.leftPad(user.getId().toString(), 6, "0"));
		user.setUniqueNumber(uniqueNumber.toString());
		userRepo.save(user);
		return userResponse;
	}

	@Override
	public KycFileResponse uploadContactByIos(int userId, ContactDetailsForIos contactDetailsForIos)
			throws IOException {
		KycFileResponse fileResponse = new KycFileResponse();
		if (contactDetailsForIos.getContactDetails() != null) {
			User user = userRepo.findById(userId).get();

			if (user != null) {
				UserDocumentStatus userDocumentStatus = userDocumentStatusRepo.findByUserIdAndDocumentSubType(userId,
						KycType.CONTACTS.toString());
				if (userDocumentStatus == null) {
					UserDocumentStatus userDocumentStatusUpdate = new UserDocumentStatus();
					userDocumentStatusUpdate.setUserId(userId);
					userDocumentStatusUpdate.setDocumentType(DocumentType.Kyc);
					userDocumentStatusUpdate.setDocumentSubType("CONTACTS");
					userDocumentStatusUpdate.setFilePath("");
					userDocumentStatusUpdate.setFileName("Agreement_contact.xls");
					userDocumentStatusUpdate.setStatus(Status.UPLOADED);
					userDocumentStatusRepo.save(userDocumentStatusUpdate);

				}

				String contactDetails = contactDetailsForIos.getContactDetails();
				String[] splitingValues = contactDetails.split(",");

				ByteArrayOutputStream output = null;
				FileOutputStream outFile = null;
				output = new ByteArrayOutputStream();
				HSSFRow row = null;
				HSSFCellStyle yellowStyle = null;
				HSSFFont headerFont = null;
				HSSFCell cell0 = null;
				HSSFCell cell1 = null;
				HSSFWorkbook wb = new HSSFWorkbook();
				HSSFSheet sheet = wb.createSheet("IOS");
				int index = 0;
				row = sheet.createRow((short) index);
				try {
					cell0 = row.createCell(0);
					cell0.setCellStyle(yellowStyle);
					cell0.setCellValue(new HSSFRichTextString("S.No"));
					cell0 = row.createCell(1);
					cell0.setCellStyle(yellowStyle);
					cell0.setCellValue(new HSSFRichTextString("MobileNumber"));
					index++;
					for (int i = 0; i < splitingValues.length; i++) {

						HSSFCell cell01 = null;
						HSSFCell cell11 = null;
						HSSFRow rows = sheet.createRow((short) index);
						cell01 = rows.createCell(0);
						cell01.setCellValue(i);
						cell01 = rows.createCell(1);
						cell01.setCellValue(new HSSFRichTextString(splitingValues[i]));
						index++;
					}
					for (int i = 0; i <= 69; i++) {
						sheet.autoSizeColumn(i);
					}
					wb.write(output);
					FileRequest fileRequest = new FileRequest();
					KycFileRequest kyc = new KycFileRequest();
					InputStream fileStream = new ByteArrayInputStream(output.toByteArray());

					fileRequest.setInputStream(fileStream);

					kyc.setFileName("contact.xls");
					fileRequest.setUserId(userId);
					fileRequest.setFileName(kyc.getFileName());
					fileRequest.setFileType(FileType.Agreement);
					fileRequest.setFilePrifix(FileType.Agreement.name());
					fileManagementService.putFile(fileRequest);
					FileResponse file = fileManagementService.getFile(fileRequest);
					String urlDownloadValue = file.getDownloadUrl();
					String[] splitingUrl = urlDownloadValue.split(Pattern.quote("?"));
					String urlValue = splitingUrl[0];

					fileResponse.setDownloadUrl(urlValue);

					fileResponse.setFileName("contact.xls");
					fileResponse.setKycType(KycType.CONTACTS);
				} catch (Exception e) {
					logger.error("Exception occured in generateContactDetails", e);

				} finally {
					try {
						if (outFile != null) {
							outFile.close();
						}

					} catch (Exception e) {
						logger.error("Exception occured in generateContactDetails", e);

					}
				}

			}
			UserDocumentStatus userDocumentStatus = userDocumentStatusRepo.findByUserIdAndDocumentSubType(userId,
					KycType.CONTACTS.toString());
			if (userDocumentStatus != null) {
				userDocumentStatus.setFilePath(fileResponse.getDownloadUrl());
				userDocumentStatusRepo.save(userDocumentStatus);
			}
		}

		return fileResponse;

	}

	@Override
	public KycFileResponse getIosContactDetails(int userId) {
		logger.info("getIosContactDetails Method start....................");
		User user = userRepo.findById(userId).get();
		KycFileResponse kycFileResponse = new KycFileResponse();

		if (user != null) {
			UserDocumentStatus userDocumentStatus = userDocumentStatusRepo.findByUserIdAndDocumentSubType(userId,
					KycType.CONTACTS.toString());
			if (userDocumentStatus != null) {
				kycFileResponse.setDownloadUrl(userDocumentStatus.getFilePath());
			} else {
				throw new OperationNotAllowedException("Borrowwer Contact details not upload",
						ErrorCodes.ENITITY_NOT_FOUND);
			}
		}
		logger.info("getIosContactDetails Method Ends....................");
		return kycFileResponse;

	}

	@Override
	public UserResponse uploadPaymentScreenshot(String uniqueNumber, List<KycFileRequest> kycFiles)
			throws FileNotFoundException, IOException {
		logger.info("uploadPaymentScreenshot Method start");

		User user = userRepo.findByUniqueNumber(uniqueNumber);

		UserDocumentStatus docStatus = new UserDocumentStatus();

		if (user != null) {

			for (KycFileRequest kyc : kycFiles) {

				FileRequest fileRequest = new FileRequest();

				InputStream fileStream = kyc.getFileInputStream();
				if (fileStream == null) {
					fileStream = decodeBase64Stream(kyc.getBase64EncodedStream());
				}
				fileRequest.setInputStream(fileStream);
				fileRequest.setUserId(user.getId());
				fileRequest.setFileName(kyc.getFileName());
				fileRequest.setFilePrifix(kyc.getKycType().name());
				FileResponse putFile = fileManagementService.putFile(fileRequest);
				UserDocumentStatus userDocumentStatus = new UserDocumentStatus();
				userDocumentStatus.setUserId(user.getId());
				userDocumentStatus.setDocumentType(DocumentType.ScrowScreenShot);
				userDocumentStatus.setDocumentSubType(kyc.getKycType().toString());
				userDocumentStatus.setFilePath(putFile.getFilePath());
				userDocumentStatus.setFileName(kyc.getFileName());
				userDocumentStatus.setStatus(Status.UPLOADED);
				docStatus = userDocumentStatusRepo.save(userDocumentStatus);
			}

		} else {

			throw new ActionNotAllowedException("User Not Found", ErrorCodes.USER_NOT_FOUND);
		}

		UserResponse response = new UserResponse();

		response.setId(user.getId());
		response.setDocumentId(docStatus.getId());
		response.setUniqueNumber(user.getUniqueNumber());

		return response;
	}

	@Override
	public PaymentUploadHistoryResponseDto uploadPaymentHistory(
			PaymentUploadHistoryRequestDto paymentUploadHistoryRequestDto) {
		logger.info("uploadPaymentHistory Method start");

		User user1 = userRepo.findByUniqueNumber(paymentUploadHistoryRequestDto.getBorrowerUniqueNumber());

		Integer borrowerId = user1.getId();
		Double amount = paymentUploadHistoryRequestDto.getAmount();
		String paidDate = paymentUploadHistoryRequestDto.getPaidDate();

		Object details = paymentUploadHistoryRepo.findByAmountAndPaidDateAndUserId(borrowerId, amount, paidDate);

		if (details != null) {
			throw new ActionNotAllowedException("Already uploaded with same details to this borrower",
					ErrorCodes.ALREDY_IN_PROGRESS);
		}

		if (paymentUploadHistoryRequestDto != null) {
			String uniqueNumber = paymentUploadHistoryRequestDto.getBorrowerUniqueNumber();
			if (uniqueNumber != null) {
				User user = userRepo.findByUniqueNumber(uniqueNumber);
				if (user != null) {
					PaymentUploadHistory paymentUploadHistory = new PaymentUploadHistory();
					paymentUploadHistory.setBorrowerUniqueNumber(uniqueNumber);
					if (paymentUploadHistoryRequestDto.getBorrowerName() != null) {
						paymentUploadHistory.setBorrowerName(paymentUploadHistoryRequestDto.getBorrowerName());
					}
					int id = paymentUploadHistoryRequestDto.getDocumentUploadId();
					UserDocumentStatus documentStatus = userDocumentStatusRepo.findByIdAndUserId(id, user.getId());
					if (documentStatus != null) {
						paymentUploadHistory.setDocumentUploadedId(id);
						if (paymentUploadHistoryRequestDto.getAmount() > 0) {
							PaymentUploadHistory paymentUpload = paymentUploadHistoryRepo.findByDocumentUploadedId(id);
							if (paymentUpload == null) {
								paymentUploadHistory.setAmount(paymentUploadHistoryRequestDto.getAmount());
								paymentUploadHistory.setUpdatedName(paymentUploadHistoryRequestDto.getUpdatedName());
								try {
									paymentUploadHistory.setPaidDate(
											expectedDateFormat.parse(paymentUploadHistoryRequestDto.getPaidDate()));
								} catch (ParseException e) {

									e.printStackTrace();
								}
								paymentUploadHistory.setPaymentType(PaymentType
										.valueOf(paymentUploadHistoryRequestDto.getPaymentType().toUpperCase()));

								paymentUploadHistory.setUserId(user.getId());

								paymentUploadHistoryRepo.save(paymentUploadHistory);
							} else {
								throw new ActionNotAllowedException(
										"Already uploaded with same document id to this borrower",
										ErrorCodes.ALREDY_IN_PROGRESS);
							}
						} else {
							throw new ActionNotAllowedException("Amount should be greater than zero",
									ErrorCodes.USER_NOT_FOUND);
						}

					} else {
						throw new ActionNotAllowedException("DocumentId is not correct please",
								ErrorCodes.USER_NOT_FOUND);
					}
				} else {
					throw new ActionNotAllowedException("User Not Exist by given uniqueNumber please check",
							ErrorCodes.USER_NOT_FOUND);
				}
			}
		}
		PaymentUploadHistoryResponseDto paymentUploadHistoryResponseDto = new PaymentUploadHistoryResponseDto();
		paymentUploadHistoryResponseDto
				.setBorrowerUniqueNumber(paymentUploadHistoryRequestDto.getBorrowerUniqueNumber());
		logger.info("uploadPaymentHistory Method start");
		return paymentUploadHistoryResponseDto;

	}

	@Override
	public LenderRatingResponseDto lenderRatingToBorrowerApplication(Integer parentRequestId,
			LenderRatingRequestDto lenderRatingRequestDto) {

		logger.info("lenderRatingToBorrowerApplication Method Starts.......................");
		LoanRequest loanRequest = loanRequestRepo.findById(parentRequestId).get();
		if (loanRequest != null) {
			int lenderId = lenderRatingRequestDto.getLenderId();

			LenderRatingToBorrowerApplication lenderRatingToBorrowerApplication = lenderRatingToBorrowerApplicationRepo
					.findByLenderIdAndParentRequestId(lenderId, parentRequestId);

			if (lenderRatingToBorrowerApplication == null) {
				LenderRatingToBorrowerApplication lenderRatingToBorrowerApplicationDetails = new LenderRatingToBorrowerApplication();
				if (lenderRatingRequestDto.getRating() > 0 && lenderRatingRequestDto.getRating() <= 5) {
					lenderRatingToBorrowerApplicationDetails.setLenderId(lenderId);
					lenderRatingToBorrowerApplicationDetails.setParentRequestId(parentRequestId);
					lenderRatingToBorrowerApplicationDetails.setRating(lenderRatingRequestDto.getRating());
					lenderRatingToBorrowerApplicationRepo.save(lenderRatingToBorrowerApplicationDetails);
				} else {
					throw new OperationNotAllowedException("Rating should be greater than zero and less than five ",
							ErrorCodes.LIMIT_REACHED);
				}

			} else {
				throw new OperationNotAllowedException("Lender already given rating to borrower application",
						ErrorCodes.ACTION_ALREDY_DONE);
			}

		}

		LenderRatingResponseDto lenderRatingResponseDto = new LenderRatingResponseDto();
		lenderRatingResponseDto.setParentRequestId(parentRequestId);
		logger.info("lenderRatingToBorrowerApplication Method Ends....................");
		return lenderRatingResponseDto;

	}

	@Override
	@Transactional
	public List<UserResponse> borrowerInformation(String uniqueNumber) {

		logger.info("borrowerInformation method start");

		String borrowerUnique = uniqueNumber.substring(uniqueNumber.length() - 4);

		List<User> borrowersBasedOnUnique = userRepo.findBorrowerBasedOnUnique(borrowerUnique);

		List<UserResponse> userResponseList = new ArrayList<UserResponse>();

		for (User borrowers : borrowersBasedOnUnique) {
			UserResponse userResponse = new UserResponse();
			userResponse.setMobileNumber(borrowers.getMobileNumber());
			userResponse.setId(borrowers.getId());
			userResponse.setUniqueNumber(borrowers.getUniqueNumber());
			PersonalDetails personalDetails = borrowers.getPersonalDetails();
			userResponse.setFirstName(personalDetails.getFirstName() + " " + personalDetails.getLastName());
			userResponseList.add(userResponse);

		}

		logger.info("borrowerInformation method start");
		return userResponseList;
	}

	@Override
	public PaymentUploadHistoryResponseDto countOfPaymentScreenshot(Integer userId) {
		logger.info("Count Of Payment Screenshots Method Starts..................");

		User user = userRepo.findById(userId).get();

		if (user == null) {
			throw new ActionNotAllowedException("User is not found", ErrorCodes.USER_NOT_FOUND);
		}

		List<PaymentUploadHistoryResponseDto> paymentUploadResponseList = new ArrayList<PaymentUploadHistoryResponseDto>();

		Integer countOfPaymentScreenShot = userRepo.getCountOfPaymentScreenshotUploaded(userId);

		List<Object[]> countOfPaymentScreenshotUpload = userRepo.getCountOfPaymentScreenshot(userId);

		for (Object[] paymentUploadResponse : countOfPaymentScreenshotUpload) {
			PaymentUploadHistoryResponseDto response = new PaymentUploadHistoryResponseDto();

			response.setBorrowerUniqueNumber(paymentUploadResponse[0].toString());

			response.setAmount((Double) paymentUploadResponse[1]);

			response.setPaidDate(paymentUploadResponse[2].toString());
			response.setUpdatedOn((Date) paymentUploadResponse[3]);
			response.setPaymentType(paymentUploadResponse[4].toString());
			response.setUserId((Integer) paymentUploadResponse[5]);
			response.setFileName(paymentUploadResponse[6].toString());
			response.setFilePath(paymentUploadResponse[7].toString());
			response.setId((Integer) paymentUploadResponse[8]);

			paymentUploadResponseList.add(response);
		}

		PaymentUploadHistoryResponseDto paymentUpload = new PaymentUploadHistoryResponseDto();

		paymentUpload.setCountOfPaymentScreenShots(countOfPaymentScreenShot);
		paymentUpload.setListOfPaymentUploaded(paymentUploadResponseList);

		logger.info("Count Of Payment Screenshots Method Ends..................");

		return paymentUpload;

	}

	@Override
	public PaymentSearchByStatus getPaymentStatistics(int days, String paymentStatus,
			PaginationRequestDto paginationRequestDto) {
		logger.info("getPaymentStatistics method starts");
		int pageNo = paginationRequestDto.getPageNo();
		int pageSize = paginationRequestDto.getPageSize();

		List<PaymentUploadHistoryResponseDto> paymentUploadHistoryResponseDtoList = new ArrayList<PaymentUploadHistoryResponseDto>();
		pageNo = (pageSize * (pageNo - 1));

		List<Object[]> paymentUploadHistoryList = paymentUploadHistoryRepo.getUploadedStatistics(days, pageSize, pageNo,
				paymentStatus.toUpperCase());
		Integer totalCount = paymentUploadHistoryRepo.getUploadedStatisticsCount(days, paymentStatus.toUpperCase());
		if (paymentUploadHistoryList != null && !paymentUploadHistoryList.isEmpty()) {
			Iterator it = paymentUploadHistoryList.iterator();
			while (it.hasNext()) {
				Object[] e = (Object[]) it.next();
				PaymentUploadHistoryResponseDto paymentUploadHistoryResponseDto = new PaymentUploadHistoryResponseDto();
				paymentUploadHistoryResponseDto.setBorrowerUniqueNumber(e[0] == null ? " " : e[0].toString());
				paymentUploadHistoryResponseDto.setBorrowerName(e[1] == null ? " " : e[1].toString());
				paymentUploadHistoryResponseDto.setAmount(Double.parseDouble(e[2] == null ? "0" : e[2].toString()));
				paymentUploadHistoryResponseDto.setUpdatedName(e[3] == null ? " " : e[3].toString());
				paymentUploadHistoryResponseDto.setPaidDate(e[4].toString());
				paymentUploadHistoryResponseDto.setUserId(Integer.parseInt(e[5] == null ? "0" : e[5].toString()));
				paymentUploadHistoryResponseDto.setPaymentType(e[6] == null ? " " : e[6].toString());
				paymentUploadHistoryResponseDto.setStatus(e[7] == null ? " " : e[7].toString());
				paymentUploadHistoryResponseDtoList.add(paymentUploadHistoryResponseDto);
			}
		}

		PaymentSearchByStatus paymentBasedOnDays = new PaymentSearchByStatus();
		paymentBasedOnDays.setPaymentUploadHistoryResponseDto(paymentUploadHistoryResponseDtoList);
		if (totalCount != null) {
			paymentBasedOnDays.setTotalCount(totalCount);
		}
		logger.info("getPaymentStatistics method ends");
		return paymentBasedOnDays;

	}

	@Override
	public PaymentUploadHistoryResponseDto updatePaymentUploadHistoryStatus(int documentId,
			PaymentUploadHistoryRequestDto paymentUploadHistoryRequestDto) {
		int currentUser = authorizationService.getCurrentUser();
		User userInfo = userRepo.findById(currentUser).get();
		if ((userInfo.getPrimaryType() != PrimaryType.ADMIN)
				&& (userInfo.getPrimaryType() != PrimaryType.PAYMENTSADMIN)) {
			throw new OperationNotAllowedException("As a " + userInfo.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}

		PaymentUploadHistory paymentUploadHistory = paymentUploadHistoryRepo.findByDocumentUploadedId(documentId);

		if (paymentUploadHistory != null) {

			if (paymentUploadHistoryRequestDto.getStatus() != null) {

				if (paymentUploadHistoryRequestDto.getStatus().equalsIgnoreCase("approved")) {
					PaymentUploadHistory paymentUploadToApprove = paymentUploadHistoryRepo
							.findByPaymentForApproving(documentId);
					if (paymentUploadToApprove != null) {
						paymentUploadHistory.setStatus(PaymentUploadHistory.Status.APPROVED);
						paymentUploadHistory.setApprovedOn(new Date());
						User user = userRepo.findByUniqueNumber(paymentUploadHistory.getBorrowerUniqueNumber());
						if (user != null) {
							User loanOwnerDetails = userRepo.findBorrowerMappedToLoanOwner(user.getId());
							if (loanOwnerDetails != null) {
								Integer numberOfApplications = oxyLoanRepo
										.findingNumberOfApplicationsToBorrower(user.getId());
								Integer applicationId = 0;
								if (numberOfApplications != null && numberOfApplications == 1) {
									Integer borrowerParentRequest = oxyLoanRepo.getCurrentApplication(user.getId());
									if (borrowerParentRequest != null) {
										applicationId = borrowerParentRequest;
									}
									List<Integer> listOfActiveLoans = oxyLoanRepo.findNumberOfActiveLoans(user.getId());

									for (int i1 = 0; i1 < listOfActiveLoans.size(); i1++) {
										int loanId = listOfActiveLoans.get(i1);
										List<LoanEmiCard> listOfEmisNumbers = loanEmiCardRepo
												.findByLoanIdOrderByEmiNumber(loanId);
										for (LoanEmiCard loansInfo : listOfEmisNumbers) {
											Integer emiNumber = loansInfo.getEmiNumber();
											LoanEmiCard loanEmiCard = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId,
													emiNumber);
											if (loanEmiCard != null) {
												loanEmiCard.setEmiLateFeeCharges(0.0);
												loanEmiCardRepo.save(loanEmiCard);
												InterestDetails interestDetails = interestDetailsRepo
														.findByloanIdAndEmiNumber(loanId, emiNumber);
												if (interestDetails != null) {
													interestDetails.setInterest(0.0);
													interestDetailsRepo.save(interestDetails);
												}
											}
										}

									}

									List<PenaltyDetails> penaltyDetails = penaltyDetailsRepo
											.findByBorrowerParentRequestidOrderByEmiNumber(applicationId);
									if (penaltyDetails != null && !penaltyDetails.isEmpty()) {
										for (PenaltyDetails penality : penaltyDetails) {
											penality.setPenalty(0.0);
											penaltyDetailsRepo.save(penality);
										}

									}

									LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto = new LoanEmiCardPaymentDetailsRequestDto();
									String applicationValue = applicationId.toString();
									loanEmiCardPaymentDetailsRequestDto.setApplicationId(applicationValue);
									loanEmiCardPaymentDetailsRequestDto
											.setPartialPaymentAmount(paymentUploadToApprove.getAmount());
									loanEmiCardPaymentDetailsRequestDto.setPenalty(0.0);
									loanEmiCardPaymentDetailsRequestDto.setTransactionReferenceNumber("12345");
									loanEmiCardPaymentDetailsRequestDto.setModeOfPayment("ICICI");
									Date date = new Date();
									loanEmiCardPaymentDetailsRequestDto
											.setAmountPaidDate(expectedDateFormat.format(date));
									loanEmiCardPaymentDetailsRequestDto.setUpdatedUserName("Mega");
									LoanEmiCardPaymentDetailsResponseDto loanEmiCardPaymentDetails = borrowerLoanService
											.updEmisByApplication(loanEmiCardPaymentDetailsRequestDto);

								}
							}
						}
					} else {
						throw new OperationNotAllowedException("Your already approved this document",
								ErrorCodes.ALREDY_IN_PROGRESS);
					}
				} else {
					if (paymentUploadHistoryRequestDto.getStatus().equalsIgnoreCase("notyetreflected")) {
						PaymentUploadHistory paymentUploadNotArReflected = paymentUploadHistoryRepo
								.findByPaymentForNotAtReflected(documentId);
						if (paymentUploadNotArReflected != null) {
							paymentUploadHistory.setStatus(PaymentUploadHistory.Status.NOTYETREFLECTED);
							if (paymentUploadHistoryRequestDto.getComments() != null) {
								paymentUploadHistory.setComments(paymentUploadHistoryRequestDto.getComments());
							}
							paymentUploadHistory.setApprovedOn(new Date());
						} else {
							throw new OperationNotAllowedException("Your already approved this document",
									ErrorCodes.ALREDY_IN_PROGRESS);
						}
					}
				}
				paymentUploadHistoryRepo.save(paymentUploadHistory);
				if (paymentUploadHistory.getPaymentType().equals(PaymentType.FULLPAYMENT)) {
					List<Object[]> loanrequestList = loanRequestRepo
							.findAllBorrowerRunningApplications(paymentUploadHistory.getUserId());
					int borrowerApplicationId = 0;

					if (loanrequestList != null) {

						for (Object[] obj : loanrequestList) {

							double amount = Double.parseDouble(obj[1].toString());
							if (obj[2] == null) {
								obj[2] = "";
							}

							if (amount == paymentUploadHistory.getAmount()
									&& (obj[2] == null || obj[2].toString().isEmpty())) {

								Date date = paymentUploadHistory.getPaidDate();
								SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
								String strDate = formatter.format(date);
								LoanEmiCardPaymentDetailsResponseDto loanResponse = new LoanEmiCardPaymentDetailsResponseDto();
								loanResponse.setAmountPaidDate(strDate);
								loanResponse.setPartialPaymentAmount(paymentUploadHistory.getAmount());
								loanResponse.setModeOfPayment("ICICI");
								loanResponse
										.setTransactionReferenceNumber("TRSC111" + Integer.parseInt(obj[0].toString()));
								loanResponse.setApplicationId(String.valueOf(obj[0].toString()));

								try {
									borrowerLoanService.updEmisByApplication(loanResponse);
								} catch (Exception ex) {

									throw new DataFormatException(ex.getMessage(), ErrorCodes.DATA_PERSIST_ERROR);
								}
								break;

							}

						}
					}
				}
			} else {
				throw new DataFormatException("Given status is null please check", ErrorCodes.DATA_PERSIST_ERROR);
			}

		} else {
			throw new DataFormatException("Document Id " + documentId + " not Found", ErrorCodes.DATA_PERSIST_ERROR);
		}

		PaymentUploadHistoryResponseDto paymentUploadHistoryResponseDto = new PaymentUploadHistoryResponseDto();
		paymentUploadHistoryResponseDto.setStatus(paymentUploadHistoryRequestDto.getStatus());

		paymentUploadHistoryResponseDto.setUserId(paymentUploadHistory.getUserId());
		paymentUploadHistoryResponseDto.setAmount(paymentUploadHistory.getAmount());
		paymentUploadHistoryResponseDto.setUpdatedName(paymentUploadHistory.getUpdatedName());
		paymentUploadHistoryResponseDto.setBorrowerUniqueNumber(paymentUploadHistory.getBorrowerUniqueNumber());

		return paymentUploadHistoryResponseDto;

	}

	@Override
	public PaymentSearchByStatus getPaymentUploadedListAndCount(String status,
			PaginationRequestDto paginationRequestDto) {
		int pageNo = paginationRequestDto.getPageNo();
		int pageSize = paginationRequestDto.getPageSize();
		logger.info("getPaymentUploadedListAndCount Method Starts");

		List<PaymentUploadHistoryResponseDto> paymentUploadHistoryResponseDtoList = new ArrayList<PaymentUploadHistoryResponseDto>();
		pageNo = (pageSize * (pageNo - 1));
		List<Object[]> paymentUploadHistoryList = paymentUploadHistoryRepo.getUploadedList(status, pageNo, pageSize);

		Integer countOfPaymentUpload = paymentUploadHistoryRepo.getCountOfPaymentUpload(status);

		for (Object[] paymentHistory : paymentUploadHistoryList) {
			PaymentUploadHistoryResponseDto paymentUploadHistoryResponseDto = new PaymentUploadHistoryResponseDto();

			paymentUploadHistoryResponseDto
					.setBorrowerUniqueNumber(paymentHistory[0] == null ? "" : paymentHistory[0].toString());
			paymentUploadHistoryResponseDto
					.setBorrowerName(paymentHistory[1] == null ? "" : paymentHistory[1].toString());
			paymentUploadHistoryResponseDto
					.setAmount(Double.parseDouble(paymentHistory[2] == null ? "0" : paymentHistory[2].toString()));
			paymentUploadHistoryResponseDto.setPaidDate(paymentHistory[3].toString());
			paymentUploadHistoryResponseDto
					.setPaymentType(paymentHistory[4] == null ? "" : paymentHistory[4].toString());
			paymentUploadHistoryResponseDto
					.setUserId(Integer.parseInt(paymentHistory[5] == null ? "0" : paymentHistory[5].toString()));
			paymentUploadHistoryResponseDto
					.setUpdatedName(paymentHistory[6] == null ? "" : paymentHistory[6].toString());
			paymentUploadHistoryResponseDto
					.setId(Integer.parseInt(paymentHistory[7] == null ? "0" : paymentHistory[7].toString()));
			paymentUploadHistoryResponseDto.setStatus(paymentHistory[8] == null ? "" : paymentHistory[8].toString());
			paymentUploadHistoryResponseDto.setDocumentUploadId(
					Integer.parseInt(paymentHistory[9] == null ? "0" : paymentHistory[9].toString()));
			int documentId = Integer.parseInt(paymentHistory[9] == null ? "0" : paymentHistory[9].toString());
			String comments = paymentHistory[10] == null ? "" : paymentHistory[10].toString();

			if (comments.length() > 0) {
				paymentUploadHistoryResponseDto.setComments(comments);
			} else {
				paymentUploadHistoryResponseDto.setComments("N/A");
			}
			UserDocumentStatus userDocumentStatus = userDocumentStatusRepo.findById(documentId).get();
			if (userDocumentStatus != null) {
				FileRequest fileRequest = new FileRequest();
				fileRequest.setFileName(userDocumentStatus.getFileName());
				fileRequest.setFileType(FileType.Kyc);
				fileRequest.setUserId(userDocumentStatus.getUserId());
				fileRequest.setFilePrifix("PAYMENTSCREENSHOT");
				FileResponse file = fileManagementService.getFile(fileRequest);
				String urlDownload = file.getDownloadUrl();

				String[] splitingTheUrl = urlDownload.split(Pattern.quote("?"));
				String url = splitingTheUrl[0];
				paymentUploadHistoryResponseDto.setPaymentUrl(url);
			}
			paymentUploadHistoryResponseDtoList.add(paymentUploadHistoryResponseDto);
		}

		PaymentSearchByStatus paymentSearchByStatus = new PaymentSearchByStatus();
		paymentSearchByStatus.setPaymentUploadHistoryResponseDto(paymentUploadHistoryResponseDtoList);
		paymentSearchByStatus.setTotalCount(countOfPaymentUpload);

		logger.info("getPaymentUploadedListAndCount Method Ends");
		return paymentSearchByStatus;

	}

	@Override
	public SearchResultsDto<PaymentUploadHistoryResponseDto> getUpdatedInfoByOxyMembers(String updatedName,
			String paymentStatus) {
		logger.info("getUpdatedInfoByOxyMembers method starts");
		List<PaymentUploadHistoryResponseDto> paymentUploadHistoryResponseDtoList = new ArrayList<PaymentUploadHistoryResponseDto>();

		List<Object[]> countAndSumByOxyMembers = paymentUploadHistoryRepo.getInfoByOxyMembers(updatedName,
				paymentStatus.toUpperCase());
		int count = 0;

		for (Object[] obj : countAndSumByOxyMembers) {

			PaymentUploadHistoryResponseDto paymentUploadHistoryResponseDto = new PaymentUploadHistoryResponseDto();
			paymentUploadHistoryResponseDto.setUserId(Integer.parseInt(obj[0] == null ? " " : obj[0].toString()));
			paymentUploadHistoryResponseDto.setBorrowerUniqueNumber(obj[1] == null ? " " : obj[1].toString());
			paymentUploadHistoryResponseDto.setBorrowerName(obj[2] == null ? " " : obj[2].toString());
			paymentUploadHistoryResponseDto.setAmount(Double.parseDouble(obj[3] == null ? " " : obj[3].toString()));
			paymentUploadHistoryResponseDto.setUpdatedName(obj[4] == null ? " " : obj[4].toString());
			paymentUploadHistoryResponseDto.setPaidDate(obj[5] == null ? " " : obj[5].toString());

			paymentUploadHistoryResponseDtoList.add(paymentUploadHistoryResponseDto);
		}
		SearchResultsDto<PaymentUploadHistoryResponseDto> searchResponse = new SearchResultsDto<>();
		searchResponse.setResults(paymentUploadHistoryResponseDtoList);
		searchResponse.setTotalCount(count);
		logger.info("getUpdatedInfoByOxyMembers method ends");
		return searchResponse;
	}

	@Override
	public LenderProfitResponse getLenderProfitInformation(String startDate, String endDate) {

		logger.info("getLenderProfitInformation Method Starts....................");

		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			date = simpleDateFormat.parse(startDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		Timestamp startDate1 = new Timestamp(date.getTime());

		Date date1 = null;
		try {
			date1 = simpleDateFormat.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Timestamp endDate1 = new Timestamp(date1.getTime());

		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();

		if (user == null) {
			throw new ActionNotAllowedException("User is not found", ErrorCodes.USER_NOT_FOUND);
		}

		Integer lenderId = user.getId();

		Double totalProfit = 0.0;

		Integer totalEmisPaid = 0;

		List<LenderProfitHistoryResponse> lenderHistoryResponse = new ArrayList<LenderProfitHistoryResponse>();

		List<Object[]> lenderHistoryResponseList = userRepo.getLenderProfitInfo(lenderId, startDate1, endDate1);

		for (Object[] response : lenderHistoryResponseList) {

			LenderProfitHistoryResponse lenderProfitResponse = new LenderProfitHistoryResponse();

			lenderProfitResponse.setBorrowerName(response[0] == null ? " " : response[0].toString());
			lenderProfitResponse.setLoanId(response[1] == null ? " " : response[1].toString());
			lenderProfitResponse.setDisbursedAmount(response[2] == null ? 0.0 : (Double) response[2]);
			lenderProfitResponse.setLoanStatus(response[3] == null ? " " : response[3].toString());
			lenderProfitResponse.setProfitReceived(response[4] == null ? 0.0 : (Double) response[4]);
			lenderProfitResponse.setRateOfInterest(response[5] == null ? 0.0 : (double) response[5]);
			lenderProfitResponse.setDuration(response[6] == null ? 0 : (int) response[6]);
			lenderProfitResponse.setCountOfEmiPaid(response[7] == null ? 0 : Integer.parseInt(response[7].toString()));

			Double profit = (Double) response[4];
			totalProfit += profit;

			Integer emisPaidOn = Integer.parseInt(response[7].toString());

			totalEmisPaid += emisPaidOn;

			lenderHistoryResponse.add(lenderProfitResponse);
		}

		LenderProfitResponse lenderProfitResponse = new LenderProfitResponse();
		lenderProfitResponse.setLenderProfitHistoryResponse(lenderHistoryResponse);
		lenderProfitResponse.setTotalProfit(totalProfit);
		lenderProfitResponse.setTotalEmisReceived(totalEmisPaid);

		logger.info("getLenderProfitInformation Method Ends....................");

		return lenderProfitResponse;
	}

	@Override
	public String sendingMailToApprovePayments() {
		String message = null;
		Integer paymentsHaveToApprove = paymentUploadHistoryRepo.getPaymentHaveToApproveTillDateCount();
		Integer payemntsApproved = paymentUploadHistoryRepo.getPaymentHadApprovedCount();
		Integer paymentsUploadedToday = paymentUploadHistoryRepo.getPaymentUploadedToday();
		Integer paymentsNotAtReflected = paymentUploadHistoryRepo.getPaymentNotAtReflectedCount();
		Calendar currentDate = Calendar.getInstance();
		String expectedCurrentDate = expectedDateFormat.format(currentDate.getTime());

		TemplateContext templateContext = new TemplateContext();
		if (paymentsHaveToApprove != null) {
			templateContext.put("paymentsHaveToApprove", paymentsHaveToApprove);
		}
		if (payemntsApproved != null) {
			templateContext.put("payemntsApproved", payemntsApproved);
		}
		if (paymentsUploadedToday != null) {
			templateContext.put("paymentsUploadedToday", paymentsUploadedToday);
		}
		if (paymentsNotAtReflected != null) {
			templateContext.put("paymentsNotAtReflected", paymentsNotAtReflected);
		}
		templateContext.put("CurrentDate", expectedCurrentDate);
		String mailsubject = "Payment Screenshot Information";

		EmailRequest emailRequest = new EmailRequest(
				new String[] { "bhargav@oxyloans.com", "ramadevi@oxyloans.com", "radhakrishna.t@oxyloans.com",
						"subbu@oxyloans.com", "sekhar@oxyloans.com", "archana.n@oxyloans.com" },
				mailsubject, "payment-screenshot.template", templateContext);
		message = "SUCCESS";
		EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
		if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
			try {
				throw new MessagingException(emailResponseFromService.getErrorMessage());
			} catch (MessagingException e1) {

				e1.printStackTrace();
			}
		}

		return message;

	}

	@Override
	public LenderProfitResponse getLenderProfitDetails(int userId, int month) {
		logger.info("getLenderProfitDetails method starts");

		User user = userRepo.findById(userId).get();

		if (user.getPrimaryType() != PrimaryType.LENDER) {
			throw new OperationNotAllowedException(
					"As a " + user.getPrimaryType() + " primary type not allowed to perform this",
					ErrorCodes.INVALID_OPERATION);
		}

		List<Object[]> find = oxyLoanRepo.getLenderProfit(userId, month);
		List<LenderHistroryResponseDto> lenderHistroryResponseList = new ArrayList<LenderHistroryResponseDto>();
		double totalProfit = 0;
		Integer totalEmisCount = 0;

		if (find != null && !find.isEmpty()) {
			for (Object[] obj : find) {
				LenderHistroryResponseDto lenderHistroryResponseDto = new LenderHistroryResponseDto();
				lenderHistroryResponseDto.setLoanId(obj[0] == null ? " " : obj[0].toString());
				lenderHistroryResponseDto.setDisbursedAmount((obj[1] == null ? " " : obj[1].toString()));
				lenderHistroryResponseDto.setLoanStatus(obj[2] == null ? " " : obj[2].toString());
				lenderHistroryResponseDto.setProfit(Double.parseDouble(obj[3] == null ? " " : obj[3].toString()));
				lenderHistroryResponseDto.setBorrowerName(obj[4] == null ? " " : obj[4].toString());
				lenderHistroryResponseDto
						.setEmisPaidByBorrower(Integer.parseInt(obj[5] == null ? " " : obj[5].toString()));

				double profit = Double.parseDouble(obj[3].toString());
				Integer emiCount = Integer.parseInt(obj[5].toString());
				if (profit != 0) {
					totalProfit += profit;

				}

				if (emiCount != null) {
					totalEmisCount += emiCount;
				}

				lenderHistroryResponseList.add(lenderHistroryResponseDto);
			}

		}

		LenderProfitResponse response = new LenderProfitResponse();
		response.setLenderHistoryResponseDto(lenderHistroryResponseList);
		response.setTotalProfit(totalProfit);
		response.setTotalEmisReceived(totalEmisCount);
		logger.info("getLenderProfitDetails method ends");
		return response;

	}

	@Override
	public UserVanNumberResponseDto fetchBorrowerVanNumber(Integer userId) {
		User user = userRepo.findById(userId).get();
		UserVanNumberResponseDto response = new UserVanNumberResponseDto();
		if (user != null) {
			PersonalDetails personalDetails = user.getPersonalDetails();
			response.setUserId(user.getId());
			response.setUniqueNumber(user.getUniqueNumber());
			response.setUserName(personalDetails.getFirstName() + " " + personalDetails.getLastName());
			if (user.getVanNumber() != null) {
				response.setVanNumber(user.getVanNumber());
			}
		} else {
			throw new OperationNotAllowedException("user not present with given id", ErrorCodes.USER_NOT_FOUND);
		}

		return response;
	}

	public LenderReferenceResponse lenderReferenceDetails(LenderReferenceRequestDto lenderReferenceDto) {
		logger.info("lenderReferenceDetails method starts.............");

		LenderReferenceResponse response = new LenderReferenceResponse();

		int refereeCount = 0;
		int suceessCount = 0;
		int count = 0;
		List<LenderReferenceDetails> ref = new ArrayList<>();
		String referrerName;
		String referrerEmail;
		String referrerMobile;
		Optional<User> optionalUser = userRepo.findById(lenderReferenceDto.getReferrerId());
		if (optionalUser.isPresent()) {
			User user1 = optionalUser.get();
			String lName = user1.getPersonalDetails().getLastName() != null ? user1.getPersonalDetails().getLastName()
					: " ";
			referrerName = user1.getPersonalDetails().getFirstName() + " " + lName;
			referrerEmail = user1.getEmail();
			referrerMobile = user1.getMobileNumber();
		} else {

			throw new ActionNotAllowedException("user is not found", ErrorCodes.USER_NOT_FOUND);

		}

		String[] mails = lenderReferenceDto.getEmail().split(",");

		LenderReferenceDetails referenceDetail = null;

		StringBuilder stringBuilder = new StringBuilder();

		for (String emailAddress : mails) {
			String mobileNumber = null;
			String name = null;
			String email = null;

			emailAddress = emailAddress.replaceAll(" ", "");

			String[] details = emailAddress.split("-");

			int countOfEmailToSent = mails.length;
			logger.info("Number of elements present: " + countOfEmailToSent);

			LenderReferenceDetails referenceDetails = new LenderReferenceDetails();

			logger.info("details[0]::" + details[0]);

			if (lenderReferenceDto.getInviteType().equals("SingleInvite")) {
				email = lenderReferenceDto.getEmail();
				name = lenderReferenceDto.getName();
				mobileNumber = lenderReferenceDto.getMobileNumber();

				Integer refereeEmail = lenderReferenceDetailsRepo.findByRefereeEmailInEqualIgnoranceCase(email);

				LenderReferenceDetails refereeMobileNumber = lenderReferenceDetailsRepo
						.findByRefereeMobileNumber(mobileNumber);

				if (refereeEmail > 0 || refereeMobileNumber != null) {

					throw new ActionNotAllowedException("Already lender has reffereed.", ErrorCodes.ALREDY_IN_PROGRESS);

				}

				Integer userEmail = userRepo.findByEmailUsingEqualIgnoranceCase(email);

				User userMobileNumber = userRepo.findByMobileNumber(mobileNumber);

				if (userEmail > 0 || userMobileNumber != null) {

					throw new ActionNotAllowedException("Already lender has registered.",
							ErrorCodes.ALREDY_IN_PROGRESS);

				}

				System.out.println("email=lenderReferenceDto.getEmail();::" + email + " " + name + " " + mobileNumber);

			} else {
				email = details[1];
				name = details[0];

				if (details.length == 3) {

					System.out.println("details..." + details[2]);
					mobileNumber = details[2];
					referenceDetails.setRefereeMobileNumber(details[2].equals("") ? "" : details[2]);
				}
			}

			if (mails.length == 1 && details.length == 1) {

				Integer refereeEmail = lenderReferenceDetailsRepo.findByRefereeEmailInEqualIgnoranceCase(email);

				// mobileNumber = lenderReferenceDto.getMobileNumber().replaceAll(" ", "");

				LenderReferenceDetails refereeMobileNumber = lenderReferenceDetailsRepo
						.findByRefereeMobileNumber(mobileNumber);

				if (refereeEmail > 0 || refereeMobileNumber != null) {

					continue;
				}

				Integer userEmail = userRepo.findByEmailUsingEqualIgnoranceCase(email);

				User userMobileNumber = userRepo.findByMobileNumber(mobileNumber);

				if (userEmail > 0 || userMobileNumber != null) {

					continue;

				}

			} else if (mails.length > 1 || details.length > 1) {

				// check for alredy invited user
				Integer refereeEmail = lenderReferenceDetailsRepo.findByRefereeEmailInEqualIgnoranceCase(email);

				if (refereeEmail > 0) {
					continue;
				}

				Integer userEmail = userRepo.findByEmailUsingEqualIgnoranceCase(email);

				if (userEmail > 0) {
					continue;
				}

				referenceDetails.setSource(LenderReferenceDetails.Source.BulkInvite);
			}

			Integer referrerId = lenderReferenceDto.getReferrerId();

			String referrername = "";
			String referrerMobileNumber = "";

			String referrerEmailId = "";

			String refereeEmailId = email;

			String refereeMobile = mobileNumber != null ? mobileNumber : "";

			String redirectionLink = "";

			String redirectionLinkForBorrower = "";

			String linkForNRI = "";

			String redirectionLinkReferrer = "";

			String referralBonusFaq = "";

			OxyUserType oxyUserType = null;

			if (lenderReferenceDto.getUserType() != null) {
				if (lenderReferenceDto.getUserType().equalsIgnoreCase("PARTNER")) {
					oxyUserType = oxyUserTypeRepo.findingPartnerDetails(referrerId);

					referrername = oxyUserType.getPartnerName();

					referrerMobileNumber = oxyUserType.getMobileNumber();

					referrerEmailId = oxyUserType.getEmail();

					redirectionLink = new StringBuffer()
							.append(this.partnerLenderRegistrationLink.replace("{utm_name}", oxyUserType.getUtmName()))
							.toString();

					redirectionLinkForBorrower = new StringBuffer().append(
							this.partnerBorrowerRegistrationLink.replace("{utm_name}", oxyUserType.getUtmName()))
							.toString();

					referenceDetails.setSource(LenderReferenceDetails.Source.Partner);

				}
			} else {

				User user = userRepo.findById(referrerId).get();

				String uniqueNumber = user.getUniqueNumber();

				referrername = user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName();

				referrerMobileNumber = user.getMobileNumber();

				referrerEmailId = user.getEmail();

				redirectionLink = new StringBuilder().append(this.redirectionLink
						.replace("{unique_number}", uniqueNumber).replace("{referee_mobilenumber}", refereeMobile)
						.replace("{referee_email}", refereeEmailId)).toString();

				redirectionLinkForBorrower = new StringBuilder().append(this.redirectionLinkForBorrower
						.replace("{unique_number}", uniqueNumber).replace("{referee_mobilenumber}", refereeMobile)
						.replace("{referee_email}", refereeEmailId)).toString();

				linkForNRI = new StringBuilder().append(this.linkForNRI.replace("{unique_number}", uniqueNumber)
						.replace("{referee_mobilenumber}", refereeMobile).replace("{referee_email}", refereeEmailId))
						.toString();

				redirectionLinkReferrer = new StringBuilder().append(this.redirectionLinkReferrer
						.replace("{referrer_mobilenumber}", refereeMobile).replace("{referrer_email}", referrerEmailId))
						.toString();

				referralBonusFaq = new StringBuilder().append(this.referralBonusFaq).toString();
			}

			name = details[0].equals("") ? "" : details[0];
			if (refereeEmailId != null) {

				referenceDetails.setRefereeEmail(refereeEmailId);

				referenceDetails.setRefereeName(name);

				referenceDetails.setReferrerId(lenderReferenceDto.getReferrerId());

				referenceDetails.setUserPrimaryType(lenderReferenceDto.getPrimaryType());

				referenceDetails
						.setRefereeName(lenderReferenceDto.getName() != null ? lenderReferenceDto.getName() : name);

				String refereeName = lenderReferenceDto.getName() != null ? lenderReferenceDto.getName() : name;

				Calendar currentDate = Calendar.getInstance();
				String expectedCurrentDate = expectedDateFormat.format(currentDate.getTime());

				TemplateContext templateContext = new TemplateContext();

				String mailSubject = "";

				String mailContent = "";

				String displayerName = "OXY Invite";

				LenderMailResponse mailResponse = new LenderMailResponse();

				if (lenderReferenceDto.getMailSubject().equals("0")) {
					mailSubject += "Discover Exciting P2P NBFC Lending Opportunities with RBI-Approved OxyLoans!";
				} else if (lenderReferenceDto.getMailSubject() != null) {
					mailSubject += lenderReferenceDto.getMailSubject();

					String mailSubjectEdited = mailSubject;
					mailResponse.setMailSubject(mailSubjectEdited);

					Gson gson = new Gson();
					String json = gson.toJson(mailResponse);

					referenceDetails.setMailContent(json);

				}

				if (!(lenderReferenceDto.getMailContent().equals("0"))) {
					mailContent += lenderReferenceDto.getMailContent();
					String editedMailContent = mailContent;

					mailResponse.setMailContent(editedMailContent);

					Gson gson = new Gson();
					String json = gson.toJson(mailResponse);

					referenceDetails.setMailContent(json);

				}

				if (lenderReferenceDto.getMailContent().equals("0")) {

					String emailTemplateName = "";

					if (lenderReferenceDto.getUserType() == null) {
						if (lenderReferenceDto.getPrimaryType().equalsIgnoreCase("LENDER")) {

							emailTemplateName = "lender-reference-edited.template";

							if (lenderReferenceDto.getCitizenType().equalsIgnoreCase("NRI")) {
								templateContext.put("redirectionLink", linkForNRI);
								referenceDetails.setCitizenType(LenderReferenceDetails.CitizenType.NRI);

							} else {
								templateContext.put("redirectionLink", redirectionLink);
							}

							templateContext.put("referrerId", "LR" + referrerId);
						} else {

							emailTemplateName = "borrower-reference.template";
							templateContext.put("redirectionLinkForBorrower", redirectionLinkForBorrower);

						}
					} else if (lenderReferenceDto.getUserType() != null) {
						if (lenderReferenceDto.getUserType().equalsIgnoreCase("PARTNER")) {

							emailTemplateName = "from_partner_to_lead.template";

							String displayerNameForPartner = "OXYLOANS";

							TemplateContext context = new TemplateContext();
							context.put("CurrentDate", expectedCurrentDate);
							context.put("primaryType", lenderReferenceDto.getPrimaryType());
							context.put("refereeName", refereeName);
							context.put("refereeMobileNumber", lenderReferenceDto.getMobileNumber());
							context.put("refereeEmail", lenderReferenceDto.getEmail());
							context.put("referrername", referrername);
							String emailTemplate = "to_partner.template";

							EmailRequest emailRequest = new EmailRequest(new String[] { oxyUserType.getEmail() },
									emailTemplate, context, mailSubject, displayerNameForPartner);

							EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
							if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
								try {

									throw new MessagingException(emailResponseFromService.getErrorMessage());
								} catch (MessagingException e1) {

									e1.printStackTrace();
								}
							}
							templateContext.put("redirectionLink", redirectionLink);
							templateContext.put("redirectionLinkForBorrower", redirectionLinkForBorrower);
						}
					}

					templateContext.put("CurrentDate", expectedCurrentDate);
					templateContext.put("referrername", referrername);
					templateContext.put("refereeName", refereeName);
					templateContext.put("referrerMobileNumber", referrerMobileNumber);
					templateContext.put("referrerEmailId", referrerEmailId);
					templateContext.put("primaryType", lenderReferenceDto.getPrimaryType());


					EmailRequest emailRequest = null;
					if (lenderReferenceDto.getUserInvite().equalsIgnoreCase("NO")) {
						emailRequest = new EmailRequest(new String[] { refereeEmailId }, emailTemplateName,
								templateContext, mailSubject, displayerName);
					} else {
						emailRequest = new EmailRequest(new String[] { refereeEmailId },
								new String[] { referrerEmailId }, emailTemplateName, templateContext, mailSubject,
								displayerName);

					}
					EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
					if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
						try {

							throw new MessagingException(emailResponseFromService.getErrorMessage());
						} catch (MessagingException e1) {

							e1.printStackTrace();
						}
					}

				} else if (lenderReferenceDto.getMailContent() != null) {
					String mailContent1 = lenderReferenceDto.getMailContent();
					String emailTemplateName = null;

					User user = userRepo.findById(referrerId).get();
					String type = null;

					String citizen = null;

					if (lenderReferenceDto.getPrimaryType() != null) {
						type = lenderReferenceDto.getPrimaryType();
					} else {
						type = user.getPrimaryType().toString();
					}
					if (type.equalsIgnoreCase(User.PrimaryType.LENDER.toString())) {

						emailTemplateName = "lender-reference-edited.template";

						if (lenderReferenceDto.getCitizenType() != null) {
							citizen = lenderReferenceDto.getCitizenType();
						} else {
							citizen = user.getCitizenship().toString();
						}
						if (citizen.equalsIgnoreCase(User.Citizenship.NRI.toString())) {
							templateContext.put("redirectionLink", linkForNRI);
						} else {
							templateContext.put("redirectionLink", redirectionLink);
						}

						templateContext.put("referrerId", "LR" + referrerId);

					} else {

						emailTemplateName = "borrower-reference-edited.template";
						templateContext.put("redirectionLinkForBorrower", redirectionLinkForBorrower);

					}
					templateContext.put("CurrentDate", expectedCurrentDate);
					templateContext.put("referrername", referrername);
					templateContext.put("refereeName", refereeName);
					templateContext.put("referrerMobileNumber", referrerMobileNumber);
					templateContext.put("referrerEmailId", referrerEmailId);
					templateContext.put("mailContent1", mailContent);


					EmailRequest emailRequest = null;
					if (lenderReferenceDto.getUserInvite().equalsIgnoreCase("NO")) {
						emailRequest = new EmailRequest(new String[] { refereeEmailId }, emailTemplateName,
								templateContext, mailSubject, displayerName);
					} else {
						emailRequest = new EmailRequest(new String[] { refereeEmailId },
								new String[] { referrerEmailId }, emailTemplateName, templateContext, mailSubject,
								displayerName);

					}

					System.out.println("refereeEmailId:..." + refereeEmailId + "emailTemplateName:  "
							+ emailTemplateName + "templateContext" + templateContext + "mailSubject::" + mailSubject
							+ "displayerName" + displayerName);

					EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);

					
					if (emailResponseFromService.getStatus() == EmailResponse.Status.SUCCESS) {

						logger.info("Suceess Mail sent to Users:" + email);
						suceessCount++;
					} else {
						continue;
					}

				}

				// only for firstTime Referrer get this mail when invite a referee
				Integer countNo = lenderReferenceDetailsRepo.finByTotalCount(referrerId);
				
				if (countNo == 0) {
					try {

						String directoryPath = referralBonusFaq;
						logger.info("directoryPath@@@@@:::::" + directoryPath);

						

						TemplateContext context1 = new TemplateContext();
						context1.put("redirectionLinkReferrer", redirectionLinkReferrer);
						context1.put("referrerEmailId", referrerEmailId);
						context1.put("currentDate", expectedCurrentDate);
						context1.put("referrername", referrername);
						context1.put("referralBonusFaq", referralBonusFaq);
						context1.put("referrerMobileNumber", referrerMobileNumber);

						String displayerNam = "Oxy Referral Calcualtion Flow";
						String emailTemplateNam = "Lender_refCalculation.template";
						logger.info("emailTemplateNam@@@:::" + emailTemplateNam);

						EmailRequest emailRequest1 = new EmailRequest(
								new String[] { referrerEmailId, "vijaydasari060@gmail.com" }, emailTemplateNam,
								context1, mailSubject, displayerNam, new String[] { directoryPath });

						EmailResponse emailResponseFromServic = emailService.sendEmail(emailRequest1);

						logger.info("emailResponseFromServic@@@@@@@@@@@::"
								+ emailResponseFromServic.getStatus().toString());

						if (emailResponseFromServic.getStatus() == EmailResponse.Status.FAILED) {
							try {

								throw new MessagingException(emailResponseFromServic.getErrorMessage());
							} catch (MessagingException e1) {

								e1.printStackTrace();
							}
						}
					} catch (Exception ex) {
						logger.error(ex.getMessage(), ex);
					}
				}

				lenderReferenceDetailsRepo.save(referenceDetails);
				ref.add(referenceDetails);
				refereeCount = refereeCount + 1;
			}

		}
		lenderReferenceDetailsRepo.saveAll(ref);
		int totalCount = 0; // Initialize count variable
		int userId = lenderReferenceDto.getReferrerId();
		try {
			LenderFavouriteUsers favUsers = lenderFavouriteUsersRepo.findbyLenderUserId(userId);
			String contacts = "";
			if (favUsers != null) {
				contacts = favUsers.getLenderGmailContacts();
			}

			ObjectMapper mapper = new ObjectMapper();
			List<String> strings = mapper.readValue(contacts, List.class);

			for (String x : strings) {
				EmailAddressResponse addressresponse = new EmailAddressResponse();
				String[] details1 = x.split(":");

				// Increment count by the number of elements in details1
				totalCount += details1.length;
			}

			logger.info("Total number of records present: " + totalCount);
		} catch (IOException e) {

			e.printStackTrace();
		}
		long inviteCount = 0;
		long lentCount = 0;
		long registerCount = 0;
		List<LenderReferenceDetails> lenderReferenceDetails = lenderReferenceDetailsRepo
				.gettingListOfReferees(lenderReferenceDto.getReferrerId());

		if (lenderReferenceDetails != null && !lenderReferenceDetails.isEmpty()) {

			logger.info("lenderReferenceDetails size.." + lenderReferenceDetails.size());

			Map<String, Long> approveCountByStatus = lenderReferenceDetails.stream()
					.collect(Collectors.groupingBy(x -> x.getStatus().toString(), Collectors.counting()));

			inviteCount = approveCountByStatus.getOrDefault("Invited", 0L);
			lentCount = approveCountByStatus.getOrDefault("Lent", 0L);
			registerCount = approveCountByStatus.getOrDefault("Registered", 0L);

		}

		LocalDate currentDate = LocalDate.now();
		if (suceessCount > 0) {
		    stringBuilder.append("\n Invited Date: ").append(currentDate).append("\n ReferrerId: ")
		            .append(lenderReferenceDto.getReferrerId()).append("\n Referrer Name: ").append(referrerName)
		            .append("\n Referrer Email: ").append(referrerEmail).append("\n Referrer Mobile Number:")
		            .append(referrerMobile)

		            .append("\n New Invited Count: ").append(suceessCount)
		            .append("\n Total Gmail contacts downloaded: ").append(totalCount)
		            .append("\n Previously Invited Count: ").append(inviteCount).append("\n Lent Count: ")
		            .append(lentCount).append("\n Registered count: ").append(registerCount);
		}


		String message = "";
		if (stringBuilder.length() > 0 ) {
			message = "*Hii Team*\n" + "*Find Below Lender Gmail Invited Details:*\n" + stringBuilder.toString();

			whatappService.sendingWhatsappMessageWithNewInstance("120363281131327528@g.us", message);
		}

		response.setCountOfReferees(refereeCount);

		response.setStatus("Sucessfully invited.");

		logger.info("lenderReferenceDetails method ends.............");
		return response;
	}

	@Override
	public LenderReferenceResponse displayingRefereesInfo(Integer lenderId, PaginationRequestDto pageRequestDto) {

		logger.info("displayingRefereesInfo method starts.......................");

		List<LenderReferenceResponseDto> listOfReferenceDetails = new ArrayList<LenderReferenceResponseDto>();

		Integer pageNo = pageRequestDto.getPageNo();
		Integer pageSize = pageRequestDto.getPageSize();

		pageNo = (pageSize * (pageNo - 1));

		Integer countOfReferees = lenderReferenceDetailsRepo.countOfReferees(lenderId);

		List<LenderReferenceDetails> listOfReferees = lenderReferenceDetailsRepo.displayLenderRefereesInfo(lenderId,
				pageNo, pageSize);

		for (LenderReferenceDetails obj : listOfReferees) {
			LenderReferenceResponseDto details = new LenderReferenceResponseDto();
			details.setRefereeEmail(obj.getRefereeEmail());
			details.setRefereeId(obj.getRefereeId());
			details.setRefereeMobileNumber(obj.getRefereeMobileNumber());
			details.setRefereeName(obj.getRefereeName());

			Date referredOn = obj.getReferredOn();
			String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(referredOn);
			details.setReferredOn(modifiedDate);

			details.setReferrerId(obj.getReferrerId());
			details.setStatus(obj.getStatus().toString());

			Double referralBonus = lenderReferralBonusUpdatedRepo.getBonusAmount(obj.getRefereeId());
			details.setAmount(referralBonus == null ? 0.0 : referralBonus);

			listOfReferenceDetails.add(details);
		}

		LenderReferenceResponse response = new LenderReferenceResponse();

		response.setListOfLenderReferenceDetails(listOfReferenceDetails);
		response.setCountOfReferees(countOfReferees);

		logger.info("displayingRefereesInfo method ends.......................");

		return response;

	}

	@Override
	public LenderReferenceResponse gettingLenderRefereesRegisteredInfo(Integer lenderId,
			PaginationRequestDto pageRequestDto) {

		logger.info("gettingLenderRefereesRegisteredInfo method starts.......................");

		List<LenderReferenceResponseDto> listOfRefereesRegistered = new ArrayList<LenderReferenceResponseDto>();

		Integer pageNo = pageRequestDto.getPageNo();
		Integer pageSize = pageRequestDto.getPageSize();

		pageNo = (pageSize * (pageNo - 1));

		Integer countOfLenderRefereesRegistered = lenderReferenceDetailsRepo.gettingCountOfRefereesRegistered(lenderId);

		List<LenderReferenceDetails> listOfReferees = lenderReferenceDetailsRepo
				.gettingLenderRefereesRegistered(lenderId, pageNo, pageSize);

		for (LenderReferenceDetails obj : listOfReferees) {
			LenderReferenceResponseDto reference = new LenderReferenceResponseDto();

			reference.setRefereeEmail(obj.getRefereeEmail());

			reference.setRefereeNewId(obj.getRefereeId() == 0 ? null : "LR" + obj.getRefereeId());

			reference.setRefereeMobileNumber(obj.getRefereeMobileNumber());
			reference.setRefereeName(obj.getRefereeName());

			Date referredOn = obj.getReferredOn();
			String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(referredOn);
			reference.setReferredOn(modifiedDate);

			reference.setReferrerId(obj.getReferrerId());
			reference.setStatus(obj.getStatus().toString());

			Double referralBonus = lenderReferralBonusUpdatedRepo.getBonusAmount(obj.getRefereeId());
			reference.setAmount(referralBonus == null ? 0.0 : referralBonus);

			listOfRefereesRegistered.add(reference);
		}

		LenderReferenceResponse refereesRegisteredResponse = new LenderReferenceResponse();

		refereesRegisteredResponse.setListOfLenderReferenceDetails(listOfRefereesRegistered);

		refereesRegisteredResponse.setCountOfReferees(countOfLenderRefereesRegistered);

		Double sumOfAmountEarned = lenderReferralBonusUpdatedRepo.findingSumOfAmountEarnedByReferrer(lenderId);
		Integer amountEarned;
		if (sumOfAmountEarned != null) {

			DecimalFormat df = new DecimalFormat("#");
			amountEarned = Integer.parseInt(df.format(sumOfAmountEarned));

			refereesRegisteredResponse.setTotalAmountEarned(amountEarned);
		} else {
			sumOfAmountEarned = 0.0;
			DecimalFormat df = new DecimalFormat("#");
			amountEarned = Integer.parseInt(df.format(sumOfAmountEarned));
			refereesRegisteredResponse.setTotalAmountEarned(amountEarned);
		}

		logger.info("gettingLenderRefereesRegisteredInfo method ends.......................");

		return refereesRegisteredResponse;

	}

	@Override
	public LenderMailResponse showingMailContentToLender() {

		int currentUser = authorizationService.getCurrentUser();
		User lenderUser = userRepo.findById(currentUser).get();

		String referrerName = lenderUser.getPersonalDetails().getFirstName() + ""
				+ lenderUser.getPersonalDetails().getLastName();

		String mailSubject = referrerName + " || OxyLoans - Interesting FinTech Platform";

		String message = "Greetings from " + referrerName + "! I am an existing " + lenderUser.getPrimaryType()
				+ " in OxyLoans.com! OXYLOANS.com– RBI Approved Peer 2 Peer Lending Platform. OxyLoans enables every individual to lend money like a bank."
				+ "\n\n";

		String mailContent = message
				+ "I am investing multiples of INR 50,000 in numerous deals, distributing risk, and maximizing monthly profit. I am earning a profit of on average 1.75% per montht. My friends from MNCs like TCS, Microsoft, IBM, Cap Gemini, Yash, etc have started their investment journey with small amounts (like 50k) and currently investing in lakhs. If this interests you and want to earn like them then OxyLoans is the connection."
				+ "\n\n";

		String bottomOfTheMail = "I am sending this e-mail on my own interest, I have experienced good response directly from the founder and respective team. Please feel free to reach me on "
				+ lenderUser.getMobileNumber();

		LenderMailResponse response = new LenderMailResponse();

		response.setMailSubject(mailSubject);
		response.setMailContent(mailContent);
		response.setBottomOfTheMail(bottomOfTheMail);

		return response;
	}

	@Override
	public FileResponse uploadExcelSheets(List<KycFileRequest> files, String date) {
		String fileName = "";
		String uploadType = "";
		FileResponse response = new FileResponse();
		try {
			FileRequest fileRequest = new FileRequest();
			for (KycFileRequest kyc : files) {

				if (kyc.getKycType().equals(KycFileRequest.KycType.NORMALEMISPENDING)) {

					InputStream fileStream = kyc.getFileInputStream();
					fileRequest.setInputStream(fileStream);
					fileRequest.setFileName(date + "normalEmisPending.xlsx");
					fileRequest.setFilePrifix(kyc.getKycType().name());
					fileName = date + "normalEmisPending.xlsx";
					uploadType = "NORMALEMISPENDING";
				} else {
					if (kyc.getKycType().equals(KycFileRequest.KycType.CRITICALEMISPENDING)) {

						InputStream fileStream = kyc.getFileInputStream();
						fileRequest.setInputStream(fileStream);
						fileRequest.setFileName(date + "criticalEmisPending.xlsx");
						fileRequest.setFilePrifix(kyc.getKycType().name());
						fileName = date + "criticalEmisPending.xlsx";
						uploadType = "CRITICALEMISPENDING";
					}
					if (kyc.getKycType().equals(KycFileRequest.KycType.LENDERINTEREST)) {

						InputStream fileStream = kyc.getFileInputStream();
						fileRequest.setInputStream(fileStream);
						fileRequest.setFileName(date + "lenderinterest.xls");
						fileRequest.setFilePrifix(kyc.getKycType().name());
						fileName = date + "lenderinterest.xls";
						uploadType = "LENDERINTEREST";
					}
					if (kyc.getKycType().equals(KycFileRequest.KycType.LENDERPRINCIPAL)) {

						InputStream fileStream = kyc.getFileInputStream();
						fileRequest.setInputStream(fileStream);
						fileRequest.setFileName(date + "lenderprincipal.xls");
						fileRequest.setFilePrifix(kyc.getKycType().name());
						fileName = date + "lenderprincipal.xls";
						uploadType = "LENDERPRINCIPAL";
					}
					if (kyc.getKycType().equals(KycFileRequest.KycType.LENDEREMI)) {

						InputStream fileStream = kyc.getFileInputStream();
						fileRequest.setInputStream(fileStream);
						fileRequest.setFileName(date + "lenderemi.xls");
						fileRequest.setFilePrifix(kyc.getKycType().name());
						fileName = date + "lenderemi.xls";
						uploadType = "LENDEREMI";
					}
					if (kyc.getKycType().equals(KycFileRequest.KycType.DISBURSEDLOANS)) {

						InputStream fileStream = kyc.getFileInputStream();
						fileRequest.setInputStream(fileStream);
						fileRequest.setFileName(date + "disbursedloans.xls");
						fileRequest.setFilePrifix(kyc.getKycType().name());
						fileName = date + "disbursedloans.xls";
						uploadType = "DISBURSEDLOANS";
					}
					if (kyc.getKycType().equals(KycFileRequest.KycType.LENDERWITHDRAW)) {

						InputStream fileStream = kyc.getFileInputStream();
						fileRequest.setInputStream(fileStream);
						fileRequest.setFileName(date + "lenderwithdraw.xls");
						fileRequest.setFilePrifix(kyc.getKycType().name());
						fileName = date + "lenderwithdraw.xls";
						uploadType = "LENDERWITHDRAW";
					}
					if (kyc.getKycType().equals(KycFileRequest.KycType.IDFCPAYMENTS)) {

						InputStream fileStream = kyc.getFileInputStream();
						fileRequest.setInputStream(fileStream);
						fileRequest.setFileName(date + "idfcpayments.xls");
						fileRequest.setFilePrifix(kyc.getKycType().name());
						fileName = date + "idfcpayments.xls";
						uploadType = "IDFCPAYMENTS";
					}
					if (kyc.getKycType().equals(KycFileRequest.KycType.LENDERRELEND)) {

						InputStream fileStream = kyc.getFileInputStream();
						fileRequest.setInputStream(fileStream);
						fileRequest.setFileName(date + "lenderrelend.xls");
						fileRequest.setFilePrifix(kyc.getKycType().name());
						fileName = date + "lenderrelend.xls";
						uploadType = "LENDERRELEND";
					}
					if (kyc.getKycType().equals(KycFileRequest.KycType.LENDERPOOLINGINTEREST)) {

						InputStream fileStream = kyc.getFileInputStream();
						fileRequest.setInputStream(fileStream);
						fileRequest.setFileName(date + "lenderpoolinginterest.xls");
						fileRequest.setFilePrifix(kyc.getKycType().name());
						fileName = date + "lenderpoolinginterest.xls";
						uploadType = "LENDERPOOLINGINTEREST";
					}
					if (kyc.getKycType().equals(KycFileRequest.KycType.POOLINGLENDERS)) {
						InputStream fileStream = kyc.getFileInputStream();
						fileRequest.setInputStream(fileStream);
						fileRequest.setFileName(date + "poolinglenders.xls");
						fileRequest.setFilePrifix(kyc.getKycType().name());
						fileName = date + "poolinglenders.xls";
						uploadType = "POOLINGLENDERS";
					}

					if (kyc.getKycType().equals(KycFileRequest.KycType.LENDERREFERRALBONUS)) {

						if (date.equalsIgnoreCase("Approved")) {

							SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
							String modified = format.format(new Date());
							fileName = "OXYLR1_OXYLR1UPLD_" + modified + "RefBN" + ".xls";

							File f = new File(iciciFilePathBeforeApproval + fileName);

							logger.info("FILE " + f);

							try {

								InputStream file = kyc.getFileInputStream();

								logger.info("FileInput........ " + file);

								FileOutputStream outFile = new FileOutputStream(f);

								byte[] buf = new byte[1024];
								int bytesRead;
								try {
									while ((bytesRead = file.read(buf)) > 0) {
										outFile.write(buf, 0, bytesRead);

										logger.info("Data...................");
									}

									uploadType = "Successfully Uploaded";

								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								logger.info("FileOutPut........ " + outFile);

								fileManagementService.putFile(fileRequest);

								FileResponse getFile = fileManagementService.getFile(fileRequest);

								String url = getFile.getDownloadUrl();

								String[] downloadUrl = url.split(Pattern.quote("?"));

								LenderCmsPayments fileInfo = lenderCmsPaymentsRepo.findbyFileName(fileName);
								if (fileInfo != null) {
									fileInfo.setFileName(f.getName());
									fileInfo.setDealId(0);
									fileInfo.setS3_download_link(downloadUrl[0]);
									fileInfo.setFileExecutionsSatus(FileExecutionStatus.INITIATED);
									lenderCmsPaymentsRepo.save(fileInfo);

								}

								String mailSubject = "Referral Bonus Sheet Moving To Before Approval";

								Calendar current = Calendar.getInstance();
								TemplateContext templateContext = new TemplateContext();
								String expectedCurrentDate = expectedDateFormat.format(current.getTime());

								templateContext.put("currentDate", expectedCurrentDate);
								EmailRequest emailRequest = new EmailRequest(
										new String[] { "tadakamallaanusha@gmail.com" }, "lenderReferenceInfo.template",
										templateContext, mailSubject, new String[] { f.getAbsolutePath() });

								EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
								if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
									try {
										throw new MessagingException(emailResponseFromService.getErrorMessage());
									} catch (MessagingException e2) {

										e2.printStackTrace();
									}
								}

							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

					}
				}
				fileManagementService.putFile(fileRequest);
			}

			response.setFileName(fileName);
			response.setUploadedDate(date);
			response.setUploadedType(uploadType);

		} catch (Exception e) {
			logger.info("Exception in uploadDisbursedLoansExcelSheet method ");
		}

		return response;

	}

	@Override
	public FileResponse downloadExcelSheets(String date, String kyc) {
		String downloadUrl = null;
		FileResponse fileResponse = new FileResponse();
		try {
			FileRequest fileRequest = new FileRequest();
			if (kyc.equals(KycFileRequest.KycType.NORMALEMISPENDING.toString())) {

				String fileName = date + "normalEmisPending.xlsx";
				fileRequest.setFileName(fileName);
				fileRequest.setFilePrifix("NORMALEMISPENDING");

			} else {
				if (kyc.equals(KycFileRequest.KycType.CRITICALEMISPENDING.toString())) {

					String fileName = date + "criticalEmisPending.xlsx";
					fileRequest.setFileName(fileName);
					fileRequest.setFilePrifix("CRITICALEMISPENDING");

				}
				if (kyc.equals(KycFileRequest.KycType.LENDERINTEREST.toString())) {

					String fileName = date + "lenderinterest.xls";
					fileRequest.setFileName(fileName);
					fileRequest.setFilePrifix("LENDERINTEREST");

				}
				if (kyc.equals(KycFileRequest.KycType.LENDERPRINCIPAL.toString())) {

					String fileName = date + "lenderprincipal.xls";
					fileRequest.setFileName(fileName);
					fileRequest.setFilePrifix("LENDERPRINCIPAL");
					FileResponse file = fileManagementService.getFile(fileRequest);

				}
				if (kyc.equals(KycFileRequest.KycType.LENDEREMI.toString())) {

					String fileName = date + "lenderemi.xls";
					fileRequest.setFileName(fileName);
					fileRequest.setFilePrifix("LENDEREMI");

				}
				if (kyc.equals(KycFileRequest.KycType.DISBURSEDLOANS.toString())) {

					String fileName = date + "disbursedloans.xls";
					fileRequest.setFileName(fileName);
					fileRequest.setFilePrifix("DISBURSEDLOAN");

				}
				if (kyc.equals(KycFileRequest.KycType.LENDERWITHDRAW.toString())) {

					String fileName = date + "lenderwithdraw.xls";
					fileRequest.setFileName(fileName);
					fileRequest.setFilePrifix("LENDERWITHDRAW");

				}
				if (kyc.equals(KycFileRequest.KycType.IDFCPAYMENTS.toString())) {

					String fileName = date + "idfcpayments.xls";
					fileRequest.setFileName(fileName);
					fileRequest.setFilePrifix("IDFCPAYMENTS");

				}
				if (kyc.equals(KycFileRequest.KycType.LENDERRELEND.toString())) {

					String fileName = date + "lenderrelend.xls";
					fileRequest.setFileName(fileName);
					fileRequest.setFilePrifix("LENDERRELEND");

				}
				if (kyc.equals(KycFileRequest.KycType.LENDERPOOLINGINTEREST.toString())) {

					String fileName = date + "lenderpoolinginterest.xls";
					fileRequest.setFileName(fileName);
					fileRequest.setFilePrifix("LENDERPOOLINGINTEREST");

				}
				if (kyc.equals(KycFileRequest.KycType.POOLINGLENDERS.toString())) {

					String fileName = date + "poolinglenders.xls";
					fileRequest.setFileName(fileName);
					fileRequest.setFilePrifix("POOLINGLENDERS");

				}

			}
			FileResponse file = fileManagementService.getFile(fileRequest);
			if (file != null) {
				String[] str = file.getDownloadUrl().split(Pattern.quote("?"));
				downloadUrl = str[0];
				fileResponse.setDownloadUrl(downloadUrl);

			}

		} catch (Exception e) {
			logger.info("Exception in downloadDisbursedLoansExcelSheet method ");
		}
		return fileResponse;

	}

	@Override
	public List<EmailAddressResponse> getContactsFromGmailAccount(Integer userId, GmailSignInResponse gmailResponse)
			throws IOException, GeneralSecurityException, InterruptedException {
		
		logger.info("fetching Gmail Contacts started");

			List<EmailAddressResponse> mailresponse = new ArrayList<>();

			LenderFavouriteUsers favouriteUsers = lenderFavouriteUsersRepo.findbyLenderUserId(userId);

			LenderFavouriteUsers usr = new LenderFavouriteUsers();

			int count = 0;

			JSONArray list = new JSONArray();

			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

			String url = "";

			if (gmailResponse.getUserType().equalsIgnoreCase("lender")) {
				url = redirectUri;
			}

			if (gmailResponse.getUserType().equalsIgnoreCase("borrower")) {
				url = redirectUri;
			}

			if (gmailResponse.getUserType().equalsIgnoreCase("Partner")) {
				url = gmailPartnerRedirectUri;
			}

			logger.info("url 2 " + url);

			gmailResponse.setGmailRedirectUri(url);

			PeopleService service = new PeopleService.Builder(HTTP_TRANSPORT, JSON_FACTORY,
					getGmailCredentials(gmailResponse)).setApplicationName(APPLICATION_NAME).build();

			ListOtherContactsResponse response = service.otherContacts().list().setReadMask("names,emailAddresses").execute();
			String nextPageToken = response.getNextPageToken();
			List<Person> allContacts = new ArrayList<>();

			// If there's no next page token, it means there's only one page of data
			if (nextPageToken == null) {
			    List<Person> connections = response.getOtherContacts();
			    if (connections != null) {
			        allContacts.addAll(connections);
			    }
			} else {
			    // Fetch all pages of contacts
			    while (nextPageToken != null) {
			        List<Person> connections = response.getOtherContacts();
			        if (connections != null) {
			            allContacts.addAll(connections);
			        }
			        
			        ListOtherContactsResponse nextPageResponse = service.otherContacts().list()
			                .setPageToken(nextPageToken)
			                .setReadMask("names,emailAddresses")
			                .execute();

			        if (nextPageResponse != null) {
			            List<Person> nextPageContacts = nextPageResponse.getOtherContacts();
			            if (nextPageContacts != null) {
			                allContacts.addAll(nextPageContacts);
			            }
			            
			            nextPageToken = nextPageResponse.getNextPageToken();
			        }
			    }
			}

			String personName = "";

			String mailAddress = "";

			logger.info("allContacts size ..." + allContacts.size());

			for (Person person : allContacts) {

				EmailAddressResponse res = new EmailAddressResponse();

				List<Name> names = person.getNames();

				List<EmailAddress> mail = person.getEmailAddresses();

				if (names != null && names.size() > 0 && !names.isEmpty()) {

					personName = person.getNames().get(0).getDisplayName();

				} else {

					personName = "";

				}

				if (mail != null && mail.size() > 0 && !mail.isEmpty()) {

					String address = mail.get(0).getValue();

					String[] split = address.split("@");

					if (split.length >= 2) {
					    mailAddress = mail.get(0).getValue();
					} else {
					    mailAddress = "";
					}


				}

				if (!mailAddress.equals("")) {

					res.setContactName(personName);

					res.setEmailAddress(mailAddress);

					mailresponse.add(res);

					list.add(personName + ":" + mailAddress);

				}

			}

			String jsonString = list.toString();

			count = list.size();

			if (!gmailResponse.getUserType().equalsIgnoreCase("PARTNER")) {

				if (favouriteUsers != null) {

					favouriteUsers.setLenderGmailContacts(jsonString);
					favouriteUsers.setGmailSize(count);

					logger.info("contacts Updated");

					lenderFavouriteUsersRepo.save(favouriteUsers);

				} else {

					usr.setLenderUserId(userId);

					usr.setBorrowerUserId(0);

					usr.setLenderGmailContacts(jsonString);

					usr.setGmailSize(count);

					logger.info("contacts count downloaded.." + count);

					logger.info("contacts Inserted");

					lenderFavouriteUsersRepo.save(usr);
				}

			} else {
				OxyUserType oxyUserType = oxyUserTypeRepo.findById(userId).get();

				if (oxyUserType != null) {
					oxyUserType.setGmailContacts(jsonString);
					oxyUserTypeRepo.save(oxyUserType);
				}
			}

			File tokenFile = new File("tokens//StoredCredential");

			if (tokenFile.exists()) {

				tokenFile.delete();

			} else {

				logger.info("token credential not deleted !!!!!!!!!!!!!!!!!");
			}

			StringBuilder stringBuilder = new StringBuilder();
			LocalDate currentDate = LocalDate.now(); // Changed to LocalDate.now()

			User user = userRepo.findByIdNum(userId);

			String referrerName = user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName();
			String referrerMobileNumber = user.getMobileNumber();
			String referrerEmailId = user.getEmail();

			if (count > 0) {
				stringBuilder.append("\n  Date: ").append(currentDate).append("\n Lender Id: ").append(userId)
						.append("\n Lender Name: ").append(referrerName).append("\n Lender Email: ").append(referrerEmailId)
						.append("\n Lender Mobile Number: ").append(referrerMobileNumber)
						.append("\n Total Gmail contacts downloaded: ").append(count); 
			}

			String message = "*Hii Team*\n" + "*Find Below Lender Gmail Downloaded Details:*\n" + stringBuilder.toString();

			if (stringBuilder.length() > 0) {

				if (!user.isTestUser()) {
					whatappService.sendingWhatsappMessageWithNewInstance("120363281131327528@g.us", message);
				} else {
					whatappService.sendingWhatsappMessageWithNewInstance("917702795895-1630419500@g.us", message);
				}
			}

			return mailresponse;
}

	@Override
	public GmailSignInResponse getGmailAuthorization(String type, String userType, String projectType)
			throws IOException, GeneralSecurityException, InterruptedException {

		logger.info("Gmail Authorization started");
		// Load client secrets.
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		GmailSignInResponse signInResponse = new GmailSignInResponse();
		GoogleClientSecrets clientSecrets = new GoogleClientSecrets();

		Details details = new Details();
		details.setClientId(clientId);
		details.setClientSecret(clientSecret);
		clientSecrets.setWeb(details);

		String url = "";

		List<String> scopes = null;
		if (type.equalsIgnoreCase("gmailcontacts")) {
			scopes = Arrays.asList(PeopleServiceScopes.CONTACTS_OTHER_READONLY);

			if (projectType.equalsIgnoreCase("OXYLOANS")) {

				logger.info("OXYLOANS");
				;

				if (userType.equalsIgnoreCase("borrower")) {
					url = redirectUri;
				}

				if (userType.equalsIgnoreCase("lender")) {
					url = redirectUri;
				}

				if (userType.equalsIgnoreCase("partner")) {
					url = gmailPartnerRedirectUri;
				}
				signInResponse.setProjectType("OXYLOANS");

			} else {

				if (userType.equalsIgnoreCase("borrower")) {
					url = redirectUrl;
				}

				if (userType.equalsIgnoreCase("lender")) {
					url = redirectUrl;
				}
				signInResponse.setProjectType("REACT");
				logger.info("REACT");
			}

		} else if (type.equalsIgnoreCase("spreadsheet")) {
			scopes = Arrays.asList(SheetsScopes.SPREADSHEETS);
			url = spreadSheetRedirectUrl;
		}

		logger.info("url " + url);

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, scopes)
				.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
				.setAccessType("offline").build();

		Credential credential = flow.loadCredential("userId");

		AuthorizationCodeRequestUrl authorizationUrl = flow.newAuthorizationUrl().setRedirectUri(url);
		// GmailSignInResponse signInResponse = new GmailSignInResponse();

		signInResponse.setSignInUrl(authorizationUrl.build());

		logger.info("Gmail Authorization Ends");
		return signInResponse;

	}

	private Credential getGmailCredentials(GmailSignInResponse gmailResponse)

			throws IOException, GeneralSecurityException, InterruptedException {
		// Load client secrets.
		logger.info("Gmail Credentials method started");
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

		GoogleClientSecrets clientSecrets = new GoogleClientSecrets();
		Details details = new Details();
		details.setClientId(clientId);
		details.setClientSecret(clientSecret);
		clientSecrets.setWeb(details);

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES)
				.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
				.setAccessType("offline").build();

		Credential credential = flow.loadCredential("userId");

		TokenResponse response = null;
		try {
			response = flow.newTokenRequest(gmailResponse.getGmailCode())
					.setRedirectUri(gmailResponse.getGmailRedirectUri()).execute();
		} catch (Exception ex) {
			throw new DataFormatException("Invalid Code", ErrorCodes.DATA_FETCH_ERROR);
		}
		logger.info("Gmail Credentials method ends");
		return flow.createAndStoreCredential(response, "userId");

	}

	@Override
	public List<EmailAddressResponse> getLenderStoredEmailContacts(int userId)
			throws JsonParseException, JsonMappingException, IOException {
		
		LenderFavouriteUsers favUsers = lenderFavouriteUsersRepo.findbyLenderUserId(userId);
		String contacts = "";
		if (favUsers != null) {
			contacts = favUsers.getLenderGmailContacts();
		} else {
			OxyUserType oxyUserType = oxyUserTypeRepo.findById(userId).get();
			if (oxyUserType != null) {
				contacts = oxyUserType.getGmailContacts()!= null ? oxyUserType.getGmailContacts() : " no Value" ;
			}

		}

		List<EmailAddressResponse> response = new ArrayList<EmailAddressResponse>();

		
		ObjectMapper mapper = new ObjectMapper();
		
		if(contacts!=null && !contacts.isEmpty()) {
		List<String> strings = mapper.readValue(contacts, List.class);

		for (String details : strings) {
			EmailAddressResponse addressresponse = new EmailAddressResponse();
			String[] details1 = details.split(":");

			addressresponse.setContactName(details1[0].equals("") ? "" : details1[0]);
			addressresponse.setEmailAddress(details1[1]);

			response.add(addressresponse);
		}}

		return response;
	}

	@Override
	public LenderReferenceAmountResponseDto gettingLenderReferenceAmountDetails(String refereeId1,
			PaginationRequestDto pageRequestDto) {
		logger.info("gettingLenderReferenceAmountDetails method starts.................................");
		Integer pageNo = pageRequestDto.getPageNo();
		Integer pageSize = pageRequestDto.getPageSize();

		pageNo = (pageSize * (pageNo - 1));

		String id = refereeId1.substring(2);

		Integer refereeId = Integer.parseInt(id);

		LenderReferenceAmountResponseDto referenceResponse = new LenderReferenceAmountResponseDto();

		List<Object[]> listOfReferees = lenderReferenceDetailsRepo.getListOfReferees(refereeId);

		Integer countOfDisbursements = lenderReferenceDetailsRepo.getCountOfDisbursments(refereeId, pageNo, pageSize);

		List<LenderReferenceAmountResponse> details = new ArrayList<LenderReferenceAmountResponse>();

		Double totalSumOfDisbursementAmount = 0.0;

		for (Object[] obj : listOfReferees) {

			LenderReferenceAmountResponse response = new LenderReferenceAmountResponse();

			response.setDisbursmentAmount((Double) obj[0]);

			Double amount = (Double) obj[0];

			totalSumOfDisbursementAmount += amount;

			response.setDisbursmentDate(obj[1].toString());

			details.add(response);
		}
		referenceResponse.setLenderReferenceAmountResponse(details);
		referenceResponse.setSumOfDisbursementAmount(totalSumOfDisbursementAmount);
		referenceResponse.setCountOfDisbursments(countOfDisbursements);
		logger.info("gettingLenderReferenceAmountDetails method ends.................................");
		return referenceResponse;

	}

	@Override
	public List<LenderReferralPaymentStatus> displayingRefereePaymentStatus(String refereeId1) {

		logger.info("displayingRefereePaymentStatus method starts.......................");

		List<LenderReferralPaymentStatus> response = new ArrayList<LenderReferralPaymentStatus>();

		String id = refereeId1.substring(2);

		Integer refereeId = Integer.parseInt(id);

		List<Object[]> amountPaymentStatus = lenderReferralBonusUpdatedRepo.getReferralPaymentStatus(refereeId);

		if (amountPaymentStatus != null) {
			for (int i = 0; i < amountPaymentStatus.size(); i++) {
				LenderReferralPaymentStatus referralPaymentStatus = new LenderReferralPaymentStatus();

				Object[] object = amountPaymentStatus.get(i);

				referralPaymentStatus.setsNo(i + 1);
				referralPaymentStatus.setRefereeId((Integer) object[0] == null ? 0 : (Integer) object[0]);
				referralPaymentStatus.setReferrerId((Integer) object[1] == null ? 0 : (Integer) object[1]);

				if ((Double) object[9] != 0) {

					referralPaymentStatus.setAmount((Double) object[9] == null ? 0.0 : (Double) object[9]);

				} else {

					referralPaymentStatus.setAmount((Double) object[2] == null ? 0.0 : (Double) object[2]);
				}

				referralPaymentStatus.setPaymentStatus(object[3].toString() == null ? "" : object[3].toString());
				if (object[4] == null) {
					referralPaymentStatus.setTransferredOn("");
				} else {
					referralPaymentStatus.setTransferredOn(object[4].toString());
				}

				Integer refereeNewId = (Integer) object[0];
				User user = userRepo.findById(refereeNewId).get();

				if (user != null) {
					if (user.getPrimaryType().equals(User.PrimaryType.LENDER)) {
						referralPaymentStatus.setRefereeNewId("LR" + refereeNewId);
					} else {
						referralPaymentStatus.setRefereeNewId("BR" + refereeNewId);
					}
				}

				referralPaymentStatus.setDealId((Integer) object[6] == null ? 0 : (Integer) object[6]);

				Integer dealId;
				if (object[6] != null) {
					dealId = (Integer) object[6];
				} else {
					dealId = 0;
				}
				if (object[7] != null) {
					referralPaymentStatus.setParticipatedOn(object[7].toString());
				} else {
					String participatedOn = oxyLendersAcceptedDealsRepo.findParticipatedDateBasedOnDealId(dealId,
							(Integer) object[0]);

					referralPaymentStatus.setParticipatedOn(participatedOn == null ? "" : participatedOn);
				}

				if ((Double) object[8] != 0.0) {
					referralPaymentStatus.setParticipatedAmount((Double) object[8]);
				} else {
					List<Object[]> dealInfo = oxyLendersAcceptedDealsRepo
							.findingParticipatedAmountBasedOnDealId(refereeId, dealId);
					if (dealInfo != null) {
						for (Object[] obj : dealInfo) {
							Double updationParticipatedAmount = oxyLendersAcceptedDealsRepo
									.findingTotalParticipatedAmountUpdated(refereeId, dealId);

							if (updationParticipatedAmount == null) {
								updationParticipatedAmount = 0.0;
							}
							Double participatedAmount = (Double) obj[0];

							Double totalParticipatedAmount = updationParticipatedAmount + participatedAmount;

							referralPaymentStatus.setParticipatedAmount(
									totalParticipatedAmount == null ? 0.0 : totalParticipatedAmount);
						}
					}

				}

				String dealName = oxyBorrowersDealsInformationRepo.findingDealNameBasedOnId(dealId);
				if (dealName != null) {
					referralPaymentStatus.setDealName(dealName);
				} else {
					referralPaymentStatus.setDealName("");
				}

				referralPaymentStatus.setUpdatedBonus((Double) object[9] == null ? 0.0 : (Double) object[9]);

				String remarks = "";

				if (object[10] != null) {
					remarks = object[10].toString();
				}

				referralPaymentStatus.setRemarks(remarks);

				response.add(referralPaymentStatus);

			}
		}

		logger.info("displayingRefereePaymentStatus method ends.......................");

		return response;
	}

	@Override
	public BorrowersLoanOwnerInformation displayingLoanOwnerNames(PaginationRequestDto pageRequestDto) {

		logger.info("displayingLoanOwnerNames method starts................................");

		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();

		Integer lenderId = user.getId();

		Integer pageNo = pageRequestDto.getPageNo();
		Integer pageSize = pageRequestDto.getPageSize();

		pageNo = (pageSize * (pageNo - 1));
		List<BorrowersLoanOwnerNames> lenderLoanInfo = new ArrayList<BorrowersLoanOwnerNames>();
		List<String> listOfOwners = userRepo.findingLoanOwnersForLender(lenderId, pageNo, pageSize);

		for (String obj : listOfOwners) {
			BorrowersLoanOwnerNames lenderInfo = new BorrowersLoanOwnerNames();

			lenderInfo.setLoanOwner(obj == null ? "Other" : obj);

			lenderLoanInfo.add(lenderInfo);

		}

		BorrowersLoanOwnerInformation response = new BorrowersLoanOwnerInformation();
		response.setBorrowersLoanOwnerNames(lenderLoanInfo);
		response.setLoanOwnersCount(listOfOwners.size());
		logger.info("displayingLoanOwnerNames method ends................................");

		return response;

	}

	@Override
	public BorrowersLoanOwnerInformation gettingLenderBorrowerOwner(String loanOwnerName,
			PaginationRequestDto pageRequestDto) {

		logger.info("gettingLenderBorrowerOwner method starts................................");

		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();

		Integer lenderId = user.getId();

		Integer pageNo = pageRequestDto.getPageNo();
		Integer pageSize = pageRequestDto.getPageSize();

		pageNo = (pageSize * (pageNo - 1));

		List<BorrowersLoanInfo> lenderLoanInfo = new ArrayList<BorrowersLoanInfo>();

		List<Object[]> listOfOwners;

		if (loanOwnerName.equalsIgnoreCase("Other")) {
			listOfOwners = userRepo.findingLenderBorrowerLoansNotBelongsToLoanOwner(lenderId, pageNo, pageSize);
		} else {

			listOfOwners = userRepo.findingLenderBorrowerLoanBasedOnLoanOwner(loanOwnerName, lenderId, pageNo,
					pageSize);
		}

		for (Object[] obj : listOfOwners) {
			BorrowersLoanInfo lenderInfo = new BorrowersLoanInfo();

			lenderInfo.setBorrowerId((Integer) obj[0]);
			lenderInfo.setFirstName(obj[1].toString());
			lenderInfo.setLastName(obj[2].toString());
			lenderInfo.setDisbursmentAmount((Double) obj[3]);
			lenderInfo.setDisbursmentDate(obj[4].toString());

			lenderLoanInfo.add(lenderInfo);

		}

		BorrowersLoanOwnerInformation response = new BorrowersLoanOwnerInformation();
		response.setListOfBorrowersMappedToLoanOwner(lenderLoanInfo);
		response.setLoanOwnersCount(listOfOwners.size());

		logger.info("gettingLenderBorrowerOwner method ends................................");

		return response;

	}

	@Override
	public PersonalDetailsResponseDto updatingWhatsAppNumberForExistingUser(Integer id,
			PersonalDetailsRequestDto userRequest) {

		logger.info("updatingWhatsAppNumberForExistingUser method starts...................");

		User user = userRepo.findById(id).get();

		PersonalDetails personalDetails = user.getPersonalDetails();

		if (personalDetails != null) {

			personalDetails.setWhatsAppNumber(userRequest.getWhatsAppNumber());

			user.setPersonalDetails(personalDetails);

			personalDetails.setUser(user);

			userRepo.save(user);
		}

		PersonalDetailsResponseDto response = new PersonalDetailsResponseDto();

		response.setWhatsAppNumber(userRequest.getWhatsAppNumber());

		logger.info("updatingWhatsAppNumberForExistingUser method ends...................");

		return response;

	}

	@Override
	public StatusResponseDto readingQueriesFromUsers(Integer userId, UserQueryDetailsRequestDto queryRequestDto) {

		logger.info("readingQueriesFromUsers method starts..........................");

		User user = userRepo.findById(userId).get();

		Integer userDocumentStatus = userDocumentStatusRepo.findByIdNum(queryRequestDto.getDocumentId());

		StatusResponseDto statusResponse = new StatusResponseDto();
		String newline = System.lineSeparator();
		if (!(queryRequestDto.getQuery().isEmpty())) {
			if (user != null) {

				UserQueryDetails queryDetails = null;
				if (queryRequestDto.getId() != null && queryRequestDto.getId() != 0) {
					queryDetails = userQueryDetailsRepo.findById(queryRequestDto.getId()).get();

					UserPendingQueries queryThread = new UserPendingQueries();

					queryThread.setTableId(queryDetails.getId());
					queryThread.setPendingComments(queryRequestDto.getQuery());
					queryThread.setRespondedOn(new Date());
					queryThread.setResolvedBy("User");
					queryThread.setRespondedBy(UserPendingQueries.RespondedBy.USER);

					if (userDocumentStatus != null) {
						queryDetails.setAdminDocumentId(userDocumentStatus);
					} else {
						queryDetails.setAdminDocumentId(0);
					}

					userPendingQueriesRepo.save(queryThread);

				} else {
					queryDetails = new UserQueryDetails();
					queryDetails.setQuery(queryRequestDto.getQuery());
					queryDetails.setUserId(user.getId());
					queryDetails.setEmail(queryRequestDto.getEmail() == null ? "" : queryRequestDto.getEmail());
					queryDetails.setMobileNumber(
							queryRequestDto.getMobileNumber() == null ? "" : queryRequestDto.getMobileNumber());
					queryDetails.setReceivedOn(new Date());

					if (userDocumentStatus != null) {
						queryDetails.setDocumentId(userDocumentStatus);
					} else {
						queryDetails.setDocumentId(0);
					}

					userQueryDetailsRepo.save(queryDetails);
				}

				String userName = user.getPersonalDetails().getFirstName() + " "
						+ user.getPersonalDetails().getLastName();
				String primaryType = null;
				if (user.getPrimaryType().equals(User.PrimaryType.LENDER)) {
					primaryType = "LR";
				} else {
					primaryType = "BR";
				}

				UserDocumentStatus ScreenshotUrl = userDocumentStatusRepo
						.findByUserIdDocumentSubTypeAndId(queryDetails.getDocumentId(), user.getId());

				String downloadUrlForScreenshot = "";
				String message = "";

				if (ScreenshotUrl != null) {
					FileRequest fileRequest = new FileRequest();
					fileRequest.setFileName(ScreenshotUrl.getFileName());
					fileRequest.setFileType(FileType.Kyc);
					fileRequest.setUserId(user.getId());
					fileRequest.setFilePrifix("USERQUERYSCREENSHOT");

					FileResponse file = fileManagementService.getFile(fileRequest);

					String downloadUrl = file.getDownloadUrl();

					logger.info(downloadUrl);

					String[] url = downloadUrl.split(Pattern.quote("?"));
					downloadUrlForScreenshot = url[0];

					message = "*Query from " + user.getPrimaryType() + " :*" + newline + "Hi Team," + newline
							+ "*Mr/Mrs " + userName + "(" + primaryType + user.getId() + ")*"
							+ " has raised a query as follows: " + newline + queryRequestDto.getQuery() + newline
							+ "Screenshot Url : " + downloadUrlForScreenshot + newline + "Thanks" + newline
							+ "OxyLoansTeam" + newline + "*This is a system generated message*";

				} else {
					message = "*Query from " + user.getPrimaryType() + " :*" + newline + "Hi Team," + newline
							+ "*Mr/Mrs " + userName + "(" + primaryType + user.getId() + ")*"
							+ " has raised a query as follows: " + newline + queryRequestDto.getQuery() + newline
							+ newline + "Thanks" + newline + "OxyLoansTeam" + newline
							+ "*This is a system generated message*";

				}

				String messageToUser = "Hi " + "*" + user.getPersonalDetails().getFirstName() + ",*" + "\n\n"
						+ "Thank you for your message." + newline
						+ "We've received it and generated a ticket. Your ticket ID is " + "*1977" + user.getId()
						+ queryDetails.getId() + "*" + newline
						+ "Our support team is reviewing it, and you can expect a response within 3 - 5 business days."
						+ "\n\n" + "Regards," + newline + "*Oxyloans Team*" + newline
						+ "*This is a system generated message*";

				String testServerChatId1 = "917702795895-1630419500@g.us";

				String testServerChatId = "917702795895-1630419021@g.us";

				String mobileNumber = "";

				if (user.getPersonalDetails().getWhatsAppNumber() != null) {
					mobileNumber = user.getPersonalDetails().getWhatsAppNumber();
				} else {
					mobileNumber = queryRequestDto.getMobileNumber();
				}

				// String messageSentTotestServer =
				// whatappService.sendingWhatsappMessages(testServerChatId, message);

				// String messageSentToMobile = "";
				/*
				 * if (user.getPrimaryType().equals(User.PrimaryType.LENDER)) {
				 * messageSentToMobile =
				 * whatappService.sendingWhatsappMsgWithMobileNumber(mobileNumber,
				 * messageToUser, wtappApi, "Queries"); } else if
				 * (user.getPrimaryType().equals(User.PrimaryType.BORROWER)) {
				 * messageSentToMobile =
				 * whatappService.sendingWhatsappMsgWithMobileNumber(mobileNumber,
				 * messageToUser, whatsaAppSendApiNewInstance, "Queries"); }
				 */

				if (!user.isTestUser()) {
					String messageSentTotestServer = whatappService.whatsappMessageToGroupUsingUltra(testServerChatId,
							message);
				} else {
					String messageSentTotestServer = whatappService.whatsappMessageToGroupUsingUltra(testServerChatId1,
							message);
				}

				whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, messageToUser);

				Calendar current = Calendar.getInstance();
				TemplateContext templateContext = new TemplateContext();

				String expectedCurrentDate = expectedDateFormat.format(current.getTime());
				templateContext.put("currentDate", expectedCurrentDate);

				String mailsubject = "OXYLOANS-TICKET RECEIVED";
				String emailTemplateName = "user-quer-details.template";
				String displayerName = "Oxyloans";

				String name = user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName();

				templateContext.put("name", name);
				templateContext.put("userId", user.getId());
				templateContext.put("tableId", queryDetails.getId());

				EmailRequest emailRequest = new EmailRequest(
						new String[] { queryDetails.getEmail(), "lakshmi@oxyloans.com" }, emailTemplateName,
						templateContext, mailsubject, displayerName);
				EmailResponse emailResponse = emailService.sendEmail(emailRequest);

				if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
					try {
						throw new MessagingException(emailResponse.getErrorMessage());
					} catch (MessagingException e2) {

						e2.printStackTrace();
					}
				}

				statusResponse.setStatus("Successfully Updated");
				statusResponse.setMessage("1977" + user.getId() + queryDetails.getId());

			} else if (user == null) {
				throw new OperationNotAllowedException("User is not there", ErrorCodes.USER_NOT_FOUND);
			}
		} else if (queryRequestDto.getQuery().isEmpty()) {
			throw new OperationNotAllowedException("Please enter the query", ErrorCodes.INVALID_OPERATION);
		}

		logger.info("readingQueriesFromUsers method ends..........................");

		return statusResponse;
	}

	@Override
	public UserResponse uploadUserQueryScreenshot(Integer userId, List<KycFileRequest> kycFiles) throws IOException {
		logger.info("uploadUserQueryScreenshot Method start");

		User user = userRepo.findById(userId).get();

		UserDocumentStatus docStatus = new UserDocumentStatus();

		if (user != null) {

			for (KycFileRequest kyc : kycFiles) {
				FileRequest fileRequest = new FileRequest();

				InputStream fileStream = kyc.getFileInputStream();
				if (fileStream == null) {
					fileStream = decodeBase64Stream(kyc.getBase64EncodedStream());
				}
				fileRequest.setInputStream(fileStream);
				fileRequest.setUserId(user.getId());
				fileRequest.setFileName(kyc.getFileName());
				fileRequest.setFilePrifix(kyc.getKycType().name());
				FileResponse putFile = fileManagementService.putFile(fileRequest);
				UserDocumentStatus userDocumentStatus = new UserDocumentStatus();
				userDocumentStatus.setUserId(user.getId());
				userDocumentStatus.setDocumentType(DocumentType.Kyc);
				userDocumentStatus.setDocumentSubType(kyc.getKycType().toString());
				userDocumentStatus.setFilePath(putFile.getFilePath());
				userDocumentStatus.setFileName(kyc.getFileName());
				userDocumentStatus.setStatus(Status.UPLOADED);
				docStatus = userDocumentStatusRepo.save(userDocumentStatus);
			}

		} else {

			throw new ActionNotAllowedException("User Not Found", ErrorCodes.USER_NOT_FOUND);
		}

		UserResponse response = new UserResponse();

		response.setId(user.getId());
		response.setDocumentId(docStatus.getId());
		response.setId(user.getId());

		return response;
	}

	@Override
	public UserResponse sendWhatsappOtp(UserRequest userRequest) {
		logger.info("sendWhatsappOtp method start");
		User user = userRepo.findById(userRequest.getId()).get();

		if (user != null) {
			PersonalDetails personalDetails = user.getPersonalDetails();
			if (personalDetails != null) {
				Random random = new Random();

				String id = String.format("%04d", random.nextInt(9999));

				String hashedOtpSession = signatureService.hashPassword(id, user.getSalt());

				String whatsappNumber = userRequest.getWhatsappNumber();
				String url = "https://oxyloans.com";

				String message = "<#> " + id + " is your OTP for whatsapp verification on " + url;

				whatappServiceRepo.sendingIndividualMessageUsingUltraApi(whatsappNumber, message);

				personalDetails.setWhatsAppNumber(whatsappNumber);
				user.setPersonalDetails(personalDetails);
				personalDetails.setUser(user);
				user.setWhatsappHash(hashedOtpSession);
				userRepo.save(user);

				logger.info("sendWhatsappOtp method end");
			}

		}
		userRepo.save(user);
		UserResponse userResponse = new UserResponse();
		userResponse.setId(userRequest.getId());
		userResponse.setWhatsappNumber(userRequest.getWhatsappNumber());
		return userResponse;
	}

	@Override
	public UserResponse verifyWhatsappOtp(UserRequest userRequest) {

		User user = userRepo.findById(userRequest.getId()).get();

		if (user != null) {
			PersonalDetails personalDetails = user.getPersonalDetails();
			if (personalDetails != null) {
				String whatsappOtp = userRequest.getWhatsappOtp();

				String hash = signatureService.hashPassword(whatsappOtp, user.getSalt());

				if (!hash.equals(user.getWhatsappHash())) {
					throw new MobileOtpVerifyFailedException("Invalid Whatsapp OTP value please check.",
							ErrorCodes.INVALID_OTP);
				}

				personalDetails.setWhatsappVerifiedOn(new Date());
				user.setPersonalDetails(personalDetails);
				user.setWhatsappVerified(true);

				userRepo.save(user);
			}
		}
		return new UserResponse();
	}

	@Override
	public StatusResponseDto assignUserExpertise(UserRequest request) {
		User user = userRepo.findByIdNum(request.getId());

		if (user != null) {
			PersonalDetails personalDetails = user.getPersonalDetails();
			if (personalDetails != null) {
				personalDetails.setUserExpertise(request.getUserExpertise());
				user.setPersonalDetails(personalDetails);
				userRepo.save(user);
			}
		}
		return new StatusResponseDto();

	}

	@Override
	public List<AdviseSeekersResponseDto> fetchAdviseSeekers(int userId) {
		List<ExpertAdviseSeekers> adviseSeekersList = expertAdviseSeekersRepo.findByAdvisorId(userId);

		List<AdviseSeekersResponseDto> response = new ArrayList<AdviseSeekersResponseDto>();

		if (adviseSeekersList != null && !adviseSeekersList.isEmpty()) {

			for (ExpertAdviseSeekers adviseSeekers : adviseSeekersList) {

				AdviseSeekersResponseDto dto = new AdviseSeekersResponseDto();

				dto.setId(adviseSeekers.getId());
				dto.setEmail(adviseSeekers.getMailId());
				dto.setName(adviseSeekers.getName());
				dto.setMobileNumber(adviseSeekers.getMobileNumber());
				dto.setContentAndFaqs(adviseSeekers.getSeekersFaqs());

				response.add(dto);

			}

		}
		return response;

	}

	@Override
	public List<ValidityResponse> gettingValidityDetails() {

		logger.info("validityDetailsOfMembershipToGroup method starts...............................");

		List<User> users = userRepo.findingListOfUsers();

		List<ValidityResponse> listOfUsers = new ArrayList<ValidityResponse>();

		if (users != null && !users.isEmpty()) {
			for (User user : users) {

				ValidityResponse singleUser = new ValidityResponse();

				Integer userId = user.getId();

				Calendar c = Calendar.getInstance();

				LocalDate validityDate = null;

				Date newDate = null;

				String modifiedDate = "";

				LenderRenewalDetails lenderRenewalDetails = lenderRenewalDetailsRepo.findingValidityAndUser(userId);

				if (lenderRenewalDetails == null) {

					LenderRenewalDetails renewalDetails = new LenderRenewalDetails();
					renewalDetails.setUser_id(userId);

					Date paymentDate = null;

					OxyLendersAcceptedDeals particiaptedIn3Yrs = oxyLendersAcceptedDealsRepo
							.findingLenderParticipatedIn3YRS(user.getId());

					OxyLendersAcceptedDeals particiaptedIn2Yrs = oxyLendersAcceptedDealsRepo
							.findingLendersParticipatedIN2YRS(user.getId());

					if (particiaptedIn3Yrs != null && particiaptedIn2Yrs != null) {

						paymentDate = particiaptedIn3Yrs.getReceivedOn();
						c.setTime(paymentDate);
						c.add(Calendar.YEAR, 3);
						newDate = c.getTime();

						modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(newDate);

						validityDate = LocalDate.parse(modifiedDate);

						singleUser.setUserId(user.getId() == null ? 0 : user.getId());

						singleUser.setValidityDate(modifiedDate == null ? "" : modifiedDate);

						singleUser.setName((user.getPersonalDetails().getFirstName() + " "
								+ user.getPersonalDetails().getLastName()) == null ? ""
										: (user.getPersonalDetails().getFirstName() + " "
												+ user.getPersonalDetails().getLastName()));

						renewalDetails.setValidityDate(newDate);

						lenderRenewalDetailsRepo.save(renewalDetails);
					} else {
						if (particiaptedIn3Yrs != null) {
							paymentDate = particiaptedIn3Yrs.getReceivedOn();
							c.setTime(paymentDate);
							c.setTime(paymentDate);
							c.add(Calendar.YEAR, 3);
							newDate = c.getTime();

							modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(newDate);

							validityDate = LocalDate.parse(modifiedDate);
							singleUser.setUserId(user.getId() == null ? 0 : user.getId());

							singleUser.setValidityDate(modifiedDate == null ? "" : modifiedDate);

							singleUser.setName((user.getPersonalDetails().getFirstName() + " "
									+ user.getPersonalDetails().getLastName()) == null ? ""
											: (user.getPersonalDetails().getFirstName() + " "
													+ user.getPersonalDetails().getLastName()));

							renewalDetails.setValidityDate(newDate);

							lenderRenewalDetailsRepo.save(renewalDetails);
						} else if (particiaptedIn2Yrs != null) {
							paymentDate = particiaptedIn2Yrs.getReceivedOn();
							c.setTime(paymentDate);
							c.add(Calendar.YEAR, 2);
							newDate = c.getTime();

							modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(newDate);

							validityDate = LocalDate.parse(modifiedDate);
							singleUser.setUserId(user.getId() == null ? 0 : user.getId());

							singleUser.setValidityDate(modifiedDate == null ? "" : modifiedDate);

							singleUser.setName((user.getPersonalDetails().getFirstName() + " "
									+ user.getPersonalDetails().getLastName()) == null ? ""
											: (user.getPersonalDetails().getFirstName() + " "
													+ user.getPersonalDetails().getLastName()));

							renewalDetails.setValidityDate(newDate);

							lenderRenewalDetailsRepo.save(renewalDetails);
						} else {

							OxyLendersAcceptedDeals lendersParticipatedBelongsToGroupOne = oxyLendersAcceptedDealsRepo
									.findingUserBelongsToGroupOne(user.getId());

							if (lendersParticipatedBelongsToGroupOne != null) {
								paymentDate = lendersParticipatedBelongsToGroupOne.getReceivedOn();
								c.setTime(paymentDate);
								c.add(Calendar.YEAR, 1);
								newDate = c.getTime();

								modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(newDate);

								validityDate = LocalDate.parse(modifiedDate);

								singleUser.setUserId(user.getId() == null ? 0 : user.getId());

								singleUser.setValidityDate(modifiedDate == null ? "" : modifiedDate);

								singleUser.setName((user.getPersonalDetails().getFirstName() + " "
										+ user.getPersonalDetails().getLastName()) == null ? ""
												: (user.getPersonalDetails().getFirstName() + " "
														+ user.getPersonalDetails().getLastName()));

								Date oldValidityDate = null;
								Calendar calendar = Calendar.getInstance();
								calendar.set(2022, 05, 30);
								oldValidityDate = calendar.getTime();
								renewalDetails.setOldValidityDate(newDate);
								renewalDetails.setValidityDate(oldValidityDate);

								lenderRenewalDetailsRepo.save(renewalDetails);
							} else {
								OxyLendersAcceptedDeals lendersPArticipatedInDeal = oxyLendersAcceptedDealsRepo
										.findingLendersParticipatedINDeals(user.getId());
								if (lendersPArticipatedInDeal != null) {
									paymentDate = lendersPArticipatedInDeal.getReceivedOn();
									c.setTime(paymentDate);
									c.add(Calendar.YEAR, 1);
									newDate = c.getTime();

									modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(newDate);

									validityDate = LocalDate.parse(modifiedDate);

									singleUser.setUserId(user.getId() == null ? 0 : user.getId());

									singleUser.setValidityDate(modifiedDate == null ? "" : modifiedDate);

									singleUser.setName((user.getPersonalDetails().getFirstName() + " "
											+ user.getPersonalDetails().getLastName()) == null ? ""
													: (user.getPersonalDetails().getFirstName() + " "
															+ user.getPersonalDetails().getLastName()));

									renewalDetails.setValidityDate(newDate);

									lenderRenewalDetailsRepo.save(renewalDetails);
								}
							}

						}
					}

					listOfUsers.add(singleUser);
				} else {

					LenderRenewalDetails details = lenderRenewalDetailsRepo.gettingPaymentDate(userId);

					if (details != null) {
						LenderRenewalDetails details1 = new LenderRenewalDetails();

						Integer count = lenderRenewalDetailsRepo.findingCount(userId);

						if (count == 0) {
							Date paymentDateInRenewalTable = details.getPaymentDate();

							c.setTime(paymentDateInRenewalTable);

							c.add(Calendar.YEAR, 1);
							newDate = c.getTime();

							modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(newDate);

							details1.setUser_id(userId);
							details1.setValidityDate(newDate);

							lenderRenewalDetailsRepo.save(details1);

							singleUser.setUserId(user.getId() == null ? 0 : user.getId());

							singleUser.setValidityDate(modifiedDate == null ? "" : modifiedDate);

							singleUser.setName((user.getPersonalDetails().getFirstName() + " "
									+ user.getPersonalDetails().getLastName()) == null ? ""
											: (user.getPersonalDetails().getFirstName() + " "
													+ user.getPersonalDetails().getLastName()));

							listOfUsers.add(singleUser);

						}

					}
				}
			}
		}
		logger.info("validityDetailsOfMembershipToGroup method ends...............................");

		return listOfUsers;

	}

	@Override
	public LenderReferenceResponse sendBulkInviteThroughExcel(KycFileRequest kyc, int userId, String content) {

		LenderReferenceResponse response = new LenderReferenceResponse();

		User user = userRepo.findByIdNum(userId);

		if (user != null) {
			String downloadUrl = "";

			FileRequest fileRequest = new FileRequest();

			InputStream fileStream = kyc.getFileInputStream();

			if (fileStream == null) {
				fileStream = decodeBase64Stream(kyc.getBase64EncodedStream());
			}

			fileRequest.setInputStream(fileStream);
			fileRequest.setUserId(userId);
			String fileName = kyc.getFileName();

			System.out.println("fileName...." + fileName);

			fileRequest.setFileName(kyc.getFileName());
			logger.info(kyc.getFileName());
			fileRequest.setFilePrifix(kyc.getKycType().name());
			try {

				FileResponse putFile = fileManagementService.putFile(fileRequest);
				FileResponse getFile = fileManagementService.getFile(fileRequest);

				String[] str = getFile.getDownloadUrl().split(Pattern.quote("?"));
				downloadUrl = str[0];

				System.out.println("downloadUrl......." + downloadUrl);

				/*
				 * Next, a new URL object is created with the downloadUrl string, and a
				 * ReadableByteChannel is obtained from the stream of the URL using the
				 * Channels.newChannel() method.
				 */
				URL url = new URL(downloadUrl);

				System.out.println("url......." + url);

				ReadableByteChannel readChannel = Channels.newChannel(url.openStream());

				/*
				 * A file name is created using the current date and the user ID, and a new
				 * FileOutputStream is created with this name. A FileChannel is obtained from
				 * the file output stream, and the data from the ReadableByteChannel is
				 * transferred to the FileChannel using the transferFrom() method. This
				 * effectively saves the data from the URL stream to a local file.
				 */

				String saveDirectory = LocalDate.now() + "LR" + userId + ".xls";

				FileOutputStream fileOS = new FileOutputStream(saveDirectory);
				FileChannel writeChannel = fileOS.getChannel();
				writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);

				File file = new File(saveDirectory);
				String nameAndEmail = "";
				String fileExtension = "";

				// here condition for file exsist then extract the . extension with nameS
				if (file.exists()) {

					try {

						int lastIndxDot = fileName.lastIndexOf('.');

						System.out.println(" fileName.lastIndexOf('.')" + fileName.lastIndexOf('.'));

						if (lastIndxDot != -1) {
							fileExtension = fileName.substring(lastIndxDot, fileName.length());

							System.out.println(" fileExtension.." + fileExtension);

						}

						nameAndEmail = excelServiceRepo.sendBulkInvite(file, fileExtension);
					} catch (InvalidFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (!nameAndEmail.equals("")) {
						LenderReferenceRequestDto dto = new LenderReferenceRequestDto();

						logger.info(nameAndEmail);

						dto.setEmail(nameAndEmail);
						dto.setReferrerId(userId);
						logger.info(userId);
						dto.setMailSubject("Invite");
						dto.setMailContent(content);
						System.out.println("nameAndEmail...." + nameAndEmail);
						System.out.println("........dto.setMailContent(content);..." + content);
						dto.setCitizenType(CitizenType.NONNRI.toString());
						dto.setSeekerRequestedId(0);
						dto.setPrimaryType(user.getPrimaryType().name());
						dto.setInviteType("BulkInvite");
						response = lenderReferenceDetails(dto);
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return response;
	}

	@Override
	public LenderInterestResponseDto lenderBankDetailsVerification(LenderInterestRequestDto lenderInterestRequestDto) {
		User user = userRepo.findById(lenderInterestRequestDto.getUserId()).get();
		BankDetails bankDetails = user.getBankDetails();
		if (user != null) {
			if (user.getPrimaryType().equals(User.PrimaryType.LENDER)) {
				if (bankDetails != null) {
					bankDetails.setBankDetailsVerifiedstatus(true);
					user.setBankDetails(bankDetails);
					userRepo.save(user);
				}
			}

		}
		LenderInterestResponseDto lenderInterestResponseDto = new LenderInterestResponseDto();
		lenderInterestResponseDto.setStatus("SuccessFully Updated");
		return lenderInterestResponseDto;

	}

	public OxyUserTypeResponseDto getSingleUserTypeInformation(String utm) {
		OxyUserTypeResponseDto oxyUserTypeResponseDto = new OxyUserTypeResponseDto();
		OxyUserType OxyUserType = oxyUserTypeRepo.getDetailsByUtmNameAndType(utm);
		if (OxyUserType != null) {
			oxyUserTypeResponseDto.setUtmName(OxyUserType.getUtmName());
			oxyUserTypeResponseDto.setUserType(OxyUserType.getUserType().toString());
			oxyUserTypeResponseDto.setRepaymentMethod(OxyUserType.getRepaymentMethod().toString());
			oxyUserTypeResponseDto.setLoanAmountCalculation(OxyUserType.getLoanAmountCalculation());
			oxyUserTypeResponseDto.setDuration(OxyUserType.getDuration());
			oxyUserTypeResponseDto.setFirstCibilScore(OxyUserType.getFirstCibilScore());
			oxyUserTypeResponseDto.setSecondCibilScore(OxyUserType.getSecondCibilScore());
			oxyUserTypeResponseDto.setThirdCibilScore(OxyUserType.getThirdCibilScore());
			oxyUserTypeResponseDto.setFourthCibilScore(OxyUserType.getFourthCibilScore());
			oxyUserTypeResponseDto.setFifthCibilScore(OxyUserType.getFifthCibilScore());
			oxyUserTypeResponseDto.setType(OxyUserType.getType().toString());
			oxyUserTypeResponseDto.setAdditionalFieldsToUser(OxyUserType.getAdditionalFieldsToUser().toString());

		}
		return oxyUserTypeResponseDto;

	}

	public ReferenceDetailsResponseDto getReferenceDetails(int userId) {
		ReferenceDetailsResponseDto referenceDetailsResponseDto = new ReferenceDetailsResponseDto();
		User user = userRepo.findById(userId).get();
		if (user != null) {
			ReferenceDetails referenceDetails = user.getReferenceDetails();
			if (referenceDetails != null) {

				if (referenceDetails.getReference1() != null) {
					referenceDetailsResponseDto.setReference1(referenceDetails.getReference1());
				}

				if (referenceDetails.getReference2() != null) {
					referenceDetailsResponseDto.setReference2(referenceDetails.getReference2());
				}
				if (referenceDetails.getReference3() != null) {
					referenceDetailsResponseDto.setReference3(referenceDetails.getReference3());
				}
				if (referenceDetails.getReference4() != null) {
					referenceDetailsResponseDto.setReference4(referenceDetails.getReference4());
				}
				if (referenceDetails.getReference5() != null) {
					referenceDetailsResponseDto.setReference5(referenceDetails.getReference5());
				}
				if (referenceDetails.getReference6() != null) {
					referenceDetailsResponseDto.setReference6(referenceDetails.getReference6());
				}
				if (referenceDetails.getReference7() != null) {
					referenceDetailsResponseDto.setReference7(referenceDetails.getReference7());
				}
				if (referenceDetails.getReference8() != null) {
					referenceDetailsResponseDto.setReference8(referenceDetails.getReference8());
				}

			}
		}
		return referenceDetailsResponseDto;

	}

	@Override
	public LenderReferenceResponse savingBonusPaymentStatusFromExcel(List<KycFileRequest> files) throws IOException {

		logger.info("savingBonusPaymentStatusFromExcel method starts................");

		LenderReferenceResponse response = new LenderReferenceResponse();

		if (files != null && !files.isEmpty()) {
			for (KycFileRequest file : files) {
				InputStream fis = file.getFileInputStream();

				XSSFWorkbook wb = new XSSFWorkbook(fis);

				XSSFSheet sheet = wb.getSheetAt(0);

				Iterator<Row> rowIterator = sheet.iterator();

				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();

					Cell refereeId = row.getCell(0);

					Cell paymnetStatus = row.getCell(5);

					Cell id = row.getCell(10);

					Cell transferredOn = row.getCell(11);

					if (paymnetStatus != null) {

						if (paymnetStatus.getStringCellValue().equalsIgnoreCase("Paid")) {

							Integer tableId = (int) id.getNumericCellValue();

							LenderReferralBonusUpdated bonus = lenderReferralBonusUpdatedRepo
									.findingPaymentStatus(tableId);

							bonus.setPaymentStatus("Paid");

							if (transferredOn == null) {

								bonus.setTransferredOn(new Date());

							} else {

								Date date = transferredOn.getDateCellValue();

								bonus.setTransferredOn(date);
							}

							bonus.setRadhaSirCommentsForBonus("Approved");

							lenderReferralBonusUpdatedRepo.save(bonus);

							response.setStatus("Successfully Updated");

							int refereeNewId = 0;

							switch (refereeId.getCellType()) {
							case Cell.CELL_TYPE_NUMERIC:
								refereeNewId = (int) refereeId.getNumericCellValue();

								break;
							case Cell.CELL_TYPE_STRING:
								String newId = refereeId.getStringCellValue();

								refereeNewId = Integer.parseInt(newId);

								break;
							}

							LenderReferenceDetails details = lenderReferenceDetailsRepo
									.findingRefereeAndReferrerInfo(refereeNewId);
							Double sumOfAmountUnpaid = lenderReferralBonusUpdatedRepo
									.findingSumOfAmountInUnPaidState(refereeNewId);

							if (details != null) {
								if (sumOfAmountUnpaid != null) {
									details.setAmount(sumOfAmountUnpaid);
								} else {
									details.setAmount(0.0);
								}

							}

							lenderReferenceDetailsRepo.save(details);

						}

					}
				}
			}
		}
		logger.info("savingBonusPaymentStatusFromExcel method ends................");

		return response;

	}

	@Override
	public LenderReferenceResponse savingBonusPaymentStatusFromExcelForDealIds(List<KycFileRequest> files)
			throws IOException {

		logger.info("savingBonusPaymentStatusFromExcelForDealIds method starts................");

		LenderReferenceResponse response = new LenderReferenceResponse();

		if (files != null && !files.isEmpty()) {
			for (KycFileRequest file : files) {
				InputStream fis = file.getFileInputStream();

				XSSFWorkbook wb = new XSSFWorkbook(fis);

				XSSFSheet sheet = wb.getSheetAt(0);

				Iterator<Row> rowIterator = sheet.iterator();

				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();

					Cell refereeId = row.getCell(0);

					Cell paymnetStatus = row.getCell(5);

					Cell id = row.getCell(10);

					Cell transferredOn = row.getCell(11);

					if (paymnetStatus != null) {

						if (paymnetStatus.getStringCellValue().equalsIgnoreCase("Paid")) {

							int refereeNewId = 0;

							switch (refereeId.getCellType()) {
							case Cell.CELL_TYPE_NUMERIC:
								refereeNewId = (int) refereeId.getNumericCellValue();

								break;
							case Cell.CELL_TYPE_STRING:
								String newId = refereeId.getStringCellValue();

								refereeNewId = Integer.parseInt(newId);

								break;
							}

							Integer tableId = (int) id.getNumericCellValue();

							List<LenderReferralBonusUpdated> listOfBonus = lenderReferralBonusUpdatedRepo
									.findingPaymnetStatusForDeal(tableId, refereeNewId);

							if (listOfBonus != null && !listOfBonus.isEmpty()) {

								for (LenderReferralBonusUpdated bonus : listOfBonus) {

									bonus.setPaymentStatus("Paid");

									if (transferredOn == null) {

										bonus.setTransferredOn(new Date());

									} else {

										Date date = transferredOn.getDateCellValue();

										bonus.setTransferredOn(date);
									}

									bonus.setRadhaSirCommentsForBonus("Approved");

									lenderReferralBonusUpdatedRepo.save(bonus);
								}
							}

							response.setStatus("Successfully Updated");

							LenderReferenceDetails details = lenderReferenceDetailsRepo
									.findingRefereeAndReferrerInfo(refereeNewId);
							Double sumOfAmountUnpaid = lenderReferralBonusUpdatedRepo
									.findingSumOfAmountInUnPaidState(refereeNewId);

							if (details != null) {
								if (sumOfAmountUnpaid != null) {
									details.setAmount(sumOfAmountUnpaid);
								} else {
									details.setAmount(0.0);
								}

							}

							lenderReferenceDetailsRepo.save(details);

						}

					}
				}
			}
		}
		logger.info("savingBonusPaymentStatusFromExcelForDealIds method ends................");

		return response;

	}

	@Override
	@Transactional
	public UserResponse newRegister(UserRequest userRequest) throws MessagingException {

		UserResponse userResponse = new UserResponse();
		// PaytmVanSuccessResponse paytmVanSuccessResponse = null;

		if (userRequest.isCreate() && userRequest.getMobileNumber() != null && userRequest.getMobileOtpValue() == null
				&& userRequest.getEmail() == null && userRequest.getEmailOtp() == null) {
			User user = userRepo.findByMobileNumber(userRequest.getMobileNumber());
			if (user != null) {

				if (user.isMobileNumberVerified() && user.isEmailVerified()) {
					throw new EntityAlreadyExistsException(
							"User " + userRequest.getMobileNumber() + " already register",
							ErrorCodes.ENTITY_ALREDY_EXISTS);
				} else if (user.isMobileNumberVerified()) {
					userResponse = new UserResponse();
					userResponse.setId(user.getId());
					userResponse.setMobileNumber(user.getMobileNumber());
					userResponse.setMobileverified(user.isMobileNumberVerified());
					return userResponse;
				}

				else {
					throw new EntityAlreadyExistsException(
							"User " + userRequest.getMobileNumber() + " already register",
							ErrorCodes.ENTITY_ALREDY_EXISTS);
				}
			}

			else {
				userResponse = new UserResponse();

				if (userRequest.getCitizenship().equals(Citizenship.NONNRI.toString())) {
					String otpSessionId = mobileUtil.sendOtp(userRequest.getMobileNumber());

					userResponse.setMobileNumber(userRequest.getMobileNumber());
					userResponse.setMobileOtpSession(otpSessionId);
				} else {
					userResponse.setMobileNumber(userRequest.getMobileNumber());
					userResponse.setMobileOtpSession("NRI");
				}
				return userResponse;
			}
		}
		if (userRequest.getWhatsappNumber() != null) {
			if (userRequest.isCreate() && userRequest.getWhatsappNumber() != null
					&& userRequest.getWhatsappOtp() == null) {
				UserResponse userResponse2 = null;

				try {

					userResponse2 = sendWhatsappOtp(userRequest);

				} catch (Exception ex) {
					throw new ActionNotAllowedException(ex.getMessage(), ErrorCodes.PERMISSION_DENIED);
				}
				if (userResponse2 != null) {
					userResponse.setId(userResponse2.getId());
					userResponse.setWhatsappNumber(userResponse2.getWhatsappNumber());
				}
				return userResponse;
			}
		}

		if (userRequest.isCreate() && userRequest.getCitizenship() != null
				&& (userRequest.getMobileOtpValue() != null || userRequest.getMobileOtpValue() == null)) {
			User user = new User();
			boolean verifyOtp = false;
			if (userRequest.getCitizenship().equals(Citizenship.NONNRI.toString())) {
				verifyOtp = mobileUtil.verifyOtp(userRequest.getMobileNumber(), userRequest.getMobileOtpSession(),
						userRequest.getMobileOtpValue());
				user.setMobileNumberVerified(true);
				user.setMobileOtpSent(!verifyOtp);
				user.setCitizenship(Citizenship.valueOf(userRequest.getCitizenship().toUpperCase()));
				if (!verifyOtp) {
					throw new MobileOtpVerifyFailedException("Invalid OTP value please check.", ErrorCodes.INVALID_OTP);
				}
			} else {
				user.setMobileNumberVerified(true);
				user.setMobileOtpSent(true);
				user.setCitizenship(Citizenship.valueOf(userRequest.getCitizenship().toUpperCase()));
			}
			user.setMobileNumber(userRequest.getMobileNumber());
			user.setPrimaryType(PrimaryType.valueOf(userRequest.getType().toUpperCase()));
			user.setMobileOtpSession("");
			user.setSalt(UUID.randomUUID().toString() + UUID.randomUUID().toString().substring(0, 10));

			user.setLongitude(userRequest.getLongitude());
			user.setLatitude(userRequest.getLatitude());
			user.setCity(userRequest.getCity());
			user.setPinCode(userRequest.getPinCode());
			user.setState(userRequest.getState());
			user.setLocality(userRequest.getLocality());
			user.setUniqueNumber(new Long(System.currentTimeMillis()).toString().substring(0, 10)); // This is temporary
			StringBuilder uniqueNumber = new StringBuilder();
			if (user.getPrimaryType() == PrimaryType.LENDER) {
				uniqueNumber.append("LR1");
			} else {
				uniqueNumber.append("BR1");
			}

			PersonalDetails personalDetails = user.getPersonalDetails();
			if (personalDetails == null) {
				if (personalDetails == null) {
					personalDetails = new PersonalDetails();
					personalDetails.setFirstName(userRequest.getFirstName());
					personalDetails.setLastName(userRequest.getLastName());
					personalDetails.setFatherName("");
					personalDetails.setCreatedAt(new Date());
					personalDetails.setModifiedAt(new Date());

					/*
					 * MultiChainResponseDto multiChainResponseDto = generateMultiChainAddress();
					 * personalDetails.setMultiChainAddress(multiChainResponseDto.getData());
					 */
					if (userRequest.getDob() != null) {
						try {
							personalDetails.setDob(expectedDateFormat.parse(userRequest.getDob()));
						}

						catch (Exception e) {
							e.printStackTrace();
						}
					}

				}
				if (userRequest.getState() != null && userRequest.getPinCode() != null) {
					user.setPinCode(userRequest.getPinCode());
					user.setState(userRequest.getState());
				}
				user.setUrchinTrackingModule(userRequest.getUtmSource());
				user = userRepo.save(user);
				uniqueNumber.append(StringUtils.leftPad(user.getId().toString(), 6, "0"));
				user.setUniqueNumber(uniqueNumber.toString());
				personalDetails.setUser(user);
				user.setPersonalDetails(personalDetails);
				user = userRepo.save(user);
				if (!userRequest.getType().equalsIgnoreCase(PrimaryType.LENDER.toString())) {
					LoanRequestDto loanRequestDto = new LoanRequestDto();
					loanRequestDto.setDuration(6);
					loanRequestDto.setLoanPurpose("Initiated the Request");
					loanRequestDto.setLoanRequestAmount(userRequest.getAmountInterested());
					loanRequestDto.setRateOfInterest(0d);
					loanRequestDto.setRepaymentMethod(RepaymentMethod.PI.name());
					Calendar expectedDate = Calendar.getInstance();
					expectedDate.add(Calendar.DATE, 1);
					loanRequestDto.setExpectedDate(expectedDateFormat.format(expectedDate.getTime()));

					PasswordGrantAccessToken accessToken = new PasswordGrantAccessToken(6000, EncodingAlgorithm.RSA);
					accessToken.setUserId(user.getId().toString());
					ThreadLocalSecurityProvider.setAccessToken(accessToken);
					LoanResponseDto loanRequest = loanServiceFactory
							.getService(user.getPrimaryType().name().toUpperCase())
							.loanRequest(user.getId(), loanRequestDto);
					logger.info("Created Loan Request for the user {} as request id {}", user.getId(),
							loanRequest.getLoanRequestId());

				}
			}

			userResponse = new UserResponse();
			userResponse.setId(user.getId());
			userResponse.setMobileverified(verifyOtp);
			userResponse.setMobileNumber(userRequest.getMobileNumber());
			logger.info("Lender Reference...........................starts.........................");
			String uniqueNumber1 = userRequest.getUniqueNumber();
			logger.info(uniqueNumber1 + "@@@@@@@@@@@@@@@@@@@@@@@");
			User referrer = userRepo.findByUniqueNumber(uniqueNumber1);

			if (!(uniqueNumber1.equals("0"))) {
				Integer referrerId = referrer.getId();

				LenderReferenceDetails referenceDetails = lenderReferenceDetailsRepo
						.findingReferrerInformation(referrerId, userRequest.getMobileNumber());

				User newUser = userRepo.findByMobileNumber(userRequest.getMobileNumber());

				Integer refereeId = newUser.getId();

				if (referenceDetails != null) {

					referenceDetails.setRefereeId(refereeId);
					referenceDetails.setSource(LenderReferenceDetails.Source.ReferralLink);

					if (newUser.getCitizenship().equals(User.Citizenship.NRI)) {
						referenceDetails.setCitizenType(LenderReferenceDetails.CitizenType.NRI);

					}

					if (user.getPrimaryType().equals(User.PrimaryType.LENDER)) {
						referenceDetails.setUserPrimaryType("LENDER");
					} else {
						referenceDetails.setUserPrimaryType("BORROWER");
					}

					lenderReferenceDetailsRepo.save(referenceDetails);
				} else {
					LenderReferenceDetails details = new LenderReferenceDetails();

					details.setRefereeId(refereeId);
					details.setRefereeMobileNumber(userRequest.getMobileNumber());
					details.setReferrerId(referrerId);
					details.setRefereeName(newUser.getPersonalDetails().getFirstName() + ""
							+ newUser.getPersonalDetails().getLastName());
					details.setSource(LenderReferenceDetails.Source.ReferralLink);

					if (newUser.getCitizenship().equals(User.Citizenship.NRI)) {
						details.setCitizenType(LenderReferenceDetails.CitizenType.NRI);

					}

					if (user.getPrimaryType().equals(User.PrimaryType.LENDER)) {
						details.setUserPrimaryType("LENDER");
					} else {
						details.setUserPrimaryType("BORROWER");
					}

					lenderReferenceDetailsRepo.save(details);
				}
			}
			logger.info("Lender Reference...........................ends.........................");

			return userResponse;
		}

		if (userRequest.isCreate() && userRequest.getEmail() != null) {
			User user = userRepo.findByMobileNumber(userRequest.getMobileNumber());

			if (user == null) {
				throw new ActionNotAllowedException("User not exist ", ErrorCodes.USER_NOT_FOUND);

			}

			if (!user.isMobileNumberVerified()) {
				throw new ActionNotAllowedException("User mobile Not Verified", ErrorCodes.PERMISSION_DENIED);
			}

			if (user != null && userRequest.getEmail() != null && userRequest.getEmailOtp() == null) {
				// User verifyEmail1 = userRepo.findByEmail(userRequest.getEmail());

				Integer verifyEmail1 = userRepo.findByEmailUsingEqualIgnoranceCase(userRequest.getEmail());
				if (verifyEmail1 > 0) {
					throw new EntityAlreadyExistsException("User " + userRequest.getEmail() + " already register",
							ErrorCodes.ENTITY_ALREDY_EXISTS);
				}

				Random random = new Random();
				String id = String.format("%06d", random.nextInt(999999));
				TemplateContext templateContext = new TemplateContext();
				templateContext.put("otp", id);
				String hashedEmailOtpSession = signatureService.hashPassword(id, user.getSalt());
				String mailOtpSubject = "OTP From oxyloans";
				EmailRequest emailRequest = new EmailRequest(new String[] { userRequest.getEmail() }, mailOtpSubject,
						"email-otp.template", templateContext);
				EmailResponse emailResponse = emailService.sendEmail(emailRequest);
				if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
					throw new MessagingException(emailResponse.getErrorMessage()); // TODO need to fill this property
				}
				user.setEmail(userRequest.getEmail());
				user.setEmailOtpSession(hashedEmailOtpSession);
				user.setEmailVerifiedOn(new Date());
				user.setEmailSent(true);
				userRepo.save(user);
				userResponse = new UserResponse();
				userResponse.setId(user.getId());
				userResponse.setMobileNumber(user.getMobileNumber());
				userResponse.setEmail(user.getEmail());
				userResponse.setEmailSent(user.isEmailSent());
				userResponse.setEmailVerified(user.getEmailVerified());
				TemplateContext templateContext1 = new TemplateContext();
				templateContext1.put("userName",
						user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName());
				Date date = new Date();
				String date1 = expectedDateFormat.format(date);
				templateContext1.put("CurrentDate", date1);

				if (user.getPrimaryType() == PrimaryType.LENDER) {
					String welcomeMailToLender = "Message from CEO, OxyLoans – RBI Approved P2P Lending Platform"; // mail
																													// subject
					String emailTemplateName = "registraction-mail-lender.template"; // template name
					EmailRequest emailRequest1 = new EmailRequest(new String[] { user.getEmail() }, welcomeMailToLender,
							emailTemplateName, templateContext1);
					EmailResponse emailResponse1 = emailService.sendEmail(emailRequest1);
					if (emailResponse1.getStatus() == EmailResponse.Status.FAILED) {
						throw new RuntimeException(emailResponse1.getErrorMessage());
					}
				} else {
					String welcomeMailToLender = "Message from CEO, OxyLoans – RBI Approved P2P Lending Platform"; // mail
																													// subject
					String emailTemplateName = "registraction-mail-borrower.template"; // template name
					EmailRequest emailRequest1 = new EmailRequest(new String[] { user.getEmail() }, welcomeMailToLender,
							emailTemplateName, templateContext1);
					EmailResponse emailResponse1 = emailService.sendEmail(emailRequest1);
					if (emailResponse1.getStatus() == EmailResponse.Status.FAILED) {
						throw new RuntimeException(emailResponse1.getErrorMessage());
					}
				}

				return userResponse;
			}

			if (user != null && userRequest.getEmail() != null && userRequest.getEmailOtp() != null) {
				User verifyEmail = userRepo.findByEmailIgnoreCase(userRequest.getEmail());
				String token = verifyEmail.getEmailOtpSession();
				String hashedEmailOtpSession = signatureService.hashPassword(userRequest.getEmailOtp(), user.getSalt());
				if (!hashedEmailOtpSession.equals(token)) {
					throw new ActionNotAllowedException("Email not verified", ErrorCodes.EMAIL_NOT_VERIFIED);
				}
				user.setEmailVerified(true);
				String hashedpassword = signatureService.hashPassword(userRequest.getPassword(), user.getSalt());
				user.setPassword(hashedpassword);

				if (userRequest.getWhatsappNumber() != null && userRequest.getWhatsappOtp() != null) {
					String whatsappOtp = userRequest.getWhatsappOtp();

					String hash = signatureService.hashPassword(whatsappOtp, user.getSalt());

					if (!hash.equals(user.getWhatsappHash())) {
						throw new MobileOtpVerifyFailedException("Invalid OTP value please check.",
								ErrorCodes.INVALID_OTP);
					}

					user.setWhatsappVerified(true);
					userRepo.save(user);
				}

				PersonalDetails personalDetails = user.getPersonalDetails();
				personalDetails.setGender(userRequest.getGender());

				user.setEmailVerified(true);
				userRepo.save(user);
				String vanNumber = null;

				if (user.getPrimaryType() == PrimaryType.BORROWER) {
					String patnerId = null;
					Random random1 = new Random();
					String value = String.format("%04d", random1.nextInt(10000));
					PaytmVanRequest paytmVanRequest = new PaytmVanRequest();
					int length = 0;
					for (length = 0; length <= 10; ++length) {
						Random rand = new Random();
						Long drand = (long) (rand.nextDouble() * 10000000000L);

						patnerId = drand.toString();
						length = patnerId.length();
					}

					logger.info("--------------------" + patnerId + "---------------------");
					logger.info("parter id lengthhhhhhhhhhhhhhhhhhhhhhhhhhhhhh" + patnerId.length());

					logger.info("Created patnerId for paytm", user.getId(), patnerId);
					paytmVanRequest.setMobile(user.getMobileNumber());

					paytmVanRequest.setName(userRequest.getFirstName() + userRequest.getLastName());
					paytmVanRequest.setPartnerRefId("Reqoxytrade" + value);
					paytmVanRequest.setPartnerId(patnerId);
					Gson gson = new Gson();
					String json = gson.toJson(paytmVanRequest);
					logger.info("body !!!!!!!!!!!!!!!!!" + paytmVanRequest.toString());
					logger.info("Created request paytm", user.getId(), json);
					/*
					 * String response = paytmService.createVan(paytmVanRequest); vanNumber =
					 * "OXYTDE" + patnerId; logger.info("responseOfPaytmVanCreation" + response);
					 */
				}

				if (loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId()) == null) {
					if (!user.getPrimaryType().toString().equalsIgnoreCase(PrimaryType.BORROWER.toString())) {
						LoanRequestDto loanRequestDto = new LoanRequestDto();
						loanRequestDto.setDuration(6);
						loanRequestDto.setLoanPurpose("Initiated the Request");
						loanRequestDto.setLoanRequestAmount(userRequest.getAmountInterested());
						loanRequestDto.setRateOfInterest(0d);
						loanRequestDto.setRepaymentMethod(RepaymentMethod.PI.name());
						Calendar expectedDate = Calendar.getInstance();
						expectedDate.add(Calendar.DATE, 1);
						loanRequestDto.setExpectedDate(expectedDateFormat.format(expectedDate.getTime()));

						PasswordGrantAccessToken accessToken = new PasswordGrantAccessToken(6000,
								EncodingAlgorithm.RSA);
						accessToken.setUserId(user.getId().toString());
						ThreadLocalSecurityProvider.setAccessToken(accessToken);
						LoanResponseDto loanRequest = loanServiceFactory
								.getService(user.getPrimaryType().name().toUpperCase())
								.loanRequest(user.getId(), loanRequestDto);
						logger.info("Created Loan Request for the user {} as request id {}", user.getId(),
								loanRequest.getLoanRequestId());

					}

				}
				user.setVanNumber(vanNumber);
				userRepo.save(user);

				logger.info("Lender Reference........................................starts.");
				String uniqueNumber = userRequest.getUniqueNumber();
				logger.info(uniqueNumber + "@@@@@@@@@@@@@@@@@@@@@@@");
				User referrer = userRepo.findByUniqueNumber(uniqueNumber);

				if (!(uniqueNumber.equals("0"))) {
					Integer referrerId = referrer.getId();

					LenderReferenceDetails referenceDetails = lenderReferenceDetailsRepo.findingReferrerInfo(referrerId,
							userRequest.getMobileNumber());

					User newUser = userRepo.findByMobileNumber(userRequest.getMobileNumber());

					Integer refereeId = newUser.getId();

					if (referenceDetails != null) {

						referenceDetails.setRefereeEmail(newUser.getEmail());
						referenceDetails.setRefereeMobileNumber(newUser.getMobileNumber());
						referenceDetails.setRefereeId(refereeId);
						referenceDetails.setStatus(LenderReferenceDetails.Status.Registered);

						if (newUser.getCitizenship().equals(User.Citizenship.NRI)) {
							referenceDetails.setCitizenType(LenderReferenceDetails.CitizenType.NRI);

						}

						if (user.getPrimaryType().equals(User.PrimaryType.LENDER)) {
							referenceDetails.setUserPrimaryType("LENDER");
						} else {
							referenceDetails.setUserPrimaryType("BORROWER");
						}

						lenderReferenceDetailsRepo.save(referenceDetails);
					}
				} else if (uniqueNumber.equals("0")) {

					LenderReferenceDetails referenceDetails = lenderReferenceDetailsRepo
							.findingReferrerInformation(userRequest.getMobileNumber());

					if (referenceDetails != null) {
						User user1 = userRepo.findByMobileNumber(userRequest.getMobileNumber());

						Integer refereeId = user1.getId();

						referenceDetails.setRefereeId(refereeId);
						referenceDetails.setRefereeEmail(userRequest.getEmail());
						referenceDetails.setStatus(LenderReferenceDetails.Status.Registered);

						if (user1.getCitizenship().equals(User.Citizenship.NRI)) {
							referenceDetails.setCitizenType(LenderReferenceDetails.CitizenType.NRI);

						}

						if (user.getPrimaryType().equals(User.PrimaryType.LENDER)) {
							referenceDetails.setUserPrimaryType("LENDER");
						} else {
							referenceDetails.setUserPrimaryType("BORROWER");
						}

						lenderReferenceDetailsRepo.save(referenceDetails);
					} else {
						LenderReferenceDetails reference = lenderReferenceDetailsRepo
								.findByRefereeEmail(userRequest.getEmail());

						if (reference != null) {
							User newUser = userRepo.findByEmailIgnoreCase(userRequest.getEmail());

							reference.setRefereeId(newUser.getId());
							reference.setStatus(LenderReferenceDetails.Status.Registered);
							reference.setRefereeMobileNumber(newUser.getMobileNumber());

							if (newUser.getCitizenship().equals(User.Citizenship.NRI)) {
								reference.setCitizenType(LenderReferenceDetails.CitizenType.NRI);

							}

							if (user.getPrimaryType().equals(User.PrimaryType.LENDER)) {
								reference.setUserPrimaryType("LENDER");
							} else {
								reference.setUserPrimaryType("BORROWER");
							}

							lenderReferenceDetailsRepo.save(reference);
						}

					}
				}

				logger.info("Lender Reference........................................ends.");

				userResponse = new UserResponse();
				userResponse.setId(user.getId());
				userResponse.setMobileNumber(user.getMobileNumber());
				userResponse.setEmail(user.getEmail());
				userResponse.setEmailVerified(user.getEmailVerified());
				return userResponse;

			}
		}

		return userResponse;
	}

	@Override
	public UserResponse uploadPartnerAgreementType(Integer partnerId, List<KycFileRequest> files) throws IOException {

		logger.info("uploadUserQueryScreenshot Method start");

		OxyUserType user = oxyUserTypeRepo.findById(partnerId).get();

		Integer documentId = 0;

		String url = "";

		InputStream fileStream;

		FileResponse putFile;

		FileRequest fileRequest;
		if (user != null) {

			for (KycFileRequest kyc : files) {
				String kycType = kyc.getKycType().toString();
				UserDocumentStatus userDocumentStatusInfo = userDocumentStatusRepo.findingPartnerAgreementType(kycType,
						partnerId);

				if (userDocumentStatusInfo == null) {
					logger.info(
							"Uploading the file anothertime : " + kyc.getFileName() + " of type " + kyc.getKycType());
					fileRequest = new FileRequest();

					fileStream = kyc.getFileInputStream();
					if (fileStream == null) {
						fileStream = decodeBase64Stream(kyc.getBase64EncodedStream());
					}
					fileRequest.setInputStream(fileStream);
					fileRequest.setUserId(user.getId());
					fileRequest.setFileName(kyc.getFileName());
					fileRequest.setFilePrifix(kyc.getKycType().name());
					putFile = fileManagementService.putFile(fileRequest);
					UserDocumentStatus userDocumentStatus = new UserDocumentStatus();
					userDocumentStatus.setUserId(user.getId());
					userDocumentStatus.setDocumentType(DocumentType.PartnerAgreementType);
					userDocumentStatus.setDocumentSubType(kyc.getKycType().toString());
					userDocumentStatus.setFilePath(putFile.getFilePath());
					userDocumentStatus.setFileName(kyc.getFileName());
					userDocumentStatus.setStatus(Status.UPLOADED);
					userDocumentStatusRepo.save(userDocumentStatus);

					documentId = userDocumentStatus.getId();

					FileResponse getFile = fileManagementService.getFile(fileRequest);

					String download[] = getFile.getDownloadUrl().split(Pattern.quote("?"));

					url = download[0];
				} else {
					logger.info(
							"Uploading the file anothertime : " + kyc.getFileName() + " of type " + kyc.getKycType());
					fileRequest = new FileRequest();

					fileStream = kyc.getFileInputStream();
					if (fileStream == null) {
						fileStream = decodeBase64Stream(kyc.getBase64EncodedStream());
					}
					fileRequest.setInputStream(fileStream);
					fileRequest.setUserId(user.getId());
					fileRequest.setFileName(kyc.getFileName());
					fileRequest.setFilePrifix(kyc.getKycType().name());
					putFile = fileManagementService.putFile(fileRequest);

					userDocumentStatusInfo.setUserId(user.getId());
					userDocumentStatusInfo.setDocumentType(DocumentType.PartnerAgreementType);
					userDocumentStatusInfo.setDocumentSubType(kyc.getKycType().toString());
					userDocumentStatusInfo.setFilePath(putFile.getFilePath());
					userDocumentStatusInfo.setFileName(kyc.getFileName());
					userDocumentStatusInfo.setStatus(Status.UPLOADED);
					userDocumentStatusRepo.save(userDocumentStatusInfo);
					documentId = userDocumentStatusInfo.getId();

					FileResponse getFile = fileManagementService.getFile(fileRequest);

					String download[] = getFile.getDownloadUrl().split(Pattern.quote("?"));

					url = download[0];
				}

				Calendar current = Calendar.getInstance();
				TemplateContext templateContext = new TemplateContext();
				String expectedCurrentDate = expectedDateFormat.format(current.getTime());

				templateContext.put("currentDate", expectedCurrentDate);

				templateContext.put("url", url);

				String displayerName = user.getPartnerName();

				String mailSubject = "Partner Uploaded " + kycType;
				EmailRequest emailRequest = new EmailRequest(
						new String[] { "ramadevi@oxyloans.com", "narendra@oxyloans.com", "liveen@oxyloans.com",
								"radhakrishna.t@oxyloans.com" },
						"partner-upload.template", templateContext, mailSubject, displayerName);

				EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
				if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
					try {
						throw new MessagingException(emailResponseFromService.getErrorMessage());
					} catch (MessagingException e2) {

						e2.printStackTrace();
					}
				}

				String msgToGroup = "Partner " + "*" + user.getPartnerName() + "*" + " has uploaded the  " + "*"
						+ kycType + "*" + ".Please download by using the below link." + "\n\n" + "*" + url.trim() + "*";

				/*
				 * whatappService.sendingWhatsappMsgWithMobileNumberToGroup(
				 * "918977849440-1562729743@g.us", msgToGroup, whatsaAppSendApiNewInstance);
				 */

				whatappService.whatsappMessageToGroupUsingUltra("918977849440-1562729743@g.us", msgToGroup);
			}

		} else {

			throw new ActionNotAllowedException("User Not Found", ErrorCodes.USER_NOT_FOUND);
		}

		UserResponse response = new UserResponse();

		response.setId(user.getId());
		response.setDocumentId(documentId);

		return response;

	}

	@Override
	public KycFileResponse downloadPartnerAgreementType(Integer partnerId, KycType kycFile)
			throws FileNotFoundException, IOException {

		OxyUserType user = oxyUserTypeRepo.findById(partnerId).get();

		UserDocumentStatus userDocumentStatus = userDocumentStatusRepo.findByUserIdAndDocumentTypeAndDocumentSubType(
				partnerId, DocumentType.PartnerAgreementType, kycFile.name());
		if (userDocumentStatus == null) {

			throw new ActionNotAllowedException("File not found to download", ErrorCodes.PERMISSION_DENIED);
		}
		FileRequest fileRequest = new FileRequest();
		fileRequest.setFileName(userDocumentStatus.getFileName());
		fileRequest.setFileType(FileType.PartnerAgreementType);
		fileRequest.setUserId(user.getId());
		fileRequest.setFilePrifix(kycFile.name());
		FileResponse file = fileManagementService.getFile(fileRequest);
		KycFileResponse fileResponse = new KycFileResponse();
		String downloadUrl = file.getDownloadUrl();

		String[] url = downloadUrl.split(Pattern.quote("?"));

		fileResponse.setDownloadUrl(url[0]);
		fileResponse.setFileName(userDocumentStatus.getFileName());
		fileResponse.setKycType(kycFile);
		return fileResponse;

	}

	public boolean lenderValidityStatus(int userId) {
		User user = userRepo.findById(userId).get();
		boolean status = false;
		if (user != null) {
			LenderRenewalDetails renewalDetails = lenderRenewalDetailsRepo.findingRenewalDetailsBasedOnUserId(userId);

			if (renewalDetails != null) {
				String newValidityDate = new SimpleDateFormat("yyyy-MM-dd").format(renewalDetails.getValidityDate());

				Date date;

				try {
					date = new SimpleDateFormat("yyyy-MM-dd").parse(newValidityDate);
					Calendar calendar1 = Calendar.getInstance();
					calendar1.setTime(date);

					Date yesterday = calendar1.getTime();

					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

					String data = dateFormat.format(new Date());

					String strDate = dateFormat.format(yesterday);

					String[] reminderDates = strDate.split("-");

					int reminderYear = Integer.parseInt(reminderDates[0]);
					int reminderMonth = Integer.parseInt(reminderDates[1]);
					int reminderDay = Integer.parseInt(reminderDates[2]);

					String[] currentDMY = data.split("-");

					int currentYear = Integer.parseInt(currentDMY[0]);
					int currentMonth = Integer.parseInt(currentDMY[1]);
					int currentDay = Integer.parseInt(currentDMY[2]);

					LocalDate start_date = LocalDate.of(currentYear, currentMonth, currentDay);

					LocalDate end_date = LocalDate.of(reminderYear, reminderMonth, reminderDay);

					if ((start_date.compareTo(end_date)) > 0) {

						status = true;
					} else {
						status = false;
					}

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				if (user.getLenderGroupId() == 0) {

					status = true;
				}
			}
		}
		return status;

	}

	@Override
	public KycFileResponse generateCICReportBasedOnDates(String startDate, String endDate) {

		logger.info("---------------------generateCICReport method start ...............");
		logger.info(startDate + " startDate................");

		logger.info(endDate + " endDate................");

		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			date = simpleDateFormat.parse(startDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		Timestamp startDate1 = new Timestamp(date.getTime());

		Date date1 = null;
		try {
			date1 = simpleDateFormat.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Timestamp endDate1 = new Timestamp(date1.getTime());

		List<Object[]> objList = lenderOxyWalletNativeRepo.generateCICReportBasedOnDates(startDate1, endDate1);

		KycFileResponse fileResponse = new KycFileResponse();
		ByteArrayOutputStream output = null;
		FileOutputStream outFile = null;
		try {
			output = new ByteArrayOutputStream();
			HSSFRow row = null;
			HSSFCellStyle yellowStyle = null;
			HSSFFont headerFont = null;
			HSSFCell cell0 = null;
			HSSFCell cell1 = null;
			HSSFWorkbook wb = new HSSFWorkbook();
			HSSFSheet sheet = wb.createSheet("CIC");
			int index = 0;
			row = sheet.createRow((short) index);
			headerFont = wb.createFont();
			headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			headerFont.setFontHeightInPoints((short) 10);
			headerFont.setColor(HSSFColor.BLACK.index);

			yellowStyle = wb.createCellStyle();
			yellowStyle.setFont(headerFont);
			yellowStyle.setBorderTop((short) 1); // double lines border
			yellowStyle.setBorderRight((short) 1);
			yellowStyle.setBorderBottom((short) 1);
			yellowStyle.setBorderLeft((short) 1);
			yellowStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
			yellowStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
			yellowStyle.setWrapText(true);

			cell0 = row.createCell(0);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CONSUMER_NAME));
			cell0 = row.createCell(1);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DATE_OF_BIRTH));
			cell0 = row.createCell(2);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.GENDER));
			cell0 = row.createCell(3);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.INCOME_TAX_ID_NUMBER));
			cell0 = row.createCell(4);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PASSPORT_NUMBER));
			cell0 = row.createCell(5);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PASSPORT_ISSUE_DATE));
			cell0 = row.createCell(6);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PASSPORT_EXPIRY_DATE));
			cell0 = row.createCell(7);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.VOTER_ID_NUMBER));
			cell0 = row.createCell(8);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DRIVING_LICENSE_NUMBER));
			cell0 = row.createCell(9);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DRIVING_LICENSE_ISSUE_DATE));
			cell0 = row.createCell(10);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DRIVING_LICENSE_EXPIRY_DATE));
			cell0 = row.createCell(11);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.RATION_CARD_NUMBER));
			cell0 = row.createCell(12);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.UNIVERSAL_ID_NUMBER));
			cell0 = row.createCell(13);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ADDITIONAL_ID_1));
			cell0 = row.createCell(14);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ADDITIONAL_ID_2));
			cell0 = row.createCell(15);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.TELEPHONE_NUMBER_MOBILE));
			cell0 = row.createCell(16);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.TELEPHONE_NUMBER_RESIDENCE));
			cell0 = row.createCell(17);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.TELEPHONE_NUMBER_OFFICE));
			cell0 = row.createCell(18);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.EXTENSION_OFFICE));
			cell0 = row.createCell(19);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.TELEPHONE_NUMBER_OTHER));
			cell0 = row.createCell(20);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.EXTENSION_OTHER));
			cell0 = row.createCell(21);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.EMAIL_ID_1));
			cell0 = row.createCell(22);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.EMAIL_ID_2));
			cell0 = row.createCell(23);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PERMANANT_ADD));

			cell0 = row.createCell(24);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.STATE_CODE_1));

			cell0 = row.createCell(25);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PIN_CODE_1));

			cell0 = row.createCell(26);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ADDRESS_CATEGORY_1));

			cell0 = row.createCell(27);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.RESIDENCE_CODE_1));

			cell0 = row.createCell(28);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CORRESPONDING_ADD));

			cell0 = row.createCell(29);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.STATE_CODE_2));

			cell0 = row.createCell(30);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PIN_CODE_2));

			cell0 = row.createCell(31);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ADDRESS_CATEGORY_2));

			cell0 = row.createCell(32);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.RESIDENCE_CATEGORY_2));

			cell0 = row.createCell(33);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CURRENT_NEW_MEMBER_CODE));

			cell0 = row.createCell(34);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CURRENT_NEW_MEMBER_SHORT_NAME));

			cell0 = row.createCell(35);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CURRENT_NEW_ACCOUNT_NUMBER));

			cell0 = row.createCell(36);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ACCOUNT_TYPE));

			cell0 = row.createCell(37);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OWNERSHIP_INDICATOR));

			cell0 = row.createCell(38);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DATE_OPENDED_DISBURSED));

			cell1 = row.createCell(39);
			cell1.setCellStyle(yellowStyle);
			cell1.setCellValue(new HSSFRichTextString(CICReportConstants.DATE_OF_LAST_PAYMENT));

			cell0 = row.createCell(40);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DATE_CLOSED));

			cell0 = row.createCell(41);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.DATE_REPORTED));

			cell0 = row.createCell(42);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.HIGH_CREDIT_SANCTIONED_AMOUNT));

			cell0 = row.createCell(43);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CURRENT_BALANCE));

			cell0 = row.createCell(44);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.AMOUNT_OVERDUE));

			cell0 = row.createCell(45);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.NUMBER_OF_DAYS_PAST_DUE));

			cell0 = row.createCell(46);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OLD_MBR_CODE));

			cell0 = row.createCell(47);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OLD_MBR_SHORT_NAME));

			cell0 = row.createCell(48);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OLD_ACCOUNT_NUMBER));

			cell0 = row.createCell(49);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OLD_ACCOUNT_TYPE));

			cell0 = row.createCell(50);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OLD_OWNERSHIP_INDICATOR));

			cell0 = row.createCell(51);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.SUIT_FILED_WILFUL_DEFAULT));

			cell0 = row.createCell(52);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.WRITTEN_OFF_AND_SETTLED_STATUS));

			cell0 = row.createCell(53);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ASSET_CLASSIFICATION));

			cell0 = row.createCell(54);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.VALUE_OF_COLLATERAL));

			cell0 = row.createCell(55);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.TYPE_OF_COLLATERAL));

			cell0 = row.createCell(56);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CREDIT_LIMIT));

			cell0 = row.createCell(57);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.CASH_LIMIT));

			cell0 = row.createCell(58);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.RATE_OF_INTEREST));

			cell0 = row.createCell(59);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.REPAYMENTTENURE));

			cell0 = row.createCell(60);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.EMI_AMOUNT));

			cell0 = row.createCell(61);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.WRITTEN_OFF_AMOUNT_TOTAL));

			cell0 = row.createCell(62);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.WRITTEN_OFF_PRINICIPAL_AMOUNT));

			cell0 = row.createCell(63);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.SETTLEMENT_AMOUNT));

			cell0 = row.createCell(64);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.PAYMENT_FREEQUENCY));

			cell0 = row.createCell(65);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.ACTUAL_PAYMENT_AMOUNT));

			cell0 = row.createCell(66);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.OCCUPATION_CODE));

			cell0 = row.createCell(67);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.INCOME));

			cell0 = row.createCell(68);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.NET_GROSS_INCOME_INDICATOR));

			cell0 = row.createCell(69);
			cell0.setCellStyle(yellowStyle);
			cell0.setCellValue(new HSSFRichTextString(CICReportConstants.MONTHLY_ANNUAL_INCOME_INDICATOR));

			if (objList != null && !objList.isEmpty()) {
				index++;
				for (Object[] obj : objList) {

					try {
						HSSFCell cell01 = null;
						HSSFCell cell11 = null;
						HSSFRow rows = sheet.createRow((short) index);
						cell01 = rows.createCell(0);
						cell01.setCellValue(new HSSFRichTextString(obj[2] == null ? "" : obj[2].toString()));
						cell01 = rows.createCell(1);
						cell01.setCellValue(new HSSFRichTextString(obj[3] == null ? "" : obj[3].toString()));
						cell01 = rows.createCell(2);
						cell01.setCellValue(new HSSFRichTextString(obj[4] == null ? "" : obj[4].toString()));
						cell01 = rows.createCell(3);
						cell01.setCellValue(new HSSFRichTextString(obj[5] == null ? "" : obj[5].toString()));
						cell01 = rows.createCell(4);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(5);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(6);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(7);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(8);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(9);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(10);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(11);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(12);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(13);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(14);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(15);
						cell01.setCellValue(new HSSFRichTextString(obj[6] == null ? "" : obj[6].toString()));
						cell01 = rows.createCell(16);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(17);
						cell01.setCellValue(new HSSFRichTextString(obj[7] == null ? "" : obj[7].toString()));
						cell01 = rows.createCell(18);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(19);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(20);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(21);
						cell01.setCellValue(new HSSFRichTextString(obj[8] == null ? "" : obj[8].toString()));
						cell01 = rows.createCell(22);
						cell01.setCellValue(new HSSFRichTextString(""));
						cell01 = rows.createCell(23);
						cell01.setCellValue(new HSSFRichTextString(obj[9] == null ? "" : obj[9].toString()));

						cell01 = rows.createCell(24);
						cell01.setCellValue(new HSSFRichTextString(obj[10] == null ? "" : obj[10].toString()));

						cell01 = rows.createCell(25);
						cell01.setCellValue(new HSSFRichTextString(obj[11] == null ? "" : obj[11].toString()));

						cell01 = rows.createCell(26);
						cell01.setCellValue(new HSSFRichTextString(obj[12] == null ? "" : obj[12].toString()));

						cell01 = rows.createCell(27);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(28);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(29);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(30);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(31);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(32);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(33);
						cell01.setCellValue(new HSSFRichTextString("036FP00239"));

						cell01 = rows.createCell(34);
						cell01.setCellValue(new HSSFRichTextString("OXYLOANS"));

						cell01 = rows.createCell(35);
						cell01.setCellValue(new HSSFRichTextString(obj[13] == null ? "" : obj[13].toString()));

						cell01 = rows.createCell(36);
						cell01.setCellValue(new HSSFRichTextString("05"));

						cell01 = rows.createCell(37);
						cell01.setCellValue(new HSSFRichTextString("1"));

						cell01 = rows.createCell(38);
						cell01.setCellValue(new HSSFRichTextString(obj[14] == null ? "" : obj[14].toString()));

						cell11 = rows.createCell(39);
						cell11.setCellValue(new HSSFRichTextString(obj[15] == null ? "" : obj[15].toString()));

						cell01 = rows.createCell(40);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(41);
						cell01.setCellValue(new HSSFRichTextString(obj[16] == null ? "" : obj[16].toString()));

						cell01 = rows.createCell(42);
						cell01.setCellValue(new HSSFRichTextString(obj[17] == null ? "" : obj[17].toString()));

						cell01 = rows.createCell(43);
						cell01.setCellValue(new HSSFRichTextString(
								obj[18] == null ? obj[17] == null ? "" : obj[17].toString() : obj[18].toString()));

						cell01 = rows.createCell(44);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(45);
						cell01.setCellValue(new HSSFRichTextString(obj[21] == null ? "" : obj[21].toString()));

						cell01 = rows.createCell(46);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(47);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(48);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(49);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(50);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(51);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(52);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(53);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(54);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(55);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(56);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(57);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(58);
						cell01.setCellValue(new HSSFRichTextString(obj[19] == null ? "" : obj[19].toString()));

						cell01 = rows.createCell(59);
						cell01.setCellValue(new HSSFRichTextString(obj[20] == null ? "" : obj[20].toString()));

						cell01 = rows.createCell(60);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(61);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(62);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(63);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(64);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(65);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(66);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(67);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(68);
						cell01.setCellValue(new HSSFRichTextString(""));

						cell01 = rows.createCell(69);
						cell01.setCellValue(new HSSFRichTextString(""));

						index++;
					} catch (Exception e1) {
						logger.info("Exception occured in generateCICReport", e1);
						throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
					}
				}
			}

			for (int i = 0; i <= 69; i++) {
				sheet.autoSizeColumn(i);
			}
			wb.write(output);

			FileRequest fileRequest = new FileRequest();
			KycFileRequest kyc = new KycFileRequest();
			InputStream fileStream = new ByteArrayInputStream(output.toByteArray());

			fileRequest.setInputStream(fileStream);
			kyc.setFileName("cic_report.xlsx");
			fileRequest.setUserId(01234);
			fileRequest.setFileName(kyc.getFileName());
			fileRequest.setFileType(FileType.Agreement);
			fileRequest.setFilePrifix(FileType.Agreement.name());
			fileManagementService.putFile(fileRequest);
			FileResponse file = fileManagementService.getFile(fileRequest);

			fileResponse.setDownloadUrl(file.getDownloadUrl());
			fileResponse.setFileName("cic_report.xlsx");
			fileResponse.setKycType(KycType.CICREPORT);

			boolean deleted = fileManagementService.deleteFile(fileRequest);
			logger.warn("Deleted the File : " + deleted);

		} catch (Exception e) {
			logger.error("Exxception occured in generateCICReport CIC Report", e);

		} finally {
			try {
				if (outFile != null) {
					outFile.close();
				}

			} catch (Exception e) {
				logger.error("Exxception occured in generateCICReport CIC Report", e);

			}
		}
		return fileResponse;

	}

	@Override
	public UserResponse updatingRenewalMessageStatus(UserRequest request) {
		UserResponse response = new UserResponse();
		if (request.getId() != null) {
			User user = userRepo.findByIdNum(request.getId());

			if (user != null) {
				LenderRenewalDetails renewalDetails = lenderRenewalDetailsRepo
						.findingRenewalDetailsBasedOnUserId(user.getId());
				if (renewalDetails != null) {
					renewalDetails.setMessageSent(request.isMailsShouldSend());

					lenderRenewalDetailsRepo.save(renewalDetails);

					response.setId(user.getId());
					response.setMailsShouldSend(renewalDetails.isMessageSent());
				}
			}
		}

		return response;

	}

	@Override
	public String callbackForGetePayment(GetepayRequestDto getepayRequestDto) {
		logger.error("callbackForGetePayment method start");
		logger.error("callbackForGetePayment fields" + getepayRequestDto);
		logger.error("callbackForGetePayment method end");
		Gson gson = new Gson();
		String requestInStringForm = gson.toJson(getepayRequestDto);
		return requestInStringForm;

	}

	@Override
	public String sendBirthdayInvitationtoRegisterLender() {
		logger.info("sendBirthdayInvitationtoRegisterLender method started..");

		List<Integer> userIdList = oxyLendersAcceptedDealsRepo.getLenderIdParticitatedIntoDeal();
		logger.info("userIdList" + userIdList.size());

		StringBuilder builder = new StringBuilder();

		for (int id : userIdList) {
			Optional<User> userOptional = userRepo.findById(id);
			logger.info("userId.:" + id);

			if (userOptional.isPresent()) {
				User user = userOptional.get();
				PersonalDetails personalDetails = user.getPersonalDetails();
				if (personalDetails != null && personalDetails.getDob() != null) {
					Date currentDates = new Date(); // Current date
					Date userBirthdate = personalDetails.getDob();

					if (personalDetails.getWhatsAppNumber() != null && !personalDetails.getWhatsAppNumber().isEmpty()) {
						String whatsAppNumber = personalDetails.getWhatsAppNumber();

						Calendar currentCalendar = Calendar.getInstance();
						currentCalendar.setTime(currentDates);

						Calendar birthdateCalendar = Calendar.getInstance();

						birthdateCalendar.setTime(userBirthdate);

						int currentDay = currentCalendar.get(Calendar.DAY_OF_MONTH);
						int currentMonth = currentCalendar.get(Calendar.MONTH);

						int birthdateDay = birthdateCalendar.get(Calendar.DAY_OF_MONTH);
						int birthdateMonth = birthdateCalendar.get(Calendar.MONTH);

						birthdateCalendar.add(Calendar.DAY_OF_MONTH, -2);

						int birthdateDay2Daysminus = birthdateCalendar.get(Calendar.DAY_OF_MONTH);
						int birthdateMonth2Daysminus = birthdateCalendar.get(Calendar.MONTH);

						Calendar previousDay = Calendar.getInstance();

						previousDay.setTime(userBirthdate);
						int birthdateDay1 = birthdateCalendar.get(Calendar.DAY_OF_MONTH);
						int birthdateMonth1 = birthdateCalendar.get(Calendar.MONTH);

						previousDay.add(Calendar.DAY_OF_MONTH, -1);

						int birthdateDay1Daysminus = birthdateCalendar.get(Calendar.DAY_OF_MONTH);
						int birthdateMonth1Daysminus = birthdateCalendar.get(Calendar.MONTH);

						if ((currentDay == birthdateDay2Daysminus && currentMonth == birthdateMonth2Daysminus)
								|| ((currentDay == birthdateDay1Daysminus
										&& currentMonth == birthdateMonth1Daysminus))) {
							logger.info(" reminder to whatsApp..");

							Date registerDate = user.getRegisteredOn();
							String registerOn = expectedDateFormat.format(registerDate);

							LenderDealsStatisticsInformation lenderStatistics = newAdminLoanService
									.getTotalDealsStatistics(user.getId());

							long inviteCount = 0;
							long lentCount = 0;
							long registerCount = 0;
							List<LenderReferenceDetails> lenderReferenceDetails = lenderReferenceDetailsRepo
									.gettingListOfReferees(user.getId());

							if (lenderReferenceDetails != null && !lenderReferenceDetails.isEmpty()) {

								logger.info("lenderReferenceDetails size.." + lenderReferenceDetails.size());

								Map<String, Long> approveCountByStatus = lenderReferenceDetails.stream().collect(
										Collectors.groupingBy(x -> x.getStatus().toString(), Collectors.counting()));

								inviteCount = approveCountByStatus.getOrDefault("Invited", 0L);
								lentCount = approveCountByStatus.getOrDefault("Lent", 0L);
								registerCount = approveCountByStatus.getOrDefault("Registered", 0L);

							}

							logger.info("inviteCount.." + inviteCount);
							// logger.info("inviteCount.."+ inviteCount);

							logger.info("lentCount.." + lentCount);

							logger.info("registerCount.." + registerCount);

							builder.append(personalDetails.getFirstName()).append(" ")
									.append(personalDetails.getLastName()).append(" (LR").append(user.getId())
									.append(") has an upcoming birthday on ").append(personalDetails.getDob())
									.append(". \n\n").append("*MobileNumber:*").append(" ")
									.append(user.getMobileNumber()).append(".\n\n").append("*Email:*").append(" ")
									.append(user.getEmail()).append(".\n\n").append("*Registered on:*").append(" ")
									.append(registerOn).append(".\n\n").append("*Deal statistics:*")
									.append("\n-  Active Deal Count: ")
									.append(lenderStatistics.getNumberOfActiveDealsCount())
									.append("\n-  Closed Deal Count: ")
									.append(lenderStatistics.getNumberOfClosedDealsCount())
									.append("\n-  Disbursed Deals Count: ")
									.append(lenderStatistics.getNumberOfDisbursedDealsCount()).append("\n\n")
									.append("*Amount:*").append("\n- Active Deal Amount: ")
									.append((lenderStatistics.getActiveDealsAmount()!=null ? lenderStatistics.getActiveDealsAmount() : 0.0))
									.append("\n- Total Closed Deals Amount: ")
									.append((lenderStatistics.getClosedDealsAmount()))
									.append("\n- Total Disbursed Deals Amount: ")
									.append((lenderStatistics.getDisbursedDealsAmount()))

									.append("\n\n").append("*Referral bonus statistics:*").append("\n\n")
									.append("*Count:*")

									.append("\n- Invited: ").append(inviteCount).append("\n- Registered: ")
									.append(registerCount).append("\n- Lent: ").append(lentCount)
									.append("\n........................................................................")
									.append("\n\n");

						}

						if (currentDay == birthdateDay && currentMonth == birthdateMonth) {
							logger.info("Sending birthday invitation to: " + user.getEmail());

							TemplateContext templateContext = new TemplateContext();
							Calendar currentDate = Calendar.getInstance();
							SimpleDateFormat expectedDateFormat = new SimpleDateFormat("yyyy-MM-dd"); // Change the
																										// format
																										// as per your
																										// requirements
							String date = expectedDateFormat.format(currentDate.getTime());
							templateContext.put("userName", user.getPersonalDetails().getFirstName() + " "
									+ user.getPersonalDetails().getLastName());
							templateContext.put("date", date);

							String mailSubject = "Happy Birthday " + personalDetails.getFirstName();

							EmailRequest emailRequest = new EmailRequest(
									new String[] { user.getEmail(), "mvinodnayak46@gmail.com" }, mailSubject,
									"lender-birthday-invitation.template", templateContext);
							EmailResponse emailResponse = emailService.sendEmail(emailRequest);
							if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
								throw new RuntimeException(emailResponse.getErrorMessage());

							}
							if (whatsAppNumber != null && !whatsAppNumber.isEmpty()) {
								logger.info("Sending birthday invitation with image.");
								String message = "Dear " + personalDetails.getFirstName() + ",\n\n"
										+ "*Wishing you a fantastic birthday filled with joy and laughter!.*"
										+ " We appreciate having you as a valued member of our lending community. "
										+ "Enjoy your special day and may the year ahead bring you prosperity and success. "
										+ "*Happy birthday from all of us at Oxyloans!*" + "\n\n" + "Best regards,\n"
										+ "Team OxyLoans";

								// whatappService.sendingIndividualMessageUsingUltraApi(whatsAppNumber,
								// message);

								String image = "https://oxyloansv1.s3.ap-south-1.amazonaws.com/hbdaymsg.jpeg";

								whatappService.whatsappImageUsingUltra(whatsAppNumber, birthdayImage, message);

							}
						}

					}
				}
			}
		}
		if (builder.length() > 0) {
			whatappService.sendingWhatsappMessageWithNewInstance("120363230983045262@g.us", builder.toString());
		}
		logger.info("sendBirthdayInvitationtoRegisterLender method started..");

		return "Sucess";

	}

	@Override
	public String sendCongrastNotificationToParticipatedLenders() {

		logger.info("sendCongrastNotificationToParticipatedLenders method started.");

		List<Integer> userIdList = oxyLendersAcceptedDealsRepo.getLenderIdParticitatedIntoDeal();

		logger.info("userIdList" + userIdList.size());

		StringBuilder sb = new StringBuilder();
		
		for (int lenderUserId : userIdList) {

			int lenderParticipationCount = oxyLendersAcceptedDealsRepo.findByUserId(lenderUserId); // here iam getting
																									// count

			logger.info("sumCount.:" + lenderParticipationCount);

			Optional<User> userOptional = userRepo.findById(lenderUserId);
			logger.info("userId.:" + lenderUserId);

			if (userOptional.isPresent()) {

				User user = userOptional.get();

				if ((lenderParticipationCount) % 5 == 0) {

					if (lenderParticipationCount > user.getParticipationCount()) {

						sendCongrastEmail(user, lenderParticipationCount);

						user.setParticipationCount(lenderParticipationCount);
						
						sb.append("\n User Name: ").append(user.getPersonalDetails().getFirstName())
						  .append("\n User Id: ").append(user.getId())
						  .append("\n Participated Count: ").append(lenderParticipationCount)
						  .append("\n Mobile Number: ").append(user.getMobileNumber())
						  .append("\n .............................");

						
						userRepo.save(user);

					}
				}

			}
		}

		if(sb.length() > 0) {
		    String adminMessage = "\n Lenders Details  Congratulations Message for Successfully Participating With Count Values: "
		            + "\n" + sb.toString();

		    whatappServiceRepo.sendingWhatsappMessageWithNewInstance("120363274037838967@g.us", adminMessage);
		}
		logger.info("sendCongrastNotificationToParticipatedLenders method ended.");

		return "Success";
	}

	private void sendCongrastEmail(User user, int participationCount) {

		logger.info("sendCongrastEmail method started.");

		String messageToMobile = "Congratulations on participating in " + participationCount
				+ " successful deals on OxyLoans! " + "Your lending expertise has made a positive impact. "
				+ "Thank you for being an exceptional lender! 🙌";

		String mobileNumber = user.getPersonalDetails().getWhatsAppNumber();

		if (!mobileNumber.isEmpty()) {
			whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, messageToMobile);
		}
		TemplateContext templateContext = new TemplateContext();
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("yyyy-MM-dd"); // Change the format as per your
																					// requirements
		String date = expectedDateFormat.format(currentDate.getTime());

		templateContext.put("userName", user.getPersonalDetails().getFirstName());
		templateContext.put("date", date);
		templateContext.put("participationCount", participationCount);

		String Subject = " Congratulations on successfully participating in " + participationCount
				+ "  deals on OxyLoans!";

		EmailRequest emailRequest = new EmailRequest(
				new String[] { user.getEmail(), "mvinodnayak46@gmail.com", "radhakrishna.t@oxyloans.com",
						"ramadevi@oxyloans.com" },
				Subject, "lender-deal-participation-congrast.template", templateContext);
		EmailResponse emailResponse = emailService.sendEmail(emailRequest);
		if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
			throw new RuntimeException(emailResponse.getErrorMessage());
		}

		logger.info("sendCongrastEmail method ended.");

	}

	@Override
	public String updateTestUsers(Integer userId) {
		logger.info("updateTestUsers methods started.");

		Optional<User> userOptional = userRepo.findById(userId);
		logger.info("userId.:" + userId);
		if (userOptional.isPresent()) {
			User user = userOptional.get();
			user.setTestUser(true);
			userRepo.save(user);
		} else {
			throw new ActionNotAllowedException("User not found.", ErrorCodes.ACTION_ALREDY_DONE);
		}
		logger.info("updateTestUsers methods ended.");
		return "updateTestUsers sucess.";

	}

	@Override
	public List<TestUserResponse> getTestUserDetails() {
		logger.info("getTestUserDetails methods started.");
		List<User> listOfTestUser = userRepo.getListIfTestUser();

		List<TestUserResponse> userResponseList = new ArrayList<>();

		for (User user : listOfTestUser) {
			TestUserResponse testResponse = new TestUserResponse();

			testResponse.setEmail(user.getEmail());
			testResponse.setUserId(user.getId());
			testResponse.setMobileNumber(user.getMobileNumber());

			userResponseList.add(testResponse);
		}
		logger.info("getTestUserDetails methods ended.");
		return userResponseList;
	}

	@Override
	public UserPersonalDetailsResponse getLenderDetails(Integer id) {
		UserPersonalDetailsResponse userPersonalDetailsResponse = new UserPersonalDetailsResponse();

		User user = userRepo.findById(id).get();

		if (user != null) {

			if (user.getPersonalDetails() != null) {

				userPersonalDetailsResponse.setFirstName(
						user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName());

			}

			userPersonalDetailsResponse.setId(user.getId());
			Double currentWalletAmount = userRepo.userWalletAmount(id);

			if (currentWalletAmount != null) {
				userPersonalDetailsResponse.setLenderWalletAmount(currentWalletAmount);

			}

			userPersonalDetailsResponse.setGroupName(user.getLenderGroupId() == 0 ? "NewLender"
					: user.getLenderGroupId() == 7 ? "OXYMARCH09" : "OxyPremiuimLenders");

			LenderRenewalDetails renewalDetailss = lenderRenewalDetailsRepo
					.findingRenewalDetailsBasedOnUserIdStatus(id);

			if (renewalDetailss != null) {
				userPersonalDetailsResponse.setLenderValidityStatus(false);

			} else {
				userPersonalDetailsResponse.setLenderValidityStatus(true);

			}

			LenderRenewalDetails renewalDetails = lenderRenewalDetailsRepo
					.findingRenewalDetailsBasedOnUserId(user.getId());

			if (renewalDetails != null) {
				userPersonalDetailsResponse.setValidityDate(renewalDetails.getValidityDate());
			}

		}

		return userPersonalDetailsResponse;
	}

	@Override
	public DealAndParticipationCount countForDealAndParticipation(int userId) {
		logger.info("countForDealAndParticipation method is started...  ");

		try {
			int dealCount = oxyBorrowersDealsInformationRepo.countForTotalDeal();

			int participationCount = oxyLendersAcceptedDealsRepo.findByUserId(userId);

			logger.info("countForDealAndParticipation method is ended...  ");
			return new DealAndParticipationCount(dealCount, participationCount);
		} catch (Exception e) {

			e.printStackTrace();
			return new DealAndParticipationCount(0, 0);
		}
	}

	@Override
	public QueriesCountsResponse countUserRaiseQueries(int userId) {
		logger.info("countUserRaiseQueries  method started..");
		try {
			int approveCount = userQueryDetailsRepo.approveCountByUserId(userId);

			int cancelCount = userQueryDetailsRepo.cancelCountByUserId(userId);

			int pendingCount = userQueryDetailsRepo.pendingCountByUserId(userId);

			int allQueriesCount = approveCount + cancelCount + pendingCount;

			logger.info("countUserRaiseQueries  method ended..");
			return new QueriesCountsResponse(allQueriesCount, approveCount, cancelCount, pendingCount);

		} catch (Exception e) {
			e.printStackTrace();
			return new QueriesCountsResponse(0, 0, 0, 0);
		}
	}

	@Override
	public BulkinviteResponse sendCampaignThroughExcel(KycFileRequest kyc, String message, String inviteType,
			String whatsappImage, String mailSubject, String mailDispalyName, String projectType, String sampleEmail,
			String sampleMobile,String userType,String userid) {
		logger.info("sendCampaignThroughExcel method is started....");

		BulkinviteResponse bulknviteResponse = new BulkinviteResponse();
		BulkInviteRequest bulkInviteRequest = new BulkInviteRequest();

		int userId = Integer.parseInt(userid);
		
		String projectNameLogo = "";

		if (inviteType == null || inviteType.isEmpty()) {
			throw new EntityNotFoundException("inviteType is mandatory .");
		}
		setMessage(bulkInviteRequest, message);

		if (inviteType.equalsIgnoreCase("samplemsg") || inviteType.equalsIgnoreCase("whatsapp")) {
			setCommonWhatsAppParameters(bulkInviteRequest, inviteType, sampleMobile, whatsappImage);
		}

		if (inviteType.equalsIgnoreCase("sampleemail") || inviteType.equalsIgnoreCase("email")) {

			projectNameLogo = determineLogoName(projectType, whatsappImage);

			bulkInviteRequest.setLogo(projectNameLogo);

			// validateTemplateName(bulkInviteRequest,templateName);

			bulkInviteRequest.setProjectType(projectType);
			bulkInviteRequest.setFromMail(projectType);

			setCommonEmailParameters(bulkInviteRequest, mailSubject, mailDispalyName, sampleEmail, inviteType,
					whatsappImage);

		}

		if (inviteType.equalsIgnoreCase("whatsapp") || inviteType.equalsIgnoreCase("email")) {
			FileRequest fileRequest = new FileRequest();
			InputStream fileStream = kyc.getFileInputStream();
			if (fileStream == null) {
				fileStream = decodeBase64Stream(kyc.getBase64EncodedStream());
			}

			fileRequest.setInputStream(fileStream);
			String fileName = kyc.getFileName();
			fileRequest.setFileName(kyc.getFileName());
			logger.info(kyc.getFileName());
			fileRequest.setFilePrifix(kyc.getKycType().name());
			try {

				FileResponse putFile = fileManagementService.putFile(fileRequest);
				FileResponse getFile = fileManagementService.getFile(fileRequest);

				String[] str = getFile.getDownloadUrl().split(Pattern.quote("?"));
				String downloadUrl = str[0];

				URL url = new URL(downloadUrl);

				String fileName1 = downloadUrl.substring(downloadUrl.lastIndexOf('/') + 1);
				String saveDirectory = LocalDate.now() + "_" + fileName1 + ".xls";

				try (ReadableByteChannel readChannel = Channels.newChannel(url.openStream());
						FileOutputStream fileOS = new FileOutputStream(saveDirectory);
						FileChannel writeChannel = fileOS.getChannel()) {

					writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);
					File file = new File(saveDirectory);
					String nameAndEmail = "";
					String fileExtension = "";

					if (file.exists()) {
						int lastIndxDot = fileName1.lastIndexOf('.');
						if (lastIndxDot != -1) {
							fileExtension = fileName1.substring(lastIndxDot);
						}

						try {
							nameAndEmail = excelServiceRepo.sendBulkInvite(file, fileExtension);
						} catch (InvalidFormatException e) {
							e.printStackTrace();
						}

						if (!nameAndEmail.isEmpty()) {

							if(userType.equalsIgnoreCase("Lender")) {
								savelenderBulkinviteExcelContacts(userId,nameAndEmail);
								}
							
							bulkInviteRequest.setEmail(nameAndEmail);
							bulkInviteRequest.setInviteType(inviteType);
							bulkInviteRequest.setFileName(fileName1);
							bulkInviteRequest.setLogo(projectNameLogo);
							bulknviteResponse = excelInviteCampaign(bulkInviteRequest);
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		logger.info("sendCampaignThroughExcel method is ended....");
		return bulknviteResponse;
	}

	private void savelenderBulkinviteExcelContacts(int userId, String nameAndEmail) {
		
		logger.info("savelenderBulkinviteExcelContacts method started.. " );
	
	String[] details = nameAndEmail.split("-");
	
	  JSONArray list = new JSONArray();
	  String emailToSent = "";
	  String name = "";
	  logger.info("details: " + details);

for (String detail : details) {
    if (details.length >= 2) {
        if (details[1] != null && !details[1].isEmpty()) {
			emailToSent = details[1];
        } else {
            continue; // continue the loop if the detail is null or empty
        }
        logger.info("emailToSent: " + emailToSent);
         name = details[0] !=null ? details[0] : "";
        logger.info("name: " + name);
        list.add(name + ":" + emailToSent);		
    }
    logger.info("list: " + list.toString());

	Integer refereeEmail = lenderReferenceDetailsRepo.findByRefereeEmailInEqualIgnoranceCase(emailToSent);

	/*
	 * LenderReferenceDetails refereeMobileNumber = lenderReferenceDetailsRepo
	 * .findByRefereeMobileNumber(mobileNumber);
	 */
	LenderReferenceDetails referenceDetails = new LenderReferenceDetails();

				Integer userEmail = userRepo.findByEmailUsingEqualIgnoranceCase(emailToSent);

			
				if (refereeEmail ==null && userEmail == null) {
					referenceDetails.setRefereeEmail(emailToSent);
					referenceDetails.setRefereeName(name);
					
					referenceDetails.setReferredOn(new Date());
					referenceDetails.setReferrerId(userId);
					referenceDetails.setStatus(LenderReferenceDetails.Status.Invited);
					referenceDetails.setSource(LenderReferenceDetails.Source.BulkInvite);
					referenceDetails.setCitizenType(CitizenType.NONNRI);
					referenceDetails.setMailContent("0");
					lenderReferenceDetailsRepo.save(referenceDetails);
				}
    
					    
}

String jsonString = list.toString();


LenderFavouriteUsers favouriteUsers = lenderFavouriteUsersRepo.findbyLenderUserId(userId);

LenderFavouriteUsers usr = new LenderFavouriteUsers();

if(favouriteUsers==null) {
	
	usr.setLenderUserId(userId);

	usr.setBorrowerUserId(0);

	usr.setLenderGmailContacts(jsonString);

	usr.setGmailSize(jsonString.length());

	logger.info("contacts Inserted");

	lenderFavouriteUsersRepo.save(usr);
	
}
 logger.info("savelenderBulkinviteExcelContacts method ended.. " );}

	private String determineLogoName(String projectType, String whatsappImage) {

		logger.info("determineLogoName method is started....");
		switch (projectType.toLowerCase()) {
		case "bmv":
			return "https://bmv.money/assets/img/BMVMONEYLOGO.png";
		case "oxybricks":
			return "https://oxyloanstestv1.s3.ap-south-1.amazonaws.com/BULKINVITE_logo (1).png";
		case "studentx":
			return "https://oxybricks.world/assets/images/FNLLOGO.png";
		case "oxyloans":
			return "https://oxyloans.com/wp-content/themes/oxyloan/oxyloan/_ui/images/logo.png";
		case "erice":
			return "https://erice.in/web/images/h2.png";
		default:
			return "";
		}
	}

	private void setMessage(BulkInviteRequest bulkInviteRequest, String message) {
		if (message == null || message.isEmpty()) {
			throw new EntityNotFoundException("Message body is mandatory.");
		}
		bulkInviteRequest.setMessage(message);

	}

	private void setCommonWhatsAppParameters(BulkInviteRequest bulkInviteRequest, String inviteType,
			String sampleMobile, String whatsappImage) {
		logger.info("setCommonWhatsAppParameters method is started....");

		if (whatsappImage != null && !whatsappImage.isEmpty()) {
			bulkInviteRequest.setWhatsappImage(whatsappImage);
		}
		if (inviteType.equalsIgnoreCase("samplemsg")) {
			if (sampleMobile == null || sampleMobile.isEmpty()) {
				throw new EntityNotFoundException("sampleMobile is mandatory for samplemsg inviteType.");
			}
			if (bulkInviteRequest.getWhatsappImage() != null && !bulkInviteRequest.getWhatsappImage().isEmpty()) {
				whatappService.whatsappImageUsingUltra(sampleMobile, bulkInviteRequest.getWhatsappImage(),
						bulkInviteRequest.getMessage());
			} else {
				whatappService.sendingIndividualMessageUsingUltraApi(sampleMobile, bulkInviteRequest.getMessage());
			}
		}

	}

	private void setCommonEmailParameters(BulkInviteRequest bulkInviteRequest, String mailSubject,
			String mailDispalyName, String sampleEmail, String inviteType, String whatsappImage) {

		logger.info("setCommonEmailParameters method is started....");
		if (mailSubject == null || mailSubject.isEmpty()) {
			throw new EntityNotFoundException("mailSubject is mandatory.");
		}
		bulkInviteRequest.setMailSubject(mailSubject);

		if (mailDispalyName == null || mailDispalyName.isEmpty()) {
			throw new EntityNotFoundException("mailDisplayName is mandatory.");
		}
		bulkInviteRequest.setMailDispalyName(mailDispalyName);

		if (whatsappImage != null && !whatsappImage.isEmpty()) {
			bulkInviteRequest.setWhatsappImage(whatsappImage);
		}

		if ((inviteType.equalsIgnoreCase("sampleemail") && (sampleEmail == null || sampleEmail.isEmpty()))) {
			throw new EntityNotFoundException("sampleEmail is mandatory for sampleemail inviteType.");
		}
		if (inviteType.equalsIgnoreCase("sampleemail")) {

			Calendar currentDate = Calendar.getInstance();
			String date = expectedDateFormat.format(currentDate.getTime());
			String tempalteName = "";
			TemplateContext templateContext = new TemplateContext();
			templateContext.put("currentDate", date);
			templateContext.put("name", "oxyloans");
			templateContext.put("logo", bulkInviteRequest.getLogo());

			if (whatsappImage != null && !whatsappImage.isEmpty()) {
				templateContext.put("imageUrl", whatsappImage);
				tempalteName = "common-image.template";
			} else {
				tempalteName = "common.template";
			}
			templateContext.put("message", bulkInviteRequest.getMessage());

			// Send a single email for sampleemail
			EmailRequest emailRequest = new EmailRequest(new String[] { sampleEmail }, tempalteName, templateContext,
					bulkInviteRequest.getMailSubject(), bulkInviteRequest.getEmailCampignTemplateName());

			emailRequest.setFromMail(bulkInviteRequest.getFromMail());

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);

			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e1) {
					e1.printStackTrace();
				}

			}
		}

	}

	private BulkinviteResponse excelInviteCampaign(BulkInviteRequest bulkInviteRequest) {
		logger.info("excelInviteCampaign method is started....");
		BulkinviteResponse bulknviteResponse = new BulkinviteResponse();
		int refereeCount = 0;

		String[] mails = bulkInviteRequest.getEmail().split(",");

		int totalCount = 0;
		Map<String, String> notDuplicateEmail = new HashMap<>();
		Map<String, String> notDuplicateMobile = new HashMap<>();
		Set<String> uniqueEmails = new HashSet<>();
		Set<String> uniqueMobiles = new HashSet<>();

		for (String emailAddress : mails) {
			emailAddress = emailAddress.replaceAll(" ", "");
			String[] details = emailAddress.split("-");

			if (details.length >= 3) {
				String emailToSent;
				if (details[1] != null && !details[1].isEmpty()) {
					emailToSent = details[1];
				} else {
					// Handle the case when details[1] is null
					continue; // Skip to the next iteration
				}
				logger.info("emailToSent: " + emailToSent);
				String name = details[0]!= null ? details[0] : " " ;
				logger.info("name: " + name);
				String mobileNumber = details[2]!=null ? details[2]: " ";
				logger.info("mobileNumber: " + mobileNumber);
				notDuplicateEmail.putIfAbsent(emailToSent, name);

				// Count occurrences of mobile numbers
				notDuplicateMobile.putIfAbsent(mobileNumber, name);

				// Add unique email addresses to the set
				uniqueEmails.add(emailToSent);
				uniqueMobiles.add(mobileNumber);

				totalCount++;
			}
		}
		logger.info("totalCount: " + totalCount);

		int uniqueEmailCount = uniqueEmails.size();

		logger.info("uniqueEmailCount: " + uniqueEmailCount);
		int uniqueMobileCount = uniqueMobiles.size();

		logger.info("uniqueMobileCount: " + uniqueMobileCount);

		Calendar currentDate = Calendar.getInstance();
		String date = expectedDateFormat.format(currentDate.getTime());

		String mailSubject = bulkInviteRequest.getMailSubject();
		String dispalyName = bulkInviteRequest.getMailDispalyName();

		// Continue sending emails or WhatsApp messages based on Excel columns
		if (bulkInviteRequest.getInviteType().equalsIgnoreCase("email")) {

			for (Map.Entry<String, String> entry : notDuplicateEmail.entrySet()) {
				String email = entry.getKey();
				String name = entry.getValue();
				String tempalteName = "";
				TemplateContext templateContext = new TemplateContext();
				templateContext.put("currentDate", date);
				templateContext.put("name", name);
				templateContext.put("message", bulkInviteRequest.getMessage());
				templateContext.put("logo", bulkInviteRequest.getLogo());

				if (bulkInviteRequest.getWhatsappImage() != null && !bulkInviteRequest.getWhatsappImage().isEmpty()) {
					templateContext.put("imageUrl", bulkInviteRequest.getWhatsappImage());
					tempalteName = "common-image.template";
				} else {
					tempalteName = "common.template";
				}

				EmailRequest emailRequest = new EmailRequest(new String[] { email }, tempalteName, templateContext,
						mailSubject, dispalyName);

				emailRequest.setFromMail(bulkInviteRequest.getFromMail());
				EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);

				if (emailResponseFromService.getStatus() == EmailResponse.Status.SUCCESS) {

					logger.info("Suceess Mail sent to Users:" + email);
					refereeCount++;
				} else {

					logger.info("Email sending failed for email: " + email);
					logger.info("Error message: " + emailResponseFromService.getErrorMessage());
					// You can also log the error using a logger instead of printing to the console
					// logger.error("Email sending failed for email: " + email, e1);

					// Continue with the next iteration of the loop
					continue;
				}
			}
			logger.info("Suceess Mail Count sent :" + refereeCount);
		} else if (bulkInviteRequest.getInviteType().equalsIgnoreCase("whatsapp")) {

			for (Map.Entry<String, String> entry : notDuplicateMobile.entrySet()) {
				String mobileNumber = entry.getKey();
				String name = entry.getValue();
				String whatsappMessage = "Hi " + name + "\n\n" + bulkInviteRequest.getMessage();

				if (bulkInviteRequest.getWhatsappImage() != null && !bulkInviteRequest.getWhatsappImage().isEmpty()) {
					whatappService.whatsappImageUsingUltra(mobileNumber, bulkInviteRequest.getWhatsappImage(),
							bulkInviteRequest.getMessage());
				} else {
					whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, whatsappMessage);
				}
				refereeCount++;

			}
		}

		bulknviteResponse.setCount(refereeCount);
		bulknviteResponse.setFileName(bulkInviteRequest.getFileName());
		bulknviteResponse.setStatus("Successfully invited.");
		bulknviteResponse.setTotalCount(totalCount);
		bulknviteResponse.setUniqueEmailCount(uniqueEmailCount);
		bulknviteResponse.setUniqueMobileCount(uniqueMobileCount);
		logger.info("excelInviteCampaign method is ended.....");
		return bulknviteResponse;
	}

	@Override
	public String convertUrltoPgnimage(KycFileRequest kyc) {

		logger.info("convertUrltoPgnimage method is started...");

		FileRequest fileRequest = new FileRequest();
		InputStream fileStream = kyc.getFileInputStream();
		if (fileStream == null) {
			fileStream = decodeBase64Stream(kyc.getBase64EncodedStream());
		}
		String json = null;
		fileRequest.setInputStream(fileStream);
		String fileName = kyc.getFileName();
		fileRequest.setFileName(kyc.getFileName());
		logger.info(kyc.getFileName());
		fileRequest.setFilePrifix(kyc.getKycType().name());
		try {

			FileResponse putFile = fileManagementService.putFile(fileRequest);
			FileResponse getFile = fileManagementService.getFile(fileRequest);

			String[] str = getFile.getDownloadUrl().split(Pattern.quote("?"));
			String downloadUrl = str[0];

			URL url = new URL(downloadUrl);
			Map<String, Object> jsonObject = new HashMap<>();
			jsonObject.put("downloadUrl", downloadUrl);

			// Use the Jackson ObjectMapper to convert the Map to a JSON object
			ObjectMapper objectMapper = new ObjectMapper();
			json = objectMapper.writeValueAsString(jsonObject);

		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return json;

	}

	@Override
	public List<EmailAddressResponse> getAllContactsFromGmailAccount(int userId, GmailSignInResponse gmailResponse) 
			throws IOException, GeneralSecurityException, InterruptedException{

		logger.info("fetching Gmail Contacts started");

		List<EmailAddressResponse> mailresponse = new ArrayList<>();

		 //LenderFavouriteUsers favouriteUsers = lenderFavouriteUsersRepo.findbyLenderUserId(userId);

		LenderFavouriteUsers usr = new LenderFavouriteUsers();

		int count = 0;

		JSONArray list = new JSONArray();

		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

		String url = "";

		if (gmailResponse.getUserType().equalsIgnoreCase("lender")) {
			url = redirectUri;
		}

		if (gmailResponse.getUserType().equalsIgnoreCase("borrower")) {
			url = redirectUri;
		}

		if (gmailResponse.getUserType().equalsIgnoreCase("Partner")) {
			url = gmailPartnerRedirectUri;
		}

		logger.info("url 2 " + url);

		gmailResponse.setGmailRedirectUri(url);

		PeopleService service = new PeopleService.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				getGmailCredentials(gmailResponse)).setApplicationName(APPLICATION_NAME).build();


		  // Set up and execute the Contacts API request
    
      List<Person> allContacts = new ArrayList<>();
      String nextPageToken = null;

      ListConnectionsResponse connectionsResponse = service.people().connections().list("people/me")
              .setPersonFields("names,phoneNumbers")
              .execute();

      logger.info("connectionsResponse size  " + connectionsResponse.size());
      
      List<Person> connections = connectionsResponse.getConnections();
      
      if (connections != null && !connections.isEmpty()) {
          allContacts.addAll(connections);
      }

      nextPageToken = connectionsResponse.getNextPageToken()!= null ? connectionsResponse.getNextPageToken() : "null";

      // If there are more pages, fetch them
      while (nextPageToken != null) {
          connectionsResponse = service.people().connections().list("people/me")
                  .setPersonFields("names,phoneNumbers")
                  .setPageToken(nextPageToken)
                  .execute();

          connections = connectionsResponse.getConnections();

          if (connections != null && !connections.isEmpty()) {
              allContacts.addAll(connections);
          }

          nextPageToken = connectionsResponse.getNextPageToken();
      }
      String personName = "";
      
      String mobileNUmber = "";
      
      if (!allContacts.isEmpty()) {
      	
      logger.info("Your Contacts size :"+ allContacts.size());
          
          for (Person person : allContacts) {
          	
          	
          	
        	  EmailAddressResponse emailAdressResponse = new EmailAddressResponse();
          	
          	List<Name> names = person.getNames();

          	logger.info("names: " + names.size());
          	
  		List<PhoneNumber> phoneNumber = person.getPhoneNumbers();
  			
  		logger.info("phoneNumber: " + phoneNumber.size());
  		
  			if (names != null && names.size() > 0 && !names.isEmpty()) {

  				personName = person.getNames().get(0).getDisplayName()!=null ? person.getNames().get(0).getDisplayName() : " ";
  				
  				 logger.info("Contact Name: " + personName);

  			} else {

  				personName = "";

  			}
  			
  			if(phoneNumber!=null && phoneNumber.size()>0 && !phoneNumber.isEmpty() ) {
  				
  				
  				mobileNUmber = person.getPhoneNumbers().get(0).getValue() != null ? person.getPhoneNumbers().get(0).getValue() : "";
  				
  				 logger.info("Your Person  Contacts :"+ mobileNUmber);
  			}
  			else {
  				
  				mobileNUmber = "";
  			}
          	
             
             
              if (!mobileNUmber.equals("")) {

              	emailAdressResponse.setContactName(personName);

              	emailAdressResponse.setMobileNumber(mobileNUmber);

  				mailresponse.add(emailAdressResponse);

  				list.add(personName + ":" + emailAdressResponse);

  			}

          }
          String jsonString = list.toString();
    		
    		if(jsonString.length()>0) {
    			
    			 logger.info("Your jsonString :"+ jsonString.length());
    		}
          
          
      } else {
          System.out.println("No contacts found.");
      }
		
		return mailresponse;
	}
}
