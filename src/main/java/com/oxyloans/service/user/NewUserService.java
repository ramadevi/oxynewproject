package com.oxyloans.service.user;

import java.util.List;

import javax.mail.MessagingException;

import com.oxyloans.partner.PartnerRegistrationRequestDto;
import com.oxyloans.partner.PartnerRegistrationResponseDto;
import com.oxyloans.registration.UserRegistrationRequestDto;
import com.oxyloans.registration.UserRegistrationResponseDto;
import com.oxyloans.request.PartnerLoanInfoResponse;
import com.oxyloans.request.PartnerNDArequestDto;
import com.oxyloans.request.PartnerRequestDto;
import com.oxyloans.request.user.ReferenceDetailsRequestDto;
import com.oxyloans.request.user.ReferenceDetailsResponseDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.LenderMailResponse;
import com.oxyloans.response.user.LendersWithdrawResponse;
import com.oxyloans.response.user.OxyUserDetailsResponse;
import com.oxyloans.response.user.PartnerResponseDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.usertypedto.OxyUserTypeRequestDto;
import com.oxyloans.usertypedto.OxyUserTypeResponseDto;

public interface NewUserService {

	public UserRegistrationResponseDto UserNewRegistration(UserRegistrationRequestDto userRegistrationRequestDto);

	public UserRegistrationResponseDto userEmailVerification(UserRegistrationRequestDto userRegistrationRequestDto);

	public UserRegistrationResponseDto sendingEmailActivationLink(
			UserRegistrationRequestDto userRegistrationRequestDto);

	public ReferenceDetailsResponseDto updateReferenceDetailsToBorrower(
			ReferenceDetailsRequestDto referenceDetailsRequestDto);

	public UserRegistrationResponseDto oldUsersRegistrationStep2Pending(
			UserRegistrationRequestDto userRegistrationRequestDto);

	public UserResponse savingPartnerDetails(UserRequest userRequest);

	public UserResponse resetpasswordForPartners(UserRequest userRequest);

	public OxyUserTypeResponseDto generatingPasswordForPartners();

	public OxyUserDetailsResponse gettingPartnerDetailsBasedOnUtm(String utmName);

	public PartnerResponseDto partnerRegistrationFlow(PartnerRequestDto requestDto);

	public LenderMailResponse showingMailContentToPartner(Integer partnerId);

	public LendersWithdrawResponse withdrawalInformationBasedOnId(Integer dealId);

	public PartnerResponseDto verifyingMobileAndEmailForPartner(OxyUserTypeRequestDto requestDto)
			throws MessagingException;

	public PartnerResponseDto gettingCountOfLendersAndBorrowersForPartner(String utmName);

	public PartnerResponseDto bankDetailsForPartner(String utmName, OxyUserTypeRequestDto requestDto);

	public PartnerResponseDto partnerProfileDetails(String utmName);

	public PartnerResponseDto preparingPartnerNDA(String utmName, PartnerNDArequestDto requestDto);

	public PartnerRegistrationResponseDto additionalFieldsApprovalFromAdmin(int id,
			PartnerRegistrationRequestDto partnerRegistrationRequestDto);

	public List<PartnerLoanInfoResponse> partnerBorrowersLoanInfo(String utmName);

	public UserResponse sendMobileOtp(UserRequest userRequest);

	public UserResponse verifyMobileOtpValue(UserRequest request);

	public UserRegistrationResponseDto forgotPasswordForMobileApp(
			UserRegistrationRequestDto userRegistrationRequestDto);

	public UserRegistrationResponseDto forgotPasswordForMobileAppVerification(
			UserRegistrationRequestDto userRegistrationRequestDto);

	public UserRegistrationResponseDto updateSiteTool(int userId);

	public UserResponse getUserUniqueNumber(int userId);

	public UserRegistrationResponseDto updateCredentialByInvalidEmailEnterAtTimeOfRegistration(
			UserRegistrationRequestDto userRegistrationRequestDto);

	public String generateSalt();

	public String generatePassword(String password, String salt);

}
