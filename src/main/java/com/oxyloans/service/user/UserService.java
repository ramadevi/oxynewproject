package com.oxyloans.service.user;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

import javax.mail.MessagingException;

import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.JsonParseException;
import com.oxyloans.enach.scheduler.EnachMinimunWithdrawUsersDto;
import com.oxyloans.enach.scheduler.EnachMiniumWithDrwUserRequest;
import com.oxyloans.enach.scheduler.EnachTypeRequestDto;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceResponse;
import com.oxyloans.entityborrowersdealsdto.DealAndParticipationCount;
import com.oxyloans.entityborrowersdealsdto.LenderInterestRequestDto;
import com.oxyloans.entityborrowersdealsdto.LenderInterestResponseDto;
import com.oxyloans.entity.expertSeekers.AdviseSeekersResponseDto;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.entity.user.Document.status.UserDocumentStatus;
import com.oxyloans.file.FileResponse;
import com.oxyloans.icici.upi.GetepayRequestDto;
import com.oxyloans.lender.wallet.ScrowLenderTransactionResponse;
import com.oxyloans.lender.wallet.ScrowTransactionRequest;
import com.oxyloans.lender.wallet.ScrowWalletResponse;
import com.oxyloans.pincodeutil.PincodeResultes;
import com.oxyloans.request.PageDto;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.request.user.BulkinviteResponse;
import com.oxyloans.request.user.KycFileRequest;
import com.oxyloans.request.user.KycFileRequest.KycType;
import com.oxyloans.request.user.KycFileResponse;
import com.oxyloans.request.user.LenderReferenceRequestDto;
import com.oxyloans.request.user.NomineeRequestDto;
import com.oxyloans.request.user.PersonalDetailsRequestDto;
import com.oxyloans.request.user.ProfessionalDetailsRequestDto;
import com.oxyloans.request.user.QueriesCountsResponse;
import com.oxyloans.request.user.TestUserResponse;
import com.oxyloans.request.user.UserQueryDetailsRequestDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.admin.AdminResponse;
import com.oxyloans.response.user.BorrowersLoanOwnerInformation;
import com.oxyloans.response.user.ContactDetailsForIos;
import com.oxyloans.response.user.EmailAddressResponse;
import com.oxyloans.response.user.GmailSignInResponse;
import com.oxyloans.response.user.LenderMailResponse;
import com.oxyloans.response.user.LenderProfitResponse;
import com.oxyloans.response.user.LenderRatingRequestDto;
import com.oxyloans.response.user.LenderRatingResponseDto;
import com.oxyloans.response.user.LenderReferenceAmountResponseDto;
import com.oxyloans.response.user.LenderReferralPaymentStatus;
import com.oxyloans.response.user.NomineeResponseDto;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.response.user.PaymentSearchByStatus;
import com.oxyloans.response.user.PaymentUploadHistoryRequestDto;
import com.oxyloans.response.user.PaymentUploadHistoryResponseDto;
import com.oxyloans.response.user.PersonalDetailsResponseDto;
import com.oxyloans.response.user.ProfessionalDetailsResponseDto;
import com.oxyloans.response.user.StatusResponseDto;
import com.oxyloans.response.user.UserPersonalDetailsResponse;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.response.user.UserVanNumberResponseDto;
import com.oxyloans.response.user.ValidityResponse;
import com.oxyloans.serviceloan.ApplicationResponseDto;
import com.oxyloans.serviceloan.LoanResponseDto;

public interface UserService {

	public UserResponse sendOtp(UserRequest userRequest) throws MessagingException;

	public UserResponse validateUser(UserRequest userRequest);

	public UserResponse uploadKyc(Integer userId, List<KycFileRequest> kycFiles)
			throws FileNotFoundException, IOException;

	public UserResponse uploadProfilePic(Integer userId, KycFileRequest profilePic)
			throws FileNotFoundException, IOException;

	public UserResponse uploadContacts(Integer userId, KycFileRequest contacts)
			throws FileNotFoundException, IOException;

	public KycFileResponse downloadFile(Integer userId, KycType kycFile) throws FileNotFoundException, IOException;

	public UserResponse verifyEmail(UserRequest userRequest, String emailToken);

	public boolean deleteByMobileNumber(String mobileNumber);

	public AdminResponse totalNumberOfUsers();

	public UserResponse resetPasswordSendEmail(UserRequest userRequest) throws MessagingException;

	public ProfessionalDetailsResponseDto updateProfessionalDetails(int id,
			ProfessionalDetailsRequestDto ProfessionalDetailsRequestDto);

	public PersonalDetailsResponseDto updatePersonalDetails(int id,
			PersonalDetailsRequestDto personalDetailsRequestDto);

	public List<UserResponse> getNewUsersData(String type);

	public PersonalDetailsResponseDto getPersonalDetails(Integer id);

	public UserPersonalDetailsResponse getLenderDetails(Integer id);

	public UserResponse updateOxyScore(int userId, UserRequest userRequest);

	public UserResponse migrate(int userId, PrimaryType toPrimaryType, boolean deleteOldData);

	public UserResponse saveCity(int id, UserRequest userRequest);

	public List<UserDocumentStatus> getDetails(int userId);

	public PincodeResultes saveCity1(String pinCode) throws ParseException;

	public PersonalDetailsResponseDto updateProfilePage(int userId,
			PersonalDetailsRequestDto personalDetailsRequestDto);

	public PersonalDetailsResponseDto ProfilePageSubmissionAtATime(int userId,
			PersonalDetailsRequestDto personalDetailsRequestDto);

	public UserResponse getLoanRequestedAmountAndPaymentFeeRoi(int userId);

	public UserResponse updateLoanRequestedAmountAndPaymentFeeRoi(int userId, UserRequest userRequest);

	public UserResponse calculatingFeeValue(int userId);

	public UserResponse uploadScowTransactionDetails(Integer userId, List<KycFileRequest> kycFiles)
			throws FileNotFoundException, IOException;

	public ScrowLenderTransactionResponse saveScrowTransactionDetails(int userId,
			ScrowTransactionRequest scrowTransactionRequest);

	public KycFileResponse downloadTransactionScreenshot(Integer documentId, Integer userId, KycType kycFile)
			throws FileNotFoundException, IOException;

	public ScrowWalletResponse getLenderWalletDetails(int userId, PageDto pageDto);

	public UserResponse loanOfferAccepetence(UserRequest userRequest);

	public ScrowLenderTransactionResponse approveLenderWalletTransaction(int documentId);

	public ScrowWalletResponse searchlenderwallet(ScrowTransactionRequest scrowTransactionRequest, PageDto pageDto);

	public KycFileResponse generateCICReport();

	public ScrowLenderTransactionResponse updateEscrowWalletTransaction(int documnetId,
			ScrowTransactionRequest scrowTransactionRequest);

	public SearchResultsDto<ApplicationResponseDto> loansByApplication(int userId, ScrowTransactionRequest dto);

	public LoanResponseDto updEnachType(int userId, EnachTypeRequestDto enachTypeRequestDto);

	public UserResponse saveEanchMinimumWtdrwUsers(EnachMiniumWithDrwUserRequest request);

	public SearchResultsDto<EnachMinimunWithdrawUsersDto> getEnachMinimumWithdrawUsers(PageDto pageDto);

	public SearchResultsDto<EnachMinimunWithdrawUsersDto> searchEnachMinimumWithdrawUsers(ScrowTransactionRequest dto);

	public NomineeResponseDto nomineeDetails(NomineeRequestDto nomineeRequestDto);

	public NomineeResponseDto getNominee(Integer id);

	public int kycPendingSchedular();

	public UserResponse registractionForOxyfarms(UserRequest userRequest);

	public KycFileResponse uploadContactByIos(int userId, ContactDetailsForIos contactDetailsForIos) throws IOException;

	public KycFileResponse getIosContactDetails(int userId);

	public UserResponse uploadPaymentScreenshot(String uniqueNumber, List<KycFileRequest> kycFiles)
			throws FileNotFoundException, IOException;

	public PaymentUploadHistoryResponseDto uploadPaymentHistory(
			PaymentUploadHistoryRequestDto paymentUploadHistoryRequestDto);

	public LenderRatingResponseDto lenderRatingToBorrowerApplication(Integer parentRequestId,
			LenderRatingRequestDto lenderRatingRequestDto);

	public List<UserResponse> borrowerInformation(String uniqueNumber);

	public PaymentUploadHistoryResponseDto countOfPaymentScreenshot(Integer userId);

	public PaymentUploadHistoryResponseDto updatePaymentUploadHistoryStatus(int documentId,
			PaymentUploadHistoryRequestDto paymentUploadHistoryRequestDto);

	public SearchResultsDto<PaymentUploadHistoryResponseDto> getUpdatedInfoByOxyMembers(String updatedName,
			String paymentStatus);

	public LenderProfitResponse getLenderProfitInformation(String startDate, String endDate);

	public String sendingMailToApprovePayments();

	public LenderProfitResponse getLenderProfitDetails(int userId, int month);

	public UserVanNumberResponseDto fetchBorrowerVanNumber(Integer userId);

	public PaymentSearchByStatus getPaymentUploadedListAndCount(String status,
			PaginationRequestDto paginationRequestDto);

	public PaymentSearchByStatus getPaymentStatistics(int days, String paymentStatus,
			PaginationRequestDto paginationRequestDto);

	public LenderReferenceResponse lenderReferenceDetails(LenderReferenceRequestDto lenderRefernceDto);

	public LenderReferenceResponse displayingRefereesInfo(Integer lenderId, PaginationRequestDto pageRequestDto);

	public LenderReferenceResponse gettingLenderRefereesRegisteredInfo(Integer lenderId,
			PaginationRequestDto pageRequestDto);

	public LenderMailResponse showingMailContentToLender();

	public FileResponse uploadExcelSheets(List<KycFileRequest> files, String date);

	public FileResponse downloadExcelSheets(String date, String kyc);

	public List<EmailAddressResponse> getContactsFromGmailAccount(Integer userId, GmailSignInResponse response)
			throws IOException, GeneralSecurityException, InterruptedException;

	public GmailSignInResponse getGmailAuthorization(String type, String userType, String projectType)
			throws IOException, GeneralSecurityException, InterruptedException;

	public List<EmailAddressResponse> getLenderStoredEmailContacts(int userId)
			throws JsonParseException, JsonMappingException, IOException;

	public LenderReferenceAmountResponseDto gettingLenderReferenceAmountDetails(String refereeId1,
			PaginationRequestDto pageRequestDto);

	public List<LenderReferralPaymentStatus> displayingRefereePaymentStatus(String refereeId1);

	public BorrowersLoanOwnerInformation displayingLoanOwnerNames(PaginationRequestDto pageRequestDto);

	public BorrowersLoanOwnerInformation gettingLenderBorrowerOwner(String loanOwnerName,
			PaginationRequestDto pageRequestDto);

	public PersonalDetailsResponseDto updatingWhatsAppNumberForExistingUser(Integer id,
			PersonalDetailsRequestDto userRequest);

	public StatusResponseDto readingQueriesFromUsers(Integer userId, UserQueryDetailsRequestDto queryRequestDto);

	public UserResponse uploadUserQueryScreenshot(Integer userId, List<KycFileRequest> files) throws IOException;

	public UserResponse sendWhatsappOtp(UserRequest userRequest);

	public UserResponse verifyWhatsappOtp(UserRequest userRequest);

	public StatusResponseDto assignUserExpertise(UserRequest request);

	public List<AdviseSeekersResponseDto> fetchAdviseSeekers(int userId);

	public LenderReferenceResponse sendBulkInviteThroughExcel(KycFileRequest kyc, int userId, String content);

	public List<ValidityResponse> gettingValidityDetails();

	public LenderInterestResponseDto lenderBankDetailsVerification(LenderInterestRequestDto lenderInterestRequestDto);

	public LenderReferenceResponse savingBonusPaymentStatusFromExcel(List<KycFileRequest> files) throws IOException;

	public LenderReferenceResponse savingBonusPaymentStatusFromExcelForDealIds(List<KycFileRequest> files)
			throws IOException;

	public UserResponse newRegister(UserRequest userRequest) throws MessagingException;

	public UserResponse uploadPartnerAgreementType(Integer partnerId, List<KycFileRequest> files) throws IOException;

	public KycFileResponse downloadPartnerAgreementType(Integer partnerId, KycType kycFile)
			throws FileNotFoundException, IOException;

	public KycFileResponse generateCICReportBasedOnDates(String startDate, String endDate);

	public UserResponse updatingRenewalMessageStatus(UserRequest request);

	public String callbackForGetePayment(GetepayRequestDto getepayRequestDto);

	public String sendBirthdayInvitationtoRegisterLender();

	public String sendCongrastNotificationToParticipatedLenders();

	public String updateTestUsers(Integer userId);

	public List<TestUserResponse> getTestUserDetails();

	public DealAndParticipationCount countForDealAndParticipation(int userId);

	public QueriesCountsResponse countUserRaiseQueries(int userId);
	
	public BulkinviteResponse sendCampaignThroughExcel(KycFileRequest fileReqest, String message, String inviteType,
			String whatsappImage,String mailSubject,String mailDispalyName,String projectType,String sampleEmail,
			String sampleMobile,String userType,String userid);

	public String convertUrltoPgnimage(KycFileRequest fileReqest);

	public List<EmailAddressResponse> getAllContactsFromGmailAccount(int userId, GmailSignInResponse response)
			throws IOException, GeneralSecurityException, InterruptedException;

}
