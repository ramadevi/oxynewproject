package com.oxyloans.service.user;

import com.oxyloans.coreauthdto.GenericAccessToken;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.UserResponse;

public interface ILoginService {

	public UserResponse login(UserRequest userRequest);

	public GenericAccessToken verifyToken(String accessToken, String decodeToken);

	public void logout(String accessToken);

	public UserResponse accessToken(int userId, String userType);

}
