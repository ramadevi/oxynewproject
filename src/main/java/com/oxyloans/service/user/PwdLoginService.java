package com.oxyloans.service.user;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.oxyloans.authenticationexception.InvalidCredentialsException;
import com.oxyloans.coreauthdto.GenericAccessToken;
import com.oxyloans.coreauthdto.GenericAccessToken.EncodingAlgorithm;
import com.oxyloans.coreauthdto.PasswordGrantAccessToken;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.type.OxyUserType;
import com.oxyloans.entity.user.type.OxyUserTypeRepo;
import com.oxyloans.entity.userlogin.UserLoginHistory;
import com.oxyloans.repository.userloginrepo.UserLoginHistoryRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.security.signature.ISignatureService;

@Service("PWD")
public class PwdLoginService extends AbstractLoginService {

	private static final Logger logger = LogManager.getLogger(PwdLoginService.class);

	@Autowired
	private UserService userService;

	@Autowired
	private ISignatureService signatureService;

	@Value("${accessTokenAlias}")
	private String accessTokenAlias;

	private long defaultTtl = 2 * 60 * 60; // Make this configurable

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private OxyUserTypeRepo oxyUserTypeRepo;

	@Autowired
	private UserLoginHistoryRepo userLoginHistoryRepo;

	/*
	 * @Autowired private OtplessLoginService otplessLoginService;
	 */
	@Override
	public UserResponse doLogin(UserRequest userRequest) {
		UserResponse user = userService.validateUser(userRequest);
		PasswordGrantAccessToken accessToken = new PasswordGrantAccessToken(defaultTtl, EncodingAlgorithm.RSA);
		accessToken.setUserId(user.getId().toString());
		String signedString = null;
		try {
			signedString = signatureService.sign(this.accessTokenAlias, EncodingAlgorithm.RSA.getAlgo(),
					new Gson().toJson(accessToken));
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		}
		user.setAccessToken(signedString);
		user.setTokenGeneratedTime(accessToken.getIat());
		UserLoginHistory userLoginHistory = new UserLoginHistory();
		userLoginHistory.setUserId(user.getId());
		userLoginHistory.setRequestBody("");
		/*
		 * IpAddressResponseDto ipAddressResponseDto = null; try { ipAddressResponseDto
		 * = otplessLoginService.ipAddressGeneration(); } catch (IOException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); } if
		 * (ipAddressResponseDto != null) {
		 * userLoginHistory.setIpAddress(ipAddressResponseDto.getIpAddress());
		 * userLoginHistory.setResponseBody(ipAddressResponseDto.getIpAddressResponse())
		 * ; // userLoginHistory.setResponseBody("same"); }
		 */
		userLoginHistoryRepo.save(userLoginHistory);
		return user;
	}

	@Override
	public GenericAccessToken doVerifyToken(String accessToken, String decodeToken) {
		boolean verified = false;
		try {
			verified = signatureService.verify(this.accessTokenAlias, EncodingAlgorithm.RSA.getAlgo(), accessToken);
			logger.info("User Token verified {}", verified);
		} catch (InvalidKeyException e) {
			logger.error(e, e);
		} catch (NoSuchAlgorithmException e) {
			logger.error(e, e);
		} catch (SignatureException e) {
			logger.error(e, e);
		}
		if (!verified) {
			throw new InvalidCredentialsException("Accesstoken is not valid", ErrorCodes.TOKEN_NOT_VALID);
		}
		return new Gson().fromJson(decodeToken, PasswordGrantAccessToken.class);
	}

	@Override
	public long getTtl(UserRequest userRequest) {
		return this.defaultTtl;
	}

	@Override
	public UserResponse accessTokenRegerating(int userId, String userType) {
		UserResponse user = new UserResponse();
		Integer id = null;
		if (userType.equalsIgnoreCase("USER")) { // from user table
			User userDetails = userRepo.findById(userId).get();
			if (userDetails != null) {
				id = userDetails.getId();
			}
		} else {
			if (userType.equalsIgnoreCase("PARTNER")) {
				OxyUserType partnerDetails = oxyUserTypeRepo.findById(userId).get(); // from partner table
				if (partnerDetails != null) {
					id = partnerDetails.getId();
				}
			}
		}

		PasswordGrantAccessToken accessToken = new PasswordGrantAccessToken(defaultTtl, EncodingAlgorithm.RSA);
		accessToken.setUserId(id.toString());
		String signedString = null;
		try {
			signedString = signatureService.sign(this.accessTokenAlias, EncodingAlgorithm.RSA.getAlgo(),
					new Gson().toJson(accessToken));
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		}
		user.setAccessToken(signedString);
		user.setId(id);
		user.setTokenGeneratedTime(accessToken.getIat());

		return user;
	}

}
