package com.oxyloans.service.user;

public interface LoginServiceFactory {
	
	public ILoginService getService(String grantType);

}
