package com.oxyloans.service.user;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import com.oxyloans.authenticationexception.AccessTokenMissingException;
import com.oxyloans.authenticationexception.InvalidAccessTokenException;
import com.oxyloans.coreauthdto.GenericAccessToken;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.entity.user.type.OxyUserType;
import com.oxyloans.entity.user.type.OxyUserTypeRepo;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.UserResponse;

public abstract class AbstractLoginService implements ILoginService {

	@Autowired
	private RedisTemplate<String, String> redisStringValueTemplate;

	@Autowired
	private OxyUserTypeRepo oxyUserTypeRepo;

	@Override
	public UserResponse login(UserRequest userRequest) {
		UserResponse userResponse = doLogin(userRequest);
		this.redisStringValueTemplate.opsForValue().set(userResponse.getAccessToken(), userResponse.getId().toString(),
				getTtl(userRequest));
		return userResponse;
	}

	public abstract long getTtl(UserRequest userRequest);

	public abstract UserResponse doLogin(UserRequest userRequest);

	@Override
	public GenericAccessToken verifyToken(String accessToken, String decodeToken) {
		String token = this.redisStringValueTemplate.opsForValue().get(accessToken);
		if (StringUtils.isEmpty(token)) {
			throw new InvalidAccessTokenException("Invalid Token please login", ErrorCodes.TOKEN_NOT_VALID);
		}
		return doVerifyToken(accessToken, decodeToken);
	}

	@Override
	public void logout(String accessToken) {
		if (StringUtils.isEmpty(accessToken)) {
			throw new AccessTokenMissingException("accessToken not passing", ErrorCodes.TOKEN_NOT_VALID); // Create new
																											// Exception
																											// and throw
		}
		redisStringValueTemplate.delete(accessToken);
	}

	public abstract GenericAccessToken doVerifyToken(String accessToken, String decodeToken);

	public UserResponse accessToken(int userId, String userType) {
		UserRequest userRequest = new UserRequest();

		if (userType.equalsIgnoreCase("USER")) {
			userRequest.setId(userId);
			userRequest.setPrimaryType("SUPERADMIN");
		} else {
			OxyUserType oxyuserType = oxyUserTypeRepo.findById(userId).get();
			if (oxyuserType != null) {
				userRequest.setPartnerUtmName(oxyuserType.getUtmName());
				userRequest.setPartnerPassword("SUPERADMIN");
			}

		}
		UserResponse response = doLogin(userRequest);

		this.redisStringValueTemplate.opsForValue().set(response.getAccessToken(), response.getId().toString(),
				getTtl(userRequest));

		return response;
	}

	public abstract UserResponse accessTokenRegerating(int userId, String userType);

	
}
