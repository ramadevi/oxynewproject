package com.oxyloans.service.withdrawalfundsservice;

import java.util.List;

import com.oxyloans.entityborrowersdealsdto.LenderPaticipatedDeal;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsFromDealsRequestDto;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsFromDealsResponseDto;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsRequest;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsResponse;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsSearchRequest;
import com.oxyloans.lender.withdrawalfunds.dto.ListOfLendersWithdrawalFundsInfo;
import com.oxyloans.lender.withdrawfunds.entity.LenderWithdrawalFunds;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.response.user.PaginationRequestDto;

public interface LenderWithdrawalFundsService {

	public LenderWithdrawalFundsResponse saveWithDwalFundsInfo(LenderWithdrawalFundsRequest request);

	public SearchResultsDto<LenderWithdrawalFundsResponse> withdrawalFundsSearch(
			LenderWithdrawalFundsSearchRequest request);

	public LenderWithdrawalFundsResponse updateWithdrawalFundsStatus(LenderWithdrawalFundsRequest request);

	public List<LenderWithdrawalFunds> withdrawalRequestsScheduler();

	public LenderWithdrawalFundsResponse lenderWithdrawFundsInfo(int userId);

	public LenderPaticipatedDeal getLenderPaticipatedDealsBasedOnDealType(int userId,
			PaginationRequestDto pageRequestDto);

	public LenderWithdrawalFundsFromDealsResponseDto withdrawalFundsFromDeals(
			LenderWithdrawalFundsFromDealsRequestDto lenderWithdrawalFundsFromDealsRequestDto);

	public ListOfLendersWithdrawalFundsInfo listOfLendersWithdrawalInfo(PaginationRequestDto pageRequestDto);

	public LenderWithdrawalFundsFromDealsResponseDto approvingWithdrawalFund(
			LenderWithdrawalFundsFromDealsRequestDto lenderWithdrawalFundsFromDealsRequestDto);

	public ListOfLendersWithdrawalFundsInfo getWithdrawalRequestedInfo(int userId, int dealId);

	public ListOfLendersWithdrawalFundsInfo h2hApprovalForWithDrawalInterestAmount(
			ListOfLendersWithdrawalFundsInfo listOfLendersWithdrawalFundsInfo);

	public LenderWithdrawalFundsFromDealsResponseDto getWithDrawalApprovedByAdmin(int id);

}
