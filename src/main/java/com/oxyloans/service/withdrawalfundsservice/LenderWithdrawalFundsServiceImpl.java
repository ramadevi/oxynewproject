package com.oxyloans.service.withdrawalfundsservice;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.oxyloans.commonutil.DateUtil;
import com.oxyloans.customexceptions.DataFormatException;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entityborrowersdealsdto.LenderPaticipatedDeal;
import com.oxyloans.entityborrowersdealsdto.LenderPaticipatedResponseDto;
import com.oxyloans.entity.borrowers.deals.information.LendersPaticipationUpdationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDeals;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDealsRepo;
import com.oxyloans.entity.cms.LenderCmsPayments;
import com.oxyloans.repository.cms.LenderCmsPaymentsRepo;
import com.oxyloans.entity.excelsheets.OxyTransactionDetailsFromExcelSheets;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWallet;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletNativeRepo;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletRepo;
import com.oxyloans.entity.lender.returns.LendersReturns;
import com.oxyloans.entity.lender.returns.LendersReturnsRepo;
import com.oxyloans.entity.user.PersonalDetails;
import com.oxyloans.entity.user.User;
import com.oxyloans.lender.principal.OxyPrincipalReturnRepo;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsFromDealsRequestDto;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsFromDealsResponseDto;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsRequest;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsResponse;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsSearchRequest;
import com.oxyloans.lender.withdrawalfunds.dto.ListOfLendersWithdrawalFundsInfo;
import com.oxyloans.lender.withdrawfunds.entity.LenderWithdrawalFunds;
import com.oxyloans.lender.withdrawfunds.repo.LenderWithdrawalFundsNativeRepo;
import com.oxyloans.lender.withdrawfunds.repo.LenderWithdrawalFundsRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.service.loan.ActionNotAllowedException;
import com.oxyloans.service.loan.LenderWalletHistoryServiceRepo;
import com.oxyloans.whatappservice.WhatappService;
import com.oxyloans.whatappservice.WhatappServiceRepo;

@Service
public class LenderWithdrawalFundsServiceImpl implements LenderWithdrawalFundsService {

	private static final Logger logger = LogManager.getLogger(LenderWithdrawalFundsServiceImpl.class);

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	protected final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Value("${mailsubject}")
	private String mailsubject;

	@Autowired
	private LenderWithdrawalFundsRepo lenderWithdrawalFundsRepo;

	@Autowired
	private LenderWithdrawalFundsNativeRepo lenderWithdrawalFundsNativeRepo;

	@Autowired
	private LenderOxyWalletRepo lenderOxyWalletRepo;

	@Autowired
	private LenderOxyWalletNativeRepo lenderOxyWalletNativeRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private IEmailService emailService;

	@Autowired
	private WhatappServiceRepo whatappServiceRepo;

	@Value("${wtappApi}")
	private String wtappApi;

	@Autowired
	private OxyLendersAcceptedDealsRepo oxyLendersAcceptedDealsRepo;

	@Value("${iciciFilePathBeforeApproval}")
	private String iciciFilePathBeforeApproval;

	@Autowired
	private LendersReturnsRepo lendersReturnsRepo;

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Autowired
	private LendersPaticipationUpdationRepo lendersPaticipationUpdationRepo;

	@Autowired
	private LenderCmsPaymentsRepo lenderCmsPaymentsRepo;

	@Autowired
	private WhatappService whatappService;

	@Autowired
	private OxyPrincipalReturnRepo oxyPrincipalReturnRepo;

	@Autowired
	private LenderWalletHistoryServiceRepo lenderWalletHistoryServiceRepo;

	@Override
	public LenderWithdrawalFundsResponse saveWithDwalFundsInfo(LenderWithdrawalFundsRequest request) {
		logger.info(" !!!!!!!! LenderWithdrawalFundsServiceImpl saveWithDwalFundsInfo method start!!!!!!!!!");

		if (request.getUserType() == null || request.getUserType().isEmpty()) {
			throw new ActionNotAllowedException("No UserType info. present in the request.",
					ErrorCodes.INVALID_OPERATION);
		}
		if (!request.getUserType().equalsIgnoreCase("LENDER")) {
			throw new OperationNotAllowedException(
					"As a " + request.getUserType() + " primary type not allowed to perform this",
					ErrorCodes.INVALID_OPERATION);
		}

		LenderWithdrawalFundsResponse response = new LenderWithdrawalFundsResponse();
		if (request.getUserId() == null || request.getUserId() == 0) {
			throw new ActionNotAllowedException("No user id present in the request.", ErrorCodes.INVALID_OPERATION);
		}
		if (request.getWithdrawalReason() == null || request.getWithdrawalReason().isEmpty()) {
			throw new ActionNotAllowedException("No Withdrwal reason info present in the request.",
					ErrorCodes.INVALID_OPERATION);
		}
		if (request.getRating() == null || request.getRating().isEmpty()) {
			throw new ActionNotAllowedException("No Rating info present in the request.", ErrorCodes.INVALID_OPERATION);
		}
		if (request.getFeedBack() == null || request.getFeedBack().isEmpty()) {
			throw new ActionNotAllowedException("No FeedbBack info present in the request.",
					ErrorCodes.INVALID_OPERATION);
		}
		if (request.getAmount() == null || request.getAmount() == 0) {
			throw new ActionNotAllowedException("No Amount info present in the request.", ErrorCodes.INVALID_OPERATION);
		}
		if (request.getStatus() == null || request.getStatus().isEmpty()) {
			throw new ActionNotAllowedException("No status info present in the request.", ErrorCodes.INVALID_OPERATION);
		}

		User user = userRepo.findByIdNum(request.getUserId());
		PersonalDetails details = user.getPersonalDetails();

		String lenderName = (details.getFirstName() + " " + details.getLastName()) != null
				? details.getFirstName() + " " + details.getLastName()
				: "";

		Double lenderWalletAmount = userRepo.userWalletAmount(request.getUserId());
		if (lenderWalletAmount != null) {
			if (lenderWalletAmount < request.getAmount()) {
				throw new ActionNotAllowedException(
						"As No Suffient Balance in the wallet. you can't raise withrawal request",
						ErrorCodes.ALREDY_IN_PROGRESS);
			}
		}
		Double totalWithdrawlAmount = lenderWithdrawalFundsNativeRepo.getTotalWithdrawlAmount(request.getUserId());

		if (totalWithdrawlAmount != null && totalWithdrawlAmount != 0) {

			Double balance = lenderWalletAmount - totalWithdrawlAmount;

			if (balance < request.getAmount()) {
				throw new ActionNotAllowedException("You can not raise withdrawl request of more than " + balance
						+ ". As already your withdrawl request of total " + totalWithdrawlAmount.intValue()
						+ " is in process", ErrorCodes.ALREDY_IN_PROGRESS);
			}
		}

		LenderWithdrawalFunds saveFunds = lenderWithdrawalFundsNativeRepo.findbyUserId(request.getUserId());
		if (saveFunds == null) {
			saveFunds = new LenderWithdrawalFunds();

			saveFunds.setUserId(request.getUserId());
			saveFunds.setAmount(request.getAmount());

		} else {
			if (request.getType().equalsIgnoreCase("add")) {
				Double oldAmount = saveFunds.getAmount();
				saveFunds.setAmount(oldAmount + request.getAmount());

			} else {
				saveFunds.setAmount(request.getAmount());
			}
		}

		saveFunds.setAmountExpectedDate(request.getAmountRequiredDate());
		saveFunds.setWithdrawalReason(request.getWithdrawalReason());
		saveFunds.setRating(request.getRating());
		saveFunds.setFeedBack(request.getFeedBack());
		saveFunds.setAdminComments(request.getAdminComments());
		saveFunds.setStatus("REQUESTED");
		saveFunds.setCreatedOn(new Date());
		try {
			LenderWithdrawalFunds respFunds = lenderWithdrawalFundsRepo.save(saveFunds);
			if (respFunds == null) {
				response.setMessage("Failed");
			} else {
				response.setMessage("Success");
				response.setId(respFunds.getId());
				response.setUserId(respFunds.getUserId());
				response.setAmount(respFunds.getAmount());
				response.setAmountExpectedDate(respFunds.getAmountExpectedDate());
				response.setWithdrawalReason(respFunds.getWithdrawalReason());
				response.setRating(respFunds.getRating());
				response.setFeedBack(respFunds.getFeedBack());
				response.setAdminComments(respFunds.getAdminComments());
				response.setStatus(respFunds.getStatus());
				response.setCreatedOn(
						DateUtil.convertyyyymmddhhmmsstoddmmyyyy(dateFormat.format(respFunds.getCreatedOn())));

				Calendar current = Calendar.getInstance();
				TemplateContext templateContext = new TemplateContext();
				String expectedCurrentDate = expectedDateFormat.format(current.getTime());
				String amount = String.valueOf(respFunds.getAmount().intValue());

				templateContext.put("currentDate", expectedCurrentDate);
				templateContext.put("lenderName", lenderName);
				templateContext.put("amount", amount);
				templateContext.put("lenderId", "LR" + user.getId());

				// mail for lender
				String mailSubject = "ATTENTION! WITHDRAWAL REQUEST RAISED";
				EmailRequest emailRequest = new EmailRequest(new String[] { user.getEmail() }, mailSubject,
						"lenderWithdrawlRequest.template", templateContext);

				EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
				if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
					try {
						throw new MessagingException(emailResponseFromService.getErrorMessage());
					} catch (MessagingException e2) {

						e2.printStackTrace();
					}
				}

				String whatsappNumber = "";
				if (details.getWhatsAppNumber() != null) {
					whatsappNumber = details.getWhatsAppNumber() != null ? details.getWhatsAppNumber() : "";
				} else {
					whatsappNumber = 91 + user.getMobileNumber();
				}

				String lineSeparator = System.lineSeparator();

				String message = "*Dear " + lenderName + ",*" + lineSeparator
						+ "We have received a withdrawal request from you on " + expectedCurrentDate
						+ " for an amount of INR " + amount
						+ ".We are processing your request and credit the funds in 7 working days." + lineSeparator
						+ "Thanks," + lineSeparator + "OxyLoansTeam";

				whatappService.sendingIndividualMessageUsingUltraApi(whatsappNumber, message);

				Map<String, String> variblesMap = new HashMap<>();
				variblesMap.put("VAR1", lenderName);
				variblesMap.put("VAR2", expectedCurrentDate);
				variblesMap.put("VAR3", amount);

				int count = whatappService.sendingSms(variblesMap, "WITHDRAWALREQWAL", user.getMobileNumber(),
						"OXYLNS");

				EmailRequest emailRequest1 = new EmailRequest(
						new String[] { "narendra@oxyloans.com", "subbu@oxyloans.com", "radhakrishna.t@oxyloans.com" },
						mailSubject, "withdrawlRequestScheduler.template", templateContext);

				EmailResponse emailResponseFromService1 = emailService.sendEmail(emailRequest1);
				if (emailResponseFromService1.getStatus() == EmailResponse.Status.FAILED) {
					try {
						throw new MessagingException(emailResponseFromService.getErrorMessage());
					} catch (MessagingException e2) {

						e2.printStackTrace();
					}
				}

				String messageForAdmin = "HI Team," + lineSeparator + "*Mr/Mrs " + lenderName + "(LR" + user.getId()
						+ ")*" + " raised a  withdrawal request on " + expectedCurrentDate + " for an amount of INR "
						+ amount + ".Please initiate the nextsteps and credit the funds in 7 working days."
						+ lineSeparator + "Thanks," + lineSeparator + "OxyLoansTeam";

				whatappService.sendingWhatsappMessageWithNewInstance("917702795895-1630418928@g.us", messageForAdmin);

			}
		} catch (Exception e) {
			response.setErrorMessageDescription(e.getMessage());
			logger.info(" !!!!!!!! Exception Occured while saving the data !!!!!!!!!", e);
		}
		logger.info(" !!!!!!!! LenderWithdrawalFundsServiceImpl saveWithDwalFundsInfo method end!!!!!!!!!");
		return response;
	}

	@Override
	public SearchResultsDto<LenderWithdrawalFundsResponse> withdrawalFundsSearch(
			LenderWithdrawalFundsSearchRequest request) {
		logger.info(" !!!!!!!! LenderWithdrawalFundsServiceImpl withdrawalFundsSearch method start!!!!!!!!!");

		List<Object[]> objList = lenderWithdrawalFundsNativeRepo.withdrawalFunds(request.getFirstName(),
				request.getLastName(), request.getUserId(), request.getPage().getPageSize(),
				request.getPage().getPageNo());
		Integer totalCount1 = 0;
		totalCount1 = lenderWithdrawalFundsNativeRepo.countTranRecords(request.getFirstName(), request.getLastName(),
				request.getUserId(), request.getPage().getPageSize(), request.getPage().getPageNo());
		SearchResultsDto<LenderWithdrawalFundsResponse> results = new SearchResultsDto<LenderWithdrawalFundsResponse>();
		List<LenderWithdrawalFundsResponse> applicationResponses = new ArrayList<LenderWithdrawalFundsResponse>();
		List<LenderWithdrawalFundsResponse> applicationResponses1 = new ArrayList<LenderWithdrawalFundsResponse>();

		try {
			if (objList != null && !objList.isEmpty()) {
				results.setTotalCount(totalCount1);
				results.setPageNo(request.getPage().getPageNo());
				results.setPageCount(objList.size());
				objList.forEach(e -> {
					try {
						LenderWithdrawalFundsResponse dto = new LenderWithdrawalFundsResponse();
						dto.setId(e[0] == null ? 0 : Integer.parseInt(e[0].toString()));

						dto.setUserId(e[1] == null ? 0 : Integer.parseInt(e[1].toString()));
						dto.setAmount(Double.parseDouble(e[2] == null ? "0" : e[2].toString()));
						dto.setAmountExpectedDate(e[3] == null ? " " : e[3].toString());
						dto.setWithdrawalReason(e[4] == null ? "" : e[4].toString());
						dto.setRating(e[5] == null ? "" : e[5].toString());
						dto.setFeedBack(e[6] == null ? "false" : e[6].toString());
						if (e[7].equals("REQUESTED")) {
							dto.setStatus("REQUESTED");
						} else {
							dto.setStatus(e[7] == null ? "" : e[7].toString());
						}

						dto.setAdminComments(e[8] == null ? "" : e[8].toString());
						SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
						DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");
						dto.setCreatedOn(expectedDateFormat.format(dateFormat1.parse(e[9].toString())));
						dto.setApprovedOn(e[10] == null ? "Yet To Be Approved"
								: expectedDateFormat.format(dateFormat1.parse(e[10].toString())));


						dto.setFirstName(e[11] == null ? "" : e[11].toString());
						dto.setLastName(e[12] == null ? "" : e[12].toString());
						dto.setPanNumber(e[13] == null ? "" : e[13].toString());
						dto.setAddress(e[14] == null ? "" : e[14].toString());
						dto.setEmail(e[15] == null ? "" : e[15].toString());
						dto.setMobileNumber(e[16] == null ? "" : e[16].toString());
						dto.setLenderCity(e[17] == null ? "" : e[17].toString());

						dto.setRequestFrom("WALLET");
						applicationResponses1.add(dto);
					} catch (Exception e1) {
						logger.info("Exception occured in withdrawalFundsSearch", e1);
						throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
					}
				});
			}

			List<LenderWithdrawalFundsResponse> listOfWithdrawsFromDeal = listOfWithdrawsFromDeals(request.getUserId());

			applicationResponses.addAll(applicationResponses1);
			applicationResponses.addAll(listOfWithdrawsFromDeal);

			if (applicationResponses != null && !applicationResponses.isEmpty()) {
				applicationResponses = applicationResponses.stream()
						.sorted((c1, c2) -> c1.getCreatedOn().compareTo(c2.getCreatedOn()))
						.collect(Collectors.toList());

			}

		} catch (Exception e) {
			logger.info("Exception occured in withdrawalFundsSearch", e);
			throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
		}

		results.setResults(applicationResponses);
		logger.info(" !!!!!!!! LenderWithdrawalFundsServiceImpl withdrawalFundsSearch method end!!!!!!!!!");
		return results;
	}

	@Override
	@Transactional
	public LenderWithdrawalFundsResponse updateWithdrawalFundsStatus(LenderWithdrawalFundsRequest request) {
		logger.info(" !!!!!!!! LenderWithdrawalFundsServiceImpl updateWithdrawalFundsStatus method start!!!!!!!!!");

		if (request.getId() == null || request.getId() == 0) {
			throw new ActionNotAllowedException("No Id present in the request.", ErrorCodes.INVALID_OPERATION);
		}
		if (request.getStatus() == null || request.getStatus().isEmpty()) {
			throw new ActionNotAllowedException("No status info present in the request.", ErrorCodes.INVALID_OPERATION);
		}

		LenderWithdrawalFundsResponse response = new LenderWithdrawalFundsResponse();
		LenderWithdrawalFunds saveFunds = lenderWithdrawalFundsRepo.findById(request.getId()).get();

		if (saveFunds != null) {
			User users = userRepo.findById(saveFunds.getUserId()).get();

			if (users != null && users.isTestUser() && !request.getStatus().equalsIgnoreCase("REJECTED")) {
				throw new ActionNotAllowedException("this is a test user", ErrorCodes.INVALID_OPERATION);

			}

		}
		double walletAmount = lenderWalletAmount(saveFunds.getUserId());

		logger.info("wallet amount :" + walletAmount);
		logger.info("saveFunds.getAmount( :" + saveFunds.getAmount());

		saveFunds.setAdminComments(request.getAdminComments());
		if (request.getStatus().equalsIgnoreCase("APPROVED")) {
			saveFunds.setStatus("INITIATED");
		} else {
			saveFunds.setStatus("ADMIN REJECTED");

		}
		saveFunds.setApprovedOn(new Date());
		try {
			LenderWithdrawalFunds respFunds = lenderWithdrawalFundsRepo.save(saveFunds);
			if (respFunds == null) {
				response.setMessage("Failed");
			} else {
				LenderOxyWallet oxyWalletresponse = null;
				if (request.getStatus().equalsIgnoreCase("APPROVED")) {
					if (saveFunds.getAmount() > walletAmount) {

						throw new ActionNotAllowedException(
								"your withdraw requested amount is greaterthan the current wallet amount",
								ErrorCodes.LIMIT_REACHED);
					}
					LenderOxyWallet oxyWallet = lenderOxyWalletRepo.findByWithdrawFundsId(request.getId());
					if (oxyWallet == null) {
						oxyWallet = new LenderOxyWallet();
					}
					oxyWallet.setUserId(respFunds.getUserId());
					oxyWallet.setScrowAccountNumber("Withdrawal Funds");
					oxyWallet.setTransactionAmount(respFunds.getAmount().intValue());
					oxyWallet.setStatus(LenderOxyWallet.Status.APPROVED);
					oxyWallet.setTransactionType("debit");
					oxyWallet.setTransactionDate(new Date());
					oxyWallet.setComments("Debit using withdrawal funds request by Lender");
					oxyWallet.setCreatedBy("Admin");
					oxyWallet.setWithdrawFundsId(request.getId());
					oxyWalletresponse = lenderOxyWalletRepo.save(oxyWallet);
					lenderWalletHistoryServiceRepo.lenderTransactionHistory(respFunds.getUserId(),
							respFunds.getAmount(), "DEBIT", "WW", 0);

					logger.info("saving notifications start");

					sendingWhatsappMessage(respFunds.getUserId(), request.getStatus(), respFunds.getAmount(),
							request.getAdminComments());

					logger.info("saving notifications ends");

					String excelResponse = movingWalletToH2H(respFunds.getUserId(), request.getId());
					List<LenderWithdrawalFunds> lenderWalletToWalletRequest = lenderWithdrawalFundsNativeRepo
							.getListOfWalletToWalletRequestRaised(respFunds.getUserId());
					if (lenderWalletToWalletRequest != null && !lenderWalletToWalletRequest.isEmpty()) {
						double temporaryAmount = 0.0;
						Double walletAmountUserId = userRepo.userWalletAmount(respFunds.getUserId());
						temporaryAmount = walletAmountUserId != null ? walletAmountUserId : 0.0;

						for (LenderWithdrawalFunds withdrawRequest : lenderWalletToWalletRequest) {
							if (temporaryAmount >= withdrawRequest.getAmount()) {
								temporaryAmount = (temporaryAmount - withdrawRequest.getAmount());
							} else {
								withdrawRequest.setStatus("AUTO REJECTED");
								withdrawRequest.setApprovedOn(new Date());
								lenderWithdrawalFundsNativeRepo.save(withdrawRequest);
							}

						}
					}

					logger.info("fileResponse :" + excelResponse);
				} else {
					sendingWhatsappMessage(respFunds.getUserId(), request.getStatus(), respFunds.getAmount(),
							request.getAdminComments());

				}
				if (oxyWalletresponse != null) {

					User user = userRepo.findById(respFunds.getUserId()).get();

					TemplateContext templateContext = new TemplateContext();
					Calendar cal = Calendar.getInstance();
					Date date = cal.getTime();
					String dt = expectedDateFormat.format(date);
					templateContext.put("date", dt);
					EmailRequest emailRequest = new EmailRequest(new String[] { user.getEmail() }, mailsubject,
							"lenderwithdrawalfunds.template", templateContext);
					EmailResponse emailResponse = emailService.sendEmail(emailRequest);
					if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
						throw new MessagingException(emailResponse.getErrorMessage());
					}

					response.setMessage("Success");
					response.setId(respFunds.getId());
					response.setUserId(respFunds.getUserId());
					response.setAmount(respFunds.getAmount());
					response.setAmountExpectedDate(respFunds.getAmountExpectedDate());
					response.setWithdrawalReason(respFunds.getWithdrawalReason());
					response.setRating(respFunds.getRating());
					response.setFeedBack(respFunds.getFeedBack());
					response.setAdminComments(respFunds.getAdminComments());
					response.setStatus(respFunds.getStatus());
					response.setCreatedOn(
							DateUtil.convertyyyymmddhhmmsstoddmmyyyy(dateFormat.format(respFunds.getCreatedOn())));

					response.setApprovedOn(expectedDateFormat.format(respFunds.getApprovedOn()));

				} else {
					response.setMessage("Failed");
				}

			}
		} catch (Exception e) {
			response.setErrorMessageDescription(e.getMessage());
			logger.info(" !!!!!!!! Exception Occured while saving the data !!!!!!!!!", e);
		}
		logger.info(" !!!!!!!! LenderWithdrawalFundsServiceImpl updateWithdrawalFundsStatus method end!!!!!!!!!");
		return response;
	}

	public String sendingWhatsappMessage(int userId, String status, double amount, String adminComments) {
		User user = userRepo.findById(userId).get();
		PersonalDetails details = user.getPersonalDetails();
		String lenderName = "";
		if (details != null) {
			lenderName = (details.getFirstName() + details.getLastName() == null) ? ""
					: details.getFirstName() + " " + details.getLastName();
		}
		String whatsappNumber = "";
		if (user.getPersonalDetails() != null) {
			whatsappNumber = user.getPersonalDetails().getWhatsAppNumber() != null
					? user.getPersonalDetails().getWhatsAppNumber()
					: "91" + user.getMobileNumber();
		}
		String message = null;
		if (status.equalsIgnoreCase("APPROVED")) {

			message = "Dear " + lenderName + "(LR" + userId + ")"
					+ " Congratulations !! Your withdrawal request for an amount of INR "
					+ BigDecimal.valueOf(amount).toBigInteger()
					+ " has been Initiated.Please check your registered bank account balance. Kindly login to https://oxyloans.com/ to view the same in withdrawal history.";

		} else {

			message = "Dear " + lenderName + "(LR" + userId + ")" + " Your withdrawal request for an amount of INR "
					+ BigDecimal.valueOf(amount).toBigInteger() + " has been Rejected because of " + adminComments;
		}
		whatappServiceRepo.sendingIndividualMessageUsingUltraApi(whatsappNumber, message);
		whatappServiceRepo.sendingWhatsappMessageWithNewInstance("917702795895-1630418928@g.us", message);
		return status;
	}

	@Override
	public List<LenderWithdrawalFunds> withdrawalRequestsScheduler() {

		logger.info("LenderWithdrawalFundsServiceImpl withdrawalRequestsScheduler method starts");
		List<LenderWithdrawalFunds> fundsList = lenderWithdrawalFundsNativeRepo.findIntiatedTransactions();

		if (fundsList != null && !fundsList.isEmpty()) {
			for (LenderWithdrawalFunds funds : fundsList) {
				User user = userRepo.findByIdNum(funds.getUserId());
				if (user != null) {
					// String lenderId=String.val
					PersonalDetails details = user.getPersonalDetails();
					if (details != null) {
						String lenderName = (details.getFirstName() + " " + details.getLastName()) != null
								? details.getFirstName() + " " + details.getLastName()
								: "";

						Calendar current = Calendar.getInstance();
						TemplateContext templateContext = new TemplateContext();
						String expectedCurrentDate = expectedDateFormat.format(current.getTime());
						String amount = String.valueOf(funds.getAmount().intValue());

						templateContext.put("currentDate", expectedCurrentDate);
						templateContext.put("lenderName", lenderName);
						templateContext.put("amount", amount);
						templateContext.put("lenderId", "LR" + user.getId());

						String mailSubject = "ATTENTION! WITHDRAWAL REQUEST RAISED";
						EmailRequest emailRequest = new EmailRequest(
								new String[] { "narendra@oxyloans.com", "subbu@oxyloans.com",
										"radhakrishna.t@oxyloans.com" },
								mailSubject, "withdrawlRequestScheduler.template", templateContext);

						EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
						if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
							try {
								throw new MessagingException(emailResponseFromService.getErrorMessage());
							} catch (MessagingException e2) {

								e2.printStackTrace();
							}
						}
						String lineSeparator = System.lineSeparator();

						String message = "HI Team," + lineSeparator + "*Mr/Mrs " + lenderName + "(LR" + user.getId()
								+ ")*" + " raised a  withdrawal request on " + expectedCurrentDate
								+ " for an amount of INR " + amount
								+ ".Please initiate the nextsteps and credit the funds soon." + lineSeparator
								+ "Thanks," + lineSeparator + "OxyLoansTeam";

						whatappService.whatsappMessageToGroupUsingUltra("917702795895-1630418928@g.us", message);

					}
				}

			}

		}
		logger.info("LenderWithdrawalFundsServiceImpl withdrawalRequestsScheduler method ends");
		return fundsList;

	}

	@Override
	public LenderWithdrawalFundsResponse lenderWithdrawFundsInfo(int userId) {
		LenderWithdrawalFundsResponse response = new LenderWithdrawalFundsResponse();

		LenderWithdrawalFunds lenderWithdrawFunds = lenderWithdrawalFundsNativeRepo.findbyUserId(userId);
		if (lenderWithdrawFunds != null) {
			response.setId(lenderWithdrawFunds.getId());
			response.setUserId(lenderWithdrawFunds.getUserId());
			response.setAmount(lenderWithdrawFunds.getAmount());
			response.setStatus(lenderWithdrawFunds.getStatus());
		}

		return response;
	}

	@Override
	public LenderPaticipatedDeal getLenderPaticipatedDealsBasedOnDealType(int userId,
			PaginationRequestDto pageRequestDto) {
		int pageNo = pageRequestDto.getPageNo();
		int pageSize = pageRequestDto.getPageSize();

		pageNo = (pageSize * (pageNo - 1));
		LenderPaticipatedDeal lenderPaticipatedDeal = new LenderPaticipatedDeal();
		Integer count = 0;

		Integer value = oxyLendersAcceptedDealsRepo.getLenderPaticipatedDealsBasedOnTypeCount(userId,
				pageRequestDto.getDealType());
		if (value != null) {
			count = value;
		}
		List<LenderPaticipatedResponseDto> listOfdeasToLender = new ArrayList<LenderPaticipatedResponseDto>();
		// try {
		List<Object[]> listOfdeals = oxyLendersAcceptedDealsRepo.getLenderPaticipatedDealsBasedOnTypes(userId,
				pageRequestDto.getDealType(), pageNo, pageSize);
		if (listOfdeals != null && !listOfdeals.isEmpty()) {
			Iterator it = listOfdeals.iterator();
			while (it.hasNext()) {
				Object[] e = (Object[]) it.next();
				LenderPaticipatedResponseDto lenderPaticipatedResponseDto = new LenderPaticipatedResponseDto();
				Integer dealId = Integer.parseInt(e[3] == null ? "0" : e[3].toString());
				OxyBorrowersDealsInformation oxyBorrowersDeals = oxyBorrowersDealsInformationRepo
						.findDealAlreadyGiven(dealId);
				if (oxyBorrowersDeals != null) {
					lenderPaticipatedResponseDto.setDealName(oxyBorrowersDeals.getDealName());
					lenderPaticipatedResponseDto.setDealBorrowerName(oxyBorrowersDeals.getBorrowerName());
					lenderPaticipatedResponseDto.setDealAmount(oxyBorrowersDeals.getDealAmount());
					lenderPaticipatedResponseDto.setDealRateofinterest(oxyBorrowersDeals.getBorrowerRateofinterest());
					lenderPaticipatedResponseDto.setDealDuration(oxyBorrowersDeals.getDuration());
					lenderPaticipatedResponseDto
							.setParticipationStatus(oxyBorrowersDeals.getDealParticipationStatus().toString());
					lenderPaticipatedResponseDto
							.setFirstInterestDate(expectedDateFormat.format(oxyBorrowersDeals.getLoanActiveDate()));
					lenderPaticipatedResponseDto.setDealType(oxyBorrowersDeals.getDealType().toString());
				}
				lenderPaticipatedResponseDto.setDealId(dealId);
				User user = userRepo.findById(userId).get();
				if (user != null) {
					lenderPaticipatedResponseDto.setGroupId(user.getLenderGroupId());
				}
				Double dealAmount = Double.parseDouble(e[0] == null ? "0" : e[0].toString());
				lenderPaticipatedResponseDto.setPaticipatedAmount(dealAmount);

				Double currentValue = Double.parseDouble(e[2] == null ? "0" : e[2].toString());

				String currentStatus = null;

				if (currentValue == 0) {
					currentStatus = "COMPLETED";
					lenderPaticipatedResponseDto.setLoanStatementValueCheck(null);
				} else {
					currentStatus = "RUNNING";
					lenderPaticipatedResponseDto.setLoanStatementValueCheck("notNull");
				}
				lenderPaticipatedResponseDto.setPricipalReturned("Returned");

				Double requestAmountForWithDraw = lendersReturnsRepo.getLenderWithdrawRequestedSumValue(userId, dealId);
				if (requestAmountForWithDraw != null) {
					lenderPaticipatedResponseDto.setRequestedAmount(requestAmountForWithDraw);
					lenderPaticipatedResponseDto.setStatusToDisplayHistory("Display");
				} else {
					lenderPaticipatedResponseDto.setRequestedAmount(0.0);
					lenderPaticipatedResponseDto.setStatusToDisplayHistory("Hide");
				}

				lenderPaticipatedResponseDto.setCurrentValue(BigDecimal.valueOf(currentValue).toBigInteger());
				lenderPaticipatedResponseDto.setCurrentStatus(currentStatus);

				OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
						.toCheckUserAlreadyInvoledInDeal(userId, dealId);
				if (oxyLendersAcceptedDeals != null) {
					lenderPaticipatedResponseDto
							.setLederReturnType(oxyLendersAcceptedDeals.getLenderReturnsType().toString());

				}

				OxyBorrowersDealsInformation oxyBorrowers = oxyBorrowersDealsInformationRepo
						.toGetCalculationType(dealId);

				DecimalFormat df = new DecimalFormat("#.##");
				if (oxyBorrowers != null) {
					lenderPaticipatedResponseDto.setRateOfInterest(oxyLendersAcceptedDeals.getRateofinterest());
				} else {
					lenderPaticipatedResponseDto.setRateOfInterest(
							Double.parseDouble(df.format(oxyLendersAcceptedDeals.getRateofinterest() / 12)));
				}

				lenderPaticipatedResponseDto.setLenderId(userId);
				listOfdeasToLender.add(lenderPaticipatedResponseDto);
			}

		}
		lenderPaticipatedDeal.setCount(count);
		lenderPaticipatedDeal.setLenderPaticipatedResponseDto(listOfdeasToLender);

		// } catch (Exception e) {
		logger.info("Exception in getLenderPaticipatedDealsInformation method ");
		// }
		return lenderPaticipatedDeal;

	}

	@Override
	@Transactional
	public LenderWithdrawalFundsFromDealsResponseDto withdrawalFundsFromDeals(
			LenderWithdrawalFundsFromDealsRequestDto lenderWithdrawalFundsFromDealsRequestDto) {
		LenderWithdrawalFundsFromDealsResponseDto lenderWithdrawalFundsFromDealsResponseDto = new LenderWithdrawalFundsFromDealsResponseDto();

		if (lenderWithdrawalFundsFromDealsRequestDto.getWithDrawalFunds() <= lenderWithdrawalFundsFromDealsRequestDto
				.getCurrentAmount()
				&& (lenderWithdrawalFundsFromDealsRequestDto.getWithDrawalFunds()
						+ lenderWithdrawalFundsFromDealsRequestDto
								.getRequestedAmount()) <= lenderWithdrawalFundsFromDealsRequestDto.getCurrentAmount()) {

			LendersReturns lendersReturns = new LendersReturns();
			lendersReturns.setUserId(lenderWithdrawalFundsFromDealsRequestDto.getUserId());
			lendersReturns.setAmount(lenderWithdrawalFundsFromDealsRequestDto.getWithDrawalFunds());
			OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
					.findById(lenderWithdrawalFundsFromDealsRequestDto.getDealId()).get();
			if (oxyBorrowersDealsInformation != null) {
				lendersReturns.setBorrowerName(oxyBorrowersDealsInformation.getDealName());
				lendersReturns.setRemarks(oxyBorrowersDealsInformation.getDealName());
			}
			lendersReturns.setAmountType(OxyTransactionDetailsFromExcelSheets.DebitedTowords.LENDERWITHDRAW.toString());
			lendersReturns.setDealId(lenderWithdrawalFundsFromDealsRequestDto.getDealId());
			lendersReturns.setRequestedOn(new Date());
			lendersReturns.setCreatedOn(new Date());
			lendersReturns.setStatus(lendersReturns.getStatus().REQUESTED);
			lendersReturnsRepo.save(lendersReturns);

			User user = userRepo.findById(lenderWithdrawalFundsFromDealsRequestDto.getUserId()).get();
			String name = null;
			String mobileNumber = null;
			if (user != null) {
				if (user.getPersonalDetails() != null) {
					name = user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName();
					if (user.getPersonalDetails().getWhatsAppNumber() != null) {
						mobileNumber = user.getPersonalDetails().getWhatsAppNumber();
					} else {
						mobileNumber = 91 + user.getMobileNumber();
					}

				}
			}
			Calendar current = Calendar.getInstance();
			String lineSeparator = System.lineSeparator();

			String expectedDate = expectedDateFormat.format(current.getTime());
			String message = "*Dear " + name + ",*" + lineSeparator + "We have received a withdrawal request from "
					+ oxyBorrowersDealsInformation.getDealName() + " you on " + expectedDate + " for an amount of INR "
					+ BigDecimal.valueOf(lenderWithdrawalFundsFromDealsRequestDto.getWithDrawalFunds()).toBigInteger()
					+ ".We are processing your request and credit the funds in 30 working days." + lineSeparator
					+ "Thanks," + lineSeparator + "OxyLoansTeam";

			whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, message);

			String messageForAdmin = "HI Team," + lineSeparator + "*Mr/Mrs " + name + "(LR" + user.getId() + ")*"
					+ " raised a  withdrawal request from " + oxyBorrowersDealsInformation.getDealName() + " on "
					+ expectedDate + " for an amount of INR "
					+ BigDecimal.valueOf(lenderWithdrawalFundsFromDealsRequestDto.getWithDrawalFunds()).toBigInteger()
					+ ".Please initiate the nextsteps and credit the funds in 30 working days." + lineSeparator
					+ "Thanks," + lineSeparator + "OxyLoansTeam";

			whatappService.sendingWhatsappMessageWithNewInstance("917702795895-1630418928@g.us", messageForAdmin);

			Map<String, String> variblesMap = new HashMap<>();
			variblesMap.put("VAR1", name);
			variblesMap.put("VAR2", oxyBorrowersDealsInformation.getDealName());
			variblesMap.put("VAR3", expectedDate);
			variblesMap.put("VAR4", BigDecimal.valueOf(lenderWithdrawalFundsFromDealsRequestDto.getWithDrawalFunds())
					.toBigInteger().toString());

			int count = whatappService.sendingSms(variblesMap, "WITHDRAWDEAL", user.getMobileNumber(), "OXYLNS");

		} else {
			throw new ActionNotAllowedException(
					"your are requesting more than the current value please check the amount",
					ErrorCodes.AMOUNT_EXCEEDED);
		}
		lenderWithdrawalFundsFromDealsResponseDto.setDealId(lenderWithdrawalFundsFromDealsRequestDto.getDealId());
		lenderWithdrawalFundsFromDealsResponseDto.setStatus("successfullyUpdated");
		return lenderWithdrawalFundsFromDealsResponseDto;

	}

	@Override
	public ListOfLendersWithdrawalFundsInfo listOfLendersWithdrawalInfo(PaginationRequestDto pageRequestDto) {
		int pageNo = pageRequestDto.getPageNo();
		int pageSize = pageRequestDto.getPageSize();

		pageNo = (pageSize * (pageNo - 1));

		ListOfLendersWithdrawalFundsInfo listOfLendersWithdrawalFundsInfo = new ListOfLendersWithdrawalFundsInfo();
		List<LenderWithdrawalFundsFromDealsResponseDto> lendersWithDrawal = new ArrayList<LenderWithdrawalFundsFromDealsResponseDto>();
		//List<Object[]> listOfFunds = lendersReturnsRepo.getLendersWithdrawRequests(pageSize, pageNo);
		List<Object[]> listOfFunds=null;
		if(pageRequestDto.getUserType().equalsIgnoreCase("LIVE")) {
			 listOfFunds = lendersReturnsRepo.getLendersWithdrawRequests(pageSize, pageNo);
			}
			else {
				
				 listOfFunds = lendersReturnsRepo.getTestLendersWithdrawRequests(pageSize, pageNo);

			}
		if (listOfFunds != null && !listOfFunds.isEmpty()) {
			Iterator it = listOfFunds.iterator();
			while (it.hasNext()) {
				Object[] e = (Object[]) it.next();
				LenderWithdrawalFundsFromDealsResponseDto lenderWithdrawalFundsFromDealsResponseDto = new LenderWithdrawalFundsFromDealsResponseDto();
				lenderWithdrawalFundsFromDealsResponseDto.setId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
				Integer id = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
				User user = userRepo.findById(id).get();
				if (user != null && user.getBankDetails() != null) {
					lenderWithdrawalFundsFromDealsResponseDto
							.setLenderName(user.getBankDetails().getUserName().trim().replaceAll(" +", " "));
					lenderWithdrawalFundsFromDealsResponseDto
							.setAccountNumber(user.getBankDetails().getAccountNumber());

					lenderWithdrawalFundsFromDealsResponseDto
							.setIfsc(user.getBankDetails().getIfscCode().toUpperCase());
				}
				lenderWithdrawalFundsFromDealsResponseDto.setUserId(id);
				lenderWithdrawalFundsFromDealsResponseDto
						.setAmount(Double.parseDouble(e[2] == null ? "0" : e[2].toString()));
				Integer dealId = Integer.parseInt(e[3] == null ? "0" : e[3].toString());
				OxyBorrowersDealsInformation dealInformation = oxyBorrowersDealsInformationRepo.findById(dealId).get();
				if (dealInformation != null) {
					lenderWithdrawalFundsFromDealsResponseDto.setDealName(dealInformation.getDealName());
				}

				lenderWithdrawalFundsFromDealsResponseDto.setDealId(dealId);
				lenderWithdrawalFundsFromDealsResponseDto.setRequestDate(e[4] == null ? " " : e[4].toString());
				lenderWithdrawalFundsFromDealsResponseDto.setStatus(e[5] == null ? " " : e[5].toString());
				lendersWithDrawal.add(lenderWithdrawalFundsFromDealsResponseDto);
			}

			listOfLendersWithdrawalFundsInfo.setLenderWithdrawalFundsFromDealsResponseDto(lendersWithDrawal);
			Integer countValue = lendersReturnsRepo.getLendersWithdrawRequestsCountValue();
			if (countValue != null) {
				listOfLendersWithdrawalFundsInfo.setTotalCount(countValue);
			}
		}

		return listOfLendersWithdrawalFundsInfo;

	}

	@Override
	public LenderWithdrawalFundsFromDealsResponseDto approvingWithdrawalFund(
			LenderWithdrawalFundsFromDealsRequestDto lenderWithdrawalFundsFromDealsRequestDto) {

		LenderWithdrawalFundsFromDealsResponseDto lenderWithdrawalFundsFromDealsResponseDto = new LenderWithdrawalFundsFromDealsResponseDto();
		LendersReturns lenderReturns = lendersReturnsRepo.findById(lenderWithdrawalFundsFromDealsRequestDto.getId())
				.get();
		if (lenderReturns != null) {
			if (lenderReturns.getStatus().equals(LendersReturns.Status.REQUESTED)) {
				User user = userRepo.findById(lenderReturns.getUserId()).get();
				if (user != null && user.isTestUser()
						&& !lenderWithdrawalFundsFromDealsRequestDto.getStatus().equalsIgnoreCase("REJECTED")) {
					throw new ActionNotAllowedException("this is a test user", ErrorCodes.INVALID_OPERATION);

				}

				if (lenderWithdrawalFundsFromDealsRequestDto.getStatus().equals("APPROVED")) {

					OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
							.toCheckUserAlreadyInvoledInDeal(lenderReturns.getUserId(), lenderReturns.getDealId());
					Double participationUpdated = lendersPaticipationUpdationRepo
							.getSumOfLenderUpdatedAmountToDeal(lenderReturns.getUserId(), lenderReturns.getDealId());

					Double paticipatedAmount = 0.0;
					Double currentValue = 0.0;
					Double participationCurrentValue = 0.0;
					Double currentParticipation = 0.0;
					Double currentAmount = 0.0;
					if (oxyLendersAcceptedDeals != null) {
						if (participationUpdated != null) {
							paticipatedAmount = oxyLendersAcceptedDeals.getParticipatedAmount() + participationUpdated;
						} else {
							paticipatedAmount = oxyLendersAcceptedDeals.getParticipatedAmount();
						}

					}

					Double sumPricipalReturnedAmount = lendersReturnsRepo.getSumOfAmountReturned(
							lenderReturns.getUserId(), "LENDERPRINCIPAL", lenderReturns.getDealId());
					if (sumPricipalReturnedAmount != null) {
						currentValue = paticipatedAmount - sumPricipalReturnedAmount;
					} else {
						currentValue = paticipatedAmount;
					}
					Double sumOfWithDramAmount = lendersReturnsRepo
							.lenderWithdrawStatusBasedSumValue(lenderReturns.getUserId(), lenderReturns.getDealId());

					if (sumOfWithDramAmount != null) {
						currentParticipation = currentValue - sumOfWithDramAmount;
					} else {
						currentParticipation = currentValue;
					}

					Double sumOfWalletAmount = lenderOxyWalletNativeRepo
							.paticipatedAmountMovedToWalletSum(lenderReturns.getUserId(), lenderReturns.getDealId());
					if (sumOfWalletAmount != null) {
						participationCurrentValue = currentParticipation - sumOfWalletAmount;
					} else {
						participationCurrentValue = currentParticipation;
					}

					Double principalAmountInHold = oxyPrincipalReturnRepo
							.sumOfPrincipalAmountReturned(lenderReturns.getUserId(), lenderReturns.getDealId());
					if (principalAmountInHold != null) {
						currentAmount = participationCurrentValue - principalAmountInHold;
					} else {
						currentAmount = participationCurrentValue;
					}

					if (lenderReturns.getAmount() <= currentAmount) {
						lenderReturns.setStatus(LendersReturns.Status
								.valueOf(lenderWithdrawalFundsFromDealsRequestDto.getStatus().toString()));
						try {
							lenderReturns.setPaidDate(
									expectedDateFormat.parse(lenderWithdrawalFundsFromDealsRequestDto.getPaidDate()));
						} catch (ParseException e) {

							e.printStackTrace();
						}
						LendersReturns latestInterestDetails = lendersReturnsRepo.getLenderLatestInterestReturnedDate(
								lenderReturns.getUserId(), lenderReturns.getDealId());
						String interestDate = null;
						int substartionValue = 0;
						int additionValue = 0;
						OxyLendersAcceptedDeals lendersAcceptedDeals = oxyLendersAcceptedDealsRepo
								.toCheckUserAlreadyInvoledInDeal(lenderReturns.getUserId(), lenderReturns.getDealId());
						if (latestInterestDetails != null) {
							if (!latestInterestDetails.getAmountType().equals(
									OxyTransactionDetailsFromExcelSheets.DebitedTowords.LENDERINTEREST.toString())) {
								interestDate = expectedDateFormat.format(latestInterestDetails.getPaidDate());
							} else {
								String paidDate = expectedDateFormat.format(latestInterestDetails.getPaidDate());
								String[] paidDateSplited = paidDate.split("/");
								OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
										.findDealAlreadyGiven(lenderReturns.getDealId());
								int loanActivedate = 0;
								if (oxyBorrowersDealsInformation != null) {
									String loanActiveDate = expectedDateFormat
											.format(oxyBorrowersDealsInformation.getLoanActiveDate());
									String[] loanActiveSpliting = loanActiveDate.split("/");
									loanActivedate = Integer.parseInt(loanActiveSpliting[0]);
								}
								StringBuilder interestGivenDate = new StringBuilder();
								interestGivenDate.append(loanActivedate).append("/")
										.append(paidDateSplited[1].toString()).append("/")
										.append(paidDateSplited[2].toString());
								interestDate = interestGivenDate.toString();
							}
							additionValue = 1;
						} else {
							if (lendersAcceptedDeals != null) {
								interestDate = expectedDateFormat.format(lendersAcceptedDeals.getReceivedOn());
							}
							substartionValue = 1;
						}
						String principalDateInExceptedFormate = lenderWithdrawalFundsFromDealsRequestDto.getPaidDate();

						String startDate = null;
						String endDate = null;
						int days = 0;
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");
						try {
							startDate = sdf3.format(sdf.parse(interestDate));
							endDate = sdf3.format(sdf.parse(principalDateInExceptedFormate));

						} catch (java.text.ParseException e) {

							e.printStackTrace();
						}

						double interestAmount = 0.0;
						double singleDayInterest = 0.0;
						double rateOfInterest = 0.0;
						OxyBorrowersDealsInformation oxyBorrowersDeals = oxyBorrowersDealsInformationRepo
								.toGetCalculationType(lenderReturns.getDealId());

						Double totalAmountReturnedTillNow = lendersReturnsRepo.getSumOfAmountReturnedForInterest(
								lenderReturns.getUserId(), lenderReturns.getDealId());
						if (oxyBorrowersDeals.getRoiForWithdrawal() != 0.0) {
							rateOfInterest = oxyBorrowersDeals.getRoiForWithdrawal();
						} else {

							rateOfInterest = lendersAcceptedDeals.getRateofinterest();

						}

						logger.info("totalAmountReturnedTillNow - " + totalAmountReturnedTillNow);

						startDate = sdf3.format(lendersAcceptedDeals.getReceivedOn());
						days = differenceInDays(startDate, endDate) - substartionValue + additionValue;

						if (totalAmountReturnedTillNow != null && totalAmountReturnedTillNow > 0) {

							Date withDrawlRequestedDate = lenderReturns.getRequestedOn();

							logger.info("withDrawlRequestedDate - " + withDrawlRequestedDate);

							String withDrawlRequestedDateString = sdf3.format(withDrawlRequestedDate.getTime());

							logger.info("withDrawlRequestedDateString - " + withDrawlRequestedDateString);

							logger.info("endDate - " + endDate);

							logger.info("days - " + days);

							if (lendersAcceptedDeals != null) {

								Double participationUpdation = lendersPaticipationUpdationRepo
										.getSumOfLenderUpdatedAmountToDeal(lenderReturns.getUserId(),
												lenderReturns.getDealId());

								logger.info("participationUpdation - " + participationUpdation);

								double participatedAmount = lendersAcceptedDeals.getParticipatedAmount()
										+ (participationUpdation != null ? participationUpdation : 0d);

								logger.info("participatedAmount - " + participatedAmount);

								Double remainingParticipatedAmount = 0d;

								if (lenderReturns.getAmount() < participatedAmount) { // anytime partial withdrawl
									remainingParticipatedAmount = participatedAmount - lenderReturns.getAmount();

									interestAmount = Math.round(remainingParticipatedAmount * rateOfInterest / 100d);
									singleDayInterest = interestAmount / 30;

								} else if (lenderReturns.getAmount() == participatedAmount) { // anytime full
																								// withdrawl

									interestAmount = Math.round(lenderReturns.getAmount() * rateOfInterest / 100d);
									singleDayInterest = interestAmount / 30;

									logger.info("singleDayInterest - " + singleDayInterest);

								}
							}

							Double withDrawlInterestAmount = (double) Math.round(days * singleDayInterest);

							logger.info("remainingInterestAmount - " + withDrawlInterestAmount);
							Double withDrawlAmount = lenderReturns.getAmount();

							logger.info("withDrawlAmount - " + withDrawlAmount);

							if (totalAmountReturnedTillNow > withDrawlInterestAmount) {

								withDrawlInterestAmount = totalAmountReturnedTillNow - withDrawlInterestAmount;

								logger.info("remainingInterestAmount1 - " + withDrawlInterestAmount);

								withDrawlAmount -= withDrawlInterestAmount;

								logger.info("withDrawlAmount - " + withDrawlAmount);

								lenderReturns.setInterestamountForWithdrawalfunds(0);

								lenderReturns.setAmount(withDrawlAmount);
								lenderReturns.setDays_difference(days);
								logger.info("saved");
							} else {
								withDrawlInterestAmount = withDrawlInterestAmount - totalAmountReturnedTillNow;

								logger.info("remainingInterestAmount2 - " + withDrawlInterestAmount);
								lenderReturns.setDays_difference(days);
								lenderReturns.setInterestamountForWithdrawalfunds(withDrawlInterestAmount);
							}

						}

						logger.info("days" + lenderReturns.getDays_difference());
						logger.info("total interest" + lenderReturns.getInterestamountForWithdrawalfunds());

						lendersReturnsRepo.save(lenderReturns);
						ListOfLendersWithdrawalFundsInfo listOfLendersWithdrawalFundsInfo = new ListOfLendersWithdrawalFundsInfo();
						listOfLendersWithdrawalFundsInfo.setLenderWithdrawalFundsFromDealsResponseDto(null);
						String amountType = "PRINCIPAL";
						String fileName = creatingExcelSheetForh2h(listOfLendersWithdrawalFundsInfo,
								lenderReturns.getUserId(), amountType, lenderWithdrawalFundsFromDealsRequestDto.getId(),
								lenderWithdrawalFundsFromDealsRequestDto.getAccountType());
						lenderReturns.setFileNameForPrincipalWithdrawal(fileName);
						lenderReturns.setCreatedOn(new Date());
						lendersReturnsRepo.save(lenderReturns);
					} else {
						throw new ActionNotAllowedException("your had already returned principal amount",
								ErrorCodes.ACTION_ALREDY_DONE);
					}
				} else {
					lenderReturns.setStatus(LendersReturns.Status
							.valueOf(lenderWithdrawalFundsFromDealsRequestDto.getStatus().toString()));
					lenderReturns.setCreatedOn(new Date());
					lendersReturnsRepo.save(lenderReturns);
				}

			} else if (lenderReturns.getStatus().equals(LendersReturns.Status.REJECTED)) {
				throw new ActionNotAllowedException("your had already rejected the request",
						ErrorCodes.ACTION_ALREDY_DONE);
			} else {
				throw new ActionNotAllowedException("your had already approved", ErrorCodes.ALREDY_IN_PROGRESS);
			}

		}
		lenderWithdrawalFundsFromDealsResponseDto.setDealId(lenderReturns.getDealId());
		lenderWithdrawalFundsFromDealsResponseDto.setUserId(lenderReturns.getUserId());
		return lenderWithdrawalFundsFromDealsResponseDto;

	}

	public String creatingExcelSheetForh2h(ListOfLendersWithdrawalFundsInfo listOfLendersWithdrawalFundsInfo,
			Integer userId, String amountType, Integer principalId, String accountType) {
		SimpleDateFormat formatter1 = new SimpleDateFormat("ddMMyy");
		Date date = new Date();
		String currentDate = formatter1.format(date);

		StringBuilder sbf = null;
		String accountNumber = null;
		if (accountType.equalsIgnoreCase("DISBURSMENT")) {
			sbf = new StringBuilder("OXYLR2_OXYLR2UPLD_");
			accountNumber = "777705849441";
		} else {
			sbf = new StringBuilder("OXYLR1_OXYLR1UPLD_");
			accountNumber = "777705849442";
		}
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		LocalDateTime now = LocalDateTime.now();

		if (amountType.equalsIgnoreCase("INTEREST")) {
			sbf.append(currentDate + "_" + userId + "I" + principalId + "_" + now);
		} else {
			sbf.append(currentDate + "_" + userId + "P" + principalId + "_" + now);
		}

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 40; i++) {
			spreadSheet.autoSizeColumn(i);
		}
		String lenderName = null;
		String lenderAccountNumber = null;
		String lenderIFSC = null;
		User user = userRepo.findById(userId).get();
		if (user != null) {
			if (user.getBankDetails() != null) {
				if (user.getBankDetails().getUserName() != null) {
					lenderName = user.getBankDetails().getUserName().trim().replaceAll(" +", " ");
				}
				if (user.getBankDetails().getAccountNumber() != null) {
					lenderAccountNumber = user.getBankDetails().getAccountNumber();
				}
				if (user.getBankDetails().getIfscCode() != null) {
					lenderIFSC = user.getBankDetails().getIfscCode().toUpperCase();
				}
			}
		}
		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		Date today = new Date();
		String currentDateValue = dateFormatter.format(today);

		String currentDateSpliting[] = currentDateValue.split("/");
		if (listOfLendersWithdrawalFundsInfo.getAmountType() != null) {

			LendersReturns withdrawal = lendersReturnsRepo.findById(listOfLendersWithdrawalFundsInfo.getId()).get();
			double interestAmount = 0.0;
			int dealId = 0;
			String dealName = null;
			if (withdrawal != null) {
				interestAmount = withdrawal.getInterestamountForWithdrawalfunds();
				dealId = withdrawal.getDealId();
				OxyBorrowersDealsInformation dealInfo = oxyBorrowersDealsInformationRepo.findById(dealId).get();
				if (dealInfo != null) {
					dealName = dealInfo.getDealName().replaceAll("[^a-zA-Z0-9]", " ");
					dealName = dealName.replaceAll("\\s+", "_");

				}
			}

			Row row1 = spreadSheet.createRow(++rowCount);
			Cell cell1 = row1.createCell(0);
			cell1.setCellValue(accountNumber);

			cell1 = row1.createCell(1);
			cell1.setCellValue(lenderAccountNumber);

			cell1 = row1.createCell(2);
			cell1.setCellValue(lenderName);

			cell1 = row1.createCell(3);
			cell1.setCellValue(interestAmount);

			cell1 = row1.createCell(4);
			cell1.setCellValue("N");

			int y = Integer.parseInt(currentDateSpliting[1]);

			String monthName12 = DateTime.now().withMonthOfYear(y).toString("MMM");

			String requiredDate = currentDateSpliting[0] + "-" + monthName12 + "-" + currentDateSpliting[2];

			cell1 = row1.createCell(5);
			cell1.setCellValue(requiredDate);

			cell1 = row1.createCell(6);
			cell1.setCellValue(lenderIFSC);

			cell1 = row1.createCell(16);
			cell1.setCellValue("WITHDRAWALINTEREST");

			cell1 = row1.createCell(17);
			cell1.setCellValue(dealId);

			cell1 = row1.createCell(18);
			cell1.setCellValue("LR" + userId);

			cell1 = row1.createCell(19);
			cell1.setCellValue("OXYLOANS " + dealName + "M" + currentDateSpliting[0]);

			cell1 = row1.createCell(20);
			cell1.setCellValue(dealName + "Interest");

			File f = new File(iciciFilePathBeforeApproval + sbf.toString() + ".xls");
			try {
				outFile = new FileOutputStream(f);
				workBook.write(outFile);

				workBook.close();
				outFile.close();

			} catch (Exception ex) {
				ex.getMessage();
			}

			LenderCmsPayments lenderCmsPayments = new LenderCmsPayments();
			lenderCmsPayments.setDealId(dealId);
			lenderCmsPayments.setTotalAmount(interestAmount);
			lenderCmsPayments.setFileName(f.getName());
			lenderCmsPayments.setLenderReturnsType("WITHDRAWALINTEREST");
			lenderCmsPayments.setPaymentDate(new Date());
			lenderCmsPaymentsRepo.save(lenderCmsPayments);

			String message = "Initiated " + dealName + " *WITHDRAWALINTEREST* with a file name " + f.getName()
					+ " for an amount of " + interestAmount;

			whatappService.sendingWhatsappMessageWithNewInstance("917702795895-1630418928@g.us", message);

			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate1 = expectedDateFormat.format(current.getTime());
			templateContext.put("currentDate", expectedCurrentDate1);

			String mailSubject = "LR" + userId + " InterestSheet";

			EmailRequest emailRequest = new EmailRequest(
					new String[] { "archana.n@oxyloans.com", "subbu@oxyloans.com" }, "Interests-Approval.template",
					templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			}
		} else {
			LendersReturns withdrawal = lendersReturnsRepo.findById(principalId).get();
			double principalAmount = 0.0;
			String dealName = null;
			Integer dealId = 0;
			if (withdrawal != null) {
				principalAmount = withdrawal.getAmount();
				dealId = withdrawal.getDealId();
				OxyBorrowersDealsInformation dealInfo = oxyBorrowersDealsInformationRepo.findById(dealId).get();
				if (dealInfo != null) {
					dealName = dealInfo.getDealName().replaceAll("[^a-zA-Z0-9]", " ");
					dealName = dealName.replaceAll("\\s+", "_");
				}
			}
			Row row1 = spreadSheet.createRow(++rowCount);
			Cell cell1 = row1.createCell(0);
			cell1.setCellValue(accountNumber);

			cell1 = row1.createCell(1);
			cell1.setCellValue(lenderAccountNumber);

			cell1 = row1.createCell(2);
			cell1.setCellValue(lenderName);

			cell1 = row1.createCell(3);
			cell1.setCellValue(principalAmount);

			cell1 = row1.createCell(4);
			cell1.setCellValue("N");

			int y = Integer.parseInt(currentDateSpliting[1]);

			String monthName12 = DateTime.now().withMonthOfYear(y).toString("MMM");

			String requiredDate = currentDateSpliting[0] + "-" + monthName12 + "-" + currentDateSpliting[2];

			cell1 = row1.createCell(5);
			cell1.setCellValue(requiredDate);

			cell1 = row1.createCell(6);
			cell1.setCellValue(lenderIFSC);

			cell1 = row1.createCell(16);
			cell1.setCellValue("LENDERWITHDRAW");

			cell1 = row1.createCell(17);
			cell1.setCellValue(dealId);

			cell1 = row1.createCell(18);
			cell1.setCellValue("LR" + userId);

			cell1 = row1.createCell(19);
			cell1.setCellValue("OXYLOANS " + dealName + "M" + currentDateSpliting[0]);

			cell1 = row1.createCell(20);
			cell1.setCellValue(dealName + "PrincipalAmount");
			File f = new File(iciciFilePathBeforeApproval + sbf.toString() + ".xls");
			try {
				outFile = new FileOutputStream(f);
				workBook.write(outFile);

				workBook.close();
				outFile.close();

			} catch (Exception ex) {
				ex.getMessage();
			}

			LenderCmsPayments lenderCmsPayments = new LenderCmsPayments();
			lenderCmsPayments.setDealId(dealId);
			lenderCmsPayments.setTotalAmount(principalAmount);
			lenderCmsPayments.setFileName(f.getName());
			lenderCmsPayments.setLenderReturnsType("LENDERWITHDRAW");
			lenderCmsPayments.setPaymentDate(new Date());
			lenderCmsPaymentsRepo.save(lenderCmsPayments);

			String message = "Initiated " + dealName + " *LENDERWITHDRAW* with a file name " + f.getName()
					+ " for an amount of " + principalAmount;

			whatappService.sendingWhatsappMessageWithNewInstance("917702795895-1630418928@g.us", message);

			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate1 = expectedDateFormat.format(current.getTime());
			templateContext.put("currentDate", expectedCurrentDate1);

			String mailSubject = "LR" + userId + " dealId" + dealId + " PrincipalSheet";

			EmailRequest emailRequest = new EmailRequest(
					new String[] { "archana.n@oxyloans.com", "ramadevi@oxyloans.com", "subbu@oxyloans.com" },
					"Interests-Approval.template", templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			}
		}

		String fileName = sbf.toString() + ".xls";
		return fileName;

	}

	public Integer differenceInDays(String startDate, String endDate) {

		String startDateValue[] = startDate.split("-");
		int startdate = Integer.parseInt(startDateValue[2]);

		String endDateValue[] = endDate.split("-");
		int endate = Integer.parseInt(endDateValue[2]);
		if (endate == 31) {
			endate = 30;
		}

		long monthsBetween = ChronoUnit.MONTHS.between(LocalDate.parse(startDate).withDayOfMonth(1),
				LocalDate.parse(endDate).withDayOfMonth(1));
		int count = 0;
		if (monthsBetween > 0) {
			count = (int) monthsBetween + 1;
		} // ChronoUnit will remove a month so added one month
		int differenceInDays = 0;
		if (count > 0) {

			String nextDate1 = startDate;
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = null;
			try {
				date = formatter.parse(startDate);

			} catch (ParseException e) {
				e.printStackTrace();

			}
			DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

			int emiCountValue = 1;
			for (int i = 0; i < count; i++) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Calendar c1 = Calendar.getInstance();
				String dateParts[] = nextDate1.split("-");

				int year = Integer.parseInt(dateParts[0]);

				int month = Integer.parseInt(dateParts[1]);

				int date1 = Integer.parseInt(dateParts[2]);
				int days = 0;
				int removedDates = 0;
				if (emiCountValue == 1) {
					if (date1 == 31) {
						removedDates = 30;
					} else {
						removedDates = date1;
					}
					days = 30 - removedDates;
				} else {
					if (emiCountValue != count) {
						days = 30;
					} else {
						if (endate == 31) {
							days = 30;
						} else {
							days = endate;
						}
					}
				}
				System.out.println(+emiCountValue + " " + days);

				c1.set(year, month, date1);

				nextDate1 = sdf.format(c1.getTime());

				try {
					date = sdf.parse(nextDate1);
				} catch (ParseException e) {

					e.printStackTrace();

				}
				emiCountValue = emiCountValue + 1;
				differenceInDays = differenceInDays + days;
			}
		} else {
			differenceInDays = endate - startdate;
		}
		return differenceInDays;

	}

	@Override
	public ListOfLendersWithdrawalFundsInfo getWithdrawalRequestedInfo(int userId, int dealId) {
		ListOfLendersWithdrawalFundsInfo listOfLendersWithdrawalFundsInfo = new ListOfLendersWithdrawalFundsInfo();
		List<LenderWithdrawalFundsFromDealsResponseDto> WithdrawalFunds = new ArrayList<LenderWithdrawalFundsFromDealsResponseDto>();
		List<LendersReturns> listOfWithdrawal = lendersReturnsRepo.getLenderWithdrawRequestedInfo(userId, dealId);
		if (listOfWithdrawal != null && !listOfWithdrawal.isEmpty()) {
			listOfWithdrawal.forEach(lenderWithdrawal -> {
				LenderWithdrawalFundsFromDealsResponseDto lendersReturns = new LenderWithdrawalFundsFromDealsResponseDto();
				lendersReturns.setAmount(lenderWithdrawal.getAmount());
				lendersReturns.setRequestDate(expectedDateFormat.format(lenderWithdrawal.getRequestedOn()));
				lendersReturns.setDealId(lenderWithdrawal.getDealId());
				WithdrawalFunds.add(lendersReturns);
			});
			listOfLendersWithdrawalFundsInfo.setLenderWithdrawalFundsFromDealsResponseDto(WithdrawalFunds);
		}

		return listOfLendersWithdrawalFundsInfo;

	}

	@Override
	public LenderWithdrawalFundsFromDealsResponseDto getWithDrawalApprovedByAdmin(int id) {

		LendersReturns lendersReturns = lendersReturnsRepo.findById(id).get();
		LenderWithdrawalFundsFromDealsResponseDto lenderWithdrawalFunds = new LenderWithdrawalFundsFromDealsResponseDto();
		if (lendersReturns != null) {

			lenderWithdrawalFunds.setAmount(lendersReturns.getAmount());
			lenderWithdrawalFunds.setApprovedDate(expectedDateFormat.format(lendersReturns.getPaidDate()));
			if (lendersReturns.getRequestedOn() != null) {
				lenderWithdrawalFunds.setRequestDate(expectedDateFormat.format(lendersReturns.getRequestedOn()));
			}
			lenderWithdrawalFunds.setInterestAmount(lendersReturns.getInterestamountForWithdrawalfunds());
			lenderWithdrawalFunds.setDealId(lendersReturns.getDealId());
			lenderWithdrawalFunds.setId(lendersReturns.getId());
		}

		return lenderWithdrawalFunds;

	}

	@Override
	public ListOfLendersWithdrawalFundsInfo h2hApprovalForWithDrawalInterestAmount(
			ListOfLendersWithdrawalFundsInfo listOfLendersWithdrawalFundsInfo) {
		ListOfLendersWithdrawalFundsInfo listOfLendersStatus = new ListOfLendersWithdrawalFundsInfo();

		LendersReturns returnsById = lendersReturnsRepo.findById(listOfLendersWithdrawalFundsInfo.getId()).get();

		if (returnsById != null) {
			returnsById.setStatus(LendersReturns.Status.H2HAPPROVAL);
			returnsById.setH2hApprovedOn(new Date());
			String type = "INTEREST";
			Integer principalId = returnsById.getId();
			String fileName = creatingExcelSheetForh2h(listOfLendersWithdrawalFundsInfo, returnsById.getUserId(), type,
					principalId, listOfLendersWithdrawalFundsInfo.getAccountType());
			returnsById.setFileNameForInterest(fileName);
			lendersReturnsRepo.save(returnsById);
		}

		listOfLendersStatus.setStatus("successfullyUpdated");
		return listOfLendersStatus;

	}

	public String movingWalletToH2H(int userId, int requestId) {

		logger.info("movingWalletToH2H starts");
		SimpleDateFormat formatter1 = new SimpleDateFormat("ddMMyy");
		Date date = new Date();
		String currentDate = formatter1.format(date);

		StringBuilder sbf = new StringBuilder("OXYLR2_OXYLR2UPLD_");
		LocalTime time = LocalTime.now();
		String currentTime = time.getHour() + "H" + time.getMinute() + "M" + time.getSecond() + "S";
		sbf.append(currentDate + "_" + "LR" + userId + currentTime + "WalletAmount");

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 40; i++) {
			spreadSheet.autoSizeColumn(i);
		}
		String lenderName = null;
		String lenderAccountNumber = null;
		String lenderIFSC = null;
		String lenderFirstName=null;
		User user = userRepo.findById(userId).get();
		if (user != null) {
			if (user.getBankDetails() != null) {
				if (user.getBankDetails().getUserName() != null) {
					lenderName = user.getBankDetails().getUserName().trim().replaceAll(" +", " ");
				}
				if (user.getBankDetails().getAccountNumber() != null) {
					lenderAccountNumber = user.getBankDetails().getAccountNumber();
				}
				if (user.getBankDetails().getIfscCode() != null) {
					lenderIFSC = user.getBankDetails().getIfscCode().toUpperCase();
				}
			}
			if (user.getPersonalDetails() != null) {

				lenderFirstName = user.getPersonalDetails().getFirstName().trim() + " "+ user.getPersonalDetails().getLastName().trim();

			}
		}
		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		Date today = new Date();
		String currentDateValue = dateFormatter.format(today);

		String currentDateSpliting[] = currentDateValue.split("/");
		LenderWithdrawalFunds lenderWithdrawalFunds = lenderWithdrawalFundsRepo.findById(requestId).get();
		double walletReturningAmount = 0.0;
		if (lenderWithdrawalFunds != null) {
			walletReturningAmount = lenderWithdrawalFunds.getAmount();
		}

		Row row1 = spreadSheet.createRow(++rowCount);
		Cell cell1 = row1.createCell(0);
		cell1.setCellValue("777705849441");

		cell1 = row1.createCell(1);
		cell1.setCellValue(lenderAccountNumber);

		cell1 = row1.createCell(2);
		cell1.setCellValue(lenderName);

		cell1 = row1.createCell(3);
		cell1.setCellValue(walletReturningAmount);

		cell1 = row1.createCell(4);
		cell1.setCellValue("N");

		int y = Integer.parseInt(currentDateSpliting[1]);

		String monthName12 = DateTime.now().withMonthOfYear(y).toString("MMM");

		String requiredDate = currentDateSpliting[0] + "-" + monthName12 + "-" + currentDateSpliting[2];

		cell1 = row1.createCell(5);
		cell1.setCellValue(requiredDate);

		cell1 = row1.createCell(6);
		cell1.setCellValue(lenderIFSC);

		cell1 = row1.createCell(16);
		cell1.setCellValue("LENDERPRINCIPAL");

		cell1 = row1.createCell(17);
		cell1.setCellValue(0);

		cell1 = row1.createCell(18);
		cell1.setCellValue("LR" + userId);

		cell1 = row1.createCell(19);
		cell1.setCellValue("WalletAmount" + currentDateSpliting[0]);

		cell1 = row1.createCell(20);
		cell1.setCellValue("WalletAmount");
		File f = new File(iciciFilePathBeforeApproval + sbf.toString() + ".xls");

		logger.info("file saving starts");
		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);
			logger.info("file saving ends");

			workBook.close();
			outFile.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		LenderCmsPayments lenderCmsPayments = new LenderCmsPayments();
		lenderCmsPayments.setDealId(0);
		lenderCmsPayments.setTotalAmount(walletReturningAmount);
		lenderCmsPayments.setFileName(f.getName());
		lenderCmsPayments.setLenderReturnsType("WALLETWITHDRAW");
		lenderCmsPayments.setPaymentDate(new Date());
		lenderCmsPaymentsRepo.save(lenderCmsPayments);

		logger.info("saved in lenderCmsPayments");

		String whatsappMessage = "Initiated *WALLETWITHDRAW* with a file name " + f.getName() + " for an amount of "
				+ walletReturningAmount + "\n" + "LenderName :*"+lenderFirstName+"*";

		whatappService.sendingWhatsappMessageWithNewInstance("120363020098924954@g.us", whatsappMessage); // int/prin
																											// chat id

		Calendar current = Calendar.getInstance();
		TemplateContext templateContext = new TemplateContext();
		String expectedCurrentDate1 = expectedDateFormat.format(current.getTime());
		templateContext.put("currentDate", expectedCurrentDate1);

		String mailSubject = "LR" + userId + " WalletAmount";

		EmailRequest emailRequest = new EmailRequest(
				new String[] { "archana.n@oxyloans.com", "ramadevi@oxyloans.com", "subbu@oxyloans.com" },
				"Interests-Approval.template", templateContext, mailSubject, new String[] { f.getAbsolutePath() });

		EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
		if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
			try {
				throw new MessagingException(emailResponseFromService.getErrorMessage());
			} catch (MessagingException e2) {

				e2.printStackTrace();
			}
		}

		String message = "successFull";

		logger.info("movingWalletToH2H ends");

		return message;
	}

	public double lenderWalletAmount(int userId) {
		double walletAmount = 0.0;
		Double lenderCurrentWalletBalance = 0.0;
		Double holdAmount = 0.0;
		Double dealValue = 0.0;
		Double dealValueWithAgreements = 0.0;
		Double equityAmount = 0.0;
		Double oldWalletAmount = 0.0;

		Double dealAmount = oxyLendersAcceptedDealsRepo.getDealParticipationAmountIncludingClosedDeals(userId);
		Double dealUpdatedAmount = lendersPaticipationUpdationRepo
				.getSumOfLenderUpdatedAmountButNotInEquityIncludingClosedDeals(userId);
		if (dealAmount != null) {
			if (dealUpdatedAmount != null) {
				dealValue = dealAmount + dealUpdatedAmount;
			} else {
				dealValue = dealAmount;
			}
		}

		Double dealAmountWithAgreements = oxyLendersAcceptedDealsRepo.getDealParticipationAmountAfterAgreements(userId);
		if (dealAmountWithAgreements != null) {
			dealValueWithAgreements = dealAmountWithAgreements;
		}
		holdAmount = dealValue - dealValueWithAgreements;

		Double equityValue = oxyLendersAcceptedDealsRepo.getDealParticipationAmountForEquity(userId);
		Double dealUpdatedAmountInEquity = lendersPaticipationUpdationRepo.getSumOfLenderUpdatedAmountInEquity(userId);
		if (equityValue != null) {
			if (dealUpdatedAmountInEquity != null) {
				equityAmount = equityValue + dealUpdatedAmountInEquity;
			} else {
				equityAmount = equityValue;
			}
		}
		Double lenderWalletCreditAmount = lenderOxyWalletNativeRepo.lenderWalletCreditAmount(userId);
		Double lenderWalletdebitAmount = lenderOxyWalletNativeRepo.lenderWalletDebitAmount(userId);
		Double lenderWalletInprocessAmount = lenderOxyWalletNativeRepo.lenderWalletInprocessAmount(userId);

		if (!(lenderWalletInprocessAmount != null && lenderWalletInprocessAmount > 0)) {
			lenderWalletInprocessAmount = 0d;
		}
		if (!(lenderWalletCreditAmount != null && lenderWalletCreditAmount > 0)) {
			lenderWalletCreditAmount = 0d;
		}
		if (!(lenderWalletdebitAmount != null && lenderWalletdebitAmount > 0)) {
			lenderWalletdebitAmount = 0d;
		}
		Double lenderWalletAmount = lenderWalletCreditAmount - lenderWalletdebitAmount;
		if (lenderWalletAmount != null && lenderWalletAmount > 0) {
			oldWalletAmount = lenderWalletAmount - lenderWalletInprocessAmount;
		} else {
			oldWalletAmount = 0.0;
		}

		lenderCurrentWalletBalance = oldWalletAmount - holdAmount - equityAmount;
		if (lenderCurrentWalletBalance > 0) {
			walletAmount = lenderCurrentWalletBalance;
		}
		return walletAmount;
	}

	public Double getLenderCurrentWalletBalance(int userId) {
		Double lenderCurrentWalletBalance = 0.0;
		Double holdAmount = 0.0;
		Double dealValue = 0.0;
		Double dealValueWithAgreements = 0.0;
		Double equityAmount = 0.0;
		Double oldWalletAmount = 0.0;
		Double dealAmount = oxyLendersAcceptedDealsRepo.getDealParticipationAmount(userId);
		Double dealUpdatedAmount = lendersPaticipationUpdationRepo.getSumOfLenderUpdatedAmountButNotInEquity(userId);
		if (dealAmount != null) {
			if (dealUpdatedAmount != null) {
				dealValue = dealAmount + dealUpdatedAmount;
			} else {
				dealValue = dealAmount;
			}
		}

		Double dealAmountWithAgreements = oxyLendersAcceptedDealsRepo.getDealParticipationAmountAfterAgreements(userId);
		if (dealAmountWithAgreements != null) {
			dealValueWithAgreements = dealAmountWithAgreements;
		}
		holdAmount = dealValue - dealValueWithAgreements;

		Double equityValue = oxyLendersAcceptedDealsRepo.getDealParticipationAmountForEquity(userId);
		Double dealUpdatedAmountInEquity = lendersPaticipationUpdationRepo.getSumOfLenderUpdatedAmountInEquity(userId);
		if (equityValue != null) {
			if (dealUpdatedAmountInEquity != null) {
				equityAmount = equityValue + dealUpdatedAmountInEquity;
			} else {
				equityAmount = equityValue;
			}
		}
		Double lenderWalletCreditAmount = lenderOxyWalletNativeRepo.lenderWalletCreditAmount(userId);
		Double lenderWalletdebitAmount = lenderOxyWalletNativeRepo.lenderWalletDebitAmount(userId);
		Double lenderWalletInprocessAmount = lenderOxyWalletNativeRepo.lenderWalletInprocessAmount(userId);

		if (!(lenderWalletInprocessAmount != null && lenderWalletInprocessAmount > 0)) {
			lenderWalletInprocessAmount = 0d;
		}
		if (!(lenderWalletCreditAmount != null && lenderWalletCreditAmount > 0)) {
			lenderWalletCreditAmount = 0d;
		}
		if (!(lenderWalletdebitAmount != null && lenderWalletdebitAmount > 0)) {
			lenderWalletdebitAmount = 0d;
		}
		Double lenderWalletAmount = lenderWalletCreditAmount - lenderWalletdebitAmount;
		if (lenderWalletAmount != null && lenderWalletAmount > 0) {
			oldWalletAmount = lenderWalletAmount - lenderWalletInprocessAmount;
		} else {
			oldWalletAmount = 0.0;
		}

		lenderCurrentWalletBalance = oldWalletAmount - holdAmount - equityAmount;
		return lenderCurrentWalletBalance;

	}

	public List<LenderWithdrawalFundsResponse> listOfWithdrawsFromDeals(Integer userId) {

		List<LenderWithdrawalFundsResponse> listOfLenderWithdrawalFundsResponse = new ArrayList<LenderWithdrawalFundsResponse>();
		List<LendersReturns> listOfWithdrawsFromDeals = lendersReturnsRepo
				.findingListOfWithdrawAndStatusNotInitiated(userId);

		if (listOfWithdrawsFromDeals != null && !listOfWithdrawsFromDeals.isEmpty()) {
			for (LendersReturns lendersInfo : listOfWithdrawsFromDeals) {
				LenderWithdrawalFundsResponse lenderWithdrawalFundsResponse = new LenderWithdrawalFundsResponse();

				lenderWithdrawalFundsResponse.setUserId(userId);
				lenderWithdrawalFundsResponse.setDealId(lendersInfo.getDealId() == null ? 0 : lendersInfo.getDealId());
				lenderWithdrawalFundsResponse.setAmount(lendersInfo.getAmount());

				if (lendersInfo.getPaidDate() != null) {
					lenderWithdrawalFundsResponse
							.setAmountPaidDate(expectedDateFormat.format(lendersInfo.getPaidDate()));
				}

				lenderWithdrawalFundsResponse.setStatus(lendersInfo.getStatus().toString());
				lenderWithdrawalFundsResponse.setRequestFrom("DEAL");

				if (lendersInfo.getCreatedOn() != null) {
					lenderWithdrawalFundsResponse.setCreatedOn(expectedDateFormat.format(lendersInfo.getCreatedOn()));
				} else {
					lenderWithdrawalFundsResponse.setCreatedOn(expectedDateFormat.format(lendersInfo.getRequestedOn()));
				}

				OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
						.findBydealId(lenderWithdrawalFundsResponse.getDealId());
				if (oxyBorrowersDealsInformation != null) {
					lenderWithdrawalFundsResponse.setWithdrawalReason(oxyBorrowersDealsInformation.getDealName());
				}
				listOfLenderWithdrawalFundsResponse.add(lenderWithdrawalFundsResponse);

			}
		}
		return listOfLenderWithdrawalFundsResponse;
	}

}
