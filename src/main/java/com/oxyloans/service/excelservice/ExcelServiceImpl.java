package com.oxyloans.service.excelservice;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.mail.MessagingException;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.oxyloans.cms.IciciCmsRequestDto;
import com.oxyloans.cms.RejectedFileDto;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.dashboard.LenderTotalPaticipationDealsInfo;
import com.oxyloans.dashboard.LenderWalletHistoryResponseDto;
import com.oxyloans.dashboard.NewLenderReturnsResponseDto;
import com.oxyloans.dashboard.ReferrerResponseDto;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entityborrowersdealsdto.DealQuarterlyReportsRequest;
import com.oxyloans.entityborrowersdealsdto.DealQuarterlyReportsResponse;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.cms.OxyCmsLoans;
import com.oxyloans.entity.lenders.hold.UserHoldAmountMappedToDeal.AmountType;
import com.oxyloans.entity.user.User;
import com.oxyloans.file.FileRequest;
import com.oxyloans.file.FileRequest.FileType;
import com.oxyloans.file.FileResponse;
import com.oxyloans.lender.holdamount.service.HoldAmountService;
import com.oxyloans.lender.participationToWallet.LenderPaticipationRequestDto;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.user.BorrowerPaymentsDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.BankTransactionsBreakUpDto;
import com.oxyloans.response.user.IciciExcelInterestsFiles;
import com.oxyloans.response.user.StatusResponseDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.service.file.IFileManagementService;
import com.oxyloans.serviceloan.InterestsApprovalDto;

@Service
public class ExcelServiceImpl implements ExcelServiceRepo {

	/* @Value("${iciciFilePathBeforeApproval}") */
	// private String iciciFilePathBeforeApproval;

	@Autowired
	protected IFileManagementService fileManagementService;

	private final Logger logger = LogManager.getLogger(ExcelServiceImpl.class);

	@Value("${iciciFilePathBeforeApproval}")
	private String iciciFilePathBeforeApproval;

	@Value("${iciciReportsUpdateFolder}")
	private String iciciReportsUpdateFolder;

	@Autowired
	protected OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Value("${iciciReports}")
	private String iciciReports;

	@Autowired
	private IEmailService emailService;

	protected final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	private SimpleDateFormat dateFormate = new SimpleDateFormat("dd/MM/yyyy");

	@Value("${iciciFilePathBeforeApprovalForPartner}")
	private String iciciFilePathBeforeApprovalForPartner;

	@Autowired
	private HoldAmountService holdAmountService;

	@Autowired
	private UserRepo userRepo;

	@Override
	public String sendBulkInvite(File file, String fileExtension) throws IOException, InvalidFormatException {
		String nameAndEmail = "";
		try {
			logger.info(file.getName());

			Workbook workbook = null;

			FileInputStream fileInputStream = new FileInputStream(file);
			if (fileExtension.equalsIgnoreCase(".xls")) {
				workbook = new HSSFWorkbook(fileInputStream);
			} else if (fileExtension.equalsIgnoreCase(".xlsx")) {
				workbook = new XSSFWorkbook(fileInputStream);
			}

			// It starts by creating a new DataFormatter object, which is used to format the
			// cell data as strings.

			DataFormatter formatter = new DataFormatter();

			Iterator<Sheet> sheets = workbook.iterator();
			/*
			 * Next, it obtains an iterator over the sheets in the Excel workbook using the
			 * workbook.iterator() method. It then iterates over each sheet in the workbook
			 * using a while loop.
			 */

			while (sheets.hasNext()) {
				Sheet sh = sheets.next();

				Iterator<Row> rowIterator = sh.iterator();

				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();

					Cell lastCell = row.getCell(row.getLastCellNum() - 1);
					/*
					 * For each row, it checks if the row number is not zero, indicating that it is
					 * not the header row. It then obtains the cell data for the first and second
					 * columns using the row.getCell() method and assigns them to name and email
					 * Cell objects, respectively.
					 */

					String lastEmail = null;
					String lastName = null;
					String formattedMobileNumber = null;
					long mobileNumberLong = 0;

					if (row.getRowNum() != 0) {

						Cell name = row.getCell(0);

						Cell email = row.getCell(1);

						Cell mobile = row.getCell(2);

						if (name != null && email != null && mobile != null) {
							// Retrieve the values as strings

							if (mobile.getCellTypeEnum() == CellType.NUMERIC) {
								double mobileNumber = mobile.getNumericCellValue();
								mobileNumberLong = (long) mobileNumber; // Convert to long if needed

								// Format the mobile number as a 10-digit string
								DecimalFormat decimalFormat = new DecimalFormat("0000000000");
								formattedMobileNumber = decimalFormat.format(mobileNumberLong);

								System.out.println("Mobile Number: " + formattedMobileNumber);

								String name1 = name.getStringCellValue();

								// System.out.println("mobile..."+mobile.getStringCellValue());

								String email1 = email.getStringCellValue();

								// Update the last email and name if they are not empty
								if (!email1.isEmpty()) {
									lastEmail = email1;
								}

								if (!name1.isEmpty()) {
									lastName = name1;
								}
							}
							if (lastName != null) {
								nameAndEmail = nameAndEmail + lastName + "-" + lastEmail + "-" + formattedMobileNumber
										+ ",";
							}

							System.out.println("nameAndEmail  ...." + nameAndEmail);

							/*
							 * It then appends the name and email cell data to a String variable named
							 * nameAndEmail, separated by a dash (-) and followed by a comma (,). Finally,
							 * it prints the nameAndEmail variable to the console.
							 */
						}
					}
				}

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		nameAndEmail = nameAndEmail.substring(0, nameAndEmail.length() - 1);

		logger.info(nameAndEmail);

		return nameAndEmail;

	}

	@Override
	public String walletCreditedOrDebitedExcel(String type, int userId,
			List<LenderWalletHistoryResponseDto> lenderWalletHistoryResponseDtoForCredited) {

		String downloadUrl = "";

		FileOutputStream outFile = null;
		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet("sheet1");
		HSSFFont headerFont = workBook.createFont();
		CellStyle style = workBook.createCellStyle();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);
		style.setFont(headerFont);
		spreadSheet.createFreezePane(0, 1);
		Row row = spreadSheet.createRow(0);

		Cell cell = row.createCell(0);
		cell.setCellValue("user id");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("Name");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("Participated Date/Wallet Loaded");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("Description");
		cell.setCellStyle(style);

		cell = row.createCell(4);
		cell.setCellValue("Amount");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 25; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		for (LenderWalletHistoryResponseDto obj : lenderWalletHistoryResponseDtoForCredited) {

			Row row1 = spreadSheet.createRow(++rowCount);

			Cell cell1 = row1.createCell(0);
			cell1.setCellValue("LR" + obj.getUserId());

			cell1 = row1.createCell(1);
			cell1.setCellValue(obj.getName());

			cell1 = row1.createCell(2);
			cell1.setCellValue(obj.getWalletLoaded() != null ? obj.getWalletLoaded() : "");

			cell1 = row1.createCell(3);
			cell1.setCellValue(obj.getRemarks() != null ? obj.getRemarks() : "");

			cell1 = row1.createCell(4);
			cell1.setCellValue(obj.getAmount() != null ? obj.getAmount().doubleValue() : 0);

		}

		String filePath = "";

		if (type.equalsIgnoreCase("credit")) {
			filePath = "LR" + userId + "_" + LocalDate.now() + "_WalletLoaded.xls";
		}

		if (type.equalsIgnoreCase("debit")) {
			filePath = "LR" + userId + "_" + LocalDate.now() + "_WalletDebited.xls";
		}

		File f = new File(filePath);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);
			if (f.exists()) {
				// response.setFileStatus("File saved");
			}

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(f.getName());
			fileRequest.setFileType(FileType.LenderWalletInfo);
			fileRequest.setFilePrifix(FileType.LenderWalletInfo.name());

			FileResponse putFile = fileManagementService.putFile(fileRequest);
			logger.info("TransactionResponses file {} uploaded to as {}", filePath, putFile.getFileName());

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			downloadUrl = getFile.getDownloadUrl();

		} catch (Exception ex) {
			throw new OperationNotAllowedException(ex.getMessage(), ErrorCodes.PERMISSION_DENIED);
		}

		return downloadUrl;
	}

	@Override
	public String interestRetrunedExcel(int userId, List<NewLenderReturnsResponseDto> lenderInterestReturnsResponseDto,
			double toatlInterest) {

		String downloadUrl = "";

		FileOutputStream outFile = null;
		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet("sheet1");
		HSSFFont headerFont = workBook.createFont();
		CellStyle style = workBook.createCellStyle();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);
		style.setFont(headerFont);
		spreadSheet.createFreezePane(0, 1);
		Row row = spreadSheet.createRow(0);

		Cell cell = row.createCell(0);
		cell.setCellValue("sNo");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("PaticipatedDate");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("ROI");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("RepaymentType");
		cell.setCellStyle(style);

		cell = row.createCell(4);
		cell.setCellValue("InterestReturned");
		cell.setCellStyle(style);

		cell = row.createCell(5);
		cell.setCellValue("DealClosedDate");
		cell.setCellStyle(style);

		cell = row.createCell(6);
		cell.setCellValue("InterestAmount");
		cell.setCellStyle(style);

		cell = row.createCell(7);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		cell = row.createCell(8);
		cell.setCellValue("DealStatus");
		cell.setCellStyle(style);

		cell = row.createCell(9);
		cell.setCellValue("PaymentType");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 25; i++) {
			spreadSheet.autoSizeColumn(i);
		}
		int sno = 0;
		for (NewLenderReturnsResponseDto obj : lenderInterestReturnsResponseDto) {
			sno += 1;
			Row row1 = spreadSheet.createRow(++rowCount);

			Cell cell1 = row1.createCell(0);
			cell1.setCellValue(sno);

			cell1 = row1.createCell(1);
			cell1.setCellValue(obj.getPaticipatedDate());

			cell1 = row1.createCell(2);
			cell1.setCellValue(obj.getRateOfInterest());

			cell1 = row1.createCell(3);
			cell1.setCellValue(obj.getRepaymentType());

			cell1 = row1.createCell(4);
			cell1.setCellValue(obj.getReturedDate());

			cell1 = row1.createCell(5);
			cell1.setCellValue(obj.getDealClosedDate());

			cell1 = row1.createCell(6);
			cell1.setCellValue(obj.getAmount().doubleValue());

			cell1 = row1.createCell(7);
			cell1.setCellValue(obj.getRemarks());

			cell1 = row1.createCell(8);
			cell1.setCellValue(obj.getDealClosingStatus());

			cell1 = row1.createCell(9);
			cell1.setCellValue(obj.getRepaymentType());

		}

		for (int i = 0; i <= 1; i++) {
			Row row1 = spreadSheet.createRow(++rowCount);
			if (i == 1) {

				Cell cell1 = row1.createCell(0);

				cell1 = row1.createCell(5);
				cell1.setCellValue("Total Interest");

				cell1 = row1.createCell(6);
				cell1.setCellValue(BigDecimal.valueOf(toatlInterest).toBigInteger().toString());

			}
		}
		String filePath = iciciFilePathBeforeApproval + "LR" + userId + "_" + LocalDate.now() + ".xls";

		File f = new File(filePath);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);
			if (f.exists()) {
				// response.setFileStatus("File saved");
			}

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(f.getName());
			fileRequest.setFileType(FileType.LenderInterestInfo);
			fileRequest.setFilePrifix(FileType.LenderInterestInfo.name());

			FileResponse putFile = fileManagementService.putFile(fileRequest);
			logger.info("TransactionResponses file {} uploaded to as {}", filePath, putFile.getFileName());

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			downloadUrl = getFile.getDownloadUrl();

		} catch (Exception ex) {

		}

		return downloadUrl;
	}

	@Override
	public String lenderPrincipalReturnedExcel(int userId,
			List<NewLenderReturnsResponseDto> lenderPricipalReturnsResponseDto) {

		String downloadUrl = "";

		FileOutputStream outFile = null;
		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet("sheet1");
		HSSFFont headerFont = workBook.createFont();
		CellStyle style = workBook.createCellStyle();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);
		style.setFont(headerFont);
		spreadSheet.createFreezePane(0, 1);
		Row row = spreadSheet.createRow(0);

		Cell cell = row.createCell(0);
		cell.setCellValue("SNo");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("DealName");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("PaticipatedAmount");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("RepaymentType");
		cell.setCellStyle(style);

		cell = row.createCell(4);
		cell.setCellValue("RateOfInterest");
		cell.setCellStyle(style);

		cell = row.createCell(5);
		cell.setCellValue("ReturnedDate");
		cell.setCellStyle(style);

		cell = row.createCell(6);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 25; i++) {
			spreadSheet.autoSizeColumn(i);
		}
		int count = 0;
		for (NewLenderReturnsResponseDto obj : lenderPricipalReturnsResponseDto) {

			Row row1 = spreadSheet.createRow(++rowCount);
			count = count + 1;
			Cell cell1 = row1.createCell(0);
			cell1.setCellValue(count);

			cell1 = row1.createCell(1);
			cell1.setCellValue(obj.getDealName());

			cell1 = row1.createCell(2);
			cell1.setCellValue(obj.getAmount().doubleValue());

			cell1 = row1.createCell(3);
			cell1.setCellValue(obj.getRepaymentType());

			cell1 = row1.createCell(4);
			cell1.setCellValue(obj.getRateOfInterest());

			cell1 = row1.createCell(5);
			cell1.setCellValue(obj.getReturedDate());

			cell1 = row1.createCell(6);
			cell1.setCellValue(obj.getRemarks());

		}

		String filePath = iciciFilePathBeforeApproval + "LR" + userId + "_" + LocalDate.now() + ".xls";

		File f = new File(filePath);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);
			if (f.exists()) {
				// response.setFileStatus("File saved");
			}

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(f.getName());
			fileRequest.setFileType(FileType.PrincipalReturned);
			fileRequest.setFilePrifix(FileType.PrincipalReturned.name());

			FileResponse putFile = fileManagementService.putFile(fileRequest);
			logger.info("TransactionResponses file {} uploaded to as {}", filePath, putFile.getFileName());

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			downloadUrl = getFile.getDownloadUrl();

		} catch (Exception ex) {

		}

		return downloadUrl;
	}

	public String lenderParticipationInfoExcel(int userId,
			List<LenderTotalPaticipationDealsInfo> lenderTotalPaticipationDealsInfo) {

		String downloadUrl = "";

		FileOutputStream outFile = null;
		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet("sheet1");
		HSSFFont headerFont = workBook.createFont();
		CellStyle style = workBook.createCellStyle();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);
		style.setFont(headerFont);
		spreadSheet.createFreezePane(0, 1);
		Row row = spreadSheet.createRow(0);

		Cell cell = row.createCell(0);
		cell.setCellValue("user id");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("Name");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("Participated Date");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("Participated Amount");
		cell.setCellStyle(style);

		cell = row.createCell(4);
		cell.setCellValue("ROI");
		cell.setCellStyle(style);

		cell = row.createCell(5);
		cell.setCellValue("Deal Name");
		cell.setCellStyle(style);

		cell = row.createCell(6);
		cell.setCellValue("Deal Status");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 25; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		for (LenderTotalPaticipationDealsInfo obj : lenderTotalPaticipationDealsInfo) {

			Row row1 = spreadSheet.createRow(++rowCount);

			Cell cell1 = row1.createCell(0);
			cell1.setCellValue("LR" + obj.getUserId());

			cell1 = row1.createCell(1);
			cell1.setCellValue(obj.getName());

			cell1 = row1.createCell(2);
			cell1.setCellValue(obj.getParticipatedDate() != null ? obj.getParticipatedDate() : "");

			cell1 = row1.createCell(3);
			cell1.setCellValue(obj.getParticipatedAmount() != null ? obj.getParticipatedAmount().doubleValue() : 0);

			cell1 = row1.createCell(4);
			cell1.setCellValue(obj.getRateofinterest());

			cell1 = row1.createCell(5);
			cell1.setCellValue(obj.getDealName() != null ? obj.getDealName() : "");

			cell1 = row1.createCell(6);
			cell1.setCellValue(obj.getPricipaleReturnedStatus() != null ? obj.getPricipaleReturnedStatus() : "");

		}

		String filePath = "LR" + userId + "_" + LocalDate.now() + ".xls";

		File f = new File(filePath);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);
			if (f.exists()) {
				// response.setFileStatus("File saved");
			}

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(f.getName());
			fileRequest.setFileType(FileType.ParticipationInfo);
			fileRequest.setFilePrifix(FileType.ParticipationInfo.name());

			FileResponse putFile = fileManagementService.putFile(fileRequest);
			logger.info("TransactionResponses file {} uploaded to as {}", filePath, putFile.getFileName());

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			downloadUrl = getFile.getDownloadUrl();

		} catch (Exception ex) {

			throw new OperationNotAllowedException(ex.getMessage(), ErrorCodes.PERMISSION_DENIED);
		}

		return downloadUrl;

	}

	@Override
	public String lenderReferalBonousInfoExcel(int userId, List<ReferrerResponseDto> referrerResponseDto) {

		String downloadUrl = "";

		FileOutputStream outFile = null;
		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet("sheet1");
		HSSFFont headerFont = workBook.createFont();
		CellStyle style = workBook.createCellStyle();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);
		style.setFont(headerFont);
		spreadSheet.createFreezePane(0, 1);
		Row row = spreadSheet.createRow(0);

		Cell cell = row.createCell(0);
		cell.setCellValue("user id");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("Name");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("Referee Id");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("Referee Name");
		cell.setCellStyle(style);

		cell = row.createCell(4);
		cell.setCellValue("Amount");
		cell.setCellStyle(style);

		cell = row.createCell(5);
		cell.setCellValue("Payment Status");
		cell.setCellStyle(style);

		cell = row.createCell(6);
		cell.setCellValue("Paid Date");
		cell.setCellStyle(style);

		cell = row.createCell(7);
		cell.setCellValue("Deal Name");
		cell.setCellStyle(style);

		cell = row.createCell(8);
		cell.setCellValue("Participated date");
		cell.setCellStyle(style);

		/*
		 * cell = row.createCell(9); cell.setCellValue("Participated Amount");
		 * cell.setCellStyle(style);
		 */
		int rowCount = 0;

		for (int i = 0; i <= 25; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		for (ReferrerResponseDto obj : referrerResponseDto) {

			Row row1 = spreadSheet.createRow(++rowCount);

			Cell cell1 = row1.createCell(0);
			cell1.setCellValue("LR" + obj.getUserId());

			cell1 = row1.createCell(1);
			cell1.setCellValue(obj.getName());

			cell1 = row1.createCell(2);
			cell1.setCellValue(obj.getRefereeId());

			cell1 = row1.createCell(3);
			cell1.setCellValue(obj.getRefereeName());

			cell1 = row1.createCell(4);
			cell1.setCellValue(obj.getAmount() != null ? obj.getAmount().doubleValue() : 0);

			cell1 = row1.createCell(5);
			cell1.setCellValue(obj.getPaymentStatus());

			cell1 = row1.createCell(6);
			cell1.setCellValue(obj.getTransferredOn());

			cell1 = row1.createCell(7);
			cell1.setCellValue(obj.getDealName());

			cell1 = row1.createCell(8);
			cell1.setCellValue(obj.getParticipatedDate());

			/*
			 * cell1 = row1.createCell(9); cell1.setCellValue(obj.getParticipatedAmount() !=
			 * null ? obj.getParticipatedAmount().intValue() : 0);
			 */
		}

		String filePath = "LR" + userId + "_" + LocalDate.now() + ".xls";

		File f = new File(filePath);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);
			if (f.exists()) {
				// response.setFileStatus("File saved");
			}

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(f.getName());
			fileRequest.setFileType(FileType.ReferalBonus);
			fileRequest.setFilePrifix(FileType.ReferalBonus.name());

			FileResponse putFile = fileManagementService.putFile(fileRequest);
			logger.info("TransactionResponses file {} uploaded to as {}", filePath, putFile.getFileName());

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			downloadUrl = getFile.getDownloadUrl();

		} catch (Exception ex) {

			throw new OperationNotAllowedException(ex.getMessage(), ErrorCodes.PERMISSION_DENIED);
		}
		return downloadUrl;

	}

	@Override
	public StatusResponseDto writeInterestsApprovalInfo(String fileName,
			List<InterestsApprovalDto> interestsApprovalDto) {

		logger.info("writing excel starts");

		StatusResponseDto response = new StatusResponseDto();
		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet("sheet1");
		HSSFFont headerFont = workBook.createFont();
		CellStyle style = workBook.createCellStyle();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);
		style.setFont(headerFont);
		spreadSheet.createFreezePane(0, 1);
		Row row = spreadSheet.createRow(0);

		Cell cell = row.createCell(0);
		cell.setCellValue("user id");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("Lender Name");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("Participated Date");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("Group Name");
		cell.setCellStyle(style);

		cell = row.createCell(4);
		cell.setCellValue("Payment Method");
		cell.setCellStyle(style);

		cell = row.createCell(5);
		cell.setCellValue("Rate of interest");
		cell.setCellStyle(style);

		cell = row.createCell(6);
		cell.setCellValue("Participated Amount");
		cell.setCellStyle(style);

		cell = row.createCell(7);
		cell.setCellValue("Interest Amount");
		cell.setCellStyle(style);

		cell = row.createCell(8);
		cell.setCellValue("Bank A/c No.");
		cell.setCellStyle(style);

		cell = row.createCell(9);
		cell.setCellValue("IFSC code");
		cell.setCellStyle(style);

		cell = row.createCell(10);
		cell.setCellValue("Name as per bank");
		cell.setCellStyle(style);

		cell = row.createCell(11);
		cell.setCellValue("Bank Name");
		cell.setCellStyle(style);

		cell = row.createCell(12);
		cell.setCellValue("Branch Name");
		cell.setCellStyle(style);

		cell = row.createCell(13);
		cell.setCellValue("Bhargav Approval status");
		cell.setCellStyle(style);

		cell = row.createCell(14);
		cell.setCellValue("Subbu Approval status");
		cell.setCellStyle(style);

		cell = row.createCell(15);
		cell.setCellValue("Radha Approval status");
		cell.setCellStyle(style);

		cell = row.createCell(16);
		cell.setCellValue("Bhargav remarks");
		cell.setCellStyle(style);

		cell = row.createCell(17);
		cell.setCellValue("Subbu remarks");
		cell.setCellStyle(style);

		cell = row.createCell(18);
		cell.setCellValue("Radha remarks");
		cell.setCellStyle(style);

		cell = row.createCell(19);
		cell.setCellValue("days diff");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 25; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		for (InterestsApprovalDto obj : interestsApprovalDto) {

			Row row1 = spreadSheet.createRow(++rowCount);

			Cell cell1 = row1.createCell(0);
			cell1.setCellValue(obj.getUserId());

			cell1 = row1.createCell(1);
			cell1.setCellValue(obj.getLenderName());

			cell1 = row1.createCell(2);
			cell1.setCellValue(obj.getParticipatedDate());

			cell1 = row1.createCell(3);
			cell1.setCellValue(obj.getGroupName() != null ? obj.getGroupName() : " ");

			cell1 = row1.createCell(4);
			cell1.setCellValue(obj.getPaymentMethod());

			cell1 = row1.createCell(6);
			cell1.setCellValue(obj.getParticipatedAmount() == null ? 0d : obj.getParticipatedAmount());

			cell1 = row1.createCell(5);
			cell1.setCellValue(obj.getRateOfInterest());

			cell1 = row1.createCell(7);
			cell1.setCellValue(obj.getInterestAmount());

			cell1 = row1.createCell(8);
			cell1.setCellValue(obj.getBankAccountNumber());

			cell1 = row1.createCell(9);
			cell1.setCellValue(obj.getIfscCode());

			cell1 = row1.createCell(10);
			cell1.setCellValue(obj.getNameAsPerBank());

			cell1 = row1.createCell(11);
			cell1.setCellValue(obj.getBankName());

			cell1 = row1.createCell(12);
			cell1.setCellValue(obj.getBranchName());

			cell1 = row1.createCell(19);
			cell1.setCellValue(obj.getDaysDifference());

		}

		logger.info("fileName while writing :" + fileName);

		File f = new File(fileName);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);
			response.setStatus(f.getName() + " Created");
			workBook.close();
			outFile.close();

		} catch (Exception ex) {
			ex.getMessage();
		}

		logger.info("writing excel ends");

		return response;

	}

	private void deleteRow(HSSFSheet sheet, Row row) {
		int lastRowNum = sheet.getLastRowNum();
		int rowIndex = row.getRowNum();
		if (rowIndex >= 0 && rowIndex < lastRowNum) {
			sheet.shiftRows(rowIndex + 1, lastRowNum, -1);
		}
		if (rowIndex == lastRowNum) {
			Row removingRow = sheet.getRow(rowIndex);
			if (removingRow != null) {
				sheet.removeRow(removingRow);
				System.out.println("Deleting.... ");
			}
		}
	}

	public List<InterestsApprovalDto> readExcelInterestPaymentsExcelAsPerApprover(File file, String name) {

		List<InterestsApprovalDto> response = new ArrayList<InterestsApprovalDto>();

		try {
			FileInputStream fileInputStream = new FileInputStream(file);

			HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);

			DataFormatter formatter = new DataFormatter();

			Iterator<Sheet> sheets = workbook.sheetIterator();

			while (sheets.hasNext()) {
				Sheet sh = sheets.next();

				Iterator<Row> rowIterator = sh.iterator();

				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();

					if (row.getRowNum() != 0) {

						Cell userIdCell = row.getCell(0);

						Cell nameCell = row.getCell(1);

						Cell participcatedDateCell = row.getCell(2);

						Cell groupNameCell = row.getCell(3);

						Cell paymentTypeCell = row.getCell(4);

						Cell rateOfinterestCell = row.getCell(5);

						Cell particpatedAmountCell = row.getCell(6);

						Cell interestAmountCell = row.getCell(7);

						Cell bankAccountNo = row.getCell(8);

						Cell ifscCode = row.getCell(9);

						Cell nameAsPerBank = row.getCell(10);

						Cell bankName = row.getCell(11);

						Cell branchName = row.getCell(12);

						Cell bhargavApprovalStatus = row.getCell(13);

						String bhargavStatus = "";
						if (bhargavApprovalStatus != null) {
							bhargavStatus = bhargavApprovalStatus.getStringCellValue();
						} else {
							bhargavStatus = "";
						}

						Cell subbuApprovalStatus = row.getCell(14);

						String subbuStatus = "";
						if (subbuApprovalStatus != null) {
							subbuStatus = subbuApprovalStatus.getStringCellValue();
						} else {
							subbuStatus = "";
						}

						Cell radhaApprovalStatus = row.getCell(15);

						String radhaStatus = "";
						if (radhaApprovalStatus != null) {
							radhaStatus = radhaApprovalStatus.getStringCellValue();
						} else {
							radhaStatus = "";
						}

						InterestsApprovalDto dto = new InterestsApprovalDto();

						dto.setUserId((int) userIdCell.getNumericCellValue());

						dto.setLenderName(nameCell.getStringCellValue());

						dto.setParticipatedDate(participcatedDateCell.getStringCellValue());

						dto.setGroupName(groupNameCell.getStringCellValue());

						dto.setPaymentMethod(paymentTypeCell.getStringCellValue());

						dto.setParticipatedAmount(particpatedAmountCell.getNumericCellValue());

						dto.setInterestAmount(interestAmountCell.getNumericCellValue());

						dto.setBankAccountNumber(bankAccountNo.getStringCellValue());

						dto.setIfscCode(ifscCode.getStringCellValue());

						dto.setBankName(bankName.getStringCellValue());

						dto.setBranchName(branchName.getStringCellValue());

						dto.setNameAsPerBank(nameAsPerBank.getStringCellValue());

						response.add(dto);

					}
				}

			}
			workbook.close();
			fileInputStream.close();

		} catch (Exception e) {

			e.printStackTrace();
		}
		return response;

	}

	@Override
	public StatusResponseDto updateApprovedExcelSheetInfo(Map<Integer, InterestsApprovalDto> map, File file,
			String name) {

		logger.info("updateApprovedExcelSheetInfo starts");

		StatusResponseDto response = new StatusResponseDto();

		try {
			FileInputStream fileInputStream = new FileInputStream(file);

			HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);

			DataFormatter formatter = new DataFormatter();

			Iterator<Sheet> sheets = workbook.sheetIterator();

			while (sheets.hasNext()) {
				Sheet sh = sheets.next();

				Iterator<Row> rowIterator = sh.iterator();

				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();

					if (row.getRowNum() != 0) {
						Cell userIdCell = row.getCell(0);

						Cell statusCell = null;

						Cell remarksCell = null;

						if (name.equalsIgnoreCase("bhargav")) {
							statusCell = row.getCell(13);
							remarksCell = row.getCell(16);

							if (statusCell == null) {
								statusCell = row.createCell(13);
							}

							if (remarksCell == null) {
								remarksCell = row.createCell(16);
							}
						}

						if (name.equalsIgnoreCase("subbu")) {
							statusCell = row.getCell(14);

							remarksCell = row.getCell(17);
							if (statusCell == null) {
								statusCell = row.createCell(14);
							}

							if (remarksCell == null) {
								remarksCell = row.createCell(17);
							}
						}

						if (name.equalsIgnoreCase("radha")) {
							statusCell = row.getCell(15);
							remarksCell = row.getCell(18);

							if (statusCell == null) {
								statusCell = row.createCell(15);
							}

							if (remarksCell == null) {
								remarksCell = row.createCell(18);
							}
						}

						int id = (int) userIdCell.getNumericCellValue();

						logger.info(id);
						if (map.containsKey(id)) {

							InterestsApprovalDto interestsApprovalDto = map.get(id);
							String status1 = interestsApprovalDto.getApprovalStatus();
							String remarks = interestsApprovalDto.getRemarks();
							if (status1.equalsIgnoreCase("onhold")) {
								status1 = "";
							}
							statusCell.setCellValue(status1);
							remarksCell.setCellValue(remarks);

						}

					}
				}
			}

			fileInputStream.close();
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			workbook.write(fileOutputStream);
			response.setStatus(file.getName() + " updated");
			fileOutputStream.close();
			workbook.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("updateApprovedExcelSheetInfo starts");
		return response;
	}

	@Override
	public StatusResponseDto updateForEveryLender(File file, String name) {

		StatusResponseDto response = new StatusResponseDto();
		try {
			FileInputStream fileInputStream = new FileInputStream(file);

			HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);

			DataFormatter formatter = new DataFormatter();

			Iterator<Sheet> sheets = workbook.sheetIterator();

			while (sheets.hasNext()) {
				Sheet sh = sheets.next();

				Iterator<Row> rowIterator = sh.iterator();

				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();

					if (row.getRowNum() != 0) {

						Cell statusCell = null;

						if (name.equalsIgnoreCase("bhargav")) {
							statusCell = row.getCell(13);

							if (statusCell == null) {
								statusCell = row.createCell(13);
							}
						}

						if (name.equalsIgnoreCase("subbu")) {
							statusCell = row.getCell(14);

							if (statusCell == null) {
								statusCell = row.createCell(14);
							}
						}

						if (name.equalsIgnoreCase("radha")) {
							statusCell = row.getCell(15);

							if (statusCell == null) {
								statusCell = row.createCell(15);
							}
						}

						statusCell.setCellValue("APPROVED");

					}

				}

			}

			fileInputStream.close();
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			workbook.write(fileOutputStream);
			response.setStatus(file.getName() + " updated");
			fileOutputStream.close();
			workbook.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return response;
	}

	@Override
	public List<InterestsApprovalDto> findAllBasedOnStatus(File file, String name, String status) {

		logger.info("reading starts");

		List<InterestsApprovalDto> response = new ArrayList<InterestsApprovalDto>();
		try {
			FileInputStream fileInputStream = new FileInputStream(file);

			int count = 0;
			HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);

			DataFormatter formatter = new DataFormatter();

			Iterator<Sheet> sheets = workbook.sheetIterator();
			while (sheets.hasNext()) {
				Sheet sh = sheets.next();

				Iterator<Row> rowIterator = sh.iterator();

				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();

					if (row.getRowNum() != 0) {

						Cell userIdCell = row.getCell(0);

						Cell nameCell = row.getCell(1);

						Cell participcatedDateCell = row.getCell(2);

						Cell groupNameCell = row.getCell(3);

						Cell paymentTypeCell = row.getCell(4);

						Cell rateOfinterestCell = row.getCell(5);

						Cell particpatedAmountCell = row.getCell(6);

						Cell interestAmountCell = row.getCell(7);

						Cell bankAccountNo = row.getCell(8);

						Cell ifscCode = row.getCell(9);

						Cell nameAsPerBank = row.getCell(10);

						Cell bankName = row.getCell(11);

						Cell branchName = row.getCell(12);

						Cell bhargavApprovalStatus = row.getCell(13);

						String bhargavStatus = "";
						if (bhargavApprovalStatus != null) {
							bhargavStatus = bhargavApprovalStatus.getStringCellValue();
						} else {
							bhargavStatus = "";
						}

						Cell subbuApprovalStatus = row.getCell(14);

						String subbuStatus = "";
						if (subbuApprovalStatus != null) {
							subbuStatus = subbuApprovalStatus.getStringCellValue();
						} else {
							subbuStatus = "";
						}

						Cell radhaApprovalStatus = row.getCell(15);

						String radhaStatus = "";
						if (radhaApprovalStatus != null) {
							radhaStatus = radhaApprovalStatus.getStringCellValue();
						} else {
							radhaStatus = "";
						}

						Cell noOfDaysCell = row.getCell(19);

						if (name.equalsIgnoreCase("bhargav") && bhargavStatus.equalsIgnoreCase(status)) {
							InterestsApprovalDto dto = new InterestsApprovalDto();

							dto.setUserId((int) userIdCell.getNumericCellValue());

							dto.setLenderName(nameCell.getStringCellValue());

							dto.setParticipatedDate(participcatedDateCell.getStringCellValue());

							dto.setGroupName(groupNameCell.getStringCellValue());

							dto.setPaymentMethod(paymentTypeCell.getStringCellValue());

							dto.setParticipatedAmount(particpatedAmountCell.getNumericCellValue());

							dto.setInterestAmount(interestAmountCell.getNumericCellValue());

							dto.setBankAccountNumber(bankAccountNo.getStringCellValue());

							dto.setIfscCode(ifscCode.getStringCellValue());

							dto.setSno(++count);
							dto.setBankName(bankName.getStringCellValue());

							dto.setBranchName(branchName.getStringCellValue());

							dto.setNameAsPerBank(nameAsPerBank.getStringCellValue());

							dto.setDaysDifference((int) noOfDaysCell.getNumericCellValue());

							response.add(dto);
						}

						if (name.equalsIgnoreCase("subbu") && bhargavStatus.equalsIgnoreCase("approved")
								&& subbuStatus.equalsIgnoreCase(status)) {

							InterestsApprovalDto dto = new InterestsApprovalDto();
							dto.setUserId((int) userIdCell.getNumericCellValue());

							dto.setLenderName(nameCell.getStringCellValue());

							dto.setParticipatedDate(participcatedDateCell.getStringCellValue());

							dto.setGroupName(groupNameCell.getStringCellValue());

							dto.setPaymentMethod(paymentTypeCell.getStringCellValue());

							dto.setParticipatedAmount(particpatedAmountCell.getNumericCellValue());

							dto.setInterestAmount(interestAmountCell.getNumericCellValue());

							dto.setBankAccountNumber(bankAccountNo.getStringCellValue());

							dto.setSno(++count);
							dto.setIfscCode(ifscCode.getStringCellValue());

							dto.setBankName(bankName.getStringCellValue());

							dto.setBranchName(branchName.getStringCellValue());

							dto.setNameAsPerBank(nameAsPerBank.getStringCellValue());

							dto.setDaysDifference((int) noOfDaysCell.getNumericCellValue());

							response.add(dto);
						}

						if (name.equalsIgnoreCase("radha") && bhargavStatus.equalsIgnoreCase("approved")
								&& subbuStatus.equalsIgnoreCase("approved") && radhaStatus.equalsIgnoreCase(status)) {
							InterestsApprovalDto dto = new InterestsApprovalDto();
							dto.setUserId((int) userIdCell.getNumericCellValue());

							dto.setLenderName(nameCell.getStringCellValue());

							dto.setParticipatedDate(participcatedDateCell.getStringCellValue());

							dto.setGroupName(groupNameCell.getStringCellValue());

							dto.setPaymentMethod(paymentTypeCell.getStringCellValue());

							dto.setParticipatedAmount(particpatedAmountCell.getNumericCellValue());

							dto.setInterestAmount(interestAmountCell.getNumericCellValue());

							dto.setSno(++count);

							dto.setBankAccountNumber(bankAccountNo.getStringCellValue());

							dto.setIfscCode(ifscCode.getStringCellValue());

							dto.setBankName(bankName.getStringCellValue());

							dto.setBranchName(branchName.getStringCellValue());

							dto.setNameAsPerBank(nameAsPerBank.getStringCellValue());

							dto.setDaysDifference((int) noOfDaysCell.getNumericCellValue());

							response.add(dto);
						}

					}
				}
			}

			workbook.close();
			fileInputStream.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		logger.info("reading ends");
		return response;
	}

	@Override
	public List<InterestsApprovalDto> readExcelInterestPaymentsExcel(File file) {

		List<InterestsApprovalDto> response = new ArrayList<InterestsApprovalDto>();

		try {
			FileInputStream fileInputStream = new FileInputStream(file);

			HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);

			DataFormatter formatter = new DataFormatter();

			Iterator<Sheet> sheets = workbook.sheetIterator();

			while (sheets.hasNext()) {
				Sheet sh = sheets.next();

				Iterator<Row> rowIterator = sh.iterator();

				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();

					if (row.getRowNum() != 0) {

						Cell userIdCell = row.getCell(0);

						Cell nameCell = row.getCell(1);

						Cell interestAmountCell = row.getCell(7);

						Cell bankAccountNo = row.getCell(8);

						Cell ifscCode = row.getCell(9);

						Cell nameAsPerBank = row.getCell(10);

						Cell bhargavApprovalStatus = row.getCell(13);

						String bhargavStatus = "";
						if (bhargavApprovalStatus != null) {
							bhargavStatus = bhargavApprovalStatus.getStringCellValue();
						} else {
							bhargavStatus = "";
						}

						Cell subbuApprovalStatus = row.getCell(14);

						String subbuStatus = "";
						if (subbuApprovalStatus != null) {
							subbuStatus = subbuApprovalStatus.getStringCellValue();
						} else {
							subbuStatus = "";
						}

						Cell radhaApprovalStatus = row.getCell(15);

						Cell daysDiff = row.getCell(19);

						String radhaStatus = "";
						if (radhaApprovalStatus != null) {
							radhaStatus = radhaApprovalStatus.getStringCellValue();
						} else {
							radhaStatus = "";
						}

						if (bhargavStatus.equalsIgnoreCase("approved") && subbuStatus.equalsIgnoreCase("approved")
								&& radhaStatus.equalsIgnoreCase("approved")) {
							InterestsApprovalDto dto = new InterestsApprovalDto();

							dto.setUserId((int) userIdCell.getNumericCellValue());

							dto.setLenderName(nameCell.getStringCellValue());

							dto.setInterestAmount(interestAmountCell.getNumericCellValue());

							dto.setBankAccountNumber(bankAccountNo.getStringCellValue());

							dto.setIfscCode(ifscCode.getStringCellValue());

							dto.setDaysDifference((int) daysDiff.getNumericCellValue());

							dto.setNameAsPerBank(nameAsPerBank.getStringCellValue());

							response.add(dto);
						}

					}
				}

			}
			workbook.close();
			fileInputStream.close();

		} catch (Exception e) {

			e.printStackTrace();
		}
		return response;

	}

	@Override
	public StatusResponseDto writeInterestsExcelForBank(List<InterestsApprovalDto> interestsApprovalExcelList,
			String paymentDate, String dealName, Integer dealId, String originalPaymentDate) {

		logger.info("writeInterestsExcelForBank starts");

		StatusResponseDto response = new StatusResponseDto();
		String[] dateParts = paymentDate.split("-");

		String updatedDealName = dealName.replaceAll(" ", "_");

		String downloadUrl = "";

		String dateInRequiredFormate = paymentDate.replaceAll("-", "");

		Double totalInterestAmount = 0d;
		int count = 0;

		SimpleDateFormat formatter1 = new SimpleDateFormat("ddMMyy");
		Date date = new Date();
		String currentDate = formatter1.format(date);

		SimpleDateFormat formatter = new SimpleDateFormat("hhmm");
		Date d = new Date();
		String hourMint = formatter.format(d);

		StringBuilder sbf = new StringBuilder("OXYLR1_OXYLR1UPLD_");
		sbf.append(currentDate + "_" + dealId + "_" + hourMint);

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();// Create font
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 40; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		for (InterestsApprovalDto obj : interestsApprovalExcelList) {
			double remainingAmount = holdAmountService.remainingAmountAfterRemovingHoldAmount(obj.getUserId(), dealId,
					AmountType.INTEREST, obj.getInterestAmount());
			if (remainingAmount > 0.0) {
				User user = userRepo.findById(obj.getUserId()).get();
				logger.info("testUserType" + user.isTestUser());
				if (user.isTestUser() == false) {

					Row row1 = spreadSheet.createRow(++rowCount);
					Cell cell1 = row1.createCell(0);
					cell1.setCellValue("777705849442");

					cell1 = row1.createCell(1);
					cell1.setCellValue(obj.getBankAccountNumber());

					cell1 = row1.createCell(2);
					cell1.setCellValue(obj.getNameAsPerBank().toUpperCase().replaceAll("\\s{2,}", " "));

					cell1 = row1.createCell(3);
					cell1.setCellValue(remainingAmount);

					totalInterestAmount += remainingAmount;
					count++;

					cell1 = row1.createCell(4);
					cell1.setCellValue("N");

					int y = Integer.parseInt(dateParts[1]);

					String monthName12 = DateTime.now().withMonthOfYear(y).toString("MMM");

					String requiredDate = dateParts[0] + "-" + monthName12 + "-" + dateParts[2];

					cell1 = row1.createCell(5);
					cell1.setCellValue(requiredDate);

					cell1 = row1.createCell(6);
					cell1.setCellValue(obj.getIfscCode().toUpperCase());

					cell1 = row1.createCell(13);
					cell1.setCellValue(originalPaymentDate);

					cell1 = row1.createCell(15);
					cell1.setCellValue(obj.getDaysDifference() + " days");

					cell1 = row1.createCell(16);
					cell1.setCellValue("LENDERINTEREST");

					cell1 = row1.createCell(17);
					cell1.setCellValue(dealId);

					cell1 = row1.createCell(18);
					cell1.setCellValue("LR" + obj.getUserId());

					cell1 = row1.createCell(19);
					cell1.setCellValue("OXYLOANS " + dealName + " M" + dateParts[1]);

					String remarks = dealName + " Interest";

					cell1 = row1.createCell(20);
					cell1.setCellValue(remarks.replaceAll("\\s{2,}", " "));
				}
			}
		}

		File f = new File(iciciFilePathBeforeApproval + sbf.toString() + ".xls");

		try {
			outFile = new FileOutputStream(f);
			logger.info("file saved");
			logger.info(iciciFilePathBeforeApproval);
			workBook.write(outFile);
			response.setFileName(f.getName());
			workBook.close();
			outFile.close();

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(f.getName());
			fileRequest.setFileType(FileType.LenderInterestInfo);
			fileRequest.setFilePrifix(FileType.LenderInterestInfo.name());

			FileResponse putFile = fileManagementService.putFile(fileRequest);

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			downloadUrl = getFile.getDownloadUrl();

			response.setDownloadUrl(downloadUrl);
			response.setTotalAmount(totalInterestAmount);
			response.setCount(count);

			// sending mail starts
			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate1 = expectedDateFormat.format(current.getTime());
			templateContext.put("currentDate", expectedCurrentDate1);
			templateContext.put("$message", "");

			String mailSubject = dealName + " (" + dealId + ") IciciExcel";

			EmailRequest emailRequest = new EmailRequest(
					new String[] { "sudisridhar9@gmail.com", "archana.n@oxyloans.com", "ramadevi@oxyloans.com",
							"subbu@oxyloans.com" },
					"Interests-Approval.template", templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			}

			// sending mail ends

		} catch (Exception ex) {
			ex.getMessage();
		}

		logger.info("writeInterestsExcelForBank ends");

		return response;
	}

	@Override
	public StatusResponseDto updateInterestsInfoExcel(List<InterestsApprovalDto> interestsApprovalDto, File file) {

		StatusResponseDto response = new StatusResponseDto();
		try {
			FileInputStream fileInputStream = new FileInputStream(file);

			HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);

			HSSFSheet sheets = workbook.getSheetAt(0);

			int rowCount = sheets.getFirstRowNum();

			logger.info(rowCount);
			for (InterestsApprovalDto obj : interestsApprovalDto) {

				rowCount += 1;
				logger.info(rowCount);
				Row row1 = sheets.getRow(rowCount);

				if (row1 == null) {

					row1 = sheets.createRow(rowCount);
					logger.info("create");
				}

				Cell cell1 = row1.createCell(0);
				cell1.setCellValue(obj.getUserId());

				cell1 = row1.createCell(1);
				cell1.setCellValue(obj.getLenderName());

				cell1 = row1.createCell(2);
				cell1.setCellValue(obj.getParticipatedDate());

				cell1 = row1.createCell(3);
				cell1.setCellValue(obj.getGroupName());

				cell1 = row1.createCell(4);
				cell1.setCellValue(obj.getPaymentMethod());

				cell1 = row1.createCell(6);
				cell1.setCellValue(obj.getParticipatedAmount() == null ? 0d : obj.getParticipatedAmount());

				cell1 = row1.createCell(5);
				cell1.setCellValue(obj.getRateOfInterest());

				cell1 = row1.createCell(7);
				cell1.setCellValue(obj.getInterestAmount());

				cell1 = row1.createCell(8);
				cell1.setCellValue(obj.getBankAccountNumber());

				cell1 = row1.createCell(9);
				cell1.setCellValue(obj.getIfscCode());

				cell1 = row1.createCell(10);
				cell1.setCellValue(obj.getNameAsPerBank());

				cell1 = row1.createCell(11);
				cell1.setCellValue(obj.getBankName());

				cell1 = row1.createCell(12);
				cell1.setCellValue(obj.getBranchName());

			}

			fileInputStream.close();
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			workbook.write(fileOutputStream);
			response.setStatus(file.getName() + "updated");
			fileOutputStream.close();
			workbook.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return response;
	}

	public List<IciciExcelInterestsFiles> readICICReturnedExcel(File file, int value) {
		logger.info("readICICReturnedExcel method start");
		List<IciciExcelInterestsFiles> responseList = new ArrayList<IciciExcelInterestsFiles>();

		// by default value is 0 because some times the file formats will be changed we
		// get text/corrupted xls format(value=1) or pure xls format(value=0)

		File updatedFile = file;
		if (value == 1) {
			updatedFile = convertTextToXLS(file); // converting the text or corrupted xls format to pure xls
		}

		if (updatedFile != null) {
			if (updatedFile.exists()) {
				String fileName = updatedFile.getName();

				logger.info(fileName);

				DataFormatter Stringform = new DataFormatter(); // excel datatype formatter

				Workbook workbook = null;
				try {
					FileInputStream fileInputStream = new FileInputStream(updatedFile);

					FormulaEvaluator formulaEvaluator = null;

					// we may also get in the xls or xlsx format
					if (fileName.endsWith(".xls")) { // correct format
						try {
							workbook = new HSSFWorkbook(fileInputStream);
						} catch (Exception e) { // corrupt/text format
							System.out.println(e.getMessage());
							if (e.getMessage().contains("Your file appears not to be a valid OLE2 document")) {
								// by using recursion we call the same function but with value=1
								responseList = readICICReturnedExcel(updatedFile, 1);
							}
						}
						formulaEvaluator = new HSSFFormulaEvaluator((HSSFWorkbook) workbook);

					} else if (fileName.endsWith(".xlsx")) {
						try {
							workbook = new XSSFWorkbook(fileInputStream); // getting error in this line
						} catch (Exception e) {
							System.out.println(e.getMessage());
							// by using recursion we call the same function but with value=1
							if (e.getMessage().contains("Your file appears not to be a valid OLE2 document")) {
								responseList = readICICReturnedExcel(updatedFile, 1);
							}

						}
						formulaEvaluator = new XSSFFormulaEvaluator((XSSFWorkbook) workbook); // formula evaluator
																								// converts number to
																								// string
					}

					if (workbook != null) {

						Iterator<Sheet> sheets = workbook.iterator();

						while (sheets.hasNext()) {
							Sheet sh = sheets.next();

							Iterator<Row> rowIterator = sh.iterator();

							while (rowIterator.hasNext()) {
								Row row = rowIterator.next();

								if (isRowEmpty(row)) {
									continue;
								}

								if (row.getRowNum() != 0) {
									// reading the cells with the cell index values
									Cell debitAccount = row.getCell(0);

									formulaEvaluator.evaluate(row.getCell(0)); // Returns string
									String debitAccountStr = Stringform.formatCellValue(debitAccount, formulaEvaluator);

									Cell accountNumberCell = row.getCell(1);

									Cell lenderName = row.getCell(2);

									Cell debitAmountCell = row.getCell(3);

									formulaEvaluator.evaluate(row.getCell(0)); // Returns string
									String debitAmountStr = Stringform.formatCellValue(debitAmountCell,
											formulaEvaluator);

									Cell payMode = row.getCell(4);

									Cell amountPaidDate = row.getCell(5);

									Cell lenderIfsc = row.getCell(6);

									Cell originalPaymentDate = row.getCell(13); // aka ben Add3

									Cell daysdifferenceCell = row.getCell(15);

									Cell amountType = row.getCell(16);

									Cell dealIdCell = row.getCell(17);

									formulaEvaluator.evaluate(row.getCell(0)); // Returns string
									String dealIdStr = Stringform.formatCellValue(dealIdCell, formulaEvaluator);

									Cell lenderId = row.getCell(18);

									// Cell amountPaidBorrower = row.getCell(13);

									Cell remarks = row.getCell(20);

									Cell status = row.getCell(22);

									Cell creditedOn = row.getCell(27);

									Cell addDetails2 = row.getCell(16);

									Cell addDetails5 = row.getCell(19);

									Cell addDetails1 = row.getCell(15);

									if (status != null) {

										logger.info("payment Status: " + status.getStringCellValue());

										if (status.getStringCellValue().replaceAll(" ", "").equals("Paid")) {

											int dealId = 0;
											IciciExcelInterestsFiles iciciExcelInterestsFiles = new IciciExcelInterestsFiles();

											iciciExcelInterestsFiles.setDebitAccount(debitAccountStr);

											iciciExcelInterestsFiles
													.setAccountNumber(accountNumberCell.getStringCellValue() != null
															? accountNumberCell.getStringCellValue()
															: " ");

											if (daysdifferenceCell.getCellType() == Cell.CELL_TYPE_STRING) {

												iciciExcelInterestsFiles.setDaysDifference(
														daysdifferenceCell.getStringCellValue() != null
																? daysdifferenceCell.getStringCellValue()
																: "");

											}

											iciciExcelInterestsFiles
													.setLenderName(lenderName.getStringCellValue() != null
															? lenderName.getStringCellValue()
															: " ");

											iciciExcelInterestsFiles
													.setAmountType(amountType.getStringCellValue() != null
															? amountType.getStringCellValue().trim()
															: "");

											iciciExcelInterestsFiles.setOriginalPaymentDate((originalPaymentDate != null
													&& originalPaymentDate.getStringCellValue() != null)
															? originalPaymentDate.getStringCellValue()
															: "");

											iciciExcelInterestsFiles.setAmount(Double.parseDouble(debitAmountStr));
											iciciExcelInterestsFiles.setPayMode(
													payMode.getStringCellValue() != null ? payMode.getStringCellValue()
															: " ");
											iciciExcelInterestsFiles.setAmountpaidDate(amountPaidDate.toString());
											iciciExcelInterestsFiles.setIfscCode(lenderIfsc.getStringCellValue() != null

													? lenderIfsc.getStringCellValue()
													: " ");
											iciciExcelInterestsFiles.setLenderId(lenderId.getStringCellValue() != null
													? lenderId.getStringCellValue()
													: " ");
											iciciExcelInterestsFiles.setRemarks(
													remarks.getStringCellValue() != null ? remarks.getStringCellValue()
															: " ");
											iciciExcelInterestsFiles.setStatus(
													status.getStringCellValue() != null ? status.getStringCellValue()
															: " ");

											iciciExcelInterestsFiles.setCreditedOn(creditedOn.getStringCellValue());

											iciciExcelInterestsFiles.setAddDetails2(addDetails2.getStringCellValue());

											if (addDetails5.getCellType() == Cell.CELL_TYPE_NUMERIC) {
												iciciExcelInterestsFiles
														.setAddDetails5((int) addDetails5.getNumericCellValue());
											} else if (addDetails5.getCellType() == Cell.CELL_TYPE_STRING) {
												String str = addDetails5.getStringCellValue();

												if (NumberUtils.isDigits(str)) {
													iciciExcelInterestsFiles.setAddDetails5(Integer.valueOf(str));
												}

											}

											if (!amountType.getStringCellValue().trim().replaceAll("\\s+", " ")
													.equalsIgnoreCase("LENDERREFERRALBONUS")) {

												if (addDetails1 != null) {
													if (addDetails1.getCellType() == Cell.CELL_TYPE_NUMERIC) {
														iciciExcelInterestsFiles.setAddDetails1(
																(int) addDetails1.getNumericCellValue());
													} else if (addDetails1.getCellType() == Cell.CELL_TYPE_STRING) {
														String str = addDetails1.getStringCellValue();

														if (NumberUtils.isDigits(str)) {
															iciciExcelInterestsFiles
																	.setAddDetails1(Integer.valueOf(str));
														}

													}
												} else {
													iciciExcelInterestsFiles.setAddDetails1(0);
												}

												if (!dealIdStr.equalsIgnoreCase("")
														&& NumberUtils.isNumber(dealIdStr)) {
													dealId = Integer.valueOf(dealIdStr);
												}
												iciciExcelInterestsFiles.setDealId(dealId);
											}

											if (amountType.getStringCellValue().trim().replaceAll("\\s+", " ")
													.equalsIgnoreCase("LENDERREFERRALBONUS")) {

												ListMultimap<Integer, Integer> refereeIdDealIdMap = ArrayListMultimap
														.create();
												StringBuilder refereeIds2 = new StringBuilder();// refereeId,DealId //
																								// loop

												if (addDetails1 != null) {
													if (addDetails1.getCellType() == Cell.CELL_TYPE_NUMERIC) {
														iciciExcelInterestsFiles.setAddDetails1(
																(int) addDetails1.getNumericCellValue());
													} else if (addDetails1.getCellType() == Cell.CELL_TYPE_STRING
															&& !addDetails1.getStringCellValue().trim()
																	.equalsIgnoreCase("NULL")) {
														String str = addDetails1.getStringCellValue();
														String[] refereeIdsArray = str.split("\\s+"); // Split // by//
																										// whitespace

														for (String refereeIdPair : refereeIdsArray) {
															refereeIdPair = refereeIdPair.trim();

															if (refereeIdPair.startsWith("[")
																	&& refereeIdPair.endsWith("]")) {
																String content = refereeIdPair.substring(1,
																		refereeIdPair.length() - 1);
																String[] values = content.split(",");

																if (values.length == 2) {
																	try {
																		int refereeId = Integer
																				.parseInt(values[0].trim());
																		int dealId1 = Integer
																				.parseInt(values[1].trim());

																		refereeIdDealIdMap.put(refereeId, dealId1);
																	} catch (NumberFormatException e) {
																		logger.error(e.getMessage(), e);
																	}
																}
															}
														}
														for (Map.Entry<Integer, Integer> entry : refereeIdDealIdMap
																.entries()) {
															int refereeId1 = entry.getKey();
															int dealId2 = entry.getValue();

															logger.info(
																	"refereeId :" + refereeId1 + "dealId :" + dealId2);

															if (refereeIds2.length() > 0) {
																refereeIds2.append(" ");
															}
															refereeIds2.append("[").append(refereeId1).append(",")
																	.append(dealId2).append("]");
														}
														iciciExcelInterestsFiles.setAddDetailsI1(
																refereeIds2.length() > 0 ? refereeIds2.toString() : "");
													}
												} else {
													iciciExcelInterestsFiles.setAddDetails1(0);
												}

												StringBuilder str1 = new StringBuilder();// tableIds loop
												if (!dealIdStr.equalsIgnoreCase("")
														&& NumberUtils.isNumber(dealIdStr)) {
													dealId = Integer.valueOf(dealIdStr);
													logger.info("dealId :" + dealId);

													// iciciExcelInterestsFiles.setDealId(dealId);
													iciciExcelInterestsFiles.setDealIds(dealIdStr);

												} else if (!dealIdStr.equalsIgnoreCase("")
														&& !dealIdStr.equalsIgnoreCase("NULL")) {
													String[] dealIdValues = dealIdStr.split(",\\s*");
													for (String id : dealIdValues) {
														int dealid2 = Integer.parseInt(id.trim());

														if (str1.length() > 0) {
															str1.append(",");
														}
														str1.append(dealid2);
													}
													String dealIds1 = str1.toString();
													iciciExcelInterestsFiles.setDealIds(dealIds1);
													logger.info("tableIds :" + dealIds1);

												} else {
													iciciExcelInterestsFiles.setDealIds("");
												}
											}
											responseList.add(iciciExcelInterestsFiles);
										}
									}

								}

							}
						}
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} else

		{
			logger.info("updated file  doesnot exist");
		}
		logger.info(responseList);
		return responseList;

	}

	private static boolean isRowEmpty(Row row) {
		boolean isEmpty = true;
		DataFormatter dataFormatter = new DataFormatter();

		if (row != null) {
			for (Cell cell : row) {
				if (dataFormatter.formatCellValue(cell).trim().length() > 0) {
					isEmpty = false;
					break;
				}
			}
		}

		return isEmpty;
	}

	private File convertTextToXLS(File file) {

		logger.info("file conversion starts");
		File updatedFile = null;

		ArrayList<ArrayList<String>> allRowAndColData = null;
		ArrayList<String> oneRowData = null;

		String fileName = file.getName();

		String currentLine;
		try {
			FileInputStream fis = new FileInputStream(file);
			DataInputStream myInput = new DataInputStream(fis);

			int i = 0;
			allRowAndColData = new ArrayList<ArrayList<String>>();

			while ((currentLine = myInput.readLine()) != null) {
				oneRowData = new ArrayList<String>();

				String oneRowArray[] = currentLine.split("\\s{2,2}");
				for (int j = 0; j < oneRowArray.length; j++) {

					oneRowData.add(oneRowArray[j]);

				}
				allRowAndColData.add(oneRowData);

				i++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			HSSFWorkbook workBook = new HSSFWorkbook();
			HSSFSheet sheet = workBook.createSheet("sheet1");
			for (int i = 0; i < allRowAndColData.size(); i++) {
				ArrayList<?> ardata = (ArrayList<?>) allRowAndColData.get(i);
				HSSFRow row = sheet.createRow((short) 0 + i);
				for (int k = 0; k < ardata.size(); k++) {

					HSSFCell cell = row.createCell((short) k);
					cell.setCellValue(ardata.get(k).toString());
				}

			}
			updatedFile = new File(iciciReportsUpdateFolder + fileName.replace(".xls", "updated") + ".xls");
			FileOutputStream fileOutputStream = new FileOutputStream(updatedFile);
			workBook.write(fileOutputStream);
			logger.info("file converted");
			fileOutputStream.close();
		} catch (Exception ex) {
		}
		logger.info("file conversion ends");
		return updatedFile;

	}

	@Override
	public void writeBorrowerPaymentsExcel(BorrowerPaymentsDto obj, String date) {

		StatusResponseDto response = new StatusResponseDto();
		String[] dateParts = date.split("/");

		String dateInRequiredFormate = date.replaceAll("/", "");
		StringBuilder sbf = new StringBuilder("P");
		sbf.append(dateInRequiredFormate + "_" + obj.getBorrowerId());

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();// Create font
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 40; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		Row row1 = spreadSheet.createRow(++rowCount);
		Cell cell1 = row1.createCell(0);
		cell1.setCellValue("777705849442");

		cell1 = row1.createCell(1);
		cell1.setCellValue(obj.getAccountNumber());

		cell1 = row1.createCell(2);
		cell1.setCellValue(obj.getAccountName());

		cell1 = row1.createCell(3);
		cell1.setCellValue(obj.getAmount());

		cell1 = row1.createCell(4);
		cell1.setCellValue("N");

		int y = Integer.parseInt(dateParts[1]);

		String monthName12 = DateTime.now().withMonthOfYear(y).toString("MMM");

		String requiredDate = dateParts[0] + "-" + monthName12 + "-" + dateParts[2];

		cell1 = row1.createCell(5);
		cell1.setCellValue(requiredDate);

		cell1 = row1.createCell(6);
		cell1.setCellValue(obj.getIfscCode());

		cell1 = row1.createCell(18);
		cell1.setCellValue(obj.getBorrowerId());

		cell1 = row1.createCell(19);
		cell1.setCellValue("loan");

		cell1 = row1.createCell(20);
		cell1.setCellValue(obj.getRemarks());

		File f = new File(iciciFilePathBeforeApproval + sbf.toString() + ".xls");

		try {
			outFile = new FileOutputStream(f);

			TemplateContext templateContext = new TemplateContext();
			templateContext.put("message", "borrower loans list");
			templateContext.put("date", dateFormate.format(new Date()));

			String mailSubject = "Borrower loan list";
			EmailRequest emailRequest = new EmailRequest(
					new String[] { "archana.n@oxyloans.com", "subbu@oxyloans.com" }, "Interests-Approval.template",
					templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			}

			logger.info("file saved");
			logger.info(iciciFilePathBeforeApproval);
			workBook.write(outFile);
			response.setStatus(f.getName() + "saved in after approvals");
			workBook.close();
			outFile.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public String generateUtmsExcel(List<UserResponse> userResponseList, String utm) {

		FileOutputStream outFile = null;

		String downloadUrl = "";

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(utm);
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();// Create font
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("user_id");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("user_name");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("email");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Status");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Registered_On");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("UTM");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Pan");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Dob");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Address");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Account No.");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bank");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Name as per Bank");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Primary Type");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 40; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		for (UserResponse obj : userResponseList) {

			Row row1 = spreadSheet.createRow(++rowCount);
			Cell cell1 = row1.createCell(0);
			cell1.setCellValue(obj.getId());

			cell1 = row1.createCell(1);
			cell1.setCellValue(obj.getName());

			cell1 = row1.createCell(2);
			cell1.setCellValue(obj.getEmail());

			cell1 = row1.createCell(3);
			cell1.setCellValue(obj.getMobileNumber());

			cell1 = row1.createCell(4);
			cell1.setCellValue(obj.getStatus());

			cell1 = row1.createCell(5);
			cell1.setCellValue(obj.getRegisteredOn());

			cell1 = row1.createCell(6);
			cell1.setCellValue(obj.getUtmSource());

			cell1 = row1.createCell(7);
			cell1.setCellValue(obj.getPanNumber());

			cell1 = row1.createCell(8);
			cell1.setCellValue(obj.getDob());

			cell1 = row1.createCell(9);
			cell1.setCellValue(obj.getAddress());

			cell1 = row1.createCell(10);
			cell1.setCellValue(obj.getAccountNumber());

			cell1 = row1.createCell(11);
			cell1.setCellValue(obj.getBankName());

			cell1 = row1.createCell(12);
			cell1.setCellValue(obj.getBankAddress());

			cell1 = row1.createCell(13);
			cell1.setCellValue(obj.getPrimaryType());

		}

		String filePath = iciciFilePathBeforeApproval + utm + LocalDate.now() + ".xls";

		File f = new File(filePath);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(f);
			if (f.exists()) {
				// response.setFileStatus("File saved");
			}

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(f.getName());
			fileRequest.setFileType(FileType.UTM);
			fileRequest.setFilePrifix(FileType.UTM.name());

			FileResponse putFile = fileManagementService.putFile(fileRequest);
			logger.info("TransactionResponses file {} uploaded to as {}", filePath, putFile.getFileName());

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			downloadUrl = getFile.getDownloadUrl();

		} catch (Exception ex) {

		}
		return downloadUrl;

	}

	@Override
	public String generateCurrentMonthTransactionAlertsExcel(List<BankTransactionsBreakUpDto> breakUpList,
			String transactionType) {

		logger.info("generateCurrentMonthTransactionAlertsExcel starts");

		FileOutputStream outFile = null;

		String downloadUrl = "";

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(transactionType);
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();// Create font
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Acc No.");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Transaction Date");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Amount");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Transaction Type");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 40; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		for (BankTransactionsBreakUpDto obj : breakUpList) {

			Row row1 = spreadSheet.createRow(++rowCount);
			Cell cell1 = row1.createCell(0);
			cell1.setCellValue(obj.getAccountNumber());

			cell1 = row1.createCell(1);
			cell1.setCellValue(obj.getTransactionDate());

			cell1 = row1.createCell(2);
			cell1.setCellValue(obj.getAmount());

			cell1 = row1.createCell(3);
			cell1.setCellValue(obj.getTransactionType());

		}

		String filePath = iciciFilePathBeforeApproval + transactionType + LocalDate.now() + ".xls";

		File f = new File(filePath);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(f);
			logger.info("file saved");
			if (f.exists()) {
				// response.setFileStatus("File saved");
			}

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(f.getName());
			fileRequest.setFileType(FileType.TransactionAlerts);
			fileRequest.setFilePrifix(FileType.TransactionAlerts.name());

			FileResponse putFile = fileManagementService.putFile(fileRequest);
			logger.info("TransactionResponses file {} uploaded to as {}", filePath, putFile.getFileName());

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			downloadUrl = getFile.getDownloadUrl();

		} catch (Exception ex) {

		}

		logger.info("generateCurrentMonthTransactionAlertsExcel ends");

		return downloadUrl;

	}

	@Override
	public StatusResponseDto writeNewCmsExcelForInterests(List<InterestsApprovalDto> breakupList, String paymentDate,
			String dealName, Integer dealId) {

		logger.info("file creation starts main");

		dealName = dealName.replaceAll(" ", "");
		dealName = dealName.replaceAll("[^a-zA-Z0-9]", "_");

		StatusResponseDto response = new StatusResponseDto();
		String[] dateParts = paymentDate.split("-");

		String updatedDealName = dealName.replaceAll("_", " ");

		String dateInRequiredFormate = paymentDate.replaceAll("-", "");

		StringBuilder sbf = new StringBuilder("OXYLR1_OXYLR1UPLD_");

		sbf.append(dateInRequiredFormate + "_" + dealId + "_" + dealName);

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		Double totalAmount = 0d;

		Set<Integer> set = new TreeSet<>();

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();// Create font
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 40; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		for (InterestsApprovalDto obj : breakupList) {

			if ((obj.getPaymentsAdminApproval().equalsIgnoreCase("approved")
					&& obj.getSubbuAdminApproval().equalsIgnoreCase("approved")
					&& obj.getSuperAdminApproval().equalsIgnoreCase("approved"))) {

				Row row1 = spreadSheet.createRow(++rowCount);
				Cell cell1 = row1.createCell(0);
				cell1.setCellValue("777705849442");

				cell1 = row1.createCell(1);
				cell1.setCellValue(obj.getBankAccountNumber());

				cell1 = row1.createCell(2);
				cell1.setCellValue(obj.getNameAsPerBank().toUpperCase().replaceAll("\\s{2,}", " "));

				cell1 = row1.createCell(3);
				cell1.setCellValue(obj.getInterestAmount());

				totalAmount += obj.getInterestAmount();

				set.add(obj.getUserId());

				cell1 = row1.createCell(4);
				cell1.setCellValue("N");

				int y = Integer.parseInt(dateParts[1]);

				String monthName12 = DateTime.now().withMonthOfYear(y).toString("MMM");

				String requiredDate = dateParts[0] + "-" + monthName12 + "-" + dateParts[2];

				cell1 = row1.createCell(5);
				cell1.setCellValue(requiredDate);

				cell1 = row1.createCell(6);
				cell1.setCellValue(obj.getIfscCode().toUpperCase());

				cell1 = row1.createCell(15);
				cell1.setCellValue(obj.getDaysDifference() + " days");

				cell1 = row1.createCell(16);
				cell1.setCellValue("LENDERINTEREST");

				cell1 = row1.createCell(17);
				cell1.setCellValue(dealId);

				cell1 = row1.createCell(18);
				cell1.setCellValue("LR" + obj.getUserId());

				cell1 = row1.createCell(19);
				cell1.setCellValue("OXYLOANS " + updatedDealName + " M" + dateParts[1]);

				cell1 = row1.createCell(20);
				cell1.setCellValue(updatedDealName + " Interest".replaceAll("\\s{2,}", " "));

			}
		}

		File f = new File(iciciFilePathBeforeApproval + sbf.toString() + ".xls");

		try {
			outFile = new FileOutputStream(f);
			logger.info("file saved");
			logger.info(iciciFilePathBeforeApproval);
			workBook.write(outFile);
			response.setFileName(f.getName());
			response.setTotalAmount(totalAmount);
			response.setCount(set.size());
			workBook.close();
			outFile.close();

			FileInputStream targetStream = new FileInputStream(f);

			FileRequest fileRequest = new FileRequest();

			fileRequest.setFilePrifix(FileType.CMSFILE.name());
			fileRequest.setInputStream(targetStream);
			fileRequest.setFileName(f.getName());
			FileResponse putFile = fileManagementService.putFile(fileRequest);

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			response.setDownloadUrl(getFile.getDownloadUrl());

			logger.info(getFile.getDownloadUrl());

			// sending mail starts
			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate1 = expectedDateFormat.format(current.getTime());
			templateContext.put("currentDate", expectedCurrentDate1);
			templateContext.put("$message", "");

			String mailSubject = dealName + " IciciExcel";

			EmailRequest emailRequest = new EmailRequest(
					new String[] { "liveen@oxyloans.com", "archana.n@oxyloans.com", "ramadevi@oxyloans.com",
							"subbu@oxyloans.com" },
					"Interests-Approval.template", templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			}

			// sending mail ends

		} catch (Exception ex) {
			ex.getMessage();
		}

		logger.info("file creation ends main");

		return response;

		// return response;
	}

	@Override
	public StatusResponseDto witeDealLevelLoanDisbursmentExcel(String paymentDate, UserRequest userRequest,
			OxyCmsLoans obj, String fileType) {

		StatusResponseDto response = new StatusResponseDto();
		String[] dateParts = paymentDate.split("-");

		int dealId = obj.getDealId();
		String updatedDealName = null;
		if (dealId != 0) {
			String dealName = oxyBorrowersDealsInformationRepo.findingDealNameBasedOnId(dealId);

			updatedDealName = dealName.replaceAll("_", " ");
		} else {
			updatedDealName = "PARTNER";
		}

		String dateInRequiredFormate = paymentDate.replaceAll("-", "");
		StringBuilder sbf = new StringBuilder("OXYLR2_OXYLR2UPLD_");

		sbf.append(dateInRequiredFormate + "_" + obj.getId() + "_" + dealId + "_" + "BR" + obj.getUserId() + "_LOAN");

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		Double totalAmount = 0d;

		Set<Integer> set = new TreeSet<>();

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();// Create font
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 40; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		Row row1 = spreadSheet.createRow(++rowCount);
		Cell cell1 = row1.createCell(0);
		cell1.setCellValue("777705849441");

		cell1 = row1.createCell(1);
		cell1.setCellValue(userRequest.getAccountNumber());

		cell1 = row1.createCell(2);
		cell1.setCellValue(userRequest.getNameAsPerBank().toUpperCase().replaceAll("\\s{2,}", " "));

		cell1 = row1.createCell(3);
		cell1.setCellValue(obj.getAmount());

		totalAmount += obj.getAmount();

		set.add(obj.getUserId());

		cell1 = row1.createCell(4);
		cell1.setCellValue("N");

		int y = Integer.parseInt(dateParts[1]);

		String monthName12 = DateTime.now().withMonthOfYear(y).toString("MMM");

		String requiredDate = dateParts[0] + "-" + monthName12 + "-" + dateParts[2];

		cell1 = row1.createCell(5);
		cell1.setCellValue(requiredDate);

		cell1 = row1.createCell(6);
		cell1.setCellValue(userRequest.getIfscCode().toUpperCase());

		cell1 = row1.createCell(16);
		cell1.setCellValue("BORROWERLOAN");

		cell1 = row1.createCell(17);
		cell1.setCellValue(dealId);

		cell1 = row1.createCell(18);
		cell1.setCellValue("BR" + obj.getUserId());

		cell1 = row1.createCell(19);
		cell1.setCellValue("OXYLOANS " + "BORROWERLOAN");

		cell1 = row1.createCell(20);
		cell1.setCellValue(updatedDealName + " BorrowerLoan");
		File f = null;
		if (fileType.equals("Partner")) {
			f = new File(iciciFilePathBeforeApprovalForPartner + sbf.toString() + ".xls");
		} else {
			f = new File(iciciFilePathBeforeApprovalForPartner + sbf.toString() + ".xls");
		}
		try {
			outFile = new FileOutputStream(f);
			logger.info("file saved");
			logger.info(iciciFilePathBeforeApproval);
			workBook.write(outFile);
			response.setFileName(f.getName());
			response.setTotalAmount(totalAmount);
			response.setCount(set.size());
			workBook.close();
			outFile.close();

			FileInputStream targetStream = new FileInputStream(f);

			FileRequest fileRequest = new FileRequest();

			fileRequest.setFilePrifix(FileType.CMSFILE.name());
			fileRequest.setInputStream(targetStream);
			fileRequest.setFileName(f.getName());
			FileResponse putFile = fileManagementService.putFile(fileRequest);

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			response.setDownloadUrl(getFile.getDownloadUrl());

			logger.info(getFile.getDownloadUrl());

			// sending mail starts
			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate1 = expectedDateFormat.format(current.getTime());
			templateContext.put("currentDate", expectedCurrentDate1);
			templateContext.put("$message", "");

			String mailSubject = "BorrowerLoan(BR" + obj.getUserId() + ")" + " IciciExcel";

			EmailRequest emailRequest = new EmailRequest(
					new String[] { "liveen@oxyloans.com", "narendra@oxyloans.com", "archana.n@oxyloans.com",
							"subbu@oxyloans.com" },
					"Interests-Approval.template", templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			}

			// sending mail ends

		} catch (Exception ex) {
			ex.getMessage();
		}

		return response;

		// return response;
	}

	@Override
	public List<String> getMobileNumbersFromExcel(File file) {

		List<String> mobileNumbersList = new ArrayList<>();

		logger.info(file.getName());

		try {
			Workbook workbook = null;

			FileInputStream fileInputStream = new FileInputStream(file);
			if (file.getName().endsWith(".xlsx")) {
				workbook = new XSSFWorkbook(fileInputStream);
			} else if (file.getName().endsWith(".xls")) {
				workbook = new HSSFWorkbook(fileInputStream);
			}

			DataFormatter formatter = new DataFormatter();

			Iterator<Sheet> sheets = workbook.iterator();

			while (sheets.hasNext()) {
				Sheet sh = sheets.next();

				Iterator<Row> rowIterator = sh.iterator();

				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();

					if (row.getRowNum() != 0) {

						Cell mobileNumber = row.getCell(0);

						mobileNumbersList.add("91" + formatter.formatCellValue(mobileNumber));

					}
				}

			}
		}

		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mobileNumbersList;
	}

	@Override
	public StatusResponseDto witeCmsExcel(String fileName, IciciCmsRequestDto obj, String paymentDate,
			String fileType) {

		StatusResponseDto response = new StatusResponseDto();
		String[] dateParts = paymentDate.split("-");

		int dealId = obj.getDealId();

		String dateInRequiredFormate = paymentDate.replaceAll("-", "");
		StringBuilder sbf = new StringBuilder(fileName);

		sbf.append(dateInRequiredFormate + "_" + obj.getPaymentType().toLowerCase() + "_" + obj.getUserUserId());

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		Double totalAmount = 0d;

		Set<Integer> set = new TreeSet<>();

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();// Create font
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 40; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		Row row1 = spreadSheet.createRow(++rowCount);
		Cell cell1 = row1.createCell(0);
		cell1.setCellValue(obj.getDebitAccountNumber());

		cell1 = row1.createCell(1);
		cell1.setCellValue(obj.getAccountNumber());

		cell1 = row1.createCell(2);
		cell1.setCellValue(obj.getName().toUpperCase().replaceAll("\\s{2,}", " "));

		cell1 = row1.createCell(3);
		cell1.setCellValue(obj.getAmount());

		totalAmount += obj.getAmount();

		set.add(obj.getUserId());

		cell1 = row1.createCell(4);
		cell1.setCellValue("N");

		int y = Integer.parseInt(dateParts[1]);

		String monthName12 = DateTime.now().withMonthOfYear(y).toString("MMM");

		String requiredDate = dateParts[0] + "-" + monthName12 + "-" + dateParts[2];

		cell1 = row1.createCell(5);
		cell1.setCellValue(requiredDate);

		cell1 = row1.createCell(6);
		cell1.setCellValue(obj.getIfscCode().toUpperCase());

		cell1 = row1.createCell(16);
		cell1.setCellValue(obj.getPaymentType());

		cell1 = row1.createCell(17);
		cell1.setCellValue(dealId);

		cell1 = row1.createCell(18);
		cell1.setCellValue(obj.getUserUserId());

		cell1 = row1.createCell(19);
		cell1.setCellValue(obj.getAddDetails5());

		cell1 = row1.createCell(20);
		cell1.setCellValue(obj.getRemarks());
		File f = null;
		if (fileType.equals("Partner")) {
			f = new File(iciciFilePathBeforeApprovalForPartner + sbf.toString() + ".xls");
		} else {
			f = new File(iciciFilePathBeforeApproval + sbf.toString() + ".xls");
		}
		try {
			outFile = new FileOutputStream(f);
			logger.info("file saved");
			logger.info(iciciFilePathBeforeApproval);
			workBook.write(outFile);
			response.setFileName(f.getName());
			response.setTotalAmount(totalAmount);
			response.setCount(set.size());
			workBook.close();
			outFile.close();

			FileInputStream targetStream = new FileInputStream(f);

			FileRequest fileRequest = new FileRequest();

			fileRequest.setFilePrifix(FileType.CMSFILE.name());
			fileRequest.setInputStream(targetStream);
			fileRequest.setFileName(f.getName());
			FileResponse putFile = fileManagementService.putFile(fileRequest);

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			response.setDownloadUrl(getFile.getDownloadUrl());

			logger.info(getFile.getDownloadUrl());

			// sending mail starts
			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate1 = expectedDateFormat.format(current.getTime());
			templateContext.put("currentDate", expectedCurrentDate1);
			templateContext.put("$message", "");

			String mailSubject = "BorrowerFee(" + obj.getUserUserId() + ")" + " IciciExcel";

			EmailRequest emailRequest = new EmailRequest(
					new String[] { "liveen@oxyloans.com", "narendra@oxyloans.com", "subbu@oxyloans.com" },
					"Interests-Approval.template", templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			}

			// sending mail ends

		} catch (Exception ex) {
			ex.getMessage();
		}

		return response;

		// return response;
	}

	@Override
	public StatusResponseDto writePrinicalToWalletExcel(int dealId,
			List<LenderPaticipationRequestDto> listOflenderPaticipationRequestDto, String paymentDate,
			String debitAccountNo) {

		StatusResponseDto response = new StatusResponseDto();
		String[] dateParts = paymentDate.split("-");

		Double totalAmount = 0d;

		String dealName = oxyBorrowersDealsInformationRepo.findingDealNameBasedOnId(dealId);

		dealName = dealName.replace(" ", "_");
		String dateAsString = new SimpleDateFormat("ddMMYYYYhhmmss").format(new Date());

		String updatedDealName = dealName.replaceAll("_", " ");

		String dateInRequiredFormate = paymentDate.replaceAll("-", "");
		StringBuilder sbf = new StringBuilder();
		if (debitAccountNo.equalsIgnoreCase("777705849441")) {
			sbf.append("OXYLR2_OXYLR2UPLD_");

		} else {
			sbf.append("OXYLR1_OXYLR1UPLD_");

		}
		sbf.append(dateInRequiredFormate + "_" + dealId + "_" + dealName + "_P2W_" + dateAsString);

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		Set<Integer> set = new TreeSet<>();

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();// Create font
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 40; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		for (LenderPaticipationRequestDto obj : listOflenderPaticipationRequestDto) {
			User user = userRepo.findById(obj.getUserId()).get();
			logger.info("testUserType" + user.isTestUser());
			if (user.isTestUser() == false) {

				Row row1 = spreadSheet.createRow(++rowCount);
				Cell cell1 = row1.createCell(0);
				cell1.setCellValue(debitAccountNo);

				cell1 = row1.createCell(1);
				cell1.setCellValue(obj.getAccountNumber());

				cell1 = row1.createCell(2);
				cell1.setCellValue(obj.getUserName());

				cell1 = row1.createCell(3);
				cell1.setCellValue(obj.getMovingToWallet());

				totalAmount += obj.getMovingToWallet();

				set.add(obj.getUserId());

				cell1 = row1.createCell(4);
				cell1.setCellValue("N");

				int y = Integer.parseInt(dateParts[1]);

				String monthName12 = DateTime.now().withMonthOfYear(y).toString("MMM");

				String requiredDate = dateParts[0] + "-" + monthName12 + "-" + dateParts[2];

				cell1 = row1.createCell(5);
				cell1.setCellValue(requiredDate);

				cell1 = row1.createCell(6);
				cell1.setCellValue(obj.getIfscCode().toUpperCase());

				cell1 = row1.createCell(15);
				cell1.setCellValue(obj.getDaysDifference() + " days");

				cell1 = row1.createCell(16);
				cell1.setCellValue("PRINCIPALTOWALLET");

				cell1 = row1.createCell(17);
				cell1.setCellValue(dealId);

				cell1 = row1.createCell(18);
				cell1.setCellValue("LR" + obj.getUserId());

				cell1 = row1.createCell(19);
				cell1.setCellValue("OXYP2W" + dealId);

				cell1 = row1.createCell(20);
				cell1.setCellValue("Moved " + (int) obj.getMovingToWallet() + " from " + updatedDealName);
			}

		}

		File f = new File(iciciFilePathBeforeApproval + sbf.toString() + ".xls");

		try {
			outFile = new FileOutputStream(f);
			logger.info("file saved");
			logger.info(iciciFilePathBeforeApproval);
			workBook.write(outFile);
			response.setFileName(f.getName());
			response.setTotalAmount(totalAmount);
			response.setCount(set.size());
			workBook.close();
			outFile.close();

			// FileInputStream targetStream = new FileInputStream(f);

			/*
			 * FileRequest fileRequest = new FileRequest();
			 * 
			 * fileRequest.setFilePrifix(FileType.CMSFILE.name());
			 * fileRequest.setInputStream(targetStream);
			 * fileRequest.setFileName(f.getName()); FileResponse putFile =
			 * fileManagementService.putFile(fileRequest);
			 * 
			 * FileResponse getFile = fileManagementService.getFile(fileRequest);
			 * 
			 * response.setDownloadUrl(getFile.getDownloadUrl());
			 * 
			 * logger.info(getFile.getDownloadUrl());
			 */
			// sending mail starts
			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate1 = expectedDateFormat.format(current.getTime());
			templateContext.put("currentDate", expectedCurrentDate1);
			templateContext.put("$message", "");

			response.setTotalAmount(totalAmount);

			String mailSubject = dealName + " IciciExcel";

			EmailRequest emailRequest = new EmailRequest(
					new String[] { "archana.n@oxyloans.com", "subbu@oxyloans.com" }, "Interests-Approval.template",
					templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			}

			// sending mail ends

		} catch (Exception ex) {
			ex.getMessage();
		}
		return response;

	}

	@Override
	public List<RejectedFileDto> readRejectedFiles(File file) {

		List<RejectedFileDto> response = new ArrayList<>();
		File updatedFile = file;

		if (updatedFile != null) {
			if (updatedFile.exists()) {
				String fileName = updatedFile.getName();

				logger.info(fileName);

				Workbook workbook = null;
				try {
					FileInputStream fileInputStream = new FileInputStream(updatedFile);

					if (fileName.endsWith(".xls")) {

						workbook = new HSSFWorkbook(fileInputStream); // getting error in this line

					} else if (fileName.endsWith(".xlsx")) {
						workbook = new XSSFWorkbook(fileInputStream); // getting error in this line
					}

					Iterator<Sheet> sheets = workbook.iterator();

					while (sheets.hasNext()) {
						Sheet sh = sheets.next();

						Iterator<Row> rowIterator = sh.iterator();

						while (rowIterator.hasNext()) {
							Row row = rowIterator.next();

							if (isRowEmpty(row)) {
								continue;
							}

							Cell entryCell = row.getCell(0);

							Cell descriptionCell = row.getCell(1);

							Cell fileNameCell = row.getCell(2);

							String dealId = "";
							String lenderId = "";
							String amountType = "";
							String amountDate = "";
							String amount = "";

							RejectedFileDto dto = new RejectedFileDto();

							if (entryCell != null) {
								String[] arr = entryCell.toString().split("\\|");

								if (arr.length >= 20) {

									if (arr[17] != null) {
										dealId = arr[17];
									}
									if (arr[18] != null) {
										lenderId = arr[18];
									}

									if (arr[16] != null) {
										amountType = arr[16];
									}

									if (arr[5] != null) {
										amountDate = arr[5];
									}

									if (arr[3] != null) {
										amount = arr[3];
									}

								}

							}

							dto.setDeald(dealId);
							dto.setLenderId(lenderId);
							dto.setAmountType(amountType);
							dto.setAmountDate(amountDate);
							dto.setAmount(amount);

							System.out.println("dealId " + dealId);

							System.out.println("lenderId " + lenderId);

							System.out.println("amountType " + amountType);

							System.out.println("amountDate " + amountDate);

							System.out.println("amount :" + amount);

							if (descriptionCell != null) {

								dto.setDescription(
										descriptionCell.toString() != null ? descriptionCell.toString() : "");

							}

							if (fileNameCell != null) {
								dto.setFileName(fileNameCell.toString() != null ? fileNameCell.toString() : "");
							}

							logger.info(dto.toString());

							response.add(dto);

						}

					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		return response;
	}

	@Override
	public StatusResponseDto createQuarterlyReportsExcel(List<DealQuarterlyReportsResponse> responseList,
			DealQuarterlyReportsRequest request) {

		StatusResponseDto response = new StatusResponseDto();

		String fileName = "QUARTERLY_reports_" + request.getToMonth() + "_" + request.getToYear();
		StringBuilder sbf = new StringBuilder(fileName);

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(fileName);
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		Double totalAmount = 0d;

		Set<Integer> set = new TreeSet<>();

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();// Create font
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Id");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Name");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Pan");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Wallet amount credited");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Total Participated Amount");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Outstanding Amount");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		for (DealQuarterlyReportsResponse obj : responseList) {

			Row row1 = spreadSheet.createRow(++rowCount);

			Cell cell1 = row1.createCell(0);
			cell1.setCellValue("LR" + obj.getId());

			cell1 = row1.createCell(1);
			cell1.setCellValue(obj.getName());

			cell1 = row1.createCell(2);
			cell1.setCellValue(obj.getPan());

			BigInteger totalWalletLoaded = obj.getTotalWalletLoaded();

			cell1 = row1.createCell(3);
			cell1.setCellValue(totalWalletLoaded == null ? 0 : totalWalletLoaded.intValue());

			cell1 = row1.createCell(4);
			cell1.setCellValue(obj.getTotalparticipatedAmount() == null ? 0d : obj.getTotalparticipatedAmount());

			cell1 = row1.createCell(5);
			cell1.setCellValue(obj.getOutStandingAmount() == null ? 0d : obj.getOutStandingAmount());

		}

		File f = new File(iciciFilePathBeforeApproval + sbf.toString() + ".xls");

		try {
			outFile = new FileOutputStream(f);
			logger.info("file saved");
			logger.info(iciciFilePathBeforeApproval);
			workBook.write(outFile);
			response.setFileName(f.getName());
			workBook.close();
			outFile.close();

			FileInputStream targetStream = new FileInputStream(f);

			FileRequest fileRequest = new FileRequest();

			fileRequest.setFilePrifix(FileType.QUARTERLY.name());
			fileRequest.setInputStream(targetStream);
			fileRequest.setFileName(f.getName());
			FileResponse putFile = fileManagementService.putFile(fileRequest);

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			response.setDownloadUrl(getFile.getDownloadUrl());

			logger.info(getFile.getDownloadUrl());

			// sending mail starts
			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate1 = expectedDateFormat.format(current.getTime());
			templateContext.put("currentDate", expectedCurrentDate1);
			templateContext.put("$message", "");

			String mailSubject = "	QUARTERLY REPORTS";

			EmailRequest emailRequest = new EmailRequest(
					new String[] { "archana.n@oxyloans.com", "subbu@oxyloans.com" }, "Interests-Approval.template",
					templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			}

			// sending mail ends

		} catch (Exception ex) {
			ex.getMessage();
		}

		return response;

		// return response;
	}

	@Override
	public List<IciciExcelInterestsFiles> readICICReturnedExcel1(File file, int value) {
		logger.info("readICICReturnedExcel1 new method start");
		List<IciciExcelInterestsFiles> responseList = new ArrayList<IciciExcelInterestsFiles>();
		// by default value is 0 because some times the file formats will be changed we
		// get text/corrupted xls format(value=1) or pure xls format(value=0)

		File updatedFile = file;

		if (value == 1) {
			updatedFile = convertTextToXLS(file);// converting the text or corrupted xls format to pure xls
		}

		if (updatedFile != null) {
			if (updatedFile.exists()) {
				String fileName = updatedFile.getName();
				logger.info(fileName);

				DataFormatter Stringform = new DataFormatter(); // excel dataType formatter

				Workbook workbook = null;
				try {
					FileInputStream fileInputStream = new FileInputStream(updatedFile);

					FormulaEvaluator formulaEvaluator = null;

					if (fileName.endsWith(".xls")) {
						try {

							workbook = new HSSFWorkbook(fileInputStream); // getting error in this line
						} catch (Exception e) {

							System.out.println(e.getMessage());
							if (e.getMessage().contains("Your file appears not to be a valid OLE2 document")) {
								responseList = readICICReturnedExcel1(updatedFile, 1);
							}
						}
						formulaEvaluator = new HSSFFormulaEvaluator((HSSFWorkbook) workbook);

					} else if (fileName.endsWith(".xlsx")) {
						try {
							workbook = new XSSFWorkbook(fileInputStream); // getting error in this line
						} catch (Exception e) {
							System.out.println(e.getMessage());
							if (e.getMessage().contains("Your file appears not to be a valid OLE2 document")) {
								responseList = readICICReturnedExcel1(updatedFile, 1);
							}

						}
						formulaEvaluator = new XSSFFormulaEvaluator((XSSFWorkbook) workbook); // formula evaluator
					}

					if (workbook != null) {
						Iterator<Sheet> sheets = workbook.iterator();
						if (sheets != null) {

							while (sheets.hasNext()) {
								Sheet sh = sheets.next();

								Iterator<Row> rowIterator = sh.iterator();

								while (rowIterator.hasNext()) {
									Row row = rowIterator.next();

									if (isRowEmpty(row)) {
										continue;
									}

									if (row.getRowNum() != 0) {
										Cell debitAccount = row.getCell(0);
										logger.info("debitAccount :" + debitAccount);

										formulaEvaluator.evaluate(row.getCell(0)); // Returns string
										String debitAccountStr = Stringform.formatCellValue(debitAccount,
												formulaEvaluator);

										Cell accountNumberCell = row.getCell(1);
										formulaEvaluator.evaluate(row.getCell(1));
										String accountNumberStr = Stringform.formatCellValue(accountNumberCell,
												formulaEvaluator);

										Cell lenderName = row.getCell(2);
										logger.info("lenderName: " + lenderName);

										Cell debitAmountCell = row.getCell(3);

										formulaEvaluator.evaluate(row.getCell(0)); // Returns string
										String debitAmountStr = Stringform.formatCellValue(debitAmountCell,
												formulaEvaluator);

										Cell payMode = row.getCell(4);

										Cell amountPaidDate = row.getCell(5);

										Cell lenderIfsc = row.getCell(6);

										Cell dealIdCell = row.getCell(17);

										Cell daysdifferenceCell = row.getCell(15);

										formulaEvaluator.evaluate(row.getCell(0)); // Returns string
										String dealIdStr = Stringform.formatCellValue(dealIdCell, formulaEvaluator);

										Cell lenderId = row.getCell(18);

										Cell remarks = row.getCell(20);
										logger.info("remarks: " + remarks);

										Cell status = row.getCell(22);

										Cell creditedOn = row.getCell(27);

										Cell addDetails2 = row.getCell(16);

										Cell amountType = row.getCell(16);

										Cell addDetails5 = row.getCell(19);

										Cell addDetails1 = row.getCell(15);

										if (status != null) {
											logger.info(status.getStringCellValue());
											if (status.getStringCellValue().replaceAll(" ", "").equals("Paid")) {

												if (remarks != null) {
													logger.info(remarks.getStringCellValue());
													if (remarks.getStringCellValue().trim().replaceAll("\\s+", " ")
															.equalsIgnoreCase("OXYLOANS REFERRAL BONUS")) {

														int dealId = 0;
														IciciExcelInterestsFiles iciciExcelInterestsFiles = new IciciExcelInterestsFiles();

														iciciExcelInterestsFiles.setDebitAccount(debitAccountStr);

														iciciExcelInterestsFiles.setAccountNumber(accountNumberStr);
														logger.info("accountNumberStr :" + accountNumberStr);
														// iciciExcelInterestsFiles.setAccountNumber(accountNumberCell.getStringCellValue()
														// != null? accountNumberCell.getStringCellValue(): " ");

														iciciExcelInterestsFiles
																.setLenderName(lenderName.getStringCellValue() != null
																		? lenderName.getStringCellValue()
																		: " ");
														iciciExcelInterestsFiles
																.setAmount(Double.parseDouble(debitAmountStr));
														iciciExcelInterestsFiles
																.setPayMode(payMode.getStringCellValue() != null
																		? payMode.getStringCellValue()
																		: " ");
														iciciExcelInterestsFiles
																.setAmountpaidDate(amountPaidDate.toString());
														iciciExcelInterestsFiles
																.setIfscCode(lenderIfsc.getStringCellValue() != null
																		? lenderIfsc.getStringCellValue()
																		: " ");

														iciciExcelInterestsFiles
																.setAmountType(amountType.getStringCellValue() != null
																		? amountType.getStringCellValue().trim()
																		: "");

														iciciExcelInterestsFiles
																.setLenderId(lenderId.getStringCellValue() != null
																		? lenderId.getStringCellValue()
																		: " ");
														iciciExcelInterestsFiles
																.setRemarks(remarks.getStringCellValue() != null
																		? remarks.getStringCellValue()
																		: " ");
														iciciExcelInterestsFiles
																.setStatus(status.getStringCellValue() != null
																		? status.getStringCellValue()
																		: " ");
														iciciExcelInterestsFiles
																.setCreditedOn(creditedOn.getStringCellValue());
														logger.info(creditedOn.getStringCellValue());

//										  if(daysdifferenceCell != null) { if (daysdifferenceCell.getCellType() ==
//										  Cell.CELL_TYPE_STRING) {
//										  iciciExcelInterestsFiles.setDaysDifference(daysdifferenceCell.
//										  getStringCellValue() != null ? daysdifferenceCell.getStringCellValue(): "");
//										  } }

														StringBuilder str1 = new StringBuilder();// tableIds loop

														if (!dealIdStr.equalsIgnoreCase("")
																&& NumberUtils.isNumber(dealIdStr)) {
															dealId = Integer.valueOf(dealIdStr);
															iciciExcelInterestsFiles.setDealId(dealId);
															iciciExcelInterestsFiles.setDealIds(dealIdStr);

														} else if (!dealIdStr.equalsIgnoreCase("")
																&& !dealIdStr.equalsIgnoreCase("NULL")) {
															String[] dealIdValues = dealIdStr.split(",\\s*");
															for (String id : dealIdValues) {
																int dealid2 = Integer.parseInt(id.trim());

																if (str1.length() > 0) {
																	str1.append(",");
																}
																str1.append(dealid2);
															}
															String dealIds1 = str1.toString();
															iciciExcelInterestsFiles.setDealIds(dealIds1);
															logger.info("tableIds :" + dealIds1);

														} else {
															iciciExcelInterestsFiles.setDealIds("");
														}

														iciciExcelInterestsFiles
																.setAddDetails2(addDetails2.getStringCellValue() != null
																		? addDetails2.getStringCellValue()
																		: "");

//			         StringBuilder refereeIds1 = new StringBuilder(); 
//			         if (addDetails5 != null ) {
//			             if (addDetails5.getCellType() == Cell.CELL_TYPE_NUMERIC) {
//			            	 double numericValue = addDetails5.getNumericCellValue();
//			            	    int intValue = (int) numericValue;
//                                 iciciExcelInterestsFiles.setAddDetails5(intValue);
//			            	     iciciExcelInterestsFiles.setAddDetailsI5(String.valueOf(intValue));
//			            	    
//			             } else if (addDetails5.getCellType() == Cell.CELL_TYPE_STRING) {
//			                 String str = addDetails5.getStringCellValue();
//			                 String[] refereeids = str.split(",\\s*");
//			                
//			                 for (String ids : refereeids) {
//			                     ids = ids.trim();
//
//			                     if (NumberUtils.isDigits(ids)) {
//			                         if (refereeIds1.length() > 0) {refereeIds1.append(",");}
//			                         refereeIds1.append(ids);
//			                         } 
//			                    }
//			                 String dealIds1 = refereeIds1.toString();
//	                         iciciExcelInterestsFiles.setAddDetailsI5(dealIds1);
//				             logger.info("refereeIds1 :" + dealIds1) ;
//			             }
//			              } else {
//			             iciciExcelInterestsFiles.setAddDetailsI5("");
//			         }
//			         

														ListMultimap<Integer, Integer> refereeIdDealIdMap = ArrayListMultimap
																.create();
														// Map<Integer, Integer> refereeIdDealIdMap = new HashMap<>();
														StringBuilder refereeIds2 = new StringBuilder();// refereeId,DealId
																										// loop

														if (addDetails1 != null) {
															if (addDetails1.getCellType() == Cell.CELL_TYPE_NUMERIC) {
																iciciExcelInterestsFiles.setAddDetails1(
																		(int) addDetails1.getNumericCellValue());
															} else if (addDetails1
																	.getCellType() == Cell.CELL_TYPE_STRING
																	&& !addDetails1.getStringCellValue().trim()
																			.equalsIgnoreCase("NULL")) {
																String str = addDetails1.getStringCellValue();
																String[] refereeIdsArray = str.split("\\s+"); // Split
																												// by
																												// whitespace

																for (String refereeIdPair : refereeIdsArray) {
																	refereeIdPair = refereeIdPair.trim();

																	if (refereeIdPair.startsWith("[")
																			&& refereeIdPair.endsWith("]")) {
																		String content = refereeIdPair.substring(1,
																				refereeIdPair.length() - 1);
																		String[] values = content.split(",");

																		if (values.length == 2) {
																			try {
																				int refereeId = Integer
																						.parseInt(values[0].trim());
																				int dealId1 = Integer
																						.parseInt(values[1].trim());

																				refereeIdDealIdMap.put(refereeId,
																						dealId1);
																			} catch (NumberFormatException e) {
																				logger.error(e.getMessage(), e);
																			}
																		}
																	}
																}
																for (Map.Entry<Integer, Integer> entry : refereeIdDealIdMap
																		.entries()) {
																	int refereeId1 = entry.getKey();
																	int dealId2 = entry.getValue();

																	logger.info("refereeId :" + refereeId1);
																	logger.info("dealId :" + dealId2);

																	if (refereeIds2.length() > 0) {
																		refereeIds2.append(" ");
																	}
																	refereeIds2.append("[").append(refereeId1)
																			.append(",").append(dealId2).append("]");
																}
																iciciExcelInterestsFiles
																		.setAddDetailsI1(refereeIds2.length() > 0
																				? refereeIds2.toString()
																				: "");
															}
														} else {
															iciciExcelInterestsFiles.setAddDetails1(0);
														}

														responseList.add(iciciExcelInterestsFiles);
													}
												}
											}
										}
									}

								}
							}
						}

					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		} else {
			logger.info("updated file  doesnot exist");
		}
		logger.info(responseList);
		return responseList;

	}

}
