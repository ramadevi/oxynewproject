package com.oxyloans.service.excelservice;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.oxyloans.cms.IciciCmsRequestDto;
import com.oxyloans.cms.RejectedFileDto;
import com.oxyloans.dashboard.LenderTotalPaticipationDealsInfo;
import com.oxyloans.dashboard.LenderWalletHistoryResponseDto;
import com.oxyloans.dashboard.NewLenderReturnsResponseDto;
import com.oxyloans.dashboard.ReferrerResponseDto;
import com.oxyloans.entityborrowersdealsdto.DealQuarterlyReportsRequest;
import com.oxyloans.entityborrowersdealsdto.DealQuarterlyReportsResponse;
import com.oxyloans.entity.cms.OxyCmsLoans;
import com.oxyloans.lender.participationToWallet.LenderPaticipationRequestDto;
import com.oxyloans.request.user.BorrowerPaymentsDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.BankTransactionsBreakUpDto;
import com.oxyloans.response.user.IciciExcelInterestsFiles;
import com.oxyloans.response.user.StatusResponseDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.serviceloan.InterestsApprovalDto;

public interface ExcelServiceRepo {

	public String sendBulkInvite(File file, String fileExtension) throws IOException, InvalidFormatException;

	public String walletCreditedOrDebitedExcel(String type, int userId,
			List<LenderWalletHistoryResponseDto> lenderWalletHistoryResponseDtoForCredited);

	public String lenderPrincipalReturnedExcel(int userId,
			List<NewLenderReturnsResponseDto> lenderPricipalReturnsResponseDto);

	public String lenderParticipationInfoExcel(int userId,
			List<LenderTotalPaticipationDealsInfo> lenderTotalPaticipationDealsInfo);

	public String lenderReferalBonousInfoExcel(int userId, List<ReferrerResponseDto> referrerResponseDto);

	public StatusResponseDto writeInterestsApprovalInfo(String string, List<InterestsApprovalDto> interestsApprovalDto);

	public List<InterestsApprovalDto> findAllBasedOnStatus(File file, String name, String status);

	public StatusResponseDto updateApprovedExcelSheetInfo(Map<Integer, InterestsApprovalDto> map, File file,
			String name);

	public StatusResponseDto updateForEveryLender(File file, String name);

	public List<InterestsApprovalDto> readExcelInterestPaymentsExcel(File file);

	public StatusResponseDto writeInterestsExcelForBank(List<InterestsApprovalDto> interestsApprovalExcelList,
			String paymentDate, String dealName, Integer dealID, String originalPaymentDate);

	public StatusResponseDto updateInterestsInfoExcel(List<InterestsApprovalDto> approvalDtoList, File file);

	public List<IciciExcelInterestsFiles> readICICReturnedExcel(File file, int value);

	public void writeBorrowerPaymentsExcel(BorrowerPaymentsDto dto, String disbursedDateString);

	public String generateUtmsExcel(List<UserResponse> userResponseList, String utm);

	public String generateCurrentMonthTransactionAlertsExcel(List<BankTransactionsBreakUpDto> breakUpList,
			String transactionType);

	public StatusResponseDto writeNewCmsExcelForInterests(List<InterestsApprovalDto> breakupList, String string,
			String dealName, Integer dealId);

	public StatusResponseDto witeDealLevelLoanDisbursmentExcel(String paymentDate, UserRequest userRequest,
			OxyCmsLoans cmsLoans, String fileType);

	public List<String> getMobileNumbersFromExcel(File file);

	public StatusResponseDto witeCmsExcel(String string, IciciCmsRequestDto obj, String paymentDate, String fileType);

	public String interestRetrunedExcel(int userId, List<NewLenderReturnsResponseDto> lenderInterestReturnsResponseDto,
			double toatlInterest);

	public StatusResponseDto writePrinicalToWalletExcel(int dealId,
			List<LenderPaticipationRequestDto> listOflenderPaticipationRequestDto, String paymentDate,
			String debitAccountNo);

	public List<RejectedFileDto> readRejectedFiles(File file);

	public StatusResponseDto createQuarterlyReportsExcel(List<DealQuarterlyReportsResponse> responseList,
			DealQuarterlyReportsRequest request);

	public List<IciciExcelInterestsFiles> readICICReturnedExcel1(File file, int value);



}
