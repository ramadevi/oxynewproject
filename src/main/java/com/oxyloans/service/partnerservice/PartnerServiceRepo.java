package com.oxyloans.service.partnerservice;

import java.io.IOException;

import com.oxyloans.engine.template.PdfGeenrationException;
import com.oxyloans.entityborrowersdealsdto.DealLevelResponseDto;
import com.oxyloans.partner.LoanRequestStatusUpdationRequestDto;
import com.oxyloans.partner.LoanResponseStatusUpdationRequestDto;
import com.oxyloans.partner.PartnerRegistrationResponseDto;
import com.oxyloans.serviceloan.LoanRequestDto;
import com.oxyloans.serviceloan.LoanResponseDto;

public interface PartnerServiceRepo {

	public LoanResponseDto updateLoanRequestByPartner(int userId, LoanRequestDto loanRequestDto);

	public LoanResponseStatusUpdationRequestDto partnerApprovingLoanRequest(int userId,
			LoanRequestStatusUpdationRequestDto loanRequestStatusUpdationRequestDto);

	public LoanResponseStatusUpdationRequestDto lendersWalletAmount(String partnerUtm);

	public DealLevelResponseDto partnerBasedAgreementsGeneration(int borrowerId)
			throws PdfGeenrationException, IOException;

	public PartnerRegistrationResponseDto partnerFundsTransfer(int borrowerId);

}
