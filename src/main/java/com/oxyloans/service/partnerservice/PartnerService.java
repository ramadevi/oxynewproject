package com.oxyloans.service.partnerservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.api.client.util.Value;
import com.oxyloans.cashfreepayment.CashfreeService;
import com.oxyloans.cms.IciciCmsRequestDto;
import com.oxyloans.commonutil.DateUtil;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.engine.template.IPdfEngine;
import com.oxyloans.engine.template.PdfGeenrationException;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entityborrowersdealsdto.DealLevelResponseDto;
import com.oxyloans.entityborrowersdealsdto.LoanResponseForDealsDto;
import com.oxyloans.entity.cms.OxyCmsLoans;
import com.oxyloans.entity.cms.OxyCmsLoans.FileExecutionStatus;
import com.oxyloans.repository.cms.OxyCmsLoansRepo;
import com.oxyloans.entity.enach.EnachMandate;
import com.oxyloans.entity.enach.EnachMandate.AmountType;
import com.oxyloans.entity.enach.EnachMandate.Frequency;
import com.oxyloans.entity.enach.EnachMandate.MandateStatus;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletNativeRepo;
import com.oxyloans.entity.loan.LoanEmiCard;
import com.oxyloans.entity.loan.LoanRequest;
import com.oxyloans.entity.loan.LoanRequest.DurationType;
import com.oxyloans.entity.loan.LoanRequest.LoanStatus;
import com.oxyloans.entity.loan.LoanRequest.RepaymentMethod;
import com.oxyloans.entity.loan.OxyLoan;
import com.oxyloans.entity.user.PersonalDetails;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.repository.userdocumentstatus.UserDocumentStatusRepo;
import com.oxyloans.entity.user.type.OxyUserType;
import com.oxyloans.entity.user.type.OxyUserTypeRepo;
import com.oxyloans.excelservice.ExcelServiceRepo;
import com.oxyloans.file.FileRequest;
import com.oxyloans.file.FileRequest.FileType;
import com.oxyloans.file.FileResponse;
import com.oxyloans.partner.LoanRequestStatusUpdationRequestDto;
import com.oxyloans.partner.LoanResponseStatusUpdationRequestDto;
import com.oxyloans.partner.PartnerRegistrationResponseDto;
import com.oxyloans.repo.enach.EnachMandateRepo;
import com.oxyloans.repo.loan.LenderBorrowerConversationRepo;
import com.oxyloans.repo.loan.LoanEmiCardRepo;
import com.oxyloans.repo.loan.OxyLoanRepo;
import com.oxyloans.repo.loan.OxyLoanRequestRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.StatusResponseDto;
import com.oxyloans.security.signature.ISignatureService;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.service.file.IFileManagementService;
import com.oxyloans.service.loan.ActionNotAllowedException;
import com.oxyloans.service.loan.AdminLoanService;
import com.oxyloans.service.loan.LoanServiceFactory;
import com.oxyloans.serviceloan.LoanRequestDto;
import com.oxyloans.serviceloan.LoanResponseDto;
import com.oxyloans.service.user.UserServiceImpl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Service
public class PartnerService implements PartnerServiceRepo {

	private static final String DATE_FORMAT = "dd-MM-yyyy";

	private SimpleDateFormat FORMAT = new SimpleDateFormat("dd-MM-YYYY");

	private static String secretKeyForPartner = "oxyloans";

	private static String saltForPartner = "sha95sha95";

	private final String LOAN_ID_FORMAT = "LN{ID}";

	private final String LOAN_REQUEST_IDFORMAT = "AP{type}{ID}";

	@Autowired
	private OxyUserTypeRepo oxyUserTypeRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private ISignatureService signatureService;

	private final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Autowired
	private LoanServiceFactory loanServiceFactory;

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	@Autowired
	private CashfreeService cashfreeService;

	@Autowired
	private IEmailService emailService;

	@Autowired
	private UserDocumentStatusRepo userDocumentStatusRepo;

	@Autowired
	private IFileManagementService fileManagementService;

	@Autowired
	private OxyLoanRequestRepo loanRequestRepo;

	@Autowired
	private AdminLoanService adminLoanService;

	@Autowired
	private LenderOxyWalletNativeRepo lenderOxyWalletNativeRepo;

	@Autowired
	private OxyLoanRepo oxyLoanRepo;

	@Autowired
	private LenderBorrowerConversationRepo lenderBorrowerConversationRepo;

	@Autowired
	protected IPdfEngine pdfEngine;

	@Autowired
	private LoanEmiCardRepo loanEmiCardRepo;

	@Autowired
	protected EnachMandateRepo enachMandateRepo;

	@Autowired
	protected DateUtil dateUtil;

	@Value("${defaultEmiDay}")
	private String defaultEmiDay;

	@Autowired
	private OxyCmsLoansRepo oxyCmsLoansRepo;

	@Autowired
	private ExcelServiceRepo excelServiceRepo;

	@Override
	@Transactional
	public LoanResponseDto updateLoanRequestByPartner(int userId, LoanRequestDto loanRequestDto) {
		User user = userRepo.findById(userId).get();
		Integer loanRequestId = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId()).getId();
		return updatingUserRequest(userId, loanRequestId, loanRequestDto, false);
	}

	private LoanResponseDto updatingUserRequest(int userId, Integer loanRequestId, LoanRequestDto loanRequestDto,
			boolean overrideCommitementAmount) {
		LoanRequest loanRequest = loanRequestRepo.findById(loanRequestId).get();
		if (loanRequest.getLoanOfferedAmount() == null) {
			if (!loanRequest.getLoanStatus().equals(LoanRequest.LoanStatus.PartnerApproved)) {
				User user = userRepo.findById(userId).get();
				if (user != null) {
					if (user.getUrchinTrackingModule() != null) {
						OxyUserType parterner = oxyUserTypeRepo
								.getDetailsByUtmNameAndType(user.getUrchinTrackingModule());
						if (parterner != null) {

							if (parterner.getBorrowerProcessingFee() == 0) {
								throw new OperationNotAllowedException("Borrower processing fee not given to partner",
										ErrorCodes.ACTION_ALREDY_DONE);
							}
						}
					}

				}
				if (loanRequestDto.getLoanRequestAmount() != null) {
					loanRequest.setLoanRequestAmount(loanRequestDto.getLoanRequestAmount());

				}
				if (loanRequestDto.getDuration() != null) {
					loanRequest.setDurationBySir(loanRequestDto.getDuration());
				}
				if (loanRequestDto.getRateOfInterestToBorrower() != null) {
					loanRequest.setRateOfInterestToBorrower(loanRequestDto.getRateOfInterestToBorrower());
				}
				if (loanRequestDto.getRateOfInterestToLender() != null) {
					loanRequest.setRateOfInterestToLender(loanRequestDto.getRateOfInterestToLender());
				} else {
					loanRequest.setRateOfInterestToLender(loanRequestDto.getRateOfInterestToBorrower());
				}
				if (loanRequestDto.getRepaymentTypeToBorrower() != null) {
					loanRequest.setRepaymentMethodForBorrower(loanRequestDto.getRepaymentTypeToBorrower());
				}
				if (loanRequestDto.getRepaymentTypeToLender() != null) {
					loanRequest.setRepaymentMethodForLender(loanRequestDto.getRepaymentTypeToLender());
				} else {
					loanRequest.setRepaymentMethodForLender(loanRequestDto.getRepaymentTypeToBorrower());
				}

				if (loanRequestDto.getDurationType().equalsIgnoreCase("months")) {
					loanRequest.setDurationType(DurationType.Months);
				} else if (loanRequestDto.getDurationType().equalsIgnoreCase("days")) {
					loanRequest.setDurationType(DurationType.Days);
				}

				loanRequest.setLoanStatus(LoanRequest.LoanStatus.PartnerEdited);
				loanRequest.setExpectedDate(new Date());
				loanRequest = loanRequestRepo.save(loanRequest);
			} else {
				if (loanRequest.getLoanStatus().equals(LoanRequest.LoanStatus.PartnerApproved)) {
					throw new OperationNotAllowedException(
							"You have already approved this loan request you can not edit",
							ErrorCodes.ACTION_ALREDY_DONE);
				}
			}
		} else {
			throw new OperationNotAllowedException("admin sent an offer now you can not change your loan request",
					ErrorCodes.ACTION_ALREDY_DONE);
		}
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setUserId(userId);
		loanResponseDto.setLoanRequestId(loanRequest.getId());
		loanResponseDto.setLoanRequest(loanRequest.getLoanRequestId());
		return loanResponseDto;
	}

	@Override
	@Transactional
	public LoanResponseStatusUpdationRequestDto partnerApprovingLoanRequest(int userId,
			LoanRequestStatusUpdationRequestDto loanRequestStatusUpdationRequestDto) {
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
		LoanResponseStatusUpdationRequestDto loanResponseStatus = new LoanResponseStatusUpdationRequestDto();
		if (loanRequest != null) {

			User user = userRepo.findById(userId).get();
			if (user.getUrchinTrackingModule() != null) {
				OxyUserType parterner = oxyUserTypeRepo.getDetailsByUtmNameAndType(user.getUrchinTrackingModule());
				if (parterner != null) {

					if (parterner.getBorrowerProcessingFee() == 0) {
						throw new OperationNotAllowedException("Borrower processing fee not given to partner",
								ErrorCodes.ACTION_ALREDY_DONE);
					}

					if (loanRequestStatusUpdationRequestDto.getStatus()
							.equals(LoanRequest.LoanStatus.PartnerApproved.toString())) {
						double totalWallet = lendersMappedToPartnerTotalWalletSum(user.getUrchinTrackingModule(),
								loanRequest.getLoanRequestAmount());
						if (totalWallet > 0.0) {
							if (totalWallet >= loanRequest.getLoanRequestAmount()) {
								LoanRequestDto loanRequestDto = new LoanRequestDto();
								loanRequestDto.setLoanOfferedAmount(loanRequest.getLoanRequestAmount());
								loanRequestDto.setLoanRequestedId(loanRequest.getLoanRequestId());
								loanRequestDto.setRateOfInterest(loanRequest.getRateOfInterestToBorrower());
								loanRequestDto.setDuration(loanRequest.getDurationBySir());
								loanRequestDto.setDurationType(loanRequest.getDurationType().toString());
								double borrowerFee = (loanRequest.getLoanRequestAmount()
										* parterner.getBorrowerProcessingFee()) / 100;
								double gstValue = (borrowerFee * 18) / 100;
								loanRequestDto.setFee(borrowerFee + gstValue);
								try {
									adminLoanService.sendOffer(userId, loanRequestDto);

								} catch (MessagingException e) {

									e.printStackTrace();
								}
								user.setAdminComments(User.Status.INTERESTED.toString());
								userRepo.save(user);
							} else {
								throw new OperationNotAllowedException(
										"your offering amount is less than the wallet amount please check your total wallet amount "
												+ BigDecimal.valueOf(totalWallet).toBigInteger(),
										ErrorCodes.AMOUNT_EXCEEDED);
							}
						} else {
							throw new OperationNotAllowedException("Insufficient amount in lender account",
									ErrorCodes.AMOUNT_LIMIT);
						}
					}
				}
			}
			loanRequest.setLoanStatus(LoanRequest.LoanStatus.valueOf(loanRequestStatusUpdationRequestDto.getStatus()));
			loanRequestRepo.save(loanRequest);
		}
		loanResponseStatus.setStatus("statusUpdated");
		return loanResponseStatus;

	}

	public double lendersMappedToPartnerTotalWalletSum(String urchinTrackingModule, double loanRequestAmount) {
		List<Object[]> listOfLendersMappedToPartner = oxyUserTypeRepo.lendersMappedToPartner(urchinTrackingModule);
		double totalWalletAmount = 0.0;
		double offeringAmount = loanRequestAmount;
		if (listOfLendersMappedToPartner != null && !listOfLendersMappedToPartner.isEmpty()) {
			for (Object[] e : listOfLendersMappedToPartner) {
				Double amountCredited = Double.parseDouble(e[1] == null ? "0" : e[1].toString());
				double remainingAmount = 0.0;
				if (amountCredited != null) {
					remainingAmount = remainingAmount + amountCredited;
					Double amountDebited = Double.parseDouble(e[2] == null ? "0" : e[2].toString());
					Double agreementsGenerated = Double.parseDouble(e[3] == null ? "0" : e[3].toString());

					if (amountDebited > 0) {
						remainingAmount = remainingAmount - amountDebited;
					}
					if (agreementsGenerated > 0) {
						remainingAmount = remainingAmount - agreementsGenerated;
					}
					if (remainingAmount >= 50000) {
						offeringAmount = offeringAmount - 50000;
					} else {
						offeringAmount = offeringAmount - remainingAmount;
					}
				}
				totalWalletAmount = totalWalletAmount + remainingAmount;
			}
			if (loanRequestAmount > 0) {
				if (offeringAmount > 0) {
					throw new OperationNotAllowedException(
							"your lenders wallet amount is less then your offering amount please check",
							ErrorCodes.AMOUNT_LIMIT);
				}
			}

		} else {
			throw new OperationNotAllowedException("There are no Lenders Mapped to This Partner",
					ErrorCodes.USER_NOT_FOUND);
		}
		return totalWalletAmount;

	}

	@Override
	@Transactional
	public DealLevelResponseDto partnerBasedAgreementsGeneration(int borrowerId)
			throws PdfGeenrationException, IOException {
		DealLevelResponseDto dealLevelResponseDto = new DealLevelResponseDto();

		User user = userRepo.findById(borrowerId).get();
		String message = null;
		if (user != null) {

			List<Integer> lendersMappedToPartner = userRepo
					.listOfLendersMappedToPartner(user.getUrchinTrackingModule());

			Double remainingAmount = 0.0;
			Double loanAmount = 0.0;

			for (int i = 0; i < lendersMappedToPartner.size(); i++) {

				int lenderId = lendersMappedToPartner.get(i);
				LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(lenderId);

				double walletAmount = lenderFromPartnerWalletInfo(lenderId);

				if (walletAmount > 0.0) {

					Double currentApplicationAmount = loanRequestRepo.checkCurrentApplication(borrowerId);
					LoanRequest loanRequestForBorrower = loanRequestRepo
							.findByUserIdAndParentRequestIdIsNull(borrowerId);
					if (currentApplicationAmount != null) {
						Double amountForAgreementsGrenerated = oxyLoanRepo
								.acceptedAmountToBorrowerBasedOnParentRequestId(loanRequestForBorrower.getId());

						int count = lenderBorrowerConversationRepo.countByLenderUserIdAndBorrowerUserId(lenderId,
								borrowerId);

						Double amount = 0.0;

						if (amountForAgreementsGrenerated != null) {
							amount = amountForAgreementsGrenerated;
						}

						remainingAmount = currentApplicationAmount - amount;
						if (remainingAmount > 0) {

							if (count == 0) {
								if (walletAmount >= remainingAmount) {
									LoanRequestDto loanRequestDto = new LoanRequestDto();
									if (remainingAmount >= 50000) {
										loanAmount = 50000.0;
									} else {
										loanAmount = remainingAmount;
									}
									Double totalAmountDisbursed = oxyLoanRepo
											.getTotalAmountDisbursedForParticularrUsers(borrowerId, lenderId);

									if (totalAmountDisbursed == null || totalAmountDisbursed < 50000) {

										loanRequestDto.setLoanRequestAmount(loanAmount);
										loanRequestDto.setRateOfInterest(
												loanRequestForBorrower.getRateOfInterestToBorrower());
										loanRequestDto.setDuration(loanRequestForBorrower.getDurationBySir());
										loanRequestDto
												.setDurationType(loanRequestForBorrower.getDurationType().toString());
										loanRequestDto.setRepaymentMethod(
												loanRequestForBorrower.getRepaymentMethodForBorrower());
										loanRequestDto.setLoanPurpose("personal loan");
										Date currentDate = new Date();
										loanRequestDto.setExpectedDate(expectedDateFormat.format(currentDate));
										loanRequestDto.setParentRequestId(loanRequest.getId());
										LoanResponseForDealsDto loanRequestForDeals = adminLoanService
												.loanRequestForDeals(borrowerId, loanRequestDto);
										Integer loanRequestId = loanRequestForDeals.getLoanRequestId();

										message = actOnRequestForDeals(lenderId, loanRequestId, "Accepted");

										// walletAmount = walletAmount - (50000);

									}
								}
							}
						}
					}
				}

			}
		}

		return dealLevelResponseDto;

	}

	public double lenderFromPartnerWalletInfo(int userId) {
		Double totalCreditDetails = lenderOxyWalletNativeRepo.lenderWalletCreditAmount(userId);
		Double totalDebitDetails = lenderOxyWalletNativeRepo.lenderWalletDebitAmount(userId);
		Double disbursedAmount = oxyLoanRepo.acceptedAmountToLender(userId);
		double walletAmount = 0.0;
		if (totalCreditDetails != null) {
			walletAmount = totalCreditDetails;
			if (totalDebitDetails != null) {
				walletAmount = walletAmount - totalDebitDetails;
			}
			if (disbursedAmount != null) {
				walletAmount = walletAmount - disbursedAmount;
			}

		}
		return walletAmount;
	}

	public String actOnRequestForDeals(int userId, Integer loanRequestId, String loanStatus)
			throws PdfGeenrationException, IOException {
		String loanIdFormanuvalEsign = null;

		User user = userRepo.findById(userId).get();

		LoanRequest loanRequest = loanRequestRepo.findById(loanRequestId).get();
		if (loanRequest.getLoanStatus() == LoanStatus.Accepted || loanRequest.getLoanStatus() == LoanStatus.Rejected) {
			throw new ActionNotAllowedException("This Request already " + loanRequest.getLoanStatus(),
					ErrorCodes.ACTION_ALREDY_DONE);
		}
		loanRequest.setLoanStatus(LoanStatus.Accepted);
		loanRequest = loanRequestRepo.save(loanRequest);
		Integer parentRequestId = loanRequest.getParentRequestId();

		LoanRequest parentRequest = loanRequestRepo.findById(parentRequestId).get();

		User lenderUser = null;
		User borrowerUser = null;
		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			borrowerUser = user;
			lenderUser = userRepo.findById(loanRequest.getUserId()).get();
		} else {
			lenderUser = user;
			borrowerUser = userRepo.findById(loanRequest.getUserId()).get();
		}

		OxyLoan finalLoan = null;
		if (loanStatus.equals(LoanStatus.Accepted.toString())) {
			User requestedUser = (user.getPrimaryType() == PrimaryType.BORROWER ? borrowerUser : lenderUser);
			LoanRequestDto loanRequestDto = new LoanRequestDto();
			loanRequestDto.setLoanRequestAmount(loanRequest.getLoanRequestAmount());
			adminLoanService.isLimitExceeded(requestedUser.getId(), user, loanRequestDto);
			finalLoan = new OxyLoan();
			finalLoan.setBorrowerUserId(borrowerUser.getId());
			LoanRequest borrowerApplication = loanRequestRepo
					.findByUserIdAndParentRequestIdIsNull(borrowerUser.getId());
			LoanRequest lenderApplication = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(lenderUser.getId());

			loanRequest.setApplication_id(borrowerApplication.getLoanRequestId());
			finalLoan.setApplication_id(borrowerApplication.getLoanRequestId());

			finalLoan.setBorrowerParentRequestId(borrowerApplication.getId());
			finalLoan.setLenderParentRequestId(lenderApplication.getId());
			finalLoan.setLenderUserId(lenderUser.getId());
			finalLoan.setLoanAcceptedDate(new Date());
			finalLoan.setLoanRequestId(parentRequestId);
			finalLoan.setLoanRespondId(loanRequest.getId());
			finalLoan.setDisbursmentAmount(loanRequest.getLoanRequestAmount());
			finalLoan.setLoanId(new Long(System.currentTimeMillis()).toString());

			String loanId = finalLoan.getLoanId();
			LoanRequest loanRequestObject = loanRequestRepo.findByLoanId(loanId);
			if (loanRequestObject != null) {

				loanRequestObject.setRepaymentMethod(RepaymentMethod.I);

				loanRequestRepo.save(loanRequestObject);
			}
			if (borrowerApplication.getDurationBySir() > 0) {
				if (borrowerApplication.getRepaymentMethodForBorrower().equals("PI")) {
					finalLoan.setRepaymentMethod(RepaymentMethod.PI);
				} else {
					finalLoan.setRepaymentMethod(RepaymentMethod.I);
				}

				finalLoan.setRateOfInterest(borrowerApplication.getRateOfInterestToBorrower());
			}
			finalLoan.setDuration(loanRequest.getDuration());
			finalLoan.setDurationType(loanRequest.getDurationType());
			finalLoan = oxyLoanRepo.save(finalLoan);
			finalLoan.setLoanId(LOAN_ID_FORMAT.replace("{ID}", finalLoan.getId().toString()));
			finalLoan = oxyLoanRepo.save(finalLoan);
			loanRequest.setLoanId(finalLoan.getLoanId());
			loanIdFormanuvalEsign = finalLoan.getLoanId();

			loanRequestRepo.save(loanRequest);

			TemplateContext templateContext = new TemplateContext();

			PersonalDetails borrwoerPersonalDetails = borrowerUser.getPersonalDetails();
			templateContext.put("borrowerFullName",
					borrwoerPersonalDetails.getFirstName() + " " + borrwoerPersonalDetails.getLastName());
			templateContext.put("borrowerFatherName", borrwoerPersonalDetails.getFatherName());

			String borrowerAddress = borrowerUser.getPersonalDetails().getAddress();
			Pattern pt = Pattern.compile("[^a-zA-Z0-9]");
			Matcher match = pt.matcher(borrowerAddress);
			while (match.find()) {
				String s = match.group();
				borrowerAddress = borrowerAddress.replaceAll("\\" + s, " ");
			}
			templateContext.put("borrowerAddress", borrowerAddress);
			PersonalDetails lenderPersonalDetails = lenderUser.getPersonalDetails();
			templateContext.put("lenderFullName",
					lenderPersonalDetails.getFirstName() + " " + lenderPersonalDetails.getLastName());
			templateContext.put("lenderFatherName", lenderPersonalDetails.getFatherName());
			String lenderAddress = lenderPersonalDetails.getAddress();
			Pattern pt1 = Pattern.compile("[^a-zA-Z0-9]");
			Matcher match1 = pt1.matcher(lenderAddress);
			while (match1.find()) {
				String s = match1.group();
				lenderAddress = lenderAddress.replaceAll("\\" + s, " ");
			}

			templateContext.put("lenderAddress", lenderAddress);
			int borrowerId = finalLoan.getBorrowerUserId();
			LoanRequest loanRequestDetails = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(borrowerId);

			if (loanRequestDetails.getDurationBySir() > 0) {
				if (loanRequestDetails.getDurationType().equals(DurationType.Months)) {
					templateContext.put("duration", loanRequestDetails.getDurationBySir());
					templateContext.put("rateOfInterestOfLender",
							BigDecimal.valueOf(loanRequestDetails.getRateOfInterestToLender()).toBigInteger());
					templateContext.put("rateOfInterestOfBorrower",
							BigDecimal.valueOf(loanRequestDetails.getRateOfInterestToBorrower()).toBigInteger());
				}
			}

			templateContext.put("loanAmount", BigDecimal.valueOf(finalLoan.getDisbursmentAmount()).toBigInteger());

			DateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

			formatter.setTimeZone(TimeZone.getTimeZone("IST")); // here set timezone

			templateContext.put("agreementDate", formatter.format(new Date()));

			Calendar calendar = Calendar.getInstance();
			String heading = "";

			double emiAmount = 0.0;
			int count = 0;
			double totalPrincipalAmount = 0.0;
			String emiLastDate = null;

			templateContext.put("heading", heading);
			templateContext.put("totalPrincipalAmount", totalPrincipalAmount);
			templateContext.put("emiLastDate", emiLastDate);
			templateContext.put("date", expectedDateFormat.format(finalLoan.getLoanAcceptedDate()));
			templateContext.put("loanId", loanRequest.getLoanId());

			double principalAmountPerMonth = finalLoan.getDisbursmentAmount() / loanRequestDetails.getDurationBySir();
			double interestAmountPerMonth = (finalLoan.getDisbursmentAmount()
					* loanRequestDetails.getRateOfInterestToBorrower()) / 100;
			double emiAmountForBorrower = principalAmountPerMonth + interestAmountPerMonth;

			templateContext.put("emiAmountForBorrower", BigDecimal.valueOf(emiAmountForBorrower).toBigInteger());

			String accountDetails = null;
			String feeDetails = null;
			for (int i = 0; i <= 3; i++) {
				if (i == 0) {
					User lenderDetails = userRepo.findById(finalLoan.getLenderUserId()).get();
					if (lenderDetails != null) {
						if (lenderDetails.getBankDetails() != null) {
							String name = lenderPersonalDetails.getFirstName() + " "
									+ lenderPersonalDetails.getLastName();
							accountDetails = accountDetails + adminLoanService.accountDetailsForAgreement(name,
									lenderDetails.getBankDetails().getBankName(), null, null, null);
						}
					}
					feeDetails = feeDetails
							+ adminLoanService.feeDetailsForAgreement("Registration fee", "Rs 0", "Rs 0");

				}
				if (i == 1) {
					User borrowerDetails = userRepo.findById(finalLoan.getBorrowerUserId()).get();
					if (borrowerDetails != null) {
						if (borrowerDetails.getBankDetails() != null) {
							String name = borrwoerPersonalDetails.getFirstName() + " "
									+ borrwoerPersonalDetails.getLastName();
							accountDetails = accountDetails + adminLoanService.accountDetailsForAgreement(name,
									borrowerDetails.getBankDetails().getBankName(),
									borrowerDetails.getBankDetails().getAccountNumber(),
									borrowerDetails.getBankDetails().getBranchName(),
									borrowerDetails.getBankDetails().getIfscCode());
						}
					}

					String fee = "Rs " + BigDecimal.valueOf(borrowerApplication.getLoanOfferedAmount().getBorrowerFee())
							.toBigInteger();

					feeDetails = feeDetails + adminLoanService
							.feeDetailsForAgreement("Service fee (Excl borrower insurance fee)", fee, "Rs 0");

				}
				if (i == 2) {
					String lenderBankName = "SRS Fintech Private Limited – Lenders Funding\r\n" + "Escrow account\r\n"
							+ "";
					accountDetails = accountDetails + adminLoanService.accountDetailsForAgreement(lenderBankName,
							"ICICI", null, null, "ICIC0000106");
					feeDetails = feeDetails + adminLoanService.feeDetailsForAgreement("Delayed payment charges",
							"Rs. 500/- + GST for delay in each EMI", "null");

				}
				if (i == 3) {
					String borrowerBankName = "SRS Fintech Private Limited – Borrower’s repayment escrow account";
					accountDetails = accountDetails
							+ adminLoanService.accountDetailsForAgreement(borrowerBankName, null, null, null, null);
					feeDetails = feeDetails + adminLoanService.feeDetailsForAgreement(
							"Installment Collection and EMI Processing.", "null", "of each EMI till loan closure.");

				}

			}
			templateContext.put("accountDetails", accountDetails);
			templateContext.put("feeDetails", feeDetails);

			logger.info("Borrower agreement start");
			String outputFileName = finalLoan.getBorrowerUserId() + finalLoan.getLoanId() + ".pdf";
			String generatedPdf = "";
			if (loanRequestDetails != null && loanRequestDetails.getDurationType().equals(DurationType.Months)) {
				generatedPdf = pdfEngine.generatePdf("agreement/agreementTemplate.xml", templateContext,
						outputFileName);
			}
			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(generatedPdf));
			fileRequest.setFileName(outputFileName);
			fileRequest.setFileType(FileType.Agreement);
			fileRequest.setFilePrifix(FileType.Agreement.name());
			try {
				FileResponse putFile = fileManagementService.putFile(fileRequest);

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			File file = new File(generatedPdf);
			boolean fileDeleted = file.delete();

			logger.info("Lender agreement start");
			String outputFileNameForLender = finalLoan.getLenderUserId() + finalLoan.getLoanId() + ".pdf";
			String generatedPdfForLender = "";
			if (loanRequestDetails != null && loanRequestDetails.getDurationType().equals(DurationType.Months)) {
				generatedPdfForLender = pdfEngine.generatePdf("agreement/lenderAgreementTemplate.xml", templateContext,
						outputFileNameForLender);
			}
			FileRequest fileRequestForLender = new FileRequest();
			fileRequestForLender.setInputStream(new FileInputStream(generatedPdfForLender));
			fileRequestForLender.setFileName(outputFileNameForLender);
			fileRequestForLender.setFileType(FileType.Agreement);
			fileRequestForLender.setFilePrifix(FileType.Agreement.name());
			try {
				FileResponse putFile = fileManagementService.putFile(fileRequestForLender);

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			File fileForLender = new File(generatedPdfForLender);
			boolean fileDeletedForLender = fileForLender.delete();

		}

		manualEsignProcessForDeal(loanIdFormanuvalEsign);

		lenderBorrowerConversationRepo.deleteByLenderUserIdAndBorrowerUserId(lenderUser.getId(), borrowerUser.getId());
		String message = "success";
		return message;
	}

	public String manualEsignProcessForDeal(String loanId) {
		String message = null;
		OxyLoan oxyLoan = oxyLoanRepo.findByLoanId(loanId);

		if (oxyLoan == null) {
			throw new NoSuchElementException("No Loan present to esign against");
		}

		int borrowerParentRequestId = oxyLoan.getBorrowerParentRequestId();
		double disburmentAmount = oxyLoan.getDisbursmentAmount();

		LoanRequest loanRequest = loanRequestRepo.findByLoanId(loanId);
		if (loanRequest == null) {
			throw new NoSuchElementException("No Loan present to esign against");
		}
		LoanRequest loanRequestDetails = loanRequestRepo.findById(borrowerParentRequestId).get();
		if (loanRequestDetails.getLoanStatus() == LoanStatus.Requested) {
			Double totalDisbursedAmount = loanRequestDetails.getDisbursmentAmount() + disburmentAmount;
			if (loanRequestDetails.getLoanOfferedAmount() != null) {
				if (totalDisbursedAmount <= loanRequestDetails.getLoanOfferedAmount().getLoanOfferedAmount()) {
					loanRequestDetails.setDisbursmentAmount(totalDisbursedAmount);
					loanRequestRepo.save(loanRequestDetails);
				} else {
					throw new OperationNotAllowedException(
							"Total disbursed amount is greater than offered amount please check",
							ErrorCodes.LIMIT_REACHED);
				}
			} else {
				loanRequestDetails.setDisbursmentAmount(totalDisbursedAmount);
				loanRequestRepo.save(loanRequestDetails);
			}

		}

		if (oxyLoan.getLoanStatus() == LoanStatus.Agreed && oxyLoan.getBorrowerDisbursedDate() == null) {

			oxyLoan.setLoanStatus(LoanStatus.Hold);
			oxyLoan.setLoanActiveDate(new Date());
			oxyLoan.setLenderEsignId(0000);
			oxyLoan.setBorrowerEsignId(0000);
			loanRequest.setAdminComments("APPROVED");

			/*
			 * if (oxyLoan.getDurationType().equals(DurationType.Months)) {
			 * generateEmiCard(oxyLoan, loanRequest); }
			 */

			oxyLoan = oxyLoanRepo.save(oxyLoan);
			loanRequest = loanRequestRepo.save(loanRequest);

		}

		// generateENACHMandate(oxyLoan);
		message = "success";
		return message;

	}

	public double generateEmiCard(OxyLoan oxyLoan, LoanRequest loanRequest) {
		List<LoanEmiCard> loanEmiCards = new ArrayList<LoanEmiCard>();
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(this.defaultEmiDay));
		double emiPrincipalAmount = 0d;
		double emiInterstAmount = 0d;

		double interstAmountForLender = 0.0;
		double principalAmountForLender = 0.0;
		double emiAmountForLender = 0.0;
		int duration = 0;
		if (oxyLoan.getRepaymentMethod().equals(LoanRequest.RepaymentMethod.I)) {
			duration = loanRequest.getDuration() + 1;
		} else {
			duration = loanRequest.getDuration();
		}
		Date lastEmiDate = null;
		for (int i = 1; i <= duration; i++) {

			calendar.add(Calendar.MONTH, 1);

			LoanEmiCard loanEmiCard = new LoanEmiCard();
			loanEmiCard.setLoanId(oxyLoan.getId());
			loanEmiCard.setEmiNumber(i);
			loanEmiCard.setEmiDueOn(calendar.getTime());
			int borrowerParentid = oxyLoan.getBorrowerParentRequestId();
			LoanRequest loanRequestDetails = loanRequestRepo.findById(borrowerParentid).get();
			if (loanRequestDetails != null) {

				if (loanRequestDetails.getRepaymentMethodForLender().equalsIgnoreCase("PI")) {
					interstAmountForLender = (oxyLoan.getDisbursmentAmount()
							* (loanRequestDetails.getRateOfInterestToLender())) / 100d;
					principalAmountForLender = oxyLoan.getDisbursmentAmount() / oxyLoan.getDuration().doubleValue();
					emiAmountForLender = interstAmountForLender + principalAmountForLender;
				} else {
					if (loanRequestDetails.getRepaymentMethodForLender().equalsIgnoreCase("I")) {

						interstAmountForLender = (oxyLoan.getDisbursmentAmount()
								* (loanRequestDetails.getRateOfInterestToLender())) / 100d;
						if (i == loanRequestDetails.getDurationBySir()) {
							lastEmiDate = loanEmiCard.getEmiDueOn();
							System.out.println("lastEmiDate" + lastEmiDate);
						}
						if (i == duration) {
							Calendar c = Calendar.getInstance();
							c.setTime(lastEmiDate);
							c.add(Calendar.DATE, 1);
							Date principalDate = c.getTime();
							loanEmiCard.setEmiDueOn(principalDate);
							principalAmountForLender = oxyLoan.getDisbursmentAmount();
							interstAmountForLender = 0.0;
						}

						emiAmountForLender = interstAmountForLender + principalAmountForLender;

					}

				}

				if (loanRequestDetails.getRepaymentMethodForBorrower().equalsIgnoreCase("PI")) {
					double interestAmount = oxyLoan.getDisbursmentAmount()
							* (loanRequestDetails.getRateOfInterestToBorrower()) / 100;
					double principalAmount = oxyLoan.getDisbursmentAmount() / oxyLoan.getDuration();
					loanEmiCard.setEmiInterstAmount(Math.round(interestAmount));
					loanEmiCard.setEmiPrincipalAmount(Math.round(principalAmount));
					loanEmiCard.setEmiAmount(Math.round(interestAmount + principalAmount));

				} else {
					double interestAmountOfI = oxyLoan.getDisbursmentAmount()
							* (loanRequestDetails.getRateOfInterestToBorrower()) / 100;
					double principalAmountOfI = 0.0;
					if (i == loanRequestDetails.getDurationBySir()) {
						lastEmiDate = loanEmiCard.getEmiDueOn();
						System.out.println("lastEmiDate" + lastEmiDate);
					}
					if (i == duration) {
						principalAmountOfI = oxyLoan.getDisbursmentAmount();
						interestAmountOfI = 0.0;
						Calendar c = Calendar.getInstance();
						c.setTime(lastEmiDate);
						c.add(Calendar.DATE, 1);
						Date principalDate = c.getTime();
						loanEmiCard.setEmiDueOn(principalDate);
					}

					loanEmiCard.setEmiInterstAmount(Math.round(interestAmountOfI));
					loanEmiCard.setEmiPrincipalAmount(Math.round(principalAmountOfI));
					loanEmiCard.setEmiAmount(Math.round(interestAmountOfI + principalAmountOfI));
				}

			}
			loanEmiCard.setLenderEmiPrincipalAmount(Math.round(principalAmountForLender));
			loanEmiCard.setLenderEmiInterestAmount(Math.round(interstAmountForLender));
			loanEmiCard.setLenderEmiAmount(Math.round(emiAmountForLender));
			loanEmiCards.add(loanEmiCard);
		}

		loanEmiCardRepo.saveAll(loanEmiCards);
		return emiPrincipalAmount + emiInterstAmount;
	}

	public void generateENACHMandate(OxyLoan loanDetails) {
		double maxAmount = 0;
		double totalAmount = 0;
		Date startDate = null;
		Date endDate = null;
		Date endDateForInterest = null;
		double finalAmount = 0;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		Set<Integer> loanIds = new HashSet<Integer>();
		String loanId = loanDetails.getLoanId();
		StringBuilder converting = new StringBuilder(loanId);
		String convertedLoanId = converting.delete(0, 2).toString();

		List<LoanEmiCard> listLoanEmicard = loanEmiCardRepo
				.findByLoanIdOrderByEmiNumber(Integer.parseInt(convertedLoanId));
		OxyLoan oxyLoanDetails = oxyLoanRepo.findByLoanId(loanId);
		for (LoanEmiCard emis : listLoanEmicard) {

			if (emis.getEmiNumber() == 1) {
				maxAmount = emis.getEmiAmount();
			}
			if (emis.getEmiNumber() == 1) {
				startDate = emis.getEmiDueOn();
			}
			if (emis.getEmiNumber() == oxyLoanDetails.getDuration()) {
				endDateForInterest = emis.getEmiDueOn();
			}
			if (emis.getEmiNumber() == listLoanEmicard.size()) {

				endDate = emis.getEmiDueOn();
			}
		}
		totalAmount = loanDetails.getDisbursmentAmount();

		String txnId = null;
		if (oxyLoanDetails.getRepaymentMethod().equals(RepaymentMethod.PI)) {
			txnId = "ECST" + dateUtil.getHHmmMMddyyyyDateFormat() + loanDetails.getBorrowerUserId()
					+ loanDetails.getId();
			dataToEnachMandate(oxyLoanDetails, startDate, endDate, maxAmount, txnId, "Monthly");
		} else {
			for (int i = 0; i <= 1; i++) {
				if (i == 0) {
					txnId = "ECST" + dateUtil.getHHmmMMddyyyyDateFormat() + loanDetails.getBorrowerUserId()
							+ loanDetails.getId() + i;

					dataToEnachMandate(oxyLoanDetails, startDate, endDateForInterest, maxAmount, txnId, "Monthly");
				} else {
					String txn = "ECST" + dateUtil.getHHmmMMddyyyyDateFormat() + loanDetails.getBorrowerUserId()
							+ loanDetails.getId() + i;
					Calendar c = Calendar.getInstance();
					c.setTime(endDate);
					c.add(Calendar.DATE, 1);
					Date principalEndDate = c.getTime();

					dataToEnachMandate(oxyLoanDetails, endDate, principalEndDate, totalAmount, txn, "Daily");
				}
			}

		}

	}

	public void dataToEnachMandate(OxyLoan oxyLoanDetails, Date startDate, Date endDate, double maxAmount, String txnId,
			String enachType) {
		EnachMandate enachMandate = new EnachMandate();

		if (oxyLoanDetails.getDurationType().equals(DurationType.Months)) {
			enachMandate.setOxyLoanId(oxyLoanDetails.getId());
			enachMandate.setOxyLoan(oxyLoanDetails);

			enachMandate.setAmountType(AmountType.valueOf(EnachMandate.AmountType.M.name()));
			if (enachType.equalsIgnoreCase("Monthly")) {
				enachMandate.setFrequency(Frequency.valueOf(EnachMandate.Frequency.MNTH.name()));
			} else {
				enachMandate.setFrequency(Frequency.valueOf(EnachMandate.Frequency.DAIL.name()));
			}

			enachMandate.setDebitStartDate(startDate);

			enachMandate.setDebitEndDate(endDate);
			enachMandate.setMandateTransactionId(txnId);
			enachMandate.setMaxAmount(maxAmount);
			enachMandate.setMandateStatus(MandateStatus.INITIATED);
			enachMandate = enachMandateRepo.save(enachMandate);
			String mailSubjectForAdmin = "Enach"; // Mail Goes to admin
			String mailTempalteForAdmin = "enach-mail-to-admin.template";

			TemplateContext templateContext = new TemplateContext();
			templateContext.put("LoanId", oxyLoanDetails.getId());

			templateContext.put("AmountType", AmountType.valueOf(EnachMandate.AmountType.M.name()));
			templateContext.put("Frequeny", enachMandate.getFrequency());

			templateContext.put("EmiStartDate", expectedDateFormat.format(startDate));
			if (endDate != null) {
				templateContext.put("EmiEndDate", expectedDateFormat.format(endDate));
			}

			templateContext.put("TransactionId", txnId);
			templateContext.put("MaxAmount", Math.round(maxAmount));
			templateContext.put("MandateStatus", MandateStatus.INITIATED);
			Date date = new Date();
			String date1 = expectedDateFormat.format(date);
			templateContext.put("CurrentDate", date1);
			User user = userRepo.findById(oxyLoanDetails.getBorrowerUserId()).get();
			EmailRequest emailRequest = new EmailRequest(
					new String[] { "archana.n@oxyloans.com", "subbu@oxyloans.com", user.getEmail() },
					mailSubjectForAdmin, mailTempalteForAdmin, templateContext);
			EmailResponse emailResponse = emailService.sendEmail(emailRequest);
			if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
				throw new RuntimeException(emailResponse.getErrorMessage());
			}

		}

	}

	@Override
	@Transactional
	public PartnerRegistrationResponseDto partnerFundsTransfer(int borrowerId) {
		PartnerRegistrationResponseDto partnerRegistrationResponseDto = new PartnerRegistrationResponseDto();
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(borrowerId);
		if (loanRequest != null) {
			Double amountToTransfer = oxyLoanRepo.findingSumOfAmountDisbursedBasedOnParentId(loanRequest.getId());
			if (amountToTransfer != null) {

				OxyCmsLoans oxyCmsExecutedStatus = oxyCmsLoansRepo.amountInExecutedState(borrowerId);
				if (oxyCmsExecutedStatus != null) {

					throw new OperationNotAllowedException("Funds already transfered", ErrorCodes.ACTION_ALREDY_DONE);

				}
				OxyCmsLoans oxyCmsFile = oxyCmsLoansRepo.amountInInitiatedState(borrowerId);
				if (oxyCmsFile != null) {
					throw new OperationNotAllowedException(
							"Excel already generated for the existing amount of " + oxyCmsFile.getAmount(),
							ErrorCodes.ACTION_ALREDY_DONE);
				} else {

					User userDetails = userRepo.findById(borrowerId).get();
					if (userDetails != null) {
						OxyUserType oxyType = oxyUserTypeRepo
								.getDetailsByUtmNameAndType(userDetails.getUrchinTrackingModule());
						if (oxyType != null) {
							double processingFee = (amountToTransfer * oxyType.getBorrowerProcessingFee()) / 100;
							double gstValue = (processingFee * 18) / 100;
							double totalFee = processingFee + gstValue;
							double remainingLoanAmount = amountToTransfer - totalFee;
							IciciCmsRequestDto obj = new IciciCmsRequestDto();

							obj.setAccountNumber("50200025316770");
							obj.setName("SRS FINTECHLABS PVT LTD");
							obj.setIfscCode("HDFC0004277");
							obj.setAmount(totalFee);
							obj.setDebitAccountNumber("777705849441");
							obj.setPaymentType("BORROWERFEE");
							obj.setUserUserId("BR" + borrowerId);
							obj.setAddDetails5("OXYLOANS BorrowerFee");
							obj.setRemarks("PARTNER");
							obj.setDealId(0);
							Date date = new Date();
							String currentDate = FORMAT.format(date);

							StatusResponseDto borrowerFeeResponse = excelServiceRepo.witeCmsExcel("OXYLR2_OXYL2UPLD_",
									obj, currentDate, "Partner");

							OxyCmsLoans cmsLoans = new OxyCmsLoans();
							cmsLoans.setUserId(borrowerId);
							cmsLoans.setDealId(0);
							cmsLoans.setAmount(remainingLoanAmount);
							cmsLoans.setTransactionDate(new Date());
							cmsLoans.setTransactionStatus(FileExecutionStatus.INITIATED);
							cmsLoans.setBorrowerCameFrom(OxyCmsLoans.BorrowerCameFrom.PARTNER);
							cmsLoans.setBorrowerFee(totalFee);
							cmsLoans.setFileNameBorrowerfee(borrowerFeeResponse.getFileName());
							oxyCmsLoansRepo.save(cmsLoans);

							UserRequest userRequest = new UserRequest();
							userRequest.setNameAsPerBank(userDetails.getBankDetails().getUserName());
							userRequest.setAccountNumber(userDetails.getBankDetails().getAccountNumber());
							userRequest.setIfscCode(userDetails.getBankDetails().getIfscCode());

							StatusResponseDto response = excelServiceRepo.witeDealLevelLoanDisbursmentExcel(currentDate,
									userRequest, cmsLoans, "Partner");
							if (response != null) {

								cmsLoans.setFileName(response.getFileName());
								oxyCmsLoansRepo.save(cmsLoans);
							}
							partnerRegistrationResponseDto.setStatus("ExcelGenerated");
						}
					}

				}

			} else {
				throw new OperationNotAllowedException("No amount to transfer", ErrorCodes.ENITITY_NOT_FOUND);
			}
		}
		return partnerRegistrationResponseDto;

	}

	@Override
	@Transactional
	public LoanResponseStatusUpdationRequestDto lendersWalletAmount(String partnerUtm) {
		LoanResponseStatusUpdationRequestDto loanResponseStatusUpdationRequestDto = new LoanResponseStatusUpdationRequestDto();
		double loanRequestAmount = 0.0;
		double totalWallet = lendersMappedToPartnerTotalWalletSum(partnerUtm, loanRequestAmount);
		loanResponseStatusUpdationRequestDto
				.setTotalLendersWalletAmount(BigDecimal.valueOf(totalWallet).toBigInteger());
		return loanResponseStatusUpdationRequestDto;

	}
}
