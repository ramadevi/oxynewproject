package com.oxyloans.service.emailservice;

import com.oxyloans.engine.template.TemplateContext;

public class EmailRequest {

	public static final String FROM_MAIL = "admin@oxyloans.com";
	public static final String OXYBRICKS_FROM_MAIL = "radha@oxybricks.world";
	public static final String STUDENT_FROM_MAIL = "sriram@studentx.world";
	public static final String BMV_MAIL = "Hi@BMV.money";

	private String[] toEmails;

	private String[] ccEmails;

	private String emailTemplateName;

	private TemplateContext emailTemplateParams;

	private String subject;

	private String[] attachmentPaths;

	private String displayerName;

	private String fromMail;

	public String[] getAttachmentPaths() {
		return attachmentPaths;
	}

	public void setAttachmentPaths(String[] attachmentPaths) {
		this.attachmentPaths = attachmentPaths;
	}

	public EmailRequest(String[] toEmails, String subject, String emialBody) {
		super();
		this.toEmails = toEmails;
		this.subject = subject;
		this.emialBody = emialBody;
	}

	private String emialBody;

	public String getEmialBody() {
		return emialBody;
	}

	public void setEmialBody(String emialBody) {
		this.emialBody = emialBody;
	}

	public EmailRequest(String[] toEmails, String subject, String emailTemplateName,
			TemplateContext emailTemplateParams) {
		this.toEmails = toEmails;
		this.subject = subject;
		this.emailTemplateName = emailTemplateName;
		this.emailTemplateParams = emailTemplateParams;
	}

	public String[] getToEmails() {
		return toEmails;
	}

	public String[] getCcEmails() {
		return ccEmails;
	}

	public void setCcEmails(String[] ccEmails) {
		this.ccEmails = ccEmails;
	}

	public String getEmailTemplateName() {
		return emailTemplateName;
	}

	public TemplateContext getEmailTemplateParams() {
		return emailTemplateParams;
	}

	public String getSubject() {
		return subject;
	}

	public EmailRequest(String[] toEmails, String emailTemplateName, TemplateContext emailTemplateParams,
			String subject, String[] attachmentPaths) {
		super();
		this.toEmails = toEmails;
		this.emailTemplateName = emailTemplateName;
		this.emailTemplateParams = emailTemplateParams;
		this.subject = subject;
		this.attachmentPaths = attachmentPaths;

	}

	public String getDisplayerName() {
		return displayerName;
	}

	public void setDisplayerName(String displayerName) {
		this.displayerName = displayerName;
	}

	public EmailRequest(String[] toEmails, String[] ccEmails, String emailTemplateName,
			TemplateContext emailTemplateParams, String subject, String displayerName) {
		super();
		this.toEmails = toEmails;
		this.ccEmails = ccEmails;
		this.emailTemplateName = emailTemplateName;
		this.emailTemplateParams = emailTemplateParams;
		this.subject = subject;
		this.displayerName = displayerName;
	}

	public EmailRequest(String[] toEmails, String emailTemplateName, TemplateContext emailTemplateParams,
			String mailSubject, String displayerName) {
		super();
		this.toEmails = toEmails;
		this.emailTemplateName = emailTemplateName;
		this.emailTemplateParams = emailTemplateParams;
		this.subject = mailSubject;
		this.displayerName = displayerName;
	}

	public EmailRequest(String[] toEmails, String emailTemplateName, TemplateContext emailTemplateParams,
			String mailSubject, String displayerName, String[] attachmentPaths) {
		super();
		this.toEmails = toEmails;
		this.emailTemplateName = emailTemplateName;
		this.emailTemplateParams = emailTemplateParams;
		this.subject = mailSubject;
		this.displayerName = displayerName;
		this.attachmentPaths = attachmentPaths;
	}

	public String getFromMail() {
		return fromMail;
	}

	public void setFromMail(String fromMail) {
		this.fromMail = fromMail;
	}
}
