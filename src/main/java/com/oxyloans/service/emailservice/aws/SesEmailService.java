package com.oxyloans.service.emailservice.aws;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.oxyloans.service.emailservice.EmailResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.oxyloans.engine.template.ITemplateEngine;
import com.oxyloans.service.emailservice.AbstractEmailService;
import com.oxyloans.service.emailservice.EmailRequest;

public class SesEmailService extends AbstractEmailService {

	private static final Logger logger = LogManager.getLogger(SesEmailService.class);

	public SesEmailService(ITemplateEngine templateEngine) {
		super(templateEngine);
	}

	@Override
	public EmailResponse sendEmail(EmailRequest emailRequest, String emailBody) {
		try {
			if (emailRequest.getDisplayerName() != null) {
				Session session = Session.getDefaultInstance(new Properties(), null);
				MimeMessage message = new MimeMessage(session);
				// subject
				message.setSubject(emailRequest.getSubject(), "UTF-8");

				// sender and receiver
				if (emailRequest.getFromMail() != null) {

					if (emailRequest.getFromMail().equalsIgnoreCase("oxybricks")) {

						message.setFrom(
								new InternetAddress(EmailRequest.OXYBRICKS_FROM_MAIL, emailRequest.getDisplayerName()));
					}

					if (emailRequest.getFromMail().equalsIgnoreCase("student")) {

						message.setFrom(
								new InternetAddress(EmailRequest.STUDENT_FROM_MAIL, emailRequest.getDisplayerName()));
					}

					if (emailRequest.getFromMail().equalsIgnoreCase("bmv")) {

						message.setFrom(new InternetAddress(EmailRequest.BMV_MAIL, emailRequest.getDisplayerName()));
					}
					if (emailRequest.getFromMail().equalsIgnoreCase("erice")) {

						message.setFrom(new InternetAddress(EmailRequest.BMV_MAIL, emailRequest.getDisplayerName()));
					}

				} else {
					message.setFrom(new InternetAddress(EmailRequest.FROM_MAIL, emailRequest.getDisplayerName()));
				}
				// To Address
				InternetAddress[] toAddress = new InternetAddress[emailRequest.getToEmails().length];
				for (int i = 0; i < emailRequest.getToEmails().length; i++) {
					toAddress[i] = new InternetAddress(emailRequest.getToEmails()[i]);
				}
				message.setRecipients(javax.mail.Message.RecipientType.TO, toAddress);

				// CC Address
				if (emailRequest.getCcEmails() != null) {
					InternetAddress[] ccAddress = new InternetAddress[emailRequest.getCcEmails().length];
					for (int i = 0; i < emailRequest.getCcEmails().length; i++) {
						ccAddress[i] = new InternetAddress(emailRequest.getCcEmails()[i]);
					}

					message.setRecipients(javax.mail.Message.RecipientType.CC, ccAddress);
				}

				MimeMultipart msg_body = new MimeMultipart("alternative");
				MimeBodyPart wrap = new MimeBodyPart();

				// define the text part
				MimeBodyPart textPart = new MimeBodyPart();
				textPart.setContent(emailBody, "text/plain; charset=UTF-8");

				MimeBodyPart htmlPart = new MimeBodyPart();
				htmlPart.setContent(emailBody, "text/html; charset=UTF-8");

				msg_body.addBodyPart(textPart);
				msg_body.addBodyPart(htmlPart);
				wrap.setContent(msg_body);

				MimeMultipart msg = new MimeMultipart("mixed");
				message.setContent(msg);
				msg.addBodyPart(wrap);

				AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard()
						.withRegion(Regions.US_EAST_1).build();

				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				message.writeTo(outputStream);

				RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
				SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
				// .withConfigurationSetName(CONFIGURATION_SET);

				client.sendRawEmail(rawEmailRequest);
				logger.debug("Email sent! to {}", Arrays.asList(emailRequest.getToEmails()));

				return new EmailResponse(EmailResponse.Status.SUCCESS);

			} else {
				AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard()
						.withRegion(Regions.US_EAST_1).build();
				SendEmailRequest request = new SendEmailRequest()
						.withDestination(new Destination().withToAddresses(emailRequest.getToEmails()))
						.withMessage(new Message()
								.withBody(new Body().withHtml(new Content().withCharset("UTF-8").withData(emailBody)))
								.withSubject(new Content().withCharset("UTF-8").withData(emailRequest.getSubject())))
						.withSource(EmailRequest.FROM_MAIL);
				client.sendEmail(request);

				logger.debug("Email sent! to {}", Arrays.asList(emailRequest.getToEmails()));
			}
		} catch (

		Exception ex) {
			System.out.println("The email was not sent. Error message: " + ex.getMessage());
			ex.printStackTrace();
		}
		return new EmailResponse(EmailResponse.Status.SUCCESS);
	}

	@Override
	public EmailResponse sendEmailWithAttachment(EmailRequest emailRequest, String emailBody, ArrayList<File> file) {

		try {
			Session session = Session.getDefaultInstance(new Properties(), null);
			MimeMessage message = new MimeMessage(session);
			logger.info("message1@@@@@:"+message);
			// subject
			message.setSubject(emailRequest.getSubject(), "UTF-8");

			// sender and receiver
			if (emailRequest.getFromMail() != null) {

				if (emailRequest.getFromMail().equalsIgnoreCase("oxybricks")) {

					message.setFrom(
							new InternetAddress(EmailRequest.OXYBRICKS_FROM_MAIL, emailRequest.getDisplayerName()));
				}

				if (emailRequest.getFromMail().equalsIgnoreCase("student")) {

					message.setFrom(
							new InternetAddress(EmailRequest.STUDENT_FROM_MAIL, emailRequest.getDisplayerName()));
				}

				if (emailRequest.getFromMail().equalsIgnoreCase("bmv")) {

					message.setFrom(
							new InternetAddress(EmailRequest.BMV_MAIL, emailRequest.getDisplayerName()));
				}

			} else {
				message.setFrom(new InternetAddress(EmailRequest.FROM_MAIL, emailRequest.getDisplayerName()));
			}

			// To Address
			InternetAddress[] toAddress = new InternetAddress[emailRequest.getToEmails().length];
			for (int i = 0; i < emailRequest.getToEmails().length; i++) {
				toAddress[i] = new InternetAddress(emailRequest.getToEmails()[i]);
			}
			message.setRecipients(javax.mail.Message.RecipientType.TO, toAddress);

			// CC Address
			if (emailRequest.getCcEmails() != null) {
				InternetAddress[] ccAddress = new InternetAddress[emailRequest.getCcEmails().length];
				for (int i = 0; i < emailRequest.getCcEmails().length; i++) {
					ccAddress[i] = new InternetAddress(emailRequest.getCcEmails()[i]);
				}

				message.setRecipients(javax.mail.Message.RecipientType.CC, ccAddress);
			}

			MimeMultipart msg_body = new MimeMultipart("alternative");
			MimeBodyPart wrap = new MimeBodyPart();

			// define the text part
			MimeBodyPart textPart = new MimeBodyPart();
			textPart.setContent(emailBody, "text/plain; charset=UTF-8");

			MimeBodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(emailBody, "text/html; charset=UTF-8");

			msg_body.addBodyPart(textPart);
			msg_body.addBodyPart(htmlPart);
			wrap.setContent(msg_body);

			MimeMultipart msg = new MimeMultipart("mixed");
			message.setContent(msg);
			msg.addBodyPart(wrap);

			for (File fileName : file) {
				MimeBodyPart att = new MimeBodyPart();
				DataSource fds = new FileDataSource(fileName.getAbsolutePath());
				att.setDataHandler(new DataHandler(fds));
				att.setFileName(fds.getName());
				msg.addBodyPart(att);
			}

			AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard()
					.withRegion(Regions.US_EAST_1).build();

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			message.writeTo(outputStream);
			logger.info("message2@@@@@:"+message);

			RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
			SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
			// .withConfigurationSetName(CONFIGURATION_SET);

			client.sendRawEmail(rawEmailRequest);
			logger.debug("Email sent! to {}", Arrays.asList(emailRequest.getToEmails()));
		} catch (Exception ex) {
			System.out.println("The email was not sent. Error message: " + ex.getMessage());
			ex.printStackTrace();
		}

		return new EmailResponse(EmailResponse.Status.SUCCESS);
	}
}
