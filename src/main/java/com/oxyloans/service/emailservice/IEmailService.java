package com.oxyloans.service.emailservice;

public interface IEmailService {
	
	public EmailResponse sendEmail(EmailRequest emailRequest);

}
