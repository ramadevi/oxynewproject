package com.oxyloans.service.emailservice;

public class EmailResponse {
	
	public static enum Status {
		SUCCESS, FAILED, PARTIAL_SUCCESS
	}
	
	private Status status;
	
	private String[] failedEmails;
	
	private String errorMessage;
	
	public EmailResponse(Status status) {
		this.status = status;
	}

	public String[] getFailedEmails() {
		return failedEmails;
	}

	public void setFailedEmails(String[] failedEmails) {
		this.failedEmails = failedEmails;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Status getStatus() {
		return status;
	}
	
}
