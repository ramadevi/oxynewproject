package com.oxyloans.service.emailservice;

import java.io.File;
import java.util.ArrayList;

import com.oxyloans.engine.template.ITemplateEngine;

public abstract class AbstractEmailService implements IEmailService {

	private ITemplateEngine templatEngine;

	public AbstractEmailService(ITemplateEngine templateEngine) {
		this.templatEngine = templateEngine;
	}

	@Override
	public EmailResponse sendEmail(EmailRequest emailRequest) {

		ArrayList<File> fileList = new ArrayList<>();

		if (emailRequest.getEmialBody() != null && emailRequest.getAttachmentPaths() == null) {

			return sendEmail(emailRequest, emailRequest.getEmialBody());

		} else if (emailRequest.getEmialBody() == null && emailRequest.getAttachmentPaths() == null) {
			String emailBody = "";

			if (emailRequest.getEmailTemplateName() != null) {
				emailBody = templatEngine.generateContent(emailRequest.getEmailTemplateName(),
						emailRequest.getEmailTemplateParams());

			}

			return sendEmail(emailRequest, emailBody);

		} else if (emailRequest.getEmialBody() != null && emailRequest.getAttachmentPaths() != null) {
			for (String path : emailRequest.getAttachmentPaths()) {
				File f = new File(path);
				fileList.add(f);
			}

			return sendEmailWithAttachment(emailRequest, emailRequest.getEmialBody(), fileList);
		} else if (emailRequest.getEmialBody() == null && emailRequest.getAttachmentPaths() != null) {
			String emailBody = "";

			if (emailRequest.getEmailTemplateName() != null) {
				emailBody = templatEngine.generateContent(emailRequest.getEmailTemplateName(),
						emailRequest.getEmailTemplateParams());
				for (String path : emailRequest.getAttachmentPaths()) {
					File f = new File(path);
					fileList.add(f);
				}

			}

			return sendEmailWithAttachment(emailRequest, emailBody, fileList);
		}
		return null;

	}

	public abstract EmailResponse sendEmail(EmailRequest emailRequest, String emailBody);

	public abstract EmailResponse sendEmailWithAttachment(EmailRequest emailRequest, String emailBody,
			ArrayList<File> file);

}
