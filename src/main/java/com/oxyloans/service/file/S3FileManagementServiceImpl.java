package com.oxyloans.service.file;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Base64;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.oxyloans.file.FileRequest;
import com.oxyloans.file.FileResponse;

@Service
public class S3FileManagementServiceImpl implements IFileManagementService {

	private static final Logger logger = LogManager.getLogger(S3FileManagementServiceImpl.class);

	@Value("${bucketName}")
	private String bucketName;
	
	AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withCredentials(new ProfileCredentialsProvider())
			.withRegion(Regions.AP_SOUTH_1).build();


	@Override
	public FileResponse putFile(FileRequest fileRequest) throws IOException {

		byte[] bytes = toByteArray(fileRequest.getInputStream());

		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(bytes.length);
		InputStream Content = new ByteArrayInputStream(bytes);
		PutObjectResult result = null;
		String filePath = null;
		try {
		logger.info("put file from S3FileManagementServiceImpl " + fileRequest.getUserId());
		PutObjectRequest putObjectRequest = new PutObjectRequest(getBucketName(fileRequest), getFullFileName(fileRequest), Content, metadata);
		logger.info("File Name S3FileManagementServiceImpl " + getFullFileName(fileRequest)+"     "+getBucketName(fileRequest));
		result = s3Client.putObject(putObjectRequest);

		filePath = "S3://" + getBucketName(fileRequest) + "/" + getFullFileName(fileRequest);
		
		logger.info("After service call S3FileManagementServiceImpl ");
		}catch(Exception e) {
			logger.info("After service call S3FileManagementServiceImpl "+e);
		}

		FileResponse fileResponse = new FileResponse();
		fileResponse.setFileName(fileRequest.getFileName());
		fileResponse.setFilePath(filePath);
		return fileResponse;
	}

	private String getBucketName(FileRequest fileRequest) {
		String filePath;
		filePath = this.bucketName + (fileRequest.getFileType().getBucketPrefix() != null ? "-" + fileRequest.getFileType().getBucketPrefix() : "");
		return filePath;
	}

	private String getFullFileName(FileRequest fileRequest) {
		String filePath;
		filePath = (fileRequest.getUserId() != null ? fileRequest.getUserId() + "/" : "") + fileRequest.getFilePrifix() + "_" + fileRequest.getFileName();
		return filePath;
	}

	@Override
	public FileResponse getFile(FileRequest fileRequest) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MILLISECOND, 60 * 60 * 1000);
		String bucketName = getBucketName(fileRequest);
		String key = getFullFileName(fileRequest);
		logger.info("generating the download link for the bucket {} and file name {}", bucketName, key);
		GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, key)
				.withMethod(HttpMethod.GET).withExpiration(calendar.getTime());
		URL generatePresignedUrl = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
		FileResponse fileResponse = new FileResponse();
		fileResponse.setFileName(key);
		fileResponse.setFilePath(bucketName);
		try {
			fileResponse.setDownloadUrl(URLDecoder.decode(generatePresignedUrl.toString(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("encoding issue");
		}
		return fileResponse;
	}

	@Override
	public Boolean deleteFile(FileRequest fileRequest) {
		if (s3Client.doesObjectExist(bucketName,
				fileRequest.getUserId() + "/" + fileRequest.getFilePrifix() + "_" + fileRequest.getFileName())) {
			s3Client.deleteObject(bucketName,
					fileRequest.getUserId() + "/" + fileRequest.getFilePrifix() + "_" + fileRequest.getFileName());
			return true;
		} else {
			return false;
		}
	}

	public static byte[] toByteArray(InputStream in) throws IOException {

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len;
		while ((len = in.read(buffer)) != -1) {
			os.write(buffer, 0, len);
		}
		return os.toByteArray();
	}
	
	public static void main(String[] args) throws IOException {
		File file = new File("/Users/damodharreddy/Downloads/Agreement_LN132.pdf");
		byte[] readFileToString = FileUtils.readFileToByteArray(file);
		System.out.println(Base64.getEncoder().encodeToString(readFileToString));
	}

}
