package com.oxyloans.service.file;

import java.io.IOException;

import com.oxyloans.file.FileRequest;
import com.oxyloans.file.FileResponse;

public interface IFileManagementService {
	
	public FileResponse putFile(FileRequest fileRequest) throws IOException;

	public FileResponse getFile(FileRequest fileRequest);

	public Boolean deleteFile(FileRequest fileRequest);

}
