package com.oxyloans.service.enach;

public class EnachMandateReponseRequestDto {

    private String mandateResponse;

    public String getMandateResponse() {
        return mandateResponse;
    }

    public void setMandateResponse(String mandateResponse) {
        this.mandateResponse = mandateResponse;
    }

}
