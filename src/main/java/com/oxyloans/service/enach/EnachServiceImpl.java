package com.oxyloans.service.enach;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.regex.Pattern;

import javax.transaction.Transactional;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.entity.enach.AppLevelEnachMandateExceptionResponse;
import com.oxyloans.repository.enach.AppLevelEnachMandateExceptionResponseRepo;
import com.oxyloans.entity.enach.AppLevelEnachMandateResponse;
import com.oxyloans.repository.enach.AppLevelEnachMandateResponseRepo;
import com.oxyloans.entity.enach.ApplicationLevelEnachMandate;
import com.oxyloans.entity.enach.ApplicationLevelEnachMandate.MandateStatus;
import com.oxyloans.repository.enach.ApplicationLevelEnachMandateRepo;
import com.oxyloans.repository.enach.EmandateResponseRepo;
import com.oxyloans.entity.enach.EnachMandate;
import com.oxyloans.entity.enach.EnachMandateExceptionResponse;
import com.oxyloans.repository.enach.EnachMandateExceptionResponseRepo;
import com.oxyloans.entity.enach.EnachMandateResponse;
import com.oxyloans.entity.loan.LoanRequest;
import com.oxyloans.entity.loan.OxyLoan;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.repo.enach.EnachMandateLoansRepo;
import com.oxyloans.repo.enach.EnachMandateRepo;
import com.oxyloans.repo.loan.LoanEmiCardRepo;
import com.oxyloans.repo.loan.OxyLoanRepo;
import com.oxyloans.repo.loan.OxyLoanRequestRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.security.authorization.IAuthorizationService;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.service.loan.ActionNotAllowedException;
import com.oxyloans.service.loan.BorrowerLoanService;

@Service
public class EnachServiceImpl implements IEnachService {

	private final Logger logger = LogManager.getLogger(getClass());

	private static final String DATE_FORMAT = "dd-MM-yyyy";

	@Value("${merchantId}")
	private String merchantId;

	@Value("${enachSalt}")
	private String enachSalt;

	@Value("${enachmandatereturnurl}")
	private String enachMandateRetUrl;

	private String loanIdConstatnt = "LN";

	@Autowired
	private BorrowerLoanService borrowerLoanService;

	@Autowired
	private EnachMandateRepo enachMandateRepo;

	@Autowired
	private EnachMandateLoansRepo enachMandateLoansRepo;

	@Autowired
	private LoanEmiCardRepo loanEmiCardRepo;

	@Autowired
	private OxyLoanRepo oxyLoanRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	protected IAuthorizationService authorizationService;

	@Autowired
	protected OxyLoanRequestRepo loanRequestRepo;

	@Autowired
	private EmandateResponseRepo emandateResponseRepo;

	@Autowired
	private EnachMandateExceptionResponseRepo enachMandateExceptionResponseRepo;
	
	@Autowired
	private AppLevelEnachMandateResponseRepo appLevelEnachMandateResponseRepo;

	@Autowired
	private AppLevelEnachMandateExceptionResponseRepo appLevelEnachMandateExceptionResponseRepo;

	@Value("${enachmandatereturnurlForAppLevel}")
	private String enachmandatereturnurlForAppLevel;
	
	@Autowired
	private ApplicationLevelEnachMandateRepo applicationLevelEnachMandateRepo;


	@Override
	@Transactional
	public ENachMandateResponse generateMandateToken(int userId, int mandateId) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		if (user.getPrimaryType() != PrimaryType.BORROWER) {
			throw new OperationNotAllowedException(
					"As a " + user.getPrimaryType() + " primary type not allowed to perform this",
					ErrorCodes.INVALID_OPERATION);
		}
		EnachMandate enachMandateData = enachMandateRepo.findById(mandateId).get();
		System.out.println("enachMandateData::" + enachMandateData.getId());
		if (enachMandateData == null) {
			throw new ActionNotAllowedException("There are no agreed loans to do eMandate.",
					ErrorCodes.INVALID_OPERATION);
		}

		ENachMandateRequest eNachMandateRequest = new ENachMandateRequest();
		eNachMandateRequest.setTotalAmount(String.valueOf(5));
		// eNachMandateRequest.setTotalAmount(String.valueOf(enachMandateData.getOxyLoan().getDisbursmentAmount()));
		eNachMandateRequest.setDebitStartDate(dateFormat.format(enachMandateData.getDebitStartDate()));
		eNachMandateRequest.setDebitEndDate(dateFormat.format(enachMandateData.getDebitEndDate()));
		eNachMandateRequest.setMaxAmount(String.valueOf(enachMandateData.getMaxAmount()));
		eNachMandateRequest.setAmountType(enachMandateData.getAmountType().name());
		eNachMandateRequest.setFrequency(enachMandateData.getFrequency().name());
		eNachMandateRequest.setAccountNo("");

		ENachMandateResponse response = new ENachMandateResponse();

		String txnId = enachMandateData.getMandateTransactionId();
		String token = genearteToken(txnId, eNachMandateRequest, user, enachMandateData);

		response.setMerchantId(merchantId);
		response.setMandateId(mandateId);
		response.setToken(token);
		response.setEnachMandateRetUrl(enachMandateRetUrl);
		response.setAccountNo(eNachMandateRequest.getAccountNo());
		response.setCapitalTxnId(token.toUpperCase());
		response.setAmountType(enachMandateData.getAmountType().name());
		response.setDebitEndDate(dateFormat.format(enachMandateData.getDebitEndDate()));
		response.setDebitStartDate(dateFormat.format(enachMandateData.getDebitStartDate()));
		response.setFrequency(enachMandateData.getFrequency().name());
		response.setMaxAmount(String.valueOf(enachMandateData.getMaxAmount()));
		response.setTotalAmount(String.valueOf(5));
		// response.setTotalAmount(String.valueOf(enachMandateData.getOxyLoan().getDisbursmentAmount()));
		response.setTxnId(txnId);

		UserResponse userResponse = new UserResponse();
		userResponse.setDisplayId("CONSUMER" + user.getUniqueNumber() + "MNDTID" + enachMandateData.getId());
		userResponse.setMobileNumber(user.getMobileNumber());
		userResponse.setEmail(user.getEmail());

		response.setUser(userResponse);

		return response;
	}

	private String genearteToken(String txnId, ENachMandateRequest eNachMandateRequest, User user,
			EnachMandate enachMandateData) {
		// consumerData.merchantId|consumerData.txnId|
		// totalamount|consumerData.accountNo|consumerData.consumerId|
		// consumerData.consumerMobileNo|consumerData.consumerEmailId |
		// consumerData.debitStartDate|consumerData.debitEndDate|consumerData.maxAmount|
		// consumerData.amountType|consumerData.frequency|
		// consumerData.cardNumber|consumerData.
		// expMonth|consumerData.expYear|consumerData.cvvCode|SALT

		StringBuilder sb = new StringBuilder();
		sb.append(merchantId).append("|");
		sb.append(txnId).append("|");
		sb.append(eNachMandateRequest.getTotalAmount()).append("|");
		sb.append(eNachMandateRequest.getAccountNo()).append("|"); // consumerData.accountNumber
		sb.append("CONSUMER" + user.getUniqueNumber() + "MNDTID" + enachMandateData.getId()).append("|"); // consumerData.consumerId
		sb.append(user.getMobileNumber()).append("|"); // consumerData.consumerMobileNo
		sb.append(user.getEmail()).append("|"); // consumerData.consumerEmailId
		sb.append(eNachMandateRequest.getDebitStartDate()).append("|"); // consumerData.debitStartDate
		sb.append(eNachMandateRequest.getDebitEndDate()).append("|"); // consumerData.debitEndDate
		sb.append(eNachMandateRequest.getMaxAmount()).append("|"); // consumerData.maxAmount
		sb.append(eNachMandateRequest.getAmountType()).append("|"); // consumerData.amountType
		sb.append(eNachMandateRequest.getFrequency()).append("|"); // consumerData.frequency
		sb.append("").append("|"); // consumerData.cardNumber
		sb.append("").append("|"); // consumerData. expMonth
		sb.append("").append("|"); // consumerData.expYear
		sb.append("").append("|"); // consumerData.cvvCode
		sb.append(enachSalt); // SALT
		logger.info("Token returing is {}", sb.toString());
		DigestUtils md = null;
		try {
			md = new DigestUtils(MessageDigest.getInstance("SHA-256"));
		} catch (NoSuchAlgorithmException e) {
			logger.error("Error ", e);
		}
		return md.digestAsHex(sb.toString().getBytes());
	}

	@Override
	public boolean updateEnachMandateReponse(int userId, EnachMandateReponseRequestDto enachMandateReponseRequestDto) {
		boolean res = false;
		logger.info("getMandateResponse {}", enachMandateReponseRequestDto.getMandateResponse());
		String[] emandateResponseArray = enachMandateReponseRequestDto.getMandateResponse().split(Pattern.quote("|"));
		logger.info("emandateResponseArray[3]", emandateResponseArray[3]);
		EnachMandate enachMandateData = enachMandateRepo.findByMandateTransactionId(emandateResponseArray[3]);
		logger.info("emandateResponseArray {}", emandateResponseArray[0]);
		User user = userRepo.findById(userId).get();
		if (enachMandateData != null) {
			if (emandateResponseArray[0] != null) {
				if ("0398".equalsIgnoreCase(emandateResponseArray[0])
						|| "0300".equalsIgnoreCase(emandateResponseArray[0])) {
					EnachMandateResponse enachMandateResponseData = emandateResponseRepo
							.findByEnachMandateId(enachMandateData.getId());
					if (enachMandateResponseData == null) {
						EnachMandateResponse enachMandateResponse = new EnachMandateResponse();
						enachMandateResponse = new EnachMandateResponse();
						enachMandateResponse.setEnachMandateId(enachMandateData.getId());
						enachMandateResponse.setConsumerId(
								"CONSUMER" + user.getUniqueNumber() + "MNDTID" + enachMandateData.getId());
						enachMandateResponse.setTxnStatus(emandateResponseArray[0]);
						enachMandateResponse.setTxnMsg(emandateResponseArray[1].toUpperCase());
						enachMandateResponse.setTxnErrMsg(emandateResponseArray[2]);
						enachMandateResponse.setClntTxnRef(emandateResponseArray[3]);
						enachMandateResponse.setTpslBankCd(emandateResponseArray[4]);
						enachMandateResponse.setTpslTxnId(emandateResponseArray[5]);
						enachMandateResponse
								.setTxnAmt((emandateResponseArray[6] != null && !"NA".equals(emandateResponseArray[6]))
										? Double.parseDouble(emandateResponseArray[6])
										: 0);
						enachMandateResponse.setClntRqstMeta(emandateResponseArray[7]);
						enachMandateResponse.setTpslTxnTime(emandateResponseArray[8]);
						enachMandateResponse
								.setBalAmt((emandateResponseArray[9] != null && !"NA".equals(emandateResponseArray[9]))
										? Double.parseDouble(emandateResponseArray[9])
										: 0);
						enachMandateResponse.setCardId(emandateResponseArray[10]);
						enachMandateResponse.setAliasName(emandateResponseArray[11]);
						enachMandateResponse.setBankTransactionId(emandateResponseArray[12]);
						enachMandateResponse.setMandateRegNo(emandateResponseArray[13]);
						enachMandateResponse.setToken(emandateResponseArray[14]);
						enachMandateResponse.setHash(emandateResponseArray[15]);
						emandateResponseRepo.save(enachMandateResponse);

						if ("0300".equalsIgnoreCase(emandateResponseArray[0])) {
							enachMandateData.setMandateStatus(EnachMandate.MandateStatus.SUCCESS);
							enachMandateRepo.save(enachMandateData);

							int loanId = enachMandateData.getOxyLoanId();
							OxyLoan oxyLoandData = oxyLoanRepo.findById(loanId).get();
							oxyLoandData.setIsECSActivated(Boolean.TRUE);
							LoanRequest loanRequest = loanRequestRepo.findByLoanId(loanIdConstatnt + loanId);
							loanRequest.setIsECSActivated(Boolean.TRUE);
							loanRequestRepo.save(loanRequest);
							oxyLoanRepo.save(oxyLoandData);
						}
						res = true;
					}
				} else if ("0399".equalsIgnoreCase(emandateResponseArray[0])
						|| "0396".equalsIgnoreCase(emandateResponseArray[0])) {										
						EnachMandateExceptionResponse enachMandateResponse = new EnachMandateExceptionResponse();
						enachMandateResponse.setEnachMandateId(enachMandateData.getId());
						enachMandateResponse.setConsumerId(
								"CONSUMER" + user.getUniqueNumber() + "MNDTID" + enachMandateData.getId());
						enachMandateResponse.setTxnStatus(emandateResponseArray[0]);
						enachMandateResponse.setTxnMsg(emandateResponseArray[1].toUpperCase());
						enachMandateResponse.setTxnErrMsg(emandateResponseArray[2]);
						enachMandateResponse.setClntTxnRef(emandateResponseArray[3]);
						enachMandateResponse.setTpslBankCd(emandateResponseArray[4]);
						enachMandateResponse.setTpslTxnId(emandateResponseArray[5]);
						enachMandateResponse
								.setTxnAmt((emandateResponseArray[6] != null && !"NA".equals(emandateResponseArray[6]))
										? Double.parseDouble(emandateResponseArray[6])
										: 0);
						enachMandateResponse.setClntRqstMeta(emandateResponseArray[7]);
						enachMandateResponse.setTpslTxnTime(emandateResponseArray[8]);
						enachMandateResponse
								.setBalAmt((emandateResponseArray[9] != null && !"NA".equals(emandateResponseArray[9]))
										? Double.parseDouble(emandateResponseArray[9])
										: 0);
						enachMandateResponse.setCardId(emandateResponseArray[10]);
						enachMandateResponse.setAliasName(emandateResponseArray[11]);
						enachMandateResponse.setBankTransactionId(emandateResponseArray[12]);
						enachMandateResponse.setMandateRegNo(emandateResponseArray[13]);
						enachMandateResponse.setToken(emandateResponseArray[14]);
						enachMandateResponse.setHash(emandateResponseArray[15]);
						enachMandateExceptionResponseRepo.save(enachMandateResponse);
						res = true;
					}
				}
			}
		return res;
	}
	
	@Override
	public ENachMandateResponse generateEnachMandateForApplicationLevel(int userId, int mandateId) {

		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		if (user.getPrimaryType() != PrimaryType.BORROWER) {
			throw new OperationNotAllowedException(
					"As a " + user.getPrimaryType() + " primary type not allowed to perform this",
					ErrorCodes.INVALID_OPERATION);
		}
		ApplicationLevelEnachMandate enachMandateData = applicationLevelEnachMandateRepo.findById(mandateId).get();
		System.out.println("enachMandateData::" + enachMandateData.getId());
		if (enachMandateData == null) {
			throw new ActionNotAllowedException("There are no agreed loans to do eMandate.",
					ErrorCodes.INVALID_OPERATION);
		}

		ENachMandateRequest eNachMandateRequest = new ENachMandateRequest();
		eNachMandateRequest.setTotalAmount(String.valueOf(5));

		eNachMandateRequest.setDebitStartDate(dateFormat.format(enachMandateData.getDebitStartDate()));
		eNachMandateRequest.setDebitEndDate(dateFormat.format(enachMandateData.getDebitEndDate()));
		eNachMandateRequest.setMaxAmount(String.valueOf(enachMandateData.getMaxAmount()));
		eNachMandateRequest.setAmountType(enachMandateData.getAmountType().name());
		eNachMandateRequest.setFrequency(enachMandateData.getFrequency().name());
		eNachMandateRequest.setAccountNo("");

		ENachMandateResponse response = new ENachMandateResponse();

		String txnId = enachMandateData.getMandateTransactionId();
		String token = genearteTokenForApplicationLevel(txnId, eNachMandateRequest, user, enachMandateData);

		response.setMerchantId(merchantId);
		response.setMandateId(mandateId);
		response.setToken(token);
		response.setEnachMandateRetUrl(enachmandatereturnurlForAppLevel);
		response.setAccountNo(eNachMandateRequest.getAccountNo());
		response.setCapitalTxnId(token.toUpperCase());
		response.setAmountType(enachMandateData.getAmountType().name());
		response.setDebitEndDate(dateFormat.format(enachMandateData.getDebitEndDate()));
		response.setDebitStartDate(dateFormat.format(enachMandateData.getDebitStartDate()));
		response.setFrequency(enachMandateData.getFrequency().name());
		response.setMaxAmount(String.valueOf(enachMandateData.getMaxAmount()));
		response.setTotalAmount(String.valueOf(5));
		// response.setTotalAmount(String.valueOf(enachMandateData.getOxyLoan().getDisbursmentAmount()));
		response.setTxnId(txnId);

		UserResponse userResponse = new UserResponse();
		userResponse.setDisplayId("CONSUMER" + user.getUniqueNumber() + "MNDTID" + enachMandateData.getId());
		userResponse.setMobileNumber(user.getMobileNumber());
		userResponse.setEmail(user.getEmail());

		response.setUser(userResponse);

		return response;

	}

	private String genearteTokenForApplicationLevel(String txnId, ENachMandateRequest eNachMandateRequest, User user,
			ApplicationLevelEnachMandate enachMandateData) {
		// consumerData.merchantId|consumerData.txnId|
		// totalamount|consumerData.accountNo|consumerData.consumerId|
		// consumerData.consumerMobileNo|consumerData.consumerEmailId |
		// consumerData.debitStartDate|consumerData.debitEndDate|consumerData.maxAmount|
		// consumerData.amountType|consumerData.frequency|
		// consumerData.cardNumber|consumerData.
		// expMonth|consumerData.expYear|consumerData.cvvCode|SALT

		StringBuilder sb = new StringBuilder();
		sb.append(merchantId).append("|");
		sb.append(txnId).append("|");
		sb.append(eNachMandateRequest.getTotalAmount()).append("|");
		sb.append(eNachMandateRequest.getAccountNo()).append("|"); // consumerData.accountNumber
		sb.append("CONSUMER" + user.getUniqueNumber() + "MNDTID" + enachMandateData.getId()).append("|"); // consumerData.consumerId
		sb.append(user.getMobileNumber()).append("|"); // consumerData.consumerMobileNo
		sb.append(user.getEmail()).append("|"); // consumerData.consumerEmailId
		sb.append(eNachMandateRequest.getDebitStartDate()).append("|"); // consumerData.debitStartDate
		sb.append(eNachMandateRequest.getDebitEndDate()).append("|"); // consumerData.debitEndDate
		sb.append(eNachMandateRequest.getMaxAmount()).append("|"); // consumerData.maxAmount
		sb.append(eNachMandateRequest.getAmountType()).append("|"); // consumerData.amountType
		sb.append(eNachMandateRequest.getFrequency()).append("|"); // consumerData.frequency
		sb.append("").append("|"); // consumerData.cardNumber
		sb.append("").append("|"); // consumerData. expMonth
		sb.append("").append("|"); // consumerData.expYear
		sb.append("").append("|"); // consumerData.cvvCode
		sb.append(enachSalt); // SALT
		logger.info("Token returing is {}", sb.toString());
		DigestUtils md = null;
		try {
			md = new DigestUtils(MessageDigest.getInstance("SHA-256"));
		} catch (NoSuchAlgorithmException e) {
			logger.error("Error ", e);
		}
		return md.digestAsHex(sb.toString().getBytes());
	}

	@Override
	public boolean updateEnachMandateReponseForApplicationLevel(int userId,
			EnachMandateReponseRequestDto enachMandateReponseRequestDto) {

		boolean res = false;
		logger.info("getMandateResponse {}", enachMandateReponseRequestDto.getMandateResponse());
		String[] emandateResponseArray = enachMandateReponseRequestDto.getMandateResponse().split(Pattern.quote("|"));
		logger.info("emandateResponseArray[3]", emandateResponseArray[3]);
		ApplicationLevelEnachMandate enachMandateData = applicationLevelEnachMandateRepo
				.findBymerchantTransactionIdentifierForAppLevel(emandateResponseArray[3]);
		logger.info("emandateResponseArray {}", emandateResponseArray[0]);
		User user = userRepo.findById(userId).get();
		if (enachMandateData != null) {
			if (emandateResponseArray[0] != null) {
				if ("0398".equalsIgnoreCase(emandateResponseArray[0])
						|| "0300".equalsIgnoreCase(emandateResponseArray[0])) {
					AppLevelEnachMandateResponse enachMandateResponseData = appLevelEnachMandateResponseRepo
							.findByEnachMandateId(enachMandateData.getId());
					if (enachMandateResponseData == null) {
						AppLevelEnachMandateResponse enachMandateResponse = new AppLevelEnachMandateResponse();
						enachMandateResponse = new AppLevelEnachMandateResponse();
						enachMandateResponse.setEnachMandateId(enachMandateData.getId());
						enachMandateResponse.setConsumerId(
								"CONSUMER" + user.getUniqueNumber() + "MNDTID" + enachMandateData.getId());
						enachMandateResponse.setTxnStatus(emandateResponseArray[0]);
						enachMandateResponse.setTxnMsg(emandateResponseArray[1].toUpperCase());
						enachMandateResponse.setTxnErrMsg(emandateResponseArray[2]);
						enachMandateResponse.setClntTxnRef(emandateResponseArray[3]);
						enachMandateResponse.setTpslBankCd(emandateResponseArray[4]);
						enachMandateResponse.setTpslTxnId(emandateResponseArray[5]);
						enachMandateResponse
								.setTxnAmt((emandateResponseArray[6] != null && !"NA".equals(emandateResponseArray[6]))
										? Double.parseDouble(emandateResponseArray[6])
										: 0);
						enachMandateResponse.setClntRqstMeta(emandateResponseArray[7]);
						enachMandateResponse.setTpslTxnTime(emandateResponseArray[8]);
						enachMandateResponse
								.setBalAmt((emandateResponseArray[9] != null && !"NA".equals(emandateResponseArray[9]))
										? Double.parseDouble(emandateResponseArray[9])
										: 0);
						enachMandateResponse.setCardId(emandateResponseArray[10]);
						enachMandateResponse.setAliasName(emandateResponseArray[11]);
						enachMandateResponse.setBankTransactionId(emandateResponseArray[12]);
						enachMandateResponse.setMandateRegNo(emandateResponseArray[13]);
						enachMandateResponse.setToken(emandateResponseArray[14]);
						enachMandateResponse.setHash(emandateResponseArray[15]);
						appLevelEnachMandateResponseRepo.save(enachMandateResponse);

						if ("0300".equalsIgnoreCase(emandateResponseArray[0])) {
							enachMandateData.setMandateStatus(MandateStatus.SUCCESS);

							applicationLevelEnachMandateRepo.save(enachMandateData);

							List<ApplicationLevelEnachMandate> appId = applicationLevelEnachMandateRepo
									.findingListByApplicationId(enachMandateData.getApplicationId());

							ApplicationLevelEnachMandate enachForPI = null;
							ApplicationLevelEnachMandate enachForP = null;
							ApplicationLevelEnachMandate enachForI = null;

							enachForPI = applicationLevelEnachMandateRepo
									.findingIsEnachActivatedForPI(enachMandateData.getApplicationId());

							enachForP = applicationLevelEnachMandateRepo
									.findingIsEnachActivatedForP(enachMandateData.getApplicationId());
							enachForI = applicationLevelEnachMandateRepo
									.findingIsEnachActivatedForI(enachMandateData.getApplicationId());

							if (enachForPI != null || (enachForP != null && enachForI != null)) {

								if (appId != null && !appId.isEmpty()) {
									for (ApplicationLevelEnachMandate id : appId) {
										String applicationId = "APBR" + id.getApplicationId();
										List<OxyLoan> oxyLoandData = oxyLoanRepo
												.findByLoanRequestIdAndLoanRespondIdAndApplicationId(applicationId);

										if (oxyLoandData != null && !oxyLoandData.isEmpty()) {

											for (OxyLoan loan : oxyLoandData) {
												loan.setIsECSActivated(Boolean.TRUE);

												oxyLoanRepo.save(loan);
											}
										}
									}
								}

								if (appId != null && !appId.isEmpty()) {
									for (ApplicationLevelEnachMandate id : appId) {
										String applicationId = "APBR" + id.getApplicationId();

										List<LoanRequest> listOfLoanRequests = loanRequestRepo
												.findingListOfLoansBasedOnAppId(applicationId);

										if (listOfLoanRequests != null && !listOfLoanRequests.isEmpty()) {
											for (LoanRequest loanRequest : listOfLoanRequests) {
												loanRequest.setIsECSActivated(Boolean.TRUE);
												loanRequest.setEnachType("DIGITALENACH");

												loanRequestRepo.save(loanRequest);
											}
										}
									}
								}
							}
						}
						res = true;
					}
				} else if ("0399".equalsIgnoreCase(emandateResponseArray[0])
						|| "0396".equalsIgnoreCase(emandateResponseArray[0])) {
					AppLevelEnachMandateExceptionResponse enachMandateResponse = new AppLevelEnachMandateExceptionResponse();
					enachMandateResponse.setEnachMandateId(enachMandateData.getId());
					enachMandateResponse
							.setConsumerId("CONSUMER" + user.getUniqueNumber() + "MNDTID" + enachMandateData.getId());
					enachMandateResponse.setTxnStatus(emandateResponseArray[0]);
					enachMandateResponse.setTxnMsg(emandateResponseArray[1].toUpperCase());
					enachMandateResponse.setTxnErrMsg(emandateResponseArray[2]);
					enachMandateResponse.setClntTxnRef(emandateResponseArray[3]);
					enachMandateResponse.setTpslBankCd(emandateResponseArray[4]);
					enachMandateResponse.setTpslTxnId(emandateResponseArray[5]);
					enachMandateResponse
							.setTxnAmt((emandateResponseArray[6] != null && !"NA".equals(emandateResponseArray[6]))
									? Double.parseDouble(emandateResponseArray[6])
									: 0);
					enachMandateResponse.setClntRqstMeta(emandateResponseArray[7]);
					enachMandateResponse.setTpslTxnTime(emandateResponseArray[8]);
					enachMandateResponse
							.setBalAmt((emandateResponseArray[9] != null && !"NA".equals(emandateResponseArray[9]))
									? Double.parseDouble(emandateResponseArray[9])
									: 0);
					enachMandateResponse.setCardId(emandateResponseArray[10]);
					enachMandateResponse.setAliasName(emandateResponseArray[11]);
					enachMandateResponse.setBankTransactionId(emandateResponseArray[12]);
					enachMandateResponse.setMandateRegNo(emandateResponseArray[13]);
					enachMandateResponse.setToken(emandateResponseArray[14]);
					enachMandateResponse.setHash(emandateResponseArray[15]);
					appLevelEnachMandateExceptionResponseRepo.save(enachMandateResponse);
					res = true;
				}
			}
		}
		return res;

	}
}
