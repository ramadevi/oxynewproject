package com.oxyloans.service.enach;

import java.util.List;

import com.oxyloans.response.user.UserResponse;
import com.oxyloans.serviceloan.LoanResponseDto;

public class ENachMandateResponse extends ENachMandateRequest {

	// consumerData.merchantId|consumerData.txnId|totalamount|
	// consumerData.accountNo|consumerData.consumerId|
	// consumerData.consumerMobileNo|consumerData.consumerEmailId |
	// consumerData.debitStartDate|consumerData.debitEndDate|
	// consumerData.maxAmount|consumerData.amountType|consumerData.frequency|
	// consumerData.cardNumber|consumerData. expMonth|consumerData.expYear|
	// consumerData.cvvCode
	private Integer mandateId;

	private String token;

	private String txnId;

	private String capitalTxnId;

	private String merchantId;

	private String enachMandateRetUrl;

	private List<LoanResponseDto> loans;

	private UserResponse user;

	public String getEnachMandateRetUrl() {
		return enachMandateRetUrl;
	}

	public void setEnachMandateRetUrl(String enachMandateRetUrl) {
		this.enachMandateRetUrl = enachMandateRetUrl;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getCapitalTxnId() {
		return capitalTxnId;
	}

	public void setCapitalTxnId(String capitalTxnId) {
		this.capitalTxnId = capitalTxnId;
	}

	public List<LoanResponseDto> getLoans() {
		return loans;
	}

	public void setLoans(List<LoanResponseDto> loans) {
		this.loans = loans;
	}

	public UserResponse getUser() {
		return user;
	}

	public void setUser(UserResponse user) {
		this.user = user;
	}

	public Integer getMandateId() {
		return mandateId;
	}

	public void setMandateId(Integer mandateId) {
		this.mandateId = mandateId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

}
