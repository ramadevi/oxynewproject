package com.oxyloans.service.enach;

public interface IEnachService {

    public ENachMandateResponse generateMandateToken(int userId, int mandateId);

    public boolean updateEnachMandateReponse(int userId, EnachMandateReponseRequestDto enachMandateReponseRequestDto);

	public ENachMandateResponse generateEnachMandateForApplicationLevel(int userId, int mandateId);

	public boolean updateEnachMandateReponseForApplicationLevel(int userId,
			EnachMandateReponseRequestDto enachMandateReponseRequestDto);

}
