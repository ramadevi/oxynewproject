package com.oxyloans.service.enach;

public class ENachMandateRequest {

    //consumerData.merchantId|consumerData.txnId|totalamount|
    //consumerData.accountNo|consumerData.consumerId|
    //consumerData.consumerMobileNo|consumerData.consumerEmailId |
    //consumerData.debitStartDate|consumerData.debitEndDate|
    //consumerData.maxAmount|consumerData.amountType|consumerData.frequency|
    //consumerData.cardNumber|consumerData. expMonth|consumerData.expYear|
    //consumerData.cvvCode|SALT
    private String accountNo;

    private String amountType = "M";

    //Populated Internally
    private String totalAmount;

    private String debitStartDate;

    private String debitEndDate;

    private String maxAmount;

    private String frequency;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getDebitStartDate() {
        return debitStartDate;
    }

    public void setDebitStartDate(String debitStartDate) {
        this.debitStartDate = debitStartDate;
    }

    public String getDebitEndDate() {
        return debitEndDate;
    }

    public void setDebitEndDate(String debitEndDate) {
        this.debitEndDate = debitEndDate;
    }

    public String getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(String maxAmount) {
        this.maxAmount = maxAmount;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return "ENachMandateRequest [accountNo=" + accountNo + ", amountType=" + amountType + ", maxAmount=" + maxAmount
                + ", frequency=" + frequency + "]";
    }

}
