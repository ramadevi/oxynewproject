package com.oxyloans.service.paytm;

public class PaytmVanRequest {

	private Meta meta = new Meta();
	private String mobile;
	private String name;
	private String partnerId;
	private String partnerRefId;
	private String prefix = "OXYTDE";
	private String solution = "VAN";
	private String strategy = "PREFIX_CUSTOM";

	public Meta getMeta() {
		return meta;
	}

	public String getMobile() {
		return mobile;
	}

	public String getName() {
		return name;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public String getPartnerRefId() {
		return partnerRefId;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getSolution() {
		return solution;
	}

	public String getStrategy() {
		return strategy;
	}

	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public void setPartnerRefId(String partnerRefId) {
		this.partnerRefId = partnerRefId;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}
}
