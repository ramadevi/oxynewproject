package com.oxyloans.service.paytm;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Calendar;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.oxyloans.response.user.NodalAccountRequestDto;
import com.oxyloans.response.user.PaytmRequestDto;

@Service
public class JwtTokenGeneration {

	private static final String JWT_HEADER = "{\"alg\":\"HS256\",\"typ\":\"JWT\"}";

	private static String hmacSha256(String data, String secret) {
		try {

			// MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = secret.getBytes(StandardCharsets.UTF_8);// digest.digest(secret.getBytes(StandardCharsets.UTF_8));

			Mac sha256Hmac = Mac.getInstance("HmacSHA256");
			SecretKeySpec secretKey = new SecretKeySpec(hash, "HmacSHA256");
			sha256Hmac.init(secretKey);

			byte[] signedBytes = sha256Hmac.doFinal(data.getBytes(StandardCharsets.UTF_8));

			return encode(signedBytes);
		} catch (NoSuchAlgorithmException | InvalidKeyException ex) {
			return null;
		}
	}

	private static String encode(byte[] bytes) {
		return Base64.getUrlEncoder().withoutPadding().encodeToString(bytes);
	}

	public String jwtToken() {
		PaytmRequestDto paytmRequestDto = new PaytmRequestDto();
		Calendar calendar = Calendar.getInstance();
		Long timeInMiilisecods = calendar.getTimeInMillis();
		paytmRequestDto.setTimestamp(timeInMiilisecods.toString());

		Gson gson = new Gson();
		String payLoad = gson.toJson(paytmRequestDto);

		String signature = hmacSha256(encode(JWT_HEADER.getBytes()) + "." + encode(payLoad.getBytes()),
				"RKsK7OJcPAXVowIznUQrlRdgyUeuO41uo1CoxSugndw=");
		String jwtToken = encode(JWT_HEADER.getBytes()) + "." + encode(payLoad.getBytes()) + "." + signature;
		System.out.println(jwtToken);
		return jwtToken;

	}

	public String callBackJwtToken() {
		JwtPaytmCallBackRequest jwtPaytmCallBackRequest = new JwtPaytmCallBackRequest();

		Gson gson = new Gson();
		String payLoad = gson.toJson(jwtPaytmCallBackRequest);

		String signature = hmacSha256(encode(JWT_HEADER.getBytes()) + "." + encode(payLoad.getBytes()),
				"RKsK7OJcPAXVowIznUQrlRdgyUeuO41uo1CoxSugndw=");
		String jwtToken = encode(JWT_HEADER.getBytes()) + "." + encode(payLoad.getBytes()) + "." + signature;
		System.out.println(jwtToken);
		return jwtToken;

	}

	public String jwtTokenForNodalAccount() {
		NodalAccountRequestDto nodalAccountRequestDto = new NodalAccountRequestDto();
		Calendar calendar = Calendar.getInstance();
		Long timeInMiilisecods = calendar.getTimeInMillis();
		nodalAccountRequestDto.setTimestamp(timeInMiilisecods.toString());

		Gson gson = new Gson();
		String payLoad = gson.toJson(nodalAccountRequestDto);

		String signature = hmacSha256(encode(JWT_HEADER.getBytes()) + "." + encode(payLoad.getBytes()),
				"f6pbiOlh_16d1gJ5s0dML3LgyoIfucvkxTu-km8bxFU=");
		String jwtToken = encode(JWT_HEADER.getBytes()) + "." + encode(payLoad.getBytes()) + "." + signature;
		System.out.println(jwtToken);
		return jwtToken;

	}

}
