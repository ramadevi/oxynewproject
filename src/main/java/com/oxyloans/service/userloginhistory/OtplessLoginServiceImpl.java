package com.oxyloans.service.userloginhistory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.oxyloans.coreauthdto.PasswordGrantAccessToken;
import com.oxyloans.coreauthdto.GenericAccessToken.EncodingAlgorithm;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.entity.user.PersonalDetails;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.entity.userlogin.UserLoginHistory;
import com.oxyloans.repository.userloginrepo.UserLoginHistoryRepo;
import com.oxyloans.otpless.IpAddressResponseDto;
import com.oxyloans.otpless.OtplessRequestDto;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.security.signature.ISignatureService;
import com.oxyloans.service.loan.ActionNotAllowedException;
import com.oxyloans.service.user.PwdLoginService;
import com.oxyloans.service.user.UserService;
import com.oxyloans.whatapp.service.WhatappServiceRepo;
import com.oxyloans.whatsappdto.WhatsappLoginResponse;
import com.oxyloans.whatsapplogindto.WhatsappRequestDto;
import com.oxyloans.whatsapplogindto.WhatsappResponseDto;
import org.apache.logging.log4j.LogManager;

@Service
public class OtplessLoginServiceImpl implements OtplessLoginService {

	@Autowired
	private UserService userService;

	@Autowired
	private UserLoginHistoryRepo userLoginHistoryRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private ISignatureService signatureService;
	
	@Autowired
	private PwdLoginService abstractLoginService;


	
	@Value("${accessTokenAlias}")
	private String accessTokenAlias;

	private long defaultTtl = 2 * 60 * 60;

	@Autowired
	private WhatappServiceRepo whatappServiceRepo;

	
	@Autowired
	private RedisTemplate<String, String> redisStringValueTemplate;

	@Value("${emailTtl}")
	private String emailTtl;

	@Value("${ipAddressUrl}")
	private String ipAddressUrl;

	org.apache.logging.log4j.Logger logger = LogManager.getLogger(OtplessLoginServiceImpl.class);

	@Override
	@Transactional
	public UserResponse otplessLogin(OtplessRequestDto otplessRequestDto) {
		User user = userRepo.findByEmailIgnoreCase(otplessRequestDto.getEmail().getEmail());
		UserResponse userResponse = new UserResponse();
		if (user != null) {

			UserRequest userRequest = new UserRequest();
			userRequest.setId(user.getId());
			userRequest.setPrimaryType(User.PrimaryType.SUPERADMIN.toString());
			userResponse = userService.validateUser(userRequest);
			PasswordGrantAccessToken accessToken = new PasswordGrantAccessToken(defaultTtl, EncodingAlgorithm.RSA);
			accessToken.setUserId(user.getId().toString());
			String signedString = null;
			try {
				signedString = signatureService.sign(this.accessTokenAlias, EncodingAlgorithm.RSA.getAlgo(),
						new Gson().toJson(accessToken));
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (SignatureException e) {
				e.printStackTrace();
			}
			userResponse.setAccessToken(signedString);
			userResponse.setTokenGeneratedTime(accessToken.getIat());
			this.redisStringValueTemplate.opsForValue().set(userResponse.getAccessToken(),
					userResponse.getId().toString(), getTtl(userRequest));
			UserLoginHistory userLoginHistory = new UserLoginHistory();
			userLoginHistory.setUserId(user.getId());
			if (otplessRequestDto.getIpAddress() != null) {
				userLoginHistory.setIpAddress(otplessRequestDto.getIpAddress());
			}
			if (otplessRequestDto.getLatitude() != null) {
				user.setLatitude(otplessRequestDto.getLatitude());
			}
			if (otplessRequestDto.getLongitude() != null) {
				user.setLongitude(otplessRequestDto.getLongitude());
			}
			if (user.getEmailVerified() != true) {
				user.setEmailVerified(true);
				user.setEmailVerifiedOn(new Date());
			}
			userRepo.save(user);
			Gson gson = new Gson();
			System.out.println(gson.toJson(otplessRequestDto));
			userLoginHistory.setRequestBody(gson.toJson(otplessRequestDto));
			// userLoginHistory.setRequestBody("");
			userLoginHistoryRepo.save(userLoginHistory);

		} else {
			throw new ActionNotAllowedException("User not registered with given email", ErrorCodes.EMAIL_IS_NULL);
		}
		return userResponse;

	}

	@Override
	@Transactional
	public WhatsappResponseDto whatsappLogin(WhatsappRequestDto whatsappRequestDto) {

		logger.info("whatsappLogin method starts...............................");

		WhatsappResponseDto whatsappResponseDto = new WhatsappResponseDto();
		// Check if WhatsApp number is provided
		if (whatsappRequestDto.getWhatsappNumber() == null) {
			throw new IllegalArgumentException("WhatsApp number is not entered.");
		}

		// If user not found by ID and WhatsApp number, attempt to find by WhatsApp
		// number only

		List<User> listUserRepo = userRepo.findByPersonalDetailsWhatsAppNumber(whatsappRequestDto.getWhatsappNumber());

		if (listUserRepo == null || listUserRepo.isEmpty()) {
			throw new IllegalArgumentException("No user found with the provided WhatsApp number.");
		}

		// If only one user found, check if WhatsApp is verified and send OTP if needed
		User user1 = listUserRepo.get(0);
		PersonalDetails personalDetails = user1.getPersonalDetails();

		if (personalDetails == null || personalDetails.getWhatsAppNumber() == null || !user1.isWhatsappVerified()) {
			throw new IllegalArgumentException("WhatsApp number is not verified.");
		}

		if (whatsappRequestDto.getOtp() == null && whatsappRequestDto.getWhatsappNumber() != null) {
			WhatsappResponseDto otp = sendOtpViaWhatsApp(user1, whatsappRequestDto.getWhatsappNumber());

			if (otp != null) {

				whatsappResponseDto.setStatus("OTP Sent Sucessfully.!   " );
				
				return otp;
			} else {
				whatsappResponseDto.setStatus("OTP Not Sent Sucessfully.! ");
			}
			// need to write here response ?
		}
		logger.info("whatsappLogin method ended...............................");
		return whatsappResponseDto;
	}

	private WhatsappResponseDto sendOtpViaWhatsApp(User user1, String whatsappNumber) {
		logger.info("sendOtpViaWhatsApp method started...............................");
		WhatsappResponseDto whatsappResponseDto = new WhatsappResponseDto();
		Random random = new Random();
		int randomNumber = random.nextInt(9000) + 1000;
		String message = "Your OTP for Oxyloans login is: " + randomNumber;
		whatappServiceRepo.sendingWhatsappMessageWithNewInstance(whatsappNumber, message);
		String hashedEmailOtpSession = signatureService.hashPassword(String.valueOf(randomNumber), user1.getSalt());

		whatsappResponseDto.setStatus("OTP Sent Successfully! " + randomNumber);
		whatsappResponseDto.setWhatsappNumber(whatsappNumber);
		whatsappResponseDto.setSession(hashedEmailOtpSession);
		whatsappResponseDto.setId(user1.getId());
		Long currentTimeInMilliSec = System.currentTimeMillis();
		whatsappResponseDto.setOtpGeneratedTime(currentTimeInMilliSec.toString());
		logger.info("sendOtpViaWhatsApp method ended...............................");
		return whatsappResponseDto;

	}

	public long getTtl(UserRequest userRequest) {
		return this.defaultTtl;
	}

	@Override
	@Transactional
	public WhatsappResponseDto whatsappLoginOtpVerification(WhatsappRequestDto whatsappRequestDto) {
		logger.info("whatsappLoginOtpVerification method started...............................");
		WhatsappResponseDto whatsappResponseDto = new WhatsappResponseDto();
		List<WhatsappLoginResponse> listResponse = new ArrayList<>();

		List<User> listUserRepo = userRepo.findByPersonalDetailsWhatsAppNumber(whatsappRequestDto.getWhatsappNumber());

		if (listUserRepo == null || listUserRepo.isEmpty()) {
			throw new IllegalArgumentException("No user found with the provided WhatsApp number.");
		}

		// If only one user found, check if WhatsApp is verified and send OTP if needed
		User user1 = listUserRepo.get(0);
		PersonalDetails personalDetails = user1.getPersonalDetails();

		if (personalDetails == null || personalDetails.getWhatsAppNumber() == null || !user1.isWhatsappVerified()) {
			throw new IllegalArgumentException("WhatsApp number is not verified.");
		}

		if (whatsappRequestDto.getOtp() != null && whatsappRequestDto.getWhatsappNumber() != null) {

			Long registerdTime = Long.parseLong(whatsappRequestDto.getOtpGeneratedTime());
			Date registerdTimeDate = new Date(registerdTime);

			Long currentTime = System.currentTimeMillis();
			Date currentTimeDate = new Date(currentTime);

			if (currentTimeDate.getTime() - registerdTimeDate.getTime() > Long.parseLong(this.emailTtl)) {
				throw new ActionNotAllowedException("whatsapp otp expired", ErrorCodes.TOKEN_NOT_VALID);
			}

			String newSession = signatureService.hashPassword(String.valueOf(whatsappRequestDto.getOtp()),
					user1.getSalt());

			if (whatsappRequestDto.getSession().equals(newSession)) {

				if (listUserRepo.size() > 1) {
					whatsappResponseDto.setMessage(
							"Multiple users mapped with given WhatsApp number, please confirm and enter below user ID to login.");
					}
String userId = null;
				for (User user : listUserRepo) {
					String userName = user.getPersonalDetails().getFirstName() + " "
							+ user.getPersonalDetails().getLastName();
					WhatsappLoginResponse whatsappLoginResponse = new WhatsappLoginResponse();
					
					if (user.getPrimaryType() == PrimaryType.LENDER) {
						userId="LR"+user.getId();
					} else {
						userId="BR"+user.getId();
					}
					
					whatsappLoginResponse.setUserId(userId);
					whatsappLoginResponse.setName(userName);
					whatsappLoginResponse.setEmail(user.getEmail());
					listResponse.add(whatsappLoginResponse);
					whatsappResponseDto.setId(user.getId());
                 	whatsappResponseDto.setWhatsappLoginResponse(listResponse);
	}

			} else {
				throw new ActionNotAllowedException("Invalid whatsapp otp please check", ErrorCodes.INVALID_OTP);
			}
		}logger.info("whatsappLoginOtpVerification method ended...............................");
		return whatsappResponseDto;

	}

	@Override
	public IpAddressResponseDto ipAddressGeneration() throws IOException {
		IpAddressResponseDto ipAddressResponseDto = new IpAddressResponseDto();
		try {
			URL ipapi = new URL(ipAddressUrl);

			URLConnection c = ipapi.openConnection();
			c.setRequestProperty("User-Agent", "java-ipapi-v1.02");
			BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));

			StringBuilder stringBuilder1 = new StringBuilder();
			StringBuilder stringBuilder2 = new StringBuilder();

			String line;
			while ((line = reader.readLine()) != null) {
				line.replace("\\s+", "");
				stringBuilder1.append(line).append("/");

			}
			String newValue = stringBuilder1.toString();
			String[] s = newValue.split(",/");
			String ipAddress = null;
			for (int i = 0; i < s.length; i++) {
				String filed = s[i].replace("{/", "").replace("/}/", "");
				String newFiled = filed.trim();

				stringBuilder2.append(newFiled).append(",");
				if (filed.contains("ip")) {
					String[] ipSplit = newFiled.split(":");
					String localValue = ipSplit[1].replace("\"", "");
					ipAddress = localValue.trim();
				}
			}
			ipAddressResponseDto.setIpAddress(ipAddress);
			ipAddressResponseDto.setIpAddressResponse(stringBuilder2.toString());
		} catch (Exception e) {
			logger.info("Exception in ipAddressGeneration method");
		}
		return ipAddressResponseDto;

	}
	
	@Override
	public UserResponse whatsappLoginBasedOnId(Integer id) {

		logger.info("whatsappLoginBasedOnId method is started...");
		UserResponse userResponse = new UserResponse();

		/*
		 * UserRequest userRequest = new UserRequest(); userRequest.setId(id);
		 * userRequest.setPrimaryType(User.PrimaryType.SUPERADMIN.toString());
		 */
		
		return userResponse =abstractLoginService.accessToken(id, "USER");
	}
}
