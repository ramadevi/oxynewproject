package com.oxyloans.service.userloginhistory;

import java.io.IOException;

import org.springframework.stereotype.Repository;

import com.oxyloans.otpless.IpAddressResponseDto;
import com.oxyloans.otpless.OtplessRequestDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.whatsapplogindto.WhatsappRequestDto;
import com.oxyloans.whatsapplogindto.WhatsappResponseDto;

@Repository
public interface OtplessLoginService {

	public UserResponse otplessLogin(OtplessRequestDto otplessRequestDto);

	public WhatsappResponseDto whatsappLogin(WhatsappRequestDto whatsappRequestDto);

	
	public IpAddressResponseDto ipAddressGeneration() throws IOException;

	public WhatsappResponseDto whatsappLoginOtpVerification(WhatsappRequestDto whatsappRequestDto);

	public UserResponse whatsappLoginBasedOnId(Integer id);

}
