package com.oxyloans.service.wallettowallettransferservice;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWallet;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletNativeRepo;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletRepo;
import com.oxyloans.entity.user.User;
import com.oxyloans.lender.wallet.transfer.dto.ActionOnRequestedTransaction;
import com.oxyloans.lender.wallet.transfer.dto.ListOfWalletToWalletTransactionsResponse;
import com.oxyloans.lender.wallet.transfer.dto.WalletTransferLenderToLenderRequestDto;
import com.oxyloans.lender.wallet.transfer.dto.WalletTransferLenderToLenderResponseDto;
import com.oxyloans.lender.withdrawfunds.entity.LenderWithdrawalFunds;
import com.oxyloans.lender.withdrawfunds.repo.LenderWithdrawalFundsNativeRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.service.loan.ActionNotAllowedException;
import com.oxyloans.service.loan.AdminLoanService;
import com.oxyloans.service.loan.LenderWalletHistoryServiceRepo;
import com.oxyloans.service.loan.NewAdminLoanService;
import com.oxyloans.whatappservice.WhatappServiceRepo;

@Service
public class WalletToWalletTransferService implements WalletToWalletTransferServiceRepo {

	@Autowired
	private LenderWithdrawalFundsNativeRepo lenderWithdrawalFundsNativeRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private NewAdminLoanService newAdminLoanService;

	@Autowired
	private LenderOxyWalletRepo lenderOxyWalletNativeRepo;

	SimpleDateFormat dateFromTimeStamp = new SimpleDateFormat("yyyy-MM-dd");

	private final Logger logger = LogManager.getLogger(AdminLoanService.class);

	@Autowired
	private LenderOxyWalletNativeRepo lenderOxyWalletRepo;

	@Autowired
	private WhatappServiceRepo whatappServiceRepo;

	SimpleDateFormat dd_MM_yyyy = new SimpleDateFormat("dd/MM/yyyy");

	@Autowired
	private LenderWalletHistoryServiceRepo lenderWalletHistoryServiceRepo;

	@Override
	@Transactional
	public WalletTransferLenderToLenderResponseDto TransferWalletAmountFromLenderToLender(
			WalletTransferLenderToLenderRequestDto walletTransferLenderToLenderRequestDto) {
		WalletTransferLenderToLenderResponseDto walletTransferLenderToLenderResponseDto = new WalletTransferLenderToLenderResponseDto();
		User receiverId = userRepo.findById(walletTransferLenderToLenderRequestDto.getReceiverId()).get();
		if (receiverId != null) {

			Double walletAmount = newAdminLoanService
					.getCurrentWalletBalanceOfLender(walletTransferLenderToLenderRequestDto.getSenderId());
			System.out.print("wallet amount" + walletAmount);
			if (walletAmount != null && walletAmount > 0.0) {
				if (walletTransferLenderToLenderRequestDto.getAmount() <= walletAmount) {
					if (walletTransferLenderToLenderRequestDto.getAmount() > 0.0) {
						LenderWithdrawalFunds transactionRequested = lenderWithdrawalFundsNativeRepo
								.walletAmountAlreadyRequested(walletTransferLenderToLenderRequestDto.getSenderId(),
										walletTransferLenderToLenderRequestDto.getReceiverId());
						if (transactionRequested == null) {
							LenderWithdrawalFunds lenderWithdrawalFunds = new LenderWithdrawalFunds();
							lenderWithdrawalFunds.setUserId(walletTransferLenderToLenderRequestDto.getSenderId());
							lenderWithdrawalFunds.setAmount(walletTransferLenderToLenderRequestDto.getAmount());
							Date date = new Date();
							lenderWithdrawalFunds.setAmountExpectedDate(dateFromTimeStamp.format(date.getTime()));
							lenderWithdrawalFunds
									.setReceiverUserId(walletTransferLenderToLenderRequestDto.getReceiverId());
							lenderWithdrawalFunds.setWithdrawalReason(null);
							lenderWithdrawalFunds.setRating(null);
							lenderWithdrawalFunds.setFeedBack(null);
							lenderWithdrawalFunds.setStatus("INITIATED");
							lenderWithdrawalFunds.setAdminComments(null);
							lenderWithdrawalFunds.setCreatedOn(new Date());
							lenderWithdrawalFundsNativeRepo.save(lenderWithdrawalFunds);
							walletTransferLenderToLenderResponseDto.setStatus("successfully updated");
						} else {
							throw new OperationNotAllowedException(
									"You have aleady requested the amount moveing to wallet to this lender id "
											+ walletTransferLenderToLenderRequestDto.getReceiverId(),
									ErrorCodes.ALREDY_IN_PROGRESS);
						}
					} else {
						throw new OperationNotAllowedException("Please enter the amount greater than zero",
								ErrorCodes.AMOUNT_LIMIT);
					}

				} else {
					throw new OperationNotAllowedException("Amount is greater than the wallet amount",
							ErrorCodes.AMOUNT_LIMIT);
				}

			} else {
				throw new OperationNotAllowedException("Lender has insufficient funds in the wallet",
						ErrorCodes.LOWER_AMOUNT);
			}

		} else {
			throw new OperationNotAllowedException("Please check receiver id ", ErrorCodes.ENITITY_NOT_FOUND);
		}

		return walletTransferLenderToLenderResponseDto;

	}

	@Override
	@Transactional
	public ListOfWalletToWalletTransactionsResponse WalletToWalletRequests(PaginationRequestDto pageRequestDto) {
		int pageSize = pageRequestDto.getPageSize();
		int pageNo = pageRequestDto.getPageSize() * (pageRequestDto.getPageNo() - 1);
		ListOfWalletToWalletTransactionsResponse listOfWalletToWalletTransactionsResponse = new ListOfWalletToWalletTransactionsResponse();
		List<WalletTransferLenderToLenderResponseDto> listOfUsers = new ArrayList<WalletTransferLenderToLenderResponseDto>();
		List<LenderWithdrawalFunds> initiatedStatusUsers = null;

		try {

//			List<LenderWithdrawalFunds> initiatedStatusUsers = lenderWithdrawalFundsNativeRepo
//					.walletToWalletTransactionRequests(pageSize, pageNo);
			if (pageRequestDto.getUserType().equalsIgnoreCase("LIVE")) {
				initiatedStatusUsers = lenderWithdrawalFundsNativeRepo.walletToWalletTransactionRequests(pageSize,
						pageNo);

			} else {
				initiatedStatusUsers = lenderWithdrawalFundsNativeRepo
						.walletToWalletTransactionRequestsTestLenders(pageSize, pageNo);

			}
			if (initiatedStatusUsers != null && !initiatedStatusUsers.isEmpty()) {
				initiatedStatusUsers.forEach(wallet -> {
					WalletTransferLenderToLenderResponseDto walletTransferLenderToLenderResponseDto = new WalletTransferLenderToLenderResponseDto();
					walletTransferLenderToLenderResponseDto.setId(wallet.getId());
					walletTransferLenderToLenderResponseDto.setSenderId(wallet.getUserId());
					User senderObject = userRepo.findById(wallet.getUserId()).get();
					if (senderObject != null) {
						if (senderObject.getPersonalDetails() != null) {
							walletTransferLenderToLenderResponseDto
									.setSenderName(senderObject.getPersonalDetails().getFirstName() + " "
											+ senderObject.getPersonalDetails().getLastName());
						}
					}
					walletTransferLenderToLenderResponseDto.setReceiverId(wallet.getReceiverUserId());
					User receiver = userRepo.findById(wallet.getReceiverUserId()).get();
					if (receiver != null) {
						if (receiver.getPersonalDetails() != null) {
							walletTransferLenderToLenderResponseDto
									.setReceiverName(receiver.getPersonalDetails().getFirstName() + " "
											+ receiver.getPersonalDetails().getLastName());
						}
					}
					walletTransferLenderToLenderResponseDto.setRequestedDate(wallet.getAmountExpectedDate());
					walletTransferLenderToLenderResponseDto.setStatus(wallet.getStatus());
					walletTransferLenderToLenderResponseDto
							.setAmount(BigInteger.valueOf(wallet.getAmount().intValue()));
					listOfUsers.add(walletTransferLenderToLenderResponseDto);
				});
				listOfWalletToWalletTransactionsResponse.setWalletTransferLenderToLenderResponseDto(listOfUsers);
				Integer totalCount = lenderWithdrawalFundsNativeRepo.walletToWalletTransactionRequestsCount();
				if (totalCount != null) {
					listOfWalletToWalletTransactionsResponse.setTotalCount(totalCount);
				}

			}

		} catch (Exception e) {
			logger.info("Exception in walletToWalletRequests method");
		}

		return listOfWalletToWalletTransactionsResponse;

	}

	@Override
	@Transactional
	public WalletTransferLenderToLenderResponseDto AdminActionOnWalletToWallet(
			ActionOnRequestedTransaction actionOnRequestedTransaction) {
		WalletTransferLenderToLenderResponseDto walletTransferLenderToLenderResponseDto = new WalletTransferLenderToLenderResponseDto();
		LenderWithdrawalFunds withdrawApprovaL = lenderWithdrawalFundsNativeRepo
				.findById(actionOnRequestedTransaction.getId()).get();
		if (withdrawApprovaL != null) {
			User users = userRepo.findById(withdrawApprovaL.getUserId()).get();

			if (users != null && users.isTestUser() && !actionOnRequestedTransaction.getStatus().equals("REJECTED")) {
				throw new ActionNotAllowedException("this is a test user", ErrorCodes.INVALID_OPERATION);
			}

			if (actionOnRequestedTransaction.getStatus().equalsIgnoreCase("APPROVED")) {

				Double walletAmount = newAdminLoanService.getCurrentWalletBalanceOfLender(withdrawApprovaL.getUserId());
				if (walletAmount != null && walletAmount > 0.0) {
					if (walletAmount >= withdrawApprovaL.getAmount()) {
						withdrawApprovaL.setStatus(actionOnRequestedTransaction.getStatus().toUpperCase());
						withdrawApprovaL.setApprovedOn(new Date());
						lenderWithdrawalFundsNativeRepo.save(withdrawApprovaL);
						for (int i = 0; i <= 1; i++) {
							LenderOxyWallet lenderOxyWallet = new LenderOxyWallet();
							if (i == 0) {
								lenderOxyWallet.setUserId(withdrawApprovaL.getUserId());
								lenderOxyWallet.setScrowAccountNumber("OXYLRV" + withdrawApprovaL.getUserId());
								double requestAmount = withdrawApprovaL.getAmount();
								lenderOxyWallet.setTransactionAmount((int) requestAmount);
								lenderOxyWallet.setTransactionDate(new Date());
								lenderOxyWallet.setStatus(LenderOxyWallet.Status.APPROVED);
								lenderOxyWallet.setCreatedDate(new Date());
								lenderOxyWallet.setComments("debited to LR" + withdrawApprovaL.getReceiverUserId());
								lenderOxyWallet.setTransactionType("debit");
								lenderOxyWallet.setWalletTransferId(withdrawApprovaL.getReceiverUserId());
								lenderOxyWalletNativeRepo.save(lenderOxyWallet);
								lenderWalletHistoryServiceRepo.lenderTransactionHistory(withdrawApprovaL.getUserId(),
										requestAmount, "DEBIT", "WTW", 0);
								List<LenderWithdrawalFunds> lenderWalletWithdrawRequest = lenderWithdrawalFundsNativeRepo
										.getListOfWalletWithdrawRaised(withdrawApprovaL.getUserId());
								if (lenderWalletWithdrawRequest != null && !lenderWalletWithdrawRequest.isEmpty()) {
									double temporaryAmount = 0.0;
									Double walletAmountUserId = userRepo.userWalletAmount(withdrawApprovaL.getUserId());
									temporaryAmount = walletAmountUserId != null ? walletAmountUserId : 0.0;

									for (LenderWithdrawalFunds withdrawRequest : lenderWalletWithdrawRequest) {
										if (temporaryAmount >= withdrawRequest.getAmount()) {
											temporaryAmount = (temporaryAmount - withdrawRequest.getAmount());
										} else {
											withdrawRequest.setStatus("AUTO REJECTED");
											withdrawRequest.setApprovedOn(new Date());
											lenderWithdrawalFundsNativeRepo.save(withdrawRequest);
										}

									}
								}

							} else {
								lenderOxyWallet.setUserId(withdrawApprovaL.getReceiverUserId());
								lenderOxyWallet.setScrowAccountNumber("OXYLRV" + withdrawApprovaL.getReceiverUserId());
								double requestAmount = withdrawApprovaL.getAmount();
								lenderOxyWallet.setTransactionAmount((int) requestAmount);
								lenderOxyWallet.setTransactionDate(new Date());
								lenderOxyWallet.setStatus(LenderOxyWallet.Status.APPROVED);
								lenderOxyWallet.setCreatedDate(new Date());
								lenderOxyWallet.setComments("credited from LR" + withdrawApprovaL.getUserId());
								lenderOxyWallet.setTransactionType("credit");
								lenderOxyWallet.setWalletTransferId(withdrawApprovaL.getUserId());
								lenderOxyWalletNativeRepo.save(lenderOxyWallet);
								lenderWalletHistoryServiceRepo.lenderTransactionHistory(
										withdrawApprovaL.getReceiverUserId(), requestAmount, "CREDIT", "WTW", 0);
							}

						}
					} else {
						throw new OperationNotAllowedException("Please check your wallet has insufficient funds",
								ErrorCodes.LOWER_AMOUNT);
					}

				} else {
					throw new OperationNotAllowedException("Please check wallet is empty ", ErrorCodes.LOWER_AMOUNT);
				}

				String message = "This is to inform you that an amount of Rs. *"
						+ withdrawApprovaL.getAmount().intValue() + "* has been credited to your account No. "
						+ "*OXYLRV" + withdrawApprovaL.getReceiverUserId() + "* from account No. " + "*OXYLRV"
						+ withdrawApprovaL.getUserId() + "* through NEFT on " + dd_MM_yyyy.format(new Date()) + ".";

				Optional<User> optionalUser = userRepo.findById(withdrawApprovaL.getReceiverUserId());

				if (optionalUser.isPresent()) {
					User user = optionalUser.get();

					whatappServiceRepo.sendingIndividualMessageUsingUltraApi("91" + user.getMobileNumber(), message);

					whatappServiceRepo.sendingWhatsappMessageWithNewInstance("120363020098924954@g.us", message);

				}

			} else {
				withdrawApprovaL.setStatus(actionOnRequestedTransaction.getStatus().toUpperCase());
				withdrawApprovaL.setApprovedOn(new Date());
				lenderWithdrawalFundsNativeRepo.save(withdrawApprovaL);
			}

		}
		walletTransferLenderToLenderResponseDto.setId(actionOnRequestedTransaction.getId());
		return walletTransferLenderToLenderResponseDto;

	}

	@Override
	public ListOfWalletToWalletTransactionsResponse WalletToWalletDebitedHistory(int userId) {
		ListOfWalletToWalletTransactionsResponse listOfWalletToWalletTransactionsResponse = new ListOfWalletToWalletTransactionsResponse();
		List<LenderOxyWallet> debitedInfo = lenderOxyWalletRepo.getWalletToWalletDebitedHistory(userId);
		List<WalletTransferLenderToLenderResponseDto> totalDebitedList = new ArrayList<WalletTransferLenderToLenderResponseDto>();
		User user = userRepo.findById(userId).get();

		if (user != null && user.getPersonalDetails() != null) {
			listOfWalletToWalletTransactionsResponse.setSenderName(
					user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName());

		}

		if (debitedInfo != null && !debitedInfo.isEmpty()) {

			totalDebitedList = debitedInfo.stream().map(lenderWalletInfo -> {
				WalletTransferLenderToLenderResponseDto walletToWalletTransferedResponse = new WalletTransferLenderToLenderResponseDto();
				walletToWalletTransferedResponse.setReceiverId(lenderWalletInfo.getWalletTransferId());
				User receiverId = userRepo.findById(lenderWalletInfo.getWalletTransferId()).get();
				if (receiverId != null && receiverId.getPersonalDetails() != null) {
					walletToWalletTransferedResponse.setReceiverName(receiverId.getPersonalDetails().getFirstName()
							+ " " + receiverId.getPersonalDetails().getLastName());
				}
				walletToWalletTransferedResponse
						.setAmount(BigDecimal.valueOf(lenderWalletInfo.getTransactionAmount()).toBigInteger());
				List<LenderWithdrawalFunds> debitHistory = lenderWithdrawalFundsNativeRepo.findByUserId(userId);
				if (debitHistory != null) {

					for (LenderWithdrawalFunds history : debitHistory) {

						walletToWalletTransferedResponse.setRequestedDate(history.getAmountExpectedDate());

					}
				}
				walletToWalletTransferedResponse
						.setTransformedDate(dateFromTimeStamp.format(lenderWalletInfo.getTransactionDate()));
				return walletToWalletTransferedResponse;
			}).collect(Collectors.toList());

		}
		List<WalletTransferLenderToLenderResponseDto> totalDebitedListAfterSorting = totalDebitedList.stream()
				.sorted((a, b) -> b.getTransformedDate().compareTo(a.getTransformedDate()))
				.collect(Collectors.toList());
		listOfWalletToWalletTransactionsResponse
				.setWalletTransferLenderToLenderResponseDto(totalDebitedListAfterSorting);
		return listOfWalletToWalletTransactionsResponse;

	}

	@Override
	public List<WalletTransferLenderToLenderResponseDto> WalletToWalletRequestsList(int userId) {
		List<LenderWithdrawalFunds> lenderWithdrawalFunds = lenderWithdrawalFundsNativeRepo.findByUserId(userId);
		logger.info("userId" + lenderWithdrawalFunds);

		List<WalletTransferLenderToLenderResponseDto> response = new ArrayList<WalletTransferLenderToLenderResponseDto>();

		if (lenderWithdrawalFunds != null && !lenderWithdrawalFunds.isEmpty()) {

			for (LenderWithdrawalFunds lenderWithdrawalFundsList : lenderWithdrawalFunds) {

				WalletTransferLenderToLenderResponseDto walletTransferLenderToLender = new WalletTransferLenderToLenderResponseDto();
				Double amount = lenderWithdrawalFundsList.getAmount();
				walletTransferLenderToLender.setSenderId(lenderWithdrawalFundsList.getUserId());
				if (lenderWithdrawalFundsList.getCreatedOn() != null) {
					walletTransferLenderToLender
							.setRequestedDate(dateFromTimeStamp.format(lenderWithdrawalFundsList.getCreatedOn()));
				}
				if (lenderWithdrawalFundsList.getApprovedOn() != null) {

					walletTransferLenderToLender
							.setTransformedDate(dateFromTimeStamp.format(lenderWithdrawalFundsList.getApprovedOn()));
				}
				
				walletTransferLenderToLender.setId(lenderWithdrawalFundsList.getId());

				walletTransferLenderToLender.setStatus(lenderWithdrawalFundsList.getStatus());

				walletTransferLenderToLender.setAmount(BigDecimal.valueOf(amount).toBigInteger());

				walletTransferLenderToLender.setReceiverId(lenderWithdrawalFundsList.getReceiverUserId());

				User receiver = userRepo.findById(lenderWithdrawalFundsList.getReceiverUserId()).get();

				logger.info("receiverId" + receiver);

				if (receiver != null) {
					if (receiver.getPersonalDetails() != null) {
						walletTransferLenderToLender.setReceiverName(receiver.getPersonalDetails().getFirstName() + " "
								+ receiver.getPersonalDetails().getLastName());
					}
				}
				response.add(walletTransferLenderToLender);
			}

		}

		return response;
	}

	@Override
	public WalletTransferLenderToLenderResponseDto walletToWalletStatusChange(
			WalletTransferLenderToLenderRequestDto walletTransferLenderToLenderRequestDto) {
		LenderWithdrawalFunds lenderWithdrawalFunds = lenderWithdrawalFundsNativeRepo
				.findById(walletTransferLenderToLenderRequestDto.getId()).get();

		logger.info("tableId" + lenderWithdrawalFunds);
		if (lenderWithdrawalFunds != null) {

			if (walletTransferLenderToLenderRequestDto.getStatus().equalsIgnoreCase("REJECTED")) {
				lenderWithdrawalFunds.setStatus("USER REJECTED");
				lenderWithdrawalFunds.setApprovedOn(new Date());
			}

			lenderWithdrawalFundsNativeRepo.save(lenderWithdrawalFunds);
		}

		return null;
	}

}