package com.oxyloans.service.wallettowallettransferservice;

import java.util.List;

import com.oxyloans.lender.wallet.transfer.dto.ActionOnRequestedTransaction;
import com.oxyloans.lender.wallet.transfer.dto.ListOfWalletToWalletTransactionsResponse;
import com.oxyloans.lender.wallet.transfer.dto.WalletTransferLenderToLenderRequestDto;
import com.oxyloans.lender.wallet.transfer.dto.WalletTransferLenderToLenderResponseDto;
import com.oxyloans.response.user.PaginationRequestDto;

public interface WalletToWalletTransferServiceRepo {

	public WalletTransferLenderToLenderResponseDto TransferWalletAmountFromLenderToLender(
			WalletTransferLenderToLenderRequestDto walletTransferLenderToLenderRequestDto);

	public ListOfWalletToWalletTransactionsResponse WalletToWalletRequests(PaginationRequestDto pageRequestDto);

	public WalletTransferLenderToLenderResponseDto AdminActionOnWalletToWallet(
			ActionOnRequestedTransaction actionOnRequestedTransaction);

	public ListOfWalletToWalletTransactionsResponse WalletToWalletDebitedHistory(int userId);
	public List<WalletTransferLenderToLenderResponseDto> WalletToWalletRequestsList(int userId);
	public WalletTransferLenderToLenderResponseDto walletToWalletStatusChange(WalletTransferLenderToLenderRequestDto walletTransferLenderToLenderRequestDto);

}
