package com.oxyloans.service.icici.upi;

import com.oxyloans.response.user.PaginationRequestDto;

public interface UnifiedPaymentsInterfaceServiceRepo {

	public TransactionResponseDto quickResponseTransactionDetails(
			QRTransactionDetailsRequestDto transactionDetailsRequestDto);

	public String callResponseForQr(String encryptedCallBack);

	public TransactionStatusCheckResponseDto transactionStatusCheck(
			TransactionStatusCheckRequestDto transactionStatusCheckRequestDto);

	public TransactionResponseDto qrStatusBasedOnId(int id);

	public QRSuccessTransactionsDetails getSuccessTransactionsFromQR(int userId, PaginationRequestDto pageRequestDto);

}
