package com.oxyloans.service.icici.upi;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.oxyloans.customexceptions.EntityAlreadyExistsException;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entity.icici.upi.IciciQuickresponseTransactionDetails;
import com.oxyloans.entity.icici.upi.IciciQuickresponseTransactionDetailsRepo;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWallet;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletRepo;
import com.oxyloans.entity.user.User;
import com.oxyloans.service.icici.ServiceImplForICICI;
import com.oxyloans.mobile.MobileRequest;
import com.oxyloans.mobile.MobileUtil;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.response.user.WtappIndividualGroupRequestDto;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.service.loan.LenderWalletHistoryServiceRepo;
import com.oxyloans.whatappservice.WhatappService;

@Service
public class UnifiedPaymentsInterfaceService implements UnifiedPaymentsInterfaceServiceRepo {
	private Client client = ClientBuilder.newClient().register(MultiPartFeature.class).register(MultiPartWriter.class);

	@Autowired
	private UserRepo userRepo;

	private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(ServiceImplForICICI.class);

	@Value("${iciciUpiIntegrationPrivateKey}")
	private String iciciUpiIntegrationPrivateKey;

	@Value("${iciciUpiIntegrationPublicKey}")
	private String iciciUpiIntegrationPublicKey;

	@Autowired
	private IciciQuickresponseTransactionDetailsRepo iciciQuickresponseTransactionDetailsRepo;

	@Autowired
	private LenderOxyWalletRepo lenderOxyWalletRepo;

	protected final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Autowired
	private WhatappService whatappService;

	@Value("${wtappApi}")
	private String wtappApi;

	@Value("${whatsappUsingUltraApi}")
	private String whatsappUsingUltraApi;

	@Value("${whatsappUltraToken}")
	private String whatsappUltraToken;

	@Value("${whatsappUsingUltraNewInstance}")
	private String whatsappUsingUltraNewInstance;

	@Value("${whatsappUltraTokenForNewInstance}")
	private String whatsappUltraTokenForNewInstance;

	@Autowired
	private LenderWalletHistoryServiceRepo lenderWalletHistoryServiceRepo;

	@Autowired
	private IEmailService emailService;

	@Autowired
	protected MobileUtil mobileUtil;

	@Override
	public TransactionResponseDto quickResponseTransactionDetails(
			QRTransactionDetailsRequestDto transactionDetailsRequestDto) {
		TransactionResponseDto transactionResponseDto = new TransactionResponseDto();

		User user = userRepo.findById(transactionDetailsRequestDto.getUserId()).get();
		if (user != null) {

			if (transactionDetailsRequestDto.getAmount() <= 100000) {
				long timeSeed = System.nanoTime();

				double randSeed = Math.random() * 1000;

				long midSeed = (long) (timeSeed * randSeed);
				String s = midSeed + "";
				String randomNumber1 = s.substring(0, 9);

				StringBuffer billNumber = new StringBuffer("OXYBN");
				billNumber.append(randomNumber1);

				SimpleDateFormat formatter1 = new SimpleDateFormat("ddMMyyyyHHmmss");
				Date date = new Date();
				String currentDate = formatter1.format(date);

				StringBuffer refId = new StringBuffer("MNO");
				refId.append(transactionDetailsRequestDto.getUserId()).append(currentDate);

				IciciQuickresponseTransactionDetails iciciQuickresponseTransactionDetails = new IciciQuickresponseTransactionDetails();
				iciciQuickresponseTransactionDetails.setUserId(transactionDetailsRequestDto.getUserId());
				DecimalFormat df = new DecimalFormat("#.00");
				String angleFormated = df.format(transactionDetailsRequestDto.getAmount());
				iciciQuickresponseTransactionDetails.setAmount(angleFormated);
				iciciQuickresponseTransactionDetails.setBillNumber(billNumber.toString());
				iciciQuickresponseTransactionDetails.setRefId(refId.toString());
				iciciQuickresponseTransactionDetails.setRefIdInitiatedOn(new Date());
				iciciQuickresponseTransactionDetailsRepo.save(iciciQuickresponseTransactionDetails);
				transactionResponseDto
						.setStatus(IciciQuickresponseTransactionDetails.TransactionStatus.INITIATED.toString());
				StringBuffer qrGenerationCode = new StringBuffer("upi://pay?pa=oxyloans@icici&pn=oxyloans&tr=");
				qrGenerationCode.append(refId.toString()).append("=&am=").append(angleFormated)
						.append("&cu=INR&mc=5411");
				transactionResponseDto.setQrGenerationString(qrGenerationCode.toString());
				transactionResponseDto.setQrTableId(iciciQuickresponseTransactionDetails.getId());
			} else {
				throw new EntityAlreadyExistsException("You are exceeding the amount limit", ErrorCodes.AMOUNT_LIMIT);
			}
		} else {
			throw new EntityAlreadyExistsException("User id not pereset in database", ErrorCodes.ENITITY_NOT_FOUND);
		}
		return transactionResponseDto;

	}

	public String iciciUpiTransactionService(String encryptedMessage) {
		logger.info("callWebService method start");

		String updatePaytmProductMappingUrl = "https://apibankingone.icicibank.com/api/MerchantAPI/UPI/v0/QR3/507802";
		WebTarget webTarget = client.target(updatePaytmProductMappingUrl);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN).accept(MediaType.TEXT_PLAIN);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
		map.add("Content-Type", "text/plain");

		invocationBuilder.headers(map);
		Response response = invocationBuilder.post(Entity.entity(encryptedMessage, MediaType.TEXT_PLAIN));

		String decryptedresponse = response.readEntity(String.class);
		logger.info("decryptedresponse" + decryptedresponse);
		String successResponse = generateDecryptedKey(decryptedresponse, iciciUpiIntegrationPrivateKey);
		logger.info("responseFromQr" + successResponse);
		// if (response.getStatus() == 200) {
		Gson gson = new Gson();
		QRTransactionDetailsResponseDto transactionDetailsResponseDto = gson.fromJson(successResponse,
				QRTransactionDetailsResponseDto.class);
		if (transactionDetailsResponseDto != null) {
			String merchantTranId = transactionDetailsResponseDto.getMerchantTranId();
			IciciQuickresponseTransactionDetails transactionDetails = iciciQuickresponseTransactionDetailsRepo
					.getTransactionDetailsByMerchantId(merchantTranId);
			if (transactionDetails != null) {
				transactionDetails.setRefId(transactionDetailsResponseDto.getRefId());

				// transactionDetails.setQrResponse(successResponse);
				iciciQuickresponseTransactionDetailsRepo.save(transactionDetails);
			}

		}
		logger.info("callWebService method end");
		return successResponse;

	}

	@Override
	public TransactionStatusCheckResponseDto transactionStatusCheck(
			TransactionStatusCheckRequestDto transactionStatusCheckRequestDto) {
		logger.info("transactionStatusCheck method start");
		TransactionStatusCheckResponseDto transactionStatusCheckResponseDto = new TransactionStatusCheckResponseDto();
		Gson gson = new Gson();
		String message = gson.toJson(transactionStatusCheckRequestDto);
		String encryptedTransactionStatus = generateEncrypetedKey(message, iciciUpiIntegrationPublicKey);
		String transactionTargetUrl = "https://apibankingone.icicibank.com/api/MerchantAPI/UPI/v0/TransactionStatus3/507802";
		WebTarget webTarget = client.target(transactionTargetUrl);
		Invocation.Builder invocationBulider = webTarget.request(MediaType.TEXT_PLAIN).accept(MediaType.TEXT_PLAIN);
		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
		map.add("contentType", "text/plain");
		invocationBulider.headers(map);
		Response response = invocationBulider.post(Entity.entity(encryptedTransactionStatus, MediaType.TEXT_PLAIN));
		String decryptedResponse = response.readEntity(String.class);
		String decryptedTransactionStatus = generateDecryptedKey(decryptedResponse, iciciUpiIntegrationPrivateKey);
		Gson gson1 = new Gson();
		transactionStatusCheckResponseDto = gson1.fromJson(decryptedTransactionStatus,
				TransactionStatusCheckResponseDto.class);
		logger.info("transactionStatusCheck method end");
		return transactionStatusCheckResponseDto;

	}

	@Override
	public String callResponseForQr(String encryptedCallBack) {
		String message = null;
		logger.info("callResponseForQr method start");
		logger.info("encryptedCallBack" + encryptedCallBack);

		String afterDecryption = generateDecryptedKey(encryptedCallBack, iciciUpiIntegrationPrivateKey);
		logger.info("afterDecryption" + afterDecryption);
		if (afterDecryption != null) {
			Gson gson = new Gson();
			QRCallBackResponseDto callBackResponseDto = gson.fromJson(afterDecryption, QRCallBackResponseDto.class);
			if (callBackResponseDto != null) {
				DecimalFormat myFormat = new DecimalFormat("#,###");
				String refId = callBackResponseDto.getMerchantTranId().replace("=", "");
				refId = refId.replaceAll("\\s", "");
				logger.info("refId :" + refId);
				IciciQuickresponseTransactionDetails iciciQuickresponseTransactionDetails = iciciQuickresponseTransactionDetailsRepo
						.getTransactionDetailsByRefId("MNO" + refId);

				if (iciciQuickresponseTransactionDetails != null) {

					if (callBackResponseDto.getSubMerchantId() != null) {
						iciciQuickresponseTransactionDetails.setSubMerchantId(callBackResponseDto.getSubMerchantId());
					}
					if (callBackResponseDto.getBankRRN() != null) {
						iciciQuickresponseTransactionDetails.setBankRRN(callBackResponseDto.getBankRRN());
					}
					iciciQuickresponseTransactionDetails.setMerchantTransactionId(refId);
					if (callBackResponseDto.getTxnStatus().equalsIgnoreCase(
							IciciQuickresponseTransactionDetails.TransactionStatus.SUCCESS.toString())) {
						iciciQuickresponseTransactionDetails
								.setTransactionStatus(IciciQuickresponseTransactionDetails.TransactionStatus.SUCCESS);

						LenderOxyWallet lenderWalletDetails = new LenderOxyWallet();
						lenderWalletDetails.setUserId(iciciQuickresponseTransactionDetails.getUserId());
						lenderWalletDetails
								.setScrowAccountNumber("OXYLRV" + iciciQuickresponseTransactionDetails.getUserId());
						double amountInDouble = Double.parseDouble(callBackResponseDto.getPayerAmount());
						Integer amountInInteger = (int) (amountInDouble);
						lenderWalletDetails.setTransactionAmount(amountInInteger);
						lenderWalletDetails.setTransactionDate(new Date());
						lenderWalletDetails.setStatus(LenderOxyWallet.Status.APPROVED);
						lenderWalletDetails.setComments("RECEIVED");
						lenderWalletDetails.setTransactionType("credit");
						lenderWalletDetails
								.setUniqueTransactionReference(iciciQuickresponseTransactionDetails.getSubMerchantId());
						lenderWalletDetails.setIciciDecryptedData(afterDecryption);
						lenderWalletDetails.setRemarks("QR");
						lenderOxyWalletRepo.save(lenderWalletDetails);
						lenderWalletHistoryServiceRepo.lenderTransactionHistory(
								iciciQuickresponseTransactionDetails.getUserId(), amountInDouble, "CREDIT", "QR", 0);

						User user = userRepo.findById(iciciQuickresponseTransactionDetails.getUserId()).get();
						String currentDate = null;
						if (user != null) {
							String mobileNumber = null;
							if (user.getPersonalDetails().getWhatsAppNumber() != null) {
								mobileNumber = user.getPersonalDetails().getWhatsAppNumber();
							} else {
								mobileNumber = "91" + user.getMobileNumber();
							}
							Date date = new Date();
							currentDate = expectedDateFormat.format(date);

							String messageToUser = "This is to inform you that an amount of Rs. " + "*"
									+ myFormat.format(amountInInteger) + "*" + " has been credited to your account No. "
									+ lenderWalletDetails.getScrowAccountNumber() + " bankRRN No. "
									+ callBackResponseDto.getBankRRN() + " on " + currentDate + ".";

							whatsappMessageUsingUltra(mobileNumber, messageToUser);
							whatsappMessageWithNewInstance("917702795895-1630419144@g.us", messageToUser);

						}

						String lenderWallet = "Wallet Loaded";

						String emailTemplateName = "lender-icici-scrow-wallet.template";

						TemplateContext templateContext = new TemplateContext();
						String name = user.getPersonalDetails().getFirstName() + " "
								+ user.getPersonalDetails().getLastName();

						String emailMessage = "This is to inform you that an amount of Rs. "
								+ myFormat.format(amountInInteger) + " has been credited  to your account No."
								+ lenderWalletDetails.getScrowAccountNumber() + " from QR on " + currentDate;

						templateContext.put("emailMessage", emailMessage);
						templateContext.put("utr", callBackResponseDto.getBankRRN());
						templateContext.put("name", name);

						EmailRequest emailRequest1 = new EmailRequest(
								new String[] { user.getEmail()}, lenderWallet,
								emailTemplateName, templateContext);
						
						EmailResponse emailResponse1 = emailService.sendEmail(emailRequest1);
						if (emailResponse1.getStatus() == EmailResponse.Status.FAILED) {
							throw new RuntimeException(emailResponse1.getErrorMessage());
						}

						String templateName = "WALLET UPDATED1";
						MobileRequest mobileRequest = new MobileRequest(user.getMobileNumber(), templateName);
						mobileRequest.setSenderId("OXYLON");

						Map<String, String> variblesMap = new HashMap<>();
						variblesMap.put("VAR1", "QR");
						variblesMap.put("VAR2", callBackResponseDto.getBankRRN());
						variblesMap.put("VAR3", myFormat.format(amountInInteger));
						variblesMap.put("VAR4", lenderWalletDetails.getScrowAccountNumber());
						variblesMap.put("VAR5", currentDate);
						mobileRequest.setVariblesMap(variblesMap);
						try {
							mobileUtil.sendTransactionalMessage(mobileRequest);
						} catch (Exception e1) {
							logger.error("SMS failed : ", e1);
						}

					}
					iciciQuickresponseTransactionDetails.setCallBackReceviedOn(new Date());
					iciciQuickresponseTransactionDetails.setCallBackResponse(afterDecryption);
					iciciQuickresponseTransactionDetailsRepo.save(iciciQuickresponseTransactionDetails);

				}
				message = "SUCCESS";
			}
		}
		logger.info("callResponseForQr method end");
		return message;

	}

	public String generateEncrypetedKey(String message, String publicKey) {
		logger.info("generateEncrypetedKey method start");
		String encryptedmsg = null;
		byte[] messageBytes;
		byte[] tempPub = null;
		String sPub = null;
		byte[] encryptedKey = null;
		byte[] textBytes = null;

		try {
			SecureRandom secureRandom = new SecureRandom();
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

			// String certFile = iciciUpiIntegrationPublicKey;
			InputStream inStream = new FileInputStream(publicKey);
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			X509Certificate cert = (X509Certificate) cf.generateCertificate(inStream);
			inStream.close();

			messageBytes = message.getBytes();
			RSAPublicKey pubkey = (RSAPublicKey) cert.getPublicKey();
			tempPub = pubkey.getEncoded();
			sPub = new String(tempPub);
			// Initialize the cipher for encryption
			cipher.init(Cipher.ENCRYPT_MODE, pubkey, secureRandom);

			// Encrypt the message
			byte[] ciphertextBytes = cipher.doFinal(messageBytes);
			encryptedmsg = new String(Base64.getEncoder().encodeToString(ciphertextBytes));

			return encryptedmsg;

		} catch (Exception e) {
			e.printStackTrace();

		}
		logger.info("generateEncrypetedKey method end");
		return encryptedmsg;

	}

	public String generateDecryptedKey(String decryptedresponse, String privateKey) {
		logger.info("generateDecryptedKey method start");
		String output = null;
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			SecureRandom secureRandom = new SecureRandom();

			// String keyFile = iciciUpiIntegrationPrivateKey;

			InputStream inStream = new FileInputStream(privateKey);
			byte[] encKey = new byte[inStream.available()];
			inStream.read(encKey);
			inStream.close();

			String pvtKey = new String(encKey);

			pvtKey = pvtKey.replaceAll("\\n", "").replace("-----BEGIN PRIVATE KEY-----", "")
					.replace("-----END PRIVATE KEY-----", "");

			PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(pvtKey));
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");

			PrivateKey priv = (RSAPrivateKey) keyFactory.generatePrivate(privKeySpec);

			cipher.init(Cipher.DECRYPT_MODE, priv, secureRandom);

			byte[] ciphertextBytes = Base64.getDecoder().decode(decryptedresponse.getBytes(StandardCharsets.UTF_8));

			output = new String(cipher.doFinal(ciphertextBytes));

			return output;
		} catch (Exception e) {
			e.printStackTrace();

		}
		logger.info("generateDecryptedKey method end");
		return output;
	}

	@Override
	public TransactionResponseDto qrStatusBasedOnId(int id) {
		IciciQuickresponseTransactionDetails transactionDetails = iciciQuickresponseTransactionDetailsRepo.findById(id)
				.get();

		if (transactionDetails != null) {
			if (transactionDetails.getTransactionStatus()
					.equals(IciciQuickresponseTransactionDetails.TransactionStatus.INITIATED)) {
				transactionDetails
						.setTransactionStatus(IciciQuickresponseTransactionDetails.TransactionStatus.NOTYETSCANNED);
				iciciQuickresponseTransactionDetailsRepo.save(transactionDetails);

			}

		} else {
			throw new EntityAlreadyExistsException("Qr status is not found based on id", ErrorCodes.ENITITY_NOT_FOUND);
		}

		TransactionResponseDto transactionResponseDto = new TransactionResponseDto();
		transactionResponseDto.setStatus(transactionDetails.getTransactionStatus().toString());
		transactionResponseDto.setUserId(transactionDetails.getUserId());
		return transactionResponseDto;

	}

	@Override
	public QRSuccessTransactionsDetails getSuccessTransactionsFromQR(int userId, PaginationRequestDto pageRequestDto) {
		QRSuccessTransactionsDetails qrSuccessTransactionsDetails = new QRSuccessTransactionsDetails();
		int pageSize = pageRequestDto.getPageSize();
		int pageNo = (pageRequestDto.getPageSize() * (pageRequestDto.getPageNo() - 1));
		List<IciciQuickresponseTransactionDetails> listOfSuccessTransactions = iciciQuickresponseTransactionDetailsRepo
				.listOfSuccessTransactions(userId, pageSize, pageNo);
		List<TransactionResponseDto> transactionResponseDto = new ArrayList<TransactionResponseDto>();
		if (listOfSuccessTransactions != null && !listOfSuccessTransactions.isEmpty()) {
			listOfSuccessTransactions.forEach(transactionInfo -> {
				TransactionResponseDto transactionResponse = new TransactionResponseDto();
				transactionResponse.setUserId(userId);
				transactionResponse.setAmount(transactionInfo.getAmount());
				transactionResponse.setMerchantTransactionId(transactionInfo.getMerchantTransactionId());
				transactionResponse.setBillNumber(transactionInfo.getBillNumber());
				transactionResponse.setBankRRN(transactionInfo.getBankRRN());
				transactionResponse
						.setTransactionDate(expectedDateFormat.format(transactionInfo.getCallBackReceviedOn()));
				transactionResponseDto.add(transactionResponse);

			});
			qrSuccessTransactionsDetails.setTransactionResponseDto(transactionResponseDto);
			qrSuccessTransactionsDetails
					.setTotalCount(iciciQuickresponseTransactionDetailsRepo.listOfSuccessTransactionsCount(userId));
		}

		return qrSuccessTransactionsDetails;

	}

	public String whatsappMessageUsingUltra(String receiver, String message) {
		WebTarget webTarget = client.target(whatsappUsingUltraApi);
		Invocation.Builder invocationBuider = webTarget.request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON);

		MultivaluedMap<String, Object> map1 = new MultivaluedHashMap<String, Object>();

		map1.add("Content-Type", "application/json");
		invocationBuider.headers(map1);
		WtappIndividualGroupRequestDto wtappIndividualGroupRequestDto = new WtappIndividualGroupRequestDto();

		wtappIndividualGroupRequestDto.setTo(receiver);
		wtappIndividualGroupRequestDto.setBody(message);
		wtappIndividualGroupRequestDto.setToken(whatsappUltraToken);

		Response responsWtappGroup = invocationBuider.post(Entity.json(wtappIndividualGroupRequestDto));

		if (responsWtappGroup.getStatus() == 200) {
			logger.info("message sent to from qr: " + receiver);

		} else {
			logger.info("message not sent from qr: " + receiver);

		}
		return null;
	}

	public String whatsappMessageWithNewInstance(String chatId, String message) {
		try {
			WebTarget webTarget = client.target(whatsappUsingUltraNewInstance);
			Invocation.Builder invocationBuider = webTarget.request(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON);

			MultivaluedMap<String, Object> map1 = new MultivaluedHashMap<String, Object>();

			map1.add("Content-Type", "application/json");
			invocationBuider.headers(map1);
			WtappIndividualGroupRequestDto wtappIndividualGroupRequestDto = new WtappIndividualGroupRequestDto();

			wtappIndividualGroupRequestDto.setTo(chatId);
			wtappIndividualGroupRequestDto.setBody(message);
			wtappIndividualGroupRequestDto.setToken(whatsappUltraTokenForNewInstance);

			Response responsWtappGroup = invocationBuider.post(Entity.json(wtappIndividualGroupRequestDto));

			if (responsWtappGroup.getStatus() == 200) {
				logger.info("message sent to chat id : ");

			} else {
				logger.info("message not sent to chat id: ");

			}

		} catch (Exception e) {
			logger.info("Exception in whatsappMessageToGroup method");
		}
		return null;

	}
}
