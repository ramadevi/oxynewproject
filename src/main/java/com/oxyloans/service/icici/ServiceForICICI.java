package com.oxyloans.service.icici;

import com.oxyloans.icici.dto.RequestFromICICI;
import com.oxyloans.transactionalert.TransactionAlertResponseDto;

public interface ServiceForICICI {
	public String decryptingTheDataByPriavteKey(RequestFromICICI requestFromICICI);

	public TransactionAlertResponseDto transactionAlert(RequestFromICICI requestFromICICI);
}
