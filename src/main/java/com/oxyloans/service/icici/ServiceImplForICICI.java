package com.oxyloans.service.icici;

import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.transaction.Transactional;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.icici.upi.OxyLoanTransactionAlerts;
import com.oxyloans.entity.icici.upi.OxyLoanTransactionAlertsRepo;
import com.oxyloans.entity.lender.oxywallet.LenderICICITransactionRequest;
import com.oxyloans.entity.lender.oxywallet.LenderICICITransactionRequestRepo;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWallet;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletRepo;
import com.oxyloans.entity.user.User;
import com.oxyloans.icici.dto.RequestFromICICI;
import com.oxyloans.icici.dto.ResponseForICICI;
import com.oxyloans.icici.dto.ResponseToICICI;
import com.oxyloans.icici.dto.TransactionFromICICI;
import com.oxyloans.mobile.MobileUtil;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.response.user.WtappIndividualGroupRequestDto;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.service.loan.LenderWalletHistoryServiceRepo;
import com.oxyloans.transactionalert.TransactionAlertRequestDto;
import com.oxyloans.transactionalert.TransactionAlertResponseDto;
import com.oxyloans.whatappservice.WhatappService;

@Service
public class ServiceImplForICICI implements ServiceForICICI {

	@Autowired
	private LenderOxyWalletRepo lenderOxyWalletRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private LenderICICITransactionRequestRepo lenderICICITransactionRequestRepo;

	private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(ServiceImplForICICI.class);

	private final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Value("${privateKeyForICICI}")
	private String privateKeyForICICI;

	@Value("${publicKeyForICICI}")
	private String publicKeyForICICI;

	@Autowired
	private IEmailService emailService;

	@Autowired
	protected MobileUtil mobileUtil;

	@Value("${wtappApi}")
	private String wtappApi;

	@Autowired
	private WhatappService whatappService;

	private Client client = ClientBuilder.newClient().register(MultiPartFeature.class).register(MultiPartWriter.class);

	@Autowired
	private OxyLoanTransactionAlertsRepo OxyLoanTransactionAlertsRepo;

	@Value("${whatsappUsingUltraApi}")
	private String whatsappUsingUltraApi;

	@Value("${whatsappUltraToken}")
	private String whatsappUltraToken;

	@Value("${fdPublickey}")
	private String fdPublickey;

	@Value("${fdPrivatekey}")
	private String fdPrivatekey;

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Autowired
	private LenderWalletHistoryServiceRepo lenderWalletHistoryServiceRepo;

	@Override
	@Transactional
	public String decryptingTheDataByPriavteKey(RequestFromICICI requestFromICICI) {
		String encryptedResponse = null;
		logger.info("decryptingTheDataByPriavteKey method start");
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			SecureRandom secureRandom = new SecureRandom();

			String keyFile = privateKeyForICICI; // private Key

			// String keyFile = "C:/privateKeyForICICI/fintech.oxyloans.com.key.txt";

			InputStream inStream = new FileInputStream(keyFile);
			byte[] encKey = new byte[inStream.available()];
			inStream.read(encKey);
			inStream.close();

			LenderICICITransactionRequest lenderICICITransactionRequest = new LenderICICITransactionRequest();
			lenderICICITransactionRequest.setEncryptedKey(requestFromICICI.getEncryptedKey());
			lenderICICITransactionRequest.setEncryptedData(requestFromICICI.getEncryptedData());
			lenderICICITransactionRequestRepo.save(lenderICICITransactionRequest);

			String pvtKey = new String(encKey);
			pvtKey = pvtKey.replaceAll("\\s", "").replace("-----BEGIN PRIVATE KEY-----", "")
					.replace("-----END PRIVATE KEY-----", "");
			System.out.println("pvtKey" + pvtKey);
			PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(pvtKey));

			KeyFactory keyFactory = KeyFactory.getInstance("RSA");

			PrivateKey priv = (RSAPrivateKey) keyFactory.generatePrivate(privKeySpec);

			cipher.init(Cipher.DECRYPT_MODE, priv, secureRandom);

			byte[] ciphertextBytes = Base64.getDecoder()
					.decode(requestFromICICI.getEncryptedKey().getBytes(StandardCharsets.UTF_8));

			String key = new String(cipher.doFinal(ciphertextBytes));

			String decryptedData = decryptByAES(key, requestFromICICI.getEncryptedData());

			if (decryptedData != null) {
				lenderICICITransactionRequest.setIciciDecryptedData(decryptedData);
				lenderICICITransactionRequestRepo.save(lenderICICITransactionRequest);
			}
			ResponseToICICI responseToICICI = new ResponseToICICI();

			Gson gson = new Gson();
			DecimalFormat myFormat = new DecimalFormat("#,###");
			TransactionFromICICI transactionFromICICI = gson.fromJson(decryptedData, TransactionFromICICI.class);

			LenderOxyWallet lenderOxyWallet = lenderOxyWalletRepo
					.findingByUniqueTransactionReference(transactionFromICICI.getUTR());
			logger.info("ecollection utr" + transactionFromICICI.getUTR());

			if (lenderOxyWallet == null) {

				User user = userRepo
						.findById(Integer.parseInt(transactionFromICICI.getVirtualAccountNumber().substring(6))).get();

				if (user != null) {
					LenderOxyWallet lenderWalletDetails = new LenderOxyWallet();
					lenderWalletDetails.setUserId(user.getId());
					lenderWalletDetails.setScrowAccountNumber(transactionFromICICI.getVirtualAccountNumber());
					lenderWalletDetails.setTransactionAmount(Integer.parseInt(transactionFromICICI.getAmount()));
					lenderWalletDetails.setTransactionDate(new Date());
					lenderWalletDetails.setStatus(LenderOxyWallet.Status.APPROVED);
					lenderWalletDetails.setComments("RECEIVED");
					lenderWalletDetails.setTransactionType("credit");
					lenderWalletDetails.setUniqueTransactionReference(transactionFromICICI.getUTR());
					lenderOxyWalletRepo.save(lenderWalletDetails);
					lenderWalletDetails.setIciciDecryptedData(decryptedData);
					if (transactionFromICICI.getSenderRemark() != null) {
						lenderWalletDetails.setRemarks(transactionFromICICI.getSenderRemark());
					}
					/*
					 * if (transactionFromICICI.getPayerName() != null) {
					 * 
					 * if (transactionFromICICI.getPayerName().substring(0,
					 * 6).equalsIgnoreCase("OXYP2W")) {
					 * 
					 * OxyBorrowersDealsInformation oxyBorrowersDealsInformation =
					 * oxyBorrowersDealsInformationRepo .findDealAlreadyGiven(
					 * Integer.valueOf(transactionFromICICI.getPayerName().substring(6))); if
					 * (oxyBorrowersDealsInformation != null) {
					 * lenderWalletDetails.setDealId(oxyBorrowersDealsInformation.getId());
					 * lenderWalletDetails.setComments("SYSTEM");
					 * lenderWalletDetails.setRemarks(oxyBorrowersDealsInformation.getDealName());
					 * lenderWalletHistoryServiceRepo.calculatingOutstandingAmount(user.getId(),
					 * Double.parseDouble(transactionFromICICI.getAmount()), "DEBIT", "PRW",
					 * oxyBorrowersDealsInformation.getId()); }
					 * 
					 * }
					 * 
					 * }
					 */
					lenderOxyWalletRepo.save(lenderWalletDetails);
					lenderWalletHistoryServiceRepo.lenderTransactionHistory(user.getId(),
							Double.parseDouble(transactionFromICICI.getAmount()), "CREDIT", "EC", 0);

					String lenderWallet = "Wallet Loaded";

					String emailTemplateName = "lender-icici-scrow-wallet.template";

					TemplateContext templateContext = new TemplateContext();
					String name = user.getPersonalDetails().getFirstName() + " "
							+ user.getPersonalDetails().getLastName();

					String emailMessage = "This is to inform you that an amount of Rs. "
							+ myFormat.format(lenderWalletDetails.getTransactionAmount())
							+ " has been credited  to your account No." + lenderWalletDetails.getScrowAccountNumber()
							+ " from" + transactionFromICICI.getPayerAccNumber() + " through"
							+ transactionFromICICI.getMode() + " on " + transactionFromICICI.getPayerPaymentDate();

					templateContext.put("emailMessage", emailMessage);
					templateContext.put("utr", lenderWalletDetails.getUniqueTransactionReference());
					templateContext.put("name", name);

					EmailRequest emailRequest1 = new EmailRequest(new String[] { user.getEmail() }, lenderWallet,
							emailTemplateName, templateContext);
					EmailResponse emailResponse1 = emailService.sendEmail(emailRequest1);
					if (emailResponse1.getStatus() == EmailResponse.Status.FAILED) {
						throw new RuntimeException(emailResponse1.getErrorMessage());
					}

					String mobileNumber = user.getPersonalDetails().getWhatsAppNumber() != null
							? user.getPersonalDetails().getWhatsAppNumber()
							: "91" + user.getMobileNumber();

					String message = "This is to inform you that an amount of Rs. "
							+ lenderWalletDetails.getTransactionAmount() + " has been credited to your account No. "
							+ lenderWalletDetails.getScrowAccountNumber() + " from account No. "
							+ transactionFromICICI.getPayerAccNumber() + " through NEFT on "
							+ transactionFromICICI.getPayerPaymentDate() + ".";

					WebTarget webTarget = client.target(whatsappUsingUltraApi);
					Invocation.Builder invocationBuider = webTarget.request(MediaType.APPLICATION_JSON)
							.accept(MediaType.APPLICATION_JSON);

					MultivaluedMap<String, Object> map1 = new MultivaluedHashMap<String, Object>();

					map1.add("Content-Type", "application/json");
					invocationBuider.headers(map1);
					WtappIndividualGroupRequestDto wtappIndividualGroupRequestDto = new WtappIndividualGroupRequestDto();
					wtappIndividualGroupRequestDto.setToken(whatsappUltraToken);
					for (int i = 0; i <= 1; i++) {
						if (i == 0) {
							wtappIndividualGroupRequestDto.setTo(mobileNumber);
							wtappIndividualGroupRequestDto.setBody(message);
						} else {
							wtappIndividualGroupRequestDto.setTo("917702795895-1630419144@g.us");
							wtappIndividualGroupRequestDto.setBody(message);
						}

						Response responsWtappGroup = invocationBuider.post(Entity.json(wtappIndividualGroupRequestDto));

						if (responsWtappGroup.getStatus() == 200) {
							logger.info("message sent from ecollection: " + mobileNumber);

						} else {
							logger.info("message not sent from ecollection: " + mobileNumber);

						}
					}
					/*
					 * String templateName = "WALLET UPDATED1"; MobileRequest mobileRequest = new
					 * MobileRequest(user.getMobileNumber(), templateName);
					 * mobileRequest.setSenderId("OXYLON");
					 * 
					 * Map<String, String> variblesMap = new HashMap<>(); variblesMap.put("VAR1",
					 * mode); variblesMap.put("VAR2", utr); variblesMap.put("VAR3", amount);
					 * variblesMap.put("VAR4", virtualAccountNumber); variblesMap.put("VAR5",
					 * payerDate); mobileRequest.setVariblesMap(variblesMap); try {
					 * mobileUtil.sendTransactionalMessage(mobileRequest); } catch (Exception e1) {
					 * logger.error("SMS failed : ", e1); }
					 */

				}

				responseToICICI.setResponse("Success");
				responseToICICI.setCode("11");
			} else {
				responseToICICI.setResponse("Duplicate UTR");
				responseToICICI.setCode("06");
			}

			Gson gsonValu = new Gson();
			String iciciResponse = gsonValu.toJson(responseToICICI);
			encryptedResponse = encryptingDataByPublicKey(iciciResponse);

		} catch (Exception e) {
			// logger.info("Exception in decryptingTheDataByPriavteKey method");
			System.out.println(e);

		}
		logger.info("decryptingTheDataByPriavteKey method end");
		return encryptedResponse;
	}

	public static String decryptByAES(String secretKey, String cipherText) throws Exception {
		logger.info("decryptByAES method start");
		int INIT_VECTOR_LENGTH = 16;

		byte[] encrypted = Base64.getDecoder().decode(cipherText);

		IvParameterSpec ivParameterSpec = new IvParameterSpec(encrypted, 0, INIT_VECTOR_LENGTH);

		SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes("UTF-8"), "AES");

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);

		String decryptedData = new String(
				cipher.doFinal(encrypted, INIT_VECTOR_LENGTH, encrypted.length - INIT_VECTOR_LENGTH));
		logger.info("decryptByAES method end");
		return decryptedData;
	}

	public String encryptingDataByPublicKey(String message) {
		logger.info("getEncryptedMessageByUsingPublicKey method start");
		byte[] messageBytes;
		byte[] tempPub = null;
		String sPub = null;
		byte[] encryptedKey = null;
		byte[] textBytes = null;

		try {
			SecureRandom secureRandom = new SecureRandom();
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			String certFile = publicKeyForICICI;

			// public key
			// String certFile = "C:/privateKeyForICICI/icicipub.cer";
			InputStream inStream = new FileInputStream(certFile);
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			X509Certificate cert = (X509Certificate) cf.generateCertificate(inStream);
			inStream.close();

			messageBytes = message.getBytes();

			String sessionkey = "1234567890123456";
			byte[] sessionkeyBytes = sessionkey.getBytes();
			String iv = "1234567890123456";
			byte[] initVectorBytes = iv.getBytes();
			RSAPublicKey pubkey = (RSAPublicKey) cert.getPublicKey();
			tempPub = pubkey.getEncoded();
			sPub = new String(tempPub);

			cipher.init(Cipher.ENCRYPT_MODE, pubkey, secureRandom);

			encryptedKey = cipher.doFinal(sessionkeyBytes);
			String encryptedkey = new String(Base64.getEncoder().encodeToString(encryptedKey));

			IvParameterSpec ivParameterSpec = new IvParameterSpec(initVectorBytes);
			byte[] ENCR_DATA = encryptSendData(message, initVectorBytes, ivParameterSpec);
			String encryptedData = Base64.getEncoder().encodeToString(ENCR_DATA);
			String request = createJsonRequest(encryptedData, encryptedkey);

			return request;

		} catch (Exception e) {
			logger.info("Exception in getEncryptedMessageByUsingPublicKey method");
		}
		logger.info("getEncryptedMessageByUsingPublicKey method end");
		return null;
	}

	public static byte[] encryptSendData(String plainText, byte[] key, IvParameterSpec ivParameterSpec)
			throws Exception {
		logger.info("encryptSendData method start");
		byte[] clean = plainText.getBytes();

		SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
		byte[] encrypted = cipher.doFinal(clean);

		byte[] encryptedIVAndText = new byte[key.length + encrypted.length];
		System.arraycopy(key, 0, encryptedIVAndText, 0, key.length);
		System.arraycopy(encrypted, 0, encryptedIVAndText, key.length, encrypted.length);
		logger.info("encryptSendData method end");
		return encryptedIVAndText;
	}

	public String createJsonRequest(String encryptedData, String encryptedKey) {
		logger.info("createJsonRequest method start");
		ResponseForICICI responseForICICI = new ResponseForICICI();
		responseForICICI.setEncryptedKey(encryptedKey);
		responseForICICI.setIv("");
		responseForICICI.setEncryptedData(encryptedData);
		Gson gsonValu = new Gson();
		String encryptedResponse = gsonValu.toJson(responseForICICI);
		logger.info("createJsonRequest method end");
		return encryptedResponse;
	}

	@Override
	public TransactionAlertResponseDto transactionAlert(RequestFromICICI requestFromICICI) {
		logger.info("transactionAlert method start");

		TransactionAlertResponseDto transactionAlertResponseDto = new TransactionAlertResponseDto();
		String encryptedResponse = null;
		String decryptedData = null;
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			SecureRandom secureRandom = new SecureRandom();

			String keyFile = privateKeyForICICI; // private Key

			InputStream inStream = new FileInputStream(keyFile);
			byte[] encKey = new byte[inStream.available()];
			inStream.read(encKey);
			inStream.close();

			String encryptedKey = requestFromICICI.getEncryptedKey();
			logger.info("transactionAlert method encryptedKey :" + encryptedKey);
			String encryptedData = requestFromICICI.getEncryptedData();
			logger.info("transactionAlert method encryptedData:" + encryptedData);
			String pvtKey = new String(encKey);
			pvtKey = pvtKey.replaceAll("\\s", "").replace("-----BEGIN PRIVATE KEY-----", "")
					.replace("-----END PRIVATE KEY-----", "");
			System.out.println("pvtKey" + pvtKey);
			PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(pvtKey));

			KeyFactory keyFactory = KeyFactory.getInstance("RSA");

			PrivateKey priv = (RSAPrivateKey) keyFactory.generatePrivate(privKeySpec);

			cipher.init(Cipher.DECRYPT_MODE, priv, secureRandom);

			byte[] ciphertextBytes = Base64.getDecoder().decode(encryptedKey.getBytes(StandardCharsets.UTF_8));

			String key = new String(cipher.doFinal(ciphertextBytes));

			decryptedData = decryptByAES(key, encryptedData);
			Gson gson = new Gson();
			logger.info("decryptedData data " + decryptedData);

			if (decryptedData != null) {
				TransactionAlertRequestDto transactionAlertRequestDto = gson.fromJson(decryptedData,
						TransactionAlertRequestDto.class);

				String transactionDate = null;
				String bankId = null;
				String valueDate = null;
				String amount = null;
				String balance = null;
				String branchId = null;
				String narration = null;
				String txType = null;
				String txSubType = null;
				String accountNumber = null;
				if (transactionAlertRequestDto.getTransactionDate() != null) {
					transactionDate = transactionAlertRequestDto.getTransactionDate();

				}
				if (transactionAlertRequestDto.getBankId() != null) {
					bankId = transactionAlertRequestDto.getBankId();
				}
				if (transactionAlertRequestDto.getValueDate() != null) {
					valueDate = transactionAlertRequestDto.getValueDate();
				}
				if (transactionAlertRequestDto.getAmount() != null) {
					amount = transactionAlertRequestDto.getAmount();
				}
				if (transactionAlertRequestDto.getBalance() != null) {
					balance = transactionAlertRequestDto.getBalance();
				}
				if (transactionAlertRequestDto.getBranchId() != null) {
					branchId = transactionAlertRequestDto.getBranchId();
				}
				if (transactionAlertRequestDto.getNarration() != null) {
					narration = transactionAlertRequestDto.getNarration();
				}
				if (transactionAlertRequestDto.getTxType() != null) {
					txType = transactionAlertRequestDto.getTxType();
				}
				if (transactionAlertRequestDto.getTxSubType() != null) {
					txSubType = transactionAlertRequestDto.getTxSubType();
				}
				if (transactionAlertRequestDto.getAccountNumber() != null) {
					accountNumber = transactionAlertRequestDto.getAccountNumber();
				}
				String transactionType = null;
				String message = null;
				BigInteger amountWithOnlyNumeric = null;
				double amountInDouble = Double.parseDouble(amount);
				amountWithOnlyNumeric = BigDecimal.valueOf(amountInDouble).toBigInteger();

				String currentDateWithTime = new SimpleDateFormat("dd-MM-yyyy.HH.mm.ss").format(new Date());

				if (txType.equalsIgnoreCase("D")) {
					transactionType = "DEBITED";
					message = "This is to inform you that an amount of Rs. " + amountWithOnlyNumeric
							+ " has been debited from account number " + accountNumber + " Narration " + narration
							+ " on " + currentDateWithTime;
				} else {
					transactionType = "CREDITED";
					message = "This is to inform you that an amount of Rs. " + amountWithOnlyNumeric
							+ " has been credited to account number " + accountNumber + " Narration " + narration
							+ " on " + currentDateWithTime;
				}

				OxyLoanTransactionAlerts checkAlertIndb = OxyLoanTransactionAlertsRepo
						.checkTransactionAlertAlreadyUpdate(transactionDate, bankId, valueDate, amount,
								transactionType);
				if (checkAlertIndb == null) {
					OxyLoanTransactionAlerts oxyLoanTransactionAlerts = new OxyLoanTransactionAlerts();
					oxyLoanTransactionAlerts.setTransactionInitiatedDate(transactionDate);
					oxyLoanTransactionAlerts.setBankTransactionId(bankId);
					oxyLoanTransactionAlerts.setTransactionCompletedDate(valueDate);
					oxyLoanTransactionAlerts.setBalance(balance);
					oxyLoanTransactionAlerts.setAmount(amount);
					oxyLoanTransactionAlerts.setBranchId(branchId);
					oxyLoanTransactionAlerts.setNarration(narration);
					oxyLoanTransactionAlerts.setAccountNumber(accountNumber);
					oxyLoanTransactionAlerts.setTransationAlertResponse(decryptedData);

					oxyLoanTransactionAlerts
							.setTransactionType(OxyLoanTransactionAlerts.TransactionType.valueOf(transactionType));
					if (txType.equalsIgnoreCase("BI")) {
						oxyLoanTransactionAlerts.setTransactionSubType(OxyLoanTransactionAlerts.TransactionSubType.BI);
					} else if (txType.equalsIgnoreCase("CI")) {
						oxyLoanTransactionAlerts.setTransactionSubType(OxyLoanTransactionAlerts.TransactionSubType.CI);
					} else if (txType.equalsIgnoreCase("I")) {
						oxyLoanTransactionAlerts.setTransactionSubType(OxyLoanTransactionAlerts.TransactionSubType.I);
					} else if (txType.equalsIgnoreCase("O")) {
						oxyLoanTransactionAlerts.setTransactionSubType(OxyLoanTransactionAlerts.TransactionSubType.O);
					} else if (txType.equalsIgnoreCase("IC")) {
						oxyLoanTransactionAlerts.setTransactionSubType(OxyLoanTransactionAlerts.TransactionSubType.IC);
					} else if (txType.equalsIgnoreCase("IP")) {
						oxyLoanTransactionAlerts.setTransactionSubType(OxyLoanTransactionAlerts.TransactionSubType.IP);
					} else if (txType.equalsIgnoreCase("SI")) {
						oxyLoanTransactionAlerts.setTransactionSubType(OxyLoanTransactionAlerts.TransactionSubType.SI);
					} else if (txType.equalsIgnoreCase("BS")) {
						oxyLoanTransactionAlerts.setTransactionSubType(OxyLoanTransactionAlerts.TransactionSubType.BS);
					} else if (txType.equalsIgnoreCase("SC")) {
						oxyLoanTransactionAlerts.setTransactionSubType(OxyLoanTransactionAlerts.TransactionSubType.SC);
					}
					oxyLoanTransactionAlerts.setAlertOn(new Date());
					OxyLoanTransactionAlertsRepo.save(oxyLoanTransactionAlerts);

					logger.info("alert check msg with chat id ..");
					String success = whatappService.sendingWhatsappMessageWithNewInstance("120363129716199064@g.us",
							message);
					logger.info("alert check msg with chat id i'm working..");
					Calendar currentDate = Calendar.getInstance();
					String expectedCurrentDate = expectedDateFormat.format(currentDate.getTime());
					TemplateContext templateContext = new TemplateContext();
					templateContext.put("transactionDate", transactionDate);
					templateContext.put("bankId", bankId);
					templateContext.put("valueDate", valueDate);
					templateContext.put("amount", amount);
					templateContext.put("balance", balance);
					templateContext.put("branchId", branchId);
					templateContext.put("narration", narration);
					templateContext.put("txType", txType);
					templateContext.put("txSubType", txSubType);
					templateContext.put("accountNumber", accountNumber);
					templateContext.put("CurrentDate", expectedCurrentDate);

					String trsansactionAlert = "Transaction Alert";

					String emailTemplateName = "transaction-alert.template";

					EmailRequest emailRequest1 = new EmailRequest(
							new String[] { "radhakrishana.t@oxyloans.com", "ramadevi@oxyloans.com",
									"archana.n@oxyloans.com", "subbu@oxyloans.com" },
							trsansactionAlert, emailTemplateName, templateContext);
					EmailResponse emailResponse1 = emailService.sendEmail(emailRequest1);
					if (emailResponse1.getStatus() == EmailResponse.Status.FAILED) {
						throw new RuntimeException(emailResponse1.getErrorMessage());
					}

					transactionAlertResponseDto.setSuccess("true");

				} else {
					transactionAlertResponseDto.setSuccess("duplicate");

				}
			}

		} catch (Exception e) {
			logger.info("exception in transactionAlert method");
		}
		logger.info("transactionAlert method end");
		return transactionAlertResponseDto;

	}

}
