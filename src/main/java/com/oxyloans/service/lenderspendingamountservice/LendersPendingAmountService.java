package com.oxyloans.service.lenderspendingamountservice;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.oxyloans.borrowerloanservice.LoanService;
import com.oxyloans.dealleveldisbursment.DealLevelInformation;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.cms.LenderCmsPayments;
import com.oxyloans.repository.cms.LenderCmsPaymentsRepo;
import com.oxyloans.entity.pending.amount.OxyLendersPendingAmount;
import com.oxyloans.entity.pending.amount.OxyLendersPendingAmountRepo;
import com.oxyloans.entity.user.User;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsFromDealsResponseDto;
import com.oxyloans.lender.withdrawalfunds.dto.ListOfLendersWithdrawalFundsInfo;
import com.oxyloans.lenderspendingamountdto.OxyLendersPendingAmountRequestDto;
import com.oxyloans.lenderspendingamountdto.OxyLendersPendingAmountResponseDto;
import com.oxyloans.lenderspendingamountdto.OxyStatusBasedPendingLendersRequest;
import com.oxyloans.lenderspendingamountdto.OxyStatusBasedPendingLendersResponse;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;

@Service
public class LendersPendingAmountService implements LendersPendingAmountServiceRepo {

	@Autowired
	private OxyLendersPendingAmountRepo oxyLendersPendingAmountRepo;

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	private final Logger logger = LogManager.getLogger(DealLevelInformation.class);

	private DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Value("${iciciFilePathBeforeApproval}")
	private String iciciFilePathBeforeApproval;

	@Autowired
	private LenderCmsPaymentsRepo lenderCmsPaymentsRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private IEmailService emailService;
	
	@Value("${mailsubject}")
	private String mailsubject1;

	@Autowired
	private LoanService loanService;

	@Override
	@Transactional
	public OxyLendersPendingAmount createUserPendingAmount(
			OxyLendersPendingAmountRequestDto oxyLendersPendingAmountRequestDto) { // used to send pending amount to
																					// lenders

		OxyLendersPendingAmount oxyLendersPendingAmount = null;
		try {

			if (oxyLendersPendingAmountRequestDto.getId() == 0) {
				oxyLendersPendingAmount = new OxyLendersPendingAmount();
				oxyLendersPendingAmount.setUserId(oxyLendersPendingAmountRequestDto.getUserId());
				oxyLendersPendingAmount.setAmount(oxyLendersPendingAmountRequestDto.getAmount());
				oxyLendersPendingAmount.setDealId(oxyLendersPendingAmountRequestDto.getDealId());
				OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
						.findById(oxyLendersPendingAmountRequestDto.getDealId()).get();
				if (oxyBorrowersDealsInformation != null) {
					oxyLendersPendingAmount.setDealName(oxyBorrowersDealsInformation.getDealName());

				}
				oxyLendersPendingAmount.setReason(oxyLendersPendingAmountRequestDto.getReason());
				oxyLendersPendingAmount.setTransactionType(oxyLendersPendingAmountRequestDto.getTransactionType());
				oxyLendersPendingAmount.setNumberOfDays(oxyLendersPendingAmountRequestDto.getNoOfDays());
				oxyLendersPendingAmount.setAmountType(
						OxyLendersPendingAmount.AmountType.valueOf(oxyLendersPendingAmountRequestDto.getAmountType()));
				oxyLendersPendingAmount.setCreatedDate(new Date());
				oxyLendersPendingAmountRepo.save(oxyLendersPendingAmount);

			} else {
				oxyLendersPendingAmount = oxyLendersPendingAmountRepo
						.findById(oxyLendersPendingAmountRequestDto.getId()).get();
				if (oxyLendersPendingAmount != null) {
					oxyLendersPendingAmount.setUserId(oxyLendersPendingAmountRequestDto.getUserId());
					oxyLendersPendingAmount.setAmount(oxyLendersPendingAmountRequestDto.getAmount());
					oxyLendersPendingAmount.setDealId(oxyLendersPendingAmountRequestDto.getDealId());
					OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
							.findById(oxyLendersPendingAmountRequestDto.getDealId()).get();
					if (oxyBorrowersDealsInformation != null) {
						oxyLendersPendingAmount.setDealName(oxyBorrowersDealsInformation.getDealName());

					}
					oxyLendersPendingAmount.setReason(oxyLendersPendingAmountRequestDto.getReason());
					oxyLendersPendingAmount.setTransactionType(oxyLendersPendingAmountRequestDto.getTransactionType());
					oxyLendersPendingAmount.setNumberOfDays(oxyLendersPendingAmountRequestDto.getNoOfDays());
					oxyLendersPendingAmount.setAmountType(OxyLendersPendingAmount.AmountType
							.valueOf(oxyLendersPendingAmountRequestDto.getAmountType()));
					oxyLendersPendingAmount.setCreatedDate(new Date());
					oxyLendersPendingAmountRepo.save(oxyLendersPendingAmount);

				}

			}
		} catch (Exception e) {
			logger.info("Exception in createUserPendingAmount method");
		}
		return oxyLendersPendingAmount;

	}

	@Override
	@Transactional
	public OxyLendersPendingAmountResponseDto getLenderPendingAmount(int id) { // userd to get single amount pending
																				// users by id
		OxyLendersPendingAmountResponseDto oxyLendersPendingAmountResponseDto = null;
		try {

			OxyLendersPendingAmount oxyLendersPendingAmount = oxyLendersPendingAmountRepo.findById(id).get();
			if (oxyLendersPendingAmount != null) {
				oxyLendersPendingAmountResponseDto = getLenderPendingAmount(oxyLendersPendingAmount);
			}
		} catch (Exception e) {
			logger.info("Exception in getLenderPendingAmount method");
		}
		return oxyLendersPendingAmountResponseDto;

	}

	@Override
	@Transactional
	public OxyStatusBasedPendingLendersResponse searchPendingAmountLendersByStatus(
			OxyStatusBasedPendingLendersRequest oxyStatusBasedPendingLendersRequest) {
		OxyStatusBasedPendingLendersResponse oxyStatusBasedPendingLendersResponse = new OxyStatusBasedPendingLendersResponse();
		long totalCount = 0;
		List<OxyLendersPendingAmountResponseDto> oxyLendersPendingAmountResponseDto = new ArrayList<OxyLendersPendingAmountResponseDto>();
		try {
			Pageable pageable = PageRequest.of(oxyStatusBasedPendingLendersRequest.getPageNo(),
					oxyStatusBasedPendingLendersRequest.getPageSize());

			Page<OxyLendersPendingAmount> approvalPendingLenders = oxyLendersPendingAmountRepo
					.findByStatusOrderByCreatedDateDesc(OxyLendersPendingAmount.Status.CREATED, pageable);
			if (approvalPendingLenders != null && !approvalPendingLenders.isEmpty()) {
				approvalPendingLenders.forEach(lenderPendingAmount -> {
					OxyLendersPendingAmountResponseDto oxyLendersPendingAmount = getLenderPendingAmount(
							lenderPendingAmount);
					oxyLendersPendingAmountResponseDto.add(oxyLendersPendingAmount);

				});
				totalCount = approvalPendingLenders.getTotalElements();
			}
		} catch (Exception e) {
			logger.info("Exception in searchPendingAmountLendersByStatus method");
		}
		oxyStatusBasedPendingLendersResponse.setOxyLendersPendingAmountResponseDto(oxyLendersPendingAmountResponseDto);
		oxyStatusBasedPendingLendersResponse.setTotalCount((int) totalCount);
		return oxyStatusBasedPendingLendersResponse;

	}

	private OxyLendersPendingAmountResponseDto getLenderPendingAmount(OxyLendersPendingAmount oxyLendersPendingAmount) {
		OxyLendersPendingAmountResponseDto oxyLendersPendingAmountResponseDto = new OxyLendersPendingAmountResponseDto();
		try {
			oxyLendersPendingAmountResponseDto.setId(oxyLendersPendingAmount.getId());
			oxyLendersPendingAmountResponseDto.setUserId(oxyLendersPendingAmount.getUserId());
			oxyLendersPendingAmountResponseDto.setAmount(oxyLendersPendingAmount.getAmount());
			oxyLendersPendingAmountResponseDto.setDealId(oxyLendersPendingAmount.getDealId());
			oxyLendersPendingAmountResponseDto.setDealName(oxyLendersPendingAmount.getDealName());
			oxyLendersPendingAmountResponseDto.setReason(oxyLendersPendingAmount.getReason());
			oxyLendersPendingAmountResponseDto.setAmountType(oxyLendersPendingAmount.getAmountType().toString());
			oxyLendersPendingAmountResponseDto.setTransactionType(oxyLendersPendingAmount.getTransactionType());
			oxyLendersPendingAmountResponseDto.setNoOfDays(oxyLendersPendingAmount.getNumberOfDays());
			oxyLendersPendingAmountResponseDto.setStatus(oxyLendersPendingAmount.getStatus().toString());
			oxyLendersPendingAmountResponseDto
					.setCreatedDate(expectedDateFormat.format(oxyLendersPendingAmount.getCreatedDate()));
		} catch (Exception e) {
			logger.info("Exception in getLenderPendingAmount method");
		}
		return oxyLendersPendingAmountResponseDto;

	}

	@Override
	@Transactional
	public OxyLendersPendingAmountResponseDto generateH2HFilePendingAmount(int id) {
		OxyLendersPendingAmountResponseDto oxyLendersPendingAmountResponseDto = new OxyLendersPendingAmountResponseDto();
		OxyLendersPendingAmount oxyLendersPendingAmount = oxyLendersPendingAmountRepo.findById(id).get();
		oxyLendersPendingAmountResponseDto.setH2hFile(creatingExcelSheetForh2h(oxyLendersPendingAmount));
		return oxyLendersPendingAmountResponseDto;

	}

	public String creatingExcelSheetForh2h(OxyLendersPendingAmount oxyLendersPendingAmount) {
		OxyBorrowersDealsInformation dealInfo = oxyBorrowersDealsInformationRepo
				.findById(oxyLendersPendingAmount.getDealId()).get();

		String dealName = null;
		if (dealInfo != null) {
			dealName = dealInfo.getDealName().replaceAll("[^a-zA-Z0-9]", "_");

		}

		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		Date today = new Date();
		String currentDateValue = dateFormatter.format(today);

		String currentDateSpliting[] = currentDateValue.split("/");

		SimpleDateFormat formatter1 = new SimpleDateFormat("ddMMyy");
		Date date = new Date();
		String currentDate = formatter1.format(date);

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		LocalDateTime now = LocalDateTime.now();
		StringBuilder sbf = null;
		String accountNumber = null;
		if (oxyLendersPendingAmount.getTransactionType().equals("REPAYMENT")) {
			sbf = new StringBuilder("OXYLR1_OXYLR1UPLD_"); // for interest
			accountNumber = "777705849442";
		} else {

			sbf = new StringBuilder("OXYLR2_OXYLR2UPLD_");
			accountNumber = "777705849441";

		}

		sbf.append(currentDate + "_" + oxyLendersPendingAmount.getUserId() + "_" + oxyLendersPendingAmount.getId());

		User user = userRepo.findById(oxyLendersPendingAmount.getUserId()).get();

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;
		if (oxyLendersPendingAmount != null) {

			Row row1 = spreadSheet.createRow(++rowCount);
			Cell cell1 = row1.createCell(0);
			cell1.setCellValue(accountNumber);

			cell1 = row1.createCell(1);
			cell1.setCellValue(user.getBankDetails().getAccountNumber());

			cell1 = row1.createCell(2);
			cell1.setCellValue(user.getBankDetails().getUserName());

			cell1 = row1.createCell(3);
			cell1.setCellValue(oxyLendersPendingAmount.getAmount());

			cell1 = row1.createCell(4);
			cell1.setCellValue("N");

			int y = Integer.parseInt(currentDateSpliting[1]);

			String monthName12 = DateTime.now().withMonthOfYear(y).toString("MMM");

			String requiredDate = currentDateSpliting[0] + "-" + monthName12 + "-" + currentDateSpliting[2];

			cell1 = row1.createCell(5);
			cell1.setCellValue(requiredDate);

			cell1 = row1.createCell(6);
			cell1.setCellValue(user.getBankDetails().getIfscCode().toUpperCase());

			cell1 = row1.createCell(15);
			cell1.setCellValue(oxyLendersPendingAmount.getNumberOfDays());

			cell1 = row1.createCell(16);
			cell1.setCellValue(oxyLendersPendingAmount.getAmountType().toString());

			cell1 = row1.createCell(17);
			cell1.setCellValue(oxyLendersPendingAmount.getDealId());

			cell1 = row1.createCell(18);
			cell1.setCellValue("LR" + oxyLendersPendingAmount.getUserId());

			cell1 = row1.createCell(19);
			cell1.setCellValue("OXYLOANS " + dealName + "M" + currentDateSpliting[0]);

			cell1 = row1.createCell(20);
			cell1.setCellValue(dealName);
		}

		File f = new File(iciciFilePathBeforeApproval + sbf.toString() + ".xls");
		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);

			workBook.close();
			outFile.close();

		} catch (Exception ex) {
			ex.getMessage();
		}

		Calendar current = Calendar.getInstance();
		TemplateContext templateContext = new TemplateContext();
		String expectedCurrentDate1 = expectedDateFormat.format(current.getTime());
		templateContext.put("currentDate", expectedCurrentDate1);
		String mailSubject = null;
		String returnType = null;
		if (oxyLendersPendingAmount.getAmountType().equals(OxyLendersPendingAmount.AmountType.LENDERINTEREST)) {
			mailSubject = "PendingAmountFor" + oxyLendersPendingAmount.getUserId() + " InterestSheet";
			returnType = "LENDERINTEREST";
		} else {
			if (oxyLendersPendingAmount.getAmountType().equals(OxyLendersPendingAmount.AmountType.LENDERPRINCIPAL)) {
				mailSubject = "PendingAmountFor" + oxyLendersPendingAmount.getUserId() + " PrincipalSheet";
				returnType = "LENDERPRINCIPAL";
			} else {
				mailSubject = "PendingAmountFor" + oxyLendersPendingAmount.getUserId() + " ReferralSheet";
				returnType = "REFERRALBONUS";
			}
		}

		LenderCmsPayments lenderCmsPayments = new LenderCmsPayments();
		lenderCmsPayments.setDealId(oxyLendersPendingAmount.getDealId());
		lenderCmsPayments.setTotalAmount(oxyLendersPendingAmount.getAmount());
		lenderCmsPayments.setFileName(f.getName());
		lenderCmsPayments.setLenderReturnsType(returnType);
		lenderCmsPayments.setPaymentDate(new Date());
		lenderCmsPaymentsRepo.save(lenderCmsPayments);

		oxyLendersPendingAmount.setFileName(f.getName());
		oxyLendersPendingAmount.setStatus(OxyLendersPendingAmount.Status.INITIATED);
		oxyLendersPendingAmountRepo.save(oxyLendersPendingAmount);

		EmailRequest emailRequest = new EmailRequest(new String[] { "archana.n@oxyloans.com" },
				"Interests-Approval.template", templateContext, mailsubject1, new String[] { f.getAbsolutePath() });

		EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
		if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
			try {
				throw new MessagingException(emailResponseFromService.getErrorMessage());
			} catch (MessagingException e2) {
				e2.printStackTrace();
			}
		}

		return f.getName();
	}

}
