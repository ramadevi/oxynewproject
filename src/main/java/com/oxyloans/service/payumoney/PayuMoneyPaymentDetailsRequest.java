package com.oxyloans.service.payumoney;

public class PayuMoneyPaymentDetailsRequest {

	public static enum PaymentTypes {
		EMIPAYMENT, BORROWERFEE
	}

	private Double borrowerFee;
	private String amount;
	private String firstname;
	private String email;
	private String phone;
	private String productinfo;
	private String udf5;
	private String surl;
	private String furl;
	private Integer emiNumber;
	private Integer loanId;
	private PaymentTypes paymentTypes;
	private String paymentKey;
	private String salt;
	private Integer id;
	private String partPayAmount;

	private String payuTransactionNumber;
	private String payuStatus;
	private int dealId;
	private int userId;

	public String getPartPayAmount() {
		return partPayAmount;
	}

	public void setPartPayAmount(String partPayAmount) {
		this.partPayAmount = partPayAmount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPaymentKey() {
		return paymentKey;
	}

	public String getSalt() {
		return salt;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public Double getBorrowerFee() {
		return borrowerFee;
	}

	public void setBorrowerFee(Double borrowerFee) {
		this.borrowerFee = borrowerFee;
	}

	public PaymentTypes getPaymentTypes() {
		return paymentTypes;
	}

	public void setPaymentTypes(PaymentTypes paymentTypes) {
		this.paymentTypes = paymentTypes;
	}

	public Integer getLoanId() {
		return loanId;
	}

	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}

	public Integer getEmiNumber() {
		return emiNumber;
	}

	public void setEmiNumber(Integer emiNumber) {
		this.emiNumber = emiNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getProductinfo() {
		return productinfo;
	}

	public void setProductinfo(String productinfo) {
		this.productinfo = productinfo;
	}

	public String getUdf5() {
		return udf5;
	}

	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}

	public String getSurl() {
		return surl;
	}

	public void setSurl(String surl) {
		this.surl = surl;
	}

	public String getFurl() {
		return furl;
	}

	public void setFurl(String furl) {
		this.furl = furl;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	private String mode;

	public String getPayuTransactionNumber() {
		return payuTransactionNumber;
	}

	public void setPayuTransactionNumber(String payuTransactionNumber) {
		this.payuTransactionNumber = payuTransactionNumber;
	}

	public String getPayuStatus() {
		return payuStatus;
	}

	public void setPayuStatus(String payuStatus) {
		this.payuStatus = payuStatus;
	}

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}