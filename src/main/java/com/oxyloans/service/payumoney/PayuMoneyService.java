package com.oxyloans.service.payumoney;

public interface PayuMoneyService {

	PayuMoneyPaymentDetaisResponce fetchPayUMoneyPaymentDetails(int userId,
			PayuMoneyPaymentDetailsRequest payuMoneyPaymentDetailsRequest, String paymentType);

	PayuMoneyPaymentDetaisResponce fetchLenderPauyUPaymentDetails(
			PayuMoneyPaymentDetailsRequest payuMoneyPaymentDetailsRequest);

}
