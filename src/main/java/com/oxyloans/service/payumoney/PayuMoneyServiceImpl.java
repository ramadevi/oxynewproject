package com.oxyloans.service.payumoney;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.oxyloans.commonutil.DateUtil;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation.ParticipationLenderType;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDeals;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDealsRepo;
import com.oxyloans.entity.lender.payu.LenderPayuDetails;
import com.oxyloans.entity.lender.payu.LenderPayuDetailsRepo;
import com.oxyloans.entity.loan.LoanEmiCard;
import com.oxyloans.entity.loan.LoanEmiCardPaymentDetails;
import com.oxyloans.entity.loan.LoanOfferdAmount;
import com.oxyloans.entity.loan.LoanRequest;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.service.payumoney.PayuMoneyPaymentDetailsRequest.PaymentTypes;
import com.oxyloans.repo.loan.LoanEmiCardPaymentDetailsRepo;
import com.oxyloans.repo.loan.LoanEmiCardRepo;
import com.oxyloans.repo.loan.OxyLoanRepo;
import com.oxyloans.repo.loan.OxyLoanRequestRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.security.authorization.IAuthorizationService;
import com.oxyloans.service.OperationNotAllowedException;

@Service
public class PayuMoneyServiceImpl implements PayuMoneyService {

	private final Logger logger = LogManager.getLogger(getClass());

	@Autowired
	protected DateUtil dateUtil;

	@Autowired
	private OxyLoanRequestRepo loanRequestRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private OxyLendersAcceptedDealsRepo oxyLendersAcceptedDealsRepo;

	@Autowired
	private OxyLoanRepo oxyLOanRepo;

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Autowired
	private IAuthorizationService authorizationService;

	@Autowired
	private LoanEmiCardRepo loanEmiCardRepo;

	@Value("${payusuccessreturnUrl}")
	private String payuSuccessReturnUrl;

	@Value("${payufailurereturnUrl}")
	private String payuFailureReturnUrl;

	@Value("borrowerFeeKey")
	private String borrowerFeeKey;

	@Value("borrowerFeeSalt")
	private String borrowerFeeSalt;

	@Value("${borrowerFeeSuccess}")
	private String borrowerFeeSuccess;

	@Value("${borrowerFeefailures}")
	private String borrowerFeefailures;

	@Autowired
	private LoanEmiCardPaymentDetailsRepo loanEmiCardPaymentDetailsRepo;

	@Autowired
	protected LenderPayuDetailsRepo lenderPayuDetailsRepo;

	@Value("${lenderFeeSuccess}")
	private String lenderFeeSuccessUrl;

	@Value("${lenderFeeFailure}")
	private String lenderFeeFailureUrl;

	@Override
	public PayuMoneyPaymentDetaisResponce fetchPayUMoneyPaymentDetails(int userId,
			PayuMoneyPaymentDetailsRequest payuMoneyPaymentDetailsRequest, String paymentType) {
		logger.info("loan id !!!!!" + payuMoneyPaymentDetailsRequest.getLoanId());
		logger.info("EMI Number !!!!!" + payuMoneyPaymentDetailsRequest.getEmiNumber());
		String txnId = "PUT" + dateUtil.getHHmmMMddyyyyDateFormat() + userId;// Need to change the format
		User user = userRepo.findById(userId).get();

		authorizationService.hasPermission(user);
		if (user.getPrimaryType() != PrimaryType.BORROWER) {
			throw new OperationNotAllowedException(
					"As a " + user.getPrimaryType() + " primary type not allowed to perform this",
					ErrorCodes.INVALID_OPERATION);
		}
		// OxyLoan OxyLoan = oxyLOanRepo.findByLoanId("LN" +
		// payuMoneyPaymentDetailsRequest.getLoanId());

		PayuMoneyPaymentDetailsRequest paymentDetaisRequest = new PayuMoneyPaymentDetailsRequest();
		PayuMoneyPaymentDetaisResponce paymentDetailsResponce = new PayuMoneyPaymentDetaisResponce();
		if (paymentType.equalsIgnoreCase(PaymentTypes.BORROWERFEE.toString())) {
			LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
			LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();
			paymentDetaisRequest.setAmount(Double.toString(loanOfferdAmount.getBorrowerFee()));
			paymentDetailsResponce.setKey("F7SJt1k8");
			paymentDetailsResponce.setSalt("531MJWsCEl");
			paymentDetailsResponce.setPaymentTypes(PaymentTypes.BORROWERFEE.toString());
			paymentDetaisRequest.setPaymentTypes(PaymentTypes.BORROWERFEE);
			paymentDetailsResponce.setAmount(Double.toString(loanOfferdAmount.getBorrowerFee()));
			loanOfferdAmount.setTxnNumber(txnId);
			loanRequest.setTxnNumber(txnId);
			loanOfferdAmount.setLoanRequest(loanRequest);
			loanRequest.setLoanOfferedAmount(loanOfferdAmount);
			loanRequestRepo.save(loanRequest);
			paymentDetailsResponce.setSurl(borrowerFeeSuccess);// create one page for surl
			paymentDetailsResponce.setFurl(borrowerFeefailures);// create one page for furl
		} else {
			LoanEmiCard loanEmiCard = loanEmiCardRepo.findByLoanIdAndEmiNumber(
					payuMoneyPaymentDetailsRequest.getLoanId(), payuMoneyPaymentDetailsRequest.getEmiNumber());

			List<LoanEmiCardPaymentDetails> loanEmiCardPaymentHistroy = loanEmiCardPaymentDetailsRepo
					.findByEmiId(loanEmiCard.getId());

			loanEmiCard.setTransactionNumber(txnId);
			loanEmiCardRepo.save(loanEmiCard);
			paymentDetaisRequest.setAmount(Double.toString(loanEmiCard.getEmiAmount()));
			paymentDetailsResponce.setKey("RXlAPVYY");
			paymentDetailsResponce.setSalt("WTLDUrS4G0");
			paymentDetailsResponce.setPaymentTypes(PaymentTypes.EMIPAYMENT.toString());
			paymentDetailsResponce.setPaymentTypes(PaymentTypes.EMIPAYMENT.toString());
			paymentDetaisRequest.setPaymentTypes(PaymentTypes.EMIPAYMENT);
			if (payuMoneyPaymentDetailsRequest.getPartPayAmount() != null
					&& !payuMoneyPaymentDetailsRequest.getPartPayAmount().isEmpty()) {
				paymentDetailsResponce.setAmount(payuMoneyPaymentDetailsRequest.getPartPayAmount());
				paymentDetaisRequest.setAmount(payuMoneyPaymentDetailsRequest.getPartPayAmount());
			} else {
				if (loanEmiCardPaymentHistroy.size() > 0) {
					LoanEmiCardPaymentDetails loanEmiCardPaymentDetails = loanEmiCardPaymentHistroy
							.get(loanEmiCardPaymentHistroy.size() - 1);
					paymentDetaisRequest.setAmount(Double.toString(loanEmiCardPaymentDetails.getRemainingEmiAmount()));
					paymentDetailsResponce.setAmount("" + loanEmiCardPaymentDetails.getRemainingEmiAmount());
				} else {
					paymentDetaisRequest.setAmount(Double.toString(loanEmiCard.getEmiAmount()));
					paymentDetailsResponce.setAmount(paymentDetaisRequest.getAmount());
				}
			}
			paymentDetailsResponce.setSurl(payuSuccessReturnUrl);// create one page for surl
			paymentDetailsResponce.setFurl(payuSuccessReturnUrl);// create one page for furl

		}

		// paymentDetaisRequest.setAmount(Double.toString(1));//please add loan emi
		// amount
		paymentDetaisRequest.setProductinfo("LENDING LOAN");
		paymentDetaisRequest.setUdf5("OXYLOANS");
		paymentDetailsResponce.setTxnid(txnId);
		String hash = genearteToken(txnId, paymentDetaisRequest, user);
		paymentDetailsResponce.setHash(hash);
		paymentDetailsResponce.setFirstname(user.getPersonalDetails().getFirstName());
		paymentDetailsResponce.setEmail(user.getEmail());
		paymentDetailsResponce.setPhone(user.getMobileNumber());
		paymentDetailsResponce.setProductinfo("LENDING LOAN");
		paymentDetailsResponce.setUdf5("OXYLOANS");
		paymentDetailsResponce.setId(payuMoneyPaymentDetailsRequest.getId());

		paymentDetailsResponce.setMode("dropout");
		return paymentDetailsResponce;

	}

	private String genearteToken(String txnId, PayuMoneyPaymentDetailsRequest paymentDetaisRequest, User user) {

		// String hashSequence = "MERCHANT_KEY | txnid | amount | pinfo | fname | email
		// ||||| BOLT_KIT_PHP7 |||||| SALT"

		StringBuilder sb = new StringBuilder();

		if (paymentDetaisRequest.getPaymentTypes() == PaymentTypes.BORROWERFEE) {
			sb.append("F7SJt1k8").append("|");
		}

		else {
			sb.append("RXlAPVYY").append("|");
		}
		sb.append(txnId).append("|");
		sb.append(paymentDetaisRequest.getAmount()).append("|");
		sb.append(paymentDetaisRequest.getProductinfo()).append("|"); // consumerData.accountNumber
		sb.append(user.getPersonalDetails().getFirstName()).append("|"); // consumerData.consumerId
		sb.append(user.getEmail()).append("|");// consumerData.consumerMobileNo
		sb.append("").append("|"); // consumerData.cardNumber
		sb.append("").append("|"); // consumerData.cardNumber
		sb.append("").append("|"); // consumerData.cardNumber
		sb.append("").append("|"); // consumerData.cardNumber
		sb.append(paymentDetaisRequest.getUdf5()).append("|"); // consumerData.cardNumber
		sb.append("").append("|"); // consumerData.consumerEmailId
		sb.append("").append("|"); // consumerData.cardNumber
		sb.append("").append("|"); // consumerData. expMonth
		sb.append("").append("|"); // consumerData.expYear
		sb.append("").append("|"); // consumerData.cvvCode
		if (paymentDetaisRequest.getPaymentTypes() == PaymentTypes.BORROWERFEE) {
			sb.append("531MJWsCEl");
		} else {
			sb.append("WTLDUrS4G0"); // SALT
		}
		// DigestUtils md = null;
		System.out.println("token: " + sb.toString());
		String token = "";
		byte[] hashseq = sb.toString().getBytes();
		StringBuffer hexString = new StringBuffer();
		try {
			// Create MessageDigest instance for MD5
			MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
			algorithm.reset();
			algorithm.update(hashseq);
			byte messageDigest[] = algorithm.digest();
			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1) {
					hexString.append("0");
				}
				hexString.append(hex);
			}
		} catch (NoSuchAlgorithmException e) {
			System.out.println(e.getMessage());
		}
		return hexString.toString();// md.digestAsHex(sb.toString().getBytes());
	}

	@Override
	public PayuMoneyPaymentDetaisResponce fetchLenderPauyUPaymentDetails(
			PayuMoneyPaymentDetailsRequest payuMoneyPaymentDetailsRequest) {

		int userId = payuMoneyPaymentDetailsRequest.getUserId();

		String txnId = "PUT" + dateUtil.getHHmmMMddyyyyDateFormat() + userId;// Need to change the format

		Calendar currentDate = Calendar.getInstance();
		User user = userRepo.findById(userId).get();

		authorizationService.hasPermission(user);

		if (user.getPrimaryType() != PrimaryType.LENDER) {
			throw new OperationNotAllowedException(
					"As a " + user.getPrimaryType() + " primary type not allowed to perform this",
					ErrorCodes.INVALID_OPERATION);
		}

		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
				.findDealAlreadyGiven(payuMoneyPaymentDetailsRequest.getDealId());

		// only new lender can participate verification check starts
		if (oxyBorrowersDealsInformation != null) {

			if (oxyBorrowersDealsInformation.getParticipationLenderType().equals(ParticipationLenderType.NEW)) {

				List<OxyLendersAcceptedDeals> oxylendersAcceptedDealsList = oxyLendersAcceptedDealsRepo
						.findbyUserId(userId);

				if (oxylendersAcceptedDealsList != null && !oxylendersAcceptedDealsList.isEmpty()) {

					OxyLendersAcceptedDeals oxylendersAcceptedDeals = oxyLendersAcceptedDealsRepo
							.toCheckUserAlreadyInvoledInDeal(userId, payuMoneyPaymentDetailsRequest.getDealId());

					if (oxylendersAcceptedDeals == null) {
						throw new OperationNotAllowedException(
								"Newly registered users or Existing users who haven't participated yet in any deal "
										+ "can only participate in this deal.",
								ErrorCodes.PERMISSION_DENIED);
					}
				}

			}
		}
		// only new lender can participate verification check ends

		LenderPayuDetails lenderPayuDetails = lenderPayuDetailsRepo.findByLenderUserIdAndDealIdandFeestatus(userId,
				payuMoneyPaymentDetailsRequest.getDealId());

		if (lenderPayuDetails == null) {
			lenderPayuDetails = new LenderPayuDetails();
		}
		PayuMoneyPaymentDetailsRequest paymentDetaisRequest = new PayuMoneyPaymentDetailsRequest();
		PayuMoneyPaymentDetaisResponce paymentDetailsResponce = new PayuMoneyPaymentDetaisResponce();

		Double amount = Double.parseDouble(payuMoneyPaymentDetailsRequest.getAmount());
		paymentDetaisRequest.setProductinfo("LENDING LOAN");
		paymentDetaisRequest.setUdf5("OXYLOANS");
		paymentDetaisRequest.setAmount(Double.toString(amount));

		String hash = genearteTokenForLenderFee(txnId, paymentDetaisRequest, user);

		// response

		paymentDetailsResponce.setSalt("531MJWsCEl");
		paymentDetailsResponce.setKey("F7SJt1k8");

		paymentDetailsResponce.setTxnid(txnId);
		paymentDetailsResponce.setAmount(Double.toString(amount));
		paymentDetailsResponce.setFirstname(user.getPersonalDetails().getFirstName());
		paymentDetailsResponce.setPhone(user.getMobileNumber());
		paymentDetailsResponce.setEmail(user.getEmail());
		paymentDetailsResponce.setHash(hash);
		paymentDetailsResponce.setUdf5("OXYLOANS");
		paymentDetailsResponce.setProductinfo("LENDING LOAN");
		paymentDetailsResponce.setSurl(lenderFeeSuccessUrl);
		paymentDetailsResponce.setFurl(lenderFeeFailureUrl);
		paymentDetailsResponce.setMode("dropout");
		paymentDetailsResponce.setPaymentTypes("LENDERFEE");

		// storing in db

		lenderPayuDetails.setLenderUserId(user.getId());
		lenderPayuDetails.setLenderName(user.getPersonalDetails().getFirstName());
		lenderPayuDetails.setTransactionNumber(txnId);
		lenderPayuDetails.setAmount(Double.parseDouble(payuMoneyPaymentDetailsRequest.getAmount()));
		lenderPayuDetails.setDealId(payuMoneyPaymentDetailsRequest.getDealId());

		lenderPayuDetails.setPaymentDate(currentDate.getTime());

		lenderPayuDetailsRepo.save(lenderPayuDetails);
		return paymentDetailsResponce;
	}

	private String genearteTokenForLenderFee(String txnId, PayuMoneyPaymentDetailsRequest paymentDetaisRequest,
			User user) {
		// String hashSequence = "MERCHANT_KEY | txnid | amount | pinfo | fname | email
		// ||||| BOLT_KIT_PHP7 |||||| SALT"

		StringBuilder sb = new StringBuilder();

		sb.append("F7SJt1k8").append("|"); // KEY
		// sb.append("RXlAPVYY").append("|");
		sb.append(txnId).append("|");
		sb.append(paymentDetaisRequest.getAmount()).append("|");
		sb.append(paymentDetaisRequest.getProductinfo()).append("|"); // consumerData.accountNumber
		sb.append(user.getPersonalDetails().getFirstName()).append("|"); // consumerData.consumerId
		sb.append(user.getEmail()).append("|");// consumerData.consumerMobileNo
		sb.append("").append("|"); // consumerData.cardNumber
		sb.append("").append("|"); // consumerData.cardNumber
		sb.append("").append("|"); // consumerData.cardNumber
		sb.append("").append("|"); // consumerData.cardNumber
		sb.append(paymentDetaisRequest.getUdf5()).append("|"); // consumerData.cardNumber

		sb.append("").append("|"); // consumerData.consumerEmailId
		sb.append("").append("|"); // consumerData.cardNumber
		sb.append("").append("|"); // consumerData. expMonth
		sb.append("").append("|"); // consumerData.expYear
		sb.append("").append("|"); // consumerData.cvvCode

		sb.append("531MJWsCEl"); // SALT
		// sb.append("WTLDUrS4G0");
		// DigestUtils md = null;
		System.out.println("token: " + sb.toString());
		logger.info("token:" + sb.toString());
		String token = "";
		byte[] hashseq = sb.toString().getBytes();
		StringBuffer hexString = new StringBuffer();
		try {
			// Create MessageDigest instance for MD5
			MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
			algorithm.reset();
			algorithm.update(hashseq);
			byte messageDigest[] = algorithm.digest();
			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1) {
					hexString.append("0");
				}
				hexString.append(hex);
			}
		} catch (NoSuchAlgorithmException e) {
			System.out.println(e.getMessage());
		}
		return hexString.toString();// md.digestAsHex(sb.toString().getBytes());
	}

}
