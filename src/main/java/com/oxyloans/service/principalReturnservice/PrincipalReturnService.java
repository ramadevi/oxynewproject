package com.oxyloans.service.principalReturnservice;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entity.borrowers.deals.information.LendersPaticipationUpdationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDeals;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDealsRepo;
import com.oxyloans.entity.cms.LenderCmsPayments;
import com.oxyloans.entity.cms.LenderCmsPaymentsDetails;
import com.oxyloans.repository.cms.LenderCmsPaymentsDetailsRepo;
import com.oxyloans.repository.cms.LenderCmsPaymentsRepo;
import com.oxyloans.entity.excelsheets.OxyTransactionDetailsFromExcelSheets;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWallet;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletNativeRepo;
import com.oxyloans.entity.lender.returns.LendersReturns;
import com.oxyloans.entity.lender.returns.LendersReturnsRepo;
import com.oxyloans.entity.lenders.hold.UserHoldAmountMappedToDeal.AmountType;
import com.oxyloans.entity.user.User;
import com.oxyloans.lender.holdamount.service.HoldAmountService;
import com.oxyloans.lender.principal.OxyPrincipalReturn;
import com.oxyloans.lender.principal.OxyPrincipalReturnRepo;
import com.oxyloans.lender.withdrawalfunds.dto.LenderPrincipal;
import com.oxyloans.lender.withdrawalfunds.dto.LenderPrincipalRequestDto;
import com.oxyloans.lender.withdrawalfunds.dto.LenderPrincipalResponseDto;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsFromDealsResponseDto;
import com.oxyloans.lender.withdrawalfunds.dto.ListOfLendersWithdrawalFundsInfo;
import com.oxyloans.lender.withdrawalfunds.service.LenderWithdrawalFundsServiceImpl;
import com.oxyloans.principal.LenderPayementDetails;
import com.oxyloans.principal.LenderPaymentsResponse;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.service.loan.ActionNotAllowedException;
import com.oxyloans.whatappservice.WhatappService;

@Service
public class PrincipalReturnService implements PrincipalReturnServiceRepo {

	private final Logger logger = LogManager.getLogger(PrincipalReturnService.class);

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Autowired
	private OxyPrincipalReturnRepo oxyPrincipalReturnRepo;

	@Autowired
	private OxyLendersAcceptedDealsRepo oxyLendersAcceptedDealsRepo;

	@Autowired
	private LendersPaticipationUpdationRepo lendersPaticipationUpdationRepo;

	@Autowired
	private WhatappService whatappService;

	@Autowired
	private LendersReturnsRepo lendersReturnsRepo;

	@Autowired
	private UserRepo userRepo;

	protected final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	protected final DateFormat expectedDateFormatAsDb = new SimpleDateFormat("yyyy-MM-dd");

	@Value("${iciciFilePathBeforeApproval}")
	private String iciciFilePathBeforeApproval;

	@Autowired
	private IEmailService emailService;

	@Autowired
	private LenderWithdrawalFundsServiceImpl lenderWithdrawalFundsServiceImpl;

	@Autowired
	private LenderOxyWalletNativeRepo lenderOxyWalletNativeRepo;

	@Autowired
	private LenderCmsPaymentsRepo lenderCmsPaymentsRepo;

	@Autowired
	private HoldAmountService holdAmountService;

	@Autowired
	private LenderCmsPaymentsDetailsRepo lenderCmsPaymentsDetailsRepo;

	@Override
	@Transactional
	public LenderPrincipalResponseDto pricipalReturnToLender(Integer dealId, LenderPrincipal lenderPrincipal) {
		List<LenderPrincipalRequestDto> listOfPricipalReturningInfo = lenderPrincipal.getLenderPrincipalRequestDto();
		if (listOfPricipalReturningInfo != null && !listOfPricipalReturningInfo.isEmpty()) {
			for (LenderPrincipalRequestDto lenderPrincipalRequest : listOfPricipalReturningInfo) {

				if (lenderPrincipalRequest.getPricipalReturnAmount() <= lenderPrincipalRequest.getCurrentAmount()) {
					double principalAmount = 0.0;

					if (lenderPrincipalRequest.getPricipalReturnAmount() <= 0.0) {
						principalAmount = lenderPrincipalRequest.getCurrentAmount();
					} else {
						principalAmount = lenderPrincipalRequest.getPricipalReturnAmount();
					}

					logger.info("current amount p2b" + lenderPrincipalRequest.getCurrentAmount());
					logger.info("principalAmount p2b" + principalAmount);

					Double lenderWithDrawAmount = lendersReturnsRepo
							.findingSumOfWithdrawApproved(lenderPrincipalRequest.getUserId(), dealId);
					if (lenderWithDrawAmount != null) {
						logger.info("previous lenderWithDrawAmount p2b" + lenderWithDrawAmount);
						if ((principalAmount + lenderWithDrawAmount) > lenderPrincipalRequest.getCurrentAmount()) {
							throw new ActionNotAllowedException(
									"Please check your are entering more than the participation amount to this lender id LR"
											+ lenderPrincipalRequest.getUserId(),
									ErrorCodes.EMPTY_REQUEST);
						}
					}

					Double principalReturnedToAccount = oxyPrincipalReturnRepo
							.principalAmountReturn(lenderPrincipalRequest.getUserId(), dealId);

					LenderOxyWallet lenderwallet = lenderOxyWalletNativeRepo
							.principalReturnedToWallet(lenderPrincipalRequest.getUserId(), dealId);

					if (principalReturnedToAccount != null) {
						logger.info("previous principalReturned p2b" + principalReturnedToAccount);
						if ((principalAmount + principalReturnedToAccount) > lenderPrincipalRequest
								.getCurrentAmount()) {
							throw new ActionNotAllowedException(
									"Please check your are entering more than the participation amount to this lender id LR"
											+ lenderPrincipalRequest.getUserId(),
									ErrorCodes.EMPTY_REQUEST);
						}
					}
					if (lenderwallet != null) {
						logger.info("previous principalReturned p2w" + lenderwallet.getTransactionAmount());
						if ((lenderwallet.getTransactionAmount() + principalAmount) > lenderPrincipalRequest
								.getCurrentAmount()) {
							throw new ActionNotAllowedException(
									"Please check your are entering more than the participation amount to this lender id LR"
											+ lenderPrincipalRequest.getUserId(),
									ErrorCodes.EMPTY_REQUEST);
						}
					}
					if (principalReturnedToAccount != null && lenderwallet != null) {
						logger.info("previous principalReturned p2w and p2b"
								+ (principalAmount + principalReturnedToAccount + lenderwallet.getTransactionAmount()));
						if ((principalAmount + principalReturnedToAccount
								+ lenderwallet.getTransactionAmount()) > lenderPrincipalRequest.getCurrentAmount()) {
							throw new ActionNotAllowedException(
									"Please check your are entering more than the participation amount to this lender id LR"
											+ lenderPrincipalRequest.getUserId(),
									ErrorCodes.EMPTY_REQUEST);
						}
					}

					OxyPrincipalReturn alreadyInserted = oxyPrincipalReturnRepo.checkPricipalAleadyGivenStatus(
							lenderPrincipalRequest.getUserId(), dealId, principalAmount);
					if (alreadyInserted == null) {
						LenderOxyWallet lenderOxyWallet = lenderOxyWalletNativeRepo
								.principalReturnedToWallet(lenderPrincipalRequest.getUserId(), dealId);
						if (lenderOxyWallet == null) {
							OxyPrincipalReturn oxyPrincipalReturn = new OxyPrincipalReturn();
							oxyPrincipalReturn.setUserId(lenderPrincipalRequest.getUserId());
							oxyPrincipalReturn.setDealId(dealId);
							oxyPrincipalReturn.setPrincipalAmount(principalAmount);
							LenderPrincipalResponseDto lenderPrincipalResponseDto = generateInterestAmount(
									lenderPrincipalRequest.getUserId(), dealId, principalAmount);
							oxyPrincipalReturn.setInterestAmount(lenderPrincipalResponseDto.getInterestAmount());
							oxyPrincipalReturn.setDifferenceInDays(lenderPrincipalResponseDto.getDifferenceInDays());
							oxyPrincipalReturn.setPrincipalApprovedOn(new Date());
							oxyPrincipalReturnRepo.save(oxyPrincipalReturn);
						} else {
							throw new ActionNotAllowedException(
									"your had aleady initiated principal amount to wallet please check"
											+ lenderPrincipalRequest.getUserId(),
									ErrorCodes.ACTION_ALREDY_DONE);

						}
					} else {
						throw new ActionNotAllowedException("your had aleady initiated principal amount to this lender "
								+ lenderPrincipalRequest.getUserId(), ErrorCodes.ACTION_ALREDY_DONE);

					}

				}

			}
		}
		LenderPrincipalResponseDto lenderPrincipalResponseDto = new LenderPrincipalResponseDto();
		lenderPrincipalResponseDto.setStatus("Success");
		return lenderPrincipalResponseDto;

	}

	public LenderPrincipalResponseDto generateInterestAmount(int userId, int dealId, double amount) {
		LenderPrincipalResponseDto lenderPrincipalResponseDto = new LenderPrincipalResponseDto();
		double interestAmountGenerated = 0.0;

		Date date = new Date();

		LendersReturns latestInterestDetails = lendersReturnsRepo.getLenderLatestInterestReturnedDate(userId, dealId);
		String interestDate = null;
		int substartionValue = 0;
		int additionValue = 0;
		OxyLendersAcceptedDeals lendersAcceptedDeals = oxyLendersAcceptedDealsRepo
				.toCheckUserAlreadyInvoledInDeal(userId, dealId);
		if (latestInterestDetails != null) {
			if (!latestInterestDetails.getAmountType()
					.equals(OxyTransactionDetailsFromExcelSheets.DebitedTowords.LENDERINTEREST.toString())) {
				interestDate = expectedDateFormat.format(latestInterestDetails.getPaidDate());
			} else {
				String paidDate = null;
				if (latestInterestDetails.getActualDate() != null) {
					paidDate = expectedDateFormat.format(latestInterestDetails.getActualDate());
				} else {
					paidDate = expectedDateFormat.format(latestInterestDetails.getPaidDate());
				}

				String[] paidDateSplited = paidDate.split("/");
				OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
						.findDealAlreadyGiven(dealId);
				int loanActivedate = 0;
				if (oxyBorrowersDealsInformation != null) {
					String loanActiveDate = expectedDateFormat.format(oxyBorrowersDealsInformation.getLoanActiveDate());
					String[] loanActiveSpliting = loanActiveDate.split("/");
					loanActivedate = Integer.parseInt(loanActiveSpliting[0]);
				}
				StringBuilder interestGivenDate = new StringBuilder();
				interestGivenDate.append(loanActivedate).append("/").append(paidDateSplited[1].toString()).append("/")
						.append(paidDateSplited[2].toString());
				interestDate = interestGivenDate.toString();
			}
			additionValue = 1;
		} else {
			if (lendersAcceptedDeals != null) {
				interestDate = expectedDateFormat.format(lendersAcceptedDeals.getReceivedOn());
			}
			substartionValue = 1;
		}
		String principalDateInExceptedFormate = expectedDateFormat.format(date);
		String startDate = null;
		String endDate = null;
		int days = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");
		try {
			startDate = sdf3.format(sdf.parse(interestDate));
			endDate = sdf3.format(sdf.parse(principalDateInExceptedFormate));

		} catch (java.text.ParseException e) {

			e.printStackTrace();
		}
		days = lenderWithdrawalFundsServiceImpl.differenceInDays(startDate, endDate) - substartionValue + additionValue;

		double interestAmount = 0.0;
		double singleDayInterest = 0.0;
		double rateOfInterest = 0.0;
		OxyBorrowersDealsInformation oxyBorrowersDeals = oxyBorrowersDealsInformationRepo.toGetCalculationType(dealId);
		if (oxyBorrowersDeals != null) {
			rateOfInterest = lendersAcceptedDeals.getRateofinterest();
		} else {
			rateOfInterest = lendersAcceptedDeals.getRateofinterest() / 12;
		}
		if (lendersAcceptedDeals != null) {
			interestAmount = Math.round(amount * rateOfInterest / 100d);
			singleDayInterest = interestAmount / 30;
		}
		interestAmountGenerated = Math.round(days * singleDayInterest);
		lenderPrincipalResponseDto.setInterestAmount(interestAmountGenerated);
		lenderPrincipalResponseDto.setDifferenceInDays(days);
		return lenderPrincipalResponseDto;

	}

	@Override
	public LenderPrincipal getPrincipalInitiated(int dealId, String status) {
		List<OxyPrincipalReturn> pricipalInitiated = oxyPrincipalReturnRepo
				.listOfPrincipalReturningBasedOnDealId(dealId, status);
		LenderPrincipal lenderPrincipal = new LenderPrincipal();
		double amount = 0.0;
		List<LenderPrincipalRequestDto> listofLenders = new ArrayList<LenderPrincipalRequestDto>();
		if (pricipalInitiated != null && !pricipalInitiated.isEmpty()) {
			for (OxyPrincipalReturn principal : pricipalInitiated) {
				LenderPrincipalRequestDto oxyPrincipalReturn = new LenderPrincipalRequestDto();
				if (status.equals("PRINCIPALRETURNED")) {
					if (principal.getInterestAmount() <= 0.0) {
						throw new ActionNotAllowedException(
								"Please check interest amount is negative to LR" + principal.getUserId(),
								ErrorCodes.AMOUNT_LIMIT);
					}
					oxyPrincipalReturn.setDifferenceInDays(principal.getDifferenceInDays());
					oxyPrincipalReturn.setAmount(principal.getInterestAmount());
					amount = amount + principal.getInterestAmount();
				} else {
					oxyPrincipalReturn.setAmount(principal.getPrincipalAmount());
					amount = amount + principal.getPrincipalAmount();
				}
				oxyPrincipalReturn.setUserId(principal.getUserId());

				User user = userRepo.findById(principal.getUserId()).get();
				if (user != null) {
					if (user.getBankDetails() != null) {
						if (user.getBankDetails().getUserName() != null) {
							oxyPrincipalReturn
									.setLenderName(user.getBankDetails().getUserName().trim().replaceAll(" +", " "));

						}
						if (user.getBankDetails().getAccountNumber() != null) {
							oxyPrincipalReturn.setAccountNumber(user.getBankDetails().getAccountNumber());

						}
						if (user.getBankDetails().getIfscCode() != null) {
							oxyPrincipalReturn.setIfscCode(user.getBankDetails().getIfscCode().toUpperCase());

						}
					}
				}
				listofLenders.add(oxyPrincipalReturn);

			}
			// lenderPrincipal.setDate(oxyPrincipalReturnRepo.getDateBasedOnDealId(dealId,
			// status));
		}
		if (status.equals("PRINCIPALRETURNED")) {
			List<LenderOxyWallet> interestAmount = lenderOxyWalletNativeRepo
					.interestToWalletLoadedFromParticipation(dealId);
			if (interestAmount != null && !interestAmount.isEmpty()) {
				for (LenderOxyWallet interestDetails : interestAmount) {
					if (interestDetails.getInterestToPrincipalMoved() <= 0.0) {
						throw new ActionNotAllowedException(
								"Please check interest amount is negative to LR" + interestDetails.getUserId(),
								ErrorCodes.AMOUNT_LIMIT);
					}
					LenderPrincipalRequestDto oxyPrincipalReturn = new LenderPrincipalRequestDto();
					oxyPrincipalReturn.setDifferenceInDays(interestDetails.getDifferenceInDays());
					oxyPrincipalReturn.setAmount(interestDetails.getInterestToPrincipalMoved());
					amount = amount + interestDetails.getInterestToPrincipalMoved();
					User user = userRepo.findById(interestDetails.getUserId()).get();
					oxyPrincipalReturn.setUserId(interestDetails.getUserId());
					if (user != null) {
						if (user.getBankDetails() != null) {
							if (user.getBankDetails().getUserName() != null) {
								oxyPrincipalReturn.setLenderName(
										user.getBankDetails().getUserName().trim().replaceAll(" +", " "));

							}
							if (user.getBankDetails().getAccountNumber() != null) {
								oxyPrincipalReturn.setAccountNumber(user.getBankDetails().getAccountNumber());

							}
							if (user.getBankDetails().getIfscCode() != null) {
								oxyPrincipalReturn.setIfscCode(user.getBankDetails().getIfscCode().toUpperCase());

							}
						}
					}
					listofLenders.add(oxyPrincipalReturn);
				}
			}

		}
		if (status.equals("PRINCIPALRETURNED")) {
			List<OxyPrincipalReturn> listOfInterestPending = oxyPrincipalReturnRepo
					.interestMovingToH2HToPrincipalToWallet(dealId);
			if (listOfInterestPending != null && !listOfInterestPending.isEmpty()) {
				for (OxyPrincipalReturn listPrincipalReturn : listOfInterestPending) {
					LenderPrincipalRequestDto oxyPrincipal = new LenderPrincipalRequestDto();
					OxyPrincipalReturn oxyPrincipalReturn = oxyPrincipalReturnRepo.findById(listPrincipalReturn.getId())
							.get();
					if (oxyPrincipalReturn != null) {
						amount = amount + oxyPrincipalReturn.getInterestAmount();
						oxyPrincipal.setAmount(oxyPrincipalReturn.getInterestAmount());
						oxyPrincipal.setDifferenceInDays(oxyPrincipalReturn.getDifferenceInDays());
						User user = userRepo.findById(oxyPrincipalReturn.getUserId()).get();
						oxyPrincipal.setUserId(oxyPrincipalReturn.getUserId());

						if (user != null) {
							if (user.getBankDetails() != null) {
								if (user.getBankDetails().getUserName() != null) {
									oxyPrincipal.setLenderName(
											user.getBankDetails().getUserName().trim().replaceAll(" +", " "));

								}
								if (user.getBankDetails().getAccountNumber() != null) {
									oxyPrincipal.setAccountNumber(user.getBankDetails().getAccountNumber());

								}
								if (user.getBankDetails().getIfscCode() != null) {
									oxyPrincipal.setIfscCode(user.getBankDetails().getIfscCode().toUpperCase());

								}
							}
						}
					}
					listofLenders.add(oxyPrincipal);
				}
			}
		}
		lenderPrincipal.setAmount(amount);
		lenderPrincipal.setLenderPrincipalRequestDto(listofLenders);
		return lenderPrincipal;

	}

	@Override
	@Transactional
	public LenderPrincipalResponseDto sendingPricipalReturnToH2H(Integer dealId, LenderPrincipal lenderPrincipal) {
		LenderPrincipalResponseDto lenderPrincipalResponseDto = new LenderPrincipalResponseDto();
		String message = null;
		ListOfLendersWithdrawalFundsInfo listOfLendersWithdrawalFundsInfo = new ListOfLendersWithdrawalFundsInfo();
		List<LenderWithdrawalFundsFromDealsResponseDto> listOfLenders = new ArrayList<LenderWithdrawalFundsFromDealsResponseDto>();
		if (lenderPrincipal.getStatus() != null) {
			List<OxyPrincipalReturn> listOfPrincipal = oxyPrincipalReturnRepo.principalMovingToH2H(dealId,
					lenderPrincipal.getStatus());

			if (listOfPrincipal != null && !listOfPrincipal.isEmpty()) {
				for (OxyPrincipalReturn listPrincipalReturn : listOfPrincipal) {
					LenderWithdrawalFundsFromDealsResponseDto lenderPrincipalValue = new LenderWithdrawalFundsFromDealsResponseDto();
					OxyPrincipalReturn oxyPrincipalReturn = oxyPrincipalReturnRepo.findById(listPrincipalReturn.getId())
							.get();
					if (oxyPrincipalReturn != null) {
						lenderPrincipalValue.setUserId(oxyPrincipalReturn.getUserId());
						if (lenderPrincipal.getStatus().equals(OxyPrincipalReturn.Status.INITIATED.toString())) {
							lenderPrincipalValue.setAmount(oxyPrincipalReturn.getPrincipalAmount());
							lenderPrincipalValue.setDifferenceInDays(0);
							oxyPrincipalReturn.setPrincipalStatus(OxyPrincipalReturn.PrincipalStatus.BEFORE);
							oxyPrincipalReturn.setStatus(OxyPrincipalReturn.Status.PRINCIPALRETURNED);

						} else {
							lenderPrincipalValue.setDifferenceInDays(oxyPrincipalReturn.getDifferenceInDays());
							lenderPrincipalValue.setAmount(oxyPrincipalReturn.getInterestAmount());
							oxyPrincipalReturn.setStatus(OxyPrincipalReturn.Status.INTERESTRETURNED);
							oxyPrincipalReturn.setInterestStatus(OxyPrincipalReturn.InterestStatus.BEFORE);
							oxyPrincipalReturn.setInterestApprovedOn(new Date());
						}
						oxyPrincipalReturnRepo.save(oxyPrincipalReturn);
						User user = userRepo.findById(oxyPrincipalReturn.getUserId()).get();
						if (user != null) {
							if (user.getBankDetails() != null) {
								if (user.getBankDetails().getUserName() != null) {
									lenderPrincipalValue.setLenderName(
											user.getBankDetails().getUserName().trim().replaceAll(" +", " "));

								}
								if (user.getBankDetails().getAccountNumber() != null) {
									lenderPrincipalValue.setAccountNumber(user.getBankDetails().getAccountNumber());

								}
								if (user.getBankDetails().getIfscCode() != null) {
									lenderPrincipalValue.setIfsc(user.getBankDetails().getIfscCode().toUpperCase());

								}
							}
						}

					}
					if (lenderPrincipal.getStatus().equals(OxyPrincipalReturn.Status.INITIATED.toString())) {
						listOfLenders.add(lenderPrincipalValue);
					} else {
						String principalDate = null;
						if (oxyPrincipalReturn.getPrincipalApprovedOn() != null) {
							principalDate = expectedDateFormat.format(oxyPrincipalReturn.getPrincipalApprovedOn());
						} else {
							principalDate = expectedDateFormat.format(oxyPrincipalReturn.getPrincipalModifiedOn());
						}
						LenderCmsPayments lenderCmsPayments = lenderCmsPaymentsRepo.previousInterestGiven(dealId,
								principalDate);
						if (lenderCmsPayments != null) {
							LenderCmsPaymentsDetails lenderCmsPaymentsDetails = lenderCmsPaymentsDetailsRepo
									.findByLenderCmsPaymentsIdAndLenderId(lenderCmsPayments.getId(),
											oxyPrincipalReturn.getUserId());
							if (lenderCmsPaymentsDetails == null) {
								listOfLenders.add(lenderPrincipalValue);
							}

						} else {
							listOfLenders.add(lenderPrincipalValue);
						}
					}
				}

			}
			/*
			 * else { if (lenderPrincipal.getStatus().equals("INITIATED")) { throw new
			 * ActionNotAllowedException("your had already sent principal to H2H",
			 * ErrorCodes.ACTION_ALREDY_DONE); } else { throw new
			 * ActionNotAllowedException("your had already sent Interest to H2H",
			 * ErrorCodes.ACTION_ALREDY_DONE); } }
			 */
		}
		if (!lenderPrincipal.getStatus().equals(OxyPrincipalReturn.Status.INITIATED.toString())) {
			List<LenderOxyWallet> interestAmount = lenderOxyWalletNativeRepo
					.interestToWalletLoadedFromParticipation(dealId);
			if (interestAmount != null && !interestAmount.isEmpty()) {
				for (LenderOxyWallet interestDetails : interestAmount) {
					interestDetails.setPrincipalToWallet(LenderOxyWallet.PrincipalToWallet.INTERESTRETURNED);
					lenderOxyWalletNativeRepo.save(interestDetails);

					LenderWithdrawalFundsFromDealsResponseDto lenderInterestReturn = new LenderWithdrawalFundsFromDealsResponseDto();
					lenderInterestReturn.setAmount(interestDetails.getInterestToPrincipalMoved());
					lenderInterestReturn.setUserId(interestDetails.getUserId());
					lenderInterestReturn.setDifferenceInDays(interestDetails.getDifferenceInDays());
					User user = userRepo.findById(interestDetails.getUserId()).get();
					if (user != null) {
						if (user.getBankDetails() != null) {
							if (user.getBankDetails().getUserName() != null) {
								lenderInterestReturn.setLenderName(
										user.getBankDetails().getUserName().trim().replaceAll(" +", " "));

							}
							if (user.getBankDetails().getAccountNumber() != null) {
								lenderInterestReturn.setAccountNumber(user.getBankDetails().getAccountNumber());

							}
							if (user.getBankDetails().getIfscCode() != null) {
								lenderInterestReturn.setIfsc(user.getBankDetails().getIfscCode().toUpperCase());

							}
						}
					}
					String principalDate = expectedDateFormat.format(interestDetails.getTransactionDate());

					LenderCmsPayments lenderCmsPayments = lenderCmsPaymentsRepo.previousInterestGiven(dealId,
							principalDate);
					if (lenderCmsPayments != null) {
						LenderCmsPaymentsDetails lenderCmsPaymentsDetails = lenderCmsPaymentsDetailsRepo
								.findByLenderCmsPaymentsIdAndLenderId(lenderCmsPayments.getId(),
										interestDetails.getUserId());
						if (lenderCmsPaymentsDetails == null) {
							listOfLenders.add(lenderInterestReturn);
						}

					} else {
						listOfLenders.add(lenderInterestReturn);
					}

				}
			}
			List<OxyPrincipalReturn> listOfInterestPending = oxyPrincipalReturnRepo
					.interestMovingToH2HToPrincipalToWallet(dealId);
			if (listOfInterestPending != null && !listOfInterestPending.isEmpty()) {
				for (OxyPrincipalReturn listPrincipalReturn : listOfInterestPending) {
					LenderWithdrawalFundsFromDealsResponseDto lenderInterestReturn = new LenderWithdrawalFundsFromDealsResponseDto();
					OxyPrincipalReturn oxyPrincipalReturn = oxyPrincipalReturnRepo.findById(listPrincipalReturn.getId())
							.get();
					if (oxyPrincipalReturn != null) {
						oxyPrincipalReturn.setStatus(OxyPrincipalReturn.Status.INTERESTRETURNED);
						oxyPrincipalReturn.setInterestApprovedOn(new Date());
						oxyPrincipalReturnRepo.save(oxyPrincipalReturn);
						lenderInterestReturn.setAmount(oxyPrincipalReturn.getInterestAmount());
						lenderInterestReturn.setDifferenceInDays(oxyPrincipalReturn.getDifferenceInDays());
						User user = userRepo.findById(oxyPrincipalReturn.getUserId()).get();
						lenderInterestReturn.setUserId(oxyPrincipalReturn.getUserId());

						if (user != null) {
							if (user.getBankDetails() != null) {
								if (user.getBankDetails().getUserName() != null) {
									lenderInterestReturn.setLenderName(
											user.getBankDetails().getUserName().trim().replaceAll(" +", " "));

								}
								if (user.getBankDetails().getAccountNumber() != null) {
									lenderInterestReturn.setAccountNumber(user.getBankDetails().getAccountNumber());

								}
								if (user.getBankDetails().getIfscCode() != null) {
									lenderInterestReturn.setIfsc(user.getBankDetails().getIfscCode().toUpperCase());

								}
							}
						}
					}
					String principalDate = null;
					if (oxyPrincipalReturn.getPrincipalApprovedOn() != null) {
						principalDate = expectedDateFormat.format(oxyPrincipalReturn.getPrincipalApprovedOn());
					} else {
						principalDate = expectedDateFormat.format(oxyPrincipalReturn.getPrincipalModifiedOn());
					}

					LenderCmsPayments lenderCmsPayments = lenderCmsPaymentsRepo.previousInterestGiven(dealId,
							principalDate);
					if (lenderCmsPayments != null) {
						LenderCmsPaymentsDetails lenderCmsPaymentsDetails = lenderCmsPaymentsDetailsRepo
								.findByLenderCmsPaymentsIdAndLenderId(lenderCmsPayments.getId(),
										oxyPrincipalReturn.getUserId());
						if (lenderCmsPaymentsDetails == null) {
							listOfLenders.add(lenderInterestReturn);
						}

					} else {
						listOfLenders.add(lenderInterestReturn);
					}

				}
			}

		}
		listOfLendersWithdrawalFundsInfo.setLenderWithdrawalFundsFromDealsResponseDto(listOfLenders);
		message = creatingExcelSheetForh2h(dealId, listOfLendersWithdrawalFundsInfo, lenderPrincipal.getStatus(),
				lenderPrincipal.getAccountType());

		lenderPrincipalResponseDto.setStatus(message);
		return lenderPrincipalResponseDto;

	}

	public String creatingExcelSheetForh2h(Integer dealId,
			ListOfLendersWithdrawalFundsInfo listOfLendersWithdrawalFundsInfo, String status, String accountType) {
		OxyBorrowersDealsInformation dealInfo = oxyBorrowersDealsInformationRepo.findById(dealId).get();
		System.out.println(iciciFilePathBeforeApproval);
		String message = null;
		String dealName = null;
		if (dealInfo != null) {
			dealName = dealInfo.getDealName().replaceAll("[^a-zA-Z0-9]", "_");

		}

		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		Date today = new Date();
		String currentDateValue = dateFormatter.format(today);

		String currentDateSpliting[] = currentDateValue.split("/");

		SimpleDateFormat formatter1 = new SimpleDateFormat("ddMMyy");
		Date date = new Date();
		String currentDate = formatter1.format(date);
		String type = null;
		String appendingValue = null;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		LocalDateTime now = LocalDateTime.now();
		StringBuilder sbf = null;
		String accountNumber = null;
		if (accountType == null) {
			sbf = new StringBuilder("OXYLR1_OXYLR1UPLD_"); // for interest
			accountNumber = "777705849442";
		} else {
			if (accountType.equalsIgnoreCase("DISBURSMENT")) { // principal amount
				sbf = new StringBuilder("OXYLR2_OXYLR2UPLD_");
				accountNumber = "777705849441";
			} else {
				sbf = new StringBuilder("OXYLR1_OXYLR1UPLD_");
				accountNumber = "777705849442";

			}

		}

		SimpleDateFormat formatter = new SimpleDateFormat("hhmm");
		Date d = new Date();
		String hourMint = formatter.format(d);
		AmountType amountType = null;
		if (status.equalsIgnoreCase("INITIATED")) {
			sbf.append(currentDate + "_" + dealId + "P" + hourMint);
			type = "LENDERPRINCIPAL";
			appendingValue = "Principal";
			amountType = AmountType.PRINCIPAL;
		} else {
			sbf.append(currentDate + "_" + dealId + "I" + hourMint);
			type = "PRINCIPALINTEREST";
			appendingValue = "Interest";
			amountType = AmountType.INTEREST;
		}

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 40; i++) {
			spreadSheet.autoSizeColumn(i);
		}
		double totalAmount = 0.0;
		List<LenderWithdrawalFundsFromDealsResponseDto> listOfLenders = listOfLendersWithdrawalFundsInfo
				.getLenderWithdrawalFundsFromDealsResponseDto();
		if (listOfLenders != null && !listOfLenders.isEmpty()) {
			for (LenderWithdrawalFundsFromDealsResponseDto lenderPricipalReturn : listOfLenders) {
				double remainingAmount = holdAmountService.remainingAmountAfterRemovingHoldAmount(
						lenderPricipalReturn.getUserId(), dealId, amountType, lenderPricipalReturn.getAmount());
				if (remainingAmount > 0.0) {
					User user = userRepo.findById(lenderPricipalReturn.getUserId()).get();
					logger.info("testUserType" + user.isTestUser());
					if (user.isTestUser() == false) {

						Row row1 = spreadSheet.createRow(++rowCount);
						Cell cell1 = row1.createCell(0);
						cell1.setCellValue(accountNumber);

						cell1 = row1.createCell(1);
						cell1.setCellValue(lenderPricipalReturn.getAccountNumber());

						cell1 = row1.createCell(2);
						cell1.setCellValue(lenderPricipalReturn.getLenderName());

						cell1 = row1.createCell(3);
						cell1.setCellValue(remainingAmount);

						totalAmount = totalAmount + remainingAmount;

						cell1 = row1.createCell(4);
						cell1.setCellValue("N");

						int y = Integer.parseInt(currentDateSpliting[1]);

						String monthName12 = DateTime.now().withMonthOfYear(y).toString("MMM");

						String requiredDate = currentDateSpliting[0] + "-" + monthName12 + "-" + currentDateSpliting[2];

						cell1 = row1.createCell(5);
						cell1.setCellValue(requiredDate);

						cell1 = row1.createCell(6);
						cell1.setCellValue(lenderPricipalReturn.getIfsc());

						cell1 = row1.createCell(15);
						cell1.setCellValue(lenderPricipalReturn.getDifferenceInDays() + " days");

						cell1 = row1.createCell(16);
						cell1.setCellValue(type);

						cell1 = row1.createCell(17);
						cell1.setCellValue(dealId);

						cell1 = row1.createCell(18);
						cell1.setCellValue("LR" + lenderPricipalReturn.getUserId());

						cell1 = row1.createCell(19);
						cell1.setCellValue("OXYLOANS " + dealName + "M" + currentDateSpliting[0]);

						cell1 = row1.createCell(20);
						cell1.setCellValue(dealName + appendingValue);
					}
				}
			}
		}
		File f = new File(iciciFilePathBeforeApproval + sbf.toString() + ".xls");
		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);

			workBook.close();
			outFile.close();

		} catch (Exception ex) {
			ex.getMessage();
		}

		Calendar current = Calendar.getInstance();
		TemplateContext templateContext = new TemplateContext();
		String expectedCurrentDate1 = expectedDateFormat.format(current.getTime());
		templateContext.put("currentDate", expectedCurrentDate1);
		String mailSubject = null;
		String returnType = null;
		if (appendingValue.equals("Interest")) {
			mailSubject = "Manual" + dealId + " InterestSheet";
			returnType = "PRINCIPALINTEREST";
		} else {
			mailSubject = "Manual" + dealId + " PrincipalSheet";
			returnType = "LENDERPRINCIPAL";
		}

		LenderCmsPayments lenderCmsPayments = new LenderCmsPayments();
		lenderCmsPayments.setDealId(dealId);
		lenderCmsPayments.setTotalAmount(totalAmount);
		lenderCmsPayments.setFileName(f.getName());
		lenderCmsPayments.setLenderReturnsType(returnType);
		lenderCmsPayments.setPaymentDate(new Date());
		lenderCmsPaymentsRepo.save(lenderCmsPayments);

		if (returnType.equalsIgnoreCase("LENDERPRINCIPAL")) {

			Double dealCurrentAmount = getDealCurrentAmount(dealId);

			Double remainingPrincipalAmount = dealCurrentAmount - totalAmount;

			logger.info("dealCurrentAmount :" + dealCurrentAmount);
			logger.info("totalAmount :" + totalAmount);

			List<OxyPrincipalReturn> principalReturnedToBank = oxyPrincipalReturnRepo
					.findAllPrincipalToBankUsers(dealId);

			List<OxyPrincipalReturn> principalReturnedToWallet = oxyPrincipalReturnRepo
					.findAllPrincipalToWalletUsers(dealId);

			int paidLenders = principalReturnedToWallet.size() + principalReturnedToBank.size();

			Integer numberOfLendersParticipated = oxyLendersAcceptedDealsRepo
					.getListOfLendersMappedToDealIdCount(dealId);

			int remainingLenders = numberOfLendersParticipated - paidLenders;
			logger.info("principalReturnedToWallet size " + principalReturnedToWallet.size());
			logger.info("listOfLenders size " + listOfLenders.size());

			String whatsappMessage = "Principal is initiated for the deal " + dealName + "(" + dealId + ") on "
					+ expectedCurrentDate1 + " to " + listOfLenders.size() + " lenders for an amount of INR"
					+ totalAmount + ".\r\n" + "PENDING Principal AMOUNT in this deal :" + remainingPrincipalAmount
					+ "\r\n" + "PENDING LENDERS COUNT in this deal :" + remainingLenders + ".";

			whatappService.sendingWhatsappMessageWithNewInstance("120363020098924954@g.us", whatsappMessage);

		}

		if (returnType.equalsIgnoreCase("PRINCIPALINTEREST")) {

			String whatsappMessage = returnType + " is initiated for the deal " + dealName + "(" + dealId + ") on "
					+ expectedCurrentDate1 + " to " + listOfLenders.size() + " lenders for an amount of INR"
					+ totalAmount;

			whatappService.sendingWhatsappMessageWithNewInstance("120363020098924954@g.us", whatsappMessage);

		}
		EmailRequest emailRequest = new EmailRequest(
				new String[] { "archana.n@oxyloans.com", "ramadevi@oxyloans.com", "subbu@oxyloans.com" },
				"Interests-Approval.template", templateContext, mailSubject, new String[] { f.getAbsolutePath() });

		EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
		if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
			try {
				throw new MessagingException(emailResponseFromService.getErrorMessage());
			} catch (MessagingException e2) {
				e2.printStackTrace();
			}
		}

		message = "SuccessSentToH2H";
		return message;
	}

	private Double getDealCurrentAmount(int dealId) {

		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo.findById(dealId)
				.get();

		double currentAmount = 0.0;

		if (oxyBorrowersDealsInformation != null) {

			Double particiaptionAmount = 0.0;
			Double dealAmount = oxyBorrowersDealsInformation.getDealAmount();
			Double amount = oxyLendersAcceptedDealsRepo.getListOfLendersParticipatedAmount(dealId);
			Double updatedAmount = lendersPaticipationUpdationRepo.getDealUpdatedSumValue(dealId);
			if (amount != null) {
				if (updatedAmount != null) {
					particiaptionAmount = amount + updatedAmount;
				} else {
					particiaptionAmount = amount;
				}
			}
			Double returnedPrincipalAmount = lendersReturnsRepo.getSumValueReturnedToDeal(dealId);
			Double returnedToWallet = lenderOxyWalletNativeRepo.sumOfAmountReturnedFromDealToWallet(dealId);

			if (returnedPrincipalAmount == null && returnedToWallet == null) {
				currentAmount = particiaptionAmount;
			}
			if (returnedPrincipalAmount != null && returnedToWallet != null) {
				currentAmount = particiaptionAmount - (returnedPrincipalAmount + returnedToWallet);

			} else {
				if (returnedPrincipalAmount == null && returnedToWallet != null) {
					currentAmount = particiaptionAmount - returnedToWallet;
				}
				if (returnedPrincipalAmount != null && returnedToWallet == null) {
					currentAmount = particiaptionAmount - returnedPrincipalAmount;
				}
			}

		}
		return currentAmount;

	}

	@Override
	public LenderPaymentsResponse getDealPrincipalHistory(Integer dealId) {

		LenderPaymentsResponse response = new LenderPaymentsResponse();

		List<LendersReturns> principalList = lendersReturnsRepo.findByDealId(dealId);

		List<LenderOxyWallet> lenderWallList = lenderOxyWalletNativeRepo.findByDealId(dealId);
		List<LenderPayementDetails> lenderPayementDetailsListForWallet = new ArrayList<>();

		List<LenderPayementDetails> lenderPayementDetailsList = new ArrayList<>();

		if (principalList != null && !principalList.isEmpty()) {

			lenderPayementDetailsList = principalList.parallelStream().map(p -> new LenderPayementDetails(p.getUserId(),
					p.getAmount(), p.getPaidDate().toString(), p.getAmountType(), "", "", ""))
					.collect(Collectors.toList());
		}

		if (lenderWallList != null && !lenderWallList.isEmpty()) {

			lenderWallList.parallelStream().forEach(p -> {
				LenderPayementDetails dto = new LenderPayementDetails(p.getUserId(), (double) p.getTransactionAmount(),
						p.getTransactionDate().toString().substring(0, 10), "LENDERWALLET", p.getComments(), "", "");
				lenderPayementDetailsListForWallet.add(dto);

			});
		}

		// adding lender wallet list to the above lenderPayementDetailsList
		lenderPayementDetailsList.addAll(lenderPayementDetailsListForWallet);

//		Map<String,Set<LenderPayementDetails>> paymentsMap=lenderPayementDetailsList.parallelStream().
//				collect(Collectors.groupingBy(LenderPayementDetails :: getPaidDate,Collectors.mapping(null, null)));

		if (lenderPayementDetailsList != null && !lenderPayementDetailsList.isEmpty()) {
			Map<String, List<LenderPayementDetails>> paymentsMap1 = lenderPayementDetailsList.parallelStream()
					.collect(Collectors.groupingBy(p -> p.getPaidDate(), Collectors.toList()));

			// response.setLenderPayementDetailsList(lenderPayementDetailsList);
			response.setPaymentsMap(paymentsMap1);
		}

		return response;

	}

	@Override
	public List<LenderPayementDetails> bothPrincipalAndInteresReturnedDetailSummary(int dealId, String amountType) {
		List<LenderPayementDetails> paymentsDetails = new ArrayList<LenderPayementDetails>();
		List<LenderPayementDetails> paymentsDetailsAfterSorting = new ArrayList<LenderPayementDetails>();
		Predicate<String> predicate = s -> s.equalsIgnoreCase("INTEREST");
		if (predicate.test(amountType)) {
			List<LenderCmsPayments> interestSummary = lenderCmsPaymentsRepo.getInterestDetailSummary(dealId);
			if (interestSummary != null && !interestSummary.isEmpty()) {
				paymentsDetails = interestSummary.stream()
						.map(cms -> new LenderPayementDetails(0, cms.getTotalAmount(),
								expectedDateFormat.format(cms.getPaymentDate()), cms.getLenderReturnsType(), "",
								cms.getFileExecutionsSatus().toString(), cms.getFileName()))
						.collect(Collectors.toList());

			}
		} else {
			List<LenderCmsPayments> principalSummary = lenderCmsPaymentsRepo.getPrincipalDetailSummary(dealId);
			if (principalSummary != null && !principalSummary.isEmpty()) {
				paymentsDetails = principalSummary.stream()
						.map(cms -> new LenderPayementDetails(0, cms.getTotalAmount(),
								expectedDateFormat.format(cms.getPaymentDate()), cms.getLenderReturnsType(), "",
								cms.getFileExecutionsSatus().toString(), cms.getFileName()))
						.collect(Collectors.toList());
			}

			List<Object[]> listOfManualPrincipalReturn = lenderOxyWalletNativeRepo.manuvalPrincipalReturn(dealId);
			if (listOfManualPrincipalReturn != null && !listOfManualPrincipalReturn.isEmpty()) {
				for (Object[] e : listOfManualPrincipalReturn) {
					LenderPayementDetails lenderPayementDetails = new LenderPayementDetails();
					lenderPayementDetails.setUserId(0);
					lenderPayementDetails.setAmount(Double.parseDouble(e[1] == null ? "0.0" : e[1].toString()));
					lenderPayementDetails.setPaidDate(e[0] == null ? "" : e[0].toString());
					lenderPayementDetails.setAmountType("PRINCIPALTOWALLET(D)");
					lenderPayementDetails.setFileStatus("EXECUTED");
					paymentsDetails.add(lenderPayementDetails);
				}

			}
		}
		paymentsDetailsAfterSorting = paymentsDetails.stream()
				.sorted((d1, d2) -> d1.getPaidDate().compareTo(d2.getPaidDate())).collect(Collectors.toList());
		return paymentsDetailsAfterSorting;

	}

}
