package com.oxyloans.service.principalReturnservice;

import java.util.List;

import com.oxyloans.lender.withdrawalfunds.dto.LenderPrincipal;
import com.oxyloans.lender.withdrawalfunds.dto.LenderPrincipalResponseDto;
import com.oxyloans.principal.LenderPayementDetails;
import com.oxyloans.principal.LenderPaymentsResponse;

public interface PrincipalReturnServiceRepo {

	public LenderPrincipalResponseDto pricipalReturnToLender(Integer dealId, LenderPrincipal lenderPrincipal);

	public LenderPrincipal getPrincipalInitiated(int dealId, String status);

	public LenderPrincipalResponseDto sendingPricipalReturnToH2H(Integer dealId, LenderPrincipal lenderPrincipal);

	public LenderPaymentsResponse getDealPrincipalHistory(Integer dealId);

	public List<LenderPayementDetails> bothPrincipalAndInteresReturnedDetailSummary(int dealId, String amountType);

}
