package com.oxyloans.service.borrowerloanservice;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.oxyloans.entityborrowersdealsdto.BorrowerPaymentsStatusDto;
import com.oxyloans.entityborrowersdealsdto.BorrowersTotalFdsResponse;
import com.oxyloans.loan.BorrowerListOfPaymentsUploadedResponse;
import com.oxyloans.loan.BorrowerLoanResponseDto;
import com.oxyloans.loan.BorrowerNewBankDetailsRequestDto;
import com.oxyloans.loan.BorrowerNewBankDetailsResponseDto;
import com.oxyloans.loan.BorrowersMappedToDealRequestDto;
import com.oxyloans.loan.BorrowersMappedToDealResponseDto;
import com.oxyloans.loan.ClosedFdsWithRepaymentRequestDto;
import com.oxyloans.loan.FdCalculationResponseDto;
import com.oxyloans.loan.FdCloseingRequestDto;
import com.oxyloans.loan.FdSearchDownloadRequest;
import com.oxyloans.loan.FdSearchDownloadResponse;
import com.oxyloans.loan.FdStatisticsRequestDto;
import com.oxyloans.loan.FdStatisticsResponseDto;
import com.oxyloans.loan.InvoiceRequestDto;
import com.oxyloans.loan.InvoiceResponceDto;
import com.oxyloans.loan.ListOfBorrowerNewBankDetailsResponse;
import com.oxyloans.loan.ListOfPaymentsDetailsRequestDto;
import com.oxyloans.loan.ListOfPaymentsDetailsResponseDto;
import com.oxyloans.loan.MonthWiseBorrowersFdRequestDto;
import com.oxyloans.loan.MonthWiseBorrowersFdResponseDto;
import com.oxyloans.loan.PaymentUploadedRequestDto;
import com.oxyloans.loan.PaymentUploadedResponseDto;
import com.oxyloans.loan.SearchTypes;
import com.oxyloans.request.user.KycFileRequest;

@Repository
public interface LoanService {

	public BorrowerNewBankDetailsResponseDto savingBankDetails(
			BorrowerNewBankDetailsRequestDto borrowerNewBankDetailsRequestDto);

	public BorrowerNewBankDetailsResponseDto getNewBankDetailsOfuser(int UserId);

	public BorrowerNewBankDetailsResponseDto addingCreatedDateAndAmount(
			BorrowerNewBankDetailsRequestDto borrowerNewBankDetailsRequestDto);

	public FdCalculationResponseDto fdFreeCalculation(int userId);

	public BorrowerLoanResponseDto uploadsPaymentScreenshot(int userId, List<KycFileRequest> files) throws IOException;

	public PaymentUploadedResponseDto paymentUploaded(PaymentUploadedRequestDto paymentUploadedRequestDto);

	public PaymentUploadedResponseDto updatePaymentStatus(int paymentId, String status);

	public ListOfBorrowerNewBankDetailsResponse bankDetailsBeforeFdCreatedAndAfter(int pageNumber, int pageSize,
			String type);

	public ListOfPaymentsDetailsResponseDto getPaymentsBasedonBankTypeAndStatus(
			ListOfPaymentsDetailsRequestDto listOfPaymentsDetailsRequestDto);

	public PaymentUploadedResponseDto closingFd(FdCloseingRequestDto fdCloseingRequestDto);

	public BorrowersMappedToDealResponseDto fdTransferFromSystem(
			BorrowersMappedToDealRequestDto borrowersMappedToDealRequestDto);

	public ListOfBorrowerNewBankDetailsResponse fdStatusBasedSearch(
			ListOfPaymentsDetailsRequestDto listOfPaymentsDetailsRequestDto);

	public List<BorrowerListOfPaymentsUploadedResponse> getUserPayments(int userId);

	public BorrowerNewBankDetailsResponseDto searchByUserId(int userId);

	public FdStatisticsResponseDto fdStatistics(FdStatisticsRequestDto fdStatisticsRequestDto);

	public List<InvoiceResponceDto> generatedInvoives(InvoiceRequestDto invoiceRequestDto);

	public MonthWiseBorrowersFdResponseDto monthlyFdInformation(
			MonthWiseBorrowersFdRequestDto monthWiseBorrowersFdRequestDto);

	public SearchTypes listUniqueSearchNames(FdSearchDownloadRequest fdSearchDownloadRequest);

	public FdSearchDownloadResponse searchByType(FdSearchDownloadRequest fdSearchDownloadRequest);

	public String excelSheetForActiveBorrower();

	public MonthWiseBorrowersFdResponseDto getClosedFdsWithRepaymentDetails(
			ClosedFdsWithRepaymentRequestDto closedFdsWithRepaymentRequestDto);

	public BorrowerPaymentsStatusDto getTotalCreatedFdsDetails(
			ListOfPaymentsDetailsRequestDto listOfPaymentsDetailsRequestDto);

	public BorrowersTotalFdsResponse updateFdRepaymentStatus(FdCloseingRequestDto fdCloseingRequestDto);

}
