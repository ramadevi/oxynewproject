package com.oxyloans.service.borrowerloanservice;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.transaction.Transactional;

import org.apache.commons.collections.map.HashedMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.engine.template.IPdfEngine;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entity.borrower.payments.BorrowerBankDetailsHistory;
import com.oxyloans.entity.borrower.payments.BorrowerBankDetailsHistoryRepo;
import com.oxyloans.entity.borrower.payments.BorrowerPayments;
import com.oxyloans.entity.borrower.payments.BorrowerPaymentsFromSystem;
import com.oxyloans.entity.borrower.payments.BorrowerPaymentsFromSystemRepo;
import com.oxyloans.entity.borrower.payments.BorrowerPaymentsImages;
import com.oxyloans.entity.borrower.payments.BorrowerPaymentsImagesRepo;
import com.oxyloans.entity.borrower.payments.BorrowerPaymentsRepo;
import com.oxyloans.entity.borrower.payments.BorrowerRepaymentsHistory;
import com.oxyloans.entity.borrower.payments.BorrowerRepaymentsHistoryRepo;
import com.oxyloans.entityborrowersdealsdto.BorrowerPaymentsStatusDto;
import com.oxyloans.entityborrowersdealsdto.BorrowersTotalFdsResponse;
import com.oxyloans.entity.cms.LenderCmsPayments;
import com.oxyloans.repository.cms.LenderCmsPaymentsRepo;
import com.oxyloans.entity.lenders.hold.UserHoldAmountMappedToDeal.AmountType;
import com.oxyloans.entity.user.User;
import com.oxyloans.file.FileRequest;
import com.oxyloans.file.FileRequest.FileType;
import com.oxyloans.file.FileResponse;
import com.oxyloans.loan.BorrowerListOfPaymentsUploadedResponse;
import com.oxyloans.loan.BorrowerLoanResponseDto;
import com.oxyloans.loan.BorrowerNewBankDetailsRequestDto;
import com.oxyloans.loan.BorrowerNewBankDetailsResponseDto;
import com.oxyloans.loan.BorrowersMappedToDealRequestDto;
import com.oxyloans.loan.BorrowersMappedToDealResponseDto;
import com.oxyloans.loan.ClosedFdsWithRepaymentRequestDto;
import com.oxyloans.loan.FdCalculationResponseDto;
import com.oxyloans.loan.FdCloseingRequestDto;
import com.oxyloans.loan.FdInformation;
import com.oxyloans.loan.FdSearchDownloadRequest;
import com.oxyloans.loan.FdSearchDownloadResponse;
import com.oxyloans.loan.FdStatisticsRequestDto;
import com.oxyloans.loan.FdStatisticsResponseDto;
import com.oxyloans.loan.InvoiceRequestDto;
import com.oxyloans.loan.InvoiceResponceDto;
import com.oxyloans.loan.ListOfBorrowerNewBankDetailsResponse;
import com.oxyloans.loan.ListOfPaymentsDetailsRequestDto;
import com.oxyloans.loan.ListOfPaymentsDetailsResponseDto;
import com.oxyloans.loan.MonthWiseBorrowersFdRequestDto;
import com.oxyloans.loan.MonthWiseBorrowersFdResponseDto;
import com.oxyloans.loan.PaymentUploadedRequestDto;
import com.oxyloans.loan.PaymentUploadedResponseDto;
import com.oxyloans.loan.PaymentsInfo;
import com.oxyloans.loan.SearchTypes;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.user.KycFileRequest;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.service.file.IFileManagementService;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;

@Service
public class LoanServiceImpl implements LoanService {

	@Autowired
	private UserRepo userRepo;

	private final Logger logger = LogManager.getLogger(LoanServiceImpl.class);

	@Autowired
	private BorrowerPaymentsRepo borrowerPaymentsRepo;

	@Value("${iciciFilePathBeforeApproval}")
	private String iciciFilePathBeforeApproval;

	@Value("${iciciFilePathForBorrowerLoansApproval}")
	private String iciciFilePathForBorrowerLoansApproval;

	@Autowired
	private IEmailService emailService;

	protected final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	private LenderCmsPaymentsRepo lenderCmsPaymentsRepo;

	@Autowired
	private IFileManagementService fileManagementService;

	@Autowired
	private BorrowerPaymentsImagesRepo borrowerPaymentsImagesRepo;

	@Autowired
	private BorrowerBankDetailsHistoryRepo borrowerBankDetailsHistoryRepo;

	@Autowired
	protected IPdfEngine pdfEngine;

	private String[] units = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };

	private String[] teens = { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen",
			"Eighteen", "Nineteen" };

	private String[] tens = { "", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

	private String[] thousands = { "", "Thousand", "Million", "Billion" };

	@Autowired
	private BorrowerPaymentsFromSystemRepo borrowerPaymentsFromSystemRepo;

	@Autowired
	private BorrowerRepaymentsHistoryRepo borrowerRepaymentsHistoryRepo;

	@Value("${iciciReportsManualFolder}")
	private String iciciReportsManualFolder;

	@Override
	@Transactional
	public BorrowerNewBankDetailsResponseDto savingBankDetails(
			BorrowerNewBankDetailsRequestDto borrowerNewBankDetailsRequestDto) {
		User user = userRepo.findById(borrowerNewBankDetailsRequestDto.getUserId()).get();
		if (user != null) {
			if (user.getPrimaryType().equals(User.PrimaryType.BORROWER)) {
				BorrowerPayments borrowerPayments = borrowerPaymentsRepo
						.findByUserIdWithLimit(borrowerNewBankDetailsRequestDto.getUserId());
				if (borrowerPayments != null) {
					BorrowerPayments borrowerClosedPayments = borrowerPaymentsRepo
							.findByUserIdAndStatusNotAtClosed(borrowerNewBankDetailsRequestDto.getUserId());
					if (borrowerClosedPayments != null) {
						borrowerClosedPayments.setUserId(borrowerNewBankDetailsRequestDto.getUserId());
						borrowerClosedPayments.setAccountNumber(borrowerNewBankDetailsRequestDto.getAccountNumber());
						borrowerClosedPayments.setIfsc(borrowerNewBankDetailsRequestDto.getIfsc().toUpperCase());
						borrowerClosedPayments.setCity(borrowerNewBankDetailsRequestDto.getCity());
						borrowerClosedPayments.setBranch(borrowerNewBankDetailsRequestDto.getBranch());
						borrowerClosedPayments.setBankName(borrowerNewBankDetailsRequestDto.getBankName());
						borrowerClosedPayments.setName(borrowerNewBankDetailsRequestDto.getUserName());
						borrowerClosedPayments.setBankChoosen(borrowerNewBankDetailsRequestDto.getBankChoosen());
						borrowerClosedPayments.setBankDetailsVerifiedOn(new Date());
						borrowerClosedPayments.setBankVerifiedStatus(Boolean.TRUE);
						borrowerClosedPayments.setLeadBy(borrowerNewBankDetailsRequestDto.getLeadBy());
						borrowerClosedPayments.setConsultancy(borrowerNewBankDetailsRequestDto.getConsultancy());
						borrowerClosedPayments.setRoi(borrowerNewBankDetailsRequestDto.getRoi());
						borrowerClosedPayments.setFundingType(BorrowerPayments.FundingType
								.valueOf(borrowerNewBankDetailsRequestDto.getFundingType()));
						borrowerClosedPayments.setCountry(borrowerNewBankDetailsRequestDto.getCountry());
						borrowerClosedPayments.setUniversity(borrowerNewBankDetailsRequestDto.getUniversity());
						borrowerClosedPayments
								.setStudentMobileNumber(borrowerNewBankDetailsRequestDto.getStudentMobileNumber());
						borrowerClosedPayments.setFdAmount(borrowerNewBankDetailsRequestDto.getFdAmount());
						borrowerClosedPayments.setPaymentsCollection(BorrowerPayments.PaymentsCollection
								.valueOf(borrowerNewBankDetailsRequestDto.getPaymentsCollection()));
						borrowerClosedPayments.setLoanType(
								BorrowerPayments.LoanType.valueOf(borrowerNewBankDetailsRequestDto.getLoanType()));
						borrowerPaymentsRepo.save(borrowerClosedPayments);
					} else {
						savingBorrowersFdDetails(borrowerNewBankDetailsRequestDto);

					}

				} else {
					savingBorrowersFdDetails(borrowerNewBankDetailsRequestDto);
				}
				savingBankDetailsHistory(borrowerNewBankDetailsRequestDto);
			} else {
				throw new OperationNotAllowedException("Please check userType is not borrower",
						ErrorCodes.PERMISSION_DENIED);
			}
		}
		BorrowerNewBankDetailsResponseDto borrowerNewBankDetailsResponseDto = new BorrowerNewBankDetailsResponseDto();
		borrowerNewBankDetailsResponseDto.setUserId(borrowerNewBankDetailsRequestDto.getUserId());
		return borrowerNewBankDetailsResponseDto;

	}

	public void savingBankDetailsHistory(BorrowerNewBankDetailsRequestDto borrowerNewBankDetailsRequestDto) {
		BorrowerBankDetailsHistory borrowerBankDetailsHistory = new BorrowerBankDetailsHistory();
		borrowerBankDetailsHistory.setUserId(borrowerNewBankDetailsRequestDto.getUserId());
		borrowerBankDetailsHistory.setAccountNumber(borrowerNewBankDetailsRequestDto.getAccountNumber());
		borrowerBankDetailsHistory.setIfsc(borrowerNewBankDetailsRequestDto.getIfsc().toUpperCase());
		borrowerBankDetailsHistory.setCity(borrowerNewBankDetailsRequestDto.getCity());
		borrowerBankDetailsHistory.setBranch(borrowerNewBankDetailsRequestDto.getBranch());
		borrowerBankDetailsHistory.setBankName(borrowerNewBankDetailsRequestDto.getBankName());
		borrowerBankDetailsHistory.setName(borrowerNewBankDetailsRequestDto.getUserName());
		borrowerBankDetailsHistory.setBankChoosen(borrowerNewBankDetailsRequestDto.getBankChoosen());
		borrowerBankDetailsHistory.setLeadBy(borrowerNewBankDetailsRequestDto.getLeadBy());
		borrowerBankDetailsHistory.setConsultancy(borrowerNewBankDetailsRequestDto.getConsultancy());
		borrowerBankDetailsHistory.setRoi(borrowerNewBankDetailsRequestDto.getRoi());
		borrowerBankDetailsHistory.setFundingType(
				BorrowerBankDetailsHistory.FundingType.valueOf(borrowerNewBankDetailsRequestDto.getFundingType()));
		borrowerBankDetailsHistory.setCountry(borrowerNewBankDetailsRequestDto.getCountry());
		borrowerBankDetailsHistory.setUniversity(borrowerNewBankDetailsRequestDto.getUniversity());
		borrowerBankDetailsHistory.setStudentMobileNumber(borrowerNewBankDetailsRequestDto.getStudentMobileNumber());
		borrowerBankDetailsHistory.setFdAmount(borrowerNewBankDetailsRequestDto.getFdAmount());
		borrowerBankDetailsHistory.setPaymentsCollection(BorrowerBankDetailsHistory.PaymentsCollection
				.valueOf(borrowerNewBankDetailsRequestDto.getPaymentsCollection()));
		borrowerBankDetailsHistoryRepo.save(borrowerBankDetailsHistory);

	}

	public void savingBorrowersFdDetails(BorrowerNewBankDetailsRequestDto borrowerNewBankDetailsRequestDto) {
		BorrowerPayments borrowerPaymentsDetails = new BorrowerPayments();
		borrowerPaymentsDetails.setUserId(borrowerNewBankDetailsRequestDto.getUserId());
		borrowerPaymentsDetails.setAccountNumber(borrowerNewBankDetailsRequestDto.getAccountNumber());
		borrowerPaymentsDetails.setIfsc(borrowerNewBankDetailsRequestDto.getIfsc().toUpperCase());
		borrowerPaymentsDetails.setCity(borrowerNewBankDetailsRequestDto.getCity());
		borrowerPaymentsDetails.setBranch(borrowerNewBankDetailsRequestDto.getBranch());
		borrowerPaymentsDetails.setBankName(borrowerNewBankDetailsRequestDto.getBankName());
		borrowerPaymentsDetails.setName(borrowerNewBankDetailsRequestDto.getUserName());
		borrowerPaymentsDetails.setBankChoosen(borrowerNewBankDetailsRequestDto.getBankChoosen());
		borrowerPaymentsDetails.setBankDetailsVerifiedOn(new Date());
		borrowerPaymentsDetails.setBankVerifiedStatus(Boolean.TRUE);
		borrowerPaymentsDetails.setLeadBy(borrowerNewBankDetailsRequestDto.getLeadBy());
		borrowerPaymentsDetails.setConsultancy(borrowerNewBankDetailsRequestDto.getConsultancy());
		borrowerPaymentsDetails.setRoi(borrowerNewBankDetailsRequestDto.getRoi());
		borrowerPaymentsDetails.setFundingType(
				BorrowerPayments.FundingType.valueOf(borrowerNewBankDetailsRequestDto.getFundingType()));
		borrowerPaymentsDetails.setCountry(borrowerNewBankDetailsRequestDto.getCountry());
		borrowerPaymentsDetails.setUniversity(borrowerNewBankDetailsRequestDto.getUniversity());
		borrowerPaymentsDetails.setStudentMobileNumber(borrowerNewBankDetailsRequestDto.getStudentMobileNumber());
		borrowerPaymentsDetails.setFdAmount(borrowerNewBankDetailsRequestDto.getFdAmount());
		borrowerPaymentsDetails.setDealId(1);
		borrowerPaymentsDetails.setPaymentsCollection(
				BorrowerPayments.PaymentsCollection.valueOf(borrowerNewBankDetailsRequestDto.getPaymentsCollection()));
		borrowerPaymentsDetails
				.setLoanType(BorrowerPayments.LoanType.valueOf(borrowerNewBankDetailsRequestDto.getLoanType()));
		borrowerPaymentsRepo.save(borrowerPaymentsDetails);
	}

	@Override
	@Transactional
	public BorrowerNewBankDetailsResponseDto getNewBankDetailsOfuser(int userId) {
		BorrowerPayments borrowerPayments = borrowerPaymentsRepo.findByUserId(userId);
		BorrowerNewBankDetailsResponseDto borrowerPaymentsDetails = new BorrowerNewBankDetailsResponseDto();
		if (borrowerPayments != null) {
			borrowerPaymentsDetails.setUserId(userId);
			borrowerPaymentsDetails.setAccountNumber(borrowerPayments.getAccountNumber());
			borrowerPaymentsDetails.setIfsc(borrowerPayments.getIfsc());
			borrowerPaymentsDetails.setCity(borrowerPayments.getCity());
			borrowerPaymentsDetails.setBranch(borrowerPayments.getBranch());
			borrowerPaymentsDetails.setBankName(borrowerPayments.getBankName());
			borrowerPaymentsDetails.setUserName(borrowerPayments.getName());
			borrowerPaymentsDetails.setBankChoosen(borrowerPayments.getBankChoosen().toString());
			borrowerPaymentsDetails
					.setBankDetailsVerifiedOn(expectedDateFormat.format(borrowerPayments.getBankDetailsVerifiedOn()));
			borrowerPaymentsDetails.setBankVerifiedStatus(borrowerPayments.getBankVerifiedStatus());
			borrowerPaymentsDetails.setLeadBy(borrowerPayments.getLeadBy());
			borrowerPaymentsDetails.setConsultancy(borrowerPayments.getConsultancy());
			borrowerPaymentsDetails.setRoi(borrowerPayments.getRoi());
			borrowerPaymentsDetails.setFundingType(borrowerPayments.getFundingType().toString());
			borrowerPaymentsDetails.setCountry(borrowerPayments.getCountry());
			borrowerPaymentsDetails.setUniversity(borrowerPayments.getUniversity());
			borrowerPaymentsDetails.setStudentMobileNumber(borrowerPayments.getStudentMobileNumber());
			borrowerPaymentsDetails.setFdAmount(borrowerPayments.getFdAmount());
			borrowerPaymentsDetails.setFdAmountFromSystem(borrowerPayments.getAmount());
			if (borrowerPayments.getFdCreated() != null) {
				borrowerPaymentsDetails.setFdCreatedDate(expectedDateFormat.format(borrowerPayments.getFdCreated()));
			}
			if (borrowerPayments.getFdValidity() != null) {
				borrowerPaymentsDetails.setFdValidityDate(expectedDateFormat.format(borrowerPayments.getFdValidity()));
			}
			if (borrowerPayments.getFdClosedDate() != null) {
				borrowerPaymentsDetails.setFdClosedDate(expectedDateFormat.format(borrowerPayments.getFdClosedDate()));
			}
			if (borrowerPayments.getFeeInvoiceUrl() != null) {
				borrowerPaymentsDetails.setFeeInvoice(borrowerPayments.getFeeInvoiceUrl());
			}
			borrowerPaymentsDetails.setPaymentsCollection(borrowerPayments.getPaymentsCollection().toString());
			borrowerPaymentsDetails.setLoanType(borrowerPayments.getLoanType().toString());
		} else {
			User user = userRepo.findById(userId).get();
			if (user != null) {
				borrowerPaymentsDetails.setRegisteredMobileNumber(user.getMobileNumber());
				if (user.getPersonalDetails() != null) {
					borrowerPaymentsDetails.setNameFromProfile(
							user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName());
				}
			}
		}
		return borrowerPaymentsDetails;

	}

	@Override
	@Transactional
	public BorrowerNewBankDetailsResponseDto addingCreatedDateAndAmount(
			BorrowerNewBankDetailsRequestDto borrowerNewBankDetailsRequestDto) {
		BorrowerPayments borrowerPayments = borrowerPaymentsRepo
				.findByUserId(borrowerNewBankDetailsRequestDto.getUserId());
		if (borrowerPayments != null) {
			borrowerPayments.setFdAmount(borrowerNewBankDetailsRequestDto.getFdAmount());
			try {
				borrowerPayments
						.setFdCreated(expectedDateFormat.parse(borrowerNewBankDetailsRequestDto.getCreatedDate()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			borrowerPayments.setFdStatus(BorrowerPayments.FdStatus.SAVED);
			borrowerPaymentsRepo.save(borrowerPayments);

		}
		BorrowerNewBankDetailsResponseDto borrowerNewBankDetailsResponseDto = new BorrowerNewBankDetailsResponseDto();
		borrowerNewBankDetailsResponseDto.setUserId(borrowerNewBankDetailsRequestDto.getUserId());
		return borrowerNewBankDetailsResponseDto;

	}

	@Override
	@Transactional
	public FdCalculationResponseDto fdFreeCalculation(int userId) {
		BorrowerPayments borrowerPayments = borrowerPaymentsRepo.findByUserId(userId);
		FdCalculationResponseDto fdCalculationResponseDto = new FdCalculationResponseDto();
		List<Map<String, Double>> mapList = new ArrayList<Map<String, Double>>();
		double interestPerDay = 0.0;
		if (borrowerPayments != null) {
			fdCalculationResponseDto.setUserId(userId);
			fdCalculationResponseDto.setFdAmount(borrowerPayments.getFdAmount());
			interestPerDay = ((borrowerPayments.getFdAmount() * borrowerPayments.getRoi()) / 100) / 30;
			fdCalculationResponseDto.setPerDayInterest(interestPerDay);
			int value = 15;
			for (int i = 0; i <= 2; i++) {
				Map<String, Double> map = new HashedMap();
				map.put("Days " + value, value * interestPerDay);

				mapList.add(map);
				value += 15;
			}
			fdCalculationResponseDto.setId(borrowerPayments.getId());
		}

		fdCalculationResponseDto.setCalculation(mapList);

		return fdCalculationResponseDto;

	}

	@Override
	@Transactional
	public BorrowerLoanResponseDto uploadsPaymentScreenshot(int userId, List<KycFileRequest> files) throws IOException {
		BorrowerLoanResponseDto borrowerLoanResponseDto = new BorrowerLoanResponseDto();
		List<Map<String, String>> listOfPayments = new ArrayList();
		for (KycFileRequest kyc : files) {
			logger.info("Uploading the file anothertime : " + kyc.getFileName() + " of type " + kyc.getKycType());
			FileRequest fileRequest = new FileRequest();

			InputStream fileStream = kyc.getFileInputStream();
			if (fileStream == null) {
				fileStream = decodeBase64Stream(kyc.getBase64EncodedStream());
			}
			LocalTime currentTime = LocalTime.now();

			// Define the desired format
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

			// Format and display the current time
			String formattedTime = currentTime.format(formatter);
			fileRequest.setInputStream(fileStream);
			fileRequest.setUserId(userId);
			fileRequest.setFileName(kyc.getFileName());
			fileRequest.setFilePrifix(kyc.getKycType().name());
			fileManagementService.putFile(fileRequest);
			FileResponse file = fileManagementService.getFile(fileRequest);
			String download = file.getDownloadUrl();
			String[] url = download.split("\\?");
			Map<String, String> map = new HashedMap();
			map.put(kyc.getKycType().toString(), url[0]);
			listOfPayments.add(map);
		}
		borrowerLoanResponseDto.setPaymentsUrls(listOfPayments);
		return borrowerLoanResponseDto;

	}

	public InputStream decodeBase64Stream(String base64EncodedStream) {
		if (base64EncodedStream == null) {
			return null;
		}
		return new ByteArrayInputStream(Base64.getDecoder().decode(base64EncodedStream));
	}

	@Override
	public BorrowersMappedToDealResponseDto fdTransferFromSystem(
			BorrowersMappedToDealRequestDto borrowersMappedToDealRequestDto) {

		BorrowerPayments borrowerPayments = borrowerPaymentsRepo
				.findByUserId(borrowersMappedToDealRequestDto.getUserId());
		if (borrowerPayments != null) {
			List<BorrowerPaymentsFromSystem> listOfBorrowerPaymentsFromSystem = borrowerPaymentsFromSystemRepo
					.findByBorrowerPaymentsId(borrowerPayments.getId());
			double previousAmount = 0.0;
			if (listOfBorrowerPaymentsFromSystem != null && !listOfBorrowerPaymentsFromSystem.isEmpty()) {
				previousAmount = listOfBorrowerPaymentsFromSystem.stream()
						.mapToDouble(BorrowerPaymentsFromSystem::getAmount).sum();
			}
			if ((previousAmount + borrowersMappedToDealRequestDto.getAmountFromSystem()) <= 1000000) {
				BorrowerPaymentsFromSystem borrowerPaymentsFromSystem = new BorrowerPaymentsFromSystem();
				borrowerPaymentsFromSystem.setBorrowerPaymentsId(borrowerPayments.getId());
				borrowerPaymentsFromSystem.setAmount(borrowersMappedToDealRequestDto.getAmountFromSystem());
				borrowerPaymentsFromSystem.setCreatedOn(new Date());
				borrowerPaymentsFromSystemRepo.save(borrowerPaymentsFromSystem);
				borrowerPayments.setAmount(
						borrowerPayments.getAmount() + borrowersMappedToDealRequestDto.getAmountFromSystem());
				borrowerPayments.setBorrowerFee(borrowersMappedToDealRequestDto.getFeeAmount() == null ? 0.0
						: borrowersMappedToDealRequestDto.getFeeAmount());
				borrowerPayments.setFdFundTransferRemarks(borrowersMappedToDealRequestDto.getFdFundTransferRemarks());
				borrowerPaymentsRepo.save(borrowerPayments);
			} else {
				throw new OperationNotAllowedException("you cant give more than 10 lakes please check",
						ErrorCodes.PERMISSION_DENIED);
			}
		}

		creatingExcelSheetForh2h(borrowerPayments, borrowersMappedToDealRequestDto.getAmountFromSystem());
		BorrowersMappedToDealResponseDto borrowersMappedToDealResponseDto = new BorrowersMappedToDealResponseDto();
		borrowersMappedToDealResponseDto.setStatus("sheetGenerated");
		return borrowersMappedToDealResponseDto;

	}

	public String creatingExcelSheetForh2h(BorrowerPayments borrowerPayments, Double amountFromSystem) {

		String message = null;

		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		Date today = new Date();
		String currentDateValue = dateFormatter.format(today);

		String currentDateSpliting[] = currentDateValue.split("/");

		SimpleDateFormat formatter1 = new SimpleDateFormat("ddMMyy");
		Date date = new Date();
		String currentDate = formatter1.format(date);
		String type = null;
		String appendingValue = null;

		LocalDateTime now = LocalDateTime.now();
		StringBuilder sbf = new StringBuilder("OXYLR2_OXYLR2UPLD_");

		String accountNumber = "777705849441";

		SimpleDateFormat formatter = new SimpleDateFormat("hhmm");
		Date d = new Date();
		String hourMint = formatter.format(d);
		sbf.append(currentDate).append('_').append("Loan").append(borrowerPayments.getId()).append('_')
				.append(hourMint);
		AmountType amountType = null;

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 40; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		Row row1 = spreadSheet.createRow(++rowCount);
		Cell cell1 = row1.createCell(0);
		cell1.setCellValue(accountNumber);

		cell1 = row1.createCell(1);
		cell1.setCellValue(borrowerPayments.getAccountNumber());

		cell1 = row1.createCell(2);
		cell1.setCellValue(borrowerPayments.getName());

		double totalAmount = amountFromSystem;

		cell1 = row1.createCell(3);
		cell1.setCellValue(amountFromSystem);

		cell1 = row1.createCell(4);
		cell1.setCellValue("N");

		int y = Integer.parseInt(currentDateSpliting[1]);

		String monthName12 = DateTime.now().withMonthOfYear(y).toString("MMM");

		String requiredDate = currentDateSpliting[0] + "-" + monthName12 + "-" + currentDateSpliting[2];

		cell1 = row1.createCell(5);
		cell1.setCellValue(requiredDate);

		cell1 = row1.createCell(6);
		cell1.setCellValue(borrowerPayments.getIfsc().toUpperCase());

		/*
		 * cell1 = row1.createCell(16); cell1.setCellValue("LOAN");
		 */

		cell1 = row1.createCell(16);
		cell1.setCellValue(borrowerPayments.getFdFundTransferRemarks());

		cell1 = row1.createCell(18);
		cell1.setCellValue("BR" + borrowerPayments.getUserId());

		/*
		 * cell1 = row1.createCell(19); cell1.setCellValue("OXYLOANS " +
		 * borrowerPayments.getUserId() + "M" + currentDateSpliting[0]);
		 */

		cell1 = row1.createCell(19);
		cell1.setCellValue(borrowerPayments.getFdFundTransferRemarks());

		cell1 = row1.createCell(20);
		cell1.setCellValue(borrowerPayments.getUserId());

		File f = new File(iciciFilePathForBorrowerLoansApproval + sbf.toString() + ".xls");
		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);

			workBook.close();
			outFile.close();

		} catch (Exception ex) {
			ex.getMessage();
		}

		Calendar current = Calendar.getInstance();
		TemplateContext templateContext = new TemplateContext();
		String expectedCurrentDate1 = expectedDateFormat.format(current.getTime());
		templateContext.put("currentDate", expectedCurrentDate1);
		String mailSubject = null;

		mailSubject = "Loan For" + borrowerPayments.getUserId();

		LenderCmsPayments lenderCmsPayments = new LenderCmsPayments();
		lenderCmsPayments.setDealId(0);
		lenderCmsPayments.setUserId(borrowerPayments.getUserId());
		lenderCmsPayments.setTotalAmount(amountFromSystem);
		lenderCmsPayments.setFileName(f.getName());
		lenderCmsPayments.setLenderReturnsType("LOAN");
		lenderCmsPayments.setPaymentDate(new Date());
		lenderCmsPayments.setBorrowerPaymentsId(borrowerPayments.getId());
		lenderCmsPaymentsRepo.save(lenderCmsPayments);

		EmailRequest emailRequest = new EmailRequest(
				new String[] { "archana.n@oxyloans.com", "subbu@oxyloans.com", "ramadevi@oxyloans.com",
						"vijaydasari060@gmail.com" },
				"Interests-Approval.template", templateContext, mailSubject, new String[] { f.getAbsolutePath() });

		EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
		if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
			try {
				throw new MessagingException(emailResponseFromService.getErrorMessage());
			} catch (MessagingException e2) {
				e2.printStackTrace();
			}
		}

		message = "SuccessSentToH2H";
		return message;
	}

	@Override
	@Transactional
	public PaymentUploadedResponseDto paymentUploaded(PaymentUploadedRequestDto paymentUploadedRequestDto) {
		PaymentUploadedResponseDto paymentUploadedResponseDto = new PaymentUploadedResponseDto();
		try {
			List<PaymentsInfo> paymentsInfo = paymentUploadedRequestDto.getPaymentsInfo();
			if (paymentsInfo != null && !paymentsInfo.isEmpty()) {
				paymentsInfo.stream().forEach(payments -> {
					BorrowerPaymentsImages borrowerPaymentsImages = new BorrowerPaymentsImages();
					borrowerPaymentsImages.setBorrowerPaymentsId(payments.getPaymentId());
					borrowerPaymentsImages.setAmountPaid(payments.getAmountPaid());
					borrowerPaymentsImages.setDocumentUploadUrl(payments.getUrl());
					borrowerPaymentsImages.setNumberOfDaysPaid(0);
					borrowerPaymentsImages
							.setAccountType(BorrowerPaymentsImages.AccountType.valueOf(payments.getAccountType()));

					borrowerPaymentsImagesRepo.save(borrowerPaymentsImages);
				});
			}

			paymentUploadedResponseDto.setStatus("updated");

		} catch (Exception e) {
			logger.info("Exception in paymentUploaded method");
		}

		return paymentUploadedResponseDto;

	}

	@Override
	@Transactional
	public PaymentUploadedResponseDto updatePaymentStatus(int paymentId, String status) {
		PaymentUploadedResponseDto paymentUploadedResponseDto = new PaymentUploadedResponseDto();
		// try {
		BorrowerPaymentsImages borrowerPaymentsImages = borrowerPaymentsImagesRepo.findById(paymentId).get();
		if (borrowerPaymentsImages != null) {
			borrowerPaymentsImages.setDocumentStatus(BorrowerPaymentsImages.DocumentStatus.valueOf(status));
			BorrowerPayments borrowerPayments = borrowerPaymentsRepo
					.findById(borrowerPaymentsImages.getBorrowerPaymentsId()).get();
			if (borrowerPayments.getFdCreated() == null) {
				throw new OperationNotAllowedException("Please check fd created date not given to this user",
						ErrorCodes.PERMISSION_DENIED);
			}
			if (status.equals("APPROVED")) {

				if (borrowerPaymentsImages.getAccountType().equals(BorrowerPaymentsImages.AccountType.HDFC)) {
					String outputFileName = borrowerPayments.getUserId() + paymentId + ".pdf";
					String generatedPdf = "";
					TemplateContext templateContext = new TemplateContext();
					templateContext.put("type", "Tax Invoice");
					templateContext.put("financialYear", getFinancialYear());
					templateContext.put("randomNumber", fourDigitsRandomNumber());
					templateContext.put("currentDate", currentDate());
					templateContext.put("borrowerId", "BR" + borrowerPayments.getUserId());
					templateContext.put("borrowerName", borrowerPayments.getName());
					templateContext.put("feePaid", borrowerPaymentsImages.getAmountPaid());
					templateContext.put("feePaidGst", Math.round(borrowerPaymentsImages.getAmountPaid() * 9 / 100));
					double centralAndState = Math.round((borrowerPaymentsImages.getAmountPaid() * 9 / 100) * 2);
					templateContext.put("totalGst", centralAndState);
					templateContext.put("feeWithGst", borrowerPaymentsImages.getAmountPaid() + centralAndState);
					templateContext.put("totalGstInNumber", numberToWord((int) centralAndState));
					templateContext.put("feeWithGstInNumber",
							numberToWord((int) (borrowerPaymentsImages.getAmountPaid() + centralAndState)));

					generatedPdf = pdfEngine.generatePdf("agreement/invoicePayment.xml", templateContext,
							outputFileName);

					FileRequest fileRequest = new FileRequest();
					try {
						fileRequest.setInputStream(new FileInputStream(generatedPdf));
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					fileRequest.setFileName(outputFileName);
					fileRequest.setFileType(FileType.Invoice);
					fileRequest.setFilePrifix(FileType.Invoice.name());
					try {
						fileManagementService.putFile(fileRequest);
						FileResponse putFileResponse = fileManagementService.getFile(fileRequest);
						String download = putFileResponse.getDownloadUrl();
						String[] url = download.split("\\?");
						paymentUploadedResponseDto.setInvoiceUrl(url[0].toString());
						borrowerPaymentsImages.setInvoiceUrl(url[0].toString());
						borrowerPaymentsImagesRepo.save(borrowerPaymentsImages);

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
					}
					File file = new File(generatedPdf);
					boolean fileDeleted = file.delete();

				}

				if (borrowerPayments != null) {
					double interestPerDay = 0.0;
					if (borrowerPayments.getPerDayInterest() == 0) {
						double perDayInterest = Math
								.round((borrowerPayments.getFdAmount() * borrowerPayments.getRoi()) / 100);
						borrowerPayments.setPerDayInterest(perDayInterest / 30);
						borrowerPaymentsRepo.save(borrowerPayments);
						interestPerDay = Math.round(borrowerPayments.getPerDayInterest());
					} else {
						interestPerDay = borrowerPayments.getPerDayInterest();
					}
					logger.error("interestPerDay" + interestPerDay);
					double doubleValue = Math.round(borrowerPaymentsImages.getAmountPaid() / interestPerDay);
					int intValue = (int) doubleValue;
					borrowerPaymentsImages.setNumberOfDaysPaid(intValue);
					borrowerPaymentsImagesRepo.save(borrowerPaymentsImages);

					String dateString = null;
					if (borrowerPayments.getFdValidity() != null) {
						dateString = dateFormat.format(borrowerPayments.getFdValidity());
					} else {

						dateString = dateFormat.format(borrowerPayments.getFdCreated());
					}
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
					LocalDate date = LocalDate.parse(dateString, formatter);

					LocalDate newDate = date.plusDays(borrowerPaymentsImages.getNumberOfDaysPaid());

					String newDateString = newDate.format(formatter);
					try {

						SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd");
						Date inputDate = inputDateFormat.parse(newDateString);

						borrowerPayments.setFdValidity(inputDate);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}

			borrowerPaymentsImagesRepo.save(borrowerPaymentsImages);
		}
		paymentUploadedResponseDto.setStatus("updated");

		/*
		 * } catch (Exception e) {
		 * logger.info("Exception in updatePaymentStatus method"); }
		 */
		return paymentUploadedResponseDto;

	}

	public String getFinancialYear() {
		LocalDate currentDate = LocalDate.now();
		int currentYear = currentDate.getYear();
		int currentMonth = currentDate.getMonthValue();

		int financialYearStart;
		int financialYearEnd;

		if (currentMonth >= Month.APRIL.getValue()) {
			// If the current month is April or later, the financial year starts from the
			// current year
			financialYearStart = currentYear;
			financialYearEnd = currentYear + 1;
		} else {
			// Otherwise, the financial year starts from the previous year
			financialYearStart = currentYear - 1;
			financialYearEnd = currentYear;
		}
		StringBuilder stringBuider = new StringBuilder();
		stringBuider.append(financialYearStart).append('-').append(financialYearEnd);
		return stringBuider.toString();

	}

	public String fourDigitsRandomNumber() {
		return String.valueOf(ThreadLocalRandom.current().nextInt(1000, 9999 + 1));
	}

	public String currentDate() {
		LocalDate currentDate = LocalDate.now();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy", Locale.ENGLISH);
		return currentDate.format(formatter);

	}

	public String numberToWord(int number) {
		return convertNumberToWords(number);

	}

	public String convertNumberToWords(int num) {
		if (num == 0) {
			return "Zero";
		}

		List<String> chunks = new ArrayList<>();
		int chunkCount = 0;

		while (num > 0) {
			if (num % 1000 != 0) {
				chunks.add(convertChunk(num % 1000) + " " + thousands[chunkCount]);
			}
			num /= 1000;
			chunkCount++;
		}

		Collections.reverse(chunks);
		return String.join(" ", chunks);
	}

	private String convertChunk(int num) {
		return (num < 10) ? units[num]
				: (num < 20) ? teens[num - 10]
						: (num < 100) ? tens[num / 10] + ((num % 10 != 0) ? " " + units[num % 10] : "")
								: units[num / 100] + " Hundred"
										+ ((num % 100 != 0) ? " and " + convertChunk(num % 100) : "");
	}

	@Override
	@Transactional
	public ListOfBorrowerNewBankDetailsResponse bankDetailsBeforeFdCreatedAndAfter(int pageNumber, int pageSize,
			String type) {
		ListOfBorrowerNewBankDetailsResponse listOfBorrowerNewBankDetails = searchCall(type, pageNumber, pageSize);
		return listOfBorrowerNewBankDetails;

	}

	public ListOfBorrowerNewBankDetailsResponse searchCall(String type, int pageNumber, int pageSize) {
		ListOfBorrowerNewBankDetailsResponse listOfBorrowerNewBankDetailsResponse = new ListOfBorrowerNewBankDetailsResponse();
		int pageNo = (pageSize * (pageNumber - 1));
		List<BorrowerPayments> borrowerPayments = null;
		int count = 0;
		if (type.equals("BEFORE")) {
			borrowerPayments = borrowerPaymentsRepo.getListOfNewBorrowersBankDetails(pageNo, pageSize);
			Integer value = borrowerPaymentsRepo.getListOfNewBorrowersBankDetailsCount();
			if (value != null) {
				count = value;
			}
		} else {
			if (type.equals("AFTER")) {
				borrowerPayments = borrowerPaymentsRepo.getListOfFdCreatedUsers(pageNo, pageSize);
				Integer value = borrowerPaymentsRepo.getListOfFdCreatedUsersCount();
				if (value != null) {
					count = value;
				}
			}
		}
		List<BorrowerNewBankDetailsResponseDto> listOfBorrowerDetails = new ArrayList<BorrowerNewBankDetailsResponseDto>();
		if (borrowerPayments != null && !borrowerPayments.isEmpty()) {
			borrowerPayments.stream().forEach(bankDetail -> {
				BorrowerNewBankDetailsResponseDto borrowerNewBankDetailsResponseDto = new BorrowerNewBankDetailsResponseDto();
				borrowerNewBankDetailsResponseDto.setUserId(bankDetail.getUserId());
				borrowerNewBankDetailsResponseDto.setAccountNumber(bankDetail.getAccountNumber());
				borrowerNewBankDetailsResponseDto.setIfsc(bankDetail.getIfsc());
				borrowerNewBankDetailsResponseDto.setBankChoosen(bankDetail.getBankChoosen().toString());
				borrowerNewBankDetailsResponseDto.setBankName(bankDetail.getBankName());
				borrowerNewBankDetailsResponseDto.setBranch(bankDetail.getBranch());
				borrowerNewBankDetailsResponseDto.setCity(bankDetail.getCity());
				borrowerNewBankDetailsResponseDto.setConsultancy(bankDetail.getConsultancy());
				borrowerNewBankDetailsResponseDto.setCountry(bankDetail.getCountry());
				borrowerNewBankDetailsResponseDto.setFdAmount(bankDetail.getFdAmount());
				borrowerNewBankDetailsResponseDto.setFundingType(bankDetail.getFundingType().toString());
				borrowerNewBankDetailsResponseDto.setLeadBy(bankDetail.getLeadBy());
				borrowerNewBankDetailsResponseDto.setUserName(bankDetail.getName());
				borrowerNewBankDetailsResponseDto.setRoi(bankDetail.getRoi());
				borrowerNewBankDetailsResponseDto.setStudentMobileNumber(bankDetail.getStudentMobileNumber());
				borrowerNewBankDetailsResponseDto.setUniversity(bankDetail.getUniversity());
				if (bankDetail.getFdCreated() != null) {
					borrowerNewBankDetailsResponseDto
							.setFdCreatedDate(expectedDateFormat.format(bankDetail.getFdCreated()));
				}
				if (bankDetail.getFeeInvoiceUrl() != null) {
					borrowerNewBankDetailsResponseDto.setFeeInvoice(bankDetail.getFeeInvoiceUrl());
				}
				borrowerNewBankDetailsResponseDto.setPaymentsCollection(bankDetail.getPaymentsCollection().toString());
				borrowerNewBankDetailsResponseDto.setLoanType(bankDetail.getLoanType().toString());
				listOfBorrowerDetails.add(borrowerNewBankDetailsResponseDto);

			});

			listOfBorrowerNewBankDetailsResponse.setCount(count);
			listOfBorrowerNewBankDetailsResponse.setBorrowerNewBankDetailsResponseDto(listOfBorrowerDetails);
		}
		return listOfBorrowerNewBankDetailsResponse;
	}

	@Override
	@Transactional
	public ListOfPaymentsDetailsResponseDto getPaymentsBasedonBankTypeAndStatus(
			ListOfPaymentsDetailsRequestDto listOfPaymentsDetailsRequestDto) {
		ListOfPaymentsDetailsResponseDto listOfPaymentsDetailsResponseDto = new ListOfPaymentsDetailsResponseDto();
		try {
			List<PaymentsInfo> paymentsInfo = new ArrayList<PaymentsInfo>();
			int pageNo = (listOfPaymentsDetailsRequestDto.getPageSize()
					* (listOfPaymentsDetailsRequestDto.getPageNo() - 1));
			int pageSize = listOfPaymentsDetailsRequestDto.getPageSize();

			List<BorrowerPaymentsImages> paymentsByStatus = borrowerPaymentsImagesRepo.listOfUsersByBankTypeAndStatus(
					listOfPaymentsDetailsRequestDto.getBankType(), listOfPaymentsDetailsRequestDto.getStatus(), pageNo,
					pageSize);

			if (paymentsByStatus != null && !paymentsByStatus.isEmpty()) {
				paymentsByStatus.stream().forEach(paymentsDetails -> {
					PaymentsInfo payments = new PaymentsInfo();
					payments.setPaymentId(paymentsDetails.getId());
					payments.setAmountPaid(paymentsDetails.getAmountPaid());
					payments.setUrl(paymentsDetails.getDocumentUploadUrl());
					payments.setAccountType(paymentsDetails.getAccountType().toString());
					if (paymentsDetails.getInvoiceUrl() != null && !paymentsDetails.getInvoiceUrl().isEmpty()) {
						payments.setInvoiceUrl(paymentsDetails.getInvoiceUrl());
					}
					BorrowerPayments borrowerPayments = borrowerPaymentsRepo
							.findById(paymentsDetails.getBorrowerPaymentsId()).get();
					if (borrowerPayments != null) {
						payments.setAccountNumber(borrowerPayments.getAccountNumber());
						payments.setIfsc(borrowerPayments.getIfsc());
						payments.setCity(borrowerPayments.getCity());
						payments.setBranch(borrowerPayments.getBranch());
						payments.setBankName(borrowerPayments.getBankName());
						payments.setUserName(borrowerPayments.getName());
						payments.setBankChoosen(borrowerPayments.getBankChoosen());
						payments.setLeadBy(borrowerPayments.getLeadBy());
						payments.setConsultancy(borrowerPayments.getConsultancy());
						payments.setCountry(borrowerPayments.getCountry());
						payments.setRoi(borrowerPayments.getRoi());
						payments.setFundingType(borrowerPayments.getFundingType().toString());
						payments.setUniversity(borrowerPayments.getUniversity());
						payments.setStudentMobileNumber(borrowerPayments.getStudentMobileNumber());
						payments.setFdAmount(borrowerPayments.getFdAmount());
						payments.setFdAmountFromSystem(borrowerPayments.getAmount());
						if (borrowerPayments.getFdCreated() != null) {
							payments.setFdcreatedDate(expectedDateFormat.format(borrowerPayments.getFdCreated()));
						}
						if (borrowerPayments.getFdValidity() != null) {
							payments.setValidityDate(expectedDateFormat.format(borrowerPayments.getFdValidity()));
						}
						if (borrowerPayments.getFeeInvoiceUrl() != null) {
							payments.setFeeInvoice(borrowerPayments.getFeeInvoiceUrl());
						}
						payments.setUserId(borrowerPayments.getUserId());
						payments.setPaymentsCollection(borrowerPayments.getPaymentsCollection().toString());

						payments.setUserId(borrowerPayments.getUserId());
					}
					paymentsInfo.add(payments);
				});
				listOfPaymentsDetailsResponseDto.setCount(borrowerPaymentsImagesRepo.usersByBankTypeAndStatusCount(
						listOfPaymentsDetailsRequestDto.getBankType(), listOfPaymentsDetailsRequestDto.getStatus()));
				listOfPaymentsDetailsResponseDto.setPaymentsInfo(paymentsInfo);
			}

		} catch (Exception e) {
			logger.info("Exception in getPaymentsBasedonBankTypeAndStatus method");
		}

		return listOfPaymentsDetailsResponseDto;

	}

	@Override
	@Transactional
	public PaymentUploadedResponseDto closingFd(FdCloseingRequestDto fdCloseingRequestDto) {
		PaymentUploadedResponseDto paymentUploadedResponseDto = new PaymentUploadedResponseDto();
		try {
			BorrowerPayments borrowerPayments = borrowerPaymentsRepo
					.findById(fdCloseingRequestDto.getBorrowerPaymentId()).get();
			if (borrowerPayments != null) {
				borrowerPayments.setInterestEarnedOnFd(fdCloseingRequestDto.getTotalInterestOnFd());
				borrowerPayments.setFdStatus(BorrowerPayments.FdStatus.CLOSED);
				borrowerPayments.setFdClosedDate(expectedDateFormat.parse(fdCloseingRequestDto.getFdClosedDate()));
				borrowerPayments.setAmountReturnedToRepayment(fdCloseingRequestDto.getAmountReturnedToRepayment());
				borrowerPayments.setAmountReturnedToAnother(fdCloseingRequestDto.getAmountReturnedToAnother());
				borrowerPayments.setComments(fdCloseingRequestDto.getComments());
				borrowerPaymentsRepo.save(borrowerPayments);
				BorrowerRepaymentsHistory borrowerRepaymentsHistory = new BorrowerRepaymentsHistory();
				borrowerRepaymentsHistory.setBorrowerPaymentsId(borrowerPayments.getId());
				borrowerRepaymentsHistory
						.setAmountReturnedToRepayment(fdCloseingRequestDto.getAmountReturnedToRepayment());
				borrowerRepaymentsHistory.setCreatedOn(new Date());
				borrowerRepaymentsHistoryRepo.save(borrowerRepaymentsHistory);
				paymentUploadedResponseDto.setStatus("updated");
				if (fdCloseingRequestDto.getTotalInterestOnFd() > 0.0) {
					String outputFileName = borrowerPayments.getUserId() + borrowerPayments.getId() + ".pdf";
					String generatedPdf = "";
					TemplateContext templateContext = new TemplateContext();
					templateContext.put("type", "Fee Invoice");
					templateContext.put("financialYear", getFinancialYear());
					templateContext.put("randomNumber", fourDigitsRandomNumber());
					templateContext.put("currentDate", currentDate());
					templateContext.put("borrowerId", "BR" + borrowerPayments.getUserId());
					templateContext.put("borrowerName", borrowerPayments.getName());
					templateContext.put("feePaid", fdCloseingRequestDto.getTotalInterestOnFd());
					templateContext.put("feePaidGst",
							Math.round(fdCloseingRequestDto.getTotalInterestOnFd() * 9 / 100));
					double centralAndState = Math.round((fdCloseingRequestDto.getTotalInterestOnFd() * 9 / 100) * 2);
					templateContext.put("totalGst", centralAndState);
					templateContext.put("feeWithGst", fdCloseingRequestDto.getTotalInterestOnFd() + centralAndState);
					templateContext.put("totalGstInNumber", numberToWord((int) centralAndState));
					templateContext.put("feeWithGstInNumber",
							numberToWord((int) (fdCloseingRequestDto.getTotalInterestOnFd() + centralAndState)));

					generatedPdf = pdfEngine.generatePdf("agreement/invoicePayment.xml", templateContext,
							outputFileName);

					FileRequest fileRequest = new FileRequest();
					try {
						fileRequest.setInputStream(new FileInputStream(generatedPdf));
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					fileRequest.setFileName(outputFileName);
					fileRequest.setFileType(FileType.FeeInvoice);
					fileRequest.setFilePrifix(FileType.FeeInvoice.name());
					try {
						fileManagementService.putFile(fileRequest);
						FileResponse putFileResponse = fileManagementService.getFile(fileRequest);
						String download = putFileResponse.getDownloadUrl();
						String[] url = download.split("\\?");
						paymentUploadedResponseDto.setInvoiceUrl(url[0].toString());
						borrowerPayments.setFeeInvoiceUrl((url[0].toString()));
						borrowerPaymentsRepo.save(borrowerPayments);

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
					}
					File file = new File(generatedPdf);
					boolean fileDeleted = file.delete();

				}
			}
		} catch (Exception e) {
			logger.info("Exception in closingFd method");
		}
		return paymentUploadedResponseDto;

	}

	@Override
	@Transactional
	public ListOfBorrowerNewBankDetailsResponse fdStatusBasedSearch(
			ListOfPaymentsDetailsRequestDto listOfPaymentsDetailsRequestDto) {
		ListOfBorrowerNewBankDetailsResponse listOfBorrowerNewBankDetailsResponse = new ListOfBorrowerNewBankDetailsResponse();
		int pageNo = (listOfPaymentsDetailsRequestDto.getPageSize()
				* (listOfPaymentsDetailsRequestDto.getPageNo() - 1));
		List<BorrowerPayments> borrowerPayments = null;
		List<Object[]> runningAndClosed = null;
		int count = 0;
		if (listOfPaymentsDetailsRequestDto.getStatus().equals("ALL")) {
			borrowerPayments = borrowerPaymentsRepo.getListOfNewBorrowersBankDetails(pageNo,
					listOfPaymentsDetailsRequestDto.getPageSize());
			Integer value = borrowerPaymentsRepo.getListOfNewBorrowersBankDetailsCount();
			if (value != null) {
				count = value;
			}
			listOfBorrowerNewBankDetailsResponse.setBorrowerNewBankDetailsResponseDto(
					getAllAndClosedFdInformation(borrowerPayments, listOfPaymentsDetailsRequestDto.getStatus()));
			listOfBorrowerNewBankDetailsResponse.setCount(count);
		} else if (listOfPaymentsDetailsRequestDto.getStatus().equals("CLOSED")) {
			borrowerPayments = borrowerPaymentsRepo.getListOfFdClosed(pageNo,
					listOfPaymentsDetailsRequestDto.getPageSize());
			Integer value = borrowerPaymentsRepo.getListOfFdClosedCount();
			if (value != null) {
				count = value;
			}
			listOfBorrowerNewBankDetailsResponse.setBorrowerNewBankDetailsResponseDto(
					getAllAndClosedFdInformation(borrowerPayments, listOfPaymentsDetailsRequestDto.getStatus()));
			listOfBorrowerNewBankDetailsResponse.setCount(count);
		} else if (listOfPaymentsDetailsRequestDto.getStatus().equals("RUNNING")) {
			runningAndClosed = borrowerPaymentsRepo.listOfRunningFds(pageNo,
					listOfPaymentsDetailsRequestDto.getPageSize());
			Integer value = borrowerPaymentsRepo.listOfRunningFdsCount();
			if (value != null) {
				count = value;
			}
			listOfBorrowerNewBankDetailsResponse
					.setBorrowerNewBankDetailsResponseDto(getFdRunningAndNegativeFdInformation(runningAndClosed,
							listOfPaymentsDetailsRequestDto.getStatus()));
			listOfBorrowerNewBankDetailsResponse.setCount(count);
		} else {
			runningAndClosed = borrowerPaymentsRepo.listOfNegativeFds(pageNo,
					listOfPaymentsDetailsRequestDto.getPageSize());
			Integer value = borrowerPaymentsRepo.listOfNegativeFdsCount();
			if (value != null) {
				count = value;
			}
			listOfBorrowerNewBankDetailsResponse
					.setBorrowerNewBankDetailsResponseDto(getFdRunningAndNegativeFdInformation(runningAndClosed,
							listOfPaymentsDetailsRequestDto.getStatus()));
			listOfBorrowerNewBankDetailsResponse.setCount(count);
		}

		return listOfBorrowerNewBankDetailsResponse;

	}

	public List<BorrowerNewBankDetailsResponseDto> getAllAndClosedFdInformation(List<BorrowerPayments> borrowerPayments,
			String status) {
		List<BorrowerNewBankDetailsResponseDto> listOfBorrowerDetails = new ArrayList<BorrowerNewBankDetailsResponseDto>();
		if (borrowerPayments != null && !borrowerPayments.isEmpty()) {
			borrowerPayments.stream().forEach(bankDetail -> {
				BorrowerNewBankDetailsResponseDto borrowerNewBankDetailsResponseDto = new BorrowerNewBankDetailsResponseDto();
				borrowerNewBankDetailsResponseDto.setPaymentId(bankDetail.getId());
				borrowerNewBankDetailsResponseDto.setUserId(bankDetail.getUserId());
				borrowerNewBankDetailsResponseDto.setAccountNumber(bankDetail.getAccountNumber());
				borrowerNewBankDetailsResponseDto.setIfsc(bankDetail.getIfsc());
				borrowerNewBankDetailsResponseDto.setBankChoosen(bankDetail.getBankChoosen().toString());
				borrowerNewBankDetailsResponseDto.setBankName(bankDetail.getBankName());
				borrowerNewBankDetailsResponseDto.setBranch(bankDetail.getBranch());
				borrowerNewBankDetailsResponseDto.setCity(bankDetail.getCity());
				borrowerNewBankDetailsResponseDto.setConsultancy(bankDetail.getConsultancy());
				borrowerNewBankDetailsResponseDto.setCountry(bankDetail.getCountry());
				borrowerNewBankDetailsResponseDto.setFdAmount(bankDetail.getFdAmount());
				borrowerNewBankDetailsResponseDto.setFundingType(bankDetail.getFundingType().toString());
				borrowerNewBankDetailsResponseDto.setLeadBy(bankDetail.getLeadBy());
				borrowerNewBankDetailsResponseDto.setUserName(bankDetail.getName());
				borrowerNewBankDetailsResponseDto.setRoi(bankDetail.getRoi());
				borrowerNewBankDetailsResponseDto.setStudentMobileNumber(bankDetail.getStudentMobileNumber());
				borrowerNewBankDetailsResponseDto.setUniversity(bankDetail.getUniversity());
				if (bankDetail.getFdCreated() != null) {
					borrowerNewBankDetailsResponseDto
							.setFdCreatedDate(expectedDateFormat.format(bankDetail.getFdCreated()));
				}
				if (bankDetail.getFdValidity() != null) {
					borrowerNewBankDetailsResponseDto
							.setFdValidityDate(expectedDateFormat.format(bankDetail.getFdValidity()));
				}
				if (bankDetail.getFdClosedDate() != null) {
					borrowerNewBankDetailsResponseDto
							.setFdClosedDate(expectedDateFormat.format(bankDetail.getFdClosedDate()));
				}
				if (status.equalsIgnoreCase("ALL")) {
					borrowerNewBankDetailsResponseDto.setStatus(bankDetail.getFdStatus().toString());
				} else {
					borrowerNewBankDetailsResponseDto.setStatus(status);
				}
				User user = userRepo.findById(bankDetail.getUserId()).get();
				if (user != null) {
					borrowerNewBankDetailsResponseDto.setRegisteredMobileNumber(user.getMobileNumber());
				}
				if (bankDetail.getFeeInvoiceUrl() != null && bankDetail.getFeeInvoiceUrl().length() > 0) {
					borrowerNewBankDetailsResponseDto.setFeeInvoice(bankDetail.getFeeInvoiceUrl());
				} else {
					borrowerNewBankDetailsResponseDto.setFeeInvoice(null);
				}
				borrowerNewBankDetailsResponseDto.setLoanType(bankDetail.getLoanType().toString());
				borrowerNewBankDetailsResponseDto.setPaymentsCollection(bankDetail.getPaymentsCollection().toString());
				listOfBorrowerDetails.add(borrowerNewBankDetailsResponseDto);

			});
		}
		return listOfBorrowerDetails;
	}

	public List<BorrowerNewBankDetailsResponseDto> getFdRunningAndNegativeFdInformation(List<Object[]> payments,
			String status) {
		List<BorrowerNewBankDetailsResponseDto> listOfBorrowerDetails = new ArrayList<BorrowerNewBankDetailsResponseDto>();
		if (payments != null && !payments.isEmpty()) {
			payments.forEach(e -> {
				BorrowerNewBankDetailsResponseDto borrowerNewBankDetailsResponseDto = new BorrowerNewBankDetailsResponseDto();
				borrowerNewBankDetailsResponseDto.setPaymentId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
				int userId = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
				borrowerNewBankDetailsResponseDto.setUserId(userId);
				borrowerNewBankDetailsResponseDto.setAccountNumber(e[8] == null ? "null" : e[8].toString());
				borrowerNewBankDetailsResponseDto.setIfsc(e[9] == null ? "null" : e[9].toString());
				borrowerNewBankDetailsResponseDto.setBankChoosen(e[31] == null ? "null" : e[31].toString());
				borrowerNewBankDetailsResponseDto.setBankName(e[14] == null ? "null" : e[14].toString());
				borrowerNewBankDetailsResponseDto.setBranch(e[13] == null ? "null" : e[13].toString());
				borrowerNewBankDetailsResponseDto.setCity(e[12] == null ? "null" : e[12].toString());
				borrowerNewBankDetailsResponseDto.setConsultancy(e[16] == null ? "null" : e[16].toString());
				borrowerNewBankDetailsResponseDto.setCountry(e[17] == null ? "null" : e[17].toString());
				borrowerNewBankDetailsResponseDto
						.setFdAmount(Double.parseDouble(e[4] == null ? "0.0" : e[4].toString()));
				borrowerNewBankDetailsResponseDto.setFundingType(e[20] == null ? "null" : e[20].toString());
				borrowerNewBankDetailsResponseDto.setLeadBy(e[15] == null ? "null" : e[15].toString());
				borrowerNewBankDetailsResponseDto.setUserName(e[10] == null ? "null" : e[10].toString());
				borrowerNewBankDetailsResponseDto.setRoi(Double.parseDouble(e[21] == null ? "0.0" : e[21].toString()));
				borrowerNewBankDetailsResponseDto.setStudentMobileNumber(e[19] == null ? "null" : e[19].toString());
				borrowerNewBankDetailsResponseDto.setUniversity(e[18] == null ? "null" : e[18].toString());
				int days = Integer.parseInt(e[32] == null ? "0" : e[32].toString());
				if (days < 0) {
					borrowerNewBankDetailsResponseDto.setStatus("NEGATIVE");
				} else {
					borrowerNewBankDetailsResponseDto.setStatus(status);
				}
				borrowerNewBankDetailsResponseDto.setDays(days);
				borrowerNewBankDetailsResponseDto
						.setFdAmountFromSystem(Double.parseDouble(e[22] == null ? "0.0" : e[22].toString()));
				String createdDate = e[24] == null ? "null" : e[24].toString();
				if (createdDate != null) {
					borrowerNewBankDetailsResponseDto.setFdCreatedDate(createdDate);
				}
				String validityDate = e[25] == null ? "null" : e[25].toString();
				if (validityDate != null) {
					borrowerNewBankDetailsResponseDto.setFdValidityDate(validityDate);
				}
				String closedDate = e[30] == null ? "null" : e[30].toString();
				if (closedDate != null) {
					borrowerNewBankDetailsResponseDto.setFdClosedDate(closedDate);
				}
				borrowerNewBankDetailsResponseDto.setPaymentsCollection(e[33] == null ? "0" : e[33].toString());
				borrowerNewBankDetailsResponseDto.setLoanType(e[34] == null ? "0" : e[34].toString());
				User user = userRepo.findById(userId).get();
				if (user != null) {
					borrowerNewBankDetailsResponseDto.setRegisteredMobileNumber(user.getMobileNumber());
				}
				listOfBorrowerDetails.add(borrowerNewBankDetailsResponseDto);

			});
		}

		return listOfBorrowerDetails;

	}

	@Override
	@Transactional
	public List<BorrowerListOfPaymentsUploadedResponse> getUserPayments(int userId) {
		List<BorrowerListOfPaymentsUploadedResponse> listOfPayments = new ArrayList<BorrowerListOfPaymentsUploadedResponse>();
		BorrowerPayments borrowerPayments = borrowerPaymentsRepo.findByUserId(userId);
		if (borrowerPayments != null) {
			List<BorrowerPaymentsImages> borrowerPaymentsImages = borrowerPaymentsImagesRepo
					.findByBorrowerPaymentsIdOrderByIdDesc(borrowerPayments.getId());
			if (borrowerPaymentsImages != null && !borrowerPaymentsImages.isEmpty()) {
				borrowerPaymentsImages.stream().forEach(payments -> {
					BorrowerListOfPaymentsUploadedResponse borrowerListOfPaymentsUploaded = new BorrowerListOfPaymentsUploadedResponse();
					borrowerListOfPaymentsUploaded.setAccountType(payments.getAccountType().toString());
					borrowerListOfPaymentsUploaded.setAmountPaid(payments.getAmountPaid());
					borrowerListOfPaymentsUploaded.setUrl(payments.getDocumentUploadUrl());
					borrowerListOfPaymentsUploaded.setDocumentStatus(payments.getDocumentStatus().toString());
					if (payments.getApprovedOn() != null) {
						borrowerListOfPaymentsUploaded
								.setApprovedDate(expectedDateFormat.format(payments.getApprovedOn()));
					}
					listOfPayments.add(borrowerListOfPaymentsUploaded);
				});
			}

		}

		return listOfPayments;

	}

	@Override
	@Transactional
	public BorrowerNewBankDetailsResponseDto searchByUserId(int userId) {
		BorrowerNewBankDetailsResponseDto borrowerNewBankDetailsResponseDto = new BorrowerNewBankDetailsResponseDto();
		// try {
		BorrowerPayments borrowerPayments = borrowerPaymentsRepo.findByUserId(userId);
		if (borrowerPayments != null) {
			borrowerNewBankDetailsResponseDto.setUserId(userId);
			borrowerNewBankDetailsResponseDto.setAccountNumber(borrowerPayments.getAccountNumber());
			borrowerNewBankDetailsResponseDto.setIfsc(borrowerPayments.getIfsc());
			borrowerNewBankDetailsResponseDto.setCity(borrowerPayments.getCity());
			borrowerNewBankDetailsResponseDto.setBranch(borrowerPayments.getBranch());
			borrowerNewBankDetailsResponseDto.setBankName(borrowerPayments.getBankName());
			borrowerNewBankDetailsResponseDto.setUserName(borrowerPayments.getName());
			borrowerNewBankDetailsResponseDto.setBankChoosen(borrowerPayments.getBankChoosen());
			borrowerNewBankDetailsResponseDto.setLeadBy(borrowerPayments.getLeadBy());
			borrowerNewBankDetailsResponseDto.setConsultancy(borrowerPayments.getConsultancy());
			borrowerNewBankDetailsResponseDto.setRoi(borrowerPayments.getRoi());
			borrowerNewBankDetailsResponseDto.setFundingType(borrowerPayments.getFundingType().toString());
			borrowerNewBankDetailsResponseDto.setCountry(borrowerPayments.getCountry());
			borrowerNewBankDetailsResponseDto.setUniversity(borrowerPayments.getUniversity());
			borrowerNewBankDetailsResponseDto.setStudentMobileNumber(borrowerPayments.getStudentMobileNumber());
			borrowerNewBankDetailsResponseDto.setFdAmount(borrowerPayments.getFdAmount());
			if (borrowerPayments.getFdClosedDate() != null) {
				borrowerNewBankDetailsResponseDto
						.setFdClosedDate(expectedDateFormat.format(borrowerPayments.getFdClosedDate()));
			}
			borrowerNewBankDetailsResponseDto.setPaymentId(borrowerPayments.getId());

			borrowerNewBankDetailsResponseDto.setFdAmountFromSystem(borrowerPayments.getAmount());
			if (borrowerPayments.getFdCreated() != null) {
				borrowerNewBankDetailsResponseDto
						.setFdCreatedDate(expectedDateFormat.format(borrowerPayments.getFdCreated()));
			}
			if (borrowerPayments.getFdValidity() != null) {

				LocalDate givenDate = LocalDate.parse(borrowerPayments.getFdValidity().toString());

				LocalDate currentDate = LocalDate.now();

				long differenceInDays = ChronoUnit.DAYS.between(currentDate, givenDate);

				borrowerNewBankDetailsResponseDto
						.setFdValidityDate(expectedDateFormat.format(borrowerPayments.getFdValidity()));
				borrowerNewBankDetailsResponseDto.setDays((int) differenceInDays);
			} else {
				borrowerNewBankDetailsResponseDto.setDays(0);
			}
			User user = userRepo.findById(userId).get();
			if (user != null) {
				borrowerNewBankDetailsResponseDto.setRegisteredMobileNumber(user.getMobileNumber());
			}

			borrowerNewBankDetailsResponseDto.setStatus(borrowerPayments.getFdStatus().toString());
			if (borrowerPayments.getFeeInvoiceUrl() != null) {
				borrowerNewBankDetailsResponseDto.setFeeInvoice(borrowerPayments.getFeeInvoiceUrl());
			}
			borrowerNewBankDetailsResponseDto
					.setPaymentsCollection(borrowerPayments.getPaymentsCollection().toString());
			borrowerNewBankDetailsResponseDto.setLoanType(borrowerPayments.getLoanType().toString());
		}
		/*
		 * } catch (Exception e) { logger.info("Exception in searchByUserId method"); }
		 */
		return borrowerNewBankDetailsResponseDto;

	}

	@Override
	@Transactional
	public FdStatisticsResponseDto fdStatistics(FdStatisticsRequestDto fdStatisticsRequestDto) {
		FdStatisticsResponseDto fdStatisticsResponseDto = new FdStatisticsResponseDto();
		if (fdStatisticsRequestDto.getType() != null && fdStatisticsRequestDto.getType().equalsIgnoreCase("ALL")) {
			List<BorrowerPayments> borrowerPayments = borrowerPaymentsRepo.findByFdCreatedIsNotNull();
			if (borrowerPayments != null && !borrowerPayments.isEmpty()) {
				fdStatisticsResponseDto.setNoOfFdsDone((int) borrowerPayments.stream().count());
				fdStatisticsResponseDto.setValueOfFd(BigDecimal
						.valueOf(borrowerPayments.stream().mapToDouble(a -> a.getFdAmount()).sum()).toBigInteger());
				fdStatisticsResponseDto.setNoOfActiveFds((int) borrowerPayments.stream()
						.filter(b -> !b.getFdStatus().equals(BorrowerPayments.FdStatus.CLOSED)).count());
				fdStatisticsResponseDto.setNoOfActiveFdsAmount(BigDecimal.valueOf(
						borrowerPayments.stream().filter(a -> !a.getFdStatus().equals(BorrowerPayments.FdStatus.CLOSED))
								.mapToDouble(b -> b.getFdAmount()).sum())
						.toBigInteger());
				fdStatisticsResponseDto.setTotalFdClosedInterest(BigDecimal.valueOf(
						borrowerPayments.stream().filter(a -> a.getFdStatus().equals(BorrowerPayments.FdStatus.CLOSED))
								.mapToDouble(b -> b.getInterestEarnedOnFd()).sum())
						.toBigInteger());

				List<BorrowerPaymentsImages> borrowerPaymentsImages = borrowerPaymentsImagesRepo
						.findByDocumentStatusEquals(BorrowerPaymentsImages.DocumentStatus.APPROVED);
				if (borrowerPaymentsImages != null && !borrowerPaymentsImages.isEmpty()) {
					fdStatisticsResponseDto.setAmountReceivedToHdfc(BigDecimal.valueOf(borrowerPaymentsImages.stream()
							.filter(a -> a.getAccountType().equals(BorrowerPaymentsImages.AccountType.HDFC))
							.mapToDouble(b -> b.getAmountPaid()).sum()).toBigInteger());
					fdStatisticsResponseDto.setAmountReceivedToIcici(BigDecimal.valueOf(borrowerPaymentsImages.stream()
							.filter(a -> a.getAccountType().equals(BorrowerPaymentsImages.AccountType.ICICI))
							.mapToDouble(b -> b.getAmountPaid()).sum()).toBigInteger());
				}

			}
		} else {

			List<BorrowerPayments> borrowerPayments = borrowerPaymentsRepo
					.findByFdCreatedGreaterThanEqualAndFdCreatedLessThanEqual(fdStatisticsRequestDto.getStartDate(),
							fdStatisticsRequestDto.getEndDate());
			if (borrowerPayments != null && !borrowerPayments.isEmpty()) {
				fdStatisticsResponseDto.setNoOfFdsDone((int) borrowerPayments.stream().count());
				fdStatisticsResponseDto.setValueOfFd(BigDecimal
						.valueOf(borrowerPayments.stream().mapToDouble(a -> a.getFdAmount()).sum()).toBigInteger());
				fdStatisticsResponseDto.setNoOfActiveFds((int) borrowerPayments.stream()
						.filter(b -> !b.getFdStatus().equals(BorrowerPayments.FdStatus.CLOSED)).count());
				fdStatisticsResponseDto.setNoOfActiveFdsAmount(BigDecimal.valueOf(
						borrowerPayments.stream().filter(a -> !a.getFdStatus().equals(BorrowerPayments.FdStatus.CLOSED))
								.mapToDouble(b -> b.getFdAmount()).sum())
						.toBigInteger());
				fdStatisticsResponseDto.setTotalFdClosedInterest(BigDecimal.valueOf(
						borrowerPayments.stream().filter(a -> a.getFdStatus().equals(BorrowerPayments.FdStatus.CLOSED))
								.mapToDouble(b -> b.getInterestEarnedOnFd()).sum())
						.toBigInteger());
				double amountToHdfc = 0.0;
				double amountToIcici = 0.0;
				for (BorrowerPayments borrowersPayments : borrowerPayments) {
					List<BorrowerPaymentsImages> borrowerPaymentsImages = borrowerPaymentsImagesRepo
							.findByBorrowerPaymentsIdOrderByIdDesc(borrowersPayments.getId());

					if (borrowerPaymentsImages != null && !borrowerPaymentsImages.isEmpty()) {
						amountToHdfc = amountToHdfc + borrowerPaymentsImages.stream()
								.filter(a -> a.getAccountType().equals(BorrowerPaymentsImages.AccountType.HDFC))
								.mapToDouble(b -> b.getAmountPaid()).sum();
					}
					if (borrowerPaymentsImages != null && !borrowerPaymentsImages.isEmpty()) {
						amountToIcici = amountToIcici + borrowerPaymentsImages.stream()
								.filter(a -> a.getAccountType().equals(BorrowerPaymentsImages.AccountType.ICICI))
								.mapToDouble(b -> b.getAmountPaid()).sum();
					}

				}
				fdStatisticsResponseDto.setAmountReceivedToHdfc(BigDecimal.valueOf(amountToHdfc).toBigInteger());
				fdStatisticsResponseDto.setAmountReceivedToIcici(BigDecimal.valueOf(amountToIcici).toBigInteger());

			}
		}
		return fdStatisticsResponseDto;

	}

	@Override
	@Transactional
	public List<InvoiceResponceDto> generatedInvoives(InvoiceRequestDto invoiceRequestDto) {
		List<InvoiceResponceDto> listOfInvoices = new ArrayList<InvoiceResponceDto>();
		List<BorrowerPaymentsImages> borrowerPaymentsImages = null;

		if (invoiceRequestDto.getType() != null && invoiceRequestDto.getType().equalsIgnoreCase("BULK")) {
			List<BorrowerPaymentsImages> borrowerPayments = borrowerPaymentsImagesRepo.findByInvoiceUrlNotNull();
			if (borrowerPayments != null && !borrowerPayments.isEmpty()) {
				borrowerPaymentsImages = borrowerPayments;
			}
		} else {
			List<BorrowerPaymentsImages> borrowerPayments = borrowerPaymentsImagesRepo
					.findByInvoiceUrlNotNullAndCreatedOnBetween(invoiceRequestDto.getStartDate(),
							invoiceRequestDto.getEndDate());
			if (borrowerPayments != null && !borrowerPayments.isEmpty()) {
				borrowerPaymentsImages = borrowerPayments;
			}
		}
		if (borrowerPaymentsImages != null && !borrowerPaymentsImages.isEmpty()) {
			borrowerPaymentsImages.parallelStream().forEach(borrowerPayments -> {
				InvoiceResponceDto invoiceResponceDto = new InvoiceResponceDto();
				invoiceResponceDto.setInvoice(borrowerPayments.getInvoiceUrl());
				listOfInvoices.add(invoiceResponceDto);
			});
		}

		return listOfInvoices;

	}

	@Override
	@Transactional
	public MonthWiseBorrowersFdResponseDto monthlyFdInformation(
			MonthWiseBorrowersFdRequestDto monthWiseBorrowersFdRequestDto) {
		MonthWiseBorrowersFdResponseDto monthWiseBorrowersFdResponseDto = new MonthWiseBorrowersFdResponseDto();
		List<FdInformation> listOfFdInformation = new ArrayList<FdInformation>();

		List<LenderCmsPayments> borrowerPayments = lenderCmsPaymentsRepo.loanExecutedFromSystem(
				monthWiseBorrowersFdRequestDto.getStartDate(), monthWiseBorrowersFdRequestDto.getEndDate());

		if (borrowerPayments != null && !borrowerPayments.isEmpty()) {

			//logger.info("borrowerPayments" + borrowerPayments.size());
			for (LenderCmsPayments loan : borrowerPayments) {

			//	logger.info("borrowerPayments loan");
			//	logger.info("borrowerPayments size" + borrowerPayments.size());

				// borrowerPayments.parallelStream().forEach(loan -> {
				FdInformation fdInformation = new FdInformation();
				fdInformation.setFdCreatedDate(loan.getPaymentDate());
				User user = userRepo.findById(loan.getUserId()).get();
				if (user != null) {
					if (user.getPersonalDetails() != null) {
						fdInformation.setPanNumber(user.getPersonalDetails().getPanNumber());

						if (user.getPersonalDetails().getDob() != null) {
							fdInformation.setDob(expectedDateFormat.format(user.getPersonalDetails().getDob()));
						}
						if (user.getPersonalDetails().getGender().equalsIgnoreCase("Male")) {
							fdInformation.setGender(2);
						} else {
							fdInformation.setGender(1);
						}
						fdInformation.setAddress(user.getPersonalDetails().getAddress());
					}

				}
				BorrowerPayments fdDetails = borrowerPaymentsRepo.findByUserIdWithLimit(loan.getUserId());
				fdInformation.setPincode(user.getPinCode());
				fdInformation.setRegisteredMobileNumber(user.getMobileNumber());
				fdInformation.setRegisteredDate(expectedDateFormat.format(user.getRegisteredOn()));
				fdInformation.setName(fdDetails.getName());
				fdInformation.setBorrowerId("BR" + fdDetails.getUserId());
				fdInformation.setTotalFdAmount(BigDecimal.valueOf(fdDetails.getFdAmount()).toBigInteger());
				fdInformation.setFdFromSystem(BigDecimal.valueOf(fdDetails.getAmount()).toBigInteger());
				if (fdDetails.getFdClosedDate() != null) {
					fdInformation.setFdClosedDate(expectedDateFormat.format(fdDetails.getFdClosedDate()));
				} else {
					fdInformation.setFdClosedDate("NA");
				}
				listOfFdInformation.add(fdInformation);
				// });

			//	logger.info("borrowerPayments" + borrowerPayments.size());

			}
		}
		monthWiseBorrowersFdResponseDto.setFdInformation(listOfFdInformation);
		monthWiseBorrowersFdResponseDto.setMonthDownloadUrl(creatingExcelSheetForFd(listOfFdInformation));
		return monthWiseBorrowersFdResponseDto;

	}

	public String creatingExcelSheetForFd(List<FdInformation> listOfFdInformation) {

		StringBuilder sbf = new StringBuilder("Fd-details"); // for interest

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("S.No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Customer Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Borrower Id");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Loan Disbursed");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("DOB");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("Gender");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Pan");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Reg Date");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Address");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("State");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Pincode");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Contact No");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Fd Closed Date");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}
		double totalAmount = 0.0;
		int serialCount = 0;
		if (listOfFdInformation != null && !listOfFdInformation.isEmpty()) {
			for (FdInformation lenderPricipalReturn : listOfFdInformation) {
				serialCount = serialCount + 1;
				Row row1 = spreadSheet.createRow(++rowCount);
				Cell cell1 = row1.createCell(0);
				cell1.setCellValue(serialCount);

				cell1 = row1.createCell(1);
				cell1.setCellValue(lenderPricipalReturn.getFdCreatedDate() == null ? "NA"
						: lenderPricipalReturn.getFdCreatedDate().toString());

				cell1 = row1.createCell(2);
				cell1.setCellValue(lenderPricipalReturn.getName() == null ? "NA" : lenderPricipalReturn.getName());

				cell1 = row1.createCell(3);
				cell1.setCellValue(
						lenderPricipalReturn.getBorrowerId() == null ? "NA" : lenderPricipalReturn.getBorrowerId());

				cell1 = row1.createCell(4);
				cell1.setCellValue(lenderPricipalReturn.getFdFromSystem() == null ? 0.0
						: lenderPricipalReturn.getFdFromSystem().doubleValue());

				cell1 = row1.createCell(5);
				cell1.setCellValue(lenderPricipalReturn.getDob() == null ? "NA" : lenderPricipalReturn.getDob());

				cell1 = row1.createCell(6);
				cell1.setCellValue(lenderPricipalReturn.getGender());

				cell1 = row1.createCell(7);
				cell1.setCellValue(
						lenderPricipalReturn.getPanNumber() == null ? "NA" : lenderPricipalReturn.getPanNumber());

				cell1 = row1.createCell(8);
				cell1.setCellValue(lenderPricipalReturn.getRegisteredDate() == null ? "NA"
						: lenderPricipalReturn.getRegisteredDate());

				cell1 = row1.createCell(9);
				cell1.setCellValue(
						lenderPricipalReturn.getAddress() == null ? "NA" : lenderPricipalReturn.getAddress());

				cell1 = row1.createCell(10);
				cell1.setCellValue(
						lenderPricipalReturn.getStateCode() == null ? "NA" : lenderPricipalReturn.getStateCode());

				cell1 = row1.createCell(11);
				cell1.setCellValue(
						lenderPricipalReturn.getPincode() == null ? "NA" : lenderPricipalReturn.getPincode());

				cell1 = row1.createCell(12);
				cell1.setCellValue(lenderPricipalReturn.getRegisteredMobileNumber() == null ? "NA"
						: lenderPricipalReturn.getRegisteredMobileNumber());

				cell1 = row1.createCell(13);
				cell1.setCellValue(
						lenderPricipalReturn.getFdClosedDate() == null ? "NA" : lenderPricipalReturn.getFdClosedDate());

			}
		}

		File f = new File(iciciReportsManualFolder + sbf.toString() + ".xls");
		String downloadUrl = null;
		try {
			outFile = new FileOutputStream(f);

			workBook.write(outFile);

			workBook.close();
			outFile.close();

			FileInputStream targetStream = new FileInputStream(f);

			FileRequest fileRequest = new FileRequest();

			fileRequest.setFilePrifix(FileType.FdDetails.name());
			fileRequest.setInputStream(targetStream);
			fileRequest.setFileName(f.getName());
			fileManagementService.putFile(fileRequest);

			FileResponse getFile = fileManagementService.getFile(fileRequest);
			downloadUrl = getFile.getDownloadUrl();
		} catch (Exception e) {
			logger.info(e.getMessage());
		}

		return downloadUrl;
	}

	@Override
	@Transactional
	public SearchTypes listUniqueSearchNames(FdSearchDownloadRequest fdSearchDownloadRequest) {
		SearchTypes searchTypes = new SearchTypes();
		List<String> types = null;

		if (fdSearchDownloadRequest.getInputType().equalsIgnoreCase("fundingType")) {
			types = new ArrayList<String>();
			types.add("FD");
			types.add("SAVINGS");
			types.add("DAYS");
		} else if (fdSearchDownloadRequest.getInputType().equalsIgnoreCase("bank")) {
			List<String> banksNames = borrowerPaymentsRepo.listOfBanks();
			if (banksNames != null && !banksNames.isEmpty()) {
				types = new ArrayList<String>();
				types = banksNames;
			}

		} else if (fdSearchDownloadRequest.getInputType().equalsIgnoreCase("consultancy")) {
			List<String> consultancyNames = borrowerPaymentsRepo.listOfConsultancy();
			if (consultancyNames != null && !consultancyNames.isEmpty()) {
				types = new ArrayList<String>();
				types = consultancyNames;
			}
		}
		searchTypes.setSearchTypes(types);
		return searchTypes;

	}

	@Override
	@Transactional
	public FdSearchDownloadResponse searchByType(FdSearchDownloadRequest fdSearchDownloadRequest) {
		FdSearchDownloadResponse fdSearchDownloadResponse = null;
		if (fdSearchDownloadRequest.getInputType().equalsIgnoreCase("fundingType")) {
			List<BorrowerPayments> borrowerPayments = borrowerPaymentsRepo
					.findByFundingType(BorrowerPayments.FundingType.valueOf(fdSearchDownloadRequest.getInputSubType()));
			if (borrowerPayments != null && !borrowerPayments.isEmpty()) {
				fdSearchDownloadResponse = downloadExcelForFdsearch(borrowerPayments);
			}

		} else if (fdSearchDownloadRequest.getInputType().equalsIgnoreCase("bank")) {
			List<BorrowerPayments> borrowerPayments = borrowerPaymentsRepo
					.findByBankName(fdSearchDownloadRequest.getInputSubType());
			if (borrowerPayments != null && !borrowerPayments.isEmpty()) {
				fdSearchDownloadResponse = downloadExcelForFdsearch(borrowerPayments);
			}

		} else if (fdSearchDownloadRequest.getInputType().equalsIgnoreCase("consultancy")) {
			List<BorrowerPayments> borrowerPayments = borrowerPaymentsRepo
					.findByConsultancy(fdSearchDownloadRequest.getInputSubType());
			if (borrowerPayments != null && !borrowerPayments.isEmpty()) {
				fdSearchDownloadResponse = downloadExcelForFdsearch(borrowerPayments);
			}
		}

		return fdSearchDownloadResponse;

	}

	public FdSearchDownloadResponse downloadExcelForFdsearch(List<BorrowerPayments> borrowerPayments) {
		List<FdInformation> listOfFds = new ArrayList<FdInformation>();
		borrowerPayments.stream().forEach(payments -> {
			FdInformation fdInformation = new FdInformation();
			fdInformation.setFdCreatedDate(payments.getFdCreated());
			fdInformation.setName(payments.getName());
			fdInformation.setBorrowerId("BR" + payments.getUserId());
			fdInformation.setFdFromSystem(BigDecimal.valueOf(payments.getAmount()).toBigInteger());
			fdInformation.setTotalFdAmount(BigDecimal.valueOf(payments.getFdAmount()).toBigInteger());
			User user = userRepo.findById(payments.getUserId()).get();
			if (user != null && user.getPersonalDetails() != null) {

				fdInformation.setPanNumber(user.getPersonalDetails().getPanNumber());
				fdInformation.setDob(expectedDateFormat.format(user.getPersonalDetails().getDob()));
				if (user.getPersonalDetails().getGender().equalsIgnoreCase("Male")) {
					fdInformation.setGender(2);
				} else {
					fdInformation.setGender(1);
				}
				fdInformation.setAddress(user.getPersonalDetails().getAddress());
			}

			fdInformation.setPincode(user.getPinCode());
			fdInformation.setRegisteredMobileNumber(user.getMobileNumber());
			fdInformation.setRegisteredDate(expectedDateFormat.format(user.getRegisteredOn()));

			if (payments.getFdClosedDate() != null) {
				fdInformation.setFdClosedDate(expectedDateFormat.format(payments.getFdClosedDate()));
			} else {
				fdInformation.setFdClosedDate("NA");
			}
			listOfFds.add(fdInformation);

		});
		FdSearchDownloadResponse fdSearchDownloadResponse = new FdSearchDownloadResponse();
		fdSearchDownloadResponse.setUrl(creatingExcelSheetForFd(listOfFds));
		fdSearchDownloadResponse.setListOfFds(listOfFds);
		return fdSearchDownloadResponse;

	}

	@Override
	public String excelSheetForActiveBorrower() {
		logger.info("excelSheetForActiveBorrower method is started..");

		List<BorrowerPayments> borrowerPayments = borrowerPaymentsRepo.getListOfActiveFdDetails();

		List<FdInformation> listOfFds = new ArrayList<FdInformation>();

		logger.info("Size :" + borrowerPayments.size());

		for (BorrowerPayments fdDetails : borrowerPayments) {

			FdInformation fdInformation = new FdInformation();
			fdInformation.setBorrowerId(fdDetails.getUserId().toString());
			fdInformation.setName(fdDetails.getName());

			if (fdDetails.getFdClosedDate() != null) {
				fdInformation.setFdClosedDate(expectedDateFormat.format(fdDetails.getFdClosedDate()));
			} else {
				fdInformation.setFdClosedDate("NA");
			}

			fdInformation.setTotalFdAmount(BigDecimal.valueOf(fdDetails.getFdAmount()).toBigInteger());
			fdInformation.setFdFromSystem(BigDecimal.valueOf(fdDetails.getAmount()).toBigInteger());

			if (fdDetails.getFdValidity() != null) {
				fdInformation.setValidityDate(expectedDateFormat.format(fdDetails.getFdValidity()));
			} else {
				fdInformation.setValidityDate("NA");
			}

			if (fdDetails.getFdCreated() != null) {
				fdInformation.setCreatedDate(expectedDateFormat.format(fdDetails.getFdCreated()));
			} else {
				fdInformation.setCreatedDate("NA");
			}

			fdInformation.setCountry(fdDetails.getCountry());
			fdInformation.setFdStatus(fdDetails.getFdStatus().toString());
			fdInformation.setIfscCode(fdDetails.getIfsc());
			fdInformation.setLeadBy(fdDetails.getLeadBy());
			fdInformation.setBankName(fdDetails.getBankName());
			fdInformation.setLocation(fdDetails.getBankName());
			fdInformation.setRoi(fdDetails.getRoi());
			fdInformation.setPerDayInterest(fdDetails.getPerDayInterest());
			fdInformation.setFdType(fdDetails.getFundingType().toString());
			fdInformation.setRegisteredMobileNumber(fdDetails.getStudentMobileNumber());
			fdInformation.setConsultancyName(fdDetails.getConsultancy());
			fdInformation.setCountry(fdDetails.getCountry());
			listOfFds.add(fdInformation);

			logger.info("borrowerId :" + fdDetails.getUserId());

		}
		logger.info("excelSheetForActiveBorrower method is ended..");
		String downloadUrl = createSheetForFdActive(listOfFds);
		return downloadUrl;
	}

	private String createSheetForFdActive(List<FdInformation> listOfFds) {

		logger.info("createSheetForFdActive method is started..");

		StringBuilder sbf = new StringBuilder(" Active-Fd-Details");

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("S.No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("User Id");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("User Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Fd Close Date");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Fd Created Date");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Fd Validity Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("Fd Amount");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Fd From System Amunt");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Bank Name");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Branch");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Ifsc Code");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Lead By");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Roi");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Fd Type");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Per Day Interest");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Student Mobile Number.");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Consultancy");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Country");
		cell.setCellStyle(style);
		int rowCount = 0;

		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}
		double totalAmount = 0.0;
		int count = 0;

		if (listOfFds != null && !listOfFds.isEmpty()) {
			for (FdInformation lenderPricipalReturn : listOfFds) {
				count = count + 1;
				Row row1 = spreadSheet.createRow(++rowCount);

				Cell cell1 = row1.createCell(0);
				cell1.setCellValue(count);

				cell1 = row1.createCell(1);
				cell1.setCellValue(lenderPricipalReturn.getBorrowerId());

				cell1 = row1.createCell(2);
				cell1.setCellValue(lenderPricipalReturn.getName());

				cell1 = row1.createCell(3);
				cell1.setCellValue(lenderPricipalReturn.getFdClosedDate());

				cell1 = row1.createCell(4);
				cell1.setCellValue(lenderPricipalReturn.getCreatedDate());

				cell1 = row1.createCell(5);
				cell1.setCellValue(lenderPricipalReturn.getValidityDate());

				cell1 = row1.createCell(6);
				cell1.setCellValue(lenderPricipalReturn.getTotalFdAmount().doubleValue());

				cell1 = row1.createCell(7);
				cell1.setCellValue(lenderPricipalReturn.getFdFromSystem().doubleValue());

				cell1 = row1.createCell(8);
				cell1.setCellValue(lenderPricipalReturn.getBankName());

				cell1 = row1.createCell(9);
				cell1.setCellValue(lenderPricipalReturn.getLocation());

				cell1 = row1.createCell(10);
				cell1.setCellValue(lenderPricipalReturn.getIfscCode());

				cell1 = row1.createCell(11);
				cell1.setCellValue(lenderPricipalReturn.getLeadBy());

				cell1 = row1.createCell(12);
				cell1.setCellValue(lenderPricipalReturn.getRoi());

				cell1 = row1.createCell(13);
				cell1.setCellValue(lenderPricipalReturn.getFdType());

				cell1 = row1.createCell(14);
				cell1.setCellValue(lenderPricipalReturn.getPerDayInterest());

				cell1 = row1.createCell(15);
				cell1.setCellValue(lenderPricipalReturn.getRegisteredMobileNumber());

				cell1 = row1.createCell(16);
				cell1.setCellValue(lenderPricipalReturn.getConsultancyName());

				cell1 = row1.createCell(17);
				cell1.setCellValue(lenderPricipalReturn.getCountry());
			}
		}

		File f = new File(iciciFilePathBeforeApproval + sbf.toString() + ".xls");
		String downloadUrl = null;

		String json = null;
		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);

			workBook.close();
			outFile.close();

			FileInputStream targetStream = new FileInputStream(f);
			FileRequest fileRequest = new FileRequest();

			fileRequest.setFilePrifix(FileType.FdDetails.name());
			fileRequest.setInputStream(targetStream);
			fileRequest.setFileName(f.getName());
			fileManagementService.putFile(fileRequest);

			FileResponse getFile = fileManagementService.getFile(fileRequest);
			// downloadUrl = getFile.getDownloadUrl();

			String[] str = getFile.getDownloadUrl().split(Pattern.quote("?"));
			downloadUrl = str[0];

			System.out.println("downloadUrl......." + downloadUrl);

			Map<String, Object> jsonObject = new HashMap<>();
			jsonObject.put("downloadUrl", downloadUrl);

			// Use the Jackson ObjectMapper to convert the Map to a JSON object
			ObjectMapper objectMapper = new ObjectMapper();
			json = objectMapper.writeValueAsString(jsonObject);

		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		logger.info("createSheetForFdActive method is ended..");

		logger.info("excel sheet download url : " + downloadUrl);

		return json;

	}

	@Override
	@Transactional
	public MonthWiseBorrowersFdResponseDto getClosedFdsWithRepaymentDetails(
			ClosedFdsWithRepaymentRequestDto closedFdsWithRepaymentRequestDto) {
		MonthWiseBorrowersFdResponseDto monthWiseBorrowersFdResponseDto = new MonthWiseBorrowersFdResponseDto();
		List<FdInformation> listOfFdsInformation = new ArrayList<FdInformation>();

		// try {
		List<BorrowerPayments> borrowerPayments = borrowerPaymentsRepo
				.findByFdClosedDateGreaterThanEqualAndFdClosedDateLessThanEqual(
						closedFdsWithRepaymentRequestDto.getStartDate(), closedFdsWithRepaymentRequestDto.getEndDate());

		if (borrowerPayments != null && !borrowerPayments.isEmpty()) {
			borrowerPayments.stream().forEach(loan -> {
				FdInformation fdInformation = new FdInformation();
				fdInformation.setFdCreatedDate(loan.getPaymentDate());
				User user = userRepo.findById(loan.getUserId()).get();
				if (user != null) {
					if (user.getPersonalDetails() != null) {
						fdInformation.setPanNumber(user.getPersonalDetails().getPanNumber());
						fdInformation.setDob(expectedDateFormat.format(user.getPersonalDetails().getDob()));
						if (user.getPersonalDetails().getGender().equalsIgnoreCase("Male")) {
							fdInformation.setGender(2);
						} else {
							fdInformation.setGender(1);
						}
						fdInformation.setAddress(user.getPersonalDetails().getAddress());
					}

				}
				BorrowerPayments fdDetails = borrowerPaymentsRepo.findByUserIdWithLimit(loan.getUserId());
				fdInformation.setPincode(user.getPinCode());
				fdInformation.setRegisteredMobileNumber(user.getMobileNumber());
				fdInformation.setRegisteredDate(expectedDateFormat.format(user.getRegisteredOn()));
				fdInformation.setName(fdDetails.getName());
				fdInformation.setBorrowerId("BR" + fdDetails.getUserId());
				fdInformation.setTotalFdAmount(BigDecimal.valueOf(fdDetails.getFdAmount()).toBigInteger());
				fdInformation.setFdFromSystem(BigDecimal.valueOf(fdDetails.getAmount()).toBigInteger());
				if (fdDetails.getFdCreated() != null) {
					fdInformation.setFdCreatedDate(fdDetails.getFdCreated());
				}
				if (fdDetails.getFdClosedDate() != null) {
					fdInformation.setFdClosedDate(expectedDateFormat.format(fdDetails.getFdClosedDate()));
				} else {
					fdInformation.setFdClosedDate("NA");
				}
				fdInformation.setAmountRetunredToRepayment(loan.getAmountReturnedToRepayment());
				fdInformation.setAmountReturnedToAnother(loan.getAmountReturnedToAnother());
				fdInformation.setComments(loan.getComments());
				if (loan.getAmount() - loan.getAmountReturnedToRepayment() == 0.0) {
					fdInformation.setFdStatus("Closed");
				} else if (loan.getAmountReturnedToRepayment() > 0.0) {
					fdInformation.setFdStatus("Partial");
				} else {
					fdInformation.setFdStatus("NotYetClosed");
				}
				fdInformation.setLoanId(loan.getId());
				fdInformation.setFeeInvoice(loan.getFeeInvoiceUrl().length() > 0 ? loan.getFeeInvoiceUrl() : null);
				listOfFdsInformation.add(fdInformation);

			});
			listOfFdsInformation.sort(Comparator.comparing(FdInformation::getFdClosedDate).reversed());

			monthWiseBorrowersFdResponseDto.setFdInformation(listOfFdsInformation);
			monthWiseBorrowersFdResponseDto.setMonthDownloadUrl(creatingExcelSheetForFdClosed(listOfFdsInformation));

		}

		/*
		 * } catch (Exception e) {
		 * logger.info("Exception in getClosedFdsWithRepaymentDetails method"); }
		 */

		return monthWiseBorrowersFdResponseDto;

	}

	public String creatingExcelSheetForFdClosed(List<FdInformation> listOfFdInformation) {

		StringBuilder sbf = new StringBuilder("Fd-details"); // for interest

		FileOutputStream outFile = null;

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet spreadSheet = workBook.createSheet(sbf.toString());
		HSSFFont headerFont = workBook.createFont();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);

		spreadSheet.createFreezePane(0, 1);

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("S.No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Customer Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Borrower Id");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Loan Disbursed");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("DOB");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("Gender");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Pan");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Reg Date");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Address");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("State");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Pincode");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Contact No");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Fd Closed Date");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Amount Returned To Repayment");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Amount Returned To Another");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Comments");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("FdStatus");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("LoanId");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}
		double totalAmount = 0.0;
		int serialCount = 0;
		if (listOfFdInformation != null && !listOfFdInformation.isEmpty()) {
			for (FdInformation lenderPricipalReturn : listOfFdInformation) {
				serialCount = serialCount + 1;
				Row row1 = spreadSheet.createRow(++rowCount);
				Cell cell1 = row1.createCell(0);
				cell1.setCellValue(serialCount);

				cell1 = row1.createCell(1);
				cell1.setCellValue(lenderPricipalReturn.getFdCreatedDate() == null ? "NA"
						: lenderPricipalReturn.getFdCreatedDate().toString());

				cell1 = row1.createCell(2);
				cell1.setCellValue(lenderPricipalReturn.getName());

				cell1 = row1.createCell(3);
				cell1.setCellValue(lenderPricipalReturn.getBorrowerId());

				cell1 = row1.createCell(4);
				cell1.setCellValue(lenderPricipalReturn.getFdFromSystem().doubleValue());

				cell1 = row1.createCell(5);
				cell1.setCellValue(lenderPricipalReturn.getDob());

				cell1 = row1.createCell(6);
				cell1.setCellValue(lenderPricipalReturn.getGender());

				cell1 = row1.createCell(7);
				cell1.setCellValue(lenderPricipalReturn.getPanNumber());

				cell1 = row1.createCell(8);
				cell1.setCellValue(lenderPricipalReturn.getRegisteredDate());

				cell1 = row1.createCell(9);
				cell1.setCellValue(lenderPricipalReturn.getAddress());

				cell1 = row1.createCell(10);
				cell1.setCellValue(lenderPricipalReturn.getStateCode());

				cell1 = row1.createCell(11);
				cell1.setCellValue(lenderPricipalReturn.getPincode());

				cell1 = row1.createCell(12);
				cell1.setCellValue(lenderPricipalReturn.getRegisteredMobileNumber());

				cell1 = row1.createCell(13);
				cell1.setCellValue(lenderPricipalReturn.getFdClosedDate());

				cell1 = row1.createCell(14);
				cell1.setCellValue(lenderPricipalReturn.getAmountRetunredToRepayment());

				cell1 = row1.createCell(15);
				cell1.setCellValue(lenderPricipalReturn.getAmountReturnedToAnother());

				cell1 = row1.createCell(16);
				cell1.setCellValue(lenderPricipalReturn.getComments());

				cell1 = row1.createCell(17);
				cell1.setCellValue(lenderPricipalReturn.getFdStatus());

				cell1 = row1.createCell(18);
				cell1.setCellValue("OXYLID" + lenderPricipalReturn.getLoanId());

			}
		}

		File f = new File(iciciFilePathBeforeApproval + sbf.toString() + ".xls");
		String downloadUrl = null;
		try {
			outFile = new FileOutputStream(f);

			workBook.write(outFile);

			workBook.close();
			outFile.close();

			FileInputStream targetStream = new FileInputStream(f);

			FileRequest fileRequest = new FileRequest();

			fileRequest.setFilePrifix(FileType.FdClosedDetails.name());
			fileRequest.setInputStream(targetStream);
			fileRequest.setFileName(f.getName());
			fileManagementService.putFile(fileRequest);

			FileResponse getFile = fileManagementService.getFile(fileRequest);
			downloadUrl = getFile.getDownloadUrl();
		} catch (Exception e) {
			logger.info(e.getMessage());
		}

		return downloadUrl;
	}

	@Override
	public BorrowerPaymentsStatusDto getTotalCreatedFdsDetails(
			ListOfPaymentsDetailsRequestDto listOfPaymentsDetailsRequestDto) {
		List<BorrowersTotalFdsResponse> borrowersTotalFds = new ArrayList<BorrowersTotalFdsResponse>();

		BorrowerPaymentsStatusDto borrowerPaymentsStatusDto = new BorrowerPaymentsStatusDto();
		int count = 0;
		int pageNo = (listOfPaymentsDetailsRequestDto.getPageSize()
				* (listOfPaymentsDetailsRequestDto.getPageNo() - 1));

		List<LenderCmsPayments> borrowerPayments = lenderCmsPaymentsRepo.loanExecutedFromSystemWithPagination(pageNo,
				listOfPaymentsDetailsRequestDto.getPageSize());

		Integer value = lenderCmsPaymentsRepo.loanExecutedFromSystemCount();
		if (value != null) {
			count = value;
		}

		if (borrowerPayments != null && !borrowerPayments.isEmpty()) {

			borrowerPayments.stream().forEach(payments -> {
				BorrowerPayments borrowersPayment = null;

				BorrowersTotalFdsResponse borrowersTotalFdsResponse = new BorrowersTotalFdsResponse();

				if (payments.getBorrowerPaymentsId() > 0) {
					borrowersPayment = borrowerPaymentsRepo.findById(payments.getBorrowerPaymentsId()).get();

				} else {
					borrowersPayment = borrowerPaymentsRepo.findLoanApplicationIdByUser(payments.getUserId());
				}
				borrowersTotalFdsResponse.setFdCreatedDate(payments.getPaymentDate());

				borrowersTotalFdsResponse.setTotalAmount(payments.getTotalAmount());
				borrowersTotalFdsResponse.setLenderReturnsType(payments.getLenderReturnsType());
				borrowersTotalFdsResponse.setS3_download_link(payments.getS3_download_link());

				if (borrowersPayment != null) {
					borrowersTotalFdsResponse.setLoanId("OXYLID" + borrowersPayment.getId());
					if (borrowersPayment.getAmountReturnedToRepayment() != null
							&& (payments.getTotalAmount() == borrowersPayment.getAmountReturnedToRepayment())) {
						borrowersTotalFdsResponse
								.setAmountRetunredToRepayment(borrowersPayment.getAmountReturnedToRepayment());

						borrowersTotalFdsResponse.setBankName(borrowersPayment.getBankName());
						borrowersTotalFdsResponse.setBorrowerId(borrowersPayment.getUserId());
						borrowersTotalFdsResponse.setRepaymentStatus("closed");
						borrowersTotalFds.add(borrowersTotalFdsResponse);
					} else {
						borrowersTotalFdsResponse
								.setAmountRetunredToRepayment(borrowersPayment.getAmountReturnedToRepayment());
						borrowersTotalFdsResponse.setBankName(borrowersPayment.getBankName());
						borrowersTotalFdsResponse.setBorrowerId(borrowersPayment.getUserId());
						borrowersTotalFdsResponse.setRemainingRepaymentAmount(
								payments.getTotalAmount() - borrowersPayment.getAmountReturnedToRepayment());
						borrowersTotalFdsResponse.setRepaymentStatus("open");
						borrowersTotalFds.add(borrowersTotalFdsResponse);
					}

				}

			});

			borrowerPaymentsStatusDto.setCount(count);
			borrowerPaymentsStatusDto.setBorrowersTotalFdsResponse(borrowersTotalFds);

		}

		return borrowerPaymentsStatusDto;
	}

	@Override
	public BorrowersTotalFdsResponse updateFdRepaymentStatus(FdCloseingRequestDto fdCloseingRequestDto) {

		BorrowersTotalFdsResponse borrowersTotalFdsResponse = new BorrowersTotalFdsResponse();

		BorrowerPayments borrowerPayments = borrowerPaymentsRepo.findById(fdCloseingRequestDto.getBorrowerPaymentId())
				.get();
		BorrowerRepaymentsHistory borrowerRepaymentHistory = borrowerRepaymentsHistoryRepo
				.findByBorrowerPaymentsId(fdCloseingRequestDto.getBorrowerPaymentId());

		if (borrowerPayments != null) {

			borrowerPayments.setAmountReturnedToRepayment(borrowerPayments.getAmountReturnedToRepayment()
					+ fdCloseingRequestDto.getAmountReturnedToRepayment());
			borrowerPayments.setAmountReturnedToAnother(
					borrowerPayments.getAmountReturnedToAnother() + fdCloseingRequestDto.getAmountReturnedToAnother());
			borrowerPaymentsRepo.save(borrowerPayments);

			if (borrowerRepaymentHistory != null) {
				borrowerRepaymentHistory
						.setAmountReturnedToRepayment(borrowerRepaymentHistory.getAmountReturnedToRepayment()
								+ fdCloseingRequestDto.getAmountReturnedToRepayment());
				borrowerRepaymentsHistoryRepo.save(borrowerRepaymentHistory);

			}

			BorrowerRepaymentsHistory borrowerRepaymentsHistory = new BorrowerRepaymentsHistory();
			borrowerRepaymentsHistory.setBorrowerPaymentsId(borrowerPayments.getId());
			borrowerRepaymentsHistory.setAmountReturnedToRepayment(fdCloseingRequestDto.getAmountReturnedToRepayment());
			borrowerRepaymentsHistoryRepo.save(borrowerRepaymentsHistory);

			borrowersTotalFdsResponse.setLoanId("OXYLID" + borrowerPayments.getId());
			borrowersTotalFdsResponse.setRemainingRepaymentAmount(fdCloseingRequestDto.getAmountReturnedToRepayment());
		}
		return borrowersTotalFdsResponse;

	}
}
