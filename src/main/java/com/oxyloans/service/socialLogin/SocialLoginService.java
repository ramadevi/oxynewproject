package com.oxyloans.service.socialLogin;

import java.io.IOException;
import java.security.GeneralSecurityException;

import com.oxyloans.request.SocialLoginRequestDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.GmailSignInResponse;
import com.oxyloans.response.user.UserResponse;

public interface SocialLoginService {

	public GmailSignInResponse veifyExistenceOfSocialLoginUser(SocialLoginRequestDto request)
			throws GeneralSecurityException, IOException;

	public UserResponse newRegistationWithSocialLogin(UserRequest request);

}
