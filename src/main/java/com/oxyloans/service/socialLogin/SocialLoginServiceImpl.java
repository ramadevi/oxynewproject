package com.oxyloans.service.socialLogin;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.oxyloans.coreauthdto.GenericAccessToken.EncodingAlgorithm;
import com.oxyloans.coreauthdto.PasswordGrantAccessToken;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceDetails;
import com.oxyloans.entity.loan.LoanRequest.RepaymentMethod;
import com.oxyloans.entity.socialLogin.SocialLoginDetails;
import com.oxyloans.repository.socialloginrepo.SocialLoginDetailsRepo;
import com.oxyloans.entity.user.PersonalDetails;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.User.Citizenship;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.mobile.MobileUtil;
import com.oxyloans.mobile.twoFactor.MobileOtpVerifyFailedException;
import com.oxyloans.paytm.PaytmService;
import com.oxyloans.repo.loan.OxyLoanRequestRepo;
import com.oxyloans.repo.user.LenderReferenceDetailsRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.SocialLoginRequestDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.GmailSignInResponse;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.security.ThreadLocalSecurityProvider;
import com.oxyloans.service.loan.ActionNotAllowedException;
import com.oxyloans.service.loan.LoanServiceFactory;
import com.oxyloans.serviceloan.LoanRequestDto;
import com.oxyloans.serviceloan.LoanResponseDto;
import com.oxyloans.service.user.UserServiceImpl;

@Service
public class SocialLoginServiceImpl implements SocialLoginService {

	private static final Logger logger = LogManager.getLogger(SocialLoginServiceImpl.class);
	final static JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	@Value("${gmailClientId}")
	private String clientId;

	@Autowired
	private SocialLoginDetailsRepo socialLoginDetailsRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private LoanServiceFactory loanServiceFactory;

	@Autowired
	private LenderReferenceDetailsRepo lenderReferenceDetailsRepo;

	@Autowired
	private PaytmService paytmService;

	private final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Autowired
	private OxyLoanRequestRepo loanRequestRepo;

	@Autowired
	private UserServiceImpl userServiceImpl;

	@Autowired
	private MobileUtil mobileUtil;

	@Override
	public GmailSignInResponse veifyExistenceOfSocialLoginUser(SocialLoginRequestDto request)
			throws GeneralSecurityException, IOException {

		User user = userRepo.findByEmailIgnoreCase(request.getEmailId());
		GmailSignInResponse response = new GmailSignInResponse();

		if (user != null) {
			SocialLoginDetails details = socialLoginDetailsRepo.findByUserId(user.getId());

			if (user.getPassword() != null && details == null) {
				throw new ActionNotAllowedException("Email already registered.Please use your password to continue.",
						ErrorCodes.ENITITY_NOT_FOUND);
			}

			if (request.getGoogleUserId() != null) {
				if (details.getGoogleUserId() == null) {
					throw new ActionNotAllowedException(
							"Email id already registered through Facebook.Please use Facebook login.",
							ErrorCodes.ACTION_ALREDY_DONE);
				} else if (request.getGoogleUserId().equalsIgnoreCase(details.getGoogleUserId())) {
					response.setUserExists(true);
				}
			}

			if (request.getFacebookUserId() != null) {
				if (details.getFacebookUserId() == null) {
					throw new ActionNotAllowedException(
							"Email id already registered through Gmail.Please use Gmail login.",
							ErrorCodes.ACTION_ALREDY_DONE);
				} else if (request.getFacebookUserId().equalsIgnoreCase(details.getFacebookUserId())) {
					response.setUserExists(true);
				}
			}

		} else {
			response.setUserExists(false);
		}

		return response;

	}

	@Override
	public UserResponse newRegistationWithSocialLogin(UserRequest userRequest) {

		UserResponse userResponse = null;

		if (userRequest.isCreate() && userRequest.getMobileOtpValue() != null) {

			User user = new User();
			boolean verifyOtp = false;
			if (userRequest.getCitizenship().equals(Citizenship.NONNRI.toString())) {
				verifyOtp = mobileUtil.verifyOtp(userRequest.getMobileNumber(), userRequest.getMobileOtpSession(),
						userRequest.getMobileOtpValue());
				user.setMobileNumberVerified(true);
				user.setMobileOtpSent(!verifyOtp);
				user.setCitizenship(Citizenship.valueOf(userRequest.getCitizenship().toUpperCase()));
				if (!verifyOtp) {
					throw new MobileOtpVerifyFailedException("Invalid OTP value please check.", ErrorCodes.INVALID_OTP);
				}
			} else {
				user.setMobileNumberVerified(true);
				user.setMobileOtpSent(true);
				user.setCitizenship(Citizenship.valueOf(userRequest.getCitizenship().toUpperCase()));
			}
			user.setMobileNumber(userRequest.getMobileNumber());
			user.setPrimaryType(PrimaryType.valueOf(userRequest.getType().toUpperCase()));
			user.setMobileOtpSession("");
			user.setSalt(UUID.randomUUID().toString() + UUID.randomUUID().toString().substring(0, 10));
			user.setLongitude(userRequest.getLongitude());
			user.setLatitude(userRequest.getLatitude());
			user.setCity(userRequest.getCity());
			user.setPinCode(userRequest.getPinCode());
			user.setState(userRequest.getState());
			user.setLocality(userRequest.getLocality());
			user.setUniqueNumber(new Long(System.currentTimeMillis()).toString().substring(0, 10)); // This is temporary

			StringBuilder uniqueNumber = new StringBuilder();
			if (user.getPrimaryType() == PrimaryType.LENDER) {
				uniqueNumber.append("LR1");
			} else {
				uniqueNumber.append("BR1");
			}

			PersonalDetails personalDetails = user.getPersonalDetails();
			if (personalDetails == null) {
				if (personalDetails == null) {
					personalDetails = new PersonalDetails();
					personalDetails.setFirstName(userRequest.getFirstName());
					personalDetails.setLastName(userRequest.getLastName());
					personalDetails.setFatherName("");
					personalDetails.setCreatedAt(new Date());
					personalDetails.setModifiedAt(new Date());
					personalDetails.setGender(userRequest.getGender());

					/*
					 * MultiChainResponseDto multiChainResponseDto = generateMultiChainAddress();
					 * personalDetails.setMultiChainAddress(multiChainResponseDto.getData());
					 */
					if (userRequest.getDob() != null) {
						try {
							personalDetails.setDob(expectedDateFormat.parse(userRequest.getDob()));
						}

						catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				if (userRequest.getState() != null && userRequest.getPinCode() != null) {
					user.setPinCode(userRequest.getPinCode());
					user.setState(userRequest.getState());
				}
				user.setUrchinTrackingModule(userRequest.getUtmSource());
				user = userRepo.save(user);
				uniqueNumber.append(StringUtils.leftPad(user.getId().toString(), 6, "0"));
				user.setUniqueNumber(uniqueNumber.toString());
				personalDetails.setUser(user);
				user.setPersonalDetails(personalDetails);
				user = userRepo.save(user);
				if (loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId()) == null) {

					LoanRequestDto loanRequestDto = new LoanRequestDto();
					loanRequestDto.setDuration(6);
					loanRequestDto.setLoanPurpose("Initiated the Request");
					loanRequestDto.setLoanRequestAmount(userRequest.getAmountInterested());
					loanRequestDto.setRateOfInterest(0d);
					loanRequestDto.setRepaymentMethod(RepaymentMethod.PI.name());
					Calendar expectedDate = Calendar.getInstance();
					expectedDate.add(Calendar.DATE, 1);
					loanRequestDto.setExpectedDate(expectedDateFormat.format(expectedDate.getTime()));

					PasswordGrantAccessToken accessToken = new PasswordGrantAccessToken(6000, EncodingAlgorithm.RSA);
					accessToken.setUserId(user.getId().toString());
					ThreadLocalSecurityProvider.setAccessToken(accessToken);
					LoanResponseDto loanRequest = loanServiceFactory
							.getService(user.getPrimaryType().name().toUpperCase())
							.loanRequest(user.getId(), loanRequestDto);
					logger.info("Created Loan Request for the user {} as request id {}", user.getId(),
							loanRequest.getLoanRequestId());

				}

			}
			userResponse = new UserResponse();
			userResponse.setMobileverified(verifyOtp);
			userResponse.setMobileNumber(userRequest.getMobileNumber());
			logger.info("Lender Reference...........................starts.........................");
			String uniqueNumber1 = userRequest.getUniqueNumber();
			logger.info(uniqueNumber1 + "@@@@@@@@@@@@@@@@@@@@@@@");
			User referrer = userRepo.findByUniqueNumber(uniqueNumber1);

			if (!(uniqueNumber1.equals("0"))) {
				Integer referrerId = referrer.getId();

				LenderReferenceDetails referenceDetails = lenderReferenceDetailsRepo
						.findingReferrerInformation(referrerId, userRequest.getMobileNumber());

				User newUser = userRepo.findByMobileNumber(userRequest.getMobileNumber());

				Integer refereeId = newUser.getId();

				if (referenceDetails != null) {

					referenceDetails.setRefereeId(refereeId);

					lenderReferenceDetailsRepo.save(referenceDetails);

				} else {
					LenderReferenceDetails details = new LenderReferenceDetails();

					details.setRefereeId(refereeId);
					details.setRefereeMobileNumber(userRequest.getMobileNumber());
					details.setReferrerId(referrerId);
					details.setRefereeName(newUser.getPersonalDetails().getFirstName() + ""
							+ newUser.getPersonalDetails().getLastName());

					lenderReferenceDetailsRepo.save(details);
				}
			}
			logger.info("Lender Reference...........................ends.........................");

			user.setEmail(userRequest.getEmail());
			userRepo.save(user);

			/*
			 * String vanNumber = null; if (user.getPrimaryType() == PrimaryType.BORROWER) {
			 * String patnerId = null; Random random1 = new Random(); String value =
			 * String.format("%04d", random1.nextInt(10000)); PaytmVanRequest
			 * paytmVanRequest = new PaytmVanRequest(); int length = 0; for (length = 0;
			 * length <= 10; ++length) { Random rand = new Random(); Long drand = (long)
			 * (rand.nextDouble() * 10000000000L);
			 * 
			 * patnerId = drand.toString(); length = patnerId.length(); }
			 * 
			 * logger.info("--------------------" + patnerId + "---------------------");
			 * logger.info("parter id lengthhhhhhhhhhhhhhhhhhhhhhhhhhhhhh" +
			 * patnerId.length());
			 * 
			 * logger.info("Created patnerId for paytm", user.getId(), patnerId);
			 * paytmVanRequest.setMobile(user.getMobileNumber());
			 * 
			 * paytmVanRequest.setName(userRequest.getFirstName() +
			 * userRequest.getLastName()); paytmVanRequest.setPartnerRefId("Reqoxytrade" +
			 * value); paytmVanRequest.setPartnerId(patnerId); Gson gson = new Gson();
			 * String json = gson.toJson(paytmVanRequest);
			 * logger.info("body !!!!!!!!!!!!!!!!!" + paytmVanRequest.toString());
			 * logger.info("Created request paytm", user.getId(), json); String response =
			 * paytmService.createVan(paytmVanRequest); vanNumber = "OXYTDE" + patnerId;
			 * logger.info("responseOfPaytmVanCreation" + response); }
			 * 
			 * // user.setVanNumber(vanNumber);
			 */
			user.setEmail(userRequest.getEmail());

			user.setEmailVerified(userRequest.isGmailAccountVerified());

			userRepo.save(user);

			SocialLoginDetails details = new SocialLoginDetails();
			userResponse = new UserResponse();
			details.setUserId(user.getId());
			details.setGmailVerified(userRequest.isGmailAccountVerified());

			if (userRequest.getUtmSource().equalsIgnoreCase("gmail")) {
				details.setGoogleUserId(userRequest.getSocialLoginUserId());
				userResponse.setSocialLoginUserId(userRequest.getSocialLoginUserId());
				userResponse.setUtmSource(userRequest.getUtmSource());
			} else if (userRequest.getUtmSource().equalsIgnoreCase("facebook")) {
				details.setFacebookUserId(userRequest.getSocialLoginUserId());
				userResponse.setSocialLoginUserId(userRequest.getSocialLoginUserId());
				userResponse.setUtmSource(userRequest.getUtmSource());
			}
			socialLoginDetailsRepo.save(details);

			

			userResponse.setMobileNumber(user.getMobileNumber());
			userResponse.setEmail(user.getEmail());
			userResponse.setEmailVerified(user.getEmailVerified());
			/*
			 * userResponse.setGoogleUserId(details.getGoogleUserId() != null ?
			 * details.getGoogleUserId() : null);
			 * userResponse.setFacebookUserId(details.getFacebookUserId() != null ?
			 * details.getFacebookUserId() : null);
			 */

		}

		return userResponse;
	}

}
