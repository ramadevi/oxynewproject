package com.oxyloans.service.relendservice;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.oxyloans.commonutil.DateUtil;
import com.oxyloans.customexceptions.DataFormatException;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWallet;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletNativeRepo;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletRepo;
import com.oxyloans.entity.loan.LoanEmiCard;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.UserProfileRisk;
import com.oxyloans.relend.LenderReLendRequest;
import com.oxyloans.relend.LenderReLendResponse;
import com.oxyloans.relend.LenderReLendSearchRequest;
import com.oxyloans.relend.entity.LenderReLendConfig;
import com.oxyloans.relend.entity.LenderReLendConfigArchive;
import com.oxyloans.relend.repo.LenderReLendConfigArchiveRepo;
import com.oxyloans.relend.repo.LenderReLendConfigNativeRepo;
import com.oxyloans.relend.repo.LenderReLendConfigRepo;
import com.oxyloans.repo.loan.LoanEmiCardRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.response.user.RiskProfileDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.service.loan.ActionNotAllowedException;

@Service
public class LenderReLendServiceImpl implements LenderReLendService{
	
    private static final Logger logger = LogManager.getLogger(LenderReLendServiceImpl.class);
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static DecimalFormat df2 = new DecimalFormat("#");
	

	
	@Autowired
	private LenderReLendConfigRepo lenderReLendConfigRepo;
	
	@Autowired
	private LenderReLendConfigArchiveRepo lenderReLendConfigArchiveRepo;
	
	@Autowired
	private UserRepo userRepo;
	
	@Autowired
	private LenderReLendConfigNativeRepo lenderReLendConfigNativeRepo;
	
	@Autowired
	private LenderOxyWalletNativeRepo lenderOxyWalletNativeRepo;
	
	@Autowired
	private LenderOxyWalletRepo lenderOxyWalletRepo;
	
	@Autowired
	private LoanEmiCardRepo loanEmiCardRepo;
	
	@Value("${lenderrelendconsiderdate}")
	private String relendConsiderDate;
	
	@Override
	public LenderReLendResponse saveReLendConfig(LenderReLendRequest request) {
		
		logger.info(" !!!!!!!! LenderReLendServiceImpl saveReLendConfig method start!!!!!!!!!");

		if (request.getUserType() == null || request.getUserType().isEmpty()) {
			throw new ActionNotAllowedException("No UserType info. present in the request.",
					ErrorCodes.INVALID_OPERATION);
		}
		if (!request.getUserType().equalsIgnoreCase("LENDER")) {
			throw new OperationNotAllowedException(
					"As a " + request.getUserType() + " primary type not allowed to perform this",
					ErrorCodes.INVALID_OPERATION);
		}

		LenderReLendResponse response = new LenderReLendResponse();
		if (request.getUserId() == null || request.getUserId() == 0) {
			throw new ActionNotAllowedException("No user id present in the request.", ErrorCodes.INVALID_OPERATION);
		}
		LenderReLendConfig getConfigInfo = lenderReLendConfigRepo.findByUserId(request.getUserId());
		if (getConfigInfo != null) {
			throw new ActionNotAllowedException("Re-Lend already Configured for this user.",
					ErrorCodes.INVALID_OPERATION);
		}		
		if (request.getStatus() == null || request.getStatus().isEmpty()) {
			throw new ActionNotAllowedException("No status info present in the request.", ErrorCodes.INVALID_OPERATION);
		} 
		
		LenderReLendConfig saveConfig = new LenderReLendConfig();
		saveConfig.setUserId(request.getUserId());
		saveConfig.setAdminComments(request.getAdminComments());
		saveConfig.setStatus(request.getStatus());
		saveConfig.setCreatedOn(new Date());
		try {
			LenderReLendConfig respconfig = lenderReLendConfigRepo.save(saveConfig);
			if (respconfig == null) {
				response.setMessage("Failed");
			} else {
				response.setMessage("Success");
				response.setId(respconfig.getId());
				response.setUserId(respconfig.getUserId());		
				response.setAdminComments(respconfig.getAdminComments());
				response.setStatus(respconfig.getStatus());
				response.setCreatedOn(
						DateUtil.convertyyyymmddhhmmsstoddmmyyyy(dateFormat.format(respconfig.getCreatedOn())));

			}
		} catch (Exception e) {
			response.setErrorMessageDescription(e.getMessage());
			logger.info(" !!!!!!!! Exception Occured while saving the data !!!!!!!!!", e);
		}
		logger.info(" !!!!!!!! LenderAutoInvestConfigServiceImpl saveAutoInvestConfig method end!!!!!!!!!");
		return response;
	}
	
	@Override
	public LenderReLendResponse getReLendConfigByLenderId(Integer userId) {

		logger.info(" !!!!!!!! getReLendConfigByLenderId  method start!!!!!!!!!");
		if (userId == null || userId == 0) {
			throw new ActionNotAllowedException("No user id present in the request.", ErrorCodes.INVALID_OPERATION);
		}
		LenderReLendConfig respconfig = lenderReLendConfigRepo.findByUserId(userId);
		if (respconfig == null) {
			throw new ActionNotAllowedException("Re-Lend config not found this user.",
					ErrorCodes.INVALID_OPERATION);
		}

		LenderReLendResponse response = new LenderReLendResponse();

		response.setMessage("Success");
		response.setId(respconfig.getId());
		response.setUserId(respconfig.getUserId());		
		response.setStatus(respconfig.getStatus());
		response.setAdminComments(respconfig.getAdminComments());
		response.setCreatedOn(DateUtil.convertyyyymmddhhmmsstoddmmyyyy(dateFormat.format(respconfig.getCreatedOn())));
		User lenderUser = userRepo.findById(userId).get();
		if (lenderUser != null) {
			UserResponse user = new UserResponse();
			user.setId(lenderUser.getId());
			userDetails(lenderUser, user);
			response.setUser(user);
		}

		logger.info(" !!!!!!!! getReLendConfigByLenderId  method end!!!!!!!!!");
		return response;

	}
	
	@Override
	public LenderReLendResponse updReLendConfig(LenderReLendRequest request) {
		logger.info(" !!!!!!!! LenderReLendServiceImpl updReLendConfig method start!!!!!!!!!");

		if (request.getUserType() == null || request.getUserType().isEmpty()) {
			throw new ActionNotAllowedException("No UserType info. present in the request.",
					ErrorCodes.INVALID_OPERATION);
		}
		if (!request.getUserType().equalsIgnoreCase("LENDER")) {
			throw new OperationNotAllowedException(
					"As a " + request.getUserType() + " primary type not allowed to perform this",
					ErrorCodes.INVALID_OPERATION);
		}

		LenderReLendResponse response = new LenderReLendResponse();
		if (request.getUserId() == null || request.getUserId() == 0) {
			throw new ActionNotAllowedException("No user id present in the request.", ErrorCodes.INVALID_OPERATION);
		}
		
		if (request.getStatus() == null || request.getStatus().isEmpty()) {
			throw new ActionNotAllowedException("No status info present in the request.", ErrorCodes.INVALID_OPERATION);
		}

		LenderReLendConfig getConfigInfo = lenderReLendConfigRepo.findByUserId(request.getUserId());
		if (getConfigInfo == null) {
			throw new ActionNotAllowedException("RE-Lend configuration not found for this user.",
					ErrorCodes.INVALID_OPERATION);
		}
		LenderReLendConfigArchive archiveConfig = new LenderReLendConfigArchive();
		archiveConfig.setUserId(getConfigInfo.getUserId());
		archiveConfig.setAdminComments(getConfigInfo.getAdminComments());
		archiveConfig.setStatus("InActive");
		archiveConfig.setCreatedOn(getConfigInfo.getCreatedOn());
		LenderReLendConfigArchive archiveConfigResponse = lenderReLendConfigArchiveRepo.save(archiveConfig);

		if (archiveConfigResponse != null) {
			getConfigInfo.setCreatedOn(new Date());
			getConfigInfo.setUserId(request.getUserId());
			getConfigInfo.setAdminComments(request.getAdminComments());
			getConfigInfo.setStatus(request.getStatus());

			try {
				LenderReLendConfig respconfig = lenderReLendConfigRepo.save(getConfigInfo);
				if (respconfig == null) {
					response.setMessage("Failed");
				} else {
					response.setMessage("Success");
					response.setId(respconfig.getId());
					response.setUserId(respconfig.getUserId());	
					response.setAdminComments(respconfig.getAdminComments());
					response.setStatus(respconfig.getStatus());
					response.setCreatedOn(
							DateUtil.convertyyyymmddhhmmsstoddmmyyyy(dateFormat.format(respconfig.getCreatedOn())));

				}
			} catch (Exception e) {
				response.setErrorMessageDescription(e.getMessage());
				logger.info(" !!!!!!!! Exception Occured while saving the data !!!!!!!!!", e);
			}
		} else {
			response.setMessage("Failed");
			response.setErrorMessageDescription("Error Occured while updating the history");
		}
		logger.info(" !!!!!!!! LenderReLendServiceImpl updReLendConfig method end!!!!!!!!!");
		return response;

	}
	
	
	@Override
	public SearchResultsDto<LenderReLendResponse> searchReLendLenders(
			LenderReLendSearchRequest request) {
		logger.info(" !!!!!!!! LenderReLendServiceImpl searchReLendLenders method start!!!!!!!!!");
		List<Object[]> objList = lenderReLendConfigNativeRepo.searchReLendLenders(request.getFirstName(),
				request.getLastName(), request.getUserId(), request.getPage().getPageSize(),
				request.getPage().getPageNo());
		Integer totalCount1 = 0;
		totalCount1 = lenderReLendConfigNativeRepo.countTranRecords(request.getFirstName(), request.getLastName(),
				request.getUserId(), request.getPage().getPageSize(), request.getPage().getPageNo());		
		SearchResultsDto<LenderReLendResponse> results = new SearchResultsDto<LenderReLendResponse>();
		List<LenderReLendResponse> applicationResponses = new ArrayList<LenderReLendResponse>();
		try {
			if (objList != null && !objList.isEmpty()) {
				results.setTotalCount(totalCount1);
				results.setPageNo(request.getPage().getPageNo());
				results.setPageCount(objList.size());
				objList.forEach(e -> {
					try {
						LenderReLendResponse dto = new LenderReLendResponse();
						dto.setId(e[0] == null ? 0 : Integer.parseInt(e[0].toString()));
						dto.setUserId(e[1] == null ? 0 : Integer.parseInt(e[1].toString()));
						dto.setStatus(e[2] == null ? "" : e[2].toString());
						dto.setAdminComments(e[3] == null ? "" : e[3].toString());
						SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
						DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");
						dto.setCreatedOn(expectedDateFormat.format(dateFormat1.parse(e[4].toString())));						
						dto.setFirstName(e[5] == null ? "" : e[5].toString());
						dto.setLastName(e[6] == null ? "" : e[6].toString());
						dto.setPanNumber(e[7] == null ? "" : e[7].toString());
						dto.setAddress(e[8] == null ? "" : e[8].toString());
						dto.setEmail(e[9] == null ? "" : e[9].toString());
						dto.setMobileNumber(e[10] == null ? "" : e[10].toString());
						dto.setLenderCity(e[11] == null ? "0" : e[11].toString());
						Double lenderWalletCreditAmount = lenderOxyWalletNativeRepo.lenderWalletCreditAmount(dto.getUserId());
						Double lenderWalletdebitAmount = lenderOxyWalletNativeRepo.lenderWalletDebitAmount(dto.getUserId());
						Double lenderWalletInprocessAmount = lenderOxyWalletNativeRepo
								.lenderWalletInprocessAmount(dto.getUserId());
						if (!(lenderWalletInprocessAmount != null && lenderWalletInprocessAmount > 0)) {
							lenderWalletInprocessAmount = 0d;
						}
						if(!(lenderWalletCreditAmount != null && lenderWalletCreditAmount > 0)) {
							lenderWalletCreditAmount=0d;
						}
						if(!(lenderWalletdebitAmount != null && lenderWalletdebitAmount > 0)) {
							lenderWalletdebitAmount=0d;
						}
						Double lenderWalletAmount=lenderWalletCreditAmount-lenderWalletdebitAmount;
						if (lenderWalletAmount != null && lenderWalletAmount > 0) {
							lenderWalletAmount = lenderWalletAmount - lenderWalletInprocessAmount;
						} else {
							lenderWalletAmount = 0.0;
						}				
						dto.setLenderWalletAmount(lenderWalletAmount);
						
						df2.setRoundingMode(RoundingMode.DOWN);
						
						Double emiRecievedAmount=lenderReLendConfigNativeRepo.totalReceivedEmiAmountAvbleForEscrow(DateUtil.convertStrYYYYMMDDToDate(relendConsiderDate), dto.getUserId());
						
						if (!(emiRecievedAmount != null && emiRecievedAmount > 0)) {
							emiRecievedAmount = 0d;
						}						
						dto.setEmiAmountAvbleForEscrow(Double.parseDouble(df2.format(emiRecievedAmount)));
						
						Double totalAmountAddedToEscrow=lenderReLendConfigNativeRepo.totalAmountAddedToEscrow(DateUtil.convertStrYYYYMMDDToDate(relendConsiderDate), dto.getUserId());
						
						if (!(totalAmountAddedToEscrow != null && totalAmountAddedToEscrow > 0)) {
							totalAmountAddedToEscrow = 0d;
						}						
						dto.setEmiAmountAddedToEscrow(Double.parseDouble(df2.format(totalAmountAddedToEscrow)));
						
						applicationResponses.add(dto);
					} catch (Exception e1) {
						logger.info("Exception occured in searchReLendLenders", e1);
						throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
					}
				});
			}
		} catch (Exception e) {
			logger.info("Exception occured in searchReLendLenders", e);
			throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
		}
		results.setResults(applicationResponses);
		logger.info(" !!!!!!!! LenderReLendServiceImpl searchReLendLenders method end!!!!!!!!!");
		return results;
	}
	
	
	@Override
	@Transactional
	public LenderReLendResponse transferFundsToLenderWallet(
			LenderReLendSearchRequest request) {
		logger.info(" !!!!!!!! LenderReLendServiceImpl transferFundsToLenderWallet method start!!!!!!!!!");
		List<Object[]> objList = lenderReLendConfigNativeRepo.searchReLendLenders(request.getFirstName(),
				request.getLastName(), request.getUserId(), 1, 1);			
		LenderReLendResponse results=new LenderReLendResponse();
			if (objList != null && !objList.isEmpty()) {				
				objList.forEach(e -> {
					
						LenderReLendResponse dto = new LenderReLendResponse();
						dto.setId(e[0] == null ? 0 : Integer.parseInt(e[0].toString()));
						dto.setUserId(e[1] == null ? 0 : Integer.parseInt(e[1].toString()));
						dto.setStatus(e[2] == null ? "" : e[2].toString());	
						Double emiRecievedAmount=lenderReLendConfigNativeRepo.totalReceivedEmiAmountAvbleForEscrow(DateUtil.convertStrYYYYMMDDToDate(relendConsiderDate), dto.getUserId());
						
						if (!(emiRecievedAmount != null && emiRecievedAmount > 0)) {
							emiRecievedAmount = 0d;
						}
						if(emiRecievedAmount==0d) {
							throw new DataFormatException("No suffiecient balance to transfer", ErrorCodes.DATA_FETCH_ERROR);
						}
						emiRecievedAmount=Double.parseDouble(df2.format(emiRecievedAmount));
						dto.setEmiAmountAvbleForEscrow(emiRecievedAmount);
						LenderOxyWallet oxyWalletresponse=null;						
						LenderOxyWallet oxyWallet =new LenderOxyWallet();
						oxyWallet.setUserId(dto.getUserId());
						oxyWallet.setScrowAccountNumber("Funds From Re-Lend");
						oxyWallet.setTransactionAmount(dto.getEmiAmountAvbleForEscrow().intValue());
						oxyWallet.setStatus(LenderOxyWallet.Status.APPROVED);
						oxyWallet.setTransactionType("credit");
						oxyWallet.setTransactionDate(new Date());
						oxyWallet.setComments("funds transferred from nodal account by re lend request ");
						oxyWallet.setCreatedBy("Admin");
						oxyWalletresponse=lenderOxyWalletRepo.save(oxyWallet);
						if(oxyWalletresponse!=null) {						
						List<Integer> emiIdList=lenderReLendConfigNativeRepo.getEmiIds(DateUtil.convertStrYYYYMMDDToDate(relendConsiderDate), dto.getUserId());
						for(Integer i: emiIdList) {
							LoanEmiCard emicard=loanEmiCardRepo.findById(i).get();
							if(emicard!=null) {
							emicard.setEscrowTranferFlag(1);
							emicard.setEscrowTransferredDate(new Date());
							}
							else {
								throw new DataFormatException("Error Occured while saving into emi card , no emi id found", ErrorCodes.DATA_FETCH_ERROR);
							}
						}							
						}else {
							throw new DataFormatException("Error Occured while saving into wallet", ErrorCodes.DATA_FETCH_ERROR);
						}	
						results.setId(oxyWalletresponse.getId());
						results.setMessage("Successfully Transferred");
				});
			}else {
				throw new DataFormatException("Re-Lend Info not found for this user", ErrorCodes.DATA_FETCH_ERROR);
				
			}
		
		logger.info(" !!!!!!!! LenderReLendServiceImpl transferFundsToLenderWallet method end!!!!!!!!!");
		return results;
	}
	
	
	
	
	protected void userDetails(User findById, UserResponse user) {
		if (findById.getPersonalDetails() != null) {
			user.setFirstName(findById.getPersonalDetails().getFirstName());
			user.setLastName(findById.getPersonalDetails().getLastName());
			user.setAddress(findById.getPersonalDetails().getAddress());

		}
		if (findById.getPersonalDetails().getFacebookUrl() != null) {
			user.setFacebookUrl(findById.getPersonalDetails().getFacebookUrl());
		}
		if (findById.getPersonalDetails().getTwitterUrl() != null) {
			user.setTwitterUrl(findById.getPersonalDetails().getTwitterUrl());
		}
		if (findById.getPersonalDetails().getLinkedinUrl() != null) {
			user.setLinkedinUrl(findById.getPersonalDetails().getLinkedinUrl());
		}
		user.setEmail(findById.getEmail());
		user.setMobileNumber(findById.getMobileNumber());
		user.setCity(findById.getCity());
		user.setPinCode(findById.getPinCode());
		user.setUtmSource(findById.getUrchinTrackingModule());
		user.setState(findById.getState());
		// user.setEnachType(findById.getEnachType());
		if (findById.getExperianSummary() != null && findById.getUserProfileRisk() != null) {
			user.setOxyScore(findById.getExperianSummary().getScore() + findById.getUserProfileRisk().getTotalScore());

		}
		if (findById.getProfessionalDetails() != null) {
			user.setCompanyName(findById.getProfessionalDetails().getCompanyName());
			user.setWorkExperience(findById.getProfessionalDetails().getWorkExperience());
		}
		if (findById.getFinancialDetails() != null) {
			user.setSalary(findById.getFinancialDetails().getNetMonthlyIncome());
		}
		if (findById.getBankDetails() != null) {
			user.setAccountNumber(findById.getBankDetails().getAccountNumber());
			user.setBankName(findById.getBankDetails().getBankName());
			user.setBranchName(findById.getBankDetails().getBranchName());
			user.setIfscCode(findById.getBankDetails().getIfscCode());
			user.setBankAddress(findById.getBankDetails().getAddress());
			user.setUserNameAccordingToBank(findById.getBankDetails().getUserName());

		}
		RiskProfileDto riskProfileDto = getReskProfileCalculation(findById.getId());
		user.setRiskProfileDto(riskProfileDto);
	}

	private RiskProfileDto getReskProfileCalculation(Integer userId) {

		RiskProfileDto riskProfileDto = new RiskProfileDto();
		User user = userRepo.findById(userId).get();
		UserProfileRisk userProfileRisk = user.getUserProfileRisk();
		if (ObjectUtils.allNotNull(user, userProfileRisk)) {
			riskProfileDto.setUserId(user.getId());
			riskProfileDto.setCibilScore(userProfileRisk.getCibilScore());
			riskProfileDto.setCompanyOrOrganization(userProfileRisk.getCompanyOrOrganization());
			riskProfileDto
					.setExperianceOrExistenceOfOrganization(userProfileRisk.getExperianceOrExistenceOfOrganization());
			riskProfileDto.setGrade(userProfileRisk.getGrade());
			riskProfileDto.setSalaryOrIncome(userProfileRisk.getSalaryOrIncome());
			return riskProfileDto;
		}
		return null;

	}
	
}
