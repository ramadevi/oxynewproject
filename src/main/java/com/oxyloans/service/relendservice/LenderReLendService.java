package com.oxyloans.service.relendservice;

import com.oxyloans.relend.LenderReLendRequest;
import com.oxyloans.relend.LenderReLendResponse;
import com.oxyloans.relend.LenderReLendSearchRequest;
import com.oxyloans.request.SearchResultsDto;

public interface LenderReLendService {

	public LenderReLendResponse saveReLendConfig(LenderReLendRequest request);

	public LenderReLendResponse getReLendConfigByLenderId(Integer userId);

	public LenderReLendResponse updReLendConfig(LenderReLendRequest request);

	public SearchResultsDto<LenderReLendResponse> searchReLendLenders(LenderReLendSearchRequest request);

	public LenderReLendResponse transferFundsToLenderWallet(LenderReLendSearchRequest request);

}
