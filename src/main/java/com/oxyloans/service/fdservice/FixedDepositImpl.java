package com.oxyloans.service.fdservice;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.transaction.Transactional;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.oxyloans.entity.fd.OxyFdCreation;
import com.oxyloans.repository.fdrepo.OxyFdCreationRepo;
import com.oxyloans.fd.CIBRegistrationRequestDto;
import com.oxyloans.fd.CIBRegistrationResponseDto;
import com.oxyloans.fd.CIBRegistrationStatusResponseDto;
import com.oxyloans.fd.FDCreationRequestDto;
import com.oxyloans.fd.FDCreationResponseDto;
import com.oxyloans.fd.FDFetchRequestDto;
import com.oxyloans.fd.FDLiquidationRequestDto;
import com.oxyloans.fd.FDLiquidationResponseDto;
import com.oxyloans.icici.ServiceImplForICICI;
import com.oxyloans.icici.dto.RequestFromICICI;
import com.oxyloans.icici.upi.UnifiedPaymentsInterfaceService;

@Service
public class FixedDepositImpl implements FixedDepositService {

	@Value("${fdPublickey}")
	private String fdPublickey;

	@Value("${fdPrivatekey}")
	private String fdPrivatekey;

	@Value("${fdCIBRegistration}")
	private String fdCIBRegistration;

	@Value("${fdCIBRegistrationStatus}")
	private String fdCIBRegistrationStatus;

	@Value("${fdCreation}")
	private String fdCreation;

	@Value("${fdFetch}")
	private String fdFetch;

	@Value("${fdLiquidation}")
	private String fdLiquidation;

	private Client client = ClientBuilder.newClient().register(MultiPartFeature.class).register(MultiPartWriter.class);

	private final Logger logger = LogManager.getLogger(getClass());

	@Autowired
	private UnifiedPaymentsInterfaceService unifiedPaymentsInterfaceService;

	@Autowired
	private OxyFdCreationRepo oxyFdCreationRepo;

	private final DateFormat expectedDateFormat = new SimpleDateFormat("yyyyMMdd");

	@Autowired
	private ServiceImplForICICI serviceImplForICICI;

	static final String SYMM_CIPHER = "AES/CBC/PKCS5PADDING";
	static final String ASYMM_CIPHER = "RSA/ECB/PKCS1Padding";
	static final String KEYSTORE_FILE = "fdPrivatekey";
	static final String KEYSTORE_PWD = "123456789";
	static final String KEYSTORE_ALIAS = "ICICI";
	static final String KEYSTORE_INSTANCE = "PKCS12";
	static final String PUBLIC_KEY = "fdPublickey";

	@Override
	@Transactional
	public String cibRegistration(CIBRegistrationRequestDto cibRegistrationRequestDto) {
		String decryptedcibData = null;
		try {
			Gson gson = new Gson();
			String cibRegistrationObject = gson.toJson(cibRegistrationRequestDto);

			logger.info("cibRegistrationObject" + cibRegistrationObject);

			String encrytedcibData = unifiedPaymentsInterfaceService.generateEncrypetedKey(cibRegistrationObject,
					fdPublickey);

			logger.info("encrytedcibData" + encrytedcibData);

			decryptedcibData = createCIBRegistation(encrytedcibData);

			logger.info("decryptedcibData" + decryptedcibData);
		} catch (Exception e) {
			logger.info("Exception in cibRegistration method");
		}
		return decryptedcibData;

	}

	public String createCIBRegistation(String encrytedCIBData) {
		String fdDecrytedResponseCIB = null;
		try {
			WebTarget webTarget = client.target(fdCIBRegistration);
			Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN).accept(MediaType.TEXT_PLAIN);

			MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
			map.add("Content-Type", "text/plain");
			map.add("apikey", "r4YMkqUmnloEKMBiUSAVnyKSrgjkZOpU");

			invocationBuilder.headers(map);

			Response response = invocationBuilder.post(Entity.entity(encrytedCIBData, MediaType.TEXT_PLAIN));

			String fdResponseCIB = response.readEntity(String.class);

			logger.info("fdResponseCIB" + fdResponseCIB);

			fdDecrytedResponseCIB = unifiedPaymentsInterfaceService.generateDecryptedKey(fdResponseCIB, fdPrivatekey);

			logger.info("fdDecrytedResponseCIB" + fdDecrytedResponseCIB);

			Gson gson = new Gson();
			CIBRegistrationResponseDto cibRegistrationResponseDto = gson.fromJson(fdDecrytedResponseCIB,
					CIBRegistrationResponseDto.class);

			logger.info("cibRegistrationResponseDto" + cibRegistrationResponseDto);

		} catch (Exception e) {
			logger.info("Exception in createCIBRegistation method");
		}
		return fdDecrytedResponseCIB;

	}

	@Override
	@Transactional
	public String cibRegistrationStatusCheck(CIBRegistrationRequestDto cibRegistrationRequestDto) {
		// try {
		Gson gson = new Gson();

		String cibRegistrationStatusData = gson.toJson(cibRegistrationRequestDto);

		String encrytedcibStatusData = unifiedPaymentsInterfaceService.generateEncrypetedKey(cibRegistrationStatusData,
				fdPublickey);
		String decrytedcibstatusResponse = cibRegistrationStatus(encrytedcibStatusData);
		/*
		 * } catch (Exception e) {
		 * logger.info("Exception in cibRegistrcationStatusCheck method"); }
		 */

		return decrytedcibstatusResponse;

	}

	public String cibRegistrationStatus(String encrytedcibStatusData) {
		String decrytedResponse = null;
		// try {
		WebTarget webTarget = client.target(fdCIBRegistrationStatus);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN).accept(MediaType.TEXT_PLAIN);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
		map.add("Content-Type", "text/plain");
		map.add("apikey", "r4YMkqUmnloEKMBiUSAVnyKSrgjkZOpU");
		invocationBuilder.headers(map);

		Response response = invocationBuilder.post(Entity.entity(encrytedcibStatusData, MediaType.TEXT_PLAIN));
		String cibStatus = response.readEntity(String.class);

		logger.info("fdResponseCIBStatus" + cibStatus);

		decrytedResponse = unifiedPaymentsInterfaceService.generateDecryptedKey(cibStatus, fdPrivatekey);

		logger.info("fdDecrytedResponseCIBStatus" + decrytedResponse);

		Gson gson = new Gson();
		CIBRegistrationStatusResponseDto cibRegistrationStatusResponseDto = gson.fromJson(decrytedResponse,
				CIBRegistrationStatusResponseDto.class);

		logger.info("cibRegistrationStatusResponseDto" + cibRegistrationStatusResponseDto);

		/*
		 * }catch(Exception e){
		 * logger.info("Exception in cibRegistrationStatus method"); }
		 */

		return decrytedResponse;

	}

	@Override
	// @Transactional
	public String fixedDeposit(int userId, FDCreationRequestDto fdCreationRequestDto) {
		logger.info("fixedDeposit method start");
		OxyFdCreation oxyFdCreation = null;
		oxyFdCreation = oxyFdCreationRepo.fdStatusUpdationUsingUserid(userId);
		if (oxyFdCreation == null) {
			oxyFdCreation = new OxyFdCreation();
		}
		oxyFdCreation.setUserId(userId);

		Random rnd = new Random();
		String otp = String.format("%06d", rnd.nextInt(999999));
		Date date = new Date();
		String currentDate = expectedDateFormat.format(date.getTime());
		fdCreationRequestDto.setUNIQUE_ID("OXYIDEAS" + currentDate + otp);

		oxyFdCreation.setUniqueNumber("OXYIDEAS" + currentDate + otp);
		oxyFdCreation.setAccountNumber(fdCreationRequestDto.getACCOUNT());
		oxyFdCreation.setDaysOnly(Integer.valueOf(fdCreationRequestDto.getDAYS_ONLY()));
		oxyFdCreation.setFdAmount(Integer.valueOf(fdCreationRequestDto.getFD_AMOUNT()));
		oxyFdCreation.setTypeOfDeposit(OxyFdCreation.TypeOfDeposit.valueOf(fdCreationRequestDto.getTYPEOFDEPOSIT()));
		oxyFdCreation.setDays(Integer.valueOf(fdCreationRequestDto.getDAYS()));
		oxyFdCreation.setMonths(Integer.valueOf(fdCreationRequestDto.getMONTHS()));
		oxyFdCreation.setYears(Integer.valueOf(fdCreationRequestDto.getYEARS()));
		oxyFdCreation.setFdType(OxyFdCreation.FdType.valueOf(fdCreationRequestDto.getFD_TYPE()));
		oxyFdCreation.setMaturityProceeds(fdCreationRequestDto.getMATURITY_PROCEEDS());
		oxyFdCreation.setCreatedOn(new Date());
		oxyFdCreationRepo.save(oxyFdCreation);

		Gson gson = new Gson();
		String fdRequestcreation = gson.toJson(fdCreationRequestDto);
		logger.info("fdRequestcreation" + fdRequestcreation);

		String encrytedFdCreationData = unifiedPaymentsInterfaceService.generateEncrypetedKey(fdRequestcreation,
				fdPublickey);
		logger.info("encrytedFdCreationData encrypted" + encrytedFdCreationData);

		String fdCreationResponse = createFixedDeposit(encrytedFdCreationData, userId);
		logger.info("fixedDeposit method end");
		return fdCreationResponse;
		// return null;

	}

	public String createFixedDeposit(String encrytedFdCreationData, int userId) {
		logger.info("createFixedDeposit method start");
		WebTarget webTarget = client.target(fdCreation);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN).accept(MediaType.TEXT_PLAIN);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
		map.add("Content-Type", "text/plain");
		map.add("apikey", "r4YMkqUmnloEKMBiUSAVnyKSrgjkZOpU");
		invocationBuilder.headers(map);

		Response response = invocationBuilder.post(Entity.entity(encrytedFdCreationData, MediaType.TEXT_PLAIN));
		String fdCreationResponseBeforeDecryption = response.readEntity(String.class);
		logger.info("fdCreationResponseBeforeDecryption" + fdCreationResponseBeforeDecryption);

		String fdCreationDecrytedResponse = unifiedPaymentsInterfaceService
				.generateDecryptedKey(fdCreationResponseBeforeDecryption, fdPrivatekey);

		logger.info("fdCreationDecrytedResponse" + fdCreationDecrytedResponse);
		Gson gson = new Gson();
		FDCreationResponseDto fdCreationResponseDto = gson.fromJson(fdCreationDecrytedResponse,
				FDCreationResponseDto.class);
		logger.info("fdCreationResponseDto status" + fdCreationResponseDto.getResponse());

		OxyFdCreation oxyFdCreation = oxyFdCreationRepo.fdStatusUpdationUsingUserid(userId);
		if (oxyFdCreation != null) {
			oxyFdCreation.setFdResponse(fdCreationDecrytedResponse);
			logger.info("fdCreationResponse" + fdCreationResponseDto.getResponse());
			if (fdCreationResponseDto.getResponse().equalsIgnoreCase("SUCCESS")) {
				oxyFdCreation.setStatus(OxyFdCreation.Status.CREATED);

			}
			oxyFdCreation.setFdResponseReceivedOn(new Date());
			oxyFdCreationRepo.save(oxyFdCreation);
		} else {
			logger.info("user not present" + userId);
		}
		return fdCreationDecrytedResponse;

	}

	@Override
	@Transactional
	public String fdFetch(FDFetchRequestDto fdFetchRequestDto) {
		String iciciResult = null;
		try {
			Gson gson = new Gson();
			String fdFetchRequest = gson.toJson(fdFetchRequestDto);
			logger.info("fdFetchRequestData" + fdFetchRequest);

			String encrytedFdCreationData = unifiedPaymentsInterfaceService.generateEncrypetedKey(fdFetchRequest,
					fdPublickey);

			logger.info("fdFetchRequestData after encryption process" + encrytedFdCreationData);

			iciciResult = fdStatusFetch(encrytedFdCreationData);

		} catch (Exception e) {
			logger.info("fdFetch method" + e.getMessage());
		}
		return iciciResult;

	}

	public String fdStatusFetch(String encrytedFdFetchData)
			throws InvalidKeyException, UnrecoverableKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			CertificateException, IllegalBlockSizeException, BadPaddingException, KeyStoreException, IOException {
		logger.info("fdStatusFetch method start" + encrytedFdFetchData);

		WebTarget webTarget = client.target(fdFetch);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN).accept(MediaType.TEXT_PLAIN);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
		map.add("Content-Type", "text/plain");
		map.add("apikey", "HQviHHvAHoz2UGWG3njmcJCY73Unv8UQ");
		//map.add("apikey", "r4YMkqUmnloEKMBiUSAVnyKSrgjkZOpU");
		

		invocationBuilder.headers(map);

		Response response = invocationBuilder.post(Entity.entity(encrytedFdFetchData, MediaType.TEXT_PLAIN));

		String fdFetchResponseBeforeDecryption = response.readEntity(String.class);
		logger.info("fdFetchResponseBeforeDecryption" + fdFetchResponseBeforeDecryption);

		Gson gson = new Gson();

		RequestFromICICI requestFromICICI = gson.fromJson(fdFetchResponseBeforeDecryption, RequestFromICICI.class);

		logger.info("requestFromICICIkeyfd" + requestFromICICI.getEncryptedKey());
		logger.info("requestFromICICIdatafd" + requestFromICICI.getEncryptedData());

		String sessionKey = "qqqqwwww11112222";

		String decryptedPayload = decryptSymm(sessionKey, requestFromICICI.getEncryptedData());
		logger.info("decryptedPayload fd " + String.format("DecryptedPayload :: %s", decryptedPayload));

		String plainKey = decryptAsymm(requestFromICICI.getEncryptedKey(), fdPrivatekey);

		logger.info("plainKey fd " + String.format("DecryptedSessionKey :: %s", plainKey));

		return fdFetchResponseBeforeDecryption;

	}

	public static String encryptSymm(String key, String initVector, String value) {
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance(SYMM_CIPHER);
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
			byte[] encrypted = cipher.doFinal(value.getBytes());
//                  System.out.println("encrypted string: " + new String(Utility.b64Encode(encrypted)));
			byte[] c = new byte[initVector.getBytes("UTF-8").length + encrypted.length];
			System.arraycopy(initVector.getBytes("UTF-8"), 0, c, 0, initVector.getBytes("UTF-8").length);
			System.arraycopy(encrypted, 0, c, initVector.getBytes("UTF-8").length, encrypted.length);
			return Base64.getEncoder().encodeToString(c);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;

	}

	public static String decryptSymm(final String key, final String encryptedStr) {
		try {
			byte[] encrypted = Base64.getDecoder().decode(encryptedStr.getBytes());
			byte[] iv = Arrays.copyOfRange(encrypted, 0, 16);
			byte[] ciphertext = Arrays.copyOfRange(encrypted, iv.length, encrypted.length);
			IvParameterSpec ivPS = new IvParameterSpec(iv);
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance(SYMM_CIPHER);
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivPS);
			byte[] original = cipher.doFinal(ciphertext);
			return new String(original);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public static String encryptAsymm(String b64Msg, String filePath)
			throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, CertificateException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance(ASYMM_CIPHER);
		Key key = loadPublicKeyFromFile(filePath);
		byte[] msg = Base64.getDecoder().decode(b64Msg);
//            System.out.println("MSG :: " + new String(msg));
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] encryptedMsg = cipher.doFinal(msg);
		return Base64.getEncoder().encodeToString(encryptedMsg);
	}

	public static String decryptAsymm(String b64EncryptedMsg, String filePath)
			throws NoSuchAlgorithmException, NoSuchPaddingException, CertificateException, InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException, UnrecoverableKeyException, KeyStoreException, IOException {
		Cipher cipher = Cipher.getInstance(ASYMM_CIPHER);
		Key key = loadPrivateKeyFromFile(filePath);
		byte[] encryptedMsg = Base64.getDecoder().decode(b64EncryptedMsg);
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] decryptedMsg = cipher.doFinal(encryptedMsg);
		return new String(decryptedMsg);
	}

	private static Key loadPublicKeyFromFile(String publicKeyPath) throws CertificateException, FileNotFoundException {
		Key key = null;
		X509Certificate x509Certificate = createCert(publicKeyPath);
		key = x509Certificate.getPublicKey();
		System.out.println("Issuer ::" + x509Certificate.getIssuerDN());
		return key;
	}

	private static X509Certificate createCert(String filePath) {
		try {
			CertificateFactory cf = CertificateFactory.getInstance("X509");
			X509Certificate cert = (X509Certificate) cf.generateCertificate(new FileInputStream(filePath));
			return cert;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static Key loadPrivateKeyFromFile(String privateKeyPath) throws CertificateException, KeyStoreException,
			NoSuchAlgorithmException, IOException, UnrecoverableKeyException {

		Key key = null;
		KeyStore keyStore = KeyStore.getInstance(KEYSTORE_INSTANCE);
		keyStore.load(new FileInputStream(privateKeyPath), KEYSTORE_PWD.toCharArray());
		key = keyStore.getKey(KEYSTORE_ALIAS, KEYSTORE_PWD.toCharArray());
		return key;
	}

	@Override
	@Transactional
	public String fdLiquidation(FDLiquidationRequestDto fdLiquidationRequestDto) {

		Gson gson = new Gson();
		String fdLiquidationData = gson.toJson(fdLiquidationRequestDto);
		logger.info("fdLiquidationData" + fdLiquidationData);

		String encrytedFdLiquidationData = unifiedPaymentsInterfaceService.generateEncrypetedKey(fdLiquidationData,
				fdPublickey);
		logger.info("encrytedFdLiquidationData encrypted" + encrytedFdLiquidationData);
		String liquidationResponse = removingFundsUsingLiquidation(encrytedFdLiquidationData,
				fdLiquidationRequestDto.getACC_ID());
		return liquidationResponse;

	}

	public String removingFundsUsingLiquidation(String encrytedFdLiquidationData, String accountNo) {

		WebTarget webTarget = client.target(fdLiquidation);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN).accept(MediaType.TEXT_PLAIN);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
		map.add("Content-Type", "text/plain");
		map.add("apikey", "r4YMkqUmnloEKMBiUSAVnyKSrgjkZOpU");
		invocationBuilder.headers(map);

		Response response = invocationBuilder.post(Entity.entity(encrytedFdLiquidationData, MediaType.TEXT_PLAIN));
		String fdLiquidationBeforeDecryption = response.readEntity(String.class);
		logger.info("fdLiquidationBeforeDecryption" + fdLiquidationBeforeDecryption);

		String fdLiquidationDecrytedResponse = unifiedPaymentsInterfaceService
				.generateDecryptedKey(fdLiquidationBeforeDecryption, fdPrivatekey);

		logger.info("fdLiquidationDecrytedResponse" + fdLiquidationDecrytedResponse);

		Gson gson = new Gson();
		FDLiquidationResponseDto fdLiquidationResponseDto = gson.fromJson(fdLiquidationDecrytedResponse,
				FDLiquidationResponseDto.class);
		OxyFdCreation oxyFdCreation = oxyFdCreationRepo.searchUserByAccountNumber(accountNo);
		if (oxyFdCreation != null) {
			logger.info("fdLiquidationResponse" + fdLiquidationResponseDto.getResponse());

			if (fdLiquidationResponseDto.getResponse().equalsIgnoreCase("SUCCESS")) {
				oxyFdCreation.setStatus(OxyFdCreation.Status.CLOSED);

			}
			oxyFdCreation.setFdClosedResponse(fdLiquidationDecrytedResponse);
			oxyFdCreation.setFdClosedOn(new Date());
			oxyFdCreationRepo.save(oxyFdCreation);
		}

		return fdLiquidationDecrytedResponse;

	}

}
