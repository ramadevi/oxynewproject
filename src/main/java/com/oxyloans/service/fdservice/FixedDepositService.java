package com.oxyloans.service.fdservice;

import com.oxyloans.fd.CIBRegistrationRequestDto;
import com.oxyloans.fd.FDCreationRequestDto;
import com.oxyloans.fd.FDFetchRequestDto;
import com.oxyloans.fd.FDLiquidationRequestDto;

public interface FixedDepositService {

	public String cibRegistration(CIBRegistrationRequestDto cibRegistrationRequestDto);

	public String cibRegistrationStatusCheck(CIBRegistrationRequestDto cibRegistrationRequestDto);

	public String fixedDeposit(int userId, FDCreationRequestDto fdCreationRequestDto);

	public String fdFetch(FDFetchRequestDto fdFetchRequestDto);

	public String fdLiquidation(FDLiquidationRequestDto fdLiquidationRequestDto);

}
