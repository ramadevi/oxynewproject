package com.oxyloans.service.whatappservice;

import java.util.List;

import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.request.user.WhatsappAttachmentRequestDto;
import com.oxyloans.response.user.WtappGroupResponseDto;
import com.oxyloans.whatsappdto.MessagesResponseDto;
import com.oxyloans.whatsappdto.WhatsappMessagesDto;

public interface WhatappServiceRepo {

	public WtappGroupResponseDto createWhatAppGroupLink(String dealName);

	public String sendingWhatAppGroupLink(OxyBorrowersDealsInformation oxyBorrowersDealsInformation, String url,
			int dealId);

	public String sendngWhatAppNotification(String mobileNumber, String dealName);

	public String sendingWhatsappMessages(String chatId, String message);

	public WhatsappMessagesDto readingWhatsappMessages(String chatId);

	public WhatsappMessagesDto readingWhatsappMessagesHistoryFalse(String chatId, int lastMessageNumber);

	public void sendingWhatsappMessagestoMultipleGroup(List<String> chatIds, String message, String whatsAppsendApi);

	public void sendMessagesWithMobileNumbers(List<String> mobileNumbers, String message, String whatsAppsendApi);

	public WhatsappMessagesDto readingWhatsappMessagesWithOutChatIdHistoryFalse(int lastMessageNumber);

	public String sendingWhatsappMsgWithMobileNumberToGroup(String mobileNumber, String message, String sendApi);

	public String sendingWhatsappMessages(String chatId, String message, String chatApi);

	public String sendAttatchements(WhatsappAttachmentRequestDto dto);

	public String sendingWhatsappMsgWithMobileNumber(String mobileNumber, String message, String wtappApi, String type);

	public String sendingWhatsappMsgBasedOnPartnerAction(String firstNumber, String firstMessage, String secondNumber,
			String secondMessage);

	public String whatsappMessageToGroupUsingUltra(String chatId, String message);

	public String sendingIndividualMessageUsingUltraApi(String mobileNumber, String message);

	public String partnerActionUsingUltra(String firstNumber, String firstMessage, String secondNumber,
			String secondMessage);

	public MessagesResponseDto whatsappGroupChatid(String dealName);

	public String whatsappImageUsingUltra(String senderId, String image, String message);

	public String sendingWhatsappMessageWithNewInstance(String chatId, String message);

	public String sendingMsgToUsingUltra(OxyBorrowersDealsInformation oxyBorrowersDealsInformation, int dealId,
			String chatIds);

	public String whatsappImageUsingUltraUsingNew(String senderId, String image, String message);

}
