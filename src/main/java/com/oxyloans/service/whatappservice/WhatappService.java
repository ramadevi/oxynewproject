package com.oxyloans.service.whatappservice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entityborrowersdealsdto.OxyLendersAcceptedDealsRequestDto;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.OxyDealsRateofinterestToLendersgroup;
import com.oxyloans.entity.borrowers.deals.information.OxyDealsRateofinterestToLendersgroupRepo;
import com.oxyloans.entity.user.User;
import com.oxyloans.repository.whatsapprepo.WhatappGroupsInformationRepo;
import com.oxyloans.mobile.MobileRequest;
import com.oxyloans.mobile.MobileUtil;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.user.WhatsappAttachmentRequestDto;
import com.oxyloans.response.user.WtappGroupRequestDto;
import com.oxyloans.response.user.WtappGroupResponseDto;
import com.oxyloans.response.user.WtappIndividualGroupRequestDto;
import com.oxyloans.response.user.WtappRequestDto;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.whatsappdto.ContactsResponseDto;
import com.oxyloans.whatsappdto.MessagesResponseDto;
import com.oxyloans.whatsappdto.WhatsappImageUsingUltra;
import com.oxyloans.whatsappdto.WhatsappMessagesDto;

@Service
public class WhatappService implements WhatappServiceRepo {

	@Value("${wtappApiForCreatingGroup}")
	private String wtappApiForCreatingGroup;

	@Value("${wtappApi}")
	private String wtappApi;

	@Value("${whatsAppMessagesApi}")
	private String whatsAppMessagesApi;

	@Value("${whatsAppMessagesApiNewInstance}")
	private String whatsAppMessagesApiNewInstance;

	@Value("${whatsappgroups}")
	private String whatsappgroups;

	private Client client = ClientBuilder.newClient().register(MultiPartFeature.class).register(MultiPartWriter.class);

	private final Logger logger = LogManager.getLogger(WhatappService.class);

	protected final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Autowired
	private WhatappGroupsInformationRepo whatappGroupsInformationRepo;

	@Autowired
	private OxyDealsRateofinterestToLendersgroupRepo oxyDealsRateofinterestToLendersgroupRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private IEmailService emailService;

	@Value("${whatsAppAttachmentApi}")
	private String whatsAppAttachmentApi;

	@Value("${whatsappUsingUltraApi}")
	private String whatsappUsingUltraApi;

	@Value("${whatsappUltraToken}")
	private String whatsappUltraToken;

	@Value("${chatidsUsingUltraApi}")
	private String chatidsUsingUltraApi;

	@Value("${whatsappImageUsingUltra}")
	private String whatsappImageUsingUltra;

	@Autowired
	protected MobileUtil mobileUtil;

	@Value("${whatsappUsingUltraNewInstance}")
	private String whatsappUsingUltraNewInstance;

	@Value("${whatsappUltraTokenForNewInstance}")
	private String whatsappUltraTokenForNewInstance;

	@Override
	public WtappGroupResponseDto createWhatAppGroupLink(String dealName) {
		logger.info("createWhatAppGroupLink method start");
		WebTarget webTarget = client.target(wtappApiForCreatingGroup);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");
		invocationBuilder.headers(map);

		WtappGroupRequestDto wtappGroupRequestDto = new WtappGroupRequestDto();
		wtappGroupRequestDto.setGroupName(dealName);
		wtappGroupRequestDto.setLinkDescription(dealName);
		wtappGroupRequestDto.setLinkTitle(dealName);
		wtappGroupRequestDto.setLinkUrl(dealName);
		String messageForGroup = "OxyLoans is Launching New Deal, it is about earning 4% pm, if ur interested join the the group test";
		wtappGroupRequestDto.setMessage(messageForGroup);
		wtappGroupRequestDto.setPhoneNumber("919966888825");
		wtappGroupRequestDto.setPreviewBase64("String");

		Response response = invocationBuilder.post(Entity.json(wtappGroupRequestDto));

		String groupResponse = response.readEntity(String.class);

		Gson gson = new Gson();
		WtappGroupResponseDto wtappGroupResponseDto = gson.fromJson(groupResponse, WtappGroupResponseDto.class);
		logger.info("createWhatAppGroupLink method end");
		return wtappGroupResponseDto;

	}

	@Override
	public String sendingWhatAppGroupLink(OxyBorrowersDealsInformation oxyBorrowersDealsInformation, String url,
			int dealId) {
		String message1 = null;
		String messageToLender = null;

		WebTarget webTargetToGroup = client.target(wtappApi);
		Invocation.Builder invocationBuilderToGroup = webTargetToGroup.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map1 = new MultivaluedHashMap<String, Object>();

		map1.add("Content-Type", "application/json");
		invocationBuilderToGroup.headers(map1);
		if (dealId == 0) {
			String line0 = "Dear Lenders, I have. Created a deal and the details are as follows. \n";
			String line1 = "Deal Name : " + oxyBorrowersDealsInformation.getDealName() + "\n";
			BigInteger amount = BigDecimal.valueOf(oxyBorrowersDealsInformation.getDealAmount()).toBigInteger();
			String line2 = "Deal value in  INR : " + amount + "\n";
			String line3 = "Funds Acceptance Start Date : "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getFundsAcceptanceStartDate()) + "\n";
			String line4 = "Funds Acceptance End Date : "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getFundsAcceptanceEndDate()) + "\n";
			String line5 = "Tenure in Months : " + oxyBorrowersDealsInformation.getDuration() + "\n";
			String line6 = "Interest PaymentDate : "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getLoanActiveDate()) + "\n";
			String line7 = "Project URL : " + oxyBorrowersDealsInformation.getDealLink() + "\n";
			String line8 = "Lender Participation limit : "
					+ BigDecimal.valueOf(oxyBorrowersDealsInformation.getPaticipationLimitToLenders()).toBigInteger()
					+ "\n";
			String line9 = "If you wish to participate,Please join through the below link. \n";
			String line10 = " ";
			if (oxyBorrowersDealsInformation.getWhatappMessageToLenders() != null
					&& oxyBorrowersDealsInformation.getWhatappMessageToLenders().toString().length() > 0) {
				line10 = oxyBorrowersDealsInformation.getWhatappMessageToLenders();
			}
			if (!line6.equals(null)) {
				messageToLender = line0 + "\n" + line1 + "\n" + line2 + "\n" + line3 + "\n" + line4 + "\n" + line5
						+ "\n" + line6 + "\n" + line7 + "\n" + line8 + "\n" + line9 + "\n" + line10;
			} else {
				messageToLender = line0 + "\n" + line1 + "\n" + line2 + "\n" + line3 + "\n" + line4 + "\n" + line5
						+ "\n" + line7 + "\n" + line8 + "\n" + line9 + "\n" + line10;
			}

		} else {
			String line0 = "Dear Lenders, I have Edited a deal and the details are as follows. \n";
			String line1 = "Deal Name : " + oxyBorrowersDealsInformation.getDealName() + "\n";
			BigInteger amount = BigDecimal.valueOf(oxyBorrowersDealsInformation.getDealAmount()).toBigInteger();
			String line2 = "Deal value in  INR : " + amount + "\n";
			String line3 = "Funds Acceptance Start Date : "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getFundsAcceptanceStartDate()) + "\n";
			String line4 = "Funds Acceptance End Date : "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getFundsAcceptanceEndDate()) + "\n";
			String line5 = "Tenure in Months : " + oxyBorrowersDealsInformation.getDuration() + "\n";
			String line6 = "Interest PaymentDate : "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getLoanActiveDate()) + "\n";
			String line7 = "Project URL : " + oxyBorrowersDealsInformation.getDealLink() + "\n";

			String line8 = "Lender Participation limit : "
					+ BigDecimal.valueOf(oxyBorrowersDealsInformation.getPaticipationLimitToLenders()).toBigInteger()
					+ "\n";
			String line9 = "If you wish to participate,Please join through the below link. \n";
			String line10 = " ";
			if (oxyBorrowersDealsInformation.getWhatappMessageToLenders() != null
					&& oxyBorrowersDealsInformation.getWhatappMessageToLenders().toString().length() > 0) {
				line10 = oxyBorrowersDealsInformation.getWhatappMessageToLenders();
			}
			if (!line6.equals(null)) {
				messageToLender = line0 + "\n" + line1 + "\n" + line2 + "\n" + line3 + "\n" + line4 + "\n" + line5
						+ "\n" + line6 + "\n" + line7 + "\n" + line8 + "\n" + line9 + "\n" + line10;
			} else {
				messageToLender = line0 + "\n" + line1 + "\n" + line2 + "\n" + line3 + "\n" + line4 + "\n" + line5
						+ "\n" + line7 + "\n" + line8 + "\n" + line9 + "\n" + line10;
			}

		}

		String listOfGroupNames = oxyBorrowersDealsInformation.getLinkSentToLendersGroup();
		String[] names = listOfGroupNames.split(",");
		String chatid = null;

		String messageTowhatapp = null;
		// if
		// (oxyBorrowersDealsInformation.getDealType().equals(OxyBorrowersDealsInformation.DealType.SCROW))
		// {
		messageTowhatapp = "*New Method for Amount Confirmation Commitment:*\r\n" + "\r\n" + "Send as follows :"
				+ "\r\n" + "'virtualaccountid' commitment amount" + "\r\n"
				+ "For ex: OXYLRV1234 5L if you wish to commit for 5L" + ".\n\n"
				+ "*This is a system generated message.*";
		/*
		 * } else { messageTowhatapp =
		 * "*New Method for Amount Confirmation Commitment:*\r\n" + "\r\n" +
		 * oxyBorrowersDealsInformation.getPaticipationLimitToLenders() + "\r\n" +
		 * "Send  'virtualaccountid' and type A/B according to your commitment amount.\r\n"
		 * + "\r\n" + "For ex: OXYLRV1234 A if you wish to commit for 50k" + ".\n\n" +
		 * "*This is a system generated message.*"; }
		 */
		for (int i = 0; i < names.length; i++) {

			chatid = names[i];
			if (chatid != null) {
				for (int i1 = 0; i1 <= 2; i1++) {
					WtappIndividualGroupRequestDto wtappIndividualGroupRequestDto = new WtappIndividualGroupRequestDto();
					if (i1 == 0) {

						/*
						 * wtappIndividualGroupRequestDto.setMessage(messageToLender);
						 * 
						 * wtappIndividualGroupRequestDto.setChatId(chatid);
						 * wtappIndividualGroupRequestDto.setLinkTitle(oxyBorrowersDealsInformation.
						 * getDealName());
						 * 
						 * wtappIndividualGroupRequestDto.setLinkUrl(url);
						 * wtappIndividualGroupRequestDto.setLinkflag(true);
						 */
						Response responseForSingleLender = invocationBuilderToGroup
								.post(Entity.json(wtappIndividualGroupRequestDto));

						if (responseForSingleLender.getStatus() == 200) {

							message1 = "success";
						} else {
							logger.info("message not sent to chat id: " + chatid);

						}
					} else if (i1 == 1) {

						String note = "*Note :* \n*Please add deal name " + oxyBorrowersDealsInformation.getDealName()
								+ " in Remarks while Transferring funds to your virtual account*";
						/*
						 * wtappIndividualGroupRequestDto.setMessage(note);
						 * 
						 * wtappIndividualGroupRequestDto.setChatId(chatid);
						 * wtappIndividualGroupRequestDto.setLinkTitle(oxyBorrowersDealsInformation.
						 * getDealName()); wtappIndividualGroupRequestDto.setLinkUrl(" ");
						 * wtappIndividualGroupRequestDto.setLinkflag(false);
						 */
						Response responseForSingleLender = invocationBuilderToGroup
								.post(Entity.json(wtappIndividualGroupRequestDto));

						if (responseForSingleLender.getStatus() == 200) {

							message1 = "success";
						} else {
							logger.info("message not sent to chat id: " + chatid);

						}
					} else {

						/*
						 * wtappIndividualGroupRequestDto.setMessage(messageTowhatapp);
						 * 
						 * wtappIndividualGroupRequestDto.setChatId(chatid);
						 * wtappIndividualGroupRequestDto.setLinkTitle(oxyBorrowersDealsInformation.
						 * getDealName()); wtappIndividualGroupRequestDto.setLinkUrl(" ");
						 * wtappIndividualGroupRequestDto.setLinkflag(false);
						 */
						Response responseForSingleLender = invocationBuilderToGroup
								.post(Entity.json(wtappIndividualGroupRequestDto));

						if (responseForSingleLender.getStatus() == 200) {

							message1 = "success";
						} else {
							logger.info("message not sent to chat id: " + chatid);

						}
					}

				}
			}
		}

		return message1;

	}

	@Override
	public String sendngWhatAppNotification(String mobileNumber, String dealName) {
		String status = null;
		WebTarget webTarget = client.target(wtappApi);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");
		invocationBuilder.headers(map);
		User user = userRepo.findByMobileNumber(mobileNumber);
		String lenderName = null;
		if (user != null) {
			lenderName = user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName();
		}

		String message = " Hi " + lenderName + " you have not paticipated in " + dealName + " Please paticipate";

		WtappRequestDto wtappRequestDto = new WtappRequestDto();
		wtappRequestDto.setMessage(message);
		wtappRequestDto.setPhoneNumber(mobileNumber);

		Response response = invocationBuilder.post(Entity.json(wtappRequestDto));

		if (response.getStatus() == 200) {

			status = "messageSent";
		} else {
			logger.info("message not sent : " + mobileNumber);
			status = "messageNotSent";
		}
		return status;
	}

	@Override
	public String sendingWhatsappMessages(String chatId, String message) {
		String status = null;
		WebTarget webTarget = client.target(wtappApi);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");

		invocationBuilder.headers(map);

		WtappGroupRequestDto wtappGroupRequestDto = new WtappGroupRequestDto();

		wtappGroupRequestDto.setMessage(message);
		wtappGroupRequestDto.setChatId(chatId);

		Response response = invocationBuilder.post(Entity.json(wtappGroupRequestDto));

		if (response.getStatus() == 200) {

			status = "messageSent";
		} else {

			status = "messageNotSent";
		}

		return status;

	}

	@Override
	public WhatsappMessagesDto readingWhatsappMessages(String chatId) {

		WebTarget webTarget = client.target(whatsAppMessagesApi);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");

		invocationBuilder.headers(map);

		WtappGroupRequestDto input = new WtappGroupRequestDto();

		input.setChatId(chatId);

		input.setHistory(true);

		Response response = invocationBuilder.post(Entity.json(input));

		String groupResponse = response.readEntity(String.class);

		Gson gson = new Gson();

		WhatsappMessagesDto messageDto = gson.fromJson(groupResponse, WhatsappMessagesDto.class);

		return messageDto;
	}

	public String message(double participationAmount) {
		String message = null;
		if (participationAmount == 50000) {
			message = "Type *A* for INR 50000\r\n";
		} else if (participationAmount == 100000) {
			message = "Type *A* for INR 50000\r\n Type *B* for INR 100000\r\n";
		} else if (participationAmount == 150000) {
			message = "Type *A* for INR 50000\r\n Type *B* for INR 100000\r\n Type *C* for INR 150000\r\n";
		} else if (participationAmount == 200000) {
			message = "Type *A* for INR 50000\r\n Type *B* for INR 100000\r\n Type *C* for INR 150000\r\n Type *D* for INR 200000\r\n";
		} else if (participationAmount == 250000) {
			message = "Type *A* for INR 50000\r\n Type *B* for INR 100000\r\n Type *C* for INR 150000\r\n Type *D* for INR 200000\r\n Type *E* for INR 250000\r\n";
		} else if (participationAmount == 300000) {
			message = "Type *A* for INR 50000\r\n Type *B* for INR 100000\r\n Type *C* for INR 150000\r\n Type *D* for INR 200000\r\n Type *E* for INR 250000\r\n Type *F* for INR 300000\r\n";
		} else if (participationAmount == 350000) {
			message = "Type *A* for INR 50000\r\n Type *B* for INR 100000\r\n Type *C* for INR 150000\r\n Type *D* for INR 200000\r\n Type *E* for INR 250000\r\n Type *F* for INR 300000\r\n Type *G* for INR 350000\r\n";
		} else if (participationAmount == 400000) {
			message = "Type *A* for INR 50000\r\n Type *B* for INR 100000\r\n Type *C* for INR 150000\r\n Type *D* for INR 200000\r\n Type *E* for INR 250000\r\n Type *F* for INR 300000\r\n Type *G* for INR 350000\r\n Type *H* for INR 400000\r\n";
		} else if (participationAmount == 450000) {
			message = "Type *A* for INR 50000\r\n Type *B* for INR 100000\r\n Type *C* for INR 150000\r\n Type *D* for INR 200000\r\n Type *E* for INR 250000\r\n Type *F* for INR 300000\r\n Type *G* for INR 350000\r\n Type *H* for INR 400000\r\n Type *I* for INR 450000\r\n";
		} else if (participationAmount == 500000) {
			message = "Type *A* for INR 50000\r\n Type *B* for INR 100000\r\n Type *C* for INR 150000\r\n Type *D* for INR 200000\r\n Type *E* for INR 250000\r\n Type *F* for INR 300000\r\n Type *G* for INR 350000\r\n Type *H* for INR 400000\r\n Type *I* for INR 450000\r\n Type *J* for INR 500000\r\n";
		} else if (participationAmount == 550000) {
			message = "Type *A* for INR 50000\r\n Type *B* for INR 100000\r\n Type *C* for INR 150000\r\n Type *D* for INR 200000\r\n Type *E* for INR 250000\r\n Type *F* for INR 300000\r\n Type *G* for INR 350000\r\n Type *H* for INR 400000\r\n Type *I* for INR 450000\r\n Type *J* for INR 500000\r\n Type *K* for INR 550000\r\n";
		}
		return message;
	}

	@Override
	public WhatsappMessagesDto readingWhatsappMessagesHistoryFalse(String chatId, int lastMessageNumber) {

		logger.info("statred again");
		WebTarget webTarget = client.target(whatsAppMessagesApi);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");

		invocationBuilder.headers(map);

		WtappGroupRequestDto input = new WtappGroupRequestDto();

		input.setChatId(chatId);

		input.setHistory(false);

		input.setLastMessageNumber(lastMessageNumber);

		Response response = invocationBuilder.post(Entity.json(input));

		String groupResponse = response.readEntity(String.class);

		Gson gson = new Gson();

		WhatsappMessagesDto messageDto = gson.fromJson(groupResponse, WhatsappMessagesDto.class);

		return messageDto;
	}

	@Override
	public void sendingWhatsappMessagestoMultipleGroup(List<String> chatIdsList, String message,
			String whatsAppsendApi) {
		String status = null;
		if (chatIdsList != null && !chatIdsList.isEmpty()) {
			for (String chatId : chatIdsList) {

				WebTarget webTarget = client.target(whatsAppsendApi);
				Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
						.accept(MediaType.APPLICATION_JSON_TYPE);

				MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

				map.add("Content-Type", "application/json");

				invocationBuilder.headers(map);

				WtappGroupRequestDto wtappGroupRequestDto = new WtappGroupRequestDto();

				wtappGroupRequestDto.setMessage(message);
				wtappGroupRequestDto.setChatId(chatId);

				Response response = invocationBuilder.post(Entity.json(wtappGroupRequestDto));

				if (response.getStatus() == 200) {

					status = "messageSent";
				} else {

					status = "messageNotSent";
				}

			}

		}
	}

	@Override
	public void sendMessagesWithMobileNumbers(List<String> mobileNumbers, String message, String whatsAppsendApi) {
		String status = null;

		if (mobileNumbers != null && !mobileNumbers.isEmpty()) {
			for (String mobileNumber : mobileNumbers) {

				WebTarget webTarget = client.target(whatsAppsendApi);
				Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
						.accept(MediaType.APPLICATION_JSON_TYPE);

				MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

				map.add("Content-Type", "application/json");

				invocationBuilder.headers(map);

				WtappRequestDto wtappRequestDto = new WtappRequestDto();

				wtappRequestDto.setMessage(message);
				wtappRequestDto.setPhoneNumber(mobileNumber);

				Response response = invocationBuilder.post(Entity.json(wtappRequestDto));

				if (response.getStatus() == 200) {

					status = "messageSent";
				} else {

					status = "messageNotSent";
				}

			}

		}
	}

	@Override
	public String sendingWhatsappMsgWithMobileNumber(String mobileNumber, String message, String wtappApi,
			String type) {
		String status = null;

		// if (checkWhatsappAvalability(mobileNumber)) {
		WebTarget webTarget = client
				.target("http://ec2-15-206-94-4.ap-south-1.compute.amazonaws.com:8888/whatsapp/send");
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");
		logger.info("message" + message);
		invocationBuilder.headers(map);
		for (int i = 0; i <= 1; i++) {
			String mobile = null;
			if (i == 0) {
				if (type == null) {
					mobile = "919492902990";
				} else {
					mobile = "916300873713";

				}
			} else {
				mobile = mobileNumber;
			}
			WtappGroupRequestDto wtappGroupRequestDto = new WtappGroupRequestDto();
			logger.info(message.toString());
			wtappGroupRequestDto.setMessage(message);
			wtappGroupRequestDto.setPhoneNumber(mobile);

			Response response = invocationBuilder.post(Entity.json(wtappGroupRequestDto));

			if (response.getStatus() == 200) {
				logger.info("message sent");
				status = "messageSent";
			} else {
				logger.info("message not sent");
				status = "messageNotSent";
			}
		}
		// }

		return status;

	}

	@Override
	public WhatsappMessagesDto readingWhatsappMessagesWithOutChatIdHistoryFalse(int lastMessageNumber) {
		WebTarget webTarget = client.target(whatsAppMessagesApiNewInstance);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");

		invocationBuilder.headers(map);

		WtappGroupRequestDto input = new WtappGroupRequestDto();

		input.setHistory(false);

		input.setLastMessageNumber(lastMessageNumber);

		Response response = invocationBuilder.post(Entity.json(input));

		String groupResponse = response.readEntity(String.class);

		Gson gson = new Gson();

		WhatsappMessagesDto messageDto = gson.fromJson(groupResponse, WhatsappMessagesDto.class);

		return messageDto;

	}

	public String lenderParticipationSendingMail(User user,
			OxyLendersAcceptedDealsRequestDto oxyLendersAcceptedDealsRequestDto, BigInteger updatedAmount,
			String dealName, String status) {
		String message = null;
		Calendar calendar = Calendar.getInstance();
		String currentDate = expectedDateFormat.format(calendar.getTime());
		String lenderName = user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName();
		TemplateContext templateContext = new TemplateContext();
		templateContext.put("lenderName", lenderName);
		templateContext.put("userId", "LR" + user.getId());
		templateContext.put("dealName", dealName);
		templateContext.put("amount", updatedAmount);
		templateContext.put("currentDate", currentDate);
		templateContext.put("rateOfInterest", oxyLendersAcceptedDealsRequestDto.getRateofInterest());
		templateContext.put("lenderReturnType", oxyLendersAcceptedDealsRequestDto.getLenderReturnType());
		String welcomeMailToLender = null;
		welcomeMailToLender = "Deal Updation Information";

		String emailTemplateName = "Lender-deal-paticipation.template";
		EmailRequest emailRequest = new EmailRequest(new String[] { user.getEmail(), "narendra@oxyloans.com" },
				welcomeMailToLender, emailTemplateName, templateContext);
		EmailResponse emailResponse1 = emailService.sendEmail(emailRequest);
		message = "SUCCESS";
		if (emailResponse1.getStatus() == EmailResponse.Status.FAILED) {
			throw new RuntimeException(emailResponse1.getErrorMessage());
		}
		return message;

	}

	@Override
	public String sendingWhatsappMsgWithMobileNumberToGroup(String mobileNumber, String message, String sendApi) {
		String status = null;
		WebTarget webTarget = client.target(sendApi);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");

		invocationBuilder.headers(map);

		WtappGroupRequestDto wtappGroupRequestDto = new WtappGroupRequestDto();

		wtappGroupRequestDto.setMessage(message);
		wtappGroupRequestDto.setChatId(mobileNumber);

		Response response = invocationBuilder.post(Entity.json(wtappGroupRequestDto));

		if (response.getStatus() == 200) {

			status = "messageSent";
		} else {

			status = "messageNotSent";
		}

		return status;

	}

	@Override
	public String sendingWhatsappMessages(String chatId, String message, String chatApi) {
		String status = null;
		WebTarget webTarget = client.target(chatApi);
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");

		invocationBuilder.headers(map);

		WtappGroupRequestDto wtappGroupRequestDto = new WtappGroupRequestDto();

		wtappGroupRequestDto.setMessage(message);
		wtappGroupRequestDto.setChatId(chatId);

		Response response = invocationBuilder.post(Entity.json(wtappGroupRequestDto));

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (response.getStatus() == 200) {

			status = "messageSent";
		} else {

			status = "messageNotSent";
		}

		return status;

	}

	@Override
	public String sendAttatchements(WhatsappAttachmentRequestDto whatsappAttachmentRequestDto) {

		String status = "";
		try {
			File file = new File(whatsappAttachmentRequestDto.getFileName());
			String fileName = whatsappAttachmentRequestDto.getFileName();

			OutputStream outputStream = new FileOutputStream(file);
			IOUtils.copy(whatsappAttachmentRequestDto.getInputSream(), outputStream);

			HttpHeaders headers = new HttpHeaders();

			headers.setContentType(org.springframework.http.MediaType.MULTIPART_FORM_DATA);

			MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
			ContentDisposition contentDisposition = ContentDisposition.builder("form-data").name("file")
					.filename(fileName).build();
			fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());

			HttpEntity<byte[]> fileEntity = new HttpEntity<>(Files.readAllBytes(file.toPath()), fileMap);

			MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
			body.add("file", fileEntity);
			body.add("phone", whatsappAttachmentRequestDto.getPhone());
			body.add("caption", whatsappAttachmentRequestDto.getCaption());
			body.add("fileName", whatsappAttachmentRequestDto.getFileName());
			body.add("chatId", whatsappAttachmentRequestDto.getChatId());

			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.postForEntity(whatsAppAttachmentApi, requestEntity,
					String.class);

			if (response.getStatusCodeValue() == 200) {
				logger.info("attachment sent");
				status = "messageSent";

			} else {
				logger.info("attachment not sent");
				status = "messageNotSent";

			}

			if (file.exists()) {
				file.delete();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return status;

	}

	@Override
	public String sendingWhatsappMsgBasedOnPartnerAction(String firstNumber, String firstMessage, String secondNumber,
			String secondMessage) {
		String status = null;
		WebTarget webTarget = client
				.target("http://ec2-15-206-94-4.ap-south-1.compute.amazonaws.com:8888/whatsapp/send");
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");

		invocationBuilder.headers(map);
		for (int i = 0; i <= 1; i++) {
			String mobile = null;
			String message = null;
			if (i == 0) {

				mobile = firstNumber;
				message = firstMessage;
			} else {
				mobile = secondNumber;
				message = secondMessage;

			}

			WtappGroupRequestDto wtappGroupRequestDto = new WtappGroupRequestDto();

			wtappGroupRequestDto.setMessage(message);
			wtappGroupRequestDto.setPhoneNumber(mobile);

			Response response = invocationBuilder.post(Entity.json(wtappGroupRequestDto));

			if (response.getStatus() == 200) {
				logger.info("message sent");
				status = "messageSent";
			} else {
				logger.info("message not sent");
				status = "messageNotSent";
			}
		}

		return status;

	}

	@Override
	public String sendingMsgToUsingUltra(OxyBorrowersDealsInformation oxyBorrowersDealsInformation, int dealId,
			String chatIds) {
		try {

			String whatsappMessage = null;

			WebTarget webTargetToGroup = client.target(whatsappUsingUltraApi);
			Invocation.Builder invocationBuilderToGroup = webTargetToGroup.request(MediaType.APPLICATION_JSON_TYPE)
					.accept(MediaType.APPLICATION_JSON_TYPE);

			MultivaluedMap<String, Object> map1 = new MultivaluedHashMap<String, Object>();

			map1.add("Content-Type", "application/json");
			invocationBuilderToGroup.headers(map1);
			if (oxyBorrowersDealsInformation.getDealOpenStatus()
					.equals(OxyBorrowersDealsInformation.DealOpenStatus.NOW)) {
				whatsappMessage = whatsAppMessageDealCreatedNow(oxyBorrowersDealsInformation, dealId);
				logger.info("deal type now");
			} else {
				whatsappMessage = whatsAppMessageDealCreatedFurther(oxyBorrowersDealsInformation, dealId);
				logger.info("deal type further");
			}

			StringBuilder stringBuilder = new StringBuilder(whatsappgroups);
			stringBuilder.append(",").append(chatIds);

			String[] names = stringBuilder.toString().split(",");

			for (String chatid : names) {

				WtappIndividualGroupRequestDto wtappIndividualGroupRequestDto = new WtappIndividualGroupRequestDto();

				wtappIndividualGroupRequestDto.setTo(chatid);
				wtappIndividualGroupRequestDto.setBody(whatsappMessage);
				wtappIndividualGroupRequestDto.setToken(whatsappUltraToken);

				Response responsWtappGroup = invocationBuilderToGroup.post(Entity.json(wtappIndividualGroupRequestDto));

				if (responsWtappGroup.getStatus() == 200) {
					logger.info("message sent to chat id : " + chatid);

				} else {
					logger.info("message not sent to chat id: " + chatid);

				}

				Thread.sleep(5000);
			}
		} catch (Exception e) {
			logger.info("Exception in sendingMsgToUsingUltra method");
		}
		return null;

	}

	public String whatsAppMessageDealCreatedNow(OxyBorrowersDealsInformation oxyBorrowersDealsInformation, int dealId) {

		String lineSpliter = System.lineSeparator();
		String messageToLender = null;
		try {

			String line1 = "Deal Name : " + oxyBorrowersDealsInformation.getDealName() + lineSpliter;
			BigInteger amount = BigDecimal.valueOf(oxyBorrowersDealsInformation.getDealAmount()).toBigInteger();
			String line2 = "Deal value in  INR : " + amount + lineSpliter;
			String line3 = "Funds Acceptance Start Date : "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getFundsAcceptanceStartDate())
					+ lineSpliter;
			String line4 = "Funds Acceptance End Date : "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getFundsAcceptanceEndDate()) + lineSpliter;
			String line5 = "Tenure in Months : " + oxyBorrowersDealsInformation.getDuration() + lineSpliter;
			String line6 = "Interest PaymentDate : "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getLoanActiveDate()) + lineSpliter;
			String line7 = "Project URL : " + oxyBorrowersDealsInformation.getDealLink() + lineSpliter;

			String line8 = "Minimum Participation limit : "
					+ BigDecimal.valueOf(oxyBorrowersDealsInformation.getMinimumPaticipationAmount()).toBigInteger()
					+ lineSpliter;

			String line9 = "Maximum Participation limit : "
					+ BigDecimal.valueOf(oxyBorrowersDealsInformation.getPaticipationLimitToLenders()).toBigInteger()
					+ lineSpliter;
			String line10 = "If you wish to participate,Please join through the below link." + lineSpliter;
			String line11 = oxyBorrowersDealsInformation.getWhatappReponseLink() + lineSpliter;
			String line13 = "Deal ID : " + oxyBorrowersDealsInformation.getId();

			String line12 = "";
			int groupId = 0;
			int dealId1 = oxyBorrowersDealsInformation.getId();

			OxyDealsRateofinterestToLendersgroup oxyDealsRate = oxyDealsRateofinterestToLendersgroupRepo
					.findByIdNumAndGroupId(dealId1, groupId);
			StringBuilder roi = new StringBuilder();

			if (oxyDealsRate != null) {
				DecimalFormat decimalFormat = new DecimalFormat("#0.##"); // Format to remove trailing zeros

				if (oxyDealsRate.getMonthlyInterest() > 0) {
					roi.append(" Monthly : ").append(decimalFormat.format(oxyDealsRate.getMonthlyInterest()))
							.append("%, ");
				}
				if (oxyDealsRate.getQuartelyInterest() > 0) {
					roi.append(" Quarterly : ").append(decimalFormat.format(oxyDealsRate.getQuartelyInterest()))
							.append("%, ");
				}
				if (oxyDealsRate.getHalfInterest() > 0) {
					roi.append(" Halfyearly : ").append(decimalFormat.format(oxyDealsRate.getHalfInterest()))
							.append("%, ");
				}
				if (oxyDealsRate.getYearlyInterest() > 0) {
					roi.append(" Yearly : ")
							.append(decimalFormat.format(Math.round(oxyDealsRate.getYearlyInterest() * 12)))
							.append("%, ");
				}
				if (oxyDealsRate.getEndofthedealInterest() > 0) {
					roi.append(" EndOfDeal : ").append(decimalFormat.format(oxyDealsRate.getEndofthedealInterest()))
							.append("%, ");
				}

				if (roi.length() > 0) {
					roi.setLength(roi.length() - 2); // Remove the last comma and space
					roi.append(" .");
				}
			}

			line12 = "Rate Of Interest :" + roi.toString();
			logger.info("interestDetails :" + roi);

			if (dealId == 0 || (dealId != 0 && oxyBorrowersDealsInformation.getDealFutureDate() != null)) {
				String line0 = "Dear Lenders, I have Created a deal and the details are as follows." + lineSpliter;

				messageToLender = line0 + lineSpliter + line1 + lineSpliter + line13 + lineSpliter + line2 + lineSpliter
						+ line3 + lineSpliter + line4 + lineSpliter + line5 + lineSpliter + line6 + lineSpliter + line7
						+ lineSpliter + line8 + lineSpliter + line9 + lineSpliter + line12 + lineSpliter + line10
						+ lineSpliter + line11;

			} else {
				String line0 = "Dear Lenders, I have Edited a deal and the details are as follows." + lineSpliter;

				messageToLender = line0 + lineSpliter + line1 + lineSpliter + line13 + lineSpliter + line2 + lineSpliter
						+ line3 + lineSpliter + line4 + lineSpliter + line5 + lineSpliter + line6 + lineSpliter + line7
						+ lineSpliter + line8 + lineSpliter + line9 + lineSpliter + line12 + lineSpliter + line10
						+ lineSpliter + line11;

			}
		} catch (Exception e) {
			logger.info("Exception in whatsAppMessage method");
		}
		return messageToLender;

	}

	public String whatsAppMessageDealCreatedFurther(OxyBorrowersDealsInformation oxyBorrowersDealsInformation,
			int dealId) {

		String lineSpliter = System.lineSeparator();
		String messageToLender = null;
		try {

			String line1 = "Deal Name : " + oxyBorrowersDealsInformation.getDealName() + lineSpliter;
			BigInteger amount = BigDecimal.valueOf(oxyBorrowersDealsInformation.getDealAmount()).toBigInteger();
			String line2 = "Deal value in  INR : " + amount + lineSpliter;
			String line3 = "Funds Acceptance Start Date : "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getFundsAcceptanceStartDate())
					+ lineSpliter;
			String line4 = "Funds Acceptance End Date : "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getFundsAcceptanceEndDate()) + lineSpliter;
			String line5 = "Tenure in Months : " + oxyBorrowersDealsInformation.getDuration() + lineSpliter;
			String line6 = "Interest PaymentDate : "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getLoanActiveDate()) + lineSpliter;
			String line7 = "Project URL : " + oxyBorrowersDealsInformation.getDealLink() + lineSpliter;

			String line8 = "Minimum Participation limit : "
					+ BigDecimal.valueOf(oxyBorrowersDealsInformation.getMinimumPaticipationAmount()).toBigInteger()
					+ lineSpliter;

			String line9 = "Maximum Participation limit : "
					+ BigDecimal.valueOf(oxyBorrowersDealsInformation.getPaticipationLimitToLenders()).toBigInteger()
					+ lineSpliter;
			String line10 = "If you wish to participate,Please join through the below link." + lineSpliter;
			String line11 = oxyBorrowersDealsInformation.getWhatappReponseLink();
			String line12 = "*Deal Launch Date and Time : "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getDealFutureDate()) + "*" + " " + "*"
					+ oxyBorrowersDealsInformation.getDealLaunchHoure() + " IST*" + lineSpliter;
			String line13 = "A Deal will be launched .Please find the details below" + lineSpliter;

			// my code started from here
			String line14 = "";
			int groupId = 0;

			int dealId1 = oxyBorrowersDealsInformation.getId();

			OxyDealsRateofinterestToLendersgroup oxyDealsRate = oxyDealsRateofinterestToLendersgroupRepo
					.findByIdNumAndGroupId(dealId1, groupId);
			StringBuilder roi = new StringBuilder();

			if (oxyDealsRate != null) {
				DecimalFormat decimalFormat = new DecimalFormat("#0.##"); // Format to remove trailing zeros

				if (oxyDealsRate.getMonthlyInterest() > 0) {
					roi.append(" Monthly : ").append(decimalFormat.format(oxyDealsRate.getMonthlyInterest()))
							.append("%, ");
				}
				if (oxyDealsRate.getQuartelyInterest() > 0) {
					roi.append(" Quarterly : ").append(decimalFormat.format(oxyDealsRate.getQuartelyInterest()))
							.append("%, ");
				}
				if (oxyDealsRate.getHalfInterest() > 0) {
					roi.append(" Halfyearly : ").append(decimalFormat.format(oxyDealsRate.getHalfInterest()))
							.append("%, ");
				}
				if (oxyDealsRate.getYearlyInterest() > 0) {
					roi.append(" Yearly : ")
							.append(decimalFormat.format(Math.round(oxyDealsRate.getYearlyInterest() * 12)))
							.append("%, ");
				}
				if (oxyDealsRate.getEndofthedealInterest() > 0) {
					roi.append(" EndOfDeal : ").append(decimalFormat.format(oxyDealsRate.getEndofthedealInterest()))
							.append("%, ");
				}

				if (roi.length() > 0) {
					roi.setLength(roi.length() - 2); // Remove the last comma and space
					roi.append(" .");
				}
			}

			line14 = "Rate Of Interest : " + roi.toString() + lineSpliter;
			logger.info("interestDetails : " + roi);

			String line0 = "Dear Lenders," + lineSpliter;
			messageToLender = line0 + lineSpliter + line13 + lineSpliter + line12 + lineSpliter + line1 + lineSpliter
					+ line2 + lineSpliter + line3 + lineSpliter + line4 + lineSpliter + line5 + lineSpliter + line6
					+ lineSpliter + line7 + lineSpliter + line8 + lineSpliter + line9 + lineSpliter + line14
					+ lineSpliter + line10 + lineSpliter + line11;

		} catch (Exception e) {
			logger.info("Exception in whatsAppMessage method");
		}
		return messageToLender;

	}

	@Override
	public String whatsappMessageToGroupUsingUltra(String chatId, String message) {
		String status = null;
		try {
			WebTarget webTarget = client.target(whatsappUsingUltraApi);

			Invocation.Builder invocationBuider = webTarget.request(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON);

			MultivaluedMap<String, Object> map1 = new MultivaluedHashMap<String, Object>();

			map1.add("Content-Type", "application/json");
			invocationBuider.headers(map1);
			WtappIndividualGroupRequestDto wtappIndividualGroupRequestDto = new WtappIndividualGroupRequestDto();

			wtappIndividualGroupRequestDto.setTo(chatId);
			wtappIndividualGroupRequestDto.setBody(message);
			wtappIndividualGroupRequestDto.setToken(whatsappUltraToken);
			Response responsWtappGroup = invocationBuider.post(Entity.json(wtappIndividualGroupRequestDto));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			Date currentDate = new Date();

			String formattedDateTime = sdf.format(currentDate);

			System.out.println("Formatted date and time: " + formattedDateTime);

			if (responsWtappGroup.getStatus() == 200) {
				logger.info("message sent to chat id : " + chatId);
				status = "success";
			} else {
				logger.info("message not sent to chat id: " + chatId);
				status = "failed";
			}

		} catch (Exception e) {
			logger.info("Exception in whatsappMessageToGroup method");
		}
		return status;

	}

	@Override
	public String sendingIndividualMessageUsingUltraApi(String mobileNumber, String message) {
		String status = null;
		try {

			WebTarget webTargetToGroup = client.target(whatsappUsingUltraApi);
			Invocation.Builder invocationBuilderToGroup = webTargetToGroup.request(MediaType.APPLICATION_JSON_TYPE)
					.accept(MediaType.APPLICATION_JSON_TYPE);

			MultivaluedMap<String, Object> map1 = new MultivaluedHashMap<String, Object>();

			map1.add("Content-Type", "application/json");
			invocationBuilderToGroup.headers(map1);

			WtappIndividualGroupRequestDto wtappIndividualGroupRequestDto = new WtappIndividualGroupRequestDto();

			wtappIndividualGroupRequestDto.setTo(mobileNumber);
			wtappIndividualGroupRequestDto.setBody(message);
			wtappIndividualGroupRequestDto.setToken(whatsappUltraToken);

			Response responsWtappGroup = invocationBuilderToGroup.post(Entity.json(wtappIndividualGroupRequestDto));

			if (responsWtappGroup.getStatus() == 200) {
				logger.info("message sent to chat id : " + mobileNumber);
				status = "success";
			} else {
				logger.info("message not sent to chat id: " + mobileNumber);
				status = "failed";
			}

		} catch (Exception e) {
			logger.info("Exception in sendingIndividualMessageUsingUlterApi method");
		}
		return status;

	}

	@Override
	public String partnerActionUsingUltra(String firstNumber, String firstMessage, String secondNumber,
			String secondMessage) {

		WebTarget webTargetToGroup = client.target(whatsappUsingUltraApi);
		Invocation.Builder invocationBuilderToGroup = webTargetToGroup.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map1 = new MultivaluedHashMap<String, Object>();

		map1.add("Content-Type", "application/json");
		invocationBuilderToGroup.headers(map1);
		for (int i = 0; i <= 1; i++) {
			String mobile = null;
			String message = null;
			if (i == 0) {

				mobile = firstNumber;
				message = firstMessage;
			} else {
				mobile = secondNumber;
				message = secondMessage;

			}

			WtappIndividualGroupRequestDto wtappIndividualGroupRequestDto = new WtappIndividualGroupRequestDto();

			wtappIndividualGroupRequestDto.setTo(mobile);
			wtappIndividualGroupRequestDto.setBody(message);
			wtappIndividualGroupRequestDto.setToken(whatsappUltraToken);

			Response responsWtappGroup = invocationBuilderToGroup.post(Entity.json(wtappIndividualGroupRequestDto));

			if (responsWtappGroup.getStatus() == 200) {
				logger.info("message sent to chat ida : " + mobile);

			} else {
				logger.info("message not sent to chat id: " + mobile);

			}
		}

		return null;

	}

	@Override
	public MessagesResponseDto whatsappGroupChatid(String dealName) {
		String chatId = null;
		try {

			ClientConfig clientConfig = new ClientConfig();
			clientConfig.register(JacksonFeature.class);

			Client client = ClientBuilder.newClient(clientConfig);

			WebTarget webTarget = client.target(chatidsUsingUltraApi);

			Builder request = webTarget.request();
			request.header("Content-type", MediaType.APPLICATION_JSON);

			Response response = request.get();
			if (response.getStatus() == 200) {
				String groupResponse = response.readEntity(String.class);

				Gson gson = new Gson();
				ContactsResponseDto[] userArray = gson.fromJson(groupResponse, ContactsResponseDto[].class);
				for (ContactsResponseDto user : userArray) {
					if (user.getName().equalsIgnoreCase(dealName)) {
						chatId = user.getId();
					}

				}

			}
		} catch (Exception e) {
			logger.info("Exception in whatsappGroupChatid method");
		}
		MessagesResponseDto messagesResponseDto = new MessagesResponseDto();
		messagesResponseDto.setId(chatId);
		return messagesResponseDto;

	}

	@Override
	public String whatsappImageUsingUltra(String senderId, String image, String message) { // we can send image to
																							// individual and groups
		try {
			WebTarget webTarget = client.target(whatsappImageUsingUltra);
			// WebTarget webTarget =
			// client.target("https://api.ultramsg.com/instance45112/messages/image");
			Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON);
			MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
			map.add("Content-Type", "application/json");

			invocationBuilder.headers(map);
			WhatsappImageUsingUltra wtappIndividualGroupRequestDto = new WhatsappImageUsingUltra();
			wtappIndividualGroupRequestDto.setTo(senderId);
			wtappIndividualGroupRequestDto.setToken(whatsappUltraToken);
			// wtappIndividualGroupRequestDto.setToken(whatsappUltraTokenForNewInstance);
			wtappIndividualGroupRequestDto.setImage(image);
			wtappIndividualGroupRequestDto.setCaption(message);
			Response response = invocationBuilder.post(Entity.json(wtappIndividualGroupRequestDto));
			if (response.getStatus() == 200) {
				logger.info("ultra image sent to groups" + senderId);
			} else {
				logger.info("ultra image not sent to groups" + senderId);
			}

		} catch (Exception e) {
			logger.info("Exception in whatsappImageUsingUltra method");
		}
		return "null";

	}

	@Override
	public String whatsappImageUsingUltraUsingNew(String senderId, String image, String message) { // we can send image
																									// to
		// individual and groups
		try {
			// WebTarget webTarget = client.target(whatsappImageUsingUltra);
			WebTarget webTarget = client.target("https://api.ultramsg.com/instance45112/messages/image");
			Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON);
			MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
			map.add("Content-Type", "application/json");

			invocationBuilder.headers(map);
			WhatsappImageUsingUltra wtappIndividualGroupRequestDto = new WhatsappImageUsingUltra();
			wtappIndividualGroupRequestDto.setTo(senderId);
			// wtappIndividualGroupRequestDto.setToken(whatsappUltraToken);
			wtappIndividualGroupRequestDto.setToken(whatsappUltraTokenForNewInstance);
			wtappIndividualGroupRequestDto.setImage(image);
			wtappIndividualGroupRequestDto.setCaption(message);
			Response response = invocationBuilder.post(Entity.json(wtappIndividualGroupRequestDto));
			if (response.getStatus() == 200) {
				logger.info("ultra image sent to groups" + senderId);
			} else {
				logger.info("ultra image not sent to groups" + senderId);
			}

		} catch (Exception e) {
			logger.info("Exception in whatsappImageUsingUltra method");
		}
		return "null";

	}

	public int sendingSms(Map variblesMap, String templateName, String mobileNumber, String senderId) {
		int count = 0;
		String mobile = null;
		if (mobileNumber != null) {
			for (int i = 0; i <= 1; i++) {
				if (i == 0) {
					mobile = "9392921295";
				} else {
					mobile = mobileNumber;
				}
				MobileRequest mobileRequest = new MobileRequest(mobileNumber, templateName);
				mobileRequest.setSenderId(senderId);

				mobileRequest.setVariblesMap(variblesMap);

				try {
					mobileUtil.sendTransactionalMessage(mobileRequest);
					count += 1;
				} catch (Exception e1) {
					logger.error("SMS failed : ", e1);
				}
			}
		} else {
			MobileRequest mobileRequest = new MobileRequest("9392921295", templateName);
			mobileRequest.setSenderId(senderId);

			mobileRequest.setVariblesMap(variblesMap);

			try {
				mobileUtil.sendTransactionalMessage(mobileRequest);
				count += 1;
			} catch (Exception e1) {
				logger.error("SMS failed : ", e1);
			}
		}
		return count;

	}

	@Override
	public String sendingWhatsappMessageWithNewInstance(String chatId, String message) {
		try {
			logger.info("alert wallet txn  check msg with chat id started ..");
			WebTarget webTarget = client.target(whatsappUsingUltraNewInstance);
			Invocation.Builder invocationBuider = webTarget.request(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON);

			MultivaluedMap<String, Object> map1 = new MultivaluedHashMap<String, Object>();

			map1.add("Content-Type", "application/json");
			invocationBuider.headers(map1);
			WtappIndividualGroupRequestDto wtappIndividualGroupRequestDto = new WtappIndividualGroupRequestDto();

			wtappIndividualGroupRequestDto.setTo(chatId);
			wtappIndividualGroupRequestDto.setBody(message);
			wtappIndividualGroupRequestDto.setToken(whatsappUltraTokenForNewInstance);

			Response responsWtappGroup = invocationBuider.post(Entity.json(wtappIndividualGroupRequestDto));

			if (responsWtappGroup.getStatus() == 200) {
				logger.info("alert wallet txn  check msg with chat id sent sucessfully ..");
				logger.info("message sent to chat id : ");

			} else {
				logger.info("alert wallet txn  check msg with chat id failed ..");
				logger.info("message not sent to chat id: ");

			}

		} catch (Exception e) {
			logger.info("Exception in whatsappMessageToGroup method");
		}
		return null;

	}

}
