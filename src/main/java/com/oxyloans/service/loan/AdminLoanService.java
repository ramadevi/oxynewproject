package com.oxyloans.service.loan;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.security.GeneralSecurityException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets.Details;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.Sheets.Spreadsheets.Values.Get;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetResponse;
import com.google.api.services.sheets.v4.model.FindReplaceRequest;
import com.google.api.services.sheets.v4.model.FindReplaceResponse;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import com.google.api.services.sheets.v4.model.UpdateSpreadsheetPropertiesRequest;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.gson.Gson;
import com.oxyloans.customexceptions.DataFormatException;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.engine.template.PdfGeenrationException;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entity.EsignTransactions;
import com.oxyloans.entity.EsignTransactions.EsignType;
import com.oxyloans.entity.EsignTransactions.TransactonStatus;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceDetails;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceResponse;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferralBonusUpdated;
import com.oxyloans.entity.LenderReferenceDetails.ReferralBonusDetails;
import com.oxyloans.repository.lenderreferencedetailesrepo.ReferralBonusDetailsRepo;
import com.oxyloans.entity.LenderReferenceDetails.ReferralBonusReading;
import com.oxyloans.repository.lenderreferencedetailesrepo.ReferralBonusReadingRepo;
//import com.oxyloans.entity.LenderReferenceDetails.LenderReferralBonus;
import com.oxyloans.entityborrowersdealsdto.BorrowerDealRateofInterestResponseDto;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsList;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsRequestDto;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsResponseDto;
import com.oxyloans.entityborrowersdealsdto.ClosedDealsInformation;
import com.oxyloans.entityborrowersdealsdto.DealInformationRoiToLender;
import com.oxyloans.entityborrowersdealsdto.DealLevelRequestDto;
import com.oxyloans.entityborrowersdealsdto.DealLevelResponseDto;
import com.oxyloans.entityborrowersdealsdto.LenderDetailsByGroupId;
import com.oxyloans.entityborrowersdealsdto.LenderMappedToGroupIdResponseDto;
import com.oxyloans.entityborrowersdealsdto.LenderPaticipatedDeal;
import com.oxyloans.entityborrowersdealsdto.LenderPaticipatedResponseDto;
import com.oxyloans.entityborrowersdealsdto.LendersDealsResponseDto;
import com.oxyloans.entityborrowersdealsdto.LoanResponseForDealsDto;
import com.oxyloans.entity.borrowers.deals.information.LendersPaticipationUpdation;
import com.oxyloans.entity.borrowers.deals.information.LendersPaticipationUpdationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation.DealType;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation.ParticipationLenderType;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationHistory;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationHistoryRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyDealsRateofinterestToLendersgroup;
import com.oxyloans.entity.borrowers.deals.information.OxyDealsRateofinterestToLendersgroupRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDeals;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDeals.LenderReturnsType;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDealsRepo;
import com.oxyloans.entity.cms.LenderCmsPayments;
import com.oxyloans.repository.cms.LenderCmsPaymentsRepo;
import com.oxyloans.entity.cms.OxyCmsLoans;
import com.oxyloans.entity.enach.AmountUpdationBasedOnDisbursment;
import com.oxyloans.repository.enach.AmountUpdationBasedOnDisbursmentRepo;
import com.oxyloans.entity.enach.ApplicationLevelEnachMandate;
import com.oxyloans.repository.enach.ApplicationLevelEnachMandateRepo;
import com.oxyloans.entity.enach.EnachMandate;
import com.oxyloans.entity.enach.EnachMandate.AmountType;
import com.oxyloans.entity.enach.EnachMandate.Frequency;
import com.oxyloans.entity.enach.EnachMandate.MandateStatus;
import com.oxyloans.entity.excelsheets.OxyTransactionDetailsFromExcelSheets;
import com.oxyloans.entity.excelsheets.OxyTransactionDetailsFromExcelSheets.DebitedAccountType;
import com.oxyloans.entity.excelsheets.OxyTransactionDetailsFromExcelSheets.DebitedTowords;
import com.oxyloans.repository.excelsheets.OxyTransactionDetailsFromExcelSheetsRepo;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWallet;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletNativeRepo;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletRepo;
import com.oxyloans.entity.lender.payu.LenderPayuDetails;
import com.oxyloans.entity.lender.payu.LenderRenewalDetails;
import com.oxyloans.entity.lender.payu.LenderRenewalDetailsRepo;
import com.oxyloans.entity.lender.returns.LendersReturns;
import com.oxyloans.entity.lender.returns.LendersReturnsRepo;
import com.oxyloans.entity.lenders.group.OxyLendersGroup;
import com.oxyloans.repository.lendergroup.OxyLendersGroupRepo;
import com.oxyloans.repository.lenderhold.UserHoldAmountMappedToDealRepo;
import com.oxyloans.entity.loan.InterestDetails;
import com.oxyloans.entity.loan.LenderBorrowerConversation;
import com.oxyloans.entity.loan.LoanEmiCard;
import com.oxyloans.entity.loan.LoanEmiCardPaymentDetails;
import com.oxyloans.entity.loan.LoanEmiCardPaymentDetails.PaymentStatus;
import com.oxyloans.entity.loan.LoanOfferdAmount;
import com.oxyloans.entity.loan.LoanOfferdAmount.LoanOfferdStatus;
import com.oxyloans.entity.loan.LoanRequest;
import com.oxyloans.entity.loan.LoanRequest.DurationType;
import com.oxyloans.entity.loan.LoanRequest.LoanStatus;
import com.oxyloans.entity.loan.LoanRequest.RepaymentMethod;
import com.oxyloans.entity.loan.OxyLoan;
import com.oxyloans.entity.loan.PaymentUploadHistory;
import com.oxyloans.entity.loan.PaymentUploadHistory.PaymentType;
import com.oxyloans.entity.loan.PenaltyDetails;
import com.oxyloans.entity.poolingAccount.OxyPoolingAccountDetails;
import com.oxyloans.repository.poolingaccountrepo.OxyPoolingAccountDetailsRepo;
import com.oxyloans.entity.user.BankDetails;
import com.oxyloans.entity.user.PersonalDetails;
import com.oxyloans.entity.user.ProfessionalDetails;
import com.oxyloans.entity.user.ProfessionalDetails.Employment;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.entity.user.User.Status;
import com.oxyloans.entity.user.UserProfileRisk;
import com.oxyloans.entity.user.Document.status.UserDocumentStatus;
import com.oxyloans.entity.user.Document.status.UserDocumentStatus.DocumentType;
import com.oxyloans.entity.user.type.OxyUserType;
import com.oxyloans.entity.user.type.OxyUserTypeRepo;
import com.oxyloans.entity.whatapp.WhatappGroupsInformation;
import com.oxyloans.repository.whatsapprepo.WhatappGroupsInformationRepo;
import com.oxyloans.excelservice.ExcelServiceRepo;
import com.oxyloans.excelsheets.LenderDetailsFromExcelSheet;
import com.oxyloans.excelsheets.OxyTransactionDetailsFromExcelSheetsRequest;
import com.oxyloans.excelsheets.OxyTransactionDetailsFromExcelSheetsResponse;
import com.oxyloans.experian.ExperianRequestDto;
import com.oxyloans.file.FileRequest;
import com.oxyloans.file.FileRequest.FileType;
import com.oxyloans.file.FileResponse;
import com.oxyloans.lender.participationToWallet.LenderIncomeRequestDto;
import com.oxyloans.lender.participationToWallet.LenderIncomeResponseDto;
import com.oxyloans.lender.wallet.ScrowLenderTransactionResponse;
import com.oxyloans.lender.wallet.ScrowWalletResponse;
import com.oxyloans.mobile.MobileRequest;
import com.oxyloans.mobile.twoFactor.MobileOtpVerifyFailedException;
import com.oxyloans.repo.EsignTransactionsRepo;
import com.oxyloans.repo.enach.EnachTransactionVerificationRepo;
import com.oxyloans.repo.loan.ApplicationLevelLoanEmiCard;
import com.oxyloans.repo.loan.ApplicationLevelLoanEmiCardRepo;
import com.oxyloans.repo.loan.InterestDetailsRepo;
import com.oxyloans.repo.loan.LenderBorrowerConversationRepo;
import com.oxyloans.repo.loan.LoanEmiCardPaymentDetailsRepo;
import com.oxyloans.repo.loan.LoanEmiCardRepo;
import com.oxyloans.repo.loan.LoansByApplicationNativeRepo;
import com.oxyloans.repo.loan.OxyLoanRepo;
import com.oxyloans.repo.loan.OxyLoanRequestRepo;
import com.oxyloans.repo.loan.PaymentUploadHistoryRepo;
import com.oxyloans.repo.loan.PaytmTransactionDetailsRepo;
import com.oxyloans.repo.loan.PenaltyDetailsRepo;
import com.oxyloans.repo.user.LenderReferenceDetailsRepo;
//import com.oxyloans.repo.user.LenderReferralBonusRepo;
import com.oxyloans.repo.user.LenderReferralBonusUpdatedRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.repo.user.UtmMasterRepo;
import com.oxyloans.request.LenderFavouriteRequestDto;
import com.oxyloans.request.LenderPayuDto;
import com.oxyloans.request.SearchRequestDto;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.request.user.BorrowerPaymentsDto;
import com.oxyloans.request.user.KycFileRequest;
import com.oxyloans.request.user.KycFileRequest.KycType;
import com.oxyloans.request.user.KycFileResponse;
import com.oxyloans.request.user.LenderReferenceRequestDto;
import com.oxyloans.request.user.SpreadSheetRequestDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.request.user.WhatsappAttachmentRequestDto;
import com.oxyloans.request.user.WhatsappCampaignDto;
import com.oxyloans.response.admin.AdminEmiDetailsResponseDto;
import com.oxyloans.response.admin.DisbursmentPendingResponseDto;
import com.oxyloans.response.admin.UserInformationResponseDto;
import com.oxyloans.response.user.ActiveLenderResponseDto;
import com.oxyloans.response.user.BankDetailsResponseDto;
import com.oxyloans.response.user.BorrowerDetailsRequestDto;
import com.oxyloans.response.user.BorrowerFreeDetailsResponseDto;
import com.oxyloans.response.user.BorrowerLoansResponse;
import com.oxyloans.response.user.BorrowerProcessingFeeResponseDto;
import com.oxyloans.response.user.BorrowersFeeBasedOnDate;
import com.oxyloans.response.user.BorrowersLoanInfo;
import com.oxyloans.response.user.BorrowersLoanOwnerInformation;
import com.oxyloans.response.user.BorrowersLoanOwnerNames;
import com.oxyloans.response.user.CommentsRequestDto;
import com.oxyloans.response.user.CommentsResponseDto;
import com.oxyloans.response.user.CommitmentAmountExcelDto;
import com.oxyloans.response.user.DocumentDetailsResponseDto;
import com.oxyloans.response.user.EmiCalculationRequestDto;
import com.oxyloans.response.user.EmiRequestDto;
import com.oxyloans.response.user.EnachScheduledMailResponseDto;
import com.oxyloans.response.user.EnachTransactionResponseDto;
import com.oxyloans.response.user.EquityFoundingLendersDto;
import com.oxyloans.response.user.EquityLendersListsDto;
import com.oxyloans.response.user.EquityNewLendersDto;
import com.oxyloans.response.user.LenderCurrentWalletBalanceResponse;
import com.oxyloans.response.user.LenderDetails;
import com.oxyloans.response.user.LenderEscrowWalletDto;
import com.oxyloans.response.user.LenderFavouriteResponseDto;
import com.oxyloans.response.user.LenderIdsAndDealIds;
import com.oxyloans.response.user.LenderLoansInformation;
import com.oxyloans.response.user.LenderNocResponseDto;
import com.oxyloans.response.user.LenderReferenceAmountResponse;
import com.oxyloans.response.user.LenderReferenceResponseDto;
import com.oxyloans.response.user.LenderReferralResponse;
import com.oxyloans.response.user.LenderReferralResponseForDuration;
import com.oxyloans.response.user.LenderWalletInfo;
import com.oxyloans.response.user.LendersLoanInfo;
import com.oxyloans.response.user.ListOfWhatappGroupNames;
import com.oxyloans.response.user.LoanDisbursmentResponse;
import com.oxyloans.response.user.OxyLendersGroups;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.response.user.PaymentHistoryRequestDto;
import com.oxyloans.response.user.PaymentHistoryResponseDto;
import com.oxyloans.response.user.PaymentSearchByStatus;
import com.oxyloans.response.user.PaymentUploadHistoryRequestDto;
import com.oxyloans.response.user.PaymentUploadHistoryResponseDto;
import com.oxyloans.response.user.PaymentUploadHistoryTableResponseDto;
import com.oxyloans.response.user.ReadingCommitmentAmountDto;
import com.oxyloans.response.user.RiskProfileDto;
import com.oxyloans.response.user.StatusResponseDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.response.user.WhatappInformation;
import com.oxyloans.response.user.WtappGroupRequestDto;
import com.oxyloans.security.authorization.IAuthorizationService;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.service.esign.EsignFetchResponse;
import com.oxyloans.service.esign.IEsignService;
import com.oxyloans.serviceloan.AdminDashboardDetails;
import com.oxyloans.serviceloan.ApplicationResponseDto;
import com.oxyloans.serviceloan.BorrowerIndividualLoansCountResposeDto;
import com.oxyloans.serviceloan.BucketPendingEmis;
import com.oxyloans.serviceloan.DurationChangeDto;
import com.oxyloans.serviceloan.LenderEmiDetailsResponseDto;
import com.oxyloans.serviceloan.LenderHistroryResponseDto;
import com.oxyloans.serviceloan.LenderTransactionHistoryResponseDto;
import com.oxyloans.serviceloan.LoanEmiCardPaymentDetailsRequestDto;
import com.oxyloans.serviceloan.LoanEmiCardPaymentDetailsResponseDto;
import com.oxyloans.serviceloan.LoanEmiCardResponseDto;
import com.oxyloans.serviceloan.LoanRequestDto;
import com.oxyloans.serviceloan.LoanResponseDto;
import com.oxyloans.serviceloan.NotificationCountResponseDto;
import com.oxyloans.serviceloan.UploadAgreementRequestDto;
import com.oxyloans.service.user.UserServiceImpl;
import com.oxyloans.whatapp.service.WhatappService;
import com.oxyloans.whatapp.service.WhatappServiceRepo;
import com.oxyloans.whatsappdto.Messages;
import com.oxyloans.whatsappdto.WhatsappMessagesDto;

@Service("ADMIN")
public class AdminLoanService extends AbstractLoanService<AdminDashboardDetails> {

	private final Logger logger = LogManager.getLogger(AdminLoanService.class);

	@Autowired
	private UserRepo userRepo;

	protected final SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	@Autowired
	private IAuthorizationService authorizationService;

	@Autowired
	private OxyLoanRequestRepo oxyLoanRequestRepo;

	@Autowired
	private IEsignService esignService;

	@Autowired
	private EsignTransactionsRepo esignTransactionsRepo;

	@Autowired
	private OxyLoanRepo oxyLoanRepo;

	@Autowired
	private ExcelServiceRepo excelServiceRepo;

	@Autowired
	private LenderBorrowerConversationRepo lenderBorrowerConversationRepo;

	@Autowired
	private LoanEmiCardRepo loanEmiCardRepo;

	@Autowired
	private IEmailService emailService;

	@Value("${emailBody}")
	private String emailBody;

	@Value("${loanEmailVerifyPath}")
	private String loanEmailVerifyPath;
	
	@Value("${iciciFilePathBeforeApproval}")
	private String iciciFilePathBeforeApproval;

	@Value("${loanMailsubject}")
	private String loanMailsubject;

	@Autowired
	private UtmMasterRepo utmMasterRepo;

	@Autowired
	private LoanEmiCardPaymentDetailsRepo loanEmiCardPaymentDetailsRepo;

	@Autowired
	private LenderOxyWalletNativeRepo lenderOxyWalletNativeRepo;

	private SimpleDateFormat dateFormate = new SimpleDateFormat("dd/MM/yyyy");

	@Autowired
	private EnachTransactionVerificationRepo enachTransactionVerificationRepo;

	@Value("${supportEmail}")
	private String supportEmail;

	@Autowired
	private PenaltyDetailsRepo penaltyDetailsRepo;

	@Autowired
	private InterestDetailsRepo interestDetailsRepo;

	@Autowired
	private LoansByApplicationNativeRepo loansByApplicationNativeRepo;

	@Value("${utm}")
	private String utm;

	@Autowired
	private UserServiceImpl userServiceImpl;

	@Value("${linkInInterestButton}")
	private String linkInInterestButton;

	@Autowired
	private PaymentUploadHistoryRepo paymentUploadHistoryRepo;

	@Autowired
	private LenderReferenceDetailsRepo lenderReferenceDetailsRepo;

	@Autowired
	private OxyTransactionDetailsFromExcelSheetsRepo oxyTransactionDetailsFromExcelSheetsRepo;

	@Autowired
	private LendersReturnsRepo lendersReturnsRepo;

	@Autowired
	private OxyBorrowersDealsInformationHistoryRepo oxyBorrowersDealsInformationHistoryRepo;

	/*
	 * @Autowired private LenderReferralBonusRepo lenderReferralBonusRepo;
	 */
	@Value("${linkForApprovalForReferenceAmount}")
	private String linkForApprovalForReferenceAmount;

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Autowired
	private OxyDealsRateofinterestToLendersgroupRepo oxyDealsRateofinterestToLendersgroupRepo;

	@Autowired
	private OxyLendersGroupRepo oxyLendersGroupRepo;

	@Autowired
	private OxyLendersAcceptedDealsRepo oxyLendersAcceptedDealsRepo;

	@Value("${wtappApiForCreatingGroup}")
	private String wtappApiForCreatingGroup;

	@Value("${wtappApi}")
	private String wtappApi;

	@Value("${whatsaAppSendApiNewInstance}")
	private String whatsaAppSendApiNewInstance;

	private SimpleDateFormat requiredDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Value("${gmailClientId}")
	private String clientId;

	@Value("${gmailClientSecret}")
	private String clientSecret;

	@Value("${spreadSheetRedirectUrl}")
	private String spreadSheetRedirectUrl;

	final static String TOKENS_DIRECTORY_PATH = "tokens";

	final static JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	private Client client = ClientBuilder.newClient().register(MultiPartFeature.class).register(MultiPartWriter.class);

	@Autowired
	private WhatappGroupsInformationRepo whatappGroupsInformationRepo;

	private final String LOAN_ID_FORMAT = "LN{ID}";

	private final String LOAN_REQUEST_IDFORMAT = "AP{type}{ID}";

	@Value("${paymentScreenshotsWhatsAppGroup}")
	private String paymentScreenshotsWhatsAppGroup;

	@Value("${whatsAppMessagesApi}")
	private String whatsAppMessagesApi;

	@Autowired
	private WhatappServiceRepo whatappServiceRepo;

	@Autowired
	private LenderOxyWalletRepo lenderOxyWalletRepo;

	@Autowired
	private PaytmTransactionDetailsRepo paytmTransactionDetailsRepo;

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	private LendersPaticipationUpdationRepo lendersPaticipationUpdationRepo;

	@Autowired
	private OxyPoolingAccountDetailsRepo oxyPoolingAccountDetailsRepo;

	@Autowired
	private NewAdminLoanService newAdminLoanService;

	@Autowired
	private OxyUserTypeRepo oxyUserTypeRepo;

	@Value("${toSaveReferralBonus}")
	private String toSaveReferralBonus;

	@Value("${toSaveReferralBonus1}")
	private String toSaveReferralBonus1;

	@Value("${referralBonusByBorrower}")
	private Integer referralBonusByBorrower;

	@Autowired
	private LenderReferralBonusUpdatedRepo lenderReferralBonusUpdatedRepo;

	@Autowired
	private WhatappService whatappService;

	@Autowired
	private ApplicationLevelLoanEmiCardRepo applicationLevelLoanEmiCardRepo;

	@Autowired
	private ApplicationLevelEnachMandateRepo applicationLevelEnachMandateRepo;

	SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	private AmountUpdationBasedOnDisbursmentRepo amountUpdationBasedOnDisbursmentRepo;

	@Autowired
	private LenderRenewalDetailsRepo lenderRenewalDetailsRepo;

	@Autowired
	private ReferralBonusDetailsRepo referralBonusDetailsRepo;

	@Autowired
	private LenderCmsPaymentsRepo lenderCmsPaymentsRepo;

	@Autowired
	private UserHoldAmountMappedToDealRepo userHoldAmountMappedToDealRepo;
	
	
	@Autowired
	private ReferralBonusReadingRepo referralBonusReadingRepo;

	@Override
	@Transactional
	public AdminDashboardDetails getDashboard(int userId, boolean current) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		AdminDashboardDetails adminDashboardDetails = new AdminDashboardDetails();
		int borrowerCount = userRepo.countByPrimaryType(PrimaryType.BORROWER).intValue();
		adminDashboardDetails.setBorrowersCount(borrowerCount);
		int lenderCount = userRepo.countByPrimaryType(PrimaryType.LENDER).intValue();
		adminDashboardDetails.setLendersCount(lenderCount);
		adminDashboardDetails.setRegisteredUsersCount(borrowerCount + lenderCount);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		adminDashboardDetails
				.setTodayRegisteredUsersCount(userRepo.countByRegisteredOnGreaterThanEqual(calendar.getTime()));
		Double borrowerSum = oxyLoanRequestRepo.sumByUserPrimaryType(PrimaryType.BORROWER);

		Double totalDisbursedAmount = oxyLoanRepo.findByDisbursedLoanAmount(Status.DISBURSED.toString());
		double convertedTotalDisbursedAmount = totalDisbursedAmount == null ? 0.0d : totalDisbursedAmount;
		adminDashboardDetails.setTotalDisbursedAmount(convertedTotalDisbursedAmount);

		Double borrowerSumOffer = oxyLoanRequestRepo
				.sumByUserPrimaryTypeAndLoanOfferedAmount(PrimaryType.BORROWER.toString());
		if (borrowerSumOffer != null) {
			adminDashboardDetails.setTotalOfferAmount(borrowerSumOffer);
		}
		adminDashboardDetails.setBorrowersRequestedAmount(borrowerSum == null ? 0d : borrowerSum);
		Double lenderSum = oxyLoanRequestRepo.sumByUserPrimaryTypeAndParentRequestIdIsNull(PrimaryType.LENDER);
		adminDashboardDetails.setLendersCommitedAmount(lenderSum == null ? 0d : lenderSum);

		adminDashboardDetails.setNoOfAggrements(
				oxyLoanRepo.countByLoanStatusIn(LoanStatus.Agreed, LoanStatus.Active, LoanStatus.Closed));
		adminDashboardDetails.setNoOfConversationRequests(new Long(lenderBorrowerConversationRepo.count()).intValue());

		int count = oxyUserTypeRepo.countByCreatedOnGreaterThanEqual(calendar.getTime());

		adminDashboardDetails.setTodayRegisteredPartnersCount(count);

		int totalNoOfPartners = oxyUserTypeRepo.countOfPartnersRegistered();

		adminDashboardDetails.setTotalRegisteredPartners(totalNoOfPartners);

		return adminDashboardDetails;
	}

	@Override
	public LoanResponseDto loanRequest(int userId, LoanRequestDto loanRequestDto) {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanResponseDto updateRespondedLoanRequest(int userId, Integer loanRequestId,
			LoanRequestDto loanRequestDto) {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	@Transactional
	public LoanResponseDto updateLoanRequest(int userId, Integer loanRequestId,
			BorrowerDetailsRequestDto borrowerDetailsRequestDto) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}
		LoanRequest loanRequest = loanRequestRepo.findById(loanRequestId).get();
		if (loanRequest.getComments() == null) {
			BorrowerDetailsRequestDto borrowerDetails = new BorrowerDetailsRequestDto();
			borrowerDetails.setLocation(borrowerDetailsRequestDto.getLocation());
			borrowerDetails.setLocationResidence(borrowerDetailsRequestDto.getLocationResidence());
			borrowerDetails.setCompanyName(borrowerDetailsRequestDto.getCompanyName());
			borrowerDetails.setCompanyResidence(borrowerDetailsRequestDto.getCompanyResidence());
			borrowerDetails.setRole(borrowerDetailsRequestDto.getRole());
			borrowerDetails.setLoanRequirement(borrowerDetailsRequestDto.getLoanRequirement());
			borrowerDetails.setEmi(borrowerDetailsRequestDto.getEmi());
			borrowerDetails.setSalary(borrowerDetailsRequestDto.getSalary());
			borrowerDetails.setEligibility(borrowerDetailsRequestDto.getEligibility());
			borrowerDetails.setCibilPassword(borrowerDetailsRequestDto.getCibilPassword());
			borrowerDetails.setAadharPassword(borrowerDetailsRequestDto.getAadharPassword());
			borrowerDetails.setPanPassword(borrowerDetailsRequestDto.getPanPassword());
			borrowerDetails.setBankPassword(borrowerDetailsRequestDto.getBankPassword());
			borrowerDetails.setPayslipsPassword(borrowerDetailsRequestDto.getPayslipsPassword());
			borrowerDetails.setComments(borrowerDetailsRequestDto.getComments());
			Gson gson = new Gson();
			String json = gson.toJson(borrowerDetails);

			loanRequest.setComments(json);
			loanRequestRepo.save(loanRequest);

		} else {
			String comments = loanRequest.getComments();
			int intIndex = comments.indexOf("locationResidence");
			if (intIndex == -1) {
				BorrowerDetailsRequestDto borrowerDetails = new BorrowerDetailsRequestDto();
				borrowerDetails.setLocation(borrowerDetailsRequestDto.getLocation());
				borrowerDetails.setLocationResidence(borrowerDetailsRequestDto.getLocationResidence());
				borrowerDetails.setCompanyName(borrowerDetailsRequestDto.getCompanyName());
				borrowerDetails.setCompanyResidence(borrowerDetailsRequestDto.getCompanyResidence());
				borrowerDetails.setRole(borrowerDetailsRequestDto.getRole());
				borrowerDetails.setLoanRequirement(borrowerDetailsRequestDto.getLoanRequirement());
				borrowerDetails.setEmi(borrowerDetailsRequestDto.getEmi());
				borrowerDetails.setSalary(borrowerDetailsRequestDto.getSalary());
				borrowerDetails.setEligibility(borrowerDetailsRequestDto.getEligibility());
				borrowerDetails.setCibilPassword(borrowerDetailsRequestDto.getCibilPassword());
				borrowerDetails.setAadharPassword(borrowerDetailsRequestDto.getAadharPassword());
				borrowerDetails.setPanPassword(borrowerDetailsRequestDto.getPanPassword());
				borrowerDetails.setBankPassword(borrowerDetailsRequestDto.getBankPassword());
				borrowerDetails.setPayslipsPassword(borrowerDetailsRequestDto.getPayslipsPassword());
				borrowerDetails.setComments(borrowerDetailsRequestDto.getComments());
				Gson gson = new Gson();
				String json = gson.toJson(borrowerDetails);

				loanRequest.setComments(json);
				loanRequestRepo.save(loanRequest);
			} else {
				Gson gson = new Gson();
				BorrowerDetailsRequestDto borrowerComments = gson.fromJson(comments, BorrowerDetailsRequestDto.class);
				if (borrowerDetailsRequestDto.getLocation() != null
						&& borrowerDetailsRequestDto.getLocation().toString().length() > 0) {
					borrowerComments.setLocation(borrowerDetailsRequestDto.getLocation());
				}
				if (borrowerDetailsRequestDto.getLocationResidence() != null
						&& borrowerDetailsRequestDto.getLocationResidence().toString().length() > 0) {
					borrowerComments.setLocationResidence(borrowerDetailsRequestDto.getLocationResidence());
				}
				if (borrowerDetailsRequestDto.getCompanyName() != null
						&& borrowerDetailsRequestDto.getCompanyName().toString().length() > 0) {
					borrowerComments.setCompanyName(borrowerDetailsRequestDto.getCompanyName());
				}
				if (borrowerDetailsRequestDto.getCompanyResidence() != null
						&& borrowerDetailsRequestDto.getCompanyResidence().toString().length() > 0) {
					borrowerComments.setCompanyResidence(borrowerDetailsRequestDto.getCompanyResidence());
				}
				if (borrowerDetailsRequestDto.getRole() != null
						&& borrowerDetailsRequestDto.getRole().toString().length() > 0) {
					borrowerComments.setRole(borrowerDetailsRequestDto.getRole());
				}
				if (borrowerDetailsRequestDto.getLoanRequirement() != null
						&& borrowerDetailsRequestDto.getLoanRequirement().toString().length() > 0) {
					borrowerComments.setLoanRequirement(borrowerDetailsRequestDto.getLoanRequirement());
				}
				if (borrowerDetailsRequestDto.getEmi() != null
						&& borrowerDetailsRequestDto.getEmi().toString().length() > 0) {
					borrowerComments.setEmi(borrowerDetailsRequestDto.getEmi());
				}
				if (borrowerDetailsRequestDto.getSalary() != null
						&& borrowerDetailsRequestDto.getSalary().toString().length() > 0) {
					borrowerComments.setSalary(borrowerDetailsRequestDto.getSalary());
				}
				if (borrowerDetailsRequestDto.getEligibility() != null
						&& borrowerDetailsRequestDto.getEligibility().toString().length() > 0) {
					borrowerComments.setEligibility(borrowerDetailsRequestDto.getEligibility());
				}
				if (borrowerDetailsRequestDto.getCibilPassword() != null
						&& borrowerDetailsRequestDto.getCibilPassword().toString().length() > 0) {
					borrowerComments.setCibilPassword(borrowerDetailsRequestDto.getCibilPassword());
				}
				if (borrowerDetailsRequestDto.getAadharPassword() != null
						&& borrowerDetailsRequestDto.getAadharPassword().toString().length() > 0) {
					borrowerComments.setAadharPassword(borrowerDetailsRequestDto.getAadharPassword());
				}
				if (borrowerDetailsRequestDto.getPanPassword() != null
						&& borrowerDetailsRequestDto.getPanPassword().toString().length() > 0) {
					borrowerComments.setPanPassword(borrowerDetailsRequestDto.getPanPassword());
				}
				if (borrowerDetailsRequestDto.getBankPassword() != null
						&& borrowerDetailsRequestDto.getBankPassword().toString().length() > 0) {
					borrowerComments.setBankPassword(borrowerDetailsRequestDto.getBankPassword());
				}
				if (borrowerDetailsRequestDto.getPayslipsPassword() != null
						&& borrowerDetailsRequestDto.getPayslipsPassword().toString().length() > 0) {
					borrowerComments.setPayslipsPassword(borrowerDetailsRequestDto.getPayslipsPassword());
				}
				if (borrowerDetailsRequestDto.getComments() != null
						&& borrowerDetailsRequestDto.getComments().toString().length() > 0) {
					borrowerComments.setComments(borrowerDetailsRequestDto.getComments());
				}

				Gson gsonDetails = new Gson();
				String json = gsonDetails.toJson(borrowerComments);

				loanRequest.setComments(json);
				loanRequestRepo.save(loanRequest);
			}
		}
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setUserId(loanRequest.getUserId());
		loanResponseDto.setLoanRequestId(loanRequest.getId());
		loanResponseDto.setLoanRequest(loanRequest.getLoanRequestId());
		return loanResponseDto;
	}

	@Override
	public LoanResponseDto getLoanRequest(int userId, int requestId) {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanResponseDto getDefaultLoanRequest(int userId) {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public List<UserResponse> getParticipatedUsers(int requestId) {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanResponseDto actOnRequest(int userId, Integer loanRequestId, LoanStatus loanStatus)
			throws PdfGeenrationException, FileNotFoundException, IOException {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public SearchResultsDto<LoanResponseDto> searchRequests(int userId, SearchRequestDto searchRequestDto) {
		SearchResultsDto<LoanResponseDto> searchRequests = super.searchRequests(userId, searchRequestDto);
		return searchRequests;
	}

	@Override
	protected void populateExtraUserDetails(User user, UserResponse userResponse) {
		userResponse.setMobileNumber(user.getMobileNumber());
		userResponse.setAddress(userResponse.getAddress());
		List<UserDocumentStatus> profilePicDocs = userDocumentStatusRepo.findByUserIdAndDocumentType(user.getId(),
				DocumentType.Kyc);
		if (profilePicDocs != null) {
			for (UserDocumentStatus profilePicDoc : profilePicDocs) {
				String generateKycDownloadUrl = generateKycDownloadUrl(profilePicDoc);
				switch (KycType.valueOf(profilePicDoc.getDocumentSubType())) {
				case AADHAR:
					userResponse.setAadharFileUrl(generateKycDownloadUrl);
					break;
				case PAN:
					userResponse.setPanUrl(generateKycDownloadUrl);
					break;
				case PASSPORT:
					userResponse.setPassportUrl(generateKycDownloadUrl);
					break;

				case CHEQUELEAF:
					userResponse.setChequeLeafUrl(generateKycDownloadUrl);
					break;
				default:
					break;
				}
			}
		}
	}

	@Override
	public SearchResultsDto<LoanResponseDto> searchLoans(int userId, SearchRequestDto searchRequestDto) {
		return super.searchLoans(userId, searchRequestDto);
	}

	/*
	 * @Override public KycFileResponse downloadLoanAgrement(int userId, Integer
	 * loanRequestId) { throw new
	 * OperationNotAllowedException("As asn Admin you can not perform this operation"
	 * , ErrorCodes.PERMISSION_DENIED); }
	 */

	@Override
	public LoanResponseDto uploadEsignedAgreementPdf(int userId, Integer loanRequestId,
			UploadAgreementRequestDto agreementFileRequest) {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	@Transactional
	public LoanResponseDto feePaid(int loanId, PrimaryType primaryType, LoanRequestDto loanRequestDto) {
		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}
		OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get();
		if (oxyLoan.getLoanStatus() != LoanStatus.Active) {
			throw new ActionNotAllowedException("Loan is not active to pay the fee. Please check the details once.",
					ErrorCodes.ENTITY_NOT_ACTIVE);
		}
		double feePaid = (loanRequestDto == null ? 0d : loanRequestDto.getFeePaid());
		switch (primaryType) {
		case PrimaryType.BORROWER:
			oxyLoan.setBorrowerFeePaid(true);
			oxyLoan.setBorrowerTransactionFee(feePaid);
			break;
		case PrimaryType.LENDER:
			if (!oxyLoan.isBorrowerFeePaid()) {
				throw new ActionNotAllowedException("Borrower has to pay fee first", ErrorCodes.BORROWER_NOT_PAID);
			}
			oxyLoan.setLenderFeePaid(true);
			oxyLoan.setLenderTransactionFee(feePaid);
			break;
		default:
			break;
		}
		oxyLoan = oxyLoanRepo.save(oxyLoan);
		LoanResponseDto response = new LoanResponseDto();
		response.setId(oxyLoan.getId());
		return response;
	}

	@Override
	protected AdminDashboardDetails populatedDetails(User user, Object[] objects) {
		AdminDashboardDetails oxyAccountDetailsDto = new AdminDashboardDetails();

		oxyAccountDetailsDto.setAmountDisbursed(objects[0] == null ? 0d : (Double) objects[0]);
		oxyAccountDetailsDto.setInterestPaid(objects[1] == null ? 0d : (Double) objects[1]);
		oxyAccountDetailsDto.setTotalTransactionFee(objects[2] == null ? 0d : (Double) objects[2]);
		oxyAccountDetailsDto.setPrincipalReceived(objects[3] == null ? 0d : (Double) objects[3]);
		if (oxyAccountDetailsDto.getAmountDisbursed() != null) {
			oxyAccountDetailsDto.setOutstandingAmount(
					oxyAccountDetailsDto.getAmountDisbursed() - oxyAccountDetailsDto.getPrincipalReceived());
		}
		Long failedEmisCount = (Long) objects[4];
		oxyAccountDetailsDto.setNoOfFailedEmis(failedEmisCount == null ? 0 : failedEmisCount.intValue());
		return oxyAccountDetailsDto;
	}

	@Override
	@Transactional
	public LoanEmiCardResponseDto emiPaid(int loanId, int emiNumber, LoanEmiCardResponseDto request) {
		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}
		OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get(); // To make sure loan exists
		LoanEmiCard loanEmiCard = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId, emiNumber);
		if (loanEmiCard == null) {
			throw new NoSuchElementException("No records present with loan " + loanId + " and emi number " + emiNumber);
		}
		if (loanEmiCard.getEmiPaidOn() != null) {
			throw new ActionNotAllowedException("This EMI is already paid.", ErrorCodes.ACTION_ALREDY_DONE);
		}
		int nonPaidEmisBefore = loanEmiCardRepo.countByLoanIdAndEmiNumberLessThanAndEmiPaidOnNull(loanId, emiNumber);
		if (emiNumber > 1 && nonPaidEmisBefore > 0) {
			throw new ActionNotAllowedException("Previous EMIs are not paid.", ErrorCodes.BORROWER_NOT_PAID);
		}
		loanEmiCard.setEmiPaidOn(Calendar.getInstance().getTime());
		if (request != null && request.getComments() != null) {
			loanEmiCard.setComments(request.getComments());
		}
		loanEmiCardRepo.save(loanEmiCard);
		if (oxyLoan.getDuration().intValue() == emiNumber) {
			oxyLoan.setLoanStatus(LoanStatus.Closed);
			oxyLoan = oxyLoanRepo.save(oxyLoan);
			LoanRequest loanRequest = loanRequestRepo.findById(oxyLoan.getLoanRespondId()).get();
			loanRequest.setLoanStatus(LoanStatus.Closed);
			loanRequestRepo.save(loanRequest);
			LoanRequest lenderLoanRequest = loanRequestRepo
					.findByUserIdAndParentRequestIdIsNull(oxyLoan.getLenderUserId());
			lenderLoanRequest.setDisbursmentAmount(
					lenderLoanRequest.getDisbursmentAmount().doubleValue() - oxyLoan.getDisbursmentAmount());
			LoanRequest borrowerLoanRequest = loanRequestRepo
					.findByUserIdAndParentRequestIdIsNull(oxyLoan.getBorrowerUserId());
			borrowerLoanRequest.setDisbursmentAmount(
					borrowerLoanRequest.getDisbursmentAmount().doubleValue() - oxyLoan.getDisbursmentAmount());
			lenderLoanRequest = loanRequestRepo.save(lenderLoanRequest);
			borrowerLoanRequest = loanRequestRepo.save(borrowerLoanRequest);
		}
		oxyLoan.setPrincipalRepaid(oxyLoan.getPrincipalRepaid() + loanEmiCard.getEmiPrincipalAmount());
		oxyLoan.setLoanInterestPaid(oxyLoan.getLoanInterestPaid() + loanEmiCard.getEmiInterstAmount());
		oxyLoan = oxyLoanRepo.save(oxyLoan);
		LoanEmiCardResponseDto response = new LoanEmiCardResponseDto();
		response.setId(loanEmiCard.getId());
		return response;
	}

	@Override
	public LoanEmiCardResponseDto markEmiFailed(int loanId, int emiNumber) {
		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}
		OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get(); // To make sure loan exists
		LoanEmiCard loanEmiCard = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId, emiNumber);
		if (loanEmiCard == null) {
			throw new NoSuchElementException("No records present with loan " + loanId + " and emi number " + emiNumber);
		}
		if (loanEmiCard.getEmiPaidOn() != null) {
			throw new ActionNotAllowedException("This EMI is already paid.", ErrorCodes.ACTION_ALREDY_DONE);
		}
		loanEmiCard.setPaidOnTime(false);

		loanEmiCard = loanEmiCardRepo.save(loanEmiCard);
		oxyLoan.setNoOfFailedEmis(oxyLoan.getNoOfFailedEmis() + 1);
		oxyLoan = oxyLoanRepo.save(oxyLoan);

		LoanEmiCardResponseDto response = new LoanEmiCardResponseDto();
		response.setId(loanEmiCard.getId());
		return response;
	}

	@Transactional
	public void migrateOldSystemToNewSystem() {
		Iterable<OxyLoan> loans = oxyLoanRepo.findAll();
		if (loans != null) {
			for (OxyLoan loan : loans) {
				LoanRequest loanRequest = oxyLoanRequestRepo.findByLoanId(loan.getLoanId());
				if (loanRequest != null) {
					generateEmiCard(loan, loanRequest);
				} else {
					logger.info("Skipping EMI generation for loan {}", loan.getId());
				}
			}
		}
	}

	@Override
	@Transactional
	public void deleteLoans(int userId) {
		LoanRequest parentRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
		int count = loanRequestRepo.countByParentRequestId(parentRequest.getId());
		if (count > 0) {
			throw new ActionNotAllowedException("There are loans responded to this user", ErrorCodes.INVALID_OPERATION);
		}
		loanRequestRepo.deleteByUserIdAndParentRequestIdIsNotNull(userId);
	}

	@Override
	@Transactional
	public void changeDefaultLoanPrimaryType(int userId, PrimaryType newPrimaryType) {
		LoanRequest parentRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
		parentRequest.setUserPrimaryType(newPrimaryType);
		PrimaryType oldPrimaryType = (newPrimaryType == PrimaryType.BORROWER) ? PrimaryType.LENDER
				: PrimaryType.BORROWER;
		parentRequest.setLoanRequestId(parentRequest.getLoanRequestId().replaceFirst(oldPrimaryType.getShortCode(),
				newPrimaryType.getShortCode()));
		loanRequestRepo.save(parentRequest);
	}

	@Override
	@Transactional
	public LoanEmiCardResponseDto loanPreClose(int loanId) {
		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}
		final OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get(); // To make sure loan exists
		List<LoanEmiCard> loanEmiCards = loanEmiCardRepo.findByLoanIdOrderByEmiNumber(loanId);

		if (oxyLoan.getLoanStatus() == LoanStatus.Closed) {
			throw new ActionNotAllowedException("This Loan + " + loanId + " is already Closed.",
					ErrorCodes.ACTION_ALREDY_DONE);
		}

		if (loanEmiCards != null && !loanEmiCards.isEmpty()) {
			LoanEmiCard firstNonPaidEmiCard = loanEmiCardRepo
					.findTop1ByLoanIdAndAndEmiPaidOnNullOrderByEmiNumber(loanId);

			loanEmiCards.stream().forEach(emiCard -> {
				/*
				 * if (emiCard.getEmiNumber().intValue() <
				 * firstNonPaidEmiCard.getEmiNumber().intValue()) { return; }
				 */
				emiCard.setEmiPaidOn(new Date());
				emiCard.setComments("Pre Closure");
				emiCard.setStatus(com.oxyloans.entity.loan.LoanEmiCard.Status.COMPLETED);
				oxyLoan.setPrincipalRepaid(oxyLoan.getPrincipalRepaid() + emiCard.getEmiPrincipalAmount());
				/*
				 * if (emiCard.getEmiNumber().intValue() >
				 * firstNonPaidEmiCard.getEmiNumber().intValue()) {
				 * emiCard.setEmiInterstAmount(0d); }
				 */
				oxyLoan.setLoanInterestPaid(oxyLoan.getLoanInterestPaid() + emiCard.getEmiInterstAmount());
			});
			loanEmiCardRepo.saveAll(loanEmiCards);
		} else {

			List<ApplicationLevelLoanEmiCard> loanEmiCardForApplicationLevel = applicationLevelLoanEmiCardRepo
					.findingListOfEmis(oxyLoan.getBorrowerParentRequestId());
			if (loanEmiCardForApplicationLevel != null && !loanEmiCardForApplicationLevel.isEmpty()) {
				loanEmiCardForApplicationLevel.stream().forEach(applicationLevel -> {
					applicationLevel.setEmiPaidOn(new Date());
					applicationLevel.setStatus(ApplicationLevelLoanEmiCard.Status.Paid);
				});
				applicationLevelLoanEmiCardRepo.saveAll(loanEmiCardForApplicationLevel);
			}

		}

		oxyLoan.setLoanStatus(LoanStatus.Closed);
		oxyLoanRepo.save(oxyLoan);

		LoanRequest loanRequest = loanRequestRepo.findById(oxyLoan.getLoanRespondId()).get();
		loanRequest.setLoanStatus(LoanStatus.Closed);
		loanRequestRepo.save(loanRequest);
		LoanRequest lenderLoanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(oxyLoan.getLenderUserId());
		lenderLoanRequest.setDisbursmentAmount(
				lenderLoanRequest.getDisbursmentAmount().doubleValue() - oxyLoan.getDisbursmentAmount());
		LoanRequest borrowerLoanRequest = loanRequestRepo
				.findByUserIdAndParentRequestIdIsNull(oxyLoan.getBorrowerUserId());
		if (borrowerLoanRequest != null) {
			borrowerLoanRequest.setDisbursmentAmount(
					borrowerLoanRequest.getDisbursmentAmount().doubleValue() - oxyLoan.getDisbursmentAmount());
			borrowerLoanRequest = loanRequestRepo.save(borrowerLoanRequest);
		}
		lenderLoanRequest = loanRequestRepo.save(lenderLoanRequest);

		LoanEmiCardResponseDto response = new LoanEmiCardResponseDto();
		response.setLoanId(loanId);
		return response;
	}

	@Override
	public SearchResultsDto<LoanResponseDto> pendingEmis(int year, int month, String type, int pageNo, int pageSize) {
		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}

		if (type.equalsIgnoreCase("Current")) {
			List<LoanResponseDto> currentMoantLoanResponse = new ArrayList<LoanResponseDto>();
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, 2020);
			calendar.set(Calendar.MONTH, month - 1);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			Date monthStart = calendar.getTime();
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			Date monthEnd = calendar.getTime();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getCurrentMonthEmis(monthStart, monthEnd, pageNo,
					pageSize);
			Integer totalCount = oxyLoanRepo.getCurrentcount(monthStart, monthEnd);
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				currentMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(currentMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;
		} else if (type.equalsIgnoreCase("feature")) {

			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, 2020);
			calendar.set(Calendar.MONTH, month - 1);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			Date monthStart = calendar.getTime();
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getFeatureMonthEmi(monthStart, pageNo, pageSize);
			Integer totalCount = oxyLoanRepo.getFeatureCount(monthStart);
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;

		}

		else {
			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.gettotalEmis(pageNo, pageSize);
			Integer totalCount = oxyLoanRepo.getTotal();
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;
		}

	}

	@Override
	@Transactional
	public UserResponse sendReport(int userId, UserRequest userRequest) throws PdfGeenrationException, IOException {

		int currentUser = authorizationService.getCurrentUser();
		User user1 = userRepo.findById(currentUser).get();
		if (user1.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user1.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);

		}

		if (userRequest.getAdminComments().equals(Status.APPROVED.toString())) {
			if (userRequest.getAdminComments().equals(Status.APPROVED.toString())) {
				LoanRequest loanRequest2 = loanRequestRepo.findByLoanId(userRequest.getLoanId());
				if (loanRequest2 == null) {
					throw new ActionNotAllowedException("Borrower don't have any loans", ErrorCodes.PERMISSION_DENIED);

				}
				if (!loanRequest2.getLoanStatus().equals(LoanStatus.Accepted)) {
					throw new ActionNotAllowedException("Loan is not in Agreed", ErrorCodes.PERMISSION_DENIED);
				}
				loanRequest2.setAdminComments(Status.APPROVED.toString());
				OxyLoan oxyLoan = oxyLoanRepo.findByLoanId(userRequest.getLoanId());

				if (oxyLoan != null) {
					User borroweUser = userRepo.findById(oxyLoan.getBorrowerUserId()).get();
					User lenderUser = userRepo.findById(oxyLoan.getLenderUserId()).get();
					PersonalDetails borrowerPersonalDetails = borroweUser.getPersonalDetails();
					PersonalDetails lenderPersonalDetails = lenderUser.getPersonalDetails();
					if (borroweUser.getPrimaryType() == PrimaryType.BORROWER) {
						if (borrowerPersonalDetails != null) {
							if (!utm.equals(borroweUser.getUrchinTrackingModule())) {
								TemplateContext context = new TemplateContext();
								context.put("requesterName",
										borrowerPersonalDetails.getFirstName() + borrowerPersonalDetails.getLastName());
								context.put("date", dateFormate.format(new Date()));
								String mailsubject = "Loan approval"; // Mail Goes to Borrower so subject is like this..
								String emailTemplateName = "admin-approved.template";
								EmailRequest emailRequest = new EmailRequest(new String[] { borroweUser.getEmail() },
										mailsubject, emailTemplateName, context);
								EmailResponse emailResponse = emailService.sendEmail(emailRequest);
								if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
									throw new RuntimeException(emailResponse.getErrorMessage());
								}
								String templateName = "Esign-Approval";
								MobileRequest mobileRequest = new MobileRequest(borroweUser.getMobileNumber(),
										templateName);
								mobileRequest.setSenderId("OXYLON");
								try {
									mobileUtil.sendTransactionalMessage(mobileRequest);
								} catch (Exception e1) {
									logger.error("SMS failed : ", e1);
								}

							}
						}
					}
					/*
					 * if (!utm.equals(borroweUser.getUrchinTrackingModule())) { if
					 * (lenderUser.getPrimaryType() == PrimaryType.LENDER) { if
					 * (lenderPersonalDetails != null) { TemplateContext context = new
					 * TemplateContext(); context.put("requesterName",
					 * lenderPersonalDetails.getFirstName() + lenderPersonalDetails.getLastName());
					 * context.put("date", dateFormate.format(new Date())); String mailsubject =
					 * "Loan approval"; // Mail Goes to Borrower so subject is like this.. String
					 * emailTemplateName = "admin-approved.template"; EmailRequest emailRequest =
					 * new EmailRequest(new String[] { lenderUser.getEmail() }, mailsubject,
					 * emailTemplateName, context); EmailResponse emailResponse =
					 * emailService.sendEmail(emailRequest); if (emailResponse.getStatus() ==
					 * EmailResponse.Status.FAILED) { throw new
					 * RuntimeException(emailResponse.getErrorMessage()); } String templateName =
					 * "Esign-Approval"; MobileRequest mobileRequest = new
					 * MobileRequest(lenderUser.getMobileNumber(), templateName);
					 * mobileRequest.setSenderId("OXYLON"); try {
					 * mobileUtil.sendTransactionalMessage(mobileRequest); } catch (Exception e1) {
					 * logger.error("SMS failed : ", e1); }
					 * 
					 * }
					 * 
					 * } }
					 */
				}

				loanRequestRepo.save(loanRequest2);
			}

		}
		if (userRequest.getAdminComments().equals(Status.INTERESTED.toString())
				&& userRequest.getAdminComments() != null) {
			logger.info("Interested status update start");
			User user = userRepo.findById(userRequest.getId()).get();
			if (user == null) {
				throw new OperationNotAllowedException("User is Alredy in INTERESTED", ErrorCodes.ENITITY_NOT_FOUND);

			}
			LoanRequest loanRequestDetails = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userRequest.getId());
			if (loanRequestDetails.getComments() != null) {
				if (user != null) {
					User interestedUser = userRepo.findByMobileNumberAndAdminComments(user.getMobileNumber(),
							Status.INTERESTED.toString());
					if (interestedUser != null) {
						throw new OperationNotAllowedException("User is Alredy iS in  INTERESTED",
								ErrorCodes.ACTION_ALREDY_DONE);
					}
				}
				PersonalDetails personalDetails = user.getPersonalDetails();
				LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId());
				user.setAdminComments(Status.INTERESTED.toString());
				if (!utm.equals(user.getUrchinTrackingModule())) {
					String templateName = "Approved-Borrower";
					MobileRequest mobileRequest = new MobileRequest(user.getMobileNumber(), templateName);
					mobileRequest.setSenderId("OXYLON");

					try {
						mobileUtil.sendTransactionalMessage(mobileRequest);
					} catch (Exception e1) {
						logger.error("SMS failed : ", e1);
					}
				}
				// userRepo.save(user);
				if (user.getPersonalDetails() == null || userDocumentStatusRepo
						.findByUserIdAndDocumentType(user.getId(), DocumentType.Kyc).isEmpty()) {
					throw new ActionNotAllowedException("User Details and Documents not given",
							ErrorCodes.PERMISSION_DENIED);
				}

				else {
					/*
					 * if (user.getStatus().toString().equals(Status.REGISTERED.toString())) {
					 * user.setStatus(Status.ACTIVE); userRepo.save(user); }
					 */
				}
				if (!utm.equals(user.getUrchinTrackingModule())) {
					TemplateContext context = new TemplateContext();
					context.put("id", user.getId());
					context.put("mobileNumber", user.getMobileNumber());
					context.put("email", user.getEmail());
					context.put("amount", loanRequest.getLoanRequestAmount());
					context.put("type", user.getPrimaryType());
					context.put("date", user.getRegisteredOn());
					if (personalDetails != null) {
						context.put("address", personalDetails.getAddress() != null);
						context.put("name", personalDetails.getFirstName() + personalDetails.getLastName());
					}
					if (user.getPrimaryType().equals(PrimaryType.LENDER)) {
						context.put("id", "LR" + user.getId());
					}
					if (loanRequest.getComments() != null) {
						context.put("comments", loanRequest.getComments());
					}
					String mailsubject = "Report"; // Mail Goes to Borrower so subject is like this..
					String emailTemplateName = "report.template";

					EmailRequest emailRequest = new EmailRequest(new String[] { userRequest.getEmail() }, mailsubject,
							emailTemplateName, context);
					EmailResponse emailResponse = emailService.sendEmail(emailRequest);
					if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
						throw new RuntimeException(emailResponse.getErrorMessage());
					}

					EmailRequest emailRequestForUser = new EmailRequest(new String[] { user.getEmail() }, mailsubject,
							emailBody);
					EmailResponse emailResponseForUser = emailService.sendEmail(emailRequestForUser);
					if (emailResponseForUser.getStatus() == EmailResponse.Status.FAILED) {
						throw new RuntimeException(emailResponse.getErrorMessage());
					}
				}
				if (user.getPrimaryType().equals(PrimaryType.BORROWER)) {
					TemplateContext context = new TemplateContext();
					List<UserDocumentStatus> userDocumentStatus = userDocumentStatusRepo.findByUserId(user.getId());
					int count = 0;
					DocumentDetailsResponseDto documentDetailsResponseDto = new DocumentDetailsResponseDto();
					for (UserDocumentStatus userDocuments : userDocumentStatus) {

						String kycType = userDocuments.getDocumentSubType();

						KycFileResponse kycFileResponse = userServiceImpl.downloadFile(user.getId(),
								KycType.valueOf(kycType));
						String urlDownload = kycFileResponse.getDownloadUrl();

						String[] splitingTheUrl = urlDownload.split(Pattern.quote("?"));
						String url = splitingTheUrl[0];
						if (kycFileResponse.getKycType().equals(KycType.PAN)) {
							documentDetailsResponseDto.setPanUrl(url);
						} else {
							if (kycFileResponse.getKycType().equals(KycType.BANKSTATEMENT)) {
								documentDetailsResponseDto.setBankstatementsUrl(url);
							}
							if (kycFileResponse.getKycType().equals(KycType.PAYSLIPS)) {
								documentDetailsResponseDto.setPayslipsUrl(url);
							}
							if (kycFileResponse.getKycType().equals(KycType.AADHAR)) {
								documentDetailsResponseDto.setAadharUrl(url);
							}
							if (kycFileResponse.getKycType().equals(KycType.DRIVINGLICENCE)) {
								documentDetailsResponseDto.setDrivinglicenceUrl(url);
							}
							if (kycFileResponse.getKycType().equals(KycType.VOTERID)) {
								documentDetailsResponseDto.setVoteridUrl(url);
							}
							if (kycFileResponse.getKycType().equals(KycType.PASSPORT)) {
								documentDetailsResponseDto.setPassportUrl(url);
							}
							if (kycFileResponse.getKycType().equals(KycType.EXPERIAN)) {
								documentDetailsResponseDto.setExperianUrl(url);
							}
							if (kycFileResponse.getKycType().equals(KycType.CIBILSCORE)) {
								documentDetailsResponseDto.setCibilscoreUrl(url);
							}

						}

					}
					LoanRequest loanRequst = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId());
					if (loanRequst != null) {
						String comments = loanRequst.getComments();
						Gson gson = new Gson();
						BorrowerDetailsRequestDto borrowerdetails = gson.fromJson(comments,
								BorrowerDetailsRequestDto.class);
						context.put("location", borrowerdetails.getLocation());
						context.put("locationResidence", borrowerdetails.getLocationResidence());
						context.put("companyName", borrowerdetails.getCompanyName());
						context.put("companyResidence", borrowerdetails.getCompanyResidence());
						context.put("role", borrowerdetails.getRole());
						context.put("loanRequirment", borrowerdetails.getLoanRequirement());
						context.put("emi", borrowerdetails.getEmi());
						context.put("salary", borrowerdetails.getSalary());
						context.put("eligibility", borrowerdetails.getEligibility());
						context.put("comments", borrowerdetails.getComments());

						if (borrowerdetails.getCibilPassword() != null)
							context.put("cibilPassword", borrowerdetails.getCibilPassword());
						else
							context.put("cibilPassword", "N/A");

						if (borrowerdetails.getAadharPassword() != null)
							context.put("aadharPassword", borrowerdetails.getAadharPassword());
						else
							context.put("aadharPassword", "N/A");

						if (borrowerdetails.getBankPassword() != null)
							context.put("bankPassword", borrowerdetails.getBankPassword());
						else
							context.put("bankPassword", "N/A");

						if (borrowerdetails.getPayslipsPassword() != null)
							context.put("payslipsPassword", borrowerdetails.getPayslipsPassword());
						else
							context.put("payslipsPassword", "N/A");

						if (borrowerdetails.getPanPassword() != null)
							context.put("panPassword", borrowerdetails.getPanPassword());
						else
							context.put("panPassword", "N/A");

					}
					String borrowerName = user.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase()
							+ user.getPersonalDetails().getFirstName().substring(1) + " "
							+ user.getPersonalDetails().getLastName();
					String borrowerId = user.getUniqueNumber();
					context.put("name", borrowerName);
					context.put("bankstatements", documentDetailsResponseDto.getBankstatementsUrl());
					context.put("payslips", documentDetailsResponseDto.getPayslipsUrl());
					String fourthDocument = null;
					if (documentDetailsResponseDto.getAadharUrl() != null) {
						fourthDocument = documentDetailsResponseDto.getAadharUrl();
					} else {
						if (documentDetailsResponseDto.getDrivinglicenceUrl() != null) {
							fourthDocument = documentDetailsResponseDto.getDrivinglicenceUrl();
						}
						if (documentDetailsResponseDto.getVoteridUrl() != null) {
							fourthDocument = documentDetailsResponseDto.getVoteridUrl();
						}
						if (documentDetailsResponseDto.getPassportUrl() != null) {
							fourthDocument = documentDetailsResponseDto.getPassportUrl();
						}
					}
					context.put("other", fourthDocument);

					context.put("experian", documentDetailsResponseDto.getExperianUrl());
					context.put("cibilscore", documentDetailsResponseDto.getCibilscoreUrl());
					context.put("pan", documentDetailsResponseDto.getPanUrl());
					LoanRequest loanRequestInfo = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId());
					int borrowerParentRequestId = 0;
					if (loanRequestInfo != null) {
						borrowerParentRequestId = loanRequestInfo.getId();
					}
					String writeComments = Integer.toString(borrowerParentRequestId);

					String verifyLink = new StringBuilder()
							.append(this.linkInInterestButton.replace("{writeComments}", writeComments)).toString();
					context.put("verifyLink", verifyLink);
					String mailsubject = "Interested Borrower Info" + "-" + borrowerId + " " + borrowerName;
					String emailTemplateName = "Interested-Borrower.template";

					EmailRequest emailRequest = new EmailRequest(
							new String[] { "ramadevithatavarti@gmail.com", "hydthatavarti@gmail.com",
									"narendra@oxyloans.com", "archana.n@oxyloans.com", "subbu@oxyloans.com" },
							mailsubject, emailTemplateName, context);
					EmailResponse emailResponse = emailService.sendEmail(emailRequest);
					if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
						throw new RuntimeException(emailResponse.getErrorMessage());
					}

				}
			} else {
				throw new OperationNotAllowedException("Borrower comments not updated please update",
						ErrorCodes.EMPTY_REQUEST);
			}
			userRepo.save(user);
			logger.info("Interested status update end");
		}
		if (userRequest.getAdminComments().equals(Status.DISBURSED.toString())) {
			OxyLoan loan = oxyLoanRepo.findByLoanId(userRequest.getLoanId());
			if (loan.getLoanStatus() != null) {

				if (!loan.getLoanStatus().toString().equalsIgnoreCase(Status.ACTIVE.toString())) {
					throw new ActionNotAllowedException("Loan is not Active", ErrorCodes.PERMISSION_DENIED);
				}
				if (loan.getLoanStatus().toString().equalsIgnoreCase(Status.ACTIVE.toString())) {

					User user = userRepo.findById(loan.getBorrowerUserId()).get();

					if (user != null) {
						String employmentType = null;
						if (user.getProfessionalDetails() != null) {
							employmentType = user.getProfessionalDetails().getEmployment().toString();
						}

						if (employmentType.equalsIgnoreCase(ProfessionalDetails.Employment.SALARIED.toString())) {

							LoanRequest loanRequest = loanRequestRepo
									.findByUserIdAndParentRequestIdIsNull(loan.getBorrowerUserId());
							LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();
							if (Double.compare((loanRequest.getDisbursmentAmount()),
									loanOfferdAmount.getLoanOfferedAmount()) == 0) {
								loanOfferdAmount.setLoanOfferdStatus(LoanOfferdStatus.LOANOFFERCOMPLETED);
								loanOfferdAmount.setLoanRequest(loanRequest);
								loanRequest.setLoanOfferedAmount(loanOfferdAmount);
								loanRequestRepo.save(loanRequest);
								/*
								 * User user1 = userRepo.findById(loan.getBorrowerUserId()).get();
								 * 
								 * ExecutorService executor = Executors.newFixedThreadPool(20); Runnable
								 * feeInvoiceThread = new FeeInvoiceThread(user1, pdfEngine, oxyLoanRequestRepo,
								 * fileManagementService); executor.submit(feeInvoiceThread);
								 */
							}

							try {
								loan.setBorrowerDisbursedDate(expectedDateFormat.parse(userRequest.getDisbursedDate()));
							} catch (ParseException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							loan.setAdminComments(Status.DISBURSED.toString());
							oxyLoanRepo.save(loan);

							double interestAmountForBorrower = 0.0;
							double interestAmountForLender = 0.0;

							String disbursedDate = userRequest.getDisbursedDate();
							String dateParts[] = disbursedDate.split("/");
							String emiDate = dateParts[0];
							Integer disbursedmentDate = Integer.parseInt(emiDate);
							Calendar c = Calendar.getInstance();

							int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
							int remainingDays = monthMaxDays - disbursedmentDate;

							int id = loan.getId();
							remainingDays = remainingDays + 5;
							List<LoanEmiCard> loanEmiCradDetails = loanEmiCardRepo.findByLoanId(id);
							int differenceInDays = 0;
							String firstEmiDueDate = null;
							if (loan.getDurationType().equals(DurationType.Months)) {
								for (LoanEmiCard firstEmiInformation : loanEmiCradDetails) {
									if (firstEmiInformation.getEmiNumber() == 1) {
										interestAmountForBorrower = firstEmiInformation.getEmiInterstAmount();
										interestAmountForLender = firstEmiInformation.getLenderEmiInterestAmount();

										firstEmiDueDate = expectedDateFormat.format(firstEmiInformation.getEmiDueOn());
										Date dateBefore = null;
										Date dateAfter = null;
										try {
											dateBefore = expectedDateFormat.parse(disbursedDate);
											dateAfter = expectedDateFormat.parse(firstEmiDueDate);
										} catch (ParseException e) {

											e.printStackTrace();
										}

										long difference = dateAfter.getTime() - dateBefore.getTime();
										float daysBetween = (difference / (1000 * 60 * 60 * 24));
										differenceInDays = (int) daysBetween;
									}

								}
								double interestPerDayToBorrower = (interestAmountForBorrower / monthMaxDays);
								double firstEmiInterestToBorrower = (differenceInDays * interestPerDayToBorrower);

								double interestPerDayToLender = (interestAmountForLender / monthMaxDays);
								double firstEmiInterestToLender = (differenceInDays * interestPerDayToLender);

								for (LoanEmiCard firstEmiInformation : loanEmiCradDetails) {
									if (firstEmiInformation.getEmiNumber() == 1) {
										firstEmiInformation.setEmiInterstAmount(Math.round(firstEmiInterestToBorrower));
										firstEmiInformation
												.setEmiAmount(Math.round(firstEmiInformation.getEmiPrincipalAmount()
														+ firstEmiInterestToBorrower));

										firstEmiInformation
												.setLenderEmiInterestAmount(Math.round(firstEmiInterestToLender));
										firstEmiInformation.setLenderEmiAmount(
												Math.round(firstEmiInformation.getLenderEmiPrincipalAmount()
														+ firstEmiInterestToLender));

									}
									loanEmiCardRepo.save(firstEmiInformation);
								}

							}

						} else { // partnership
									// disbursment starts

							String disbursedDateString = userRequest.getDisbursedDate();

							int borrowerParentRequestId = loan.getBorrowerParentRequestId();

							Date disbursedDate = null;
							try {
								disbursedDate = expectedDateFormat.parse(disbursedDateString);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							loan.setBorrowerDisbursedDate(disbursedDate);
							Double disburmentAmount = loan.getDisbursmentAmount();

							LoanRequest loanRequest = loanRequestRepo.findById(borrowerParentRequestId).get();

							loan.setAdminComments(Status.DISBURSED.toString());
							oxyLoanRepo.save(loan);
							// //aasbsabasbjasjhajhbfssdfsddfssdfsdfdsfdsfsdfsdsdfsdfsdfsdfsdfasdgfdfdjhjgghjgghjgj
							int id = loan.getId();

							int dealId = loan.getDealId();

							String dealName = oxyBorrowersDealsInformationRepo.findingDealNameBasedOnId(dealId);

							double interestAmount = 0.0;

							String dateParts[] = disbursedDateString.split("/");
							String emiDate = dateParts[0];
							Integer disbursedmentDate = Integer.parseInt(emiDate);
							Calendar c = Calendar.getInstance();

							int monthMaxDays = 30;

							List<LoanEmiCard> loanEmiCradDetails = loanEmiCardRepo.findByLoanId(id);
							int differenceInDays = 0;
							String firstEmiDueDate = null;
							if (loan.getDurationType().equals(DurationType.Months)) {

								Date dateOfDisbursment = null;
								Date dateOfFirstEmi = null;

								for (LoanEmiCard firstEmiInformation : loanEmiCradDetails) {
									if (firstEmiInformation.getEmiNumber() == 1) {
										interestAmount = firstEmiInformation.getEmiInterstAmount();
										firstEmiDueDate = expectedDateFormat.format(firstEmiInformation.getEmiDueOn());

										dateOfDisbursment = disbursedDate;
										try {
											dateOfFirstEmi = expectedDateFormat.parse(firstEmiDueDate);
										} catch (ParseException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

										long difference = dateOfFirstEmi.getTime() - dateOfDisbursment.getTime();
										float daysBetween = (difference / (1000 * 60 * 60 * 24));
										differenceInDays = (int) daysBetween;

										differenceInDays = differenceInDays - 30;
										logger.info(differenceInDays);

									}

								}
								if (differenceInDays > 20) {
									double interestPerDay = (interestAmount / monthMaxDays);

									double firstEmiInterest = (differenceInDays * interestPerDay);

									for (LoanEmiCard firstEmiInformation : loanEmiCradDetails) {
										if (firstEmiInformation.getEmiNumber() == 1) {
											firstEmiInformation.setEmiInterstAmount(Math.round(firstEmiInterest));
											firstEmiInformation.setEmiAmount(Math.round(
													firstEmiInformation.getEmiPrincipalAmount() + firstEmiInterest));

										}

										logger.info(firstEmiInformation.toString());

										loanEmiCardRepo.save(firstEmiInformation);

									}
								}
							}

							if (loanRequest != null) {
								generateBorrowerBankIciciExcel(loanRequest, disbursedDateString);
							}

						}
					}
				}
			}
			OxyLoan oxyLoan = oxyLoanRepo.findByLoanId(userRequest.getLoanId());
			int borrowerUserId = oxyLoan.getBorrowerUserId();
			int lenderUserId = oxyLoan.getLenderUserId();

			User user = userRepo.findById(borrowerUserId).get();
			User user2 = userRepo.findById(lenderUserId).get();

			String loanId = userRequest.getLoanId();

			LoanResponseDto response = borrowerDealStructure(userRequest.getLoanId());
			TemplateContext templateContext = new TemplateContext();
			templateContext.put("borrowerDealStrucuture", response.getDownloadUrl());

			String mailsubject = "BorrowerDealStructure"; // Mail to Borrower
			String emailTemplateName = "borrower-deal-structure.template";
			if (!utm.equals(user.getUrchinTrackingModule())) {
				EmailRequest emailRequest = new EmailRequest(new String[] { user.getEmail() }, mailsubject,
						emailTemplateName, templateContext);
				EmailResponse emailResponse = emailService.sendEmail(emailRequest);

				if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
					throw new RuntimeException(emailResponse.getErrorMessage());
				}
			}

			String templateName = "Disbursment";
			if (!utm.equals(user.getUrchinTrackingModule())) {
				MobileRequest mobileRequest = new MobileRequest(user.getMobileNumber(), templateName);
				mobileRequest.setSenderId("OXYLON");

				try {
					mobileUtil.sendTransactionalMessage(mobileRequest);
				} catch (Exception e1) {
					logger.error("SMS failed : ", e1);
				}
			}

			LoanResponseDto response1 = lenderDealStructure(loanId);
			TemplateContext context = new TemplateContext();
			context.put("lenderDealStrucuture", response1.getDownloadUrl());

			String mailsubject1 = "LenderDealStructure"; // Mail to Lender
			String emailTemplateName1 = "lender-deal-structure.template";

			EmailRequest emailRequest1 = new EmailRequest(new String[] { user2.getEmail() }, mailsubject1,
					emailTemplateName1, context);
			EmailResponse emailResponse1 = emailService.sendEmail(emailRequest1);

			if (emailResponse1.getStatus() == EmailResponse.Status.FAILED) {
				throw new RuntimeException(emailResponse1.getErrorMessage());
			}

			String templateName1 = "Disbursment";
			MobileRequest mobileRequest1 = new MobileRequest(user2.getMobileNumber(), templateName1);
			mobileRequest1.setSenderId("OXYLON");

			try {
				mobileUtil.sendTransactionalMessage(mobileRequest1);
			} catch (Exception e1) {
				logger.error("SMS failed : ", e1);
			}

			logger.info("Reference Amount Starts................................");
			Integer borrowerRefereeId = user.getId();

			LenderReferenceDetails borrowerRefereeInfo = lenderReferenceDetailsRepo.findRefereeInfo(borrowerRefereeId);

			if (borrowerRefereeInfo != null) {
				User borrowerReferee = userRepo.findById(borrowerRefereeInfo.getRefereeId()).get();
				if (borrowerReferee.getPrimaryType().equals(User.PrimaryType.BORROWER)) {

					Double disbursmentAmount = oxyLoan.getDisbursmentAmount();

					Double amount = 0.0;

					amount = disbursmentAmount / referralBonusByBorrower;

					borrowerRefereeInfo.setStatus(LenderReferenceDetails.Status.Disbursed);

					LenderReferralBonusUpdated referralBonusDetails = new LenderReferralBonusUpdated();

					OxyBorrowersDealsInformation dealInfo = oxyBorrowersDealsInformationRepo
							.findByDealName(user.getLoanOwner());
					if (dealInfo != null) {
						referralBonusDetails.setDealId(dealInfo.getId());
					}

					referralBonusDetails.setReferrerUserId(borrowerRefereeInfo.getReferrerId());
					referralBonusDetails.setAmount(amount);
					referralBonusDetails.setRefereeUserId(borrowerRefereeId);
					referralBonusDetails.setParticipatedOn(new Date());
					referralBonusDetails.setParticipatedAmount(disbursmentAmount);

					lenderReferralBonusUpdatedRepo.save(referralBonusDetails);

					Double sumOfAmountUnpaid = lenderReferenceDetailsRepo
							.findingAmountForUnpaid(borrowerRefereeInfo.getRefereeId());
					if (sumOfAmountUnpaid != null) {
						borrowerRefereeInfo.setAmount(sumOfAmountUnpaid);
						lenderReferenceDetailsRepo.save(borrowerRefereeInfo);
					} else {
						borrowerRefereeInfo.setAmount(0.0);
						lenderReferenceDetailsRepo.save(borrowerRefereeInfo);
					}
				}
			}
			logger.info("Reference Amount Ends................................");

		}

		UserResponse userResponse = new UserResponse();
		userResponse.setId(userRequest.getId());
		userResponse.setAdminComments(userRequest.getAdminComments());
		return userResponse;

	}

	private void generateBorrowerBankIciciExcel(LoanRequest loanRequest, String disbursedDateString) {

		if (loanRequest != null) {
			LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();

			if (loanOfferdAmount != null) {
				if (loanRequest.getDisbursmentAmount().equals(loanOfferdAmount.getLoanOfferedAmount())) {
					BorrowerPaymentsDto dto = new BorrowerPaymentsDto();
					User user = userRepo.findById(loanRequest.getUserId()).get();
					PersonalDetails personalDetails = user.getPersonalDetails();
					BankDetails bankDetails = user.getBankDetails();

					dto.setAccountNumber(bankDetails.getAccountNumber());
					dto.setAccountName(bankDetails.getUserName());
					dto.setIfscCode(bankDetails.getIfscCode());
					dto.setAmount(loanRequest.getDisbursmentAmount());
					dto.setBorrowerId("BR" + user.getId());
					dto.setRemarks("FROM OXYLOANS");

					excelServiceRepo.writeBorrowerPaymentsExcel(dto, disbursedDateString);

				}
			}

		}

	}

	@Override
	public LenderFavouriteResponseDto selectFavouriteBorrower(LenderFavouriteRequestDto lenderFavouriteRequestDto) {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public UserResponse BorrowerFee(int userId, UserRequest userRequest) {

		User user = userRepo.findById(userRequest.getId()).get();
		UserResponse response = null;
		if (user != null) {

			if (user.getBorrowerFee() != null) {
				throw new OperationNotAllowedException("borrower Fee Paid", ErrorCodes.BORROWER_FEE_PAID);
			}
			Double borrowerRequestTotal = oxyLoanRequestRepo.sumByRequestApprovedAmount(userRequest.getId(),
					Status.APPROVED.toString());
			LoanRequest loanRequest = oxyLoanRequestRepo.findByUserIdAndParentRequestIdIsNull(userRequest.getId());
			Double borrowerOfferedTotal = oxyLoanRequestRepo.sumByOfferedApprovedAmount(loanRequest.getId(),
					Status.APPROVED.toString());
			double convertedborrowerRequestTotal = (borrowerRequestTotal == null) ? 0.00 : borrowerRequestTotal;
			double convertedborrowerOfferedTotal = (borrowerOfferedTotal == null) ? 0.00 : borrowerOfferedTotal;
			double totalAmount = convertedborrowerRequestTotal + convertedborrowerOfferedTotal;
			double rateOfInterestForFee = (totalAmount * userRequest.getRoi()) / 100;
			double gst = (rateOfInterestForFee * 18) / 100;
			double borrowerFee = rateOfInterestForFee + gst;
			user.setBorrowerFee(borrowerFee);
			userRepo.save(user);
			response = new UserResponse();
			response.setLoanAmountApprovedByAdmin(totalAmount);
			response.setAmountForGivenInterest(rateOfInterestForFee);
			response.setGst(gst);
			response.setBorrowerFee(borrowerFee);

			return response;

		}
		return null;

	}

	@Override
	@Transactional
	public UserResponse calculateprofileRisk(int userID, UserRequest userRequest) {
		int currentUser = authorizationService.getCurrentUser();
		User user1 = userRepo.findById(currentUser).get();
		if (user1.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user1.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);

		}
		User user = userRepo.findById(userRequest.getId()).get();
		if (user != null) {
			ProfessionalDetails professionalDetails = user.getProfessionalDetails();
			if (professionalDetails != null && user.getExperianSummary() != null) {
				if (professionalDetails.getEmployment() == Employment.SALARIED
						|| professionalDetails.getEmployment() == Employment.SELFEMPLOYED) {
					UserProfileRisk userProfileRisk = user.getUserProfileRisk();
					if (userProfileRisk == null) {
						userProfileRisk = new UserProfileRisk();
						userProfileRisk.setUserId(user.getId());
						if (userRequest.getSalaryOrIncomePoints() >= 0 && userRequest.getSalaryOrIncomePoints() <= 25) {
							userProfileRisk.setSalaryOrIncome(userRequest.getSalaryOrIncomePoints());
						} else {
							throw new OperationNotAllowedException(
									"You can not enter SalaryOrIncomePoints lessthan 0 and greterthen 25"
											+ userRequest.getSalaryOrIncomePoints(),
									ErrorCodes.SCORE_ERROR);
						}
						if (userRequest.getCompanyOrOrganizationPoints() >= 0
								&& userRequest.getCompanyOrOrganizationPoints() <= 25) {

							userProfileRisk.setCompanyOrOrganization(userRequest.getCompanyOrOrganizationPoints());
						}

						else {
							throw new OperationNotAllowedException(
									"You can not enter CompanyOrOrganizationPoints lessthan 0 and greterthen 25"
											+ userRequest.getCompanyOrOrganizationPoints(),
									ErrorCodes.SCORE_ERROR);
						}
						if (userRequest.getExperianceOrExistenceOfOrganizationPoints() >= 0
								&& userRequest.getExperianceOrExistenceOfOrganizationPoints() <= 25) {
							userProfileRisk.setExperianceOrExistenceOfOrganization(
									userRequest.getExperianceOrExistenceOfOrganizationPoints());
						} else {
							throw new OperationNotAllowedException(
									"You can not enter ExperianceOrExistenceOfOrganizationPoints lessthan 0 and greterthen 25"
											+ userRequest.getExperianceOrExistenceOfOrganizationPoints(),
									ErrorCodes.SCORE_ERROR);
						}
						if (userRequest.getCibilScorePoints() >= 0 && userRequest.getCibilScorePoints() <= 25) {
							userProfileRisk.setCibilScore(userRequest.getCibilScorePoints());
						}

						else {
							throw new OperationNotAllowedException(
									"You can not enter CibilScorePoints lessthan 0 and greterthen 25"
											+ userRequest.getCibilScorePoints(),
									ErrorCodes.SCORE_ERROR);
						}
						Integer totalScore = userProfileRisk.getCibilScore()
								+ userProfileRisk.getCompanyOrOrganization()
								+ userProfileRisk.getExperianceOrExistenceOfOrganization()
								+ userProfileRisk.getSalaryOrIncome();
						userProfileRisk.setTotalScore(totalScore);
						userProfileRisk.setGrade(userRequest.getGrade());
						userProfileRisk.setUser(user);
						user.setUserProfileRisk(userProfileRisk);
						userRepo.save(user);
					} else {
						if (userRequest.getSalaryOrIncomePoints() >= 0 && userRequest.getSalaryOrIncomePoints() <= 25) {
							userProfileRisk.setSalaryOrIncome(userRequest.getSalaryOrIncomePoints());
						} else {
							throw new OperationNotAllowedException(
									"You can not enter SalaryOrIncomePoints lessthan 0 and greterthen 25"
											+ userRequest.getSalaryOrIncomePoints(),
									ErrorCodes.SCORE_ERROR);
						}
						if (userRequest.getCompanyOrOrganizationPoints() >= 0
								&& userRequest.getCompanyOrOrganizationPoints() <= 25) {

							userProfileRisk.setCompanyOrOrganization(userRequest.getCompanyOrOrganizationPoints());
						} else {
							throw new OperationNotAllowedException(
									"You can not enter CompanyOrOrganizationPoints lessthan 0 and greterthen 25"
											+ userRequest.getCompanyOrOrganizationPoints(),
									ErrorCodes.SCORE_ERROR);
						}
						if (userRequest.getExperianceOrExistenceOfOrganizationPoints() >= 0
								&& userRequest.getExperianceOrExistenceOfOrganizationPoints() <= 25) {

							userProfileRisk.setExperianceOrExistenceOfOrganization(
									userRequest.getExperianceOrExistenceOfOrganizationPoints());
						} else {
							throw new OperationNotAllowedException(
									"You can not enter ExperianceOrExistenceOfOrganizationPoints lessthan 0 and greterthen 25"
											+ userRequest.getExperianceOrExistenceOfOrganizationPoints(),
									ErrorCodes.SCORE_ERROR);
						}
						if (userRequest.getCibilScorePoints() >= 0 && userRequest.getCibilScorePoints() <= 25) {
							userProfileRisk.setCibilScore(userRequest.getCibilScorePoints());
						}

						else {
							throw new OperationNotAllowedException(
									"You can not enter CibilScorePoints lessthan 0 and greterthen 25"
											+ userRequest.getCibilScorePoints(),
									ErrorCodes.SCORE_ERROR);
						}
						Integer totalScore = userProfileRisk.getCibilScore()
								+ userProfileRisk.getCompanyOrOrganization()
								+ userProfileRisk.getExperianceOrExistenceOfOrganization()
								+ userProfileRisk.getSalaryOrIncome();
						userProfileRisk.setTotalScore(totalScore);
						userProfileRisk.setGrade(userRequest.getGrade());
						userProfileRisk.setUser(user);
						user.setUserProfileRisk(userProfileRisk);
						userRepo.save(user);
					}
				}
			} else {
				throw new OperationNotAllowedException(
						"User ProfessionalDetails Not Found Or Experian Report Not Generated",
						ErrorCodes.PROFESSIONSALDETAILS_NOT_FOUND);

			}
		} else {
			throw new OperationNotAllowedException("User not Found", ErrorCodes.ENITITY_NOT_FOUND);
		}
		UserResponse userResponse = new UserResponse();
		userResponse.setId(user.getId());
		return userResponse;
	}

	@Override
	public UserResponse getExperianAndProfile(int userId) {
		int currentUser = authorizationService.getCurrentUser();
		User user1 = userRepo.findById(currentUser).get();
		if (user1.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user1.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);

		}
		UserResponse userResponse = null;
		User user = userRepo.findById(userId).get();
		if (user != null) {
			if (user.getProfessionalDetails() != null && user.getExperianSummary() != null
					&& user.getFinancialDetails() != null) {
				if (user.getProfessionalDetails().getEmployment() == Employment.SALARIED) {
					userResponse = new UserResponse();
					userResponse.setId(user.getId());
					userResponse.setCompanyName(user.getProfessionalDetails().getCompanyName());
					userResponse.setExperinaScore(user.getExperianSummary().getScore());
					userResponse.setSalary(user.getFinancialDetails().getNetMonthlyIncome());
					userResponse.setExperinace(user.getProfessionalDetails().getWorkExperience());
					userResponse.setEmployementType(user.getProfessionalDetails().getEmployment().toString());
					RiskProfileDto riskProfileDto = getReskProfileCalculation(user.getId());
					userResponse.setRiskProfileDto(riskProfileDto);
					return userResponse;
				} else {
					userResponse = new UserResponse();
					userResponse.setId(user.getId());
					userResponse.setOrganization(user.getProfessionalDetails().getCompanyName());
					userResponse.setExperinaScore(user.getExperianSummary().getScore());
					userResponse.setNetIncome(user.getFinancialDetails().getNetMonthlyIncome());
					userResponse.setOrganizationFound(user.getProfessionalDetails().getWorkExperience());
					userResponse.setEmployementType(user.getProfessionalDetails().getEmployment().toString());
					RiskProfileDto riskProfileDto = getReskProfileCalculation(user.getId());
					userResponse.setRiskProfileDto(riskProfileDto);
					return userResponse;
				}
			} else {
				throw new OperationNotAllowedException(
						"Profiledetails Not Found Or Experian score Not Taken Or Montly Income",
						ErrorCodes.PROFESSIONSALDETAILS_NOT_FOUND);
			}
		} else {
			throw new OperationNotAllowedException("User Not Found", ErrorCodes.USER_NOT_FOUND);
		}
	}

	@Override
	public LoanResponseDto sendOffer(int userId, LoanRequestDto loanRequestDto) throws MessagingException {
		/*
		 * int currentUser = authorizationService.getCurrentUser(); User user1 =
		 * userRepo.findById(currentUser).get();
		 * 
		 * if (user1.getPrimaryType() != PrimaryType.ADMIN) { throw new
		 * OperationNotAllowedException("As a " + user1.getPrimaryType() +
		 * " you are not authorized", ErrorCodes.PERMISSION_DENIED);
		 * 
		 * }
		 */
		User user = userRepo.findById(userId).get();
		LoanResponseDto loanResponseDto = null;
		if (user != null) {
			LoanRequest loanRequest = loanRequestRepo.findByUserIdAndLoanRequestId(userId,
					loanRequestDto.getLoanRequestedId());
			if (loanRequest != null) {
				LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();
				if (loanOfferdAmount != null) {
					if (loanRequest.getLoanRequestId().equalsIgnoreCase(loanOfferdAmount.getLoanofferedId())) {
						throw new OperationNotAllowedException(
								"Offer Already Sent To This Application   " + loanOfferdAmount.getLoanofferedId(),
								ErrorCodes.ACTION_ALREDY_DONE);
					}

					if (loanRequest.getLoanOfferedAmount()
							.getLoanOfferdStatus() == LoanOfferdStatus.LOANOFFERACCEPTED) {
						throw new OperationNotAllowedException(
								"Offer Already Sent To This Application   " + loanOfferdAmount.getLoanofferedId(),
								ErrorCodes.ACTION_ALREDY_DONE);
					}
				}
				// String uuid = UUID.randomUUID().toString().replace("-", "");
				if (loanOfferdAmount == null) {
					if (loanRequest.getRateOfInterest() == 0.0) {
						throw new OperationNotAllowedException("User not submitted his loan request",
								ErrorCodes.LOAN_REQUEST_NULL);
					}
					if (loanRequest.getRateOfInterestToBorrower() == 0.0
							&& loanRequest.getRateOfInterestToLender() == 0.0) {
						throw new OperationNotAllowedException("Radha sir reviewing the application",
								ErrorCodes.LOAN_REQUEST_NULL);
					}
					if (loanRequest.getRateOfInterestToBorrower() != loanRequestDto.getRateOfInterest()) {
						throw new OperationNotAllowedException(
								"Rate of interest is not matching with radha sir rate of interest",
								ErrorCodes.LOAN_REQUEST_NULL);
					}
					if (loanRequest.getDurationBySir() != loanRequestDto.getDuration()) {
						throw new OperationNotAllowedException("Duration is not matching with radha sir duration",
								ErrorCodes.LOAN_REQUEST_NULL);
					}

					if (!loanRequest.getDurationType().toString().equalsIgnoreCase(loanRequestDto.getDurationType())) {
						throw new OperationNotAllowedException(
								"Duration Type is not matching with radha sir duration type",
								ErrorCodes.LOAN_REQUEST_NULL);
					}

					Double emiAmount = 0d;
					if (loanRequestDto.getDurationType().equalsIgnoreCase("months")) {

						Double emiPrincialAmount = (loanRequestDto.getLoanOfferedAmount()
								* (loanRequestDto.getRateOfInterest() / 12)) / 100d;
						Double interestAmount = loanRequestDto.getLoanOfferedAmount()
								/ loanRequestDto.getDuration().doubleValue();

						emiAmount = emiPrincialAmount + interestAmount;

					} else if (loanRequestDto.getDurationType().equalsIgnoreCase("days")) {
						emiAmount = loanRequest.getRateOfInterestToBorrower() * loanRequestDto.getDuration();
					}

					loanOfferdAmount = new LoanOfferdAmount();
					loanOfferdAmount.setLoanofferedId(loanRequestDto.getLoanRequestedId());
					loanOfferdAmount.setUserId(user.getId());
					loanOfferdAmount.setComments(loanRequestDto.getComments());
					loanOfferdAmount.setLoanOfferedAmount(loanRequestDto.getLoanOfferedAmount());
					loanOfferdAmount.setEmailSent(true);
					loanOfferdAmount.setLoanRequest(loanRequest);
					loanOfferdAmount.setRateOfInterest(loanRequestDto.getRateOfInterest());
					loanOfferdAmount.setId(loanRequest.getId());
					loanOfferdAmount.setDuration(loanRequestDto.getDuration());
					loanOfferdAmount.setDurationType(
							loanRequestDto.getDurationType().equalsIgnoreCase("months") ? DurationType.Months
									: DurationType.Days);
					// loanOfferdAmount.setEmailToken(uuid);
					loanOfferdAmount.setBorrowerFee(loanRequestDto.getFee());
					loanOfferdAmount.setEmiAmount((double) Math.round(emiAmount));
					double netDisbursementAmount = loanRequestDto.getLoanOfferedAmount() - loanRequestDto.getFee();
					loanOfferdAmount.setNetDisbursementAmount((double) Math.round(netDisbursementAmount));
					loanRequest.setLoanOfferedAmount(loanOfferdAmount);
					loanRequestRepo.save(loanRequest);

					String offeredDate = expectedDateFormat.format(loanOfferdAmount.getOfferSentOn());
					if (!utm.equals(user.getUrchinTrackingModule())) {
						TemplateContext templateContext = new TemplateContext();
						templateContext.put("id", "BR" + user.getId());
						if (user.getPersonalDetails() != null) {
							templateContext.put("name",
									user.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase()
											+ user.getPersonalDetails().getFirstName().substring(1) + " "
											+ user.getPersonalDetails().getLastName());
							templateContext.put("address", user.getPersonalDetails().getAddress());

						}
						templateContext.put("State", user.getState());
						templateContext.put("pin", user.getPinCode());
						templateContext.put("city", user.getCity());
						templateContext.put("loanamount",
								BigDecimal.valueOf(loanRequestDto.getLoanOfferedAmount()).toBigInteger());
						templateContext.put("fee", BigDecimal.valueOf(loanRequestDto.getFee()).toBigInteger());
						templateContext.put("tenure", loanRequestDto.getDuration());
						templateContext.put("durationType", loanRequestDto.getDurationType());
						templateContext.put("date", offeredDate);
						templateContext.put("verifyLink", loanEmailVerifyPath);
						EmailRequest emailRequest = new EmailRequest(
								new String[] { user.getEmail(), "narendra@oxyloans.com", "ramadevi@oxyloans.com",
										"admin@oxyloans.com" },
								loanMailsubject, "loan-offer.template", templateContext);
						EmailResponse emailResponse = emailService.sendEmail(emailRequest);
						if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
							throw new MessagingException(emailResponse.getErrorMessage()); // TODO need to fill this
																							// property
						}
						OxyUserType oxyUserType = oxyUserTypeRepo
								.getDetailsByUtmNameAndType(user.getUrchinTrackingModule());
						if (oxyUserType != null) {
							String lineSeparator = System.lineSeparator();
							String firstMessage = "Dear " + user.getPersonalDetails().getFirstName() + ","
									+ lineSeparator
									+ "You have received a loan offer to your registered email from oxyloans .Please review."
									+ lineSeparator + "Thanks," + lineSeparator + "Oxyloans Team";
							String secondMessage = "Please note that  Partner " + oxyUserType.getUtmName()
									+ "has sent a loan offer to " + user.getPersonalDetails().getFirstName() + " "
									+ "( BR" + user.getId() + " ) on " + offeredDate + ".The application id is "
									+ loanRequest.getLoanRequestId() + lineSeparator + "Thanks," + lineSeparator
									+ "Oxyloans Team " + lineSeparator;
							whatappService.partnerActionUsingUltra(user.getPersonalDetails().getWhatsAppNumber(),
									firstMessage, "919492902990", secondMessage);
						}
					}
					loanResponseDto = new LoanResponseDto();
					loanResponseDto.setLoanRequestedId(loanRequest.getLoanRequestId());
					return loanResponseDto;
				}

				loanResponseDto = new LoanResponseDto();
				loanResponseDto.setLoanRequestedId(loanRequest.getLoanRequestId());
				return loanResponseDto;

			} else {
				throw new OperationNotAllowedException("Loan Request Not Found", ErrorCodes.LOAN_REQUEST_NULL);

			}
		} else {
			throw new OperationNotAllowedException("User Not Found", ErrorCodes.USER_NOT_FOUND);
		}

	}

	private RiskProfileDto getReskProfileCalculation(Integer userId) {

		RiskProfileDto riskProfileDto = new RiskProfileDto();
		User user = userRepo.findById(userId).get();
		UserProfileRisk userProfileRisk = user.getUserProfileRisk();
		if (ObjectUtils.allNotNull(user, userProfileRisk)) {
			riskProfileDto.setUserId(user.getId());
			riskProfileDto.setCibilScore(userProfileRisk.getCibilScore());
			riskProfileDto.setCompanyOrOrganization(userProfileRisk.getCompanyOrOrganization());
			riskProfileDto
					.setExperianceOrExistenceOfOrganization(userProfileRisk.getExperianceOrExistenceOfOrganization());
			riskProfileDto.setGrade(userProfileRisk.getGrade());
			riskProfileDto.setSalaryOrIncome(userProfileRisk.getSalaryOrIncome());
			return riskProfileDto;
		}
		return null;

	}

	@Override
	public UserResponse addingCreditScoreByPaisaBazaar(int userId, ExperianRequestDto experianRequestDto) {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	@Transactional
	public UserResponse getAllUtmFieldValues() {

		int currentUser = authorizationService.getCurrentUser();
		User user1 = userRepo.findById(currentUser).get();
		if (user1.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user1.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);

		}
		List<String> listOfUsers = userRepo.getUtmFields();

		UserResponse userResponse = new UserResponse();
		userResponse.setUtm(listOfUsers);

		return userResponse;

	}

	@Override
	@Transactional
	public LoanEmiCardPaymentDetailsResponseDto updatePaymentDetails(int emiId,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto) {

		LoanEmiCard loanEmiCard = loanEmiCardRepo.findById(emiId).get();

//		int currentUser = authorizationService.getCurrentUser();
//		User user = userRepo.findById(currentUser).get();
//		if (user.getPrimaryType() != PrimaryType.ADMIN) {
//			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
//					ErrorCodes.PERMISSION_DENIED);
//		}

		int loanId = loanEmiCard.getLoanId();
		OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get();
		double rateOfInterest = oxyLoan.getRateOfInterest();

		LoanEmiCardPaymentDetails loanEmiCardPaymentDetails = new LoanEmiCardPaymentDetails();
		loanEmiCardPaymentDetails.setEmiId(loanEmiCardPaymentDetailsRequestDto.getEmiId());
		loanEmiCardPaymentDetails.setLoanId(loanEmiCardPaymentDetailsRequestDto.getLoanId());
		loanEmiCardPaymentDetails.setModeOfPayment(loanEmiCardPaymentDetailsRequestDto.getModeOfPayment());
		loanEmiCard.setModeOfPayment(loanEmiCardPaymentDetailsRequestDto.getModeOfPayment());
		loanEmiCardPaymentDetails
				.setTransactionRefereceNumber(loanEmiCardPaymentDetailsRequestDto.getTransactionReferenceNumber());
		List<LoanEmiCardPaymentDetails> loanEmiCardPayment = loanEmiCardPaymentDetailsRepo.findByEmiId(emiId);
		double excessEmiAmount = 0.0;
		if (loanEmiCardPaymentDetailsRequestDto.getPaymentStatus().toString()
				.equals(PaymentStatus.FULLYPAID.toString())) {
			loanEmiCardPaymentDetails.setPaymentStatus(
					PaymentStatus.valueOf(loanEmiCardPaymentDetailsRequestDto.getPaymentStatus().toUpperCase()));
			loanEmiCardPaymentDetails
					.setPartialPaymentAmount(loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount());

			if (loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount() > loanEmiCard.getEmiAmount()) {

				excessEmiAmount = loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount()
						- loanEmiCard.getEmiAmount();
				loanEmiCard.setExcessOfEmiAmount(excessEmiAmount);
				loanEmiCardRepo.save(loanEmiCard);
			}
			try {
				loanEmiCardPaymentDetails.setAmountPaidDate(
						expectedDateFormat.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
			} catch (ParseException e) {
				throw new DataFormatException("Illegal Date format. It should be dd/MM/yyyy",
						ErrorCodes.INVALID_DATE_FORMAT);
			}
			loanEmiCardPaymentDetails.setPartpayDate(Calendar.getInstance().getTime());

			loanEmiCard.setStatus(loanEmiCard.getStatus().COMPLETED);
			loanEmiCard.setEmiPaidOn(Calendar.getInstance().getTime());
			loanEmiCardRepo.save(loanEmiCard);
			loanEmiCardPaymentDetailsRepo.save(loanEmiCardPaymentDetails);
		} else {

			if (loanEmiCardPaymentDetailsRequestDto.getPaymentStatus().toString()
					.equals(PaymentStatus.PARTPAID.toString())) {
				loanEmiCardPaymentDetails.setPartpayDate(Calendar.getInstance().getTime());

				loanEmiCardPaymentDetails.setPaymentStatus(
						PaymentStatus.valueOf(loanEmiCardPaymentDetailsRequestDto.getPaymentStatus().toUpperCase()));

				loanEmiCardPaymentDetails
						.setPartialPaymentAmount(loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount());

				try {
					loanEmiCardPaymentDetails.setAmountPaidDate(
							expectedDateFormat.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
				} catch (ParseException e) {
					throw new DataFormatException("Illegal Date format. It should be dd/MM/yyyy",
							ErrorCodes.INVALID_DATE_FORMAT);
				}

				double emiAmount = loanEmiCard.getEmiAmount();
				double amtexcess = 0.0;
				double remainingPenality = 0.0;
				Date emiStartDate = loanEmiCard.getEmiDueOn();

				Date emiPaidDate = loanEmiCardPaymentDetails.getAmountPaidDate();

				if (loanEmiCard.getRemainingEmiAmount() > 0
						|| loanEmiCard.getStatus().toString().equalsIgnoreCase("COMPLETED")) {
					// if partial payment more than one timw
					emiAmount = loanEmiCard.getRemainingEmiAmount();
					if (loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount() > loanEmiCard
							.getRemainingEmiAmount()) { // patial amt is greater than Remaining EMI

						amtexcess = loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount()
								- loanEmiCard.getRemainingEmiAmount();
						if (amtexcess >= 0) {
							loanEmiCard.setRemainingEmiAmount(0.0);
							loanEmiCard.setStatus(loanEmiCard.getStatus().COMPLETED);
							loanEmiCard.setEmiPaidOn(Calendar.getInstance().getTime());

							loanEmiCardRepo.save(loanEmiCard);
						}
						if (loanEmiCard.getRemainingPenaltyAndRemainingEmi() > 0.0
								&& loanEmiCard.getRemainingEmiAmount() == 0) {
							if (amtexcess >= loanEmiCard.getRemainingPenaltyAndRemainingEmi()) {

								amtexcess = amtexcess - loanEmiCard.getRemainingPenaltyAndRemainingEmi();
								loanEmiCard.setRemainingPenaltyAndRemainingEmi(0.0);
								loanEmiCard.setExcessOfEmiAmount(amtexcess - loanEmiCardPaymentDetails.getPenalty());
								loanEmiCardRepo.save(loanEmiCard);

							} else {
								loanEmiCard.setRemainingPenaltyAndRemainingEmi(
										loanEmiCard.getRemainingPenaltyAndRemainingEmi() - amtexcess);
								amtexcess = 0.0;

								loanEmiCardRepo.save(loanEmiCard);
							}
						}
						if (amtexcess > 0.0) {

							LoanEmiCardPaymentDetails loanEmiCardPaydetails = loanEmiCardPayment
									.get(loanEmiCardPayment.size() - 1);

							remainingPenality = (loanEmiCardPaydetails.getPenalty() - amtexcess);

							loanEmiCardRepo.save(loanEmiCard);

							if (remainingPenality <= 0) {

								loanEmiCardPaymentDetails.setPenalty(0.0);
								loanEmiCardPaymentDetails
										.setPaymentStatus(loanEmiCardPaydetails.getPaymentStatus().FULLYPAID);
								loanEmiCard.setEmiPaidOn(Calendar.getInstance().getTime());
								loanEmiCard.setExcessOfEmiAmount(amtexcess - loanEmiCardPaydetails.getPenalty());

								loanEmiCardRepo.save(loanEmiCard);
								loanEmiCardPaymentDetailsRepo.save(loanEmiCardPaymentDetails);

							} else {
								loanEmiCardPaymentDetails.setPenalty(remainingPenality);

								loanEmiCard.setRemainingEmiAmount(0.0);
								loanEmiCard.setStatus(loanEmiCard.getStatus().COMPLETED);
								loanEmiCard.setEmiPaidOn(Calendar.getInstance().getTime());
								// loanEmiCard.setExcessOfEmiAmount(loanEmiCardPaymentDetails.getPenalty()-amtexcess);

								loanEmiCardRepo.save(loanEmiCard);

								loanEmiCardPaymentDetailsRepo.save(loanEmiCardPaymentDetails);

							}
						}

					} else {
						// patial amt is Less than Remaining EMI
						double remainingEmiAmount = (loanEmiCard.getRemainingEmiAmount()
								- loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount());

						loanEmiCard.setRemainingEmiAmount(remainingEmiAmount);
						if (remainingEmiAmount == 0.0) {
							loanEmiCard.setStatus(loanEmiCard.getStatus().COMPLETED);
							loanEmiCard.setEmiPaidOn(Calendar.getInstance().getTime());

							loanEmiCardRepo.save(loanEmiCard);
						}
						loanEmiCardPaymentDetails.setRemainingEmiAmount(remainingEmiAmount);
						LoanEmiCardPaymentDetails loanEmiCardPaydetails = loanEmiCardPayment
								.get(loanEmiCardPayment.size() - 1);

						loanEmiCardPaymentDetails.setPenalty(loanEmiCardPaydetails.getPenalty());

						loanEmiCardPaymentDetailsRepo.save(loanEmiCardPaymentDetails);

						loanEmiCardRepo.save(loanEmiCard);

					}

				} else {
					if (loanEmiCard.getStatus().toString().equalsIgnoreCase("INITIATED")
							&& loanEmiCard.getRemainingEmiAmount() == 0.0) {

						double amountExcess = 0.0;
						loanEmiCardPaymentDetails
								.setPenalty(calculatingPenalty(emiStartDate, emiPaidDate, emiAmount, rateOfInterest));
						if (loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount() > loanEmiCard
								.getEmiAmount()) {
							amountExcess = loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount()
									- loanEmiCard.getEmiAmount();
							loanEmiCard.setRemainingEmiAmount(0.0);
							loanEmiCard.setStatus(loanEmiCard.getStatus().COMPLETED);
							loanEmiCard.setEmiPaidOn(Calendar.getInstance().getTime());
							if (amountExcess > loanEmiCard.getRemainingPenaltyAndRemainingEmi()) {
								amountExcess = amountExcess - loanEmiCard.getRemainingPenaltyAndRemainingEmi();
								loanEmiCard.setRemainingPenaltyAndRemainingEmi(0.0);

								if (amountExcess > loanEmiCardPaymentDetails.getPenalty()) {
									loanEmiCardPaymentDetails.setPenalty(0.0);
									loanEmiCard.setExcessOfEmiAmount(
											amountExcess - loanEmiCardPaymentDetails.getPenalty());
									loanEmiCardRepo.save(loanEmiCard);
									loanEmiCardPaymentDetailsRepo.save(loanEmiCardPaymentDetails);
								} else {
									loanEmiCardPaymentDetails
											.setPenalty(loanEmiCardPaymentDetails.getPenalty() - amountExcess);
									loanEmiCardPaymentDetailsRepo.save(loanEmiCardPaymentDetails);

								}

								loanEmiCardPaymentDetailsRepo.save(loanEmiCardPaymentDetails);
								loanEmiCardRepo.save(loanEmiCard);

							} else {
								loanEmiCard.setRemainingPenaltyAndRemainingEmi(
										loanEmiCard.getRemainingPenaltyAndRemainingEmi() - amountExcess);
								loanEmiCardRepo.save(loanEmiCard);
							}

							loanEmiCardRepo.save(loanEmiCard);
							loanEmiCardPaymentDetailsRepo.save(loanEmiCardPaymentDetails);

						} else {
							double remainingEmiAmount = (loanEmiCard.getEmiAmount()
									- loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount());
							loanEmiCard.setRemainingEmiAmount(Math.round(remainingEmiAmount));
							loanEmiCardPaymentDetails.setRemainingEmiAmount(Math.round(remainingEmiAmount));
							loanEmiCardPaymentDetails.setPenalty(
									calculatingPenalty(emiStartDate, emiPaidDate, emiAmount, rateOfInterest));
							if (remainingEmiAmount == 0.0) {

								loanEmiCardPaymentDetails.setRemainingEmiAmount(0.0);
								loanEmiCard.setStatus(loanEmiCard.getStatus().COMPLETED);
								loanEmiCard.setEmiPaidOn(Calendar.getInstance().getTime());
								loanEmiCardRepo.save(loanEmiCard);
							} else {
								loanEmiCard.setStatus(loanEmiCard.getStatus().INPROCESS);
								emiAmount = loanEmiCard.getEmiAmount();
								loanEmiCardRepo.save(loanEmiCard);
							}
							loanEmiCardRepo.save(loanEmiCard);
							loanEmiCardPaymentDetailsRepo.save(loanEmiCardPaymentDetails);

						}

						loanEmiCardRepo.save(loanEmiCard);
						loanEmiCardPaymentDetailsRepo.save(loanEmiCardPaymentDetails);

					}
				}

			}
		}

		LoanEmiCardPaymentDetailsResponseDto loanEmiCardPaymentDetailsResponseDto = new LoanEmiCardPaymentDetailsResponseDto();
		loanEmiCardPaymentDetailsResponseDto.setEmiId(loanEmiCardPaymentDetails.getEmiId());
		loanEmiCardPaymentDetailsResponseDto.setLoanId(loanEmiCardPaymentDetails.getLoanId());
		loanEmiCardPaymentDetailsResponseDto.setModeOfPayment(loanEmiCardPaymentDetails.getModeOfPayment());
		loanEmiCardPaymentDetailsResponseDto
				.setTransactionReferenceNumber(loanEmiCardPaymentDetails.getTransactionRefereceNumber());
		loanEmiCardPaymentDetailsResponseDto.setPaymentStatus(loanEmiCardPaymentDetails.getPaymentStatus().toString());
		if (loanEmiCardPaymentDetails.getPenalty() > 0) {
			loanEmiCardPaymentDetailsResponseDto.setPenalty(loanEmiCardPaymentDetails.getPenalty());
		}
		if (loanEmiCardPaymentDetails.getRemainingEmiAmount() > 0) {
			loanEmiCardPaymentDetailsResponseDto
					.setRemainingEmiAmount(loanEmiCardPaymentDetails.getRemainingEmiAmount());
		}
		if (loanEmiCardPaymentDetails.getPartpayDate() != null) {
			loanEmiCardPaymentDetailsResponseDto
					.setPenaltyDate(expectedDateFormat.format(loanEmiCardPaymentDetails.getPartpayDate()));
		}
		return loanEmiCardPaymentDetailsResponseDto;

	}

	public double calculatingPenalty(Date emiStartDate, Date emiPaidDate, double emiAmount, double rateOfInterest) {
		String date = expectedDateFormat.format(emiStartDate); // "05/02/2020"
		String dateParts[] = date.split("/");
		String emiDate = dateParts[0];

		String month = dateParts[1];
		int monthInIntType = Integer.parseInt(month);
		String year = dateParts[2];
		int yearInIntType = Integer.parseInt(year);

		int number_Of_DaysInMonth = 0;
		switch (monthInIntType) {
		case 1:
			monthInIntType = 1;
			number_Of_DaysInMonth = 31; // MonthOfName = "January";
			break;
		case 2:
			monthInIntType = 2;
			if ((yearInIntType % 400 == 0) || ((yearInIntType % 4 == 0) && (yearInIntType % 100 != 0))) {
				number_Of_DaysInMonth = 29; // This is for leap year
			} else { // MonthOfName = "February";
				number_Of_DaysInMonth = 28; // This for not leap year
			}
			break;
		case 3:
			monthInIntType = 3;
			number_Of_DaysInMonth = 31; // MonthOfName = "March";
			break;
		case 4:
			monthInIntType = 4;
			number_Of_DaysInMonth = 30; // MonthOfName = "April";
			break;
		case 5:
			monthInIntType = 5;
			number_Of_DaysInMonth = 31; // MonthOfName = "May";
			break;
		case 6:
			monthInIntType = 6;
			number_Of_DaysInMonth = 30; // MonthOfName = "June";
			break;
		case 7:
			monthInIntType = 7;
			number_Of_DaysInMonth = 31; // MonthOfName = "July";
			break;
		case 8:
			monthInIntType = 8;
			number_Of_DaysInMonth = 31; // MonthOfName = "August";
			break;
		case 9:
			monthInIntType = 9;
			number_Of_DaysInMonth = 30; // MonthOfName = "September";
			break;
		case 10:
			monthInIntType = 10;
			number_Of_DaysInMonth = 31; // MonthOfName = "October";
			break;
		case 11:
			monthInIntType = 11;
			number_Of_DaysInMonth = 30; // MonthOfName = "November";
			break;
		case 12:
			monthInIntType = 12;
			number_Of_DaysInMonth = 31; // MonthOfName = "December";
		}

		long differenceInTime = emiPaidDate.getTime() - emiStartDate.getTime();
		int differenceInDays = (int) (differenceInTime / (24 * 60 * 60 * 1000));
		double totalPenalty = 0.0;
		if (differenceInDays > 5) {

			double emiAmountPerDay = emiAmount / number_Of_DaysInMonth;
			double interestPerDay = (emiAmountPerDay * rateOfInterest) / 100;

			double interestPenaltyGstValue = ((interestPerDay * 18) / 100);
			totalPenalty = Math.round((differenceInDays * interestPerDay) + interestPenaltyGstValue + 1000);

		}
		if (differenceInDays <= 0) {
			totalPenalty = 0.0;
		}
		if (differenceInDays >= 1 && differenceInDays <= 5) {
			double gstValue = (500 * 18) / 100;
			totalPenalty = gstValue + 500;

		}

		return totalPenalty;
	}

	@Override
	public List<LenderEmiDetailsResponseDto> getLenderEmiDetails(int userId) {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public void disbursedLoans() {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public List<LenderHistroryResponseDto> getLenderHistory(int userId) {
		return super.getLenderHistory(userId);
	}

	@Override
	public SearchResultsDto featurePending(int year, int month, SearchRequestDto searchRequestDto) {

		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2020);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		Date monthStart = calendar.getTime();
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date monthEnd = calendar.getTime();

		/*
		 * {"leftOperand":{"fieldName":"emiPaidOn","operator":"NULL"},
		 * "logicalOperator":"AND",
		 * "rightOperand":{"leftOperand":{"fieldName":"emiDueOn","fieldValue":
		 * "2020-04-01","operator":"GTE"},"logicalOperator":"AND",
		 * "rightOperand":{"fieldName":"emiDueOn","fieldValue":"2020-04-31","operator":
		 * "LTE"}}}
		 */

		Sort sort = Sort.by(Direction.ASC, "emiDueOn");
		Pageable searchPage = PageRequest.of(searchRequestDto.getPage().getPageNo() - 1,
				searchRequestDto.getPage().getPageSize(), sort);
		Specification<LoanEmiCard> spec = null;
		try {
			spec = specificationProvider.construct(LoanEmiCard.class, searchRequestDto);
		} catch (ClassNotFoundException e) {
			logger.error(e, e);
		} catch (IllegalAccessException e) {
			logger.error(e, e);
		} catch (InvocationTargetException e) {
			logger.error(e, e);
		} catch (NoSuchMethodException e) {
			logger.error(e, e);
		} catch (InstantiationException e) {
			logger.error(e, e);
		}
		Page<LoanEmiCard> findAll = loanEmiCardRepo.findAll(spec, searchPage);
		SearchResultsDto<LoanEmiCardResponseDto> results = new SearchResultsDto<LoanEmiCardResponseDto>();
		if (findAll != null) {
			results.setTotalCount(Long.valueOf(findAll.getTotalElements()).intValue());
			results.setPageNo(searchRequestDto.getPage().getPageNo());
			results.setPageCount(findAll.getNumberOfElements());
			List<LoanEmiCardResponseDto> emiCardResponses = new ArrayList<LoanEmiCardResponseDto>();
			;
			results.setResults(emiCardResponses);
			Map<Integer, Integer> emiToLoanId = new HashMap<Integer, Integer>();
			for (LoanEmiCard card : findAll.getContent()) {
				emiToLoanId.put(card.getId(), card.getLoanId());
				LoanEmiCardResponseDto responseDto = new LoanEmiCardResponseDto();
				responseDto.setComments(card.getComments());
				responseDto.setEmiAmount(card.getEmiAmount());
				responseDto.setEmiDueOn(this.expectedDateFormat.format(card.getEmiDueOn()));
				responseDto.setEmiPaidOn(
						card.getEmiPaidOn() == null ? null : this.expectedDateFormat.format(card.getEmiPaidOn()));
				responseDto.setEmiInterstAmount(card.getEmiInterstAmount());
				responseDto.setEmiNumber(card.getEmiNumber());
				responseDto.setEmiPrincipalAmount(card.getEmiPrincipalAmount());
				responseDto.setId(card.getId());
				responseDto.setLoanId(card.getLoanId());
				responseDto.setEmiStatus(card.getEmiPaidOn() != null);
				responseDto.setPaidOnTime(card.isPaidOnTime());
				emiCardResponses.add(responseDto);
			}
			Iterable<OxyLoan> loans = oxyLoanRepo.findAllById(emiToLoanId.values());
			Map<Integer, Integer> loanToLenderMap = new HashMap<>();
			Map<Integer, Integer> loanToBorrowerMap = new HashMap<>();
			loans.forEach(loan -> {
				loanToLenderMap.put(loan.getId(), loan.getLenderUserId());
				loanToBorrowerMap.put(loan.getId(), loan.getBorrowerUserId());

			});
			Iterable<User> lenders = userRepo.findAllById(loanToLenderMap.values());
			Iterable<User> borrowers = userRepo.findAllById(loanToBorrowerMap.values());
			Map<Integer, UserResponse> userResponseMap = new HashMap<>();
			lenders.forEach(lender -> {
				UserResponse userResponse = new UserResponse();
				userDetails(lender, userResponse);
				userResponseMap.put(lender.getId(), userResponse);
			});
			borrowers.forEach(borrower -> {
				UserResponse userResponse = new UserResponse();
				userDetails(borrower, userResponse);
				userResponseMap.put(borrower.getId(), userResponse);
			});
			emiCardResponses.forEach(emiCardResponse -> {
				Integer loanId = emiToLoanId.get(emiCardResponse.getId());
				Integer lenderId = loanToLenderMap.get(loanId);
				Integer borrowerId = loanToBorrowerMap.get(loanId);
				emiCardResponse.setLenderUser(userResponseMap.get(lenderId));
				emiCardResponse.setBorrowerUser(userResponseMap.get(borrowerId));
			});

		}
		return results;
	}

	/*
	 * @Override public TotalLendersInformationResponseDto
	 * getUsersLoansInformation(String primaryType, PageDto pageDto) { throw new
	 * OperationNotAllowedException("As asn Admin you can not perform this operation"
	 * , ErrorCodes.PERMISSION_DENIED); }
	 */

	private UserResponse populateUserDetails(int id) {
		User user = userRepo.findById(id).get();
		if (user != null) {
			UserResponse userResponse = new UserResponse();
			userResponse.setId(user.getId());
			userResponse
					.setFirstName(user.getPersonalDetails() == null ? null : user.getPersonalDetails().getFirstName());
			userResponse
					.setLastName(user.getPersonalDetails() == null ? null : user.getPersonalDetails().getLastName());
			userResponse.setMobileNumber(user.getMobileNumber());
			userResponse.setEmail(user.getEmail());
			return userResponse;
		}

		return null;

	}

	@Override
	public SearchResultsDto<LoanResponseDto> bucketPendingEmis(int start, int end, String type, int pageNo,
			int pageSize) {
		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}

		if (type.equalsIgnoreCase("30days")) {
			List<LoanResponseDto> currentMoantLoanResponse = new ArrayList<LoanResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getBuckets(start, end, pageNo, pageSize);
			Integer totalCount = oxyLoanRepo.getBucketCount(start, end);
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				currentMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(currentMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount == null ? 0 : totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;
		} else if (type.equalsIgnoreCase("60days")) {

			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getBuckets(start, end, pageNo, pageSize); // Integer
			Integer totalCount = oxyLoanRepo.getBucketCount(start, end);
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount == null ? 0 : totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;

		}

		else if (type.equalsIgnoreCase("90days")) {
			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getBuckets(start, end, pageNo, pageSize);
			Integer totalCount = oxyLoanRepo.getBucketCount(start, end);
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount == null ? 0 : totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;
		} else {
			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getBucketsNpa(start, pageNo, pageSize);
			Integer totalCount = oxyLoanRepo.getBucketCountNpa(start);

			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount == null ? 0 : totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;
		}

	}

	@Override
	public NotificationCountResponseDto getAdminPendingActionsDetails() {

		int interestedPendingBorrowers = userRepo.getBorrowersInformation();
		int interestedPendeingLenders = userRepo.getLendersInformation();
		// int InterestedCount = userRepo.getInterestedCount();
		// int offerSendCount = loanRequestRepo.getNumberOffersSentCount();
		// int sendOfferpendingFromAdminSide = InterestedCount - offerSendCount;
		int sendOfferpendingFromAdminSide = 2;
		int lenderTransactionUploadedPending = lenderOxyWalletNativeRepo.getLenderTransactionPendingInfo();

		int countNumberOfLoansInAgreedState = oxyLoanRepo.getcountNumberOfLoansInAgreedState();
		int countNumberPendingDisbursmentDate = oxyLoanRepo.getcountPendingAtDisbusmentDate();
		int totalCount = (interestedPendingBorrowers + interestedPendeingLenders + lenderTransactionUploadedPending
				+ sendOfferpendingFromAdminSide + countNumberOfLoansInAgreedState + countNumberPendingDisbursmentDate);
		NotificationCountResponseDto notificationCountResponseDto = new NotificationCountResponseDto();
		notificationCountResponseDto.setTotalCount(totalCount);
		notificationCountResponseDto.setInterestedPendingBorrowers(interestedPendingBorrowers);
		notificationCountResponseDto.setInterestedPendingLenders(interestedPendeingLenders);
		notificationCountResponseDto.setLenderTransactionUploadedPending(lenderTransactionUploadedPending);
		notificationCountResponseDto.setOfferPendingFromAdminSide(sendOfferpendingFromAdminSide);
		notificationCountResponseDto.setCountNumberOfLoansInAgreedState(countNumberOfLoansInAgreedState);
		notificationCountResponseDto.setCountNumberPendingDisbursmentDate(countNumberPendingDisbursmentDate);

		return notificationCountResponseDto;

	}

	@Override
	@Transactional
	public LoanResponseDto rejectLoanOffer(int id) {

		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}

		List<String> loanIds = new ArrayList<String>();
		List<Integer> loanId = new ArrayList<Integer>();
		Integer countOfDisbursedLoans = oxyLoanRepo.getCountValueOfDisbursedLoansBasedOnId(id);
		List<OxyLoan> LoansList = oxyLoanRepo.findByBorrowerParentRequestId(id);

		if (countOfDisbursedLoans != null) {
			if (countOfDisbursedLoans > 0) {
				throw new OperationNotAllowedException(
						"You can reject this application because Number of loans Disbursed for this application "
								+ countOfDisbursedLoans,
						ErrorCodes.LIMIT_REACHED);
			}
		} else {

			if (!LoansList.isEmpty()) {
				for (OxyLoan oxyLoan : LoansList) {

					loanIds.add(oxyLoan.getLoanId());
					loanId.add(oxyLoan.getId());
					oxyLoan.setLoanStatus(LoanStatus.ADMINREJECTED);
					oxyLoan.setAdminComments(Status.ADMINREJECTED.toString());
					int count = lenderBorrowerConversationRepo.countByLenderUserIdAndBorrowerUserId(
							oxyLoan.getLenderUserId(), oxyLoan.getBorrowerUserId());
					if (count > 0) {
						lenderBorrowerConversationRepo.deleteByLenderUserIdAndBorrowerUserId(oxyLoan.getLenderUserId(),
								oxyLoan.getBorrowerUserId());
					}

				}
				for (int i = 0; i < loanIds.size(); i++) {
					String loanIdValue = loanIds.get(i);

					LoanRequest loanRequest = loanRequestRepo.findByLoanId(loanIdValue);

					loanRequest.setLoanStatus(LoanStatus.ADMINREJECTED);
					loanRequest.setAdminComments(Status.ADMINREJECTED.toString());
					loanRequestRepo.save(loanRequest);
				}

				// List<LoanEmiCard> emiCards = loanEmiCardRepo.findByLoanId(loanId);
				for (int i = 0; i < loanId.size(); i++) {
					int loanId1 = loanId.get(i);
					List<LoanEmiCard> emiCards = loanEmiCardRepo.findByLoanId(loanId1);
					for (LoanEmiCard loanEmiCard : emiCards) {
						loanEmiCard.setStatus(com.oxyloans.entity.loan.LoanEmiCard.Status.ADMINREJECTED);
						loanEmiCardRepo.save(loanEmiCard);
					}

				}

				LoanRequest loanRequest = loanRequestRepo.findById(id).get();
				if (loanRequest != null) {
					loanRequest.setParentRequestId(0);
					LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();
					if (loanOfferdAmount != null) {
						loanOfferdAmount.setLoanOfferdStatus(LoanOfferdStatus.ADMINREJECTED);
					}
					User borrowerUser = userRepo.findById(loanRequest.getUserId()).get();
					if (borrowerUser != null) {
						borrowerUser.setAdminComments(Status.ADMINREJECTED.toString());
					}
					loanOfferdAmount.setLoanRequest(loanRequest);
					userRepo.save(user);
					loanRequestRepo.save(loanRequest);
				}

				// loanEmiCardRepo.saveAll(emiCards);
				// loanRequestRepo.saveAll(loanRequest);
				oxyLoanRepo.saveAll(LoansList);

			}

			else {
				LoanRequest loanRequest = loanRequestRepo.findById(id).get();
				if (loanRequest != null) {
					loanRequest.setParentRequestId(0);
					LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();
					if (loanOfferdAmount != null) {
						loanOfferdAmount.setLoanOfferdStatus(LoanOfferdStatus.ADMINREJECTED);
					}
					User borrowerUser = userRepo.findById(loanRequest.getUserId()).get();
					if (borrowerUser != null) {
						borrowerUser.setAdminComments(Status.ADMINREJECTED.toString());
					}
					loanOfferdAmount.setLoanRequest(loanRequest);

					userRepo.save(user);
					loanRequestRepo.save(loanRequest);

				}

			}
		}
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setId(id);
		return loanResponseDto;

	}

	@Override
	public void calculatingPenalityUsingSchedular() {
		List<Object[]> loanEmiCard = loanEmiCardRepo.getPenalityCalculation();
		Iterator it = loanEmiCard.iterator();
		while (it.hasNext()) {
			Object e[] = (Object[]) it.next();
			int loanEmiCardId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
			int loanId = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
			double emiAmount = Double.parseDouble(e[2] == null ? "0" : e[2].toString());
			String emiDueOnDate = (e[3] == null ? " " : e[3].toString());
			double remainingEmiAmount = Double.parseDouble(e[4] == null ? "0" : e[4].toString());
			double remainingPenalityAndRemainingEmi = Double.parseDouble(e[5] == null ? "0" : e[5].toString());
			String loanStatus = (e[6] == null ? "0" : e[6].toString());
			double rateOfInterest = Double.parseDouble((e[7] == null ? "0" : e[7].toString()));
			double penality = calculatingPenality(loanEmiCardId, loanId, emiAmount, emiDueOnDate, remainingEmiAmount,
					remainingPenalityAndRemainingEmi, loanStatus, rateOfInterest);
			LoanEmiCard loanEmiCard1 = loanEmiCardRepo.findById(loanEmiCardId).get();
			loanEmiCard1.setPenality(Math.round(penality));
			loanEmiCardRepo.save(loanEmiCard1);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {

				e1.printStackTrace();
			}

		}

	}

	public double calculatingPenality(int loanEmiCardId, int loanId, double emiAmount, String emiDueOnDate,
			double remainingEmiAmount, double remainingPenalityAndRemainingEmi, String loanStatus,
			double rateOfInterest) {

		String dueOnDate = emiDueOnDate;

		LocalDate from = LocalDate.now();
		LocalDate to = from.parse(dueOnDate);
		long differenceInDays = ChronoUnit.DAYS.between(to, from);

		double totalPenalty = 0.0;
		if (differenceInDays > 5) {

			double emiAmountPerDay = (emiAmount + remainingPenalityAndRemainingEmi) / 30;
			rateOfInterest = rateOfInterest % 12;
			double interestPerDay = (emiAmountPerDay * rateOfInterest) / 100;

			double interestPenaltyGstValue = ((interestPerDay * 18) / 100);
			totalPenalty = Math.round((differenceInDays * interestPerDay) + interestPenaltyGstValue + 1000);

		}

		if (differenceInDays >= 1 && differenceInDays <= 5) {
			double gstValue = (500 * 18) / 100;
			totalPenalty = gstValue + 500;

		}
		return totalPenalty;

	}

	@Override
	public int getUnpaidLoanEmiDetails(int loanId,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto) {
		logger.info("UpdEmisByApplication method start !!!!!!");
		List<Object[]> objects = loanEmiCardRepo.getUnpaidEmiDetails(loanId);
		if (objects != null && !objects.isEmpty()) {
			Iterator it = objects.iterator();
			int count = 0;
			while (it.hasNext()) {
				Object e[] = (Object[]) it.next();
				int emiNumber = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
				if (count == 0) {
					count = count + 1;
					LoanEmiCard emiDetails = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId, emiNumber);
					double excessAmount = updateEmiPayment(emiDetails, loanEmiCardPaymentDetailsRequestDto);
				}
			}
		}
		return loanId;

	}

	public double updateEmiPayment(LoanEmiCard emiDetails,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto) {

		double paritialPaymentAmount = loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount();

		double excessAmount = 0.0;

		int loanEmiCardId = emiDetails.getId();
		int loanId = emiDetails.getLoanId();
		Integer userId = oxyLoanRepo.getLenderEmail(loanId);
		User user = userRepo.findById(userId).get();
		String email = user.getEmail();
		LoanEmiCard loanEmiCard = loanEmiCardRepo.findById(loanEmiCardId).get();

		loanEmiCard.setModeOfPayment(loanEmiCardPaymentDetailsRequestDto.getModeOfPayment());
		loanEmiCard.setTransactionNumber(loanEmiCardPaymentDetailsRequestDto.getTransactionReferenceNumber());
		loanEmiCard.setUpdatedUserName(loanEmiCardPaymentDetailsRequestDto.getUpdatedUserName());
		if (loanEmiCardPaymentDetailsRequestDto.getModeOfPayment().equals("ENACH")) {
			try {
				loanEmiCard.setEmiPaidOn(
						expectedDateFormat.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));

			} catch (ParseException e1) {

				e1.printStackTrace();
			}
		} else {
			LoanEmiCardPaymentDetails loanEmiCardPaymentDetails = new LoanEmiCardPaymentDetails();
			loanEmiCardPaymentDetailsRequestDto.setEmiId(loanEmiCardId);
			loanEmiCardPaymentDetails.setEmiId(loanEmiCardPaymentDetailsRequestDto.getEmiId());
			loanEmiCardPaymentDetails.setLoanId(emiDetails.getLoanId() + "");
			loanEmiCardPaymentDetails.setModeOfPayment(loanEmiCardPaymentDetailsRequestDto.getModeOfPayment());
			loanEmiCardPaymentDetails
					.setTransactionRefereceNumber(loanEmiCardPaymentDetailsRequestDto.getTransactionReferenceNumber());
			loanEmiCardPaymentDetails.setPaymentStatus(
					PaymentStatus.valueOf(loanEmiCardPaymentDetailsRequestDto.getPaymentStatus().toUpperCase()));
			loanEmiCardPaymentDetails
					.setPartialPaymentAmount(loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount());
			loanEmiCardPaymentDetails.setPartpayDate(new Date());

			if (loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate() != null) {
				try {
					loanEmiCardPaymentDetails.setAmountPaidDate(
							expectedDateFormat.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
				} catch (ParseException e) {
					throw new DataFormatException("Illegal Date format. It should be dd/MM/yyyy",
							ErrorCodes.INVALID_DATE_FORMAT);
				}
			}
			loanEmiCardPaymentDetailsRepo.save(loanEmiCardPaymentDetails);
		}
		if (loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount() >= loanEmiCard.getEmiAmount()) {
			/*
			 * TemplateContext context = new TemplateContext(); String firstName =
			 * user.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase() +
			 * user.getPersonalDetails().getFirstName().substring(1); String lastName =
			 * user.getPersonalDetails().getLastName().substring(0, 1).toUpperCase() +
			 * user.getPersonalDetails().getLastName().substring(1);
			 * context.put("lenderName", firstName + " " + lastName);
			 * context.put("emiAmount", Math.round((loanEmiCard.getEmiAmount() * 100.0) /
			 * 100.0)); // context.put("date", dateFormate.format(new Date()));
			 * context.put("date", loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate());
			 * String mailsubject = "Full payment"; String emailTemplateName =
			 * "full-payment.template";
			 * 
			 * EmailRequest emailRequest = new EmailRequest(new String[] { email },
			 * mailsubject, emailTemplateName, context); EmailResponse emailResponse =
			 * emailService.sendEmail(emailRequest); if (emailResponse.getStatus() ==
			 * EmailResponse.Status.FAILED) { throw new
			 * RuntimeException(emailResponse.getErrorMessage()); }
			 */
			try {
				loanEmiCard.setEmiPaidOn(
						expectedDateFormat.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			List<InterestDetails> interestDetailsValues = interestDetailsRepo.findByloanId(loanId);
			if (interestDetailsValues != null && !interestDetailsValues.isEmpty()) {
				InterestDetails interestDetails = interestDetailsRepo.findByloanIdAndEmiNumber(loanId,
						loanEmiCard.getEmiNumber());
				if (interestDetails != null) {
					try {
						interestDetails.setInterestPaid(
								expectedDateFormat.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
					} catch (ParseException e) {

						e.printStackTrace();
					}
				}
				interestDetailsRepo.save(interestDetails);
			}

		} else {
			/*
			 * TemplateContext context = new TemplateContext(); String firstName =
			 * user.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase() +
			 * user.getPersonalDetails().getFirstName().substring(1); String lastName =
			 * user.getPersonalDetails().getLastName().substring(0, 1).toUpperCase() +
			 * user.getPersonalDetails().getLastName().substring(1);
			 * context.put("lenderName", firstName + " " + lastName);
			 * context.put("emiAmount",
			 * Math.round((loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount() *
			 * 100.0) / 100.0)); // context.put("date", dateFormate.format(new Date()));
			 * context.put("date", loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate());
			 * String mailsubject = "Part payment"; String emailTemplateName =
			 * "part-payment.template";
			 * 
			 * EmailRequest emailRequest = new EmailRequest(new String[] { email },
			 * mailsubject, emailTemplateName, context); EmailResponse emailResponse =
			 * emailService.sendEmail(emailRequest); if (emailResponse.getStatus() ==
			 * EmailResponse.Status.FAILED) { throw new
			 * RuntimeException(emailResponse.getErrorMessage()); }
			 */

		}

		if (loanEmiCard.getStatus() == com.oxyloans.entity.loan.LoanEmiCard.Status.COMPLETED) {
			if (loanEmiCard.getEmiLateFeeCharges() > 0) {
				if (paritialPaymentAmount >= loanEmiCard.getEmiLateFeeCharges()) {

					excessAmount = paritialPaymentAmount - loanEmiCard.getEmiLateFeeCharges();
					loanEmiCard.setEmiLateFeeCharges(0);
					loanEmiCard.setExcessOfEmiAmount(loanEmiCard.getExcessOfEmiAmount() + excessAmount);
					InterestDetails interestDetailsInfo = interestDetailsRepo
							.findByloanIdAndEmiNumber(loanEmiCard.getLoanId(), loanEmiCard.getEmiNumber());
					if (interestDetailsInfo != null) {
						// interestDetailsInfo.setInterest(Math.round(loanEmiCard.getEmiLateFeeCharges()));
						if (loanEmiCard.getEmiLateFeeCharges() == 0) {
							interestDetailsInfo.setInterestPaid(new Date());
						}
						interestDetailsRepo.save(interestDetailsInfo);
					}
				} else {
					loanEmiCard.setEmiLateFeeCharges(loanEmiCard.getEmiLateFeeCharges() - paritialPaymentAmount);
					InterestDetails interestDetailsInfo = interestDetailsRepo
							.findByloanIdAndEmiNumber(loanEmiCard.getLoanId(), loanEmiCard.getEmiNumber());
					if (interestDetailsInfo != null) {
						// interestDetailsInfo.setInterest(Math.round(paritialPaymentAmount));
						if (loanEmiCard.getEmiLateFeeCharges() == 0) {
							interestDetailsInfo.setInterestPaid(new Date());
						}
						interestDetailsRepo.save(interestDetailsInfo);
					}

				}
			} else {
				loanEmiCard.setExcessOfEmiAmount(loanEmiCard.getExcessOfEmiAmount() + paritialPaymentAmount);
			}
			if (loanEmiCardPaymentDetailsRequestDto.getPaymentStatus() == "FULLYPAID") {
				try {
					loanEmiCard.setEmiPaidOn(
							expectedDateFormat.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
				} catch (ParseException e) {

					e.printStackTrace();
				}
				loanEmiCardRepo.save(loanEmiCard);
			}
		} else {
			if (loanEmiCard.getStatus() == com.oxyloans.entity.loan.LoanEmiCard.Status.INPROCESS) {
				if (paritialPaymentAmount >= loanEmiCard.getRemainingEmiAmount()) {
					excessAmount = paritialPaymentAmount - loanEmiCard.getRemainingEmiAmount();
					loanEmiCard.setRemainingEmiAmount(loanEmiCard.getEmiAmount());
					loanEmiCard.setStatus(loanEmiCard.getStatus().COMPLETED);
					if (loanEmiCard.getEmiLateFeeCharges() > 0) {
						if (excessAmount >= loanEmiCard.getEmiLateFeeCharges()) {
							excessAmount = excessAmount - loanEmiCard.getEmiLateFeeCharges();

							loanEmiCard.setEmiLateFeeCharges(0);
							loanEmiCard.setExcessOfEmiAmount(excessAmount);
							InterestDetails interestDetailsInfo = interestDetailsRepo
									.findByloanIdAndEmiNumber(loanEmiCard.getLoanId(), loanEmiCard.getEmiNumber());
							if (interestDetailsInfo != null) {
								// interestDetailsInfo.setInterest(Math.round(loanEmiCard.getEmiLateFeeCharges()));
								if (loanEmiCard.getEmiLateFeeCharges() == 0) {
									interestDetailsInfo.setInterestPaid(new Date());
								}
								interestDetailsRepo.save(interestDetailsInfo);
							}
						} else {
							loanEmiCard.setEmiLateFeeCharges(loanEmiCard.getEmiLateFeeCharges() - excessAmount);
							InterestDetails interestDetailsInfo = interestDetailsRepo
									.findByloanIdAndEmiNumber(loanEmiCard.getLoanId(), loanEmiCard.getEmiNumber());
							if (interestDetailsInfo != null) {
								// interestDetailsInfo.setInterest(Math.round(excessAmount));
								if (loanEmiCard.getEmiLateFeeCharges() == 0) {
									interestDetailsInfo.setInterestPaid(new Date());
								}
								interestDetailsRepo.save(interestDetailsInfo);
							}

						}
					} else {
						loanEmiCard.setExcessOfEmiAmount(excessAmount);
					}

				} else {
					loanEmiCard.setRemainingEmiAmount(loanEmiCard.getRemainingEmiAmount() - paritialPaymentAmount);
				}
				if (loanEmiCardPaymentDetailsRequestDto.getPaymentStatus().equals(PaymentStatus.FULLYPAID.toString())) {
					try {
						loanEmiCard.setEmiPaidOn(
								expectedDateFormat.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
					} catch (ParseException e) {

						e.printStackTrace();
					}
					loanEmiCardRepo.save(loanEmiCard);
				}

			} else {
				if (paritialPaymentAmount >= loanEmiCard.getEmiAmount()) {
					loanEmiCard.setRemainingEmiAmount(loanEmiCard.getEmiAmount());
					loanEmiCard.setStatus(loanEmiCard.getStatus().COMPLETED);
					excessAmount = paritialPaymentAmount - loanEmiCard.getEmiAmount();
					if (loanEmiCard.getEmiLateFeeCharges() > 0) {
						if (excessAmount >= loanEmiCard.getEmiLateFeeCharges()) {
							excessAmount = excessAmount - loanEmiCard.getEmiLateFeeCharges();

							loanEmiCard.setEmiLateFeeCharges(0);

							loanEmiCard.setExcessOfEmiAmount(excessAmount);
							InterestDetails interestDetailsInfo = interestDetailsRepo
									.findByloanIdAndEmiNumber(loanEmiCard.getLoanId(), loanEmiCard.getEmiNumber());
							if (interestDetailsInfo != null) {
								// interestDetailsInfo.setInterest(Math.round(loanEmiCard.getEmiLateFeeCharges()));
								if (loanEmiCard.getEmiLateFeeCharges() == 0) {
									interestDetailsInfo.setInterestPaid(new Date());
								}
								interestDetailsRepo.save(interestDetailsInfo);
							}
						} else {
							loanEmiCard.setEmiLateFeeCharges(loanEmiCard.getEmiLateFeeCharges() - excessAmount);
							InterestDetails interestDetailsInfo = interestDetailsRepo
									.findByloanIdAndEmiNumber(loanEmiCard.getLoanId(), loanEmiCard.getEmiNumber());
							if (interestDetailsInfo != null) {
								// interestDetailsInfo.setInterest(Math.round(excessAmount));
								if (loanEmiCard.getEmiLateFeeCharges() == 0) {
									interestDetailsInfo.setInterestPaid(new Date());
								}
								interestDetailsRepo.save(interestDetailsInfo);
							}

						}
					} else {
						loanEmiCard.setExcessOfEmiAmount(excessAmount);
					}
					if (loanEmiCardPaymentDetailsRequestDto.getPaymentStatus()
							.equals(PaymentStatus.FULLYPAID.toString())) {
						try {
							loanEmiCard.setEmiPaidOn(
									expectedDateFormat.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
						} catch (ParseException e) {

							e.printStackTrace();
						}
						loanEmiCardRepo.save(loanEmiCard);
					}

				} else {
					loanEmiCard.setRemainingEmiAmount(loanEmiCard.getEmiAmount() - paritialPaymentAmount);
					loanEmiCard.setStatus(loanEmiCard.getStatus().INPROCESS);
					if (loanEmiCardPaymentDetailsRequestDto.getPaymentStatus()
							.equals(PaymentStatus.FULLYPAID.toString())) {
						try {
							loanEmiCard.setEmiPaidOn(
									expectedDateFormat.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));

						} catch (ParseException e) {

							e.printStackTrace();
						}
						loanEmiCardRepo.save(loanEmiCard);
					}
				}

			}

		}

		loanEmiCardRepo.save(loanEmiCard);

		logger.info("UpdEmisByApplication method end !!!!!!");
		return excessAmount;

	}

	@Override
	public List<AdminEmiDetailsResponseDto> getEmiDetails() {

		List<Object[]> proecssedEMis = enachTransactionVerificationRepo.getNoOfEMIProcessedAndAmount();
		List<Object[]> notProecssedEMis = enachTransactionVerificationRepo.getNoOfEMINotProcessedAndAmount();
		List<Object[]> emipending = loanEmiCardRepo.getNoPendingEmisAndPendingAmount();
		List<Object[]> npaBucket = loanEmiCardRepo.getSumAmountAndCount(91);
		List<Object[]> thirtyDayBucket = loanEmiCardRepo.getSumAmountBucketsAndCount(1, 30);
		List<Object[]> sixtyDayBucket = loanEmiCardRepo.getSumAmountBucketsAndCount(31, 60);
		List<Object[]> nintyDayBucket = loanEmiCardRepo.getSumAmountBucketsAndCount(61, 90);

		List<AdminEmiDetailsResponseDto> compoundList = createIterator(proecssedEMis, notProecssedEMis, emipending,
				npaBucket, thirtyDayBucket, sixtyDayBucket, nintyDayBucket);
		return compoundList;
	}

	private List<AdminEmiDetailsResponseDto> createIterator(List<Object[]> proecssedEMis,
			List<Object[]> notProecssedEMis, List<Object[]> emipending, List<Object[]> npaBucket,
			List<Object[]> thirtyDayBucket, List<Object[]> sixtyDayBucket, List<Object[]> nintyDayBucket) {

		Integer thirtyDayCount = oxyLoanRequestRepo.getApplicationBucketscount(1, 30);
		Integer sixtyDayCount = oxyLoanRequestRepo.getApplicationBucketscount(31, 60);
		Integer nintyDayCount = oxyLoanRequestRepo.getApplicationBucketscount(61, 90);
		Integer npaDayCount = oxyLoanRequestRepo.getApplicationNpaBucketscount(91);

		List<Object[]> totalCurrntEmiCount = loanEmiCardRepo.getCurrntTotalEmiCount();

		Integer totalEmiCount = loanEmiCardRepo.getTotalEmiCount();
		List<AdminEmiDetailsResponseDto> adminEmiDetailsResponseDtos = new ArrayList<AdminEmiDetailsResponseDto>();

		for (Object[] obj : totalCurrntEmiCount) {
			AdminEmiDetailsResponseDto adminEmiDetailsResponseDto = new AdminEmiDetailsResponseDto();
			adminEmiDetailsResponseDto.setCurrentMonthTotalEmis(((BigInteger) obj[0]).intValue());
			adminEmiDetailsResponseDto.setCurrnentMonthTotalAmount((Double) obj[1] == null ? 0.0d : (Double) obj[1]);
			adminEmiDetailsResponseDto.setTotalPendingEMis(totalEmiCount);
			adminEmiDetailsResponseDtos.add(adminEmiDetailsResponseDto);
		}

		for (Object[] objProecssedEMis : proecssedEMis) {
			AdminEmiDetailsResponseDto adminEmiDetailsResponseDto = new AdminEmiDetailsResponseDto();
			adminEmiDetailsResponseDto.setNoOfEMIProcessed(((BigInteger) objProecssedEMis[0]).intValue());
			adminEmiDetailsResponseDto
					.setAmountReceived((Double) objProecssedEMis[1] == null ? 0.0d : (Double) objProecssedEMis[1]);
			adminEmiDetailsResponseDto
					.setEarnedAmount((Double) objProecssedEMis[2] == null ? 0.0d : (Double) objProecssedEMis[1]);
			adminEmiDetailsResponseDtos.add(adminEmiDetailsResponseDto);

		}

		for (Object[] objNotProecssedEMis : notProecssedEMis) {
			AdminEmiDetailsResponseDto adminEmiDetailsResponseDto = new AdminEmiDetailsResponseDto();
			adminEmiDetailsResponseDto.setNoOfEmiNotProcessed(((BigInteger) objNotProecssedEMis[0]).intValue());
			adminEmiDetailsResponseDto.setAmountNotReceived(
					(Double) objNotProecssedEMis[1] == null ? 0.0d : (Double) objNotProecssedEMis[1]);
			adminEmiDetailsResponseDtos.add(adminEmiDetailsResponseDto);
		}

		for (Object[] objEmipending : emipending) {
			AdminEmiDetailsResponseDto adminEmiDetailsResponseDto = new AdminEmiDetailsResponseDto();
			adminEmiDetailsResponseDto.setNoOFEMISpending(((BigInteger) objEmipending[0]).intValue());
			adminEmiDetailsResponseDtos.add(adminEmiDetailsResponseDto);
		}

		for (Object[] objNpaBucket : npaBucket) {
			AdminEmiDetailsResponseDto adminEmiDetailsResponseDto = new AdminEmiDetailsResponseDto();
			adminEmiDetailsResponseDto.setNpaDayCount(npaDayCount);

			adminEmiDetailsResponseDto
					.setNpaDayAmount((Double) objNpaBucket[1] == null ? 0.0d : (Double) objNpaBucket[1]);
			adminEmiDetailsResponseDtos.add(adminEmiDetailsResponseDto);
		}

		for (Object[] ObjThirtyDayBucket : thirtyDayBucket) {
			AdminEmiDetailsResponseDto adminEmiDetailsResponseDto = new AdminEmiDetailsResponseDto();
			adminEmiDetailsResponseDto.setThirtyDayCount(thirtyDayCount);
			adminEmiDetailsResponseDto
					.setThirtyDayAmount((Double) ObjThirtyDayBucket[1] == null ? 0.0d : (Double) ObjThirtyDayBucket[1]);
			adminEmiDetailsResponseDtos.add(adminEmiDetailsResponseDto);
		}

		for (Object[] objSixtyDayBucket : sixtyDayBucket) {
			AdminEmiDetailsResponseDto adminEmiDetailsResponseDto = new AdminEmiDetailsResponseDto();
			adminEmiDetailsResponseDto.setSixtyDayCount(sixtyDayCount);
			adminEmiDetailsResponseDto
					.setSixtyDayAmount((Double) objSixtyDayBucket[1] == null ? 0.0d : (Double) objSixtyDayBucket[1]);
			adminEmiDetailsResponseDtos.add(adminEmiDetailsResponseDto);
		}
		for (Object[] ObjeNintyDayBucket : nintyDayBucket) {
			AdminEmiDetailsResponseDto adminEmiDetailsResponseDto = new AdminEmiDetailsResponseDto();
			adminEmiDetailsResponseDto.setNintyDayCount(nintyDayCount);
			adminEmiDetailsResponseDto
					.setNintyDayAmount((Double) ObjeNintyDayBucket[1] == null ? 0.0d : (Double) ObjeNintyDayBucket[1]);
			adminEmiDetailsResponseDtos.add(adminEmiDetailsResponseDto);
		}
		return adminEmiDetailsResponseDtos;
	}

	@Override
	public LenderTransactionHistoryResponseDto generateLenderHistoryPdf(int userId)
			throws PdfGeenrationException, IOException {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BucketPdfresponse ninetyDaysBucketDownload() throws PdfGeenrationException, IOException {

		BucketPdfresponse response = new BucketPdfresponse();

		TemplateContext templateContext = new TemplateContext();

		templateContext.put("ninetydaysbucket", getBucketDaysPdf("90days", 61, 90));

		String outputFileName = "NinetyDaysBucket" + ".pdf";
		// String outputFileName = "C:/data/LenderTransactionHistory.pdf";
		String lenderTransactionHistory = pdfEngine.generatePdf("agreement/ninetydaysbucket.xml", templateContext,
				outputFileName);

		FileRequest fileRequest = new FileRequest();
		fileRequest.setInputStream(new FileInputStream(lenderTransactionHistory));
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.ninetydaysbucket);
		fileRequest.setFilePrifix(FileType.ninetydaysbucket.name());
		FileResponse file1 = fileManagementService.getFile(fileRequest);
		try {
			FileResponse putFile = fileManagementService.putFile(fileRequest);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		File file = new File(lenderTransactionHistory);
		boolean fileDeleted = file.delete();

		response.setDownloadUrl(file1.getDownloadUrl());

		return response;
	}

	@Override
	public BucketPdfresponse sixtyDaysBucketDownload() throws PdfGeenrationException, IOException {

		BucketPdfresponse response = new BucketPdfresponse();

		TemplateContext templateContext = new TemplateContext();

		templateContext.put("sixtydaysbucket", getBucketDaysPdf("60days", 31, 60));

		String outputFileName = "SixtyDaysBucket" + ".pdf";
		// String outputFileName = "C:/data/LenderTransactionHistory.pdf";
		String lenderTransactionHistory = pdfEngine.generatePdf("agreement/sixtydaysbucket.xml", templateContext,
				outputFileName);

		FileRequest fileRequest = new FileRequest();
		fileRequest.setInputStream(new FileInputStream(lenderTransactionHistory));
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.sixtydaysbucket);
		fileRequest.setFilePrifix(FileType.sixtydaysbucket.name());
		FileResponse file1 = fileManagementService.getFile(fileRequest);
		try {
			FileResponse putFile = fileManagementService.putFile(fileRequest);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		File file = new File(lenderTransactionHistory);
		boolean fileDeleted = file.delete();

		response.setDownloadUrl(file1.getDownloadUrl());

		return response;
	}

	@Override
	public BucketPdfresponse thirtyDaysBucketDownload() throws PdfGeenrationException, IOException {

		BucketPdfresponse response = new BucketPdfresponse();

		TemplateContext templateContext = new TemplateContext();

		templateContext.put("thirtydaysbucket", getBucketDaysPdf("30days", 1, 30));

		String outputFileName = "ThirtyDaysBucket" + ".pdf";
		// String outputFileName = "C:/data/LenderTransactionHistory.pdf";
		String lenderTransactionHistory = pdfEngine.generatePdf("agreement/thirtydaysbucket.xml", templateContext,
				outputFileName);

		FileRequest fileRequest = new FileRequest();
		fileRequest.setInputStream(new FileInputStream(lenderTransactionHistory));
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.thirtydaysbucket);
		fileRequest.setFilePrifix(FileType.thirtydaysbucket.name());
		FileResponse file1 = fileManagementService.getFile(fileRequest);
		try {
			FileResponse putFile = fileManagementService.putFile(fileRequest);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		File file = new File(lenderTransactionHistory);
		boolean fileDeleted = file.delete();

		response.setDownloadUrl(file1.getDownloadUrl());

		return response;
	}

	@Override
	public BucketPdfresponse npaBucketDownload() throws PdfGeenrationException, IOException {

		BucketPdfresponse response = new BucketPdfresponse();

		TemplateContext templateContext = new TemplateContext();

		templateContext.put("npadaysbucket", getBucketDaysPdf("NPA", 91, 0));

		String outputFileName = "NpaBucket" + ".pdf";
		// String outputFileName = "C:/data/LenderTransactionHistory.pdf";
		String lenderTransactionHistory = pdfEngine.generatePdf("agreement/npabuckettemplate.xml", templateContext,
				outputFileName);

		FileRequest fileRequest = new FileRequest();
		fileRequest.setInputStream(new FileInputStream(lenderTransactionHistory));
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.npabucket);
		fileRequest.setFilePrifix(FileType.npabucket.name());
		FileResponse file1 = fileManagementService.getFile(fileRequest);
		try {
			FileResponse putFile = fileManagementService.putFile(fileRequest);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		File file = new File(lenderTransactionHistory);
		boolean fileDeleted = file.delete();

		response.setDownloadUrl(file1.getDownloadUrl());

		return response;
	}

	private String getBucketDaysPdf(String bucketDays, int start, int end) {
		String returnStr = "";
		SearchResultsDto<LoanResponseDto> searchresultsDto = bucketPendingEmis(start, end, bucketDays, 1, 5000);
		List<LoanResponseDto> dtoList = searchresultsDto.getResults();
		if (dtoList.size() == 0) {
			returnStr = "<fo:table-row>\r\n<fo:table-cell number-columns-spanned=\"5\">  <fo:block text-align=\"left\">"
					+ " No Loans Found. </fo:block></fo:table-cell> </fo:table-row>";
		} else {
			for (LoanResponseDto dto : dtoList) {
				returnStr = returnStr + generateRows(dto);
			}
		}

		return returnStr;

	}

	public String generateRows(LoanResponseDto dto) {

		String lenderDetails = "LR" + dto.getLenderUser().getId() + " \r\n" + dto.getLenderUser().getFirstName() + " "
				+ dto.getLenderUser().getLastName() + " \r\n" + dto.getLenderUser().getMobileNumber();

		String borrowerDetails = "BR" + dto.getBorrowerUser().getId() + " \r\n" + dto.getBorrowerUser().getFirstName()
				+ " " + dto.getBorrowerUser().getLastName() + " \r\n" + dto.getBorrowerUser().getMobileNumber();

		String emiLastPaidOn = "";

		String rowdata =

				"<fo:table-row>\r\n"

						+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
						+ " <fo:block text-align=\"right\">"

						+ dto.getLoanId() + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

						+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
						+ " <fo:block text-align=\"right\">" + lenderDetails + "</fo:block>\r\n"
						+ " </fo:table-cell>\r\n"

						+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
						+ " <fo:block text-align=\"right\">" + borrowerDetails + "</fo:block>\r\n"
						+ " </fo:table-cell>\r\n"

						+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
						+ " <fo:block text-align=\"right\">" + dto.getLoanDisbursedAmount() + "</fo:block>\r\n"
						+ " </fo:table-cell>\r\n"

						+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
						+ " <fo:block text-align=\"right\">"

						+ emiLastPaidOn + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

						+ " </fo:table-row>";
		return rowdata;
	}

	@Override
	public SearchResultsDto<LoanResponseDto> PendingEmisWithUserId(int start, int end, String type, int userId,
			int pageNo, int pageSize) {

		if (type.equalsIgnoreCase("30days")) {
			List<LoanResponseDto> currentMoantLoanResponse = new ArrayList<LoanResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getBucketsByUserId(start, end, userId, pageNo, pageSize);
			Integer totalCount = oxyLoanRepo.getBucketsByUserIdCount(start, end, userId);
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				currentMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(currentMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount == null ? 0 : totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;
		} else if (type.equalsIgnoreCase("60days")) {

			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getBucketsByUserId(start, end, userId, pageNo, pageSize); // Integer
			Integer totalCount = oxyLoanRepo.getBucketsByUserIdCount(start, end, userId);
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount == null ? 0 : totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;

		}

		else if (type.equalsIgnoreCase("90days")) {
			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getBucketsByUserId(start, end, userId, pageNo, pageSize);
			;
			Integer totalCount = oxyLoanRepo.getBucketsByUserIdCount(start, end, userId);
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount == null ? 0 : totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;
		} else {
			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getBucketsNpaByUserId(start, userId, pageNo, pageSize);
			Integer totalCount = oxyLoanRepo.getBucketsNpaByUserIdCount(start, userId);

			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount == null ? 0 : totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;

		}
	}

	@Override
	public SearchResultsDto<LoanResponseDto> emiPaidwithUserId(int year, int month, String type, int userId, int pageNo,
			int pageSize) {

		if (type.equalsIgnoreCase("Current")) {
			List<LoanResponseDto> currentMoantLoanResponse = new ArrayList<LoanResponseDto>();
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, 2020);
			calendar.set(Calendar.MONTH, month - 1);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			Date monthStart = calendar.getTime();
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(6));
			Date monthEnd = calendar.getTime();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getCurrentMonthEmisByUserId(monthStart, monthEnd, userId,
					pageNo, pageSize);
			Integer totalCount = oxyLoanRepo.getCurrentcount(monthStart, monthEnd);
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				currentMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(currentMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;
		} else if (type.equalsIgnoreCase("feature")) {

			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, 2020);
			calendar.set(Calendar.MONTH, month - 1);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			Date monthStart = calendar.getTime();
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getFeatureMonthEmiByUserId(monthStart, userId, pageNo,
					pageSize);
			Integer totalCount = oxyLoanRepo.getFeatureCount(monthStart);
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;

		}

		else {
			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.gettotalEmisByUserId(userId, pageNo, pageSize);
			Integer totalCount = oxyLoanRepo.getTotal();
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setId((int) obj[0]);
				loanResponseDto.setLoanId((String) obj[1]);
				loanResponseDto.setLenderUserId((int) obj[2]);
				loanResponseDto.setBorrowerUserId((int) obj[3]);
				loanResponseDto.setLoanDisbursedAmount((double) obj[4]);
				loanResponseDto.setBorrowerDisbursementDate(obj[5].toString());
				loanResponseDto.setBorrowerUser(populateUserDetails((int) obj[3]));
				loanResponseDto.setLenderUser(populateUserDetails((int) obj[2]));
				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;
		}

	}

	@Override
	public int getAdminPendingActionsDetailsSchedular() {
		Date currentDate = new Date();
		String dateFormate = expectedDateFormat.format(currentDate);

		int interestedPendingBorrowers = userRepo.getBorrowersInformation();
		int interestedPendeingLenders = userRepo.getLendersInformation();
		int InterestedCount = userRepo.getInterestedCount();
		int offerSendCount = loanRequestRepo.getNumberOffersSentCount();
		int loanOfferPending = InterestedCount - offerSendCount;

		int lenderTransactionUploadedPending = lenderOxyWalletNativeRepo.getLenderTransactionPendingInfo();

		int countNumberOfLoansInAgreedState = oxyLoanRepo.getcountNumberOfLoansInAgreedState();
		int countNumberPendingDisbursmentDate = oxyLoanRepo.getcountPendingAtDisbusmentDate();

		TemplateContext context = new TemplateContext();
		context.put("InterestedPendingBorrowers", interestedPendingBorrowers);
		context.put("InterestedPendingLenders", interestedPendeingLenders);
		context.put("LoanOfferPending", loanOfferPending);
		context.put("LenderTransactionUpload", lenderTransactionUploadedPending);
		context.put("LoansForApproval", countNumberOfLoansInAgreedState);
		context.put("DisburmentPending", countNumberPendingDisbursmentDate);
		context.put("CurrentDate", dateFormate);

		String mailsubject = "Pending actions"; // Mails Going to Lenders so subject is like this..
		String emailTemplateName = "admin-pendingactions.template";

		EmailRequest emailRequest = new EmailRequest(new String[] { this.supportEmail }, mailsubject, emailTemplateName,
				context);
		EmailResponse emailResponse = emailService.sendEmail(emailRequest);
		if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
			throw new RuntimeException(emailResponse.getErrorMessage());
		}

		return 1;
	}

	@Override
	public SearchResultsDto<ApplicationResponseDto> bucketsByApplication(int start, int end, String type, int pageNo,
			int pageSize) {

		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}

		if (type.equalsIgnoreCase("30days")) {
			List<ApplicationResponseDto> currentMoanthLoanResponse = new ArrayList<ApplicationResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> thirtyApplication = oxyLoanRequestRepo.getApplicationBuckets(start, end, pageNo, pageSize);
			Integer totalCount = oxyLoanRequestRepo.getApplicationBucketscount(start, end);
			for (Object[] obj : thirtyApplication) {
				ApplicationResponseDto applicationResponseDto = new ApplicationResponseDto();
				// R.user_primary_type
				// ,R.id,R.loan_request_amount,A.disbursment_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id
				UserResponse userResponse = new UserResponse();
				applicationResponseDto.setId((int) obj[1]);
				applicationResponseDto.setLoanRequestAmount((double) obj[2]);
				userResponse.setPrimaryType(obj[0].toString());
				userResponse.setMobileNumber(obj[3].toString());
				userResponse.setEmail(obj[4].toString());
				userResponse.setFirstName(obj[5].toString());
				userResponse.setLastName(obj[6].toString());
				userResponse.setId((int) obj[8]);
				applicationResponseDto.setApplicationId(obj[7].toString());
				applicationResponseDto.setLoanOfferedAmount((double) obj[9]);
				applicationResponseDto.setBorrowerUser(userResponse);
				currentMoanthLoanResponse.add(applicationResponseDto);

			}
			SearchResultsDto<ApplicationResponseDto> searchResultsDto = new SearchResultsDto<ApplicationResponseDto>();
			searchResultsDto.setResults(currentMoanthLoanResponse);
			searchResultsDto.setTotalCount(totalCount == null ? 0 : totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;
		} else if (type.equalsIgnoreCase("60days")) {

			List<ApplicationResponseDto> currentMoanthLoanResponse = new ArrayList<ApplicationResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> thirtyApplication = oxyLoanRequestRepo.getApplicationBuckets(start, end, pageNo, pageSize);
			Integer totalCount = oxyLoanRequestRepo.getApplicationBucketscount(start, end);

			for (Object[] obj : thirtyApplication) {
				ApplicationResponseDto applicationResponseDto = new ApplicationResponseDto();
				// R.user_primary_type
				// ,R.id,R.loan_request_amount,A.disbursment_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id
				UserResponse userResponse = new UserResponse();
				applicationResponseDto.setId((int) obj[1]);
				applicationResponseDto.setLoanRequestAmount((double) obj[2]);
				userResponse.setPrimaryType(obj[0].toString());
				userResponse.setMobileNumber(obj[3].toString());
				userResponse.setEmail(obj[4].toString());
				userResponse.setFirstName(obj[5].toString());
				userResponse.setLastName(obj[6].toString());
				userResponse.setId((int) obj[8]);
				applicationResponseDto.setApplicationId(obj[7].toString());
				applicationResponseDto.setLoanOfferedAmount((double) obj[9]);
				applicationResponseDto.setBorrowerUser(userResponse);
				currentMoanthLoanResponse.add(applicationResponseDto);

			}
			SearchResultsDto<ApplicationResponseDto> searchResultsDto = new SearchResultsDto<ApplicationResponseDto>();
			searchResultsDto.setResults(currentMoanthLoanResponse);
			searchResultsDto.setTotalCount(totalCount == null ? 0 : totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;

		}

		else if (type.equalsIgnoreCase("90days")) {

			List<ApplicationResponseDto> currentMoanthLoanResponse = new ArrayList<ApplicationResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> thirtyApplication = oxyLoanRequestRepo.getApplicationBuckets(start, end, pageNo, pageSize);
			Integer totalCount = oxyLoanRequestRepo.getApplicationBucketscount(start, end);

			for (Object[] obj : thirtyApplication) {
				ApplicationResponseDto applicationResponseDto = new ApplicationResponseDto();
				// R.user_primary_type
				// ,R.id,R.loan_request_amount,A.disbursment_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id
				UserResponse userResponse = new UserResponse();
				applicationResponseDto.setId((int) obj[1]);
				applicationResponseDto.setLoanRequestAmount((double) obj[2]);
				userResponse.setPrimaryType(obj[0].toString());
				userResponse.setMobileNumber(obj[3].toString());
				userResponse.setEmail(obj[4].toString());
				userResponse.setFirstName(obj[5].toString());
				userResponse.setLastName(obj[6].toString());
				userResponse.setId((int) obj[8]);
				applicationResponseDto.setApplicationId(obj[7].toString());
				applicationResponseDto.setLoanOfferedAmount((double) obj[9]);
				applicationResponseDto.setBorrowerUser(userResponse);
				currentMoanthLoanResponse.add(applicationResponseDto);

			}
			SearchResultsDto<ApplicationResponseDto> searchResultsDto = new SearchResultsDto<ApplicationResponseDto>();
			searchResultsDto.setResults(currentMoanthLoanResponse);
			searchResultsDto.setTotalCount(totalCount == null ? 0 : totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;

		}

		else {

			List<ApplicationResponseDto> currentMoanthLoanResponse = new ArrayList<ApplicationResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> thirtyApplication = oxyLoanRequestRepo.getApplicationNpaBuckets(start, pageNo, pageSize);
			Integer totalCount = oxyLoanRequestRepo.getApplicationNpaBucketscount(start);

			for (Object[] obj : thirtyApplication) {
				ApplicationResponseDto applicationResponseDto = new ApplicationResponseDto();
				// R.user_primary_type
				// ,R.id,R.loan_request_amount,A.disbursment_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id
				UserResponse userResponse = new UserResponse();
				applicationResponseDto.setId((int) obj[1]);
				applicationResponseDto.setLoanRequestAmount((double) obj[2]);
				userResponse.setPrimaryType(obj[0].toString());
				userResponse.setMobileNumber(obj[3].toString());
				userResponse.setEmail(obj[4].toString());
				userResponse.setFirstName(obj[5].toString());
				userResponse.setLastName(obj[6].toString());
				userResponse.setId((int) obj[8]);
				applicationResponseDto.setLoanOfferedAmount((double) obj[9]);
				applicationResponseDto.setApplicationId(obj[7].toString());
				applicationResponseDto.setBorrowerUser(userResponse);
				currentMoanthLoanResponse.add(applicationResponseDto);

			}
			SearchResultsDto<ApplicationResponseDto> searchResultsDto = new SearchResultsDto<ApplicationResponseDto>();
			searchResultsDto.setResults(currentMoanthLoanResponse);
			searchResultsDto.setTotalCount(totalCount == null ? 0 : totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;

		}
	}

	@Override
	public SearchResultsDto<ApplicationResponseDto> bucketsByApplicationByUserId(int start, int end, String type,
			int userId) {
		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}

		if (type.equalsIgnoreCase("30days")) {
			List<ApplicationResponseDto> currentMoanthLoanResponse = new ArrayList<ApplicationResponseDto>();
			List<Object[]> thirtyApplication = oxyLoanRequestRepo.getApplicationBucketsByUserId(start, end, userId);
			for (Object[] obj : thirtyApplication) {
				ApplicationResponseDto applicationResponseDto = new ApplicationResponseDto();
				// R.user_primary_type
				// ,R.id,R.loan_request_amount,A.disbursment_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id
				UserResponse userResponse = new UserResponse();
				applicationResponseDto.setId((int) obj[1]);
				applicationResponseDto.setLoanRequestAmount((double) obj[2]);
				userResponse.setPrimaryType(obj[0].toString());
				userResponse.setMobileNumber(obj[3].toString());
				userResponse.setEmail(obj[4].toString());
				userResponse.setFirstName(obj[5].toString());
				userResponse.setLastName(obj[6].toString());
				userResponse.setId((int) obj[8]);
				applicationResponseDto.setApplicationId(obj[7].toString());
				applicationResponseDto.setLoanOfferedAmount((double) obj[9]);
				applicationResponseDto.setBorrowerUser(userResponse);
				currentMoanthLoanResponse.add(applicationResponseDto);

			}
			SearchResultsDto<ApplicationResponseDto> searchResultsDto = new SearchResultsDto<ApplicationResponseDto>();
			searchResultsDto.setResults(currentMoanthLoanResponse);
			return searchResultsDto;
		} else if (type.equalsIgnoreCase("60days")) {

			List<ApplicationResponseDto> currentMoanthLoanResponse = new ArrayList<ApplicationResponseDto>();
			List<Object[]> thirtyApplication = oxyLoanRequestRepo.getApplicationBucketsByUserId(start, end, userId);
			;

			for (Object[] obj : thirtyApplication) {
				ApplicationResponseDto applicationResponseDto = new ApplicationResponseDto();
				// R.user_primary_type
				// ,R.id,R.loan_request_amount,A.disbursment_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id
				UserResponse userResponse = new UserResponse();
				applicationResponseDto.setId((int) obj[1]);
				applicationResponseDto.setLoanRequestAmount((double) obj[2]);
				userResponse.setPrimaryType(obj[0].toString());
				userResponse.setMobileNumber(obj[3].toString());
				userResponse.setEmail(obj[4].toString());
				userResponse.setFirstName(obj[5].toString());
				userResponse.setLastName(obj[6].toString());
				userResponse.setId((int) obj[8]);
				applicationResponseDto.setApplicationId(obj[7].toString());
				applicationResponseDto.setLoanOfferedAmount((double) obj[9]);
				applicationResponseDto.setBorrowerUser(userResponse);
				currentMoanthLoanResponse.add(applicationResponseDto);

			}
			SearchResultsDto<ApplicationResponseDto> searchResultsDto = new SearchResultsDto<ApplicationResponseDto>();
			searchResultsDto.setResults(currentMoanthLoanResponse);
			return searchResultsDto;

		}

		else if (type.equalsIgnoreCase("90days")) {

			List<ApplicationResponseDto> currentMoanthLoanResponse = new ArrayList<ApplicationResponseDto>();
			List<Object[]> thirtyApplication = oxyLoanRequestRepo.getApplicationBucketsByUserId(start, end, userId);

			for (Object[] obj : thirtyApplication) {
				ApplicationResponseDto applicationResponseDto = new ApplicationResponseDto();
				// R.user_primary_type
				// ,R.id,R.loan_request_amount,A.disbursment_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id
				UserResponse userResponse = new UserResponse();
				applicationResponseDto.setId((int) obj[1]);
				applicationResponseDto.setLoanRequestAmount((double) obj[2]);
				userResponse.setPrimaryType(obj[0].toString());
				userResponse.setMobileNumber(obj[3].toString());
				userResponse.setEmail(obj[4].toString());
				userResponse.setFirstName(obj[5].toString());
				userResponse.setLastName(obj[6].toString());
				userResponse.setId((int) obj[8]);
				applicationResponseDto.setApplicationId(obj[7].toString());
				applicationResponseDto.setLoanOfferedAmount((double) obj[9]);
				applicationResponseDto.setBorrowerUser(userResponse);
				currentMoanthLoanResponse.add(applicationResponseDto);

			}
			SearchResultsDto<ApplicationResponseDto> searchResultsDto = new SearchResultsDto<ApplicationResponseDto>();
			searchResultsDto.setResults(currentMoanthLoanResponse);
			return searchResultsDto;

		}

		else {

			List<ApplicationResponseDto> currentMoanthLoanResponse = new ArrayList<ApplicationResponseDto>();
			List<Object[]> thirtyApplication = oxyLoanRequestRepo.getApplicationNpaBucketsByUserId(start, userId);

			for (Object[] obj : thirtyApplication) {
				ApplicationResponseDto applicationResponseDto = new ApplicationResponseDto();
				// R.user_primary_type
				// ,R.id,R.loan_request_amount,A.disbursment_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id
				UserResponse userResponse = new UserResponse();
				applicationResponseDto.setId((int) obj[1]);
				applicationResponseDto.setLoanRequestAmount((double) obj[2]);
				userResponse.setPrimaryType(obj[0].toString());
				userResponse.setMobileNumber(obj[3].toString());
				userResponse.setEmail(obj[4].toString());
				userResponse.setFirstName(obj[5].toString());
				userResponse.setLastName(obj[6].toString());
				userResponse.setId((int) obj[8]);
				applicationResponseDto.setApplicationId(obj[7].toString());
				applicationResponseDto.setLoanOfferedAmount((double) obj[9]);
				applicationResponseDto.setBorrowerUser(userResponse);
				currentMoanthLoanResponse.add(applicationResponseDto);

			}
			SearchResultsDto<ApplicationResponseDto> searchResultsDto = new SearchResultsDto<ApplicationResponseDto>();
			searchResultsDto.setResults(currentMoanthLoanResponse);
			return searchResultsDto;
		}
	}

	@Override
	public SearchResultsDto<BucketPendingEmis> bucketsEmiPending(int start, int end, String type, int loanId) {
		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}

		if (type.equalsIgnoreCase("30days")) {
			List<BucketPendingEmis> currentMoanthLoanResponse = new ArrayList<BucketPendingEmis>();
			List<Object[]> thirtyApplication = loanEmiCardRepo.getBucketEmis(start, end, loanId);
			for (Object[] obj : thirtyApplication) {
				BucketPendingEmis bucketPendingEmis = new BucketPendingEmis();
				// B.emi_amount,B.emi_due_on,loan_id,now()\\:\\:date - B.emi_due_on\\:\\:date as
				// diff_days,B.emi_number

				bucketPendingEmis.setEmiAmount((double) obj[0]);
				bucketPendingEmis.setEmiDueOn(obj[1].toString());
				bucketPendingEmis.setLoanId((int) obj[2]);
				bucketPendingEmis.setDaysDiff((int) obj[3]);
				bucketPendingEmis.setEmiNumber((int) obj[4]);
				currentMoanthLoanResponse.add(bucketPendingEmis);

			}
			SearchResultsDto<BucketPendingEmis> searchResultsDto = new SearchResultsDto<BucketPendingEmis>();
			searchResultsDto.setResults(currentMoanthLoanResponse);
			return searchResultsDto;
		} else if (type.equalsIgnoreCase("60days")) {

			List<BucketPendingEmis> currentMoanthLoanResponse = new ArrayList<BucketPendingEmis>();
			List<Object[]> thirtyApplication = loanEmiCardRepo.getBucketEmis(start, end, loanId);

			for (Object[] obj : thirtyApplication) {
				BucketPendingEmis bucketPendingEmis = new BucketPendingEmis();
				// B.emi_amount,B.emi_due_on,loan_id,now()\\:\\:date - B.emi_due_on\\:\\:date as
				// diff_days,B.emi_number

				bucketPendingEmis.setEmiAmount((double) obj[0]);
				bucketPendingEmis.setEmiDueOn(obj[1].toString());
				bucketPendingEmis.setLoanId((int) obj[2]);
				bucketPendingEmis.setDaysDiff((int) obj[3]);
				bucketPendingEmis.setEmiNumber((int) obj[4]);
				currentMoanthLoanResponse.add(bucketPendingEmis);

			}
			SearchResultsDto<BucketPendingEmis> searchResultsDto = new SearchResultsDto<BucketPendingEmis>();
			searchResultsDto.setResults(currentMoanthLoanResponse);
			return searchResultsDto;

		}

		else if (type.equalsIgnoreCase("90days")) {

			List<BucketPendingEmis> currentMoanthLoanResponse = new ArrayList<BucketPendingEmis>();
			List<Object[]> thirtyApplication = loanEmiCardRepo.getBucketEmis(start, end, loanId);

			for (Object[] obj : thirtyApplication) {
				BucketPendingEmis bucketPendingEmis = new BucketPendingEmis();
				// B.emi_amount,B.emi_due_on,loan_id,now()\\:\\:date - B.emi_due_on\\:\\:date as
				// diff_days,B.emi_number

				bucketPendingEmis.setEmiAmount((double) obj[0]);
				bucketPendingEmis.setEmiDueOn(obj[1].toString());
				bucketPendingEmis.setLoanId((int) obj[2]);
				bucketPendingEmis.setDaysDiff((int) obj[3]);
				bucketPendingEmis.setEmiNumber((int) obj[4]);
				currentMoanthLoanResponse.add(bucketPendingEmis);

			}
			SearchResultsDto<BucketPendingEmis> searchResultsDto = new SearchResultsDto<BucketPendingEmis>();
			searchResultsDto.setResults(currentMoanthLoanResponse);
			return searchResultsDto;

		}

		else {

			List<BucketPendingEmis> currentMoanthLoanResponse = new ArrayList<BucketPendingEmis>();
			List<Object[]> thirtyApplication = loanEmiCardRepo.getNpaBucketEmis(start, loanId);

			for (Object[] obj : thirtyApplication) {
				BucketPendingEmis bucketPendingEmis = new BucketPendingEmis();

				bucketPendingEmis.setEmiAmount((double) obj[0]);
				bucketPendingEmis.setEmiDueOn(obj[1].toString());
				bucketPendingEmis.setLoanId((int) obj[2]);
				bucketPendingEmis.setDaysDiff((int) obj[3]);
				bucketPendingEmis.setEmiNumber((int) obj[4]);
				currentMoanthLoanResponse.add(bucketPendingEmis);

			}
			SearchResultsDto<BucketPendingEmis> searchResultsDto = new SearchResultsDto<BucketPendingEmis>();
			searchResultsDto.setResults(currentMoanthLoanResponse);
			return searchResultsDto;
		}
	}

	@Override
	public SearchResultsDto<LoanResponseDto> pendingEmisApplicationLevel(int year, int month, String type, int pageNo,
			int pageSize) {
		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}

		if (type.equalsIgnoreCase("Current")) {
			List<LoanResponseDto> currentMoantLoanResponse = new ArrayList<LoanResponseDto>();
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, 2020);
			calendar.set(Calendar.MONTH, month - 1);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(5));
			Date monthStart = calendar.getTime();

			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			Date monthEnd = calendar.getTime();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getCurrentMonthEmisBasedOnApplication(monthStart, monthEnd,
					pageNo, pageSize);

			Integer totalCount = oxyLoanRepo.getCurrentMonthCount(monthStart, monthEnd);

			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setApplicationNumber(obj[0] == null ? "" : obj[0].toString());
				loanResponseDto.setBorrowerUserId(Integer.parseInt(obj[1] == null ? "0" : obj[1].toString()));
				loanResponseDto.setCity(obj[2] == null ? "" : obj[2].toString());
				loanResponseDto.setEmail(obj[3] == null ? "" : obj[3].toString());
				loanResponseDto.setMobileNumber(obj[4] == null ? "" : obj[4].toString());
				String firstName = obj[5] == null ? "" : obj[5].toString();
				String lastName = obj[6] == null ? "" : obj[6].toString();
				loanResponseDto.setBorrowerName(firstName + " " + lastName);

				String borrowerParentRequestId = obj[0] == null ? "" : obj[0].toString();
				String[] parts = borrowerParentRequestId.split("APBR");
				String part1 = parts[0];
				String part2 = parts[1];
				int parentRequestId = Integer.parseInt(part2);
				double disburmentAmount = 0.0;
				List<OxyLoan> oxyLoans = oxyLoanRepo.findByBorrowerParentRequestId(parentRequestId);
				for (OxyLoan loan : oxyLoans) {
					if (loan.getLoanStatus().toString().equals(LoanStatus.Active.toString())) {
						disburmentAmount = disburmentAmount + loan.getDisbursmentAmount();
					}
				}
				loanResponseDto.setDisburmentAmount(disburmentAmount);

				currentMoantLoanResponse.add(loanResponseDto);

			}

			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(currentMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;

		} else if (type.equalsIgnoreCase("feature")) {

			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, 2020);
			calendar.set(Calendar.MONTH, month - 1);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			Date monthStart = calendar.getTime();
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getFurtherMonthEmisBasedOnApplication(monthStart, pageNo,
					pageSize);
			Integer totalCount = oxyLoanRepo.getFurtherMonthCount(monthStart);
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setApplicationNumber(obj[0] == null ? "" : obj[0].toString());
				loanResponseDto.setBorrowerUserId(Integer.parseInt(obj[1] == null ? "0" : obj[1].toString()));
				loanResponseDto.setCity(obj[2] == null ? "" : obj[2].toString());
				loanResponseDto.setEmail(obj[3] == null ? "" : obj[3].toString());
				loanResponseDto.setMobileNumber(obj[4] == null ? "" : obj[4].toString());
				String firstName = obj[5] == null ? "" : obj[5].toString();
				String lastName = obj[6] == null ? "" : obj[6].toString();
				loanResponseDto.setBorrowerName(firstName + " " + lastName);
				String borrowerParentRequestId = obj[0] == null ? "" : obj[0].toString();
				String[] parts = borrowerParentRequestId.split("APBR");
				String part1 = parts[0];
				String part2 = parts[1];
				int parentRequestId = Integer.parseInt(part2);
				double disburmentAmount = 0.0;
				List<OxyLoan> oxyLoans = oxyLoanRepo.findByBorrowerParentRequestId(parentRequestId);
				for (OxyLoan loan : oxyLoans) {
					if (loan.getLoanStatus().toString().equals(LoanStatus.Active.toString())) {
						disburmentAmount = disburmentAmount + loan.getDisbursmentAmount();
					}
				}
				loanResponseDto.setDisburmentAmount(disburmentAmount);

				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;

		} else {
			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();
			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> currentMonthPending = oxyLoanRepo.getPendingBasedOnApplication(pageNo, pageSize);
			Integer totalCount = oxyLoanRepo.getPendingCount();
			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setApplicationNumber(obj[0] == null ? "" : obj[0].toString());
				loanResponseDto.setBorrowerUserId(Integer.parseInt(obj[1] == null ? "0" : obj[1].toString()));
				loanResponseDto.setCity(obj[2] == null ? "" : obj[2].toString());
				loanResponseDto.setEmail(obj[3] == null ? "" : obj[3].toString());
				loanResponseDto.setMobileNumber(obj[4] == null ? "" : obj[4].toString());
				String firstName = obj[5] == null ? "" : obj[5].toString();
				String lastName = obj[6] == null ? "" : obj[6].toString();
				loanResponseDto.setBorrowerName(firstName + " " + lastName);
				String borrowerParentRequestId = obj[0] == null ? "" : obj[0].toString();
				String[] parts = borrowerParentRequestId.split("APBR");
				String part1 = parts[0];
				String part2 = parts[1];
				int parentRequestId = Integer.parseInt(part2);
				double disburmentAmount = 0.0;
				List<OxyLoan> oxyLoans = oxyLoanRepo.findByBorrowerParentRequestId(parentRequestId);
				for (OxyLoan loan : oxyLoans) {
					if (loan.getLoanStatus().toString().equals(LoanStatus.Active.toString())) {
						disburmentAmount = disburmentAmount + loan.getDisbursmentAmount();
					}
				}
				loanResponseDto.setDisburmentAmount(disburmentAmount);

				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);
			searchResultsDto.setTotalCount(totalCount);
			searchResultsDto.setPageCount(pageSize);
			searchResultsDto.setPageNo(pageNo);
			return searchResultsDto;
		}

	}

	@Override
	public LoanResponseDto SearchByParentRequestId(String borrowerParentRequestId) {
		List<Object[]> loanRequest = loanRequestRepo.searchByBorrowerParentRequestId(borrowerParentRequestId);
		if (loanRequest.size() == 0) {
			throw new OperationNotAllowedException("user not exits with this parentApplication",
					ErrorCodes.ENITITY_NOT_FOUND);
		}
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		if (loanRequest != null && !loanRequest.isEmpty()) {
			Iterator it = loanRequest.iterator();
			while (it.hasNext()) {
				Object e[] = (Object[]) it.next();
				loanResponseDto.setApplicationNumber(e[0] == null ? " " : e[0].toString());
				loanResponseDto.setBorrowerUserId(Integer.parseInt(e[1] == null ? "0" : e[1].toString()));
				loanResponseDto.setEmail(e[2] == null ? "" : e[2].toString());
				loanResponseDto.setMobileNumber(e[3] == null ? "" : e[3].toString());
				loanResponseDto.setCity(e[4] == null ? "" : e[4].toString());
				String firstName = e[5] == null ? "" : e[5].toString();
				String lastName = e[6] == null ? "" : e[6].toString();
				loanResponseDto.setBorrowerName(firstName + " " + lastName);
				String borrowerParentRequestId1 = (e[0] == null ? "" : e[0].toString());
				String[] parts = borrowerParentRequestId1.split("APBR");
				String part1 = parts[0];
				String part2 = parts[1];
				int parentRequestId = Integer.parseInt(part2);
				double disburmentAmount = 0.0;
				List<OxyLoan> oxyLoans = oxyLoanRepo.findByBorrowerParentRequestId(parentRequestId);
				for (OxyLoan loan : oxyLoans) {
					if (loan.getLoanStatus().toString().equals(LoanStatus.Active.toString())) {
						disburmentAmount = disburmentAmount + loan.getDisbursmentAmount();
					}
				}
				loanResponseDto.setDisburmentAmount(disburmentAmount);
			}

		}
		return loanResponseDto;

	}

	@Override
	public LoanResponseDto SearchByBorrowerId(int borrowerId) {
		List<Object[]> loanRequest = loanRequestRepo.searchByBorrowerId(borrowerId);
		if (loanRequest.size() == 0) {
			throw new OperationNotAllowedException("user not exits with this borrowerId", ErrorCodes.ENITITY_NOT_FOUND);
		}
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		if (loanRequest != null && !loanRequest.isEmpty()) {
			Iterator it = loanRequest.iterator();
			while (it.hasNext()) {
				Object e[] = (Object[]) it.next();
				loanResponseDto.setApplicationNumber(e[0] == null ? " " : e[0].toString());
				loanResponseDto.setBorrowerUserId(Integer.parseInt(e[1] == null ? "0" : e[1].toString()));
				loanResponseDto.setEmail(e[2] == null ? "" : e[2].toString());
				loanResponseDto.setMobileNumber(e[3] == null ? "" : e[3].toString());
				loanResponseDto.setCity(e[4] == null ? "" : e[4].toString());
				String firstName = e[5] == null ? "" : e[5].toString();
				String lastName = e[6] == null ? "" : e[6].toString();
				loanResponseDto.setBorrowerName(firstName + " " + lastName);
				String borrowerParentRequestId1 = (e[0] == null ? "" : e[0].toString());
				String[] parts = borrowerParentRequestId1.split("APBR");
				String part1 = parts[0];
				String part2 = parts[1];
				int parentRequestId = Integer.parseInt(part2);
				double disburmentAmount = 0.0;
				List<OxyLoan> oxyLoans = oxyLoanRepo.findByBorrowerParentRequestId(parentRequestId);
				for (OxyLoan loan : oxyLoans) {
					if (loan.getLoanStatus().toString().equals(LoanStatus.Active.toString())) {
						disburmentAmount = disburmentAmount + loan.getDisbursmentAmount();
					}
				}
				loanResponseDto.setDisburmentAmount(disburmentAmount);
			}

		}
		return loanResponseDto;

	}

	@Override
	public SearchResultsDto<LoanResponseDto> pendingEmisBasedOnLoanLevel(int year, int month, String type, int loanId) {

		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}

		if (type.equalsIgnoreCase("Current")) {
			List<LoanResponseDto> currentMoantLoanResponse = new ArrayList<LoanResponseDto>();
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, 2020);
			calendar.set(Calendar.MONTH, month - 1);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(5));
			Date monthStart = calendar.getTime();

			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			Date monthEnd = calendar.getTime();

			List<Object[]> currentMonthPending = oxyLoanRepo.getCurrentMonthEmisByLoanId(monthStart, monthEnd, loanId);

			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();
				loanResponseDto.setEmiNumber(Integer.parseInt(obj[0] == null ? "0" : obj[0].toString()));
				loanResponseDto.setEmiAmount(Double.parseDouble(obj[1] == null ? "0" : obj[1].toString()));
				loanResponseDto.setEmiDueOnDate((obj[2] == null ? "" : obj[2].toString()));
				loanResponseDto.setPenality(Double.parseDouble(obj[3] == null ? "0" : obj[3].toString()));

				currentMoantLoanResponse.add(loanResponseDto);

			}

			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(currentMoantLoanResponse);

			return searchResultsDto;

		} else if (type.equalsIgnoreCase("feature")) {

			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, 2020);
			calendar.set(Calendar.MONTH, month - 1);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			Date monthStart = calendar.getTime();
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

			List<Object[]> currentMonthPending = oxyLoanRepo.getFurtherMonthEmisByLoanId(monthStart, loanId);

			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();

				loanResponseDto.setEmiNumber(Integer.parseInt(obj[0] == null ? "0" : obj[0].toString()));
				loanResponseDto.setEmiAmount(Double.parseDouble(obj[1] == null ? "0" : obj[1].toString()));
				loanResponseDto.setEmiDueOnDate((obj[2] == null ? "" : obj[2].toString()));
				loanResponseDto.setPenality(Double.parseDouble(obj[3] == null ? "0" : obj[3].toString()));

				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);

			return searchResultsDto;

		} else {
			List<LoanResponseDto> featureMoantLoanResponse = new ArrayList<LoanResponseDto>();

			List<Object[]> currentMonthPending = oxyLoanRepo.getPendingEmisByLoanId(loanId);

			for (Object[] obj : currentMonthPending) {
				LoanResponseDto loanResponseDto = new LoanResponseDto();

				loanResponseDto.setEmiNumber(Integer.parseInt(obj[0] == null ? "0" : obj[0].toString()));
				loanResponseDto.setEmiAmount(Double.parseDouble(obj[1] == null ? "0" : obj[1].toString()));
				loanResponseDto.setEmiDueOnDate((obj[2] == null ? "" : obj[2].toString()));
				loanResponseDto.setPenality(Double.parseDouble(obj[3] == null ? "0" : obj[3].toString()));

				featureMoantLoanResponse.add(loanResponseDto);
			}
			SearchResultsDto<LoanResponseDto> searchResultsDto = new SearchResultsDto<LoanResponseDto>();
			searchResultsDto.setResults(featureMoantLoanResponse);

			return searchResultsDto;
		}

	}

	@Override
	public String makingPenalityValueZero(int loanId, int emiNumber) {
		LoanEmiCard loanEmiCard = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId, emiNumber);
		loanEmiCard.setPenality(0.0);
		loanEmiCard.setRemainingEmiAmount(0.0);
		loanEmiCardRepo.save(loanEmiCard);
		return "successfullyUpdated";

	}

	@Override
	public String updatingEmiPaidOnDate(int loanId, int emiNumber, LoanEmiCard loanEmiCard) {
		LoanEmiCard loanEmiCardDetails = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId, emiNumber);
		if (loanEmiCardDetails.getPenality() == 0.0) {
			loanEmiCardDetails.setStatus(com.oxyloans.entity.loan.LoanEmiCard.Status.COMPLETED);
			if (loanEmiCard == null) {
				loanEmiCardDetails.setEmiPaidOn(new Date());

			} else {
				Date date = loanEmiCard.getEmiPaidOn();
				Timestamp ts = new Timestamp(date.getTime());
				loanEmiCardDetails.setEmiPaidOn(ts);
			}
			loanEmiCardRepo.save(loanEmiCardDetails);
		} else {
			throw new OperationNotAllowedException("please check the remainingEmiAmount are penality values",
					ErrorCodes.ENITITY_NOT_FOUND);
		}
		return "successfullyUpdated";

	}

	@Override
	public String deleteExtraPaidEmiDetails(int loanId, int emiNumber) {
		LoanEmiCard loanEmiCardDetails = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId, emiNumber);
		if (loanEmiCardDetails != null) {
			if (loanEmiCardDetails.getExcessOfEmiAmount() == 0.0) {
				loanEmiCardDetails.setEmiPaidOn(null);
				loanEmiCardDetails.setStatus(com.oxyloans.entity.loan.LoanEmiCard.Status.INITIATED);
				loanEmiCardDetails.setModeOfPayment(null);
				loanEmiCardDetails.setTransactionNumber(null);
				loanEmiCardDetails.setRemainingEmiAmount(0.0);
				loanEmiCardRepo.save(loanEmiCardDetails);
			} else {
				throw new OperationNotAllowedException("User excess amount is greater than zero",
						ErrorCodes.ENITITY_NOT_FOUND);
			}
		} else {
			throw new OperationNotAllowedException("please check loanId and EmiNumber", ErrorCodes.ENITITY_NOT_FOUND);
		}
		return "successfullyUpdated";

	}

	@Override
	public List<LoanEmiCardResponseDto> getApplicationLevelPenalityWithEmi(int borrowerApplicationId) {
		List<Object[]> objects = oxyLoanRepo.getSumOfEmiAndInterest(borrowerApplicationId);
		List<LoanEmiCardResponseDto> loanEmiCardApplicationLevel = new ArrayList<LoanEmiCardResponseDto>();

		double applicationLevelDue = 0.0;
		int count = 0;
		int size = objects.size();
		if (objects != null && !objects.isEmpty()) {
			Iterator it = objects.iterator();
			while (it.hasNext()) {
				Object e[] = (Object[]) it.next();
				count = count + 1;
				LoanEmiCardResponseDto loanEmiCardResponseDto = new LoanEmiCardResponseDto();
				int emiNumber = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
				loanEmiCardResponseDto.setEmiNumber(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
				double dueAmountForEmiAndInterest = 0.0;
				double dueAmountIncludingPenality = 0.0;
				double emiAmountPaid = 0.0;
				double interestAmountPaid = 0.0;
				double halfInterestAmountPaid = 0.0;
				double totalAmountPaid = 0.0;
				double excessAmount = 0.0;
				String emiDueOnDate = null;
				String status = null;
				String emiPaidOn = null;
				int differenceInDays = 0;

				List<Integer> listLoansIds = oxyLoanRepo.getloansForAnApplication(borrowerApplicationId);
				double interestAmount = 0.0;
				int countEmis = 0;
				for (int i = 0; i < listLoansIds.size(); i++) {
					int loanId = listLoansIds.get(i);
					countEmis = countEmis + 1;
					LoanEmiCard loanEmiCard = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId, emiNumber);
					emiDueOnDate = expectedDateFormat.format(loanEmiCard.getEmiDueOn());
					status = loanEmiCard.getStatus().toString();
					if (loanEmiCard.getEmiPaidOn() != null) {
						emiPaidOn = expectedDateFormat.format(loanEmiCard.getEmiPaidOn());
					}
					differenceInDays = (loanEmiCard.getDifferenceInDays() == null ? 0
							: loanEmiCard.getDifferenceInDays());

					InterestDetails interestDetails = interestDetailsRepo.getInterestAmount(loanId, emiNumber);
					if (interestDetails != null) {
						interestAmount = interestAmount + interestDetails.getInterest();

						if (interestDetails.getInterestPaid() == null) {
							halfInterestAmountPaid = halfInterestAmountPaid + loanEmiCard.getEmiLateFeeCharges();

						}
						if (countEmis == listLoansIds.size()) {
							if (interestAmountPaid <= 0) {
								double interestAmountToParticularEmi = interestAmount - halfInterestAmountPaid;
								if (interestAmountToParticularEmi == 0) {
									interestAmountPaid = 0.0;
								} else {
									interestAmountPaid = interestAmountToParticularEmi;
								}
							} else {
								interestAmountPaid = interestAmount;
							}
						}
					}

					if (loanEmiCard.getEmiPaidOn() == null) {
						if (loanEmiCard.getStatus() == com.oxyloans.entity.loan.LoanEmiCard.Status.COMPLETED) {
							dueAmountForEmiAndInterest = dueAmountForEmiAndInterest
									+ loanEmiCard.getEmiLateFeeCharges();
							emiAmountPaid = emiAmountPaid + loanEmiCard.getEmiAmount();
						} else {
							if (loanEmiCard.getStatus() == com.oxyloans.entity.loan.LoanEmiCard.Status.INITIATED) {
								dueAmountForEmiAndInterest = dueAmountForEmiAndInterest
										+ loanEmiCard.getEmiLateFeeCharges() + loanEmiCard.getEmiAmount();
								emiAmountPaid = emiAmountPaid + 0;
							} else {
								dueAmountForEmiAndInterest = dueAmountForEmiAndInterest
										+ loanEmiCard.getEmiLateFeeCharges() + loanEmiCard.getRemainingEmiAmount();
								emiAmountPaid = emiAmountPaid + loanEmiCard.getEmiAmount()
										- loanEmiCard.getRemainingEmiAmount();
							}
						}
					} else {
						emiAmountPaid = emiAmountPaid + loanEmiCard.getEmiAmount();
					}
					excessAmount = excessAmount + loanEmiCard.getExcessOfEmiAmount();
				}

				loanEmiCardResponseDto.setEmiAmount(Double.parseDouble(e[1] == null ? "0" : e[1].toString()));
				loanEmiCardResponseDto.setInterestAmount(interestAmount);

				PenaltyDetails penaltyDetails = penaltyDetailsRepo.findByEmiNumberAndBorrowerParentRequestid(emiNumber,
						borrowerApplicationId);
				if (penaltyDetails != null) {
					if (penaltyDetails.getPenalty() > 0 && penaltyDetails.getPenaltyPaidOn() == null) {
						if (penaltyDetails.getPenaltyDue() > 0) {
							dueAmountIncludingPenality = dueAmountForEmiAndInterest + penaltyDetails.getPenaltyDue();
						} else {
							dueAmountIncludingPenality = dueAmountForEmiAndInterest + penaltyDetails.getPenalty();
						}

					} else {
						dueAmountIncludingPenality = dueAmountForEmiAndInterest;
					}

					totalAmountPaid = emiAmountPaid + interestAmountPaid;
					if (penaltyDetails.getPenalty() > 0 && penaltyDetails.getPenaltyPaidOn() != null) {
						totalAmountPaid = totalAmountPaid + penaltyDetails.getPenalty();

					}
					double emiNumber1 = Double.parseDouble(e[1] == null ? "0" : e[1].toString());

					double penalty1 = penaltyDetails.getPenalty();
					if (penaltyDetails.getPenaltyPaidOn() != null) {

						loanEmiCardResponseDto
								.setEmiPaidOn(expectedDateFormat.format(penaltyDetails.getPenaltyPaidOn()));
					}

					loanEmiCardResponseDto
							.setApplicationLevelTotalAmount(Math.round(emiNumber1 + interestAmount + penalty1));

					loanEmiCardResponseDto.setPenalty(Math.round(penaltyDetails.getPenalty()));
					loanEmiCardResponseDto.setPenaltyPaid(Math.round(penaltyDetails.getPenaltyPaid()));
					loanEmiCardResponseDto.setPenaltyDue(penaltyDetails.getPenaltyDue());
				}
				loanEmiCardResponseDto.setDueAmountWithoutPenality(Math.round(dueAmountForEmiAndInterest));
				loanEmiCardResponseDto.setDueAmountWithPenality(Math.round(dueAmountIncludingPenality));
				loanEmiCardResponseDto.setTotalAmountPaid(Math.round(totalAmountPaid));
				loanEmiCardResponseDto.setEmiAmountPaid(Math.round(emiAmountPaid));
				loanEmiCardResponseDto.setInterestAmountPaid(Math.round(interestAmountPaid));

				loanEmiCardResponseDto.setExcessAmount(Math.round(excessAmount));
				loanEmiCardResponseDto.setStatus(status);
				loanEmiCardResponseDto.setEmiDueOn(emiDueOnDate);
				loanEmiCardResponseDto.setEmiPaidOn(emiPaidOn);
				loanEmiCardResponseDto.setDifferenceInDays(differenceInDays);

				applicationLevelDue = applicationLevelDue + dueAmountIncludingPenality;

				if (count == size) {
					loanEmiCardResponseDto.setTotalApplicationLevelDue(applicationLevelDue);
				} else {
					loanEmiCardResponseDto.setTotalApplicationLevelDue(0.0);
				}

				loanEmiCardApplicationLevel.add(loanEmiCardResponseDto);

			}

		}

		return loanEmiCardApplicationLevel;
	}

	@Override
	public LoanEmiCardResponseDto calculatePendingPenaltyTillDate(int borrowerApplicationId) {
		List<Integer> listOfEmiNumbers = penaltyDetailsRepo.getpenaltyTilldateEmiNumbers(borrowerApplicationId);
		double penaltyDue = 0.0;
		LoanEmiCardResponseDto loanEmiCardResponseDto = new LoanEmiCardResponseDto();
		if (listOfEmiNumbers != null && !listOfEmiNumbers.isEmpty()) {
			for (int i = 0; i < listOfEmiNumbers.size(); i++) {

				int emiNumberForApplication = listOfEmiNumbers.get(i);
				PenaltyDetails penaltyDetailsForEmiNumber = penaltyDetailsRepo
						.findByEmiNumberAndBorrowerParentRequestid(emiNumberForApplication, borrowerApplicationId);
				if (penaltyDetailsForEmiNumber != null) {
					if (penaltyDetailsForEmiNumber.getPenaltyDue() > 0) {
						penaltyDue = penaltyDue + penaltyDetailsForEmiNumber.getPenaltyDue();
					} else {
						penaltyDue = penaltyDue + penaltyDetailsForEmiNumber.getPenalty();
					}
				}

			}

			loanEmiCardResponseDto.setPenaltyDue(penaltyDue);

		}
		List<LoanEmiCardResponseDto> applicationDetails = getApplicationLevelPenalityWithEmi(borrowerApplicationId);

		int sizeValue = applicationDetails.size();
		double applicationDue = 0.0;
		if (applicationDetails != null && !applicationDetails.isEmpty()) {
			for (LoanEmiCardResponseDto applicationinfo1 : applicationDetails) {
				if (applicationinfo1.getEmiNumber() == sizeValue) {
					applicationDue = applicationinfo1.getTotalApplicationLevelDue();
				}
			}
		}
		Double disbursedAmount = oxyLoanRepo.getDisburmentAmount(borrowerApplicationId);
		if (disbursedAmount != null) {
			loanEmiCardResponseDto.setApplicationLevelDisbursedAmount(disbursedAmount);
		}
		int borrowerUserId = borrowerApplicationId;
		List<Integer> getLoanIdsForApplication = oxyLoanRepo.getloansForAnApplication(borrowerUserId);
		String emiStartDate = null;
		double doubleAmount = 0.0;
		double sumOfEmiAmount = 0.0;
		double sumOfPrincipalAmount = 0.0;
		double totalPenaltyDue = 0.0;
		for (int i = 0; i < getLoanIdsForApplication.size(); i++) {
			int count = 0;
			if (i == 0) {
				int loanId = getLoanIdsForApplication.get(i);
				OxyLoan oxyloan = oxyLoanRepo.findById(loanId).get();

				if (oxyloan != null) {
					loanEmiCardResponseDto.setRateOfInterest(oxyloan.getRateOfInterest());
					loanEmiCardResponseDto.setDuration(oxyloan.getDuration());
					loanEmiCardResponseDto
							.setDisbursedDate(expectedDateFormat.format(oxyloan.getBorrowerDisbursedDate()));

					List<Integer> listOfEmis = loanEmiCardRepo.getDueInformationEmiNumbers(loanId);
					for (int i1 = 0; i1 < listOfEmis.size(); i1++) {
						int emiNumber = listOfEmis.get(i);
						int borrowerParentRequestid = borrowerApplicationId;
						PenaltyDetails penalityDetails = penaltyDetailsRepo
								.findByEmiNumberAndBorrowerParentRequestid(emiNumber, borrowerParentRequestid);
						if (penalityDetails.getPenaltyDue() > 0) {

							totalPenaltyDue = totalPenaltyDue + penalityDetails.getPenaltyDue();
						} else {

							totalPenaltyDue = totalPenaltyDue + penalityDetails.getPenalty();
						}

					}
				}
				Integer emisToBePaidTillDate = loanEmiCardRepo.getEmisToBePaid(loanId);
				Integer emisPaid = loanEmiCardRepo.getPaidEmisCount(loanId);
				Integer unpaidEmis = loanEmiCardRepo.getUnpaidEmisCount(loanId);
				Integer furtherEmis = loanEmiCardRepo.getFurtherEmisCount(loanId);
				Integer emisPendingTillDate = emisToBePaidTillDate - emisPaid;

				loanEmiCardResponseDto.setEmisToBePaidTillDate(emisToBePaidTillDate);
				loanEmiCardResponseDto.setEmisPaid(emisPaid);
				loanEmiCardResponseDto.setUnpaidEmis(unpaidEmis);
				loanEmiCardResponseDto.setFurtherEmis(furtherEmis);
				if (emisPendingTillDate > 0) {
					loanEmiCardResponseDto.setEmisPendingTillDate(emisPendingTillDate);
				}

				List<LoanEmiCard> loanEmiCardDetails = loanEmiCardRepo.findByLoanIdOrderByEmiNumber(loanId);
				if (loanEmiCardDetails != null && !loanEmiCardDetails.isEmpty()) {
					for (LoanEmiCard loanEmiCard : loanEmiCardDetails) {
						if (count == 0) {
							emiStartDate = expectedDateFormat.format(loanEmiCard.getEmiDueOn());
							count = count + 1;
						}
					}
				}

			}

			int loanId = getLoanIdsForApplication.get(i);

			List<Object[]> listOfDueInformationLoanIds = loanEmiCardRepo.getDueInformation(loanId);
			if (listOfDueInformationLoanIds != null && !listOfDueInformationLoanIds.isEmpty()) {
				Iterator it = listOfDueInformationLoanIds.iterator();
				while (it.hasNext()) {
					Object e[] = (Object[]) it.next();
					int emiNumber = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
					InterestDetails interestDetails = interestDetailsRepo.findByloanIdAndEmiNumber(loanId, emiNumber);
					double emiAmount = Double.parseDouble(e[2] == null ? "0" : e[2].toString());

					String status = (e[3] == null ? "0" : e[3].toString());
					if (status.equals("INITIATED")) {
						if (interestDetails != null) {
							doubleAmount = doubleAmount + emiAmount + interestDetails.getInterest();

						} else {
							doubleAmount = doubleAmount + emiAmount;
						}
					} else {
						if (status.equals("INPROCESS")) {
							Double remainingEmiAmount = Double.parseDouble(e[4] == null ? "0" : e[4].toString());
							if (interestDetails != null) {
								doubleAmount = doubleAmount + remainingEmiAmount + interestDetails.getInterest();

							} else {
								doubleAmount = doubleAmount + remainingEmiAmount;
							}
						} else {
							if (interestDetails != null) {
								Double interestAmount = Double.parseDouble(e[5] == null ? "0" : e[5].toString());

								doubleAmount = doubleAmount + interestAmount;

							}
						}
					}
				}
			}
			List<Object[]> getEmiAmountAndPrincipalAmount = loanEmiCardRepo.getEmiAmountAndprincipalAmount(loanId);
			if (getEmiAmountAndPrincipalAmount != null && !getEmiAmountAndPrincipalAmount.isEmpty()) {
				Iterator it = getEmiAmountAndPrincipalAmount.iterator();
				while (it.hasNext()) {
					Object e1[] = (Object[]) it.next();
					Double principalAmount = Double.parseDouble(e1[0] == null ? "0" : e1[0].toString());
					Double emiAmount = Double.parseDouble(e1[1] == null ? "0" : e1[1].toString());
					sumOfPrincipalAmount = sumOfPrincipalAmount + principalAmount;
					sumOfEmiAmount = sumOfEmiAmount + emiAmount;
				}
			}
		}
		doubleAmount = doubleAmount + totalPenaltyDue;
		loanEmiCardResponseDto.setDueAmount(doubleAmount);
		loanEmiCardResponseDto.setPreclose(doubleAmount + sumOfPrincipalAmount);
		loanEmiCardResponseDto.setOutStandingAmount(doubleAmount + sumOfEmiAmount);
		double firstEmiAmount = oxyLoanRepo.getFirstEmiAmountForApplication(borrowerApplicationId);
		double secondEmiAmount = oxyLoanRepo.getSecondEmiAmountForApplication(borrowerApplicationId);
		loanEmiCardResponseDto.setFirstEmiAmount(firstEmiAmount);
		loanEmiCardResponseDto.setSecondEmiAmount(secondEmiAmount);
		loanEmiCardResponseDto.setEmiStartDate(emiStartDate);
		loanEmiCardResponseDto.setApplicationId(borrowerApplicationId);
		loanEmiCardResponseDto.setTotalApplicationLevelDue(applicationDue);
		loanEmiCardResponseDto
				.setGetApplicationLevelPenalityWithEmi(getApplicationLevelPenalityWithEmi(borrowerApplicationId));

		return loanEmiCardResponseDto;
	}

	@Override
	public int countValue(int loanId) {
		List<LoanEmiCard> loanEmiCard = loanEmiCardRepo.findByLoanId(loanId);
		OxyLoan oxyLoans = oxyLoanRepo.findByLoanId("LN" + loanId);
		int count = 0;
		if (oxyLoans != null) {
			int duration = oxyLoans.getDuration();
			for (LoanEmiCard emiDetails : loanEmiCard) {
				if (emiDetails.getEmiPaidOn() != null) {
					count = count + 1;
				}
			}
			if (count == duration) {
				oxyLoans.setLoanStatus(LoanStatus.Closed);
				oxyLoanRepo.save(oxyLoans);
			}
		}
		return count;
	}

	private LoanResponseDto getLenderStatement(Integer lenderParentRequestId) {
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		UserResponse lenderUser = null;
		List<OxyLoan> loans = oxyLoanRepo.findByLenderParentRequestId(lenderParentRequestId);
		List<LoanEmiCardResponseDto> emiCards = loans.stream()
				.filter(oxyLoan -> oxyLoan.getBorrowerDisbursedDate() != null)
				.flatMap(oxyLoan -> oxyLoan.getLoanEmiCards().stream())
				.filter(loanemicard -> loanemicard.getEmiPaidOn() != null).map(card -> {

					OxyLoan loan = oxyLoanRepo.findById(card.getLoanId()).get();

					User user = userRepo.findById(loan.getBorrowerUserId()).get();
					PersonalDetails details = user.getPersonalDetails();

					LoanEmiCardResponseDto responseDto = new LoanEmiCardResponseDto();

					responseDto.setMobileNumber(user.getMobileNumber());
					responseDto.setBorrowerName(details.getFirstName() + details.getLastName());
					responseDto.setBorrowerId(user.getId());

					responseDto.setEmiAmount((double) Math.round(card.getEmiAmount()));
					responseDto.setEmiDueOn(this.expectedDateFormat.format(card.getEmiDueOn()));
					responseDto.setEmiPaidOn(
							card.getEmiPaidOn() == null ? null : this.expectedDateFormat.format(card.getEmiPaidOn()));
					responseDto.setEmiInterstAmount((double) Math.round(card.getEmiInterstAmount()));
					responseDto.setEmiNumber(card.getEmiNumber());
					responseDto.setEmiPrincipalAmount((double) Math.round(card.getEmiPrincipalAmount()));
					responseDto.setId(card.getId());
					responseDto.setExcessOfEmiAmount((double) Math.round(card.getExcessOfEmiAmount()));
					responseDto.setModeOfPayment(card.getModeOfPayment());

					responseDto.setLoanPaymenHistory(getPaymentHistory(card.getId()));

					responseDto.setLoanId(card.getLoanId());
					responseDto.setEmiStatus(card.getEmiPaidOn() != null);
					responseDto.setPaidOnTime(card.isPaidOnTime());
					responseDto
							.setDifferenceInDays(card.getDifferenceInDays() == null ? 0 : card.getDifferenceInDays());
					responseDto.setInterestAmount(card.getEmiLateFeeCharges());
					responseDto.setPenalty(card.getPenality());
					responseDto.setDueAmount(card.getRemainingPenaltyAndRemainingEmi());
					if (card.getEmiPaidOn() != null) {
						responseDto.setEmiStatusBasedOnPayment("EMI Received");
					} else {
						Calendar cal = Calendar.getInstance();
						int month = cal.get(Calendar.MONTH);
						month = month + 1;
						int year = cal.get(Calendar.YEAR);
						int day = cal.get(Calendar.DAY_OF_MONTH);
						Date date = cal.getTime();
						String dt = expectedDateFormat.format(date);
						Date date1 = new Date();
						Date date2 = new Date();
						try {
							date1 = expectedDateFormat.parse(responseDto.getEmiDueOn());
							date2 = expectedDateFormat.parse(dt);
						} catch (Exception e) {
							logger.info("Exception", e);
						}
						String emiDueOn = responseDto.getEmiDueOn();
						if (emiDueOn != null) {
							String arr[] = emiDueOn.split("/");
							String dueMonth = arr[1];
							String dueYear = arr[2];
							if (month == Integer.parseInt(dueMonth) && year == Integer.parseInt(dueYear)) {
								if (day >= 0 && day <= 20) {
									responseDto.setEmiStatusBasedOnPayment("EMI Process Initiated");
								} else {
									responseDto.setEmiStatusBasedOnPayment("EMI Pending");
								}
							} else if (date1.compareTo(date2) < 0) {
								responseDto.setEmiStatusBasedOnPayment("EMI Pending");
							} else if (date1.compareTo(date2) > 0) {
								responseDto.setEmiStatusBasedOnPayment("Future EMI");
							}

						}

					}
					responseDto.setRemainingEmiAmount((double) Math.round(card.getRemainingEmiAmount()));
					responseDto.setRemainingPenaltyAndRemainingEmi(
							(double) Math.round(card.getRemainingPenaltyAndRemainingEmi()));
					responseDto.setPenalty((double) Math.round(card.getPenality()));

					return responseDto;
				}).collect(Collectors.toList());

		LoanRequest loanRequest = oxyLoanRequestRepo.findById(lenderParentRequestId).get();
		if (loanRequest != null) {
			lenderUser = new UserResponse();
			Double walletAmount = lenderOxyWalletNativeRepo.lenderWalletCreditAmount(loanRequest.getUserId());
			User user = userRepo.findById(loanRequest.getUserId()).get();
			BankDetails bankDetails = user.getBankDetails();
			BankDetailsResponseDto bankDetailsResponseDto = new BankDetailsResponseDto();
			bankDetailsResponseDto.setAccountNumber(bankDetails.getAccountNumber());
			bankDetailsResponseDto.setAccountType(bankDetails.getAccountType());
			bankDetailsResponseDto.setAddress(bankDetails.getAddress());
			bankDetailsResponseDto.setBankName(bankDetails.getBankName());
			bankDetailsResponseDto.setBranchName(bankDetails.getBranchName());
			bankDetailsResponseDto.setIfscCode(bankDetails.getIfscCode());
			bankDetailsResponseDto.setUserId(bankDetails.getUserId());
			PersonalDetails personalDetails = user.getPersonalDetails();
			lenderUser.setId(user.getId());
			lenderUser.setName(personalDetails.getFirstName() + personalDetails.getLastName());
			lenderUser.setAmountForGivenInterest(walletAmount == null ? 0.0d : walletAmount);
			lenderUser.setMobileNumber(user.getMobileNumber());
			lenderUser.setBankDetailsResponseDto(bankDetailsResponseDto);
			loanResponseDto.setLenderUser(lenderUser);
		}

		List<LoanEmiCardPaymentDetailsResponseDto> partPayEmis = loans.stream()
				.flatMap(loan -> loan.getLoanEmiCards().stream()).map(emiCard -> {

					List<LoanEmiCardPaymentDetails> loanEmiCardPaymentDetails = loanEmiCardPaymentDetailsRepo
							.findByEmiId(emiCard.getId()).stream()
							.filter(partPaymentDetails -> partPaymentDetails.getAmountPaidDate() != null
									&& emiCard.getEmiPaidOn() == null)
							.collect(Collectors.toList());

					return loanEmiCardPaymentDetails;
				}).flatMap(loanEmiCardPaymentDetail -> loanEmiCardPaymentDetail.stream()).map(PaymentDetail -> {
					LoanEmiCardPaymentDetailsResponseDto loanEmiCardPaymentDetailsResponseDto = new LoanEmiCardPaymentDetailsResponseDto();
					loanEmiCardPaymentDetailsResponseDto
							.setPartialPaymentAmount(PaymentDetail.getPartialPaymentAmount());
					loanEmiCardPaymentDetailsResponseDto
							.setAmountPaidDate(PaymentDetail.getAmountPaidDate().toString());
					OxyLoan oxyLoan = oxyLoanRepo.findById(Integer.parseInt(PaymentDetail.getLoanId())).get();
					User user = userRepo.findById(oxyLoan.getBorrowerUserId()).get();
					PersonalDetails personalDetails = user.getPersonalDetails();
					loanEmiCardPaymentDetailsResponseDto.setBorrowerId(user.getId());
					loanEmiCardPaymentDetailsResponseDto
							.setBorrowerName(personalDetails.getFirstName() + personalDetails.getLastName());
					loanEmiCardPaymentDetailsResponseDto.setMobileNumber(user.getMobileNumber());
					return loanEmiCardPaymentDetailsResponseDto;
				}).collect(Collectors.toList());

		loanResponseDto.setEmiDto(emiCards);
		loanResponseDto.setLoanEmiCardPaymentDetailsResponseDtos(partPayEmis);
		return loanResponseDto;
	}

	@Override
	public KycFileResponse lenderReport(Integer lenderParentRequestId) {

		LoanResponseDto loanResponseDto = getLenderStatement(lenderParentRequestId);
		KycFileResponse fileResponse = new KycFileResponse();
		ByteArrayOutputStream output = null;
		FileOutputStream outFile = null;

		try {

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet spreadsheet = workbook.createSheet(" lender Info ");
			Row row0 = spreadsheet.createRow(0);

			Cell cell = row0.createCell(0);
			cell.setCellValue("srsbankaccount");

			cell = row0.createCell(1);
			cell.setCellValue("lenderbankaccountnumber");

			cell = row0.createCell(2);
			cell.setCellValue("ifsccode of lender");

			cell = row0.createCell(3);
			cell.setCellValue("lendername");

			cell = row0.createCell(4);
			cell.setCellValue("amount");

			cell = row0.createCell(5);
			cell.setCellValue("partpayamount");

			cell = row0.createCell(6);
			cell.setCellValue("lenderid");

			cell = row0.createCell(7);
			cell.setCellValue("borrowername");

			cell = row0.createCell(8);
			cell.setCellValue("borrowerid");

			cell = row0.createCell(9);
			cell.setCellValue("month");

			int rowCount = 0;

			for (LoanEmiCardResponseDto loanEmiCardResponseDto : loanResponseDto.getEmiDto()) {

				Row row = spreadsheet.createRow(++rowCount);

				Cell cell1 = row.createCell(0);
				cell1.setCellValue("123srsbankaccount123");

				cell1 = row.createCell(1);
				cell1.setCellValue(loanResponseDto.getLenderUser().getBankDetailsResponseDto().getAccountNumber());

				cell1 = row.createCell(2);
				cell1.setCellValue(loanResponseDto.getLenderUser().getBankDetailsResponseDto().getIfscCode());

				cell1 = row.createCell(3);
				cell1.setCellValue(loanResponseDto.getLenderUser().getName());

				cell1 = row.createCell(4);
				cell1.setCellValue(loanEmiCardResponseDto.getEmiAmount());

				cell1 = row.createCell(6);
				cell1.setCellValue(loanResponseDto.getLenderUser().getId());

				cell1 = row.createCell(7);
				cell1.setCellValue(loanEmiCardResponseDto.getBorrowerName());

				cell1 = row.createCell(8);
				cell1.setCellValue(loanEmiCardResponseDto.getBorrowerId());

				cell1 = row.createCell(9);
				cell1.setCellValue(loanEmiCardResponseDto.getEmiPaidOn());

			}

			for (LoanEmiCardPaymentDetailsResponseDto loanEmiCardResponseDetailsResponseDto : loanResponseDto
					.getLoanEmiCardPaymentDetailsResponseDtos()) {

				Row row = spreadsheet.createRow(++rowCount);

				Cell cell1 = row.createCell(0);
				cell1.setCellValue("123srsbankaccount123");

				cell1 = row.createCell(1);
				cell1.setCellValue(loanResponseDto.getLenderUser().getBankDetailsResponseDto().getAccountNumber());

				cell1 = row.createCell(2);
				cell1.setCellValue(loanResponseDto.getLenderUser().getBankDetailsResponseDto().getIfscCode());

				cell1 = row.createCell(3);
				cell1.setCellValue(loanResponseDto.getLenderUser().getName());

				cell1 = row.createCell(5);
				cell1.setCellValue(loanEmiCardResponseDetailsResponseDto.getPartialPaymentAmount());

				cell1 = row.createCell(6);
				cell1.setCellValue(loanResponseDto.getLenderUser().getId());

				cell1 = row.createCell(7);
				cell1.setCellValue(loanEmiCardResponseDetailsResponseDto.getBorrowerName());

				cell1 = row.createCell(8);
				cell1.setCellValue(loanEmiCardResponseDetailsResponseDto.getBorrowerId());

				cell1 = row.createCell(9);
				cell1.setCellValue(loanEmiCardResponseDetailsResponseDto.getAmountPaidDate());

			}

			output = new ByteArrayOutputStream();
			workbook.write(output);
			FileRequest fileRequest = new FileRequest();
			KycFileRequest kyc = new KycFileRequest();
			InputStream fileStream = new ByteArrayInputStream(output.toByteArray());
			fileRequest.setInputStream(fileStream);
			kyc.setFileName(lenderParentRequestId + " " + LocalDate.now() + "lenderreport.xlsx");
			fileRequest.setUserId(lenderParentRequestId);
			fileRequest.setFileName(kyc.getFileName());
			fileRequest.setFileType(FileType.Agreement);
			fileRequest.setFilePrifix(FileType.Agreement.name());
			fileManagementService.putFile(fileRequest);
			FileResponse file = fileManagementService.getFile(fileRequest);
			fileResponse.setDownloadUrl(file.getDownloadUrl());
			fileResponse.setFileName(lenderParentRequestId + " " + LocalDate.now() + "lenderreport.xlsx");
			fileResponse.setKycType(KycType.LENDERREPORT);
			boolean deleted = fileManagementService.deleteFile(fileRequest);
			logger.warn("Deleted the File : " + deleted);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileResponse;
	}

	@Override
	public ApplicationResponseDto emiDetails(int parentRequestID) {

		ApplicationResponseDto applicationResponseDto = new ApplicationResponseDto();

		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}

		List<OxyLoan> loans = oxyLoanRepo.findByBorrowerParentRequestId(parentRequestID);
		double totalProfit = 0.0d;
		double disbursmentAmount = 0.0d;

		for (OxyLoan oxyLoan : loans) {
			double profit = (oxyLoan.getDisbursmentAmount() * oxyLoan.getDuration()
					* (oxyLoan.getRateOfInterest() / 12)) / 100d;
			totalProfit = +profit;
			disbursmentAmount = +oxyLoan.getDisbursmentAmount();
		}
		Calendar calendar = Calendar.getInstance();
		Double totalAmountPaid = loansByApplicationNativeRepo.getAmountPaidByApplication(parentRequestID);
		List<Object[]> totalPendingINformation = loansByApplicationNativeRepo.getAmountNotPaid(parentRequestID);
		List<Object[]> currentTotalAmountPaid = loansByApplicationNativeRepo
				.getAmountPaidByApplicationCurrentMonth(calendar.getTime(), parentRequestID);
		List<Object[]> currentTotalPendingINformation = loansByApplicationNativeRepo
				.getAmountNotPaidCurrentMonth(calendar.getTime(), parentRequestID);
		List<Object[]> startDateAndDuration = loansByApplicationNativeRepo.getMaxDurationAndstartDate(parentRequestID);
		List<Object[]> featureMonthEmiDetails = loansByApplicationNativeRepo.getAmountNotPaidfeature(calendar.getTime(),
				parentRequestID);

		applicationResponseDto.setApplicationAmountPaid(totalAmountPaid == null ? 0.0d : totalAmountPaid);

		double principulsum = 0.0d;
		double penality = 0.0d;

		for (Object[] obj : currentTotalAmountPaid) {
			applicationResponseDto.setCurrentApplicationAmountPaid(obj[0] == null ? 0.0d : (double) obj[0]);
			principulsum = obj[1] == null ? 0.0d : (double) obj[1];

		}

		for (Object[] obj : startDateAndDuration) {
			applicationResponseDto.setEmiStartDate(obj[1].toString());
			applicationResponseDto.setDuration((int) obj[0]);
		}
		// sum(emi_amount),count(emi_amount),Sum(penality)
		for (Object[] obj : currentTotalPendingINformation) {
			applicationResponseDto.setCurrentApplicationAmountNotPaid(obj[0] == null ? 0.0d : (double) obj[0]);
			applicationResponseDto.setCurrentemiNotPaid(((BigInteger) obj[1]).intValue());
			penality = obj[2] == null ? 0.0d : (double) obj[2];

		}

		for (Object[] obj : featureMonthEmiDetails) {
			applicationResponseDto.setFeatureMonthEMiAmount(obj[0] == null ? 0.0d : (double) obj[0]);
			// applicationResponseDto.setNoOfFeatureMOnth(((BigInteger) obj[1]).intValue());
			int borrowerUserId = parentRequestID;
			Integer furtherCount = 0;
			List<Integer> listOfLoans = oxyLoanRepo.getloansForAnApplication(borrowerUserId);
			for (int i = 0; i < listOfLoans.size(); i++) {
				if (i == 0) {
					int loanId = listOfLoans.get(i);
					furtherCount = loanEmiCardRepo.getFurtherEmisCount(loanId);
				}
			}
			applicationResponseDto.setNoOfFeatureMOnth(furtherCount);

		}
		// sum(emi_amount),count(emi_amount)
		for (Object[] obj : totalPendingINformation) {
			applicationResponseDto.setApplicationAmountNotPaid(obj[0] == null ? 0.0d : (double) obj[0]);
			Integer unpaidEmisCount = penaltyDetailsRepo.getEmisUnPaidCount(parentRequestID);
			applicationResponseDto.setEmiNotPaid(unpaidEmisCount);

		}

		double principulPending = disbursmentAmount - principulsum;
		double outstandingAmount = principulPending + penality + totalProfit;
		applicationResponseDto.setToatlOutStndingAmount(outstandingAmount);
		return applicationResponseDto;
	}

	@Override
	public String sendingNotificationToLender() {
		String message = null;
		List<Integer> listOfLenderApplicationIds = oxyLoanRepo.getLenderApplicationIds();
		for (int i = 0; i < listOfLenderApplicationIds.size(); i++) {
			int lenderApplicationId = listOfLenderApplicationIds.get(i);
			List<Integer> listOfLoanIds = oxyLoanRepo.getLoanIdsForApplication(lenderApplicationId);
			double emiAmountNotPaid = 0.0;
			int count = 0;
			String email = null;
			String firstName = null;
			String lastName = null;

			for (int i1 = 0; i1 < listOfLoanIds.size(); i1++) {
				int loanId = listOfLoanIds.get(i1);

				if (count == 0) {
					Integer userId = oxyLoanRepo.getLenderEmail(loanId);
					User user = userRepo.findById(userId).get();
					firstName = user.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase()
							+ user.getPersonalDetails().getFirstName().substring(1);
					lastName = user.getPersonalDetails().getLastName().substring(0, 1).toUpperCase()
							+ user.getPersonalDetails().getLastName().substring(1);
					email = user.getEmail();
				}
				count = count + 1;

				LoanEmiCard loanEmiCard = loanEmiCardRepo.getLoanEmiInfo(loanId);

				if (loanEmiCard.getStatus() == com.oxyloans.entity.loan.LoanEmiCard.Status.INITIATED) {
					emiAmountNotPaid = emiAmountNotPaid + loanEmiCard.getEmiAmount();
				} else {
					if (loanEmiCard.getStatus() == com.oxyloans.entity.loan.LoanEmiCard.Status.INPROCESS) {
						emiAmountNotPaid = emiAmountNotPaid + loanEmiCard.getRemainingEmiAmount();
					} else {

						emiAmountNotPaid = emiAmountNotPaid + 0.0;
					}

				}

			}
			if (emiAmountNotPaid > 0) {
				TemplateContext context = new TemplateContext();

				context.put("lenderName", firstName + " " + lastName);

				context.put("date", dateFormate.format(new Date()));
				String mailsubject = "Emi not paid";
				String emailTemplateName = "emi-not-paid.template";
				EmailRequest emailRequest = new EmailRequest(new String[] { email }, mailsubject, emailTemplateName,
						context);
				EmailResponse emailResponse = emailService.sendEmail(emailRequest);
				if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
					throw new RuntimeException(emailResponse.getErrorMessage());
				}
			}
			message = "success";
		}
		return message;

	}

	@Override
	public LoanEmiCardResponseDto getInterestPendingTillDate(int loanId) {
		List<LoanEmiCard> loanEmiCardDetails = loanEmiCardRepo.findByLoanId(loanId);
		Double interestPending = 0.0;
		if (loanEmiCardDetails != null && !loanEmiCardDetails.isEmpty()) {
			interestPending = loanEmiCardRepo.getInterestPendingTillDate(loanId);
		} else {
			throw new OperationNotAllowedException("please check loanId", ErrorCodes.ENITITY_NOT_FOUND);
		}
		LoanEmiCardResponseDto loanEmiCardResponseDto = new LoanEmiCardResponseDto();
		loanEmiCardResponseDto.setInterestPendingTillDate(interestPending);
		return loanEmiCardResponseDto;

	}

	@Override
	public LoanEmiCardResponseDto interestWaiveOff(int loanId, LoanEmiCardResponseDto loanEmiCardResponseDto) {

		List<LoanEmiCard> loanEmiCardDetails = loanEmiCardRepo.findByLoanId(loanId);
		Double interestPending = 0.0;
		Double afterWaiveOff = 0.0;
		int duration = 0;
		if (loanEmiCardDetails != null && !loanEmiCardDetails.isEmpty()) {

			OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get();
			if (oxyLoan != null) {
				duration = oxyLoan.getDuration();
			}
			interestPending = loanEmiCardRepo.getInterestPendingTillDate(loanId);
			Double interestWaiveOff = loanEmiCardResponseDto.getInterestWaiveOffAmount();
			List<LoanEmiCard> loanEmiCardInfo = loanEmiCardRepo.findByLoanIdOrderByEmiNumber(loanId);
			int count = 0;
			if (interestWaiveOff != null) {
				if (interestWaiveOff <= interestPending) {
					double remainingInterestValue = interestPending - interestWaiveOff;
					int countOfEmis = loanEmiCardRepo.getEmisPendingInterestCount(loanId);
					double interestForEachEmi = remainingInterestValue / countOfEmis;
					List<Integer> listOfEmisNumbers = loanEmiCardRepo.getEmiNumbers(loanId);
					for (int i = 0; i < listOfEmisNumbers.size(); i++) {
						int emiNumber = listOfEmisNumbers.get(i);
						LoanEmiCard loanEmiCard = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId, emiNumber);
						if (loanEmiCard != null) {
							loanEmiCard.setEmiLateFeeCharges(interestForEachEmi);
							loanEmiCardRepo.save(loanEmiCard);
							InterestDetails interestDetails = interestDetailsRepo.findByloanIdAndEmiNumber(loanId,
									emiNumber);
							if (interestDetails != null) {
								interestDetails.setInterest(interestForEachEmi);
								interestDetailsRepo.save(interestDetails);
							}
						}
					}

				} else {
					throw new OperationNotAllowedException("given more than the Interest WaiveOff amount",
							ErrorCodes.ENITITY_NOT_FOUND);
				}
				for (LoanEmiCard loanDetails : loanEmiCardInfo) {
					int emiNumber = loanDetails.getEmiNumber();
					LoanEmiCard loanEmiCard1 = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId, emiNumber);
					if (emiNumber == duration) {
						loanEmiCard1.setPenality(loanEmiCard1.getPenality() + interestWaiveOff);
						loanEmiCardRepo.save(loanEmiCard1);
					} else {
						if (loanEmiCard1.getPenality() == 0) {
							if (count == 0) {
								loanEmiCard1.setPenality(interestWaiveOff);
								loanEmiCard1.setInterest_waived_off(new Date());
								loanEmiCardRepo.save(loanEmiCard1);
								count = count + 1;
							}
						}
					}
				}
			}

		} else {
			throw new OperationNotAllowedException("please check loanId", ErrorCodes.ENITITY_NOT_FOUND);
		}
		LoanEmiCardResponseDto loanEmiCardResponseDto1 = new LoanEmiCardResponseDto();
		afterWaiveOff = loanEmiCardRepo.getInterestPendingTillDate(loanId);
		loanEmiCardResponseDto1.setInterestPendingTillDate(afterWaiveOff);

		return loanEmiCardResponseDto1;

	}

	@Override
	public CommentsResponseDto uploadCommentsFromRadhaSir(int id, CommentsRequestDto commentsRequestDto) {
		String message = null;
		LoanRequest loanRequest = loanRequestRepo.findById(id).get();
		if (loanRequest != null) {
			if (loanRequest.getRatingBySir() == null) {
				loanRequest.setCommentsBySir(commentsRequestDto.getComments());
				if (commentsRequestDto.getRating() <= 5 && commentsRequestDto.getRating() > 0) {
					loanRequest.setRatingBySir(commentsRequestDto.getRating());
				} else {
					throw new OperationNotAllowedException(
							"Rating should be less than 5 and greater than 0 please check ",
							ErrorCodes.PERMISSION_DENIED);
				}
				loanRequest.setRateOfInterestToBorrower(commentsRequestDto.getRateOfInterestToBorrower());
				loanRequest.setRateOfInterestToLender(commentsRequestDto.getRateOfInterestToLender());
				loanRequest.setDurationBySir(commentsRequestDto.getDurationToTheApplication());
				loanRequest.setRepaymentMethodForBorrower(commentsRequestDto.getRepaymentMethodForBorrower());
				loanRequest.setRepaymentMethodForLender(commentsRequestDto.getRepaymentMethodForLender());
				loanRequest.setDurationType(
						commentsRequestDto.getDurationType().equalsIgnoreCase("months") ? DurationType.Months
								: DurationType.Days);

				loanRequestRepo.save(loanRequest);
				message = "SUCCESSFULLY UPDATED";
			} else {
				throw new OperationNotAllowedException("comments already updated", ErrorCodes.ALREDY_IN_PROGRESS);
			}
		}
		CommentsResponseDto commentsResponseDto = new CommentsResponseDto();
		commentsResponseDto.setStatus(message);
		return commentsResponseDto;

	}

	@Override
	public PaymentSearchByStatus getBorrowerPaymentList(String borrowerUniqueNumber, String paymentStatus) {
		logger.info("getBorrowerPaymentList method starts");
		int count = 0;
		List<PaymentUploadHistoryResponseDto> borrowerFeestatusListResponse = new ArrayList<PaymentUploadHistoryResponseDto>();
		List<PaymentUploadHistory> borrowerPaymentList = paymentUploadHistoryRepo
				.listOfPaymentsByBorrower(borrowerUniqueNumber, paymentStatus.toUpperCase());

		if (borrowerPaymentList != null && !borrowerPaymentList.isEmpty()) {
			for (PaymentUploadHistory paymentUploadHistory : borrowerPaymentList) {

				count++;
				PaymentUploadHistoryResponseDto paymentUploadHistoryResponseDto = new PaymentUploadHistoryResponseDto();

				paymentUploadHistoryResponseDto.setUserId(paymentUploadHistory.getUserId());
				paymentUploadHistoryResponseDto.setBorrowerUniqueNumber(paymentUploadHistory.getBorrowerUniqueNumber());
				paymentUploadHistoryResponseDto.setBorrowerName(paymentUploadHistory.getBorrowerName());
				paymentUploadHistoryResponseDto.setAmount(paymentUploadHistory.getAmount());
				paymentUploadHistoryResponseDto.setUpdatedName(paymentUploadHistory.getUpdatedName());
				paymentUploadHistoryResponseDto.setPaidDate(paymentUploadHistory.getPaidDate().toString());
				paymentUploadHistoryResponseDto.setStatus(paymentUploadHistory.getStatus().toString());

				paymentUploadHistoryResponseDto.setPaymentType(paymentUploadHistory.getPaymentType().toString());
				borrowerFeestatusListResponse.add(paymentUploadHistoryResponseDto);
			}

		}
		PaymentSearchByStatus response = new PaymentSearchByStatus();
		response.setPaymentUploadHistoryResponseDto(borrowerFeestatusListResponse);
		response.setTotalCount(count);
		logger.info("getBorrowerPaymentList method ends");
		return response;

	}

	@Override
	public BorrowersFeeBasedOnDate getBorrowersFreeBasedOnGivenDate(int month) {
		logger.info("getBorrowersFreeBasedOnGivenDate method end");
		BorrowersFeeBasedOnDate borrowersFeeBasedOnDate = new BorrowersFeeBasedOnDate();
		Date d = new Date();
		int yearValue = d.getYear();
		int year = yearValue + 1900;
		Double borrowersPaidFreeBasedOnMonth = oxyLoanRepo.getSumOfBorrersFeeBasedOndate(month, year);
		List<BorrowerFreeDetailsResponseDto> borrowerFreeDetailslist = new ArrayList<BorrowerFreeDetailsResponseDto>();

		if (borrowersPaidFreeBasedOnMonth != null) {
			List<Integer> listOfApplicationIds = oxyLoanRepo.getApplictionIdsBasedOnDate(month, year);

			for (int i = 0; i < listOfApplicationIds.size(); i++) {
				int borrowerParentRequestId = listOfApplicationIds.get(i);
				BorrowerFreeDetailsResponseDto borrowerFreeDetailsResponseDto = new BorrowerFreeDetailsResponseDto();
				Double applicationLevelFee = oxyLoanRepo.getBorrowerFeeBasedOnDateAndId(month, year,
						borrowerParentRequestId);
				if (applicationLevelFee != null) {
					borrowerFreeDetailsResponseDto.setApplicationLevelFreePaid(applicationLevelFee);
					LoanRequest loanRequest = loanRequestRepo.findById(borrowerParentRequestId).get();
					if (loanRequest != null && loanRequest.getLoanOfferedAmount() != null) {
						borrowerFreeDetailsResponseDto
								.setOfferedAmount(loanRequest.getLoanOfferedAmount().getLoanOfferedAmount());
						borrowerFreeDetailsResponseDto.setApplictionId(borrowerParentRequestId);
						borrowerFreeDetailsResponseDto.setBorrowerId(loanRequest.getLoanOfferedAmount().getUserId());
						int borrowerId = loanRequest.getLoanOfferedAmount().getUserId();
						User user = userRepo.findById(borrowerId).get();
						String name = null;
						if (user != null) {
							name = user.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase()
									+ user.getPersonalDetails().getFirstName().substring(1) + " "
									+ user.getPersonalDetails().getLastName().substring(0, 1).toUpperCase()
									+ user.getPersonalDetails().getLastName().substring(1);
						}
						borrowerFreeDetailsResponseDto.setBorrowerName(name);
						borrowerFreeDetailsResponseDto
								.setApplicationLevelFreeHaveToPay(loanRequest.getLoanOfferedAmount().getBorrowerFee());
						List<Integer> listOfLoanIds = oxyLoanRepo.getPercentageForFee(month, year,
								borrowerParentRequestId);
						int count = 0;
						if (listOfLoanIds != null) {
							if (count == 0) {
								for (int i1 = 0; i1 < listOfLoanIds.size(); i1++) {
									int loanId = listOfLoanIds.get(i1);
									count = count + 1;
									OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get();
									if (oxyLoan != null) {
										borrowerFreeDetailsResponseDto.setPercentage(oxyLoan.getFeePercentage());
									}
								}
							}

						}

					}
					borrowerFreeDetailslist.add(borrowerFreeDetailsResponseDto);
				}

			}
			borrowersFeeBasedOnDate.setTotalFreeBasedOndate(borrowersPaidFreeBasedOnMonth);
			borrowersFeeBasedOnDate.setBorrowerFreeDetailsResponseDto(borrowerFreeDetailslist);
			Double totalFreePaid = oxyLoanRepo.getTotalFree();
			if (totalFreePaid != null) {
				borrowersFeeBasedOnDate.setTotalFreeTillDate(totalFreePaid);
			}

		} else {
			throw new OperationNotAllowedException("Borrowers free is null based on given month",
					ErrorCodes.ENITITY_NOT_FOUND);
		}
		logger.info("getBorrowersFreeBasedOnGivenDate method end");
		return borrowersFeeBasedOnDate;

	}

	@Override
	public String calculatingLoanLevelFeeForOldBorrower(int applicationId) {
		String status = null;
		logger.info("calculatingLoanLevelFeeForOldBorrower method start");
		LoanRequest loanReuest = loanRequestRepo.findById(applicationId).get();
		if (loanReuest != null && loanReuest.getLoanOfferedAmount() != null) {
			int borrowerParentRequestId = applicationId;
			double applicationLevelOfferedAmount = loanReuest.getLoanOfferedAmount().getLoanOfferedAmount();

			Date accepetedDate = loanReuest.getLoanOfferedAmount().getAccepetedOn();
			Double applicationLevelFreeValue = loanReuest.getLoanOfferedAmount().getBorrowerFee();
			double applicationLevelFree = Math.round(applicationLevelFreeValue);

			if (accepetedDate != null) {
				List<Object[]> listOfLoansBasedOnAppplication = oxyLoanRepo
						.getBorrowersBasedOnApplicationId(borrowerParentRequestId, accepetedDate);
				if (listOfLoansBasedOnAppplication != null && !listOfLoansBasedOnAppplication.isEmpty()) {

					for (Object[] e1 : listOfLoansBasedOnAppplication) {
						int loanId = Integer.parseInt(e1[0] == null ? "0" : e1[0].toString());
						double loanLevelDisbursedAmount = Double.parseDouble(e1[1] == null ? "0" : e1[1].toString());

						double freeApplicationLevel = 0.0;
						double GstApplicationLevel = 0.0;
						int percentageForFree = 1;
						freeApplicationLevel = (applicationLevelOfferedAmount * percentageForFree) / 100;
						GstApplicationLevel = (freeApplicationLevel * 18) / 100;
						double total = freeApplicationLevel + GstApplicationLevel;
						percentageForFree = (int) (applicationLevelFree / total);

						double loanLevelFree = (loanLevelDisbursedAmount * percentageForFree) / 100;
						double loanLevelFreeWithGst = (loanLevelFree * 18) / 100;
						double totalFreeLoanLevel = loanLevelFree + loanLevelFreeWithGst;

						OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get();
						if (oxyLoan != null) {
							oxyLoan.setBorrowerTransactionFee(totalFreeLoanLevel);
							oxyLoan.setFeePercentage(percentageForFree);
							oxyLoan.setBorrowerFeePaid(true);
							oxyLoanRepo.save(oxyLoan);
						}

					}
				}

			}
		}

		status = "success";
		logger.info("calculatingLoanLevelFeeForOldBorrower method end");
		return status;

	}

	@Override
	public EnachScheduledMailResponseDto sendEnachScheduleEmailExcel() {
		logger.info("---------------------  sendEnachScheduleEmailExcel method start ...............");
		EnachScheduledMailResponseDto response = new EnachScheduledMailResponseDto();

		Calendar currentTime = Calendar.getInstance();
		Calendar emiDate = Calendar.getInstance();
		Date date = null;
		int monthDate = 0;
		if (currentTime.get(Calendar.DAY_OF_MONTH) == 2) {
			monthDate = 5;
		} else if (currentTime.get(Calendar.DAY_OF_MONTH) == 17) {
			monthDate = 20;
		}

		emiDate.set(Calendar.DAY_OF_MONTH, monthDate);
		date = emiDate.getTime();

		List<Object[]> loanEmiCardDetails = loanEmiCardRepo.findAllEnachScheduled(date);

		FileOutputStream outFile = null;

		XSSFWorkbook workBook = new XSSFWorkbook();
		XSSFSheet spreadSheet = workBook.createSheet(LocalDate.now() + "EnachScheduledReport.xls");
		XSSFFont headerFont = workBook.createFont();
		CellStyle style = workBook.createCellStyle();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.index);
		style.setFont(headerFont);
		spreadSheet.createFreezePane(0, 1);
		Row row = spreadSheet.createRow(0);

		Cell cell = row.createCell(0);
		cell.setCellValue("Unique Number");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("Borrower Name");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("Loan ID");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("Emi Amount");
		cell.setCellStyle(style);

		cell = row.createCell(4);
		cell.setCellValue("Emi Due Date");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (Object[] obj : loanEmiCardDetails) {

			Row row1 = spreadSheet.createRow(++rowCount);

			Cell cell1 = row1.createCell(0);
			cell1.setCellValue(obj[0].toString());

			cell1 = row1.createCell(1);
			cell1.setCellValue(obj[1].toString());

			cell1 = row1.createCell(2);
			cell1.setCellValue(obj[2].toString());

			cell1 = row1.createCell(3);
			cell1.setCellValue(obj[3].toString());

			cell1 = row1.createCell(4);
			cell1.setCellValue(obj[4].toString());

		}

		String filePath = LocalDate.now() + "EnachScheduledReport.xls";
		File f = new File(filePath);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);
			if (f.exists()) {
				response.setFileStatus("File saved");
			}

			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate = expectedDateFormat.format(current.getTime());
			templateContext.put("currentDate", expectedCurrentDate);

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(filePath);
			fileRequest.setFileType(FileType.EnachScheduledReport);
			fileRequest.setFilePrifix(FileType.EnachScheduledReport.name());

			try {
				FileResponse putFile = fileManagementService.putFile(fileRequest);

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

			String mailSubject = "Enach Scheduler List";
			EmailRequest emailRequest = new EmailRequest(
					new String[] { "subbu@oxyloans.com", "ramadevi@oxyloans.com", "archana.n@oxyloans.com",
							"narendra@oxyloans.com" },
					"sendScheduledENach.template", templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			} else {
				response.setEmailStatus("Email Sent!");

				if (f.delete()) {
					System.out.println("File deleted");
				} else {
					System.out.println("Not deleted");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;

	}

	@Override
	@Transactional
	public EnachTransactionResponseDto enachSuccessVerificationList() {
		logger.info("enachSuccessVerificationList method start...........................");
		EnachScheduledMailResponseDto response = new EnachScheduledMailResponseDto();
		Calendar currentTime = Calendar.getInstance();

		String dateValue = null;
		if (currentTime.get(Calendar.DAY_OF_MONTH) == 6) {
			dateValue = "5";

		} else if (currentTime.get(Calendar.DAY_OF_MONTH) == 21) {
			dateValue = "20";

		}

		Calendar calendar = Calendar.getInstance();

		calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dateValue));

		String modifiedDate = new SimpleDateFormat("ddMMyyyy").format(calendar.getTime());

		List<EnachTransactionResponseDto> enachTransactionResponses = new ArrayList<EnachTransactionResponseDto>();

		List<Object[]> enachTransactionResponsesList = enachTransactionVerificationRepo
				.findDetailsOfEnachTransactionResponses(modifiedDate);

		for (Object[] enachTransactions : enachTransactionResponsesList) {

			EnachTransactionResponseDto responseInfo = new EnachTransactionResponseDto();

			responseInfo.setUniqueNumber(enachTransactions[0].toString());
			responseInfo.setFirstName(enachTransactions[1].toString());
			responseInfo.setLoanId((Integer) enachTransactions[2]);

			enachTransactionResponses.add(responseInfo);

		}

		ByteArrayOutputStream output = null;
		FileOutputStream outFile = null;

		XSSFWorkbook workBook = new XSSFWorkbook();
		XSSFSheet spreadSheet = workBook.createSheet(LocalDate.now() + "TransactionResponses.xls");
		Row row0 = spreadSheet.createRow(0);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Unique Number");

		cell = row0.createCell(1);
		cell.setCellValue("Name");

		cell = row0.createCell(2);
		cell.setCellValue("Loan Id");

		int rowCount = 0;

		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		int countForSno = 0;

		for (Object[] enachTransactions : enachTransactionResponsesList) {

			Row row = spreadSheet.createRow(++rowCount);

			Cell cell1 = row.createCell(0);
			cell1.setCellValue(enachTransactions[0].toString());

			cell1 = row.createCell(1);
			cell1.setCellValue(enachTransactions[1].toString());

			cell1 = row.createCell(2);
			cell1.setCellValue((Integer) enachTransactions[2]);

			countForSno = countForSno + 1;

		}

		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		String filePath = LocalDate.now() + "TransactionResponses.xls";

		File f = new File(filePath);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);
			if (f.exists()) {
				response.setFileStatus("File saved");
			}

			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate = expectedDateFormat.format(current.getTime());
			templateContext.put("currentDate", expectedCurrentDate);

			String mailSubject = "Enach Verification Response";

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(filePath);
			fileRequest.setFileType(FileType.TransactionResponses);
			fileRequest.setFilePrifix(FileType.TransactionResponses.name());

			try {
				FileResponse putFile = fileManagementService.putFile(fileRequest);

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

			EmailRequest emailRequest = new EmailRequest(
					new String[] { "subbu@oxyloans.com", "ramadevi@oxyloans.com", "archana.n@oxyloans.com",
							"narendra@oxyloans.com" },
					"enachVerificationResponses.template", templateContext, mailSubject,
					new String[] { f.getAbsolutePath() });
			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			} else {
				response.setEmailStatus("Email Sent!");

				if (f.delete()) {
					System.out.println("File deleted");
				} else {
					System.out.println("Not deleted");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		EnachTransactionResponseDto enachTransactionResponse = new EnachTransactionResponseDto();
		enachTransactionResponse.setStatus("SUCCESS");

		logger.info("enachSuccessVerificationList method end...........................");
		return enachTransactionResponse;
	}

	@Override
	public LenderNocResponseDto getLendersInformation(int userId, LenderIncomeRequestDto lenderIncomeRequestDto)
			throws FileNotFoundException {

		LenderNocResponseDto lenderNocResponseDto = new LenderNocResponseDto();
		String downloadUrl = null;

		List<Integer> listOfDealIds = lendersReturnsRepo.interestReturnedDealIds(userId,
				lenderIncomeRequestDto.getStartDate(), lenderIncomeRequestDto.getEndDate());
		double interestAmount = 0.0;
		List<LenderIncomeResponseDto> listOfdealsInfo = new ArrayList<LenderIncomeResponseDto>();
		if (listOfDealIds != null && !listOfDealIds.isEmpty()) {
			int sno = 0;
			for (int i = 0; i < listOfDealIds.size(); i++) {
				int dealId = listOfDealIds.get(i);
				if (dealId != 0) {
					sno += 1;
					OxyBorrowersDealsInformation oxyDealsInformation = oxyBorrowersDealsInformationRepo
							.findDealAlreadyGiven(dealId);
					if (oxyDealsInformation != null) {
						LenderIncomeResponseDto lenderIncomeResponseDto = new LenderIncomeResponseDto();
						lenderIncomeResponseDto.setsNo(sno);
						lenderIncomeResponseDto.setDealId(dealId);
						lenderIncomeResponseDto.setDealName(oxyDealsInformation.getDealName());

						OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
								.toCheckUserAlreadyInvoledInDeal(userId, dealId);
						Double participationUpdated = lendersPaticipationUpdationRepo
								.getSumOfLenderUpdatedAmountToDeal(userId, dealId);
						double participatedAmount = 0.0;
						if (oxyLendersAcceptedDeals != null) {
							if (participationUpdated != null) {
								participatedAmount = oxyLendersAcceptedDeals.getParticipatedAmount()
										+ participationUpdated;
							} else {
								participatedAmount = oxyLendersAcceptedDeals.getParticipatedAmount();
							}

						}

						lenderIncomeResponseDto
								.setParticipatedAmount(BigDecimal.valueOf(participatedAmount).toBigInteger());
						lenderIncomeResponseDto
								.setLoanActiveDate(expectedDateFormat.format(oxyDealsInformation.getLoanActiveDate()));
						if (oxyDealsInformation.getBorrowerclosingstatus()
								.equals(OxyBorrowersDealsInformation.BorrowerClosingStatus.CLOSED)) {

							lenderIncomeResponseDto.setLoanStatus(
									OxyBorrowersDealsInformation.BorrowerClosingStatus.CLOSED.toString());
						} else {
							Double principalReturnedToDeal = lendersReturnsRepo
									.principalAmountReturnedToLenderBasedOnDeal(userId, dealId);

							Double sumOfWalletAmount = lenderOxyWalletNativeRepo
									.paticipatedAmountMovedToWalletSum(userId, dealId);
							double principalReturnedAmount = 0.0;
							if (principalReturnedToDeal != null) {
								principalReturnedAmount = principalReturnedAmount + principalReturnedToDeal;

							}
							if (sumOfWalletAmount != null) {
								principalReturnedAmount = principalReturnedAmount + sumOfWalletAmount;
							}
							double remainingAmount = 0.0;
							if (principalReturnedAmount > 0.0) {
								remainingAmount = participatedAmount - principalReturnedAmount;
							} else {
								remainingAmount = participatedAmount;
							}

							if (remainingAmount > 0.0) {
								lenderIncomeResponseDto.setLoanStatus(
										OxyBorrowersDealsInformation.BorrowerClosingStatus.NOTYETCLOSED.toString());
							} else {
								lenderIncomeResponseDto.setLoanStatus(
										OxyBorrowersDealsInformation.BorrowerClosingStatus.CLOSED.toString());
							}
						}
						Double interestEarnedToDeal = lendersReturnsRepo.interestEarnedInFinancialYearToDeal(userId,
								lenderIncomeRequestDto.getStartDate(), lenderIncomeRequestDto.getEndDate(), dealId);
						if (interestEarnedToDeal != null) {
							interestAmount = interestAmount + interestEarnedToDeal;
							lenderIncomeResponseDto
									.setIncomeEarnedToDeal(BigDecimal.valueOf(interestEarnedToDeal).toBigInteger());
						}
						listOfdealsInfo.add(lenderIncomeResponseDto);
					}

				} /*
					 * else { List<LendersReturns> oldDeals =
					 * lendersReturnsRepo.oldDealsFinancialYearInterestEarned(userId,
					 * lenderIncomeRequestDto.getStartDate(), lenderIncomeRequestDto.getEndDate());
					 * if (oldDeals != null && !oldDeals.isEmpty()) { for (LendersReturns
					 * interestReturnedInfo : oldDeals) { sno += 1; LenderIncomeResponseDto
					 * lenderIncomeResponseDto = new LenderIncomeResponseDto();
					 * lenderIncomeResponseDto.setsNo(sno); lenderIncomeResponseDto.setDealId(0);
					 * lenderIncomeResponseDto.setDealName(interestReturnedInfo.getRemarks());
					 * double participatedAmount = 0.0; lenderIncomeResponseDto
					 * .setParticipatedAmount(BigDecimal.valueOf(participatedAmount).toBigInteger())
					 * ; lenderIncomeResponseDto.setLoanActiveDate("NA"); interestAmount =
					 * interestAmount + interestReturnedInfo.getAmount();
					 * lenderIncomeResponseDto.setIncomeEarnedToDeal(
					 * BigDecimal.valueOf(interestReturnedInfo.getAmount()).toBigInteger());
					 * lenderIncomeResponseDto.setLoanStatus("NA");
					 * listOfdealsInfo.add(lenderIncomeResponseDto); } }
					 * 
					 * }
					 */
			}

		}
		FileOutputStream outFile = null;

		XSSFWorkbook workBook = new XSSFWorkbook();

		XSSFSheet spreadSheet = workBook.createSheet("IncomeEarnings.xls");

		CellStyle style = workBook.createCellStyle();

		style.setFillForegroundColor(IndexedColors.LEMON_CHIFFON.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		XSSFFont font = workBook.createFont(); // Create font font.setBold(true); style.setFont(font);

		Row row = spreadSheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue("S.No");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("Deal Id");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("Deal Name");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("Participated Amount");
		cell.setCellStyle(style);

		cell = row.createCell(4);
		cell.setCellValue("First Interest Paid Date");
		cell.setCellStyle(style);

		cell = row.createCell(5);
		cell.setCellValue("Interest Earned");
		cell.setCellStyle(style);

		cell = row.createCell(6);
		cell.setCellValue("Deal Status");
		cell.setCellStyle(style);

		int rowCount = 0;

		if (!listOfdealsInfo.isEmpty()) {

			int countForSno = 0;
			for (LenderIncomeResponseDto oxyBorrowersDeals : listOfdealsInfo) {

				Row row1 = spreadSheet.createRow(++rowCount);
				countForSno = countForSno + 1;
				Cell cell1 = row1.createCell(0);
				cell1.setCellValue(oxyBorrowersDeals.getsNo());

				cell1 = row1.createCell(1);
				cell1.setCellValue(oxyBorrowersDeals.getDealId());

				cell1 = row1.createCell(2);
				cell1.setCellValue(oxyBorrowersDeals.getDealName());

				cell1 = row1.createCell(3);
				cell1.setCellValue(oxyBorrowersDeals.getParticipatedAmount().doubleValue());

				cell1 = row1.createCell(4);
				cell1.setCellValue(oxyBorrowersDeals.getLoanActiveDate());

				cell1 = row1.createCell(5);
				cell1.setCellValue(oxyBorrowersDeals.getIncomeEarnedToDeal().doubleValue());

				cell1 = row1.createCell(6);
				cell1.setCellValue(oxyBorrowersDeals.getLoanStatus());

			}
		}

		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		String filePath = "(LR" + userId + ")" + ".xls";

		File fileForExcel = new File(filePath);

		outFile = new FileOutputStream(fileForExcel);

		try {
			workBook.write(outFile);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (fileForExcel.exists()) {
			logger.info("filesaved");
		}

		Calendar current = Calendar.getInstance();
		String expectedCurrentDate = expectedDateFormat.format(current.getTime());
		TemplateContext templateContext = new TemplateContext();
		templateContext.put("currentDate", expectedCurrentDate);
		User user = userRepo.findById(userId).get();
		if (user != null) {
			templateContext.put("lenderName",
					user.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase()
							+ user.getPersonalDetails().getFirstName().substring(1) + " "
							+ user.getPersonalDetails().getLastName());
			templateContext.put("lenderId", user.getId());
			templateContext.put("mobileNumber", user.getMobileNumber());
			templateContext.put("email", user.getEmail());
			templateContext.put("totalProfitAmount", BigDecimal.valueOf(interestAmount).toBigInteger().toString());
			templateContext.put("startDate", expectedDateFormat.format(lenderIncomeRequestDto.getStartDate()));
			templateContext.put("endDate", expectedDateFormat.format(lenderIncomeRequestDto.getEndDate()));
			String mailSubject = "Lender Profit Details";

			// String outputFileName = user.getId() + "LENDERINCOME" + ".pdf";

			/*
			 * String generatedPdf = ""; generatedPdf =
			 * pdfEngine.generatePdf("agreement/lenderProfit.xml", templateContext,
			 * outputFileName);
			 */

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(fileForExcel));
			fileRequest.setFileName(filePath);
			fileRequest.setFileType(FileType.LENDERINCOME);
			fileRequest.setFilePrifix(FileType.LENDERINCOME.name());

			try {
				FileResponse putFile = fileManagementService.putFile(fileRequest);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			FileResponse file = fileManagementService.getFile(fileRequest);
			if (file != null) {
				String[] str = file.getDownloadUrl().split(Pattern.quote("?"));
				downloadUrl = str[0];

			}
			if (lenderIncomeRequestDto.getInputType().equalsIgnoreCase("EMAIL")) {
				EmailRequest emailRequest = new EmailRequest(
						new String[] { user.getEmail(), "archana.n@oxyloans.com", "ramadevi@oxyloans.com",
								"subbu@oxyloans.com" },
						"lenderInformation.template", templateContext, mailSubject,
						new String[] { fileForExcel.getAbsolutePath() });

				EmailResponse emailResponse1 = emailService.sendEmail(emailRequest);
				if (emailResponse1.getStatus() == EmailResponse.Status.FAILED) {
					throw new RuntimeException(emailResponse1.getErrorMessage());
				}
			}
		}

		lenderNocResponseDto.setStatus("mailSentSuccessFully");
		lenderNocResponseDto.setLenderProfit(downloadUrl);
		lenderNocResponseDto.setTotalInterestEarned(BigDecimal.valueOf(interestAmount).toBigInteger());

		return lenderNocResponseDto;

	}

	@Override
	public PaymentHistoryResponseDto getEcsAndNonEcsPaymentHistory(String monthName, String year) {

		int sreedeviHyundaiId = 34515;
		int luckyHyundaiId = 35121;
		int padiminiBollineniId = 5674;
		int sharanyaLodgeId = 35829;
		int GongineniId = 35218;
		int citlyId = 35220;
		int lakshminrusimhamVenkataId = 25174;
		int saralTalwarId = 32672;
		int mahenderReddyId = 37603;

		String sreeDeviHyndaiName = "Sai Sreedevi Hyundai";
		String luckyHyundaiName = "Lucky Hyundai";
		String tollGatesName = "TollGate";
		String padiminiBollineniName = "Padimini Bollineni";
		String sharanyaLodgeName = "Sharanya Lodge";
		String GongineniName = "Gongineni Advertising";
		String citlyName = "Citly";
		String lakshminrusimhamVenkataName = "Lakshminrusimham Venkata";
		String saralTalwarName = "Saral Talwar";
		String mahenderReddyName = "Mahender Reddy";

		StringBuilder sb = new StringBuilder();

		int y = 0;
		try {
			Date date = new SimpleDateFormat("MMMM").parse(monthName);
			y = date.getMonth() + 1;

			sb.append(String.format("%02d", y)).append('-').append(year);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		String monthAndYear = String.format("%02d", y) + year;
		Double paymentScreenShotAmount = paymentUploadHistoryRepo.getAmountThroughPaymentScreesnShot(sb.toString());
		Double paymentScreenshotFullPayment = paymentUploadHistoryRepo
				.getFullPaymentThroughPaymentScreesnShot(sb.toString());
		Double paymentScreenshotPartPayment = paymentUploadHistoryRepo
				.getPartPaymentThroughPaymentScreesnShot(sb.toString());

		Double totalPayuFeeCollected = lenderPayuDetailsRepo.getTotalFeeReceivedThroughPayu();

		Double lenderFreeWthPayuGivenMonth = lenderPayuDetailsRepo.getFeeReceivedInGivenMonth(sb.toString());

		List<LenderPayuDto> lenderPayuList = lenderFeeStatisticsThroughPayu(sb.toString());

		String[] fullDate = { "05" + monthAndYear, "20" + monthAndYear };
		Double ecsAmount = 0.0;
		Double amount = 0.0;
		int count = 0;
		Double amountOnFifthEcs = 0.0;
		Double amountOnTwentyEcs = 0.0;
		for (String givenDate : fullDate) {

			amount = loanEmiCardRepo.getAmountThroughEcs(givenDate);
			if (amount != null) {
				ecsAmount += amount;
			}
			if (count == 0) {
				amountOnFifthEcs = amount;
				count = count + 1;
			} else {
				amountOnTwentyEcs = amount;
			}
		}

		Double payUAmount = loanEmiCardRepo.getAmountThroughPayU(sb.toString());

		Double partPaidAmount = loanEmiCardRepo.getPartPaidAmount(sb.toString());

		Double fullPayment = loanEmiCardRepo.getFullAmount(sb.toString());
		Double amountThroughIDFC = 0.0;

		List<PaymentHistoryRequestDto> sreedeviHyundaiPayments = getAmountReceivedByLoanOwner(sb.toString(),
				sreedeviHyundaiId, sreeDeviHyndaiName);
		List<PaymentHistoryRequestDto> luckyHyndaiPayments = getAmountReceivedByLoanOwner(sb.toString(), luckyHyundaiId,
				luckyHyundaiName);

		List<PaymentHistoryRequestDto> tollGatesPayments = getAmountReceivedByLoanOwner(sb.toString(), luckyHyundaiId,
				tollGatesName);

		List<PaymentHistoryRequestDto> padiminiBollineniPayments = getAmountReceivedByLoanOwner(sb.toString(),
				padiminiBollineniId, padiminiBollineniName);

		List<PaymentHistoryRequestDto> sharanyaLodgePayments = getAmountReceivedByLoanOwner(sb.toString(),
				sharanyaLodgeId, sharanyaLodgeName);

		List<PaymentHistoryRequestDto> gongineniAdvertisingPayments = getAmountReceivedByLoanOwner(sb.toString(),
				GongineniId, GongineniName);

		List<PaymentHistoryRequestDto> citlyPayments = getAmountReceivedByLoanOwner(sb.toString(), citlyId, citlyName);

		List<PaymentHistoryRequestDto> lakshminrusimhamVenkataPayments = getAmountReceivedByLoanOwner(sb.toString(),
				lakshminrusimhamVenkataId, lakshminrusimhamVenkataName);

		List<PaymentHistoryRequestDto> saralTalwarPayments = getAmountReceivedByLoanOwner(sb.toString(), saralTalwarId,
				saralTalwarName);

		List<PaymentHistoryRequestDto> mahenderReddyPayments = getAmountReceivedByLoanOwner(sb.toString(),
				mahenderReddyId, mahenderReddyName);

		PaymentHistoryResponseDto payementHistoryResponseDto = getIdfcSheetInfo(sb.toString());
		if (payementHistoryResponseDto != null) {
			amountThroughIDFC = payementHistoryResponseDto.getAmountThroughIdfc();
		}

		Double processingFee = oxyLoanRepo.getBorrowerMonthFeeSumValue(sb.toString());

		PaymentHistoryResponseDto response = new PaymentHistoryResponseDto();
		response.setAmountThroughEcs(ecsAmount == null ? 0d : Math.round(ecsAmount * 100.0) / 100.0);
		response.setAmountThroughPaymentScreenShot(
				paymentScreenShotAmount == null ? 0d : Math.round(paymentScreenShotAmount * 100.0) / 100.0);

		response.setAmountThroughPayU(payUAmount == null ? 0d : Math.round(payUAmount * 100.0) / 100.0);
		response.setPartPayment(partPaidAmount == null ? 0d : Math.round(partPaidAmount * 100.0) / 100.0);
		response.setFullPayment(fullPayment == null ? 0d : Math.round(fullPayment * 100.0) / 100.0);
		response.setAmountThroughIdfc(amountThroughIDFC == null ? 0d : Math.round(amountThroughIDFC * 100.0) / 100.0);
		response.setAmountOnFifthEcs(amountOnFifthEcs == null ? 0d : Math.round(amountOnFifthEcs * 100.0) / 100.0);
		response.setAmountOnTwentyEcs(amountOnTwentyEcs == null ? 0d : Math.round(amountOnTwentyEcs * 100.0) / 100.0);
		response.setIdfcAmountOnFifth(payementHistoryResponseDto.getIdfcAmountOnFifth() == null ? 0d
				: Math.round(payementHistoryResponseDto.getIdfcAmountOnFifth() * 100.0) / 100.0);
		response.setIdfcAmountOnTenth(payementHistoryResponseDto.getIdfcAmountOnTenth() == null ? 0d
				: Math.round(payementHistoryResponseDto.getIdfcAmountOnTenth() * 100.0 / 100.0));
		response.setPaymentScreenshotFullPayment(
				paymentScreenshotFullPayment == null ? 0d : Math.round(paymentScreenshotFullPayment * 100.0 / 100.0));
		response.setPaymentScreenshotPartPayment(
				paymentScreenshotPartPayment == null ? 0d : Math.round(paymentScreenshotPartPayment * 100.0 / 100.0));
		Double totalAmount = (response.getAmountThroughEcs() + response.getAmountThroughPaymentScreenShot()
				+ response.getAmountThroughPayU() + response.getAmountThroughIdfc());
		response.setTotalAmount(totalAmount == null ? 0d : Math.round(totalAmount * 100.0 / 100.0));
		Double totalEcsAmount = (response.getAmountThroughEcs() + response.getAmountThroughIdfc());
		response.setTotalEcsAmount(totalEcsAmount == null ? 0d : Math.round(totalEcsAmount * 100.0 / 100.0));
		if (!sreedeviHyundaiPayments.isEmpty()) {
			response.setAmountFromSreedeviHyundai(sreedeviHyundaiPayments);
		}
		if (processingFee != null) {
			response.setTotalProcessingFee(processingFee);
		}
		response.setListOfBorrowersIncludingFee(getBorrowersFeeBasedOnGivenDate(sb.toString()));
		if (!luckyHyndaiPayments.isEmpty()) {
			response.setAmountFromLuckyHyundai(luckyHyndaiPayments);
		}
		if (!tollGatesPayments.isEmpty()) {
			response.setAmountFromTollGates(tollGatesPayments);
		}
		if (!padiminiBollineniPayments.isEmpty()) {
			response.setAmountFromPadiminiBollineni(padiminiBollineniPayments);
		}
		if (!sharanyaLodgePayments.isEmpty()) {
			response.setAmountFromSharanyaLodge(sharanyaLodgePayments);
		}
		if (!gongineniAdvertisingPayments.isEmpty()) {
			response.setAmountFromGongineniAdvertising(gongineniAdvertisingPayments);
		}
		if (!citlyPayments.isEmpty()) {
			response.setAmountFromCitly(citlyPayments);
		}
		if (!lakshminrusimhamVenkataPayments.isEmpty()) {
			response.setAmountFromLakshminrusimhamVenkata(lakshminrusimhamVenkataPayments);
		}
		if (!saralTalwarPayments.isEmpty()) {
			response.setAmountFromSaralTalwar(saralTalwarPayments);
		}
		if (!mahenderReddyPayments.isEmpty()) {
			response.setAmountFromMahenderReddy(mahenderReddyPayments);
		}

		if (lenderPayuList != null && !lenderPayuList.isEmpty()) {
			response.setLenderPayuDetails(lenderPayuList);
		}
		Double sumOfPaytmAmount = paytmTransactionDetailsRepo.getPaytmTransactionBasedOnMonthAndYear(sb.toString());
		if (sumOfPaytmAmount != null) {
			response.setSumOfPaytmAmount(BigDecimal.valueOf(sumOfPaytmAmount).toBigInteger());

		}

		response.setTotalLenderFeeThroughPayu(totalPayuFeeCollected);
		response.setLenderFeeThroughPayu(lenderFreeWthPayuGivenMonth);

		return response;

	}

	public List<PaymentHistoryRequestDto> getAmountReceivedByLoanOwner(String givenDate, int ownerId,
			String ownerName) {

		List<PaymentHistoryRequestDto> paymentHistoryRequestDtoList = new ArrayList<PaymentHistoryRequestDto>();

		List<Object[]> paymentList = paymentUploadHistoryRepo.getLoanOwnerAmount(givenDate, ownerId, ownerName);

		for (Object[] obj : paymentList) {
			PaymentHistoryRequestDto paymentHistoryRequestDto = new PaymentHistoryRequestDto();
			paymentHistoryRequestDto.setLoanOwnerName(ownerName);
			paymentHistoryRequestDto.setAmount(obj[0] == null ? 0d : (double) obj[0]);
			List<PaymentUploadHistoryTableResponseDto> list = new ArrayList<PaymentUploadHistoryTableResponseDto>();
			if (obj[1] != null) {
				int userid = Integer.parseInt(obj[1].toString());
				List<PaymentUploadHistory> payments = paymentUploadHistoryRepo.getAmountsWithDate(givenDate, userid,
						ownerName);

				for (PaymentUploadHistory p : payments) {
					PaymentUploadHistoryTableResponseDto paymentUploadHistoryTableResponseDto = new PaymentUploadHistoryTableResponseDto();
					paymentUploadHistoryTableResponseDto.setPaid_date(p.getPaidDate().toString());
					paymentUploadHistoryTableResponseDto.setIndividualAmountByOwner(p.getAmount());
					list.add(paymentUploadHistoryTableResponseDto);
				}

				paymentHistoryRequestDto.setIndvidualPaymentsByOwner(list);
			}
			paymentHistoryRequestDtoList.add(paymentHistoryRequestDto);
		}
		return paymentHistoryRequestDtoList;

	}

	public List<BorrowerProcessingFeeResponseDto> getBorrowersFeeBasedOnGivenDate(String givenDate) {
		List<Object[]> listOfBorrowers = oxyLoanRepo.getListOfBorrowersWithFeeValue(givenDate);
		List<BorrowerProcessingFeeResponseDto> listOfBorrowersIncludingFee = new ArrayList<BorrowerProcessingFeeResponseDto>();
		if (listOfBorrowers != null && !listOfBorrowers.isEmpty()) {
			Iterator it = listOfBorrowers.iterator();
			while (it.hasNext()) {
				Object[] e = (Object[]) it.next();
				BorrowerProcessingFeeResponseDto borrowerProcessingFeeResponseDto = new BorrowerProcessingFeeResponseDto();
				borrowerProcessingFeeResponseDto.setBorrowerId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
				borrowerProcessingFeeResponseDto.setBorrowerName(e[1] == null ? " " : e[1].toString());
				borrowerProcessingFeeResponseDto
						.setProcessingFee(Double.parseDouble(e[2] == null ? "0" : e[2].toString()));
				listOfBorrowersIncludingFee.add(borrowerProcessingFeeResponseDto);
			}
		}
		return listOfBorrowersIncludingFee;

	}

	private PaymentHistoryResponseDto getIdfcSheetInfo(String date) {

		String fullDate[] = { "05-" + date, "10-" + date };
		Double sumOfAmount = 0.0;
		Double IdfcAmountOnFifth = 0.0;
		Double IdfcAmountOnTenth = 0.0;
		int count = 0;
		for (int i = 0; i < fullDate.length; i++) {
			try {
				String kyc = "IDFCPAYMENTS";
				FileResponse fileResponse = userServiceImpl.downloadExcelSheets(fullDate[i], kyc);
				URL url = new URL(fileResponse.getDownloadUrl());

				ReadableByteChannel readChannel = Channels.newChannel(url.openStream());

				String saveDirectory = LocalDate.now() + "data.xlsx";
				FileOutputStream fileOS = new FileOutputStream(saveDirectory);
				FileChannel writeChannel = fileOS.getChannel();
				writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);

				File file = new File(saveDirectory);
				if (file.exists()) {
					FileInputStream fis = new FileInputStream(file);
					XSSFWorkbook wb = new XSSFWorkbook(fis);
					XSSFSheet sheet = wb.getSheetAt(0);
					Iterator<Row> itr = sheet.iterator();
					while (itr.hasNext()) {
						Row row = itr.next();
						Iterator<Cell> cellIterator = row.cellIterator();
						while (cellIterator.hasNext()) {
							Cell cell = cellIterator.next();

							if (cell.getColumnIndex() == 3) {
								if (cell.getRowIndex() != 0) {
									Double value = cell.getNumericCellValue();
									sumOfAmount += value;
									if (count == 0) {
										IdfcAmountOnFifth = IdfcAmountOnFifth + value;

									} else {
										if (count == 1) {
											IdfcAmountOnTenth = IdfcAmountOnTenth + value;
										}

									}
								}
							}
						}
					}

				}

			} catch (FileNotFoundException f) {
				f.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			count = count + 1;
		}
		PaymentHistoryResponseDto paymentHistoryResponseDto = new PaymentHistoryResponseDto();
		paymentHistoryResponseDto.setAmountThroughIdfc(sumOfAmount);
		paymentHistoryResponseDto.setIdfcAmountOnFifth(IdfcAmountOnFifth);
		paymentHistoryResponseDto.setIdfcAmountOnTenth(IdfcAmountOnTenth);
		return paymentHistoryResponseDto;

	}

	public List<LenderPayuDto> lenderFeeStatisticsThroughPayu(String monthAndYear) {

		List<LenderPayuDto> responseList = new ArrayList<>();
		List<LenderPayuDetails> payuList = lenderPayuDetailsRepo.getListOfAmountsReceivedWithGivenDate(monthAndYear);

		if (payuList != null && !payuList.isEmpty()) {
			for (LenderPayuDetails payu : payuList) {

				LenderPayuDto dto = new LenderPayuDto();

				dto.setUserId(payu.getLenderUserId());
				dto.setAmount(payu.getAmount());
				dto.setDealId(payu.getDealId());
				dto.setLenderName(payu.getLenderName());
				dto.setPaymentDate(payu.getPaymentDate().toString());

				responseList.add(dto);

			}

		}
		return responseList;
	}

	@Override
	public LenderEscrowWalletDto getLenderScrowWalletDetails(String date1, String date2) {

		logger.info("Lender Scrow Wallet Details Method Starts ");

		LenderEscrowWalletDto lenderScrowWalletList = new LenderEscrowWalletDto();

		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			date = simpleDateFormat.parse(date1);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		Timestamp startDate = new Timestamp(date.getTime());

		Date newDate = null;
		try {
			newDate = simpleDateFormat.parse(date2);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Timestamp endDate = new Timestamp(newDate.getTime());

		List<Object[]> lenderWalletDetails = oxyLoanRepo.getLenderWalletDetails(startDate, endDate);

		FileOutputStream outFile = null;

		XSSFWorkbook workBook = new XSSFWorkbook();
		XSSFSheet spreadSheet = workBook.createSheet(LocalDate.now() + "LenderScrowWalletDetails.xls");
		CellStyle style = workBook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		XSSFFont font = workBook.createFont();// Create font
		font.setBold(true);
		style.setFont(font);

		Row row = spreadSheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue("UserId");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("Transaction Amount");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("Transaction Date");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("Lender Name");
		cell.setCellStyle(style);

		cell = row.createCell(4);
		cell.setCellValue("UniqueNumber");
		cell.setCellStyle(style);

		cell = row.createCell(5);
		cell.setCellValue("Registered On");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		int countForSno = 0;
		for (Object[] obj : lenderWalletDetails) {

			Row row1 = spreadSheet.createRow(++rowCount);

			Cell cell1 = row1.createCell(0);
			cell1.setCellValue(obj[0].toString());

			cell1 = row1.createCell(1);
			cell1.setCellValue(obj[1].toString());

			cell1 = row1.createCell(2);
			cell1.setCellValue(obj[2].toString());

			cell1 = row1.createCell(3);
			cell1.setCellValue(obj[3].toString());

			cell1 = row1.createCell(4);
			cell1.setCellValue(obj[4].toString());

			cell1 = row1.createCell(5);
			cell1.setCellValue(obj[5].toString());

			countForSno = countForSno + 1;

		}

		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		String filePath = LocalDate.now() + "LenderScrowWalletDetails.xls";
		File f = new File(filePath);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);
			if (f.exists()) {
				lenderScrowWalletList.setFileStatus("File saved");
			}

			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate = expectedDateFormat.format(current.getTime());
			templateContext.put("currentDate", expectedCurrentDate);

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(filePath);
			fileRequest.setFileType(FileType.LenderScrowWalletDetails);
			fileRequest.setFilePrifix(FileType.LenderScrowWalletDetails.name());

			try {
				FileResponse putFile = fileManagementService.putFile(fileRequest);

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

			String mailSubject = "Lender Scrow Wallet List";
			EmailRequest emailRequest = new EmailRequest(
					new String[] { "subbu@oxyloans.com", "ramadevi@oxyloans.com", "narendra@oxyloans.com" },
					"Lender-Scrow-Wallet-Details.template", templateContext, mailSubject,
					new String[] { f.getAbsolutePath() });
			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			} else {
				lenderScrowWalletList.setEmailStatus("Email Sent!");

				if (f.delete()) {
					System.out.println("File deleted");
				} else {
					System.out.println("Not deleted");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("Lender Scrow Wallet Details Method Ends ");
		return lenderScrowWalletList;
	}

	@Override
	public DurationChangeDto incresingDurationToExistingBorrower(int userId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public ActiveLenderResponseDto getAllLenderWalletInfo(PaginationRequestDto paginationRequestDto) {
		logger.info("------getAllLenderWalletInfo method Starts-----------");
		int pageNo = paginationRequestDto.getPageNo();
		int pageSize = paginationRequestDto.getPageSize();

		List<LenderWalletInfo> lenderWalletList = new ArrayList<LenderWalletInfo>();

		pageNo = (pageSize * (pageNo - 1));
		List<Integer> lenderIds = userRepo.getLenderIds(pageNo, pageSize);

		int countOfLenders = userRepo.getLenderIdsCountValue();

		for (int i = 0; i < lenderIds.size(); i++) {
			int id = lenderIds.get(i);

			LenderWalletInfo lenderDetails = getLenderWalletAmount(id);
			if (lenderDetails != null) {
				lenderWalletList.add(lenderDetails);
			}
		}

		ActiveLenderResponseDto response = new ActiveLenderResponseDto();
		response.setTotalCount(countOfLenders);
		response.setLenderWalletInfo(lenderWalletList);
		logger.info("--------getAllLenderWalletInfo method Ends--------");

		return response;
	}

	public LenderWalletInfo getLenderWalletAmount(int userId) {
		logger.info("---------------------   getLenderWalletAmount method start ...............");
		User user = userRepo.findById(userId).get();
		LenderWalletInfo lenderWalletInfo = new LenderWalletInfo();
		if (user != null) {
			if (!user.getPersonalDetails().getFirstName().isEmpty()
					&& !user.getPersonalDetails().getLastName().isEmpty()) {
				String name = user.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase()
						+ user.getPersonalDetails().getFirstName().substring(1) + " "
						+ user.getPersonalDetails().getLastName().substring(0, 1).toUpperCase()
						+ user.getPersonalDetails().getLastName().substring(1);

				lenderWalletInfo.setLenderName(name);

			} else {
				lenderWalletInfo.setLenderName("N/A");
			}
			lenderWalletInfo.setId(userId);
			lenderWalletInfo.setScrowAccountNumber("OXYLRV" + userId);

			Double lenderWalletCreditAmount = lenderOxyWalletNativeRepo.lenderWalletCreditAmount(userId);
			Double lenderWalletdebitAmount = lenderOxyWalletNativeRepo.lenderWalletDebitAmount(userId);
			Double lenderWalletInprocessAmount = lenderOxyWalletNativeRepo.lenderWalletInprocessAmount(userId);
			if (!(lenderWalletInprocessAmount != null && lenderWalletInprocessAmount > 0)) {
				lenderWalletInprocessAmount = 0d;
			}
			if (!(lenderWalletCreditAmount != null && lenderWalletCreditAmount > 0)) {
				lenderWalletCreditAmount = 0d;
			}
			if (!(lenderWalletdebitAmount != null && lenderWalletdebitAmount > 0)) {
				lenderWalletdebitAmount = 0d;
			}
			Double lenderWalletAmount = lenderWalletCreditAmount - lenderWalletdebitAmount;
			if (lenderWalletAmount != null && lenderWalletAmount > 0) {
				lenderWalletInfo.setWalletAmount(lenderWalletAmount - lenderWalletInprocessAmount);
			} else {
				lenderWalletInfo.setWalletAmount(0.0);
			}
			lenderWalletInfo.setStatus("APPROVED");
		}

		logger.info("---------------------   getLenderWalletAmount method end ...............");

		return lenderWalletInfo;

	}

	@Override
	public LenderLoansInformation gettingLenderLoanInformation(Integer currentMonth, Integer year) {

		logger.info("Lenders Loan Information Method Starts..............................");
		LenderEscrowWalletDto lenderScrowWalletList = new LenderEscrowWalletDto();

		List<LenderLoansInformation> listOfLendersEmiAmountBasedOnGivenDate = new ArrayList<LenderLoansInformation>();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		List<Object[]> lenderLoansList = oxyLoanRepo.gettingLendersLoanInfo(currentMonth, year);

		for (Object[] lenderInfo : lenderLoansList) {

			LenderLoansInformation lendersLoanInfo = new LenderLoansInformation();
			lendersLoanInfo.setLenderId(Integer.parseInt(lenderInfo[0] == null ? "0" : lenderInfo[0].toString()));
			lendersLoanInfo.setLenderName(lenderInfo[1] == null ? " " : lenderInfo[1].toString());
			lendersLoanInfo.setLenderAccountNumber(lenderInfo[2] == null ? " " : lenderInfo[2].toString());
			int borrowerId = Integer.parseInt(lenderInfo[3] == null ? "0" : lenderInfo[3].toString());
			lendersLoanInfo.setBorrowerId(borrowerId);
			User user = userRepo.findById(borrowerId).get();
			if (user != null) {
				lendersLoanInfo.setBorrowerName(
						user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName());
			}
			lendersLoanInfo.setDisbursementDate(lenderInfo[4].toString());
			lendersLoanInfo.setLoanId(lenderInfo[5] == null ? " " : lenderInfo[5].toString());
			lendersLoanInfo.setEmiAmount(Double.parseDouble(lenderInfo[6] == null ? "0" : lenderInfo[6].toString()));
			lendersLoanInfo.setEmiDueOn(lenderInfo[7].toString());

			if (lenderInfo[8] != null) {
				lendersLoanInfo.setEmiPaidOn(lenderInfo[8].toString());
			} else {
				lendersLoanInfo.setEmiPaidOn("NO");
			}
			listOfLendersEmiAmountBasedOnGivenDate.add(lendersLoanInfo);
		}

		FileOutputStream outFile = null;

		XSSFWorkbook workBook = new XSSFWorkbook();
		XSSFSheet spreadSheet = workBook.createSheet(LocalDate.now() + "CurrentMonthClosedAndActiveLenderInfo.xls");
		CellStyle style = workBook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		XSSFFont font = workBook.createFont();
		font.setBold(true);
		style.setFont(font);

		Row row = spreadSheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue("Lender Id");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("Lender Name");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("Lender AccountNumber");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("Borrower Id");
		cell.setCellStyle(style);

		cell = row.createCell(4);
		cell.setCellValue("Borrower Name");
		cell.setCellStyle(style);

		cell = row.createCell(5);
		cell.setCellValue("Loan Active Date");
		cell.setCellStyle(style);

		cell = row.createCell(6);
		cell.setCellValue("Loan Id");
		cell.setCellStyle(style);

		cell = row.createCell(7);
		cell.setCellValue("Emi Amount");
		cell.setCellStyle(style);

		cell = row.createCell(8);
		cell.setCellValue("Emi Date");
		cell.setCellStyle(style);

		cell = row.createCell(9);
		cell.setCellValue("Emi Paid Date");
		cell.setCellStyle(style);

		int rowCount = 0;

		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		int countForSno = 0;
		for (LenderLoansInformation LenderLoansInformation : listOfLendersEmiAmountBasedOnGivenDate) {

			Row row1 = spreadSheet.createRow(++rowCount);

			Cell cell1 = row1.createCell(0);
			cell1.setCellValue(LenderLoansInformation.getLenderId());

			cell1 = row1.createCell(1);
			cell1.setCellValue(LenderLoansInformation.getLenderName());

			cell1 = row1.createCell(2);
			cell1.setCellValue(LenderLoansInformation.getLenderAccountNumber());

			cell1 = row1.createCell(3);
			cell1.setCellValue(LenderLoansInformation.getBorrowerId());

			cell1 = row1.createCell(4);
			cell1.setCellValue(LenderLoansInformation.getBorrowerName());

			cell1 = row1.createCell(5);
			cell1.setCellValue(LenderLoansInformation.getDisbursementDate());

			cell1 = row1.createCell(6);
			cell1.setCellValue(LenderLoansInformation.getLoanId());

			cell1 = row1.createCell(7);
			cell1.setCellValue(LenderLoansInformation.getEmiAmount());

			cell1 = row1.createCell(8);
			cell1.setCellValue(LenderLoansInformation.getEmiDueOn());

			cell1 = row1.createCell(9);
			cell1.setCellValue(LenderLoansInformation.getEmiPaidOn());

		}

		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		String filePath = LocalDate.now() + "LendersLoansInfo.xls";
		File f = new File(filePath);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);
			if (f.exists()) {
				lenderScrowWalletList.setFileStatus("File saved");
			}

			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate = expectedDateFormat.format(current.getTime());
			templateContext.put("currentDate", expectedCurrentDate);

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(filePath);
			fileRequest.setFileType(FileType.LendersLoansInfo);
			fileRequest.setFilePrifix(FileType.LendersLoansInfo.name());

			try {
				FileResponse putFile = fileManagementService.putFile(fileRequest);

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

			String mailSubject = "Lenders Loan Information";

			EmailRequest emailRequest = new EmailRequest(
					new String[] { "ramadevi@oxyloans.com", "archana.n@oxyloans.com", "subbu@oxyloans.com" },
					"Lenders-Loans-Information.template", templateContext, mailSubject,
					new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			} else {
				lenderScrowWalletList.setFileStatus("Email Sent!");

				if (f.delete()) {
					System.out.println("File deleted");
				} else {
					System.out.println("Not deleted");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		LenderLoansInformation lendersInformation = new LenderLoansInformation();
		lendersInformation.setStatus("SUCCESS");

		logger.info("Lenders Loan Information Method Ends..............................");
		return lendersInformation;
	}

	@Override
	public LendersLoanInfo gettingLendersLoanInfoBasedOnMonthAndYear(Integer currentMonth, Integer year,
			PaginationRequestDto paginationRequestDto) {

		logger.info("gettingLendersLoanInfoBasedOnMonthAndYear Method Starts..................");

		int pageNo = paginationRequestDto.getPageNo();
		int pageSize = paginationRequestDto.getPageSize();

		List<LenderDetails> lenderDetailsListBasedOnYearAndMonth = new ArrayList<LenderDetails>();
		pageNo = (pageSize * (pageNo - 1));
		List<Object[]> lenderLoansInfo = oxyLoanRepo.getLenderLoansEmiInfo(currentMonth, year, pageNo, pageSize);

		Integer totalCountOfLenders = oxyLoanRepo.getCountOfTotalLenders(currentMonth, year);

		for (Object[] lendersInfo : lenderLoansInfo) {
			LenderDetails lenderDetails = new LenderDetails();

			lenderDetails.setLenderId((Integer) lendersInfo[0] == null ? 0 : (Integer) lendersInfo[0]);
			lenderDetails.setLenderName(lendersInfo[1].toString() == null ? "" : lendersInfo[1].toString());
			lenderDetails.setLenderAccountNumber(lendersInfo[2].toString() == null ? "" : lendersInfo[2].toString());
			lenderDetails.setSumOfEmiAmount(
					(Double) lendersInfo[3] == null ? 0.0 : Math.round((Double) lendersInfo[3] * 100.0 / 100.0));
			lenderDetails.setDueOnDate(lendersInfo[4].toString() == null ? "" : lendersInfo[4].toString());
			lenderDetailsListBasedOnYearAndMonth.add(lenderDetails);
		}
		LendersLoanInfo lendersInfoList = new LendersLoanInfo();

		lendersInfoList.setListOfLenderDetails(lenderDetailsListBasedOnYearAndMonth);
		lendersInfoList.setTotalCountOfLenders(totalCountOfLenders);

		logger.info("gettingLendersLoanInfoBasedOnMonthAndYear Method Ends..................");

		return lendersInfoList;

	}

	@Override
	public List<LenderLoansInformation> getLendersDetailedLoanInfo(Integer lenderId) {

		logger.info("getLendersDetailedLoanInfo Method Starts.........................");

		List<LenderLoansInformation> listOfLendersDetailedInfoBasedOnGivenDate = new ArrayList<LenderLoansInformation>();

		List<Object[]> listOfLendersEmiAmount = oxyLoanRepo.getLenderLoanInfoBasedOnLenderId(lenderId);

		for (Object[] object : listOfLendersEmiAmount) {
			LenderLoansInformation lendersLoanInfo = new LenderLoansInformation();

			lendersLoanInfo.setLenderId((Integer) object[0] == null ? 0 : (Integer) object[0]);
			lendersLoanInfo.setLenderName(object[1].toString() == null ? "" : object[1].toString());
			lendersLoanInfo.setLenderAccountNumber(object[2].toString() == null ? "" : object[2].toString());
			int borrowerId = Integer.parseInt(object[3] == null ? "0" : object[3].toString());
			lendersLoanInfo.setBorrowerId(borrowerId);
			User user = userRepo.findById(borrowerId).get();
			if (user != null) {
				lendersLoanInfo.setBorrowerName(
						user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName());
			}
			lendersLoanInfo.setDisbursementDate(object[4].toString());
			lendersLoanInfo.setLoanId(object[5].toString());
			lendersLoanInfo
					.setEmiAmount((Double) object[6] == null ? 0.0 : Math.round((Double) object[6] * 100.0 / 100.0));
			lendersLoanInfo.setEmiDueOn(object[7].toString());
			if (object[8] == null) {
				lendersLoanInfo.setEmiPaidOn("");
			} else {
				lendersLoanInfo.setEmiPaidOn(object[8].toString());
			}
			listOfLendersDetailedInfoBasedOnGivenDate.add(lendersLoanInfo);

		}

		logger.info("getLendersDetailedLoanInfo Method Ends.........................");
		return listOfLendersDetailedInfoBasedOnGivenDate;

	}

	@Override
	public LendersLoanInfo getLenderInfoBasedOnId(Integer lenderId, PaginationRequestDto paginationRequestDto) {
		logger.info("getLenderInfoBasedOnId method starts...................................");
		Integer month = paginationRequestDto.getMonth();
		Integer year = paginationRequestDto.getYear();

		List<LenderLoansInformation> lenderLoansInformation = new ArrayList<LenderLoansInformation>();
		List<LenderDetails> listOfLenderDetails = new ArrayList<LenderDetails>();
		Calendar calendar = Calendar.getInstance();

		if (month == null && year == null) {
			month = calendar.get(Calendar.MONTH) + 1;
			year = calendar.get(Calendar.YEAR);
		}
		List<Object[]> lenderInfo = oxyLoanRepo.gettingLendersLoanInformation(lenderId, month, year);

		List<Object[]> lenderLoanInfo = oxyLoanRepo.getLenderInfo(lenderId, month, year);

		if (lenderLoanInfo.isEmpty()) {
			throw new ActionNotAllowedException("Lender don't have any emi's in this month", ErrorCodes.USER_NOT_FOUND);
		}

		for (Object[] obj : lenderLoanInfo) {
			LenderDetails lenderDetails = new LenderDetails();
			lenderDetails.setLenderId((Integer) obj[0] == null ? 0 : (Integer) obj[0]);
			lenderDetails.setLenderName(obj[1].toString() == null ? "" : obj[1].toString());
			lenderDetails.setLenderAccountNumber(obj[2].toString() == null ? "" : obj[2].toString());

			lenderDetails
					.setSumOfEmiAmount((Double) obj[3] == null ? 0.0 : Math.round((Double) obj[3] * 100.0 / 100.0));

			listOfLenderDetails.add(lenderDetails);

		}

		for (Object[] obj : lenderInfo) {
			LenderLoansInformation lenderInformation = new LenderLoansInformation();

			int borrowerId = Integer.parseInt(obj[0] == null ? "0" : obj[0].toString());
			lenderInformation.setBorrowerId(borrowerId);
			User user = userRepo.findById(borrowerId).get();
			if (user != null) {
				lenderInformation.setBorrowerName(
						user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName());
			}
			lenderInformation.setDisbursementDate(obj[1].toString());
			lenderInformation.setLoanId(obj[2].toString() == null ? "" : obj[2].toString());
			lenderInformation.setEmiAmount((Double) obj[3] == null ? 0.0 : Math.round((Double) obj[3] * 100.0 / 100.0));

			lenderInformation.setEmiDueOn(obj[4].toString() == null ? "" : obj[4].toString());
			if (obj[5] == null) {
				lenderInformation.setEmiPaidOn("");
			} else {
				lenderInformation.setEmiPaidOn(obj[5].toString());
			}
			lenderLoansInformation.add(lenderInformation);

		}

		LendersLoanInfo lendersLoanInfo = new LendersLoanInfo();
		lendersLoanInfo.setListOfLenderLoansInformation(lenderLoansInformation);
		lendersLoanInfo.setListOfLenderDetails(listOfLenderDetails);

		logger.info("getLenderInfoBasedOnId method ends...................................");
		return lendersLoanInfo;
	}

	@Override
	@Transactional
	public StatusResponseDto rejectingLoanByLoanId(int loanId) {
		OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get();
		if (oxyLoan != null) {
			OxyLoan oxyLoanDetails = oxyLoanRepo.getDetailsByLoanIdToReject(loanId);
			if (oxyLoanDetails != null) {
				String loanIdInRequestDetails = "LN" + loanId;
				LoanRequest loanRequest = oxyLoanRequestRepo.findByLoanId(loanIdInRequestDetails);
				if (loanRequest != null) {
					loanRequest.setLoanStatus(LoanStatus.ADMINREJECTED);
					oxyLoanRequestRepo.save(loanRequest);
				}
				oxyLoan.setLoanStatus(LoanStatus.ADMINREJECTED);

				oxyLoanRepo.save(oxyLoan);
				int lenderUserId = oxyLoan.getLenderUserId();
				int borrowerUserId = oxyLoan.getBorrowerUserId();
				lenderBorrowerConversationRepo.deleteByLenderUserIdAndBorrowerUserId(lenderUserId, borrowerUserId);

				List<LoanEmiCard> loanEmiCard = loanEmiCardRepo.findByLoanId(loanId);
				if (loanEmiCard != null && !loanEmiCard.isEmpty()) {
					for (LoanEmiCard loanInfo : loanEmiCard) {
						loanInfo.setStatus(com.oxyloans.entity.loan.LoanEmiCard.Status.ADMINREJECTED);
						loanEmiCardRepo.save(loanInfo);
					}
				}

			} else {
				throw new OperationNotAllowedException("Please check loan id not in agreed or action state",
						ErrorCodes.NO_LOAN_FOUND_FOR_BORROWER);
			}

		} else {
			throw new OperationNotAllowedException("Please check loan id not present",
					ErrorCodes.NO_LOAN_FOUND_FOR_BORROWER);
		}
		StatusResponseDto statusResponseDto = new StatusResponseDto();
		statusResponseDto.setStatus("SUCCESSFULLY UPDATED");
		return statusResponseDto;

	}

	public EmiCalculationRequestDto getEmiCalcualtion(double disbursedAmount, int duration, int emisPaidCount,
			double rateOfInterest) {
		EmiCalculationRequestDto emiCalculationRequestDto = new EmiCalculationRequestDto();
		double interestAmount = 0.0;
		double principalAmount = 0.0;
		double emiAmount = 0.0;
		double totalEmiRecivied = 0.0;
		double totalInterestRecivied = 0.0;
		int count = 0;
		if (emisPaidCount > 0) {
			interestAmount = (disbursedAmount * rateOfInterest) / 100;
			principalAmount = disbursedAmount / duration;
			emiAmount = interestAmount + principalAmount;
			totalEmiRecivied = emiAmount * emisPaidCount;
			totalInterestRecivied = interestAmount * emisPaidCount;
			if (count == 0) {
				emiCalculationRequestDto.setFirstEmiAmount(Math.round(emiAmount));
				count = count + 1;
			}
		}
		emiCalculationRequestDto.setTotalEmiAmount(Math.round(totalEmiRecivied));
		emiCalculationRequestDto.setTotalInterestAmount(Math.round(totalInterestRecivied));

		return emiCalculationRequestDto;
	}

	@Override
	public LenderNocResponseDto getLenderProfitValue(int userId) {
		logger.info("getLenderProfitValue method start!!!!");
		LenderNocResponseDto lenderNocResponseDto = new LenderNocResponseDto();
		try {
			User user = userRepo.findById(userId).get();
			if (user != null) {
				List<Object[]> listOfLenderInfo = oxyLoanRepo.getLenderClosedLoansInfo(userId);
				Double totalProfitAmount = 0.0;
				if (listOfLenderInfo != null && !listOfLenderInfo.isEmpty()) {
					Iterator it = listOfLenderInfo.iterator();
					int count = 0;
					double rateOfInterest = 0.0;
					Integer duration = 0;
					Double disbursedMonth = 0.0;
					Integer emisPaidCount = 0;
					while (it.hasNext()) {
						Object[] e = (Object[]) it.next();

						double amountDisbursed = Double.parseDouble(e[1] == null ? "0" : e[1].toString());

						String loanOwner = e[6] == null ? " " : e[6].toString();
						duration = Integer.parseInt(e[4] == null ? "0" : e[4].toString());

						disbursedMonth = Double.parseDouble(e[7] == null ? "0.0" : e[7].toString());
						if (loanOwner.equalsIgnoreCase("Sai Sreedevi Hyundai")
								|| loanOwner.equalsIgnoreCase("Padimini Bollineni")
								|| (loanOwner.equalsIgnoreCase("TollGate"))
								|| loanOwner.equalsIgnoreCase("Sharanya Lodge")
								|| loanOwner.equalsIgnoreCase("Lucky Hyundai") || loanOwner.equalsIgnoreCase("Citly")
								|| loanOwner.equalsIgnoreCase("Saral Talwar")
								|| loanOwner.equalsIgnoreCase("Gongineni Advertising")) {
							rateOfInterest = 2;
						} else {
							if (loanOwner.equalsIgnoreCase("Lakshminrusimham Venkata")) {
								if (disbursedMonth == 2) {
									rateOfInterest = 2.5;
								} else if (disbursedMonth == 11) {
									rateOfInterest = 2;
								}
							}
							if (loanOwner.equalsIgnoreCase("Our Food")) {
								rateOfInterest = 3;
							}

						}
						if (rateOfInterest == 0.0) {
							rateOfInterest = 2;
						}
						int loanId = Integer.parseInt(e[2] == null ? "0" : e[2].toString());

						emisPaidCount = loanEmiCardRepo.getEmisPaidCountForClosedLoans(loanId);
						if (emisPaidCount == null) {
							emisPaidCount = 0;
						}
						EmiCalculationRequestDto emiCalculationRequestDto = getEmiCalcualtion(amountDisbursed, duration,
								emisPaidCount, rateOfInterest);

						double interestAmount = emiCalculationRequestDto.getTotalInterestAmount();

						totalProfitAmount = totalProfitAmount + interestAmount;

					}
				}

				List<Object[]> listOfLenderActiveLoans = oxyLoanRepo.getLenderActiveLoanInfo(userId);
				if (listOfLenderActiveLoans != null && !listOfLenderActiveLoans.isEmpty()) {
					Iterator it1 = listOfLenderActiveLoans.iterator();
					int count = 0;
					double rateOfInterest = 0.0;
					Integer duration = 0;
					Double disbursedMonth = 0.0;
					Integer emisPaidCount = 0;
					while (it1.hasNext()) {
						Object[] e1 = (Object[]) it1.next();

						double amountDisbursed = Double.parseDouble(e1[1] == null ? "0.0" : e1[1].toString());

						String loanOwner = e1[6] == null ? " " : e1[6].toString();
						duration = Integer.parseInt(e1[4] == null ? "0" : e1[4].toString());

						disbursedMonth = Double.parseDouble(e1[7] == null ? "0.0" : e1[7].toString());
						if (loanOwner.equalsIgnoreCase("Sai Sreedevi Hyundai")
								|| loanOwner.equalsIgnoreCase("Padimini Bollineni")
								|| (loanOwner.equalsIgnoreCase("TollGate"))
								|| loanOwner.equalsIgnoreCase("Sharanya Lodge")
								|| loanOwner.equalsIgnoreCase("Lucky Hyundai") || loanOwner.equalsIgnoreCase("Citly")
								|| loanOwner.equalsIgnoreCase("Saral Talwar")
								|| loanOwner.equalsIgnoreCase("Gongineni Advertising")) {
							rateOfInterest = 2;
						} else {
							if (loanOwner.equalsIgnoreCase("Lakshminrusimham Venkata")) {
								if (disbursedMonth == 2) {
									rateOfInterest = 2.5;
								} else if (disbursedMonth == 11) {
									rateOfInterest = 2;
								}
							}
							if (loanOwner.equalsIgnoreCase("Our Food")) {
								rateOfInterest = 3;
							}

						}
						if (rateOfInterest == 0.0) {
							rateOfInterest = 2;
						}
						int loanId = Integer.parseInt(e1[2] == null ? "0" : e1[2].toString());

						emisPaidCount = loanEmiCardRepo.getEmisPaidCountForActiveLoans(loanId);
						if (emisPaidCount == null) {
							emisPaidCount = 0;
						}
						EmiCalculationRequestDto emiCalculationRequestDto = getEmiCalcualtion(amountDisbursed, duration,
								emisPaidCount, rateOfInterest);

						double interestAmount = emiCalculationRequestDto.getTotalInterestAmount();

						totalProfitAmount = totalProfitAmount + interestAmount;

					}
				}

				if (totalProfitAmount > 0) {
					lenderNocResponseDto.setProfitValue(totalProfitAmount);
					lenderNocResponseDto.setLenderUniqueNumber(user.getUniqueNumber());
				} else {
					throw new OperationNotAllowedException("Till now you have not earned any profit value",
							ErrorCodes.SCORE_ERROR);
				}

			}

		} catch (Exception e) {

		}

		logger.info("getLenderProfitValue method end!!!!");

		return lenderNocResponseDto;
	}

	@Override
	public LenderNocResponseDto generateLenderProfitPdf(int userId) throws FileNotFoundException {
		logger.info("generateLenderProfitPdf method start!!!!");
		LenderNocResponseDto lenderNocResponseDetails = new LenderNocResponseDto();
		// try {
		LenderNocResponseDto lenderNocResponseDto = getLenderProfitValue(userId);
		if (lenderNocResponseDto != null) {
			User user = userRepo.findById(userId).get();
			if (user != null) {
				TemplateContext templateContext = new TemplateContext();
				templateContext.put("lenderName",
						user.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase()
								+ user.getPersonalDetails().getFirstName().substring(1) + " "
								+ user.getPersonalDetails().getLastName());
				templateContext.put("lenderId", user.getUniqueNumber());
				templateContext.put("mobileNumber", user.getMobileNumber());
				templateContext.put("email", user.getEmail());
				templateContext.put("totalProfitAmount", lenderNocResponseDto.getProfitValue());
				templateContext.put("accountNumber", "OXYLRV" + userId);
				Date date = new Date();
				String date1 = expectedDateFormat.format(date);
				templateContext.put("date", date1);
				String address = null;
				String panNumber = null;
				if (user.getPersonalDetails().getPermanentAddress() != null) {
					address = user.getPersonalDetails().getPermanentAddress();
				} else {
					address = "N/A";
				}
				if (user.getPersonalDetails().getPanNumber() != null) {
					panNumber = user.getPersonalDetails().getPanNumber();
				} else {
					panNumber = "N/A";
				}
				templateContext.put("address", address);
				templateContext.put("panNumber", panNumber);
				String outputFileName = LocalDate.now() + " " + userId + ".pdf";

				String lenderNoc = pdfEngine.generatePdf("agreement/lenderNoc.xml", templateContext, outputFileName);

				FileRequest fileRequest = new FileRequest();
				fileRequest.setInputStream(new FileInputStream(lenderNoc));
				fileRequest.setFileName(outputFileName);
				fileRequest.setFileType(FileType.lenderNoc);
				fileRequest.setFilePrifix(FileType.lenderNoc.name());

				FileResponse file1 = fileManagementService.getFile(fileRequest);
				try {
					FileResponse putFile = fileManagementService.putFile(fileRequest);

				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
				File file = new File(lenderNoc);
				boolean fileDeleted = file.delete();

				String urlDownload = file1.getDownloadUrl();

				String[] splitingTheUrl = urlDownload.split(Pattern.quote("?"));
				String url = splitingTheUrl[0];
				lenderNocResponseDetails.setLenderProfitUrl(url);
			}
		}

		return lenderNocResponseDetails;

	}

	@Override
	public LoanDisbursmentResponse updateingDisbursedDateBasedOnExcelSheet(String excelUploadedDate)
			throws PdfGeenrationException, IOException {
		LoanDisbursmentResponse loanDisbursmentResponse = new LoanDisbursmentResponse();

		int updatedCount = 0;
		int notUpdatedCount = 0;
		try {
			// String excelUploadedDate = "22-04-2021";
			String kyc = "DISBURSEDLOANS";
			FileResponse fileResponse = userServiceImpl.downloadExcelSheets(excelUploadedDate, kyc);

			if (fileResponse != null) {
				URL url = new URL(fileResponse.getDownloadUrl());

				ReadableByteChannel readChannel = Channels.newChannel(url.openStream());

				String saveDirectory = LocalDate.now() + "disbursedloans.xls";
				FileOutputStream fileOS = new FileOutputStream(saveDirectory);
				FileChannel writeChannel = fileOS.getChannel();
				writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);

				File file = new File(saveDirectory);

				FileInputStream fis = new FileInputStream(file);

				HSSFWorkbook wb = new HSSFWorkbook(fis);

				HSSFSheet sheet = wb.getSheetAt(0);

				for (Row row : sheet) {
					String uniqueNumber = null;
					double amount = 0.0;
					String date = null;
					for (Cell cell : row) {

						if (cell.getRowIndex() != 0) {
							if (cell.getStringCellValue().equalsIgnoreCase("50200025316770")) {
								break;
							} else {
								if (cell.getColumnIndex() == 2) {
									uniqueNumber = cell.getStringCellValue();

								} else {
									if (cell.getColumnIndex() == 4) {
										amount = Double.parseDouble(cell.getStringCellValue());

									}
									if (cell.getColumnIndex() == 6) {
										date = cell.getStringCellValue();
									}
								}

							}

						}

					}
					if (uniqueNumber != null && amount > 0) {
						User user = userRepo.findByUniqueNumber(uniqueNumber);
						if (user != null) {
							LoanRequest loanRequest = loanRequestRepo
									.findByUserIdAndParentRequestIdIsNull(user.getId());
							int borrowerParentRequestId = loanRequest.getId();
							Double amountBeforeDisburment = oxyLoanRepo
									.getDisbursmentAmountBeforeDisbursment(borrowerParentRequestId);
							if (amountBeforeDisburment != null) {
								if (amount == amountBeforeDisburment) {
									String a[] = date.split("-");
									String date1 = a[0];
									Calendar cal = Calendar.getInstance();
									int month = cal.get(Calendar.MONTH) + 1;
									int year = cal.get(Calendar.YEAR);
									StringBuffer disbursedDate = new StringBuffer();
									disbursedDate.append(date1).append('/').append(month).append('/').append(year);

									List<String> oxyloanList = oxyLoanRepo
											.getAllLoansByParentRequestId(borrowerParentRequestId);
									if (oxyloanList != null && !oxyloanList.isEmpty()) {
										for (String loanId : oxyloanList) {
											OxyLoan loan = oxyLoanRepo.findByLoanId(loanId);
											if (loan != null) {
												UserRequest userRequest = new UserRequest();
												userRequest.setDisbursedDate(disbursedDate.toString());
												userRequest.setAdminComments("DISBURSED");
												userRequest.setLoanId(loanId);
												UserResponse userResponse = sendReport(user.getId(), userRequest);

											}
										}
										updatedCount = updatedCount + 1;
									}

								} else {
									notUpdatedCount = notUpdatedCount + 1;

								}

							} else {
								notUpdatedCount = notUpdatedCount + 1;

							}

						}
					}

				}

			}

		} catch (Exception e) {
			logger.info("Exception in updateingDisbursedDateBasedOnExcelSheet method ");
		}

		loanDisbursmentResponse.setCountForUpdatedBorrowers(updatedCount);
		loanDisbursmentResponse.setCountForNotUpdatedBorrowers(notUpdatedCount);
		return loanDisbursmentResponse;

	}

	@Override
	public DisbursmentPendingResponseDto getDisbursmentPendingLoansDetails(PaginationRequestDto paginationRequestDto) {
		DisbursmentPendingResponseDto disbursmentPendingResponseDto = new DisbursmentPendingResponseDto();
		List<UserInformationResponseDto> listForPendingLoansInfo = new ArrayList<UserInformationResponseDto>();
		try {
			int pageSize = paginationRequestDto.getPageSize();
			int pageNo = paginationRequestDto.getPageNo();
			Integer countValueForPendingLoanIds = oxyLoanRepo.getDisbusmentPendingLoanIdsDetailsCount();
			if (countValueForPendingLoanIds != null) {
				disbursmentPendingResponseDto.setCountForDisbursmentPending(countValueForPendingLoanIds);
			}

			pageNo = (pageSize * (pageNo - 1));
			List<Object[]> listOfLoansInfo = oxyLoanRepo.getDisbusmentPendingLoanIdsDetails(pageSize, pageNo);
			for (Object[] e : listOfLoansInfo) {

				UserInformationResponseDto userInformationResponseDto = new UserInformationResponseDto();
				userInformationResponseDto.setLoanId(e[7] == null ? "" : e[7].toString());
				int lenderId = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
				User lenderDetails = userRepo.findById(lenderId).get();
				if (lenderDetails != null) {
					userInformationResponseDto.setLenderId(lenderDetails.getId());
					userInformationResponseDto.setLenderName(lenderDetails.getPersonalDetails().getFirstName() + " "
							+ lenderDetails.getPersonalDetails().getLastName());
					userInformationResponseDto.setLenderCity(lenderDetails.getCity());
					userInformationResponseDto.setLenderbankName(lenderDetails.getBankDetails().getBankName());
					userInformationResponseDto
							.setLenderAccountNumber(lenderDetails.getBankDetails().getAccountNumber());
					userInformationResponseDto.setLenderBranchName(lenderDetails.getBankDetails().getBranchName());
					userInformationResponseDto.setLenderIfscCode(lenderDetails.getBankDetails().getIfscCode());
				}
				int borrowerId = Integer.parseInt(e[2] == null ? "0" : e[2].toString());
				User borrowerDetails = userRepo.findById(borrowerId).get();
				if (borrowerDetails != null) {
					userInformationResponseDto.setBorrowerId(borrowerDetails.getId());
					userInformationResponseDto.setBorrowerName(borrowerDetails.getPersonalDetails().getFirstName() + " "
							+ borrowerDetails.getPersonalDetails().getLastName());
					userInformationResponseDto.setBorrowerCity(borrowerDetails.getCity());
					userInformationResponseDto.setBorrowerbankName(borrowerDetails.getBankDetails().getBankName());
					userInformationResponseDto
							.setBorrowerAccountNumber(borrowerDetails.getBankDetails().getAccountNumber());
					userInformationResponseDto.setBorrowerBranchName(borrowerDetails.getBankDetails().getBranchName());
					userInformationResponseDto.setBorrowerIfscCode(borrowerDetails.getBankDetails().getIfscCode());
				}
				userInformationResponseDto
						.setDisbursedAmount(Double.parseDouble(e[3] == null ? "0.0" : e[3].toString()));
				userInformationResponseDto.setDuration(Integer.parseInt(e[4] == null ? "0" : e[4].toString()));
				userInformationResponseDto.setLoanRequestId(e[5] == null ? " " : e[5].toString());
				userInformationResponseDto.setEnachType(e[6] == null ? " " : e[6].toString());
				userInformationResponseDto.setBorrowerKycDocuments(getKycDetails(borrowerId));
				userInformationResponseDto.setCommentsRequestDto(getCommentAndRateOfInterestFromRadhaSir(borrowerId));
				if (borrowerDetails.getExperianSummary() != null) {
					userInformationResponseDto.setOxyScore(borrowerDetails.getExperianSummary().getScore());
				}
				listForPendingLoansInfo.add(userInformationResponseDto);

			}
			disbursmentPendingResponseDto.setDisbursmentPendingInformation(listForPendingLoansInfo);
		} catch (Exception e) {
			logger.info("Exception in getDisbursmentPendingLoansDetails method");
		}

		return disbursmentPendingResponseDto;
	}

	@Override
	@Transactional
	public LenderReferenceResponse displayingAllLenderReferenceDetailsInfo(PaginationRequestDto pageRequestDto) {
		logger.info("displayingAllLenderReferenceDetailsInfo method starts..........................");
		List<LenderReferenceResponseDto> listOfAllReferenceDetails = new ArrayList<LenderReferenceResponseDto>();

		Integer pageNo = pageRequestDto.getPageNo();
		Integer pageSize = pageRequestDto.getPageSize();

		pageNo = (pageSize * (pageNo - 1));

		Integer totalCountOfReferees = lenderReferenceDetailsRepo.totalCountOfReferees();

		List<LenderReferenceDetails> referenceDetailsInfo = lenderReferenceDetailsRepo.findAll(pageNo, pageSize);

		for (LenderReferenceDetails obj : referenceDetailsInfo) {
			LenderReferenceResponseDto response = new LenderReferenceResponseDto();

			response.setRefereeEmail(obj.getRefereeEmail());

			Integer userId = (Integer) obj.getRefereeId();

			if (userId != 0) {
				User user = userRepo.findById(obj.getRefereeId()).get();
				if (user != null) {
					if (user.getPrimaryType().equals(User.PrimaryType.LENDER)) {
						response.setRefereeNewId(obj.getRefereeId() == 0 ? null : "LR" + obj.getRefereeId());
						response.setPrimaryType(user.getPrimaryType().toString());

					} else {
						response.setRefereeNewId(obj.getRefereeId() == 0 ? null : "BR" + obj.getRefereeId());
						response.setPrimaryType(user.getPrimaryType().toString());

					}
				}
			} else {
				response.setRefereeNewId("0");

			}

			Integer referrerId = (Integer) obj.getReferrerId();

			if (obj.getSource().equals(LenderReferenceDetails.Source.Partner)) {

				OxyUserType oxyUserType = oxyUserTypeRepo.findingPartnerDetails(obj.getReferrerId());
				if (oxyUserType != null) {
					response.setReferrerNewId("PR" + referrerId);
				}
			} else {
				User referrer = userRepo.findByIdNum(referrerId);
				if (referrer != null) {
					if (referrer.getPrimaryType().equals(User.PrimaryType.LENDER)) {
						response.setReferrerNewId("LR" + referrerId);
					} else {
						response.setReferrerNewId("BR" + referrerId);
					}
				}
			}

			response.setRefereeMobileNumber(obj.getRefereeMobileNumber());
			response.setRefereeName(obj.getRefereeName());

			Date referredOn = obj.getReferredOn();
			String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(referredOn);
			response.setReferredOn(modifiedDate);

			response.setReferrerId(obj.getReferrerId());
			response.setStatus(obj.getStatus().toString());

			/*
			 * Double referralBonus =
			 * lenderReferralBonusRepo.getBonusAmount(obj.getRefereeId());
			 * response.setAmount(referralBonus == null ? 0.0 : referralBonus);
			 */

			Double amount = 0.0;

			Double totalAmount = 0.0;

			List<LenderReferralBonusUpdated> listOfBonus = lenderReferralBonusUpdatedRepo
					.findingReferralBonusForRefereeAndReferrer(obj.getReferrerId(), obj.getRefereeId());
			if (listOfBonus != null && !listOfBonus.isEmpty()) {
				for (LenderReferralBonusUpdated referral : listOfBonus) {

					if (referral.getUpdatedBonus() != 0) {
						amount = referral.getUpdatedBonus();
					} else {
						amount = referral.getAmount();
					}
					totalAmount += amount;

				}
			}
			response.setAmount(totalAmount == null ? 0.0 : totalAmount);
			listOfAllReferenceDetails.add(response);
		}

		LenderReferenceResponse response = new LenderReferenceResponse();
		response.setListOfLenderReferenceDetails(listOfAllReferenceDetails);
		response.setCountOfReferees(totalCountOfReferees);

		logger.info("displayingAllLenderReferenceDetailsInfo method ends..........................");
		return response;

	}

	@Override
	public LoanDisbursmentResponse approvingAllLoansInApplicationLevel(int userId, UserRequest userRequest)
			throws PdfGeenrationException, IOException {
		LoanRequest loanrequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userRequest.getId());
		LoanDisbursmentResponse loanDisbursmentResponse = new LoanDisbursmentResponse();

		if (loanrequest != null) {
			List<String> oxyloanList = oxyLoanRepo.fetchApplicationLevelLoansToApprove(loanrequest.getId());
			Integer count = oxyLoanRepo.getCountOfLoansToApprove(loanrequest.getId());
			if (!oxyloanList.isEmpty()) {
				UserResponse userResponse = null;
				List<UserResponse> userResponseList = new ArrayList<>();
				for (String loanId : oxyloanList) {

					OxyLoan loan = oxyLoanRepo.findByLoanId(loanId);

					userRequest.setLoanId(loanId);
					userResponse = sendReport(userId, userRequest);

					userResponseList.add(userResponse);
				}

				if (count == oxyloanList.size()) {
					loanDisbursmentResponse.setStatus("UPDATED SUCCESSFULLY");
					loanDisbursmentResponse.setCountOfApprovedLoans(count);
				} else {
					int failedCount = count - oxyloanList.size();
					loanDisbursmentResponse.setStatus("All loans not Approved, " + failedCount + "loans failed");
					loanDisbursmentResponse.setCountOfApprovedLoans(oxyloanList.size());
				}
			}
		}
		return loanDisbursmentResponse;
	}

	@Override
	public UserInformationResponseDto searchByLoanIdDisbursmentPending(String loanId) {
		UserInformationResponseDto userInformationResponseDto = new UserInformationResponseDto();

		List<Object[]> oxyloan = oxyLoanRepo.searchByLoanIdAndDisburmentPending(loanId);
		if (oxyloan != null && !oxyloan.isEmpty()) {
			Iterator it = oxyloan.iterator();
			while (it.hasNext()) {
				Object[] e = (Object[]) it.next();

				int lenderId = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
				User lenderDetails = userRepo.findById(lenderId).get();
				if (lenderDetails != null) {
					userInformationResponseDto.setLenderId(lenderDetails.getId());
					userInformationResponseDto.setLenderName(lenderDetails.getPersonalDetails().getFirstName() + " "
							+ lenderDetails.getPersonalDetails().getLastName());
					userInformationResponseDto.setLenderCity(lenderDetails.getCity());
					userInformationResponseDto.setLenderbankName(lenderDetails.getBankDetails().getBankName());
					userInformationResponseDto
							.setLenderAccountNumber(lenderDetails.getBankDetails().getAccountNumber());
					userInformationResponseDto.setLenderBranchName(lenderDetails.getBankDetails().getBranchName());
					userInformationResponseDto.setLenderIfscCode(lenderDetails.getBankDetails().getIfscCode());
				}
				int borrowerId = Integer.parseInt(e[2] == null ? "0" : e[2].toString());
				User borrowerDetails = userRepo.findById(borrowerId).get();
				if (borrowerDetails != null) {
					userInformationResponseDto.setBorrowerId(borrowerDetails.getId());
					userInformationResponseDto.setBorrowerName(borrowerDetails.getPersonalDetails().getFirstName() + " "
							+ borrowerDetails.getPersonalDetails().getLastName());
					userInformationResponseDto.setBorrowerCity(borrowerDetails.getCity());
					userInformationResponseDto.setBorrowerbankName(borrowerDetails.getBankDetails().getBankName());
					userInformationResponseDto
							.setBorrowerAccountNumber(borrowerDetails.getBankDetails().getAccountNumber());
					userInformationResponseDto.setBorrowerBranchName(borrowerDetails.getBankDetails().getBranchName());
					userInformationResponseDto.setBorrowerIfscCode(borrowerDetails.getBankDetails().getIfscCode());
					userInformationResponseDto.setBorrowerKycDocuments(getKycDetails(borrowerId));
					userInformationResponseDto
							.setCommentsRequestDto(getCommentAndRateOfInterestFromRadhaSir(borrowerId));
					if (borrowerDetails.getExperianSummary() != null) {
						userInformationResponseDto.setOxyScore(borrowerDetails.getExperianSummary().getScore());
					}
					userInformationResponseDto
							.setDisbursedAmount(Double.parseDouble(e[3] == null ? "0" : e[3].toString()));
					userInformationResponseDto.setEnachType(e[4] == null ? " " : e[4].toString());
					userInformationResponseDto.setLoanId(e[0] == null ? " " : e[0].toString());
					userInformationResponseDto.setLoanRequestId(e[5] == null ? " " : e[5].toString());
				}
			}

		} else {
			throw new OperationNotAllowedException("Loan Id not there in L4 stage", ErrorCodes.ENITITY_NOT_FOUND);
		}

		return userInformationResponseDto;

	}

	@Override
	public DisbursmentPendingResponseDto searchByLenderIdAndBorrowerId(int userId) {
		User user = userRepo.findById(userId).get();
		DisbursmentPendingResponseDto disbursmentPendingResponseDto = new DisbursmentPendingResponseDto();
		List<UserInformationResponseDto> listForPendingLoansInfo = new ArrayList<UserInformationResponseDto>();

		if (user != null) {
			if (user.getPrimaryType() == PrimaryType.LENDER) {

				Integer count = oxyLoanRepo.countByLenderIdAndDisburmentPending(userId);
				List<Object[]> lenderPendingLoansInfo = oxyLoanRepo.searchByLenderIdAndDisburmentPending(userId);
				if (lenderPendingLoansInfo != null && !lenderPendingLoansInfo.isEmpty()) {
					Iterator it = lenderPendingLoansInfo.iterator();
					while (it.hasNext()) {
						Object[] e = (Object[]) it.next();
						UserInformationResponseDto userInformationResponseDto = new UserInformationResponseDto();
						int lenderId = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
						User lenderDetails = userRepo.findById(lenderId).get();
						if (lenderDetails != null) {
							userInformationResponseDto.setLenderId(lenderDetails.getId());
							userInformationResponseDto.setLenderName(lenderDetails.getPersonalDetails().getFirstName()
									+ " " + lenderDetails.getPersonalDetails().getLastName());
							userInformationResponseDto.setLenderCity(lenderDetails.getCity());
							userInformationResponseDto.setLenderbankName(lenderDetails.getBankDetails().getBankName());
							userInformationResponseDto
									.setLenderAccountNumber(lenderDetails.getBankDetails().getAccountNumber());
							userInformationResponseDto
									.setLenderBranchName(lenderDetails.getBankDetails().getBranchName());
							userInformationResponseDto.setLenderIfscCode(lenderDetails.getBankDetails().getIfscCode());
						}
						int borrowerId = Integer.parseInt(e[2] == null ? "0" : e[2].toString());
						User borrowerDetails = userRepo.findById(borrowerId).get();
						if (borrowerDetails != null) {
							userInformationResponseDto.setBorrowerId(borrowerDetails.getId());
							userInformationResponseDto
									.setBorrowerName(borrowerDetails.getPersonalDetails().getFirstName() + " "
											+ borrowerDetails.getPersonalDetails().getLastName());
							userInformationResponseDto.setBorrowerCity(borrowerDetails.getCity());
							userInformationResponseDto
									.setBorrowerbankName(borrowerDetails.getBankDetails().getBankName());
							userInformationResponseDto
									.setBorrowerAccountNumber(borrowerDetails.getBankDetails().getAccountNumber());
							userInformationResponseDto
									.setBorrowerBranchName(borrowerDetails.getBankDetails().getBranchName());
							userInformationResponseDto
									.setBorrowerIfscCode(borrowerDetails.getBankDetails().getIfscCode());
							userInformationResponseDto.setBorrowerKycDocuments(getKycDetails(borrowerId));
							userInformationResponseDto
									.setCommentsRequestDto(getCommentAndRateOfInterestFromRadhaSir(borrowerId));
							if (borrowerDetails.getExperianSummary() != null) {
								userInformationResponseDto.setOxyScore(borrowerDetails.getExperianSummary().getScore());
							}
							userInformationResponseDto
									.setDisbursedAmount(Double.parseDouble(e[3] == null ? "0" : e[3].toString()));
							userInformationResponseDto.setEnachType(e[4] == null ? " " : e[4].toString());
							userInformationResponseDto.setLoanId(e[0] == null ? " " : e[0].toString());
							userInformationResponseDto.setLoanRequestId(e[5] == null ? " " : e[5].toString());
						}
						listForPendingLoansInfo.add(userInformationResponseDto);
					}
					disbursmentPendingResponseDto.setCountForDisbursmentPending(count);
					disbursmentPendingResponseDto.setDisbursmentPendingInformation(listForPendingLoansInfo);

				} else {
					throw new OperationNotAllowedException("Lender not having disbursment pending loans",
							ErrorCodes.ENITITY_NOT_FOUND);
				}

			} else {
				Integer count = oxyLoanRepo.countByBorrowerIdAndDisburmentPending(userId);
				List<Object[]> borrowerPendingLoansInfo = oxyLoanRepo.searchByBorrowerIdAndDisburmentPending(userId);
				if (borrowerPendingLoansInfo != null && !borrowerPendingLoansInfo.isEmpty()) {
					Iterator it = borrowerPendingLoansInfo.iterator();
					while (it.hasNext()) {
						Object[] e = (Object[]) it.next();
						UserInformationResponseDto userInformationResponseDto = new UserInformationResponseDto();
						int lenderId = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
						User lenderDetails = userRepo.findById(lenderId).get();
						if (lenderDetails != null) {
							userInformationResponseDto.setLenderId(lenderDetails.getId());
							userInformationResponseDto.setLenderName(lenderDetails.getPersonalDetails().getFirstName()
									+ " " + lenderDetails.getPersonalDetails().getLastName());
							userInformationResponseDto.setLenderCity(lenderDetails.getCity());
							userInformationResponseDto.setLenderbankName(lenderDetails.getBankDetails().getBankName());
							userInformationResponseDto
									.setLenderAccountNumber(lenderDetails.getBankDetails().getAccountNumber());
							userInformationResponseDto
									.setLenderBranchName(lenderDetails.getBankDetails().getBranchName());
							userInformationResponseDto.setLenderIfscCode(lenderDetails.getBankDetails().getIfscCode());
						}
						int borrowerId = Integer.parseInt(e[2] == null ? "0" : e[2].toString());
						User borrowerDetails = userRepo.findById(borrowerId).get();
						if (borrowerDetails != null) {
							userInformationResponseDto.setBorrowerId(borrowerDetails.getId());
							userInformationResponseDto
									.setBorrowerName(borrowerDetails.getPersonalDetails().getFirstName() + " "
											+ borrowerDetails.getPersonalDetails().getLastName());
							userInformationResponseDto.setBorrowerCity(borrowerDetails.getCity());
							userInformationResponseDto
									.setBorrowerbankName(borrowerDetails.getBankDetails().getBankName());
							userInformationResponseDto
									.setBorrowerAccountNumber(borrowerDetails.getBankDetails().getAccountNumber());
							userInformationResponseDto
									.setBorrowerBranchName(borrowerDetails.getBankDetails().getBranchName());
							userInformationResponseDto
									.setBorrowerIfscCode(borrowerDetails.getBankDetails().getIfscCode());
							userInformationResponseDto.setBorrowerKycDocuments(getKycDetails(borrowerId));
							userInformationResponseDto
									.setCommentsRequestDto(getCommentAndRateOfInterestFromRadhaSir(borrowerId));
							if (borrowerDetails.getExperianSummary() != null) {
								userInformationResponseDto.setOxyScore(borrowerDetails.getExperianSummary().getScore());
							}
							userInformationResponseDto
									.setDisbursedAmount(Double.parseDouble(e[3] == null ? "0" : e[3].toString()));
							userInformationResponseDto.setEnachType(e[4] == null ? " " : e[4].toString());
							userInformationResponseDto.setLoanId(e[0] == null ? " " : e[0].toString());
							userInformationResponseDto.setLoanRequestId(e[5] == null ? " " : e[5].toString());
						}
						listForPendingLoansInfo.add(userInformationResponseDto);
					}
					disbursmentPendingResponseDto.setCountForDisbursmentPending(count);
					disbursmentPendingResponseDto.setDisbursmentPendingInformation(listForPendingLoansInfo);

				} else {
					throw new OperationNotAllowedException("Borrower not having disbursment pending loans",
							ErrorCodes.ENITITY_NOT_FOUND);
				}

			}
		}

		return disbursmentPendingResponseDto;

	}

	@Override
	public String sendingMailsToNormalEmisPendingFromAdmin() {
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String date = formatter.format(currentDate);
		// String date = "11-05-2021";
		String message = null;
		String kyc = null;
		try {

			List<String> listOfKyc = new ArrayList<String>();
			listOfKyc.add("NORMALEMISPENDING");
			listOfKyc.add("CRITICALEMISPENDING");
			for (int i = 0; i < listOfKyc.size(); i++) {
				kyc = listOfKyc.get(i);
				FileResponse fileResponse = userServiceImpl.downloadExcelSheets(date, kyc);
				if (fileResponse != null) {

					URL url = new URL(fileResponse.getDownloadUrl());
					ReadableByteChannel readChannel = Channels.newChannel(url.openStream());

					String saveDirectory = LocalDate.now() + kyc + ".xlxs";

					FileOutputStream fileOS = new FileOutputStream(saveDirectory);
					FileChannel writeChannel = fileOS.getChannel();
					writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);

					File file = new File(saveDirectory);

					FileInputStream fis = new FileInputStream(file);

					XSSFWorkbook wb = new XSSFWorkbook(fis);
					XSSFSheet sheet = wb.getSheetAt(0);
					Iterator<Row> itr = sheet.iterator();
					for (Row row : sheet) {

						String borrowerName = null;
						String borrowerUniqueNumber = null;
						String email = null;
						String mobileNumber = null;
						double emisPending = 0.0;
						int emi = 0;
						double mobileNumberInDouble = 0.0;
						BigInteger number = null;
						String crmName = null;
						double crmNumber = 0.0;
						Iterator<Cell> cellIterator = row.cellIterator();

						while (cellIterator.hasNext()) {
							Cell cell = cellIterator.next();
							if (cell.getRowIndex() != 0) {

								if (cell.getColumnIndex() == 2) {
									borrowerName = cell.getStringCellValue();
								}
								if (cell.getColumnIndex() == 3) {
									borrowerUniqueNumber = cell.getStringCellValue();
								}
								if (cell.getColumnIndex() == 4) {
									mobileNumberInDouble = cell.getNumericCellValue();
									number = BigDecimal.valueOf(mobileNumberInDouble).toBigInteger();
									mobileNumber = number.toString();

								}
								if (cell.getColumnIndex() == 7) {
									email = cell.getStringCellValue();
								}

								if (cell.getColumnIndex() == 8) {
									emisPending = cell.getNumericCellValue();
									emi = (int) Math.round(emisPending);
								}

								if (cell.getColumnIndex() == 5) {
									crmName = cell.getStringCellValue();
								}
								if (cell.getColumnIndex() == 10) {

									crmNumber = cell.getNumericCellValue();
									number = BigDecimal.valueOf(crmNumber).toBigInteger();

								}

							}
						}

						if (emisPending > 2 && number != null) {
							User user = userRepo.findByMobileNumber(mobileNumber);
							if (user != null) {
								if (kyc == "NORMALEMISPENDING") {
									TemplateContext templateContext = new TemplateContext();
									templateContext.put("borrowerName", borrowerName);
									templateContext.put("borrowerUniqueNumber", borrowerUniqueNumber);
									templateContext.put("emi", emi);
									templateContext.put("crmName", crmName);
									templateContext.put("number", number);

									String mailsubject = "Payments Reminder From OxyLoans";
									String emailTemplateName = "borrowers-normal-emis-pending.template";
									String displayerName = "Oxyloans Admin";

									EmailRequest emailRequest = new EmailRequest(new String[] { email },
											emailTemplateName, templateContext, mailsubject, displayerName);
									EmailResponse emailResponse = emailService.sendEmail(emailRequest);
									if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
										throw new MessagingException(emailResponse.getErrorMessage());
									}
									break;
								} else {
									if (kyc == "CRITICALEMISPENDING") {
										List<Object[]> loansForBorrower = loanEmiCardRepo
												.getLendersMappedToBorrower(user.getId());
										if (loansForBorrower != null && !loansForBorrower.isEmpty()) {
											Iterator it = loansForBorrower.iterator();
											String lenderName = null;
											Double amountPending = 0.0;
											while (it.hasNext()) {
												Object[] e = (Object[]) it.next();
												int loanId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
												int lenderId = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
												Integer pendingEmis = loanEmiCardRepo.getEmisPendingCount(loanId);
												if (pendingEmis != null) {
													Double emiAmount = loanEmiCardRepo
															.getEmiAmountPendingTillDate(loanId);
													if (emiAmount != null) {
														amountPending = emiAmount;
													}
													String firstName = e[2] == null ? "" : e[2].toString();
													String fN = firstName.substring(0, 1).toUpperCase()
															+ firstName.substring(1);
													String lastName = e[3] == null ? "" : e[3].toString();
													String lN = lastName.substring(0, 1).toUpperCase()
															+ lastName.substring(1);
													lenderName = fN + " " + lN;
													Double disbursedAmount = Double
															.parseDouble(e[5] == null ? "0" : e[5].toString());
													TemplateContext templateContext = new TemplateContext();
													templateContext.put("borrowerName", borrowerName);
													templateContext.put("borrowerUniqueNumber", borrowerUniqueNumber);

													templateContext.put("pendingEmis", pendingEmis);
													templateContext.put("amountPending", amountPending);

													String mailsubject = "Payments Reminder From OxyLoans";
													String emailTemplateName = "borrowers-critical-emis-pending.template";
													String displayerName = "Oxyloans Admin";

													EmailRequest emailRequest = new EmailRequest(new String[] { email },
															emailTemplateName, templateContext, mailsubject,
															displayerName);
													EmailResponse emailResponse = emailService.sendEmail(emailRequest);
													if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
														throw new MessagingException(emailResponse.getErrorMessage());
													}
													break;

												}

											}
										}

									}
								}
								// break;
							}

						}
					}

				}
			}

			message = "mail sent";

		} catch (

		Exception e) {
			logger.info("Exception in sendingMailsToNormalEmisPending method ");

		}
		return message;

	}

	@Override
	public String sendingMailsToCriticalEmisPending() {
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String date = null;

		date = "20-11-2021";

		String message = null;
		try {
			String kyc = "CRITICALEMISPENDING";

			FileResponse fileResponse = userServiceImpl.downloadExcelSheets(date, kyc);
			if (fileResponse != null) {

				URL url = new URL(fileResponse.getDownloadUrl());
				ReadableByteChannel readChannel = Channels.newChannel(url.openStream());

				String saveDirectory = LocalDate.now() + "criticalEmisPending.xlsx";

				FileOutputStream fileOS = new FileOutputStream(saveDirectory);
				FileChannel writeChannel = fileOS.getChannel();
				writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);

				File file = new File(saveDirectory);

				FileInputStream fis = new FileInputStream(file);

				XSSFWorkbook wb = new XSSFWorkbook(fis);
				XSSFSheet sheet = wb.getSheetAt(0);
				Iterator<Row> itr = sheet.iterator();
				for (Row row : sheet) {

					String borrowerName = null;
					String borrowerUniqueNumber = null;
					String email = null;
					String mobileNumber = null;
					double emisPending = 0.0;

					double mobileNumberInDouble = 0.0;
					double emisPendingAmount = 0.0;
					BigInteger number = null;
					int emi = 0;
					Iterator<Cell> cellIterator = row.cellIterator();

					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();
						if (cell.getRowIndex() != 0) {

							if (cell.getColumnIndex() == 2) {
								borrowerName = cell.getStringCellValue();
							}
							if (cell.getColumnIndex() == 3) {
								borrowerUniqueNumber = cell.getStringCellValue();
							}
							if (cell.getColumnIndex() == 4) {
								mobileNumberInDouble = cell.getNumericCellValue();
								number = BigDecimal.valueOf(mobileNumberInDouble).toBigInteger();
								mobileNumber = number.toString();

							}
							if (cell.getColumnIndex() == 7) {
								email = cell.getStringCellValue();
							}

							if (cell.getColumnIndex() == 8) {
								emisPending = cell.getNumericCellValue();
								emi = (int) Math.round(emisPending);
							}
							if (cell.getColumnIndex() == 9) {
								emisPendingAmount = cell.getNumericCellValue();

							}

						}
					}

					if (emisPending >= 1) {
						User user = userRepo.findByMobileNumber(mobileNumber);
						if (user != null) {

							List<Object[]> loansForBorrower = loanEmiCardRepo.getLendersMappedToBorrower(user.getId());
							if (loansForBorrower != null && !loansForBorrower.isEmpty()) {
								Iterator it = loansForBorrower.iterator();
								String lenderName = null;
								Double amountPending = 0.0;
								while (it.hasNext()) {
									Object[] e = (Object[]) it.next();
									int loanId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
									int lenderId = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
									// Integer pendingEmis = loanEmiCardRepo.getEmisPendingCount(loanId);
									// if (pendingEmis != null) {
									/*
									 * Double emiAmount = loanEmiCardRepo.getEmiAmountPendingTillDate(loanId); if
									 * (emiAmount != null) { amountPending = emiAmount; }
									 */
									String firstName = e[2] == null ? "" : e[2].toString();
									String fN = firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
									String lastName = e[3] == null ? "" : e[3].toString();
									String lN = lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
									lenderName = fN + " " + lN;
									Double disbursedAmount = Double.parseDouble(e[5] == null ? "0" : e[5].toString());
									TemplateContext templateContext = new TemplateContext();
									templateContext.put("borrowerName", borrowerName);
									templateContext.put("borrowerUniqueNumber", borrowerUniqueNumber);
									templateContext.put("email", email);
									templateContext.put("lenderName", lenderName);
									templateContext.put("pendingEmis", BigDecimal.valueOf(emisPending).toBigInteger());
									templateContext.put("amountPending",
											BigDecimal.valueOf(emisPendingAmount).toBigInteger());
									templateContext.put("disbursedAmount", disbursedAmount);

									String mailsubject = "Payments Reminder From OxyLoans Lender";
									String emailTemplateName = "borrowers-critical-emis-pending-from-lender.template";
									String displayerName = lenderName;

									EmailRequest emailRequest = new EmailRequest(new String[] { email },
											emailTemplateName, templateContext, mailsubject, displayerName);
									EmailResponse emailResponse = emailService.sendEmail(emailRequest);
									if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
										throw new MessagingException(emailResponse.getErrorMessage());
									}

									// }

								}

							}
							break;
						}

					}
				}

				message = "mail sent";
			}
		} catch (Exception e) {

		}
		return message;

	}

	@Override
	public String sendingMailsToNormalEmisPendingFromCRM() { // customer relationship Manager
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String date = formatter.format(currentDate);
		// String date = "11-05-2021";
		String message = null;

		try {

			String kyc = "NORMALEMISPENDING";

			FileResponse fileResponse = userServiceImpl.downloadExcelSheets(date, kyc);
			if (fileResponse != null) {

				URL url = new URL(fileResponse.getDownloadUrl());
				ReadableByteChannel readChannel = Channels.newChannel(url.openStream());

				String saveDirectory = LocalDate.now() + "normalEmisPending.xlsx";

				FileOutputStream fileOS = new FileOutputStream(saveDirectory);
				FileChannel writeChannel = fileOS.getChannel();
				writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);

				File file = new File(saveDirectory);

				FileInputStream fis = new FileInputStream(file);

				XSSFWorkbook wb = new XSSFWorkbook(fis);
				XSSFSheet sheet = wb.getSheetAt(0);
				Iterator<Row> itr = sheet.iterator();
				for (Row row : sheet) {

					String borrowerName = null;
					String borrowerUniqueNumber = null;
					String email = null;
					String mobileNumber = null;
					double emisPending = 0.0;
					String crmName = null;
					double crmNumber = 0.0;
					BigInteger number = null;
					double mobileNumberInDouble = 0.0;
					BigInteger borrowerNumber = null;
					String crmNumberInString = null;
					int emi = 0;
					Iterator<Cell> cellIterator = row.cellIterator();

					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();
						if (cell.getRowIndex() != 0) {

							if (cell.getColumnIndex() == 2) {
								borrowerName = cell.getStringCellValue();
							}
							if (cell.getColumnIndex() == 3) {
								borrowerUniqueNumber = cell.getStringCellValue();
							}
							if (cell.getColumnIndex() == 4) {
								mobileNumberInDouble = cell.getNumericCellValue();
								borrowerNumber = BigDecimal.valueOf(mobileNumberInDouble).toBigInteger();
								mobileNumber = borrowerNumber.toString();

							}
							if (cell.getColumnIndex() == 7) {
								email = cell.getStringCellValue();
							}

							if (cell.getColumnIndex() == 8) {
								emisPending = cell.getNumericCellValue();
								emi = (int) Math.round(emisPending);
							}
							if (cell.getColumnIndex() == 5) {
								crmName = cell.getStringCellValue();
							}
							if (cell.getColumnIndex() == 10) {

								crmNumber = cell.getNumericCellValue();
								number = BigDecimal.valueOf(crmNumber).toBigInteger();

							}

						}
					}

					if (emisPending > 2 && number != null) {
						User user = userRepo.findByMobileNumber(mobileNumber);
						if (user != null) {

							TemplateContext templateContext = new TemplateContext();

							templateContext.put("borrowerName", borrowerName);
							templateContext.put("borrowerUniqueNumber", borrowerUniqueNumber);
							templateContext.put("emi", emi);
							templateContext.put("crmName", crmName);
							templateContext.put("number", number);

							String mailsubject = "Payments Reminder From OxyLoans Manager";
							String emailTemplateName = "borrowers-normal-emis-pending-from-crm.template";
							String displayerName = crmName;

							EmailRequest emailRequest = new EmailRequest(new String[] { email }, emailTemplateName,
									templateContext, mailsubject, displayerName);
							EmailResponse emailResponse = emailService.sendEmail(emailRequest);
							if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
								throw new MessagingException(emailResponse.getErrorMessage());
							}
							break;
						}

					}

				}
			}

			message = "mail sent";

		} catch (Exception e) {

		}
		return message;

	}

	@Override
	public LoanDisbursmentResponse settingDisbursmentDatesForAllLoans(int userId, UserRequest userRequest)
			throws PdfGeenrationException, IOException {
		LoanDisbursmentResponse loanDisbursmentResponse = new LoanDisbursmentResponse();
		User user = userRepo.findById(userId).get();
		if (user != null) {
			Boolean enachActivationStatus = null;
			if (user.getLoanOwner() != null) {
				OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
						.findByDealName(user.getLoanOwner());
				if (oxyBorrowersDealsInformation != null) {
					enachActivationStatus = oxyBorrowersDealsInformation.getEnachStatus();
				}
			}
			LoanRequest loanrequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);

			if (loanrequest != null) {
				List<String> oxyloanList = null;
				if (enachActivationStatus != null) {
					if (enachActivationStatus.equals(true)) {
						oxyloanList = oxyLoanRepo.getAllLoansEnachActivatedByParentRequestId(loanrequest.getId());
					}
				} else {
					oxyloanList = oxyLoanRepo.getAllLoansByParentRequestId(loanrequest.getId());
				}
				int updatedCount = 0;
				if (!oxyloanList.isEmpty()) {
					for (String loanId : oxyloanList) {

						OxyLoan loan = oxyLoanRepo.findByLoanId(loanId);
						UserResponse userResponse = null;

						if (loan.getAdminComments() == null && loan.getBorrowerDisbursedDate() == null) {
							userRequest.setLoanId(loanId);
							userResponse = sendReport(userId, userRequest);
							updatedCount += 1;
						}

					}

				}
				loanDisbursmentResponse.setCountForUpdatedBorrowers(updatedCount);
				loanDisbursmentResponse.setStatus("UPDATED SUCCESSFULLY");

			}
		}
		return loanDisbursmentResponse;
	}

	@Override
	public BorrowerIndividualLoansCountResposeDto loansForBorrowerApplication(int borrowerId) {

		LoanRequest loanrequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(borrowerId);

		BorrowerIndividualLoansCountResposeDto borrowerIndividualLoansCountResposeDto = new BorrowerIndividualLoansCountResposeDto();

		if (loanrequest != null) {
			List<Object[]> loanrequest1 = oxyLoanRepo.fetchBorrowerApplicationLoans(loanrequest.getId());
			User borrowerUser = userRepo.findByIdNum(borrowerId);
			PersonalDetails borrowerPersonalDetails = borrowerUser.getPersonalDetails();
			if (!borrowerPersonalDetails.getFirstName().isEmpty() && !borrowerPersonalDetails.getLastName().isEmpty()) {
				borrowerIndividualLoansCountResposeDto.setBorrowerName(
						borrowerPersonalDetails.getFirstName() + " " + borrowerPersonalDetails.getLastName());
			} else {
				borrowerIndividualLoansCountResposeDto.setBorrowerName("N/A");
			}

			List<BorrowerLoansResponse> loanList = new ArrayList<>();
			Double disbursmentAmount = 0d;

			Integer loansCount = oxyLoanRepo.getTotalCountOfLoans(loanrequest.getId());
			for (Object[] obj : loanrequest1) {
				BorrowerLoansResponse response = new BorrowerLoansResponse();
				response.setLoanid(obj[0] == null ? " " : obj[0].toString());
				response.setLender_id(obj[1] == null ? null : (int) obj[1]);
				response.setDisbursment_amount(obj[2] == null ? 0d : (double) obj[2]);
				Double amount = (double) obj[2];
				disbursmentAmount += amount;

				if (obj[1] != null) {
					User user = userRepo.findByIdNum((int) obj[1]);
					PersonalDetails p = user.getPersonalDetails();
					if (!p.getFirstName().isEmpty() && !p.getLastName().isEmpty()) {
						response.setLender_name(p.getFirstName() + " " + p.getLastName());
					} else {
						response.setLender_name("N/A");
					}
					loanList.add(response);
				}
			}

			borrowerIndividualLoansCountResposeDto.setBorrowerId(borrowerId);
			borrowerIndividualLoansCountResposeDto.setApplicationId(loanrequest.getId());
			borrowerIndividualLoansCountResposeDto.setDuration(loanrequest.getDuration());
			borrowerIndividualLoansCountResposeDto.setDisbursmentAmount(loanrequest.getDisbursmentAmount());
			borrowerIndividualLoansCountResposeDto
					.setAmountDisbursed(loanrequest.getDisbursmentAmount() - disbursmentAmount);
			borrowerIndividualLoansCountResposeDto.setAmountToBeDisbursed(disbursmentAmount);
			borrowerIndividualLoansCountResposeDto.setRateOfInterest(loanrequest.getRateOfInterest());
			borrowerIndividualLoansCountResposeDto.setDurationType(loanrequest.getDurationType().toString());
			borrowerIndividualLoansCountResposeDto.setCount(loansCount);
			borrowerIndividualLoansCountResposeDto.setBorrowersLoansResponse(loanList);

		}
		return borrowerIndividualLoansCountResposeDto;
	}

	@Override
	public BorrowersLoanOwnerInformation getLoanOwners() {

		BorrowersLoanOwnerInformation borrowersLoanOwnerInformation = new BorrowersLoanOwnerInformation();
		List<String> listofLoanOwners = userRepo.getLoanOwners();
		List<BorrowersLoanOwnerNames> loanOwners = new ArrayList<BorrowersLoanOwnerNames>();
		int count = 0;
		int totalNumberOfLoanOwners = 0;
		int sizeValueOfdeals = 0;
		try {
			if (listofLoanOwners != null && !listofLoanOwners.isEmpty()) {

				for (String ownerName : listofLoanOwners) {
					BorrowersLoanOwnerNames borrowersLoanOwnerNames = new BorrowersLoanOwnerNames();
					borrowersLoanOwnerNames.setLoanOwner(ownerName);
					loanOwners.add(borrowersLoanOwnerNames);
					count = count + 1;
				}
				if (count == listofLoanOwners.size()) {
					List<String> listOfdealNames = oxyBorrowersDealsInformationRepo.getListOfDealsNames();
					if (listOfdealNames != null) {
						sizeValueOfdeals = listOfdealNames.size();
						for (String value : listOfdealNames) {
							BorrowersLoanOwnerNames borrowersLoanOwnerName = new BorrowersLoanOwnerNames();
							borrowersLoanOwnerName.setLoanOwner(value);
							loanOwners.add(borrowersLoanOwnerName);
						}
					}
				}
				totalNumberOfLoanOwners = listofLoanOwners.size() + sizeValueOfdeals;
				borrowersLoanOwnerInformation.setLoanOwnersCount(totalNumberOfLoanOwners);
				borrowersLoanOwnerInformation.setBorrowersLoanOwnerNames(loanOwners);
			}

		} catch (Exception e) {
			logger.info("Exception in  gettingBorrowerEmisInfoBasedOnLoanOwner method ");
		}

		return borrowersLoanOwnerInformation;

	}

	@Override
	public BorrowersLoanOwnerInformation gettingBorrowerDetailedLoanInfo(String loanOwner) {

		BorrowersLoanOwnerInformation borrowersLoanOwnerInformation = new BorrowersLoanOwnerInformation();
		List<BorrowersLoanInfo> listOfBorrowers = new ArrayList<BorrowersLoanInfo>();
		try {
			List<Object[]> listOfBorrowersByLoanOwners = userRepo.getBorrowersMappedToLoanOwner(loanOwner);
			if (listOfBorrowersByLoanOwners != null && !listOfBorrowersByLoanOwners.isEmpty()) {
				Iterator it = listOfBorrowersByLoanOwners.iterator();
				while (it.hasNext()) {
					Object[] e = (Object[]) it.next();
					BorrowersLoanInfo borrowersDetails = new BorrowersLoanInfo();
					borrowersDetails.setBorrowerId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
					borrowersDetails.setFirstName(e[1] == null ? " " : e[1].toString());
					borrowersDetails.setLastName(e[2] == null ? " " : e[2].toString());
					listOfBorrowers.add(borrowersDetails);
				}

			}
			borrowersLoanOwnerInformation.setLoanOwnersCount(listOfBorrowersByLoanOwners.size());
			borrowersLoanOwnerInformation.setListOfBorrowersMappedToLoanOwner(listOfBorrowers);
		} catch (Exception e) {

		}
		return borrowersLoanOwnerInformation;

	}

	@Override
	public OxyTransactionDetailsFromExcelSheetsResponse readingLendersOutGoingAmount(String date, String deditedType) {

		OxyTransactionDetailsFromExcelSheetsResponse oxyTransactionDetailsFromExcelSheetsResponse = new OxyTransactionDetailsFromExcelSheetsResponse();

		Integer count = 0;

		String newDate = null;
		String transactionDate = date;
		String[] dateArray = transactionDate.split("-");
		StringBuffer sb = new StringBuffer();
		int length = dateArray.length;
		for (int i = 0; i < dateArray.length; i++) {
			if (i == length - 1) {
				sb.append(dateArray[i]);
			} else {
				sb.append(dateArray[i] + "/");
			}
		}
		newDate = sb.toString();
		try {
			if (deditedType.equalsIgnoreCase(OxyTransactionDetailsFromExcelSheets.DebitedTowords.LENDEREMI.toString())
					|| deditedType.equalsIgnoreCase(
							OxyTransactionDetailsFromExcelSheets.DebitedTowords.LENDERINTEREST.toString())
					|| deditedType.equalsIgnoreCase(
							OxyTransactionDetailsFromExcelSheets.DebitedTowords.LENDERPRINCIPAL.toString())
					|| deditedType.equalsIgnoreCase(
							OxyTransactionDetailsFromExcelSheets.DebitedTowords.LENDERWITHDRAW.toString())
					|| deditedType.equalsIgnoreCase(
							OxyTransactionDetailsFromExcelSheets.DebitedTowords.LENDERRELEND.toString())
					|| deditedType.equalsIgnoreCase(
							OxyTransactionDetailsFromExcelSheets.DebitedTowords.LENDERPOOLINGINTEREST.toString())) {
				OxyTransactionDetailsFromExcelSheets oxyTransactionDetails = oxyTransactionDetailsFromExcelSheetsRepo
						.getTransactionExcelSheetByDateAndType(deditedType, newDate);
				if (oxyTransactionDetails != null) {
					FileResponse fileResponse = userServiceImpl.downloadExcelSheets(date, deditedType);
					if (fileResponse != null) {

						URL url = new URL(fileResponse.getDownloadUrl());
						ReadableByteChannel readChannel = Channels.newChannel(url.openStream());

						String saveDirectory = LocalDate.now() + deditedType + ".xls";

						FileOutputStream fileOS = new FileOutputStream(saveDirectory);
						FileChannel writeChannel = fileOS.getChannel();
						writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);

						File file = new File(saveDirectory);

						OPCPackage fis = OPCPackage.open(file);

						// FileInputStream fis = new FileInputStream(file);

						// HSSFWorkbook wb = new HSSFWorkbook();
						XSSFWorkbook wb = new XSSFWorkbook(file);

						XSSFSheet sheet = wb.getSheetAt(0);

						for (Row row : sheet) {

							Iterator<Cell> cellIterator = row.cellIterator();
							String accountNumber = null;
							double amount = 0.0;
							double paidamount = 0.0;
							String amountPaidDate = null;
							String amountPaidBorrower = null;
							String remarks = null;
							String amountInString = null;
							String lenderId = null;
							String debitAccount = null;
							String lenderName = null;
							String payMode = null;
							String lenderIfsc = null;
							String dealId = null;
							while (cellIterator.hasNext()) {
								Cell cell = cellIterator.next();
								if (cell.getRowIndex() != 0) {
									if (cell.getColumnIndex() == 0) {
										switch (cell.getCellType()) {
										case Cell.CELL_TYPE_NUMERIC:
											amount = cell.getNumericCellValue();
											debitAccount = String.valueOf(amount);
											break;
										case Cell.CELL_TYPE_STRING:
											debitAccount = cell.getStringCellValue();
											break;
										}

									}

									if (cell.getColumnIndex() == 1) {
										switch (cell.getCellType()) {
										case Cell.CELL_TYPE_NUMERIC:
											amount = cell.getNumericCellValue();
											accountNumber = String.valueOf(amount);
											break;
										case Cell.CELL_TYPE_STRING:
											accountNumber = cell.getStringCellValue();
											break;
										}

									}
									if (cell.getColumnIndex() == 2) {

										switch (cell.getCellType()) {
										case Cell.CELL_TYPE_NUMERIC:
											amount = cell.getNumericCellValue();
											lenderName = String.valueOf(amount);
											break;
										case Cell.CELL_TYPE_STRING:
											lenderName = cell.getStringCellValue();
											break;
										}

									}

									if (cell.getColumnIndex() == 3) {
										switch (cell.getCellType()) {
										case Cell.CELL_TYPE_NUMERIC:
											paidamount = cell.getNumericCellValue();
											break;
										case Cell.CELL_TYPE_STRING:
											amountInString = cell.getStringCellValue();
											paidamount = Double.parseDouble(amountInString);
											break;
										}
									}

									if (cell.getColumnIndex() == 4) {
										switch (cell.getCellType()) {
										case Cell.CELL_TYPE_NUMERIC:
											amount = cell.getNumericCellValue();
											payMode = String.valueOf(amount);
											break;
										case Cell.CELL_TYPE_STRING:
											payMode = cell.getStringCellValue();
											break;
										}

									}

									if (cell.getColumnIndex() == 5) {
										switch (cell.getCellType()) {
										case Cell.CELL_TYPE_NUMERIC:
											amount = cell.getNumericCellValue();
											amountPaidDate = String.valueOf(amount);
											break;
										case Cell.CELL_TYPE_STRING:
											amountPaidDate = cell.getStringCellValue();
											break;
										}

									}
									if (cell.getColumnIndex() == 6) {

										switch (cell.getCellType()) {
										case Cell.CELL_TYPE_NUMERIC:
											amount = cell.getNumericCellValue();
											lenderIfsc = String.valueOf(amount);
											break;
										case Cell.CELL_TYPE_STRING:
											lenderIfsc = cell.getStringCellValue();
											break;
										}

									}
									if (cell.getColumnIndex() == 17) {
										switch (cell.getCellType()) {
										case Cell.CELL_TYPE_NUMERIC:
											amount = cell.getNumericCellValue();
											dealId = String.valueOf(amount);
											break;
										case Cell.CELL_TYPE_STRING:
											dealId = cell.getStringCellValue();
											break;
										}

									}

									if (cell.getColumnIndex() == 18) {
										switch (cell.getCellType()) {
										case Cell.CELL_TYPE_NUMERIC:
											amount = cell.getNumericCellValue();
											lenderId = String.valueOf(amount);
											break;
										case Cell.CELL_TYPE_STRING:
											lenderId = cell.getStringCellValue();
											break;
										}

									}

									if (cell.getColumnIndex() == 19) {
										switch (cell.getCellType()) {
										case Cell.CELL_TYPE_NUMERIC:
											amount = cell.getNumericCellValue();
											amountPaidBorrower = String.valueOf(amount);
											break;
										case Cell.CELL_TYPE_STRING:
											amountPaidBorrower = cell.getStringCellValue();
											break;
										}

									}

									if (cell.getColumnIndex() == 20) {
										switch (cell.getCellType()) {
										case Cell.CELL_TYPE_NUMERIC:
											amount = cell.getNumericCellValue();
											remarks = String.valueOf(amount);
											break;
										case Cell.CELL_TYPE_STRING:
											remarks = cell.getStringCellValue();
											break;
										}

									}

								}

							}
							if (remarks != null) {

								String s = lenderId.substring(2);
								Integer id = Integer.parseInt(s);

								User user = userRepo.getDetailsById(id);
								if (user != null) {
									String a[] = amountPaidDate.split("-");
									String date1 = a[0];

									String month = a[1];
									String year = a[2];

									Date dateOfMonth = null;
									try {
										dateOfMonth = new SimpleDateFormat("MMMM").parse(month);
									} catch (ParseException e) {
										e.printStackTrace();
									}
									Calendar cal = Calendar.getInstance();
									cal.setTime(dateOfMonth);
									int monthNumber = cal.get(Calendar.MONTH) + 1;
									StringBuffer disbursedDate = new StringBuffer();
									disbursedDate.append(date1).append('/').append(monthNumber).append('/')
											.append(year);
									Date paidDate = expectedDateFormat.parse(disbursedDate.toString());

									LenderDetailsFromExcelSheet lenderDetailsFromExcelSheet = new LenderDetailsFromExcelSheet();
									lenderDetailsFromExcelSheet.setAccountNumber(accountNumber);
									lenderDetailsFromExcelSheet.setDebitAccount(debitAccount);
									lenderDetailsFromExcelSheet.setLenderName(lenderName);
									lenderDetailsFromExcelSheet.setPayMode(payMode);
									lenderDetailsFromExcelSheet.setLenderIfsc(lenderIfsc);
									Gson gson = new Gson();
									String json = gson.toJson(lenderDetailsFromExcelSheet);

									LendersReturns lendersReturns = new LendersReturns();
									lendersReturns.setUserId(id);
									lendersReturns.setAmount(paidamount);
									lendersReturns.setPaidDate(expectedDateFormat.parse(disbursedDate.toString()));
									lendersReturns.setBorrowerName(amountPaidBorrower);
									lendersReturns.setRemarks(remarks);
									lendersReturns.setAmountType(oxyTransactionDetails.getDebitedTowords().toString());
									String[] s1 = dealId.split(".0");
									lendersReturns.setDealId(Integer.parseInt(s1[0].toString()));
									lendersReturns.setLenderInfomationExcelSheet(json);
									count = count + 1;
									lendersReturnsRepo.save(lendersReturns);

								} else if (user == null) {
									continue;
								}
							}
						}
					}
				}
			} else {
				if (deditedType.equalsIgnoreCase(
						OxyTransactionDetailsFromExcelSheets.DebitedTowords.POOLINGLENDERS.toString())) {
					OxyTransactionDetailsFromExcelSheets oxyTransactionDetails = oxyTransactionDetailsFromExcelSheetsRepo
							.getTransactionExcelSheetByDateAndType(deditedType, newDate);
					if (oxyTransactionDetails != null) {
						FileResponse fileResponse = userServiceImpl.downloadExcelSheets(date, deditedType);
						if (fileResponse != null) {

							URL url = new URL(fileResponse.getDownloadUrl());
							ReadableByteChannel readChannel = Channels.newChannel(url.openStream());

							String saveDirectory = LocalDate.now() + deditedType + ".xls";

							FileOutputStream fileOS = new FileOutputStream(saveDirectory);
							FileChannel writeChannel = fileOS.getChannel();
							writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);

							File file = new File(saveDirectory);

							OPCPackage fis = OPCPackage.open(file);

							XSSFWorkbook wb = new XSSFWorkbook(file);

							XSSFSheet sheet = wb.getSheetAt(0);

							for (Row row : sheet) {

								Iterator<Cell> cellIterator = row.cellIterator();

								String amountInString = null;

								String lenderName = null;
								double currnetAmount = 0.0;
								double monthlyInterest = 0.0;
								double interestDate = 0.0;

								while (cellIterator.hasNext()) {
									Cell cell = cellIterator.next();

									if (cell.getRowIndex() != 0) {
										if (cell.getColumnIndex() == 1) {
											lenderName = cell.getStringCellValue();
										}
										if (cell.getColumnIndex() == 2) {
											switch (cell.getCellType()) {
											case Cell.CELL_TYPE_NUMERIC:
												interestDate = cell.getNumericCellValue();
												break;
											case Cell.CELL_TYPE_STRING:
												amountInString = cell.getStringCellValue();
												interestDate = Double.parseDouble(amountInString);
												break;
											}

										}
										if (cell.getColumnIndex() == 3) {
											switch (cell.getCellType()) {
											case Cell.CELL_TYPE_NUMERIC:
												currnetAmount = cell.getNumericCellValue();
												break;
											case Cell.CELL_TYPE_STRING:
												amountInString = cell.getStringCellValue();
												currnetAmount = Double.parseDouble(amountInString);
												break;
											}
										}
										if (cell.getColumnIndex() == 4) {
											switch (cell.getCellType()) {
											case Cell.CELL_TYPE_NUMERIC:
												monthlyInterest = cell.getNumericCellValue();
												break;
											case Cell.CELL_TYPE_STRING:
												amountInString = cell.getStringCellValue();
												monthlyInterest = Double.parseDouble(amountInString);
												break;
											}
										}

									}

								}
								if (monthlyInterest > 0.0) {
									OxyPoolingAccountDetails oxyPoolingAccountDetails = new OxyPoolingAccountDetails();
									oxyPoolingAccountDetails.setName(lenderName);
									oxyPoolingAccountDetails.setInterestDate((int) interestDate);
									oxyPoolingAccountDetails.setCurrentAmount(currnetAmount);
									oxyPoolingAccountDetails.setInterestAmount(monthlyInterest);
									oxyPoolingAccountDetailsRepo.save(oxyPoolingAccountDetails);
									count = count + 1;
								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			logger.info(e);
		}
		oxyTransactionDetailsFromExcelSheetsResponse.setCountValueForUpdateLender(count);

		return oxyTransactionDetailsFromExcelSheetsResponse;

	}

	@Override
	public OxyTransactionDetailsFromExcelSheetsResponse updatingExcelSheet(
			OxyTransactionDetailsFromExcelSheetsRequest oxyTransactionDetailsFromExcelSheetsRequest) {
		OxyTransactionDetailsFromExcelSheetsResponse oxyTransactionDetailsFromExcelSheetsResponse = new OxyTransactionDetailsFromExcelSheetsResponse();
		String paidDate = null;
		String transactionDate = oxyTransactionDetailsFromExcelSheetsRequest.getTransferredDate();
		String[] date = transactionDate.split("-");
		StringBuffer sb = new StringBuffer();
		int length = date.length;
		for (int i = 0; i < date.length; i++) {
			if (i == length - 1) {
				sb.append(date[i]);
			} else {
				sb.append(date[i] + "/");
			}
		}
		paidDate = sb.toString();
		OxyTransactionDetailsFromExcelSheets oxyTransactionDetails = oxyTransactionDetailsFromExcelSheetsRepo
				.getTransactionExcelSheetByName(oxyTransactionDetailsFromExcelSheetsRequest.getFileName()); // checking
																											// by excel
																											// sheet
																											// name
		if (oxyTransactionDetails == null) {

			OxyTransactionDetailsFromExcelSheets oxyTransactionDetailsFromExcelSheets = new OxyTransactionDetailsFromExcelSheets();
			oxyTransactionDetailsFromExcelSheets.setDebitedAccountType(DebitedAccountType
					.valueOf(oxyTransactionDetailsFromExcelSheetsRequest.getDebitedAccountType().toUpperCase()));
			oxyTransactionDetailsFromExcelSheets.setDebitedTowords(
					DebitedTowords.valueOf(oxyTransactionDetailsFromExcelSheetsRequest.getDebitedTowords()));

			try {
				oxyTransactionDetailsFromExcelSheets.setTransferredDate(expectedDateFormat.parse(paidDate));
			} catch (ParseException e) {

				e.printStackTrace();
			}

			oxyTransactionDetailsFromExcelSheets.setFileName(oxyTransactionDetailsFromExcelSheetsRequest.getFileName());
			oxyTransactionDetailsFromExcelSheets.setRemarks(oxyTransactionDetailsFromExcelSheetsRequest.getRemarks());
			oxyTransactionDetailsFromExcelSheets.setUpdatedOn(new Date());
			oxyTransactionDetailsFromExcelSheetsRepo.save(oxyTransactionDetailsFromExcelSheets);
			oxyTransactionDetailsFromExcelSheetsResponse.setStatus("updated");
		}

		return oxyTransactionDetailsFromExcelSheetsResponse;

	}

	@Override
	public CommentsResponseDto updatingLoanOwnerToBorrower(int userId,
			BorrowersLoanOwnerNames borrowersLoanOwnerNames) {
		CommentsResponseDto statusResponseDto = new CommentsResponseDto();

		User user = userRepo.findById(userId).get();
		if (user != null) {
			user.setLoanOwner(borrowersLoanOwnerNames.getLoanOwner());
			OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
					.findByDealName(borrowersLoanOwnerNames.getLoanOwner());
			if (oxyBorrowersDealsInformation != null) {
				LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
				if (loanRequest != null) {
					loanRequest.setRateOfInterestToBorrower(oxyBorrowersDealsInformation.getBorrowerRateofinterest());
					loanRequest.setDurationBySir(oxyBorrowersDealsInformation.getDuration());
					loanRequest.setRepaymentMethodForLender("I");
					loanRequest.setRepaymentMethodForBorrower("PI");
					loanRequestRepo.save(loanRequest);
					user.setAdminComments("INTERESTED");

				}

			}
			userRepo.save(user);
			statusResponseDto.setStatus("Updated");

		} else {
			throw new OperationNotAllowedException("Borrower not there with given Id", ErrorCodes.USER_NOT_FOUND);
		}
		return statusResponseDto;
	}

	@Override
	public LenderReferenceResponse readingLenderReferenceToSheet() throws ParseException {

		logger.info("readingLenderReferenceToSheet method starts.............");

		List<LenderReferralResponse> response = new ArrayList<LenderReferralResponse>();

		List<LenderReferralResponseForDuration> newListResponse = new ArrayList<LenderReferralResponseForDuration>();

		List<Integer> referrerIds = lenderReferralBonusUpdatedRepo.findingListOfReferees();

		if (referrerIds != null && !referrerIds.isEmpty()) {
			for (Integer referrerId : referrerIds) {

				boolean status = false;
				String validity = "";
				String currentDateNew = "";
				User user = userRepo.findByIdNum(referrerId);
				LenderRenewalDetails lenderRenewalDetails = lenderRenewalDetailsRepo
						.findingRenewalDetailsBasedOnUserId(referrerId);
				if (lenderRenewalDetails != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					validity = format.format(lenderRenewalDetails.getValidityDate());

					currentDateNew = format.format(new Date());

					Date validityDate = format.parse(validity);
					Date currentModifiedDate = format.parse(currentDateNew);

					if (validityDate.compareTo(currentModifiedDate) >= 0) {

						status = true;
					} else {
						Integer noOfPaymentsDoneForPerDeal = lenderPayuDetailsRepo
								.findingCountOfPaymentsDoneForPerDealFromValidityDateToCurrentDate(referrerId, validity,
										currentDateNew);
						if (noOfPaymentsDoneForPerDeal > 0) {
							status = true;
						}
					}

				}

				// logger.info("ReferralLog1" + status);

				List<LenderIdsAndDealIds> listOfLenderIdsAndDealIds = new ArrayList<LenderIdsAndDealIds>();

				List<LenderIdsAndDealIds> listOfNewLenderAndDealIds = new ArrayList<LenderIdsAndDealIds>();

				Double totalAmount = 0.0;
				Double total = 0.0;
				// if (status == true) {
				List<Integer> listOfReferees = lenderReferralBonusUpdatedRepo
						.findingListOfRefereesOfReferrer(referrerId);

				if (listOfReferees != null && !listOfReferees.isEmpty()) {
					for (Integer refereeId : listOfReferees) {

						// logger.info("ReferralLog2" + refereeId);

						List<Integer> listOfDealIdsForReferee = lenderReferralBonusUpdatedRepo
								.findingListOfDealIdsForReferee(referrerId, refereeId);

						Integer dealNumber = 0;

						List<Integer> listOfDealIds = new ArrayList<Integer>();

						Double sumOfAmountBasedOnDealId = 0.0;

						if (listOfDealIdsForReferee != null && !listOfDealIdsForReferee.isEmpty()) {
							for (Integer dealId : listOfDealIdsForReferee) {

								// logger.info("ReferralLog3" + dealId);

								OxyBorrowersDealsInformation dealInformation = oxyBorrowersDealsInformationRepo
										.findBydealId(dealId);
								LenderIdsAndDealIds lenderIdAndDealId = new LenderIdsAndDealIds();
								LenderIdsAndDealIds newLenderIdAndDealId = new LenderIdsAndDealIds();

								String remarks = "";
								if (dealId != 0) {
									OxyLendersAcceptedDeals acceptedDeals = oxyLendersAcceptedDealsRepo
											.findingLenderParticipatedInDeal(refereeId, dealId);

									Double totalAmountUpdated = lendersPaticipationUpdationRepo
											.findingSumOfAmountUpdated(refereeId, dealId);

									if (totalAmountUpdated == null) {
										totalAmountUpdated = 0.0;
									}

									LendersReturns returnsDetails = lendersReturnsRepo.findingWithdralDate(refereeId,
											dealId);

									if (acceptedDeals != null) {
										SimpleDateFormat obj = new SimpleDateFormat("yyyy-MM-dd");
										Date participatedOn = acceptedDeals.getReceivedOn();
										String modifiedParticipatedDate = obj.format(participatedOn);
										Date date1 = obj.parse(modifiedParticipatedDate);

										LenderOxyWallet lenderScrowWallet = lenderOxyWalletRepo
												.findingLenderPrincipalMovedToWallet(refereeId, dealId);

										if (dealInformation != null) {
											dealNumber = dealInformation.getId();
											if (dealInformation.getDuration() <= 3
													&& (returnsDetails != null || lenderScrowWallet != null)) {

												Double sumOfAmountEarnedByReferrer = lenderReferralBonusUpdatedRepo
														.findingSumOfAmountEarnedByRefereeNew(refereeId);

												Double totalBonus = 0.0;

												if (sumOfAmountEarnedByReferrer == null) {
													sumOfAmountEarnedByReferrer = 0.0;
												}
												Double bonusAmount = acceptedDeals.getParticipatedAmount()
														+ totalAmountUpdated;

												if (sumOfAmountEarnedByReferrer < 1000) {

													Date withdrawDate = null;
													if (lenderScrowWallet != null) {
														if (lenderScrowWallet.getTransactionDate() != null) {
															withdrawDate = lenderScrowWallet.getTransactionDate();
														}
													} else {
														if (returnsDetails != null) {
															if (returnsDetails.getPaidDate() != null) {
																withdrawDate = returnsDetails.getPaidDate();
															}
														}
													}
													String modifiedwithDrawDate = obj.format(withdrawDate);
													Date date2 = obj.parse(modifiedwithDrawDate);

													long time_difference = date2.getTime() - date1.getTime();

													long days_difference = (time_difference / (1000 * 60 * 60 * 24))
															% 365;
													// logger.info("date2" + date2.getTime());
													// logger.info("date1" + date1.getTime());
													// logger.info("days_difference" + days_difference);
													if (days_difference >= 90) {

														// logger.info("ReferralLog43772" +
														// sumOfAmountEarnedByReferrer);

														totalBonus = lenderReferralBonusUpdatedRepo
																.findingSumOfAmountBasedOnDealId(dealId, refereeId);

														// logger.info("ReferralLog43772" + totalBonus);

														newLenderIdAndDealId.setDealId(dealNumber);
														newLenderIdAndDealId.setRefereeId(refereeId);
														newLenderIdAndDealId.setAmountForDeal(totalBonus);

														listOfNewLenderAndDealIds.add(newLenderIdAndDealId);
													} else {

														totalBonus = Math.floor(bonusAmount / 2000);

														// logger.info("ReferralLog43772" + bonusAmount);

														newLenderIdAndDealId.setDealId(dealNumber);
														newLenderIdAndDealId.setRefereeId(refereeId);
														newLenderIdAndDealId.setAmountForDeal(totalBonus);

														listOfNewLenderAndDealIds.add(newLenderIdAndDealId);
													}

													if (days_difference < 90) {
														if (returnsDetails != null && returnsDetails.getAmountType()
																.equals(OxyTransactionDetailsFromExcelSheets.DebitedTowords.LENDERWITHDRAW)) {
															remarks = "Withdrawn from " + dealInformation.getDealName()
																	+ " deal in " + days_difference + " days";
														} else {
															remarks = "PrincipalReturned from "
																	+ dealInformation.getDealName() + " deal in "
																	+ days_difference + " days";
														}

													}

													List<LenderReferralBonusUpdated> listOfDealAndRefereeIdsForRemarks = lenderReferralBonusUpdatedRepo
															.findingListOfDealAndRefereeIdsForAmountUpdation(refereeId,
																	dealId);
													if (listOfDealAndRefereeIdsForRemarks != null
															&& !listOfDealAndRefereeIdsForRemarks.isEmpty()) {
														for (LenderReferralBonusUpdated referralRemarksUpdation : listOfDealAndRefereeIdsForRemarks) {

															referralRemarksUpdation.setRemarks(remarks);
															lenderReferralBonusUpdatedRepo
																	.save(referralRemarksUpdation);
														}

													}
												} else {

													// logger.info("ReferralLogLog5" + bonusAmount);

													totalBonus = Math.floor(bonusAmount / 2000);

													newLenderIdAndDealId.setDealId(dealNumber);
													newLenderIdAndDealId.setRefereeId(refereeId);
													newLenderIdAndDealId.setAmountForDeal(totalBonus);

													listOfNewLenderAndDealIds.add(newLenderIdAndDealId);

												}

												List<LenderReferralBonusUpdated> listOfDealAndRefereeIdsForUpdatingAmount = lenderReferralBonusUpdatedRepo
														.findingListOfDealAndRefereeIdsForAmountUpdation(refereeId,
																dealId);
												if (listOfDealAndRefereeIdsForUpdatingAmount != null
														&& !listOfDealAndRefereeIdsForUpdatingAmount.isEmpty()) {
													for (LenderReferralBonusUpdated referralBonusUpdated : listOfDealAndRefereeIdsForUpdatingAmount) {
														referralBonusUpdated.setUpdatedBonus(totalBonus);

														lenderReferralBonusUpdatedRepo.save(referralBonusUpdated);
													}

												}

											} else {

												if (returnsDetails != null || lenderScrowWallet != null) {
													try {
														Date withdrawDate = null;
														if (lenderScrowWallet != null) {
															if (lenderScrowWallet.getTransactionDate() != null) {
																withdrawDate = lenderScrowWallet.getTransactionDate();
															}
														} else {
															if (returnsDetails != null) {
																if (returnsDetails.getPaidDate() != null) {
																	withdrawDate = returnsDetails.getPaidDate();
																}
															}
														}
														String modifiedwithDrawDate = obj.format(withdrawDate);
														Date date2 = obj.parse(modifiedwithDrawDate);

														long time_difference = date2.getTime() - date1.getTime();

														long days_difference = (time_difference / (1000 * 60 * 60 * 24))
																% 365;

														// logger.info("ReferralLogLog6" + dealId + days_difference);
														Double totalBonus = 0.0;
														if (days_difference >= 90) {

															if (returnsDetails != null) {
																dealNumber = returnsDetails.getDealId();
															} else {
																if (lenderScrowWallet != null) {
																	dealNumber = lenderScrowWallet.getDealId();
																}
															}

															sumOfAmountBasedOnDealId = lenderReferralBonusUpdatedRepo
																	.findingSumOfAmountBasedOnDealId(dealNumber,
																			refereeId);

															if (sumOfAmountBasedOnDealId == null) {
																sumOfAmountBasedOnDealId = 0.0;
															}

															lenderIdAndDealId.setDealId(dealNumber);
															lenderIdAndDealId.setRefereeId(refereeId);

															listOfLenderIdsAndDealIds.add(lenderIdAndDealId);

															// logger.info("ReferralLogLog7" + dealId +
															// days_difference);

														} else {

															Double bonusAmount = acceptedDeals.getParticipatedAmount()
																	+ totalAmountUpdated;

															totalBonus = Math.floor(bonusAmount / 2000);

															// logger.info("ReferralLog42364" + bonusAmount);

															newLenderIdAndDealId.setDealId(dealNumber);
															newLenderIdAndDealId.setRefereeId(refereeId);
															newLenderIdAndDealId.setAmountForDeal(totalBonus);

															listOfNewLenderAndDealIds.add(newLenderIdAndDealId);
														}

														if (days_difference < 90) {
															if (returnsDetails != null && returnsDetails.getAmountType()
																	.equals(OxyTransactionDetailsFromExcelSheets.DebitedTowords.LENDERWITHDRAW)) {
																remarks = "Withdrawn from "
																		+ dealInformation.getDealName() + " deal in "
																		+ days_difference + " days";
															} else {
																remarks = "PrincipalReturned from "
																		+ dealInformation.getDealName() + " deal in "
																		+ days_difference + " days";
															}
														}

														List<LenderReferralBonusUpdated> listOfDealAndRefereeIdsForRemarks = lenderReferralBonusUpdatedRepo
																.findingListOfDealAndRefereeIdsForAmountUpdation(
																		refereeId, dealId);
														if (listOfDealAndRefereeIdsForRemarks != null
																&& !listOfDealAndRefereeIdsForRemarks.isEmpty()) {
															for (LenderReferralBonusUpdated referralRemarksUpdation : listOfDealAndRefereeIdsForRemarks) {

																referralRemarksUpdation.setRemarks(remarks);
																referralRemarksUpdation.setUpdatedBonus(totalBonus);
																lenderReferralBonusUpdatedRepo
																		.save(referralRemarksUpdation);
															}

														}

													} catch (ParseException excep) {
														excep.printStackTrace();
													}

												} else {

													Date currentDate = new Date();
													String currentDate1 = obj.format(currentDate);

													Date currentDate2 = obj.parse(currentDate1);

													long time_difference1 = currentDate2.getTime() - date1.getTime();

													long days_difference1 = (time_difference1 / (1000 * 60 * 60 * 24))
															% 365;

													// logger.info("ReferralLogLog8" + dealId + days_difference1);

													if (days_difference1 >= 90) {

														dealNumber = acceptedDeals.getDealId();
														sumOfAmountBasedOnDealId = lenderReferralBonusUpdatedRepo
																.findingSumOfAmountBasedOnDealId(dealNumber, refereeId);
														if (sumOfAmountBasedOnDealId == null) {
															sumOfAmountBasedOnDealId = 0.0;
														}

														lenderIdAndDealId.setDealId(dealNumber);
														lenderIdAndDealId.setRefereeId(refereeId);

														listOfLenderIdsAndDealIds.add(lenderIdAndDealId);
													}
												}
											}
										}
									}

								}
							}
						}

					}
				}

				Double sumOfAmountForRefereeAndDealId = 0.0;
				if (listOfLenderIdsAndDealIds != null && !listOfLenderIdsAndDealIds.isEmpty()) {

					for (LenderIdsAndDealIds lenderId : listOfLenderIdsAndDealIds) {

						sumOfAmountForRefereeAndDealId = lenderReferralBonusUpdatedRepo
								.findingSumOfAmountBasedOnRerefeeIdAndDealId(lenderId.getRefereeId(),
										lenderId.getDealId());

						if (sumOfAmountForRefereeAndDealId == null) {
							sumOfAmountForRefereeAndDealId = 0.0;
						}

						total += sumOfAmountForRefereeAndDealId;
						sumOfAmountForRefereeAndDealId = 0.0;
					}

				}

				if (listOfNewLenderAndDealIds != null && !listOfNewLenderAndDealIds.isEmpty()) {

					for (LenderIdsAndDealIds ids : listOfNewLenderAndDealIds) {

						total += ids.getAmountForDeal();

					}
				}

				logger.info("Log111" + total);

				if (total >= 500) {

					if (status == true) {
						if (listOfLenderIdsAndDealIds != null && !listOfLenderIdsAndDealIds.isEmpty()) {

							for (LenderIdsAndDealIds lenderId : listOfLenderIdsAndDealIds) {
								List<LenderReferralBonusUpdated> listOfDetails = lenderReferralBonusUpdatedRepo
										.findingListOfDealInfo(lenderId.getDealId(), lenderId.getRefereeId());

								if (listOfDetails != null && !listOfDetails.isEmpty()) {

									for (LenderReferralBonusUpdated referralBonus : listOfDetails) {

										LenderReferralResponse referralResponse = new LenderReferralResponse();

										referralResponse.setAmount(referralBonus.getAmount());
										referralResponse.setId(referralBonus.getId());
										referralResponse.setRefereeId(referralBonus.getRefereeUserId());

										response.add(referralResponse);
									}
								}

							}

						}

						if (listOfNewLenderAndDealIds != null && !listOfNewLenderAndDealIds.isEmpty()) {
							logger.info("ReferralLogLog13" + listOfNewLenderAndDealIds.size());
							for (LenderIdsAndDealIds ids : listOfNewLenderAndDealIds) {

								LenderReferralResponseForDuration newResponse = new LenderReferralResponseForDuration();
								logger.info("dealIds" + ids.getDealId());
								newResponse.setAmount(ids.getAmountForDeal());
								newResponse.setDealId(ids.getDealId());
								newResponse.setRefereeId(ids.getRefereeId());

								newListResponse.add(newResponse);
							}
						}

					} else {
						if (user != null) {
							if (user.getLenderGroupId() != 0) {

								ReferralBonusDetails referralDetails = new ReferralBonusDetails();

								referralDetails.setReferrerId(referrerId);
								referralDetails.setComments("Should generate referral bonus on " + currentDateNew
										+ " but the lender is not renewed and the validity date is " + validity);

								referralBonusDetailsRepo.save(referralDetails);
							}
						}
					}

				}

			}
		}

		LenderReferenceResponse lenderReferenceResponse = new LenderReferenceResponse();

		lenderReferenceResponse.setListOfLenderReferralResponse(response);

		lenderReferenceResponse.setListOfNewReferralResponse(newListResponse);

		logger.info("readingLenderReferenceToSheet method ends.............");

		return lenderReferenceResponse;
	}

	@Override
	public EnachScheduledMailResponseDto readingLenderReferenceInfoToExcelSheet() throws ParseException {

		logger.info("readingLenderReferenceInfoToExcelSheet method starts.............");

		EnachScheduledMailResponseDto response1 = new EnachScheduledMailResponseDto();

		LenderReferenceResponse referenceResponse = readingLenderReferenceToSheet();

		logger.info("readingLenderReferenceInfo");

		List<LenderReferralResponseForDuration> listOfReferralResponseForDuration = referenceResponse
				.getListOfNewReferralResponse();

		List<LenderReferralResponse> listOfLenderReferralResponse = referenceResponse.getListOfLenderReferralResponse();

		String excelurl = "";

		FileOutputStream outFile = null;

		Workbook workBook = new HSSFWorkbook();

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyy");

		StringBuilder sbf = new StringBuilder("OXYLR1_OXYLR1UPLD_");

		String requiredDate = formatter.format(new Date());

		sbf = sbf.append(requiredDate + "_RefBN");

		org.apache.poi.ss.usermodel.Sheet spreadSheet = workBook.createSheet(sbf.toString());

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();// Create font
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;
		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}
		int countForSno = 0;

		String referrerName = "";
		Integer referrerId = 0;

		String userNamePerBank = "";
		String accountNumber = "";

		String ifscCode = "";

		String referrerUserId = "";

		Double totalAmount = 0.0;
		if (listOfLenderReferralResponse != null && !listOfLenderReferralResponse.isEmpty()) {
			logger.info("readingLenderList1" + listOfLenderReferralResponse.size());

			for (LenderReferralResponse obj : listOfLenderReferralResponse) {

				Row row = spreadSheet.createRow(++rowCount);

				User referee = userRepo.findByIdNum(obj.getRefereeId());

				if (referee != null) {
					LenderReferenceDetails referenceDetails = lenderReferenceDetailsRepo
							.findingReferrerInfo(referee.getId());

					logger.info("Source Type" + referenceDetails.getSource());

					if (referenceDetails.getSource() != null) {
						if (referenceDetails.getSource().equals(LenderReferenceDetails.Source.Partner)) {
							OxyUserType oxyUser = oxyUserTypeRepo.findById(referenceDetails.getReferrerId()).get();

							if (oxyUser != null) {
								referrerName = oxyUser.getPartnerName();
								referrerId = oxyUser.getId();
								userNamePerBank = oxyUser.getUserName();
								accountNumber = oxyUser.getAccountNumber();
								ifscCode = oxyUser.getIfscCode().toUpperCase();

								referrerUserId = "PR" + oxyUser.getId();
							}

						} else if ((referenceDetails.getSource().equals(LenderReferenceDetails.Source.ReferralLink))
								|| (referenceDetails.getSource().equals(LenderReferenceDetails.Source.BulkInvite))) {

							logger.info("ReferrerId" + referenceDetails.getReferrerId());
							logger.info("Source" + referenceDetails.getSource());
							User referrer = userRepo.findByIdNum(referenceDetails.getReferrerId());

							if (referrer != null) {
								referrerName = referrer.getPersonalDetails().getFirstName() + " "
										+ referrer.getPersonalDetails().getLastName();
								referrerId = referrer.getId();
								userNamePerBank = referrer.getBankDetails().getUserName();
								accountNumber = referrer.getBankDetails().getAccountNumber();
								ifscCode = referrer.getBankDetails().getIfscCode().toUpperCase();
								referrerUserId = "LR" + referrer.getId();

							}

						}
					}
				}
				Double previousEarnedAmount = lenderReferralBonusUpdatedRepo
						.findingSumOfAmountEarnedByReferrerId(referrerId);
				String type = null;
				if (previousEarnedAmount != null) {
					type = "Old";
				} else {
					type = "New";
				}
				User user = userRepo.findById(referrerId).get();
				logger.info("testUserType" + user.isTestUser());
				if (user.isTestUser() == false) {

					Cell cell1 = row.createCell(0);
					cell1.setCellValue("777705849442");

					cell1 = row.createCell(1);
					cell1.setCellValue(accountNumber == null ? "" : accountNumber);

					cell1 = row.createCell(2);
					cell1.setCellValue(referrerName == null ? "" : referrerName);

					cell1 = row.createCell(3);
					cell1.setCellValue(obj.getAmount());

					cell1 = row.createCell(4);
					cell1.setCellValue("N");

					SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");

					String modifiedDate = format.format(new Date());

					cell1 = row.createCell(5);
					cell1.setCellValue(modifiedDate);

					cell1 = row.createCell(6);
					cell1.setCellValue(ifscCode == null ? "" : ifscCode);

					cell1 = row.createCell(14);
					cell1.setCellValue(type);

					cell1 = row.createCell(16);
					cell1.setCellValue("LENDERREFERRALBONUS");

					cell1 = row.createCell(17);
					cell1.setCellValue(obj.getId());

					cell1 = row.createCell(18);
					cell1.setCellValue(referrerUserId);

					cell1 = row.createCell(19);
					cell1.setCellValue(referee.getId());

					cell1 = row.createCell(20);
					cell1.setCellValue("OXYLOANS REFERRAL BONUS");

					cell1 = row.createCell(15);
					cell1.setCellValue(referee.getId());

					totalAmount += obj.getAmount();

					countForSno = countForSno + 1;
				}

			}
		}
		if (listOfReferralResponseForDuration != null && !listOfReferralResponseForDuration.isEmpty()) {
			logger.info("readingLenderList2" + listOfReferralResponseForDuration.size());
			for (LenderReferralResponseForDuration obj : listOfReferralResponseForDuration) {

				Row row = spreadSheet.createRow(++rowCount);

				User referee = userRepo.findById(obj.getRefereeId()).get();

				if (referee != null) {
					LenderReferenceDetails referenceDetails = lenderReferenceDetailsRepo
							.findingReferrerInfo(referee.getId());

					logger.info("Source Type" + referenceDetails.getSource());

					logger.info("Source TypedealId" + obj.getDealId());

					if (referenceDetails.getSource() != null) {
						if (referenceDetails.getSource().equals(LenderReferenceDetails.Source.Partner)) {
							OxyUserType oxyUser = oxyUserTypeRepo.findById(referenceDetails.getReferrerId()).get();

							if (oxyUser != null) {
								referrerName = oxyUser.getPartnerName();
								referrerId = oxyUser.getId();
								userNamePerBank = oxyUser.getUserName();
								accountNumber = oxyUser.getAccountNumber();
								ifscCode = oxyUser.getIfscCode().toUpperCase();

								referrerUserId = "PR" + oxyUser.getId();
							}

						} else if ((referenceDetails.getSource().equals(LenderReferenceDetails.Source.ReferralLink))
								|| (referenceDetails.getSource().equals(LenderReferenceDetails.Source.BulkInvite))) {

							logger.info("ReferrerId" + referenceDetails.getReferrerId());
							logger.info("Source" + referenceDetails.getSource());
							User referrer = userRepo.findByIdNum(referenceDetails.getReferrerId());

							if (referrer != null) {
								referrerName = referrer.getPersonalDetails().getFirstName() + " "
										+ referrer.getPersonalDetails().getLastName();
								referrerId = referrer.getId();
								userNamePerBank = referrer.getBankDetails().getUserName();
								accountNumber = referrer.getBankDetails().getAccountNumber();
								ifscCode = referrer.getBankDetails().getIfscCode().toUpperCase();
								referrerUserId = "LR" + referrer.getId();

							}

						}
					}
				}

				Double previousEarnedAmount = lenderReferralBonusUpdatedRepo
						.findingSumOfAmountEarnedByReferrerId(referrerId);
				String type = null;
				if (previousEarnedAmount != null) {
					type = "Old";
				} else {
					type = "New";
				}
				User user = userRepo.findById(referrerId).get();
				logger.info("testUserType" + user.isTestUser());
				if (user.isTestUser() == false) {

					Cell cell1 = row.createCell(0);
					cell1.setCellValue("777705849442");

					cell1 = row.createCell(1);
					cell1.setCellValue(accountNumber == null ? "" : accountNumber);

					cell1 = row.createCell(2);
					cell1.setCellValue(referrerName == null ? "" : referrerName);

					cell1 = row.createCell(3);
					cell1.setCellValue(obj.getAmount());

					cell1 = row.createCell(4);
					cell1.setCellValue("N");

					SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");

					String modifiedDate = format.format(new Date());

					cell1 = row.createCell(5);
					cell1.setCellValue(modifiedDate);

					cell1 = row.createCell(6);
					cell1.setCellValue(ifscCode == null ? "" : ifscCode);

					cell1 = row.createCell(14);
					cell1.setCellValue(type);

					cell1 = row.createCell(16);
					cell1.setCellValue("LENDERREFERRALBONUSFORDEAL");

					cell1 = row.createCell(17);
					cell1.setCellValue(obj.getDealId());

					cell1 = row.createCell(18);
					cell1.setCellValue(referrerUserId);

					cell1 = row.createCell(19);
					cell1.setCellValue(referee.getId());

					cell1 = row.createCell(20);
					cell1.setCellValue("OXYLOANS REFERRAL BONUS");

					cell1 = row.createCell(15);
					cell1.setCellValue(referee.getId());

					totalAmount += obj.getAmount();

					countForSno = countForSno + 1;
				}

			}
		}

		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		String fileName = sbf.toString() + ".xls";
		File f = new File(toSaveReferralBonus + fileName);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);

			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate = expectedDateFormat.format(current.getTime());

			templateContext.put("currentDate", expectedCurrentDate);

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(fileName);

			fileRequest.setFileType(FileType.LenderReferenceInfo);
			fileRequest.setFilePrifix(FileType.LenderReferenceInfo.name());

			fileManagementService.putFile(fileRequest);

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			excelurl = getFile.getDownloadUrl();

			String[] downloadUrl = excelurl.split(Pattern.quote("?"));

			response1.setDownloadUrl(downloadUrl[0]);

			FileInputStream in = new FileInputStream(f.getAbsolutePath());

			String mailSubject = "Lender Reference Information";

			EmailRequest emailRequest = new EmailRequest(
					new String[] { "archana.n@oxyloans.com", "ramadevi@oxyloans.com" ,"vijaydasari060@gmail.com" }, "lenderReferenceInfo.template",
					templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			} else {
				response1.setEmailStatus("Email Sent!");

				if (f.delete()) {
					System.out.println("File deleted");
				} else {
					System.out.println("Not deleted");
				}

			}

			LenderCmsPayments cmsStats = lenderCmsPaymentsRepo.findbyFileName(f.getName());

			if (cmsStats != null) {
				cmsStats.setFileName(f.getName());
				cmsStats.setDealId(0);
				if (cmsStats.getFileExecutionsSatus() == OxyCmsLoans.FileExecutionStatus.MOVEDTOS3) {
					cmsStats.setFileExecutionsSatus(OxyCmsLoans.FileExecutionStatus.MOVEDTOS3);
				}

				cmsStats.setTotalAmount(totalAmount);
				cmsStats.setLenderReturnsType("ReferralBonus");
				// cmsStats.setPaymentMode(LenderCmsPayments.PaymentMode.SYSTEM.toString());
				lenderCmsPaymentsRepo.save(cmsStats);
			} else {
				LenderCmsPayments iciciCmsStats = new LenderCmsPayments();
				iciciCmsStats.setFileName(f.getName());
				iciciCmsStats.setDealId(0);
				iciciCmsStats.setFileExecutionsSatus(OxyCmsLoans.FileExecutionStatus.MOVEDTOS3);
				iciciCmsStats.setTotalAmount(totalAmount);
				iciciCmsStats.setS3_download_link(downloadUrl[0]);
				iciciCmsStats.setLenderReturnsType("ReferralBonus");
				// iciciCmsStats.setPaymentMode(LenderCmsPayments.PaymentMode.SYSTEM.toString());
				lenderCmsPaymentsRepo.save(iciciCmsStats);
			}

			WhatsappAttachmentRequestDto dto = new WhatsappAttachmentRequestDto();

			dto.setFileName(f.getName());
			dto.setInputSream(in);
			dto.setChatId("120363020098924954@g.us");

			// whatappServiceRepo.sendAttatchements(dto);

			WhatsappAttachmentRequestDto dto1 = new WhatsappAttachmentRequestDto();

			dto1.setFileName(f.getName());
			dto1.setInputSream(in);
			dto1.setChatId("919652612942@c.us");

			// whatappServiceRepo.sendAttatchements(dto1);

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

			String message = "Total Referral Bonus is INR " + totalAmount + " on " + format.format(new Date())
					+ " in production.";

			/*
			 * whatappServiceRepo.sendingWhatsappMessages("917702795895-1630419500@g.us",
			 * message, whatsaAppSendApiNewInstance);
			 */

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		if (f.exists()) {
			f.delete();
		}

		/*
		 * } else {
		 * 
		 * SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		 * 
		 * String message = "Total Referral Bonus is INR 0 on " + format.format(new
		 * Date());
		 * 
		 * 
		 * whatappServiceRepo.sendingWhatsappMessages("917702795895-1630419500@g.us",
		 * message, whatsaAppSendApiNewInstance);
		 * 
		 * 
		 * }
		 */
		logger.info("readingLenderReferenceInfoToExcelSheet method ends.............");

		return response1;

	}

	@Override
	public LenderReferenceResponse readingCommentsOfRadhaSir(LenderReferenceRequestDto lenderReferenceRequestDto)
			throws ParseException {

		logger.info("readingCommentsOfRadhaSir method starts.................");

		LenderReferenceResponse referenceResponse = readingLenderReferenceToSheet();

		List<LenderReferralResponse> listOfLenderReferralResponse = referenceResponse.getListOfLenderReferralResponse();

		LenderReferenceResponse response = new LenderReferenceResponse();

		if (listOfLenderReferralResponse != null && !listOfLenderReferralResponse.isEmpty()) {
			for (LenderReferralResponse obj : listOfLenderReferralResponse) {

				Integer id = obj.getId();

				LenderReferralBonusUpdated referralInfo = lenderReferralBonusUpdatedRepo.gettingReferralInfo(id);
				referralInfo.setRadhaSirCommentsForBonus(lenderReferenceRequestDto.getComments());

				lenderReferralBonusUpdatedRepo.save(referralInfo);

				response.setStatus("Successfully Updated");

			}
		}

		logger.info("readingCommentsOfRadhaSir method ends.................");

		return response;
	}

	@Override
	public LenderReferenceResponse readingResponseForReferenceBonus(LenderReferenceRequestDto lenderReferenceRequestDto)
			throws ParseException {
		logger.info("readingResponseForReferenceBonus method starts....................");

		LenderReferenceResponse referenceResponse = new LenderReferenceResponse();

		LenderReferenceResponse referenceResponses = readingLenderReferenceToSheet();

		List<LenderReferralResponse> listOfLenderReferralResponse = referenceResponses
				.getListOfLenderReferralResponse();

		if (listOfLenderReferralResponse != null && !listOfLenderReferralResponse.isEmpty()) {
			for (LenderReferralResponse referral : listOfLenderReferralResponse) {

				LenderReferralBonusUpdated referralBonus = lenderReferralBonusUpdatedRepo
						.getIdOfReferrer(referral.getId());

				referralBonus.setPaymentStatus("Paid");
				try {
					referralBonus
							.setTransferredOn(expectedDateFormat.parse(lenderReferenceRequestDto.getTransferredOn()));
				} catch (ParseException e) {

					e.printStackTrace();
				}

				lenderReferralBonusUpdatedRepo.save(referralBonus);
			}
		}

		if (listOfLenderReferralResponse != null && !listOfLenderReferralResponse.isEmpty()) {
			for (LenderReferralResponse refereeId : listOfLenderReferralResponse) {
				LenderReferenceDetails details = lenderReferenceDetailsRepo
						.findingRefereeAndReferrerInfo(refereeId.getRefereeId());
				Double sumOfAmountUnpaid = lenderReferralBonusUpdatedRepo
						.findingSumOfAmountInUnPaidState(refereeId.getRefereeId());
				if (sumOfAmountUnpaid != null) {
					details.setAmount(sumOfAmountUnpaid);
				} else {
					details.setAmount(0.0);
				}

				lenderReferenceDetailsRepo.save(details);

				referenceResponse.setStatus("Successfully Updated");
			}
		}

		logger.info("readingResponseForReferenceBonus method ends....................");

		return referenceResponse;

	}

	@Override
	public LenderReferenceResponse gettingLenderReferralBonusDetails() {
		List<LenderReferenceResponseDto> response = new ArrayList<LenderReferenceResponseDto>();

		List<Object[]> referenceDetails = lenderReferralBonusUpdatedRepo.findingReferralBonusDetails();

		Integer countOfDetails = lenderReferenceDetailsRepo.gettingCountOfLenderReferralBonusDetails();

		for (Object[] obj : referenceDetails) {
			LenderReferenceResponseDto lenderResponse = new LenderReferenceResponseDto();
			lenderResponse.setRefereeId((Integer) obj[0] == null ? 0 : (Integer) obj[0]);
			lenderResponse.setRefereeName(obj[1].toString() == null ? "" : obj[1].toString());
			lenderResponse.setReferrerId((Integer) obj[2] == null ? 0 : (Integer) obj[2]);
			lenderResponse.setAmount((Double) obj[3] == null ? 0.0 : (Double) obj[3]);
			lenderResponse.setPaymentStatus(obj[4].toString() == null ? "" : obj[4].toString());
			lenderResponse.setStatus(obj[5].toString() == null ? "" : obj[5].toString());

			if (obj[6] == null) {
				lenderResponse.setRadhaSirComments("");
			} else {
				lenderResponse.setRadhaSirComments(obj[6].toString());
			}

			response.add(lenderResponse);

		}

		LenderReferenceResponse lenderReferenceResponse = new LenderReferenceResponse();
		lenderReferenceResponse.setListOfLenderReferenceDetails(response);
		lenderReferenceResponse.setCountOfReferees(countOfDetails);

		return lenderReferenceResponse;

	}

	@Override
	public BorrowersDealsResponseDto updateBorrowerDealsInformation(BorrowersDealsRequestDto borrowersDealsRequestDto) {
		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = new OxyBorrowersDealsInformation();
		if (borrowersDealsRequestDto.getDealId() == 0) {

			OxyBorrowersDealsInformation dealName = oxyBorrowersDealsInformationRepo
					.dealNameAlreadyGiven(borrowersDealsRequestDto.getDealName());

			if (dealName == null) {
				oxyBorrowersDealsInformation.setDealName(borrowersDealsRequestDto.getDealName());
			} else {
				throw new OperationNotAllowedException("Deal name already present in database",
						ErrorCodes.ACTION_ALREDY_DONE);
			}
			if (borrowersDealsRequestDto.getOxyLoanRequestId() != 0) {
				oxyBorrowersDealsInformation.setOxyLoanRequestId(borrowersDealsRequestDto.getOxyLoanRequestId());
				LoanRequest loanRequest = loanRequestRepo.findById(borrowersDealsRequestDto.getOxyLoanRequestId())
						.get();
				if (loanRequest != null) {
					User user = userRepo.findById(loanRequest.getUserId()).get();
					if (user != null) {
						user.setLoanOwner(borrowersDealsRequestDto.getDealName());
						userRepo.save(user);
					}
					if (!loanRequest.getLoanOfferedAmount().getLoanOfferedAmount()
							.equals(borrowersDealsRequestDto.getDealAmount())) {
						throw new OperationNotAllowedException(
								"Deal Amount is not matching with offersent amount please check",
								ErrorCodes.AMOUNT_EXCEEDED);
					}
				}
			} else {
				oxyBorrowersDealsInformation.setOxyLoanRequestId(0);
			}

			oxyBorrowersDealsInformation.setBorrowerName(borrowersDealsRequestDto.getBorrowerName());
			
		if (borrowersDealsRequestDto.getDealAmount() != null) {
			oxyBorrowersDealsInformation.setDealAmount(borrowersDealsRequestDto.getDealAmount());
		}
			
			oxyBorrowersDealsInformation.setBorrowerRateofinterest(borrowersDealsRequestDto.getBorrowerRateOfInterest());
			
		if(borrowersDealsRequestDto.getFundsAcceptanceStartDate() != null) {
			try {
				oxyBorrowersDealsInformation.setFundsAcceptanceStartDate(
						expectedDateFormat.parse(borrowersDealsRequestDto.getFundsAcceptanceStartDate()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if(borrowersDealsRequestDto.getFundsAcceptanceEndDate() != null) {
			try {
				oxyBorrowersDealsInformation.setFundsAcceptanceEndDate(
						expectedDateFormat.parse(borrowersDealsRequestDto.getFundsAcceptanceEndDate()));
			} catch (ParseException e) {

				e.printStackTrace();
			}
		}
			if (borrowersDealsRequestDto.getLoanActiveDate() != null) {
				try {
					oxyBorrowersDealsInformation
							.setLoanActiveDate(expectedDateFormat.parse(borrowersDealsRequestDto.getLoanActiveDate()));
				} catch (ParseException e) {

					e.printStackTrace();
				}
			}

			oxyBorrowersDealsInformation.setReceivedOn(new Date());
			
       if(borrowersDealsRequestDto.getParticipcationLenderType() != null) {
			oxyBorrowersDealsInformation.setParticipationLenderType(ParticipationLenderType
					.valueOf(borrowersDealsRequestDto.getParticipcationLenderType().toUpperCase()));
      }
			if (borrowersDealsRequestDto.getLifeTimeWaiver() != null) {
				oxyBorrowersDealsInformation.setLifeTimeWaiver(borrowersDealsRequestDto.getLifeTimeWaiver());
			}

			if (borrowersDealsRequestDto.getLifeTimeWaiverLimit() != null) {
				oxyBorrowersDealsInformation
						.setLifeTimeWaiverLimit(Double.valueOf(borrowersDealsRequestDto.getLifeTimeWaiverLimit()));

			}

			oxyBorrowersDealsInformation.setDealLink(borrowersDealsRequestDto.getDealLink());
           
		if(borrowersDealsRequestDto.getDuration() != null) {
			oxyBorrowersDealsInformation.setDuration(borrowersDealsRequestDto.getDuration());
           }
		
			oxyBorrowersDealsInformation.setDealType(DealType.valueOf(borrowersDealsRequestDto.getDealType().toUpperCase()));

			if (borrowersDealsRequestDto.getFeeROIforBorrower() != null) {
				Double interestAmount = (borrowersDealsRequestDto.getDealAmount()
						* borrowersDealsRequestDto.getFeeROIforBorrower()) / 100;

				Double GstAmount = (interestAmount * 18) / 100;

				Double feeCollectedFromBorrower = (interestAmount + GstAmount);

				oxyBorrowersDealsInformation.setFeeCollectedFromBorrower(feeCollectedFromBorrower);

				oxyBorrowersDealsInformation.setFeeROIForBorrower(borrowersDealsRequestDto.getFeeROIforBorrower());
			}

			if (borrowersDealsRequestDto.getFeeCollectedFromBorrower() != null) {
				oxyBorrowersDealsInformation
						.setFeeCollectedFromBorrower(borrowersDealsRequestDto.getFeeCollectedFromBorrower());
			}

			if (borrowersDealsRequestDto.getWhatsappChatId() != null) {
				oxyBorrowersDealsInformation.setWhatappChatid(borrowersDealsRequestDto.getWhatsappChatId());

			}
			if (borrowersDealsRequestDto.getWhatsappResponseLink() != null) {
				oxyBorrowersDealsInformation.setWhatappReponseLink(borrowersDealsRequestDto.getWhatsappResponseLink());
			}

			oxyBorrowersDealsInformation.setLinkSentToLendersGroup(borrowersDealsRequestDto.getWhatappGroupNames());

			if (borrowersDealsRequestDto.getParticipationLimitToLenders() != null) {
				oxyBorrowersDealsInformation
						.setPaticipationLimitToLenders(borrowersDealsRequestDto.getParticipationLimitToLenders());
			}
			if (borrowersDealsRequestDto.getWhatappMessageToLenders() != null) {
				oxyBorrowersDealsInformation
						.setWhatappMessageToLenders(borrowersDealsRequestDto.getWhatappMessageToLenders());
			}
			if (borrowersDealsRequestDto.getWithdrawalStatus() != null) {
				oxyBorrowersDealsInformation.setAnyTimeWithdrawal(OxyBorrowersDealsInformation.AnyTimeWithdrawal
						.valueOf(borrowersDealsRequestDto.getWithdrawalStatus()));
			}

			if (borrowersDealsRequestDto.getWithdrawalRoi() != null) {
				oxyBorrowersDealsInformation.setRoiForWithdrawal(borrowersDealsRequestDto.getWithdrawalRoi());
			}
			if (borrowersDealsRequestDto.getMinimumPaticipationAmount() != null) {
				oxyBorrowersDealsInformation
						.setMinimumPaticipationAmount(borrowersDealsRequestDto.getMinimumPaticipationAmount());
			}
			if (borrowersDealsRequestDto.getDealSubtype() != null) {
				oxyBorrowersDealsInformation.setDealSubType(
						OxyBorrowersDealsInformation.DealSubtype.valueOf(borrowersDealsRequestDto.getDealSubtype()));

			}

			// DealLevelRequestDto borroweridsAndLoanAmount =
			// automaticSendingOffer(borrowersDealsRequestDto);
			List<DealLevelRequestDto> borroweridsAndAmounts = borrowersDealsRequestDto.getIdsWithLoanAmount();
			if (borroweridsAndAmounts != null && !borroweridsAndAmounts.isEmpty()) {
				StringBuilder listOfBorrowerids = new StringBuilder();
				StringBuilder listOfLoanAmounts = new StringBuilder();

				borroweridsAndAmounts.forEach(borrowerInfo -> {
					User user = userRepo.findByIdNum(borrowerInfo.getBorrowerId());
					if (user != null) {
						listOfBorrowerids.append(borrowerInfo.getBorrowerId()).append(",");
						listOfLoanAmounts.append(borrowerInfo.getLoanAmount()).append(",");
					}
				});

				oxyBorrowersDealsInformation.setBorrowersIdsMappedToDeal(listOfBorrowerids.toString());
				oxyBorrowersDealsInformation.setBorrowersAmountMappedTodeal(listOfLoanAmounts.toString());
			}
			if (borrowersDealsRequestDto.getFeeStatusToParticipate() != null) {
				oxyBorrowersDealsInformation
						.setFeeStatusToParticipate(OxyBorrowersDealsInformation.FeeStatusToParticipate
								.valueOf(borrowersDealsRequestDto.getFeeStatusToParticipate()));
			}

			Integer duration = borrowersDealsRequestDto.getDuration();

			String fundsStartDate = borrowersDealsRequestDto.getFundsAcceptanceStartDate();

			String[] dateParts = fundsStartDate.split("/");

			String dateRequired = dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];

			LocalDate date = LocalDate.parse(dateRequired);

			LocalDate newDate = date.plusMonths(borrowersDealsRequestDto.getDuration());

			Date emiEndDate = null;
			try {
				emiEndDate = dateFormat1.parse(newDate.toString());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			oxyBorrowersDealsInformation.setEmiEndDate(emiEndDate);
			if (borrowersDealsRequestDto.getDealOpenStatus() == null
					|| borrowersDealsRequestDto.getDealOpenStatus().isEmpty()) {
				oxyBorrowersDealsInformation.setDealOpenStatus(OxyBorrowersDealsInformation.DealOpenStatus.NOW);
				borrowersDealsRequestDto.setDealOpenStatus(OxyBorrowersDealsInformation.DealOpenStatus.NOW.toString());
			} else {
				oxyBorrowersDealsInformation.setDealOpenStatus(OxyBorrowersDealsInformation.DealOpenStatus
						.valueOf(borrowersDealsRequestDto.getDealOpenStatus()));
			}
			oxyBorrowersDealsInformationRepo.save(oxyBorrowersDealsInformation);
			

			if (borrowersDealsRequestDto.getDealOpenStatus()
					.equals(OxyBorrowersDealsInformation.DealOpenStatus.NOW.toString())) {

				/*
				 * if (borrowersDealsRequestDto.getWhatsappChatId() != null &&
				 * !borrowersDealsRequestDto.getDealType()
				 * .equals(OxyBorrowersDealsInformation.DealType.TEST.toString())) {
				 * whatappServiceRepo.sendingMsgToUsingUltra(oxyBorrowersDealsInformation,
				 * borrowersDealsRequestDto.getDealId(),
				 * borrowersDealsRequestDto.getWhatsappChatId());
				 * 
				 * }
				 */
				// oxyBorrowersDealsInformationRepo.save(oxyBorrowersDealsInformation);
			} else {

				oxyBorrowersDealsInformation.setDealLaunchHoure(borrowersDealsRequestDto.getDealLaunchHoure());

				String newLunchHoure = Removehourse(oxyBorrowersDealsInformation.getDealLaunchHoure());

				oxyBorrowersDealsInformation.setNewDealLunchHoure(newLunchHoure);
				try {
					oxyBorrowersDealsInformation
							.setDealFutureDate(expectedDateFormat.parse(borrowersDealsRequestDto.getDealFutureDate()));
				} catch (ParseException e) {
					e.printStackTrace();
				}

				oxyBorrowersDealsInformation
						.setDealParticipationStatus(OxyBorrowersDealsInformation.DealParticipationStatus.OPENINFUTURE);
				oxyBorrowersDealsInformationRepo.save(oxyBorrowersDealsInformation);
				/*
				 * if (borrowersDealsRequestDto.getWhatsappChatId() != null &&
				 * !borrowersDealsRequestDto.getDealType()
				 * .equals(OxyBorrowersDealsInformation.DealType.TEST.toString())) {
				 * whatappServiceRepo.sendingMsgToUsingUltra(oxyBorrowersDealsInformation,
				 * borrowersDealsRequestDto.getDealId(),
				 * borrowersDealsRequestDto.getWhatsappChatId());
				 * 
				 * }
				 */
			}
			oxyBorrowersDealsInformationRepo.save(oxyBorrowersDealsInformation);

			OxyDealsRateofinterestToLendersgroup OxyDealsForOxyPremium = new OxyDealsRateofinterestToLendersgroup();
			if (borrowersDealsRequestDto.getOxyPremiumMonthlyInterest() != null) {
				OxyDealsForOxyPremium.setMonthlyInterest(borrowersDealsRequestDto.getOxyPremiumMonthlyInterest());
			}
			if (borrowersDealsRequestDto.getOxyPremiumQuartelyInterest() != null) {
				OxyDealsForOxyPremium.setQuartelyInterest(borrowersDealsRequestDto.getOxyPremiumQuartelyInterest());
			}
			if (borrowersDealsRequestDto.getOxyPremiumHalfInterest() != null) {
				OxyDealsForOxyPremium.setHalfInterest(borrowersDealsRequestDto.getOxyPremiumHalfInterest());
			}
			if (borrowersDealsRequestDto.getOxyPremiumYearlyInterest() != null) {
				OxyDealsForOxyPremium.setYearlyInterest(borrowersDealsRequestDto.getOxyPremiumYearlyInterest());
			}
			if (borrowersDealsRequestDto.getOxyPremiumEndOfTheDealInterest() != null) {
				OxyDealsForOxyPremium
						.setEndofthedealInterest(borrowersDealsRequestDto.getOxyPremiumEndOfTheDealInterest());
			}
			OxyDealsForOxyPremium.setDealId(oxyBorrowersDealsInformation.getId());
			OxyDealsForOxyPremium.setGroupId(3);
			oxyDealsRateofinterestToLendersgroupRepo.save(OxyDealsForOxyPremium);

			OxyDealsRateofinterestToLendersgroup OxyDealsForNewLenders = new OxyDealsRateofinterestToLendersgroup();
			if (borrowersDealsRequestDto.getNewLendersMonthlyInterest() != null) {
				OxyDealsForNewLenders.setMonthlyInterest(borrowersDealsRequestDto.getNewLendersMonthlyInterest());
			}
			if (borrowersDealsRequestDto.getNewLendersQuartelyInterest() != null) {

				OxyDealsForNewLenders.setQuartelyInterest(borrowersDealsRequestDto.getNewLendersQuartelyInterest());
			}
			if (borrowersDealsRequestDto.getNewLendersHalfInterest() != null) {
				OxyDealsForNewLenders.setHalfInterest(borrowersDealsRequestDto.getNewLendersHalfInterest());
			}
			if (borrowersDealsRequestDto.getNewLendersYearlyInterest() != null) {
				OxyDealsForNewLenders.setYearlyInterest(borrowersDealsRequestDto.getNewLendersYearlyInterest());
			}
			if (borrowersDealsRequestDto.getNewLendersEndOfTheDealInterest() != null) {
				OxyDealsForNewLenders
						.setEndofthedealInterest(borrowersDealsRequestDto.getNewLendersEndOfTheDealInterest());
			}
			OxyDealsForNewLenders.setDealId(oxyBorrowersDealsInformation.getId());
			OxyDealsForNewLenders.setGroupId(0);
			oxyDealsRateofinterestToLendersgroupRepo.save(OxyDealsForNewLenders);

			if (borrowersDealsRequestDto.getWhatsappChatId() != null
					&& !borrowersDealsRequestDto.getDealType()
							.equals(OxyBorrowersDealsInformation.DealType.TEST.toString())
					&& !borrowersDealsRequestDto.getDealOpenStatus()
							.equals(OxyBorrowersDealsInformation.DealOpenStatus.FUTURE.toString())) {
				whatappServiceRepo.sendingMsgToUsingUltra(oxyBorrowersDealsInformation,
						borrowersDealsRequestDto.getDealId(), borrowersDealsRequestDto.getWhatsappChatId());

			}

			if (!borrowersDealsRequestDto.getDealType().equals(OxyBorrowersDealsInformation.DealType.TEST.toString())
					&& !borrowersDealsRequestDto.getDealOpenStatus()
							.equals(OxyBorrowersDealsInformation.DealOpenStatus.FUTURE.toString())) {
				mailToActiveLendersCreatingDeal(oxyBorrowersDealsInformation, "DEALCREATION");
			}
			 dealCreationAndEditHistory(oxyBorrowersDealsInformation);
			
		} else {
			Integer id = borrowersDealsRequestDto.getDealId();

			OxyBorrowersDealsInformation oxyBorrowersDeal = oxyBorrowersDealsInformationRepo.findDealAlreadyGiven(id);
			if (oxyBorrowersDeal != null) {
				if (borrowersDealsRequestDto.getDealName() != null) {
					OxyBorrowersDealsInformation dealName = oxyBorrowersDealsInformationRepo
							.dealNameAlreadyGiven(borrowersDealsRequestDto.getDealName());

					oxyBorrowersDeal.setDealName(borrowersDealsRequestDto.getDealName());

				}
				// checkingDateValidation(borrowersDealsRequestDto);
				if (borrowersDealsRequestDto.getParticipcationLenderType() != null) {
					oxyBorrowersDeal.setParticipationLenderType(ParticipationLenderType
							.valueOf(borrowersDealsRequestDto.getParticipcationLenderType().toUpperCase()));
				}

				if (borrowersDealsRequestDto.getBorrowerName() != null) {
					oxyBorrowersDeal.setBorrowerName(borrowersDealsRequestDto.getBorrowerName());
				}
				if (borrowersDealsRequestDto.getDealAmount() != null) {
					oxyBorrowersDeal.setDealAmount(borrowersDealsRequestDto.getDealAmount());
				}
				if (borrowersDealsRequestDto.getBorrowerRateOfInterest() != null) {
					oxyBorrowersDeal.setBorrowerRateofinterest(borrowersDealsRequestDto.getBorrowerRateOfInterest());
				}
				if (borrowersDealsRequestDto.getFundsAcceptanceStartDate() != null) {
					try {
						oxyBorrowersDeal.setFundsAcceptanceStartDate(
								expectedDateFormat.parse(borrowersDealsRequestDto.getFundsAcceptanceStartDate()));
					} catch (ParseException e) {

						e.printStackTrace();
					}
				}
				if (borrowersDealsRequestDto.getFundsAcceptanceEndDate() != null) {
					try {
						oxyBorrowersDeal.setFundsAcceptanceEndDate(
								expectedDateFormat.parse(borrowersDealsRequestDto.getFundsAcceptanceEndDate()));
					} catch (ParseException e) {

						e.printStackTrace();
					}
				}

				if (borrowersDealsRequestDto.getLoanActiveDate() != null) {
					try {
						oxyBorrowersDeal.setLoanActiveDate(
								expectedDateFormat.parse(borrowersDealsRequestDto.getLoanActiveDate()));
					} catch (ParseException e) {

						e.printStackTrace();
					}
				}
				oxyBorrowersDeal.setModifiedOn(new Date());

				if (borrowersDealsRequestDto.getDealLink() != null) {
					oxyBorrowersDeal.setDealLink(borrowersDealsRequestDto.getDealLink());
				}
				if (borrowersDealsRequestDto.getDuration() != null) {
					oxyBorrowersDeal.setDuration(borrowersDealsRequestDto.getDuration());
				}

				if (borrowersDealsRequestDto.getLifeTimeWaiver() != null) {
					oxyBorrowersDeal.setLifeTimeWaiver(borrowersDealsRequestDto.getLifeTimeWaiver());
				}

				if (borrowersDealsRequestDto.getLifeTimeWaiverLimit() != null) {
					oxyBorrowersDeal
							.setLifeTimeWaiverLimit(Double.valueOf(borrowersDealsRequestDto.getLifeTimeWaiverLimit()));

				}
				if (borrowersDealsRequestDto.getWhatappGroupNames() != null) {
					oxyBorrowersDeal.setLinkSentToLendersGroup(borrowersDealsRequestDto.getWhatappGroupNames());
				}
				if (borrowersDealsRequestDto.getParticipationLimitToLenders() != null) {
					oxyBorrowersDeal
							.setPaticipationLimitToLenders(borrowersDealsRequestDto.getParticipationLimitToLenders());
				}
				if (borrowersDealsRequestDto.getWhatappMessageToLenders() != null) {
					oxyBorrowersDeal.setWhatappMessageToLenders(borrowersDealsRequestDto.getWhatappMessageToLenders());
				}

				if (borrowersDealsRequestDto.getFeeROIforBorrower() != null) {
					Double interestAmount = (borrowersDealsRequestDto.getDealAmount()
							* borrowersDealsRequestDto.getFeeROIforBorrower()) / 100;

					Double GstAmount = (interestAmount * 18) / 100;

					Double feeCollectedFromBorrower = (interestAmount + GstAmount);

					oxyBorrowersDeal.setFeeCollectedFromBorrower(feeCollectedFromBorrower);
					oxyBorrowersDeal.setFeeROIForBorrower(borrowersDealsRequestDto.getFeeROIforBorrower());
				}
				if (borrowersDealsRequestDto.getFeeCollectedFromBorrower() != null) {
					oxyBorrowersDeal
							.setFeeCollectedFromBorrower(borrowersDealsRequestDto.getFeeCollectedFromBorrower());
				}

				if (borrowersDealsRequestDto.getDealType() != null) {
					oxyBorrowersDeal
							.setDealType(DealType.valueOf(borrowersDealsRequestDto.getDealType().toUpperCase()));
				}

				if (borrowersDealsRequestDto.getWithdrawalStatus() != null) {
					oxyBorrowersDeal.setAnyTimeWithdrawal(OxyBorrowersDealsInformation.AnyTimeWithdrawal
							.valueOf(borrowersDealsRequestDto.getWithdrawalStatus()));
				}

				if (borrowersDealsRequestDto.getWithdrawalRoi() != null) {
					oxyBorrowersDeal.setRoiForWithdrawal(borrowersDealsRequestDto.getWithdrawalRoi());
				}

				List<DealLevelRequestDto> borroweridsAndAmounts = borrowersDealsRequestDto.getIdsWithLoanAmount();
				if (borroweridsAndAmounts != null && !borroweridsAndAmounts.isEmpty()) {
					StringBuilder listOfBorrowerids = new StringBuilder();
					StringBuilder listOfLoanAmounts = new StringBuilder();

					borroweridsAndAmounts.forEach(borrowerInfo -> {
						User user = userRepo.findByIdNum(borrowerInfo.getBorrowerId());
						if (user != null) {
							listOfBorrowerids.append(borrowerInfo.getBorrowerId()).append(",");
							listOfLoanAmounts.append(borrowerInfo.getLoanAmount()).append(",");
						}
					});

					oxyBorrowersDeal.setBorrowersIdsMappedToDeal(listOfBorrowerids.toString());
					oxyBorrowersDeal.setBorrowersAmountMappedTodeal(listOfLoanAmounts.toString());
				}
				if (borrowersDealsRequestDto.getEnachStatus() != null) {
					oxyBorrowersDeal.setEnachStatus(borrowersDealsRequestDto.getEnachStatus() != null);
				}
				if (borrowersDealsRequestDto.getMinimumPaticipationAmount() != null) {
					oxyBorrowersDeal
							.setMinimumPaticipationAmount(borrowersDealsRequestDto.getMinimumPaticipationAmount());
				}
				if (borrowersDealsRequestDto.getDealSubtype() != null) {
					oxyBorrowersDeal.setDealSubType(OxyBorrowersDealsInformation.DealSubtype
							.valueOf(borrowersDealsRequestDto.getDealSubtype()));

				}
				if (borrowersDealsRequestDto.getFeeStatusToParticipate() != null) {
					oxyBorrowersDeal.setFeeStatusToParticipate(OxyBorrowersDealsInformation.FeeStatusToParticipate
							.valueOf(borrowersDealsRequestDto.getFeeStatusToParticipate()));
				}

				oxyBorrowersDeal.setDealOpenStatus(OxyBorrowersDealsInformation.DealOpenStatus
						.valueOf(borrowersDealsRequestDto.getDealOpenStatus()));

				oxyBorrowersDealsInformationRepo.save(oxyBorrowersDeal);
				// dealCreationAndEditHistory(oxyBorrowersDeal);

				if (borrowersDealsRequestDto.getDealOpenStatus()
						.equals(OxyBorrowersDealsInformation.DealOpenStatus.NOW.toString())) {

					if (borrowersDealsRequestDto.getWhatsappChatId() != null && !borrowersDealsRequestDto.getDealType()
							.equals(OxyBorrowersDealsInformation.DealType.TEST.toString())) {
						whatappServiceRepo.sendingMsgToUsingUltra(oxyBorrowersDeal, oxyBorrowersDeal.getId(),
								borrowersDealsRequestDto.getWhatsappChatId());

					}
					// oxyBorrowersDealsInformationRepo.save(oxyBorrowersDeal);
				}

				else {

					oxyBorrowersDealsInformation.setDealLaunchHoure(borrowersDealsRequestDto.getDealLaunchHoure());

					String newLunchHoure = Removehourse(oxyBorrowersDealsInformation.getDealLaunchHoure());

					oxyBorrowersDeal.setNewDealLunchHoure(newLunchHoure);
					try {
						oxyBorrowersDeal.setDealFutureDate(
								expectedDateFormat.parse(borrowersDealsRequestDto.getDealFutureDate()));
					} catch (ParseException e) {
						e.printStackTrace();
					}

					oxyBorrowersDeal.setDealParticipationStatus(
							OxyBorrowersDealsInformation.DealParticipationStatus.OPENINFUTURE);
					oxyBorrowersDealsInformationRepo.save(oxyBorrowersDealsInformation);
				}

				oxyBorrowersDealsInformationRepo.save(oxyBorrowersDeal);

				OxyDealsRateofinterestToLendersgroup OxyDealsForNewLenders = oxyDealsRateofinterestToLendersgroupRepo
						.getDealMappedToSingleGroup(id, 0);

				if (borrowersDealsRequestDto.getNewLendersMonthlyInterest() != null) {
					OxyDealsForNewLenders.setMonthlyInterest(borrowersDealsRequestDto.getNewLendersMonthlyInterest());
				}
				if (borrowersDealsRequestDto.getNewLendersQuartelyInterest() != null) {

					OxyDealsForNewLenders.setQuartelyInterest(borrowersDealsRequestDto.getNewLendersQuartelyInterest());
				}
				if (borrowersDealsRequestDto.getNewLendersHalfInterest() != null) {
					OxyDealsForNewLenders.setHalfInterest(borrowersDealsRequestDto.getNewLendersHalfInterest());
				}
				if (borrowersDealsRequestDto.getNewLendersYearlyInterest() != null) {
					OxyDealsForNewLenders.setYearlyInterest(borrowersDealsRequestDto.getNewLendersYearlyInterest());
				}
				if (borrowersDealsRequestDto.getNewLendersEndOfTheDealInterest() != null) {
					OxyDealsForNewLenders
							.setEndofthedealInterest(borrowersDealsRequestDto.getNewLendersEndOfTheDealInterest());
				}

				oxyDealsRateofinterestToLendersgroupRepo.save(OxyDealsForNewLenders);

				OxyDealsRateofinterestToLendersgroup OxyDealsForOxyPremium = oxyDealsRateofinterestToLendersgroupRepo
						.getDealMappedToSingleGroup(id, 3);
				if (borrowersDealsRequestDto.getOxyPremiumMonthlyInterest() != null) {
					OxyDealsForOxyPremium.setMonthlyInterest(borrowersDealsRequestDto.getOxyPremiumMonthlyInterest());
				}
				if (borrowersDealsRequestDto.getOxyPremiumQuartelyInterest() != null) {
					OxyDealsForOxyPremium.setQuartelyInterest(borrowersDealsRequestDto.getOxyPremiumQuartelyInterest());
				}
				if (borrowersDealsRequestDto.getOxyPremiumHalfInterest() != null) {
					OxyDealsForOxyPremium.setHalfInterest(borrowersDealsRequestDto.getOxyPremiumHalfInterest());
				}
				if (borrowersDealsRequestDto.getOxyPremiumYearlyInterest() != null) {
					OxyDealsForOxyPremium.setYearlyInterest(borrowersDealsRequestDto.getOxyPremiumYearlyInterest());
				}
				if (borrowersDealsRequestDto.getOxyPremiumEndOfTheDealInterest() != null) {
					OxyDealsForOxyPremium
							.setEndofthedealInterest(borrowersDealsRequestDto.getOxyPremiumEndOfTheDealInterest());
				}

				oxyDealsRateofinterestToLendersgroupRepo.save(OxyDealsForOxyPremium);

			} else {
				throw new OperationNotAllowedException("Deal not present with given deal id please check",
						ErrorCodes.USER_NOT_FOUND);

			}
			 dealCreationAndEditHistory(oxyBorrowersDeal);
		}

		BorrowersDealsResponseDto borrowersDealsResponseDto = new BorrowersDealsResponseDto();
		borrowersDealsResponseDto.setDealId(oxyBorrowersDealsInformation.getId());
		borrowersDealsResponseDto.setDealName(oxyBorrowersDealsInformation.getBorrowerName());
		borrowersDealsResponseDto.setDealType(oxyBorrowersDealsInformation.getDealType().toString());
		return borrowersDealsResponseDto;

	}

	public String Removehourse(String dealLunchHoure) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

		LocalTime originalTime = LocalTime.parse(dealLunchHoure, formatter);

		LocalTime modifiedTime = originalTime.minusHours(5).minusMinutes(30);

		if (modifiedTime.isBefore(LocalTime.MIN)) {
			modifiedTime = modifiedTime.plusHours(24);
		}

		String modifiedTimeStr = modifiedTime.format(formatter);

		return modifiedTimeStr;
	}

	public String checkingDateValidation(BorrowersDealsRequestDto borrowersDealsRequestDto) {
		DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date currentDate = new Date();
		String dateInRequiredFormate = expectedDateFormat.format(currentDate.getTime());
		Date current = null;
		Date fundsStart = null;
		Date fundsEnd = null;
		Date interest = null;
		try {
			current = expectedDateFormat.parse(dateInRequiredFormate);
			if (borrowersDealsRequestDto.getFundsAcceptanceStartDate() != null) {
				fundsStart = expectedDateFormat.parse(borrowersDealsRequestDto.getFundsAcceptanceStartDate());
			}
			if (borrowersDealsRequestDto.getFundsAcceptanceEndDate() != null) {
				fundsEnd = expectedDateFormat.parse(borrowersDealsRequestDto.getFundsAcceptanceEndDate());
			}
			if (borrowersDealsRequestDto.getLoanActiveDate() != null) {
				interest = expectedDateFormat.parse(borrowersDealsRequestDto.getLoanActiveDate());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (borrowersDealsRequestDto.getFundsAcceptanceStartDate() != null) {
			if ((current.compareTo(fundsStart) == 0) || (current.compareTo(fundsStart) < 0)) {
				logger.info("Given fundsStart date is valid to dealName" + borrowersDealsRequestDto.getDealName());
			} else {
				throw new OperationNotAllowedException("Funds start date is not validate please check",
						ErrorCodes.DATA_PERSIST_ERROR);
			}
		}
		if ((borrowersDealsRequestDto.getFundsAcceptanceEndDate() != null)
				&& (borrowersDealsRequestDto.getFundsAcceptanceStartDate() != null)) {
			if ((fundsStart.compareTo(fundsEnd) < 0) || (fundsStart.compareTo(fundsEnd) == 0)) {
				logger.info("Given fundsEnd date is valid to dealName" + borrowersDealsRequestDto.getDealName());
			} else {
				throw new OperationNotAllowedException("Funds end date is not validate please check",
						ErrorCodes.DATA_PERSIST_ERROR);
			}
		}

		if ((borrowersDealsRequestDto.getFundsAcceptanceEndDate() != null)
				&& (borrowersDealsRequestDto.getLoanActiveDate() != null)) {
			if ((fundsEnd.compareTo(interest) < 0)) {
				logger.info("Given interest date is valid to dealName" + borrowersDealsRequestDto.getDealName());
			} else {
				throw new OperationNotAllowedException("Interest date is not validate please check",
						ErrorCodes.DATA_PERSIST_ERROR);
			}
		}

		return null;

	}

	@Override
	public BorrowersDealsList getListOfDealsInformation(PaginationRequestDto pageRequestDto) {
		int pageNo = pageRequestDto.getPageNo();
		int pageSize = pageRequestDto.getPageSize();

		pageNo = (pageSize * (pageNo - 1));

		String dealStatusForFrontEnd = null;
		String dealStatus = null;
		
String dealOpenStatus = null;
		
		String dealType = pageRequestDto.getDealType().toUpperCase();

		if (dealType.equalsIgnoreCase("CLOSED")) {
		    dealStatusForFrontEnd = "Achieved";
		    dealStatus = "ACHIEVED";
		} else if (dealType.equalsIgnoreCase("OPENINFUTURE")) {
		    dealStatusForFrontEnd = "Yet to be open in future";
		    dealStatus = "OPENINFUTURE";
		} else {
		    dealStatusForFrontEnd = "Yet to be Achieved";
		    dealStatus = "NOTATACHIEVED";
		}
		
		BorrowersDealsList borrowersDealsList = new BorrowersDealsList();
		List<Object[]> listOfDeals = oxyBorrowersDealsInformationRepo.getListOfDealsInformationForNormalDeals(pageNo,
				pageSize, dealStatus);

		logger.info("listOfDeal size .."+listOfDeals.size());
		
		Integer count = oxyBorrowersDealsInformationRepo.getListOfDealsInformationCountForNormalDeals(dealStatus);
		Integer totalCount = 0;
		if (count != null) {
			totalCount = count;
			
			logger.info("totalCount  .."+totalCount);
		}
		
		
		List<BorrowersDealsResponseDto> borrowersDealsResponse = new ArrayList<BorrowersDealsResponseDto>();
		if (listOfDeals != null && !listOfDeals.isEmpty()) {
			Iterator it = listOfDeals.iterator();
			while (it.hasNext()) {
				Object[] e = (Object[]) it.next();
				BorrowersDealsResponseDto borrowersDealsInfo = new BorrowersDealsResponseDto();

				int dealId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());

				borrowersDealsInfo.setDealId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
				borrowersDealsInfo.setDealName(e[1] == null ? " " : e[1].toString());
				borrowersDealsInfo.setBorrowerName(e[2] == null ? " " : e[2].toString());
				borrowersDealsInfo.setBorrowerRateOfInterest(Double.parseDouble(e[6] == null ? "0" : e[6].toString()));
				borrowersDealsInfo.setFundsAcceptanceStartDate(e[4] == null ? " " : e[4].toString());
				borrowersDealsInfo.setFundsAcceptanceEndDate(e[5] == null ? " " : e[5].toString());
				borrowersDealsInfo.setDealAmount(Double.parseDouble(e[3] == null ? "0" : e[3].toString()));
				borrowersDealsInfo.setDealLink(e[7] == null ? " " : e[7].toString());
				borrowersDealsInfo.setDuration(Integer.parseInt(e[8] == null ? "0" : e[8].toString()));
				borrowersDealsInfo.setWhatAppResponseUrl(e[9] == null ? " " : e[9].toString());
				borrowersDealsInfo.setWhatAppLinkSentToLendersGroup(e[10] == null ? " " : e[10].toString());
				borrowersDealsInfo.setLoanActiveDate(e[11] == null ? " " : e[11].toString());
				borrowersDealsInfo.setMessageSentToLenders(e[12] == null ? " " : e[12].toString());
				borrowersDealsInfo.setEmiEndDate(e[18] == null ? "" : e[18].toString());
				if (e[17] != null) {
					String lastParticipationDate = null;

					logger.info(e[17].toString());

					Date date = new Date();
					try {
						date = simpleDateFormat1.parse(e[17].toString());

						lastParticipationDate = addfiveAndHalfHoursToTimeStamp(date);

						borrowersDealsInfo.setLastParticipationDate(lastParticipationDate);

					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}

				OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
						.getFirstParticipationDate(dealId);

				if (oxyLendersAcceptedDeals != null) {

					String firstParticipationDate = addfiveAndHalfHoursToTimeStamp(
							oxyLendersAcceptedDeals.getReceivedOn());

					borrowersDealsInfo.setFirstParticipationDate(firstParticipationDate);
				}

				borrowersDealsInfo
						.setPaticipationLimitToLenders(Double.parseDouble(e[13] == null ? " " : e[13].toString()));

				borrowersDealsInfo.setGetIndividualDealsInformation(getIndividualDealsInformation(dealId));
				Double lenderAverageValue = oxyDealsRateofinterestToLendersgroupRepo.getAverageValuForLender(dealId);
				borrowersDealsInfo.setAverageValueForLender(lenderAverageValue);
				Double particiaptionAmount = 0.0;
				Double dealAmount = Double.parseDouble(e[3] == null ? "0" : e[3].toString());
				Double amount = oxyLendersAcceptedDealsRepo.getListOfLendersParticipatedAmount(dealId);
				Double updatedAmount = lendersPaticipationUpdationRepo.getDealUpdatedSumValue(dealId);
				if (amount != null) {
					if (updatedAmount != null) {
						particiaptionAmount = amount + updatedAmount;
					} else {
						particiaptionAmount = amount;
					}
				}
				borrowersDealsInfo.setDealPaticipatedAmount(BigDecimal.valueOf(particiaptionAmount).toBigInteger());

				String closingStatus = e[14] == null ? " " : e[14].toString();

				Double returnedPrincipalAmount = lendersReturnsRepo.getSumValueReturnedToDeal(dealId);
				Double returnedToWallet = lenderOxyWalletNativeRepo.sumOfAmountReturnedFromDealToWallet(dealId);
				double currentAmount = 0.0;
				if (returnedPrincipalAmount == null && returnedToWallet == null) {
					currentAmount = particiaptionAmount;
				}
				if (returnedPrincipalAmount != null && returnedToWallet != null) {
					currentAmount = particiaptionAmount - (returnedPrincipalAmount + returnedToWallet);

				} else {
					if (returnedPrincipalAmount == null && returnedToWallet != null) {
						currentAmount = particiaptionAmount - returnedToWallet;
					}
					if (returnedPrincipalAmount != null && returnedToWallet == null) {
						currentAmount = particiaptionAmount - returnedPrincipalAmount;
					}
				}
				if (returnedPrincipalAmount != null) {
					borrowersDealsInfo.setWithdrawalAndPrincipalReturned(
							BigDecimal.valueOf(returnedPrincipalAmount).toBigInteger());
				}
				if (returnedToWallet != null) {
					borrowersDealsInfo
							.setDealAmountReturnedToWallet(BigDecimal.valueOf(returnedToWallet).toBigInteger());
				}

				borrowersDealsInfo.setDealCurrentAmount(BigDecimal.valueOf(currentAmount).toBigInteger());

				borrowersDealsInfo.setFundingStatus(dealStatusForFrontEnd);

				Double disbursedAmount = oxyLoanRepo.getDealIdDisbursmentCompletedAmount(dealId);
				if (disbursedAmount != null) {
					if (dealAmount.equals(disbursedAmount)) {
						borrowersDealsInfo.setAgreementsGenerationStatus("COMPLETED");
					} else {
						borrowersDealsInfo.setAgreementsGenerationStatus("PENDING");
					}

				} else {
					borrowersDealsInfo.setAgreementsGenerationStatus("PENDING");
				}
				Double roi = oxyDealsRateofinterestToLendersgroupRepo.getMaxRoiToDeal(dealId);
				borrowersDealsInfo.setRateOfInterest(roi);
				borrowersDealsResponse.add(borrowersDealsInfo);
			}

		}

		borrowersDealsList.setCount(totalCount);
		borrowersDealsList.setListOfBorrowersDealsResponseDto(borrowersDealsResponse);
		return borrowersDealsList;

	}

	public String addfiveAndHalfHoursToTimeStamp(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR_OF_DAY, 5);

		calendar.add(Calendar.MINUTE, 30);

		return simpleDateFormat.format(calendar.getTime());

	}

	public List<BorrowerDealRateofInterestResponseDto> getIndividualDealsInformation(int dealId) {
		List<Object[]> listOfLendersGroupRoi = oxyDealsRateofinterestToLendersgroupRepo
				.getDealMappedToGroupOfLenders(dealId);
		List<BorrowerDealRateofInterestResponseDto> listOfDealInformation = new ArrayList<BorrowerDealRateofInterestResponseDto>();

		if (listOfLendersGroupRoi != null && !listOfLendersGroupRoi.isEmpty()) {
			Iterator it = listOfLendersGroupRoi.iterator();

			while (it.hasNext()) {
				Object[] e = (Object[]) it.next();
				BorrowerDealRateofInterestResponseDto borrowerDealRateofInterestResponseDto = new BorrowerDealRateofInterestResponseDto();
				Integer id = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
				String groupName = oxyLendersGroupRepo.getLenderGroupNameByid(id);
				String lenderGroupName = null;
				if (groupName != null) {
					lenderGroupName = groupName;
				} else {
					lenderGroupName = "newLender";
				}
				borrowerDealRateofInterestResponseDto.setGroupId(id);
				borrowerDealRateofInterestResponseDto.setGroupownerName(lenderGroupName);
				borrowerDealRateofInterestResponseDto
						.setMonthlyInterest(Double.parseDouble(e[1] == null ? "0" : e[1].toString()));
				borrowerDealRateofInterestResponseDto
						.setQuartelyInterest(Double.parseDouble(e[2] == null ? "0" : e[2].toString()));
				borrowerDealRateofInterestResponseDto
						.setHalfInterest(Double.parseDouble(e[3] == null ? "0" : e[3].toString()));
				borrowerDealRateofInterestResponseDto
						.setYearlyInterest(Double.parseDouble(e[4] == null ? "0" : e[4].toString()));
				double yearlyRoi = Double.parseDouble(e[4] == null ? "0" : e[4].toString());

				borrowerDealRateofInterestResponseDto
						.setEndOfTheDealInterest(Double.parseDouble(e[5] == null ? "0" : e[5].toString()));

				listOfDealInformation.add(borrowerDealRateofInterestResponseDto);
			}
		}
		return listOfDealInformation;

	}

	@Override
	public BorrowersDealsResponseDto getSingleDeal(int dealId) {
		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
				.findDealAlreadyGiven(dealId);
		BorrowersDealsResponseDto borrowersDealsInfo = new BorrowersDealsResponseDto();
		if (oxyBorrowersDealsInformation != null) {

			borrowersDealsInfo.setDealId(dealId);
			borrowersDealsInfo.setDealName(oxyBorrowersDealsInformation.getDealName());
			borrowersDealsInfo.setBorrowerName(oxyBorrowersDealsInformation.getBorrowerName());
			borrowersDealsInfo.setBorrowerRateOfInterest(oxyBorrowersDealsInformation.getBorrowerRateofinterest());
			borrowersDealsInfo.setFundsAcceptanceStartDate(
					expectedDateFormat.format(oxyBorrowersDealsInformation.getFundsAcceptanceStartDate()));
			borrowersDealsInfo.setFundsAcceptanceEndDate(
					expectedDateFormat.format(oxyBorrowersDealsInformation.getFundsAcceptanceEndDate()));
			borrowersDealsInfo
					.setParticipationLenderType(oxyBorrowersDealsInformation.getParticipationLenderType().toString());

			borrowersDealsInfo.setDealAmount(oxyBorrowersDealsInformation.getDealAmount());
			borrowersDealsInfo.setDealLink(oxyBorrowersDealsInformation.getDealLink());
			borrowersDealsInfo.setGetIndividualDealsInformation(getIndividualDealsInformation(dealId));
			borrowersDealsInfo.setDuration(oxyBorrowersDealsInformation.getDuration());
			if (oxyBorrowersDealsInformation.getMinimumPaticipationAmount() != null) {
				borrowersDealsInfo.setMinimumPaticipationAmount(
						BigDecimal.valueOf(oxyBorrowersDealsInformation.getMinimumPaticipationAmount()).toBigInteger());
			}
			Double lenderAverageValue = oxyDealsRateofinterestToLendersgroupRepo.getAverageValuForLender(dealId);

			if (oxyBorrowersDealsInformation.getLoanActiveDate() != null) {
				borrowersDealsInfo
						.setLoanActiveDate(expectedDateFormat.format(oxyBorrowersDealsInformation.getLoanActiveDate()));

			} else {
				borrowersDealsInfo.setLoanActiveDate("");
			}
			if (oxyBorrowersDealsInformation.getWhatappMessageToLenders() != null) {
				borrowersDealsInfo.setMessageSentToLenders(oxyBorrowersDealsInformation.getWhatappMessageToLenders());
			}
			if (oxyBorrowersDealsInformation.getPaticipationLimitToLenders() != null) {
				borrowersDealsInfo
						.setPaticipationLimitToLenders(oxyBorrowersDealsInformation.getPaticipationLimitToLenders());
			}
			if (oxyBorrowersDealsInformation.getFeeCollectedFromBorrower() != null) {
				borrowersDealsInfo
						.setFeeCollectedFromBorrower(oxyBorrowersDealsInformation.getFeeCollectedFromBorrower());

			}
			if (oxyBorrowersDealsInformation.getFeeROIForBorrower() != null) {
				borrowersDealsInfo.setFeeROIforBorrower(oxyBorrowersDealsInformation.getFeeROIForBorrower());
			}
			if (oxyBorrowersDealsInformation.getDealType() != null) {
				borrowersDealsInfo.setDealType(oxyBorrowersDealsInformation.getDealType().toString());
			}
			if (oxyBorrowersDealsInformation.getAnyTimeWithdrawal() != null) {
				borrowersDealsInfo.setWithDrawalStatus(oxyBorrowersDealsInformation.getAnyTimeWithdrawal().toString());
			}
			if (oxyBorrowersDealsInformation.getRoiForWithdrawal() != null) {
				borrowersDealsInfo.setWithDrawalRoi(oxyBorrowersDealsInformation.getRoiForWithdrawal());
			}

			if (oxyBorrowersDealsInformation.getWhatappChatid() != null) {
				borrowersDealsInfo.setWhatsappGroupChatId(oxyBorrowersDealsInformation.getWhatappChatid());
			}
			if (oxyBorrowersDealsInformation.getWhatappReponseLink() != null) {
				borrowersDealsInfo.setWhatAppResponseUrl(oxyBorrowersDealsInformation.getWhatappReponseLink());
			}
			
			if (oxyBorrowersDealsInformation.getDealOpenStatus() != null) {
				borrowersDealsInfo.setDealOpenStatus(oxyBorrowersDealsInformation.getDealOpenStatus().toString());
			}

			OxyBorrowersDealsInformation statusToCheckDealType = oxyBorrowersDealsInformationRepo
					.dealGroupUpdationStatus(dealId);
			if (statusToCheckDealType == null) {
				borrowersDealsInfo.setStatusToDisplayTotalGroups("Display");
			} else {
				borrowersDealsInfo.setStatusToDisplayTotalGroups("Hide");
			}
			if (oxyBorrowersDealsInformation.getBorrowersIdsMappedToDeal() != null) {
				borrowersDealsInfo
						.setBorrowerIdsMappedTodeal(oxyBorrowersDealsInformation.getBorrowersIdsMappedToDeal());
			}
			if (oxyBorrowersDealsInformation.getEnachStatus() != null) {
				borrowersDealsInfo.setEnachStatus(oxyBorrowersDealsInformation.getEnachStatus());
			}

			if (oxyBorrowersDealsInformation.getLifeTimeWaiver() != null) {
				borrowersDealsInfo.setLifeTimeWaiver(oxyBorrowersDealsInformation.getLifeTimeWaiver());
			}
			if (oxyBorrowersDealsInformation.getLifeTimeWaiverLimit() != null) {
				borrowersDealsInfo
						.setLifeTimeWaiverLimit(Double.valueOf(oxyBorrowersDealsInformation.getLifeTimeWaiverLimit()));
			}
			borrowersDealsInfo.setDealSubtype(oxyBorrowersDealsInformation.getDealSubType().toString());
			borrowersDealsInfo
					.setFeeStatusToParticipate(oxyBorrowersDealsInformation.getFeeStatusToParticipate().toString());

			String idsMappedTodeal = oxyBorrowersDealsInformation.getBorrowersIdsMappedToDeal();
			if (idsMappedTodeal != null && !idsMappedTodeal.isEmpty()) {
				String borrowerIds[] = idsMappedTodeal.split(",");
				String loanAmountsMappedToDeal = oxyBorrowersDealsInformation.getBorrowersAmountMappedTodeal();
				String amounts[] = loanAmountsMappedToDeal.split(",");
				List<DealLevelRequestDto> idsMappedWithAmounts = new ArrayList<DealLevelRequestDto>();
				for (int i = 0; i < borrowerIds.length; i++) {
					for (int j = 0; j < amounts.length; j++) {
						if (i == j) {
							DealLevelRequestDto dealLevelRequestDto = new DealLevelRequestDto();
							dealLevelRequestDto.setBorrowerId(Integer.parseInt(borrowerIds[i]));
							dealLevelRequestDto.setLoanAmount(Double.parseDouble(amounts[j]));
							idsMappedWithAmounts.add(dealLevelRequestDto);
						}
					}
				}

				borrowersDealsInfo.setIdsWithLoanAmount(idsMappedWithAmounts);

			}
		}

		return borrowersDealsInfo;

	}

	@Override
	public LenderMappedToGroupIdResponseDto getListOfLendersMappedToGroupId(int groupId) {
		LenderMappedToGroupIdResponseDto lenderMappedToGroupIdResponseDto = new LenderMappedToGroupIdResponseDto();
		List<LenderDetailsByGroupId> listLenderDetailsByGroupId = new ArrayList<LenderDetailsByGroupId>();
		Integer count = 0;
		String groupName = null;
		Integer value = userRepo.getListOfLendersMappedToGroupIdCount(groupId);
		if (value != null) {
			count = value;
		}
		try {
			List<Object[]> listOfLendes = userRepo.getListOfLendersMappedToGroupId(groupId);
			if (listOfLendes != null && !listOfLendes.isEmpty()) {
				Iterator it = listOfLendes.iterator();
				while (it.hasNext()) {
					Object[] e = (Object[]) it.next();
					LenderDetailsByGroupId lenderDetailsByGroupId = new LenderDetailsByGroupId();
					lenderDetailsByGroupId.setLenderId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
					lenderDetailsByGroupId.setEmail(e[1] == null ? " " : e[1].toString());
					lenderDetailsByGroupId.setMobileNumber(e[2] == null ? " " : e[2].toString());
					String firstName = e[3] == null ? " " : e[3].toString();
					String lastName = e[4] == null ? " " : e[4].toString();
					lenderDetailsByGroupId.setName(firstName + " " + lastName);
					lenderDetailsByGroupId.setGroupId(groupId);
					if (groupId == 0) {
						groupName = "newLender";
					} else {
						groupName = oxyLendersGroupRepo.getLenderGroupNameByid(groupId);

					}
					lenderDetailsByGroupId.setGroupName(groupName);
					listLenderDetailsByGroupId.add(lenderDetailsByGroupId);
				}

			}
			lenderMappedToGroupIdResponseDto.setCount(count);
			lenderMappedToGroupIdResponseDto.setListOfLendersDetailsByGroupId(listLenderDetailsByGroupId);
		} catch (Exception e) {
			logger.info("Exception in getListOfLendersMappedToGroupId method ");
		}
		return lenderMappedToGroupIdResponseDto;

	}

	@Override
	public LendersDealsResponseDto listOfDealsInformationDispayingToLender(int userId,
			PaginationRequestDto pageRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public DealInformationRoiToLender getSigleDealInformation(int userId, int dealId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderPaticipatedDeal getLenderPaticipatedDealsInformation(int userId, PaginationRequestDto pageRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderPaticipatedDeal getListOfLendersByDealId(int dealId) {
		LenderPaticipatedDeal lenderPaticipatedDeal = new LenderPaticipatedDeal();
		List<Object[]> listOfLendersMappedToDealId = oxyLendersAcceptedDealsRepo.getListOfLendersMappedToDealId(dealId);
		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
				.findDealAlreadyGiven(dealId);
		Integer count = 0;
		String dealName = "";
		Double paticipatedAmount = 0.0;
		Integer value = oxyLendersAcceptedDealsRepo.getListOfLendersMappedToDealIdCount(dealId);
		if (value != null) {
			count = value;
		}
		Double amount = oxyLendersAcceptedDealsRepo.getListOfLendersParticipatedAmount(dealId);
		Double updatedAmount = lendersPaticipationUpdationRepo.getDealUpdatedSumValue(dealId);
		if (amount != null) {
			if (updatedAmount != null) {
				paticipatedAmount = amount + updatedAmount;
			} else {
				paticipatedAmount = amount;
			}
		}
		List<LenderPaticipatedResponseDto> listOfLenders = new ArrayList<LenderPaticipatedResponseDto>();
		if (listOfLendersMappedToDealId != null && !listOfLendersMappedToDealId.isEmpty()) {
			Iterator it = listOfLendersMappedToDealId.iterator();
			String groupName = null;
			while (it.hasNext()) {
				Object[] e = (Object[]) it.next();
				LenderPaticipatedResponseDto lenderPaticipatedResponseDto = new LenderPaticipatedResponseDto();
				lenderPaticipatedResponseDto.setLenderId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
				lenderPaticipatedResponseDto.setGroupId(Integer.parseInt(e[1] == null ? "0" : e[1].toString()));
				int groupId = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
				if (groupId == 0) {
					groupName = "newLender";
				} else {
					groupName = oxyLendersGroupRepo.getLenderGroupNameByid(groupId);
				}
				int userId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
				String lenderName = null;
				User user = userRepo.findById(userId).get();
				if (user != null) {
					lenderName = user.getPersonalDetails().getFirstName() + " "
							+ user.getPersonalDetails().getLastName();
				}

				if (user.getBankDetails() != null) {
					if (user.getBankDetails().getAccountNumber() != null) {
						lenderPaticipatedResponseDto.setAccountNumber(user.getBankDetails().getAccountNumber());
					}
					if (user.getBankDetails().getIfscCode() != null) {
						lenderPaticipatedResponseDto.setIfscCode(user.getBankDetails().getIfscCode());
					}
				}
				Double dealAmount = 0.0;
				Double participationUpdated = lendersPaticipationUpdationRepo.getSumOfLenderUpdatedAmountToDeal(userId,
						dealId);
				if (participationUpdated != null) {
					dealAmount = Double.parseDouble(e[2] == null ? "0" : e[2].toString()) + participationUpdated;
				} else {
					dealAmount = Double.parseDouble(e[2] == null ? "0" : e[2].toString());
				}
				Double sumPricipalReturnedAmount = lendersReturnsRepo.getSumOfAmountReturned(userId, "LENDERPRINCIPAL",
						dealId);
				Double currentValue = 0.0;
				Double participationCurrentValue = 0.0;
				Double currentParticipation = 0.0;
				Double currentAmount = 0.0;
				if (sumPricipalReturnedAmount != null) {
					currentValue = dealAmount - sumPricipalReturnedAmount;
				} else {
					currentValue = dealAmount;
				}
				Double sumOfWithDramAmount = lendersReturnsRepo.getLenderWithdrawSumValue(userId, dealId);
				if (sumOfWithDramAmount != null) {
					participationCurrentValue = currentValue - sumOfWithDramAmount;
				} else {
					participationCurrentValue = currentValue;
				}

				Double sumOfWalletAmount = lenderOxyWalletNativeRepo.paticipatedAmountMovedToWalletSum(userId, dealId);
				if (sumOfWalletAmount != null) {
					currentParticipation = participationCurrentValue - sumOfWalletAmount;
				} else {
					currentParticipation = participationCurrentValue;
				}

				Double holdAmount = userHoldAmountMappedToDealRepo.principalDebited(userId, dealId);
				if (holdAmount != null) {
					currentAmount = currentParticipation - holdAmount;
				} else {
					currentAmount = currentParticipation;
				}

				lenderPaticipatedResponseDto.setPaticipatedAmount(dealAmount);
				lenderPaticipatedResponseDto.setCurrentAmount(BigDecimal.valueOf(currentAmount).toBigInteger());
				lenderPaticipatedResponseDto.setProcessingFee(Double.parseDouble(e[3] == null ? "0" : e[3].toString()));
				lenderPaticipatedResponseDto.setFeeStatus(e[4] == null ? " " : e[4].toString());
				lenderPaticipatedResponseDto.setLederReturnType(e[5] == null ? " " : e[5].toString());
				OxyBorrowersDealsInformation oxyBorrowers = oxyBorrowersDealsInformationRepo
						.toGetCalculationType(dealId);
				double rateOfInterest = Double.valueOf(e[6] == null ? "0" : e[6].toString());
				if (oxyBorrowers != null) {
					lenderPaticipatedResponseDto.setRateOfInterest(rateOfInterest);
				} else {
					lenderPaticipatedResponseDto.setRateOfInterest(rateOfInterest / 12);
				}
				lenderPaticipatedResponseDto.setRegisteredDate(e[7] == null ? " " : e[7].toString());
				lenderPaticipatedResponseDto.setPaticipatedState(e[8] == null ? " " : e[8].toString());
				lenderPaticipatedResponseDto.setAccountType(e[9] == null ? " " : e[9].toString());
				lenderPaticipatedResponseDto.setDealId(dealId);
				lenderPaticipatedResponseDto.setDealName(oxyBorrowersDealsInformation.getDealName());
				dealName = oxyBorrowersDealsInformation.getDealName();
				lenderPaticipatedResponseDto.setDealBorrowerName(oxyBorrowersDealsInformation.getBorrowerName());
				lenderPaticipatedResponseDto.setDealAmount(oxyBorrowersDealsInformation.getDealAmount());
				lenderPaticipatedResponseDto
						.setDealRateofinterest(oxyBorrowersDealsInformation.getBorrowerRateofinterest());
				lenderPaticipatedResponseDto.setDealDuration(oxyBorrowersDealsInformation.getDuration());
				lenderPaticipatedResponseDto.setGroupName(groupName);
				lenderPaticipatedResponseDto.setLenderName(lenderName);
				List<String> principalReturnedToLender = newAdminLoanService.listOfPrincipalReturned(userId, dealId);
				if (!principalReturnedToLender.isEmpty()) {
					lenderPaticipatedResponseDto.setListOfPrincipalReturned(principalReturnedToLender);
				} else {
					lenderPaticipatedResponseDto.setListOfPrincipalReturned(null);
				}
				listOfLenders.add(lenderPaticipatedResponseDto);
			}
		}
		String url = generateParticipatedLendersExcelSheet(listOfLenders, dealName);

		lenderPaticipatedDeal.setParticipatedLendersExcel(url);
		lenderPaticipatedDeal.setLenderPaticipatedResponseDto(listOfLenders);
		lenderPaticipatedDeal.setCount(count);
		lenderPaticipatedDeal.setTotalAmountPaticipatedByLenders(BigDecimal.valueOf(paticipatedAmount).toBigInteger());
		return lenderPaticipatedDeal;

	}

	private String generateParticipatedLendersExcelSheet(List<LenderPaticipatedResponseDto> listOfLenders,
			String dealName) {
		String name = dealName;
		name = name.replaceAll("[^a-zA-Z0-9]", " ");
		String excelurl = "";
		if (listOfLenders != null && !listOfLenders.isEmpty()) {
			FileOutputStream outFile = null;
			XSSFWorkbook workBook = new XSSFWorkbook();
			XSSFSheet spreadSheet = workBook.createSheet(name + ".xls");
			XSSFFont headerFont = workBook.createFont();
			CellStyle style = workBook.createCellStyle();

			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 13);
			headerFont.setColor(IndexedColors.BLACK.index);
			style.setFont(headerFont);
			spreadSheet.createFreezePane(0, 1);
			Row row = spreadSheet.createRow(0);

			Cell cell = row.createCell(0);
			cell.setCellValue("Deal Name");
			cell.setCellStyle(style);

			cell = row.createCell(1);
			cell.setCellValue("Lender id");
			cell.setCellStyle(style);

			cell = row.createCell(2);
			cell.setCellValue("Lender Name");
			cell.setCellStyle(style);

			cell = row.createCell(3);
			cell.setCellValue("Group Name");
			cell.setCellStyle(style);

			cell = row.createCell(4);
			cell.setCellValue("Date");
			cell.setCellStyle(style);

			cell = row.createCell(5);
			cell.setCellValue("participated Amount");
			cell.setCellStyle(style);

			cell = row.createCell(6);
			cell.setCellValue("ROI");
			cell.setCellStyle(style);

			cell = row.createCell(7);
			cell.setCellValue("Fee Status");
			cell.setCellStyle(style);

			cell = row.createCell(8);
			cell.setCellValue("Agreement Status");
			cell.setCellStyle(style);

			cell = row.createCell(9);
			cell.setCellValue("Account Number");
			cell.setCellStyle(style);

			cell = row.createCell(10);
			cell.setCellValue("IFSC Code");
			cell.setCellStyle(style);

			int rowCount = 0;
			for (int i = 0; i <= 10; i++) {
				spreadSheet.autoSizeColumn(i);
			}

			for (LenderPaticipatedResponseDto lenders : listOfLenders) {

				Row row1 = spreadSheet.createRow(++rowCount);

				Cell cell1 = row1.createCell(0);
				cell1.setCellValue(lenders.getDealName());

				cell1 = row1.createCell(1);
				cell1.setCellValue(lenders.getLenderId());

				String nameFromBankDetails = null;
				String groupName = null;
				User user = userRepo.findById(lenders.getLenderId()).get();
				if (user != null) {
					if (user.getBankDetails() != null) {
						if (user.getBankDetails().getUserName() != null) {
							nameFromBankDetails = user.getBankDetails().getUserName();
						}
					}
				}
				if (user.getLenderGroupId() == 0) {
					groupName = "newLender";
				} else {
					groupName = oxyLendersGroupRepo.getLenderGroupNameByid(user.getLenderGroupId());
				}

				cell1 = row1.createCell(2);
				cell1.setCellValue(nameFromBankDetails);

				cell1 = row1.createCell(3);
				cell1.setCellValue(groupName);

				cell1 = row1.createCell(4);
				cell1.setCellValue(lenders.getRegisteredDate());

				cell1 = row1.createCell(5);
				cell1.setCellValue(lenders.getPaticipatedAmount());

				cell1 = row1.createCell(6);
				cell1.setCellValue(lenders.getRateOfInterest());

				cell1 = row1.createCell(7);
				cell1.setCellValue(lenders.getFeeStatus());

				cell1 = row1.createCell(8);
				cell1.setCellValue(lenders.getPaticipatedState());

				cell1 = row1.createCell(9);
				cell1.setCellValue(lenders.getAccountNumber());

				cell1 = row1.createCell(10);
				cell1.setCellValue(lenders.getIfscCode());
			}
			String fileName = name + ".xls";
			File f = new File(fileName);

			try {
				outFile = new FileOutputStream(f);
				workBook.write(outFile);

				Calendar current = Calendar.getInstance();
				TemplateContext templateContext = new TemplateContext();
				String expectedCurrentDate = expectedDateFormat.format(current.getTime());

				FileRequest fileRequest = new FileRequest();
				fileRequest.setInputStream(new FileInputStream(f));
				fileRequest.setFileName(fileName);
				fileRequest.setFileType(FileType.Participatedlenders);
				fileRequest.setFilePrifix(FileType.Participatedlenders.name());

				fileManagementService.putFile(fileRequest);

				FileResponse getFile = fileManagementService.getFile(fileRequest);

				excelurl = getFile.getDownloadUrl();

			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
			}

			if (f.exists()) {
				f.delete();
			}

		}

		return excelurl;
	}

	@Override
	@Transactional
	public DealLevelResponseDto dealIdBasedAgreementsGeneration(int dealId, DealLevelRequestDto dealLevelRequestDto)
			throws PdfGeenrationException, IOException {
		DealLevelResponseDto dealLevelResponseDto = new DealLevelResponseDto();
		String message = null;
		OxyBorrowersDealsInformation dealinfo = oxyBorrowersDealsInformationRepo.findDealAlreadyGiven(dealId);
		String loanOwner = null;
		if (dealinfo != null) {
			loanOwner = dealinfo.getDealName();
			List<Object[]> listOfLendersMappedToDeal = oxyLendersAcceptedDealsRepo.getListOfLendersByDealId(dealId);
			if (listOfLendersMappedToDeal != null && !listOfLendersMappedToDeal.isEmpty()) {
				Iterator it = listOfLendersMappedToDeal.iterator();
				Double particiationAmount = 0.0;
				Double loanAmount = 0.0;
				Double lenderAmount = 0.0;
				while (it.hasNext()) {
					Object[] e = (Object[]) it.next();
					Integer userId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
					LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);

					Double totalLenderUpdatedAmount = lendersPaticipationUpdationRepo
							.getSumOfLenderUpdatedAmountToDeal(userId, dealId);
					if (totalLenderUpdatedAmount != null) {
						lenderAmount = Double.parseDouble(e[1] == null ? "0" : e[1].toString())
								+ totalLenderUpdatedAmount;
					} else {
						lenderAmount = Double.parseDouble(e[1] == null ? "0" : e[1].toString());
					}

					Double amountForAggrments = oxyLoanRepo.checkAlreadyAgreementsAgeneratedToLender(userId, dealId);
					if (amountForAggrments != null) {
						particiationAmount = lenderAmount - amountForAggrments;
					} else {
						particiationAmount = lenderAmount;
					}
					Double walletAmount = getLenderWallet(userId);
					if (walletAmount >= particiationAmount) {
						List<Integer> listOfBorrersMappedTodeal = userRepo
								.getListOfBorrowersMappedToLoanOwner(loanOwner);
						int countValueForPaticipation = 0;
						Double remainingAmount = 0.0;
						if (listOfBorrersMappedTodeal != null && !listOfBorrersMappedTodeal.isEmpty()) {
							for (int i = 0; i < listOfBorrersMappedTodeal.size(); i++) {
								if (particiationAmount != null && particiationAmount > 0) {

									int borrowerId = listOfBorrersMappedTodeal.get(i);

									Double currentApplicationAmount = loanRequestRepo
											.checkCurrentApplication(borrowerId);
									LoanRequest loanRequestForBorrower = loanRequestRepo
											.findByUserIdAndParentRequestIdIsNull(borrowerId);
									if (currentApplicationAmount != null) {
										Double amountForAgreementsGrenerated = oxyLoanRepo
												.checkAlreadyAgreementsAgenerated(borrowerId, dealId);

										int count = lenderBorrowerConversationRepo
												.countByLenderUserIdAndBorrowerUserId(userId, borrowerId);
										Double amount = 0.0;
										if (amountForAgreementsGrenerated != null) {
											amount = amountForAgreementsGrenerated;
										}
										remainingAmount = currentApplicationAmount - amount;
										if (remainingAmount > 0) {
											if (count == 0) {
												LoanRequestDto loanRequestDto = new LoanRequestDto();
												if (particiationAmount >= 50000) {
													loanAmount = 50000.0;
												} else {
													loanAmount = particiationAmount;
												}
												Double totalAmountDisbursed = oxyLoanRepo
														.getTotalAmountDisbursedForParticularrUsers(userId, borrowerId);
												if (totalAmountDisbursed == null || totalAmountDisbursed < 50000) {
													loanRequestDto.setLoanRequestAmount(loanAmount);
													loanRequestDto.setRateOfInterest(
															loanRequestForBorrower.getRateOfInterestToBorrower());
													loanRequestDto
															.setDuration(loanRequestForBorrower.getDurationBySir());
													loanRequestDto.setDurationType(
															loanRequestForBorrower.getDurationType().toString());
													loanRequestDto.setRepaymentMethod(
															loanRequestForBorrower.getRepaymentMethodForBorrower());
													loanRequestDto.setLoanPurpose("personal loan");
													loanRequestDto.setExpectedDate(expectedDateFormat
															.format(dealinfo.getFundsAcceptanceEndDate()));
													loanRequestDto.setParentRequestId(loanRequest.getId());
													LoanResponseForDealsDto loanRequestForDeals = loanRequestForDeals(
															borrowerId, loanRequestDto);
													Integer loanRequestId = loanRequestForDeals.getLoanRequestId();

													message = actOnRequestForDeals(userId, loanRequestId, "Accepted",
															dealId, dealLevelRequestDto);

													particiationAmount = particiationAmount - (50000);
													if (particiationAmount == 0 || particiationAmount < 0) {
														OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
																.toCheckUserAlreadyInvoledInDeal(userId, dealId);
														oxyLendersAcceptedDeals.setParticipationState(
																OxyLendersAcceptedDeals.ParticipationState.PARTICIPATED);
														oxyLendersAcceptedDealsRepo.save(oxyLendersAcceptedDeals);
													}
												}
											}
										}
									}
								}

							}

						}

					} else {
						continue;
					}

				}

			}
		}
		message = "successfullyUpdated";
		dealLevelResponseDto.setStatus(message);
		dealLevelResponseDto.setDealId(dealId);
		return dealLevelResponseDto;

	}

	public Double getLenderWallet(int userId) {
		Double walletAmount = 0.0;
		Double lenderWalletCreditAmount = lenderOxyWalletNativeRepo.lenderWalletCreditAmount(userId);
		Double lenderWalletdebitAmount = lenderOxyWalletNativeRepo.lenderWalletDebitAmount(userId);
		Double lenderWalletInprocessAmount = lenderOxyWalletNativeRepo.lenderWalletInprocessAmount(userId);
		if (!(lenderWalletInprocessAmount != null && lenderWalletInprocessAmount > 0)) {
			lenderWalletInprocessAmount = 0d;
		}
		if (!(lenderWalletCreditAmount != null && lenderWalletCreditAmount > 0)) {
			lenderWalletCreditAmount = 0d;
		}
		if (!(lenderWalletdebitAmount != null && lenderWalletdebitAmount > 0)) {
			lenderWalletdebitAmount = 0d;
		}
		Double lenderWalletAmount = lenderWalletCreditAmount - lenderWalletdebitAmount;
		if (lenderWalletAmount != null && lenderWalletAmount > 0) {
			walletAmount = (lenderWalletAmount - lenderWalletInprocessAmount);
		} else {
			walletAmount = 0.0;
		}
		return walletAmount;
	}

	public LoanResponseForDealsDto loanRequestForDeals(int userId, LoanRequestDto loanRequestDto) {
		User user = userRepo.findById(userId).get();
		LoanRequest loanRequest = new LoanRequest();
		loanRequest.setLoanRequestAmount(loanRequestDto.getLoanRequestAmount());
		loanRequest.setDuration(loanRequestDto.getDuration());
		loanRequest.setLoanPurpose(loanRequestDto.getLoanPurpose());
		loanRequest.setRateOfInterest(loanRequestDto.getRateOfInterest());
		loanRequest.setUserPrimaryType(user.getPrimaryType());
		loanRequest.setRepaymentMethod(RepaymentMethod.valueOf(loanRequestDto.getRepaymentMethod()));
		if (loanRequestDto.getDurationType() != null) {
			loanRequest
					.setDurationType(loanRequestDto.getDurationType().equalsIgnoreCase("months") ? DurationType.Months
							: DurationType.Days);

		}
		try {
			loanRequest.setExpectedDate(expectedDateFormat.parse(loanRequestDto.getExpectedDate()));
		} catch (ParseException e1) {

			e1.printStackTrace();
		}
		LoanRequest parentRequest = null;

		if (loanRequestDto.getParentRequestId() != null) {
			parentRequest = loanRequestRepo.findById(loanRequestDto.getParentRequestId()).get();
			if (user.getPrimaryType() == PrimaryType.BORROWER) {
				LoanRequest loanRequest2 = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId());
				if (loanRequest2 != null) {
					LoanOfferdAmount loanOfferdAmount = loanRequest2.getLoanOfferedAmount();
					if (loanOfferdAmount == null) {
						throw new OperationNotAllowedException("User don't have loanoffer",
								ErrorCodes.INVALID_OPERATION);
					} else if (loanOfferdAmount != null) {
						if (loanOfferdAmount.getLoanOfferdStatus() != LoanOfferdStatus.LOANOFFERACCEPTED) {
							throw new OperationNotAllowedException("You  did not accepet loanoffer",
									ErrorCodes.INVALID_OPERATION);

						}
						if (loanRequest2.getDurationBySir() > 0) {
							if (loanRequest2.getRateOfInterestToBorrower() != loanRequestDto.getRateOfInterest()) {
								throw new ActionNotAllowedException(
										"Please check rate of interest it not matching to admin rate of interest",
										ErrorCodes.ENTITY_ALREDY_EXISTS);

							}
							if (!loanRequest2.getLoanOfferedAmount().getDuration()
									.equals(loanRequestDto.getDuration())) {
								throw new ActionNotAllowedException(
										"Please check duration it is not matching to admin duration value",
										ErrorCodes.ENTITY_ALREDY_EXISTS);

							}

						}
					}
				}
			}

			Integer lenderUserId = user.getPrimaryType() == PrimaryType.LENDER ? userId : parentRequest.getUserId();
			Integer borrowerUserId = user.getPrimaryType() == PrimaryType.BORROWER ? userId : parentRequest.getUserId();

			loanRequest.setParentRequestId(loanRequestDto.getParentRequestId());
			loanRequest.setLoanStatus(LoanStatus.Conversation);
			Integer requestedUser = parentRequest.getUserId(); // You are responding to this user's request

			LenderBorrowerConversation lenderBorrowerConversation = new LenderBorrowerConversation();
			lenderBorrowerConversation.setLenderUserId(lenderUserId);
			lenderBorrowerConversation.setBorrowerUserId(borrowerUserId);
			LenderBorrowerConversation addedEntry = lenderBorrowerConversationRepo.save(lenderBorrowerConversation);
			logger.info("Added entry to LenderBorrowerConversation with id {}", addedEntry.getId());
			loanRequest.setUserId(userId);

			loanRequest.setLoanRequestId(new Long(System.currentTimeMillis()).toString()); // This is temporary
			loanRequest = loanRequestRepo.save(loanRequest);
			loanRequest.setLoanRequestId(LOAN_REQUEST_IDFORMAT.replace("{type}", user.getPrimaryType().getShortCode())
					.replace("{ID}", loanRequest.getId().toString()));
			loanRequest = loanRequestRepo.save(loanRequest);
		}
		LoanResponseForDealsDto loanResponseForDealsDto = new LoanResponseForDealsDto();
		loanResponseForDealsDto.setLoanRequestId(loanRequest.getId());
		return loanResponseForDealsDto;
	}

	public String actOnRequestForDeals(int userId, Integer loanRequestId, String loanStatus, Integer dealId,
			DealLevelRequestDto dealLevelRequestDto) throws PdfGeenrationException, IOException {
		String loanIdFormanuvalEsign = null;
		OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
				.toCheckUserAlreadyInvoledInDeal(userId, dealId);

		User user = userRepo.findById(userId).get();

		LoanRequest loanRequest = loanRequestRepo.findById(loanRequestId).get();
		if (loanRequest.getLoanStatus() == LoanStatus.Accepted || loanRequest.getLoanStatus() == LoanStatus.Rejected) {
			throw new ActionNotAllowedException("This Request already " + loanRequest.getLoanStatus(),
					ErrorCodes.ACTION_ALREDY_DONE);
		}
		loanRequest.setLoanStatus(LoanStatus.Accepted);
		loanRequest = loanRequestRepo.save(loanRequest);
		Integer parentRequestId = loanRequest.getParentRequestId();

		LoanRequest parentRequest = loanRequestRepo.findById(parentRequestId).get();

		User lenderUser = null;
		User borrowerUser = null;
		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			borrowerUser = user;
			lenderUser = userRepo.findById(loanRequest.getUserId()).get();
		} else {
			lenderUser = user;
			borrowerUser = userRepo.findById(loanRequest.getUserId()).get();
		}
		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo.findById(dealId)
				.get();
		OxyLoan finalLoan = null;
		if (loanStatus.equals(LoanStatus.Accepted.toString())) {
			User requestedUser = (user.getPrimaryType() == PrimaryType.BORROWER ? borrowerUser : lenderUser);
			LoanRequestDto loanRequestDto = new LoanRequestDto();
			loanRequestDto.setLoanRequestAmount(loanRequest.getLoanRequestAmount());
			isLimitExceeded(requestedUser.getId(), user, loanRequestDto);
			finalLoan = new OxyLoan();
			finalLoan.setBorrowerUserId(borrowerUser.getId());
			LoanRequest borrowerApplication = loanRequestRepo
					.findByUserIdAndParentRequestIdIsNull(borrowerUser.getId());
			LoanRequest lenderApplication = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(lenderUser.getId());

			loanRequest.setApplication_id(borrowerApplication.getLoanRequestId());
			finalLoan.setApplication_id(borrowerApplication.getLoanRequestId());

			finalLoan.setBorrowerParentRequestId(borrowerApplication.getId());
			finalLoan.setLenderParentRequestId(lenderApplication.getId());
			finalLoan.setLenderUserId(lenderUser.getId());
			finalLoan.setLoanAcceptedDate(new Date());
			finalLoan.setLoanRequestId(parentRequestId);
			finalLoan.setLoanRespondId(loanRequest.getId());
			finalLoan.setDisbursmentAmount(loanRequest.getLoanRequestAmount());
			finalLoan.setLoanId(new Long(System.currentTimeMillis()).toString());
			finalLoan.setDealId(dealId);
			finalLoan.setLenderReturnTypeForDeal(oxyLendersAcceptedDeals.getLenderReturnsType().toString());

			if (oxyBorrowersDealsInformation != null) {
				finalLoan.setDealType(oxyBorrowersDealsInformation.getDealType().toString());
			}
			String loanId = finalLoan.getLoanId();
			LoanRequest loanRequestObject = loanRequestRepo.findByLoanId(loanId);
			if (loanRequestObject != null) {

				loanRequestObject.setRepaymentMethod(RepaymentMethod.I);

				loanRequestRepo.save(loanRequestObject);
			}
			if (borrowerApplication.getDurationBySir() > 0) {

				finalLoan.setRepaymentMethod(RepaymentMethod.I);

				finalLoan.setRateOfInterest(borrowerApplication.getRateOfInterestToBorrower());
			} else {

				finalLoan.setRepaymentMethod(loanRequest.getRepaymentMethod());
				finalLoan.setRateOfInterest(loanRequest.getRateOfInterest());
			}
			finalLoan.setDuration(loanRequest.getDuration());
			finalLoan.setDurationType(loanRequest.getDurationType());
			finalLoan = oxyLoanRepo.save(finalLoan);
			finalLoan.setLoanId(LOAN_ID_FORMAT.replace("{ID}", finalLoan.getId().toString()));
			finalLoan = oxyLoanRepo.save(finalLoan);
			loanRequest.setLoanId(finalLoan.getLoanId());
			loanIdFormanuvalEsign = finalLoan.getLoanId();

			loanRequestRepo.save(loanRequest);

			TemplateContext templateContext = new TemplateContext();

			PersonalDetails borrwoerPersonalDetails = borrowerUser.getPersonalDetails();
			templateContext.put("borrowerFullName",
					borrwoerPersonalDetails.getFirstName() + " " + borrwoerPersonalDetails.getLastName());
			templateContext.put("borrowerFatherName", borrwoerPersonalDetails.getFatherName());

			String borrowerAddress = borrowerUser.getPersonalDetails().getAddress();
			Pattern pt = Pattern.compile("[^a-zA-Z0-9]");
			Matcher match = pt.matcher(borrowerAddress);
			while (match.find()) {
				String s = match.group();
				borrowerAddress = borrowerAddress.replaceAll("\\" + s, " ");
			}
			templateContext.put("borrowerAddress", borrowerAddress);
			templateContext.put("borrowerPan", borrwoerPersonalDetails.getPanNumber());

			PersonalDetails lenderPersonalDetails = lenderUser.getPersonalDetails();
			templateContext.put("lenderFullName",
					lenderPersonalDetails.getFirstName() + " " + lenderPersonalDetails.getLastName());
			templateContext.put("lenderFatherName", lenderPersonalDetails.getFatherName());
			String lenderAddress = lenderPersonalDetails.getAddress();
			Pattern pt1 = Pattern.compile("[^a-zA-Z0-9]");
			Matcher match1 = pt1.matcher(lenderAddress);
			while (match1.find()) {
				String s = match1.group();
				lenderAddress = lenderAddress.replaceAll("\\" + s, " ");
			}

			templateContext.put("lenderAddress", lenderAddress);
			templateContext.put("lenderPan", lenderPersonalDetails.getPanNumber());

			if (lenderUser.getBankDetails() != null) {
				templateContext.put("lenderBankName", lenderUser.getBankDetails().getBankName());
				templateContext.put("lenderNameAccordingToBank", lenderUser.getBankDetails().getUserName());
				templateContext.put("lenderAccountNumber", lenderUser.getBankDetails().getAccountNumber());
			}

			templateContext.put("dealAchievedDate",
					expectedDateFormat.format(oxyBorrowersDealsInformation.getDealAcheivedDate()));
			templateContext.put("dealAchievedDateWithTimeStamp", oxyBorrowersDealsInformation.getDealAcheivedDate());
			templateContext.put("loanAmount", BigDecimal.valueOf(finalLoan.getDisbursmentAmount()).toBigInteger());
			templateContext.put("dealSubtype", oxyBorrowersDealsInformation.getDealSubType());
			templateContext.put("roi", oxyLendersAcceptedDeals.getRateofinterest());
			templateContext.put("duration", oxyBorrowersDealsInformation.getDuration());
			templateContext.put("loanActiveDate",
					expectedDateFormat.format(oxyBorrowersDealsInformation.getLoanActiveDate()));

			int borrowerId = finalLoan.getBorrowerUserId();
			LoanRequest loanRequestDetails = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(borrowerId);

			if (loanRequestDetails.getDurationBySir() > 0) {
				if (loanRequestDetails.getDurationType().equals(DurationType.Months)) {
					templateContext.put("duration", loanRequestDetails.getDurationBySir());
					templateContext.put("rateOfInterestOfLender",
							BigDecimal.valueOf(oxyLendersAcceptedDeals.getRateofinterest()).toBigInteger());
					templateContext.put("rateOfInterestOfBorrower",
							BigDecimal.valueOf(loanRequestDetails.getRateOfInterestToBorrower()).toBigInteger());
				}
			}

			templateContext.put("loanAmount", BigDecimal.valueOf(finalLoan.getDisbursmentAmount()).toBigInteger());

			DateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

			formatter.setTimeZone(TimeZone.getTimeZone("IST")); // here set timezone

			templateContext.put("agreementDate", formatter.format(new Date()));

			Calendar calendar = Calendar.getInstance();
			String heading = "";

			double emiAmount = 0.0;
			int count = 0;
			double totalPrincipalAmount = 0.0;
			String emiLastDate = null;

			templateContext.put("heading", heading);
			templateContext.put("totalPrincipalAmount", totalPrincipalAmount);
			templateContext.put("emiLastDate", emiLastDate);
			templateContext.put("date", expectedDateFormat.format(finalLoan.getLoanAcceptedDate()));
			templateContext.put("loanId", loanRequest.getLoanId());

			double principalAmountPerMonth = finalLoan.getDisbursmentAmount() / loanRequestDetails.getDurationBySir();
			double interestAmountPerMonth = (finalLoan.getDisbursmentAmount()
					* loanRequestDetails.getRateOfInterestToBorrower()) / 100;
			double emiAmountForBorrower = principalAmountPerMonth + interestAmountPerMonth;

			templateContext.put("emiAmountForBorrower", BigDecimal.valueOf(emiAmountForBorrower).toBigInteger());

			String lenderReturnType = oxyLendersAcceptedDeals.getLenderReturnsType().toString();
			String accountDetails = null;
			String feeDetails = null;
			for (int i = 0; i <= 3; i++) {
				if (i == 0) {
					User lenderDetails = userRepo.findById(finalLoan.getLenderUserId()).get();
					if (lenderDetails != null) {
						if (lenderDetails.getBankDetails() != null) {
							String name = lenderPersonalDetails.getFirstName() + " "
									+ lenderPersonalDetails.getLastName();
							accountDetails = accountDetails + accountDetailsForAgreement(name,
									lenderDetails.getBankDetails().getBankName(), null, null, null);
						}
					}
					feeDetails = feeDetails + feeDetailsForAgreement("Registration fee", "Rs 0", "Rs 0");

				}
				if (i == 1) {
					User borrowerDetails = userRepo.findById(finalLoan.getBorrowerUserId()).get();
					if (borrowerDetails != null) {
						if (borrowerDetails.getBankDetails() != null) {
							String name = borrwoerPersonalDetails.getFirstName() + " "
									+ borrwoerPersonalDetails.getLastName();
							accountDetails = accountDetails
									+ accountDetailsForAgreement(name, borrowerDetails.getBankDetails().getBankName(),
											borrowerDetails.getBankDetails().getAccountNumber(),
											borrowerDetails.getBankDetails().getBranchName(),
											borrowerDetails.getBankDetails().getIfscCode());
						}
					}

					String fee = "Rs " + BigDecimal.valueOf(borrowerApplication.getLoanOfferedAmount().getBorrowerFee())
							.toBigInteger();

					feeDetails = feeDetails
							+ feeDetailsForAgreement("Service fee (Excl borrower insurance fee)", fee, "Rs 0");

				}
				if (i == 2) {
					String lenderBankName = "SRS Fintech Private Limited – Lenders Funding\r\n" + "Escrow account\r\n"
							+ "";
					accountDetails = accountDetails
							+ accountDetailsForAgreement(lenderBankName, "ICICI", null, null, "ICIC0000106");
					feeDetails = feeDetails + feeDetailsForAgreement("Delayed payment charges",
							"Rs. 500/- + GST for delay in each EMI", "null");

				}
				if (i == 3) {
					String borrowerBankName = "SRS Fintech Private Limited – Borrower’s repayment escrow account";
					accountDetails = accountDetails
							+ accountDetailsForAgreement(borrowerBankName, null, null, null, null);
					feeDetails = feeDetails + feeDetailsForAgreement("Installment Collection and EMI Processing.",
							"null", "of each EMI till loan closure.");

				}

			}
			templateContext.put("accountDetails", accountDetails);
			templateContext.put("feeDetails", feeDetails);

			logger.info("Borrower agreement start");
			String outputFileName = finalLoan.getBorrowerUserId() + finalLoan.getLoanId() + ".pdf";
			String generatedPdf = "";
			if (loanRequestDetails != null && loanRequestDetails.getDurationType().equals(DurationType.Months)) {
				generatedPdf = pdfEngine.generatePdf("agreement/agreementTemplate.xml", templateContext,
						outputFileName);
			}
			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(generatedPdf));
			fileRequest.setFileName(outputFileName);
			fileRequest.setFileType(FileType.Agreement);
			fileRequest.setFilePrifix(FileType.Agreement.name());
			try {
				FileResponse putFile = fileManagementService.putFile(fileRequest);

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			File file = new File(generatedPdf);
			boolean fileDeleted = file.delete();

			logger.info("Lender agreement start");
			String outputFileNameForLender = finalLoan.getLenderUserId() + finalLoan.getLoanId() + ".pdf";
			String generatedPdfForLender = "";
			if (loanRequestDetails != null && loanRequestDetails.getDurationType().equals(DurationType.Months)) {
				generatedPdfForLender = pdfEngine.generatePdf("agreement/lenderAgreementTemplate.xml", templateContext,
						outputFileNameForLender);
			}
			FileRequest fileRequestForLender = new FileRequest();
			fileRequestForLender.setInputStream(new FileInputStream(generatedPdfForLender));
			fileRequestForLender.setFileName(outputFileNameForLender);
			fileRequestForLender.setFileType(FileType.Agreement);
			fileRequestForLender.setFilePrifix(FileType.Agreement.name());
			try {
				FileResponse putFile = fileManagementService.putFile(fileRequestForLender);

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			File fileForLender = new File(generatedPdfForLender);
			boolean fileDeletedForLender = fileForLender.delete();

		}

		manualEsignProcessForDeal(loanIdFormanuvalEsign, dealId, dealLevelRequestDto);

		lenderBorrowerConversationRepo.deleteByLenderUserIdAndBorrowerUserId(lenderUser.getId(), borrowerUser.getId());
		String message = "success";
		return message;
	}

	public String manualEsignProcessForDeal(String loanId, int dealId, DealLevelRequestDto dealLevelRequestDto) {
		String message = null;
		OxyLoan oxyLoan = oxyLoanRepo.findByLoanId(loanId);
		logger.info("manualEsignProcess method start");
		if (oxyLoan == null) {
			throw new NoSuchElementException("No Loan present to esign against");
		}

		int borrowerParentRequestId = oxyLoan.getBorrowerParentRequestId();
		double disburmentAmount = oxyLoan.getDisbursmentAmount();
		int lenderUserId = oxyLoan.getLenderUserId();
		int borrowerUserId = oxyLoan.getBorrowerUserId();

		LoanRequest loanRequest = loanRequestRepo.findByLoanId(loanId);
		if (loanRequest == null) {
			throw new NoSuchElementException("No Loan present to esign against");
		}
		LoanRequest loanRequestDetails = loanRequestRepo.findById(borrowerParentRequestId).get();
		if (loanRequestDetails.getLoanStatus() == LoanStatus.Requested) {
			Double totalDisbursedAmount = loanRequestDetails.getDisbursmentAmount();
			if (loanRequestDetails.getLoanOfferedAmount() != null) {
				if (totalDisbursedAmount <= loanRequestDetails.getLoanOfferedAmount().getLoanOfferedAmount()) {
					loanRequestDetails.setDisbursmentAmount(totalDisbursedAmount);
					loanRequestRepo.save(loanRequestDetails);
				} else {
					throw new OperationNotAllowedException(
							"Total disbursed amount is greater than offered amount please check",
							ErrorCodes.LIMIT_REACHED);
				}
			} else {
				loanRequestDetails.setDisbursmentAmount(totalDisbursedAmount);
				loanRequestRepo.save(loanRequestDetails);
			}

		}

		if (oxyLoan.getLoanStatus() == LoanStatus.Agreed && oxyLoan.getBorrowerDisbursedDate() == null) {

			oxyLoan.setLoanStatus(LoanStatus.Active);
			oxyLoan.setLoanActiveDate(new Date());
			oxyLoan.setLenderEsignId(0000);
			oxyLoan.setBorrowerEsignId(0000);
			loanRequest.setAdminComments("APPROVED");
			OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
					.findById(dealId).get();
			if (oxyLoan.getDurationType().equals(DurationType.Months)) {
				generateEmiCardForDeal(oxyLoan, loanRequest, dealId, dealLevelRequestDto);
			}
			if (oxyBorrowersDealsInformation.getEnachStatus().equals(false)) {

				loanRequest.setEnachType("MANUALENACH");
				loanRequest.setIsECSActivated(true);
			} else {
				generateENACHMandateForZaggle(oxyLoan);
			}

			oxyLoan = oxyLoanRepo.save(oxyLoan);
			loanRequest = loanRequestRepo.save(loanRequest);

		}
		message = "success";
		return message;

	}

	public void generateENACHMandateForZaggle(OxyLoan loanDetails) {
		double maxAmount = 0;
		double totalAmount = 0;
		Date startDate = null;
		Date endDate = null;
		double finalAmount = 0;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		Set<Integer> loanIds = new HashSet<Integer>();
		String loanId = loanDetails.getLoanId();
		StringBuilder converting = new StringBuilder(loanId);
		String convertedLoanId = converting.delete(0, 2).toString();

		System.out.println(convertedLoanId);
		List<LoanEmiCard> listLoanEmicard = loanEmiCardRepo
				.findByLoanIdOrderByEmiNumber(Integer.parseInt(convertedLoanId));

		for (LoanEmiCard emis : listLoanEmicard) {

			if (emis.getEmiNumber() == 1) {
				maxAmount = emis.getEmiAmount();
			}
			if (emis.getEmiNumber() == 1) {
				startDate = emis.getEmiDueOn();
			}
			if (emis.getEmiNumber() == listLoanEmicard.size()) {
				endDate = emis.getEmiDueOn();
				finalAmount = emis.getEmiPrincipalAmount();
			}
		}
		totalAmount = loanDetails.getDisbursmentAmount();
		String txnId = "ECST" + dateUtil.getHHmmMMddyyyyDateFormat() + loanDetails.getBorrowerUserId()
				+ loanDetails.getId();// Need to change
		// the format

		OxyLoan oxyLoanDetails = oxyLoanRepo.findByLoanId(loanId);

		if (oxyLoanDetails.getDurationType().equals(DurationType.Months)) {
			Date endDate3 = null;

			List<EnachMandate> mandateList = new ArrayList<>();

			int size = 0;
			LoanRequest loanRequest = loanRequestRepo
					.findByUserIdAndParentRequestIdIsNull(loanDetails.getBorrowerUserId());

			if (loanRequest != null) {

				if (loanRequest.getRepaymentMethodForBorrower().equalsIgnoreCase(RepaymentMethod.PI.name())) {
					size = 1;
				} else if (loanRequest.getRepaymentMethodForBorrower().equalsIgnoreCase(RepaymentMethod.I.name())) {
					size = 2;
				}
			}

			for (int i = 1; i <= size; i++) {
				EnachMandate enachMandate = new EnachMandate();

				enachMandate.setOxyLoanId(oxyLoanDetails.getId());
				enachMandate.setOxyLoan(oxyLoanDetails);

				enachMandate.setAmountType(AmountType.valueOf(EnachMandate.AmountType.M.name()));
				enachMandate.setFrequency(Frequency.valueOf(EnachMandate.Frequency.MNTH.name()));

				enachMandate.setDebitStartDate(startDate);

				enachMandate.setDebitEndDate(endDate);
				enachMandate.setMandateTransactionId(txnId + i);
				enachMandate.setMaxAmount(maxAmount);
				if (i == 2) {
					enachMandate.setMaxAmount(finalAmount);
					enachMandate.setDebitStartDate(endDate);
					enachMandate.setDebitEndDate(endDate);

				}
				enachMandate.setMandateStatus(MandateStatus.INITIATED);

				logger.info(enachMandate.toString());

				mandateList.add(enachMandate);
			}

			logger.info(mandateList);

			enachMandateRepo.saveAll(mandateList);

			String mailSubjectForAdmin = "Enach"; // Mail Goes to admin
			String mailTempalteForAdmin = "enach-mail-to-admin.template";

			TemplateContext templateContext = new TemplateContext();
			templateContext.put("LoanId", oxyLoanDetails.getId());

			templateContext.put("AmountType", AmountType.valueOf(EnachMandate.AmountType.M.name()));
			templateContext.put("Frequeny", Frequency.valueOf(EnachMandate.Frequency.MNTH.name()));

			templateContext.put("EmiStartDate", expectedDateFormat.format(startDate));
			templateContext.put("EmiEndDate", expectedDateFormat.format(endDate));

			templateContext.put("TransactionId", txnId);
			templateContext.put("MaxAmount", Math.round(maxAmount));
			templateContext.put("MandateStatus", MandateStatus.INITIATED);
			Date date = new Date();
			String date1 = expectedDateFormat.format(date);
			templateContext.put("CurrentDate", date1);

			EmailRequest emailRequest = new EmailRequest(new String[] { this.supportEmail }, mailSubjectForAdmin,
					mailTempalteForAdmin, templateContext);
			EmailResponse emailResponse = emailService.sendEmail(emailRequest);
			if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
				throw new RuntimeException(emailResponse.getErrorMessage());
			}

		} else if (oxyLoanDetails.getDurationType().equals(DurationType.Days)) {
			Date endDate2 = null;

			List<EnachMandate> mandateList = new ArrayList<>();

			for (int i = 1; i <= 2; i++) {
				EnachMandate enachMandate1 = new EnachMandate();
				enachMandate1.setOxyLoanId(oxyLoanDetails.getId());
				enachMandate1.setOxyLoan(oxyLoanDetails);

				enachMandate1.setAmountType(AmountType.valueOf(EnachMandate.AmountType.M.name()));
				enachMandate1.setFrequency(Frequency.valueOf(EnachMandate.Frequency.DAIL.name()));

				enachMandate1.setMandateTransactionId(txnId + Integer.toString(i));
				enachMandate1.setDebitStartDate(startDate);
				enachMandate1.setDebitEndDate(endDate);

				enachMandate1.setMaxAmount(maxAmount);
				if (i == 2) {
					enachMandate1.setMaxAmount(finalAmount);
					enachMandate1.setDebitStartDate(endDate);
					enachMandate1.setDebitEndDate(endDate);

				}

				enachMandate1.setMandateStatus(MandateStatus.INITIATED);
				mandateList.add(enachMandate1);
			}
			logger.info("asdaa " + mandateList);
			enachMandateRepo.saveAll(mandateList);
		}

	}

	public double generateEmiCardForDeal(OxyLoan oxyLoan, LoanRequest loanRequest, int dealId,
			DealLevelRequestDto dealLevelRequestDto) {
		List<LoanEmiCard> loanEmiCards = new ArrayList<LoanEmiCard>();
		double emiPrincipalAmount = 0d;
		double emiInterstAmount = 0d;

		int userId = oxyLoan.getLenderUserId();
		OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
				.toCheckUserAlreadyInvoledInDeal(userId, dealId);
		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
				.findDealAlreadyGiven(dealId);
		String nextDate1 = dealLevelRequestDto.getDisbursedDate();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = formatter.parse(nextDate1);

		} catch (ParseException e) {
			e.printStackTrace();

		}
		int quartlyValue = 0;
		int halflyValue = 0;
		int yearlyValue = 0;
		int endofdealValue = 0;

		double interstAmountForLender = 0.0;
		double principalAmountForLender = 0.0;
		double emiAmountForLender = 0.0;
		for (int i = 1; i <= loanRequest.getDuration(); i++) {

			LoanEmiCard loanEmiCard = new LoanEmiCard();
			loanEmiCard.setLoanId(oxyLoan.getId());
			loanEmiCard.setEmiNumber(i);

			loanEmiCard.setEmiDueOn(date);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c1 = Calendar.getInstance();
			String dateParts[] = nextDate1.split("-");
			int year = Integer.parseInt(dateParts[0]);

			int month = Integer.parseInt(dateParts[1]);

			int date1 = Integer.parseInt(dateParts[2]);

			c1.set(year, month, date1);

			nextDate1 = sdf.format(c1.getTime());

			try {
				date = sdf.parse(nextDate1);
			} catch (ParseException e) {

				e.printStackTrace();

			}

			if (loanRequest.getDuration().intValue() == i && loanRequest.getRepaymentMethod() == RepaymentMethod.I) {
				loanEmiCard.setEmiPrincipalAmount(oxyLoan.getDisbursmentAmount());
			}
			int borrowerParentid = oxyLoan.getBorrowerParentRequestId();
			LoanRequest loanRequestDetails = loanRequestRepo.findById(borrowerParentid).get();
			if (loanRequestDetails != null) {

				if (loanRequestDetails.getRepaymentMethodForLender().equalsIgnoreCase("I")) {
					interstAmountForLender = (oxyLoan.getDisbursmentAmount()
							* (oxyLendersAcceptedDeals.getRateofinterest() / 12)) / 100d;
					if (i == loanRequest.getDuration()) {
						principalAmountForLender = oxyLoan.getDisbursmentAmount();

					}

					emiAmountForLender = interstAmountForLender + principalAmountForLender;

				}

				if (loanRequestDetails.getRepaymentMethodForBorrower().equalsIgnoreCase("PI")) {
					double interestAmount = oxyLoan.getDisbursmentAmount()
							* (loanRequestDetails.getRateOfInterestToBorrower() / 12) / 100;
					double principalAmount = oxyLoan.getDisbursmentAmount() / oxyLoan.getDuration();
					loanEmiCard.setEmiInterstAmount(Math.round(interestAmount));
					loanEmiCard.setEmiPrincipalAmount(Math.round(principalAmount));
					loanEmiCard.setEmiAmount(Math.round(interestAmount + principalAmount));

				} else {
					double interestAmountOfI = oxyLoan.getDisbursmentAmount()
							* (loanRequestDetails.getRateOfInterestToBorrower() / 12) / 100;
					double principalAmountOfI = 0.0;
					if (i == loanRequestDetails.getDurationBySir()) {
						principalAmountOfI = oxyLoan.getDisbursmentAmount();
					}
					loanEmiCard.setEmiInterstAmount(Math.round(interestAmountOfI));
					loanEmiCard.setEmiPrincipalAmount(Math.round(principalAmountOfI));
					loanEmiCard.setEmiAmount(Math.round(interestAmountOfI + principalAmountOfI));
				}

			}

			if (oxyLendersAcceptedDeals.getLenderReturnsType().equals(LenderReturnsType.MONTHLY)) {
				loanEmiCard.setLenderEmiInterestAmount(Math.round(interstAmountForLender));
				loanEmiCard.setLenderEmiAmount(Math.round(emiAmountForLender));
				loanEmiCard.setLenderEmiPrincipalAmount(Math.round(principalAmountForLender));
			} else {
				if (oxyLendersAcceptedDeals.getLenderReturnsType().equals(LenderReturnsType.QUARTELY)) {
					if (loanEmiCard.getEmiNumber() % 3 == 0) {
						loanEmiCard.setLenderEmiInterestAmount(interstAmountForLender * 3);
						loanEmiCard.setLenderEmiAmount(interstAmountForLender * 3);
						quartlyValue = quartlyValue + 3;
						if (loanEmiCard.getEmiNumber() == loanRequest.getDuration()) {
							loanEmiCard.setLenderEmiPrincipalAmount(oxyLoan.getDisbursmentAmount());
							loanEmiCard.setLenderEmiAmount(loanEmiCard.getLenderEmiPrincipalAmount()
									+ loanEmiCard.getLenderEmiInterestAmount());
						} else {
							loanEmiCard.setLenderEmiPrincipalAmount(0.0);
						}

					} else {
						loanEmiCard.setLenderEmiInterestAmount(0.0);
						loanEmiCard.setLenderEmiAmount(0.0);

						if (loanEmiCard.getEmiNumber() == loanRequest.getDuration()) {
							int pendingEmiInterest = loanRequest.getDuration() - quartlyValue;
							loanEmiCard.setLenderEmiPrincipalAmount(
									oxyLoan.getDisbursmentAmount() + (interstAmountForLender * pendingEmiInterest));
							loanEmiCard.setLenderEmiAmount(loanEmiCard.getLenderEmiPrincipalAmount()
									+ loanEmiCard.getLenderEmiInterestAmount());
						} else {
							loanEmiCard.setLenderEmiPrincipalAmount(0.0);
						}
					}
				}
				if (oxyLendersAcceptedDeals.getLenderReturnsType().equals(LenderReturnsType.HALFLY)) {
					if (loanEmiCard.getEmiNumber() % 6 == 0) {
						loanEmiCard.setLenderEmiInterestAmount(interstAmountForLender * 6);
						loanEmiCard.setLenderEmiAmount(interstAmountForLender * 6);
						halflyValue = halflyValue + 6;
						if (loanEmiCard.getEmiNumber() == loanRequest.getDuration()) {
							loanEmiCard.setLenderEmiPrincipalAmount(oxyLoan.getDisbursmentAmount());
							loanEmiCard.setLenderEmiAmount(loanEmiCard.getLenderEmiPrincipalAmount()
									+ loanEmiCard.getLenderEmiInterestAmount());
						} else {
							loanEmiCard.setLenderEmiPrincipalAmount(0.0);
						}

					} else {
						loanEmiCard.setLenderEmiInterestAmount(0.0);
						loanEmiCard.setLenderEmiAmount(0.0);
						if (loanEmiCard.getEmiNumber() == loanRequest.getDuration()) {
							int pendingEmiInterest = loanRequest.getDuration() - halflyValue;
							loanEmiCard.setLenderEmiPrincipalAmount(
									oxyLoan.getDisbursmentAmount() + (interstAmountForLender * pendingEmiInterest));
							loanEmiCard.setLenderEmiAmount(loanEmiCard.getLenderEmiPrincipalAmount()
									+ loanEmiCard.getLenderEmiInterestAmount());
						} else {
							loanEmiCard.setLenderEmiPrincipalAmount(0.0);
						}
					}
				}
				if (oxyLendersAcceptedDeals.getLenderReturnsType().equals(LenderReturnsType.YEARLY)) {
					if (loanEmiCard.getEmiNumber() % 12 == 0) {
						yearlyValue = yearlyValue + 12;
						loanEmiCard.setLenderEmiInterestAmount(interstAmountForLender * 12);
						loanEmiCard.setLenderEmiAmount(interstAmountForLender * 12);
						if (loanEmiCard.getEmiNumber() == loanRequest.getDuration()) {
							loanEmiCard.setLenderEmiPrincipalAmount(oxyLoan.getDisbursmentAmount());
							loanEmiCard.setLenderEmiAmount(loanEmiCard.getLenderEmiPrincipalAmount()
									+ loanEmiCard.getLenderEmiInterestAmount());
						} else {
							loanEmiCard.setLenderEmiPrincipalAmount(0.0);
						}

					} else {
						loanEmiCard.setLenderEmiInterestAmount(0.0);
						loanEmiCard.setLenderEmiAmount(0.0);
						if (loanEmiCard.getEmiNumber() == loanRequest.getDuration()) {
							int pendingEmiInterest = loanRequest.getDuration() - yearlyValue;
							loanEmiCard.setLenderEmiPrincipalAmount(
									oxyLoan.getDisbursmentAmount() + (interstAmountForLender * pendingEmiInterest));
							loanEmiCard.setLenderEmiAmount(loanEmiCard.getLenderEmiPrincipalAmount()
									+ loanEmiCard.getLenderEmiInterestAmount());
						} else {
							loanEmiCard.setLenderEmiPrincipalAmount(0.0);
						}
					}
				}
				if (oxyLendersAcceptedDeals.getLenderReturnsType().equals(LenderReturnsType.ENDOFTHEDEAL)) {
					if (loanEmiCard.getEmiNumber() % loanRequest.getDuration() == 0) {
						endofdealValue = endofdealValue + loanRequest.getDuration();
						loanEmiCard.setLenderEmiInterestAmount(interstAmountForLender * loanRequest.getDuration());
						loanEmiCard.setLenderEmiAmount(interstAmountForLender * loanRequest.getDuration());
						if (loanEmiCard.getEmiNumber() == loanRequest.getDuration()) {
							loanEmiCard.setLenderEmiPrincipalAmount(oxyLoan.getDisbursmentAmount());
							loanEmiCard.setLenderEmiAmount(loanEmiCard.getLenderEmiPrincipalAmount()
									+ loanEmiCard.getLenderEmiInterestAmount());
						} else {
							loanEmiCard.setLenderEmiPrincipalAmount(0.0);
						}

					} else {
						loanEmiCard.setLenderEmiInterestAmount(0.0);
						loanEmiCard.setLenderEmiAmount(0.0);
						if (loanEmiCard.getEmiNumber() == loanRequest.getDuration()) {
							int pendingEmiInterest = loanRequest.getDuration() - endofdealValue;
							loanEmiCard.setLenderEmiPrincipalAmount(
									oxyLoan.getDisbursmentAmount() + (interstAmountForLender * pendingEmiInterest));
							loanEmiCard.setLenderEmiAmount(loanEmiCard.getLenderEmiPrincipalAmount()
									+ loanEmiCard.getLenderEmiInterestAmount());
						} else {
							loanEmiCard.setLenderEmiPrincipalAmount(0.0);
						}
					}
				}
			}

			loanEmiCards.add(loanEmiCard);

			loanEmiCardRepo.saveAll(loanEmiCards);

		}
		return emiPrincipalAmount + emiInterstAmount;
	}

	public EmiRequestDto generatingEmiTableForLenderToDeal(double disbursedAmount, double rateOfInterestForLender,
			String repaymentType, int duration, String lenderReturnType, int dealId,
			DealLevelRequestDto dealLevelRequestDto) {
		EmiRequestDto lenderEmiRequestDto = new EmiRequestDto();
		// double firstPrincipalAmount = 0.0;
		String emiDataTable = "";
		double interstAmountForLender = 0.0;
		double diminishingAmount = 0.0;
		Integer quartlyValue = 0;
		Integer halflyValue = 0;
		Integer yearlyValue = 0;
		Integer endofdealValue = 0;
		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
				.findDealAlreadyGiven(dealId);
		String nextDate1 = dealLevelRequestDto.getDisbursedDate();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = formatter.parse(nextDate1);

		} catch (ParseException e) {
			e.printStackTrace();

		}
		if (repaymentType.equalsIgnoreCase("I")) {
			double principleAmountChange = 0.0;
			double totalInterestAmount = 0.0;
			double payableAmount = 0.0;
			double interestAmount = 0.0;
			int countForQuartlyValue = 0;
			int countForhalflyValue = 0;
			int countForYearlyValue = 0;
			int countForEndofdealValue = 0;
			if (oxyBorrowersDealsInformation.getDealType().equals(OxyBorrowersDealsInformation.DealType.NORMAL)) {
				interstAmountForLender = Math.round((disbursedAmount * (rateOfInterestForLender / 12)) / 100d);
			} else {
				if (repaymentType.equals(OxyLendersAcceptedDeals.LenderReturnsType.MONTHLY.toString())) {
					interstAmountForLender = Math.round((disbursedAmount * rateOfInterestForLender) / 100d);
				} else {
					interstAmountForLender = Math.round((disbursedAmount * (rateOfInterestForLender / 12)) / 100d);
				}
			}
			for (int i = 1; i <= duration; i++) {

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Calendar c1 = Calendar.getInstance();
				String dateParts[] = nextDate1.split("-");
				int year = Integer.parseInt(dateParts[0]);

				int month = Integer.parseInt(dateParts[1]);

				int date1 = Integer.parseInt(dateParts[2]);

				c1.set(year, month, date1);

				nextDate1 = sdf.format(c1.getTime());

				try {
					date = sdf.parse(nextDate1);
				} catch (ParseException e) {

					e.printStackTrace();

				}

				if (lenderReturnType == "MONTHLY") {

					if (oxyBorrowersDealsInformation.getDealType()
							.equals(OxyBorrowersDealsInformation.DealType.NORMAL)) {
						interestAmount = Math.round((disbursedAmount * (rateOfInterestForLender / 12)) / 100d);
					} else {
						if (repaymentType.equals(OxyLendersAcceptedDeals.LenderReturnsType.MONTHLY.toString())) {
							interestAmount = Math.round((disbursedAmount * rateOfInterestForLender) / 100d);
						} else {
							interestAmount = Math.round((disbursedAmount * (rateOfInterestForLender / 12)) / 100d);
						}
					}

					totalInterestAmount = totalInterestAmount + interestAmount;

					if (i == duration) {
						principleAmountChange = disbursedAmount;
					}
					emiDataTable = emiDataTable + generateFobLoanTableForLenderForDeal(i + "",
							principleAmountChange + "", interestAmount + "", date);
				} else {
					if (lenderReturnType == "QUARTELY") {
						if (i % 3 == 0) {
							interestAmount = interstAmountForLender * 3;
							totalInterestAmount = totalInterestAmount + interestAmount;
							quartlyValue = quartlyValue + 3;
							if (i == duration) {
								principleAmountChange = disbursedAmount;

							} else {
								principleAmountChange = 0.0;
							}

						} else {

							interestAmount = 0.0;
							totalInterestAmount = totalInterestAmount + interestAmount;
							if (i == duration) {

								int pendingEmiInterest = duration - quartlyValue;
								totalInterestAmount = totalInterestAmount
										+ (interstAmountForLender * pendingEmiInterest);
								principleAmountChange = (disbursedAmount
										+ (interstAmountForLender * pendingEmiInterest));

							} else {
								principleAmountChange = 0.0;

							}

						}
						if (interestAmount > 0.0 || principleAmountChange > 0.0) {
							countForQuartlyValue = countForQuartlyValue + 1;

							emiDataTable = emiDataTable + generateFobLoanTableForLenderForDeal(
									countForQuartlyValue + "", principleAmountChange + "", interestAmount + "", date);

						}
					}
					if (lenderReturnType == "HALFLY") {
						if (i % 6 == 0) {
							interestAmount = interstAmountForLender * 6;
							totalInterestAmount = totalInterestAmount + interestAmount;
							halflyValue = halflyValue + 6;
							if (i == duration) {
								principleAmountChange = disbursedAmount;

							} else {
								principleAmountChange = 0.0;
							}

						} else {

							interestAmount = 0.0;
							totalInterestAmount = totalInterestAmount + interestAmount;
							if (i == duration) {

								int pendingEmiInterest = duration - halflyValue;
								totalInterestAmount = totalInterestAmount
										+ (interstAmountForLender * pendingEmiInterest);
								principleAmountChange = (disbursedAmount
										+ (interstAmountForLender * pendingEmiInterest));

							} else {
								principleAmountChange = 0.0;

							}

						}
						if (interestAmount > 0.0 || principleAmountChange > 0.0) {
							countForhalflyValue = countForhalflyValue + 1;

							emiDataTable = emiDataTable + generateFobLoanTableForLenderForDeal(countForhalflyValue + "",
									principleAmountChange + "", interestAmount + "", date);

						}
					}

					if (lenderReturnType == "YEARLY") {
						if (i % 12 == 0) {
							interestAmount = interstAmountForLender * 12;
							totalInterestAmount = totalInterestAmount + interestAmount;
							yearlyValue = yearlyValue + 12;
							if (i == duration) {
								principleAmountChange = disbursedAmount;

							} else {
								principleAmountChange = 0.0;
							}

						} else {

							interestAmount = 0.0;
							totalInterestAmount = totalInterestAmount + interestAmount;
							if (i == duration) {

								int pendingEmiInterest = duration - yearlyValue;
								totalInterestAmount = totalInterestAmount
										+ (interstAmountForLender * pendingEmiInterest);
								principleAmountChange = (disbursedAmount
										+ (interstAmountForLender * pendingEmiInterest));

							} else {
								principleAmountChange = 0.0;

							}

						}
						if (interestAmount > 0.0 || principleAmountChange > 0.0) {
							countForYearlyValue = countForYearlyValue + 1;

							emiDataTable = emiDataTable + generateFobLoanTableForLenderForDeal(countForYearlyValue + "",
									principleAmountChange + "", interestAmount + "", date);

						}
					}
					if (lenderReturnType == "ENDOFTHEDEAL") {
						if (i % duration == 0) {
							interestAmount = interstAmountForLender * duration;
							totalInterestAmount = totalInterestAmount + interestAmount;
							endofdealValue = endofdealValue + duration;
							if (i == duration) {
								principleAmountChange = disbursedAmount;

							} else {
								principleAmountChange = 0.0;
							}

						} else {

							interestAmount = 0.0;
							totalInterestAmount = totalInterestAmount + interestAmount;
							if (i == duration) {

								int pendingEmiInterest = duration - endofdealValue;
								totalInterestAmount = totalInterestAmount
										+ (interstAmountForLender * pendingEmiInterest);
								principleAmountChange = (disbursedAmount
										+ (interstAmountForLender * pendingEmiInterest));

							} else {
								principleAmountChange = 0.0;

							}

						}
						if (interestAmount > 0.0 || principleAmountChange > 0.0) {
							countForEndofdealValue = countForEndofdealValue + 1;

							emiDataTable = emiDataTable + generateFobLoanTableForLenderForDeal(
									countForEndofdealValue + "", principleAmountChange + "", interestAmount + "", date);

						}
					}
				}
			}

			payableAmount = disbursedAmount + totalInterestAmount;
			diminishingAmount = (disbursedAmount + totalInterestAmount) / duration;

			lenderEmiRequestDto.setTotalInterestAmount(totalInterestAmount);
			lenderEmiRequestDto.setPayableAmount(payableAmount);
			lenderEmiRequestDto.setDiminishingAmount(diminishingAmount);
			lenderEmiRequestDto.setEmiTable(emiDataTable);
		}
		return lenderEmiRequestDto;

	}

	public String generateFobLoanTableForLenderForDeal(String emis, String principleAmountChange, String interest,
			Date date) {

		String emiReceivingDate = expectedDateFormat.format(date);

		String rowdata = "<fo:table-row>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">"

				+ emis + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">"

				+ emiReceivingDate + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">"

				+ principleAmountChange + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">" + interest + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " </fo:table-row>";

		return rowdata;
	}

	@Override
	public WhatappInformation getWhatappGroupsInformation(String type) {
		WhatappInformation whatappInformation = new WhatappInformation();
		List<ListOfWhatappGroupNames> listOfGroupNames = new ArrayList<ListOfWhatappGroupNames>();

		String chatType = "";
		try {

			if (type.equalsIgnoreCase("groups")) {
				chatType = "GROUP";
			} else {
				chatType = "BROADCAST";
			}
			Integer countValue = whatappGroupsInformationRepo.getListOfNamesCountBasedOnChatType(chatType);
			if (countValue != null) {
				List<WhatappGroupsInformation> listOfStrings = whatappGroupsInformationRepo
						.getListOfNamesBasedOnChatType(chatType);
				if (listOfStrings != null && !listOfStrings.isEmpty()) {
					for (WhatappGroupsInformation whatsappGroupInfo : listOfStrings) {
						String groupName = whatsappGroupInfo.getGroupName();
						String chatId = whatsappGroupInfo.getChatId();
						ListOfWhatappGroupNames listOfWhatappGroupNames = new ListOfWhatappGroupNames();
						listOfWhatappGroupNames.setGroupName(groupName);
						listOfWhatappGroupNames.setChatId(chatId);
						listOfGroupNames.add(listOfWhatappGroupNames);
					}
					whatappInformation.setTotalWhatappGroupsCount(countValue);
					whatappInformation.setListOfWhatappGroups(listOfGroupNames);
				}

			}

		} catch (Exception e) {
			logger.info("Exception in getWhatappGroupsInformation method");
		}
		return whatappInformation;

	}

	@Override
	public ScrowWalletResponse gettingLenderSrowWalletTransactions(int id, PaginationRequestDto pageRequestDto) {

		logger.info("gettingLenderSrowWalletTransactions method starts.............................");

		ScrowWalletResponse walletResponse = new ScrowWalletResponse();
		List<Object[]> objList = lenderOxyWalletNativeRepo.getLenderWalletDetails(id, pageRequestDto.getPageSize(),
				pageRequestDto.getPageNo());
		Integer totalCount = 0;
		Long totalCount1 = 0l;
		if (id == 0) {
			totalCount1 = lenderOxyWalletRepo.count();
			walletResponse.setTotalCount(totalCount1.intValue());
		} else {
			totalCount = lenderOxyWalletRepo.countByUserId(id);
			walletResponse.setTotalCount(totalCount);
		}
		List<ScrowLenderTransactionResponse> response = new ArrayList<ScrowLenderTransactionResponse>();
		try {
			if (objList != null && !objList.isEmpty()) {

				objList.forEach(e -> {

					try {
						ScrowLenderTransactionResponse oxyWalletresponse = new ScrowLenderTransactionResponse();
						oxyWalletresponse.setId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
						oxyWalletresponse.setUserId(Integer.parseInt(e[1] == null ? "0" : e[1].toString()));
						oxyWalletresponse.setScrowAccountNumber(e[2] == null ? "" : e[2].toString());
						oxyWalletresponse.setTransactionAmount(Integer.parseInt(e[3] == null ? "0" : e[3].toString()));
						oxyWalletresponse
								.setTransactionDate(expectedDateFormat.format(dateFormat.parse(e[4].toString())));
						oxyWalletresponse.setDocumentUploadedId(Integer.parseInt(e[5] == null ? "0" : e[5].toString()));
						oxyWalletresponse.setStatus(e[6] == null ? "" : e[6].toString());
						oxyWalletresponse.setFileName(e[7] == null ? "Image Not Uploaded" : e[7].toString());
						oxyWalletresponse.setFilePath(e[8] == null ? "" : e[8].toString());
						oxyWalletresponse.setDocumentType(e[9] == null ? "" : e[9].toString());
						oxyWalletresponse.setDocumentSubType(e[10] == null ? "" : e[10].toString());
						oxyWalletresponse.setFirstName(e[11] == null ? "" : e[11].toString());
						oxyWalletresponse.setLastName(e[12] == null ? "" : e[12].toString());
						oxyWalletresponse.setComments(e[13] == null ? "" : e[13].toString());

						response.add(oxyWalletresponse);

					} catch (Exception e1) {

						throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
					}
				});
			}
		} catch (Exception e) {

			throw new DataFormatException("Error While fetching the Data", ErrorCodes.DATA_FETCH_ERROR);
		}
		walletResponse.setResults(response);

		logger.info("gettingLenderSrowWalletTransactions method ends ...............");

		return walletResponse;

	}

	@Override
	public LenderReferenceDetails toEditTheLenderReferenceInformation(Integer refereeId, Integer referrerId,
			LenderReferenceRequestDto requestDto) {

		logger.info("toEditTheLenderReferenceInformation method starts..............................");

		LenderReferenceDetails refereeDetails = lenderReferenceDetailsRepo
				.findingRefereeAndReferrerInfoBasedOnId(refereeId);
		LenderReferenceDetails referenceDetails = lenderReferenceDetailsRepo.findingRefereeAndReferrerInfo(refereeId,
				referrerId);

		if (refereeDetails != null && referenceDetails == null) {

			throw new OperationNotAllowedException(
					"Referee Id: " + refereeId + " is already referred by another lender",
					ErrorCodes.ACTION_ALREDY_DONE);
		} else {

			if (referenceDetails != null) {

				if (requestDto.getRefereeId() != null && !requestDto.getRefereeId().equals("")) {
					referenceDetails.setRefereeId(requestDto.getRefereeId());
				}
				if (requestDto.getReferrerId() != null && !requestDto.getReferrerId().equals("")) {
					referenceDetails.setReferrerId(requestDto.getReferrerId());
				}
				if (requestDto.getEmail() != null && !requestDto.getEmail().isEmpty()) {
					referenceDetails.setRefereeEmail(requestDto.getEmail());
				}
				if (requestDto.getMobileNumber() != null && !requestDto.getMobileNumber().isEmpty()) {
					referenceDetails.setRefereeMobileNumber(requestDto.getMobileNumber());
				}

				if (requestDto.getName() != null && !requestDto.getName().isEmpty()) {
					referenceDetails.setRefereeName(requestDto.getName());
				}

				if (requestDto.getPrimaryType() != null && !requestDto.getPrimaryType().isEmpty()) {
					referenceDetails.setUserPrimaryType(requestDto.getPrimaryType().toUpperCase());
				}

				if (requestDto.getMailContent() != null && !requestDto.getMailContent().isEmpty()) {
					referenceDetails.setMailContent(requestDto.getMailContent());
				}
				if (requestDto.getMailSubject() != null && !requestDto.getMailSubject().isEmpty()) {
					referenceDetails.setMailContent(requestDto.getMailSubject());
				}

				if (requestDto.getReferredOn() != null && !requestDto.getReferredOn().equals("")) {
					referenceDetails.setReferredOn(requestDto.getReferredOn());
				}

				if (requestDto.getComments() != null && !requestDto.getComments().isEmpty()) {
					referenceDetails.setRadhaSirComments(requestDto.getComments());
				}

				if (requestDto.getAmount() != null && !requestDto.getAmount().equals("")) {
					referenceDetails.setAmount(requestDto.getAmount());
				}

				if (requestDto.getStatus() != null && !requestDto.getStatus().isEmpty()) {

					referenceDetails.setStatus(LenderReferenceDetails.Status.valueOf(requestDto.getStatus()));
				}

				lenderReferenceDetailsRepo.save(referenceDetails);

			} else if (referenceDetails == null) {

				referenceDetails = new LenderReferenceDetails();

				if (requestDto.getRefereeId() != null) {
					referenceDetails.setRefereeId(requestDto.getRefereeId());
				}
				if (requestDto.getReferrerId() != null) {
					referenceDetails.setReferrerId(requestDto.getReferrerId());
				}
				if (requestDto.getEmail() != null) {
					referenceDetails.setRefereeEmail(requestDto.getEmail());
				}
				if (requestDto.getMobileNumber() != null) {
					referenceDetails.setRefereeMobileNumber(requestDto.getMobileNumber());
				}

				if (requestDto.getName() != null) {
					referenceDetails.setRefereeName(requestDto.getName());
				}

				if (requestDto.getPrimaryType() != null) {
					referenceDetails.setUserPrimaryType(requestDto.getPrimaryType().toUpperCase());
				}

				if (requestDto.getMailContent() != null) {
					referenceDetails.setMailContent(requestDto.getMailContent());
				}
				if (requestDto.getMailSubject() != null) {
					referenceDetails.setMailContent(requestDto.getMailSubject());
				}

				if (requestDto.getReferredOn() != null) {
					referenceDetails.setReferredOn(requestDto.getReferredOn());
				}

				if (requestDto.getComments() != null) {
					referenceDetails.setRadhaSirComments(requestDto.getComments());
				}

				if (requestDto.getAmount() != null) {
					referenceDetails.setAmount(requestDto.getAmount());
				}

				if (requestDto.getStatus() != null) {

					referenceDetails.setStatus(LenderReferenceDetails.Status.valueOf(requestDto.getStatus()));
				}

				lenderReferenceDetailsRepo.save(referenceDetails);
			}
		}

		logger.info("toEditTheLenderReferenceInformation method ends..............................");

		return referenceDetails;

	}

	@Override
	public List<OxyLendersGroup> gettingListOfOxyLendersGroups() {

		List<OxyLendersGroup> listOfLenderGroups = new ArrayList<OxyLendersGroup>();

		List<OxyLendersGroup> lenderGroups = oxyLendersGroupRepo.findingListOfLenderGroups();

		for (OxyLendersGroup groups : lenderGroups) {
			OxyLendersGroup response = new OxyLendersGroup();
			response.setId(groups.getId());
			response.setLenderGroupName(groups.getLenderGroupName());

			listOfLenderGroups.add(response);

		}
		OxyLendersGroup response = new OxyLendersGroup();
		response.setId(0);
		response.setLenderGroupName("other");
		listOfLenderGroups.add(response);
		return listOfLenderGroups;
	}

	/*
	 * @Override public CommentsResponseDto updatingLenderToLendersGroup(Integer
	 * userId, OxyLendersGroups oxyLenderGroup) {
	 * 
	 * logger.
	 * info("updatingLenderToLendersGroup method starts.......................");
	 * 
	 * CommentsResponseDto response = new CommentsResponseDto();
	 * 
	 * User user = userRepo.findById(userId).get();
	 * 
	 * Double paidAmount = 0.0; if (oxyLenderGroup.getAmount() != null) { paidAmount
	 * = oxyLenderGroup.getAmount();
	 * 
	 * }
	 * 
	 * Integer lenderGroupId =
	 * oxyLendersGroupRepo.gettingOxyLenderGroupId(oxyLenderGroup.getGroupName());
	 * 
	 * if (user != null) {
	 * 
	 * if (user.getLenderGroupId() == 0 &&
	 * oxyLenderGroup.getStatus().equalsIgnoreCase("paid")) { LenderPayuDto
	 * lenderpayuDto = new LenderPayuDto(); lenderpayuDto.setUserId(user.getId());
	 * lenderpayuDto.setAmount(paidAmount);
	 * lenderpayuDto.setPaymentDate(oxyLenderGroup.getPaymentDate());
	 * lenderpayuDto.setDealId(1);
	 * 
	 * lenderPayuDetailsInsertion(lenderpayuDto);
	 * 
	 * }
	 * 
	 * user.setLenderGroupId(lenderGroupId);
	 * 
	 * userRepo.save(user);
	 * 
	 * List<OxyLendersAcceptedDeals> oxyLendersAcceptedDeals =
	 * oxyLendersAcceptedDealsRepo .findingListOfLendersAcceptedDeals(user.getId());
	 * if (oxyLendersAcceptedDeals != null && !oxyLendersAcceptedDeals.isEmpty()) {
	 * for (OxyLendersAcceptedDeals obj : oxyLendersAcceptedDeals) {
	 * 
	 * obj.setGroupId(user.getLenderGroupId());
	 * obj.setFeeStatus(OxyLendersAcceptedDeals.FeeStatus.COMPLETED);
	 * obj.setProcessingFee((Double) 0.0);
	 * 
	 * oxyLendersAcceptedDealsRepo.save(obj); } }
	 * 
	 * response.setComments("Updated"); } else if (user == null) { throw new
	 * OperationNotAllowedException("User Not Found With Given Id",
	 * ErrorCodes.USER_NOT_FOUND); }
	 * 
	 * logger.info("updatingLenderToLendersGroup method ends......................."
	 * );
	 * 
	 * return response;
	 * 
	 * }
	 * 
	 * private StatusResponseDto lenderPayuDetailsInsertion(LenderPayuDto
	 * lenderPayuDto) {
	 * 
	 * StatusResponseDto response = new StatusResponseDto();
	 * 
	 * LenderPayuDetails payuDetails = lenderPayuDetailsRepo
	 * .findByLenderUserIdAndDealIdAndStatus(lenderPayuDto.getUserId(),
	 * lenderPayuDto.getDealId()); if (payuDetails != null) {
	 * 
	 * return response;
	 * 
	 * } else { payuDetails = new LenderPayuDetails(); }
	 * 
	 * Date date; Timestamp ts = null; try { date = new
	 * SimpleDateFormat("dd/MM/yyyy").parse(lenderPayuDto.getPaymentDate()); ts =
	 * new Timestamp(date.getTime()); } catch (ParseException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); }
	 * 
	 * User user = userRepo.findByIdNum(lenderPayuDto.getUserId());
	 * 
	 * if (user != null) { String txnId = "PUT" +
	 * dateUtil.getHHmmMMddyyyyDateFormat() + user.getId();// Need to change the
	 * format
	 * 
	 * PersonalDetails personalDetails = user.getPersonalDetails(); if
	 * (personalDetails != null) {
	 * 
	 * payuDetails.setLenderUserId(user.getId());
	 * payuDetails.setDealId(lenderPayuDto.getDealId());
	 * payuDetails.setLenderName(personalDetails.getFirstName() + " " +
	 * personalDetails.getLastName()); payuDetails.setPaymentDate(ts);
	 * payuDetails.setPayuTransactionNumber(txnId.substring(3));
	 * payuDetails.setStatus(PayuStatus.COMPLETED);
	 * payuDetails.setTransactionNumber(txnId);
	 * payuDetails.setAmount(lenderPayuDto.getAmount());
	 * 
	 * lenderPayuDetailsRepo.save(payuDetails);
	 * response.setStatus("UPDATED SUCCESSFULLY");
	 * 
	 * }
	 * 
	 * }
	 * 
	 * return response; }
	 */
	@Override
	public LenderMappedToGroupIdResponseDto gettingListOfLendersMappedToGroupName(OxyLendersGroups groupName) {

		logger.info("gettingListOfLendersMappedToGroupName method starts.................................");

		int pageNo = groupName.getPageNo();
		int pageSize = groupName.getPageSize();

		pageNo = (pageSize * (pageNo - 1));

		List<LenderDetailsByGroupId> listOfLenders = new ArrayList<LenderDetailsByGroupId>();

		List<Object[]> listOfLendersMappedToGroup = oxyLoanRepo
				.findingListOfLendersMappedToGroup(groupName.getGroupName(), pageNo, pageSize);

		for (Object[] obj : listOfLendersMappedToGroup) {
			LenderDetailsByGroupId lenderDetails = new LenderDetailsByGroupId();

			lenderDetails.setLenderId((Integer) obj[0]);
			lenderDetails.setEmail(obj[1].toString() == null ? "" : obj[1].toString());
			lenderDetails.setMobileNumber(obj[2].toString() == null ? "" : obj[2].toString());
			lenderDetails.setName(obj[3].toString() == null ? "" : obj[3].toString());
			lenderDetails.setGroupId((Integer) obj[4] == null ? 0 : (Integer) obj[4]);
			lenderDetails.setGroupName(obj[5].toString() == null ? "" : obj[5].toString());

			listOfLenders.add(lenderDetails);

		}

		LenderMappedToGroupIdResponseDto response = new LenderMappedToGroupIdResponseDto();

		response.setListOfLendersDetailsByGroupId(listOfLenders);
		response.setCount(listOfLendersMappedToGroup.size());

		logger.info("gettingListOfLendersMappedToGroupName method ends.................................");

		return response;

	}

	@Override
	public LoanEmiCardResponseDto loanPreCloseByPlatForm(int loanId) {
		int currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN) {
			throw new OperationNotAllowedException("As a " + user.getPrimaryType() + " you are not authorized",
					ErrorCodes.PERMISSION_DENIED);
		}
		final OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get(); // To make sure loan exists
		List<LoanEmiCard> loanEmiCards = loanEmiCardRepo.findByLoanIdOrderByEmiNumber(loanId);
		if (CollectionUtils.isEmpty(loanEmiCards)) {
			throw new NoSuchElementException("No EMI Cards present with loan " + loanId);
		}
		if (oxyLoan.getLoanStatus() == LoanStatus.Closed || oxyLoan.getLoanStatus() == LoanStatus.CLOSEDBYPLATFORM) {
			throw new ActionNotAllowedException("This Loan + " + loanId + " is already Closed.",
					ErrorCodes.ACTION_ALREDY_DONE);
		}

		LoanEmiCard firstNonPaidEmiCard = loanEmiCardRepo.findTop1ByLoanIdAndAndEmiPaidOnNullOrderByEmiNumber(loanId);

		loanEmiCards.stream().forEach(emiCard -> {
			if (emiCard.getEmiNumber().intValue() < firstNonPaidEmiCard.getEmiNumber().intValue()) {
				return;
			}
			emiCard.setEmiPaidOn(new Date());
			emiCard.setComments("Pre Closure");
			emiCard.setStatus(com.oxyloans.entity.loan.LoanEmiCard.Status.COMPLETED);
			oxyLoan.setPrincipalRepaid(oxyLoan.getPrincipalRepaid() + emiCard.getEmiPrincipalAmount());
			if (emiCard.getEmiNumber().intValue() > firstNonPaidEmiCard.getEmiNumber().intValue()) {
				emiCard.setEmiInterstAmount(0d);
			}
			oxyLoan.setLoanInterestPaid(oxyLoan.getLoanInterestPaid() + emiCard.getEmiInterstAmount());
		});
		loanEmiCardRepo.saveAll(loanEmiCards);

		oxyLoan.setLoanStatus(LoanStatus.CLOSEDBYPLATFORM);
		oxyLoanRepo.save(oxyLoan);

		LoanRequest loanRequest = loanRequestRepo.findById(oxyLoan.getLoanRespondId()).get();
		loanRequest.setLoanStatus(LoanStatus.CLOSEDBYPLATFORM);
		loanRequestRepo.save(loanRequest);
		LoanRequest lenderLoanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(oxyLoan.getLenderUserId());
		lenderLoanRequest.setDisbursmentAmount(
				lenderLoanRequest.getDisbursmentAmount().doubleValue() - oxyLoan.getDisbursmentAmount());
		LoanRequest borrowerLoanRequest = loanRequestRepo
				.findByUserIdAndParentRequestIdIsNull(oxyLoan.getBorrowerUserId());
		if (borrowerLoanRequest != null) {
			borrowerLoanRequest.setDisbursmentAmount(
					borrowerLoanRequest.getDisbursmentAmount().doubleValue() - oxyLoan.getDisbursmentAmount());
			borrowerLoanRequest = loanRequestRepo.save(borrowerLoanRequest);
		}
		lenderLoanRequest = loanRequestRepo.save(lenderLoanRequest);

		LoanEmiCardResponseDto response = new LoanEmiCardResponseDto();
		response.setLoanId(loanId);
		return response;
	}

	@Override
	public List<PaymentUploadHistoryResponseDto> readingWhatsAppPaymentScreenshots(WhatsappMessagesDto request)
			throws IOException, ParseException {

		logger.info("readingWhatsAppPaymentScreenshots method starts");

		List<PaymentUploadHistoryResponseDto> list = new ArrayList<>();

		Map<String, String> failedMap = new HashMap<String, String>();

		HashMap<String, Double> usersEmptyList = new HashMap<String, Double>();

		WebTarget webTarget = client.target(whatsAppMessagesApi);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE);

		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

		map.add("Content-Type", "application/json");

		invocationBuilder.headers(map);

		WtappGroupRequestDto input = new WtappGroupRequestDto();

		input.setChatId(paymentScreenshotsWhatsAppGroup);

		input.setHistory(true);

		Response response = invocationBuilder.post(Entity.json(input));

		String groupResponse = response.readEntity(String.class);

		Gson gson = new Gson();

		WhatsappMessagesDto messageDto = gson.fromJson(groupResponse, WhatsappMessagesDto.class);

		Messages[] indvidualMsgList = messageDto.getMessages();

		int count = 0;

		for (Messages invidualMsg : indvidualMsgList) {

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

			Long unixTimestamp = Long.parseLong(invidualMsg.getTime());

			Date date = new Date(unixTimestamp * 1000L);

			String messageDate = formatter.format(date);

			// Date d = new Date();

			// String currentDate = formatter.format(d);

			Date fromDate = formatter.parse(request.getFromDate());
			Date toDate = formatter.parse(request.getToDate());

			logger.info(fromDate);
			logger.info(toDate);
			logger.info(date);

			logger.info(fromDate.compareTo(date) * date.compareTo(toDate) >= 0);
			logger.info(fromDate.getTime() <= date.getTime());
			logger.info(date.getTime() <= toDate.getTime());
			logger.info(fromDate.getTime() <= date.getTime() && date.getTime() <= toDate.getTime());

			if (fromDate.getTime() <= date.getTime() && date.getTime() <= toDate.getTime()) {

				if (invidualMsg.getType().equalsIgnoreCase("image")) {

					String paymentDetails = "";

					if (invidualMsg.getCaption() != null) { // if message body is in caption

						paymentDetails = invidualMsg.getCaption();

						for (Messages individualMessages1 : indvidualMsgList) {

							Long unixTimestamp1 = Long.parseLong(individualMessages1.getTime());

							Date individualMessages1Date = new Date(unixTimestamp1 * 1000L);

							String stringdate2 = formatter.format(individualMessages1Date);

							if (fromDate.getTime() <= date.getTime() && date.getTime() <= toDate.getTime()) {
								if (individualMessages1.getQuotedMsgId() != null) {

									if (invidualMsg.getId().equals(individualMessages1.getQuotedMsgId())) {

										paymentDetails = individualMessages1.getBody();

										break;
									}
								}
							} else {
								break;
							}

						}

					} else { // if message body is given as reply

						String msgId = invidualMsg.getId();

						for (Messages seperateMsgs : indvidualMsgList) {

							Long unixTimestamp2 = Long.parseLong(seperateMsgs.getTime());

							Date individualMessages1Date = new Date(unixTimestamp2 * 1000L);

							String stringdate1 = formatter.format(individualMessages1Date);

							if (fromDate.getTime() <= date.getTime() && date.getTime() <= toDate.getTime()) {

								if (seperateMsgs.getQuotedMsgId() != null) {

									if (seperateMsgs.getQuotedMsgId().equals(msgId)) {

										System.out.println(seperateMsgs.toString());

										paymentDetails = seperateMsgs.getBody();

										break;
									}
								}

							} else {

								break;

							}
						}

					}

					logger.info("message body:" + paymentDetails);

					if (paymentDetails != null && !paymentDetails.equals("")) {
						// paymentDetails = paymentDetails.replace("\n", "").replace("\r", "");

						if (paymentDetails.length() > 19) {

							try {

								if (paymentDetails.subSequence(0, 19).equals("Borrower unique id:")) {

									String[] split = paymentDetails.split("\\r?\\n");

									HashMap<String, String> hashMap = new HashMap<>();

									for (String details : split) {

										String[] detailsSplit = details.split(":");

										hashMap.put(detailsSplit[0], detailsSplit[1]);
									}

									String uniquNumber = hashMap.get("Borrower unique id");
									String paidDate1 = hashMap.get("Paid date");
									String paymentType = hashMap.get("Payment type");

									String messageNumber = invidualMsg.getMessageNumber();
									String updatedName = invidualMsg.getSenderName();
									String amountAmount = hashMap.get("Amount");

									logger.info("message number :" + messageNumber);

									uniquNumber = uniquNumber.replaceAll("[\\p{Punct}&&[^-]]", "").replaceAll(" ", "");

									paidDate1 = paidDate1.replaceAll("[^a-zA-Z0-9_-]", "").replaceAll(" ", "");

									paymentType = paymentType.replaceAll("[\\p{Punct}&&[^-]]", "").replaceAll(" ", "");

									amountAmount = amountAmount.replaceAll("[\\p{Punct}&&[^-]]", "").replaceAll(" ",
											"");

									if (!paymentType.equalsIgnoreCase("part")
											|| !paymentType.equalsIgnoreCase("full")) {
										paymentType = "FULL";
									}

									SimpleDateFormat format1 = new SimpleDateFormat();

									if (paidDate1.length() == 8) {

										format1 = new SimpleDateFormat("ddMMyyyy");
									}

									if (paidDate1.length() == 7) {

										format1 = new SimpleDateFormat("dMMyyyy");
									}

									SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/YYYY");

									Date dateMain = new Date();
									String paidDate = "";
									Double amount = 0d;
									int userId = 0;

									try {
										dateMain = format1.parse(paidDate1);
										paidDate = format2.format(dateMain);

										amount = Double.parseDouble(amountAmount);
										userId = Integer.parseInt(uniquNumber.substring(2));

									} catch (Exception e) {
										e.printStackTrace();
										failedMap.put(messageNumber, uniquNumber + "-" + amountAmount);

									}
									User user = userRepo.findByIdNum(userId);

									if (user == null) {
										user = userRepo.findByUniqueNumber(uniquNumber);
									}

									PaymentUploadHistory paymentUploadHistory = paymentUploadHistoryRepo
											.findByMessageNumber(invidualMsg.getMessageNumber());

									if (user != null && paymentUploadHistory == null) {
										PersonalDetails personalDetails = user.getPersonalDetails();

										KycFileRequest kyc = new KycFileRequest();

										// downloading image from whatsapp s3
										String imageUrl = invidualMsg.getBody();

										URL url = new URL(imageUrl);

										ReadableByteChannel readChannel = Channels.newChannel(url.openStream());

										String saveDirectory = LocalDate.now() + "paymentScreenshot.jpg";

										FileOutputStream fileOS = new FileOutputStream(saveDirectory);

										FileChannel writeChannel = fileOS.getChannel();

										writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);

										// saving image in s3
										File imageFile = new File(saveDirectory);

										InputStream fileStream = new FileInputStream(imageFile);

										kyc.setFileInputStream(fileStream);

										if (fileStream == null) {

											fileStream = decodeBase64Stream(kyc.getBase64EncodedStream());

										}
										FileRequest fileRequest = new FileRequest();

										UserDocumentStatus docStatus = new UserDocumentStatus();

										fileRequest.setInputStream(fileStream);

										fileRequest.setUserId(user.getId());

										fileRequest.setFileName(imageFile.getName());

										fileRequest.setFilePrifix(KycType.PAYMENTSCREENSHOT.name());

										FileResponse putFile = fileManagementService.putFile(fileRequest);

										UserDocumentStatus userDocumentStatus = new UserDocumentStatus();

										userDocumentStatus.setUserId(user.getId());

										userDocumentStatus.setDocumentType(DocumentType.ScrowScreenShot);

										userDocumentStatus.setDocumentSubType(KycType.PAYMENTSCREENSHOT.name());

										userDocumentStatus.setFilePath(putFile.getFilePath());

										userDocumentStatus.setFileName(imageFile.getName());

										userDocumentStatus.setStatus(
												com.oxyloans.entity.user.Document.status.UserDocumentStatus.Status.UPLOADED);

										docStatus = userDocumentStatusRepo.save(userDocumentStatus);

										PaymentUploadHistoryRequestDto paymentUploadHistoryRequestDto = new PaymentUploadHistoryRequestDto();

										paymentUploadHistoryRequestDto.setBorrowerUniqueNumber(user.getUniqueNumber());

										paymentUploadHistoryRequestDto.setBorrowerName(personalDetails.getFirstName());

										paymentUploadHistoryRequestDto.setAmount(amount);

										paymentUploadHistoryRequestDto.setDocumentUploadId(docStatus.getId());

										paymentUploadHistoryRequestDto.setPaidDate(paidDate);

										paymentUploadHistoryRequestDto.setPaymentType(paymentType);

										paymentUploadHistoryRequestDto.setMessageNumber(messageNumber);

										paymentUploadHistoryRequestDto.setUpdatedName("whatsapp");

										PaymentUploadHistoryResponseDto paymentUploadHistoryResponseDto = uploadPaymentHistoryThroughWhatsapp(
												paymentUploadHistoryRequestDto);

										if (paymentUploadHistoryResponseDto != null
												&& paymentUploadHistoryResponseDto.getBorrowerUniqueNumber() != null) {
											list.add(paymentUploadHistoryResponseDto);

										}
									} else {
										if (user == null) {
											usersEmptyList.put(uniquNumber, amount);
										}
									}

								}

							} catch (Exception e) {
								e.printStackTrace();

							}

						}
					}

				}

			} else {

				break;
			}

		}
		if (usersEmptyList != null && !usersEmptyList.isEmpty()) {
			StringBuilder message = new StringBuilder();
			message.append("Count of payment Screenshots uploaded :" + list.size()).append("\n\n")
					.append("Payment screenshots are not uploaded for following borrower id's:\n")
					.append(usersEmptyList.toString()).append("\n").append("Please check the borrower id's.");

			if (failedMap != null && !failedMap.isEmpty()) {

				message.append("\n\n").append("failed messages :" + failedMap.toString());

			}

			logger.info(message.toString());
			String status = whatappServiceRepo.sendingWhatsappMessages(paymentScreenshotsWhatsAppGroup,
					message.toString());
		}

		logger.info("readingWhatsAppPaymentScreenshots method ends");

		return list;

	}

	private InputStream decodeBase64Stream(String base64EncodedStream) {
		if (base64EncodedStream == null) {
			return null;
		}
		return new ByteArrayInputStream(Base64.getDecoder().decode(base64EncodedStream));
	}

	private PaymentUploadHistoryResponseDto uploadPaymentHistoryThroughWhatsapp(
			PaymentUploadHistoryRequestDto paymentUploadHistoryRequestDto) {
		logger.info("uploadPaymentHistorythrough whatsapp Method start");

		PaymentUploadHistoryResponseDto paymentUploadHistoryResponseDto = new PaymentUploadHistoryResponseDto();

		User user1 = userRepo.findByUniqueNumber(paymentUploadHistoryRequestDto.getBorrowerUniqueNumber());

		Integer borrowerId = user1.getId();
		Double amount = paymentUploadHistoryRequestDto.getAmount();
		String paidDate = paymentUploadHistoryRequestDto.getPaidDate();

		Object details = paymentUploadHistoryRepo.findByAmountAndPaidDateAndUserId(borrowerId, amount, paidDate);

		if (details != null) {
			return paymentUploadHistoryResponseDto;
		}

		if (paymentUploadHistoryRequestDto != null) {
			String uniqueNumber = paymentUploadHistoryRequestDto.getBorrowerUniqueNumber();
			if (uniqueNumber != null) {
				User user = userRepo.findByUniqueNumber(uniqueNumber);
				if (user != null) {
					PaymentUploadHistory paymentUploadHistory = new PaymentUploadHistory();
					paymentUploadHistory.setBorrowerUniqueNumber(uniqueNumber);
					if (paymentUploadHistoryRequestDto.getBorrowerName() != null) {
						paymentUploadHistory.setBorrowerName(paymentUploadHistoryRequestDto.getBorrowerName());
					}
					int id = paymentUploadHistoryRequestDto.getDocumentUploadId();
					UserDocumentStatus documentStatus = userDocumentStatusRepo.findByIdAndUserId(id, user.getId());
					if (documentStatus != null) {
						paymentUploadHistory.setDocumentUploadedId(id);
						if (paymentUploadHistoryRequestDto.getAmount() > 0) {
							PaymentUploadHistory paymentUpload = paymentUploadHistoryRepo.findByDocumentUploadedId(id);
							if (paymentUpload == null) {
								paymentUploadHistory.setAmount(paymentUploadHistoryRequestDto.getAmount());
								paymentUploadHistory.setUpdatedName(paymentUploadHistoryRequestDto.getUpdatedName());
								try {
									paymentUploadHistory.setPaidDate(
											expectedDateFormat.parse(paymentUploadHistoryRequestDto.getPaidDate()));
								} catch (ParseException e) {

									e.printStackTrace();
								}
								String paymentType = "";
								if (paymentUploadHistoryRequestDto.getPaymentType().equalsIgnoreCase("partpayment")
										|| paymentUploadHistoryRequestDto.getPaymentType().equalsIgnoreCase("part")) {
									paymentType = PaymentType.PARTPAYMENT.name();
								}

								if (paymentUploadHistoryRequestDto.getPaymentType().equalsIgnoreCase("fullpayment")
										|| paymentUploadHistoryRequestDto.getPaymentType().equalsIgnoreCase("full")) {
									paymentType = PaymentType.FULLPAYMENT.name();
								}

								paymentUploadHistory.setPaymentType(PaymentType.valueOf(paymentType));

								paymentUploadHistory.setUserId(user.getId());

								paymentUploadHistory
										.setWhatsAppMessageNo(paymentUploadHistoryRequestDto.getMessageNumber() != null
												? paymentUploadHistoryRequestDto.getMessageNumber()
												: "0");

								paymentUploadHistoryRepo.save(paymentUploadHistory);
							} else {
								return paymentUploadHistoryResponseDto;
							}
						} else {
							return paymentUploadHistoryResponseDto;
						}

					} else {
						return paymentUploadHistoryResponseDto;
					}
				} else {
					return paymentUploadHistoryResponseDto;
				}
			}
		}

		paymentUploadHistoryResponseDto
				.setBorrowerUniqueNumber(paymentUploadHistoryRequestDto.getBorrowerUniqueNumber());
		logger.info("uploadPaymentHistorythrough whatsapp Method ends");
		return paymentUploadHistoryResponseDto;

	}

	@Override
	public ReadingCommitmentAmountDto readingCommittmentAmount(BorrowersDealsRequestDto borrowersDealsRequestDto) {

		ReadingCommitmentAmountDto dto = new ReadingCommitmentAmountDto();
		List<Messages> msg = new ArrayList<>();
		int lastMessageNumber = 1;
		int count = 0;
		boolean commitmentFullFilled = false;
		Double amount = 0d;
		Map<String, Double> map = new LinkedHashMap<>();

		List<String> list = new ArrayList<String>();
		List<String> commitmentAmountList = new ArrayList<String>();
		List<CommitmentAmountExcelDto> commitmentAmountExcelDtoList = new ArrayList<>();

		List<String> chatIds = new ArrayList<String>();
		String commitmentAmountDetails = "";

		if (borrowersDealsRequestDto.getWhatappGroupNames() != null) {
			String[] groupNames = borrowersDealsRequestDto.getWhatappGroupNames().split(",");
			for (String chatId : groupNames) {
				String whatsappChatId = chatId;

				if (whatsappChatId != null) {
					chatIds.add(whatsappChatId);
				}
			}
		}

		OxyBorrowersDealsInformation dealsInformation = oxyBorrowersDealsInformationRepo
				.findByDealName(borrowersDealsRequestDto.getDealName());

		if (dealsInformation != null) {
			if (dealsInformation.getWhatappChatid() != null) {

				String chatId = dealsInformation.getWhatappChatid();
				chatIds.add(chatId);
				while (lastMessageNumber > 0) {
					WhatsappMessagesDto messageDto = whatappServiceRepo.readingWhatsappMessagesHistoryFalse(chatId,
							lastMessageNumber);

					Messages[] indvidualMsgList = messageDto.getMessages();

					if (indvidualMsgList.length > 0) {

						for (Messages individualMessage : indvidualMsgList) {

							if (individualMessage.getBody() != null) {

								if (individualMessage.getBody().length() > 6) {
									if (individualMessage.getBody().substring(0, 6).equalsIgnoreCase("oxylrv")) {

										String body = individualMessage.getBody();
										try {
											String split[] = null;
											if (body.contains("-")) {
												body = body.replaceAll(" ", "");
												split = body.split("-");
											} else if (body.contains(":")) {
												body = body.replaceAll(" ", "");
												split = body.split(":");
											} else {
												Pattern pattern = Pattern.compile("\\s+");
												Matcher matcher = pattern.matcher(body);
												body = matcher.replaceAll(" ");
												split = body.split("\\s", 2);
											}

											String vanNumber = split[0].toUpperCase();

											String amountMessage = "";
											if (split.length > 1) {
												amountMessage = split[1].toUpperCase();
												amountMessage = amountMessage.replaceAll("\\s", "");// replaceAll("[^0-9\\.]",
												// "");
												Double commitmentAmount = 0d;

												if (amountMessage.charAt(amountMessage.length() - 1) == 'K') {
													amountMessage = amountMessage.replaceAll("[^0-9\\.]", "");

													commitmentAmount = Double.parseDouble(amountMessage) * 1000d;

												} else if (amountMessage.contains("LAKHS")
														|| amountMessage.charAt(amountMessage.length() - 1) == 'L'
														|| amountMessage.contains("LAC")
														|| amountMessage.contains("LAKH")
														|| amountMessage.contains("LACS")) {

													amountMessage = amountMessage.replaceAll("[^0-9\\.]", "");

													commitmentAmount = Double.parseDouble(amountMessage) * 100000d;

												} else if (NumberUtils.isNumber(amountMessage)) {

													commitmentAmount = Double.parseDouble(amountMessage);

												} else {

													if (amountMessage.charAt(amountMessage.length() - 1) == 'A') {
														commitmentAmount = 50000.0;
													}

													if (amountMessage.charAt(amountMessage.length() - 1) == 'B') {
														commitmentAmount = 100000.0;
													}

													if (amountMessage.charAt(amountMessage.length() - 1) == 'C') {
														commitmentAmount = 150000.0;
													}

													if (amountMessage.charAt(amountMessage.length() - 1) == 'D') {
														commitmentAmount = 200000.0;
													}

													if (amountMessage.charAt(amountMessage.length() - 1) == 'E') {
														commitmentAmount = 250000.0;
													}

													if (amountMessage.charAt(amountMessage.length() - 1) == 'F') {
														commitmentAmount = 300000.0;
													}

													if (amountMessage.charAt(amountMessage.length() - 1) == 'G') {
														commitmentAmount = 350000.0;
													}

													if (amountMessage.charAt(amountMessage.length() - 1) == 'H') {
														commitmentAmount = 400000.0;
													}

													if (amountMessage.charAt(amountMessage.length() - 1) == 'I') {
														commitmentAmount = 450000.0;
													}

													if (amountMessage.charAt(amountMessage.length() - 1) == 'J') {
														commitmentAmount = 500000.0;
													}

												}

												CommitmentAmountExcelDto commitmentAmountExcelDto = new CommitmentAmountExcelDto();
												commitmentAmountExcelDto.setCommitmentAmountPromised(commitmentAmount);

												if (commitmentAmount > dealsInformation
														.getPaticipationLimitToLenders()) {
													double diffAmount = commitmentAmount
															- dealsInformation.getPaticipationLimitToLenders();

													commitmentAmount = commitmentAmount - diffAmount;

												}

												if (map.containsKey(vanNumber)) {
													map.replace(vanNumber, commitmentAmount);

												} else {
													map.put(vanNumber, commitmentAmount);
												}

											}
										} catch (Exception ex) {
											ex.printStackTrace();
										}

									}
								}
							}

							lastMessageNumber = Integer.parseInt(individualMessage.getMessageNumber());
						}

					} else {
						lastMessageNumber = indvidualMsgList.length;
					}
				}

				Iterator<Map.Entry<String, Double>> itr = map.entrySet().iterator();

				while (itr.hasNext()) {
					Map.Entry<String, Double> entry = itr.next();

					if (entry.getKey().length() > 6) {
						String userId = entry.getKey().substring(6);

						if (StringUtils.isNumeric(userId)) {
							User user = userRepo.findByIdNum(Integer.parseInt(userId));

							if (user != null) {
								PersonalDetails personalDetails = user.getPersonalDetails();

								CommitmentAmountExcelDto commitmentAmountExcelDto = new CommitmentAmountExcelDto();
								commitmentAmountExcelDto.setCommitmentAmountConsidered(entry.getValue());
								commitmentAmountExcelDto.setVirtualAccountnumber(entry.getKey());

								commitmentAmountExcelDto.setUserId(user.getId());
								commitmentAmountExcelDto.setLenderName(
										personalDetails.getFirstName() + " " + personalDetails.getLastName());

								commitmentAmountExcelDtoList.add(commitmentAmountExcelDto);

								commitmentAmountList.add(entry.getKey() + " : " + entry.getValue().intValue());

								amount += entry.getValue();
								count++;

								if (amount >= dealsInformation.getDealAmount()) {
									commitmentFullFilled = true;
								}

							}
						}
					}

				}
				commitmentAmountDetails += "*Commitment Details:*\n";
				commitmentAmountDetails += "*Deal Value in INR :" + dealsInformation.getDealAmount().intValue() + "*"
						+ "\n";
				commitmentAmountDetails += "No. of Committed Lenders : " + "*" + count + "*" + "\n\n";

				if (commitmentAmountList != null && !commitmentAmountList.isEmpty()) {
					for (String commit : commitmentAmountList) {
						commitmentAmountDetails += commit + "\n\n";

					}

					commitmentAmountDetails += "*" + "Total commitment Amount in INR: " + amount.intValue() + "*"
							+ "\r\n\n" + "*This is a system generated message*";

					whatappServiceRepo.sendingWhatsappMessagestoMultipleGroup(chatIds, commitmentAmountDetails,
							wtappApi);

					if (commitmentFullFilled && dealsInformation.getLendersCommitmentAmount() == null) {
						if (commitmentAmountExcelDtoList != null && !commitmentAmountExcelDtoList.isEmpty()) {
							StringBuffer message = new StringBuffer();

							message.append(
									"*Dear Lenders ,* Happy to inform you that the commitment amount has reached the deal value."
											+ "\r\n\n"
											+ "Request all the lenders to *transfer funds to their wallets and start participating in the deal by logging into the portal.*"
											+ "\r\n\n"
											+ "*System will consider only if the transaction is done and participation is complete.*"
											+ "\r\n\n" + "We will keep you posted on the progress" + "\r\n\n"
											+ "*This is a system generated message*");

							whatappServiceRepo.sendingWhatsappMessagestoMultipleGroup(chatIds, message.toString(),
									wtappApi);
							dealsInformation.setLendersCommitmentAmount(commitmentAmountDetails.toString());
							oxyBorrowersDealsInformationRepo.save(dealsInformation);

						}

						writeCommitmentDeatilsExcelFile(commitmentAmountExcelDtoList, dealsInformation.getDealName());

					}

				}

			}
		}

		dto.setCommitmentAmount(amount);
		dto.setMap(map);
		dto.setCount(count);
		dto.setList(list);

		return dto;
	}

	private void writeCommitmentDeatilsExcelFile(List<CommitmentAmountExcelDto> commitmentAmountExcelDtoList,
			String dealName) {

		FileOutputStream outFile = null;
		XSSFWorkbook workBook = new XSSFWorkbook();
		XSSFSheet spreadSheet = workBook.createSheet(dealName + "_CommitmentReport.xls");
		XSSFFont headerFont = workBook.createFont();
		CellStyle style = workBook.createCellStyle();

		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 13);
		headerFont.setColor(IndexedColors.BLACK.index);
		style.setFont(headerFont);
		spreadSheet.createFreezePane(0, 1);
		Row row = spreadSheet.createRow(0);

		Cell cell = row.createCell(0);
		cell.setCellValue("Lender_Id");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("Virtual Account No.");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("Lender Name");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("Commitment Amount");
		cell.setCellStyle(style);

		int rowCount = 0;
		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		for (CommitmentAmountExcelDto dto : commitmentAmountExcelDtoList) {

			Row row1 = spreadSheet.createRow(++rowCount);

			Cell cell1 = row1.createCell(0);
			cell1.setCellValue(dto.getUserId());

			cell1 = row1.createCell(1);
			cell1.setCellValue(dto.getVirtualAccountnumber());

			cell1 = row1.createCell(2);
			cell1.setCellValue(dto.getLenderName());

			cell1 = row1.createCell(3);
			cell1.setCellValue(dto.getCommitmentAmountConsidered().intValue());

		}

		String filePath = dealName + "_CommitmentReport.xls";
		File f = new File(filePath);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);
			if (f.exists()) {
				logger.info("file saved");
			}

			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate = expectedDateFormat.format(current.getTime());
			templateContext.put("currentDate", expectedCurrentDate);

			String mailSubject = dealName + " Commitement Report";
			EmailRequest emailRequest = new EmailRequest(
					new String[] { "archana.n@oxyloans.com", "subbu@oxyloans.com" }, "sendScheduledENach.template",
					templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			} else {
				logger.info("Email Sent!");

				if (f.delete()) {
					logger.info("File deleted");
				} else {
					logger.info("File Not deleted");

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	@Transactional
	public LenderMappedToGroupIdResponseDto gettingLenderCurrentWalletBalanceInfo(OxyLendersGroups oxyLendersGroups) {
		LenderMappedToGroupIdResponseDto lenderMappedToGroupIdResponseDto = new LenderMappedToGroupIdResponseDto();
		List<LenderCurrentWalletBalanceResponse> listOfValues = null;
		List<Object[]> lenderWithWalletAmount = null;
		double totalWalletAmount = 0.0;

		if (oxyLendersGroups.getWalletAmountType().equalsIgnoreCase("ZERO")) {
			lenderWithWalletAmount = userRepo.findingListOfUsersWalletEqualsZero();
			if (lenderWithWalletAmount != null && !lenderWithWalletAmount.isEmpty()) {
				listOfValues = singleLenderWalletDetails(lenderWithWalletAmount);

			}

		} else if (oxyLendersGroups.getWalletAmountType().equalsIgnoreCase("GREATERTHANZERO")) {
			lenderWithWalletAmount = userRepo.findingListOfUsersWalletGreaterThanZero();
			if (lenderWithWalletAmount != null && !lenderWithWalletAmount.isEmpty()) {
				listOfValues = singleLenderWalletDetails(lenderWithWalletAmount);

			}

		} else {
			lenderWithWalletAmount = userRepo.findingListOfUsersWalletLessThanZero();
			if (lenderWithWalletAmount != null && !lenderWithWalletAmount.isEmpty()) {
				listOfValues = singleLenderWalletDetails(lenderWithWalletAmount);

			}
		}

		lenderMappedToGroupIdResponseDto.setListOfLenderCurrentWalletBalanceResponse(listOfValues);
		lenderMappedToGroupIdResponseDto.setLenderWalletsExcelLink(generateLenderWalletBalanceExcelSheet(listOfValues));
		lenderMappedToGroupIdResponseDto.setTotalWalletAmount(BigDecimal.valueOf(totalWalletAmount).toBigInteger());
		logger.info("gettingLenderCurrentWalletBalanceInfo method ends...............");
		return lenderMappedToGroupIdResponseDto;
	}

	public List<LenderCurrentWalletBalanceResponse> singleLenderWalletDetails(List<Object[]> lenderWithWalletAmount) {
		List<LenderCurrentWalletBalanceResponse> listOfValues = new ArrayList<LenderCurrentWalletBalanceResponse>();

		if (lenderWithWalletAmount != null && !lenderWithWalletAmount.isEmpty()) {
			Iterator it = lenderWithWalletAmount.iterator();
			while (it.hasNext()) {
				Object[] e = (Object[]) it.next();
				int id = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
				String firstName = e[1] == null ? " " : e[1].toString();
				String lastName = e[2] == null ? " " : e[2].toString();
				double walletAmount = Double.parseDouble(e[3] == null ? "0.0" : e[3].toString());
				LenderCurrentWalletBalanceResponse lenderCurrentWalletBalanceResponse = new LenderCurrentWalletBalanceResponse();
				lenderCurrentWalletBalanceResponse.setUserId(id);
				lenderCurrentWalletBalanceResponse.setName(firstName + " " + lastName);
				lenderCurrentWalletBalanceResponse
						.setCurrentWalletAmount(BigDecimal.valueOf(walletAmount).toBigInteger());
				listOfValues.add(lenderCurrentWalletBalanceResponse);
			}
		}

		return listOfValues;

	}

	public LenderCurrentWalletBalanceResponse singleLenderWalletDetails(Integer id, String name, double credit,
			double debit, double agreements, double agreementsToDeals, double firstPaticipation,
			double secondPaticipation, double walletAmount) {
		LenderCurrentWalletBalanceResponse lenderCurrentWalletBalanceResponse = new LenderCurrentWalletBalanceResponse();
		lenderCurrentWalletBalanceResponse.setUserId(id);
		lenderCurrentWalletBalanceResponse.setName(name);
		lenderCurrentWalletBalanceResponse.setCredit(BigDecimal.valueOf(credit).toBigInteger());
		lenderCurrentWalletBalanceResponse.setDebit(BigDecimal.valueOf(debit).toBigInteger());
		lenderCurrentWalletBalanceResponse.setTotalLoanAmount(BigDecimal.valueOf(agreements).toBigInteger());
		lenderCurrentWalletBalanceResponse.setLoanAmountToDeals(BigDecimal.valueOf(agreementsToDeals).toBigInteger());
		lenderCurrentWalletBalanceResponse.setFirstPaticipation(BigDecimal.valueOf(firstPaticipation).toBigInteger());
		lenderCurrentWalletBalanceResponse.setSecondPaticipation(BigDecimal.valueOf(secondPaticipation).toBigInteger());
		lenderCurrentWalletBalanceResponse.setCurrentWalletAmount(BigDecimal.valueOf(walletAmount).toBigInteger());
		return lenderCurrentWalletBalanceResponse;

	}

	public String generateLenderWalletBalanceExcelSheet(List<LenderCurrentWalletBalanceResponse> listOfValues) {

		String excelurl = "";

		if (listOfValues != null && !listOfValues.isEmpty()) {
			FileOutputStream outFile = null;
			XSSFWorkbook workBook = new XSSFWorkbook();
			XSSFSheet spreadSheet = workBook.createSheet("LendersWallet" + ".xls");
			XSSFFont headerFont = workBook.createFont();
			CellStyle style = workBook.createCellStyle();

			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 13);
			headerFont.setColor(IndexedColors.BLACK.index);
			style.setFont(headerFont);
			spreadSheet.createFreezePane(0, 1);
			Row row = spreadSheet.createRow(0);

			Cell cell = row.createCell(0);
			cell.setCellValue("Id");
			cell.setCellStyle(style);

			cell = row.createCell(1);
			cell.setCellValue("Name");
			cell.setCellStyle(style);

			cell = row.createCell(2);
			cell.setCellValue("Wallet amount");
			cell.setCellStyle(style);

			int rowCount = 0;
			for (LenderCurrentWalletBalanceResponse lenderInfo : listOfValues) {

				Row row1 = spreadSheet.createRow(++rowCount);

				Cell cell1 = row1.createCell(0);
				cell1.setCellValue("LR" + lenderInfo.getUserId());

				cell1 = row1.createCell(1);
				cell1.setCellValue(lenderInfo.getName());

				cell1 = row1.createCell(2);
				cell1.setCellValue(lenderInfo.getCurrentWalletAmount().toString());

			}

			String fileName = "LendersWallet" + ".xls";
			File f = new File(fileName);

			try {
				outFile = new FileOutputStream(f);
				workBook.write(outFile);

				Calendar current = Calendar.getInstance();
				TemplateContext templateContext = new TemplateContext();
				String expectedCurrentDate = expectedDateFormat.format(current.getTime());

				FileRequest fileRequest = new FileRequest();
				fileRequest.setInputStream(new FileInputStream(f));
				fileRequest.setFileName(fileName);
				fileRequest.setFileType(FileType.TotalLendersWallet);
				fileRequest.setFilePrifix(FileType.TotalLendersWallet.name());

				fileManagementService.putFile(fileRequest);

				FileResponse getFile = fileManagementService.getFile(fileRequest);

				excelurl = getFile.getDownloadUrl();

			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
			}

			if (f.exists()) {
				f.delete();
			}

		}

		return excelurl;
	}

	@Override
	public WhatappInformation gettingListOfDealsNames() {
		logger.info("gettingListOfDealsNames method starts......................");

		List<ListOfWhatappGroupNames> listOfDeals = new ArrayList<ListOfWhatappGroupNames>();

		List<String> listOfDealsNames = oxyBorrowersDealsInformationRepo.getListOfDealsNames();
		if (listOfDealsNames != null && !listOfDealsNames.isEmpty()) {
			for (String dealName : listOfDealsNames) {
				ListOfWhatappGroupNames dealsNames = new ListOfWhatappGroupNames();
				dealsNames.setDealName(dealName);

				listOfDeals.add(dealsNames);

			}
		}
		WhatappInformation dealInformation = new WhatappInformation();

		dealInformation.setListOfWhatappGroups(listOfDeals);
		dealInformation.setTotalWhatappGroupsCount(listOfDeals.size());

		logger.info("gettingListOfDealsNames method ends......................");

		return dealInformation;

	}

	@Override
	public WhatsappCampaignDto sendwhatsAppCampaignMessage(WhatsappCampaignDto whatsappCampaignDto) {

		List<String> chatIds = new ArrayList<String>();

		if (whatsappCampaignDto.getMessageType().equalsIgnoreCase("campaign")) {
			String[] groupNames = whatsappCampaignDto.getWhatsappGroupNames().split(",");
			for (String chatId : groupNames) {
				String whatsappChatId = chatId;

				if (whatsappChatId != null) {
					chatIds.add(whatsappChatId);

				}
			}
			String message = whatsappCampaignDto.getMessage();

			message += "\n\n*This is a system generated message*";

			whatappServiceRepo.sendingWhatsappMessagestoMultipleGroup(chatIds, message, wtappApi);

		} else {
			String phoneNumber = whatsappCampaignDto.getPhoneNumber();

			String message = whatsappCampaignDto.getMessage();

			whatappServiceRepo.sendingWhatsappMsgWithMobileNumber(phoneNumber, message, whatsaAppSendApiNewInstance,
					null);
		}
		WhatsappCampaignDto response = new WhatsappCampaignDto();
		response.setMessageStatus("message Sent");

		return response;

	}

	@Override
	public EnachScheduledMailResponseDto sendingMailToReferrerAboutReference1() {

		logger.info("sendingMailToReferrerAboutReference1 method starts......................");

		EnachScheduledMailResponseDto response1 = new EnachScheduledMailResponseDto();

		List<Integer> listOfReferrers = lenderReferenceDetailsRepo.findingReferrers();

		if (listOfReferrers != null && !listOfReferrers.isEmpty()) {
			for (Integer id : listOfReferrers) {

				List<LenderReferenceResponseDto> referrerInfo = new ArrayList<LenderReferenceResponseDto>();

				List<LenderReferenceAmountResponse> referralBonusInfo = new ArrayList<LenderReferenceAmountResponse>();

				List<LenderReferenceDetails> listOfReferees = lenderReferenceDetailsRepo.gettingListOfReferees(id);

				if (listOfReferees != null && !listOfReferees.isEmpty()) {
					for (LenderReferenceDetails referee : listOfReferees) {

						LenderReferenceResponseDto refereeResponse = new LenderReferenceResponseDto();

						refereeResponse
								.setRefereeName(referee.getRefereeName() == null ? "" : referee.getRefereeName());
						refereeResponse.setStatus(
								referee.getStatus().toString() == null ? "" : referee.getStatus().toString());

						if (referee.getReferredOn() != null) {
							String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(referee.getReferredOn());
							refereeResponse.setReferredOn(modifiedDate);
						} else {
							refereeResponse.setReferredOn("");
						}
						refereeResponse
								.setRefereeEmail(referee.getRefereeEmail() == null ? "" : referee.getRefereeEmail());
						refereeResponse.setRefereeMobileNumber(
								referee.getRefereeMobileNumber() == null ? "" : referee.getRefereeMobileNumber());

						List<LenderReferralBonusUpdated> listOfBonusAmount = null;

						listOfBonusAmount = lenderReferralBonusUpdatedRepo.findingListOfReferralBonus(id,
								referee.getRefereeId());

						if (listOfBonusAmount != null && !listOfBonusAmount.isEmpty()) {
							for (LenderReferralBonusUpdated bonusAmount : listOfBonusAmount) {

								LenderReferenceAmountResponse referralBonus = new LenderReferenceAmountResponse();

								referralBonus
										.setAmount(bonusAmount.getAmount() == null ? 0.0 : bonusAmount.getAmount());
								referralBonus.setRefereeId(
										bonusAmount.getRefereeUserId() == null ? 0 : bonusAmount.getRefereeUserId());
								referralBonus.setReferrerId(
										bonusAmount.getReferrerUserId() == null ? 0 : bonusAmount.getReferrerUserId());
								referralBonus.setPaymentStatus(
										bonusAmount.getPaymentStatus() == null ? "" : bonusAmount.getPaymentStatus());

								if (bonusAmount.getTransferredOn() != null) {
									String modifiedDate1 = new SimpleDateFormat("yyyy-MM-dd")
											.format(bonusAmount.getTransferredOn());

									referralBonus.setTransferredOn(modifiedDate1);
								} else {
									referralBonus.setTransferredOn("N/A");
								}
								referralBonusInfo.add(referralBonus);

							}
						}

						refereeResponse.setLenderReferenceAmountResponse(referralBonusInfo);
						referrerInfo.add(refereeResponse);

					}
				}

				ByteArrayOutputStream output = null;
				FileOutputStream outFile = null;

				XSSFWorkbook workBook = new XSSFWorkbook();
				XSSFSheet spreadSheet = workBook.createSheet(LocalDate.now() + "ReferralBonusEarnedInfo.xls");

				XSSFSheet sheet1 = workBook.createSheet("Bonus Amount Status");

				CellStyle style = workBook.createCellStyle();
				style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
				style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				XSSFFont font = workBook.createFont();// Create font
				font.setBold(true);
				style.setFont(font);

				Row row0 = spreadSheet.createRow(0);
				Cell cell = row0.createCell(0);
				cell.setCellValue("Referee Id");
				cell.setCellStyle(style);

				cell = row0.createCell(1);
				cell.setCellValue("Name");
				cell.setCellStyle(style);

				cell = row0.createCell(3);
				cell.setCellValue("Email");
				cell.setCellStyle(style);

				cell = row0.createCell(4);
				cell.setCellValue("Mobile Number");
				cell.setCellStyle(style);

				cell = row0.createCell(5);
				cell.setCellValue("Status");
				cell.setCellStyle(style);

				cell = row0.createCell(6);
				cell.setCellValue("Referrerd On");
				cell.setCellStyle(style);

				cell = row0.createCell(2);
				cell.setCellValue("Earned Amount");
				cell.setCellStyle(style);

				int rowCount = 0;

				for (int i = 0; i <= 10; i++) {
					spreadSheet.autoSizeColumn(i);
				}

				int countForSno = 0;

				if (listOfReferees != null && !listOfReferees.isEmpty()) {
					for (LenderReferenceDetails obj : listOfReferees) {

						List<LenderReferralBonusUpdated> listOfBonusAmount = lenderReferralBonusUpdatedRepo
								.findingListOfReferralBonusForReferrer(id);

						Row row = spreadSheet.createRow(++rowCount);

						Cell cell1 = row.createCell(1);
						cell1.setCellValue(obj.getRefereeName() == null ? "" : obj.getRefereeName());

						cell1 = row.createCell(3);
						cell1.setCellValue(obj.getRefereeEmail() == null ? "" : obj.getRefereeEmail());

						cell1 = row.createCell(4);
						cell1.setCellValue(obj.getRefereeMobileNumber() == null ? "" : obj.getRefereeMobileNumber());

						cell1 = row.createCell(5);
						cell1.setCellValue(obj.getStatus().toString() == null ? "" : obj.getStatus().toString());

						if (obj.getReferredOn() != null) {
							String modifiedDate1 = new SimpleDateFormat("yyyy-MM-dd").format(obj.getReferredOn());

							cell1 = row.createCell(6);
							cell1.setCellValue(modifiedDate1);
						} else {
							cell1 = row.createCell(6);
							cell1.setCellValue("");
						}

						String refereeId = "LR" + obj.getRefereeId();

						cell1 = row.createCell(0);
						cell1.setCellValue(refereeId == null ? "LR0" : refereeId);

						Double sumOfAmountEarned = lenderReferralBonusUpdatedRepo
								.findingSumOfAmount(obj.getReferrerId(), obj.getRefereeId());
						if (sumOfAmountEarned != null) {
							cell1 = row.createCell(2);
							cell1.setCellValue(sumOfAmountEarned);

						} else {
							cell1 = row.createCell(2);
							cell1.setCellValue(0.0);

						}

						countForSno = countForSno + 1;

						Row row1 = sheet1.createRow(0);
						Cell cell2 = row1.createCell(0);
						cell2.setCellValue("Referee Id");
						cell2.setCellStyle(style);

						cell2 = row1.createCell(1);
						cell2.setCellValue("Referee Name");
						cell2.setCellStyle(style);

						cell2 = row1.createCell(2);
						cell2.setCellValue("Payment Status");
						cell2.setCellStyle(style);

						cell2 = row1.createCell(3);
						cell2.setCellValue("Transferred On");
						cell2.setCellStyle(style);

						cell2 = row1.createCell(4);
						cell2.setCellValue("Amount");
						cell2.setCellStyle(style);

						int rowCount1 = 0;

						for (int i = 0; i <= 10; i++) {
							sheet1.autoSizeColumn(i);
						}

						int countForSno1 = 0;

						if (listOfBonusAmount != null && !listOfBonusAmount.isEmpty()) {
							for (LenderReferralBonusUpdated obj1 : listOfBonusAmount) {

								Row row2 = sheet1.createRow(++rowCount1);

								User lender = userRepo.findById(obj1.getRefereeUserId()).get();

								Cell cell3 = row2.createCell(1);
								cell3.setCellValue((lender.getPersonalDetails().getFirstName() + ""
										+ lender.getPersonalDetails().getLastName()) == null ? ""
												: (lender.getPersonalDetails().getFirstName() + ""
														+ lender.getPersonalDetails().getLastName()));

								cell3 = row2.createCell(4);
								cell3.setCellValue(obj1.getAmount() == null ? 0.0 : obj1.getAmount());

								cell3 = row2.createCell(2);
								cell3.setCellValue(obj1.getPaymentStatus() == null ? "" : obj1.getPaymentStatus());

								if (obj1.getTransferredOn() != null) {
									String modifiedDate2 = new SimpleDateFormat("yyyy-MM-dd")
											.format(obj1.getTransferredOn());

									cell3 = row2.createCell(3);
									cell3.setCellValue(modifiedDate2);
								} else {
									cell3 = row2.createCell(3);
									cell3.setCellValue("N/A");
								}

								String refereeUserId = "LR" + obj1.getRefereeUserId();

								cell3 = row2.createCell(0);
								cell3.setCellValue(refereeUserId == null ? "LR0" : refereeUserId);

								countForSno1 = countForSno1 + 1;

							}
						}
						for (int i = 0; i <= 10; i++) {
							sheet1.autoSizeColumn(i);
						}

					}
				}
				for (int i = 0; i <= 10; i++) {
					spreadSheet.autoSizeColumn(i);
				}

				String filePath = LocalDate.now() + "ReferralBonusEarnedInfo.xls";

				File f = new File(filePath);

				User user = userRepo.findById(id).get();

				Double sumOfAmountEarned = lenderReferralBonusUpdatedRepo.findingSumOfAmountEarnedByReferrer(id);
				if (sumOfAmountEarned == null) {
					sumOfAmountEarned = 0.0;
				} else {
					sumOfAmountEarned = sumOfAmountEarned;
				}

				try {
					outFile = new FileOutputStream(f);
					workBook.write(outFile);
					if (f.exists()) {
						response1.setFileStatus("File saved");
					}

					String displayerName = "Referral Bonus";
					String referrerName = user.getPersonalDetails().getFirstName() + ""
							+ user.getPersonalDetails().getLastName();
					Calendar current = Calendar.getInstance();
					TemplateContext templateContext = new TemplateContext();
					String expectedCurrentDate = expectedDateFormat.format(current.getTime());
					templateContext.put("currentDate", expectedCurrentDate);
					templateContext.put("referrerName", referrerName);
					templateContext.put("sumOfAmountEarned", sumOfAmountEarned);

					String mailSubject = "Referral Bonus Earned Information Of " + "LR" + id;

					FileRequest fileRequest = new FileRequest();
					fileRequest.setInputStream(new FileInputStream(f));
					fileRequest.setFileName(filePath);
					fileRequest.setFileType(FileType.ReferralBonusEarnedInfo);
					fileRequest.setFilePrifix(FileType.ReferralBonusEarnedInfo.name());

					try {
						FileResponse putFile = fileManagementService.putFile(fileRequest);

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
					}

					EmailRequest emailRequest = new EmailRequest(
							new String[] { "tadakamallaanusha@gmail.com", "narendra@oxyloans.com" },
							"RefereeInfo-Referrer.template", templateContext, mailSubject, displayerName,
							new String[] { f.getAbsolutePath() });

					EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
					if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
						try {
							throw new MessagingException(emailResponseFromService.getErrorMessage());
						} catch (MessagingException e2) {

							e2.printStackTrace();
						}
					} else {
						response1.setEmailStatus("Email Sent!");

						if (f.delete()) {
							System.out.println("File deleted");
						} else {
							System.out.println("Not deleted");
						}

					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		}

		logger.info("sendingMailToReferrerAboutReference1 method ends......................");

		return response1;
	}

	@Override
	public EquityLendersListsDto lendersInAllEquityDeals() {

		List<EquityNewLendersDto> newLenderGrp = new ArrayList<>();
		List<EquityFoundingLendersDto> FoundingGroup = new ArrayList<>();

		int newLendersCount = 0;
		int lenderGroupMembersCount = 0;

		EquityLendersListsDto response = new EquityLendersListsDto();

		int equityDealsCount = 0;
		List<OxyBorrowersDealsInformation> equityDealsList = oxyBorrowersDealsInformationRepo.getEquityDealsList();

		if (equityDealsList != null && !equityDealsList.isEmpty()) {

			equityDealsCount = oxyBorrowersDealsInformationRepo.getListOfEquityDeals();

			for (OxyBorrowersDealsInformation equityDeals : equityDealsList) {

				List<OxyLendersAcceptedDeals> lendersAccepetdDealsList = oxyLendersAcceptedDealsRepo
						.findByDealId(equityDeals.getId());

				if (lendersAccepetdDealsList != null && !lendersAccepetdDealsList.isEmpty()) {
					for (OxyLendersAcceptedDeals lendersAcceptedDeal : lendersAccepetdDealsList) {

						User user = userRepo.findByIdNum(lendersAcceptedDeal.getUserId());
						String name = "";
						if (user != null) {
							PersonalDetails personalDetails = user.getPersonalDetails();
							if (personalDetails != null) {
								name = personalDetails.getFirstName() + " " + personalDetails.getLastName();
							}

						}

						if (lendersAcceptedDeal.getGroupId() == 0) {

							EquityNewLendersDto newLender = new EquityNewLendersDto();
							newLender.setLenderId(lendersAcceptedDeal.getUserId());

							newLender.setLenderName(name);
							newLender.setGroupId(lendersAcceptedDeal.getGroupId());
							String groupName = oxyLendersGroupRepo
									.getLenderGroupNameByid(lendersAcceptedDeal.getGroupId());
							newLender.setGroupName(groupName);
							newLender.setDealId(lendersAcceptedDeal.getDealId());
							newLender.setDealName(equityDeals.getDealName());
							newLender.setParticipatedAmount(lendersAcceptedDeal.getParticipatedAmount());
							newLendersCount++;

							newLenderGrp.add(newLender);

						} else if (lendersAcceptedDeal.getGroupId() > 0) {

							EquityFoundingLendersDto foundingLenders = new EquityFoundingLendersDto();
							foundingLenders.setLenderId(lendersAcceptedDeal.getUserId());
							foundingLenders.setGroupId(lendersAcceptedDeal.getGroupId());
							String groupName = oxyLendersGroupRepo
									.getLenderGroupNameByid(lendersAcceptedDeal.getGroupId());
							foundingLenders.setGroupName(groupName);
							foundingLenders.setLenderName(name);
							foundingLenders.setDealId(lendersAcceptedDeal.getDealId());
							foundingLenders.setDealName(equityDeals.getDealName());
							foundingLenders.setParticipatedAmount(lendersAcceptedDeal.getParticipatedAmount());
							lenderGroupMembersCount++;

							FoundingGroup.add(foundingLenders);

						}

					}
				}

			}

		}

		response.setNewLendersCount(newLendersCount);
		response.setLenderGroupMembersCount(lenderGroupMembersCount);
		response.setEquityDealsCount(equityDealsCount);
		response.setEquityNewLendersDto(newLenderGrp);
		response.setEquityFoundingLendersDto(FoundingGroup);
		return response;

	}

	@Override
	public StatusResponseDto getBorrowersMsgContent(String type) {

		String msgContent = "";
		if (type.equalsIgnoreCase("normal")) {
			msgContent = "*PAYMENT REMINDER FROM OXYLOANS*\r\n" + "\r\n" + "Hi *$borrowerName*,\r\n" + "\r\n" + "\r\n"
					+ "Hope you are doing well and Safe in this unprecedented situation.\r\n" + "\r\n"
					+ "We would like to remind you about your pending EMIs with us.\r\n" + "\r\n"
					+ "You have *$pendingEmis Pending EMIS* with us please clear the dues asap to avoid further actions.\r\n"
					+ "\r\n"
					+ "At this unprecedented time lenders are waiting for your payment please support us by clearing the Dues.\r\n"
					+ "\r\n" + "For any Queries please contact *$crmName ,$number*.\r\n" + "\r\n" + "Regards,\r\n"
					+ "\r\n" + "OXYLOANS Team";

		} else if (type.equalsIgnoreCase("critical")) {
			msgContent = "*PAYMENT REMINDER FROM OXYLOANS*\r\n" + "\r\n" + "Hi *$borrowerName*,\r\n" + "\r\n" + "\r\n"
					+ "You are in default of OXYLOANS P2P loan. *Your Pending EMIS : $pendingEmis , Emi Amount : $pendingAmount*\r\n"
					+ "\r\n"
					+ "Please make due payment today, failing which we shall be forced to take below actions:\r\n"
					+ "\r\n"
					+ "*Our Lender will post the default status at your social profile, which could be accessible to your friends, relatives and Colleagues as well as employment portals.*\r\n"
					+ "\r\n" + "*Platform Actions:*\r\n" + "\r\n"
					+ "1. Initiate legal proceedings to recover the money. Our Debt Recovery Agents (DRAs) will get in touch with you at your office/home premises for legitimate recovery.\r\n"
					+ "\r\n" + "2. Contact your current Company’s HR, and intimate them about your default.\r\n"
					+ "\r\n"
					+ "3. As Per RBI guidelines, Report default to CIBIL, which will then adversely impact your credit score.\r\n"
					+ "\r\n" + "For any Queries please contact *$crmName ,$number*.\r\n" + "\r\n"
					+ "Note : Please ignore the message, If you have already paid EMI.\r\n" + "\r\n" + " \r\n"
					+ "Regards,\r\n" + "\r\n" + "OXYLOANS Team";
		} else if (type.equalsIgnoreCase("lendertocritical")) {
			msgContent = "Hi $borrowerName,\r\n"
					+ "This is from your Lender $lenderName , I have helped you by providing the loan but you are in default of loan hence you are liable to pay delayed Interest according to your loan Amount on your $pendingEmis Pending EMIs and $amountPending Pending EMIS"
					+ "\r\n" + "Regards,\r\n" + "\r\n" + "$lenderName\r\n" + "\r\n" + "Lender OXYLOANS";

		} else if (type.equalsIgnoreCase("normalcrm")) {
			msgContent = "Hi $borrowerName,\r\n" + "This is From Customer Relationship Manager.\r\n"
					+ "You have $emi Pending EMIS with us please clear the dues asap to avoid further actions." + "\r\n"
					+ "Regards,\r\n" + "\r\n" + "*OXYLOANS Team*";
		}

		StatusResponseDto response = new StatusResponseDto();
		response.setMessage(msgContent);
		return response;

	}

	@Override
	public StatusResponseDto sendNotoficationsToBorrowerFromAdmin(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException {

		logger.info("sendNotoficationsToBorrowerFromAdmin method starts");

		String spreadSheetId = request.getSpreadSheetId();

		List<String> s = new ArrayList<String>();

		String range = request.getRange();

		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

		Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getGmailCredentialss(request))
				.setApplicationName("reading sheet").build();
		Get get = service.spreadsheets().values().get(spreadSheetId, range);

		String msgFailedList = "";

		ValueRange value = get.execute();

		String status = "";

		List<List<Object>> values = value.getValues();

		Double val = 0.0;

		if (values == null || values.isEmpty()) {
			System.out.println("No data found.");
		} else {

			values.remove(0);

			for (List row : values) {

				String mobileNumber = row.get(3).toString();
				// Double emiAmount = Double.parseDouble(row.get(5).toString());
				String borrowerUniqueNumber = row.get(2).toString();
				String emailId = row.get(6).toString();
				int emisPending = Integer.parseInt(row.get(12).toString());
				String crmName = row.get(4).toString();

				String borrowerName = row.get(1).toString();
				Double emisPendingAmount = Double.parseDouble(row.get(15).toString());

				if (emisPending >= 1 && emisPendingAmount > 0) {

					if (range.equalsIgnoreCase("normal")) {

						TemplateContext templateContext = new TemplateContext();
						templateContext.put("borrowerName", borrowerName);
						templateContext.put("borrowerUniqueNumber", borrowerUniqueNumber);
						templateContext.put("emi", emisPending);
						templateContext.put("crmName", crmName);

						String mailsubject = "Payments Reminder From OxyLoans";
						String emailTemplateName = "borrowers-normal-emis-pending.template";
						String displayerName = "Oxyloans Admin";

						/*
						 * try { EmailRequest emailRequest = new EmailRequest(new String[] { emailId },
						 * emailTemplateName, templateContext, mailsubject, displayerName);
						 * EmailResponse emailResponse = emailService.sendEmail(emailRequest);
						 * 
						 * if (emailResponse.getStatus() == EmailResponse.Status.FAILED) { throw new
						 * MessagingException(emailResponse.getErrorMessage()); } } catch (Exception ex)
						 * {
						 * 
						 * }
						 */
						String phoneNumber = "91" + mobileNumber;

						String message = "";

						message = request.getMessage();

						message = message.replace("$borrowerName", borrowerName);
						message = message.replace("$pendingEmis", String.valueOf(emisPending));
						message = message.replace("$amountPending", String.valueOf(emisPendingAmount));
						message = message.replace("$crmName", crmName);

						status = whatappServiceRepo.sendingWhatsappMsgWithMobileNumber(phoneNumber, message,
								whatsaAppSendApiNewInstance, null);

						Thread.sleep(10000);

						if (status.equalsIgnoreCase("messageNotSent")) {
							// msgFailedList += borrowerName + ":" + borrowerUniqueNumber + "\r\n";

						} else {
							logger.info("Message sent");
						}

					} else if (range.equalsIgnoreCase("critical")) {

						TemplateContext templateContext = new TemplateContext();
						templateContext.put("borrowerName", borrowerName);
						templateContext.put("borrowerUniqueNumber", borrowerUniqueNumber);
						templateContext.put("emi", emisPending);
						templateContext.put("crmName", crmName);

						templateContext.put("amountPending", emisPendingAmount);

						String mailsubject = "Payments Reminder From OxyLoans";
						String emailTemplateName = "borrowers-critical-emis-pending-from-lender.template";
						String displayerName = "Admin";

						/*
						 * try { EmailRequest emailRequest = new EmailRequest(new String[] { emailId },
						 * emailTemplateName, templateContext, mailsubject, displayerName);
						 * EmailResponse emailResponse = emailService.sendEmail(emailRequest);
						 * 
						 * if (emailResponse.getStatus() == EmailResponse.Status.FAILED) { throw new
						 * MessagingException(emailResponse.getErrorMessage()); } } catch (Exception ex)
						 * {
						 * 
						 * }
						 */
						String phoneNumber = "91" + mobileNumber;

						String message = "";

						message = request.getMessage();

						message = message.replace("$borrowerName", borrowerName);
						message = message.replace("$pendingEmis", String.valueOf(emisPending));
						message = message.replace("$amountPending", String.valueOf(emisPendingAmount.intValue()));
						message = message.replace("$crmName", crmName);

						status = whatappServiceRepo.sendingWhatsappMsgWithMobileNumber(phoneNumber, message,
								whatsaAppSendApiNewInstance, null);

						Thread.sleep(10000);

					}
				}

			}

		}

		File tokenFile = new File("tokens//StoredCredential");

		if (tokenFile.exists()) {

			tokenFile.delete();

		} else {

			logger.info("token file not deleted !!!!!!!!!!!!!!!!!");
		}

		StatusResponseDto response = new StatusResponseDto();

		logger.info("sendNotoficationsToBorrowerFromAdmin method starts");
		return response;

	}

	public Credential getGmailCredentialss(SpreadSheetRequestDto request)

			throws IOException, GeneralSecurityException, InterruptedException {

		logger.info("Gmail Credentials method started");

		String code = URLDecoder.decode(request.getGmailCode(), "UTF-8");
		logger.info("Gmail Credentials......" +code);


		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

		GoogleClientSecrets clientSecrets = new GoogleClientSecrets();

		Details details = new Details();
		details.setClientId(clientId);
		details.setClientSecret(clientSecret);
		clientSecrets.setWeb(details);

		// Build flow and trigger user authorization request.
		List<String> scopes = Arrays.asList(SheetsScopes.SPREADSHEETS);

		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, scopes)
						.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
						.setAccessType("offline").build();

		Credential credential = flow.loadCredential("userId");

		TokenResponse response = null;
		logger.info(" response......" +response);
		try {

			response = flow.newTokenRequest(code).setRedirectUri(spreadSheetRedirectUrl).execute();
			logger.info(" response......" +response);
		} catch (Exception ex) {

			throw new DataFormatException("Invalid Code", ErrorCodes.DATA_FETCH_ERROR);

		}
		logger.info("Gmail Credentials method end");
		return flow.createAndStoreCredential(response, "userId");

	}

	@Override
	public StatusResponseDto getUnsentBorrowerNotifications(SpreadSheetRequestDto request)
			throws GeneralSecurityException, IOException, InterruptedException {
		String spreadSheetId = request.getSpreadSheetId();

		String range = request.getRange();

		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

		Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getGmailCredentialss(request))
				.setApplicationName("reading sheet").build();
		Get get = service.spreadsheets().values().get(spreadSheetId, range);

		ValueRange value = get.execute();

		Map<String, String> unSentMap = new LinkedHashMap<String, String>();

		Map<String, String> sentMsgsMap = new LinkedHashMap<String, String>();

		List<List<Object>> values = value.getValues();

		String unSentMessages = "";

		int lastMessageNumber = 1;

		while (lastMessageNumber > 0) {

			WhatsappMessagesDto messageDto = whatappServiceRepo
					.readingWhatsappMessagesWithOutChatIdHistoryFalse(lastMessageNumber);

			Messages[] indvidualMsgList = messageDto.getMessages();

			if (indvidualMsgList.length > 0) {

				for (Messages individualMessage : indvidualMsgList) {

					String chatId = individualMessage.getChatId();

					String author = individualMessage.getAuthor();

					Long unixTimestamp = Long.parseLong(individualMessage.getTime());

					Date date = new Date(unixTimestamp * 1000L);

					SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

					String messageDate = formatter.format(date);

					String currentDate = request.getDate();

					if (messageDate.equals(currentDate)) {

						if (individualMessage.getBody() != null) {
							if (individualMessage.getBody().length() > 100) {

								if (individualMessage.getBody().substring(1, 94).equals(
										"GENTLE REMINDER FROM OXYLOANS---YOUR INTEREST IS ADDING DAY BY DAY—PLEASE PAY AT THE EARLIEST")) {

									String mobileNumber = individualMessage.getChatId().substring(2, 12);

									if (!sentMsgsMap.containsKey(mobileNumber)) {
										sentMsgsMap.put(mobileNumber, individualMessage.getMessageNumber());

									}
								}
							}
						}

					}

					lastMessageNumber = Integer.parseInt(individualMessage.getMessageNumber());

				}

			} else {
				lastMessageNumber = indvidualMsgList.length;
			}

		}

		if (sentMsgsMap != null && !sentMsgsMap.isEmpty()) {
			if (values == null || values.isEmpty()) {
				System.out.println("No data found.");
			} else {

				values.remove(0);
				for (List row : values) {

					String mobileNumber = row.get(3).toString();

					String borrowerUniqueNumber = row.get(2).toString();

					String borrowerName = row.get(1).toString();

					if (!sentMsgsMap.containsKey(mobileNumber)) {

						unSentMessages += borrowerUniqueNumber + "(" + borrowerName + "):-" + mobileNumber + "\n\n";

					}

				}
			}
		}

		StatusResponseDto response = new StatusResponseDto();
		List<String> chatIds = new ArrayList<String>();

		chatIds.add("917702795895-1628773475@g.us");

		whatappServiceRepo.sendingWhatsappMessagestoMultipleGroup(chatIds, unSentMessages, whatsaAppSendApiNewInstance);

		response.setMessage(unSentMessages);

		return response;
	}

	@Override
	public StatusResponseDto writeNotificationsResponseMessages(SpreadSheetRequestDto request)
			throws GeneralSecurityException, IOException, InterruptedException, ParseException {

		String spreadSheetId = request.getSpreadSheetId();

		String range = request.getRange();

		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

		Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getGmailCredentialss(request))
				.setApplicationName("reading sheet").build();
		Get get = service.spreadsheets().values().get(spreadSheetId, range);

		ValueRange value = get.execute();

		Map<String, String> map = new LinkedHashMap<String, String>();

		Map<String, String> map1 = new LinkedHashMap<String, String>();

		List<List<Object>> values = value.getValues();

		List<Integer> lastNumbersList = new ArrayList<>();

		int lastMessageNumber = Integer.parseInt(request.getLastMessageNumber());

		if (values == null || values.isEmpty()) {
			System.out.println("No data found.");
		} else {

			values.remove(0);
			for (List row : values) {

				String mobileNumber = row.get(3).toString();

				String chatId = 91 + mobileNumber + "@c.us";

				String borrowerUniqueNumber = row.get(2).toString();

				String status = row.get(20).toString();

				String borrowerName = row.get(1).toString();

				String oldMessage = row.get(21).toString();

				if (status.equalsIgnoreCase("running")) {

					map.put(chatId, oldMessage);

				}

			}
		}

		while (lastMessageNumber > 0) {

			WhatsappMessagesDto messageDto = whatappServiceRepo
					.readingWhatsappMessagesWithOutChatIdHistoryFalse(lastMessageNumber);

			Messages[] indvidualMsgList = messageDto.getMessages();

			if (indvidualMsgList.length > 0) {

				for (Messages individualMessage : indvidualMsgList) {

					Long unixTimestamp = Long.parseLong(individualMessage.getTime());

					Date date = new Date(unixTimestamp * 1000L);

					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

					String messageDate = formatter.format(date);

					// Date

					String givenDate = request.getDate();

					Date d1 = formatter.parse(messageDate);
					Date d2 = formatter.parse(givenDate);

					String chatId = individualMessage.getChatId();

					String author = individualMessage.getAuthor();

					if (map.containsKey(individualMessage.getChatId())) {

						if (chatId.equalsIgnoreCase(author)) {
							if (messageDate.equals(givenDate) || d1.after(d2)) {

								if (individualMessage.getBody() != null) {

									String oldMessage = map.get(individualMessage.getChatId());

									String newMessage = messageDate + ":-" + individualMessage.getBody();
									String finalMessage = oldMessage + "\n" + newMessage;

									int messageNumber = Integer.parseInt(individualMessage.getMessageNumber());

									batchUpdate(spreadSheetId, range, oldMessage, finalMessage, service);

									lastNumbersList.add(messageNumber);

								}

							}
						}
					}

					lastMessageNumber = Integer.parseInt(individualMessage.getMessageNumber());

				}

			} else {
				lastMessageNumber = indvidualMsgList.length;
			}

		}

		String message = "";
		if (lastNumbersList.size() > 0) {
			Collections.sort(lastNumbersList);

			message = "Last message number:-" + lastNumbersList.get(lastNumbersList.size() - 1);

			List<String> chatIds = new ArrayList<String>();
			chatIds.add("917702795895-1628773475@g.us");

			whatappServiceRepo.sendingWhatsappMessagestoMultipleGroup(chatIds, message, whatsaAppSendApiNewInstance);

		}

		StatusResponseDto response = new StatusResponseDto();
		response.setMessage(message);
		return response;

	}

	private BatchUpdateSpreadsheetResponse batchUpdate(String spreadSheetId, String range, String find,
			String replacement, Sheets service) throws IOException {
		List<Request> requests = new ArrayList<>();
		// Change the spreadsheet's title.
		requests.add(new Request().setUpdateSpreadsheetProperties(new UpdateSpreadsheetPropertiesRequest()
				.setProperties(new SpreadsheetProperties().setTitle(range)).setFields("title")));
		// Find and replace text.
		requests.add(new Request()
				.setFindReplace(new FindReplaceRequest().setFind(find).setReplacement(replacement).setAllSheets(true)));
		// Add additional requests (operations) ...

		BatchUpdateSpreadsheetRequest body = new BatchUpdateSpreadsheetRequest().setRequests(requests);

		BatchUpdateSpreadsheetResponse response = service.spreadsheets().batchUpdate(spreadSheetId, body).execute();
		FindReplaceResponse findReplaceResponse = response.getReplies().get(1).getFindReplace();
		System.out.printf("%d replacements made.", findReplaceResponse.getOccurrencesChanged());
		// [END sheets_batch_update]
		return response;
	}

	@Override
	public WhatappInformation gettingListOfWhatsAppGroupNamesBasedOnGroupName(String groupName) {

		logger.info("gettingListOfWhatsAppGroupNamesBasedOnGroupName method starts....................");

		List<ListOfWhatappGroupNames> listOfNames = new ArrayList<ListOfWhatappGroupNames>();

		List<String> listOfWhatsAppGroupNames = whatappGroupsInformationRepo
				.findingGroupNamesBasedOnGroupName(groupName.toUpperCase());

		if (listOfWhatsAppGroupNames != null && !listOfWhatsAppGroupNames.isEmpty()) {
			for (String name : listOfWhatsAppGroupNames) {
				ListOfWhatappGroupNames groupNames = new ListOfWhatappGroupNames();

				groupNames.setGroupName(name);

				listOfNames.add(groupNames);
			}
		}

		WhatappInformation response = new WhatappInformation();

		response.setListOfWhatappGroups(listOfNames);
		response.setTotalWhatappGroupsCount(listOfNames.size());

		logger.info("gettingListOfWhatsAppGroupNamesBasedOnGroupName method ends....................");

		return response;

	}

	@Override
	public ClosedDealsInformation getLenderClosedDealsInfo(int userId, PaginationRequestDto pageRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderPaticipatedDeal getListOfLendersByDealIdForExcelSheet(int dealId) {
		LenderPaticipatedDeal lenderPaticipatedDeal = new LenderPaticipatedDeal();
		List<Object[]> listOfLendersMappedToDealId = oxyLendersAcceptedDealsRepo.getListOfLendersMappedToDealId(dealId);
		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
				.findDealAlreadyGiven(dealId);
		Integer count = 0;
		String dealName = "";
		Double paticipatedAmount = 0.0;
		Integer value = oxyLendersAcceptedDealsRepo.getListOfLendersMappedToDealIdCount(dealId);
		if (value != null) {
			count = value;
		}
		Double amount = oxyLendersAcceptedDealsRepo.getListOfLendersParticipatedAmount(dealId);
		Double updatedAmount = lendersPaticipationUpdationRepo.getDealUpdatedSumValue(dealId);
		if (amount != null) {
			if (updatedAmount != null) {
				paticipatedAmount = amount + updatedAmount;
			} else {
				paticipatedAmount = amount;
			}
		}
		List<LenderPaticipatedResponseDto> listOfLenders = new ArrayList<LenderPaticipatedResponseDto>();
		if (listOfLendersMappedToDealId != null && !listOfLendersMappedToDealId.isEmpty()) {
			Iterator it = listOfLendersMappedToDealId.iterator();
			String groupName = null;
			while (it.hasNext()) {
				Object[] e = (Object[]) it.next();
				LenderPaticipatedResponseDto lenderPaticipatedResponseDto = new LenderPaticipatedResponseDto();
				lenderPaticipatedResponseDto.setLenderId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
				lenderPaticipatedResponseDto.setGroupId(Integer.parseInt(e[1] == null ? "0" : e[1].toString()));
				int groupId = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
				if (groupId == 0) {
					groupName = "newLender";
				} else {
					groupName = oxyLendersGroupRepo.getLenderGroupNameByid(groupId);
				}
				int userId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
				String lenderName = null;
				User user = userRepo.findById(userId).get();
				if (user != null) {
					lenderName = user.getPersonalDetails().getFirstName() + " "
							+ user.getPersonalDetails().getLastName();
				}

				if (user.getBankDetails() != null) {
					if (user.getBankDetails().getAccountNumber() != null) {
						lenderPaticipatedResponseDto.setAccountNumber(user.getBankDetails().getAccountNumber());
					}
					if (user.getBankDetails().getIfscCode() != null) {
						lenderPaticipatedResponseDto.setIfscCode(user.getBankDetails().getIfscCode());
					}
				}
				Double dealAmount = Double.parseDouble(e[2] == null ? "0" : e[2].toString());
				List<LendersPaticipationUpdation> participationUpdated = lendersPaticipationUpdationRepo
						.getParticipationUpdatedAmounts(userId, dealId);

				lenderPaticipatedResponseDto.setPaticipatedAmount(dealAmount);
				lenderPaticipatedResponseDto.setProcessingFee(Double.parseDouble(e[3] == null ? "0" : e[3].toString()));
				lenderPaticipatedResponseDto.setFeeStatus(e[4] == null ? " " : e[4].toString());
				lenderPaticipatedResponseDto.setLederReturnType(e[5] == null ? " " : e[5].toString());
				lenderPaticipatedResponseDto
						.setRateOfInterest(Double.parseDouble(e[6] == null ? "0" : e[6].toString()));
				lenderPaticipatedResponseDto.setRegisteredDate(e[7] == null ? " " : e[7].toString());
				lenderPaticipatedResponseDto.setPaticipatedState(e[8] == null ? " " : e[8].toString());
				lenderPaticipatedResponseDto.setDealId(dealId);
				lenderPaticipatedResponseDto.setDealName(oxyBorrowersDealsInformation.getDealName());
				dealName = oxyBorrowersDealsInformation.getDealName();
				lenderPaticipatedResponseDto.setDealBorrowerName(oxyBorrowersDealsInformation.getBorrowerName());
				lenderPaticipatedResponseDto.setDealAmount(oxyBorrowersDealsInformation.getDealAmount());
				lenderPaticipatedResponseDto
						.setDealRateofinterest(oxyBorrowersDealsInformation.getBorrowerRateofinterest());
				lenderPaticipatedResponseDto.setDealDuration(oxyBorrowersDealsInformation.getDuration());
				lenderPaticipatedResponseDto.setGroupName(groupName);
				lenderPaticipatedResponseDto.setLenderName(lenderName);
				listOfLenders.add(lenderPaticipatedResponseDto);

				if (participationUpdated != null && !participationUpdated.isEmpty()) {

					for (LendersPaticipationUpdation lenderUpdation : participationUpdated) {

						LenderPaticipatedResponseDto lenderPaticipatedResponseDto1 = new LenderPaticipatedResponseDto();

						lenderPaticipatedResponseDto1
								.setLenderId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
						lenderPaticipatedResponseDto1
								.setGroupId(Integer.parseInt(e[1] == null ? "0" : e[1].toString()));
						lenderPaticipatedResponseDto1.setAccountNumber(user.getBankDetails().getAccountNumber());

						lenderPaticipatedResponseDto1.setIfscCode(user.getBankDetails().getIfscCode());
						lenderPaticipatedResponseDto1.setPaticipatedAmount(lenderUpdation.getUpdationAmount());
						lenderPaticipatedResponseDto1
								.setRegisteredDate(requiredDateFormat.format(lenderUpdation.getUpdatedOn()));

						lenderPaticipatedResponseDto1
								.setProcessingFee(Double.parseDouble(e[3] == null ? "0" : e[3].toString()));
						lenderPaticipatedResponseDto1.setFeeStatus(e[4] == null ? " " : e[4].toString());
						lenderPaticipatedResponseDto1.setLederReturnType(e[5] == null ? " " : e[5].toString());
						lenderPaticipatedResponseDto1
								.setRateOfInterest(Double.parseDouble(e[6] == null ? "0" : e[6].toString()));

						lenderPaticipatedResponseDto1.setPaticipatedState(e[8] == null ? " " : e[8].toString());
						lenderPaticipatedResponseDto1.setDealId(dealId);
						lenderPaticipatedResponseDto1.setDealName(oxyBorrowersDealsInformation.getDealName());
						dealName = oxyBorrowersDealsInformation.getDealName();
						lenderPaticipatedResponseDto1
								.setDealBorrowerName(oxyBorrowersDealsInformation.getBorrowerName());
						lenderPaticipatedResponseDto1.setDealAmount(oxyBorrowersDealsInformation.getDealAmount());
						lenderPaticipatedResponseDto1
								.setDealRateofinterest(oxyBorrowersDealsInformation.getBorrowerRateofinterest());
						lenderPaticipatedResponseDto1.setDealDuration(oxyBorrowersDealsInformation.getDuration());
						lenderPaticipatedResponseDto1.setGroupName(groupName);
						lenderPaticipatedResponseDto1.setLenderName(lenderName);
						listOfLenders.add(lenderPaticipatedResponseDto1);

					}

				}

			}
		}
		String url = generateParticipatedLendersExcelSheet(listOfLenders, dealName);

		lenderPaticipatedDeal.setParticipatedLendersExcel(url);
		lenderPaticipatedDeal.setLenderPaticipatedResponseDto(listOfLenders);
		lenderPaticipatedDeal.setCount(count);
		lenderPaticipatedDeal.setTotalAmountPaticipatedByLenders(BigDecimal.valueOf(paticipatedAmount).toBigInteger());
		return lenderPaticipatedDeal;

	}

	@Override
	@Transactional
	public DealLevelResponseDto dealIdBasedAgreementsGenerationForZaggle(int dealId,
			DealLevelRequestDto dealLevelRequestDto) throws PdfGeenrationException, IOException {
		DealLevelResponseDto dealLevelResponseDto = new DealLevelResponseDto();
		String message = null;
		OxyBorrowersDealsInformation dealinfo = oxyBorrowersDealsInformationRepo.findDealAlreadyGiven(dealId);
		String loanOwner = null;
		if (dealinfo != null) {
			loanOwner = dealinfo.getDealName();
			List<Object[]> listOfLendersMappedToDeal = oxyLendersAcceptedDealsRepo.getListOfLendersByDealId(dealId);
			if (listOfLendersMappedToDeal != null && !listOfLendersMappedToDeal.isEmpty()) {
				Iterator it = listOfLendersMappedToDeal.iterator();
				Double particiationAmount = 0.0;
				Double loanAmount = 0.0;
				Double lenderAmount = 0.0;
				while (it.hasNext()) {
					Object[] e = (Object[]) it.next();
					Integer userId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
					LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);

					Double totalLenderUpdatedAmount = lendersPaticipationUpdationRepo
							.getSumOfLenderUpdatedAmountToDeal(userId, dealId);

					logger.info(userId);
					logger.info(dealId);
					logger.info(totalLenderUpdatedAmount);
					if (totalLenderUpdatedAmount != null) {
						lenderAmount = Double.parseDouble(e[1] == null ? "0" : e[1].toString())
								+ totalLenderUpdatedAmount;
					} else {
						lenderAmount = Double.parseDouble(e[1] == null ? "0" : e[1].toString());
					}

					logger.info(lenderAmount);

					Double amountForAggrments = oxyLoanRepo.checkAlreadyAgreementsAgeneratedToLender(userId, dealId);
					logger.info(amountForAggrments);

					if (amountForAggrments != null) {
						particiationAmount = lenderAmount - amountForAggrments;
					} else {
						particiationAmount = lenderAmount;
					}

					logger.info(particiationAmount);

					Double walletAmount = getLenderWallet(userId);
					logger.info(walletAmount);
					if (walletAmount >= particiationAmount) {

						int countValueForPaticipation = 0;
						Double remainingAmount = 0.0;
						logger.info(particiationAmount);
						if (particiationAmount != null && particiationAmount > 0) {

							List<User> userList = userRepo
									.findbyUtmAndParentRequestIdisNull(dealLevelRequestDto.getUtm());

							if (userList != null && !userList.isEmpty()) {
								for (User userInfo : userList) {

									if (userInfo.getLoanOwner() != null
											&& userInfo.getLoanOwner().equalsIgnoreCase(loanOwner)) {

										int borrowerId = userInfo.getId();
										logger.info(borrowerId);

										LoanRequest loanRequest1 = loanRequestRepo
												.findByUserIdAndParentRequestIdIsNull(borrowerId);
										if (loanRequest1 != null) {

											Double currentApplicationAmount = loanRequestRepo
													.checkCurrentApplication(borrowerId);
											logger.info(currentApplicationAmount);
											if (currentApplicationAmount != null) {
												Double amountForAgreementsGrenerated = oxyLoanRepo
														.checkAlreadyAgreementsAgenerated(borrowerId, dealId);

												logger.info(amountForAgreementsGrenerated);
												int count = lenderBorrowerConversationRepo
														.countByLenderUserIdAndBorrowerUserId(userId, borrowerId);

												logger.info(count);
												OxyLoan oxyloan = oxyLoanRepo
														.findbyBorrowerUserIdAndLenderUserIdAndLanStatus(userId,
																borrowerId);

												Double amount = 0.0;
												if (amountForAgreementsGrenerated != null) {
													amount = amountForAgreementsGrenerated;
												}

												logger.info(amount);
												remainingAmount = currentApplicationAmount - amount;
												logger.info(remainingAmount);
												if (remainingAmount > 0) {
													if (count == 0) {
														LoanRequestDto loanRequestDto = new LoanRequestDto();
														if (particiationAmount >= 50000) {
															loanAmount = 50000.0;
														} else {
															loanAmount = particiationAmount;
														}

														if (remainingAmount < loanAmount) {

															loanAmount = remainingAmount;

														}

														Double totalAmountDisbursed = oxyLoanRepo
																.getTotalAmountDisbursedForParticularrUsers(userId,
																		borrowerId);

														if (loanAmount >= 5000d) {
															if (totalAmountDisbursed == null
																	|| totalAmountDisbursed < 50000) {
																loanRequestDto.setLoanRequestAmount(loanAmount);
																loanRequestDto.setRateOfInterest(
																		dealinfo.getBorrowerRateofinterest());
																loanRequestDto
																		.setDuration(loanRequest1.getDurationBySir());
																loanRequestDto.setDurationType(
																		loanRequest1.getDurationType().toString());
																loanRequestDto.setRepaymentMethod("I");
																loanRequestDto.setLoanPurpose("personal loan");
																loanRequestDto.setExpectedDate(expectedDateFormat
																		.format(dealinfo.getFundsAcceptanceEndDate()));
																loanRequestDto.setParentRequestId(loanRequest.getId());
																LoanResponseForDealsDto loanRequestForDeals = loanRequestForDeals1(
																		borrowerId, loanRequestDto);
																Integer loanRequestId = loanRequestForDeals
																		.getLoanRequestId();

																message = actOnRequestForDeals1(userId, loanRequestId,
																		"Accepted", dealId, dealLevelRequestDto);
																logger.info("message");
																particiationAmount = particiationAmount - (50000);
																if (particiationAmount == 0 || particiationAmount < 0) {
																	OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
																			.toCheckUserAlreadyInvoledInDeal(userId,
																					dealId);
																	oxyLendersAcceptedDeals.setParticipationState(
																			OxyLendersAcceptedDeals.ParticipationState.PARTICIPATED);
																	oxyLendersAcceptedDealsRepo
																			.save(oxyLendersAcceptedDeals);
																}
															}
														}
													}
												}
											}
										}
									}
								}

							}
						}

					} else {
						continue;
					}

				}

			} else {
				throw new ActionNotAllowedException("Deal is full Please select other deal.",
						ErrorCodes.ENTITY_NOT_ACTIVE);
			}
		}

		message = "successfullyUpdated";
		dealLevelResponseDto.setStatus(message);
		dealLevelResponseDto.setDealId(dealId);
		return dealLevelResponseDto;

	}

	public LoanResponseForDealsDto loanRequestForDeals1(int userId, LoanRequestDto loanRequestDto) {
		User user = userRepo.findById(userId).get();
		LoanRequest loanRequest = new LoanRequest();
		loanRequest.setLoanRequestAmount(loanRequestDto.getLoanRequestAmount());
		loanRequest.setDuration(loanRequestDto.getDuration());
		loanRequest.setLoanPurpose(loanRequestDto.getLoanPurpose());
		loanRequest.setRateOfInterest(loanRequestDto.getRateOfInterest());
		loanRequest.setUserPrimaryType(user.getPrimaryType());
		loanRequest.setRepaymentMethod(RepaymentMethod.valueOf(loanRequestDto.getRepaymentMethod()));
		if (loanRequestDto.getDurationType() != null) {
			loanRequest
					.setDurationType(loanRequestDto.getDurationType().equalsIgnoreCase("months") ? DurationType.Months
							: DurationType.Days);

		}
		try {
			loanRequest.setExpectedDate(expectedDateFormat.parse(loanRequestDto.getExpectedDate()));
		} catch (ParseException e1) {

			e1.printStackTrace();
		}
		LoanRequest parentRequest = null;

		if (loanRequestDto.getParentRequestId() != null) {
			parentRequest = loanRequestRepo.findById(loanRequestDto.getParentRequestId()).get();
			if (user.getPrimaryType() == PrimaryType.BORROWER) {
				LoanRequest loanRequest2 = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId());
				if (loanRequest2 != null) {
					LoanOfferdAmount loanOfferdAmount = loanRequest2.getLoanOfferedAmount();
					if (loanOfferdAmount == null) {
						throw new OperationNotAllowedException("User don't have loanoffer",
								ErrorCodes.INVALID_OPERATION);
					} else if (loanOfferdAmount != null) {
						if (loanOfferdAmount.getLoanOfferdStatus() != LoanOfferdStatus.LOANOFFERACCEPTED) {
							throw new OperationNotAllowedException("You  did not accepet loanoffer",
									ErrorCodes.INVALID_OPERATION);

						}
					}
				}
			}

			Integer lenderUserId = user.getPrimaryType() == PrimaryType.LENDER ? userId : parentRequest.getUserId();
			Integer borrowerUserId = user.getPrimaryType() == PrimaryType.BORROWER ? userId : parentRequest.getUserId();

			loanRequest.setParentRequestId(loanRequestDto.getParentRequestId());
			loanRequest.setLoanStatus(LoanStatus.Conversation);
			Integer requestedUser = parentRequest.getUserId(); // You are responding to this user's request
			// isLimitExceeded(requestedUser, user, loanRequestDto);
			LenderBorrowerConversation lenderBorrowerConversation = new LenderBorrowerConversation();
			lenderBorrowerConversation.setLenderUserId(lenderUserId);
			lenderBorrowerConversation.setBorrowerUserId(borrowerUserId);
			LenderBorrowerConversation addedEntry = lenderBorrowerConversationRepo.save(lenderBorrowerConversation);
			logger.info("Added entry to LenderBorrowerConversation with id {}", addedEntry.getId());
			loanRequest.setUserId(userId);
			// loanRequest.setEnachType("MANUALENACH");
			loanRequest.setLoanRequestId(new Long(System.currentTimeMillis()).toString()); // This is temporary
			loanRequest = loanRequestRepo.save(loanRequest);
			loanRequest.setLoanRequestId(LOAN_REQUEST_IDFORMAT.replace("{type}", user.getPrimaryType().getShortCode())
					.replace("{ID}", loanRequest.getId().toString()));
			loanRequest = loanRequestRepo.save(loanRequest);
		}
		LoanResponseForDealsDto loanResponseForDealsDto = new LoanResponseForDealsDto();
		loanResponseForDealsDto.setLoanRequestId(loanRequest.getId());
		return loanResponseForDealsDto;
	}

	public String actOnRequestForDeals1(int userId, Integer loanRequestId, String loanStatus, Integer dealId,
			DealLevelRequestDto dealLevelRequestDto) throws PdfGeenrationException, IOException {
		String loanIdFormanuvalEsign = null;
		OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
				.toCheckUserAlreadyInvoledInDeal(userId, dealId);

		User user = userRepo.findById(userId).get();

		LoanRequest loanRequest = loanRequestRepo.findById(loanRequestId).get();
		if (loanRequest.getLoanStatus() == LoanStatus.Accepted || loanRequest.getLoanStatus() == LoanStatus.Rejected) {
			throw new ActionNotAllowedException("This Request already " + loanRequest.getLoanStatus(),
					ErrorCodes.ACTION_ALREDY_DONE);
		}
		loanRequest.setLoanStatus(LoanStatus.Accepted);
		loanRequest = loanRequestRepo.save(loanRequest);
		Integer parentRequestId = loanRequest.getParentRequestId();

		LoanRequest parentRequest = loanRequestRepo.findById(parentRequestId).get();

		User lenderUser = null;
		User borrowerUser = null;
		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			borrowerUser = user;
			lenderUser = userRepo.findById(loanRequest.getUserId()).get();
		} else {
			lenderUser = user;
			borrowerUser = userRepo.findById(loanRequest.getUserId()).get();
		}
		if (loanStatus.equals(LoanStatus.Accepted.toString())) {
			User requestedUser = (user.getPrimaryType() == PrimaryType.BORROWER ? borrowerUser : lenderUser);
			LoanRequestDto loanRequestDto = new LoanRequestDto();
			loanRequestDto.setLoanRequestAmount(loanRequest.getLoanRequestAmount());
			isLimitExceeded(requestedUser.getId(), user, loanRequestDto);
			OxyLoan finalLoan = new OxyLoan();
			finalLoan.setBorrowerUserId(borrowerUser.getId());
			LoanRequest borrowerApplication = loanRequestRepo
					.findByUserIdAndParentRequestIdIsNull(borrowerUser.getId());
			LoanRequest lenderApplication = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(lenderUser.getId());

			loanRequest.setApplication_id(borrowerApplication.getLoanRequestId());
			finalLoan.setApplication_id(borrowerApplication.getLoanRequestId());

			finalLoan.setBorrowerParentRequestId(borrowerApplication.getId());
			finalLoan.setLenderParentRequestId(lenderApplication.getId());
			finalLoan.setLenderUserId(lenderUser.getId());
			finalLoan.setLoanAcceptedDate(new Date());
			finalLoan.setLoanRequestId(parentRequestId);
			finalLoan.setLoanRespondId(loanRequest.getId());
			finalLoan.setDisbursmentAmount(loanRequest.getLoanRequestAmount());
			finalLoan.setLoanId(new Long(System.currentTimeMillis()).toString());
			finalLoan.setDealId(dealId);
			OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
					.findById(dealId).get();
			if (oxyBorrowersDealsInformation != null) {
				finalLoan.setDealType(oxyBorrowersDealsInformation.getDealType().toString());
			}
			finalLoan.setLenderReturnTypeForDeal(oxyLendersAcceptedDeals.getLenderReturnsType().toString());
			String loanId = finalLoan.getLoanId();
			LoanRequest loanRequestObject = loanRequestRepo.findByLoanId(loanId);
			if (loanRequestObject != null) {

				loanRequestObject.setRepaymentMethod(RepaymentMethod.I);

				loanRequestRepo.save(loanRequestObject);
			}
			if (borrowerApplication.getDurationBySir() > 0) {

				finalLoan.setRepaymentMethod(RepaymentMethod.I);

				finalLoan.setRateOfInterest(borrowerApplication.getRateOfInterestToBorrower());
			} else {

				finalLoan.setRepaymentMethod(loanRequest.getRepaymentMethod());
				finalLoan.setRateOfInterest(loanRequest.getRateOfInterest());
			}
			finalLoan.setDuration(loanRequest.getDuration());
			finalLoan.setDurationType(loanRequest.getDurationType());
			finalLoan = oxyLoanRepo.save(finalLoan);
			finalLoan.setLoanId(LOAN_ID_FORMAT.replace("{ID}", finalLoan.getId().toString()));
			finalLoan = oxyLoanRepo.save(finalLoan);
			loanRequest.setLoanId(finalLoan.getLoanId());
			loanIdFormanuvalEsign = finalLoan.getLoanId();
			loanRequestRepo.save(loanRequest);

			TemplateContext templateContext = new TemplateContext();

			PersonalDetails borrwoerPersonalDetails = borrowerUser.getPersonalDetails();
			templateContext.put("borrowerFullName",
					borrwoerPersonalDetails.getFirstName() + " " + borrwoerPersonalDetails.getLastName());
			templateContext.put("borrowerFatherName", borrwoerPersonalDetails.getFatherName());
			templateContext.put("borrowerPan", borrwoerPersonalDetails.getPanNumber());

			String borrowerAddress = borrowerUser.getPersonalDetails().getAddress();
			Pattern pt = Pattern.compile("[^a-zA-Z0-9]");
			Matcher match = pt.matcher(borrowerAddress);
			while (match.find()) {
				String s = match.group();
				borrowerAddress = borrowerAddress.replaceAll("\\" + s, " ");
			}
			templateContext.put("borrowerAddress", borrowerAddress);
			PersonalDetails lenderPersonalDetails = lenderUser.getPersonalDetails();
			templateContext.put("lenderFullName",
					lenderPersonalDetails.getFirstName() + " " + lenderPersonalDetails.getLastName());
			templateContext.put("lenderFatherName", lenderPersonalDetails.getFatherName());
			templateContext.put("lenderPan", lenderPersonalDetails.getPanNumber());

			String lenderAddress = lenderPersonalDetails.getAddress();
			Pattern pt1 = Pattern.compile("[^a-zA-Z0-9]");
			Matcher match1 = pt1.matcher(lenderAddress);
			while (match1.find()) {
				String s = match1.group();
				lenderAddress = lenderAddress.replaceAll("\\" + s, " ");
			}

			templateContext.put("lenderAddress", lenderAddress);
			int borrowerId = finalLoan.getBorrowerUserId();
			LoanRequest loanRequestDetails = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(borrowerId);

			if (loanRequestDetails.getDurationBySir() > 0) {
				if (loanRequestDetails.getDurationType().equals(DurationType.Months)) {
					templateContext.put("duration", loanRequestDetails.getDurationBySir());
					templateContext.put("rateOfInterestOfLender",
							BigDecimal.valueOf(oxyLendersAcceptedDeals.getRateofinterest()).toBigInteger());
					templateContext.put("rateOfInterestOfBorrower",
							BigDecimal.valueOf(loanRequestDetails.getRateOfInterestToBorrower()).toBigInteger());
				} else {
					templateContext.put("duration", loanRequestDetails.getDurationBySir());
					templateContext.put("durationType", loanRequestDetails.getDurationType());
					templateContext.put("rateOfInterestOfLender",
							BigDecimal.valueOf(loanRequestDetails.getRateOfInterestToLender()).toBigInteger());
					templateContext.put("rateOfInterestOfBorrower",
							BigDecimal.valueOf(loanRequestDetails.getRateOfInterestToBorrower()).toBigInteger());
				}
			}
			templateContext.put("loanAmount", BigDecimal.valueOf(finalLoan.getDisbursmentAmount()).toBigInteger());
			templateContext.put("date", expectedDateFormat.format(finalLoan.getLoanAcceptedDate()));
			templateContext.put("loanId", loanRequest.getLoanId());

			double principalAmountPerMonth = finalLoan.getDisbursmentAmount() / loanRequestDetails.getDurationBySir();
			double interestAmountPerMonth = (finalLoan.getDisbursmentAmount()
					* loanRequestDetails.getRateOfInterestToBorrower()) / 100;
			double emiAmountForBorrower = principalAmountPerMonth + interestAmountPerMonth;

			templateContext.put("emiAmountForBorrower", BigDecimal.valueOf(emiAmountForBorrower).toBigInteger());
			DateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

			formatter.setTimeZone(TimeZone.getTimeZone("IST")); // here set timezone

			templateContext.put("agreementDate", formatter.format(new Date()));
			logger.info("agreement date", formatter.format(new Date()));

			Calendar calendar = Calendar.getInstance();
			String heading = "";
			// calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(this.defaultEmiDay));
			double emiAmount = 0.0;
			int count = 0;
			double totalPrincipalAmount = 0.0;
			String emiLastDate = null;

			templateContext.put("heading", heading);
			templateContext.put("totalPrincipalAmount", totalPrincipalAmount);
			templateContext.put("emiLastDate", emiLastDate);

			int duration = 0;
			double rateOfInterestFromLender = 0.0;
			double rateOfInterestFromBorrower = 0.0;
			double disbursmentAmount = finalLoan.getDisbursmentAmount();
			String repaymentTypeForLender = null;
			String repaymentTypeForBorrower = null;
			double platFormEarningRateOfInterest = 0.0;
			if (loanRequestDetails != null) {
				if (loanRequestDetails.getDurationBySir() > 0) {
					if (loanRequestDetails.getDurationType().equals(DurationType.Months)) {
						duration = loanRequestDetails.getDurationBySir();
						rateOfInterestFromLender = oxyLendersAcceptedDeals.getRateofinterest();
						rateOfInterestFromBorrower = loanRequestDetails.getRateOfInterestToBorrower();
						repaymentTypeForLender = finalLoan.getRepaymentMethod().toString();
						repaymentTypeForBorrower = finalLoan.getRepaymentMethod().toString();
						platFormEarningRateOfInterest = rateOfInterestFromBorrower - rateOfInterestFromLender;

					} else {
						duration = loanRequestDetails.getDurationBySir();
						rateOfInterestFromLender = loanRequestDetails.getRateOfInterestToLender();
						rateOfInterestFromBorrower = loanRequestDetails.getRateOfInterestToBorrower();
						repaymentTypeForBorrower = loanRequestDetails.getRepaymentMethodForBorrower();
						repaymentTypeForLender = loanRequestDetails.getRepaymentMethodForLender();
						platFormEarningRateOfInterest = rateOfInterestFromBorrower - rateOfInterestFromLender;

					}
				}
			}
			templateContext.put("platFormEarningRateOfInterest", platFormEarningRateOfInterest);

			logger.info("Lender emi table for agreement start");
			String lenderReturnType = oxyLendersAcceptedDeals.getLenderReturnsType().toString();

			logger.info("Lender emi table for agreement end");

			String accountDetails = null;
			String feeDetails = null;
			for (int i = 0; i <= 3; i++) {
				if (i == 0) {
					User lenderDetails = userRepo.findById(finalLoan.getLenderUserId()).get();
					if (lenderDetails != null) {
						if (lenderDetails.getBankDetails() != null) {
							String name = lenderPersonalDetails.getFirstName() + " "
									+ lenderPersonalDetails.getLastName();
							accountDetails = accountDetails + accountDetailsForAgreement(name,
									lenderDetails.getBankDetails().getBankName(), null, null, null);
						}
					}
					feeDetails = feeDetails + feeDetailsForAgreement("Registration fee", "Rs 0", "Rs 0");

				}
				if (i == 1) {
					User borrowerDetails = userRepo.findById(finalLoan.getBorrowerUserId()).get();
					if (borrowerDetails != null) {
						if (borrowerDetails.getBankDetails() != null) {
							String name = borrwoerPersonalDetails.getFirstName() + " "
									+ borrwoerPersonalDetails.getLastName();
							accountDetails = accountDetails
									+ accountDetailsForAgreement(name, borrowerDetails.getBankDetails().getBankName(),
											borrowerDetails.getBankDetails().getAccountNumber(),
											borrowerDetails.getBankDetails().getBranchName(),
											borrowerDetails.getBankDetails().getIfscCode());
						}
					}

					String fee = "Rs " + BigDecimal.valueOf(borrowerApplication.getLoanOfferedAmount().getBorrowerFee())
							.toBigInteger();

					feeDetails = feeDetails
							+ feeDetailsForAgreement("Service fee (Excl borrower insurance fee)", fee, "Rs 0");

				}
				if (i == 2) {
					String lenderBankName = "SRS Fintech Private Limited – Lenders Funding\r\n" + "Escrow account\r\n"
							+ "";
					accountDetails = accountDetails
							+ accountDetailsForAgreement(lenderBankName, "ICICI", null, null, "ICIC0000106");
					feeDetails = feeDetails + feeDetailsForAgreement("Delayed payment charges",
							"Rs. 500/- + GST for delay in each EMI", "null");

				}
				if (i == 3) {
					String borrowerBankName = "SRS Fintech Private Limited – Borrower’s repayment escrow account";
					accountDetails = accountDetails
							+ accountDetailsForAgreement(borrowerBankName, null, null, null, null);
					feeDetails = feeDetails + feeDetailsForAgreement("Installment Collection and EMI Processing.",
							"null", "of each EMI till loan closure.");

				}

			}
			templateContext.put("accountDetails", accountDetails);
			templateContext.put("feeDetails", feeDetails);

			logger.info("Borrower emi table for agreement end");
			logger.info("Borrower agreement start");

			String outputFileName = finalLoan.getBorrowerUserId() + finalLoan.getLoanId() + ".pdf";
			String generatedPdf = "";
			if (loanRequestDetails != null && loanRequestDetails.getDurationType().equals(DurationType.Months)) {
				generatedPdf = pdfEngine.generatePdf("agreement/agreementTemplate.xml", templateContext,
						outputFileName);
			} else {
				generatedPdf = pdfEngine.generatePdf("agreement/agreementDaysTemplate.xml", templateContext,
						outputFileName);
			}
			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(generatedPdf));
			fileRequest.setFileName(outputFileName);
			fileRequest.setFileType(FileType.Agreement);
			fileRequest.setFilePrifix(FileType.Agreement.name());
			try {
				FileResponse putFile = fileManagementService.putFile(fileRequest);
				logger.info("Agreement file {} uploaded to as {}", outputFileName, putFile.getFileName());
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			File file = new File(generatedPdf);
			boolean fileDeleted = file.delete();
			logger.debug("Deleted temp file {} {}", generatedPdf, fileDeleted);
			logger.info("Borrower agreement end");

			logger.info("Lender agreement start");
			String outputFileNameForLender = finalLoan.getLenderUserId() + finalLoan.getLoanId() + ".pdf";
			String generatedPdfForLender = "";
			if (loanRequestDetails != null && loanRequestDetails.getDurationType().equals(DurationType.Months)) {
				generatedPdfForLender = pdfEngine.generatePdf("agreement/lenderAgreementTemplate.xml", templateContext,
						outputFileNameForLender);
			} else {
				generatedPdf = pdfEngine.generatePdf("agreement/lenderAgreementForDaysTemplate.xml", templateContext,
						outputFileName);
			}
			FileRequest fileRequestForLender = new FileRequest();
			fileRequestForLender.setInputStream(new FileInputStream(generatedPdfForLender));
			fileRequestForLender.setFileName(outputFileNameForLender);
			fileRequestForLender.setFileType(FileType.Agreement);
			fileRequestForLender.setFilePrifix(FileType.Agreement.name());
			try {
				FileResponse putFile = fileManagementService.putFile(fileRequestForLender);
				logger.info("Agreement file {} uploaded to as {}", outputFileNameForLender, putFile.getFileName());
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			File fileForLender = new File(generatedPdfForLender);
			boolean fileDeletedForLender = fileForLender.delete();
			logger.debug("Deleted temp file {} {}", generatedPdfForLender, fileDeletedForLender);
			logger.info("Lender agreement end");
			UserRequest userRequest = new UserRequest();
			userRequest.setAdminComments("APPROVED");
			userRequest.setLoanId("LN" + finalLoan.getId());
			User adminId = userRepo.getAdminDetails();

			UserResponse userResponse = sendReport(adminId.getId(), userRequest);

		}

		String message = "success";
		return message;
	}

	@Override
	@Transactional
	public LoanResponseDto uploadEsignedAgreementPdfforDeal(int userId, Integer loanRequestId,
			UploadAgreementRequestDto agreementFileRequest) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		LoanRequest oxyLoanRequest = loanRequestRepo.findById(loanRequestId).get();
		if (oxyLoanRequest == null) {
			throw new NoSuchElementException("No Loan present to esign against");
		}
		Integer parentRequestId = oxyLoanRequest.getParentRequestId();
		if (parentRequestId == null) {
			throw new ActionNotAllowedException(
					"This loan request " + loanRequestId + " is not having any agreement to esign and upload",
					ErrorCodes.ENITITY_NOT_FOUND);
		}
		OxyLoan finalLoan = oxyLoanRepo.findByLoanRequestIdAndLoanRespondId(parentRequestId, loanRequestId);
		if ((user.getPrimaryType() == PrimaryType.BORROWER && finalLoan.isBorrowerEsigned())
				|| (user.getPrimaryType() == PrimaryType.LENDER && finalLoan.isLenderEsigned())) {
			throw new ActionNotAllowedException("You had esigned this document already", ErrorCodes.ACTION_ALREDY_DONE);
		}
		if (user.getPrimaryType() == PrimaryType.BORROWER && !finalLoan.isLenderEsigned()) {
			throw new ActionNotAllowedException("Lender has to esign first before you esign. Please contact support",
					ErrorCodes.LENDER_NOT_RESPONDED);
		}
		Integer otherUserId = null;
		Integer esignTransactionId = null;
		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			finalLoan.setBorrowerEsigned(true);
			finalLoan.setLenderEsigned(true);
			otherUserId = finalLoan.getLenderUserId();
			esignTransactionId = finalLoan.getBorrowerEsignId();
		} else {
			finalLoan.setLenderEsigned(true);
			otherUserId = finalLoan.getBorrowerUserId();
			esignTransactionId = finalLoan.getLenderEsignId();
		}

		// Download file using the esign api
		EsignFetchResponse downloadEsignedDocument = null;
		EsignTransactions esignTransactions = esignTransactionsRepo.findById(esignTransactionId).get();
		String transactionId = esignTransactions.getTransactionId();
		switch (esignTransactions.getEsignType()) {
		case EsignType.AADHAR:
			downloadEsignedDocument = esignService.downloadEsignedDocument(transactionId);
			break;
		case EsignType.MOBILE:
			boolean otpVerified = mobileUtil.verifyOtp(user.getMobileNumber(), transactionId,
					agreementFileRequest.getOtpValue());
			if (!otpVerified) {
				throw new MobileOtpVerifyFailedException("Invalid OTP value please check.", ErrorCodes.INVALID_OTP);
			}
			break;
		}
		esignTransactions.setTransactonStatus(TransactonStatus.DOWNLOADED);
		esignTransactions.setEsignedDocDownloaded(true);
		if (downloadEsignedDocument != null) {
			if (downloadEsignedDocument.getDocuments() != null) {
				logger.info("Total Number of Docs received {}", downloadEsignedDocument.getDocuments().length);
				logger.info("Doc Info {}", downloadEsignedDocument.getDocuments()[0]);
			}
			BufferedInputStream inputStream;
			try {
				inputStream = new BufferedInputStream(
						new URL(downloadEsignedDocument.getDocuments()[0].getDynamic_url()).openStream());
			} catch (IOException e1) {
				throw new RuntimeException("Error while downloading esigned documnet", e1);
			}

			String outputFileName = finalLoan.getLoanId() + ".pdf";
			String originalFileName = outputFileName;
			FileRequest agreementFile = new FileRequest();
			agreementFile.setFileName(outputFileName);
			agreementFile.setFileType(FileType.Agreement);
			agreementFile.setFilePrifix(FileType.Agreement.name());

			agreementFile.setInputStream(inputStream);
			try {
				FileResponse putFile = fileManagementService.putFile(agreementFile);
				logger.info("Esigned Agreement file {} uploaded to as {}", originalFileName, putFile.getFileName());
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		} else if (esignTransactions.getEsignType() == EsignType.AADHAR) {
			logger.error("There was some issue in downloading the file");
			esignTransactions.setTransactonStatus(TransactonStatus.FAILED);
			esignTransactions.setEsignedDocDownloaded(false);
		}

		if (finalLoan.isBorrowerEsigned() && finalLoan.isLenderEsigned()) {
			finalLoan.setLoanStatus(LoanStatus.Active);
			LoanRequest esigningUserRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
			LoanRequest otherUserRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(otherUserId);
			esigningUserRequest.setDisbursmentAmount(
					esigningUserRequest.getDisbursmentAmount() + finalLoan.getDisbursmentAmount());
			otherUserRequest
					.setDisbursmentAmount(otherUserRequest.getDisbursmentAmount() + finalLoan.getDisbursmentAmount());
			esigningUserRequest = loanRequestRepo.save(esigningUserRequest);
			otherUserRequest = loanRequestRepo.save(otherUserRequest);
			logger.info(finalLoan.getLenderUserId());
			logger.info(finalLoan.getBorrowerUserId());

			lenderBorrowerConversationRepo.deleteByLenderUserIdAndBorrowerUserId(finalLoan.getLenderUserId(),
					finalLoan.getBorrowerUserId());
			if (finalLoan.getDurationType().equals(DurationType.Months)) {
				oxyLoanRequest.setAdminComments("APPROVED");

				DealLevelRequestDto dealLevelRequestDto = new DealLevelRequestDto();

				int dealId = finalLoan.getDealId();
				generateEmiCardForPartners(finalLoan, oxyLoanRequest, dealId, dealLevelRequestDto);
			} else {
				generateEmiCardForDays(finalLoan, oxyLoanRequest);
			}

			generateENACHMandateForPartners(finalLoan);

		}
		finalLoan = oxyLoanRepo.save(finalLoan);
		esignTransactions = esignTransactionsRepo.save(esignTransactions);
		String loanId = oxyLoanRequest.getLoanId();
		OxyLoan oxyLoan = oxyLoanRepo.findByLoanId(loanId);
		if (oxyLoan != null) {
			int borrowerParentRequestId = oxyLoan.getBorrowerParentRequestId();
			int agreedLoansCount = oxyLoanRepo.getAgreedLoansCount(borrowerParentRequestId);
			if (agreedLoansCount == 0) {
				String mailsubject = "BorrowerDealStructure"; // Mail Goes to Borrower so subject is like this..
				String emailTemplateName = "agreement.template";

				int userId1 = finalLoan.getBorrowerUserId();
				KycFileResponse kycFileResponse = downloadLoanAgrement(userId1, loanRequestId);
				TemplateContext context = new TemplateContext();
				if (kycFileResponse != null) {
					context.put("agreement", kycFileResponse.getDownloadUrl());
				}

				int id = 1;
				KycFileResponse kycFileResponseOfRepayment = null;
				try {
					kycFileResponseOfRepayment = userServiceImpl.downloadFile(id, KycType.REPAYMENTACCOUNTDETAILS);
				} catch (IOException e) {

					e.printStackTrace();
				}

				if (kycFileResponseOfRepayment != null) {
					String urlDownload = kycFileResponseOfRepayment.getDownloadUrl();
					String[] splitingTheUrl = urlDownload.split(Pattern.quote("?"));
					String url = splitingTheUrl[0];
					context.put("repaymentAccountDetails", url);
				}
				context.put("name",
						user.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase()
								+ user.getPersonalDetails().getFirstName().substring(1) + " "
								+ user.getPersonalDetails().getLastName().substring(0, 1).toUpperCase()
								+ user.getPersonalDetails().getLastName().substring(1));
				context.put("borrowerId", user.getUniqueNumber());
				LoanRequest loanRequestValues = loanRequestRepo.findById(borrowerParentRequestId).get();
				List<String> listOfLoanIds = oxyLoanRepo.getLoanIdsBasedOnApplicationId(borrowerParentRequestId);
				String loanIds = " ";
				for (int i = 0; i < listOfLoanIds.size(); i++) {
					loanIds = loanIds + listOfLoanIds.get(i) + ",";
				}
				Date date = new Date();
				String date1 = expectedDateFormat.format(date);
				context.put("loanIds", loanIds);
				context.put("loanAmount", loanRequestValues.getLoanOfferedAmount().getLoanOfferedAmount());
				context.put("processingFree", loanRequestValues.getLoanOfferedAmount().getBorrowerFee());
				context.put("netDisburmentAmount", loanRequestValues.getLoanOfferedAmount().getNetDisbursementAmount());
				context.put("duration", loanRequestValues.getLoanOfferedAmount().getDuration());
				context.put("emi", loanRequestValues.getLoanOfferedAmount().getEmiAmount());
				LoanEmiCard firstEmiInfo = loanEmiCardRepo.getFirstEmiDetails(oxyLoan.getId());
				int emiNumber = loanRequestValues.getLoanOfferedAmount().getDuration();
				LoanEmiCard lastEmiInfo = loanEmiCardRepo.getLastEmiDetails(oxyLoan.getId(), emiNumber);
				if (firstEmiInfo != null) {
					context.put("emiStartDate", expectedDateFormat.format(firstEmiInfo.getEmiDueOn()));
				}
				if (lastEmiInfo != null) {
					context.put("emiEndDate", expectedDateFormat.format(lastEmiInfo.getEmiDueOn()));
				}
				context.put("bankAccount", user.getBankDetails().getAccountNumber());
				context.put("ifsc", user.getBankDetails().getIfscCode());
				context.put("address", user.getPersonalDetails().getAddress());
				context.put("email", user.getEmail());
				context.put("mobileNumber", user.getMobileNumber());
				context.put("currentDate", date1);

				EmailRequest emailRequest = new EmailRequest(new String[] { "archana.n@oxyloans.com",
						"narendra@oxyloans.com", "ramadevi@oxyloans.com", "subbu@oxyloans.com" }, mailsubject,
						emailTemplateName, context);
				EmailResponse emailResponse = emailService.sendEmail(emailRequest);
				if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
					throw new RuntimeException(emailResponse.getErrorMessage());
				}
			}
		}
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setUserId(oxyLoanRequest.getUserId());
		loanResponseDto.setLoanRequestId(oxyLoanRequest.getId());
		loanResponseDto.setLoanRequest(oxyLoanRequest.getLoanRequestId());
		return loanResponseDto;
	}

	public double generateEmiCardForPartners(OxyLoan oxyLoan, LoanRequest loanRequest, int dealId,
			DealLevelRequestDto dealLevelRequestDto) {
		List<LoanEmiCard> loanEmiCards = new ArrayList<LoanEmiCard>();

		Calendar calendar = Calendar.getInstance();

		double emiPrincipalAmount = 0d;
		double emiInterstAmount = 0d;

		int quartlyValue = 0;
		int halflyValue = 0;
		int yearlyValue = 0;
		int endofdealValue = 0;

		double interstAmountForLender = 0.0;
		double principalAmountForLender = 0.0;
		double emiAmountForLender = 0.0;

		calendar.add(Calendar.DAY_OF_MONTH, -1);

		for (int i = 1; i <= loanRequest.getDuration(); i++) {

			logger.info(calendar.getTime());
			LoanEmiCard loanEmiCard = new LoanEmiCard();
			loanEmiCard.setLoanId(oxyLoan.getId());
			loanEmiCard.setEmiNumber(i);

			loanEmiCard.setEmiDueOn(calendar.getTime());

			logger.info(calendar.getTime());

			if (loanRequest.getDuration().intValue() == i && loanRequest.getRepaymentMethod() == RepaymentMethod.I) {
				loanEmiCard.setEmiPrincipalAmount(oxyLoan.getDisbursmentAmount());
			}
			int borrowerParentid = oxyLoan.getBorrowerParentRequestId();
			LoanRequest loanRequestDetails = loanRequestRepo.findById(borrowerParentid).get();
			if (loanRequestDetails != null) {

				if (loanRequestDetails.getRepaymentMethodForBorrower().equalsIgnoreCase("PI")) {
					double interestAmount = oxyLoan.getDisbursmentAmount()
							* (loanRequestDetails.getRateOfInterestToBorrower() / 12) / 100;
					double principalAmount = oxyLoan.getDisbursmentAmount() / oxyLoan.getDuration();
					loanEmiCard.setEmiInterstAmount(Math.round(interestAmount));
					loanEmiCard.setEmiPrincipalAmount(Math.round(principalAmount));
					loanEmiCard.setEmiAmount(Math.round(interestAmount + principalAmount));

				} else {
					double interestAmountOfI = oxyLoan.getDisbursmentAmount()
							* (loanRequestDetails.getRateOfInterestToBorrower() / 12) / 100;
					double principalAmountOfI = 0.0;
					if (i == loanRequestDetails.getDurationBySir()) {
						principalAmountOfI = oxyLoan.getDisbursmentAmount();
					}
					loanEmiCard.setEmiInterstAmount(Math.round(interestAmountOfI));
					loanEmiCard.setEmiPrincipalAmount(Math.round(principalAmountOfI));
					loanEmiCard.setEmiAmount(Math.round(interestAmountOfI + principalAmountOfI));

				}

			}

			calendar.add(Calendar.MONTH, 1);
			loanEmiCards.add(loanEmiCard);

		}

		loanEmiCardRepo.saveAll(loanEmiCards);
		return emiPrincipalAmount + emiInterstAmount;

	}

	public void generateENACHMandateForPartners(OxyLoan loanDetails) {
		double maxAmount = 0;
		double totalAmount = 0;
		Date startDate = null;
		Date endDate = null;
		double finalAmount = 0;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		Set<Integer> loanIds = new HashSet<Integer>();
		String loanId = loanDetails.getLoanId();
		StringBuilder converting = new StringBuilder(loanId);
		String convertedLoanId = converting.delete(0, 2).toString();

		System.out.println(convertedLoanId);
		List<LoanEmiCard> listLoanEmicard = loanEmiCardRepo
				.findByLoanIdOrderByEmiNumber(Integer.parseInt(convertedLoanId));

		for (LoanEmiCard emis : listLoanEmicard) {

			if (emis.getEmiNumber() == 1) {
				maxAmount = emis.getEmiAmount();
			}
			if (emis.getEmiNumber() == 1) {
				startDate = emis.getEmiDueOn();
			}
			if (emis.getEmiNumber() == listLoanEmicard.size()) {
				endDate = emis.getEmiDueOn();
				finalAmount = emis.getEmiPrincipalAmount();
			}
		}
		totalAmount = loanDetails.getDisbursmentAmount();
		String txnId = "ECST" + dateUtil.getHHmmMMddyyyyDateFormat() + loanDetails.getBorrowerUserId()
				+ loanDetails.getId();// Need to change
		// the format

		OxyLoan oxyLoanDetails = oxyLoanRepo.findByLoanId(loanId);

		if (oxyLoanDetails.getDurationType().equals(DurationType.Months)) {
			Date endDate3 = null;

			List<EnachMandate> mandateList = new ArrayList<>();

			int size = 0;
			LoanRequest loanRequest = loanRequestRepo
					.findByUserIdAndParentRequestIdIsNull(loanDetails.getBorrowerUserId());

			if (loanRequest != null) {

				if (loanRequest.getRepaymentMethodForBorrower().equalsIgnoreCase(RepaymentMethod.PI.name())) {
					size = 1;
				} else if (loanRequest.getRepaymentMethodForBorrower().equalsIgnoreCase(RepaymentMethod.I.name())) {
					size = 2;
				}
			}

			for (int i = 1; i <= size; i++) {
				EnachMandate enachMandate = new EnachMandate();

				enachMandate.setOxyLoanId(oxyLoanDetails.getId());
				enachMandate.setOxyLoan(oxyLoanDetails);

				enachMandate.setAmountType(AmountType.valueOf(EnachMandate.AmountType.M.name()));
				enachMandate.setFrequency(Frequency.valueOf(EnachMandate.Frequency.MNTH.name()));

				enachMandate.setDebitStartDate(startDate);

				enachMandate.setDebitEndDate(endDate);
				enachMandate.setMandateTransactionId(txnId + i);
				enachMandate.setMaxAmount(maxAmount);
				if (i == 2) {
					enachMandate.setMaxAmount(finalAmount);
					enachMandate.setDebitStartDate(endDate);
					enachMandate.setDebitEndDate(endDate);

				}
				enachMandate.setMandateStatus(MandateStatus.INITIATED);

				logger.info(enachMandate.toString());

				mandateList.add(enachMandate);
			}

			logger.info(mandateList);

			enachMandateRepo.saveAll(mandateList);

			String mailSubjectForAdmin = "Enach"; // Mail Goes to admin
			String mailTempalteForAdmin = "enach-mail-to-admin.template";

			TemplateContext templateContext = new TemplateContext();
			templateContext.put("LoanId", oxyLoanDetails.getId());

			templateContext.put("AmountType", AmountType.valueOf(EnachMandate.AmountType.M.name()));
			templateContext.put("Frequeny", Frequency.valueOf(EnachMandate.Frequency.MNTH.name()));

			templateContext.put("EmiStartDate", expectedDateFormat.format(startDate));
			templateContext.put("EmiEndDate", expectedDateFormat.format(endDate));

			templateContext.put("TransactionId", txnId);
			templateContext.put("MaxAmount", Math.round(maxAmount));
			templateContext.put("MandateStatus", MandateStatus.INITIATED);
			Date date = new Date();
			String date1 = expectedDateFormat.format(date);
			templateContext.put("CurrentDate", date1);

			EmailRequest emailRequest = new EmailRequest(new String[] { this.supportEmail }, mailSubjectForAdmin,
					mailTempalteForAdmin, templateContext);
			EmailResponse emailResponse = emailService.sendEmail(emailRequest);
			if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
				throw new RuntimeException(emailResponse.getErrorMessage());
			}

		} else if (oxyLoanDetails.getDurationType().equals(DurationType.Days)) {
			Date endDate2 = null;

			List<EnachMandate> mandateList = new ArrayList<>();

			for (int i = 1; i <= 2; i++) {
				EnachMandate enachMandate1 = new EnachMandate();
				enachMandate1.setOxyLoanId(oxyLoanDetails.getId());
				enachMandate1.setOxyLoan(oxyLoanDetails);

				enachMandate1.setAmountType(AmountType.valueOf(EnachMandate.AmountType.M.name()));
				enachMandate1.setFrequency(Frequency.valueOf(EnachMandate.Frequency.DAIL.name()));

				enachMandate1.setMandateTransactionId(txnId + Integer.toString(i));
				enachMandate1.setDebitStartDate(startDate);
				enachMandate1.setDebitEndDate(endDate);

				enachMandate1.setMaxAmount(maxAmount);
				if (i == 2) {
					enachMandate1.setMaxAmount(finalAmount);
					enachMandate1.setDebitStartDate(endDate);
					enachMandate1.setDebitEndDate(endDate);

				}

				enachMandate1.setMandateStatus(MandateStatus.INITIATED);
				mandateList.add(enachMandate1);
			}
			logger.info("asdaa " + mandateList);
			enachMandateRepo.saveAll(mandateList);
		}

	}

	public String accountDetailsForAgreement(String userName, String bankName, String accountNumber, String branch,
			String ifsc) {
		String rowdata = "<fo:table-row>\r\n" + " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"4px\">\r\n" + " <fo:block text-align=\"left\">"

				+ userName + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">"

				+ bankName + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">"

				+ accountNumber + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">"

				+ branch + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">" + ifsc + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " </fo:table-row>";

		return rowdata;

	}

	public String feeDetailsForAgreement(String type, String amountForBorrower, String amountForLender) {
		String rowdata = "<fo:table-row>\r\n" + " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"4px\">\r\n" + " <fo:block text-align=\"left\">"

				+ type + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">"

				+ amountForBorrower + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">" + amountForLender + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " </fo:table-row>";

		return rowdata;

	}

	@Override
	@Transactional
	public LoanResponseDto uploadEsignedAgreementPdfforDealInApplicationLevel(int userId, Integer applicationId,
			UploadAgreementRequestDto agreementFileRequest) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		LoanRequest oxyLoanRequest = loanRequestRepo.findById(applicationId).get();
		if (oxyLoanRequest == null) {
			throw new NoSuchElementException("No Loan present to esign against");
		}

		String appId = "APBR" + applicationId;
		List<OxyLoan> finalLoan = oxyLoanRepo.findByLoanRequestIdAndLoanRespondIdAndApplicationId(appId);

		Integer otherUserId = null;
		Integer esignTransactionId = null;

		if (finalLoan != null && !finalLoan.isEmpty()) {
			for (OxyLoan oxyLoan : finalLoan) {
				if (user.getPrimaryType() == PrimaryType.BORROWER) {
					oxyLoan.setBorrowerEsigned(true);
					oxyLoan.setLenderEsigned(true);
					otherUserId = oxyLoan.getLenderUserId();
					esignTransactionId = oxyLoan.getBorrowerEsignId();
				} else {
					oxyLoan.setLenderEsigned(true);
					otherUserId = oxyLoan.getBorrowerUserId();
					esignTransactionId = oxyLoan.getLenderEsignId();
				}

				oxyLoan = oxyLoanRepo.save(oxyLoan);

				if (oxyLoan.isBorrowerEsigned() && oxyLoan.isLenderEsigned()) {
					oxyLoan.setLoanStatus(LoanStatus.Active);
					oxyLoanRepo.save(oxyLoan);
					/*
					 * LoanRequest esigningUserRequest =
					 * loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId); LoanRequest
					 * otherUserRequest =
					 * loanRequestRepo.findByUserIdAndParentRequestIdIsNull(otherUserId);
					 * esigningUserRequest.setDisbursmentAmount(
					 * esigningUserRequest.getDisbursmentAmount() + oxyLoan.getDisbursmentAmount());
					 * otherUserRequest.setDisbursmentAmount(
					 * otherUserRequest.getDisbursmentAmount() + oxyLoan.getDisbursmentAmount());
					 * esigningUserRequest = loanRequestRepo.save(esigningUserRequest);
					 * otherUserRequest = loanRequestRepo.save(otherUserRequest);
					 */
					logger.info(oxyLoan.getLenderUserId());
					logger.info(oxyLoan.getBorrowerUserId());

					lenderBorrowerConversationRepo.deleteByLenderUserIdAndBorrowerUserId(oxyLoan.getLenderUserId(),
							oxyLoan.getBorrowerUserId());
					if (oxyLoan.getDurationType().equals(DurationType.Months)) {
						oxyLoanRequest.setAdminComments("APPROVED");
						loanRequestRepo.save(oxyLoanRequest);

					}

				}

				if (oxyLoanRequest.getLoanOfferedAmount().getLoanOfferedAmount() < 50000) {
					generateEmiCard(oxyLoan, oxyLoanRequest);
					generateENACHMandate(oxyLoan);
				}
			}

			LoanRequest loanForBorrower = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);

			if (loanForBorrower != null) {
				Double disbursmentAmount = loanRequestRepo.findingSumOfDisbursmentAmount(userId, applicationId);
				if (disbursmentAmount != null) {
					loanForBorrower.setDisbursmentAmount(disbursmentAmount);
					loanRequestRepo.save(loanForBorrower);
				}
			}
		}

		String otpValueForEsign = "";

		EsignFetchResponse downloadEsignedDocument = null;
		EsignTransactions esignTransactions = esignTransactionsRepo.findById(esignTransactionId).get();
		String transactionId = esignTransactions.getTransactionId();
		switch (esignTransactions.getEsignType()) {
		case EsignType.AADHAR:
			downloadEsignedDocument = esignService.downloadEsignedDocument(transactionId);
			break;
		case EsignType.MOBILE:
			boolean otpVerified = mobileUtil.verifyOtp(user.getMobileNumber(), transactionId,
					agreementFileRequest.getOtpValue());
			if (!otpVerified) {
				throw new MobileOtpVerifyFailedException("Invalid OTP value please check.", ErrorCodes.INVALID_OTP);
			}

			interestCalculationForApplicationLevel(user.getId());
			otpValueForEsign = agreementFileRequest.getOtpValue();
			break;
		}
		esignTransactions.setTransactonStatus(TransactonStatus.DOWNLOADED);
		esignTransactions.setEsignedDocDownloaded(true);

		esignTransactions = esignTransactionsRepo.save(esignTransactions);

		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setUserId(oxyLoanRequest.getUserId());
		loanResponseDto.setLoanRequestId(oxyLoanRequest.getId());
		loanResponseDto.setLoanRequest(oxyLoanRequest.getLoanRequestId());
		return loanResponseDto;
	}

	public void interestCalculationForApplicationLevel(int userId) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		if (user.getPrimaryType() != PrimaryType.BORROWER) {
			throw new OperationNotAllowedException(
					"As a " + user.getPrimaryType() + " primary type not allowed to perform this",
					ErrorCodes.INVALID_OPERATION);
		}

		if (user != null) {

			LoanRequest loanRequest = oxyLoanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);

			if (loanRequest.getLoanOfferedAmount().getLoanOfferdStatus() == LoanOfferdStatus.LOANOFFERACCEPTED) {

				if (loanRequest.getLoanOfferedAmount().getLoanOfferedAmount() >= 50000
						&& loanRequest.getLoanOfferedAmount().getLoanOfferedAmount() <= 1000000) {

					Double emiAmount = 0.0;
					Double emiPrincialAmount = 0.0;
					Double interestAmount = 0.0;
					if (loanRequest.getDurationType().equals(DurationType.Months)) {
						if (loanRequest.getRepaymentMethodForBorrower().equalsIgnoreCase("PI")) {
							emiPrincialAmount = loanRequest.getLoanOfferedAmount().getLoanOfferedAmount()
									/ loanRequest.getDurationBySir().doubleValue();
							interestAmount = (loanRequest.getLoanOfferedAmount().getLoanOfferedAmount()
									* loanRequest.getRateOfInterestToBorrower()) / 100d;

							emiAmount = emiPrincialAmount + interestAmount;
						} else {
							emiPrincialAmount = loanRequest.getLoanOfferedAmount().getLoanOfferedAmount()
									/ loanRequest.getDurationBySir().doubleValue();

							interestAmount = (loanRequest.getLoanOfferedAmount().getLoanOfferedAmount()
									* loanRequest.getRateOfInterestToBorrower()) / 100d;

							emiAmount = interestAmount;
						}

					}

					String applicationID = loanRequest.getLoanRequestId();

					String loanRequestId = applicationID.substring(4, applicationID.length());

					Integer applicationId = Integer.parseInt(loanRequestId);

					Date enachStartDate = null;
					Date enachEndDate = null;
					ApplicationLevelLoanEmiCard applicationLevelLoanEmiCard = applicationLevelLoanEmiCardRepo
							.findByApplicationId(applicationId);

					List<ApplicationLevelLoanEmiCard> loanEmiCards = new ArrayList<ApplicationLevelLoanEmiCard>();
					if (applicationLevelLoanEmiCard == null) {
						Calendar calendar = Calendar.getInstance();
						calendar.add(Calendar.MONTH, 1);
						calendar.set(Calendar.DATE, 5);
						if (loanRequest.getRepaymentMethodForBorrower().equalsIgnoreCase("PI")) {
							for (int i = 1; i <= loanRequest.getDurationBySir(); i++) {

								Date date = calendar.getTime();
								String formattedDate = dateFormat1.format(date);

								ApplicationLevelLoanEmiCard loanEmiCard = new ApplicationLevelLoanEmiCard();

								loanEmiCard.setBorrowerId(userId);
								loanEmiCard.setApplicationId(applicationId);
								loanEmiCard.setEmiAmount(emiAmount);
								loanEmiCard.setEmiNumber(i);
								loanEmiCard.setInterstAmount(interestAmount);
								loanEmiCard.setPrincipalAmount(emiPrincialAmount);
								try {
									loanEmiCard.setEmiDueOn(dateFormat1.parse(formattedDate));
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								calendar.add(Calendar.MONTH, 1);

								loanEmiCards.add(loanEmiCard);

							}
						} else {
							for (int i = 1; i <= loanRequest.getDurationBySir() + 1; i++) {
								ApplicationLevelLoanEmiCard loanEmiCard = new ApplicationLevelLoanEmiCard();

								Date date = calendar.getTime();
								String formattedDate = dateFormat1.format(date);

								loanEmiCard.setBorrowerId(userId);
								loanEmiCard.setApplicationId(applicationId);
								try {
									loanEmiCard.setEmiDueOn(dateFormat1.parse(formattedDate));
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								if (i <= loanRequest.getDurationBySir()) {
									loanEmiCard.setInterstAmount(interestAmount);
									loanEmiCard.setPrincipalAmount(0);
									loanEmiCard.setEmiAmount(interestAmount);
								} else if (i == loanRequest.getDurationBySir()) {
									loanEmiCard.setInterstAmount(0);
									loanEmiCard.setPrincipalAmount(emiPrincialAmount);
									loanEmiCard.setEmiAmount(emiAmount);
								}

								if (i == loanRequest.getDurationBySir() + 1) {
									loanEmiCard.setInterstAmount(0);
									loanEmiCard.setPrincipalAmount(
											loanRequest.getLoanOfferedAmount().getLoanOfferedAmount());
									loanEmiCard.setEmiAmount(loanRequest.getLoanOfferedAmount().getLoanOfferedAmount());
								}

								loanEmiCard.setEmiNumber(i);

								if (i < loanRequest.getDurationBySir()) {
									calendar.add(Calendar.MONTH, 1);
								}

								loanEmiCards.add(loanEmiCard);

							}

						}

						applicationLevelLoanEmiCardRepo.saveAll(loanEmiCards);

					}

					List<ApplicationLevelLoanEmiCard> listOfEmiNumbers = applicationLevelLoanEmiCardRepo
							.findByApplicationIdOrderByEmiNumber(applicationId);

					for (ApplicationLevelLoanEmiCard emis : listOfEmiNumbers) {

						if (emis.getEmiNumber() == 1) {
							enachStartDate = emis.getEmiDueOn();
						}
						if (emis.getEmiNumber() == listOfEmiNumbers.size()) {
							enachEndDate = emis.getEmiDueOn();

						}
					}
					String txnId = "ECST" + dateUtil.getHHmmMMddyyyyDateFormat() + loanRequest.getUserId()
							+ loanRequest.getId();

					Date enachEndDateForPrincipal = enachEndDate;

					Calendar c = Calendar.getInstance();
					c.setTime(enachEndDateForPrincipal);
					c.add(Calendar.DATE, 1);
					enachEndDateForPrincipal = c.getTime();

					Date enachEndDateForPrincipalNew = enachEndDate;

					Calendar c1 = Calendar.getInstance();
					c1.setTime(enachEndDateForPrincipalNew);
					c1.add(Calendar.DATE, 2);
					enachEndDateForPrincipalNew = c1.getTime();

					ApplicationLevelEnachMandate applicationLevelEnachMandate = applicationLevelEnachMandateRepo
							.findByApplicationId(applicationId);
					List<ApplicationLevelEnachMandate> listOfEnachMandate = new ArrayList<ApplicationLevelEnachMandate>();

					if (applicationLevelEnachMandate == null) {
						if (loanRequest.getRepaymentMethodForBorrower().equalsIgnoreCase("PI")) {
							ApplicationLevelEnachMandate enachMandate = new ApplicationLevelEnachMandate();

							enachMandate.setApplicationId(applicationId);

							emiPrincialAmount = loanRequest.getLoanOfferedAmount().getLoanOfferedAmount()
									/ loanRequest.getDurationBySir().doubleValue();
							interestAmount = (loanRequest.getLoanOfferedAmount().getLoanOfferedAmount()
									* loanRequest.getRateOfInterestToBorrower()) / 100d;

							emiAmount = emiPrincialAmount + interestAmount;
							enachMandate.setMaxAmount(emiAmount);

							enachMandate.setIsPrincialOrInterest("PI");
							enachMandate.setDebitStartDate(enachStartDate);
							enachMandate.setDebitEndDate(enachEndDate);
							enachMandate.setMandateTransactionId(txnId);
							enachMandate.setMandateStatus(ApplicationLevelEnachMandate.MandateStatus.INITIATED);
							enachMandate.setFrequency(ApplicationLevelEnachMandate.Frequency.MNTH);

							applicationLevelEnachMandateRepo.save(enachMandate);
						} else {

							for (int i = 1; i <= 2; i++) {
								ApplicationLevelEnachMandate applicationEnachMandate = new ApplicationLevelEnachMandate();

								applicationEnachMandate.setApplicationId(applicationId);
								if (i == 1) {
									applicationEnachMandate.setApplicationId(applicationId);
									applicationEnachMandate.setIsPrincialOrInterest("P");

									emiPrincialAmount = loanRequest.getLoanOfferedAmount().getLoanOfferedAmount()
											/ loanRequest.getDurationBySir().doubleValue();
									applicationEnachMandate
											.setMaxAmount(loanRequest.getLoanOfferedAmount().getLoanOfferedAmount());

									applicationEnachMandate.setDebitStartDate(enachEndDateForPrincipal);
									applicationEnachMandate.setDebitEndDate(enachEndDateForPrincipalNew);
									applicationEnachMandate.setMandateTransactionId(txnId + i);
									applicationEnachMandate
											.setMandateStatus(ApplicationLevelEnachMandate.MandateStatus.INITIATED);
									applicationEnachMandate.setFrequency(ApplicationLevelEnachMandate.Frequency.DAIL);

									Integer duration = loanRequest.getDurationBySir() + 1;
									ApplicationLevelLoanEmiCard loanEmiInfo = applicationLevelLoanEmiCardRepo
											.findingLastEmiInfo(duration, applicationId);
									if (loanEmiInfo != null) {
										loanEmiInfo.setEmiDueOn(enachEndDateForPrincipal);
										applicationLevelLoanEmiCardRepo.save(loanEmiInfo);

									}

								} else if (i == 2) {
									applicationEnachMandate.setApplicationId(applicationId);
									applicationEnachMandate.setIsPrincialOrInterest("I");

									interestAmount = (loanRequest.getLoanOfferedAmount().getLoanOfferedAmount()
											* loanRequest.getRateOfInterestToBorrower()) / 100d;

									applicationEnachMandate.setMaxAmount(interestAmount);
									applicationEnachMandate.setDebitStartDate(enachStartDate);
									applicationEnachMandate.setDebitEndDate(enachEndDate);
									applicationEnachMandate.setMandateTransactionId(txnId + i);
									applicationEnachMandate
											.setMandateStatus(ApplicationLevelEnachMandate.MandateStatus.INITIATED);
									applicationEnachMandate.setFrequency(ApplicationLevelEnachMandate.Frequency.MNTH);
								}

								listOfEnachMandate.add(applicationEnachMandate);
							}

							applicationLevelEnachMandateRepo.saveAll(listOfEnachMandate);
						}

					}
				}
			}

		}

	}

	@Override
	public String updatingAmountBasedOnDisbursment(Integer applicationId,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto) {

		if (loanEmiCardPaymentDetailsRequestDto.getApplicationIdNew() != null
				&& loanEmiCardPaymentDetailsRequestDto.getApplicationIdNew() != 0) {

			String appId = "APBR" + applicationId;

			List<OxyLoan> listOfLenders = oxyLoanRepo.findingListOfLoansByAppId(appId);

			Double sumOfAmountDisbursed = oxyLoanRepo.findingSumOfAmountDisbursedBasedOnAppId(appId);
			if (listOfLenders != null && !listOfLenders.isEmpty()) {

				for (OxyLoan loan : listOfLenders) {

					AmountUpdationBasedOnDisbursment amountData = amountUpdationBasedOnDisbursmentRepo
							.findingUsersBasedOnAppIdEmiId(applicationId,
									loanEmiCardPaymentDetailsRequestDto.getEmiId(), loan.getLenderUserId());

					if (amountData == null) {
						AmountUpdationBasedOnDisbursment amountUpdationBasedOnDisbursment = new AmountUpdationBasedOnDisbursment();

						Double amount = (loan.getDisbursmentAmount()
								* loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount()) / sumOfAmountDisbursed;

						amountUpdationBasedOnDisbursment.setAmount(amount);

						amountUpdationBasedOnDisbursment.setApplicationId(applicationId);
						amountUpdationBasedOnDisbursment.setEmiNumber(loanEmiCardPaymentDetailsRequestDto.getEmiId());
						amountUpdationBasedOnDisbursment.setUserId(loan.getLenderUserId());

						amountUpdationBasedOnDisbursmentRepo.save(amountUpdationBasedOnDisbursment);
					}

				}

			}

			ApplicationLevelLoanEmiCard loanEmiCard = applicationLevelLoanEmiCardRepo
					.findingEmiInfoBasedOnEmiNumberAndAppId(applicationId,
							loanEmiCardPaymentDetailsRequestDto.getEmiId());
			if (loanEmiCard != null) {
				loanEmiCard.setStatus(ApplicationLevelLoanEmiCard.Status.Paid);

				if (loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate() != null) {

					try {
						loanEmiCard.setEmiPaidOn(
								expectedDateFormat.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				loanEmiCard.setComments("From Enach");
				applicationLevelLoanEmiCardRepo.save(loanEmiCard);
			}

			List<AmountUpdationBasedOnDisbursment> listOfAmountToLenders = amountUpdationBasedOnDisbursmentRepo
					.listOfDetailsBasedOnEmiNumberAndApplicationId(applicationId,
							loanEmiCardPaymentDetailsRequestDto.getEmiId());

			String fileToFolder = generatingFileForEmiAmount(listOfAmountToLenders);

		}
		return "APBR" + applicationId;

	}

	public String generatingFileForEmiAmount(List<AmountUpdationBasedOnDisbursment> dto) {

		FileOutputStream outFile = null;

		Workbook workBook = new HSSFWorkbook();

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");

		StringBuilder sbf = new StringBuilder("OXYLR1_OXYLR1UPLD_");

		String requiredDate = formatter.format(new Date());

		sbf = sbf.append(requiredDate + "EmiAmount");

		org.apache.poi.ss.usermodel.Sheet spreadSheet = workBook.createSheet(sbf.toString());

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();// Create font
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;
		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}
		int countForSno = 0;

		for (AmountUpdationBasedOnDisbursment obj : dto) {

			Row row = spreadSheet.createRow(++rowCount);

			User lender = userRepo.findByIdNum(obj.getUserId());

			Cell cell1 = row.createCell(0);
			cell1.setCellValue("777705849442");

			cell1 = row.createCell(1);
			cell1.setCellValue(lender.getBankDetails().getAccountNumber() == null ? ""
					: lender.getBankDetails().getAccountNumber());

			cell1 = row.createCell(2);
			cell1.setCellValue(
					lender.getBankDetails().getUserName() == null ? "" : lender.getBankDetails().getUserName());

			cell1 = row.createCell(3);
			cell1.setCellValue(obj.getAmount());

			cell1 = row.createCell(4);
			cell1.setCellValue("N");

			SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");

			String modifiedDate = format.format(new Date());

			cell1 = row.createCell(5);
			cell1.setCellValue(modifiedDate);

			cell1 = row.createCell(6);
			cell1.setCellValue(
					lender.getBankDetails().getIfscCode() == null ? "" : lender.getBankDetails().getIfscCode());

			cell1 = row.createCell(16);
			cell1.setCellValue("LENDEREMIAMOUNT");

			cell1 = row.createCell(17);
			cell1.setCellValue(obj.getUserId());

			cell1 = row.createCell(18);
			cell1.setCellValue(obj.getApplicationId());

			cell1 = row.createCell(19);
			cell1.setCellValue(obj.getEmiNumber());

			cell1 = row.createCell(20);
			cell1.setCellValue("EMI AMOUNT");

			countForSno = countForSno + 1;

		}

		String fileName = sbf.toString() + ".xls";
		File f = new File(toSaveReferralBonus + fileName);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);

			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate = expectedDateFormat.format(current.getTime());

			templateContext.put("currentDate", expectedCurrentDate);

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(fileName);

			fileRequest.setFileType(FileType.LenderReferenceInfo);
			fileRequest.setFilePrifix(FileType.LenderReferenceInfo.name());

			fileManagementService.putFile(fileRequest);

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			FileInputStream in = new FileInputStream(f.getAbsolutePath());

			String mailSubject = "EMI AMOUNT";

			EmailRequest emailRequest = new EmailRequest(
					new String[] { "archana.n@oxyloans.com", "ramadevi@oxyloans.com" }, "emi-amount-to-lender.template",
					templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			} else {

				if (f.delete()) {
					System.out.println("File deleted");
				} else {
					System.out.println("Not deleted");
				}

			}

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		if (f.exists()) {
			f.delete();
		}

		return "";

	}

	public String mailToActiveLendersCreatingDeal(OxyBorrowersDealsInformation oxyBorrowersDealsInformation,
			String type) {

		Calendar currentDate = Calendar.getInstance();
		String expectedCurrentDate = expectedDateFormat.format(currentDate.getTime());

		TemplateContext templateContext = new TemplateContext();
		templateContext.put("dealName", oxyBorrowersDealsInformation.getDealName());
		DecimalFormat myFormat = new DecimalFormat("#,###");
		templateContext.put("dealAmount", myFormat.format(oxyBorrowersDealsInformation.getDealAmount()));
		templateContext.put("fundsStartDate",
				expectedDateFormat.format(oxyBorrowersDealsInformation.getFundsAcceptanceStartDate()));
		templateContext.put("fundsEndDate",
				expectedDateFormat.format(oxyBorrowersDealsInformation.getFundsAcceptanceEndDate()));
		templateContext.put("duration", oxyBorrowersDealsInformation.getDuration());
		templateContext.put("firstInterestDate",
				expectedDateFormat.format(oxyBorrowersDealsInformation.getLoanActiveDate()));
		templateContext.put("projectLink", oxyBorrowersDealsInformation.getDealLink());
		templateContext.put("minimumAmount",
				myFormat.format(oxyBorrowersDealsInformation.getMinimumPaticipationAmount()));
		templateContext.put("maxmumAmount",
				myFormat.format(oxyBorrowersDealsInformation.getPaticipationLimitToLenders()));
		templateContext.put("currentDate", expectedCurrentDate);
		String messageToLenders = null;
		if (oxyBorrowersDealsInformation.getDealOpenStatus().equals(OxyBorrowersDealsInformation.DealOpenStatus.NOW)) {
			messageToLenders = "A deal is created and the details are as follows:";
		} else {
			messageToLenders = "A Deal will be launched on "
					+ expectedDateFormat.format(oxyBorrowersDealsInformation.getDealFutureDate()) + " "
					+ oxyBorrowersDealsInformation.getDealLaunchHoure() + " IST .please find the details below:";
		}
		templateContext.put("messageToLenders", messageToLenders);

		int groupId = 0;
		int dealId = oxyBorrowersDealsInformation.getId();
		logger.info("  dealId declartion... :" + dealId);

		OxyDealsRateofinterestToLendersgroup oxyDealsRate = oxyDealsRateofinterestToLendersgroupRepo
				.findByDealIdAndGroupId(dealId, groupId);

		StringBuilder interestDetails = new StringBuilder();
		if (oxyDealsRate != null) {

			if (oxyDealsRate.getMonthlyInterest() > 0) {
				interestDetails.append("Monthly : ").append(oxyDealsRate.getMonthlyInterest()).append("%, ");
			}
			if (oxyDealsRate.getQuartelyInterest() > 0) {
				interestDetails.append("Quarterly : ").append(oxyDealsRate.getQuartelyInterest()).append("%, ");
			}
			if (oxyDealsRate.getHalfInterest() > 0) {
				interestDetails.append("Halfyearly : ").append(oxyDealsRate.getHalfInterest()).append("%, ");
			}
			if (oxyDealsRate.getYearlyInterest() > 0) {
				interestDetails.append("Yearly : ").append(Math.round(oxyDealsRate.getYearlyInterest() * 12))
						.append("%, ");
			}
			if (oxyDealsRate.getEndofthedealInterest() > 0) {
				interestDetails.append("Eod : ").append(oxyDealsRate.getEndofthedealInterest()).append("%, ");
			}
			if (interestDetails.length() > 0) {
				interestDetails.setLength(interestDetails.length() - 2); // Remove the last comma and space
				interestDetails.append(" .");
			}
		}
		templateContext.put("rateOfInterests", interestDetails);

		String mailsubject = "A New Deal is Launched...Please participate";
		if (type.equalsIgnoreCase("DEALCREATION")) {

			List<String> activeLendersMails = userRepo.listActiveLendersMails();
			if (activeLendersMails != null && !activeLendersMails.isEmpty()) {
				for (String mail : activeLendersMails) {
					EmailRequest emailRequest = new EmailRequest(new String[] { mail }, mailsubject,
							"deal-creation.template", templateContext);

					EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
					if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
						try {
							throw new MessagingException(emailResponseFromService.getErrorMessage());
						} catch (MessagingException e1) {

							e1.printStackTrace();
						}
					}
				}
			}

		} else {
			List<User> user = userRepo.getTotalActiveLenders();
			if (user != null && !user.isEmpty()) {
				user.stream().forEach(userDetails -> {
					List<OxyLendersAcceptedDeals> oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
							.findbyUserId(userDetails.getId());
					if (oxyLendersAcceptedDeals == null || oxyLendersAcceptedDeals.isEmpty()) {
						EmailRequest emailRequest = new EmailRequest(new String[] { userDetails.getEmail() },
								mailsubject, "deal-creation.template", templateContext);

						EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
						if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
							try {
								throw new MessagingException(emailResponseFromService.getErrorMessage());
							} catch (MessagingException e1) {

								e1.printStackTrace();
							}
						}
					}
				});
			}
		}
		return " ";
	}

	@Override
	@Transactional
	public DealLevelRequestDto automaticSendingOffer(BorrowersDealsRequestDto borrowersDealsRequestDto) {
		DealLevelRequestDto dealLevelRequestDto = new DealLevelRequestDto();
		try {
			StringBuilder listOfBorrowerids = new StringBuilder();
			StringBuilder listOfLoanAmounts = new StringBuilder();
			List<DealLevelRequestDto> borroweridsAndAmounts = borrowersDealsRequestDto.getIdsWithLoanAmount();
			if (borroweridsAndAmounts != null && !borroweridsAndAmounts.isEmpty()) {
				if (borrowersDealsRequestDto.getDealSubtype()
						.equalsIgnoreCase(OxyBorrowersDealsInformation.DealSubtype.STUDENT.toString())) {
					borroweridsAndAmounts.forEach(borrowerInfo -> {
						User user = userRepo.findById(borrowerInfo.getBorrowerId()).get();
						listOfBorrowerids.append(borrowerInfo.getBorrowerId()).append(",");
						listOfLoanAmounts.append(borrowerInfo.getLoanAmount()).append(",");
						if (user != null) {
							user.setLoanOwner(borrowersDealsRequestDto.getDealName());
							user.setAdminComments("INTERESTED");
							userRepo.save(user);
						}
						LoanRequest loanRequest = loanRequestRepo
								.findByUserIdAndParentRequestIdIsNull(borrowerInfo.getBorrowerId());
						if (loanRequest != null) {
							loanRequest
									.setRateOfInterestToBorrower(borrowersDealsRequestDto.getBorrowerRateOfInterest());
							if (user.getLenderGroupId() != 0) {
								loanRequest.setRateOfInterestToLender(
										borrowersDealsRequestDto.getOxyPremiumMonthlyInterest());
							} else {
								loanRequest.setRateOfInterestToLender(
										borrowersDealsRequestDto.getNewLendersMonthlyInterest());
							}
							loanRequest.setDurationBySir(borrowersDealsRequestDto.getDuration());
							loanRequest.setRepaymentMethodForBorrower("I");
							loanRequest.setRepaymentMethodForLender("I");
							loanRequest.setRateOfInterest(borrowersDealsRequestDto.getBorrowerRateOfInterest());
							loanRequest.setDurationType(LoanRequest.DurationType.Months);
							loanRequest.setDuration(borrowersDealsRequestDto.getDuration());
							loanRequestRepo.save(loanRequest);
						}
						LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();
						if (loanOfferdAmount == null) {
							loanOfferdAmount = new LoanOfferdAmount();
							loanOfferdAmount.setLoanofferedId(loanRequest.getLoanRequestId());
							loanOfferdAmount.setUserId(user.getId());
							loanOfferdAmount.setComments("testing");
							loanOfferdAmount.setLoanOfferedAmount(borrowerInfo.getLoanAmount());
							loanOfferdAmount.setEmailSent(true);
							loanOfferdAmount.setLoanRequest(loanRequest);
							loanOfferdAmount.setRateOfInterest(loanRequest.getRateOfInterestToBorrower());
							loanOfferdAmount.setId(loanRequest.getId());
							loanOfferdAmount.setDuration(loanRequest.getDurationBySir());
							loanOfferdAmount.setDurationType(LoanRequest.DurationType.Months);
							loanOfferdAmount.setBorrowerFee(0.0);
							Double emiAmount = (borrowerInfo.getLoanAmount()
									* loanRequest.getRateOfInterestToBorrower()) / 100d;
							loanOfferdAmount.setEmiAmount((double) Math.round(emiAmount));
							loanOfferdAmount.setNetDisbursementAmount(borrowerInfo.getLoanAmount());
							loanOfferdAmount.setLoanOfferdStatus(LoanOfferdAmount.LoanOfferdStatus.LOANOFFERACCEPTED);
							loanRequest.setLoanOfferedAmount(loanOfferdAmount);
							loanRequestRepo.save(loanRequest);
						} else {
							loanOfferdAmount.setLoanOfferedAmount(borrowerInfo.getLoanAmount());
							loanOfferdAmount.setRateOfInterest(loanRequest.getRateOfInterestToBorrower());
							loanOfferdAmount.setDuration(loanRequest.getDurationBySir());
							Double emiAmount = (borrowerInfo.getLoanAmount()
									* loanRequest.getRateOfInterestToBorrower()) / 100d;
							loanOfferdAmount.setEmiAmount((double) Math.round(emiAmount));
							loanOfferdAmount.setNetDisbursementAmount(borrowerInfo.getLoanAmount());
							loanRequest.setLoanOfferedAmount(loanOfferdAmount);
							loanRequestRepo.save(loanRequest);
						}

					});
				}
			}
			dealLevelRequestDto.setIdsMappedToDeals(listOfBorrowerids.toString());
			dealLevelRequestDto.setAmountMappedToDeals(listOfLoanAmounts.toString());
		} catch (Exception e) {
			logger.info("Exception in automaticSendingOffer method");
		}
		return dealLevelRequestDto;

	}

	@Override
	public String sendingEmailNotifiaction(int dealId) {
		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
				.findBydealId(dealId);
		if (oxyBorrowersDealsInformation != null) {
			mailToActiveLendersCreatingDeal(oxyBorrowersDealsInformation, "AFTERDEALCREATION");
		}
		return "sentSuccessfully";

	}

	@Override
	public EnachScheduledMailResponseDto readingLenderReferenceInfoToExcelSheet1() throws ParseException {

		logger.info("readingLenderReferenceInfoToExcelSheet method starts.............");

		EnachScheduledMailResponseDto response1 = new EnachScheduledMailResponseDto();

		LenderReferenceResponse referenceResponse = readingLenderReferenceToSheet1();

		logger.info("readingLenderReferenceInfo");

		List<LenderReferralResponse> listOfLenderReferralResponse = referenceResponse.getListOfLenderReferralResponse();

		String excelurl = "";

		FileOutputStream outFile = null;

		Workbook workBook = new HSSFWorkbook();
		
		Calendar current1 = Calendar.getInstance();
        SimpleDateFormat expectedDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String expectedCurrentDateTime = expectedDateFormat.format(current1.getTime());

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyy");

		StringBuilder sbf = new StringBuilder("OXYLR1_OXYLR1UPLD_");

		String requiredDate = formatter.format(new Date());

		sbf = sbf.append(requiredDate + "_RefBN");

		org.apache.poi.ss.usermodel.Sheet spreadSheet = workBook.createSheet(sbf.toString());

		Row row0 = spreadSheet.createRow(0);

		CellStyle style = workBook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.RED.getIndex());

		Font font = workBook.createFont();// Create font
		font.setBold(true);
		style.setFont(font);

		Cell cell = row0.createCell(0);
		cell.setCellValue("Debit Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(1);
		cell.setCellValue("Beneficiary Ac No");
		cell.setCellStyle(style);

		cell = row0.createCell(2);
		cell.setCellValue("Beneficiary Name");
		cell.setCellStyle(style);

		cell = row0.createCell(3);
		cell.setCellValue("Amt");
		cell.setCellStyle(style);

		cell = row0.createCell(4);
		cell.setCellValue("Pay Mod");
		cell.setCellStyle(style);

		cell = row0.createCell(5);
		cell.setCellValue("Date");
		cell.setCellStyle(style);

		cell = row0.createCell(6);
		cell.setCellValue("IFSC");
		cell.setCellStyle(style);

		cell = row0.createCell(7);
		cell.setCellValue("Payable Location");
		cell.setCellStyle(style);

		cell = row0.createCell(8);
		cell.setCellValue("Print Location");
		cell.setCellStyle(style);

		cell = row0.createCell(9);
		cell.setCellValue("Bene Mobile No.");
		cell.setCellStyle(style);

		cell = row0.createCell(10);
		cell.setCellValue("Bene Email ID");
		cell.setCellStyle(style);

		cell = row0.createCell(11);
		cell.setCellValue("Bene add1");
		cell.setCellStyle(style);

		cell = row0.createCell(12);
		cell.setCellValue("Bene add2");
		cell.setCellStyle(style);

		cell = row0.createCell(13);
		cell.setCellValue("Bene add3");
		cell.setCellStyle(style);

		cell = row0.createCell(14);
		cell.setCellValue("Bene add4");
		cell.setCellStyle(style);

		cell = row0.createCell(15);
		cell.setCellValue("Add Details 1");
		cell.setCellStyle(style);

		cell = row0.createCell(16);
		cell.setCellValue("Add Details 2");
		cell.setCellStyle(style);

		cell = row0.createCell(17);
		cell.setCellValue("Add Details 3");
		cell.setCellStyle(style);

		cell = row0.createCell(18);
		cell.setCellValue("Add Details 4");
		cell.setCellStyle(style);

		cell = row0.createCell(19);
		cell.setCellValue("Add Details 5");
		cell.setCellStyle(style);

		cell = row0.createCell(20);
		cell.setCellValue("Remarks");
		cell.setCellStyle(style);

		int rowCount = 0;
		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}
		int countForSno = 0;

		String referrerName = "";
		Integer referrerId = 0;

		String userNamePerBank = "";
		String accountNumber = "";

		String ifscCode = "";

		String referrerUserId = "";

		Double totalAmount = 0.0;

		if (listOfLenderReferralResponse != null && !listOfLenderReferralResponse.isEmpty()) {
			logger.info("readingLenderList1" + listOfLenderReferralResponse.size());

			for (LenderReferralResponse obj : listOfLenderReferralResponse) {

				Row row = spreadSheet.createRow(++rowCount);

				List<String> dealAndRefereePairs1 = obj != null ? obj.getDealAndRefereePairs() : new ArrayList<>();
				String dealRefereePair = dealAndRefereePairs1 == null ? "NULL" : String.join(" ", dealAndRefereePairs1);

				String refereeIdss = "";
				StringBuilder sb = new StringBuilder();

				String refereeIds1 = obj.getRefereeIds();
				String[] refereeids = refereeIds1.split(",\\s*");

				for (String id : refereeids) {

					int refereeId = Integer.parseInt(id.trim());
					logger.info("refereeId : " + refereeId);

					User referee = userRepo.findByIdNum(refereeId);

					if (sb.length() > 0) {
						sb.append(",");
					}
					sb.append(referee.getId());
					refereeIdss = sb.toString();

					if (referee != null) {
						LenderReferenceDetails referenceDetails = lenderReferenceDetailsRepo
								.findingReferrerInfo(referee.getId());

						logger.info("Source Type" + referenceDetails.getSource());

						if (referenceDetails.getSource() != null) {
							if (referenceDetails.getSource().equals(LenderReferenceDetails.Source.Partner)) {
								OxyUserType oxyUser = oxyUserTypeRepo.findById(referenceDetails.getReferrerId()).get();

								if (oxyUser != null) {
									referrerName = oxyUser.getPartnerName();
									referrerId = oxyUser.getId();
									userNamePerBank = oxyUser.getUserName();
									accountNumber = oxyUser.getAccountNumber();
									ifscCode = oxyUser.getIfscCode().toUpperCase();

									referrerUserId = "PR" + oxyUser.getId();
								}

							} else if ((referenceDetails.getSource().equals(LenderReferenceDetails.Source.ReferralLink))
									|| (referenceDetails.getSource()
											.equals(LenderReferenceDetails.Source.BulkInvite))) {

								logger.info("ReferrerId :" + referenceDetails.getReferrerId());
								logger.info("Source :" + referenceDetails.getSource());

								User referrer = userRepo.findByIdNum(referenceDetails.getReferrerId());

								if (referrer != null) {
									referrerName = referrer.getPersonalDetails().getFirstName() + " "
											+ referrer.getPersonalDetails().getLastName();
									referrerId = referrer.getId();
									userNamePerBank = referrer.getBankDetails().getUserName();
									accountNumber = referrer.getBankDetails().getAccountNumber();
									ifscCode = referrer.getBankDetails().getIfscCode().toUpperCase();
									referrerUserId = "LR" + referrer.getId();

								}
							}
						}
					}
				}

				User user = userRepo.findById(referrerId).get();
				logger.info("testUserType" + user.isTestUser());
				if (user.isTestUser() == false) {

					Cell cell1 = row.createCell(0);
					cell1.setCellValue("777705849442");

					cell1 = row.createCell(1);
					cell1.setCellValue(accountNumber == null ? "" : accountNumber);

					cell1 = row.createCell(2);
					cell1.setCellValue(referrerName == null ? "" : referrerName);

					cell1 = row.createCell(3);
					cell1.setCellValue(Math.round(obj.getAmount()));

					cell1 = row.createCell(4);
					cell1.setCellValue("N");

					SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");

					String modifiedDate = format.format(new Date());

					cell1 = row.createCell(5);
					cell1.setCellValue(modifiedDate);

					cell1 = row.createCell(6);
					cell1.setCellValue(ifscCode == null ? "" : ifscCode);

					cell1 = row.createCell(16);
					cell1.setCellValue("LENDERREFERRALBONUS");


					cell1 = row.createCell(17);

					if (obj != null && obj.getIds() != null) {
						cell1.setCellValue(obj.getIds());
					} else {
						cell1.setCellValue("NULL");
					}

					cell1 = row.createCell(18);
					cell1.setCellValue(referrerUserId);

					cell1 = row.createCell(19);
					cell1.setCellValue(refereeIdss);

					cell1 = row.createCell(20);
					cell1.setCellValue("OXYLOANS REFERRAL BONUS");

					cell1 = row.createCell(15);
					cell1.setCellValue(dealRefereePair);

					totalAmount +=Math.round(obj.getAmount());

					countForSno = countForSno + 1;
					
					// New Table Added for saving Data
					
					  ReferralBonusReading referral = referralBonusReadingRepo.findByData2(referrerId);
		               if(referral != null) {
		            	  
		            	   referral.setUserId(referrerId);
		            	   referral.setRefereeDealIdPair(dealRefereePair);
		            	   referral.setRefferalBonusUpdatedId(obj.getIds() == null ? "NULL" : obj.getIds());
		            	   referral.setTotalAmount(Double.valueOf(Math.round(obj.getAmount())));
		            	   referral.setStatus(ReferralBonusReading.Status.GENERATED.toString());
		            	   referral.setSheetGeneratedOn(Timestamp.valueOf(expectedCurrentDateTime));

		    				referralBonusReadingRepo.save(referral);
		    				
		             } else {
					    
		            ReferralBonusReading bonus = new ReferralBonusReading(); 
					
					bonus.setUserId(referrerId);
					bonus.setRefereeDealIdPair(dealRefereePair);
					bonus.setRefferalBonusUpdatedId(obj.getIds() == null ? "NULL" : obj.getIds());
					bonus.setTotalAmount(Double.valueOf(Math.round(obj.getAmount())));
			        bonus.setStatus(ReferralBonusReading.Status.GENERATED.toString());
	                bonus.setSheetGeneratedOn(Timestamp.valueOf(expectedCurrentDateTime));
					
					referralBonusReadingRepo.save(bonus);
					
		             }	
				}
			}
		}

		for (int i = 0; i <= 10; i++) {
			spreadSheet.autoSizeColumn(i);
		}

		String fileName = sbf.toString() + ".xls";
		File f = new File(iciciFilePathBeforeApproval+ fileName);

		try {
			outFile = new FileOutputStream(f);
			workBook.write(outFile);

			Calendar current = Calendar.getInstance();
			TemplateContext templateContext = new TemplateContext();
			String expectedCurrentDate = expectedDateFormat.format(current.getTime());

			templateContext.put("currentDate", expectedCurrentDate);

			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(f));
			fileRequest.setFileName(fileName);

			fileRequest.setFileType(FileType.LenderReferenceInfo);
			fileRequest.setFilePrifix(FileType.LenderReferenceInfo.name());

			fileManagementService.putFile(fileRequest);

			FileResponse getFile = fileManagementService.getFile(fileRequest);

			excelurl = getFile.getDownloadUrl();

			String[] downloadUrl = excelurl.split(Pattern.quote("?"));

			response1.setDownloadUrl(downloadUrl[0]);

			FileInputStream in = new FileInputStream(f.getAbsolutePath());

			String mailSubject = "Lender Reference Information";

			EmailRequest emailRequest = new EmailRequest(
					new String[] { "archana.n@oxyloans.com", "ramadevi@oxyloans.com", "vijaydasari060@gmail.com" },
					"lenderReferenceInfo.template", templateContext, mailSubject, new String[] { f.getAbsolutePath() });

			EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
			if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponseFromService.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			} else {
				response1.setEmailStatus("Email Sent!");

				if (f.delete()) {
					System.out.println("File deleted");
				} else {
					System.out.println("Not deleted");
				}

			}
			
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = format1.parse(format1.format(new Date()));
			
			LenderCmsPayments cmsStats = lenderCmsPaymentsRepo.findbyFileName(f.getName());

			if (cmsStats != null) {
				cmsStats.setFileName(f.getName());
				cmsStats.setDealId(0);
				if (cmsStats.getFileExecutionsSatus() == OxyCmsLoans.FileExecutionStatus.MOVEDTOS3) {
					cmsStats.setFileExecutionsSatus(OxyCmsLoans.FileExecutionStatus.MOVEDTOS3);
				}

				cmsStats.setTotalAmount(totalAmount);
				cmsStats.setCreatedOn(date);
				cmsStats.setLenderReturnsType("ReferralBonus");
				// cmsStats.setPaymentMode(LenderCmsPayments.PaymentMode.SYSTEM.toString());
				lenderCmsPaymentsRepo.save(cmsStats);
			} else {
				LenderCmsPayments iciciCmsStats = new LenderCmsPayments();
				iciciCmsStats.setFileName(f.getName());
				iciciCmsStats.setDealId(0);
				iciciCmsStats.setFileExecutionsSatus(OxyCmsLoans.FileExecutionStatus.MOVEDTOS3);
				iciciCmsStats.setTotalAmount(totalAmount);
				iciciCmsStats.setCreatedOn(date);
				iciciCmsStats.setS3_download_link(downloadUrl[0]);
				iciciCmsStats.setLenderReturnsType("ReferralBonus");
				// iciciCmsStats.setPaymentMode(LenderCmsPayments.PaymentMode.SYSTEM.toString());
				lenderCmsPaymentsRepo.save(iciciCmsStats);
				
	     List<ReferralBonusReading> sets =referralBonusReadingRepo.findlistOfcreatedOn(iciciCmsStats.getCreatedOn());
	     logger.info("ListOfdataSize@;;;----- "+sets.size());  
		     if(!sets.isEmpty() && sets!=null)	{
			 for(ReferralBonusReading data : sets) {
				data.setLendercmspaymentsid(iciciCmsStats.getId());
				referralBonusReadingRepo.save(data);
						
			logger.info("LenderCmsPaymentsId..... "+data.getLendercmspaymentsid());
		}
	}
				
		}
			
			
			//My reference
			String testServerChatId = "9640035218,120363020098924954@g.us";
			
			String messageToUser  = "The *"+ "ReferralBonus"+ "* sheet totalAmount of INR "+"*"+ Math.round(totalAmount) + "* successfully Generated on *"+format1.format(new Date())+"*"+" With the fileName *" +f.getName()+"*";
			String messageSentToMobile = whatappService.whatsappMessageToGroupUsingUltra(testServerChatId,messageToUser);


			WhatsappAttachmentRequestDto dto = new WhatsappAttachmentRequestDto();

			dto.setFileName(f.getName());
			dto.setInputSream(in);
			dto.setChatId("120363020098924954@g.us");

			// whatappServiceRepo.sendAttatchements(dto);

			WhatsappAttachmentRequestDto dto1 = new WhatsappAttachmentRequestDto();

			dto1.setFileName(f.getName());
			dto1.setInputSream(in);
			dto1.setChatId("919652612942@c.us");

			// whatappServiceRepo.sendAttatchements(dto1);

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

			String message = "Total Referral Bonus is INR " + totalAmount + " on " + format.format(new Date())
					+ " in production.";

			/*
			 * whatappServiceRepo.sendingWhatsappMessages("917702795895-1630419500@g.us",
			 * message, whatsaAppSendApiNewInstance);
			 */

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		if (f.exists()) {
			f.delete();
		}

		/*
		 * } else {
		 * 
		 * SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		 * 
		 * String message = "Total Referral Bonus is INR 0 on " + format.format(new
		 * Date());
		 * 
		 * 
		 * whatappServiceRepo.sendingWhatsappMessages("917702795895-1630419500@g.us",
		 * message, whatsaAppSendApiNewInstance);
		 * 
		 * 
		 * }
		 */
		logger.info("readingLenderReferenceInfoToExcelSheet method ends.............");

		return response1;

	}

	@Override
	public LenderReferenceResponse readingLenderReferenceToSheet1() throws ParseException {

		logger.info("readingLenderReferenceToSheet method starts.............");

		List<LenderReferralResponse> response = new ArrayList<LenderReferralResponse>();

		// List<LenderReferralResponseForDuration> newListResponse = new
		// ArrayList<LenderReferralResponseForDuration>();

		List<Integer> referrerIds = lenderReferralBonusUpdatedRepo.findingListOfReferees();

		if (referrerIds != null && !referrerIds.isEmpty()) {
			for (Integer referrerId : referrerIds) {
				logger.info("referrerId.." + referrerId);

				boolean status = false;
				String validity = "";
				String currentDateNew = "";
				User user = userRepo.findByIdNum(referrerId);
				LenderRenewalDetails lenderRenewalDetails = lenderRenewalDetailsRepo
						.findingRenewalDetailsBasedOnUserIdNum(referrerId);
				if (lenderRenewalDetails != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					validity = format.format(lenderRenewalDetails.getValidityDate());

					currentDateNew = format.format(new Date());

					Date validityDate = format.parse(validity);
					Date currentModifiedDate = format.parse(currentDateNew);

					if (validityDate.compareTo(currentModifiedDate) >= 0) {

						status = true;
					} else {
						Integer noOfPaymentsDoneForPerDeal = lenderPayuDetailsRepo
								.findingCountOfPaymentsDoneForPerDealFromValidityDateToCurrentDate(referrerId, validity,
										currentDateNew);
						if (noOfPaymentsDoneForPerDeal > 0) {
							status = true;
						}
					}

				}

				// logger.info("ReferralLog1" + status);

				List<LenderIdsAndDealIds> listOfLenderIdsAndDealIds = new ArrayList<LenderIdsAndDealIds>();

				List<LenderIdsAndDealIds> listOfNewLenderAndDealIds = new ArrayList<LenderIdsAndDealIds>();

				Integer tableId = 0;
				Double totalAmount = 0.0;
				Double total = 0.0;
				// if (status == true) {
				List<Integer> listOfReferees = lenderReferralBonusUpdatedRepo
						.findingListOfRefereesOfReferrer(referrerId);

				if (listOfReferees != null && !listOfReferees.isEmpty()) {
					for (Integer refereeId : listOfReferees) {
						logger.info("refereeIds are.." + refereeId);

						List<Integer> listOfDealIdsForReferee = lenderReferralBonusUpdatedRepo
								.findingListOfDealIdsForReferee(referrerId, refereeId);

						Integer dealNumber = 0;

						List<Integer> listOfDealIds = new ArrayList<Integer>();

						Double sumOfAmountBasedOnDealId = 0.0;

						if (listOfDealIdsForReferee != null && !listOfDealIdsForReferee.isEmpty()) {
							for (Integer dealId : listOfDealIdsForReferee) {
								logger.info("Referral dealIds are..." + dealId);

								OxyBorrowersDealsInformation dealInformation = oxyBorrowersDealsInformationRepo
										.findBydealId(dealId);
								LenderIdsAndDealIds lenderIdAndDealId = new LenderIdsAndDealIds();
								LenderIdsAndDealIds newLenderIdAndDealId = new LenderIdsAndDealIds();

								String remarks = "";
								if (dealId != 0) {
									OxyLendersAcceptedDeals acceptedDeals = oxyLendersAcceptedDealsRepo
											.findingLenderParticipatedInDeal(refereeId, dealId);

									Double totalAmountUpdated = lendersPaticipationUpdationRepo
											.findingSumOfAmountUpdated(refereeId, dealId);

									if (totalAmountUpdated == null) {
										totalAmountUpdated = 0.0;
									}

									LendersReturns returnsDetails = lendersReturnsRepo.findingWithdralDate(refereeId,
											dealId);

									if (acceptedDeals != null) {
										SimpleDateFormat obj = new SimpleDateFormat("yyyy-MM-dd");
										Date participatedOn = acceptedDeals.getReceivedOn();
										String modifiedParticipatedDate = obj.format(participatedOn);
										Date date1 = obj.parse(modifiedParticipatedDate);

										LenderOxyWallet lenderScrowWallet = lenderOxyWalletRepo
												.findingLenderPrincipalMovedToWallet(refereeId, dealId);

										if (dealInformation != null) {
											dealNumber = dealInformation.getId();
											if (dealInformation.getDuration() <= 3
													&& (returnsDetails != null || lenderScrowWallet != null)) {

												tableId = lenderReferralBonusUpdatedRepo
														.findByIdBasedOnRefereeIdAndDealId(refereeId, dealNumber);
												logger.info("TableIds are ... " + tableId);

												Double sumOfAmountEarnedByReferrer = lenderReferralBonusUpdatedRepo
														.findingSumOfAmountEarnedByRefereeNew(refereeId);

												Double totalBonus = 0.0;

												if (sumOfAmountEarnedByReferrer == null) {
													sumOfAmountEarnedByReferrer = 0.0;
												}
												Double bonusAmount = acceptedDeals.getParticipatedAmount()
														+ totalAmountUpdated;

												if (sumOfAmountEarnedByReferrer < 1000) {

													Date withdrawDate = null;
													if (lenderScrowWallet != null) {
														if (lenderScrowWallet.getTransactionDate() != null) {
															withdrawDate = lenderScrowWallet.getTransactionDate();
														}
													} else {
														if (returnsDetails != null) {
															if (returnsDetails.getPaidDate() != null) {
																withdrawDate = returnsDetails.getPaidDate();
															}
														}
													}
													String modifiedwithDrawDate = obj.format(withdrawDate);
													Date date2 = obj.parse(modifiedwithDrawDate);

													long time_difference = date2.getTime() - date1.getTime();

													long days_difference = (time_difference / (1000 * 60 * 60 * 24))
															% 365;
													// logger.info("date2" + date2.getTime());
													// logger.info("date1" + date1.getTime());
													// logger.info("days_difference" + days_difference);
													if (days_difference >= 90) {

														// logger.info("ReferralLog43772" +
														// sumOfAmountEarnedByReferrer);

														totalBonus = lenderReferralBonusUpdatedRepo
																.findingSumOfAmountBasedOnDealId(dealId, refereeId);

														// logger.info("ReferralLog43772" + totalBonus);

														newLenderIdAndDealId.setDealId(dealNumber);
														newLenderIdAndDealId.setRefereeId(refereeId);
														newLenderIdAndDealId.setAmountForDeal(totalBonus);
														newLenderIdAndDealId.setId(tableId);

														listOfNewLenderAndDealIds.add(newLenderIdAndDealId);
													} else {

														totalBonus = Math.floor(bonusAmount / 2000);

														// logger.info("ReferralLog43772" + bonusAmount);

														newLenderIdAndDealId.setDealId(dealNumber);
														newLenderIdAndDealId.setRefereeId(refereeId);
														newLenderIdAndDealId.setAmountForDeal(totalBonus);
														newLenderIdAndDealId.setId(tableId);

														listOfNewLenderAndDealIds.add(newLenderIdAndDealId);
													}

													if (days_difference < 90) {
														if (returnsDetails != null && returnsDetails.getAmountType()
																.equals(OxyTransactionDetailsFromExcelSheets.DebitedTowords.LENDERWITHDRAW)) {
															remarks = "Withdrawn from " + dealInformation.getDealName()
																	+ " deal in " + days_difference + " days";
														} else {
															remarks = "PrincipalReturned from "
																	+ dealInformation.getDealName() + " deal in "
																	+ days_difference + " days";
														}

													}

													List<LenderReferralBonusUpdated> listOfDealAndRefereeIdsForRemarks = lenderReferralBonusUpdatedRepo
															.findingListOfDealAndRefereeIdsForAmountUpdation(refereeId,
																	dealId);
													if (listOfDealAndRefereeIdsForRemarks != null
															&& !listOfDealAndRefereeIdsForRemarks.isEmpty()) {
														for (LenderReferralBonusUpdated referralRemarksUpdation : listOfDealAndRefereeIdsForRemarks) {

															referralRemarksUpdation.setRemarks(remarks);
															lenderReferralBonusUpdatedRepo
																	.save(referralRemarksUpdation);
														}

													}
												} else {

													// logger.info("ReferralLogLog5" + bonusAmount);

													totalBonus = Math.floor(bonusAmount / 2000);

													newLenderIdAndDealId.setDealId(dealNumber);
													newLenderIdAndDealId.setRefereeId(refereeId);
													newLenderIdAndDealId.setAmountForDeal(totalBonus);
													newLenderIdAndDealId.setId(tableId);

													listOfNewLenderAndDealIds.add(newLenderIdAndDealId);

												}

												List<LenderReferralBonusUpdated> listOfDealAndRefereeIdsForUpdatingAmount = lenderReferralBonusUpdatedRepo
														.findingListOfDealAndRefereeIdsForAmountUpdation(refereeId,
																dealId);
												if (listOfDealAndRefereeIdsForUpdatingAmount != null
														&& !listOfDealAndRefereeIdsForUpdatingAmount.isEmpty()) {
													for (LenderReferralBonusUpdated referralBonusUpdated : listOfDealAndRefereeIdsForUpdatingAmount) {
														referralBonusUpdated.setUpdatedBonus(totalBonus);

														lenderReferralBonusUpdatedRepo.save(referralBonusUpdated);
													}

												}

											} else {

												if (returnsDetails != null || lenderScrowWallet != null) {
													try {
														Date withdrawDate = null;
														if (lenderScrowWallet != null) {
															if (lenderScrowWallet.getTransactionDate() != null) {
																withdrawDate = lenderScrowWallet.getTransactionDate();
															}
														} else {
															if (returnsDetails != null) {
																if (returnsDetails.getPaidDate() != null) {
																	withdrawDate = returnsDetails.getPaidDate();
																}
															}
														}
														String modifiedwithDrawDate = obj.format(withdrawDate);
														Date date2 = obj.parse(modifiedwithDrawDate);

														long time_difference = date2.getTime() - date1.getTime();

														long days_difference = (time_difference / (1000 * 60 * 60 * 24))
																% 365;

														// logger.info("ReferralLogLog6" + dealId +
														// days_difference);
														Double totalBonus = 0.0;
														if (days_difference >= 90) {

															if (returnsDetails != null) {
																dealNumber = returnsDetails.getDealId();
															} else {
																if (lenderScrowWallet != null) {
																	dealNumber = lenderScrowWallet.getDealId();
																}
															}

															sumOfAmountBasedOnDealId = lenderReferralBonusUpdatedRepo
																	.findingSumOfAmountBasedOnDealId(dealNumber,
																			refereeId);

															if (sumOfAmountBasedOnDealId == null) {
																sumOfAmountBasedOnDealId = 0.0;
															}

															lenderIdAndDealId.setDealId(dealNumber);
															lenderIdAndDealId.setRefereeId(refereeId);

															listOfLenderIdsAndDealIds.add(lenderIdAndDealId);

															// logger.info("ReferralLogLog7" + dealId +
															// days_difference);

														}

														else {

															Double bonusAmount = acceptedDeals.getParticipatedAmount()
																	+ totalAmountUpdated;

															totalBonus = Math.floor(bonusAmount / 2000);

															// logger.info("ReferralLog42364" + bonusAmount);

															newLenderIdAndDealId.setDealId(dealNumber);
															newLenderIdAndDealId.setRefereeId(refereeId);
															newLenderIdAndDealId.setAmountForDeal(totalBonus);

															listOfNewLenderAndDealIds.add(newLenderIdAndDealId);
														}

														if (days_difference < 90) {
															if (returnsDetails != null && returnsDetails.getAmountType()
																	.equals(OxyTransactionDetailsFromExcelSheets.DebitedTowords.LENDERWITHDRAW)) {
																remarks = "Withdrawn from "
																		+ dealInformation.getDealName() + " deal in "
																		+ days_difference + " days";
															} else {
																remarks = "PrincipalReturned from "
																		+ dealInformation.getDealName() + " deal in "
																		+ days_difference + " days";
															}
														}

														List<LenderReferralBonusUpdated> listOfDealAndRefereeIdsForRemarks = lenderReferralBonusUpdatedRepo
																.findingListOfDealAndRefereeIdsForAmountUpdation(
																		refereeId, dealId);
														if (listOfDealAndRefereeIdsForRemarks != null
																&& !listOfDealAndRefereeIdsForRemarks.isEmpty()) {
															for (LenderReferralBonusUpdated referralRemarksUpdation : listOfDealAndRefereeIdsForRemarks) {

																referralRemarksUpdation.setRemarks(remarks);
																lenderReferralBonusUpdatedRepo
																		.save(referralRemarksUpdation);
															}

														}

													} catch (ParseException excep) {
														excep.printStackTrace();
													}

												} else {

													Date currentDate = new Date();
													String currentDate1 = obj.format(currentDate);

													Date currentDate2 = obj.parse(currentDate1);

													long time_difference1 = currentDate2.getTime() - date1.getTime();

													long days_difference1 = (time_difference1 / (1000 * 60 * 60 * 24))
															% 365;

													// logger.info("ReferralLogLog8" + dealId + days_difference1);

													if (days_difference1 >= 90) {

														dealNumber = acceptedDeals.getDealId();
														sumOfAmountBasedOnDealId = lenderReferralBonusUpdatedRepo
																.findingSumOfAmountBasedOnDealId(dealNumber, refereeId);
														if (sumOfAmountBasedOnDealId == null) {
															sumOfAmountBasedOnDealId = 0.0;
														}

														lenderIdAndDealId.setDealId(dealNumber);
														lenderIdAndDealId.setRefereeId(refereeId);

														listOfLenderIdsAndDealIds.add(lenderIdAndDealId);
													}
												}
											}
										}
									}

								}
							}
						}

					}
				}

				Double sumOfAmountForRefereeAndDealId = 0.0;
				if (listOfLenderIdsAndDealIds != null && !listOfLenderIdsAndDealIds.isEmpty()) {

					for (LenderIdsAndDealIds lenderId : listOfLenderIdsAndDealIds) {
						logger.info("RefereeId :" + lenderId.getRefereeId() + " DealId :" + lenderId.getDealId());
						sumOfAmountForRefereeAndDealId = lenderReferralBonusUpdatedRepo
								.findingSumOfAmountBasedOnRerefeeIdAndDealId(lenderId.getRefereeId(),
										lenderId.getDealId());

						if (sumOfAmountForRefereeAndDealId == null) {
							sumOfAmountForRefereeAndDealId = 0.0;
						}

						total += sumOfAmountForRefereeAndDealId;
						logger.info("Total Amount :" + total);
						sumOfAmountForRefereeAndDealId = 0.0;
					}

				}

				if (listOfNewLenderAndDealIds != null && !listOfNewLenderAndDealIds.isEmpty()) {

					for (LenderIdsAndDealIds ids : listOfNewLenderAndDealIds) {

						total += ids.getAmountForDeal();
						logger.info("ids.getRefereeId : " + ids.getRefereeId() + " ids.getDealId : " + ids.getDealId()
								+ " ids.getAmountForDeal : " + ids.getAmountForDeal());

					}
				}

				logger.info("Log111 Amount : " + total);

				if (total >= 500) {

					if (status == true) {

						Double sumAmount = 0.0;
						LenderReferralResponse referralResponse = new LenderReferralResponse();
						List<String> dealAndRefereePairs = new ArrayList<>();

						StringBuilder refereeIdsBuilder = new StringBuilder();
						StringBuilder refereeIdsBuilder1 = new StringBuilder();
						StringBuilder dealIds = new StringBuilder();

						if (listOfLenderIdsAndDealIds != null && !listOfLenderIdsAndDealIds.isEmpty()) {

							for (LenderIdsAndDealIds lenderId : listOfLenderIdsAndDealIds) {
								List<LenderReferralBonusUpdated> listOfDetails = lenderReferralBonusUpdatedRepo
										.findingListOfDealInfo(lenderId.getDealId(), lenderId.getRefereeId());

								if (listOfDetails != null && !listOfDetails.isEmpty()) {

									for (LenderReferralBonusUpdated referralBonus : listOfDetails) {

										sumAmount = sumAmount + referralBonus.getAmount();
										logger.info("sumAmount....." + sumAmount);
										referralResponse.setAmount(sumAmount);

										if (dealIds.length() > 0) {
											dealIds.append(",");
										}
										dealIds.append(referralBonus.getDealId());
										String dealIds1 = dealIds.toString();
										logger.info("dealIds1.....:" + dealIds1);
										referralResponse.setDealIds(dealIds1);

										if (refereeIdsBuilder.length() > 0) {
											refereeIdsBuilder.append(", ");
										}
										refereeIdsBuilder.append(referralBonus.getRefereeUserId());

										String refereeIds = refereeIdsBuilder.toString();
										logger.info("refereeIds.....:" + refereeIds);
										referralResponse.setRefereeIds(refereeIds);

										if (refereeIdsBuilder1.length() > 0) {
											refereeIdsBuilder1.append(", ");
										}
										refereeIdsBuilder1.append(referralBonus.getId());

										String tableIds = refereeIdsBuilder1.toString();
										logger.info("tableIds.....:" + tableIds);
										referralResponse.setIds(tableIds);

									}
								}
							}

						}

						if (listOfNewLenderAndDealIds != null && !listOfNewLenderAndDealIds.isEmpty()) {
							for (LenderIdsAndDealIds ids : listOfNewLenderAndDealIds) {

								sumAmount = sumAmount + ids.getAmountForDeal();
								logger.info("sumAmount....." + sumAmount);
								referralResponse.setAmount(sumAmount);

								dealAndRefereePairs.add("[" + ids.getRefereeId() + "," + ids.getDealId() + "]");
								referralResponse.setDealAndRefereePairs(dealAndRefereePairs);

								if (refereeIdsBuilder.length() > 0) {
									refereeIdsBuilder.append(", ");
								}
								refereeIdsBuilder.append(ids.getRefereeId());
								String listRefereeIds2 = refereeIdsBuilder.toString();
								logger.info("listRefereeIds2.....:" + listRefereeIds2);
								referralResponse.setRefereeIds(listRefereeIds2);

								if (dealIds.length() > 0) {
									dealIds.append(", ");
								}
								dealIds.append(ids.getDealId());
								String listOfDealIds2 = dealIds.toString();
								logger.info("listOfDealIds2.....:" + listOfDealIds2);
								referralResponse.setDealIds(listOfDealIds2);

								/*
								 * if(refereeIdsBuilder1.length() > 0) { refereeIdsBuilder1.append(" ,"); }
								 * refereeIdsBuilder1.append(ids.getId()); String listOfIds2 =
								 * refereeIdsBuilder1.toString(); logger.info("listOfIds2.....:"+listOfIds2);
								 * referralResponse.setIds(listOfIds2);
								 */

							}

						}
						for (String pair : dealAndRefereePairs) {
							logger.info("pair :" + pair);
						}
						response.add(referralResponse);
					} else {
						if (user != null) {
							if (user.getLenderGroupId() != 0) {

								ReferralBonusDetails referralDetails = new ReferralBonusDetails();

								referralDetails.setReferrerId(referrerId);
								referralDetails.setComments("Should generate referral bonus on " + currentDateNew
										+ " but the lender is not renewed and the validity date is " + validity);

								referralBonusDetailsRepo.save(referralDetails);
							}
						}
					}

				}

			}
		}

		LenderReferenceResponse lenderReferenceResponse = new LenderReferenceResponse();

		lenderReferenceResponse.setListOfLenderReferralResponse(response);

		// lenderReferenceResponse.setListOfNewReferralResponse(newListResponse);

		logger.info("readingLenderReferenceToSheet method ends.............");

		return lenderReferenceResponse;
	}

	@Override
	public LenderReferenceResponse readingCommentsOfRadhaSir1(LenderReferenceRequestDto lenderReferenceRequestDto)
			throws ParseException {

		logger.info("readingCommentsOfRadhaSir1 method starts.................");

		LenderReferenceResponse referenceResponse = readingLenderReferenceToSheet1();

		List<LenderReferralResponse> listOfLenderReferralResponse = referenceResponse.getListOfLenderReferralResponse();

		LenderReferenceResponse response = new LenderReferenceResponse();

		if (listOfLenderReferralResponse != null && !listOfLenderReferralResponse.isEmpty()) {
			for (LenderReferralResponse obj : listOfLenderReferralResponse) {

				Integer id = obj.getId();

				LenderReferralBonusUpdated referralInfo = lenderReferralBonusUpdatedRepo.gettingReferralInfo(id);
				referralInfo.setRadhaSirCommentsForBonus(lenderReferenceRequestDto.getComments());

				lenderReferralBonusUpdatedRepo.save(referralInfo);

				response.setStatus("Successfully Updated");

			}
		}

		logger.info("readingCommentsOfRadhaSir1 method ends.................");

		return response;
	}

	@Override
	public LenderReferenceResponse readingResponseForReferenceBonus1(
			LenderReferenceRequestDto lenderReferenceRequestDto) throws ParseException {
		logger.info("readingResponseForReferenceBonus method starts....................");

		LenderReferenceResponse referenceResponse = new LenderReferenceResponse();

		LenderReferenceResponse referenceResponses = readingLenderReferenceToSheet1();

		List<LenderReferralResponse> listOfLenderReferralResponse = referenceResponses
				.getListOfLenderReferralResponse();

		if (listOfLenderReferralResponse != null && !listOfLenderReferralResponse.isEmpty()) {
			for (LenderReferralResponse referral : listOfLenderReferralResponse) {

				LenderReferralBonusUpdated referralBonus = lenderReferralBonusUpdatedRepo
						.getIdOfReferrer(referral.getId());

				referralBonus.setPaymentStatus("Paid");
				try {
					referralBonus
							.setTransferredOn(expectedDateFormat.parse(lenderReferenceRequestDto.getTransferredOn()));
				} catch (ParseException e) {

					e.printStackTrace();
				}

				lenderReferralBonusUpdatedRepo.save(referralBonus);
			}
		}

		if (listOfLenderReferralResponse != null && !listOfLenderReferralResponse.isEmpty()) {
			for (LenderReferralResponse refereeId : listOfLenderReferralResponse) {
				LenderReferenceDetails details = lenderReferenceDetailsRepo
						.findingRefereeAndReferrerInfo(refereeId.getRefereeId());
				Double sumOfAmountUnpaid = lenderReferralBonusUpdatedRepo
						.findingSumOfAmountInUnPaidState(refereeId.getRefereeId());
				if (sumOfAmountUnpaid != null) {
					details.setAmount(sumOfAmountUnpaid);
				} else {
					details.setAmount(0.0);
				}

				lenderReferenceDetailsRepo.save(details);

				referenceResponse.setStatus("Successfully Updated");
			}
		}

		logger.info("readingResponseForReferenceBonus method ends....................");

		return referenceResponse;

	}

	public void dealCreationAndEditHistory(OxyBorrowersDealsInformation oxyBorrowersDealsInformation) {

		OxyBorrowersDealsInformationHistory oxyBorrowersDealsInformationHistory = new OxyBorrowersDealsInformationHistory();

		oxyBorrowersDealsInformationHistory.setBorrowerName(oxyBorrowersDealsInformation.getBorrowerName());

		oxyBorrowersDealsInformationHistory.setDealName(oxyBorrowersDealsInformation.getDealName());

		oxyBorrowersDealsInformationHistory
				.setBorrowerRateofinterest(oxyBorrowersDealsInformation.getBorrowerRateofinterest());

		oxyBorrowersDealsInformationHistory.setDealAmount(oxyBorrowersDealsInformation.getDealAmount());

		oxyBorrowersDealsInformationHistory.setDealId(oxyBorrowersDealsInformation.getId());

		oxyBorrowersDealsInformationHistory.setDuration(oxyBorrowersDealsInformation.getDuration());

		oxyBorrowersDealsInformationHistory
				.setFeeCollectedFromBorrower(oxyBorrowersDealsInformation.getFeeCollectedFromBorrower());

		oxyBorrowersDealsInformationHistory.setFeeROIForBorrower(oxyBorrowersDealsInformation.getFeeROIForBorrower());

		oxyBorrowersDealsInformationHistory
				.setFundsAcceptanceEndDate(oxyBorrowersDealsInformation.getFundsAcceptanceEndDate());

		oxyBorrowersDealsInformationHistory
				.setFundsAcceptanceStartDate(oxyBorrowersDealsInformation.getFundsAcceptanceStartDate());

		oxyBorrowersDealsInformationHistory
				.setLifeTimeWaiverLimit(oxyBorrowersDealsInformation.getLifeTimeWaiverLimit());

		oxyBorrowersDealsInformationHistory.setLifeTimeWaiver(oxyBorrowersDealsInformation.getLifeTimeWaiver());

		oxyBorrowersDealsInformationHistory.setRoiForWithdrawal(oxyBorrowersDealsInformation.getRoiForWithdrawal());

		oxyBorrowersDealsInformationHistory
				.setMinimumPaticipationAmount(oxyBorrowersDealsInformation.getMinimumPaticipationAmount());

		oxyBorrowersDealsInformationHistoryRepo.save(oxyBorrowersDealsInformationHistory);

	}
	
	
	public Credential getGmailCredentialss1(SpreadSheetRequestDto request)

			throws IOException, GeneralSecurityException, InterruptedException {
		// Load client secrets.
		logger.info("Gmail Credentials method started");
		
		String clientId="307164236410-eprs04dvs2uqn2ah5n43tjnaceup7eo9.apps.googleusercontent.com";
		String clientSecret="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";

		String code = URLDecoder.decode(request.getGmailCode(), "UTF-8");
		logger.info(" emailcode"+code);

		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

		GoogleClientSecrets clientSecrets = new GoogleClientSecrets();

		Details details = new Details();
		details.setClientId(clientId);
		details.setClientSecret(clientSecret);
		clientSecrets.setWeb(details);

		// Build flow and trigger user authorization request.
		List<String> scopes = Arrays.asList(SheetsScopes.SPREADSHEETS);

		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, scopes)
				.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
				.setAccessType("offline").build();

		Credential credential = flow.loadCredential("userId");

		TokenResponse response = null;
		logger.info(" Credentials response"+response);
		try {
			response = flow.newTokenRequest(code).setRedirectUri(spreadSheetRedirectUrl).execute();
			logger.info(" Credentials response"+response);

		} catch (Exception ex) {
			throw new DataFormatException("Invalid Code", ErrorCodes.DATA_FETCH_ERROR);
		}
		logger.info("Gmail Credentials method ends");
		return flow.createAndStoreCredential(response, "userId");

	}

	
}
