package com.oxyloans.service.loan;

import com.oxyloans.service.OxyRuntimeException;

public class ActionNotAllowedException extends OxyRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ActionNotAllowedException(String message, String errorCode) {
		super(message, errorCode);
	}

}
