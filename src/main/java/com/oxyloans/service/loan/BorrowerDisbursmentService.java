package com.oxyloans.service.loan;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oxyloans.cms.IciciCmsRequestDto;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.disbursment.BorrowerDisbusmentResponseDto;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.cms.OxyCmsLoans;
import com.oxyloans.entity.cms.OxyCmsLoans.FileExecutionStatus;
import com.oxyloans.repository.cms.OxyCmsLoansRepo;
import com.oxyloans.entity.user.User;
import com.oxyloans.excelservice.ExcelServiceRepo;
import com.oxyloans.repo.loan.OxyLoanRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.user.StatusResponseDto;
import com.oxyloans.service.OperationNotAllowedException;

@Service
public class BorrowerDisbursmentService implements BorrowerDisbursmentServiceRepo {

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Autowired
	private OxyLoanRepo oxyLoanRepo;

	@Autowired
	private OxyCmsLoansRepo oxyCmsLoansRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private ExcelServiceRepo excelServiceRepo;

	private SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-YYYY");

	@Override
	public BorrowerDisbusmentResponseDto applicationLevelFundsTransfer(UserRequest request) {

		int borrowerId = request.getBorrowerId();

		int dealId = request.getDealId();

		User user = userRepo.findByIdNum(borrowerId);

		IciciCmsRequestDto obj = new IciciCmsRequestDto();

		if (user != null) {
			Double totalDisbursmentAmount = oxyLoanRepo.sumOfBorrowerDisbursmentAmountToDeal(borrowerId, dealId);

			if (totalDisbursmentAmount == 0) {
				throw new OperationNotAllowedException("No amount to transfer", ErrorCodes.ENITITY_NOT_FOUND);
			}

			Double transferedAmount = oxyCmsLoansRepo.sumOfAmountTransfered(borrowerId, dealId);

			Double amountYetToTransfer = 0d;

			if (totalDisbursmentAmount.equals(transferedAmount)) {
				throw new OperationNotAllowedException("Funds already transfered", ErrorCodes.ACTION_ALREDY_DONE);
			}

			if (transferedAmount == 0) {
				amountYetToTransfer = totalDisbursmentAmount;
			}

			if (totalDisbursmentAmount > transferedAmount) {
				amountYetToTransfer = totalDisbursmentAmount - transferedAmount;
			}

			OxyCmsLoans cmsLoans = oxyCmsLoansRepo.findByUserIdAndDealId(dealId, borrowerId);

			if (cmsLoans != null) {
				if (cmsLoans.getAmount() == amountYetToTransfer) {
					throw new OperationNotAllowedException(
							"Excel already generated for the existing amount of " + cmsLoans.getAmount(),
							ErrorCodes.ACTION_ALREDY_DONE);
				}
			}

			if (cmsLoans == null) {

				cmsLoans = new OxyCmsLoans();

			}

			if (!request.isBorrowerFeePaid()) {

				double rateOfInterestForFee = (amountYetToTransfer * request.getPaymentFeeRateOfInterest()) / 100;
				double gst = (rateOfInterestForFee * 18) / 100;
				double borrowerFee = rateOfInterestForFee + gst;

				amountYetToTransfer = amountYetToTransfer - borrowerFee;

				obj.setAccountNumber("50200025316770");
				obj.setName("SRS FINTECHLABS PVT. LTD.");
				obj.setIfscCode("HDFC0004277");
				obj.setAmount(borrowerFee);
				obj.setDebitAccountNumber("777705849441");
				obj.setPaymentType("BORROWERFEE");
				obj.setUserUserId("BR" + user.getId());
				obj.setAddDetails5("OXYLOANS BorrowerFee");
				obj.setRemarks("BorrowerFee");
				obj.setDealId(dealId);

			}

			cmsLoans.setUserId(borrowerId);
			cmsLoans.setDealId(dealId);
			cmsLoans.setAmount(amountYetToTransfer);
			cmsLoans.setTransactionDate(new Date());
			cmsLoans.setTransactionStatus(FileExecutionStatus.INITIATED);

			cmsLoans = oxyCmsLoansRepo.save(cmsLoans);

			UserRequest userRequest = new UserRequest();
			userRequest.setNameAsPerBank(user.getBankDetails().getUserName());
			userRequest.setAccountNumber(user.getBankDetails().getAccountNumber());
			userRequest.setIfscCode(user.getBankDetails().getIfscCode());

			String paymentDate = DATE_FORMAT.format(Calendar.getInstance().getTime());

			StatusResponseDto response = excelServiceRepo.witeDealLevelLoanDisbursmentExcel(paymentDate, userRequest,
					cmsLoans, "null");

			if (response != null) {

				cmsLoans.setFileName(response.getFileName());
				oxyCmsLoansRepo.save(cmsLoans);

				if (obj != null && !request.isBorrowerFeePaid()) {
					StatusResponseDto feeResponse = excelServiceRepo.witeCmsExcel("OXYLR2_OXYL2UPLD_", obj, paymentDate,
							"null");
				}

			}

		}

		return new BorrowerDisbusmentResponseDto();

	}

}
