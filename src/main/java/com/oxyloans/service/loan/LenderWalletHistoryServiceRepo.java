package com.oxyloans.service.loan;

public interface LenderWalletHistoryServiceRepo {

	public String lenderTransactionHistory(int userId, double amount, String type, String subType, int dealId);

	public String calculatingOutstandingAmount(int userId, double amount, String type, String subType, int dealId);

}
