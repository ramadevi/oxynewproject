package com.oxyloans.service.loan;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.List;

import javax.mail.MessagingException;

import com.oxyloans.engine.template.PdfGeenrationException;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceDetails;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceResponse;
import com.oxyloans.entityborrowersdealsdto.BorrowerAutoLendingResponse;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsList;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsRequestDto;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsResponseDto;
import com.oxyloans.entityborrowersdealsdto.ClosedDealsInformation;
import com.oxyloans.entityborrowersdealsdto.DealInformationRoiToLender;
import com.oxyloans.entityborrowersdealsdto.DealLevelRequestDto;
import com.oxyloans.entityborrowersdealsdto.DealLevelResponseDto;
import com.oxyloans.entityborrowersdealsdto.LenderMappedToGroupIdResponseDto;
import com.oxyloans.entityborrowersdealsdto.LenderPaticipatedDeal;
import com.oxyloans.entityborrowersdealsdto.LendersDealsResponseDto;
import com.oxyloans.entityborrowersdealsdto.OxyLendersAcceptedDealsRequestDto;
import com.oxyloans.entityborrowersdealsdto.OxyLendersAcceptedDealsResponseDto;
import com.oxyloans.entity.lenders.group.OxyLendersGroup;
import com.oxyloans.entity.loan.LoanEmiCard;
import com.oxyloans.entity.loan.LoanRequest.LoanStatus;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.excelsheets.OxyTransactionDetailsFromExcelSheetsRequest;
import com.oxyloans.excelsheets.OxyTransactionDetailsFromExcelSheetsResponse;
import com.oxyloans.experian.ExperianRequestDto;
import com.oxyloans.lender.participationToWallet.LenderIncomeRequestDto;
import com.oxyloans.lender.wallet.LenderReturnsResponseDto;
import com.oxyloans.lender.wallet.ScrowWalletResponse;
import com.oxyloans.loan.LenderInvestmentDataDtoResponse;
import com.oxyloans.payumoney.PayuMoneyPaymentDetailsRequest;
import com.oxyloans.payumoney.PayuMoneyPaymentDetaisResponce;
import com.oxyloans.request.ApplicationLevelEnachResponseDto;
import com.oxyloans.request.LenderFavouriteRequestDto;
import com.oxyloans.request.PageDto;
import com.oxyloans.request.SearchRequestDto;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.request.user.KycFileResponse;
import com.oxyloans.request.user.LenderReferenceRequestDto;
import com.oxyloans.request.user.SpreadSheetRequestDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.request.user.WhatsappCampaignDto;
import com.oxyloans.response.admin.AdminEmiDetailsResponseDto;
import com.oxyloans.response.admin.DisbursmentPendingResponseDto;
import com.oxyloans.response.admin.UserInformationResponseDto;
import com.oxyloans.response.user.ActiveLenderResponseDto;
import com.oxyloans.response.user.BorrowerDetailsRequestDto;
import com.oxyloans.response.user.BorrowersFeeBasedOnDate;
import com.oxyloans.response.user.BorrowersLoanOwnerInformation;
import com.oxyloans.response.user.BorrowersLoanOwnerNames;
import com.oxyloans.response.user.CommentsRequestDto;
import com.oxyloans.response.user.CommentsResponseDto;
import com.oxyloans.response.user.EnachScheduledMailResponseDto;
import com.oxyloans.response.user.EnachTransactionResponseDto;
import com.oxyloans.response.user.EquityLendersListsDto;
import com.oxyloans.response.user.LenderEscrowWalletDto;
import com.oxyloans.response.user.LenderFavouriteResponseDto;
import com.oxyloans.response.user.LenderLoansInformation;
import com.oxyloans.response.user.LenderNocResponseDto;
import com.oxyloans.response.user.LendersLoanInfo;
import com.oxyloans.response.user.LoanDisbursmentResponse;
import com.oxyloans.response.user.OxyLendersGroups;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.response.user.PaymentHistoryResponseDto;
import com.oxyloans.response.user.PaymentSearchByStatus;
import com.oxyloans.response.user.PaymentUploadHistoryResponseDto;
import com.oxyloans.response.user.ReadingCommitmentAmountDto;
import com.oxyloans.response.user.StatusResponseDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.response.user.WhatappInformation;
import com.oxyloans.service.esign.EsignInitResponse;
import com.oxyloans.serviceloan.ApplicationResponseDto;
import com.oxyloans.serviceloan.BorrowerApplicationSummryDto;
import com.oxyloans.serviceloan.BorrowerIndividualLoansCountResposeDto;
import com.oxyloans.serviceloan.BucketPendingEmis;
import com.oxyloans.serviceloan.DurationChangeDto;
import com.oxyloans.serviceloan.LenderEmiDetailsResponseDto;
import com.oxyloans.serviceloan.LenderHistroryResponseDto;
import com.oxyloans.serviceloan.LenderIndividualLoansCountResposeDto;
import com.oxyloans.serviceloan.LenderTransactionHistoryResponseDto;
import com.oxyloans.serviceloan.LoanEmiCardPaymentDetailsRequestDto;
import com.oxyloans.serviceloan.LoanEmiCardPaymentDetailsResponseDto;
import com.oxyloans.serviceloan.LoanEmiCardResponseDto;
import com.oxyloans.serviceloan.LoanEmiGenerationResDto;
import com.oxyloans.serviceloan.LoanRequestDto;
import com.oxyloans.serviceloan.LoanResponseDto;
import com.oxyloans.serviceloan.NotificationCountResponseDto;
import com.oxyloans.serviceloan.OfferSentDetails;
import com.oxyloans.serviceloan.OxyDashboardDetailsDto;
import com.oxyloans.serviceloan.TotalLendersInformationResponseDto;
import com.oxyloans.serviceloan.UploadAgreementRequestDto;
import com.oxyloans.serviceloan.UsersLoansInformationResponseDto;
import com.oxyloans.whatsappdto.WhatsappMessagesDto;

public interface ILoanService<T extends OxyDashboardDetailsDto> {

	public T getDashboard(int userId, boolean current);

	public LoanResponseDto loanRequest(int userId, LoanRequestDto loanRequestDto);

	public LoanResponseDto updateRespondedLoanRequest(int userId, Integer loanRequestId, LoanRequestDto loanRequestDto);

	public LoanResponseDto updateLoanRequest(int userId, LoanRequestDto loanRequestDto);

	public LoanResponseDto updateLoanRequest(int userId, Integer loanRequestId,
			BorrowerDetailsRequestDto borrowerDetailsRequestDto);

	public LoanResponseDto getLoanRequest(int userId, int requestId);

	public LoanResponseDto getDefaultLoanRequest(int userId);

	public List<UserResponse> getParticipatedUsers(int requestId);

	public LoanResponseDto actOnRequest(int userId, Integer loanRequestId, LoanStatus loanStatus)
			throws PdfGeenrationException, FileNotFoundException, IOException;

	public SearchResultsDto<LoanResponseDto> searchRequests(int userId, SearchRequestDto searchRequestDto);

	public SearchResultsDto<LoanResponseDto> searchLoans(int userId, SearchRequestDto searchRequestDto);

	public KycFileResponse downloadLoanAgrement(int userId, Integer loanRequestId);

	public EsignInitResponse esign(int userId, Integer loanRequestId, UploadAgreementRequestDto agreementFileRequest);

	public LoanResponseDto uploadEsignedAgreementPdf(int userId, Integer loanRequestId,
			UploadAgreementRequestDto agreementFileRequest);

	public LoanResponseDto feePaid(int loanId, PrimaryType primaryType, LoanRequestDto loanRequestDto);

	public LoanEmiCardResponseDto emiPaid(int loanId, int emiNumber, LoanEmiCardResponseDto request);

	public LoanEmiCardResponseDto markEmiFailed(int loanId, int emiNumber);

	public SearchResultsDto<LoanEmiCardResponseDto> getEmiCard(int loanId);

	public void deleteLoans(int userId);

	public void migrateOldSystemToNewSystem();

	public void changeDefaultLoanPrimaryType(int userId, PrimaryType newPrimaryType);

	public LoanEmiCardResponseDto loanPreClose(int loanId);

	public SearchResultsDto<LoanResponseDto> pendingEmis(int year, int month, String type, int pageNo, int pageSize);

	public UserResponse updateUserStatus(int userId, LoanRequestDto loanRequestDto);

	// public UserResponse sendReport(int userId, UserRequest userRequest);
	public UserResponse sendReport(int userId, UserRequest userRequest) throws PdfGeenrationException, IOException;

	public LoanEmiCardResponseDto emiPaidByBorrowerUsingPaymentGateway(String oxyLoansTransactionNumber,
			LoanEmiCardResponseDto request);

	public LenderFavouriteResponseDto selectFavouriteBorrower(LenderFavouriteRequestDto lenderFavouriteRequestDto);

	public UserResponse BorrowerFee(int userId, UserRequest userRequest);

	public UserResponse calculateprofileRisk(int userId, UserRequest userRequest);

	public UserResponse getExperianAndProfile(int userId);

	public LoanResponseDto sendOffer(int userId, LoanRequestDto loanRequestDto) throws MessagingException;

	public LoanResponseDto getBorrowerStatements(int id, String type) throws PdfGeenrationException, IOException;

	public void loanApplicationScheduler();

	public LoanResponseDto newLoanRequest(int userId, LoanRequestDto loanRequestDto);

	// public LoanResponseDto borrowerDealStructure(int id) throws
	// PdfGeenrationException, IOException;

	public OfferSentDetails getOfferSendDetails(int id, int loanRequestId);

	public LoanResponseDto lenderDealStructure(String loanId) throws PdfGeenrationException, IOException;

	public UserResponse addingCreditScoreByPaisaBazaar(int userId, ExperianRequestDto experianRequestDto);

	public LoanResponseDto borrowerDealStructure(String loanId) throws PdfGeenrationException, IOException;

	public UserResponse getAllUtmFieldValues();

	public PayuMoneyPaymentDetaisResponce borrowerFeeUpdation(String oxyLoansTransactionNumber,
			LoanEmiCardResponseDto request);

	public LoanEmiCardPaymentDetailsResponseDto updatePaymentDetails(int emiId,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto);

	public List<LenderEmiDetailsResponseDto> getLenderEmiDetails(int userId);

	public void disbursedLoans();

	public List<LenderHistroryResponseDto> getLenderHistory(int userId);

	public LenderIndividualLoansCountResposeDto getloanDetails(int userId, PageDto pageDto)
			throws PdfGeenrationException, IOException;

	public LoanEmiCardPaymentDetailsResponseDto updEmisByApplication(
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto);

	public UsersLoansInformationResponseDto getLenderLoansInformationPdf(int userId);

	public SearchResultsDto featurePending(int year, int month, SearchRequestDto searchRequestDto);

	public BorrowerIndividualLoansCountResposeDto getloanDetailsForBorrower(int borrowerUserId, PageDto pageDto)
			throws PdfGeenrationException, IOException;

	public UsersLoansInformationResponseDto getBorrowerLoansInformationPdf(int userId);

	public LoanResponseDto manualEsignProcess(String loanId);

	public SearchResultsDto<LoanResponseDto> bucketPendingEmis(int start, int end, String type, int pageNo,
			int pageSize);

	public NotificationCountResponseDto getAdminPendingActionsDetails();

	public LoanResponseDto rejectLoanOffer(int id);

	public void calculatingPenalityUsingSchedular();

	public int getUnpaidLoanEmiDetails(int loanId,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto);

	public LoanResponseDto sendingEmailsBeforeEmiDate();

	public List<AdminEmiDetailsResponseDto> getEmiDetails();

	public LenderTransactionHistoryResponseDto generateLenderHistoryPdf(int userId)
			throws PdfGeenrationException, IOException;

	public BucketPdfresponse thirtyDaysBucketDownload() throws PdfGeenrationException, IOException;

	public BucketPdfresponse sixtyDaysBucketDownload() throws PdfGeenrationException, IOException;

	public BucketPdfresponse ninetyDaysBucketDownload() throws PdfGeenrationException, IOException;

	public SearchResultsDto<LoanResponseDto> PendingEmisWithUserId(int start, int end, String type, int userId,
			int pageNo, int pageSize);

	public SearchResultsDto<LoanResponseDto> emiPaidwithUserId(int year, int month, String type, int userId, int pageNo,
			int pageSize);

	public int getAdminPendingActionsDetailsSchedular();

	public BucketPdfresponse npaBucketDownload() throws PdfGeenrationException, IOException;

	public UsersLoansInformationResponseDto searchForLenderAndBorrowerInfo(int userId);

	public TotalLendersInformationResponseDto getLendersLoansInformation(PageDto pageDto);

	public TotalLendersInformationResponseDto getBorrowersLoansInformation(PageDto pageDto);

	public SearchResultsDto<ApplicationResponseDto> bucketsByApplication(int start, int end, String type, int pageNo,
			int pageSize);

	public SearchResultsDto<ApplicationResponseDto> bucketsByApplicationByUserId(int start, int end, String type,
			int userId);

	public SearchResultsDto<BucketPendingEmis> bucketsEmiPending(int start, int end, String type, int loanId);

	public SearchResultsDto<LoanResponseDto> pendingEmisApplicationLevel(int year, int month, String type, int pageNo,
			int pageSize);

	public LoanResponseDto SearchByParentRequestId(String borrowerParentRequestId);

	public LoanResponseDto SearchByBorrowerId(int borrowerId);

	public SearchResultsDto<LoanResponseDto> pendingEmisBasedOnLoanLevel(int year, int month, String type, int loanId);

	public LoanEmiGenerationResDto getLoanEmiCard(int loanId, LoanEmiCardResponseDto loanEmiCardResponseDto);

	public String makingPenalityValueZero(int loanId, int emiNumber);

	public String updatingEmiPaidOnDate(int loanId, int emiNumber, LoanEmiCard loanEmiCard);

	public String deleteExtraPaidEmiDetails(int loanId, int emiNumber);

	public List<LoanEmiCardResponseDto> getApplicationLevelPenalityWithEmi(int borrowerApplicationId);

	public LoanEmiCardResponseDto calculatePendingPenaltyTillDate(int borrowerApplicationId);

	public int countValue(int loanId);

	public OxyDashboardDetailsDto borrowerDashBordDetails(int userId);

	public KycFileResponse lenderReport(Integer lenderParentRequestId);

	public LoanEmiCardResponseDto getBorrowerApplicationDetails(int borrowerApplicationId);

	public List<BorrowerApplicationSummryDto> borrowerApplictionSummary(int userId);

	// public String calculatingInterestAmount(int borrowerId);

	public ApplicationResponseDto emiDetails(int parentRequestID);

	public String sendingNotificationToLender();

	public KycFileResponse getDataFromExcel();

	public String calculatingInterestAmount();

	public LoanEmiCardResponseDto getInterestPendingTillDate(int loanId);

	public LoanEmiCardResponseDto interestWaiveOff(int loanId, LoanEmiCardResponseDto loanEmiCardResponseDto);

	public String enachSchedulingToOldBorrowers(int loanId);

	public BorrowerDetailsRequestDto getBorrowerComments(int userId);

	public String updateingEnachBasedOnEnachResponce();

	public CommentsResponseDto uploadCommentsFromRadhaSir(int id, CommentsRequestDto commentsRequestDto);

	public PaymentSearchByStatus getBorrowerPaymentList(String borrowerUniqueNumber, String paymentStatus);

	public BorrowersFeeBasedOnDate getBorrowersFreeBasedOnGivenDate(int month);

	public String calculatingLoanLevelFeeForOldBorrower(int applicationId);

	public EnachScheduledMailResponseDto sendEnachScheduleEmailExcel();

	public EnachTransactionResponseDto enachSuccessVerificationList();

	public PaymentHistoryResponseDto getEcsAndNonEcsPaymentHistory(String monthName, String year);

	public LenderEscrowWalletDto getLenderScrowWalletDetails(String date1, String date2);

	public DurationChangeDto incresingDurationToExistingBorrower(int userId);

	public ActiveLenderResponseDto getAllLenderWalletInfo(PaginationRequestDto pageRequestDto);

	public LenderLoansInformation gettingLenderLoanInformation(Integer currentMonth, Integer year);

	public LendersLoanInfo gettingLendersLoanInfoBasedOnMonthAndYear(Integer currentMonth, Integer year,
			PaginationRequestDto paginationRequestDto);

	public List<LenderLoansInformation> getLendersDetailedLoanInfo(Integer lenderId);

	public LendersLoanInfo getLenderInfoBasedOnId(Integer lenderId, PaginationRequestDto paginationRequestDto);

	public StatusResponseDto rejectingLoanByLoanId(int loanId);

	public LenderNocResponseDto getLendersInformation(int userId, LenderIncomeRequestDto lenderIncomeRequestDto)
			throws FileNotFoundException;

	public LenderNocResponseDto getLenderProfitValue(int userId);

	public LenderNocResponseDto generateLenderProfitPdf(int userId) throws FileNotFoundException;

	public LoanDisbursmentResponse updateingDisbursedDateBasedOnExcelSheet(String excelUploadedDate)
			throws PdfGeenrationException, IOException;

	public DisbursmentPendingResponseDto getDisbursmentPendingLoansDetails(PaginationRequestDto paginationRequestDto);

	public LenderReferenceResponse displayingAllLenderReferenceDetailsInfo(PaginationRequestDto pageRequestDto);

	public LoanDisbursmentResponse approvingAllLoansInApplicationLevel(int borrowerId, UserRequest userRequest)
			throws PdfGeenrationException, IOException;

	public UserInformationResponseDto searchByLoanIdDisbursmentPending(String loanId);

	public DisbursmentPendingResponseDto searchByLenderIdAndBorrowerId(int userId);

	public String sendingMailsToNormalEmisPendingFromAdmin();

	public String sendingMailsToCriticalEmisPending();

	public String sendingMailsToNormalEmisPendingFromCRM();

	public LoanDisbursmentResponse settingDisbursmentDatesForAllLoans(int userId, UserRequest userRequest)
			throws PdfGeenrationException, IOException;

	public BorrowerIndividualLoansCountResposeDto loansForBorrowerApplication(int borrowerId);

	public BorrowersLoanOwnerInformation getLoanOwners();

	public BorrowersLoanOwnerInformation gettingBorrowerDetailedLoanInfo(String loanOwner);

	public OxyTransactionDetailsFromExcelSheetsResponse readingLendersOutGoingAmount(String date, String deditedType);

	public OxyTransactionDetailsFromExcelSheetsResponse updatingExcelSheet(
			OxyTransactionDetailsFromExcelSheetsRequest oxyTransactionDetailsFromExcelSheetsRequest);

	public LenderReturnsResponseDto getLenderNewDashBoard(int userId);

	public CommentsResponseDto updatingLoanOwnerToBorrower(int userId, BorrowersLoanOwnerNames borrowersLoanOwnerNames);

	public EnachScheduledMailResponseDto readingLenderReferenceInfoToExcelSheet() throws ParseException;

	public LenderReferenceResponse gettingLenderReferralBonusDetails();

	public LenderReferenceResponse readingResponseForReferenceBonus(LenderReferenceRequestDto lenderReferenceRequestDto)
			throws ParseException;

	public LenderReferenceResponse readingCommentsOfRadhaSir(LenderReferenceRequestDto lenderReferenceRequestDto)
			throws ParseException;

	public BorrowersDealsResponseDto updateBorrowerDealsInformation(BorrowersDealsRequestDto borrowersDealsRequestDto);

	public BorrowersDealsResponseDto getSingleDeal(int dealId);

	public LenderMappedToGroupIdResponseDto getListOfLendersMappedToGroupId(int groupId);

	public LendersDealsResponseDto listOfDealsInformationDispayingToLender(int userId,
			PaginationRequestDto pageRequestDto);

	public OxyLendersAcceptedDealsResponseDto updateLenderDealInformation(
			OxyLendersAcceptedDealsRequestDto oxyLendersAcceptedDealsRequestDto);

	public DealInformationRoiToLender getSigleDealInformation(int userId, int dealId);

	public LenderPaticipatedDeal getLenderPaticipatedDealsInformation(int userId, PaginationRequestDto pageRequestDto);

	public LenderPaticipatedDeal getListOfLendersByDealId(int dealId);

	public PayuMoneyPaymentDetaisResponce lenderFeeUpdation(String transactionNumber,
			PayuMoneyPaymentDetailsRequest request);

	public PayuMoneyPaymentDetaisResponce lenderFeePayments(String transactionNumber,
			PayuMoneyPaymentDetailsRequest request);

	public DealLevelResponseDto dealIdBasedAgreementsGeneration(int dealId, DealLevelRequestDto dealLevelRequestDto)
			throws PdfGeenrationException, IOException;

	public WhatappInformation getWhatappGroupsInformation(String type);

	public ScrowWalletResponse gettingLenderSrowWalletTransactions(int id, PaginationRequestDto pageRequestDto);

	public LenderReferenceDetails toEditTheLenderReferenceInformation(Integer refereeId, Integer referrerId,
			LenderReferenceRequestDto requestDto);

	public List<OxyLendersGroup> gettingListOfOxyLendersGroups();

	// public CommentsResponseDto updatingLenderToLendersGroup(Integer userId,
	// OxyLendersGroups oxyLenderGroup);

	public LenderMappedToGroupIdResponseDto gettingListOfLendersMappedToGroupName(OxyLendersGroups groupName);

	public LoanEmiCardResponseDto loanPreCloseByPlatForm(int loanId);

	public SearchResultsDto<LoanResponseDto> searchEnachMandate(int userId, SearchRequestDto searchRequestDto);

	public List<PaymentUploadHistoryResponseDto> readingWhatsAppPaymentScreenshots(WhatsappMessagesDto request)
			throws IOException, ParseException;

	public ReadingCommitmentAmountDto readingCommittmentAmount(BorrowersDealsRequestDto borrowersDealsRequestDto);

	public LenderMappedToGroupIdResponseDto gettingLenderCurrentWalletBalanceInfo(OxyLendersGroups oxyLendersGroups);

	public LenderMappedToGroupIdResponseDto gettingLenderCurrentWalletBalanceExcelSheet(
			OxyLendersGroups oxyLendersGroups);

	public BorrowersDealsList getListOfDealsInformation(PaginationRequestDto pageRequestDto);

	public WhatappInformation gettingListOfDealsNames();

	public WhatsappCampaignDto sendwhatsAppCampaignMessage(WhatsappCampaignDto whatsappCampaignDto);

	public EnachScheduledMailResponseDto sendingMailToReferrerAboutReference1();

	public EquityLendersListsDto lendersInAllEquityDeals();

	public StatusResponseDto getBorrowersMsgContent(String type);

	public StatusResponseDto sendNotoficationsToBorrowerFromAdmin(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException;

	public StatusResponseDto getUnsentBorrowerNotifications(SpreadSheetRequestDto request)
			throws GeneralSecurityException, IOException, InterruptedException;

	public StatusResponseDto writeNotificationsResponseMessages(SpreadSheetRequestDto request)
			throws GeneralSecurityException, IOException, InterruptedException, ParseException;

	public WhatappInformation gettingListOfWhatsAppGroupNamesBasedOnGroupName(String groupName);

	public ClosedDealsInformation getLenderClosedDealsInfo(int userId, PaginationRequestDto pageRequestDto);

	public LenderPaticipatedDeal getListOfLendersByDealIdForExcelSheet(int dealId);

	public LenderReferenceResponse readingLenderReferenceToSheet() throws ParseException;

	public DealLevelResponseDto dealIdBasedAgreementsGenerationForZaggle(int dealId,
			DealLevelRequestDto dealLevelRequestDto) throws PdfGeenrationException, IOException;

	public LoanResponseDto uploadEsignedAgreementPdfforDeal(int userId, Integer loanRequestId,
			UploadAgreementRequestDto agreementFileRequest);

	public LoanResponseDto updateLoanRequestNewMethod(int userId, LoanRequestDto loanRequestDto);

	public LenderMappedToGroupIdResponseDto gettingLenderCurrentWalletBalanceExcelSheetTwo(
			OxyLendersGroups oxyLendersGroups);

	public EsignInitResponse esignForApplicationLevel(Integer userId, Integer applicationId,
			UploadAgreementRequestDto agreementFileRequest);

	public LoanResponseDto uploadEsignedAgreementPdfforDealInApplicationLevel(int userId, Integer applicationId,
			UploadAgreementRequestDto agreementFileRequest);

	public List<ApplicationLevelEnachResponseDto> searchEnachMandateApplicationLevel(int userId);

	public String updatingAmountBasedOnDisbursment(Integer applicationId,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto);

	public String sendingEmailNotifiaction(int dealId);

	public String autoPaticiaption(int dealId);

	public DealLevelRequestDto automaticSendingOffer(BorrowersDealsRequestDto borrowersDealsRequestDto);

	public EnachScheduledMailResponseDto readingLenderReferenceInfoToExcelSheet1() throws ParseException;

	public LenderReferenceResponse readingLenderReferenceToSheet1() throws ParseException;

	public LenderReferenceResponse readingCommentsOfRadhaSir1(LenderReferenceRequestDto lenderReferenceRequestDto)
			throws ParseException;

	public LenderReferenceResponse readingResponseForReferenceBonus1(
			LenderReferenceRequestDto lenderReferenceRequestDto) throws ParseException;

	public LenderInvestmentDataDtoResponse lenderTotalInvestmentData(Integer userId);

	public BorrowerAutoLendingResponse getLendersUsingAutoLending();

}
