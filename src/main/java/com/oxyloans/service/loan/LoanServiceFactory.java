package com.oxyloans.service.loan;

public interface LoanServiceFactory {
	
	@SuppressWarnings("rawtypes")
	public ILoanService getService(String primaryRole);

}
