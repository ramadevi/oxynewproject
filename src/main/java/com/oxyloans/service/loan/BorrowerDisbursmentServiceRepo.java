package com.oxyloans.service.loan;

import com.oxyloans.disbursment.BorrowerDisbusmentResponseDto;
import com.oxyloans.request.user.UserRequest;

public interface BorrowerDisbursmentServiceRepo {

	public BorrowerDisbusmentResponseDto applicationLevelFundsTransfer(UserRequest request);

}
