package com.oxyloans.service.loan;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;

import com.amazonaws.services.organizations.model.AccessDeniedException;
import com.google.gson.Gson;
import com.oxyloans.autoinvest.entity.LenderAutoInvestConfig;
import com.oxyloans.autoinvest.repo.LenderAutoInvestConfigRepo;
import com.oxyloans.autoinvestservice.LenderAutoInvestConfigServive;
import com.oxyloans.commonutil.DateUtil;
import com.oxyloans.customexceptions.DataFormatException;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.customexceptions.IllegalValueException;
import com.oxyloans.dashboard.LenderWalletHistoryResponseDto;
import com.oxyloans.engine.template.IPdfEngine;
import com.oxyloans.engine.template.PdfGeenrationException;
import com.oxyloans.engine.template.TemplateContext;
import com.oxyloans.entity.EsignTransactions;
import com.oxyloans.entity.EsignTransactions.EsignType;
import com.oxyloans.entity.EsignTransactions.TransactonStatus;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceDetails;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceResponse;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferralBonusUpdated;
import com.oxyloans.entityborrowersdealsdto.BorrowerAutoLendingResponse;
//import com.oxyloans.entity.LenderReferenceDetails.LenderReferralBonus;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsList;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsRequestDto;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsResponseDto;
import com.oxyloans.entityborrowersdealsdto.DealInformationRoiToLender;
import com.oxyloans.entityborrowersdealsdto.DealLevelRequestDto;
import com.oxyloans.entityborrowersdealsdto.DealLevelResponseDto;
import com.oxyloans.entityborrowersdealsdto.LenderMappedToGroupIdResponseDto;
import com.oxyloans.entityborrowersdealsdto.LenderPaticipatedDeal;
import com.oxyloans.entityborrowersdealsdto.LenderPaticipatedResponseDto;
import com.oxyloans.entityborrowersdealsdto.LendersDealsResponseDto;
import com.oxyloans.entityborrowersdealsdto.ListOfDealsInformationToLender;
import com.oxyloans.entityborrowersdealsdto.OxyLendersAcceptedDealsRequestDto;
import com.oxyloans.entityborrowersdealsdto.OxyLendersAcceptedDealsResponseDto;
import com.oxyloans.entity.borrowers.deals.information.LendersPaticipationUpdation;
import com.oxyloans.entity.borrowers.deals.information.LendersPaticipationUpdationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation.ParticipationLenderType;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyDealsRateofinterestToLendersgroup;
import com.oxyloans.entity.borrowers.deals.information.OxyDealsRateofinterestToLendersgroupRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDeals;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDeals.FeeStatus;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDeals.LenderReturnsType;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDealsRepo;
import com.oxyloans.entity.enach.ApplicationLevelEnachMandate;
import com.oxyloans.repository.enach.ApplicationLevelEnachMandateRepo;
import com.oxyloans.repository.enach.EmandateResponseRepo;
import com.oxyloans.entity.enach.EnachMandate;
import com.oxyloans.entity.enach.EnachMandate.AmountType;
import com.oxyloans.entity.enach.EnachMandate.Frequency;
import com.oxyloans.entity.enach.EnachMandate.MandateStatus;
import com.oxyloans.entity.enach.EnachMandateResponse;
import com.oxyloans.repository.excelsheets.OxyTransactionDetailsFromExcelSheetsRepo;
import com.oxyloans.entity.experian.ExperianSummary;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletNativeRepo;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletRepo;
import com.oxyloans.entity.lender.payu.LenderPayuDetails;
import com.oxyloans.entity.lender.payu.LenderPayuDetails.PayuStatus;
import com.oxyloans.entity.lender.payu.LenderPayuDetailsRepo;
import com.oxyloans.entity.lender.payu.LenderRenewalDetails;
import com.oxyloans.entity.lender.payu.LenderRenewalDetailsRepo;
import com.oxyloans.entity.lender.returns.LendersReturnsRepo;
import com.oxyloans.entity.lenders.group.OxyLendersGroup;
import com.oxyloans.repository.lendergroup.OxyLendersGroupRepo;
import com.oxyloans.repository.lenderhold.UserHoldAmountMappedToDealRepo;
import com.oxyloans.entity.loan.InterestDetails;
import com.oxyloans.entity.loan.LenderBorrowerConversation;
import com.oxyloans.entity.loan.LoanEmiCard;
import com.oxyloans.entity.loan.LoanEmiCardPaymentDetails;
import com.oxyloans.entity.loan.LoanOfferdAmount;
import com.oxyloans.entity.loan.LoanOfferdAmount.LoanOfferdStatus;
import com.oxyloans.entity.loan.LoanRequest;
import com.oxyloans.entity.loan.LoanRequest.DurationType;
import com.oxyloans.entity.loan.LoanRequest.LoanStatus;
import com.oxyloans.entity.loan.LoanRequest.RepaymentMethod;
import com.oxyloans.entity.loan.OxyLoan;
import com.oxyloans.entity.loan.PenaltyDetails;
import com.oxyloans.entity.user.Nominee;
import com.oxyloans.entity.user.PersonalDetails;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.entity.user.User.Status;
import com.oxyloans.entity.user.UserProfileRisk;
import com.oxyloans.entity.user.Document.status.UserDocumentStatus;
import com.oxyloans.entity.user.Document.status.UserDocumentStatus.DocumentType;
import com.oxyloans.repository.userdocumentstatus.UserDocumentStatusRepo;
import com.oxyloans.entity.user.LenderFavouriteUsers.LenderFavouriteUsers;
import com.oxyloans.entity.user.LenderFavouriteUsers.LenderFavouriteUsers.FavouriteType;
import com.oxyloans.repository.lenderfavouriteusersrepo.LenderFavouriteUsersRepo;
import com.oxyloans.entity.user.type.OxyUserType;
import com.oxyloans.entity.user.type.OxyUserTypeRepo;
import com.oxyloans.excelsheets.OxyTransactionDetailsFromExcelSheetsRequest;
import com.oxyloans.excelsheets.OxyTransactionDetailsFromExcelSheetsResponse;
import com.oxyloans.experian.ExperianRequestDto;
import com.oxyloans.file.FileRequest;
import com.oxyloans.file.FileRequest.FileType;
import com.oxyloans.file.FileResponse;
import com.oxyloans.jpa.specification.ISpecificationProvider;
import com.oxyloans.lender.participationToWallet.LenderIncomeRequestDto;
import com.oxyloans.lender.wallet.LenderCreditedDetails;
import com.oxyloans.lender.wallet.LenderIndividualReturns;
import com.oxyloans.lender.wallet.LenderReturnsResponseDto;
import com.oxyloans.lender.wallet.ScrowWalletResponse;
import com.oxyloans.loan.LenderInvestmentDataDtoResponse;
import com.oxyloans.mobile.MobileRequest;
import com.oxyloans.mobile.MobileUtil;
import com.oxyloans.mobile.twoFactor.MobileOtpVerifyFailedException;
import com.oxyloans.notifications.NotificationsRepo;
import com.oxyloans.partner.service.PartnerService;
import com.oxyloans.payumoney.PayuMoneyPaymentDetailsRequest;
import com.oxyloans.payumoney.PayuMoneyPaymentDetaisResponce;
import com.oxyloans.repo.EsignTransactionsRepo;
import com.oxyloans.repo.enach.EnachMandateRepo;
import com.oxyloans.repo.loan.AddressDetailsRepo;
import com.oxyloans.repo.loan.InterestDetailsRepo;
import com.oxyloans.repo.loan.LenderBorrowerConversationRepo;
import com.oxyloans.repo.loan.LenderRatingToBorrowerApplicationRepo;
import com.oxyloans.repo.loan.LoanEmiCardPaymentDetailsRepo;
import com.oxyloans.repo.loan.LoanEmiCardRepo;
import com.oxyloans.repo.loan.LoansByApplicationNativeRepo;
import com.oxyloans.repo.loan.OxyLoanRepo;
import com.oxyloans.repo.loan.OxyLoanRequestRepo;
import com.oxyloans.repo.loan.PenaltyDetailsRepo;
import com.oxyloans.repo.user.LenderReferenceDetailsRepo;
//import com.oxyloans.repo.user.LenderReferralBonusRepo;
import com.oxyloans.repo.user.LenderReferralBonusUpdatedRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.ApplicationLevelEnachResponseDto;
import com.oxyloans.request.LenderFavouriteRequestDto;
import com.oxyloans.request.PageDto;
import com.oxyloans.request.SearchRequestDto;
import com.oxyloans.request.SearchRequestDto.LogicalOperator;
import com.oxyloans.request.SearchRequestDto.Operator;
import com.oxyloans.request.SearchResultsDto;
import com.oxyloans.request.user.KycFileRequest.KycType;
import com.oxyloans.request.user.KycFileResponse;
import com.oxyloans.request.user.LenderReferenceRequestDto;
import com.oxyloans.request.user.SpreadSheetRequestDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.request.user.WhatsappCampaignDto;
import com.oxyloans.response.admin.AdminEmiDetailsResponseDto;
import com.oxyloans.response.admin.DisbursmentPendingResponseDto;
import com.oxyloans.response.admin.UserInformationResponseDto;
import com.oxyloans.response.user.ActiveLenderResponseDto;
import com.oxyloans.response.user.BorrowerDetailsRequestDto;
import com.oxyloans.response.user.BorrowersFeeBasedOnDate;
import com.oxyloans.response.user.BorrowersLoanOwnerInformation;
import com.oxyloans.response.user.BorrowersLoanOwnerNames;
import com.oxyloans.response.user.CommentsRequestDto;
import com.oxyloans.response.user.CommentsResponseDto;
import com.oxyloans.response.user.EmiRequestDto;
import com.oxyloans.response.user.EnachMandateResponseForEmi;
import com.oxyloans.response.user.EnachMandateResponseForPrincipal;
import com.oxyloans.response.user.EnachScheduledMailResponseDto;
import com.oxyloans.response.user.EnachTransactionResponseDto;
import com.oxyloans.response.user.EquityLendersListsDto;
import com.oxyloans.response.user.LenderEmiDetails;
import com.oxyloans.response.user.LenderEscrowWalletDto;
import com.oxyloans.response.user.LenderFavouriteResponseDto;
import com.oxyloans.response.user.LenderLoansInformation;
import com.oxyloans.response.user.LenderNocResponseDto;
import com.oxyloans.response.user.LendersLoanInfo;
import com.oxyloans.response.user.ListOfWhatappGroupNames;
import com.oxyloans.response.user.LoanDisbursmentResponse;
import com.oxyloans.response.user.NomineeResponseDto;
import com.oxyloans.response.user.OxyLendersGroups;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.response.user.PaymentHistoryResponseDto;
import com.oxyloans.response.user.PaymentSearchByStatus;
import com.oxyloans.response.user.PaymentUploadHistoryResponseDto;
import com.oxyloans.response.user.PersonalDetailsResponseDto;
import com.oxyloans.response.user.ReadingCommitmentAmountDto;
import com.oxyloans.response.user.RiskProfileDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.response.user.WhatappInformation;
import com.oxyloans.security.authorization.IAuthorizationService;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.emailservice.EmailRequest;
import com.oxyloans.emailservice.EmailResponse;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.service.esign.EsignFetchResponse;
import com.oxyloans.service.esign.EsignInitRequest;
import com.oxyloans.service.esign.EsignInitResponse;
import com.oxyloans.service.esign.IEsignService;
import com.oxyloans.service.file.IFileManagementService;
import com.oxyloans.serviceloan.ApplicationResponseDto;
import com.oxyloans.serviceloan.BorrowerApplicationSummryDto;
import com.oxyloans.serviceloan.BorrowerIndividualLoansCountResposeDto;
import com.oxyloans.serviceloan.BucketPendingEmis;
import com.oxyloans.serviceloan.DurationChangeDto;
import com.oxyloans.serviceloan.LenderEmiDetailsResponseDto;
import com.oxyloans.serviceloan.LenderHistroryResponseDto;
import com.oxyloans.serviceloan.LenderIndividualLoansCountResposeDto;
import com.oxyloans.serviceloan.LenderTransactionHistoryResponseDto;
import com.oxyloans.serviceloan.LoanEmiCardPaymentDetailsRequestDto;
import com.oxyloans.serviceloan.LoanEmiCardPaymentDetailsResponseDto;
import com.oxyloans.serviceloan.LoanEmiCardResponseDto;
import com.oxyloans.serviceloan.LoanEmiGenerationResDto;
import com.oxyloans.serviceloan.LoanRequestDto;
import com.oxyloans.serviceloan.LoanResponseDto;
import com.oxyloans.serviceloan.NotificationCountResponseDto;
import com.oxyloans.serviceloan.OfferSentDetails;
import com.oxyloans.serviceloan.OxyAccountDetailsDto;
import com.oxyloans.serviceloan.OxyDashboardDetailsDto;
import com.oxyloans.serviceloan.StatementPdfResponseDto;
import com.oxyloans.serviceloan.StatusResponseDto;
import com.oxyloans.serviceloan.TotalLendersInformationResponseDto;
import com.oxyloans.serviceloan.UploadAgreementRequestDto;
import com.oxyloans.serviceloan.UsersLoansInformationResponseDto;
import com.oxyloans.service.user.UserServiceImpl;
import com.oxyloans.whatapp.service.WhatappService;
import com.oxyloans.whatapp.service.WhatappServiceRepo;
import com.oxyloans.whatsappdto.WhatsappMessagesDto;

public abstract class AbstractLoanService<T extends OxyDashboardDetailsDto> implements ILoanService<T> {

	private final Logger logger = LogManager.getLogger(getClass());

	protected static final String DATE_FORMAT = "dd-MM-yyyy";

	protected final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-YYYY hh:mm:ss");

	protected final SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	@Autowired
	private WhatappServiceRepo whatappServiceRepo;

	@Autowired
	protected UserRepo userRepo;

	@Autowired
	protected UserDocumentStatusRepo userDocumentStatusRepo;

	@Autowired
	protected OxyLoanRepo oxyLoanRepo;

	@Autowired
	protected OxyLoanRequestRepo loanRequestRepo;

	@Value("${interestAmountPerDay}")
	private double interestAmountPerDay;

	@Autowired
	protected DateUtil dateUtil;

	@Autowired
	protected LenderBorrowerConversationRepo lenderBorrowerConversationRepo;

	protected final DateFormat expectedDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	private final String LOAN_REQUEST_IDFORMAT = "AP{type}{ID}";

	private final String LOAN_ID_FORMAT = "LN{ID}";

	@Autowired
	protected ISpecificationProvider specificationProvider;

	@Autowired
	protected IPdfEngine pdfEngine;

	@Autowired
	protected IFileManagementService fileManagementService;

	@Autowired
	protected IAuthorizationService authorizationService;

	@Autowired
	protected AddressDetailsRepo addressDetailsRepo;

	@Autowired
	protected LoanEmiCardRepo loanEmiCardRepo;

	@Autowired
	protected MobileUtil mobileUtil;

	@Value("${minimumRequestAmount}")
	private String minimumRequestAmount;

	@Value("${maximumRequestAmount}")
	private String maximumRequestAmount;

	@Value("${maximumOutstandingAmount}")
	private String maximumOutstandingAmount;

	@Value("${defaultProfilePicUrl}")
	private String defaultProfilePicUrl;

	@Value("${defaultEmiDay}")
	private String defaultEmiDay;

	@Value("${midOftheMonth}")
	private String midOftheMonth;

	@Value("${loanRequestMailSubject}")
	private String loanRequestMailSubject;

	@Value("${esignTemplateName}")
	private String esignTemplateName;

	@Value("${supportEmail}")
	private String supportEmail;

	@Value("${intpaymentCal}")
	private String intpaymentCal;

	@Value("${firstInterestPaymentlink}")
	private String firstInterestPaymentlink;

	@Autowired
	protected LenderPayuDetailsRepo lenderPayuDetailsRepo;

	@Autowired
	private EmandateResponseRepo emandateResponseRepo;

	@Autowired
	private IEmailService emailService;

	@Autowired
	private LenderOxyWalletNativeRepo lenderOxyWalletNativeRepo;

	@Autowired
	private IEsignService esignService;

	@Autowired
	private EsignTransactionsRepo esignTransactionsRepo;

	@Autowired
	protected EnachMandateRepo enachMandateRepo;

	@Autowired
	private LenderFavouriteUsersRepo lenderFavouriteUsersRepo;

	@Autowired
	private LoanEmiCardPaymentDetailsRepo loanEmiCardPaymentDetailsRepo;

	@Autowired
	private LenderAutoInvestConfigRepo lenderAutoInvestConfigRepo;

	@Autowired
	private LenderOxyWalletRepo lenderOxyWalletRepo;

	@Value("${sendingEmailBeforeThreeDays}")
	private String sendingEmailBeforeThreeDays;

	@Value("${sendingEmailBeforeSevenDays}")
	private String sendingEmailBeforeSevenDays;

	@Autowired
	private PenaltyDetailsRepo penaltyDetailsRepo;

	@Autowired
	private InterestDetailsRepo interestDetailsRepo;

	@Autowired
	private LoansByApplicationNativeRepo loansByApplicationNativeRepo;

	@Value("${utm}")
	private String utm;

	@Autowired
	private UserServiceImpl userServiceImpl;

	@Autowired
	private LenderRatingToBorrowerApplicationRepo lenderRatingToBorrowerApplicationRepo;

	@Value("${referrerLink}")
	private String referrerLink;

	@Autowired
	private LendersReturnsRepo lendersReturnsRepo;

	@Autowired
	private OxyTransactionDetailsFromExcelSheetsRepo oxyTransactionDetailsFromExcelSheetsRepo;

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Autowired
	private OxyDealsRateofinterestToLendersgroupRepo oxyDealsRateofinterestToLendersgroupRepo;

	@Autowired
	private OxyLendersGroupRepo oxyLendersGroupRepo;

	@Autowired
	private OxyLendersAcceptedDealsRepo oxyLendersAcceptedDealsRepo;

	@Value("${wtappApi}")
	private String wtappApi;

	@Autowired
	private WhatappService whatappService;

	private Client client = ClientBuilder.newClient().register(MultiPartFeature.class).register(MultiPartWriter.class);

	@Autowired
	private LenderReferenceDetailsRepo lenderReferenceDetailsRepo;

	/*
	 * @Autowired private LenderReferralBonusRepo lenderReferralBonusRepo;
	 */
	@Autowired
	private LendersPaticipationUpdationRepo lendersPaticipationUpdationRepo;

	@Autowired
	private NotificationsRepo notifications;

	@Autowired
	private OxyUserTypeRepo oxyUserTypeRepo;

	@Autowired
	private LenderRenewalDetailsRepo lenderRenewalDetailsRepo;

	@Autowired
	private LenderReferralBonusUpdatedRepo lenderReferralBonusUpdatedRepo;

	@Autowired
	private ApplicationLevelEnachMandateRepo applicationLevelEnachMandateRepo;

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private UserHoldAmountMappedToDealRepo userHoldAmountMappedToDealRepo;

	@Autowired
	private LenderWalletHistoryServiceRepo lenderWalletHistoryServiceRepo;

	@Autowired
	private LenderAutoInvestConfigServive lenderAutoInvestConfigServive;

	@Override
	@Transactional
	public T getDashboard(int userId, boolean current) {

		User user = userRepo.findById(userId).get();
		// authorizationService.hasPermission(user);
		return getDashboard(userId, current, user);
	}

	private T getDashboard(int userId, boolean current, User user) {

		List<LoanStatus> loanStatuses = new ArrayList<LoanStatus>();
		loanStatuses.add(LoanStatus.Closed);
		loanStatuses.add(LoanStatus.Agreed);
		loanStatuses.add(LoanStatus.Active);

		Object sumOfLenderAllLoans = null;
		List<Object[]> countByLoanStatus = null;
		Object sumOfAllClosedLoans = null;
		Object sumOfAllAgreedLoans = null;
		Object sumOfAllActiveLoans = null;

		if (user.getPrimaryType() == PrimaryType.LENDER) {
			sumOfLenderAllLoans = oxyLoanRepo.sumOfLenderAllLoans(userId, loanStatuses.toArray(new LoanStatus[0]));
			countByLoanStatus = oxyLoanRepo.lenderCountByLoanStatus(userId, loanStatuses.toArray(new LoanStatus[0]));
			sumOfAllClosedLoans = oxyLoanRepo.sumOfLenderAllLoans(userId, new LoanStatus[] { LoanStatus.Closed });
			sumOfAllAgreedLoans = oxyLoanRepo.sumOfLenderAllLoans(userId, new LoanStatus[] { LoanStatus.Agreed });
			sumOfAllActiveLoans = oxyLoanRepo.sumOfLenderAllLoans(userId, new LoanStatus[] { LoanStatus.Active });
		} else {
			LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
			int parentLoanRequestId = loanRequest.getId();
			// sumOfLenderAllLoans = oxyLoanRepo.sumOfBorrowerAllLoans(userId,
			// loanStatuses.toArray(new LoanStatus[0]));
			sumOfLenderAllLoans = oxyLoanRepo.sumOfBorrowerAllLoansByCurrentLoan(userId, loanRequest.getId(),
					loanStatuses.toArray(new LoanStatus[0]));// current allloans
			// countByLoanStatus = oxyLoanRepo.borrowerCountByLoanStatus(userId,
			// loanStatuses.toArray(new LoanStatus[0]));
			countByLoanStatus = oxyLoanRepo.borrowerCountByLoanStatusOfCurrentLoan(userId, parentLoanRequestId,
					loanStatuses.toArray(new LoanStatus[0]));
			// sumOfAllClosedLoans = oxyLoanRepo.sumOfBorrowerAllLoans(userId, new
			// LoanStatus[] { LoanStatus.Closed });

			sumOfAllClosedLoans = oxyLoanRepo.sumOfBorrowerAllLoansByCurrentLoan(userId, parentLoanRequestId,
					new LoanStatus[] { LoanStatus.Closed });// current allloans

			// sumOfAllAgreedLoans = oxyLoanRepo.sumOfBorrowerAllLoans(userId, new
			// LoanStatus[] { LoanStatus.Agreed });
			sumOfAllAgreedLoans = oxyLoanRepo.sumOfBorrowerAllLoansByCurrentLoan(userId, parentLoanRequestId,
					new LoanStatus[] { LoanStatus.Closed });
			// sumOfAllActiveLoans = oxyLoanRepo.sumOfBorrowerAllLoans(userId, new
			// LoanStatus[] { LoanStatus.Active });
			sumOfAllActiveLoans = oxyLoanRepo.sumOfBorrowerAllLoansByCurrentLoan(userId, parentLoanRequestId,
					new LoanStatus[] { LoanStatus.Active });
		}
		Object[] objects = (Object[]) sumOfLenderAllLoans;
		Object[] closedLoansObjects = (Object[]) sumOfAllClosedLoans;
		Object[] agreedLoansObjects = (Object[]) sumOfAllAgreedLoans;
		Object[] activeLoansObjects = (Object[]) sumOfAllActiveLoans;

		T oxyAccountDetailsDto1 = populatedDetails(user, objects);

		OxyAccountDetailsDto oxyAccountDetailsDto = (OxyAccountDetailsDto) oxyAccountDetailsDto1;
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
		if (loanRequest != null) {
			oxyAccountDetailsDto.setCommitmentAmount(loanRequest.getLoanRequestAmount()); // Total Commitment
			LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();
			if (loanOfferdAmount != null) {
				oxyAccountDetailsDto.setLoanOfferedAmount(loanOfferdAmount.getLoanOfferedAmount());
				oxyAccountDetailsDto.setTotalTransactionFee(
						loanOfferdAmount.getPayuStatus() == null || loanOfferdAmount.getBorrowerFee() == null ? 0.0d
								: loanOfferdAmount.getBorrowerFee());
			}
		}
		Double lenderWalletCreditAmount = lenderOxyWalletNativeRepo.lenderWalletCreditAmount(userId);
		Double lenderWalletdebitAmount = lenderOxyWalletNativeRepo.lenderWalletDebitAmount(userId);
		Double lenderWalletInprocessAmount = lenderOxyWalletNativeRepo.lenderWalletInprocessAmount(userId);
		PersonalDetailsResponseDto personalDetailsResponseDto = new PersonalDetailsResponseDto();

		if (!(lenderWalletInprocessAmount != null && lenderWalletInprocessAmount > 0)) {
			lenderWalletInprocessAmount = 0d;
		}
		if (!(lenderWalletCreditAmount != null && lenderWalletCreditAmount > 0)) {
			lenderWalletCreditAmount = 0d;
		}
		if (!(lenderWalletdebitAmount != null && lenderWalletdebitAmount > 0)) {
			lenderWalletdebitAmount = 0d;
		}
		Double lenderWalletAmount = lenderWalletCreditAmount - lenderWalletdebitAmount;
		if (lenderWalletAmount != null && lenderWalletAmount > 0) {
			personalDetailsResponseDto.setLenderWalletAmount(lenderWalletAmount - lenderWalletInprocessAmount);
		} else {
			personalDetailsResponseDto.setLenderWalletAmount(0.0);
		}
		oxyAccountDetailsDto.setLenderWalletAmount(personalDetailsResponseDto.getLenderWalletAmount());
		if (user.getExperianSummary() != null && user.getUserProfileRisk() != null) {
			oxyAccountDetailsDto
					.setOxyScore(user.getExperianSummary().getScore() + user.getUserProfileRisk().getTotalScore());
		}
		Map<LoanStatus, Integer> counts = new HashMap<LoanStatus, Integer>();

		Map<LoanStatus, BigDecimal> amounts = new HashMap<LoanStatus, BigDecimal>();
		if (countByLoanStatus != null && countByLoanStatus.size() > 0) {
			for (Object[] entries : countByLoanStatus) {

				LoanStatus loanStatus = LoanStatus.valueOf(entries[0].toString());
				counts.put(loanStatus, Integer.valueOf(entries[1].toString()));
				BigDecimal amount = amounts.get(loanStatus);
				if (amount == null) {
					amounts.put(loanStatus, new BigDecimal((Double) objects[0]));
				} else {
					amounts.put(loanStatus, amount.add(new BigDecimal((Double) objects[0])));
				}
			}
			if (!counts.isEmpty()) {
				int activeCount = counts.containsKey(LoanStatus.Active) ? counts.get(LoanStatus.Active) : 0;
				int closedCount = counts.containsKey(LoanStatus.Closed) ? counts.get(LoanStatus.Closed) : 0;
				oxyAccountDetailsDto.setNoOfActiveLoans(activeCount);
				oxyAccountDetailsDto.setNoOfClosedLoans(closedCount);
				oxyAccountDetailsDto.setNoOfDisbursedLoans(activeCount + closedCount);
			}
			oxyAccountDetailsDto.setNoOfFailedEmis(((Long) objects[4]).intValue());
			oxyAccountDetailsDto
					.setActiveLoansAmount(activeLoansObjects[0] == null ? 0d : (double) activeLoansObjects[0]);
			oxyAccountDetailsDto
					.setClosedLoansAmount(closedLoansObjects[0] == null ? 0d : (double) closedLoansObjects[0]);
			oxyAccountDetailsDto
					.setInProcessAmount(agreedLoansObjects[0] == null ? 0d : (double) agreedLoansObjects[0]);
			oxyAccountDetailsDto.setAmountDisbursed(oxyAccountDetailsDto.getAmountDisbursed().doubleValue()
					- oxyAccountDetailsDto.getInProcessAmount().doubleValue());
		}
		int noOfLoanRequests = loanRequestRepo.countByUserIdAndParentRequestIdIsNotNull(userId);
		LoanRequest parentRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
		int noOfLoanResponses = loanRequestRepo.countByParentRequestId(parentRequest.getId());
		oxyAccountDetailsDto.setNoOfLoanRequests(noOfLoanRequests);
		oxyAccountDetailsDto.setNoOfLoanResponses(noOfLoanResponses);
		if (user.getPrimaryType() == PrimaryType.LENDER) {

			Double totalPendingEmi = oxyLoanRepo.findSumOfOutstandingAmount(user.getId());
			double convrtedtotalPendingEmi = totalPendingEmi == null ? 0.0d : totalPendingEmi;
			oxyAccountDetailsDto.setOutstandingAmount((double) Math.round(convrtedtotalPendingEmi));

		}
		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			oxyAccountDetailsDto.setOutstandingAmount(oxyAccountDetailsDto.getLoanOfferedAmount().doubleValue()
					- oxyAccountDetailsDto.getInProcessAmount().doubleValue()
					- (activeLoansObjects[0] == null ? 0d : (double) activeLoansObjects[0]));
		}

		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			oxyAccountDetailsDto.setMyTrackingStatus(statusForBorrower(userId));
		}

		if (user.getPrimaryType() == PrimaryType.LENDER) {
			oxyAccountDetailsDto.setMyTrackingStatus(statusForLender(userId));
		}
		oxyAccountDetailsDto.setRegistractionStatus(user.getStatus().toString());

		if (user.getPrimaryType() == PrimaryType.LENDER) {
			String uniqueNumber = user.getUniqueNumber();
			String referrerLink = new StringBuilder().append(this.referrerLink.replace("{unique_number}", uniqueNumber))
					.toString();
			oxyAccountDetailsDto.setReferrerLink(referrerLink);
		}

		if (user.getPrimaryType() == PrimaryType.LENDER) {
			Double maxmumLimit = 0.0;
			Double activeAmount = oxyLoanRepo.getLenderActiveAmountButNotDealAmount(user.getId());
			if (activeAmount != null) {
				maxmumLimit = maxmumLimit + activeAmount;
			}
			Double participatedAmount = oxyLendersAcceptedDealsRepo.getDealParticipationAmount(user.getId());
			if (participatedAmount != null) {
				maxmumLimit = maxmumLimit + participatedAmount;
			}

			Double participationUpdatedValue = lendersPaticipationUpdationRepo
					.getSumOfLenderUpdatedAmountButNotInEquity(userId);
			if (participationUpdatedValue != null) {
				maxmumLimit = maxmumLimit + participationUpdatedValue;
			}

			Double remainingAmountToParticipate = Double.parseDouble(maximumOutstandingAmount) - maxmumLimit;

			oxyAccountDetailsDto.setRemainingAmountToParticipateInDeals(remainingAmountToParticipate);

			oxyAccountDetailsDto.setMaximumAmount(maxmumLimit == null ? 0.0 : maxmumLimit);

		}
		return oxyAccountDetailsDto1;
	}

	protected abstract T populatedDetails(User user, Object[] objects);

	public StatusResponseDto statusForLender(int id) {
		LoanRequest loanDetails = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(id);
		StatusResponseDto loanResponseDto = new StatusResponseDto();
		if (loanDetails != null) {
			List<OxyLoan> oxyLoans = oxyLoanRepo.findByLenderParentRequestId(loanDetails.getId());
			int numberOfLoans = 0;
			int numberOfEsigns = 0;
			int numberOfLoansDisbursed = 0;
			for (OxyLoan loans : oxyLoans) {

				LoanRequest loanRequest1 = loanRequestRepo.findByLoanId(loans.getLoanId());
				numberOfLoans = numberOfLoans + 1;
				loanResponseDto.setNumberOfLoans(numberOfLoans);
				if (loanRequest1.getAdminComments() != null) {
					numberOfEsigns = numberOfEsigns + 1;
				}
				loanResponseDto.setNumberOfEsignsCompleted(numberOfEsigns);
				if (numberOfLoans == numberOfEsigns) {
					loanResponseDto.seteSignStatus("True");
				} else {
					loanResponseDto.seteSignStatus("False");
				}
				if (loans.getAdminComments() != null) {
					numberOfLoansDisbursed = numberOfLoansDisbursed + 1;
				}
				loanResponseDto.setNumberOfLoansDisbursed(numberOfLoansDisbursed);
				if (numberOfLoans == numberOfLoansDisbursed) {
					loanResponseDto.setDisbursementStatus("True");
				} else {
					loanResponseDto.setDisbursementStatus("False");
				}
			}
		}
		return loanResponseDto;

	}

	public StatusResponseDto statusForBorrower(int id) {
		LoanRequest loanDetails = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(id);
		StatusResponseDto statusResponseDto = new StatusResponseDto();
		if (loanDetails != null) {
			List<OxyLoan> oxyLoans = oxyLoanRepo.findByBorrowerParentRequestId(loanDetails.getId());
			int numberOfLoans = 0;
			int numberOfEsigns = 0;
			int numberOfLoansDisbursed = 0;
			int numberOfENachCompleted = 0;
			for (OxyLoan loans : oxyLoans) {

				LoanRequest loanRequest1 = loanRequestRepo.findByLoanId(loans.getLoanId());
				numberOfLoans = numberOfLoans + 1;
				statusResponseDto.setNumberOfLoans(numberOfLoans);

				if (loanRequest1.getAdminComments() != null) {
					numberOfEsigns = numberOfEsigns + 1;

				}
				statusResponseDto.setNumberOfEsignsCompleted(numberOfEsigns);

				if (numberOfLoans == numberOfEsigns) {
					statusResponseDto.seteSignStatus("True");
				} else {
					statusResponseDto.seteSignStatus("False");
				}

				if (loans.getAdminComments() != null) {
					numberOfLoansDisbursed = numberOfLoansDisbursed + 1;
				}
				statusResponseDto.setNumberOfLoansDisbursed(numberOfLoansDisbursed);
				if (numberOfLoans == numberOfLoansDisbursed) {
					statusResponseDto.setDisbursementStatus("True");
				} else {
					statusResponseDto.setDisbursementStatus("False");
				}

				EnachMandate enachMandate = enachMandateRepo.findByoxyLoanId(loans.getId());
				if (enachMandate != null) {
					numberOfENachCompleted = numberOfENachCompleted + 1;

				}
				statusResponseDto.setNumberOfENachCompleted(numberOfENachCompleted);
				if (numberOfLoans == numberOfENachCompleted) {
					statusResponseDto.seteNachStatus("True");
				} else {
					statusResponseDto.seteNachStatus("False");
				}

			}
		}

		return statusResponseDto;

	}

	@SuppressWarnings("unlikely-arg-type")
	@Override
	@Transactional
	public LoanResponseDto loanRequest(int userId, LoanRequestDto loanRequestDto) {
		logger.error("!!!!!!!!!!!!!!!! loanRequest !!!!!!!!!!!!!!!");
		User user = userRepo.findById(userId).get();
		// authorizationService.hasPermission(user);
		isValidLoanRequest(loanRequestDto, user);
		LoanRequest loanRequest = new LoanRequest();
		loanRequest.setLoanRequestAmount(loanRequestDto.getLoanRequestAmount());
		loanRequest.setDuration(loanRequestDto.getDuration());
		if (loanRequestDto.getDurationType() != null) {
			loanRequest
					.setDurationType(loanRequestDto.getDurationType().equalsIgnoreCase("months") ? DurationType.Months
							: DurationType.Days);

		}
		loanRequest.setLoanPurpose(loanRequestDto.getLoanPurpose());
		loanRequest.setRateOfInterest(loanRequestDto.getRateOfInterest());
		loanRequest.setUserPrimaryType(user.getPrimaryType());
		loanRequest.setRepaymentMethod(RepaymentMethod.valueOf(loanRequestDto.getRepaymentMethod()));
		if (loanRequestDto.getLoanProcessType() != null
				&& loanRequestDto.getLoanProcessType().equalsIgnoreCase("autoinvest")) {
			loanRequest.setLoanProcessType("autoinvest");
		} else {
			loanRequest.setLoanProcessType("manualprocess");
		}
		try {
			loanRequest.setExpectedDate(expectedDateFormat.parse(loanRequestDto.getExpectedDate()));
		} catch (ParseException e) {
			logger.error("Error: ", e);
			throw new DataFormatException("Illegal Date format. It should be dd/MM/yyyy",
					ErrorCodes.INVALID_DATE_FORMAT);
		}
		LoanRequest parentRequest = null;
		if (loanRequestDto.getParentRequestId() != null) {
			parentRequest = loanRequestRepo.findById(loanRequestDto.getParentRequestId()).get();
			if (user.getPrimaryType() == PrimaryType.BORROWER) {
				LoanRequest loanRequest2 = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId());
				if (loanRequest2 != null) {
					LoanOfferdAmount loanOfferdAmount = loanRequest2.getLoanOfferedAmount();
					if (loanOfferdAmount == null) {
						throw new OperationNotAllowedException("User don't have loanoffer",
								ErrorCodes.INVALID_OPERATION);
					} else if (loanOfferdAmount != null) {
						if (loanOfferdAmount.getLoanOfferdStatus() != LoanOfferdStatus.LOANOFFERACCEPTED) {
							throw new OperationNotAllowedException("You  did not accepet loanoffer",
									ErrorCodes.INVALID_OPERATION);

						}
						if (loanRequest2.getDurationBySir() > 0) {
							if (loanRequest2.getRateOfInterestToBorrower() != loanRequestDto.getRateOfInterest()) {
								throw new ActionNotAllowedException(
										"Please check rate of interest it not matching to admin rate of interest",
										ErrorCodes.ENTITY_ALREDY_EXISTS);

							}
							if (!loanRequest2.getLoanOfferedAmount().getDuration()
									.equals(loanRequestDto.getDuration())) {
								throw new ActionNotAllowedException(
										"Please check duration it is not matching to admin duration value",
										ErrorCodes.ENTITY_ALREDY_EXISTS);

							}
							if (!loanRequestDto.getRepaymentMethod()
									.equals(loanRequest2.getRepaymentMethodForBorrower())) {
								throw new ActionNotAllowedException(
										"Please check repayment method it is not matching to admin repayment method",
										ErrorCodes.ENTITY_ALREDY_EXISTS);
							}
							if (!loanRequest.getDurationType().toString()
									.equalsIgnoreCase(loanRequestDto.getDurationType())) {
								throw new OperationNotAllowedException(
										"Duration Type is not matching with radha sir duration type",
										ErrorCodes.LOAN_REQUEST_NULL);
							}

						}
					}
				}
			}
			if (user.getPrimaryType() == PrimaryType.LENDER) {
				Double lenderWalletCreditAmount = lenderOxyWalletNativeRepo.lenderWalletCreditAmount(user.getId());
				Double lenderWalletdebitAmount = lenderOxyWalletNativeRepo.lenderWalletDebitAmount(user.getId());

				if (!(lenderWalletCreditAmount != null && lenderWalletCreditAmount > 0)) {
					lenderWalletCreditAmount = 0d;
				}
				if (!(lenderWalletdebitAmount != null && lenderWalletdebitAmount > 0)) {
					lenderWalletdebitAmount = 0d;
				}
				Double lenderWalletAmount = lenderWalletCreditAmount - lenderWalletdebitAmount;

				double convertedlenderWalletAmount = lenderWalletAmount == null ? 0.0d : lenderWalletAmount;
				if (convertedlenderWalletAmount == 0.0d) {
					throw new OperationNotAllowedException("Lender wallet is Zero", ErrorCodes.INVALID_OPERATION);
				}
				LoanRequest loanRequestDetails = loanRequestRepo.findById(loanRequestDto.getParentRequestId()).get();
				if (loanRequestDetails != null) {
					if (loanRequestDetails.getDurationBySir() > 0) {
						if (loanRequestDetails.getRateOfInterestToLender() != loanRequestDto.getRateOfInterest()) {
							throw new ActionNotAllowedException(
									"Please check rate of interest it not matching to admin rate of interest",
									ErrorCodes.ENTITY_ALREDY_EXISTS);

						}
						if (!loanRequestDetails.getLoanOfferedAmount().getDuration()
								.equals(loanRequestDto.getDuration())) {
							throw new ActionNotAllowedException(
									"Please check duration it is not matching to admin duration value",
									ErrorCodes.ENTITY_ALREDY_EXISTS);

						}
						if (!loanRequest.getDurationType().toString()
								.equalsIgnoreCase(loanRequestDto.getDurationType())) {
							throw new OperationNotAllowedException(
									"Duration Type is not matching with radha sir duration type",
									ErrorCodes.LOAN_REQUEST_NULL);
						}

						if (!loanRequestDto.getRepaymentMethod()
								.equals(loanRequestDetails.getRepaymentMethodForLender())) {
							throw new ActionNotAllowedException(
									"Please check repayment method it is not matching to admin repayment method",
									ErrorCodes.ENTITY_ALREDY_EXISTS);
						}
					}
				}
			}
			Integer lenderUserId = user.getPrimaryType() == PrimaryType.LENDER ? userId : parentRequest.getUserId();
			Integer borrowerUserId = user.getPrimaryType() == PrimaryType.BORROWER ? userId : parentRequest.getUserId();

			Double totalAmountDisbursed = oxyLoanRepo.getTotalAmountDisbursedForParticularrUsers(borrowerUserId,
					lenderUserId);
			if (totalAmountDisbursed != null) {
				if (totalAmountDisbursed > 0) {
					if (totalAmountDisbursed >= 50000
							|| (totalAmountDisbursed + loanRequestDto.getLoanRequestAmount()) > 50000) {
						throw new OperationNotAllowedException(
								"Amount should not exceed INR 50000 Your application already disbursed with INR "
										+ totalAmountDisbursed,
								ErrorCodes.INVALID_OPERATION);
					}
				}
			}

			int count = lenderBorrowerConversationRepo.countByLenderUserIdAndBorrowerUserId(lenderUserId,
					borrowerUserId);
			if (count > 0) {
				throw new ActionNotAllowedException("Already one loan in conversation. Plesae check your responses.",
						ErrorCodes.ALREDY_IN_PROGRESS);
			}

			loanRequest.setParentRequestId(loanRequestDto.getParentRequestId());
			loanRequest.setLoanStatus(LoanStatus.Conversation);
			Integer requestedUser = parentRequest.getUserId(); // You are responding to this user's request
			isLimitExceeded(requestedUser, user, loanRequestDto);
			LenderBorrowerConversation lenderBorrowerConversation = new LenderBorrowerConversation();
			lenderBorrowerConversation.setLenderUserId(lenderUserId);
			lenderBorrowerConversation.setBorrowerUserId(borrowerUserId);
			LenderBorrowerConversation addedEntry = lenderBorrowerConversationRepo.save(lenderBorrowerConversation);

		}
		loanRequest.setUserId(userId);
		loanRequest.setEnachType("DIGITALENACH");
		loanRequest.setLoanRequestId(new Long(System.currentTimeMillis()).toString()); // This is temporary
		loanRequest = loanRequestRepo.save(loanRequest);
		loanRequest.setLoanRequestId(LOAN_REQUEST_IDFORMAT.replace("{type}", user.getPrimaryType().getShortCode())
				.replace("{ID}", loanRequest.getId().toString()));
		loanRequest = loanRequestRepo.save(loanRequest);
		if (loanRequestDto.getParentRequestId() != null) {
			// sending corresponding email to borrower/lender
			double emiPrincipalAmount = 0d;
			double emiInterstAmount = 0d;
			if (loanRequestDto.getDurationType().equalsIgnoreCase("months")) {

				emiInterstAmount = (loanRequest.getLoanRequestAmount() * (loanRequest.getRateOfInterest() / 12)) / 100d;
				switch (loanRequest.getRepaymentMethod()) {
				case RepaymentMethod.PI:
					emiPrincipalAmount = loanRequest.getLoanRequestAmount() / loanRequest.getDuration().doubleValue();
					break;
				default:
					break;
				}
			} else if (loanRequestDto.getDurationType().equalsIgnoreCase("days")) {
				emiInterstAmount = loanRequest.getRateOfInterest() * loanRequest.getDuration();
				switch (loanRequest.getRepaymentMethod()) {
				case RepaymentMethod.PI:
					emiPrincipalAmount = loanRequest.getLoanRequestAmount();
					break;
				default:
					break;
				}

			}

			TemplateContext templateContext = new TemplateContext();
			templateContext.put("firstName", user.getPersonalDetails().getFirstName());
			templateContext.put("requesterName", parentRequest.getUser().getPersonalDetails().getFirstName());
			templateContext.put("applicationNumber", loanRequest.getLoanRequestId());
			templateContext.put("userId", user.getUniqueNumber());
			templateContext.put("loanRequestedAmount", loanRequest.getLoanRequestAmount());
			templateContext.put("loanInterestRate", loanRequest.getRateOfInterest());
			templateContext.put("loanDuration", loanRequest.getDuration());
			templateContext.put("loanEmiType", loanRequest.getRepaymentMethod().getDisplayValue());
			templateContext.put("loanEmiAmount", emiPrincipalAmount + emiInterstAmount);
			templateContext.put("loanRespondedDate",
					this.expectedDateFormat.format(loanRequest.getLoanRequestedDate()));
			String mailsubject = (user.getPrimaryType() == PrimaryType.LENDER
					? loanRequestMailSubject.replace("$responseType", "Request")
					: loanRequestMailSubject.replace("$responseType", "Offer")); // Mail Goes to Borrower so subject is
																					// like this..
			String emailTemplateName = user.getPrimaryType() == PrimaryType.LENDER ? "Loanrequest-response.template"
					: "Loanoffer-response.template";

			EmailRequest emailRequest = new EmailRequest(new String[] { parentRequest.getUser().getEmail() },
					mailsubject, emailTemplateName, templateContext);
			if (!utm.equals(parentRequest.getUser().getUrchinTrackingModule())) {
				EmailResponse emailResponse = emailService.sendEmail(emailRequest);
				if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
					throw new RuntimeException(emailResponse.getErrorMessage());
				}
			}

			// Sending SMS to Lender/Borrower
			String mobileNumber = parentRequest.getUser().getMobileNumber();
			String templateName = user.getPrimaryType() == PrimaryType.LENDER ? "lendersentsrequest"
					: "borrowersentrequest";
			MobileRequest mobileRequest = new MobileRequest(mobileNumber, templateName);
			if (!utm.equals(parentRequest.getUser().getUrchinTrackingModule())) {
				mobileRequest.setSenderId("OXYLON");
				Map<String, String> variblesMap = new HashMap<>();
				variblesMap.put("VAR1", user.getUniqueNumber());
				mobileRequest.setVariblesMap(variblesMap);
				try {
					mobileUtil.sendTransactionalMessage(mobileRequest);
				} catch (Exception e) {
					logger.error("SMS failed : ", e);
				}
			}
		}
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setUserId(userId);
		loanResponseDto.setLoanRequestId(loanRequest.getId());
		loanResponseDto.setLoanRequest(loanRequest.getLoanRequestId());
		return loanResponseDto;
	}

	public void isLimitExceeded(Integer requestedUser, User user, LoanRequestDto loanRequestDto) {
		List<OxyLoan> existingLoans = null;
		List<LoanStatus> loanStatuses = new ArrayList<LoanStatus>();
		loanStatuses.add(LoanStatus.Agreed);
		loanStatuses.add(LoanStatus.Active);
		LoanRequest requestedLoanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(requestedUser);
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId());
		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			/*
			 * existingLoans =
			 * oxyLoanRepo.findByLenderUserIdAndBorrowerUserIdAndLoanStatusIn(requestedUser,
			 * user.getId(), loanStatuses);
			 */

			existingLoans = oxyLoanRepo
					.findByLenderUserIdAndBorrowerUserIdAndBorrowerParentRequestIdAndLenderParentRequestIdAndLoanStatusIn(
							requestedUser, user.getId(), loanRequest.getId(), requestedLoanRequest.getId(),
							loanStatuses);

		} else {
			/*
			 * existingLoans =
			 * oxyLoanRepo.findByLenderUserIdAndBorrowerUserIdAndLoanStatusIn(requestedUser,
			 * user.getId(), loanStatuses);
			 */

			/*
			 * existingLoans = oxyLoanRepo
			 * .findByLenderUserIdAndBorrowerUserIdAndBorrowerParentRequestIdAndLenderParentRequestIdAndLoanStatusIn(
			 * requestedUser, user.getId(), loanRequest.getId(),
			 * requestedLoanRequest.getId(), loanStatuses);
			 */ // wrong

			existingLoans = oxyLoanRepo
					.findByLenderUserIdAndBorrowerUserIdAndBorrowerParentRequestIdAndLenderParentRequestIdAndLoanStatusIn(
							user.getId(), requestedUser, requestedLoanRequest.getId(), loanRequest.getId(),
							loanStatuses);

		}
		if (CollectionUtils.isNotEmpty(existingLoans)) {
			double presentLoanAmount = 0.0d;
			for (OxyLoan loan : existingLoans) {
				presentLoanAmount += loan.getDisbursmentAmount();
			}
			if (presentLoanAmount + loanRequestDto.getLoanRequestAmount().doubleValue() > 50000d) {
				String errorMessage = "Can not trasact more than 50K. You can request/offer "
						+ (50000d - presentLoanAmount);
				throw new LimitExceededException(errorMessage, ErrorCodes.LIMIT_REACHED);
			}
		}
		validateLimits(user.getId(), loanRequestDto);
		validateLimits(requestedUser, loanRequestDto);
	}

	private void validateLimits(Integer requestedUser, LoanRequestDto loanRequestDto) {

		User user = userRepo.findById(requestedUser).get();
		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			OxyAccountDetailsDto dashboard = (OxyAccountDetailsDto) this.getDashboard(requestedUser, true,
					userRepo.findById(requestedUser).get());

			double offeredAmount = dashboard.getLoanOfferedAmount() == null ? 0d : dashboard.getLoanOfferedAmount();
			LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(requestedUser);

			double remainingRequstedAmount = offeredAmount - ((getDisbursedAmount(requestedUser, loanRequest.getId())
					+ getinProcessAmount(requestedUser, loanRequest.getId()) + loanRequestDto.getLoanRequestAmount())
					- getClosedAmount(requestedUser, loanRequest.getId()));

			if (remainingRequstedAmount != 0d && remainingRequstedAmount < 5000d) {
				throw new LimitExceededException("you can't respond/Accept with " + remainingRequstedAmount
						+ ", or less.Please update the amount and try again", ErrorCodes.LIMIT_REACHED);
			}
		}
		if (user.getPrimaryType() == PrimaryType.LENDER) {
			OxyAccountDetailsDto dashboard = (OxyAccountDetailsDto) this.getDashboard(requestedUser, true,
					userRepo.findById(requestedUser).get());
			Double remainingRequstedAmount = dashboard.getLenderWalletAmount() - loanRequestDto.getLoanRequestAmount();

			if (remainingRequstedAmount < 0) {
				throw new LimitExceededException("you can not respond/Accept with " + remainingRequstedAmount
						+ ", or less.Please update the amount and try again", ErrorCodes.LIMIT_REACHED);
			}
			if (loanRequestDto.getLoanRequestAmount() < 5000d) {
				throw new LimitExceededException(
						"You Requested Amount " + remainingRequstedAmount
								+ "is less than 5000 .Please update the amount and try again", // we have changed the
																								// code for LoanRequest
																								// limit
						ErrorCodes.LIMIT_REACHED);
			}

		}
	}

	private void isValidLoanRequest(LoanRequestDto loanRequestDto, User user) {
		if (loanRequestDto.getParentRequestId() == null) {
			LoanRequest alreadyRequested = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId());
			if (alreadyRequested != null) {
				throw new ActionNotAllowedException(
						"You can not raise a loan rquest/offer again instead update existing one",
						ErrorCodes.ENTITY_ALREDY_EXISTS);
			}
		}
		if (loanRequestDto.getParentRequestId() != null && !user.isEmailVerified()) {
			throw new ActionNotAllowedException(
					"You have not verified your email yet. Please verify before respond/raise",
					ErrorCodes.EMAIL_NOT_VERIFIED);
		}
		if (loanRequestDto.getParentRequestId() != null && user.getStatus() != Status.ACTIVE) {
			throw new ActionNotAllowedException("Your account is not active to respond.", ErrorCodes.ENTITY_NOT_ACTIVE);
		}
		if (loanRequestDto.getLoanRequestAmount().doubleValue() > new Double(this.maximumOutstandingAmount)
				.doubleValue()) {
			throw new IllegalValueException("The maximum Outstanding amount should be " + this.maximumOutstandingAmount,
					ErrorCodes.LIMIT_REACHED);
		}
		if (user.getStatus() != Status.ACTIVE && loanRequestDto.getParentRequestId() != null) {
			throw new ActionNotAllowedException("Make sure you filled all the documents and got approved",
					ErrorCodes.ENTITY_NOT_ACTIVE);
		}
		if (loanRequestDto.getLoanRequestAmount() == null || loanRequestDto.getLoanRequestAmount()
				.doubleValue() < new Double(this.minimumRequestAmount).doubleValue()) {
			throw new IllegalValueException("The minimum request amount should be " + this.minimumRequestAmount,
					ErrorCodes.LIMIT_REACHED);
		}
		if (loanRequestDto.getParentRequestId() != null && loanRequestDto.getLoanRequestAmount()
				.doubleValue() > new Double(this.maximumRequestAmount).doubleValue()) {
			throw new IllegalValueException("The maximum request amount should be " + this.maximumRequestAmount,
					ErrorCodes.LIMIT_REACHED);
		}
	}

	@Override
	@Transactional
	public SearchResultsDto<LoanResponseDto> searchRequests(int userId, SearchRequestDto searchRequestDto) {
		User user = userRepo.findById(userId).get();
		// authorizationService.hasPermission(user);
		Sort sort = null;
		if (searchRequestDto.getSortBy() != null) {
			sort = Sort.by(Direction.valueOf(searchRequestDto.getSortOrder().name()), searchRequestDto.getSortBy());
		} else {
			sort = Sort.by(Direction.DESC, "loanRequestedDate");
		}
		Pageable searchPage = PageRequest.of(searchRequestDto.getPage().getPageNo() - 1,
				searchRequestDto.getPage().getPageSize(), sort);
		Specification<LoanRequest> spec = null;
		try {
			spec = specificationProvider.construct(LoanRequest.class, searchRequestDto);
		} catch (ClassNotFoundException e) {
			logger.error(e, e);
		} catch (IllegalAccessException e) {
			logger.error(e, e);
		} catch (InvocationTargetException e) {
			logger.error(e, e);
		} catch (NoSuchMethodException e) {
			logger.error(e, e);
		} catch (InstantiationException e) {
			logger.error(e, e);
		}
		Page<LoanRequest> findAll = loanRequestRepo.findAll(spec, searchPage);
		SearchResultsDto<LoanResponseDto> results = new SearchResultsDto<LoanResponseDto>();
		if (findAll != null) {
			results.setTotalCount(Long.valueOf(findAll.getTotalElements()).intValue());
			results.setPageNo(searchRequestDto.getPage().getPageNo());
			results.setPageCount(findAll.getNumberOfElements());
			List<LoanResponseDto> loanRequests = new ArrayList<LoanResponseDto>();
			results.setResults(loanRequests);
			for (LoanRequest request : findAll.getContent()) {
				loanRequests.add(populateLoanRequestDetails(request));
			}
		}
		return results;

	}

	private LoanResponseDto populateLoanRequestDetails(LoanRequest request) {
		logger.info(" populateLoanRequestDetails method started");
		LoanResponseDto loanRequest = new LoanResponseDto();
		if (request.getAdminComments() != null) {
			if (request.getAdminComments().equalsIgnoreCase("APPROVED")) {
				String loanId = request.getLoanId();
				if (request.getLoanId() != null && !request.getLoanId().isEmpty()) {
					String splitLoanId[] = loanId.split("LN");
					String id = splitLoanId[1];
					OxyLoan oxyloans = oxyLoanRepo.findById(Integer.parseInt(id)).get();
					if (oxyloans != null) {
						loanRequest.setId(oxyloans.getBorrowerParentRequestId());
						loanRequest.setLoanRequestId(oxyloans.getBorrowerParentRequestId());
					}
				}
			}
		} else {

			loanRequest.setId(request.getId());
			loanRequest.setLoanRequestId(request.getId());
		}
		// loanRequest.setId(request.getId());
		if (request.getLoanStatus().equals(LoanRequest.LoanStatus.PartnerApproved)
				|| request.getLoanStatus().equals(LoanRequest.LoanStatus.PartnerShortList)
				|| request.getLoanStatus().equals(LoanRequest.LoanStatus.PartnerEdited)) {
			loanRequest.setDuration(request.getDurationBySir());
			if (request.getRepaymentMethodForBorrower() != null) {
				loanRequest.setRepaymentMethod(request.getRepaymentMethodForBorrower());
			}
			loanRequest.setRateOfInterest(request.getRateOfInterestToBorrower());
			if (request.getLoanOfferedAmount() != null) {
				if (request.getLoanOfferedAmount().getLoanOfferdStatus()
						.equals(LoanOfferdAmount.LoanOfferdStatus.LOANOFFERACCEPTED)) {
					Double esign = oxyLoanRepo.esignStatus(request.getId());
					Double enach = oxyLoanRepo.enachStatus(request.getId());

					if (esign != null) {
						loanRequest.setPartnerEsign(true);
					}
					if (request.getLoanOfferedAmount().getLoanOfferedAmount() >= 50000) {
						if (request.getRepaymentMethodForBorrower().equalsIgnoreCase("PI")) {
							ApplicationLevelEnachMandate applicationLevel = applicationLevelEnachMandateRepo
									.findingIsEnachActivatedForPI(request.getId());
							if (applicationLevel != null) {
								loanRequest.setPartnerEnach(true);
							}
						} else {
							ApplicationLevelEnachMandate interest = applicationLevelEnachMandateRepo
									.findingIsEnachActivatedForI(request.getId());

							ApplicationLevelEnachMandate principalAmount = applicationLevelEnachMandateRepo
									.findingIsEnachActivatedForP(request.getId());

							if (interest != null && principalAmount != null) {
								loanRequest.setPartnerEnach(true);
							}
						}
					} else {
						if (enach != null) {
							loanRequest.setPartnerEnach(true);
						}
					}
					loanRequest.setOfferAcceptedStatus(true);
				} else {
					loanRequest.setPartnerEnach(false);
					loanRequest.setPartnerEsign(false);
					loanRequest.setOfferAcceptedStatus(false);
				}

			}
		} else {
			loanRequest.setDuration(request.getDuration());
			loanRequest.setRepaymentMethod(request.getRepaymentMethod().toString());
			loanRequest.setRateOfInterest(request.getRateOfInterest());
		}

		loanRequest.setDurationType(request.getDurationType().toString());
		loanRequest.setExpectedDate(expectedDateFormat.format(request.getExpectedDate()));
		loanRequest.setLoanPurpose(request.getLoanPurpose());
		loanRequest.setLoanRequest(request.getLoanRequestId());
		loanRequest.setLoanRequestAmount(request.getLoanRequestAmount());
		loanRequest.setLoanRequestedDate(expectedDateFormat.format(request.getLoanRequestedDate()));
		// loanRequest.setLoanRequestId(request.getId());
		loanRequest.setUserDisplayId(request.getUserId().toString());
		loanRequest.setLoanStatus(request.getLoanStatus().name());
		loanRequest.setEnachType(request.getEnachType());
		loanRequest.setLoanId(request.getLoanId());
		loanRequest.setLoanProcessType(request.getLoanProcessType());
		if (request.getLoanId() != null) {
			OxyLoan oxyLoan = oxyLoanRepo.findByLoanId(request.getLoanId());
			if (oxyLoan != null) {
				loanRequest.setOxyLoanAdminComments(oxyLoan.getAdminComments());
			}
			List<LoanEmiCard> loanEmiCard = loanEmiCardRepo.findByLoanId(oxyLoan.getId());
			if (loanEmiCard.size() > 0) {
				loanRequest.setLoanEmiCardStatus("True");
			} else {
				loanRequest.setLoanEmiCardStatus("False");
			}
		}
		loanRequest.setOutStandingAmount(Double.parseDouble("" + request.getLoanRequestAmount())
				- Double.parseDouble("" + request.getDisbursmentAmount()));
		loanRequest.setComments(request.getComments());
		loanRequest.setBorrowerCommentsDetails(getBorrowerComments(request.getUserId()));
		if (request.getAdminComments() != null) {
			loanRequest.setLoanAdminComments(request.getAdminComments());
		}
		User requestedUserId = userRepo.findById(request.getUserId()).get();
		UserResponse user = new UserResponse();
		NomineeResponseDto nomineeResponseDto = getNominee(request.getUserId());
		user.setNomineeResponseDto(nomineeResponseDto);
		if (requestedUserId.getAdminComments() != null) {
			if (requestedUserId.getAdminComments().equals(Status.INTERESTED.toString())) {
				user.setAdminComments(Status.VERIFIED.toString());
			}
			if (requestedUserId.getAdminComments().equals(Status.APPROVED.toString())) {
				user.setAdminComments(Status.OXYRECOMMENDS.toString());
			}
		}

		user.setStatus(requestedUserId.getStatus().toString());
		loanRequest.setUser(user);
		user.setId(requestedUserId.getId());
		userDetails(requestedUserId, user);
		OfferSentDetails offerSentDetails = offeredDetails(requestedUserId.getId(), request.getLoanRequestId());

		boolean flag = false;
		if (ObjectUtils.allNotNull(offerSentDetails)) {
			flag = true;
			loanRequest.setSendofferstatus(flag);
		} else if (ObjectUtils.allNotNull(requestedUserId.getUserProfileRisk())) {
			flag = true;
			loanRequest.setSendofferstatus(flag);
		}
		loanRequest.setOfferSentDetails(offerSentDetails);
		UserDocumentStatus profilePicDoc = userDocumentStatusRepo.findByUserIdAndDocumentTypeAndDocumentSubType(
				user.getId(), DocumentType.Kyc, KycType.PROFILEPIC.name());
		if (profilePicDoc != null) {
			String url = generateKycDownloadUrl(profilePicDoc);
			user.setProfilePicUrl(url);
		} else {
			user.setProfilePicUrl(this.defaultProfilePicUrl);
		}
		populateExtraUserDetails(requestedUserId, user);
		UserResponse anotherUserResponse = new UserResponse();
		if (request.getParentRequest() != null) {
			if (request.getParentRequestId() != 0) {
				Integer parentRequestUserId = request.getParentRequest().getUserId();
				User anotherUser = userRepo.findById(parentRequestUserId).get();
				anotherUserResponse.setId(requestedUserId.getId());
				userDetails(anotherUser, anotherUserResponse);
				Integer lenderUserId = requestedUserId.getPrimaryType() == PrimaryType.LENDER ? requestedUserId.getId()
						: anotherUser.getId();
				Integer borrowerUserId = requestedUserId.getPrimaryType() == PrimaryType.BORROWER
						? requestedUserId.getId()
						: anotherUser.getId();
				int count = lenderBorrowerConversationRepo.countByLenderUserIdAndBorrowerUserId(lenderUserId,
						borrowerUserId);

				loanRequest.setAlreadyOffered(count > 0);
				List<LoanStatus> loanStatuses = new ArrayList<LoanStatus>();
				loanStatuses.add(LoanStatus.Active);
				List<OxyLoan> activeLoans = oxyLoanRepo.findByLenderUserIdAndBorrowerUserIdAndLoanStatusIn(lenderUserId,
						borrowerUserId, loanStatuses);
				loanRequest.setAlreadyActive(CollectionUtils.isNotEmpty(activeLoans));
				loanRequest.setLenderUserId(lenderUserId);
				loanRequest.setBorrowerUserId(borrowerUserId);
				if (lenderUserId != null) {
					Integer userId = lenderUserId;
					loanRequest.setLenderKycDocuments(getKycDetails(userId));
				}
				if (borrowerUserId != null) {
					Integer userId = borrowerUserId;
					loanRequest.setBorrowerKycDocuments(getKycDetails(userId));
					loanRequest.setFavouriteBorrowers(getFavouriteBorrowers(userId));
				}

			}

		} else {
			if (requestedUserId.getPrimaryType() == PrimaryType.BORROWER) {
				double disbursedAmount = getDisbursedAmount(requestedUserId.getId(),
						loanRequestRepo.findByUserIdAndParentRequestIdIsNull(requestedUserId.getId()).getId()) == null
								? 0.0d
								: getDisbursedAmount(requestedUserId.getId(), loanRequestRepo
										.findByUserIdAndParentRequestIdIsNull(requestedUserId.getId()).getId());
				loanRequest.setLoanDisbursedAmount(disbursedAmount);
				LoanOfferdAmount loanOfferdAmount = loanRequestRepo
						.findByUserIdAndParentRequestIdIsNull(requestedUserId.getId()).getLoanOfferedAmount();
				if (loanOfferdAmount != null) {
					loanRequest.setLoanDisbursedPercentage(new Double((disbursedAmount * 100d)
							/ loanRequestRepo.findByUserIdAndParentRequestIdIsNull(requestedUserId.getId())
									.getLoanOfferedAmount().getLoanOfferedAmount())
							.intValue());
				}

			}
		}
		if (requestedUserId.getPrimaryType() == PrimaryType.BORROWER) {
			loanRequest.setBorrowerUser(user);
			loanRequest.setLenderUser(anotherUserResponse);
			Integer id = requestedUserId.getId();
			loanRequest.setBorrowerKycDocuments(getKycDetails(id));
			loanRequest.setFavouriteBorrowers(getFavouriteBorrowers(id));

		} else {
			loanRequest.setLenderUser(user);
			loanRequest.setBorrowerUser(anotherUserResponse);
			Integer id = requestedUserId.getId();
			loanRequest.setLenderKycDocuments(getKycDetails(id));

		}

		LenderReferenceDetails details = lenderReferenceDetailsRepo.findRefereeInfo(requestedUserId.getId());

		if (details != null) {

			if (details.getSource().equals(LenderReferenceDetails.Source.Partner)) {

				OxyUserType oxyUser = oxyUserTypeRepo.findingPartnerDetails(details.getReferrerId());

				if (oxyUser != null) {
					loanRequest.setReferredBy("PR" + details.getReferrerId());
				}

			} else {
				User referrer = userRepo.findById(details.getReferrerId()).get();
				if (referrer != null) {
					if (referrer.getPrimaryType().equals(User.PrimaryType.LENDER)) {
						loanRequest.setReferredBy("LR" + details.getReferrerId());
					} else {
						loanRequest.setReferredBy("BR" + details.getReferrerId());
					}
				}
			}

		} else {
			loanRequest.setReferredBy("0");
		}

		if (requestedUserId.getPrimaryType() == PrimaryType.LENDER) {

			Double lenderCurrentWalletBalance = 0.0;
			Double holdAmount = 0.0;
			Double dealValue = 0.0;
			Double dealValueWithAgreements = 0.0;
			Double equityAmount = 0.0;
			Double oldWalletAmount = 0.0;

			Double dealAmount = oxyLendersAcceptedDealsRepo
					.getDealParticipationAmountIncludingClosedDeals(requestedUserId.getId());
			Double dealUpdatedAmount = lendersPaticipationUpdationRepo
					.getSumOfLenderUpdatedAmountButNotInEquityIncludingClosedDeals(requestedUserId.getId());
			if (dealAmount != null) {
				if (dealUpdatedAmount != null) {
					dealValue = dealAmount + dealUpdatedAmount;
				} else {
					dealValue = dealAmount;
				}
			}

			Double dealAmountWithAgreements = oxyLendersAcceptedDealsRepo
					.getDealParticipationAmountAfterAgreements(requestedUserId.getId());
			if (dealAmountWithAgreements != null) {
				dealValueWithAgreements = dealAmountWithAgreements;
			}
			holdAmount = dealValue - dealValueWithAgreements;

			Double equityValue = oxyLendersAcceptedDealsRepo
					.getDealParticipationAmountForEquity(requestedUserId.getId());
			Double dealUpdatedAmountInEquity = lendersPaticipationUpdationRepo
					.getSumOfLenderUpdatedAmountInEquity(requestedUserId.getId());
			if (equityValue != null) {
				if (dealUpdatedAmountInEquity != null) {
					equityAmount = equityValue + dealUpdatedAmountInEquity;
				} else {
					equityAmount = equityValue;
				}
			}
			Double lenderWalletCreditAmount = lenderOxyWalletNativeRepo
					.lenderWalletCreditAmount(requestedUserId.getId());
			Double lenderWalletdebitAmount = lenderOxyWalletNativeRepo.lenderWalletDebitAmount(requestedUserId.getId());
			Double lenderWalletInprocessAmount = lenderOxyWalletNativeRepo
					.lenderWalletInprocessAmount(requestedUserId.getId());

			if (!(lenderWalletInprocessAmount != null && lenderWalletInprocessAmount > 0)) {
				lenderWalletInprocessAmount = 0d;
			}
			if (!(lenderWalletCreditAmount != null && lenderWalletCreditAmount > 0)) {
				lenderWalletCreditAmount = 0d;
			}
			if (!(lenderWalletdebitAmount != null && lenderWalletdebitAmount > 0)) {
				lenderWalletdebitAmount = 0d;
			}
			Double lenderWalletAmount = lenderWalletCreditAmount - lenderWalletdebitAmount;
			if (lenderWalletAmount != null && lenderWalletAmount > 0) {
				oldWalletAmount = lenderWalletAmount - lenderWalletInprocessAmount;
			} else {
				oldWalletAmount = 0.0;
			}

			lenderCurrentWalletBalance = oldWalletAmount - holdAmount - equityAmount;
			loanRequest.setWalletAmount(lenderCurrentWalletBalance == null ? 0.0 : lenderCurrentWalletBalance);
			User lenderDetails = userRepo.findById(requestedUserId.getId()).get();
			OxyUserType oxyUserType = oxyUserTypeRepo
					.getDetailsByUtmNameAndType(lenderDetails.getUrchinTrackingModule());
			if (oxyUserType != null) {
				loanRequest.setWalletCalculationToLender(false);
				loanRequest.setWalletAmountToLender(
						BigDecimal.valueOf(partnerService.lenderFromPartnerWalletInfo(user.getId())).toBigInteger());
			} else {
				loanRequest.setWalletCalculationToLender(true);
				loanRequest.setWalletAmountToLender(null);
			}

			loanRequest.setSumOfDealAmount(dealValue == null ? 0.0 : dealValue);
			loanRequest.setEquityValue(equityAmount == null ? 0.0 : equityAmount);
			loanRequest.setHoldAmount(holdAmount == null ? 0.0 : holdAmount);
			loanRequest.setDealValueWithAgreements(dealValueWithAgreements == null ? 0.0 : dealValueWithAgreements);
			loanRequest.setGroupId(requestedUserId.getLenderGroupId());

			String lenderGroupName = oxyLendersGroupRepo.getLenderGroupNameByid(requestedUserId.getLenderGroupId());

			if (lenderGroupName != null) {
				loanRequest.setGroupName(lenderGroupName);
			} else {
				loanRequest.setGroupName("New Lender");
			}

		}

		if (requestedUserId != null && requestedUserId.getPrimaryType().equals(User.PrimaryType.BORROWER)
				&& requestedUserId.getProfessionalDetails() != null) {
			if (requestedUserId.getProfessionalDetails().getDocumentDriveLink() != null) {
				loanRequest.setDriveLink(requestedUserId.getProfessionalDetails().getDocumentDriveLink());
			} else {
				loanRequest.setDriveLink("");
			}
		}

		if (requestedUserId != null) {
			if (requestedUserId.getPersonalDetails() != null) {

				loanRequest.setCifNumber(requestedUserId.getPersonalDetails().getCifNumber() == null ? ""
						: requestedUserId.getPersonalDetails().getCifNumber());

				loanRequest.setFinoEmployeeMobileNumber(
						requestedUserId.getPersonalDetails().getFinoEmployeeMobileNumber() == null ? ""
								: requestedUserId.getPersonalDetails().getFinoEmployeeMobileNumber());

			}
		}

		logger.info(" populateLoanRequestDetails method end");
		return loanRequest;

	}

	public List<KycFileResponse> getKycDetails(int userId) {
		List<KycFileResponse> listOfKycDocuments = new ArrayList<KycFileResponse>();

		HashMap<Integer, String> hash_map = new LinkedHashMap<Integer, String>();

		hash_map.put(1, "PAN");
		hash_map.put(2, "PAYSLIPS");
		hash_map.put(3, "BANKSTATEMENT");
		hash_map.put(4, "AADHAR");
		hash_map.put(5, "DRIVINGLICENCE");
		hash_map.put(6, "VOTERID");
		hash_map.put(7, "PASSPORT");
		hash_map.put(8, "CONTACTS");
		hash_map.put(9, "EXPERIAN");
		hash_map.put(10, "CIBILSCORE");
		hash_map.put(11, "CHEQUELEAF");
		hash_map.put(12, "COMPANYPAN");
		hash_map.put(13, "MEMORANDUMOFASSOCIATION");
		hash_map.put(14, "CERTIFICATEOFINSURANCE");
		hash_map.put(15, "GST");
		hash_map.put(16, "TAN");
		hash_map.put(17, "COMPANYBANKSTMT");
		hash_map.put(18, "AOI");
		hash_map.put(19, "MAOA");
		hash_map.put(20, "GOVTIDPROOF");
		hash_map.put(21, "FINANCIALS");
		hash_map.put(22, "DEALERSHIPAGREEMENT");
		hash_map.put(23, "INVENTORYDTLS");
		hash_map.put(24, "PURCHASEORDER");
		hash_map.put(25, "GSTREGCERT");
		hash_map.put(26, "GSTFILLINGREC");
		hash_map.put(27, "FUNDINGCOMPANYPROFILE");
		hash_map.put(28, "OSBOOKINGS");
		hash_map.put(29, "SALESINFO");
		hash_map.put(30, "LOANBOOKS");
		hash_map.put(31, "SALESPROJECTIONS");
		hash_map.put(32, "PF");
		hash_map.put(33, "MONTHREVENUE");
		hash_map.put(34, "AUDITEDFINANCIALS");
		hash_map.put(35, "FACILITYAGREEMENTS");
		hash_map.put(36, "OPERATIONALMETRICS");
		hash_map.put(37, "REVENUEANDEXPENSES");
		hash_map.put(38, "PANVIEDOKYC");
		hash_map.put(39, "AADHARVIEDOKYC");
		hash_map.put(40, "TENTH");
		hash_map.put(41, "INTER");
		hash_map.put(42, "GRADUATION");
		hash_map.put(43, "UNIVERSITYOFFERLETTER");
		hash_map.put(44, "FEE");
		Iterator<Entry<Integer, String>> it = hash_map.entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry<Integer, String> set = (Map.Entry<Integer, String>) it.next();

			String documentSubType = set.getValue();

			UserDocumentStatus userDocument = userDocumentStatusRepo
					.findByUserIdAndDocumentTypeAndDocumentSubType(userId, DocumentType.Kyc, documentSubType);
			KycFileResponse document = new KycFileResponse();
			if (userDocument == null) {
				document.setUserId(null);
				document.setDocumentType(null);
				document.setDocumentSubType(null);
				document.setStatus(null);
				document.setDownloadUrl(null);
				listOfKycDocuments.add(document);
			} else {
				document.setUserId(userDocument.getUserId());
				document.setDocumentType(userDocument.getDocumentType().toString());
				document.setDocumentSubType(userDocument.getDocumentSubType());
				document.setStatus(userDocument.getStatus().toString());
				String kycType = userDocument.getDocumentSubType();
				User user = userRepo.findById(userId).get();
				KycFileResponse kycFileResponse = null;
				try {
					kycFileResponse = userServiceImpl.downloadFile(user.getId(), KycType.valueOf(kycType));
				} catch (IOException e) {

					e.printStackTrace();
				}
				if (kycFileResponse != null) {
					String urlDownload = kycFileResponse.getDownloadUrl();

					String[] splitingTheUrl = urlDownload.split(Pattern.quote("?"));
					String url = splitingTheUrl[0];
					document.setDownloadUrl(url);

				}
				listOfKycDocuments.add(document);

			}
		}
		return listOfKycDocuments;
	}

	protected String generateKycDownloadUrl(UserDocumentStatus profilePicDoc) {
		FileRequest fileRequest = new FileRequest();
		fileRequest.setFileName(profilePicDoc.getFileName());
		fileRequest.setFileType(FileType.Kyc);
		fileRequest.setUserId(profilePicDoc.getUserId());
		fileRequest.setFilePrifix(profilePicDoc.getDocumentSubType());
		FileResponse file = fileManagementService.getFile(fileRequest);
		return file.getDownloadUrl();
	}

	protected void populateExtraUserDetails(User user, UserResponse userResponse) {
		// Nothing to do here
	}

	protected void userDetails(User findById, UserResponse user) {
		if (findById.getPersonalDetails() != null) {
			user.setFirstName(findById.getPersonalDetails().getFirstName());
			user.setLastName(findById.getPersonalDetails().getLastName());
			user.setAddress(findById.getPersonalDetails().getAddress());
			if (findById.getPersonalDetails().getPanNumber() != null) {
				user.setPanNumber(findById.getPersonalDetails().getPanNumber());
			}
			if (findById.getPersonalDetails().getDob() != null) {
				user.setDob(expectedDateFormat.format(findById.getPersonalDetails().getDob()));
			}
			if (findById.getPersonalDetails().getOrginalDob() != null) {
				user.setOriginalDob(expectedDateFormat.format(findById.getPersonalDetails().getOrginalDob()));
			}

		}
		if (findById.getPersonalDetails().getFacebookUrl() != null) {
			user.setFacebookUrl(findById.getPersonalDetails().getFacebookUrl());
		}
		if (findById.getPersonalDetails().getTwitterUrl() != null) {
			user.setTwitterUrl(findById.getPersonalDetails().getTwitterUrl());
		}
		if (findById.getPersonalDetails().getLinkedinUrl() != null) {
			user.setLinkedinUrl(findById.getPersonalDetails().getLinkedinUrl());
		}
		user.setEmail(findById.getEmail());
		user.setMobileNumber(findById.getMobileNumber());
		user.setCity(findById.getCity());
		user.setPinCode(findById.getPinCode());
		user.setUtmSource(findById.getUrchinTrackingModule());
		user.setState(findById.getState());
		// user.setEnachType(findById.getEnachType());

		user.setVanNumber(findById.getVanNumber() == null ? "" : findById.getVanNumber());

		if (findById.getExperianSummary() != null && findById.getUserProfileRisk() != null) {
			user.setOxyScore(findById.getExperianSummary().getScore() + findById.getUserProfileRisk().getTotalScore());

		}
		if (findById.getProfessionalDetails() != null) {
			user.setCompanyName(findById.getProfessionalDetails().getCompanyName());
			user.setWorkExperience(findById.getProfessionalDetails().getWorkExperience());
		}
		if (findById.getFinancialDetails() != null) {
			user.setSalary(findById.getFinancialDetails().getNetMonthlyIncome());
		}
		if (findById.getBankDetails() != null) {
			user.setAccountNumber(findById.getBankDetails().getAccountNumber());
			user.setBankName(findById.getBankDetails().getBankName());
			user.setBranchName(findById.getBankDetails().getBranchName());
			user.setIfscCode(findById.getBankDetails().getIfscCode());
			user.setBankAddress(findById.getBankDetails().getAddress());
			user.setUserNameAccordingToBank(findById.getBankDetails().getUserName());

		}
		RiskProfileDto riskProfileDto = getReskProfileCalculation(findById.getId());
		User userDetails = userRepo.findById(findById.getId()).get();
		if (userDetails != null) {
			if (userDetails.getPrimaryType().equals(PrimaryType.BORROWER)) {
				CommentsRequestDto commentsRequestDto = getCommentAndRateOfInterestFromRadhaSir(userDetails.getId());
				user.setCommentsRequestDto(commentsRequestDto);
			}
		}
		user.setRiskProfileDto(riskProfileDto);
	}

	@Override
	@Transactional
	public SearchResultsDto<LoanResponseDto> searchLoans(int userId, SearchRequestDto searchRequestDto) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		Sort sort = null;
		if (searchRequestDto.getSortBy() != null) {
			sort = Sort.by(Direction.valueOf(searchRequestDto.getSortOrder().name()), searchRequestDto.getSortBy());
		} else {
			sort = Sort.by(Direction.DESC, "loanActiveDate");
		}
		Pageable searchPage = PageRequest.of(searchRequestDto.getPage().getPageNo() - 1,
				searchRequestDto.getPage().getPageSize(), sort);
		Specification<OxyLoan> spec = null;
		try {
			spec = specificationProvider.construct(OxyLoan.class, searchRequestDto);
		} catch (ClassNotFoundException e) {
			logger.error(e, e);
		} catch (IllegalAccessException e) {
			logger.error(e, e);
		} catch (InvocationTargetException e) {
			logger.error(e, e);
		} catch (NoSuchMethodException e) {
			logger.error(e, e);
		} catch (InstantiationException e) {
			logger.error(e, e);
		}
		Page<OxyLoan> findAll = oxyLoanRepo.findAll(spec, searchPage);
		SearchResultsDto<LoanResponseDto> results = new SearchResultsDto<LoanResponseDto>();
		if (findAll != null) {
			results.setTotalCount(Long.valueOf(findAll.getTotalElements()).intValue());
			results.setPageNo(searchRequestDto.getPage().getPageNo());
			results.setPageCount(findAll.getNumberOfElements());
			List<LoanResponseDto> loanRequests = new ArrayList<LoanResponseDto>();
			;
			results.setResults(loanRequests);
			for (OxyLoan request : findAll.getContent()) {
				int count = 0;
				loanRequests.add(populateLoanDetails(request, count));
			}
		}
		return results;
	}

	private LoanResponseDto populateLoanDetails(OxyLoan loan, int count) {
		LoanResponseDto loanResponse = new LoanResponseDto();
		loanResponse.setId(loan.getId());
		Double interestPendingAmount = loanEmiCardRepo.getInterestPendingTillDate(loan.getId());
		if (interestPendingAmount != null) {
			loanResponse.setInterestPendingTillDate(interestPendingAmount);
		}
		loanResponse.setDuration(loan.getDuration());
		loanResponse.setDurationType(loan.getDurationType().toString());
		loanResponse.setLoanRequestAmount(loan.getDisbursmentAmount());
		loanResponse.setLoanDisbursedAmount(loan.getDisbursmentAmount());
		loanResponse.setRateOfInterest(loan.getRateOfInterest());
		loanResponse.setRepaymentMethod(loan.getRepaymentMethod().name());
		loanResponse.setLenderDisplayId(loan.getLenderUserId().toString());
		loanResponse.setBorrowerDisplayId(loan.getBorrowerUserId().toString());
		loanResponse.setLoanStatus(loan.getLoanStatus().name());
		loanResponse.setLoanRequestId(loan.getLoanRespondId());
		LoanRequest loanRequest = loanRequestRepo.findById(loan.getLoanRespondId()).get();
		if(loanRequest!=null) {
		loanResponse.setLoanRequest(loanRequest.getLoanRequestId());
		}
		loanResponse.setLoanId(loan.getLoanId());

		User lenderUser = userRepo.findById(loan.getLenderUserId()).get();
		UserResponse user = new UserResponse();
		loanResponse.setLenderUser(user);
		user.setId(lenderUser.getId());
		userDetails(lenderUser, user);
		User borrowerUser = userRepo.findById(loan.getBorrowerUserId()).get();
		/*
		 * if (borrowerUser.getPrimaryType() == PrimaryType.BORROWER) { String
		 * mandateResponce = enachMandateResponce(loan.getLoanId());
		 * loanResponse.setBeforeEnach( mandateResponce == null ||
		 * mandateResponce.equalsIgnoreCase("DeActivated") ? true : false); if
		 * (mandateResponce != null) {
		 * loanResponse.setEnachMandateResponce(mandateResponce.equalsIgnoreCase(
		 * "Initiated") ? true : false);
		 * loanResponse.setAftreEnach(mandateResponce.equalsIgnoreCase("SUCCESS") ? true
		 * : false); } }
		 */
		user = new UserResponse();
		loanResponse.setBorrowerUser(user);
		loanResponse.setBorrowerFeePaid(loan.isBorrowerFeePaid());
		loanResponse.setLenderFeePaid(loan.isLenderFeePaid());
		user.setId(borrowerUser.getId());
		userDetails(borrowerUser, user);
		double profit = 0d;

		if (loan.getDurationType().equals(DurationType.Months)) {
			profit = (loan.getDisbursmentAmount() * loan.getDuration() * (loan.getRateOfInterest() / 12)) / 100d;

		} else {
			double calculatedRateOfInterest = (loan.getDisbursmentAmount() * loan.getRateOfInterest()) / 100000d;
			profit = Math.round(calculatedRateOfInterest) * loan.getDuration();
		}
		loanResponse.setProfit((double) Math.round(profit));
		List<LoanEmiCard> loanEmiCards = loan.getLoanEmiCards();
		if (CollectionUtils.isNotEmpty(loanEmiCards)) {
			LoanEmiCard firstPendingEmiCard = null;
			int noOfEmisPaid = 0;
			double dueEmiAmount = 0;
			double emiAmount = 0;
			double remaingAmount = 0;
			double paidAmount = 0;
			for (LoanEmiCard card : loanEmiCards) {
				firstPendingEmiCard = (firstPendingEmiCard == null && card.getEmiPaidOn() == null) ? card
						: firstPendingEmiCard;
				noOfEmisPaid += (card.getEmiPaidOn() == null ? 0 : 1);
				dueEmiAmount += (card.getEmiPaidOn() == null && card.getRemainingEmiAmount() == 0 ? 0d
						: card.getEmiAmount());
				emiAmount = card.getEmiAmount();
				remaingAmount += (card.getRemainingEmiAmount() == 0 ? 0d : card.getRemainingEmiAmount());
				paidAmount += (card.getEmiPaidOn() == null ? 0d : card.getEmiAmount());
				if (card.getEmiNumber() == 1) {
					loanResponse.setEmiStartDate(expectedDateFormat.format(card.getEmiDueOn()));
				}

			}
			if (loan.getDurationType().equals(DurationType.Months)) {
				loanResponse.setNoRemaingEmis(loanRequest.getDuration() - noOfEmisPaid);
			} else {

				loanResponse.setNoRemaingEmis(loanEmiCards.size() - noOfEmisPaid);

			}
			loanResponse.setNoOfEmisPaid(noOfEmisPaid);
			loanResponse.setDueEmiAmount(dueEmiAmount + remaingAmount - paidAmount);
			loanResponse.setPrincipalAmountPending(
					(double) Math.round(loanRequest.getLoanRequestAmount() - loan.getPrincipalRepaid()));
			loanResponse.setTotalAmountPaid(
					(emiAmount * loanRequest.getDuration()) - (dueEmiAmount + remaingAmount) + paidAmount);
			loanResponse.setTotalAmountPending(dueEmiAmount + remaingAmount - paidAmount);

			if (loan.getBorrowerDisbursedDate() != null) {
				loanResponse.setBorrowerDisbursementDate(expectedDateFormat.format(loan.getBorrowerDisbursedDate()));
			}
			if (firstPendingEmiCard != null) {
				loanResponse.setEmiAmount((double) Math.round(firstPendingEmiCard.getEmiAmount()));
			}
		}
		loanResponse.setIsECSActivated(loan.getIsECSActivated());
		int mandateId = 0;

		if (loan.getEnachMandate() != null && !loan.getEnachMandate().isEmpty()) {
			for (EnachMandate mandate : loan.getEnachMandate()) {

				mandateId = mandate.getId();
			}
		}
		loanResponse.setEnachMandateId(mandateId);
		List<EnachMandate> enachMandate = enachMandateRepo.findByOxyLoanId(loan.getId());

		EnachMandateResponseForEmi emi = new EnachMandateResponseForEmi();
		EnachMandateResponseForPrincipal principal = new EnachMandateResponseForPrincipal();

		if (enachMandate != null && !enachMandate.isEmpty()) {
			for (int i = 0; i < enachMandate.size(); i++) {
				String status = enachMandate.get(i).getMandateStatus().name();
				if (i == 0 & count == 0) {
					emi.setMandateId(enachMandate.get(i).getId());
					loanResponse.setEnachMandateId(enachMandate.get(i).getId());
					emi.setAmount(enachMandate.get(i).getMaxAmount());
					loanResponse.setEmiAmount(enachMandate.get(i).getMaxAmount());
					emi.setStartDate(enachMandate.get(i).getDebitStartDate());
					emi.setEndDate(enachMandate.get(i).getDebitEndDate());
					emi.setMandateStatus(enachMandate.get(i).getMandateStatus().name());
					if (status.equalsIgnoreCase(MandateStatus.INITIATED.toString())) {
						loanResponse.setEnachInitiated(true);
						loanResponse.setEnachSuccess(false);
					} else if (status.equalsIgnoreCase(MandateStatus.SUCCESS.toString())) {
						loanResponse.setEnachInitiated(false);
						loanResponse.setEnachSuccess(true);
					}

				} else if (i == 1 && count == 1) {
					principal.setMandateId(enachMandate.get(i).getId());
					loanResponse.setEnachMandateId(enachMandate.get(i).getId());
					principal.setAmount(enachMandate.get(i).getMaxAmount());
					loanResponse.setEmiAmount(enachMandate.get(i).getMaxAmount());
					principal.setStartDate(enachMandate.get(i).getDebitStartDate());
					principal.setEndDate(enachMandate.get(i).getDebitEndDate());
					principal.setMandateStatus(enachMandate.get(i).getMandateStatus().name());
					if (status.equalsIgnoreCase(MandateStatus.INITIATED.toString())) {
						loanResponse.setEnachInitiated(true);
						loanResponse.setEnachSuccess(false);
					} else if (status.equalsIgnoreCase(MandateStatus.SUCCESS.toString())) {
						loanResponse.setEnachInitiated(false);
						loanResponse.setEnachSuccess(true);
					}
				}

			}

		}
		loanResponse.setEnachMandateResponseForEmi(emi);
		loanResponse.setEnachMandateResponseForPrincipal(principal);

		FileRequest fileRequest = new FileRequest();
		String outputFileName = loan.getLoanId() + ".pdf";
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.Agreement);
		fileRequest.setFilePrifix(FileType.Agreement.name());
		FileResponse file = fileManagementService.getFile(fileRequest);
		loanResponse.setAgreementUrl(file.getDownloadUrl());
		return loanResponse;
	}

	@Override
	@Transactional
	public LoanResponseDto actOnRequest(int userId, Integer loanRequestId, LoanStatus loanStatus)
			throws PdfGeenrationException, IOException {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		LoanRequest loanRequest = loanRequestRepo.findById(loanRequestId).get();
		if (loanRequest.getLoanStatus() == LoanStatus.Accepted || loanRequest.getLoanStatus() == LoanStatus.Rejected) {
			throw new ActionNotAllowedException("This Request already " + loanRequest.getLoanStatus(),
					ErrorCodes.ACTION_ALREDY_DONE);
		}
		loanRequest.setLoanStatus(loanStatus);
		loanRequest = loanRequestRepo.save(loanRequest);
		Integer parentRequestId = loanRequest.getParentRequestId();
		if (parentRequestId == null) {
			throw new ActionNotAllowedException("Action " + loanStatus + " is not allowed as this is not a respond",
					ErrorCodes.PERMISSION_DENIED);
		}
		LoanRequest parentRequest = loanRequestRepo.findById(parentRequestId).get();
		if (!parentRequest.getUserId().equals(userId)) {
			throw new AccessDeniedException("Not authorized to perform this operation on this object");
		}
		User lenderUser = null;
		User borrowerUser = null;
		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			borrowerUser = user;
			lenderUser = userRepo.findById(loanRequest.getUserId()).get();
		} else {
			lenderUser = user;
			borrowerUser = userRepo.findById(loanRequest.getUserId()).get();
		}
		if (loanStatus == LoanStatus.Accepted) {
			User requestedUser = (user.getPrimaryType() == PrimaryType.BORROWER ? borrowerUser : lenderUser);
			LoanRequestDto loanRequestDto = new LoanRequestDto();
			loanRequestDto.setLoanRequestAmount(loanRequest.getLoanRequestAmount());
			isLimitExceeded(requestedUser.getId(), user, loanRequestDto);
			OxyLoan finalLoan = new OxyLoan();
			finalLoan.setBorrowerUserId(borrowerUser.getId());
			LoanRequest borrowerApplication = loanRequestRepo
					.findByUserIdAndParentRequestIdIsNull(borrowerUser.getId());
			LoanRequest lenderApplication = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(lenderUser.getId());
			finalLoan.setBorrowerParentRequestId(borrowerApplication.getId());
			finalLoan.setLenderParentRequestId(lenderApplication.getId());
			finalLoan.setLenderUserId(lenderUser.getId());
			finalLoan.setLoanAcceptedDate(new Date());
			finalLoan.setLoanRequestId(parentRequestId);
			finalLoan.setLoanRespondId(loanRequest.getId());
			finalLoan.setDisbursmentAmount(loanRequest.getLoanRequestAmount());
			finalLoan.setLoanId(new Long(System.currentTimeMillis()).toString());
			String loanId = finalLoan.getLoanId();
			LoanRequest loanRequestObject = loanRequestRepo.findByLoanId(loanId);
			if (loanRequestObject != null) {
				if (borrowerApplication.getDurationBySir() > 0) {
					if (borrowerApplication.getRepaymentMethodForBorrower().equalsIgnoreCase("PI")) {
						loanRequestObject.setRepaymentMethod(RepaymentMethod.PI);
					} else {
						loanRequestObject.setRepaymentMethod(RepaymentMethod.I);
					}
				}
				loanRequestRepo.save(loanRequestObject);
			}
			if (borrowerApplication.getDurationBySir() > 0) {
				if (borrowerApplication.getRepaymentMethodForBorrower().equalsIgnoreCase("PI")) {

					finalLoan.setRepaymentMethod(RepaymentMethod.PI);

				} else {
					finalLoan.setRepaymentMethod(RepaymentMethod.I);

				}
				finalLoan.setRateOfInterest(borrowerApplication.getRateOfInterestToBorrower());
			} else {

				finalLoan.setRepaymentMethod(loanRequest.getRepaymentMethod());
				finalLoan.setRateOfInterest(loanRequest.getRateOfInterest());
			}
			finalLoan.setDuration(loanRequest.getDuration());
			finalLoan.setDurationType(loanRequest.getDurationType());
			finalLoan = oxyLoanRepo.save(finalLoan);
			finalLoan.setLoanId(LOAN_ID_FORMAT.replace("{ID}", finalLoan.getId().toString()));
			finalLoan = oxyLoanRepo.save(finalLoan);
			loanRequest.setLoanId(finalLoan.getLoanId());
			loanRequestRepo.save(loanRequest);

			TemplateContext templateContext = new TemplateContext();

			PersonalDetails borrwoerPersonalDetails = borrowerUser.getPersonalDetails();
			templateContext.put("borrowerFullName",
					borrwoerPersonalDetails.getFirstName() + " " + borrwoerPersonalDetails.getLastName());
			templateContext.put("borrowerFatherName", borrwoerPersonalDetails.getFatherName());

			String borrowerAddress = borrowerUser.getPersonalDetails().getAddress();
			Pattern pt = Pattern.compile("[^a-zA-Z0-9]");
			Matcher match = pt.matcher(borrowerAddress);
			while (match.find()) {
				String s = match.group();
				borrowerAddress = borrowerAddress.replaceAll("\\" + s, " ");
			}
			templateContext.put("borrowerAddress", borrowerAddress);
			PersonalDetails lenderPersonalDetails = lenderUser.getPersonalDetails();
			templateContext.put("lenderFullName",
					lenderPersonalDetails.getFirstName() + " " + lenderPersonalDetails.getLastName());
			templateContext.put("lenderFatherName", lenderPersonalDetails.getFatherName());
			String lenderAddress = lenderPersonalDetails.getAddress();
			Pattern pt1 = Pattern.compile("[^a-zA-Z0-9]");
			Matcher match1 = pt1.matcher(lenderAddress);
			while (match1.find()) {
				String s = match1.group();
				lenderAddress = lenderAddress.replaceAll("\\" + s, " ");
			}

			templateContext.put("lenderAddress", lenderAddress);
			int borrowerId = finalLoan.getBorrowerUserId();
			LoanRequest loanRequestDetails = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(borrowerId);

			if (loanRequestDetails.getDurationBySir() > 0) {
				if (loanRequestDetails.getDurationType().equals(DurationType.Months)) {
					templateContext.put("duration", loanRequestDetails.getDurationBySir());
					templateContext.put("rateOfInterestOfLender", loanRequestDetails.getRateOfInterestToLender());
					templateContext.put("rateOfInterestOfBorrower", loanRequestDetails.getRateOfInterestToBorrower());
				} else {
					templateContext.put("duration", loanRequestDetails.getDurationBySir());
					templateContext.put("durationType", loanRequestDetails.getDurationType());
					templateContext.put("rateOfInterestOfLender", loanRequestDetails.getRateOfInterestToLender());
					templateContext.put("rateOfInterestOfBorrower", loanRequestDetails.getRateOfInterestToBorrower());
				}
			}
			templateContext.put("loanAmount", finalLoan.getDisbursmentAmount());

			DateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

			formatter.setTimeZone(TimeZone.getTimeZone("IST")); // here set timezone

			templateContext.put("agreementDate", formatter.format(new Date()));

			Calendar calendar = Calendar.getInstance();
			String heading = "";
			calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(this.defaultEmiDay));
			double emiAmount = 0.0;
			int count = 0;
			double totalPrincipalAmount = 0.0;
			String emiLastDate = null;

			templateContext.put("heading", heading);
			templateContext.put("totalPrincipalAmount", totalPrincipalAmount);
			templateContext.put("emiLastDate", emiLastDate);

			int duration = 0;
			double rateOfInterestFromLender = 0.0;
			double rateOfInterestFromBorrower = 0.0;
			double disbursmentAmount = finalLoan.getDisbursmentAmount();
			String repaymentTypeForLender = null;
			String repaymentTypeForBorrower = null;
			double platFormEarningRateOfInterest = 0.0;
			String durationType = "";
			if (loanRequestDetails != null) {
				if (loanRequestDetails.getDurationBySir() > 0) {
					duration = loanRequestDetails.getDurationBySir();
					rateOfInterestFromLender = loanRequestDetails.getRateOfInterestToLender();
					rateOfInterestFromBorrower = loanRequestDetails.getRateOfInterestToBorrower();
					repaymentTypeForLender = loanRequestDetails.getRepaymentMethodForLender();
					repaymentTypeForBorrower = loanRequestDetails.getRepaymentMethodForBorrower();
					durationType = loanRequestDetails.getDurationType().toString();
					platFormEarningRateOfInterest = rateOfInterestFromBorrower - rateOfInterestFromLender;
				}

			}
			templateContext.put("platFormEarningRateOfInterest", platFormEarningRateOfInterest);

			EmiRequestDto lenderEmiRequestDto = generatingEmiTableForLender(disbursmentAmount, rateOfInterestFromLender,
					repaymentTypeForLender, duration, durationType);
			if (lenderEmiRequestDto != null) {
				templateContext.put("totalInterestAmount", lenderEmiRequestDto.getTotalInterestAmount());
				templateContext.put("payableAmount", lenderEmiRequestDto.getPayableAmount());
				if (repaymentTypeForLender.equalsIgnoreCase("PI")) {
					templateContext.put("diminishingAmount", lenderEmiRequestDto.getDiminishingAmount());
				} else {
					templateContext.put("diminishingAmount", lenderEmiRequestDto.getDiminishingAmount());
				}
				templateContext.put("emiDataTable", lenderEmiRequestDto.getEmiTable());
			}

			EmiRequestDto borrowerEmiRequestDto = null;
			if (loanRequestDetails != null) {
				if (loanRequestDetails.getDurationType().equals(DurationType.Months)) {
					borrowerEmiRequestDto = generatingEmiTableForBorrower(disbursmentAmount, rateOfInterestFromBorrower,
							repaymentTypeForBorrower, duration);
				} else {
					borrowerEmiRequestDto = generateEmiTableOfDaysForBorrower(disbursmentAmount,
							rateOfInterestFromBorrower, repaymentTypeForBorrower, duration);
				}
			}
			if (borrowerEmiRequestDto != null) {
				templateContext.put("interstAmount", borrowerEmiRequestDto.getOneInterestAmount());
				templateContext.put("emiDataTableForBorrower", borrowerEmiRequestDto.getEmiTable());
				templateContext.put("totalInterestForBorrower", borrowerEmiRequestDto.getTotalInterestAmount());
				templateContext.put("totalPayableForBorrower", borrowerEmiRequestDto.getPayableAmount());
				templateContext.put("emiForBorrower", borrowerEmiRequestDto.getEmiAmount());
			}

			String outputFileName = finalLoan.getBorrowerUserId() + finalLoan.getLoanId() + ".pdf";
			String generatedPdf = "";
			if (loanRequestDetails != null && loanRequestDetails.getDurationType().equals(DurationType.Months)) {
				generatedPdf = pdfEngine.generatePdf("agreement/agreementTemplate.xml", templateContext,
						outputFileName);
			} else {
				generatedPdf = pdfEngine.generatePdf("agreement/agreementDaysTemplate.xml", templateContext,
						outputFileName);
			}
			FileRequest fileRequest = new FileRequest();
			fileRequest.setInputStream(new FileInputStream(generatedPdf));
			fileRequest.setFileName(outputFileName);
			fileRequest.setFileType(FileType.Agreement);
			fileRequest.setFilePrifix(FileType.Agreement.name());
			try {
				FileResponse putFile = fileManagementService.putFile(fileRequest);

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			File file = new File(generatedPdf);
			boolean fileDeleted = file.delete();

			String outputFileNameForLender = finalLoan.getLenderUserId() + finalLoan.getLoanId() + ".pdf";
			String generatedPdfForLender = "";
			if (loanRequestDetails != null && loanRequestDetails.getDurationType().equals(DurationType.Months)) {
				generatedPdfForLender = pdfEngine.generatePdf("agreement/lenderAgreementTemplate.xml", templateContext,
						outputFileNameForLender);
			} else {
				generatedPdfForLender = pdfEngine.generatePdf("agreement/lenderAgreementForDaysTemplate.xml",
						templateContext, outputFileNameForLender);
			}
			FileRequest fileRequestForLender = new FileRequest();
			fileRequestForLender.setInputStream(new FileInputStream(generatedPdfForLender));
			fileRequestForLender.setFileName(outputFileNameForLender);
			fileRequestForLender.setFileType(FileType.Agreement);
			fileRequestForLender.setFilePrifix(FileType.Agreement.name());
			try {
				FileResponse putFile = fileManagementService.putFile(fileRequestForLender);

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			File fileForLender = new File(generatedPdfForLender);
			boolean fileDeletedForLender = fileForLender.delete();

			double emiInterstAmount = computeEmpUponAccepted(finalLoan, loanRequest);

			// Sending email and SMS
			TemplateContext emailTemplateContext = new TemplateContext();
			emailTemplateContext.put("firstName",
					user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName());
			emailTemplateContext.put("requesterName", loanRequest.getUser().getPersonalDetails().getFirstName() + " "
					+ loanRequest.getUser().getPersonalDetails().getLastName());
			emailTemplateContext.put("applicationNumber", loanRequest.getLoanRequestId());
			emailTemplateContext.put("userId", user.getUniqueNumber());
			emailTemplateContext.put("loanRequestedAmount", loanRequest.getLoanRequestAmount());
			emailTemplateContext.put("acceptedLoanAmount", loanRequest.getLoanRequestAmount());
			emailTemplateContext.put("loanInterestRate", loanRequest.getRateOfInterest());
			emailTemplateContext.put("loanDuration", loanRequest.getDuration());
			emailTemplateContext.put("loanEmiType", loanRequest.getRepaymentMethod().getDisplayValue());
			emailTemplateContext.put("loanEmiAmount", Math.round(emiInterstAmount));
			emailTemplateContext.put("loanRespondedDate",
					this.expectedDateFormat.format(loanRequest.getLoanRequestedDate()));
			String mailsubject = (user.getPrimaryType() == PrimaryType.LENDER
					? loanRequestMailSubject.replace("$responseType", "Request")
					: loanRequestMailSubject.replace("$responseType", "Offer")); // Mail Goes to Borrower so subject is
																					// like this..
			String emailTemplateName = user.getPrimaryType() == PrimaryType.LENDER ? "lender-accepts.template"
					: "borrower-accepts.template";

			EmailRequest emailRequest = new EmailRequest(new String[] {
					user.getPrimaryType() == PrimaryType.LENDER ? borrowerUser.getEmail() : lenderUser.getEmail() },
					mailsubject, emailTemplateName, emailTemplateContext);
			if (!utm.equals(borrowerUser.getUrchinTrackingModule())) {
				EmailResponse emailResponse = emailService.sendEmail(emailRequest);
				if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
					throw new RuntimeException(emailResponse.getErrorMessage());
				}
			}

			String mailSubjectForAdmin = "Mutual Acceptance"; // Mail Goes to admin
			String mailTempalteForAdmin = "mutual-acceptance-mail-to-admin.template";

			EmailRequest emailRequest1 = new EmailRequest(new String[] { this.supportEmail }, mailSubjectForAdmin,
					mailTempalteForAdmin, emailTemplateContext);
			EmailResponse emailResponse1 = emailService.sendEmail(emailRequest1);
			if (emailResponse1.getStatus() == EmailResponse.Status.FAILED) {
				throw new RuntimeException(emailResponse1.getErrorMessage());
			}

			// Sending SMS to Lender/Borrower
			String mobileNumber = user.getPrimaryType() == PrimaryType.LENDER ? borrowerUser.getMobileNumber()
					: lenderUser.getMobileNumber();
			String templateName = user.getPrimaryType() == PrimaryType.LENDER ? "newLenderAccepts"
					: "newBorrowerAccepts";
			MobileRequest mobileRequest = new MobileRequest(mobileNumber, templateName);
			if (!utm.equals(borrowerUser.getUrchinTrackingModule())) {
				mobileRequest.setSenderId("OXYLON");
				Map<String, String> variblesMap = new HashMap<>();
				variblesMap.put("VAR1", user.getUniqueNumber());
				mobileRequest.setVariblesMap(variblesMap);
				try {
					mobileUtil.sendTransactionalMessage(mobileRequest);
				} catch (Exception e) {
					logger.error("SMS failed : ", e);
				}
			}
		} else

		{
			lenderBorrowerConversationRepo.deleteByLenderUserIdAndBorrowerUserId(lenderUser.getId(),
					borrowerUser.getId());

		}

		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setUserId(loanRequest.getUserId());
		loanResponseDto.setLoanRequestId(loanRequest.getId());
		loanResponseDto.setLoanRequest(loanRequest.getLoanRequestId());
		return loanResponseDto;
	}

	private EmiRequestDto generatingEmiTableForLender(double disbursedAmount, double rateOfInterestForLender,
			String repaymentType, int duration, String durationType) {
		EmiRequestDto lenderEmiRequestDto = new EmiRequestDto();
		// double firstPrincipalAmount = 0.0;
		String emiDataTable = "";

		double diminishingAmount = 0.0;
		if (repaymentType.equalsIgnoreCase("I")) {
			double principleAmountChange = 0.0;
			double totalInterestAmount = 0.0;

			double payableAmount = 0.0;
			for (int i = 1; i <= duration; i++) {

				double interestAmount = 0d;
				if (durationType.equalsIgnoreCase(DurationType.Months.toString())) {
					interestAmount = Math.round((disbursedAmount * (rateOfInterestForLender / 12)) / 100d);

				} else {
					interestAmount = Math.round((disbursedAmount * ((rateOfInterestForLender / 12) / 100d)) / 30);

				}
				totalInterestAmount = totalInterestAmount + interestAmount;

				if (i == duration) {
					principleAmountChange = disbursedAmount;
				}
				emiDataTable = emiDataTable
						+ generateFobLoanTableForLender(i + "", principleAmountChange + "", interestAmount + "");
			}

			payableAmount = disbursedAmount + totalInterestAmount;
			if (durationType.equalsIgnoreCase(DurationType.Months.toString())) {
				diminishingAmount = (disbursedAmount + totalInterestAmount) / duration;
			} else {
				diminishingAmount = (disbursedAmount + totalInterestAmount);

			}

			lenderEmiRequestDto.setTotalInterestAmount(totalInterestAmount);
			lenderEmiRequestDto.setPayableAmount(payableAmount);
			lenderEmiRequestDto.setDiminishingAmount(diminishingAmount);
			lenderEmiRequestDto.setEmiTable(emiDataTable);
		} else {
			if (repaymentType.equalsIgnoreCase("PI")) {
				double totalInterestAmount = 0.0;
				double payableAmount = 0.0;
				for (int i = 1; i <= duration; i++) {
					double interest = 0d;
					double emiPrincipalAmount = 0d;
					if (durationType.equalsIgnoreCase(DurationType.Months.toString())) {
						interest = Math.round((disbursedAmount * (rateOfInterestForLender / 12)) / 100d);

						emiPrincipalAmount = Math.round(disbursedAmount / duration);
					} else {
						interest = Math.round(((disbursedAmount * (rateOfInterestForLender / 12)) / 100d) / 30);
						emiPrincipalAmount = Math.round(disbursedAmount / duration);
					}

					emiDataTable = emiDataTable
							+ generateFobLoanTableForLender(i + "", emiPrincipalAmount + "", interest + "");
					totalInterestAmount = totalInterestAmount + interest;
				}
				payableAmount = disbursedAmount + totalInterestAmount;
				payableAmount = disbursedAmount + totalInterestAmount;
				if (durationType.equalsIgnoreCase(DurationType.Months.toString())) {
					diminishingAmount = (disbursedAmount + totalInterestAmount) / duration;
				} else {
					diminishingAmount = (disbursedAmount + totalInterestAmount);

				}
				lenderEmiRequestDto.setTotalInterestAmount(totalInterestAmount);
				lenderEmiRequestDto.setPayableAmount(payableAmount);
				lenderEmiRequestDto.setDiminishingAmount(diminishingAmount);
				lenderEmiRequestDto.setEmiTable(emiDataTable);
			}
		}
		return lenderEmiRequestDto;

	}

	public EmiRequestDto generatingEmiTableForBorrower(double disbursedAmount, double rateOfInterestFromBorrower,
			String repaymentType, int duration) {
		EmiRequestDto lenderEmiRequestDto = new EmiRequestDto();
		String emiDataTableForBorrower = "";

		if (repaymentType.equalsIgnoreCase("PI")) {
			double totalInterestForBorrower = 0.0;
			for (int i = 1; i <= duration; i++) {

				double interstAmount = Math.round((disbursedAmount * (rateOfInterestFromBorrower / 12)) / 100d);
				if (i == 1) {
					lenderEmiRequestDto.setOneInterestAmount(interstAmount);
				}
				double emiPrincipalAmount = Math.round(disbursedAmount / duration);
				double emiAmountValue = interstAmount + emiPrincipalAmount;

				totalInterestForBorrower = totalInterestForBorrower + interstAmount;
				emiDataTableForBorrower = emiDataTableForBorrower + generateFobLoanTableForBorrower(i + "",
						emiPrincipalAmount + "", interstAmount + "", emiAmountValue + "");
			}
			double totalPayableForBorrower = disbursedAmount + totalInterestForBorrower;
			double emiForBorrower = Math.round(totalPayableForBorrower / duration);
			lenderEmiRequestDto.setEmiTable(emiDataTableForBorrower);
			lenderEmiRequestDto.setTotalInterestAmount(totalInterestForBorrower);
			lenderEmiRequestDto.setPayableAmount(totalPayableForBorrower);
			lenderEmiRequestDto.setEmiAmount(emiForBorrower);

		} else {
			if (repaymentType.equalsIgnoreCase("I")) {
				double totalInterestForBorrower = 0.0;
				double emiPrincipalAmount = 0.0;
				for (int i = 1; i <= duration; i++) {

					double interstAmount = Math.round((disbursedAmount * (rateOfInterestFromBorrower / 12)) / 100d);
					if (i == 1) {
						lenderEmiRequestDto.setOneInterestAmount(interstAmount);
					}
					if (i == duration) {
						emiPrincipalAmount = disbursedAmount;
					} else {

						emiPrincipalAmount = 0.0;
					}
					double emiAmountValue = interstAmount + emiPrincipalAmount;

					totalInterestForBorrower = totalInterestForBorrower + interstAmount;
					emiDataTableForBorrower = emiDataTableForBorrower + generateFobLoanTableForBorrower(i + "",
							emiPrincipalAmount + "", interstAmount + "", emiAmountValue + "");
				}
				double totalPayableForBorrower = disbursedAmount + totalInterestForBorrower;
				double emiForBorrower = Math.round(totalPayableForBorrower / duration);
				lenderEmiRequestDto.setEmiTable(emiDataTableForBorrower);
				lenderEmiRequestDto.setTotalInterestAmount(totalInterestForBorrower);
				lenderEmiRequestDto.setPayableAmount(totalPayableForBorrower);
				lenderEmiRequestDto.setEmiAmount(emiForBorrower);
			}
		}
		return lenderEmiRequestDto;
	}

	public EmiRequestDto generateEmiTableOfDaysForBorrower(double disbursmentAmount, double rateOfInterestFromBorrower,
			String repaymentTypeForBorrower, int duration) {
		EmiRequestDto emiRequestDto = new EmiRequestDto();
		double totalInterestForBorrower = 0.0;
		String emiDataTableForBorrower = "";

		double payableAmount = 0d;

		for (int i = 1; i <= duration; i++) {

			double principalAmount = 0d;
			double calculatedInterestPerDay = (disbursmentAmount * rateOfInterestFromBorrower) / 100000d;

			double interestAmount = (double) Math.round(calculatedInterestPerDay);

			if (i == duration) {
				principalAmount = disbursmentAmount;
			}

			double emiAmountValue = interestAmount + principalAmount;
			totalInterestForBorrower += interestAmount;

			emiDataTableForBorrower += generateFobLoanTableForBorrower(i + "", principalAmount + "",
					interestAmount + "", emiAmountValue + "");

		}

		emiRequestDto.setTotalInterestAmount(totalInterestForBorrower);
		emiRequestDto.setEmiTable(emiDataTableForBorrower);
		emiRequestDto.setPayableAmount(disbursmentAmount + totalInterestForBorrower);
		emiRequestDto.setEmiAmount(totalInterestForBorrower / duration);

		return emiRequestDto;

	}

	public EmiRequestDto generatingEmiTableInDaysForLender(double disbursmentAmount, double rateOfInterestFromLender,
			String repaymentTypeForBorrower, int duration) {
		EmiRequestDto emiRequestDto = new EmiRequestDto();
		double totalInterestForBorrower = 0.0;
		String emiDataTableForBorrower = "";
		double interestAmount = Math.round((disbursmentAmount * (rateOfInterestFromLender / 12 / 100) * duration) / 30);
		totalInterestForBorrower += interestAmount;
		double emiAmountValue = interestAmount + disbursmentAmount;
		emiDataTableForBorrower += generateFobLoanTableForLender(1 + "", emiAmountValue + "", interestAmount + "");
		emiRequestDto.setTotalInterestAmount(interestAmount);
		emiRequestDto.setEmiTable(emiDataTableForBorrower);
		emiRequestDto.setPayableAmount(disbursmentAmount + interestAmount);
		emiRequestDto.setEmiAmount(disbursmentAmount + interestAmount);
		emiRequestDto.setDiminishingAmount(disbursmentAmount + interestAmount);

		return emiRequestDto;

	}

	public String generateFobLoanTableForLender(String emis, String principleAmountChange, String interest) {
		String emiStartDate = null;

		String rowdata = "<fo:table-row>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">"

				+ emis + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">"

				+ emiStartDate + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">"

				+ principleAmountChange + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">" + interest + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " </fo:table-row>";

		return rowdata;
	}

	public String generateFobLoanTableForBorrower(String emis, String principleAmountChange, String interstAmount,
			String emiAmountValue) {

		String rowdata = "<fo:table-row>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">"

				+ emis + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">"

				+ principleAmountChange + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">" + interstAmount + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"4px\">\r\n"
				+ " <fo:block text-align=\"left\">"

				+ emiAmountValue + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " </fo:table-row>";

		return rowdata;
	}

	protected double computeEmpUponAccepted(OxyLoan oxyLoan, LoanRequest loanRequest) {
		double emiPrincipalAmount = 0d;
		double emiInterstAmount = (oxyLoan.getDisbursmentAmount() * (oxyLoan.getRateOfInterest() / 12)) / 100d;
		switch (loanRequest.getRepaymentMethod()) {
		case RepaymentMethod.PI:
			emiPrincipalAmount = oxyLoan.getDisbursmentAmount() / oxyLoan.getDuration().doubleValue();
			break;
		default:
			break;
		}
		return emiPrincipalAmount + emiInterstAmount;
	}

	protected double generateEmiCard(OxyLoan oxyLoan, LoanRequest loanRequest) {
		List<LoanEmiCard> loanEmiCards = new ArrayList<LoanEmiCard>();
		Calendar calendar = Calendar.getInstance();
		Calendar getDay = Calendar.getInstance();
		Calendar currentDate = Calendar.getInstance();
		getDay.set(Calendar.DAY_OF_MONTH, Integer.valueOf(this.midOftheMonth));
		calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(this.defaultEmiDay));
		double emiPrincipalAmount = 0d;
		double emiInterstAmount = 0d;

		if (getDay.getTime().before(currentDate.getTime())) {

			double interstAmountForLender = 0.0;
			double principalAmountForLender = 0.0;
			double emiAmountForLender = 0.0;

			for (int i = 1; i <= loanRequest.getDuration(); i++) {
				/*
				 * if (i == 1) { calendar.add(Calendar.MONTH, 2); } else {
				 */
				calendar.add(Calendar.MONTH, 1);
				// }
				LoanEmiCard loanEmiCard = new LoanEmiCard();
				loanEmiCard.setLoanId(oxyLoan.getId());
				loanEmiCard.setEmiNumber(i);
				loanEmiCard.setEmiDueOn(calendar.getTime());
				int borrowerParentid = oxyLoan.getBorrowerParentRequestId();
				LoanRequest loanRequestDetails = loanRequestRepo.findById(borrowerParentid).get();
				if (loanRequestDetails != null) {

					if (loanRequestDetails.getRepaymentMethodForLender().equalsIgnoreCase("PI")) {
						interstAmountForLender = (oxyLoan.getDisbursmentAmount()
								* (loanRequestDetails.getRateOfInterestToLender())) / 100d;
						principalAmountForLender = oxyLoan.getDisbursmentAmount() / oxyLoan.getDuration().doubleValue();
						emiAmountForLender = interstAmountForLender + principalAmountForLender;
					} else {
						if (loanRequestDetails.getRepaymentMethodForLender().equalsIgnoreCase("I")) {

							interstAmountForLender = (oxyLoan.getDisbursmentAmount()
									* (loanRequestDetails.getRateOfInterestToLender())) / 100d;
							if (i == loanRequest.getDuration()) {
								principalAmountForLender = oxyLoan.getDisbursmentAmount();

							}

							emiAmountForLender = interstAmountForLender + principalAmountForLender;

						}

					}

					if (loanRequestDetails.getRepaymentMethodForBorrower().equalsIgnoreCase("PI")) {
						double interestAmount = oxyLoan.getDisbursmentAmount()
								* (loanRequestDetails.getRateOfInterestToBorrower()) / 100;
						double principalAmount = oxyLoan.getDisbursmentAmount() / oxyLoan.getDuration();
						loanEmiCard.setEmiInterstAmount(Math.round(interestAmount));
						loanEmiCard.setEmiPrincipalAmount(Math.round(principalAmount));
						loanEmiCard.setEmiAmount(Math.round(interestAmount + principalAmount));

					} else {
						double interestAmountOfI = oxyLoan.getDisbursmentAmount()
								* (loanRequestDetails.getRateOfInterestToBorrower()) / 100;
						double principalAmountOfI = 0.0;
						if (i == loanRequestDetails.getDurationBySir()) {
							principalAmountOfI = oxyLoan.getDisbursmentAmount();
						}
						loanEmiCard.setEmiInterstAmount(Math.round(interestAmountOfI));
						loanEmiCard.setEmiPrincipalAmount(Math.round(principalAmountOfI));
						loanEmiCard.setEmiAmount(Math.round(interestAmountOfI + principalAmountOfI));
					}

				}
				loanEmiCard.setLenderEmiPrincipalAmount(Math.round(principalAmountForLender));
				loanEmiCard.setLenderEmiInterestAmount(Math.round(interstAmountForLender));
				loanEmiCard.setLenderEmiAmount(Math.round(emiAmountForLender));
				loanEmiCards.add(loanEmiCard);
			}
		} else {

			double interstAmountForLender = 0.0;
			double principalAmountForLender = 0.0;
			double emiAmountForLender = 0.0;
			for (int i = 1; i <= loanRequest.getDuration(); i++) {
				calendar.add(Calendar.MONTH, 1);
				LoanEmiCard loanEmiCard = new LoanEmiCard();
				loanEmiCard.setLoanId(oxyLoan.getId());
				loanEmiCard.setEmiNumber(i);
				loanEmiCard.setEmiDueOn(calendar.getTime());
				if (loanRequest.getDuration().intValue() == i
						&& loanRequest.getRepaymentMethod() == RepaymentMethod.I) {
					loanEmiCard.setEmiPrincipalAmount(oxyLoan.getDisbursmentAmount());
				}
				int borrowerParentid = oxyLoan.getBorrowerParentRequestId();
				LoanRequest loanRequestDetails = loanRequestRepo.findById(borrowerParentid).get();
				if (loanRequestDetails != null) {

					if (loanRequestDetails.getRepaymentMethodForLender().equalsIgnoreCase("PI")) {
						interstAmountForLender = (oxyLoan.getDisbursmentAmount()
								* (loanRequestDetails.getRateOfInterestToLender())) / 100d;
						principalAmountForLender = oxyLoan.getDisbursmentAmount() / oxyLoan.getDuration().doubleValue();
						emiAmountForLender = interstAmountForLender + principalAmountForLender;
					} else {
						if (loanRequestDetails.getRepaymentMethodForLender().equalsIgnoreCase("I")) {
							interstAmountForLender = (oxyLoan.getDisbursmentAmount()
									* (loanRequestDetails.getRateOfInterestToLender())) / 100d;
							if (i == loanRequest.getDuration()) {
								principalAmountForLender = oxyLoan.getDisbursmentAmount();

							}

							emiAmountForLender = interstAmountForLender + principalAmountForLender;

						}

					}

					if (loanRequestDetails.getRepaymentMethodForBorrower().equalsIgnoreCase("PI")) {
						double interestAmount = oxyLoan.getDisbursmentAmount()
								* (loanRequestDetails.getRateOfInterestToBorrower()) / 100;
						double principalAmount = oxyLoan.getDisbursmentAmount() / oxyLoan.getDuration();
						loanEmiCard.setEmiInterstAmount(Math.round(interestAmount));
						loanEmiCard.setEmiPrincipalAmount(Math.round(principalAmount));
						loanEmiCard.setEmiAmount(Math.round(interestAmount + principalAmount));

					} else {
						double interestAmountOfI = oxyLoan.getDisbursmentAmount()
								* (loanRequestDetails.getRateOfInterestToBorrower()) / 100;
						double principalAmountOfI = 0.0;
						if (i == loanRequestDetails.getDurationBySir()) {
							principalAmountOfI = oxyLoan.getDisbursmentAmount();
						}
						loanEmiCard.setEmiInterstAmount(Math.round(interestAmountOfI));
						loanEmiCard.setEmiPrincipalAmount(Math.round(principalAmountOfI));
						loanEmiCard.setEmiAmount(Math.round(interestAmountOfI + principalAmountOfI));
					}

				}

				loanEmiCard.setLenderEmiPrincipalAmount(Math.round(principalAmountForLender));
				loanEmiCard.setLenderEmiInterestAmount(Math.round(interstAmountForLender));
				loanEmiCard.setLenderEmiAmount(Math.round(emiAmountForLender));
				loanEmiCards.add(loanEmiCard);

			}
		}

		loanEmiCardRepo.saveAll(loanEmiCards);
		return emiPrincipalAmount + emiInterstAmount;
	}

	@Override
	@Transactional
	public LoanResponseDto getLoanRequest(int userId, int loanRequestId) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		LoanRequest loanRequest = loanRequestRepo.findById(loanRequestId).get();

		// If you are a borrower/lender then you are allowed to see a lender/borrower
		// request
		if (loanRequest.getParentRequestId() == null && loanRequest.getUserPrimaryType() == user.getPrimaryType()
				&& !loanRequest.getUserId().equals(userId)) {
			throw new AccessDeniedException("You are not permitted to view this object");
		}
		// If it is a responded request then you are allowed to see other responses of
		// same primary type but not opposite type.

		/*
		 * if (loanRequest.getParentRequestId() != null &&
		 * loanRequest.getUserPrimaryType() != user.getPrimaryType()) { throw new
		 * AccessDeniedException("You are not permitted to view this object"); }
		 */

		LoanResponseDto populateLoanDetails = populateLoanDetails(loanRequest, user);
		if (loanRequest.getParentRequestId() == null) {
			OxyAccountDetailsDto dashboardResponse = (OxyAccountDetailsDto) getDashboard(loanRequest.getUserId(), false,
					userRepo.findById(loanRequest.getUserId()).get());
			populateLoanDetails.setLoanDisbursedAmount(dashboardResponse.getAmountDisbursed());
		}

		/*
		 * int borrowerId = loanRequest.getUserId();
		 * 
		 * LoanRequest request =
		 * loanRequestRepo.findByUserIdAndParentRequestIdIsNull(borrowerId);
		 */

		String applicationId = loanRequest.getLoanRequestId();

		LoanRequest request = loanRequestRepo.findByLoanRequestId(applicationId);
		int idBasedOnApplication = request.getUserId();
		populateLoanDetails.setAcceptedAmount(getAcceptedAndAgreedState(idBasedOnApplication));

		return populateLoanDetails;
	}

	private LoanResponseDto populateLoanDetails(LoanRequest loanRequest, User user1) {
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setUserId(loanRequest.getUserId());
		loanResponseDto.setLoanRequestId(loanRequest.getId());
		loanResponseDto.setLoanRequest(loanRequest.getLoanRequestId());
		loanResponseDto.setDuration(loanRequest.getDuration());
		loanResponseDto.setExpectedDate(expectedDateFormat.format(loanRequest.getExpectedDate()));
		loanResponseDto.setLoanPurpose(loanRequest.getLoanPurpose());
		loanResponseDto.setLoanDisbursedAmount(
				loanRequest.getDisbursmentAmount() == null ? 0d : loanRequest.getDisbursmentAmount());
		loanResponseDto.setLoanRequestedDate(expectedDateFormat.format(loanRequest.getLoanRequestedDate()));
		loanResponseDto.setParentRequestId(loanRequest.getParentRequestId());
		loanResponseDto.setRateOfInterest(loanRequest.getRateOfInterest());
		loanResponseDto.setRepaymentMethod(loanRequest.getRepaymentMethod().name());
		loanResponseDto.setUserDisplayId(loanRequest.getUserId().toString());
		loanResponseDto.setLoanRequestAmount(loanRequest.getLoanRequestAmount());
		if (loanRequest.getLoanOfferedAmount() == null) {
			loanResponseDto.setDuration(loanRequest.getDuration());
			loanResponseDto.setRateOfInterest(loanRequest.getRateOfInterest());
			loanResponseDto.setLoanRequestAmount(loanRequest.getLoanRequestAmount());
		} else {
			if (loanRequest.getLoanOfferedAmount() != null) {
				loanResponseDto.setDurationGivenByAdmin(loanRequest.getLoanOfferedAmount().getDuration());
				loanResponseDto.setRateOfInterestGivenByAdmin(loanRequest.getLoanOfferedAmount().getRateOfInterest());
				loanResponseDto.setLoanOfferedAmount(loanRequest.getLoanOfferedAmount().getLoanOfferedAmount());
			}

		}

		User requestedUserId = userRepo.findById(loanRequest.getUserId()).get();
		UserResponse user = new UserResponse();
		loanResponseDto.setUser(user);
		user.setId(requestedUserId.getId());
		userDetails(requestedUserId, user);

		if (requestedUserId.getPrimaryType() == PrimaryType.BORROWER && loanRequest.getParentRequestId() == null) {
			Integer lenderUserId = requestedUserId.getPrimaryType() == PrimaryType.LENDER ? requestedUserId.getId()
					: user1.getId();
			Integer borrowerUserId = requestedUserId.getPrimaryType() == PrimaryType.BORROWER ? requestedUserId.getId()
					: user1.getId();
			int count = lenderBorrowerConversationRepo.countByLenderUserIdAndBorrowerUserId(lenderUserId,
					borrowerUserId);
			loanResponseDto.setAlreadyOffered(count > 0);
			List<LoanStatus> loanStatuses = new ArrayList<LoanStatus>();
			loanStatuses.add(LoanStatus.Active);
			List<OxyLoan> activeLoans = oxyLoanRepo.findByLenderUserIdAndBorrowerUserIdAndLoanStatusIn(lenderUserId,
					borrowerUserId, loanStatuses);
			loanResponseDto.setAlreadyActive(CollectionUtils.isNotEmpty(activeLoans));
		}
		Integer borrowerUserId = loanRequest.getUserId();
		Integer parentRequestId = loanRequest.getId();
		if (parentRequestId != null) {
			int averageRating = 0;
			Integer count = lenderRatingToBorrowerApplicationRepo.getCountValueOfLenders(parentRequestId);
			Integer sum = lenderRatingToBorrowerApplicationRepo.getSumValueOfRating(parentRequestId);
			if (count != null && sum != null) {
				averageRating = sum / count;
				loanResponseDto.setLendersRatingValue(averageRating);
			}
		}
		if (borrowerUserId != null) {
			loanResponseDto.setBorrowerKycDocuments(getKycDetails(borrowerUserId));
			loanResponseDto.setBorrowerCommentsDetails(getBorrowerComments(borrowerUserId));
			if (loanRequest.getCommentsBySir() != null) {
				loanResponseDto.setCommentsByRadhaSir(loanRequest.getCommentsBySir());
				loanResponseDto.setRatingByRadhaSir(loanRequest.getRatingBySir());
			}
		} else {
			loanResponseDto.setBorrowerKycDocuments(null);
		}
		User userDetails = userRepo.findById(user1.getId()).get();
		if (userDetails != null) {
			if (userDetails.getPrimaryType().equals(PrimaryType.BORROWER)) {
				CommentsRequestDto commentsRequestDto = getCommentAndRateOfInterestFromRadhaSir(userDetails.getId());
				loanResponseDto.setCommentsRequestDto(commentsRequestDto);
			}

		}
		return loanResponseDto;
	}

	public double getAcceptedAndAgreedState(int idBasedOnApplication) {
		List<OxyLoan> oxyLoan = oxyLoanRepo.findByBorrowerUserId(idBasedOnApplication);
		double disburmentedAmountBasedOnStatus = 0.0;

		for (OxyLoan oxy : oxyLoan) {
			if (LoanStatus.Active.toString().equals(oxy.getLoanStatus().toString())
					|| LoanStatus.Agreed.toString().equals(oxy.getLoanStatus().toString())) {

				disburmentedAmountBasedOnStatus = disburmentedAmountBasedOnStatus + oxy.getDisbursmentAmount();

			}

		}

		return disburmentedAmountBasedOnStatus;

	}

	@Override
	@Transactional
	public LoanResponseDto getDefaultLoanRequest(int userId) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);

		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
		if (loanRequest != null) {
			return populateLoanDetails(loanRequest, user);

		} else {
			return null;
		}
	}

	@Override
	@Transactional
	public KycFileResponse downloadLoanAgrement(int userId, Integer loanRequestId) {
		LoanRequest oxyLoanRequest = loanRequestRepo.findById(loanRequestId).get();
		Integer parentRequestId = oxyLoanRequest.getParentRequestId();
		if (parentRequestId == null) {
			throw new ActionNotAllowedException(
					"This loan request " + loanRequestId + "will not have any agreement to download",
					ErrorCodes.ENITITY_NOT_FOUND);
		}
		OxyLoan oxyLoan = oxyLoanRepo.findByLoanRequestIdAndLoanRespondId(parentRequestId, loanRequestId);
		KycFileResponse fileResponse = new KycFileResponse();
		FileRequest fileRequest = new FileRequest();
		if (oxyLoan != null) {
			String outputFileName = oxyLoan.getLoanId() + ".pdf";
			fileRequest.setFileName(outputFileName);
			fileRequest.setFileType(FileType.Agreement);
			fileRequest.setFilePrifix(FileType.Agreement.name());
			FileResponse file = fileManagementService.getFile(fileRequest);

			fileResponse.setDownloadUrl(file.getDownloadUrl());
			fileResponse.setFileName(outputFileName);
			OxyLoan oxyloansDetails = oxyLoanRepo.getLenderAndBorrowerAgreement(oxyLoan.getId());
			if (oxyloansDetails != null) {

				FileRequest fileRequestForLender = new FileRequest();
				String outputFileNameForLender = oxyLoan.getLenderUserId() + oxyLoan.getLoanId() + ".pdf";
				fileRequestForLender.setFileName(outputFileNameForLender);
				fileRequestForLender.setFileType(FileType.Agreement);
				fileRequestForLender.setFilePrifix(FileType.Agreement.name());
				FileResponse fileForLender = fileManagementService.getFile(fileRequestForLender);
				fileResponse.setDownloadUrlForLender(fileForLender.getDownloadUrl());

				FileRequest fileRequestForBorrower = new FileRequest();
				String outputFileNameForBorrower = oxyLoan.getBorrowerUserId() + oxyLoan.getLoanId() + ".pdf";
				fileRequestForBorrower.setFileName(outputFileNameForBorrower);
				fileRequestForBorrower.setFileType(FileType.Agreement);
				fileRequestForBorrower.setFilePrifix(FileType.Agreement.name());
				FileResponse fileForBorrower = fileManagementService.getFile(fileRequestForBorrower);
				fileResponse.setDownloadUrlForBorrower(fileForBorrower.getDownloadUrl());
				logger.info("borrower Agreement end");
			}

		}

		return fileResponse;
	}

	@Override
	public List<UserResponse> getParticipatedUsers(int requestId) {
		Integer currentUser = authorizationService.getCurrentUser();
		User user = userRepo.findById(currentUser).get();
		user.getBankDetails();
		return null;
	}

	@Override
	@Transactional
	public LoanResponseDto updateRespondedLoanRequest(int userId, Integer loanRequestId,
			LoanRequestDto loanRequestDto) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		return updateRequest(userId, loanRequestId, loanRequestDto, true);
	}

	private LoanResponseDto updateRequest(int userId, Integer loanRequestId, LoanRequestDto loanRequestDto,
			boolean overrideCommitementAmount) {
		LoanRequest loanRequest = loanRequestRepo.findById(loanRequestId).get();
		if (loanRequestDto.getLoanRequestAmount() != null) {
			if (overrideCommitementAmount) {
				loanRequest.setLoanRequestAmount(loanRequestDto.getLoanRequestAmount());
			} else {
				double requestedAmount = (loanRequest.getLoanRequestAmount() - loanRequest.getDisbursmentAmount())
						+ loanRequestDto.getLoanRequestAmount();
				if (requestedAmount >= 0d) {
					loanRequest.setLoanRequestAmount(loanRequest.getLoanRequestAmount().doubleValue()
							+ loanRequestDto.getLoanRequestAmount().doubleValue());
				} else {
					throw new LimitExceededException("You can drop upto "
							+ (loanRequest.getLoanRequestAmount() - loanRequest.getDisbursmentAmount())
							+ " amount onlly", ErrorCodes.LIMIT_REACHED);
				}
				if (loanRequest.getParentRequestId() == null && loanRequest.getLoanRequestAmount()
						.doubleValue() > new Double(this.maximumOutstandingAmount).doubleValue()) {
					throw new LimitExceededException("You can not request/offer more than "
							+ this.maximumOutstandingAmount + ", maximum can be"
							+ (loanRequestDto.getLoanRequestAmount()
									- (requestedAmount - Double.valueOf(this.maximumOutstandingAmount).doubleValue()))
							+ " amount onlly", ErrorCodes.LIMIT_REACHED);
				}
			}
		}
		if (loanRequestDto.getDuration() != null) {
			loanRequest.setDuration(loanRequestDto.getDuration());
		}
		if (loanRequestDto.getLoanPurpose() != null) {
			loanRequest.setLoanPurpose(loanRequestDto.getLoanPurpose());
		}
		if (loanRequestDto.getRateOfInterest() != null) {
			loanRequest.setRateOfInterest(loanRequestDto.getRateOfInterest());
		}
		if (loanRequestDto.getRepaymentMethod() != null) {
			loanRequest.setRepaymentMethod(RepaymentMethod.valueOf(loanRequestDto.getRepaymentMethod()));
		}
		if (loanRequestDto.getExpectedDate() != null) {
			try {
				loanRequest.setExpectedDate(expectedDateFormat.parse(loanRequestDto.getExpectedDate()));
			} catch (ParseException e) {
				logger.error("Error: ", e);
				throw new DataFormatException("Illegal Date format. It should be dd/MM/yyyy",
						ErrorCodes.INVALID_DATE_FORMAT);
			}
		}
		if (loanRequestDto.getDurationType().equalsIgnoreCase("months")) {
			loanRequest.setDurationType(DurationType.Months);
		} else if (loanRequestDto.getDurationType().equalsIgnoreCase("days")) {
			loanRequest.setDurationType(DurationType.Days);
		}
		loanRequest = loanRequestRepo.save(loanRequest);

		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setUserId(userId);
		loanResponseDto.setLoanRequestId(loanRequest.getId());
		loanResponseDto.setLoanRequest(loanRequest.getLoanRequestId());
		return loanResponseDto;
	}

	@Override
	@Transactional
	public LoanResponseDto updateLoanRequest(int userId, LoanRequestDto loanRequestDto) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		Integer loanRequestId = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId()).getId();
		return updateRequest(userId, loanRequestId, loanRequestDto, false);
	}

	@Override
	public LoanResponseDto updateLoanRequest(int userId, Integer loanRequestId,
			BorrowerDetailsRequestDto borrowerDetailsRequestDto) {
		throw new OperationNotAllowedException("Not Allowed to comment at present", ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	@Transactional
	public EsignInitResponse esign(int userId, Integer loanRequestId, UploadAgreementRequestDto agreementFileRequest) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		LoanRequest oxyLoanRequest = loanRequestRepo.findById(loanRequestId).get();
		if (oxyLoanRequest == null) {
			throw new NoSuchElementException("No Loan present to esign against");
		}

		if (oxyLoanRequest.getAdminComments() == null) {
			throw new OperationNotAllowedException("Lone is not Approved", ErrorCodes.INVALID_OPERATION);
		}

		Integer parentRequestId = oxyLoanRequest.getParentRequestId();
		if (parentRequestId == null) {
			throw new ActionNotAllowedException(
					"This loan request " + loanRequestId + " is not having any agreement to esign and upload",
					ErrorCodes.ENITITY_NOT_FOUND);
		}
		OxyLoan finalLoan = oxyLoanRepo.findByLoanRequestIdAndLoanRespondId(parentRequestId, loanRequestId);
		if ((user.getPrimaryType() == PrimaryType.BORROWER && finalLoan.isBorrowerEsigned())
				|| (user.getPrimaryType() == PrimaryType.LENDER && finalLoan.isLenderEsigned())) {
			throw new ActionNotAllowedException("You had esigned this document already", ErrorCodes.ACTION_ALREDY_DONE);
		}
		/*
		 * if (user.getPrimaryType() == PrimaryType.BORROWER &&
		 * !finalLoan.isLenderEsigned()) { throw new
		 * ActionNotAllowedException("Lender has to esign first before you esign. Please contact support"
		 * , ErrorCodes.LENDER_NOT_RESPONDED); }
		 */
		if (agreementFileRequest == null) {
			agreementFileRequest = new UploadAgreementRequestDto();
		}
		String outputFileName = finalLoan.getLoanId() + ".pdf";
		FileRequest fileRequest = new FileRequest();
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.Agreement);
		fileRequest.setFilePrifix(FileType.Agreement.name());

		String esignTypeString = agreementFileRequest.geteSignType();
		EsignType esignType = (esignTypeString == null) ? EsignType.AADHAR : EsignType.valueOf(esignTypeString);
		EsignInitResponse esignResponse = null;
		EsignInitRequest esignRequest = new EsignInitRequest();
		switch (esignType) {
		case EsignType.AADHAR:
			esignRequest.setSignerCity("Hyderabad"); // HaardCodede for now..replace
			esignRequest.setSignerName(user.getPersonalDetails().getFirstName());
			esignRequest.setData(getBase64EncodedFileStream(fileRequest));
			esignRequest.setInfo("Esign to lend/borrower a loan");
			esignRequest.setPurpose("Esign to lend/borrower a loan");
			esignRequest.setType("pdf");
			esignRequest.setSignPageNumber("1"); // hard coded for now..remove later
			String xCoordinate = null;
			String yCoordinate = null;
			if (user.getPrimaryType() == PrimaryType.LENDER) {
				xCoordinate = "40";
				yCoordinate = "50";
			} else {
				xCoordinate = "570";
				yCoordinate = "50";
			}
			esignRequest.setxCoordinate(xCoordinate);
			esignRequest.setyCoordinate(yCoordinate);
			esignResponse = esignService.initEsign(esignRequest);
			break;
		case EsignType.MOBILE:
			Random random = new Random();
			String otpValue = String.format("%04d", random.nextInt(10000));
			String otpSessionId = mobileUtil.sendCustomOtp(user.getMobileNumber(), otpValue, this.esignTemplateName);
			esignResponse = new EsignInitResponse();
			esignResponse.setId(otpSessionId);
			StringBuilder subject = new StringBuilder(user.getPrimaryType().name())
					.append(" esign mobile OTP for loan ").append(finalLoan.getLoanId()).append(" is - " + otpValue);
			EmailRequest emailRequest = new EmailRequest(new String[] { this.supportEmail }, subject.toString(), null,
					null);
			emailService.sendEmail(emailRequest);
			break;
		}

		EsignTransactions esignTransaction = new EsignTransactions();
		esignTransaction.setEsignType(esignType);
		esignTransaction.setTransactionId(esignResponse.getId());
		esignTransaction.setCoOrdinateX(
				esignRequest.getxCoordinate() == null ? -1 : Integer.valueOf(esignRequest.getxCoordinate()));
		esignTransaction.setCoOrdinateY(
				esignRequest.getyCoordinate() == null ? -1 : Integer.valueOf(esignRequest.getyCoordinate()));
		esignTransaction.setEsignedDocDownloaded(false);
		esignTransaction.setTransactonStatus(TransactonStatus.INITIATED);
		esignTransaction = esignTransactionsRepo.save(esignTransaction);
		if (user.getPrimaryType() == PrimaryType.LENDER) {
			finalLoan.setLenderEsignId(esignTransaction.getId());
		} else {
			finalLoan.setBorrowerEsignId(esignTransaction.getId());
			finalLoan.setLenderEsignId(0000);
		}
		finalLoan = oxyLoanRepo.save(finalLoan);
		esignResponse.setWebhook_security_key(null);
		return esignResponse;
	}

	private String getBase64EncodedFileStream(FileRequest fileRequest) {
		FileResponse file = fileManagementService.getFile(fileRequest);
		try {
			byte[] byteArray = IOUtils
					.toByteArray(new BufferedInputStream(new URL(file.getDownloadUrl()).openStream()));
			return Base64.getEncoder().encodeToString(byteArray);
		} catch (MalformedURLException e) {
			logger.error("", e);
			return null;
		} catch (IOException e) {
			logger.error("", e);
			return null;
		}
	}

	@Override
	@Transactional
	public LoanResponseDto uploadEsignedAgreementPdf(int userId, Integer loanRequestId,
			UploadAgreementRequestDto agreementFileRequest) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		LoanRequest oxyLoanRequest = loanRequestRepo.findById(loanRequestId).get();
		if (oxyLoanRequest == null) {
			throw new NoSuchElementException("No Loan present to esign against");
		}
		Integer parentRequestId = oxyLoanRequest.getParentRequestId();
		if (parentRequestId == null) {
			throw new ActionNotAllowedException(
					"This loan request " + loanRequestId + " is not having any agreement to esign and upload",
					ErrorCodes.ENITITY_NOT_FOUND);
		}
		OxyLoan finalLoan = oxyLoanRepo.findByLoanRequestIdAndLoanRespondId(parentRequestId, loanRequestId);
		if ((user.getPrimaryType() == PrimaryType.BORROWER && finalLoan.isBorrowerEsigned())
				|| (user.getPrimaryType() == PrimaryType.LENDER && finalLoan.isLenderEsigned())) {
			throw new ActionNotAllowedException("You had esigned this document already", ErrorCodes.ACTION_ALREDY_DONE);
		}
		/*
		 * if (user.getPrimaryType() == PrimaryType.BORROWER &&
		 * !finalLoan.isLenderEsigned()) { throw new
		 * ActionNotAllowedException("Lender has to esign first before you esign. Please contact support"
		 * , ErrorCodes.LENDER_NOT_RESPONDED); }
		 */

		Integer otherUserId = null;
		Integer esignTransactionId = null;
		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			finalLoan.setBorrowerEsigned(true);
			finalLoan.setLenderEsigned(true);
			otherUserId = finalLoan.getLenderUserId();
			esignTransactionId = finalLoan.getBorrowerEsignId();

		} else {
			finalLoan.setLenderEsigned(true);
			otherUserId = finalLoan.getBorrowerUserId();
			esignTransactionId = finalLoan.getLenderEsignId();
		}

		// Download file using the esign api
		EsignFetchResponse downloadEsignedDocument = null;
		EsignTransactions esignTransactions = esignTransactionsRepo.findById(esignTransactionId).get();
		String transactionId = esignTransactions.getTransactionId();
		switch (esignTransactions.getEsignType()) {
		case EsignType.AADHAR:
			downloadEsignedDocument = esignService.downloadEsignedDocument(transactionId);
			break;
		case EsignType.MOBILE:
			boolean otpVerified = mobileUtil.verifyOtp(user.getMobileNumber(), transactionId,
					agreementFileRequest.getOtpValue());
			if (!otpVerified) {
				throw new MobileOtpVerifyFailedException("Invalid OTP value please check.", ErrorCodes.INVALID_OTP);
			}
			break;
		}
		esignTransactions.setTransactonStatus(TransactonStatus.DOWNLOADED);
		esignTransactions.setEsignedDocDownloaded(true);
		if (downloadEsignedDocument != null) {
			if (downloadEsignedDocument.getDocuments() != null) {

				logger.info("Doc Info {}", downloadEsignedDocument.getDocuments()[0]);
			}
			BufferedInputStream inputStream;
			try {
				inputStream = new BufferedInputStream(
						new URL(downloadEsignedDocument.getDocuments()[0].getDynamic_url()).openStream());
			} catch (IOException e1) {
				throw new RuntimeException("Error while downloading esigned documnet", e1);
			}

			String outputFileName = finalLoan.getLoanId() + ".pdf";
			String originalFileName = outputFileName;
			FileRequest agreementFile = new FileRequest();
			agreementFile.setFileName(outputFileName);
			agreementFile.setFileType(FileType.Agreement);
			agreementFile.setFilePrifix(FileType.Agreement.name());

			agreementFile.setInputStream(inputStream);
			try {
				FileResponse putFile = fileManagementService.putFile(agreementFile);

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		} else if (esignTransactions.getEsignType() == EsignType.AADHAR) {
			logger.error("There was some issue in downloading the file");
			esignTransactions.setTransactonStatus(TransactonStatus.FAILED);
			esignTransactions.setEsignedDocDownloaded(false);
		}

		if (finalLoan.isBorrowerEsigned() && finalLoan.isLenderEsigned()) {
			finalLoan.setLoanStatus(LoanStatus.Active);
			LoanRequest esigningUserRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
			LoanRequest otherUserRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(otherUserId);
			esigningUserRequest.setDisbursmentAmount(
					esigningUserRequest.getDisbursmentAmount() + finalLoan.getDisbursmentAmount());
			otherUserRequest
					.setDisbursmentAmount(otherUserRequest.getDisbursmentAmount() + finalLoan.getDisbursmentAmount());
			esigningUserRequest = loanRequestRepo.save(esigningUserRequest);
			otherUserRequest = loanRequestRepo.save(otherUserRequest);
			lenderBorrowerConversationRepo.deleteByLenderUserIdAndBorrowerUserId(finalLoan.getLenderUserId(),
					finalLoan.getBorrowerUserId());
			if (finalLoan.getDurationType().equals(DurationType.Months)) {
				generateEmiCard(finalLoan, oxyLoanRequest);
			} else {
				generateEmiCardForDays(finalLoan, oxyLoanRequest);
			}
			generateENACHMandate(finalLoan);
		}
		finalLoan = oxyLoanRepo.save(finalLoan);
		esignTransactions = esignTransactionsRepo.save(esignTransactions);
		String loanId = oxyLoanRequest.getLoanId();
		OxyLoan oxyLoan = oxyLoanRepo.findByLoanId(loanId);
		if (oxyLoan != null) {
			int borrowerParentRequestId = oxyLoan.getBorrowerParentRequestId();
			int agreedLoansCount = oxyLoanRepo.getAgreedLoansCount(borrowerParentRequestId);
			if (agreedLoansCount == 0) {
				String mailsubject = "BorrowerDealStructure"; // Mail Goes to Borrower so subject is like this..
				String emailTemplateName = "agreement.template";

				int userId1 = finalLoan.getBorrowerUserId();
				KycFileResponse kycFileResponse = downloadLoanAgrement(userId1, loanRequestId);
				TemplateContext context = new TemplateContext();
				if (kycFileResponse != null) {
					context.put("agreement", kycFileResponse.getDownloadUrl());
				}

				int id = 6680; // admin id
				KycFileResponse kycFileResponseOfRepayment = null;
				try {
					kycFileResponseOfRepayment = userServiceImpl.downloadFile(id, KycType.REPAYMENTACCOUNTDETAILS);
				} catch (IOException e) {

					e.printStackTrace();
				}

				if (kycFileResponseOfRepayment != null) {
					String urlDownload = kycFileResponseOfRepayment.getDownloadUrl();
					String[] splitingTheUrl = urlDownload.split(Pattern.quote("?"));
					String url = splitingTheUrl[0];
					context.put("repaymentAccountDetails", url);
				}
				context.put("name", user.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase());
				context.put("borrowerId", user.getUniqueNumber());
				LoanRequest loanRequestValues = loanRequestRepo.findById(borrowerParentRequestId).get();
				List<String> listOfLoanIds = oxyLoanRepo.getLoanIdsBasedOnApplicationId(borrowerParentRequestId);
				String loanIds = " ";
				for (int i = 0; i < listOfLoanIds.size(); i++) {
					loanIds = loanIds + listOfLoanIds.get(i) + ",";
				}
				Date date = new Date();
				String date1 = expectedDateFormat.format(date);
				context.put("loanIds", loanIds);
				context.put("loanAmount", loanRequestValues.getLoanOfferedAmount().getLoanOfferedAmount());
				context.put("processingFree", loanRequestValues.getLoanOfferedAmount().getBorrowerFee());
				context.put("netDisburmentAmount", loanRequestValues.getLoanOfferedAmount().getNetDisbursementAmount());
				context.put("duration", loanRequestValues.getLoanOfferedAmount().getDuration());
				context.put("emi", loanRequestValues.getLoanOfferedAmount().getEmiAmount());
				LoanEmiCard firstEmiInfo = loanEmiCardRepo.getFirstEmiDetails(oxyLoan.getId());
				int emiNumber = loanRequestValues.getLoanOfferedAmount().getDuration();
				LoanEmiCard lastEmiInfo = loanEmiCardRepo.getLastEmiDetails(oxyLoan.getId(), emiNumber);
				if (firstEmiInfo != null) {
					context.put("emiStartDate", expectedDateFormat.format(firstEmiInfo.getEmiDueOn()));
				}
				if (lastEmiInfo != null) {
					context.put("emiEndDate", expectedDateFormat.format(lastEmiInfo.getEmiDueOn()));
				}
				context.put("bankAccount", user.getBankDetails().getAccountNumber());
				context.put("ifsc", user.getBankDetails().getIfscCode());
				context.put("address", user.getPersonalDetails().getAddress());
				context.put("email", user.getEmail());
				context.put("mobileNumber", user.getMobileNumber());
				context.put("currentDate", date1);

				EmailRequest emailRequest = new EmailRequest(new String[] { "radhakrishna.t@oxyloans.com",
						"ramadevi@oxyloans.com", "archana.n@oxyloans.com", "subbu@oxyloans.com" }, mailsubject,
						emailTemplateName, context);
				EmailResponse emailResponse = emailService.sendEmail(emailRequest);
				if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
					throw new RuntimeException(emailResponse.getErrorMessage());
				}
			}
		}
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setUserId(oxyLoanRequest.getUserId());
		loanResponseDto.setLoanRequestId(oxyLoanRequest.getId());
		loanResponseDto.setLoanRequest(oxyLoanRequest.getLoanRequestId());
		return loanResponseDto;
	}

	public void generateENACHMandate(OxyLoan loanDetails) {
		double maxAmount = 0;
		double totalAmount = 0;
		Date startDate = null;
		Date endDate = null;
		double finalAmount = 0;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		Set<Integer> loanIds = new HashSet<Integer>();
		String loanId = loanDetails.getLoanId();
		StringBuilder converting = new StringBuilder(loanId);
		String convertedLoanId = converting.delete(0, 2).toString();

		List<LoanEmiCard> listLoanEmicard = loanEmiCardRepo
				.findByLoanIdOrderByEmiNumber(Integer.parseInt(convertedLoanId));

		for (LoanEmiCard emis : listLoanEmicard) {

			if (emis.getEmiNumber() == 1) {
				maxAmount = emis.getEmiAmount();
			}
			if (emis.getEmiNumber() == 1) {
				startDate = emis.getEmiDueOn();
			}
			if (emis.getEmiNumber() == listLoanEmicard.size()) {
				endDate = emis.getEmiDueOn();
			}
		}
		totalAmount = loanDetails.getDisbursmentAmount();
		String txnId = "ECST" + dateUtil.getHHmmMMddyyyyDateFormat() + loanDetails.getBorrowerUserId()
				+ loanDetails.getId();
		OxyLoan oxyLoanDetails = oxyLoanRepo.findByLoanId(loanId);

		EnachMandate enachMandate = new EnachMandate();

		if (oxyLoanDetails.getDurationType().equals(DurationType.Months)) {
			enachMandate.setOxyLoanId(oxyLoanDetails.getId());
			enachMandate.setOxyLoan(oxyLoanDetails);

			enachMandate.setAmountType(AmountType.valueOf(EnachMandate.AmountType.M.name()));
			enachMandate.setFrequency(Frequency.valueOf(EnachMandate.Frequency.MNTH.name()));

			enachMandate.setDebitStartDate(startDate);

			enachMandate.setDebitEndDate(endDate);
			enachMandate.setMandateTransactionId(txnId);
			enachMandate.setMaxAmount(maxAmount);
			enachMandate.setMandateStatus(MandateStatus.INITIATED);
			String mailSubjectForAdmin = "Enach"; // Mail Goes to admin
			String mailTempalteForAdmin = "enach-mail-to-admin.template";

			TemplateContext templateContext = new TemplateContext();
			templateContext.put("LoanId", oxyLoanDetails.getId());

			templateContext.put("AmountType", AmountType.valueOf(EnachMandate.AmountType.M.name()));
			templateContext.put("Frequeny", Frequency.valueOf(EnachMandate.Frequency.MNTH.name()));

			templateContext.put("EmiStartDate", expectedDateFormat.format(startDate));
			templateContext.put("EmiEndDate", expectedDateFormat.format(endDate));

			templateContext.put("TransactionId", txnId);
			templateContext.put("MaxAmount", Math.round(maxAmount));
			templateContext.put("MandateStatus", MandateStatus.INITIATED);
			Date date = new Date();
			String date1 = expectedDateFormat.format(date);
			templateContext.put("CurrentDate", date1);

			EmailRequest emailRequest = new EmailRequest(new String[] { this.supportEmail }, mailSubjectForAdmin,
					mailTempalteForAdmin, templateContext);
			EmailResponse emailResponse = emailService.sendEmail(emailRequest);
			if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
				throw new RuntimeException(emailResponse.getErrorMessage());
			}

			enachMandate = enachMandateRepo.save(enachMandate);

		} else if (oxyLoanDetails.getDurationType().equals(DurationType.Days)) {
			Date endDate2 = null;
			for (LoanEmiCard emis : listLoanEmicard) {

				if (emis.getEmiNumber() == listLoanEmicard.size() - 1) {
					endDate = emis.getEmiDueOn();
				}

				if (emis.getEmiNumber() == listLoanEmicard.size()) {

					endDate2 = emis.getEmiDueOn();
					finalAmount = emis.getEmiAmount();

				}

			}

			List<EnachMandate> mandateList = new ArrayList<>();

			for (int i = 1; i <= 2; i++) {
				EnachMandate enachMandate1 = new EnachMandate();
				enachMandate1.setOxyLoanId(oxyLoanDetails.getId());
				enachMandate1.setOxyLoan(oxyLoanDetails);

				enachMandate1.setAmountType(AmountType.valueOf(EnachMandate.AmountType.M.name()));
				enachMandate1.setFrequency(Frequency.valueOf(EnachMandate.Frequency.DAIL.name()));

				enachMandate1.setMandateTransactionId(txnId + Integer.toString(i));
				enachMandate1.setDebitStartDate(startDate);
				enachMandate1.setDebitEndDate(endDate);

				enachMandate1.setMaxAmount(maxAmount);
				if (i == 2) {
					enachMandate1.setMaxAmount(finalAmount);
					enachMandate1.setDebitStartDate(endDate);
					enachMandate1.setDebitEndDate(endDate2);

				}

				enachMandate1.setMandateStatus(MandateStatus.INITIATED);
				mandateList.add(enachMandate1);
			}

			enachMandateRepo.saveAll(mandateList);
		}

	}

	public LoanResponseDto feePaid(int loanId, PrimaryType primaryType, LoanRequestDto loanRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public SearchResultsDto<LoanEmiCardResponseDto> getEmiCard(int loanId) {
		User user = userRepo.findById(authorizationService.getCurrentUser()).get();
		OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get();
		if (user.getPrimaryType() != PrimaryType.ADMIN && !(oxyLoan.getLenderUserId().equals(user.getId())
				|| oxyLoan.getBorrowerUserId().equals(user.getId()))) {
			throw new OperationNotAllowedException("This loan does not belong to you.", ErrorCodes.PERMISSION_DENIED);
		}
		List<LoanEmiCardResponseDto> emiCards = new ArrayList<LoanEmiCardResponseDto>();
		SearchResultsDto<LoanEmiCardResponseDto> response = new SearchResultsDto<LoanEmiCardResponseDto>();
		List<LoanEmiCard> loanEmiCards = oxyLoan.getLoanEmiCards();

		if (CollectionUtils.isNotEmpty(loanEmiCards)) {
			loanEmiCards = loanEmiCardRepo.findByLoanIdOrderByEmiNumber(loanId);

			loanEmiCards.stream().forEach(card -> {
				LoanEmiCardResponseDto responseDto = new LoanEmiCardResponseDto();
				responseDto.setComments(card.getComments());
				responseDto.setEmiAmount((double) Math.round(card.getEmiAmount()));
				responseDto.setEmiDueOn(this.expectedDateFormat.format(card.getEmiDueOn()));
				responseDto.setEmiPaidOn(
						card.getEmiPaidOn() == null ? null : this.expectedDateFormat.format(card.getEmiPaidOn()));
				responseDto.setEmiInterstAmount((double) Math.round(card.getEmiInterstAmount()));
				responseDto.setEmiNumber(card.getEmiNumber());
				responseDto.setEmiPrincipalAmount((double) Math.round(card.getEmiPrincipalAmount()));
				responseDto.setId(card.getId());
				responseDto.setExcessOfEmiAmount((double) Math.round(card.getExcessOfEmiAmount()));
				responseDto.setModeOfPayment(card.getModeOfPayment());

				responseDto.setLoanPaymenHistory(getPaymentHistory(card.getId()));

				responseDto.setLoanId(card.getLoanId());
				responseDto.setEmiStatus(card.getEmiPaidOn() != null);
				responseDto.setPaidOnTime(card.isPaidOnTime());
				OxyLoan oxyloan = oxyLoanRepo.getLenderAndBorrowerAgreement(loanId);
				if (oxyloan != null) {
					LenderEmiDetails lenderEmiDetails = new LenderEmiDetails();
					lenderEmiDetails.setInterestDetails(card.getLenderEmiInterestAmount());
					lenderEmiDetails.setPrincipalDetails(card.getLenderEmiPrincipalAmount());
					lenderEmiDetails.setEmiDetails(card.getLenderEmiAmount());
					responseDto.setLenderEmiDetails(lenderEmiDetails);
				}
				double emiValue = (double) Math.round(card.getEmiAmount());
				double interestValue = (double) Math.round(card.getEmiLateFeeCharges());
				responseDto.setEmiAndInterest(emiValue + interestValue);
				if (card.getEmiPaidOn() != null) {
					responseDto.setEmiStatusBasedOnPayment("EMI Received");
				} else {
					Calendar cal = Calendar.getInstance();
					int month = cal.get(Calendar.MONTH);
					month = month + 1;
					int year = cal.get(Calendar.YEAR);
					int day = cal.get(Calendar.DAY_OF_MONTH);
					Date date = cal.getTime();
					String dt = expectedDateFormat.format(date);
					Date date1 = new Date();
					Date date2 = new Date();
					try {
						date1 = expectedDateFormat.parse(responseDto.getEmiDueOn());
						date2 = expectedDateFormat.parse(dt);
					} catch (Exception e) {
						logger.info("Exception", e);
					}
					String emiDueOn = responseDto.getEmiDueOn();
					if (emiDueOn != null) {
						String arr[] = emiDueOn.split("/");
						String dueMonth = arr[1];
						String dueYear = arr[2];
						if (month == Integer.parseInt(dueMonth) && year == Integer.parseInt(dueYear)) {
							if (day >= 0 && day <= 20) {
								responseDto.setEmiStatusBasedOnPayment("EMI Process Initiated");
							} else {
								responseDto.setEmiStatusBasedOnPayment("EMI Pending");
							}
						} else if (date1.compareTo(date2) < 0) {
							responseDto.setEmiStatusBasedOnPayment("EMI Pending");
						} else if (date1.compareTo(date2) > 0) {
							responseDto.setEmiStatusBasedOnPayment("Future EMI");
						}

					}

				}
				responseDto.setRemainingEmiAmount((double) Math.round(card.getRemainingEmiAmount()));
				responseDto.setRemainingPenaltyAndRemainingEmi(
						(double) Math.round(card.getRemainingPenaltyAndRemainingEmi()));
				responseDto.setPenalty((double) Math.round(card.getPenality()));

				emiCards.add(responseDto);
			});

		}
		response.setPageNo(1);
		response.setPageCount(emiCards.size());
		response.setResults(emiCards);
		response.setTotalCount(emiCards.size());
		return response;
	}

	public double getPrevioueEmiAndPenaltyAmt(int emiId) {
		LoanEmiCard loanEmiCard = loanEmiCardRepo.findById(emiId - 1).get();
		List<LoanEmiCardPaymentDetailsResponseDto> paymentHistory = getPaymentHistory(emiId - 1);
		double penality = 0.0;
		double remainingEmiAmount = 0.0;
		if (paymentHistory.size() > 0) {
			LoanEmiCardPaymentDetailsResponseDto loanEmiCardPaymentDetailsResponseDto = paymentHistory
					.get(paymentHistory.size() - 1);
			penality = Math.round(loanEmiCardPaymentDetailsResponseDto.getPenalty());
			remainingEmiAmount = Math.round(loanEmiCardPaymentDetailsResponseDto.getRemainingEmiAmount());

		}
		return loanEmiCard.getRemainingPenaltyAndRemainingEmi() + penality + remainingEmiAmount;
	}

	public double getPaymentDetails(int emiId) {
		double penality = 0.0;
		List<LoanEmiCardPaymentDetails> loanEmiCardPayment = loanEmiCardPaymentDetailsRepo.findByEmiId(emiId);
		if (loanEmiCardPayment.size() > 0) {
			LoanEmiCardPaymentDetails loanEmiCardPaydetails = loanEmiCardPayment.get(loanEmiCardPayment.size() - 1);
			penality = Math.round(loanEmiCardPaydetails.getPenalty());

		}

		return penality;

	}

	public HashMap<String, String> getPreviousExcessAmount(int loanId, int emiNumber) {
		HashMap<String, String> map = new HashMap<String, String>();
		double remainingEmiAmount = 0.0;
		double excessAmount = 0.0;
		if (emiNumber > 1) {
			LoanEmiCard previousEmiDetails = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId, emiNumber - 1);
			LoanEmiCard presentEmiDetails = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId, emiNumber);
			if (previousEmiDetails.getExcessOfEmiAmount() > 0 && presentEmiDetails.getRemainingEmiAmount() == 0) {
				if (previousEmiDetails.getExcessOfEmiAmount() > presentEmiDetails.getEmiAmount()) {
					// map.put("emiStatus","COMPLETED");
					excessAmount = (previousEmiDetails.getExcessOfEmiAmount() - presentEmiDetails.getEmiAmount());
					map.put("emiStatus", "COMPLETED");

					map.put("excessAmount", excessAmount + "");

				} else {
					remainingEmiAmount = (presentEmiDetails.getEmiAmount() - previousEmiDetails.getExcessOfEmiAmount());
					map.put("emiStatus", "INPROCESS");
					map.put("remainingEmiAmount", remainingEmiAmount + "");
				}

			}
		}
		return map;
	}

	public List<LoanEmiCardPaymentDetailsResponseDto> getPaymentHistory(int emiId) {
		List<LoanEmiCardPaymentDetails> loanEmiCardPaymentHistroy = loanEmiCardPaymentDetailsRepo.findByEmiId(emiId);

		List<LoanEmiCardPaymentDetailsResponseDto> listPaymentHistory = new ArrayList<LoanEmiCardPaymentDetailsResponseDto>();
		for (LoanEmiCardPaymentDetails paymentDetails : loanEmiCardPaymentHistroy) {

			LoanEmiCardPaymentDetailsResponseDto loanEmiCardPaymentDetailsResponseDto = new LoanEmiCardPaymentDetailsResponseDto();
			loanEmiCardPaymentDetailsResponseDto.setEmiId(paymentDetails.getEmiId());
			loanEmiCardPaymentDetailsResponseDto.setLoanId(paymentDetails.getLoanId());
			loanEmiCardPaymentDetailsResponseDto.setModeOfPayment(paymentDetails.getModeOfPayment());
			loanEmiCardPaymentDetailsResponseDto
					.setTransactionReferenceNumber(paymentDetails.getTransactionRefereceNumber());
			loanEmiCardPaymentDetailsResponseDto.setPaymentStatus(paymentDetails.getPaymentStatus().toString());
			loanEmiCardPaymentDetailsResponseDto.setPenalty(Math.round(paymentDetails.getPenalty()));
			loanEmiCardPaymentDetailsResponseDto.setRemainingEmiAmount(paymentDetails.getRemainingEmiAmount());
			if (loanEmiCardPaymentHistroy.size() > 0) {
				LoanEmiCardPaymentDetails loanEmiCardPaydetails = loanEmiCardPaymentHistroy
						.get(loanEmiCardPaymentHistroy.size() - 1);
				loanEmiCardPaymentDetailsResponseDto.setRemainingPenaltyAndRemainingEmi(
						loanEmiCardPaydetails.getPenalty() + loanEmiCardPaydetails.getRemainingEmiAmount());

			}
			if (paymentDetails.getAmountPaidDate() != null) {
				loanEmiCardPaymentDetailsResponseDto
						.setAmountPaidDate(expectedDateFormat.format(paymentDetails.getAmountPaidDate()));
			}
			if (paymentDetails.getPartpayDate() != null) {
				loanEmiCardPaymentDetailsResponseDto
						.setPartParymentDate(expectedDateFormat.format(paymentDetails.getPartpayDate()));
			}
			listPaymentHistory.add(loanEmiCardPaymentDetailsResponseDto);

		}

		return listPaymentHistory;

	}

	@Override
	public LoanEmiCardResponseDto emiPaid(int loanId, int emiNumber, LoanEmiCardResponseDto request) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanEmiCardResponseDto emiPaidByBorrowerUsingPaymentGateway(String oxyLoansTransactionNumber,
			LoanEmiCardResponseDto request) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanEmiCardResponseDto markEmiFailed(int loanId, int emiNumber) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	public void migrateOldSystemToNewSystem() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public void deleteLoans(int userId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public void changeDefaultLoanPrimaryType(int userId, PrimaryType newPrimaryType) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanEmiCardResponseDto loanPreClose(int loanId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public SearchResultsDto<LoanResponseDto> pendingEmis(int year, int month, String type, int pageNo, int pageSize) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public UserResponse updateUserStatus(int userId, LoanRequestDto loanRequestDto) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		User regestreduser = userRepo.findById(loanRequestDto.getId()).get();

		if (regestreduser.getPersonalDetails() == null || userDocumentStatusRepo
				.findByUserIdAndDocumentType(regestreduser.getId(), DocumentType.Kyc).isEmpty()) {
			throw new ActionNotAllowedException("User Details and Documents not given", ErrorCodes.PERMISSION_DENIED);
		} else {
			if (loanRequestDto.getStatus().equals(Status.ACTIVE.toString())) {
				regestreduser.setStatus(Status.ACTIVE);
				userRepo.save(regestreduser);
			}
			UserResponse userResponse = new UserResponse();
			userResponse.setId(regestreduser.getId());
			userResponse.setStatus(regestreduser.getStatus().toString());
			return userResponse;

		}
	}

	@Override
	public UserResponse sendReport(int userId, UserRequest userRequest) throws PdfGeenrationException, IOException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	@Transactional
	public LenderFavouriteResponseDto selectFavouriteBorrower(LenderFavouriteRequestDto lenderFavouriteRequestDto) {
		LenderFavouriteUsers lenderFavouriteUsers = lenderFavouriteUsersRepo.findByLenderUserIdAndBorrowerUserId(
				lenderFavouriteRequestDto.getLenderUserId(), lenderFavouriteRequestDto.getBorrowerUserId());

		if (lenderFavouriteUsers == null) {
			lenderFavouriteUsers = new LenderFavouriteUsers();
			lenderFavouriteUsers.setLenderUserId(lenderFavouriteRequestDto.getLenderUserId());
			lenderFavouriteUsers.setBorrowerUserId(lenderFavouriteRequestDto.getBorrowerUserId());
			lenderFavouriteUsers
					.setFavouriteType(FavouriteType.valueOf(lenderFavouriteRequestDto.getType().toUpperCase()));
			lenderFavouriteUsers.setBorrowerFavouriteDate(new Date());
			lenderFavouriteUsersRepo.save(lenderFavouriteUsers);
		} else {
			if (lenderFavouriteUsers != null) {

				lenderFavouriteUsers
						.setFavouriteType(FavouriteType.valueOf(lenderFavouriteRequestDto.getType().toUpperCase()));

				lenderFavouriteUsersRepo.save(lenderFavouriteUsers);
			}
		}

		LenderFavouriteResponseDto lenderFavouriteResponseDto = new LenderFavouriteResponseDto();
		lenderFavouriteResponseDto.setLenderUserId(lenderFavouriteUsers.getLenderUserId());
		lenderFavouriteResponseDto.setBorrowerUserId(lenderFavouriteUsers.getBorrowerUserId());
		lenderFavouriteResponseDto.setType(lenderFavouriteUsers.getFavouriteType().toString());

		return lenderFavouriteResponseDto;

	}

	@Override
	public UserResponse BorrowerFee(int userId, UserRequest userRequest) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	protected List<LenderFavouriteResponseDto> getFavouriteBorrowers(int userId) {

		List<LenderFavouriteUsers> favouriteUsers = lenderFavouriteUsersRepo.findByBorrowerUserId(userId);

		List<LenderFavouriteResponseDto> favouriteBorrowersList = new ArrayList<LenderFavouriteResponseDto>();

		if (favouriteUsers == null) {
			favouriteBorrowersList.add(null);
		}
		for (LenderFavouriteUsers lenderFavouriteUsers : favouriteUsers) {

			if (lenderFavouriteUsers.getFavouriteType().toString().equals(FavouriteType.FAVOURITE.toString())) {
				LenderFavouriteResponseDto favouriteResponseDto = new LenderFavouriteResponseDto();
				favouriteResponseDto.setLenderUserId(lenderFavouriteUsers.getLenderUserId());
				favouriteResponseDto.setBorrowerUserId(lenderFavouriteUsers.getBorrowerUserId());
				favouriteResponseDto.setType(lenderFavouriteUsers.getFavouriteType().toString());
				favouriteBorrowersList.add(favouriteResponseDto);
			}
		}

		return favouriteBorrowersList;

	}

	@Override
	public UserResponse calculateprofileRisk(int userId, UserRequest userRequest) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public UserResponse getExperianAndProfile(int userId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanResponseDto sendOffer(int userId, LoanRequestDto loanRequestDto) throws MessagingException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	private OfferSentDetails offeredDetails(int userId, String loanRequestedId) {
		OfferSentDetails offerSentDetails = new OfferSentDetails();
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndLoanRequestId(userId, loanRequestedId);
		LoanOfferdAmount loanOfferdAmount = loanRequestRepo.findByUserIdAndLoanRequestId(userId, loanRequestedId)
				.getLoanOfferedAmount();
		if (ObjectUtils.allNotNull(loanRequest, loanOfferdAmount)) {
			offerSentDetails.setComments(loanOfferdAmount.getComments());
			offerSentDetails.setDuaration(loanOfferdAmount.getDuration());
			offerSentDetails.setLoanOfferdStatus(loanOfferdAmount.getLoanOfferdStatus().toString());
			offerSentDetails.setId(userId);
			offerSentDetails.setOfferAccepeted(loanOfferdAmount.getAccepetedOn());
			offerSentDetails.setOfferSentOn(loanOfferdAmount.getOfferSentOn());
			offerSentDetails.setLoanOfferedAmount(loanOfferdAmount.getLoanOfferedAmount());
			offerSentDetails.setLoanRequestId(loanRequestedId);
			offerSentDetails.setBorrowerFee(loanOfferdAmount.getBorrowerFee());
			offerSentDetails.setEmiAmount(loanOfferdAmount.getEmiAmount());
			offerSentDetails.setNetDisbursementAmount(loanOfferdAmount.getNetDisbursementAmount());
			offerSentDetails.setRateOfInterest(loanOfferdAmount.getRateOfInterest());
			offerSentDetails.setPayUStatus(loanOfferdAmount.getPayuStatus());
			return offerSentDetails;
		}

		return null;
	}

	public List<StatementPdfResponseDto> getOxyLoanDetails(int id, String type) {
		User user = userRepo.findById(id).get();
		List<OxyLoan> oxyLoan = null;
		if (type.equalsIgnoreCase(User.PrimaryType.BORROWER.toString())) {
			oxyLoan = oxyLoanRepo.findByBorrowerUserId(id);
		} else {
			oxyLoan = oxyLoanRepo.findByLenderUserId(id);
		}
		List<StatementPdfResponseDto> listOfRecords = new ArrayList<StatementPdfResponseDto>();
		String loanEmi = "";
		String loanEmiTableInBorrowerDealStructure = "";
		String dateDisbursed = null;
		for (OxyLoan oxyloan : oxyLoan) {
			logger.info("loan info Method starts!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			StatementPdfResponseDto statementPdfResponseDto = new StatementPdfResponseDto();

			if (oxyloan.getLoanStatus() != null
					&& oxyloan.getLoanStatus().toString().equals(LoanStatus.Active.toString())
					|| oxyloan.getLoanStatus().toString().equals(LoanStatus.Closed.toString())) {

				int loanId = oxyloan.getId();

				double rateOfInterest = oxyloan.getRateOfInterest();
				int duration = oxyloan.getDuration();

				List<LoanEmiCard> loanEmiCard = loanEmiCardRepo.findByLoanIdOrderByEmiNumber(loanId);
				// double outstandingAmount=0.0;
				int totalEmis = loanEmiCard.size();
				if (totalEmis > 0) {
					int installmentPaidNumber = 0;
					int installmentPendingNumber = 0;
					int furtherInstallmentNumber = 0;
					double installmentPaidAmount = 0.0;
					double installmentPendingAmount = 0.0;
					double furtherInstallmentAmount = 0.0;
					double emi = 0;
					Date emiStartDate = null;
					Date emiEndDate = null;
					double interestAccrued = 0.0;
					double balancePrincipal = 0.0;
					double emiInterestAmount = 0.0;
					double emiPrincipalAmount = 0.0;
					int emiNumber = 0;
					String emiDueOn = null;
					String emiPaidon = null;
					String comments = "";
					String emiCardDetails = null;
					int emiCardLoanId = 0;
					int emiLoanId = 0;

					String loanEmiHeading = null;
					loanEmi = loanEmi + loanEmiCardWithHeadings(loanId + "");
					loanEmiTableInBorrowerDealStructure = loanEmiTableInBorrowerDealStructure
							+ borrowerDealStructureEmiTableHeadings(loanId + "");

					for (LoanEmiCard emiCard : loanEmiCard) {
						emiLoanId = emiCard.getLoanId();
						emiNumber = emiCard.getEmiNumber();
						emi = Math.round(emiCard.getEmiAmount());

						emiInterestAmount = Math.round(emiCard.getEmiInterstAmount());
						emiPrincipalAmount = Math.round(emiCard.getEmiPrincipalAmount());
						Date dateDue = emiCard.getEmiDueOn();
						emiDueOn = expectedDateFormat.format(dateDue);
						comments = (emiCard.getComments() == null ? " " : emiCard.getComments());

						if (emiCard.getEmiPaidOn() == null) {
							installmentPendingNumber = installmentPendingNumber + 1;
							installmentPendingAmount = Math.round(installmentPendingAmount + emi);
							balancePrincipal = Math.round(balancePrincipal + emiPrincipalAmount);
							emiPaidon = " ";

						}

						if (emiCard.getEmiPaidOn() != null && emiCard.getEmiPaidOn().toString().length() > 0) {

							installmentPaidNumber = installmentPaidNumber + 1;
							installmentPaidAmount = Math.round(installmentPaidAmount + emi);
							interestAccrued = Math.round(interestAccrued + emiInterestAmount);
							Date datePaid = emiCard.getEmiPaidOn();
							emiPaidon = expectedDateFormat.format(datePaid);

						}
						// outstandingAmount= outstandingAmount-emi;

						loanEmi = loanEmi + loanEmiCard(emiNumber + " ", emiPrincipalAmount + " ",
								emiInterestAmount + " ", emi + " ", emiDueOn + " ", emiPaidon + " ", comments + " ");
						loanEmiTableInBorrowerDealStructure = loanEmiTableInBorrowerDealStructure
								+ borrowerDealStructureEmiTableBody(emiDueOn + " ", emiPrincipalAmount + " ",
										emiInterestAmount + " ", emi + " ");

						if (emiCard.getEmiNumber() == 1) {
							emiStartDate = emiCard.getEmiDueOn();
						}

						if (emiCard.getEmiNumber() == duration) {
							emiEndDate = emiCard.getEmiDueOn();
						}

					}
					loanEmi = loanEmi + "</fo:table>" + "<fo:block font-size=\"20pt\" font-family=\"sans-serif\"\r\n"
							+ "				line-height=\"25pt\" space-after.optimum=\"3pt\" text-align=\"justify\">"
							+ "<br />" + "<br />" + "</fo:block>"
							+ "<fo:block font-size=\"20pt\" font-family=\"sans-serif\"\r\n"
							+ "				line-height=\"25pt\" space-after.optimum=\"3pt\" text-align=\"justify\">"
							+ "<br />" + "<br />" + "</fo:block>"
							+ "<fo:block font-size=\"20pt\" font-family=\"sans-serif\"\r\n"
							+ "				line-height=\"25pt\" space-after.optimum=\"3pt\" text-align=\"justify\">"
							+ "<br />" + "<br />" + "</fo:block>"
							+ "<fo:block font-size=\"20pt\" font-family=\"sans-serif\"\r\n"
							+ "				line-height=\"25pt\" space-after.optimum=\"3pt\" text-align=\"justify\">"
							+ "<br />" + "<br />" + "</fo:block>";
					statementPdfResponseDto.setEmiTable(loanEmi);

					loanEmiTableInBorrowerDealStructure = loanEmiTableInBorrowerDealStructure + "</fo:table>"
							+ "<fo:block>\r\n" + "				<br />\r\n" + "				<br />\r\n"
							+ "			</fo:block>" + "<fo:block>\r\n" + "				<br />\r\n"
							+ "				<br />\r\n" + "			</fo:block>" + "<fo:block>\r\n"
							+ "				<br />\r\n" + "				<br />\r\n" + "			</fo:block>";

					statementPdfResponseDto.setBorrowerDealStructureEmiTable(loanEmiTableInBorrowerDealStructure);

					furtherInstallmentNumber = furtherInstallmentNumber
							- (installmentPendingNumber + installmentPaidNumber);
					furtherInstallmentAmount = furtherInstallmentNumber * emi;

					double disburmentAmount = oxyloan.getDisbursmentAmount();

					if (oxyloan.getBorrowerDisbursedDate() == null) {

						String disburmentDate = " ";

						String loanEmiCardDetails = loanEmiCardPdf(loanId + "", disburmentDate + "",
								disburmentAmount + "", rateOfInterest + "", installmentPaidNumber + "",
								installmentPaidAmount + "", installmentPendingNumber + "",
								installmentPendingAmount + "");

						statementPdfResponseDto.setLoanEmiCardDetails(loanEmiCardDetails);

						String disburmentAmountDetails = disburmentAmount(loanId + "", disburmentDate + "",
								disburmentAmount + "");

						statementPdfResponseDto.setDisburmentAmountDetails(disburmentAmountDetails);
					}

					else {
						if (oxyloan.getBorrowerDisbursedDate() != null) {
							Date date = oxyloan.getBorrowerDisbursedDate();
							String disburmentDate = expectedDateFormat.format(date);

							String loanEmiCardDetails = loanEmiCardPdf(loanId + "", disburmentDate + "",
									disburmentAmount + "", rateOfInterest + "", installmentPaidNumber + "",
									installmentPaidAmount + "", installmentPendingNumber + "",
									installmentPendingAmount + "");

							statementPdfResponseDto.setLoanEmiCardDetails(loanEmiCardDetails);

							String disburmentAmountDetails = disburmentAmount(loanId + "", disburmentDate + "",
									disburmentAmount + "");

							statementPdfResponseDto.setDisburmentAmountDetails(disburmentAmountDetails);
						}
					}

					if (oxyloan.getBorrowerDisbursedDate() != null) {
						statementPdfResponseDto.setBorrowerDisburmentDate(
								expectedDateFormat.format(oxyloan.getBorrowerDisbursedDate()));
					}
					OxyLoan oxyloansDetails = oxyLoanRepo.findById(loanId).get();
					int borrowerUserId = oxyloansDetails.getBorrowerUserId();
					User userDetails = userRepo.findById(borrowerUserId).get();
					String borrowerMobileNumber = userDetails.getMobileNumber();
					String borrowerName = userDetails.getPersonalDetails().getFirstName() + " "
							+ userDetails.getPersonalDetails().getLastName();
					String emiStartAndEnd = emiStartAndEndDates(loanId + "", borrowerName + "",
							borrowerMobileNumber + "", duration + "", disburmentAmount + "", emiStartDate + "",
							emiEndDate + "");

					statementPdfResponseDto.setEmiStartDateAndEmiEndDate(emiStartAndEnd);

					listOfRecords.add(statementPdfResponseDto);

				}
			}

		}
		logger.info("loan info Method ends!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		return listOfRecords;

	}

	public String disburmentAmount(String loanId, String disburmentDate, String disburmentAmount) {

		String rowdata = "<fo:table-row>\r\n" + " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"2px\">\r\n" + " <fo:block>" + "LN" + loanId + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" + " <fo:block>"
				+ disburmentDate + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" // loop iteration first
																								// table in borrower
																								// statements to display
																								// disbursement details
				+ " <fo:block text-align=\"right\">" + disburmentAmount + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" + " <fo:block>"
				+ " Amt Disb Rs." + disburmentAmount + " vide RTGS dated " + disburmentDate + "</fo:block>\r\n"
				+ " </fo:table-cell>\r\n" +

				"    </fo:table-row>";

		return rowdata;
	}

	public String loanEmiCardPdf(String loanId, String disburmentDate, String disburmentAmount, String rateOfInterest,
			String installmentPaidNumber, String installmentPaidAmount, String installmentPendingNumber,
			String installmentPendingAmount) {
		String rowdata = "<fo:table-row>\r\n" + " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"2px\">\r\n" + " <fo:block>" + "LN" + loanId + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" + " <fo:block>"
				+ disburmentDate + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">" + disburmentAmount + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">" + rateOfInterest + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">" + installmentPaidNumber + " " + "/" + " " + installmentPaidAmount
				+ "</fo:block>\r\n" + " </fo:table-cell>\r\n" + " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"2px\">\r\n" + " <fo:block text-align=\"right\">" + installmentPendingNumber + " " + "/"
				+ " " + installmentPendingAmount + "</fo:block>\r\n" + " </fo:table-cell>\r\n" +

				"    </fo:table-row>";
		return rowdata;
	}

	public String emiStartAndEndDates(String loanId, String borrowerName, String borrowerMobileNumber, String duration,
			String disburmentAmount, String emiStartDate, String emiEndDate) {
		String rowdata = "<fo:table-row>\r\n" + " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"2px\">\r\n" + " <fo:block>" + "LN" + loanId + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" + " <fo:block>"
				+ borrowerName + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" + " <fo:block>"
				+ borrowerMobileNumber + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" + " <fo:block>" + duration
				+ "</fo:block>\r\n" + " </fo:table-cell>\r\n" + " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"2px\">\r\n" + " <fo:block>"

				+ disburmentAmount + "</fo:block>\r\n" + " </fo:table-cell>\r\n" // loop iteration third table in
																					// borrower statements to display
																					// emiStart and End Date details
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">"

				+ emiStartDate + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">"

				+ emiEndDate + "</fo:block>\r\n" + " </fo:table-cell>\r\n" +

				"    </fo:table-row>";

		return rowdata;
	}

	public String loanEmiCard(String emiNumber, String emiPrincipalAmount, String emiInterestAmount, String emi,
			String emiDueOn, String emiPaidon, String comments) {

		String rowdata = "<fo:table-body>\r\n " + "<fo:table-row>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" + " <fo:block>" + emiNumber
				+ "</fo:block>\r\n" + " </fo:table-cell>\r\n" + " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"2px\">\r\n" + " <fo:block>" + emiPrincipalAmount + "</fo:block>\r\n"
				+ " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">"

				+ emiInterestAmount + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">"

				+ emi + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">"

				+ emiDueOn + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">"

				+ emiPaidon + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">"

				+ comments + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " </fo:table-row>" + "</fo:table-body>";
		return rowdata;
	}

	public String loanEmiCardWithHeadings(String loanId) {
		String rowdata = "<fo:table>\r\n" + "				<fo:table-body>\r\n"
				+ "					<fo:table-row background-color=\"#00BFFF\">\r\n"
				+ "						<fo:table-cell padding-left=\"3pt\" padding-top=\"4pt\">\r\n"
				+ "							<fo:block font-size=\"12pt\" font-family=\"sans-serif\"\r\n"
				+ "								line-height=\"18pt\" space-after.optimum=\"3pt\" text-align=\"center\">\r\n"
				+ "Loan Application Number : " + "LN" + loanId + "							</fo:block>\r\n"
				+ "						</fo:table-cell>\r\n" + "					</fo:table-row>\r\n"
				+ "				</fo:table-body>\r\n" + "			</fo:table>" + "<fo:block >\r\n"
				+ "				<br />\r\n" + "				<br />\r\n" + "			</fo:block>\r\n" +

				"<fo:table>\r\n" + "<fo:table-header>\r\n" + "<fo:table-row>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding= \"4px 0px\" background-color=\"#ddd\" > + <fo:block text-align=\"center\">"
				+ " Emi Number " + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding= \"4px 0px\" background-color=\"#ddd\" > + <fo:block text-align=\"center\">"
				+ "Emi Principal Amount" + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"4px 0px\" background-color=\"#ddd\" > + <fo:block text-align=\"center\">"

				+ "Emi Interest Amount" + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"4px 0px\" background-color=\"#ddd\" > + <fo:block text-align=\"center\">"

				+ "Emi Amount" + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"4px 0px\" background-color=\"#ddd\" > + <fo:block text-align=\"center\">"

				+ "Emi Due Date" + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"4px 0px\" background-color=\"#ddd\" > + <fo:block text-align=\"center\">"

				+ "Emi Paid Date" + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"4px 0px\" background-color=\"#ddd\" > + <fo:block text-align=\"center\">"

				+ "Comments" + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " </fo:table-row>" + "</fo:table-header>";

		return rowdata;
	}

	@Override
	public void loanApplicationScheduler() {

		SearchRequestDto leftLeftOperand = new SearchRequestDto();
		leftLeftOperand.setFieldName("userPrimaryType");
		leftLeftOperand.setFieldValue("BORROWER");
		leftLeftOperand.setOperator(Operator.EQUALS);
		SearchRequestDto leftRightOperand = new SearchRequestDto();
		leftRightOperand.setFieldName("user.status");
		leftRightOperand.setFieldValue("ACTIVE");
		leftRightOperand.setOperator(Operator.EQUALS);

		SearchRequestDto rightLeftOperand = new SearchRequestDto();
		rightLeftOperand.setFieldName("parentRequestId");
		rightLeftOperand.setOperator(Operator.NULL);

		SearchRequestDto rightRightOperand = new SearchRequestDto();
		rightRightOperand.setFieldName("loanOfferedAmount.loanOfferdStatus");
		rightRightOperand.setFieldValue("LOANOFFERACCEPTED");
		rightRightOperand.setOperator(Operator.EQUALS);

		SearchRequestDto parentLeftOperand = new SearchRequestDto();
		parentLeftOperand.setLeftOperand(leftLeftOperand);
		parentLeftOperand.setLogicalOperator(LogicalOperator.AND);
		parentLeftOperand.setRightOperand(leftRightOperand);

		SearchRequestDto parentRightOperand = new SearchRequestDto();
		parentRightOperand.setLeftOperand(rightLeftOperand);
		parentRightOperand.setLogicalOperator(LogicalOperator.AND);
		parentRightOperand.setRightOperand(rightRightOperand);

		SearchRequestDto resultsRequest = new SearchRequestDto();
		resultsRequest.setLeftOperand(parentLeftOperand);
		resultsRequest.setLogicalOperator(LogicalOperator.AND);
		resultsRequest.setRightOperand(parentRightOperand);
		SearchResultsDto<LoanResponseDto> results = getLoanRequestInformation(resultsRequest);

		List<LoanResponseDto> listLoanRequest = results.getResults();

		for (LoanResponseDto loanResponseDto : listLoanRequest) {
			Integer userId = loanResponseDto.getUser().getId();
			String loanRequestId = loanResponseDto.getLoanRequest();
			LoanRequest loanRequest = loanRequestRepo.findByUserIdAndLoanRequestId(userId, loanRequestId);
			loanRequest.setParentRequestId(0);
			LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();
			Date date = loanOfferdAmount.getAccepetedOn();
			LocalDate localDate = LocalDate.now();
			String systemDate = localDate.toString();
			try {
				Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(systemDate);
				long diff = date1.getTime() - date.getTime();

				long daysDiff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

				if (daysDiff == 7) {
					OxyLoan oxyLoan = oxyLoanRepo.findByBorrowerUserIdAndBorrowerParentRequestId(userId,
							loanRequest.getId());
					loanOfferdAmount.setLoanOfferdStatus(LoanOfferdStatus.LOANOFFEREXPIRED);
					loanOfferdAmount.setLoanRequest(loanRequest);
					User user = userRepo.findById(userId).get();
					user.setAdminComments(null);
					loanRequest.setLoanOfferedAmount(loanOfferdAmount);

					if (!oxyLoan.getAdminComments().equalsIgnoreCase(Status.DISBURSED.toString())) {
						userRepo.save(user);
						loanRequestRepo.save(loanRequest);
					}

					user = userRepo.findById(userId).get();
					user.setAdminComments(null);
					userRepo.save(user);
					loanRequestRepo.save(loanRequest);

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	protected SearchResultsDto<LoanResponseDto> getLoanRequestInformation(SearchRequestDto results) {
		Sort sort = null;
		if (results.getSortBy() != null) {
			sort = Sort.by(Direction.valueOf(results.getSortOrder().name()), results.getSortBy());
		} else {
			sort = Sort.by(Direction.DESC, "loanRequestedDate");
		}
		Pageable searchPage = PageRequest.of(results.getPage().getPageNo() - 1, results.getPage().getPageSize(), sort);
		Specification<LoanRequest> spec = null;
		try {
			spec = specificationProvider.construct(LoanRequest.class, results);
		} catch (ClassNotFoundException e) {
			logger.error(e, e);
		} catch (IllegalAccessException e) {
			logger.error(e, e);
		} catch (InvocationTargetException e) {
			logger.error(e, e);
		} catch (NoSuchMethodException e) {
			logger.error(e, e);
		} catch (InstantiationException e) {
			logger.error(e, e);
		}
		Page<LoanRequest> findAll = loanRequestRepo.findAll(spec, searchPage);
		SearchResultsDto<LoanResponseDto> resultsOfLoanRequest = new SearchResultsDto<LoanResponseDto>();
		if (findAll != null) {
			resultsOfLoanRequest.setTotalCount(Long.valueOf(findAll.getTotalElements()).intValue());
			resultsOfLoanRequest.setPageNo(results.getPage().getPageNo());
			resultsOfLoanRequest.setPageCount(findAll.getNumberOfElements());
			List<LoanResponseDto> loanRequests = new ArrayList<LoanResponseDto>();
			;
			resultsOfLoanRequest.setResults(loanRequests);
			for (LoanRequest request : findAll.getContent()) {
				loanRequests.add(populateLoanRequestDetails(request));
			}
		}
		return resultsOfLoanRequest;
	}

	@Override
	public LoanResponseDto newLoanRequest(int userId, LoanRequestDto loanRequestDto) {

		LoanRequest oldLoanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
		if (oldLoanRequest != null) {
			LoanOfferdAmount loanOfferdAmount = oldLoanRequest.getLoanOfferedAmount();
			if (loanOfferdAmount != null) {
				if (loanOfferdAmount.getLoanOfferdStatus() != LoanOfferdStatus.LOANOFFERREJECTED) {

					if (Double.compare((oldLoanRequest.getDisbursmentAmount()),
							loanOfferdAmount.getLoanOfferedAmount()) != 0) {

						if (Double.compare((oldLoanRequest.getDisbursmentAmount()),
								loanOfferdAmount.getLoanOfferedAmount()) != 0) {

							throw new OperationNotAllowedException("your offered amount is not full filled",
									ErrorCodes.INVALID_OPERATION);

						} else {
							User user = userRepo.findById(userId).get();
							user.setAdminComments(null);
							oldLoanRequest.setParentRequestId(0);
							userRepo.save(user);
							loanRequestRepo.save(oldLoanRequest);

						}
					}
				}

			}
		}
		Double totalActiveAmount = oxyLoanRepo.getTotalActiveAmount(userId);
		if (totalActiveAmount != null && totalActiveAmount >= 1000000) {
			throw new OperationNotAllowedException("Limit is exceeding please check your active loans amount",
					ErrorCodes.LIMIT_REACHED);
		}
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		isValidLoanRequest(loanRequestDto, user);
		LoanRequest loanRequest = new LoanRequest();
		loanRequest.setLoanRequestAmount(loanRequestDto.getLoanRequestAmount());
		loanRequest.setDuration(loanRequestDto.getDuration());
		loanRequest.setLoanPurpose(loanRequestDto.getLoanPurpose());
		loanRequest.setRateOfInterest(loanRequestDto.getRateOfInterest());
		loanRequest.setUserPrimaryType(user.getPrimaryType());
		loanRequest.setRepaymentMethod(RepaymentMethod.valueOf(loanRequestDto.getRepaymentMethod()));
		loanRequest.setDurationType(
				loanRequestDto.getDurationType().equalsIgnoreCase("months") ? DurationType.Months : DurationType.Days);
		try {
			loanRequest.setExpectedDate(expectedDateFormat.parse(loanRequestDto.getExpectedDate()));
		} catch (ParseException e) {
			logger.error("Error: ", e);
			throw new DataFormatException("Illegal Date format. It should be dd/MM/yyyy",
					ErrorCodes.INVALID_DATE_FORMAT);
		}
		loanRequest.setUserId(userId);
		loanRequest.setLoanRequestId(new Long(System.currentTimeMillis()).toString()); // This is temporary
		loanRequest = loanRequestRepo.save(loanRequest);
		loanRequest.setLoanRequestId(LOAN_REQUEST_IDFORMAT.replace("{type}", user.getPrimaryType().getShortCode())
				.replace("{ID}", loanRequest.getId().toString()));
		loanRequest = loanRequestRepo.save(loanRequest);
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setUserId(userId);
		loanResponseDto.setLoanRequestId(loanRequest.getId());
		loanResponseDto.setLoanRequest(loanRequest.getLoanRequestId());

		return loanResponseDto;

	}

	@Override
	@Transactional
	public LoanResponseDto borrowerDealStructure(String loanId) throws PdfGeenrationException, IOException {
		OxyLoan oxyLoan = oxyLoanRepo.findByLoanId(loanId);
		int id = oxyLoan.getBorrowerUserId();

		User user = userRepo.findById(id).get();
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(id);
		int loanRequestId = loanRequest.getId();

		LoanRequest loanRequestForOfferedAmount = loanRequestRepo.findById(loanRequestId).get();
		if (loanRequestForOfferedAmount.getLoanOfferedAmount() == null) {
			throw new OperationNotAllowedException("user details not present", ErrorCodes.USER_NOT_FOUND);
		}

		double adminOfferedAmount = loanRequestForOfferedAmount.getLoanOfferedAmount().getLoanOfferedAmount();

		int duration = loanRequestForOfferedAmount.getLoanOfferedAmount().getDuration();
		double rateOfInterest = loanRequestForOfferedAmount.getLoanOfferedAmount().getRateOfInterest();

		double emiAmount = 0d;
		double interestAmount = 0d;
		double principalAmount = 0d;
		if (oxyLoan.getDurationType().equals(DurationType.Months)) {

			principalAmount = Math.round(((adminOfferedAmount) * (rateOfInterest / 12)) / 100d);
			interestAmount = Math.round(adminOfferedAmount / duration);
			emiAmount = principalAmount + interestAmount;
		} else {
			principalAmount = oxyLoan.getDisbursmentAmount();

			double calculatedRateOfinterest = (principalAmount * rateOfInterest) / 100000d;

			interestAmount = (double) Math.round(calculatedRateOfinterest * oxyLoan.getDuration());

			emiAmount = principalAmount + interestAmount;
		}

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(this.defaultEmiDay));
		calendar.add(Calendar.MONTH, 1);
		Date date = calendar.getTime();
		String dateExpected = expectedDateFormat.format(date);
		TemplateContext templateContext = new TemplateContext();
		templateContext.put("borrowerName",
				user.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase()
						+ user.getPersonalDetails().getFirstName().substring(1) + " "
						+ user.getPersonalDetails().getLastName());
		templateContext.put("borrowerApplicationId", user.getUniqueNumber());
		String borrowerAddress = user.getPersonalDetails().getAddress();
		Pattern pt = Pattern.compile("[^a-zA-Z0-9]");
		Matcher match = pt.matcher(borrowerAddress);
		while (match.find()) {
			String s = match.group();
			borrowerAddress = borrowerAddress.replaceAll("\\" + s, " ");
		}
		templateContext.put("borrowerPresentAddress", borrowerAddress);
		templateContext.put("city", user.getCity());
		templateContext.put("state", user.getState());
		templateContext.put("pinCode", user.getPinCode());
		templateContext.put("accountNumber", user.getBankDetails().getAccountNumber());
		templateContext.put("adminOfferedAmount", adminOfferedAmount);
		templateContext.put("duration", duration);
		templateContext.put("durationType", loanRequest.getDurationType());
		templateContext.put("rateOfInterest", rateOfInterest);
		templateContext.put("emiAmount", emiAmount);
		if (oxyLoan.getDurationType().equals(DurationType.Months)) {
			templateContext.put("emiStartDate", dateExpected);
		} else {
			LoanEmiCard emiCard = loanEmiCardRepo.findByLoanIdAndEmiNumber(oxyLoan.getId(), 1);
			templateContext.put("emiStartDate", emiCard.getEmiDueOn());
		}
		templateContext.put("ApplicationNumber", loanRequest.getLoanRequestId());
		templateContext.put("borrowerFee", loanRequest.getLoanOfferedAmount().getBorrowerFee());
		templateContext.put("netDisburmentAmount", loanRequest.getLoanOfferedAmount().getNetDisbursementAmount());
		// templateContext.put("googleReview",googleReview);
		List<StatementPdfResponseDto> statementPdfResponseDto = getOxyLoanDetails(id, "BORROWER");
		if (statementPdfResponseDto.isEmpty()) {
			throw new OperationNotAllowedException("user details not present", ErrorCodes.USER_NOT_FOUND);
		}

		String borrowerDealStructureEmiTable = "";
		String borrowerDisburmentDate = "";

		for (StatementPdfResponseDto responseDto : statementPdfResponseDto) {

			borrowerDealStructureEmiTable = responseDto.getBorrowerDealStructureEmiTable();
			borrowerDisburmentDate = responseDto.getBorrowerDisburmentDate();
			templateContext.put("borrowerDisburmentDate", borrowerDisburmentDate);
		}
		templateContext.put("borrowerDealStructureEmiTable", borrowerDealStructureEmiTable);

		String outputFileName = loanId + "borrowerDealStructure" + ".pdf";
		// String outputFileName = "C:/data/borrowerDealStructure6.pdf";

		String borrwerDealStructure = pdfEngine.generatePdf("agreement/borrowerDealStructure.xml", templateContext,
				outputFileName);

		FileRequest fileRequest = new FileRequest();
		fileRequest.setInputStream(new FileInputStream(borrwerDealStructure));
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.dealStructure);
		fileRequest.setFilePrifix(FileType.dealStructure.name());

		FileResponse file1 = fileManagementService.getFile(fileRequest);
		try {
			FileResponse putFile = fileManagementService.putFile(fileRequest);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		File file = new File(borrwerDealStructure);
		boolean fileDeleted = file.delete();
		logger.debug("Deleted temp file {} {}", borrwerDealStructure, fileDeleted);

		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setUserId(user.getId());
		loanResponseDto.setDownloadUrl(file1.getDownloadUrl());

		return loanResponseDto;

	}

	public String borrowerDealStructureEmiTableHeadings(String loanId) {
		String rowdata = "<fo:table>\r\n" + "				<fo:table-body>\r\n"
				+ "					<fo:table-row background-color=\"#00BFFF\">\r\n"
				+ "						<fo:table-cell padding-left=\"3pt\" padding-top=\"4pt\">\r\n"
				+ "							<fo:block font-size=\"12pt\" font-family=\"sans-serif\"\r\n"
				+ "								line-height=\"18pt\" space-after.optimum=\"3pt\" text-align=\"center\">\r\n"
				+ "Loan Application Number : " + "LN" + loanId + "							</fo:block>\r\n"
				+ "						</fo:table-cell>\r\n" + "					</fo:table-row>\r\n"
				+ "				</fo:table-body>\r\n" + "			</fo:table>" + "<fo:block >\r\n"
				+ "				<br />\r\n" + "				<br />\r\n" + "			</fo:block>\r\n" +

				"<fo:table>\r\n" + "<fo:table-header>\r\n" + "<fo:table-row>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding= \"4px 0px\" background-color=\"#ddd\" > + <fo:block text-align=\"center\">" + " EMIDate"
				+ "</fo:block>\r\n" + " </fo:table-cell>\r\n" + " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding= \"4px 0px\" background-color=\"#ddd\" > + <fo:block text-align=\"center\">"
				+ "Principal Amount" + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"4px 0px\" background-color=\"#ddd\" > + <fo:block text-align=\"center\">"

				+ "Interest Amount" + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ " padding=\"4px 0px\" background-color=\"#ddd\" > + <fo:block text-align=\"center\">"

				+ "EMIAmount" + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " </fo:table-row>" + "</fo:table-header>";
		return rowdata;
	}

	public String borrowerDealStructureEmiTableBody(String emiDueOn, String emiPrincipalAmount,
			String emiInterestAmount, String emi) {

		String rowdata = "<fo:table-body>\r\n " + "<fo:table-row>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" + " <fo:block>" + emiDueOn
				+ "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" + " <fo:block>"
				+ emiPrincipalAmount + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" + " <fo:block>"
				+ emiInterestAmount + "</fo:block>\r\n" + " </fo:table-cell>\r\n" // loop iteration for loan Emi card
																					// body

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">"

				+ emi + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " </fo:table-row>" + "</fo:table-body>";
		return rowdata;
	}

	@Override
	public OfferSentDetails getOfferSendDetails(int id, int loanRequestId) {

		User user = userRepo.findById(id).get();
		authorizationService.hasPermission(user);

		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			return offeredDetails(id, "APBR" + loanRequestId);
		} else {
			return offeredDetails(id, "APLR" + loanRequestId);

		}

	}

	@Override
	@Transactional
	public LoanResponseDto lenderDealStructure(String loanId) throws PdfGeenrationException, IOException {
		OxyLoan oxyLoan = oxyLoanRepo.findByLoanId(loanId);
		if (oxyLoan == null) {
			throw new OperationNotAllowedException("user details not present", ErrorCodes.USER_NOT_FOUND);
		}
		int id = oxyLoan.getId();
		User lenderDetails = userRepo.findById(oxyLoan.getLenderUserId()).get();
		User borrowerDetails = userRepo.findById(oxyLoan.getBorrowerUserId()).get();

		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(oxyLoan.getLenderUserId());

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(this.defaultEmiDay));
		calendar.add(Calendar.MONTH, 1);
		Date date = calendar.getTime();
		String dateExpected = expectedDateFormat.format(date);
		String disbursedDate = expectedDateFormat.format(oxyLoan.getBorrowerDisbursedDate());

		TemplateContext templateContext = new TemplateContext();
		templateContext.put("lenderName",
				lenderDetails.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase()
						+ lenderDetails.getPersonalDetails().getFirstName().substring(1) + " "
						+ lenderDetails.getPersonalDetails().getLastName());
		templateContext.put("lenderApplicationNumber", lenderDetails.getUniqueNumber());
		String lenderAddress = lenderDetails.getPersonalDetails().getAddress();
		Pattern pt = Pattern.compile("[^a-zA-Z0-9]");
		Matcher match = pt.matcher(lenderAddress);
		while (match.find()) {
			String s = match.group();
			lenderAddress = lenderAddress.replaceAll("\\" + s, " ");
		}

		templateContext.put("lenderPresentAddress", lenderAddress);
		templateContext.put("city", lenderDetails.getCity());
		templateContext.put("state", lenderDetails.getState());
		templateContext.put("pinCode", lenderDetails.getPinCode());
		templateContext.put("borrowerName",
				borrowerDetails.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase()
						+ borrowerDetails.getPersonalDetails().getFirstName().substring(1) + " "
						+ borrowerDetails.getPersonalDetails().getLastName());
		templateContext.put("borrowerApplicationNumber", borrowerDetails.getUniqueNumber());
		templateContext.put("loanApplicationNumber", oxyLoan.getLoanId());
		templateContext.put("loanAmount", oxyLoan.getDisbursmentAmount());
		templateContext.put("disburmentDate", disbursedDate);
		templateContext.put("applicationNumber", loanRequest.getLoanRequestId());
		templateContext.put("emiStartDate", dateExpected);

		String lenderEmiTable = "";
		List<StatementPdfResponseDto> statementPdfResponseDto = getLoanEmiCard(id);

		for (StatementPdfResponseDto responses : statementPdfResponseDto) {
			lenderEmiTable = lenderEmiTable + responses.getLenderDealStructureEmiTable();

		}

		templateContext.put("lenderEmiTable", lenderEmiTable);

		// String outputFileName = "C:/data/lenderDealStructure8.pdf";
		String outputFileName = loanId + "lenderDealStructure" + ".pdf";

		String lenderDealStructure = pdfEngine.generatePdf("agreement/lenderDealStructure.xml", templateContext,
				outputFileName);

		FileRequest fileRequest = new FileRequest();
		fileRequest.setInputStream(new FileInputStream(lenderDealStructure));
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.dealStructure);
		fileRequest.setFilePrifix(FileType.dealStructure.name());
		FileResponse file1 = fileManagementService.getFile(fileRequest);
		try {
			FileResponse putFile = fileManagementService.putFile(fileRequest);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		File file = new File(lenderDealStructure);
		boolean fileDeleted = file.delete();
		logger.debug("Deleted temp file {} {}", lenderDealStructure, fileDeleted);

		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setDownloadUrl(file1.getDownloadUrl());

		LoanResponseDto response = new LoanResponseDto();
		response.setUserId(lenderDetails.getId());
		response.setDownloadUrl(file1.getDownloadUrl());
		return response;

	}

	public List<StatementPdfResponseDto> getLoanEmiCard(int id) {
		List<LoanEmiCard> loanEmiCard = loanEmiCardRepo.findByLoanId(id);
		List<StatementPdfResponseDto> listOfRecords = new ArrayList<StatementPdfResponseDto>();

		for (LoanEmiCard loanEmi : loanEmiCard) {
			StatementPdfResponseDto statementPdfResponseDto = new StatementPdfResponseDto();
			double principalAmount = Math.round(loanEmi.getEmiPrincipalAmount());
			double interestAmount = Math.round(loanEmi.getEmiInterstAmount());
			double emiAmount = Math.round(loanEmi.getEmiAmount());
			String emiDueOnDate = expectedDateFormat.format(loanEmi.getEmiDueOn());
			String emiCardDetailsForLender = lenderDealStructureEmiTableBody(emiDueOnDate + " ", principalAmount + " ",
					interestAmount + " ", emiAmount + "");
			statementPdfResponseDto.setLenderDealStructureEmiTable(emiCardDetailsForLender);
			listOfRecords.add(statementPdfResponseDto);
		}

		return listOfRecords;

	}

	public String lenderDealStructureEmiTableBody(String emiDueOn, String emiPrincipalAmount, String emiInterestAmount,
			String emi) {

		String rowdata = "<fo:table-body>\r\n " + "<fo:table-row>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" + " <fo:block>" + emiDueOn
				+ "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" + " <fo:block>"
				+ emiPrincipalAmount + "</fo:block>\r\n" + " </fo:table-cell>\r\n"
				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n" + " <fo:block>"
				+ emiInterestAmount + "</fo:block>\r\n" + " </fo:table-cell>\r\n" // loop iteration for loan Emi card
																					// body

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">"

				+ emi + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " </fo:table-row>" + "</fo:table-body>";
		return rowdata;
	}

	@Override
	@Transactional
	public UserResponse addingCreditScoreByPaisaBazaar(int userId, ExperianRequestDto experianRequestDto) {
		User user = userRepo.findById(userId).get();
		ExperianSummary experianSummary = user.getExperianSummary();
		if (user.getPrimaryType().toString().equals(PrimaryType.BORROWER.toString())) {
			if (user.getAdminComments().toString().equals(Status.INTERESTED.toString())) {
				if (experianSummary == null) {
					experianSummary = new ExperianSummary();
					experianSummary.setScore(experianRequestDto.getCreditScoreByPaisabazaar());
					experianSummary.setCreditAccountTotal(0);
					experianSummary.setCreditAccountActive(0);
					experianSummary.setCreditAccountClosed(0);
					experianSummary.setOutstandingBalanceAll(0);
					experianSummary.setOutstandingBalanceSecured(0);
					experianSummary.setOutstandingBalanceUnSecured(0);
					experianSummary.setExperianFilePath(null);
					experianSummary.setFileName(null);
					user.setExperianSummary(experianSummary);
					experianSummary.setUser(user);
					userRepo.save(user);

				} else {
					if (experianRequestDto.getCreditScoreByPaisabazaar() != null) {
						experianSummary.setCreditScoreByPaisabazaar(experianRequestDto.getCreditScoreByPaisabazaar());
					}
				}

			}
		}
		UserResponse userReponse = new UserResponse();
		userReponse.setCreditScoreByPaisabazaar(experianSummary.getCreditScoreByPaisabazaar());
		return userReponse;

	}

	private Double getinProcessAmount(int userId, int BorrowerParentRequestId) {
		Double userInprocessAmount = lenderOxyWalletNativeRepo.findsumOfInProecss(userId, BorrowerParentRequestId);

		Double Amount = userInprocessAmount == null ? 0.0 : userInprocessAmount;
		return Amount;

	}

	private Double getClosedAmount(int userId, int BorrowerParentRequestId) {
		Double userInprocessAmount = lenderOxyWalletNativeRepo.findsumOfClosedAmount(userId, BorrowerParentRequestId);
		Double Amount = userInprocessAmount == null ? 0.0 : userInprocessAmount;
		return Amount;

	}

	private Double getDisbursedAmount(int userId, int BorrowerParentRequestId) {
		Double userInprocessAmount = lenderOxyWalletNativeRepo.findsumOfDisbursedAmount(userId,
				BorrowerParentRequestId);
		Double Amount = userInprocessAmount == null ? 0.0 : userInprocessAmount;
		return Amount;

	}

	private RiskProfileDto getReskProfileCalculation(Integer userId) {

		RiskProfileDto riskProfileDto = new RiskProfileDto();
		User user = userRepo.findById(userId).get();
		UserProfileRisk userProfileRisk = user.getUserProfileRisk();
		if (ObjectUtils.allNotNull(user, userProfileRisk)) {
			riskProfileDto.setUserId(user.getId());
			riskProfileDto.setCibilScore(userProfileRisk.getCibilScore());
			riskProfileDto.setCompanyOrOrganization(userProfileRisk.getCompanyOrOrganization());
			riskProfileDto
					.setExperianceOrExistenceOfOrganization(userProfileRisk.getExperianceOrExistenceOfOrganization());
			riskProfileDto.setGrade(userProfileRisk.getGrade());
			riskProfileDto.setSalaryOrIncome(userProfileRisk.getSalaryOrIncome());
			return riskProfileDto;
		}
		return null;

	}

	private String enachMandateResponce(String loanId) {
		String enachMandateResponseStatus = null;
		StringBuilder converting = new StringBuilder(loanId);
		String convertedLoanId = converting.delete(0, 2).toString();
		EnachMandate enachMandate = enachMandateRepo.findByoxyLoanId(Integer.parseInt(convertedLoanId));
		if (enachMandate != null) {
			EnachMandateResponse enachMandateResponse = emandateResponseRepo.findByEnachMandateId(enachMandate.getId());
			if (ObjectUtils.allNotNull(enachMandate, enachMandateResponse)) {
				enachMandateResponseStatus = enachMandateResponse.getTxnMsg();
				return enachMandateResponseStatus;
			} else {
				return enachMandateResponseStatus;
			}

		} else {
			return enachMandateResponseStatus;
		}
	}

	public UserResponse getAllUtmFieldValues() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public PayuMoneyPaymentDetaisResponce borrowerFeeUpdation(String oxyLoansTransactionNumber,
			LoanEmiCardResponseDto request) {
		LoanRequest loanRequest = loanRequestRepo.findBytxnNumber(oxyLoansTransactionNumber);
		LoanOfferdAmount loanOfferdAmount = loanRequest.getLoanOfferedAmount();
		loanOfferdAmount.setPayuTransactionNumber(request.getPayuTransactionNumber());
		loanOfferdAmount.setPayuStatus(request.getPayuStatus());
		loanOfferdAmount.setLoanRequest(loanRequest);
		loanRequest.setLoanOfferedAmount(loanOfferdAmount);
		loanRequestRepo.save(loanRequest);
		PayuMoneyPaymentDetaisResponce paymentDetaisResponce = new PayuMoneyPaymentDetaisResponce();
		paymentDetaisResponce.setAmount(Double.toString(loanOfferdAmount.getBorrowerFee()));
		paymentDetaisResponce.setTxnid(oxyLoansTransactionNumber);
		return paymentDetaisResponce;
	}

	@Override
	public LoanEmiCardPaymentDetailsResponseDto updatePaymentDetails(int emiId,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public List<LenderEmiDetailsResponseDto> getLenderEmiDetails(int userId) {
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);

		// List<OxyLoan> oxyLoan =
		// oxyLoanRepo.findByLenderParentRequestId(loanRequest.getId());
		List<OxyLoan> oxyLoan = oxyLoanRepo.findByLenderUserId(userId);
		List<LenderEmiDetailsResponseDto> listOfLenderEmiDetailsResponseDto = new ArrayList<LenderEmiDetailsResponseDto>();

		for (OxyLoan loan : oxyLoan) {

			int borrowerId = loan.getBorrowerUserId();
			User user = userRepo.findById(borrowerId).get();

			int loanId = loan.getId();
			List<LoanEmiCard> loanEmiCard = loanEmiCardRepo.findByLoanId(loanId);
			int length = loanEmiCard.size();
			int count = 0;
			double emiRecived = 0.0;
			double profit = 0.0;
			int numberOfEmisPaid = 0;
			double remainingEmiAmount = 0.0;
			LenderEmiDetailsResponseDto lenderEmiDetailsResponseDto = new LenderEmiDetailsResponseDto();

			for (LoanEmiCard emiCard : loanEmiCard) {

				count = count + 1;

				if (emiCard.getEmiPaidOn() != null) {
					emiRecived = emiRecived + emiCard.getEmiAmount();
					profit = profit + emiCard.getEmiInterstAmount();
					numberOfEmisPaid = numberOfEmisPaid + 1;
				} else {
					if (emiCard.getStatus().toString().equals(com.oxyloans.entity.loan.LoanEmiCard.Status.INPROCESS)) {
						remainingEmiAmount = remainingEmiAmount + emiCard.getRemainingEmiAmount();
					}

				}

				if (count == length) {
					lenderEmiDetailsResponseDto.setBorrowerId(borrowerId);
					lenderEmiDetailsResponseDto.setBorrowerName(
							user.getPersonalDetails().getFirstName() + user.getPersonalDetails().getLastName());
					lenderEmiDetailsResponseDto.setLoanId(loan.getLoanId());

					lenderEmiDetailsResponseDto.setDisburmentAmount(loan.getDisbursmentAmount());
					lenderEmiDetailsResponseDto.setEmisReceived(Math.round(emiRecived + remainingEmiAmount));
					lenderEmiDetailsResponseDto.setProfit(Math.round(profit));
					lenderEmiDetailsResponseDto.setNumbersOfEmisPaid(numberOfEmisPaid);
					listOfLenderEmiDetailsResponseDto.add(lenderEmiDetailsResponseDto);
				}
			}

		}
		return listOfLenderEmiDetailsResponseDto;

	}

	@Override
	public void disbursedLoans() {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		SearchRequestDto leftLeftOperand = new SearchRequestDto();
		leftLeftOperand.setFieldName("loanStatus");
		leftLeftOperand.setFieldValue("Active");
		leftLeftOperand.setOperator(Operator.EQUALS);

		SearchRequestDto rightLeftOperand = new SearchRequestDto();
		rightLeftOperand.setFieldName("borrowerDisbursedDate");
		rightLeftOperand.setFieldValue(sdf.format(new Date()));
		rightLeftOperand.setOperator(Operator.EQUALS);

		SearchRequestDto resultsRequest = new SearchRequestDto();
		resultsRequest.setLeftOperand(leftLeftOperand);
		resultsRequest.setLogicalOperator(LogicalOperator.AND);
		resultsRequest.setRightOperand(rightLeftOperand);
		SearchResultsDto<LoanResponseDto> results = disbursedLoans(resultsRequest);
		List<LoanResponseDto> Loans = results.getResults();
		for (LoanResponseDto result : Loans) {
			TemplateContext context = new TemplateContext();
			context.put("requesterName",
					result.getBorrowerUser().getFirstName() + result.getBorrowerUser().getLastName());

			context.put("salary", result.getBorrowerUser().getSalary());
			context.put("companyName", result.getBorrowerUser().getCompanyName());
			context.put("borrowerDisbursementDate", result.getBorrowerDisbursementDate());
			context.put("emiStartDate", result.getEmiStartDate());
			context.put("loanDisbursedAmount", result.getLoanDisbursedAmount());
			String mailsubject = "DisbursementInformation"; // Mail Goes to Borrower so subject is like this..
			String emailTemplateName = "disbursement-information.template";

			EmailRequest emailRequest = new EmailRequest(new String[] { "muni.sankar@oxyloans.com",
					"subbu@oxyloans.com", "lakshmi@oxyloans.com", "radhakrishna.t@oxyloans.com" }, mailsubject,
					emailTemplateName, context);
			EmailResponse emailResponse = emailService.sendEmail(emailRequest);
			if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
				throw new RuntimeException(emailResponse.getErrorMessage());
			}
		}
	}

	private SearchResultsDto<LoanResponseDto> disbursedLoans(SearchRequestDto searchRequestDto) {
		Sort sort = null;
		if (searchRequestDto.getSortBy() != null) {
			sort = Sort.by(Direction.valueOf(searchRequestDto.getSortOrder().name()), searchRequestDto.getSortBy());
		} else {
			sort = Sort.by(Direction.DESC, "loanActiveDate");
		}
		Pageable searchPage = PageRequest.of(searchRequestDto.getPage().getPageNo() - 1,
				searchRequestDto.getPage().getPageSize(), sort);
		Specification<OxyLoan> spec = null;
		try {
			spec = specificationProvider.construct(OxyLoan.class, searchRequestDto);
		} catch (ClassNotFoundException e) {
			logger.error(e, e);
		} catch (IllegalAccessException e) {
			logger.error(e, e);
		} catch (InvocationTargetException e) {
			logger.error(e, e);
		} catch (NoSuchMethodException e) {
			logger.error(e, e);
		} catch (InstantiationException e) {
			logger.error(e, e);
		}
		Page<OxyLoan> findAll = oxyLoanRepo.findAll(spec, searchPage);
		SearchResultsDto<LoanResponseDto> results = new SearchResultsDto<LoanResponseDto>();
		if (findAll != null) {
			results.setTotalCount(Long.valueOf(findAll.getTotalElements()).intValue());
			results.setPageNo(searchRequestDto.getPage().getPageNo());
			results.setPageCount(findAll.getNumberOfElements());
			List<LoanResponseDto> loanRequests = new ArrayList<LoanResponseDto>();
			;
			results.setResults(loanRequests);
			for (OxyLoan request : findAll.getContent()) {
				int count = 0;
				loanRequests.add(populateLoanDetails(request, count));
			}
		}
		return results;
	}

	@Override
	public List<LenderHistroryResponseDto> getLenderHistory(int userId) {
		List<Object[]> lenderOxyWallet = lenderOxyWalletNativeRepo.transactionHistory(userId);
		List<LenderHistroryResponseDto> lenderHistoryList = new ArrayList<LenderHistroryResponseDto>();
		lenderOxyWallet.forEach(e -> {
			LenderHistroryResponseDto lenderHistroryResponseDto = new LenderHistroryResponseDto();

			String transactionType = e[3] == null ? " " : e[3].toString();
			String loadedType = " ";
			if (transactionType.equalsIgnoreCase("credit")) {
				Integer creditedBy = Integer.parseInt(e[5] == null ? "0" : e[5].toString());
				Integer dealId = Integer.parseInt(e[6] == null ? "0" : e[6].toString());
				if (creditedBy > 0) {
					User user = userRepo.findById(creditedBy).get();
					if (user != null) {
						if (user.getPersonalDetails() != null) {
							loadedType = "Credited To Wallet from " + user.getPersonalDetails().getFirstName() + " "
									+ user.getPersonalDetails().getLastName();

						}

					}
				} else {
					if (dealId > 0) {
						OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
								.findDealAlreadyGiven(dealId);
						if (oxyBorrowersDealsInformation != null) {
							loadedType = "Credited To Wallet from " + oxyBorrowersDealsInformation.getDealName();
						}

					} else {
						loadedType = "Credited To Wallet";
					}

				}
				lenderHistroryResponseDto.setAccountNumber("OXYLRV" + userId);
				lenderHistroryResponseDto.setCreditedAmount(Double.parseDouble(e[1] == null ? "0.0" : e[1].toString()));
				lenderHistroryResponseDto.setDebitedAmount(0.0);
				lenderHistroryResponseDto.setAmountFrom(loadedType);
			} else if (transactionType.equalsIgnoreCase("debit")) {
				Integer debitedBy = Integer.parseInt(e[5] == null ? "0" : e[5].toString());
				if (debitedBy > 0) {
					User user = userRepo.findById(debitedBy).get();
					if (user != null) {
						if (user.getPersonalDetails() != null) {
							loadedType = "Withdraw From Wallet And Credited To "
									+ user.getPersonalDetails().getFirstName() + " "
									+ user.getPersonalDetails().getLastName();

						}

					}
				} else {
					loadedType = "Withdraw From Wallet";

				}

				lenderHistroryResponseDto.setAccountNumber("OXYLRV" + userId);
				lenderHistroryResponseDto.setCreditedAmount(0.0);
				lenderHistroryResponseDto.setDebitedAmount(Double.parseDouble(e[1] == null ? "0.0" : e[1].toString()));
				lenderHistroryResponseDto.setAmountFrom(loadedType);

			} else if (transactionType.equalsIgnoreCase("NORMAL") || transactionType.equalsIgnoreCase("ESCROW")
					|| transactionType.equalsIgnoreCase("EQUITY") || transactionType.equalsIgnoreCase("PERSONAL")) {
				loadedType = e[4] == null ? " " : e[4].toString() + " deal";
				lenderHistroryResponseDto.setAccountNumber(" ");
				lenderHistroryResponseDto.setCreditedAmount(0.0);
				lenderHistroryResponseDto.setDebitedAmount(Double.parseDouble(e[1] == null ? "0.0" : e[1].toString()));
				lenderHistroryResponseDto.setAmountFrom("Updated in " + loadedType);

			} else if (transactionType.equalsIgnoreCase("PARTICIPATED")
					|| transactionType.equalsIgnoreCase("NOTPARTICIPATED")) {
				loadedType = e[4] == null ? " " : e[4].toString() + " deal";
				lenderHistroryResponseDto.setAccountNumber(" ");
				lenderHistroryResponseDto.setCreditedAmount(0.0);
				lenderHistroryResponseDto.setDebitedAmount(Double.parseDouble(e[1] == null ? "0.0" : e[1].toString()));
				lenderHistroryResponseDto.setAmountFrom("Participated in " + loadedType);

			} else {
				loadedType = " ";
				lenderHistroryResponseDto.setAccountNumber("Borrower name :" + e[4].toString());
				lenderHistroryResponseDto.setCreditedAmount(0.0);
				lenderHistroryResponseDto.setDebitedAmount(Double.parseDouble(e[1] == null ? "0.0" : e[1].toString()));
				lenderHistroryResponseDto.setAmountFrom(loadedType);
			}

			lenderHistroryResponseDto.setTransactionDate(e[2].toString());

			lenderHistoryList.add(lenderHistroryResponseDto);
		});
		return lenderHistoryList;
	}

	@Override
	public TotalLendersInformationResponseDto getLendersLoansInformation(PageDto pageDto) {
		TotalLendersInformationResponseDto totalLendersInformationResponseDto = new TotalLendersInformationResponseDto();
		List<UsersLoansInformationResponseDto> listOfUsersLoansInformationResponseDto = new ArrayList<UsersLoansInformationResponseDto>();

		List<Integer> listOfUsers = userRepo.getLendersLoanInformation(pageDto.getPageSize(), pageDto.getPageNo());

		int totalCount = userRepo.getLendersCount();
		String lenderName = null;
		int lenderId = 0;

		for (int i = 0; i < listOfUsers.size(); i++) {

			UsersLoansInformationResponseDto usersLoansInformationResponseDto = new UsersLoansInformationResponseDto();
			int id = listOfUsers.get(i);
			lenderId = id;

			User user = userRepo.findById(id).get();

			lenderName = user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName();
			List<OxyLoan> oxyLoan = oxyLoanRepo.findByLenderUserId(id);

			double totalDisburmentAmount = 0.0;
			double principalRecevied = 0.0;
			double totalProfit = 0.0;
			double oustandingAmount = 0.0;
			if (oxyLoan.size() > 0) {
				for (OxyLoan loan : oxyLoan) {
					int oxyloansId = loan.getId();

					totalDisburmentAmount = totalDisburmentAmount + loan.getDisbursmentAmount();
					List<LoanEmiCard> EmiDetails = loanEmiCardRepo.findByLoanId(oxyloansId);

					for (LoanEmiCard loanEmiCard : EmiDetails) {
						if (loanEmiCard.getEmiPaidOn() != null) {
							principalRecevied = principalRecevied + Math.round(loanEmiCard.getEmiPrincipalAmount());
							totalProfit = totalProfit + Math.round(loanEmiCard.getEmiInterstAmount());

						} else {
							Date date = new Date();
							Date emiDueOnDate = loanEmiCard.getEmiDueOn();
							if (emiDueOnDate.before(date)) {
								oustandingAmount = oustandingAmount + Math.round(loanEmiCard.getEmiAmount());
							}
						}

					}
				}

			}

			usersLoansInformationResponseDto.setLenderId(lenderId);
			usersLoansInformationResponseDto.setLenderName(lenderName);

			usersLoansInformationResponseDto.setTotalDisbursementAmount(Math.round(totalDisburmentAmount));
			usersLoansInformationResponseDto.setTotalPrincipalRecevied(Math.round(principalRecevied));
			usersLoansInformationResponseDto.setTotalProfit(Math.round(totalProfit));
			usersLoansInformationResponseDto.setTotalOutstandingAmount(Math.round(oustandingAmount));
			listOfUsersLoansInformationResponseDto.add(usersLoansInformationResponseDto);

		}

		totalLendersInformationResponseDto.setResults(listOfUsersLoansInformationResponseDto);
		totalLendersInformationResponseDto.setTotalCount(totalCount);
		return totalLendersInformationResponseDto;

	}

	public String individualLoansInformation(String borrowerName, String loanId, String disburmentAmount,
			String individualPrincipalPaid, String individualProfitPaid, String individualOutstanding) {

		String rowdata = "<fo:table-row>\r\n" + " <fo:table-cell border=\"solid black 1px\"\r\n"
				+ "padding=\"2px\">\r\n" + " <fo:block>" + borrowerName + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + "padding=\"2px\">\r\n" + " <fo:block>" + "LN"
				+ loanId + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + "padding=\"2px\">\r\n" + " <fo:block>"
				+ disburmentAmount + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + "padding=\"2px\">\r\n" + " <fo:block>"
				+ individualPrincipalPaid + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + "padding=\"2px\">\r\n" + " <fo:block>"
				+ individualProfitPaid + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + "padding=\"2px\">\r\n" + " <fo:block>"
				+ individualOutstanding + "</fo:block>\r\n" + " </fo:table-cell>\r\n" +

				"    </fo:table-row>";

		return rowdata;
	}

	@Override
	public LenderIndividualLoansCountResposeDto getloanDetails(int lenderUserId, PageDto pageDto)
			throws PdfGeenrationException, IOException {

		List<UsersLoansInformationResponseDto> listOfUsersLoansInformationResponseDto = new ArrayList<UsersLoansInformationResponseDto>();
		List<Object[]> oxyLoan = oxyLoanRepo.getIndividualLoans(lenderUserId, pageDto.getPageSize(),
				pageDto.getPageNo());
		if (oxyLoan.size() == 0) {
			throw new ActionNotAllowedException("Loans are not active to this user", ErrorCodes.USER_NOT_FOUND);
		}
		TemplateContext templateContext = new TemplateContext();
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(lenderUserId);
		User user = userRepo.findById(lenderUserId).get();
		String applicationId = loanRequest.getLoanRequestId();
		String lenderName = user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName();
		List<OxyLoan> Loan = oxyLoanRepo.findByLenderUserId(lenderUserId);
		double totalDisburmentAmount = 0.0;
		double principalRecevied = 0.0;
		double totalProfit = 0.0;
		double totalOutstanding = 0.0;
		String individualLoanInformation = "";
		for (OxyLoan loan : Loan) {
			UsersLoansInformationResponseDto usersLoansInformationResponseDto = new UsersLoansInformationResponseDto();
			int oxyloansId = loan.getId();
			int borrowerId = loan.getBorrowerUserId();
			User user1 = userRepo.findById(borrowerId).get();
			String borrowerName = user1.getPersonalDetails().getFirstName() + " "
					+ user1.getPersonalDetails().getLastName();
			double disburmentAmount = loan.getDisbursmentAmount();
			totalDisburmentAmount = totalDisburmentAmount + loan.getDisbursmentAmount();
			List<LoanEmiCard> EmiDetails = loanEmiCardRepo.findByLoanId(oxyloansId);
			double individualPrincipalPaid = 0.0;
			double individualProfitPaid = 0.0;
			double individualOutstanding = 0.0;

			for (LoanEmiCard loanEmiCard : EmiDetails) {
				if (loanEmiCard.getEmiPaidOn() != null) {
					principalRecevied = principalRecevied + Math.round(loanEmiCard.getEmiPrincipalAmount());
					totalProfit = totalProfit + Math.round(loanEmiCard.getEmiInterstAmount());
					individualPrincipalPaid = individualPrincipalPaid + Math.round(loanEmiCard.getEmiPrincipalAmount());
					individualProfitPaid = individualProfitPaid + Math.round(loanEmiCard.getEmiInterstAmount());
				} else {
					Date date = new Date();
					Date emiDueDate = loanEmiCard.getEmiDueOn();
					if (emiDueDate.before(date)) {
						individualOutstanding = individualOutstanding + Math.round(loanEmiCard.getEmiAmount());
						totalOutstanding = totalOutstanding + Math.round(loanEmiCard.getEmiAmount());
					}
				}

			}

			individualLoanInformation = individualLoanInformation
					+ individualLoansInformation(borrowerName + " ", oxyloansId + "", disburmentAmount + " ",
							individualPrincipalPaid + " ", individualProfitPaid + " ", individualOutstanding + " ");

		}

		templateContext.put("individualLoans", individualLoanInformation);
		templateContext.put("lenderName", lenderName);
		templateContext.put("lenderId", lenderUserId);
		templateContext.put("totalDisburmentAmount", totalDisburmentAmount);
		templateContext.put("totalPrincipalReceived", principalRecevied);
		templateContext.put("totalProfitReceived", totalProfit);
		templateContext.put("totalOutstandingAmount", totalOutstanding);

		String outputFileName = applicationId + "lenderLoanInformation" + ".pdf";

		String lenderLoanInformation = pdfEngine.generatePdf("agreement/lenderLoanInformation.xml", templateContext,
				outputFileName);

		FileRequest fileRequest = new FileRequest();
		fileRequest.setInputStream(new FileInputStream(lenderLoanInformation));
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.lenderLoanInformation);
		fileRequest.setFilePrifix(FileType.lenderLoanInformation.name());

		try {
			FileResponse putFile = fileManagementService.putFile(fileRequest);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		File file = new File(lenderLoanInformation);
		boolean fileDeleted = file.delete();

		int length = Loan.size();

		if (oxyLoan != null && !oxyLoan.isEmpty()) {

			oxyLoan.forEach(e -> {

				UsersLoansInformationResponseDto usersLoansInformationResponseDto = new UsersLoansInformationResponseDto();
				int id = Integer.parseInt(e[0] == null ? "0" : e[0].toString());

				int borrowerId = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
				double individualDisburmentAmount = Double.parseDouble(e[2] == null ? "0" : e[2].toString());

				User user2 = userRepo.findById(borrowerId).get();
				List<LoanEmiCard> loanEmiCard = loanEmiCardRepo.findByLoanId(id);
				double individualPricipalAmount = 0.0;
				double individualProfit = 0.0;
				double individualOutstandingAmount = 0.0;

				Date date = new Date();

				for (LoanEmiCard emiDetails : loanEmiCard) {

					if (emiDetails.getEmiPaidOn() != null) {
						individualPricipalAmount = individualPricipalAmount + emiDetails.getEmiPrincipalAmount();
						individualProfit = individualProfit + emiDetails.getEmiInterstAmount();
					} else {
						Date date3 = emiDetails.getEmiDueOn();

						if (date3.before(date)) {
							individualOutstandingAmount = individualOutstandingAmount + emiDetails.getEmiAmount();
						}
					}

				}

				usersLoansInformationResponseDto.setLoanId("LN" + id);
				usersLoansInformationResponseDto.setIndividualDisburmentAmount(individualDisburmentAmount);
				usersLoansInformationResponseDto.setIndividualPrincipalPaid(Math.round(individualPricipalAmount));
				usersLoansInformationResponseDto.setIndividualProfitPaid(Math.round(individualProfit));
				usersLoansInformationResponseDto.setIndividualOutstanding(Math.round(individualOutstandingAmount));
				usersLoansInformationResponseDto.setBorrowerName(
						user2.getPersonalDetails().getFirstName() + " " + user2.getPersonalDetails().getLastName());

				listOfUsersLoansInformationResponseDto.add(usersLoansInformationResponseDto);

			});

		}
		LenderIndividualLoansCountResposeDto lenderIndividualLoansCountResposeDto = new LenderIndividualLoansCountResposeDto();
		lenderIndividualLoansCountResposeDto.setCount(length);
		lenderIndividualLoansCountResposeDto.setUserLoansInfoList(listOfUsersLoansInformationResponseDto);

		return lenderIndividualLoansCountResposeDto;

	}

	@Override
	public UsersLoansInformationResponseDto getLenderLoansInformationPdf(int userId) {
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
		String applicationId = loanRequest.getLoanRequestId();
		FileRequest fileRequest = new FileRequest();
		String outputFileName = applicationId + "lenderLoanInformation" + ".pdf";
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.lenderLoanInformation);
		fileRequest.setFilePrifix(FileType.lenderLoanInformation.name());
		FileResponse file = fileManagementService.getFile(fileRequest);
		UsersLoansInformationResponseDto usersLoansInformationResponseDto = new UsersLoansInformationResponseDto();
		usersLoansInformationResponseDto.setDownloadUrl(file.getDownloadUrl());
		usersLoansInformationResponseDto.setFileName(outputFileName);

		return usersLoansInformationResponseDto;

	}

	@Override
	public LoanEmiCardPaymentDetailsResponseDto updEmisByApplication(
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public SearchResultsDto featurePending(int year, int month, SearchRequestDto searchRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BorrowerIndividualLoansCountResposeDto getloanDetailsForBorrower(int borrowerUserId, PageDto pageDto)
			throws PdfGeenrationException, IOException {

		List<UsersLoansInformationResponseDto> listOfUsersLoansInformationResponseDto = new ArrayList<UsersLoansInformationResponseDto>();
		List<Object[]> oxyLoan = oxyLoanRepo.getIndividualLoansForBorrower(borrowerUserId, pageDto.getPageSize(),
				pageDto.getPageNo());
		if (oxyLoan.size() == 0) {
			throw new ActionNotAllowedException("Loans are not active to this user", ErrorCodes.USER_NOT_FOUND);
		}
		TemplateContext templateContext = new TemplateContext();
		// LoanRequest loanRequest =
		// loanRequestRepo.findByUserIdAndParentRequestIdIsNull(borrowerUserId);
		User user = userRepo.findById(borrowerUserId).get();
		// String applicationId = loanRequest.getLoanRequestId();
		String borrowerName = user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName();
		List<OxyLoan> Loan = oxyLoanRepo.findByBorrowerUserId(borrowerUserId);
		double totalDisburmentAmount = 0.0;
		double principalRecevied = 0.0;
		double totalProfit = 0.0;
		double totalOutstanding = 0.0;
		String individualLoanInformation = "";
		for (OxyLoan loan : Loan) {
			UsersLoansInformationResponseDto usersLoansInformationResponseDto = new UsersLoansInformationResponseDto();
			int oxyloansId = loan.getId();
			int lenderId = loan.getLenderUserId();
			User user1 = userRepo.findById(lenderId).get();
			String lenderName = user1.getPersonalDetails().getFirstName() + " "
					+ user1.getPersonalDetails().getLastName();
			double disburmentAmount = loan.getDisbursmentAmount();
			totalDisburmentAmount = totalDisburmentAmount + loan.getDisbursmentAmount();
			List<LoanEmiCard> EmiDetails = loanEmiCardRepo.findByLoanId(oxyloansId);
			double individualPrincipalPaid = 0.0;
			double individualProfitPaid = 0.0;
			double individualOutstanding = 0.0;

			for (LoanEmiCard loanEmiCard : EmiDetails) {
				if (loanEmiCard.getEmiPaidOn() != null) {
					principalRecevied = principalRecevied + Math.round(loanEmiCard.getEmiPrincipalAmount());
					totalProfit = totalProfit + Math.round(loanEmiCard.getEmiInterstAmount());
					individualPrincipalPaid = individualPrincipalPaid + Math.round(loanEmiCard.getEmiPrincipalAmount());
					individualProfitPaid = individualProfitPaid + Math.round(loanEmiCard.getEmiInterstAmount());
				} else {
					Date date = new Date();
					Date emiDueDate = loanEmiCard.getEmiDueOn();
					if (emiDueDate.before(date)) {
						individualOutstanding = individualOutstanding + Math.round(loanEmiCard.getEmiAmount());
						totalOutstanding = totalOutstanding + Math.round(loanEmiCard.getEmiAmount());
					}
				}

			}

			individualLoanInformation = individualLoanInformation
					+ individualLoansInformation(lenderName + " ", oxyloansId + "", disburmentAmount + " ",
							individualPrincipalPaid + " ", individualProfitPaid + " ", individualOutstanding + " ");

		}

		templateContext.put("individualLoans", individualLoanInformation);
		templateContext.put("borrowerName", borrowerName);
		templateContext.put("borrowerId", borrowerUserId);
		templateContext.put("totalDisburmentAmount", totalDisburmentAmount);
		templateContext.put("totalPrincipalReceived", principalRecevied);
		templateContext.put("totalProfitReceived", totalProfit);
		templateContext.put("totalOutstandingAmount", totalOutstanding);

		String outputFileName = borrowerUserId + "borrowerLoanInformation" + ".pdf";

		String lenderLoanInformation = pdfEngine.generatePdf("agreement/borrowerLoanInformation.xml", templateContext,
				outputFileName);

		FileRequest fileRequest = new FileRequest();
		fileRequest.setInputStream(new FileInputStream(lenderLoanInformation));
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.borrowerLoanInformation);
		fileRequest.setFilePrifix(FileType.borrowerLoanInformation.name());

		try {
			FileResponse putFile = fileManagementService.putFile(fileRequest);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		File file = new File(lenderLoanInformation);
		boolean fileDeleted = file.delete();

		int length = Loan.size();

		if (oxyLoan != null && !oxyLoan.isEmpty()) {

			oxyLoan.forEach(e -> {

				UsersLoansInformationResponseDto usersLoansInformationResponseDto = new UsersLoansInformationResponseDto();
				int id = Integer.parseInt(e[0] == null ? "0" : e[0].toString());

				int lenderId = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
				double individualDisburmentAmount = Double.parseDouble(e[2] == null ? "0" : e[2].toString());

				User user2 = userRepo.findById(lenderId).get();
				List<LoanEmiCard> loanEmiCard = loanEmiCardRepo.findByLoanId(id);
				double individualPricipalAmount = 0.0;
				double individualProfit = 0.0;
				double individualOutstandingAmount = 0.0;

				Date date = new Date();

				for (LoanEmiCard emiDetails : loanEmiCard) {

					if (emiDetails.getEmiPaidOn() != null) {
						individualPricipalAmount = individualPricipalAmount + emiDetails.getEmiPrincipalAmount();
						individualProfit = individualProfit + emiDetails.getEmiInterstAmount();
					} else {
						Date date3 = emiDetails.getEmiDueOn();

						if (date3.before(date)) {
							individualOutstandingAmount = individualOutstandingAmount + emiDetails.getEmiAmount();
						}
					}

				}

				usersLoansInformationResponseDto.setLoanId("LN" + id);
				usersLoansInformationResponseDto.setIndividualDisburmentAmount(individualDisburmentAmount);
				usersLoansInformationResponseDto.setIndividualPrincipalPaid(Math.round(individualPricipalAmount));
				usersLoansInformationResponseDto.setIndividualProfitPaid(Math.round(individualProfit));
				usersLoansInformationResponseDto.setIndividualOutstanding(Math.round(individualOutstandingAmount));
				usersLoansInformationResponseDto.setLenderName(
						user2.getPersonalDetails().getFirstName() + " " + user2.getPersonalDetails().getLastName());

				listOfUsersLoansInformationResponseDto.add(usersLoansInformationResponseDto);

			});

		}
		BorrowerIndividualLoansCountResposeDto borrowerIndividualLoansCountResposeDto = new BorrowerIndividualLoansCountResposeDto();
		borrowerIndividualLoansCountResposeDto.setCount(length);
		borrowerIndividualLoansCountResposeDto.setUserLoansInfoList(listOfUsersLoansInformationResponseDto);

		return borrowerIndividualLoansCountResposeDto;

	}

	@Override
	public UsersLoansInformationResponseDto getBorrowerLoansInformationPdf(int userId) {

		FileRequest fileRequest = new FileRequest();
		String outputFileName = userId + "borrowerLoanInformation" + ".pdf";
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.borrowerLoanInformation);
		fileRequest.setFilePrifix(FileType.borrowerLoanInformation.name());
		FileResponse file = fileManagementService.getFile(fileRequest);
		UsersLoansInformationResponseDto usersLoansInformationResponseDto = new UsersLoansInformationResponseDto();
		usersLoansInformationResponseDto.setDownloadUrl(file.getDownloadUrl());
		usersLoansInformationResponseDto.setFileName(outputFileName);

		return usersLoansInformationResponseDto;

	}

	@Override
	@Transactional
	public LoanResponseDto manualEsignProcess(String loanId) {
		OxyLoan oxyLoan = oxyLoanRepo.findByLoanId(loanId);

		if (oxyLoan == null) {
			throw new NoSuchElementException("No Loan present to esign against");
		}

		int borrowerParentRequestId = oxyLoan.getBorrowerParentRequestId();
		double disburmentAmount = oxyLoan.getDisbursmentAmount();
		int lenderUserId = oxyLoan.getLenderUserId();
		int borrowerUserId = oxyLoan.getBorrowerUserId();

		LoanRequest loanRequest = loanRequestRepo.findByLoanId(loanId);
		if (loanRequest == null) {
			throw new NoSuchElementException("No Loan present to esign against");
		}
		LoanRequest loanRequestDetails = loanRequestRepo.findById(borrowerParentRequestId).get();
		if (loanRequestDetails.getLoanStatus() == LoanStatus.Requested) {
			Double totalDisbursedAmount = loanRequestDetails.getDisbursmentAmount() + disburmentAmount;
			// Double totalDisbursedAmount = loanRequestDetails.getDisbursmentAmount();
			if (loanRequestDetails.getLoanOfferedAmount() != null) {
				if (totalDisbursedAmount <= loanRequestDetails.getLoanOfferedAmount().getLoanOfferedAmount()) {
					loanRequestDetails.setDisbursmentAmount(totalDisbursedAmount);
					loanRequestRepo.save(loanRequestDetails);
				} else {
					throw new OperationNotAllowedException(
							"Total disbursed amount is greater than offered amount please check",
							ErrorCodes.LIMIT_REACHED);
				}
			} else {
				loanRequestDetails.setDisbursmentAmount(totalDisbursedAmount);
				loanRequestRepo.save(loanRequestDetails);
			}

		}

		if (oxyLoan.getLoanStatus() == LoanStatus.Agreed && oxyLoan.getBorrowerDisbursedDate() == null) {

			oxyLoan.setLoanStatus(LoanStatus.Active);
			oxyLoan.setLoanActiveDate(new Date());
			oxyLoan.setLenderEsignId(0000);
			oxyLoan.setBorrowerEsignId(0000);
			loanRequest.setAdminComments("APPROVED");

			lenderBorrowerConversationRepo.deleteByLenderUserIdAndBorrowerUserId(lenderUserId, borrowerUserId);
			if (oxyLoan.getDurationType().equals(DurationType.Months)) {
				generateEmiCard(oxyLoan, loanRequest);
			} else {
				generateEmiCardForDays(oxyLoan, loanRequest);
			}

			oxyLoan = oxyLoanRepo.save(oxyLoan);
			loanRequest = loanRequestRepo.save(loanRequest);

		}
		// generateENACHMandate(oxyLoan);
		if (oxyLoan != null) {
			if (oxyLoan.getEnachMandate() == null || oxyLoan.getEnachMandate().isEmpty()) {
				generateENACHMandate(oxyLoan);

			}

		}
		int agreedLoansCount = oxyLoanRepo.getAgreedLoansCount(borrowerParentRequestId);
		if (agreedLoansCount == 0) {
			String mailsubject = "BorrowerDealStructure"; // Mail Goes to Borrower so subject is like this..
			String emailTemplateName = "agreement.template";
			int loanRequestId = loanRequest.getId();
			int userId = oxyLoan.getBorrowerUserId();
			User user = userRepo.findById(userId).get();
			KycFileResponse kycFileResponse = downloadLoanAgrement(userId, loanRequestId);
			TemplateContext context = new TemplateContext();
			if (kycFileResponse != null) {
				context.put("agreement", kycFileResponse.getDownloadUrl());
			}
			int id = 6680;
			KycFileResponse kycFileResponseOfRepayment = null;
			try {
				kycFileResponseOfRepayment = userServiceImpl.downloadFile(id, KycType.REPAYMENTACCOUNTDETAILS);
			} catch (IOException e) {

				e.printStackTrace();
			}
			if (kycFileResponseOfRepayment != null) {
				String urlDownload = kycFileResponseOfRepayment.getDownloadUrl();
				String[] splitingTheUrl = urlDownload.split(Pattern.quote("?"));
				String url = splitingTheUrl[0];
				context.put("repaymentAccountDetails", url);
			}
			context.put("name",
					user.getPersonalDetails().getFirstName().substring(0, 1).toUpperCase()
							+ user.getPersonalDetails().getFirstName().substring(1) + " "
							+ user.getPersonalDetails().getLastName().substring(0, 1).toUpperCase()
							+ user.getPersonalDetails().getLastName().substring(1));
			context.put("borrowerId", user.getUniqueNumber());
			LoanRequest loanRequestValues = loanRequestRepo.findById(borrowerParentRequestId).get();
			List<String> listOfLoanIds = oxyLoanRepo.getLoanIdsBasedOnApplicationId(borrowerParentRequestId);
			String loanIds = " ";
			for (int i = 0; i < listOfLoanIds.size(); i++) {
				loanIds = loanIds + listOfLoanIds.get(i) + ",";
			}
			Date date = new Date();
			String date1 = expectedDateFormat.format(date);
			context.put("loanIds", loanIds);
			context.put("loanAmount", loanRequestValues.getLoanOfferedAmount().getLoanOfferedAmount());
			context.put("processingFree", loanRequestValues.getLoanOfferedAmount().getBorrowerFee());
			context.put("netDisburmentAmount", loanRequestValues.getLoanOfferedAmount().getNetDisbursementAmount());
			context.put("duration", loanRequestValues.getLoanOfferedAmount().getDuration());
			context.put("emi", loanRequestValues.getLoanOfferedAmount().getEmiAmount());
			LoanEmiCard firstEmiInfo = loanEmiCardRepo.getFirstEmiDetails(oxyLoan.getId());
			int emiNumber = oxyLoan.getDuration();
			LoanEmiCard lastEmiInfo = loanEmiCardRepo.getLastEmiDetails(oxyLoan.getId(), emiNumber);
			if (firstEmiInfo != null) {
				context.put("emiStartDate", expectedDateFormat.format(firstEmiInfo.getEmiDueOn()));
			}
			if (lastEmiInfo != null) {
				context.put("emiEndDate", expectedDateFormat.format(lastEmiInfo.getEmiDueOn()));
			}
			context.put("bankAccount", user.getBankDetails().getAccountNumber());
			context.put("ifsc", user.getBankDetails().getIfscCode());
			context.put("address", user.getPersonalDetails().getAddress());
			context.put("email", user.getEmail());
			context.put("mobileNumber", user.getMobileNumber());
			context.put("currentDate", date1);
			EmailRequest emailRequest = new EmailRequest(new String[] { "radhakrishna.t@oxyloans.com",
					"ramadevi@oxyloans.com", "archana.n@oxyloans.com", "subbu@oxyloans.com" }, mailsubject,
					emailTemplateName, context);
			EmailResponse emailResponse = emailService.sendEmail(emailRequest);
			if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
				throw new RuntimeException(emailResponse.getErrorMessage());
			}
		}
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setLoanId(oxyLoan.getLoanId());
		loanResponseDto.setAdminComments(loanRequest.getAdminComments());
		logger.info("manualEsignProcess method end");
		return loanResponseDto;

	}

	@Override
	public SearchResultsDto<LoanResponseDto> bucketPendingEmis(int start, int end, String type, int pageNo,
			int pageSize) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public NotificationCountResponseDto getAdminPendingActionsDetails() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanResponseDto rejectLoanOffer(int id) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public void calculatingPenalityUsingSchedular() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public int getUnpaidLoanEmiDetails(int loanId,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanResponseDto sendingEmailsBeforeEmiDate() {

		List<User> users = userRepo.findByPrimaryType(PrimaryType.BORROWER);
		for (User user : users) {
			if (!utm.equals(user.getUrchinTrackingModule())) {
				PersonalDetails personalDetails = user.getPersonalDetails();
				Calendar currentDate = Calendar.getInstance();
				Calendar currentCalendar = Calendar.getInstance();
				Calendar beforeThreecalendar = Calendar.getInstance();
				beforeThreecalendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(this.sendingEmailBeforeThreeDays));
				boolean beforeThreeDays = currentCalendar.get(Calendar.DAY_OF_YEAR) == beforeThreecalendar
						.get(Calendar.DAY_OF_YEAR)
						&& currentCalendar.get(Calendar.YEAR) == beforeThreecalendar.get(Calendar.YEAR);
				Calendar beforeSevencalendar = Calendar.getInstance();

				beforeSevencalendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(this.sendingEmailBeforeSevenDays));

				boolean beforeSevenDays = currentCalendar.get(Calendar.DAY_OF_YEAR) == beforeSevencalendar
						.get(Calendar.DAY_OF_YEAR)
						&& currentCalendar.get(Calendar.YEAR) == beforeSevencalendar.get(Calendar.YEAR);

				if (beforeThreeDays) {
					Calendar calendar = Calendar.getInstance();
					calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(this.defaultEmiDay));
					// testing
					List<Object[]> totalAmount = loanEmiCardRepo.findByEmiDueOnAndEmiPaidOnIsNull(calendar.getTime(),
							user.getId());

					if (totalAmount != null) {
						TemplateContext templateContext = new TemplateContext();
						if (personalDetails != null) {
							templateContext.put("borrowerName", personalDetails.getFirstName());
						}

						for (Object[] object : totalAmount) {
							templateContext.put("loanRequestId", object[1]);
							templateContext.put("emiAmount", Math.round((double) object[0]));
							String DueOnDate = expectedDateFormat.format(calendar.getTime());
							templateContext.put("borrowerDueOnDate", DueOnDate);
							String expectedCurrentDate = expectedDateFormat.format(currentDate.getTime());
							templateContext.put("currentDate", expectedCurrentDate);
							String mailsubject = "Notification";
							EmailRequest emailRequest = new EmailRequest(
									new String[] { user.getEmail(), "narendra@oxyloans.com", "recovery@oxyloans.com" },
									mailsubject, "Email-Borrower-Before-ThreeDays.template", templateContext);
							EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
							if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
								try {
									throw new MessagingException(emailResponseFromService.getErrorMessage());
								} catch (MessagingException e1) {

									e1.printStackTrace();
								}
							}

							String templateName = "3-reminder- template";
							MobileRequest mobileRequest = new MobileRequest(user.getMobileNumber(), templateName);
							mobileRequest.setSenderId("OXYLNS");
							Map<String, String> variblesMap = new HashMap<>();
							double emiAmount = Math.round(Math.round((double) object[0]));
							String s = String.valueOf(emiAmount);
							variblesMap.put("VAR1", object[1].toString());
							variblesMap.put("VAR2", s);
							variblesMap.put("VAR3", DueOnDate);
							mobileRequest.setVariblesMap(variblesMap);
							try {
								mobileUtil.sendTransactionalMessage(mobileRequest);
							} catch (Exception e1) {
								logger.error("SMS failed : ", e1);
							}
						}
					}

				}
				if (beforeSevenDays) {
					Calendar calendar = Calendar.getInstance();
					calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(this.defaultEmiDay));
					// testing
					calendar.add(Calendar.MONTH, 1);
					List<Object[]> totalAmount = loanEmiCardRepo.findByEmiDueOnAndEmiPaidOnIsNull(calendar.getTime(),
							user.getId());
					if (totalAmount != null) {
						TemplateContext templateContext = new TemplateContext();
						if (personalDetails != null) {
							templateContext.put("borrowerName", personalDetails.getFirstName());
						}
						for (Object[] object : totalAmount) {
							templateContext.put("loanRequestId", object[1]);
							templateContext.put("emiAmount", Math.round((double) object[0]));
							String DueOnDate = expectedDateFormat.format(calendar.getTime());
							templateContext.put("borrowerDueOnDate", DueOnDate);
							String expectedCurrentDate = expectedDateFormat.format(currentDate.getTime());
							templateContext.put("currentDate", expectedCurrentDate);
							String mailsubject = "Notification";
							EmailRequest emailRequest = new EmailRequest(
									new String[] { user.getEmail(), "narendra@oxyloans.com", "recovery@oxyloans.com" },
									mailsubject, "Email-Borrower-Before-SevenDays.template", templateContext);
							EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest);
							if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
								try {
									throw new MessagingException(emailResponseFromService.getErrorMessage());
								} catch (MessagingException e1) {

									e1.printStackTrace();
								}

							}

							String templateName = "7-days-reminder -template";
							MobileRequest mobileRequest = new MobileRequest(user.getMobileNumber(), templateName);
							mobileRequest.setSenderId("OXYLNS");
							Map<String, String> variblesMap = new HashMap<>();
							double emiAmount = Math.round(Math.round((double) object[0]));
							String s = String.valueOf(emiAmount);
							variblesMap.put("VAR1", object[1].toString());
							variblesMap.put("VAR2", s);
							variblesMap.put("VAR3", DueOnDate);
							mobileRequest.setVariblesMap(variblesMap);
							try {
								mobileUtil.sendTransactionalMessage(mobileRequest);
							} catch (Exception e1) {
								logger.error("SMS failed : ", e1);
							}
						}
					}

				}

			}
		}
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setBorrowerUserId(1);
		return loanResponseDto;

	}

	@Override
	public List<AdminEmiDetailsResponseDto> getEmiDetails() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BucketPdfresponse thirtyDaysBucketDownload() throws PdfGeenrationException, IOException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BucketPdfresponse sixtyDaysBucketDownload() throws PdfGeenrationException, IOException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BucketPdfresponse ninetyDaysBucketDownload() throws PdfGeenrationException, IOException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BucketPdfresponse npaBucketDownload() throws PdfGeenrationException, IOException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	public String getPdfLenderHistory(int userId) {
		List<LenderHistroryResponseDto> lenderHistoryList = getLenderHistory(userId);
		String lenderHistory = null;
		for (LenderHistroryResponseDto lenderHistroryResponseDto : lenderHistoryList) {
			BigInteger creditedAmount = BigDecimal.valueOf(lenderHistroryResponseDto.getCreditedAmount())
					.toBigInteger();
			BigInteger debitedAmount = BigDecimal.valueOf(lenderHistroryResponseDto.getDebitedAmount()).toBigInteger();
			lenderHistory = lenderHistory + transactionHistorResponse(lenderHistroryResponseDto.getTransactionDate(),
					lenderHistroryResponseDto.getAccountNumber(), creditedAmount, debitedAmount,
					lenderHistroryResponseDto.getAmountFrom());
		}
		return lenderHistory;
	}

	public String transactionHistorResponse(String transactionDate, String accountNumber, BigInteger creditedAmount,
			BigInteger debitedAmount, String amountFrom) {

		String rowdata = "<fo:table-row>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">"

				+ transactionDate + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">" + accountNumber + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">" + creditedAmount + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">" + debitedAmount + "</fo:block>\r\n" + " </fo:table-cell>"

				+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
				+ " <fo:block text-align=\"right\">" + amountFrom + "</fo:block>\r\n" + " </fo:table-cell>"

				+ " </fo:table-row>";

		return rowdata;
	}

	@Override
	public LenderTransactionHistoryResponseDto generateLenderHistoryPdf(int userId)
			throws PdfGeenrationException, IOException {

		TemplateContext templateContext = new TemplateContext();

		templateContext.put("LenderHistory", getPdfLenderHistory(userId));

		String outputFileName = userId + "LenderTransactionHistory" + ".pdf";
		// String outputFileName = "C:/data/LenderTransactionHistory.pdf";
		String lenderTransactionHistory = pdfEngine.generatePdf("agreement/LenderTransactionHistory.xml",
				templateContext, outputFileName);

		FileRequest fileRequest = new FileRequest();
		fileRequest.setInputStream(new FileInputStream(lenderTransactionHistory));
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.Statement);
		fileRequest.setFilePrifix(FileType.Statement.name());
		FileResponse file1 = fileManagementService.getFile(fileRequest);
		try {
			FileResponse putFile = fileManagementService.putFile(fileRequest);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		File file = new File(lenderTransactionHistory);
		boolean fileDeleted = file.delete();

		LenderTransactionHistoryResponseDto lenderTransactionHistoryResponseDto = new LenderTransactionHistoryResponseDto();

		lenderTransactionHistoryResponseDto.setDownloadUrl(file1.getDownloadUrl());

		return lenderTransactionHistoryResponseDto;
	}

	public String LenderTransactionHistory(String accountName, double creditedAmount, double deditedAmount,
			String loanStatus, String borrowerName, String loanId, String transactionDate1, String disbursedAmount,
			double walletBalance, String amountFrom, Double withdrawAmount, String dealName,
			BigInteger totalInterestAmount, BigInteger totalPrincipalAmount) {

		if (accountName == null) {
			accountName = "LoanID: " + loanId + "\r\n" + "Borrower Name: " + borrowerName;
		}
		if (disbursedAmount == null) {
			disbursedAmount = "";
		}

		if (totalInterestAmount == null) {
			totalInterestAmount = BigInteger.ZERO;
		}

		if (totalPrincipalAmount == null) {
			totalPrincipalAmount = BigInteger.ZERO;
		}

		if (dealName == null) {
			dealName = "";
		}

		if (amountFrom == null) {
			amountFrom = "";
		}

		DecimalFormat df = new DecimalFormat("#");
		df.setRoundingMode(RoundingMode.DOWN);
		String rowdata =

				"<fo:table-row>\r\n"

						+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
						+ " <fo:block text-align=\"right\">"

						+ transactionDate1 + "</fo:block>\r\n" + " </fo:table-cell>\r\n"

						+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
						+ " <fo:block text-align=\"right\">" + df.format(creditedAmount) + "</fo:block>\r\n"
						+ " </fo:table-cell>\r\n"

						+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
						+ " <fo:block text-align=\"right\">" + df.format(deditedAmount) + "</fo:block>\r\n"
						+ " </fo:table-cell>\r\n"

						+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
						+ " <fo:block text-align=\"right\">" + amountFrom + "</fo:block>\r\n" + " </fo:table-cell>"

						+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
						+ " <fo:block text-align=\"right\">" + df.format(withdrawAmount) + "</fo:block>\r\n"
						+ " </fo:table-cell>"

						+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
						+ " <fo:block text-align=\"right\">" + totalInterestAmount + "</fo:block>\r\n"
						+ " </fo:table-cell>"

						+ " <fo:table-cell border=\"solid black 1px\"\r\n" + " padding=\"2px\">\r\n"
						+ " <fo:block text-align=\"right\">" + totalPrincipalAmount + "</fo:block>\r\n"
						+ " </fo:table-cell>"

						+ " </fo:table-row>";
		return rowdata;
	}

	@Override
	public SearchResultsDto<LoanResponseDto> PendingEmisWithUserId(int start, int end, String type, int userId,
			int pageNo, int pageSize) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public SearchResultsDto<LoanResponseDto> emiPaidwithUserId(int year, int month, String type, int userId, int pageNo,
			int pageSize) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public int getAdminPendingActionsDetailsSchedular() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public UsersLoansInformationResponseDto searchForLenderAndBorrowerInfo(int userId) {
		Integer id1 = 0;
		User user1 = userRepo.findById(userId).get();
		if (user1.getPrimaryType() == PrimaryType.LENDER) {
			id1 = userRepo.getIdForLender(userId);
		} else {
			id1 = userRepo.getIdForBorrower(userId);
		}

		double totalDisburmentAmount = 0.0;
		double principalRecevied = 0.0;
		double totalProfit = 0.0;
		double oustandingAmount = 0.0;

		String userName = null;

		UsersLoansInformationResponseDto usersLoansInformationResponseDto = new UsersLoansInformationResponseDto();

		if (id1 != null) {
			User user = userRepo.findById(id1).get();

			userName = user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName();
			List<OxyLoan> oxyLoan = null;
			if (user.getPrimaryType() == PrimaryType.LENDER) {

				oxyLoan = oxyLoanRepo.findByLenderUserId(userId);

			} else {

				oxyLoan = oxyLoanRepo.findByBorrowerUserId(userId);

			}

			if (oxyLoan.size() > 0) {
				for (OxyLoan loan : oxyLoan) {
					int oxyloansId = loan.getId();

					totalDisburmentAmount = totalDisburmentAmount + loan.getDisbursmentAmount();
					List<LoanEmiCard> EmiDetails = loanEmiCardRepo.findByLoanId(oxyloansId);

					for (LoanEmiCard loanEmiCard : EmiDetails) {
						if (loanEmiCard.getEmiPaidOn() != null) {
							principalRecevied = principalRecevied + Math.round(loanEmiCard.getEmiPrincipalAmount());
							totalProfit = totalProfit + Math.round(loanEmiCard.getEmiInterstAmount());

						} else {
							Date date = new Date();
							Date emiDueOnDate = loanEmiCard.getEmiDueOn();
							if (emiDueOnDate.before(date)) {
								oustandingAmount = oustandingAmount + Math.round(loanEmiCard.getEmiAmount());
							}
						}

					}
					if (user.getPrimaryType() == PrimaryType.LENDER) {

						usersLoansInformationResponseDto.setLenderId(userId);
						usersLoansInformationResponseDto.setLenderName(userName);
					} else {

						usersLoansInformationResponseDto.setBorrowerId(userId);
						usersLoansInformationResponseDto.setBorrowerName(userName);
					}

				}
			} else {

				throw new OperationNotAllowedException("user not exits", ErrorCodes.ENITITY_NOT_FOUND);

			}
		} else {
			throw new OperationNotAllowedException("user not exits", ErrorCodes.ENITITY_NOT_FOUND);

		}

		usersLoansInformationResponseDto.setTotalDisbursementAmount(Math.round(totalDisburmentAmount));
		usersLoansInformationResponseDto.setTotalPrincipalRecevied(Math.round(principalRecevied));
		usersLoansInformationResponseDto.setTotalProfit(Math.round(totalProfit));
		usersLoansInformationResponseDto.setTotalOutstandingAmount(Math.round(oustandingAmount));
		return usersLoansInformationResponseDto;

	}

	private NomineeResponseDto getNominee(Integer id) {
		NomineeResponseDto nomineeResponseDto = new NomineeResponseDto();
		User user = userRepo.findById(id).get();
		Nominee nominee = user.getNominee();
		if (nominee != null) {
			nomineeResponseDto.setEmial(nominee.getEmail());
			nomineeResponseDto.setMobileNumber(nominee.getMobileNumber());
			nomineeResponseDto.setUserId(nominee.getUserId());
			nomineeResponseDto.setName(nominee.getName());
			nomineeResponseDto.setRelation(nominee.getRelation());
			nomineeResponseDto.setAccountNumber(nominee.getAccountNumber());
			nomineeResponseDto.setBankName(nominee.getBankName());
			nomineeResponseDto.setBranchName(nominee.getBranchName());
			nomineeResponseDto.setIfscCode(nominee.getIfscCode());
			nomineeResponseDto.setCity(nominee.getCity());
			return nomineeResponseDto;

		}
		return null;
	}

	@Override
	public TotalLendersInformationResponseDto getBorrowersLoansInformation(PageDto pageDto) {
		TotalLendersInformationResponseDto totalLendersInformationResponseDto = new TotalLendersInformationResponseDto();
		List<UsersLoansInformationResponseDto> listOfUsersLoansInformationResponseDto = new ArrayList<UsersLoansInformationResponseDto>();

		List<Integer> listOfUsers = userRepo.getBorrowersLoanInformation(pageDto.getPageSize(), pageDto.getPageNo());

		int totalCount = userRepo.getBorrowerscount();
		String borrowerName = null;
		int borrowerId = 0;

		for (int i = 0; i < listOfUsers.size(); i++) {

			UsersLoansInformationResponseDto usersLoansInformationResponseDto = new UsersLoansInformationResponseDto();
			int id = listOfUsers.get(i);
			borrowerId = id;

			User user = userRepo.findById(id).get();

			borrowerName = user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName();
			List<OxyLoan> oxyLoan = oxyLoanRepo.findByBorrowerUserId(id);

			double totalDisburmentAmount = 0.0;
			double principalRecevied = 0.0;
			double totalProfit = 0.0;
			double oustandingAmount = 0.0;
			if (oxyLoan.size() > 0) {
				for (OxyLoan loan : oxyLoan) {
					int oxyloansId = loan.getId();

					totalDisburmentAmount = totalDisburmentAmount + loan.getDisbursmentAmount();
					List<LoanEmiCard> EmiDetails = loanEmiCardRepo.findByLoanId(oxyloansId);

					for (LoanEmiCard loanEmiCard : EmiDetails) {
						if (loanEmiCard.getEmiPaidOn() != null) {
							principalRecevied = principalRecevied + Math.round(loanEmiCard.getEmiPrincipalAmount());
							totalProfit = totalProfit + Math.round(loanEmiCard.getEmiInterstAmount());

						} else {
							Date date = new Date();
							Date emiDueOnDate = loanEmiCard.getEmiDueOn();
							if (emiDueOnDate.before(date)) {
								oustandingAmount = oustandingAmount + Math.round(loanEmiCard.getEmiAmount());
							}
						}

					}
				}

			}

			usersLoansInformationResponseDto.setBorrowerId(borrowerId);
			usersLoansInformationResponseDto.setBorrowerName(borrowerName);

			usersLoansInformationResponseDto.setTotalDisbursementAmount(Math.round(totalDisburmentAmount));
			usersLoansInformationResponseDto.setTotalPrincipalRecevied(Math.round(principalRecevied));
			usersLoansInformationResponseDto.setTotalProfit(Math.round(totalProfit));
			usersLoansInformationResponseDto.setTotalOutstandingAmount(Math.round(oustandingAmount));
			listOfUsersLoansInformationResponseDto.add(usersLoansInformationResponseDto);

		}

		totalLendersInformationResponseDto.setResults(listOfUsersLoansInformationResponseDto);
		totalLendersInformationResponseDto.setTotalCount(totalCount);
		return totalLendersInformationResponseDto;

	}

	@Override
	public SearchResultsDto<ApplicationResponseDto> bucketsByApplication(int start, int end, String type, int pageNo,
			int pageSize) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public SearchResultsDto<ApplicationResponseDto> bucketsByApplicationByUserId(int start, int end, String type,
			int userId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public SearchResultsDto<BucketPendingEmis> bucketsEmiPending(int start, int end, String type, int loanId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public SearchResultsDto<LoanResponseDto> pendingEmisApplicationLevel(int year, int month, String type, int pageNo,
			int pageSize) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanResponseDto SearchByParentRequestId(String borrowerParentRequestId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanResponseDto SearchByBorrowerId(int borrowerId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public SearchResultsDto<LoanResponseDto> pendingEmisBasedOnLoanLevel(int year, int month, String type, int loanId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanEmiGenerationResDto getLoanEmiCard(int loanId, LoanEmiCardResponseDto loanEmiCardResponseDto) {
		LoanEmiGenerationResDto response = new LoanEmiGenerationResDto();
		String rt = "success";

		List<LoanEmiCard> loanEmiCardList = loanEmiCardRepo.getLoanEmiCardDetails(loanId);

		String nextDate1 = loanEmiCardResponseDto.getEmiDueOnDate();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		Date date = null;
		try {

			date = formatter.parse(nextDate1);

		} catch (ParseException e) {
			e.printStackTrace();
			rt = "failure";
		}

		if (loanEmiCardList != null && !loanEmiCardList.isEmpty()) {
			for (LoanEmiCard loan : loanEmiCardList) {

				loan.setEmiDueOn(date);
				loanEmiCardRepo.save(loan);

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Calendar c1 = Calendar.getInstance();
				String dateParts[] = nextDate1.split("-");
				int year = Integer.parseInt(dateParts[0]);

				int month = Integer.parseInt(dateParts[1]);

				int date1 = Integer.parseInt(dateParts[2]);

				c1.set(year, month, date1);

				nextDate1 = sdf.format(c1.getTime());

				try {
					date = sdf.parse(nextDate1);
				} catch (ParseException e) {

					e.printStackTrace();
					rt = "failure";
				}

			}
		} else {
			List<LoanEmiCard> loanEmiCards = new ArrayList<LoanEmiCard>();
			OxyLoan oxyLoan = oxyLoanRepo.findByLoanId("LN" + loanId);
			if (oxyLoan == null) {
				rt = "no loans found";
				response.setReturnMessage(rt);
				return response;
			}
			LoanRequest loanRequest = loanRequestRepo.findByLoanId("LN" + loanId);
			if (loanRequest == null) {
				rt = "no loan requests found";
				response.setReturnMessage(rt);
				return response;
			}

			double emiPrincipalAmount = 0d;
			double emiInterstAmount = (oxyLoan.getDisbursmentAmount() * (oxyLoan.getRateOfInterest() / 12)) / 100d;
			switch (loanRequest.getRepaymentMethod()) {
			case RepaymentMethod.PI:
				emiPrincipalAmount = oxyLoan.getDisbursmentAmount() / oxyLoan.getDuration().doubleValue();
				break;
			default:
				break;
			}

			for (int i = 1; i <= loanRequest.getDuration(); i++) {
				LoanEmiCard loanEmiCard = new LoanEmiCard();
				loanEmiCard.setLoanId(oxyLoan.getId());
				loanEmiCard.setEmiNumber(i);
				loanEmiCard.setEmiAmount(emiPrincipalAmount + emiInterstAmount);
				loanEmiCard.setEmiInterstAmount(emiInterstAmount);
				loanEmiCard.setEmiPrincipalAmount(emiPrincipalAmount);
				loanEmiCard.setEmiDueOn(date);
				if (loanRequest.getDuration().intValue() == i
						&& loanRequest.getRepaymentMethod() == RepaymentMethod.I) {
					loanEmiCard.setEmiPrincipalAmount(oxyLoan.getDisbursmentAmount());
				}
				loanEmiCards.add(loanEmiCard);

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Calendar c1 = Calendar.getInstance();
				String dateParts[] = nextDate1.split("-");
				int year = Integer.parseInt(dateParts[0]);
				int month = Integer.parseInt(dateParts[1]);
				int date1 = Integer.parseInt(dateParts[2]);
				c1.set(year, month, date1);
				nextDate1 = sdf.format(c1.getTime());
				try {
					date = sdf.parse(nextDate1);
				} catch (ParseException e) {

					e.printStackTrace();
					rt = "failure";
				}

			}
			loanEmiCardRepo.saveAll(loanEmiCards);
		}
		response.setReturnMessage(rt);
		return response;
	}

	@Override
	public String makingPenalityValueZero(int loanId, int emiNumber) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public String updatingEmiPaidOnDate(int loanId, int emiNumber, LoanEmiCard loanEmiCard) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public String deleteExtraPaidEmiDetails(int loanId, int emiNumber) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public String calculatingInterestAmount() {
		logger.info("Interest and penality method starts!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		List<Object[]> listOfObjects = loansByApplicationNativeRepo.penalityForApplicationLevel();

		if (listOfObjects != null && !listOfObjects.isEmpty()) {

			Iterator it = listOfObjects.iterator();
			while (it.hasNext()) {
				Object e[] = (Object[]) it.next();

				int borrowerUserId = Integer.parseInt(e[4] == null ? "0" : e[4].toString());

				List<Object[]> objects = oxyLoanRepo.getDueDateForApplication(borrowerUserId);
				double moreThanTwoDays = 1180;
				double lessThanTwoDays = 590;

				int loanId1 = 0;
				if (objects != null && !objects.isEmpty()) {

					Iterator it1 = objects.iterator();
					while (it1.hasNext()) {
						Object e1[] = (Object[]) it1.next();
						String emiDueDate = (e1[1] == null ? "" : e1[1].toString());
						int emiNumber = (Integer.parseInt(e1[0] == null ? "" : e1[0].toString()));
						List<Integer> listOfLoans = oxyLoanRepo.getloansForAnApplication(borrowerUserId);

						for (int i = 0; i < listOfLoans.size(); i++) {
							if (i == 0) {
								int id = listOfLoans.get(i);

								loanId1 = id;
							}
							LoanEmiCard loanEmiCardDetails = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId1,
									emiNumber);
							if (loanEmiCardDetails != null) {
								if (loanEmiCardDetails.getEmiPaidOn() != null) {
									PenaltyDetails penaltyValue1 = penaltyDetailsRepo
											.findByEmiNumberAndBorrowerParentRequestid(emiNumber, borrowerUserId);
									if (penaltyValue1 == null) {
										PenaltyDetails penaltyDetailsToUpdate = new PenaltyDetails();
										penaltyDetailsToUpdate
												.setEmiNumber(Integer.parseInt(e1[0] == null ? "0" : e1[0].toString()));
										penaltyDetailsToUpdate.setPenalty(0.0);
										penaltyDetailsToUpdate.setBorrowerParentRequestid(borrowerUserId);
										penaltyDetailsToUpdate.setPenaltyPaidOn(loanEmiCardDetails.getEmiPaidOn());
										penaltyDetailsRepo.save(penaltyDetailsToUpdate);
									}
								} else {

									PenaltyDetails penaltyValue = penaltyDetailsRepo
											.findByEmiNumberAndBorrowerParentRequestid(emiNumber, borrowerUserId);

									if (penaltyValue == null) {
										PenaltyDetails penaltyDetailsToUpdate = new PenaltyDetails();
										penaltyDetailsToUpdate
												.setEmiNumber(Integer.parseInt(e1[0] == null ? "0" : e1[0].toString()));
										Double penalty = calculatePenalty(emiDueDate, moreThanTwoDays, lessThanTwoDays);
										penaltyDetailsToUpdate.setPenalty(penalty);
										penaltyDetailsToUpdate.setBorrowerParentRequestid(borrowerUserId);
										penaltyDetailsRepo.save(penaltyDetailsToUpdate);
									} else {

										Double penalty = calculatePenalty(emiDueDate, moreThanTwoDays, lessThanTwoDays);
										penaltyValue.setPenalty(penalty);
										penaltyDetailsRepo.save(penaltyValue);
									}

								}
							}

						}
					}
					logger.info("penality method ends!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				}
				List<OxyLoan> oxyLoans = oxyLoanRepo.findByBorrowerParentRequestId(borrowerUserId);
				int numberOfLoans = oxyLoans.size();

				for (OxyLoan loanInfo : oxyLoans) {
					logger.info("Interest method starts!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
					int loanId = loanInfo.getId();
					List<LoanEmiCard> loanEmiCardDetails = loanEmiCardRepo.findByLoanIdOrderByEmiNumber(loanId);
					for (LoanEmiCard loanEmiCard : loanEmiCardDetails) {
						double moreThanFiveDays = 1180 / numberOfLoans;
						double lessThanFiveDays = 590 / numberOfLoans;
						double interestAmount = 0.0;
						int differenceIndays = 0;
						double penality = 0.0;
						if ((loanEmiCard.getStatus().toString()
								.equals(com.oxyloans.entity.loan.LoanEmiCard.Status.INITIATED.toString()))
								|| (loanEmiCard.getStatus().toString()
										.equals(com.oxyloans.entity.loan.LoanEmiCard.Status.INPROCESS.toString()))) {
							if (loanEmiCard.getStatus().toString()
									.equals(com.oxyloans.entity.loan.LoanEmiCard.Status.INITIATED.toString())) {

								LoanEmiCardResponseDto loanEmiCardResponseDto = calculatingInterest(
										loanEmiCard.getEmiAmount(), loanEmiCard.getEmiDueOn().toString(),
										loanInfo.getRateOfInterest(), moreThanFiveDays, lessThanFiveDays);
								interestAmount = loanEmiCardResponseDto.getInterestAmount();
								differenceIndays = loanEmiCardResponseDto.getDifferenceInDays();

							} else {
								int id = loanEmiCard.getId();

								List<Integer> loanEmiCardPaymentDetails = loanEmiCardPaymentDetailsRepo
										.getEmiIdsForPaymentDetails(id);

								for (int i = 0; i < loanEmiCardPaymentDetails.size(); i++) {
									if (i == 0) {
										int paymentDetailsId = loanEmiCardPaymentDetails.get(i);

										LoanEmiCardPaymentDetails paymentDetails = loanEmiCardPaymentDetailsRepo
												.findById(paymentDetailsId).get();

										double calculateInterestAmount = loanEmiCard.getEmiAmount()
												- loanEmiCard.getRemainingEmiAmount();
										String emiDueOnDate = null;
										if (paymentDetails == null) {

											emiDueOnDate = loanEmiCard.getEmiDueOn().toString();
										} else {
											emiDueOnDate = paymentDetails.getAmountPaidDate().toString();
										}

										LoanEmiCardResponseDto loanEmiCardResponseDto = calculatingInterest(
												calculateInterestAmount, emiDueOnDate, loanInfo.getRateOfInterest(),
												moreThanFiveDays, lessThanFiveDays);
										if (loanEmiCardResponseDto != null) {
											interestAmount = loanEmiCard.getEmiLateFeeCharges()
													+ loanEmiCardResponseDto.getInterestAmount();
											differenceIndays = loanEmiCard.getDifferenceInDays()
													+ loanEmiCardResponseDto.getDifferenceInDays();
										}

									}
								}

							}
							if (interestAmount < 0) {
								interestAmount = 0;

							}
						} else {
							interestAmount = 0;
						}
						if (differenceIndays < 0) {
							differenceIndays = 0;
						}

						if (loanEmiCard.getStatus().toString()
								.equals(com.oxyloans.entity.loan.LoanEmiCard.Status.INPROCESS.toString())) {
							loanEmiCard.setEmiLateFeeCharges(
									Math.round(loanEmiCard.getEmiLateFeeCharges() + interestAmount));
							loanEmiCard.setDifferenceInDays(loanEmiCard.getDifferenceInDays() + differenceIndays);
						} else {
							loanEmiCard.setEmiLateFeeCharges(Math.round(interestAmount));
							loanEmiCard.setDifferenceInDays(differenceIndays);
						}
						InterestDetails interestDetailsInfo = interestDetailsRepo
								.findByloanIdAndEmiNumber(loanEmiCard.getLoanId(), loanEmiCard.getEmiNumber());
						if (interestDetailsInfo == null) {

							InterestDetails interestDetails = new InterestDetails();
							LoanEmiCard loanEmiCardInfo = loanEmiCardRepo
									.findByLoanIdAndEmiNumber(loanEmiCard.getLoanId(), loanEmiCard.getEmiNumber());
							if (loanEmiCardInfo.getEmiPaidOn() != null) {
								interestDetails.setInterestPaid(loanEmiCardInfo.getEmiPaidOn());
							}
							interestDetails.setInterest(Math.round(interestAmount));
							interestDetails.setLoanId(loanEmiCard.getLoanId());
							interestDetails.setEmiNumber(loanEmiCard.getEmiNumber());
							interestDetails.setDifferenceInDays(differenceIndays);
							interestDetailsRepo.save(interestDetails);

						} else {
							if (interestAmount > 0) {
								if (loanEmiCard.getStatus().toString()
										.equals(com.oxyloans.entity.loan.LoanEmiCard.Status.INPROCESS.toString())) {
									interestDetailsInfo.setInterest(
											Math.round(interestDetailsInfo.getInterest() + interestAmount));
									interestDetailsInfo.setDifferenceInDays(
											interestDetailsInfo.getDifferenceInDays() + differenceIndays);
								} else {
									interestDetailsInfo.setInterest(Math.round(interestAmount));
									interestDetailsInfo.setDifferenceInDays(differenceIndays);
								}
								interestDetailsRepo.save(interestDetailsInfo);
							}
						}
						double dueAmount = 0.0;
						if (loanEmiCard.getStatus().toString()
								.equals(com.oxyloans.entity.loan.LoanEmiCard.Status.INPROCESS.toString())) {
							double paidAmount = loanEmiCard.getEmiAmount() - loanEmiCard.getRemainingEmiAmount();
							dueAmount = paidAmount + loanEmiCard.getEmiLateFeeCharges() + loanEmiCard.getPenality();

						}
						loanEmiCard.setRemainingPenaltyAndRemainingEmi(Math.round(dueAmount));

						loanEmiCardRepo.save(loanEmiCard);

					}

				}
			}
		}
		logger.info("Interest and penality method starts!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		return "success";

	}

	public LoanEmiCardResponseDto calculatingInterest(double emiAmount, String emiDueOnDate, double rateOfInterest,
			double moreThanFiveDays, double lessThanFiveDays) {
		LoanEmiCardResponseDto loanEmiCardResponseDto = new LoanEmiCardResponseDto();
		String dueOnDate = emiDueOnDate;
		LocalDate from = LocalDate.now();
		LocalDate to = from.parse(dueOnDate);
		long differenceInDays = ChronoUnit.DAYS.between(to, from);
		int days = (int) differenceInDays;

		double interestWithDaysValue = emiAmount * ((rateOfInterest / 100) / 365) * differenceInDays;
		loanEmiCardResponseDto.setInterestAmount(interestWithDaysValue);
		loanEmiCardResponseDto.setDifferenceInDays(days);
		loanEmiCardResponseDto.setEmiDueOn(dueOnDate);

		return loanEmiCardResponseDto;
	}

	public double calculatePenalty(String emiDueDate, double moreThanTwoDays, double lessThanTwoDays) {
		String dueOnDate = emiDueDate;
		LocalDate from = LocalDate.now();
		LocalDate to = from.parse(dueOnDate);
		long differenceInDays = ChronoUnit.DAYS.between(to, from);
		int days = (int) differenceInDays;
		double penalty = 0.0;
		if (days > 2) {
			penalty = moreThanTwoDays;
		}
		if (days >= 1 && days <= 2) {
			penalty = lessThanTwoDays;
		}
		if (days <= 0) {
		}

		return penalty;

	}

	@Override
	public LoanEmiCardResponseDto calculatePendingPenaltyTillDate(int borrowerApplicationId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public List<LoanEmiCardResponseDto> getApplicationLevelPenalityWithEmi(int borrowerApplicationId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public int countValue(int loanId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public OxyDashboardDetailsDto borrowerDashBordDetails(int userId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public KycFileResponse lenderReport(Integer lenderParentRequestId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanEmiCardResponseDto getBorrowerApplicationDetails(int borrowerApplicationId) {
		LoanEmiCardResponseDto loanEmiCardResponseDto = new LoanEmiCardResponseDto();
		Double disbursedAmount = oxyLoanRepo.getDisburmentAmount(borrowerApplicationId);
		loanEmiCardResponseDto.setApplicationLevelDisbursedAmount(disbursedAmount);
		loanEmiCardResponseDto.setLenderDetails(getBorrowerApplicationLevelDetails(borrowerApplicationId));
		loanEmiCardResponseDto.setPaymentHistory(getPartPayment(borrowerApplicationId));
		return loanEmiCardResponseDto;

	}

	public List<LoanEmiCardResponseDto> getBorrowerApplicationLevelDetails(int borrowerApplicationId) {
		List<Object[]> objects = oxyLoanRepo.getLendersForApplication(borrowerApplicationId);

		List<LoanEmiCardResponseDto> loanEmiCardApplicationLevel = new ArrayList<LoanEmiCardResponseDto>();

		if (objects != null && !objects.isEmpty()) {
			Iterator it = objects.iterator();
			while (it.hasNext()) {
				Object e[] = (Object[]) it.next();
				LoanEmiCardResponseDto loanEmiCardResponseDto = new LoanEmiCardResponseDto();
				loanEmiCardResponseDto.setLenderId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
				String firstName = e[1] == null ? "" : e[1].toString();
				String fN = firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
				String lastName = e[2] == null ? "" : e[2].toString();
				String lN = lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
				int loanId = Integer.parseInt(e[3] == null ? "0" : e[3].toString());

				loanEmiCardResponseDto.setLenderName(fN + " " + lN);

				loanEmiCardApplicationLevel.add(loanEmiCardResponseDto);

			}
		}
		return loanEmiCardApplicationLevel;

	}

	public List<LoanEmiCardResponseDto> getPartPayment(int borrowerApplicationId) {
		List<LoanEmiCardResponseDto> loanEmiCardApplicationLevel = new ArrayList<LoanEmiCardResponseDto>();

		int count = 0;

		List<Integer> listOfLoanIds = oxyLoanRepo.getloansForAnApplication(borrowerApplicationId);
		String emiPaidOnDate = null;
		for (int i1 = 0; i1 < listOfLoanIds.size(); i1++) {

			int loanId = listOfLoanIds.get(i1);
			OxyLoan oxy = oxyLoanRepo.findById(loanId).get();
			List<LoanEmiCard> loanEmiCard = loanEmiCardRepo.findByLoanIdOrderByEmiNumber(loanId);

			for (LoanEmiCard loanInfo : loanEmiCard) {
				int emiId = loanInfo.getId();

				if (count < oxy.getDuration()) {
					count = count + 1;
					LoanEmiCardResponseDto loanEmiCardResponseDto = new LoanEmiCardResponseDto();

					loanEmiCardResponseDto.setEmiNumber(loanInfo.getEmiNumber());
					double emiAmount = oxyLoanRepo.getEmiAmount(borrowerApplicationId, loanInfo.getEmiNumber());
					loanEmiCardResponseDto.setEmiAmount(emiAmount);
					List<Integer> loansInfo = oxyLoanRepo.getloansForAnApplication(borrowerApplicationId);
					double interestAmount = 0.0;
					if (loansInfo != null) {
						for (int i = 0; i < loansInfo.size(); i++) {
							int loanId1 = loansInfo.get(i);
							InterestDetails interestDetails = interestDetailsRepo.findByloanIdAndEmiNumber(loanId1,
									loanInfo.getEmiNumber());
							interestAmount = interestAmount + interestDetails.getInterest();
						}
					}
					loanEmiCardResponseDto.setInterestAmount(interestAmount);
					loanEmiCardResponseDto.setEmiDueOnDate(expectedDateFormat.format(loanInfo.getEmiDueOn()));
					loanEmiCardResponseDto.setApplicationId(borrowerApplicationId);
					loanEmiCardResponseDto.setPartPayment(partPayment(emiId, borrowerApplicationId));
					if (loanInfo.getEmiPaidOn() != null) {
						loanEmiCardResponseDto.setEmiPaidOn(expectedDateFormat.format(loanInfo.getEmiPaidOn()));
					}
					if (loanInfo.getModeOfPayment() != null) {
						loanEmiCardResponseDto.setModeOfPayment(loanInfo.getModeOfPayment());
					}
					if (loanInfo.getTransactionNumber() != null) {
						loanEmiCardResponseDto.setPayuTransactionNumber(loanInfo.getTransactionNumber());
					}
					PenaltyDetails penaltyDetails = penaltyDetailsRepo
							.findByEmiNumberAndBorrowerParentRequestid(loanInfo.getEmiNumber(), borrowerApplicationId);
					if (penaltyDetails.getPenaltyPaidOn() == null) {
						List<Integer> listLoansIds = oxyLoanRepo.getloansForAnApplication(borrowerApplicationId);

						double dueAmountForEmiAndInterest = 0.0;
						for (int i = 0; i < listLoansIds.size(); i++) {
							int loanId1 = listLoansIds.get(i);

							LoanEmiCard loanEmiCard1 = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId1,
									loanInfo.getEmiNumber());
							if (loanEmiCard1.getEmiPaidOn() == null) {
								if (loanEmiCard1.getStatus() == com.oxyloans.entity.loan.LoanEmiCard.Status.COMPLETED) {
									dueAmountForEmiAndInterest = dueAmountForEmiAndInterest
											+ loanEmiCard1.getEmiLateFeeCharges();

								} else {
									if (loanEmiCard1
											.getStatus() == com.oxyloans.entity.loan.LoanEmiCard.Status.INITIATED) {
										dueAmountForEmiAndInterest = dueAmountForEmiAndInterest
												+ loanEmiCard1.getEmiLateFeeCharges() + loanEmiCard1.getEmiAmount();

									} else {
										dueAmountForEmiAndInterest = dueAmountForEmiAndInterest
												+ loanEmiCard1.getEmiLateFeeCharges()
												+ loanEmiCard1.getRemainingEmiAmount();

									}
								}

							}

						}
						if (penaltyDetails.getPenaltyPaidOn() == null && penaltyDetails.getPenalty() > 0) {
							if (penaltyDetails.getPenaltyDue() > 0) {
								dueAmountForEmiAndInterest = dueAmountForEmiAndInterest
										+ penaltyDetails.getPenaltyDue();
							} else {
								dueAmountForEmiAndInterest = dueAmountForEmiAndInterest + penaltyDetails.getPenalty();
							}
						}
						loanEmiCardResponseDto.setDueAmountWithPenality(dueAmountForEmiAndInterest);

					} else {
						loanEmiCardResponseDto.setDueAmountWithPenality(0);
					}
					if (penaltyDetails != null) {
						if (penaltyDetails.getPenaltyPaidOn() == null) {
							loanEmiCardResponseDto.setPenalty(penaltyDetails.getPenalty());
						} else {
							loanEmiCardResponseDto.setPenalty(penaltyDetails.getPenaltyPaid());
						}
					}

					loanEmiCardApplicationLevel.add(loanEmiCardResponseDto);
				}
			}
		}
		return loanEmiCardApplicationLevel;

	}

	public List<LoanEmiCardResponseDto> partPayment(int emiId, int borrowerApplicationId) {
		List<LoanEmiCardResponseDto> loanEmiCardApplicationLevel = new ArrayList<LoanEmiCardResponseDto>();
		List<Integer> loanEmiCardPaymentDetails = loanEmiCardPaymentDetailsRepo.getEmiIdsForPaymentDetails(emiId);
		if (loanEmiCardPaymentDetails != null && !loanEmiCardPaymentDetails.isEmpty()) {

			for (int i = 0; i < loanEmiCardPaymentDetails.size(); i++) {
				LoanEmiCardResponseDto loanEmiCardResponseDto = new LoanEmiCardResponseDto();
				int id = loanEmiCardPaymentDetails.get(i);
				LoanEmiCardPaymentDetails loanEmiCardPayment = loanEmiCardPaymentDetailsRepo.findById(id).get();
				loanEmiCardResponseDto.setModeOfPayment(loanEmiCardPayment.getModeOfPayment());
				loanEmiCardResponseDto.setPayuTransactionNumber(loanEmiCardPayment.getTransactionRefereceNumber());
				loanEmiCardResponseDto.setPayuStatus(loanEmiCardPayment.getPaymentStatus().toString());
				Date paidDate = loanEmiCardPayment.getAmountPaidDate();
				loanEmiCardResponseDto
						.setPartPaymentDate(expectedDateFormat.format(loanEmiCardPayment.getAmountPaidDate()));
				List<Integer> listOfLoanIds = oxyLoanRepo.getloansForAnApplication(borrowerApplicationId);
				double amountPaid = 0.0;
				for (int i1 = 0; i1 < listOfLoanIds.size(); i1++) {
					int loanId = listOfLoanIds.get(i1);
					List<LoanEmiCard> loanEmiCard = loanEmiCardRepo.findByLoanIdOrderByEmiNumber(loanId);
					for (LoanEmiCard loanInfo : loanEmiCard) {
						int id1 = loanInfo.getId();

						List<LoanEmiCardPaymentDetails> loanEmiCardPaymentDetails1 = loanEmiCardPaymentDetailsRepo
								.findByEmiIdAndAmountPaidDate(id1, paidDate);
						if (loanEmiCardPaymentDetails1 != null && !loanEmiCardPaymentDetails1.isEmpty()) {
							for (LoanEmiCardPaymentDetails paymentDetails : loanEmiCardPaymentDetails1)
								amountPaid = amountPaid + paymentDetails.getPartialPaymentAmount();
						}
					}
				}

				loanEmiCardResponseDto.setApplicationLevelPartPaid(amountPaid);
				loanEmiCardApplicationLevel.add(loanEmiCardResponseDto);
			}
		}
		return loanEmiCardApplicationLevel;

	}

	@Override
	public List<BorrowerApplicationSummryDto> borrowerApplictionSummary(int userId) {
		List<LoanRequest> loanRequestsList = loanRequestRepo.findByUserId(userId);
		User user = userRepo.findById(userId)
				.orElseThrow(() -> new ActionNotAllowedException("User Not Found", ErrorCodes.ENITITY_NOT_FOUND));
		if (loanRequestsList.isEmpty()) {
			throw new ActionNotAllowedException("User don't request and offers", ErrorCodes.INVALID_OPERATION);
		}
		BorrowerApplicationSummryDto borrowerApplicationSummryDto = new BorrowerApplicationSummryDto();
		List<BorrowerApplicationSummryDto> applicationSummryDtos = loanRequestsList.stream().filter(
				loanrequest -> loanrequest.getParentRequestId() == null || loanrequest.getParentRequestId() == 0)
				.map(loanrequest -> {
					LoanOfferdAmount loanOfferdAmount = loanrequest.getLoanOfferedAmount();
					Double disburmentAmount = 0.0;
					if (loanOfferdAmount == null) {
						disburmentAmount = oxyLoanRepo.getDisbursmentAmountForOldUsers(userId);
						if (disburmentAmount == null) {
							throw new OperationNotAllowedException("user is not haveing any disburment amount",
									ErrorCodes.ENITITY_NOT_FOUND);

						} else {

							String applicationId = loanrequest.getLoanRequestId();
							String[] ids = applicationId.split("APBR");
							int id = Integer.parseInt(ids[1]);
							borrowerApplicationSummryDto.setId(id);
							borrowerApplicationSummryDto.setApplicationId(loanrequest.getLoanRequestId());
							borrowerApplicationSummryDto.setLoanreRequestAmount(loanrequest.getLoanRequestAmount());
							borrowerApplicationSummryDto.setLoanOfferAmount(disburmentAmount);
							borrowerApplicationSummryDto.setUserId(user.getId());
							return borrowerApplicationSummryDto;
						}

					}
					borrowerApplicationSummryDto.setId(loanrequest.getId());
					borrowerApplicationSummryDto.setApplicationId(loanrequest.getLoanRequestId());
					borrowerApplicationSummryDto.setLoanreRequestAmount(loanrequest.getLoanRequestAmount());
					borrowerApplicationSummryDto.setOfferStatus(loanOfferdAmount.getLoanOfferdStatus().toString());
					borrowerApplicationSummryDto.setLoanOfferAmount(loanOfferdAmount.getLoanOfferedAmount());
					borrowerApplicationSummryDto.setUserId(user.getId());
					borrowerApplicationSummryDto
							.setOfferSentDate(expectedDateFormat.format(loanOfferdAmount.getOfferSentOn()));
					borrowerApplicationSummryDto
							.setOfferAccpetedDate(expectedDateFormat.format(loanOfferdAmount.getAccepetedOn()));
					borrowerApplicationSummryDto.setRateOfInterest(loanOfferdAmount.getRateOfInterest());
					return borrowerApplicationSummryDto;
				}).collect(Collectors.toList());

		return applicationSummryDtos;
	}

	@Override
	public ApplicationResponseDto emiDetails(int parentRequestID) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public String sendingNotificationToLender() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public KycFileResponse getDataFromExcel() {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);

	}

	@Override
	public LoanEmiCardResponseDto getInterestPendingTillDate(int loanId) {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanEmiCardResponseDto interestWaiveOff(int loanId, LoanEmiCardResponseDto loanEmiCardResponseDto) {
		throw new OperationNotAllowedException("As asn Admin you can not perform this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	@Transactional
	public LoanResponseDto getBorrowerStatements(int id, String type) throws PdfGeenrationException, IOException {
		logger.info("loan statement Method started!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		User user = userRepo.findById(id).get();

		TemplateContext templateContext = new TemplateContext();
		templateContext.put("name",
				user.getPersonalDetails().getFirstName() + " " + user.getPersonalDetails().getLastName());
		templateContext.put("city", user.getCity());
		templateContext.put("state", user.getState());
		templateContext.put("pincode", user.getPinCode());
		templateContext.put("mobileNumber", user.getMobileNumber());
		templateContext.put("email", user.getEmail());
		templateContext.put("borrowerUniqueNumber", user.getUniqueNumber());

		List<StatementPdfResponseDto> statementPdfResponseDto = getOxyLoanDetails(id, type);

		if (statementPdfResponseDto.isEmpty()) {
			throw new OperationNotAllowedException("user details not present", ErrorCodes.USER_NOT_FOUND);
		}

		String disburmentDetailsPdf = "";
		String loanEmiCardPdf = "";
		String emiStartAndEndDates = "";
		String emiCardFullTable = "";

		for (StatementPdfResponseDto responseDto : statementPdfResponseDto) {

			disburmentDetailsPdf = disburmentDetailsPdf + responseDto.getDisburmentAmountDetails();
			loanEmiCardPdf = loanEmiCardPdf + responseDto.getLoanEmiCardDetails();
			emiStartAndEndDates = emiStartAndEndDates + responseDto.getEmiStartDateAndEmiEndDate();

			emiCardFullTable = responseDto.getEmiTable();

		}

		templateContext.put("emiStartAndEndDates", emiStartAndEndDates); // ThirdTable in borrower Statement
		templateContext.put("disburmentAmount", disburmentDetailsPdf); // firstTable in borrower Statement
		templateContext.put("loanEmiCard", loanEmiCardPdf); // SecondTable in borrower statement
		templateContext.put("emiTable", emiCardFullTable);

		String outputFileName = id + ".pdf";
		// String outputFileName = "C:/data/borrowerStructure.pdf";
		String generatePdf = pdfEngine.generatePdf("agreement/statement.xml", templateContext, outputFileName);

		FileRequest fileRequest = new FileRequest();
		fileRequest.setInputStream(new FileInputStream(generatePdf));
		fileRequest.setFileName(outputFileName);
		fileRequest.setFileType(FileType.Statement);
		fileRequest.setFilePrifix(FileType.Statement.name());
		FileResponse file1 = fileManagementService.getFile(fileRequest);
		try {
			FileResponse putFile = fileManagementService.putFile(fileRequest);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		File file = new File(generatePdf);
		boolean fileDeleted = file.delete();

		LoanResponseDto loanResponse = new LoanResponseDto();

		loanResponse.setDownloadUrl(file1.getDownloadUrl());
		loanResponse.setFileName(outputFileName);
		logger.info("loan statement Method ends!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		return loanResponse;

	}

	@Override
	public BorrowerDetailsRequestDto getBorrowerComments(int userId) {
		LoanRequest loanRequst = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
		BorrowerDetailsRequestDto borrowerDetailsRequestDto = new BorrowerDetailsRequestDto();
		if (loanRequst != null) {
			if (loanRequst.getComments() != null) {
				String comments = loanRequst.getComments();
				int intIndex = comments.indexOf("locationResidence");
				if (intIndex == -1) {
					borrowerDetailsRequestDto = null;
				} else {

					Gson gson = new Gson();

					BorrowerDetailsRequestDto borrowerdetails = gson.fromJson(comments,
							BorrowerDetailsRequestDto.class);

					if (borrowerdetails.getLocation() != null) {
						borrowerDetailsRequestDto.setLocation(borrowerdetails.getLocation());
					}
					if (borrowerdetails.getLocationResidence() != null) {
						borrowerDetailsRequestDto.setLocationResidence(borrowerdetails.getLocationResidence());
					}
					if (borrowerdetails.getCompanyName() != null) {
						borrowerDetailsRequestDto.setCompanyName(borrowerdetails.getCompanyName());
					}
					if (borrowerdetails.getCompanyResidence() != null) {
						borrowerDetailsRequestDto.setCompanyResidence(borrowerdetails.getCompanyResidence());
					}
					if (borrowerdetails.getRole() != null) {
						borrowerDetailsRequestDto.setRole(borrowerdetails.getRole());
					}
					if (borrowerdetails.getLoanRequirement() != null) {
						borrowerDetailsRequestDto.setLoanRequirement(borrowerdetails.getLoanRequirement());
					}
					if (borrowerdetails.getEmi() != null) {
						borrowerDetailsRequestDto.setEmi(borrowerdetails.getEmi());
					}
					if (borrowerdetails.getSalary() != null) {
						borrowerDetailsRequestDto.setSalary(borrowerdetails.getSalary());
					}
					if (borrowerdetails.getEligibility() != null) {
						borrowerDetailsRequestDto.setEligibility(borrowerdetails.getEligibility());
					}
					if (borrowerdetails.getCibilPassword() != null) {
						borrowerDetailsRequestDto.setCibilPassword(borrowerdetails.getCibilPassword());
					}
					if (borrowerdetails.getAadharPassword() != null) {
						borrowerDetailsRequestDto.setAadharPassword(borrowerdetails.getAadharPassword());
					}
					if (borrowerdetails.getPanPassword() != null) {
						borrowerDetailsRequestDto.setPanPassword(borrowerdetails.getPanPassword());
					}
					if (borrowerdetails.getBankPassword() != null) {
						borrowerDetailsRequestDto.setBankPassword(borrowerdetails.getBankPassword());
					}
					if (borrowerdetails.getPayslipsPassword() != null) {
						borrowerDetailsRequestDto.setPayslipsPassword(borrowerdetails.getPayslipsPassword());
					}

					if (borrowerdetails.getComments() != null) {
						borrowerDetailsRequestDto.setComments(borrowerdetails.getComments());
					}
				}

			}

		}
		return borrowerDetailsRequestDto;
	}

	@Override
	public String enachSchedulingToOldBorrowers(int loanId) {
		String message = null;
		OxyLoan oxyloan = oxyLoanRepo.findById(loanId).get();
		if (oxyloan != null) {
			if (oxyloan.getEnachMandate() != null) {
				throw new OperationNotAllowedException("Enach is already scheduled for this loanId",
						ErrorCodes.ENTITY_ALREDY_EXISTS);
			} else {

				generateENACHMandate(oxyloan);
				message = "success";
			}

		}
		return message;

	}

	@Override
	public String updateingEnachBasedOnEnachResponce() {
		String message = null;
		logger.info("--------------------- updateingEnachBasedOnEnachResponce method start ...............");
		List<Object[]> listOfEnachSuccessUsers = emandateResponseRepo.getEnachSuccessUsersData();
		if (listOfEnachSuccessUsers != null) {
			Iterator it = listOfEnachSuccessUsers.iterator();
			while (it.hasNext()) {
				Object[] e = (Object[]) it.next();
				int loanId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
				int emiNumber = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
				String date = (e[2] == null ? " " : e[2].toString());
				Timestamp paidDate = Timestamp.valueOf(date);
				LoanEmiCard loanEmiCard = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId, emiNumber);
				if (loanEmiCard != null) {
					loanEmiCard.setEmiPaidOn(paidDate);
					loanEmiCard.setModeOfPayment("ENACH");
					loanEmiCard.setRemainingEmiAmount(loanEmiCard.getEmiAmount());
					loanEmiCard.setStatus(com.oxyloans.entity.loan.LoanEmiCard.Status.COMPLETED);
					loanEmiCard.setEmiLateFeeCharges(0.0);
					loanEmiCard.setDifferenceInDays(0);
					loanEmiCardRepo.save(loanEmiCard);
					InterestDetails interestDetails = interestDetailsRepo.findByloanIdAndEmiNumber(loanId, emiNumber);
					if (interestDetails != null) {
						interestDetails.setInterest(0.0);
						interestDetails.setDifferenceInDays(0);
						interestDetails.setInterestPaid(paidDate);
						interestDetailsRepo.save(interestDetails);
					}
				}
				message = "SUCCESSFULLY UPDATED";
			}
		}
		logger.info("--------------------- updateingEnachBasedOnEnachResponce method end ...............");
		return message;

	}

	@Override
	public CommentsResponseDto uploadCommentsFromRadhaSir(int id, CommentsRequestDto commentsRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public PaymentSearchByStatus getBorrowerPaymentList(String borrowerUniqueNumber, String paymentStatus) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BorrowersFeeBasedOnDate getBorrowersFreeBasedOnGivenDate(int month) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public String calculatingLoanLevelFeeForOldBorrower(int applicationId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public EnachScheduledMailResponseDto sendEnachScheduleEmailExcel() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public EnachTransactionResponseDto enachSuccessVerificationList() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public PaymentHistoryResponseDto getEcsAndNonEcsPaymentHistory(String monthName, String year) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderEscrowWalletDto getLenderScrowWalletDetails(String date1, String date2) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public DurationChangeDto incresingDurationToExistingBorrower(int userId) {
		logger.info("incresingDurationToExistingBorrower method start");
		DurationChangeDto durationChangeDto = new DurationChangeDto();
		int duration = 9;

		List<Integer> listOfLoanIds = oxyLoanRepo.getLoanIdsMappedToLenderId(userId);
		for (int i1 = 0; i1 < listOfLoanIds.size(); i1++) {
			int loanId = listOfLoanIds.get(i1);
			OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get();

			if (oxyLoan != null) {
				oxyLoan.setNewDuration(duration);
				oxyLoan.setRepaymentMethod(RepaymentMethod.I);
				oxyLoanRepo.save(oxyLoan);
				LoanRequest loanRequest = loanRequestRepo.findByLoanId("LN" + loanId);
				if (loanRequest != null) {
					loanRequest.setRepaymentMethod(RepaymentMethod.I);
					loanRequestRepo.save(loanRequest);
				}

				double emiPrincipalAmount = 0d;
				double emiInterstAmount = (oxyLoan.getDisbursmentAmount() * (oxyLoan.getRateOfInterest() / 12)) / 100d;
				switch (loanRequest.getRepaymentMethod()) {
				case RepaymentMethod.PI:
					emiPrincipalAmount = oxyLoan.getDisbursmentAmount() / oxyLoan.getNewDuration().doubleValue();
					break;
				default:
					break;
				}
				String nextDate1 = loanEmiCardRepo.getFirstEmiDate(loanId);
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

				Date date = null;
				try {

					date = formatter.parse(nextDate1);

				} catch (ParseException e) {
					e.printStackTrace();

				}

				for (int i = 1; i <= 9; i++) {
					int emiNumber = i;
					LoanEmiCard loanEmiCard = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId, emiNumber);
					if (loanEmiCard != null) {
						loanEmiCard.setLoanId(oxyLoan.getId());
						loanEmiCard.setEmiNumber(i);
						if (emiNumber == 1) {
							loanEmiCard.setEmiInterstAmount(loanEmiCard.getEmiInterstAmount());
						} else {
							loanEmiCard.setEmiInterstAmount(emiInterstAmount);
						}
						loanEmiCard.setEmiPrincipalAmount(0d);
						loanEmiCard.setEmiDueOn(date);
						if (loanRequest.getRepaymentMethod() == RepaymentMethod.I && loanEmiCard.getEmiNumber() == 9) {
							loanEmiCard.setEmiPrincipalAmount(oxyLoan.getDisbursmentAmount());
							loanEmiCard.setEmiAmount(oxyLoan.getDisbursmentAmount() + emiInterstAmount);
						} else {
							loanEmiCard.setEmiAmount(emiPrincipalAmount + loanEmiCard.getEmiInterstAmount());
						}
						loanEmiCardRepo.save(loanEmiCard);
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						Calendar c1 = Calendar.getInstance();
						String dateParts[] = nextDate1.split("-");
						int year = Integer.parseInt(dateParts[0]);

						int month = Integer.parseInt(dateParts[1]);

						int date1 = Integer.parseInt(dateParts[2]);

						c1.set(year, month, date1);

						nextDate1 = sdf.format(c1.getTime());

						try {
							date = sdf.parse(nextDate1);
						} catch (ParseException e) {

							e.printStackTrace();

						}

					} else {
						LoanEmiCard loanEmiCardDetails = new LoanEmiCard();
						loanEmiCardDetails.setLoanId(oxyLoan.getId());
						loanEmiCardDetails.setEmiNumber(i);
						loanEmiCardDetails.setEmiInterstAmount(emiInterstAmount);
						loanEmiCardDetails.setEmiPrincipalAmount(0d);
						loanEmiCardDetails.setEmiDueOn(date);
						if (loanRequest.getRepaymentMethod() == RepaymentMethod.I
								&& loanEmiCardDetails.getEmiNumber() == 9) {
							loanEmiCardDetails.setEmiPrincipalAmount(oxyLoan.getDisbursmentAmount());
							loanEmiCardDetails.setEmiAmount(oxyLoan.getDisbursmentAmount() + emiInterstAmount);
						} else {
							loanEmiCardDetails.setEmiAmount(emiPrincipalAmount + emiInterstAmount);
						}

						loanEmiCardRepo.save(loanEmiCardDetails);
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						Calendar c1 = Calendar.getInstance();
						String dateParts[] = nextDate1.split("-");
						int year = Integer.parseInt(dateParts[0]);

						int month = Integer.parseInt(dateParts[1]);

						int date1 = Integer.parseInt(dateParts[2]);

						c1.set(year, month, date1);

						nextDate1 = sdf.format(c1.getTime());

						try {
							date = sdf.parse(nextDate1);
						} catch (ParseException e) {

							e.printStackTrace();

						}

					}
				}

			}

		}
		durationChangeDto.setStatus("success");
		logger.info("incresingDurationToExistingBorrower method end");
		return durationChangeDto;

	}

	@Override
	public ActiveLenderResponseDto getAllLenderWalletInfo(PaginationRequestDto paginationRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderLoansInformation gettingLenderLoanInformation(Integer currentMonth, Integer year) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LendersLoanInfo gettingLendersLoanInfoBasedOnMonthAndYear(Integer currentMonth, Integer year,
			PaginationRequestDto paginationRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public List<LenderLoansInformation> getLendersDetailedLoanInfo(Integer lenderId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LendersLoanInfo getLenderInfoBasedOnId(Integer lenderId, PaginationRequestDto paginationRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	public CommentsRequestDto getCommentAndRateOfInterestFromRadhaSir(int userId) {
		LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(userId);
		CommentsRequestDto commentsRequestDto = new CommentsRequestDto();
		if (loanRequest != null && loanRequest.getRateOfInterestToBorrower() > 0) {
			commentsRequestDto.setLoanRequestId(loanRequest.getId());
			commentsRequestDto.setComments(loanRequest.getCommentsBySir());
			commentsRequestDto.setRating(loanRequest.getRatingBySir());
			commentsRequestDto.setRateOfInterestToBorrower(loanRequest.getRateOfInterestToBorrower());
			commentsRequestDto.setRateOfInterestToLender(loanRequest.getRateOfInterestToLender());
			commentsRequestDto.setDurationToTheApplication(loanRequest.getDurationBySir());
			commentsRequestDto.setDurationType(loanRequest.getDurationType().toString());
			commentsRequestDto.setRepaymentMethodForBorrower(loanRequest.getRepaymentMethodForBorrower());
			commentsRequestDto.setRepaymentMethodForLender(loanRequest.getRepaymentMethodForLender());
		}
		return commentsRequestDto;
	}

	@Override
	public com.oxyloans.response.user.StatusResponseDto rejectingLoanByLoanId(int loanId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderNocResponseDto getLendersInformation(int userId, LenderIncomeRequestDto lenderIncomeRequestDto)
			throws FileNotFoundException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderNocResponseDto getLenderProfitValue(int userId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderNocResponseDto generateLenderProfitPdf(int userId) throws FileNotFoundException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanDisbursmentResponse updateingDisbursedDateBasedOnExcelSheet(String excelUploadedDate)
			throws PdfGeenrationException, IOException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public DisbursmentPendingResponseDto getDisbursmentPendingLoansDetails(PaginationRequestDto paginationRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderReferenceResponse displayingAllLenderReferenceDetailsInfo(PaginationRequestDto pageRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanDisbursmentResponse approvingAllLoansInApplicationLevel(int borrowerId, UserRequest userRequest)
			throws PdfGeenrationException, IOException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	public void generateEmiCardForDays(OxyLoan oxyLoan, LoanRequest loanRequest) {

		List<LoanEmiCard> loanEmiCards = new ArrayList<LoanEmiCard>();
		Calendar calendar = Calendar.getInstance();

		for (int i = 1; i <= oxyLoan.getDuration(); i++) {
			double emiInterstAmount = 0d;
			double interstAmountForLender = 0.0;
			double principalAmountForLender = 0d;
			double emiAmountForLender = 0.0;

			LoanEmiCard loanEmiCard = new LoanEmiCard();
			loanEmiCard.setEmiNumber(i);
			loanEmiCard.setLoanId(oxyLoan.getId());
			double emiPrincipalAmount = 0d;
			double calculatedRateOfinterest = interestAmountPerDay;

			Double interestAmount = (double) Math.round(calculatedRateOfinterest);
			if (i == oxyLoan.getDuration()) {
				emiPrincipalAmount = oxyLoan.getDisbursmentAmount();
			}

			loanEmiCard.setEmiPrincipalAmount(emiPrincipalAmount);
			loanEmiCard.setEmiInterstAmount(interestAmount);
			loanEmiCard.setEmiAmount(emiPrincipalAmount + interestAmount);

			calendar.setTime(new Date());
			calendar.add(Calendar.DATE, i);
			loanEmiCard.setEmiDueOn(calendar.getTime());

			int borrowerParentid = oxyLoan.getBorrowerParentRequestId();
			LoanRequest loanRequestDetails = loanRequestRepo.findById(borrowerParentid).get();
			if (loanRequestDetails != null) {

				if (loanRequestDetails.getRepaymentMethodForLender().equalsIgnoreCase("PI")) {
					interstAmountForLender = Math.round((oxyLoan.getDisbursmentAmount()
							* ((loanRequestDetails.getRateOfInterestToLender() / 12) / 100d)) / 30);

					principalAmountForLender = oxyLoan.getDisbursmentAmount() / oxyLoan.getDuration().doubleValue();
					emiAmountForLender = interstAmountForLender + principalAmountForLender;
				} else {
					if (loanRequestDetails.getRepaymentMethodForLender().equalsIgnoreCase("I")) {

						interstAmountForLender = Math.round((oxyLoan.getDisbursmentAmount()
								* ((loanRequestDetails.getRateOfInterestToLender() / 12) / 100d)) / 30);

						if (i == loanRequest.getDuration()) {
							principalAmountForLender = oxyLoan.getDisbursmentAmount();

						}

						emiAmountForLender = interstAmountForLender + principalAmountForLender;

					}

				}
			}
			loanEmiCard.setLenderEmiPrincipalAmount(principalAmountForLender);
			loanEmiCard.setLenderEmiAmount(emiAmountForLender);
			loanEmiCard.setLenderEmiInterestAmount(interstAmountForLender);
			loanEmiCards.add(loanEmiCard);

		}

		loanEmiCardRepo.saveAll(loanEmiCards);
	}

	@Override
	public UserInformationResponseDto searchByLoanIdDisbursmentPending(String loanId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public DisbursmentPendingResponseDto searchByLenderIdAndBorrowerId(int userId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public String sendingMailsToNormalEmisPendingFromAdmin() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public String sendingMailsToCriticalEmisPending() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public String sendingMailsToNormalEmisPendingFromCRM() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanDisbursmentResponse settingDisbursmentDatesForAllLoans(int userId, UserRequest useerRequest)
			throws PdfGeenrationException, IOException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BorrowerIndividualLoansCountResposeDto loansForBorrowerApplication(int borrowerId) {

		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BorrowersLoanOwnerInformation getLoanOwners() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BorrowersLoanOwnerInformation gettingBorrowerDetailedLoanInfo(String loanOwner) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public OxyTransactionDetailsFromExcelSheetsResponse readingLendersOutGoingAmount(String date, String deditedType) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public OxyTransactionDetailsFromExcelSheetsResponse updatingExcelSheet(
			OxyTransactionDetailsFromExcelSheetsRequest oxyTransactionDetailsFromExcelSheetsRequest) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderReturnsResponseDto getLenderNewDashBoard(int userId) {
		LenderReturnsResponseDto lenderReturnsResponseDto = new LenderReturnsResponseDto();
		try {
			Double credited = 0.0;
			Double totalPricipalOnApril = 0.0;
			Double sumOfInterestAmount = 0.0;
			Double totalPricipalInLending = 0.0;
			Double sumOfPricipalAmount = 0.0;
			Double sumOfWithDraw = 0.0;
			Double creditedAmount = lenderOxyWalletNativeRepo.lenderWalletCreditAmount(userId);
			if (creditedAmount != null) {
				credited = creditedAmount;

			}
			Double walletAmount = lenderOxyWalletNativeRepo.getWalletAmountOnApril(userId);
			if (walletAmount != null) {
				totalPricipalOnApril = walletAmount;
			}
			Double interestAmount = lendersReturnsRepo.getSumOfInterestAmount(userId);
			if (interestAmount != null) {
				sumOfInterestAmount = interestAmount;
			}
			Double pricipalAmount = lendersReturnsRepo.getSumOfPrincipalAmount(userId);
			if (pricipalAmount != null) {
				sumOfPricipalAmount = pricipalAmount;
			}
			Double withDrawAmount = lendersReturnsRepo.getSumOfWithDraw(userId);
			if (withDrawAmount != null) {
				sumOfWithDraw = withDrawAmount;
			}
			totalPricipalInLending = credited - (sumOfPricipalAmount + sumOfWithDraw);
			List<Object[]> listOfCreditedDetails = lenderOxyWalletNativeRepo.getLenderCreditedAmountDetails(userId);
			List<LenderCreditedDetails> listOfCreditedLenders = new ArrayList<LenderCreditedDetails>();
			if (listOfCreditedDetails != null && !listOfCreditedDetails.isEmpty()) {
				Iterator it = listOfCreditedDetails.iterator();
				int count = 0;
				while (it.hasNext()) {
					Object[] e = (Object[]) it.next();
					LenderCreditedDetails lenderCredited = new LenderCreditedDetails();
					lenderCredited.setAmountCredited(Double.parseDouble(e[0] == null ? "0" : e[0].toString()));
					lenderCredited.setPaidDate(e[1] == null ? " " : e[1].toString());
					count = count + 1;
					lenderCredited.setSno(count);
					listOfCreditedLenders.add(lenderCredited);
				}
			}
			lenderReturnsResponseDto.setLenderPricipalAmountThroughWalletTransaction(listOfCreditedLenders);
			List<LenderIndividualReturns> listOfLenderInterestEarned = new ArrayList<LenderIndividualReturns>();
			List<LenderIndividualReturns> listOfLenderPricipalEarned = new ArrayList<LenderIndividualReturns>();
			List<LenderIndividualReturns> listOfLenderEmiEarned = new ArrayList<LenderIndividualReturns>();
			List<LenderIndividualReturns> listOfLenderWithDraw = new ArrayList<LenderIndividualReturns>();
			List<LenderIndividualReturns> listOfLenderReLend = new ArrayList<LenderIndividualReturns>();
			List<LenderIndividualReturns> listOfLenderPoolingInterest = new ArrayList<LenderIndividualReturns>();
			HashMap<Integer, String> hash_map = new LinkedHashMap<Integer, String>();

			hash_map.put(1, "LENDERINTEREST");
			hash_map.put(2, "LENDERPRINCIPAL");
			hash_map.put(3, "LENDEREMI");
			hash_map.put(4, "LENDERWITHDRAW");
			hash_map.put(5, "LENDERRELEND");
			hash_map.put(6, "LENDERPOOLINGINTEREST");

			Iterator<Entry<Integer, String>> it = hash_map.entrySet().iterator();

			while (it.hasNext()) {
				Map.Entry<Integer, String> set = (Map.Entry<Integer, String>) it.next();

				String amountType = set.getValue();
				Integer key = set.getKey();
				List<Object[]> lenderInterest = lendersReturnsRepo.getLenderAmountByType(userId, amountType);

				Iterator it1 = lenderInterest.iterator();
				int count = 0;
				while (it1.hasNext()) {
					Object[] e = (Object[]) it1.next();

					LenderIndividualReturns lenderIndividual = new LenderIndividualReturns();
					lenderIndividual.setAmountPaid(Double.parseDouble(e[2] == null ? "0" : e[2].toString()));
					String paidDate = e[3] == null ? " " : e[3].toString();
					lenderIndividual.setPaidDate(e[3] == null ? " " : e[3].toString());
					lenderIndividual.setRemarksFromSheet(e[5] == null ? " " : e[5].toString());
					String remarksFromAdmin = null;
					if (key == 1) {

						remarksFromAdmin = oxyTransactionDetailsFromExcelSheetsRepo.getAdminRemarks(amountType,
								paidDate);
						if (remarksFromAdmin != null) {
							lenderIndividual.setRemarksFromAdmin(remarksFromAdmin);
						}
						count = count + 1;
						lenderIndividual.setSno(count);
						listOfLenderInterestEarned.add(lenderIndividual);
					} else {
						if (key == 2) {
							remarksFromAdmin = oxyTransactionDetailsFromExcelSheetsRepo.getAdminRemarks(amountType,
									paidDate);
							if (remarksFromAdmin != null) {
								lenderIndividual.setRemarksFromAdmin(remarksFromAdmin);
							}
							count = count + 1;
							lenderIndividual.setSno(count);
							listOfLenderPricipalEarned.add(lenderIndividual);
						}
						if (key == 3) {
							remarksFromAdmin = oxyTransactionDetailsFromExcelSheetsRepo.getAdminRemarks(amountType,
									paidDate);
							if (remarksFromAdmin != null) {
								lenderIndividual.setRemarksFromAdmin(remarksFromAdmin);
							}
							count = count + 1;
							lenderIndividual.setSno(count);
							listOfLenderEmiEarned.add(lenderIndividual);
						}
						if (key == 4) {
							remarksFromAdmin = oxyTransactionDetailsFromExcelSheetsRepo.getAdminRemarks(amountType,
									paidDate);
							if (remarksFromAdmin != null) {
								lenderIndividual.setRemarksFromAdmin(remarksFromAdmin);
							}
							count = count + 1;
							lenderIndividual.setSno(count);
							listOfLenderWithDraw.add(lenderIndividual);
						}
						if (key == 5) {
							remarksFromAdmin = oxyTransactionDetailsFromExcelSheetsRepo.getAdminRemarks(amountType,
									paidDate);
							if (remarksFromAdmin != null) {
								lenderIndividual.setRemarksFromAdmin(remarksFromAdmin);
							}
							count = count + 1;
							lenderIndividual.setSno(count);
							listOfLenderReLend.add(lenderIndividual);
						}
						if (key == 6) {
							remarksFromAdmin = oxyTransactionDetailsFromExcelSheetsRepo.getAdminRemarks(amountType,
									paidDate);
							if (remarksFromAdmin != null) {
								lenderIndividual.setRemarksFromAdmin(remarksFromAdmin);
							}
							count = count + 1;
							lenderIndividual.setSno(count);
							listOfLenderPoolingInterest.add(lenderIndividual);
						}
					}
				}
			}
			lenderReturnsResponseDto.setLenderInterestEarned(listOfLenderInterestEarned);
			lenderReturnsResponseDto.setLenderPrincipalReturned(listOfLenderPricipalEarned);
			lenderReturnsResponseDto.setLenderEmiReturned(listOfLenderEmiEarned);
			lenderReturnsResponseDto.setLenderWithDraw(listOfLenderWithDraw);
			lenderReturnsResponseDto.setLenderReLend(listOfLenderReLend);
			lenderReturnsResponseDto.setLenderPoolingInterest(listOfLenderPoolingInterest);
			lenderReturnsResponseDto.setTotalPrincipalOnFirstApril(totalPricipalOnApril);
			lenderReturnsResponseDto.setSumOfInterestAmount(sumOfInterestAmount);
			lenderReturnsResponseDto.setTotalPrincipalInLending(totalPricipalInLending);
		} catch (Exception e) {
			logger.info(e);
		}

		return lenderReturnsResponseDto;

	}

	@Override
	public CommentsResponseDto updatingLoanOwnerToBorrower(int userId,
			BorrowersLoanOwnerNames borrowersLoanOwnerNames) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public EnachScheduledMailResponseDto readingLenderReferenceInfoToExcelSheet() throws ParseException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderReferenceResponse readingCommentsOfRadhaSir(LenderReferenceRequestDto lenderReferenceRequestDto)
			throws ParseException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderReferenceResponse readingResponseForReferenceBonus(LenderReferenceRequestDto lenderReferenceRequestDto)
			throws ParseException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderReferenceResponse gettingLenderReferralBonusDetails() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BorrowersDealsResponseDto updateBorrowerDealsInformation(BorrowersDealsRequestDto borrowersDealsRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BorrowersDealsList getListOfDealsInformation(PaginationRequestDto pageRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BorrowersDealsResponseDto getSingleDeal(int dealId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderMappedToGroupIdResponseDto getListOfLendersMappedToGroupId(int groupId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LendersDealsResponseDto listOfDealsInformationDispayingToLender(int userId,
			PaginationRequestDto pageRequestDto) {
		int pageNo = pageRequestDto.getPageNo();
		int pageSize = pageRequestDto.getPageSize();

		pageNo = (pageSize * (pageNo - 1));

		User user = userRepo.findById(userId).get();
		LendersDealsResponseDto lendersDealsResponseDto = new LendersDealsResponseDto();
		List<ListOfDealsInformationToLender> listOfDealsInformationToLender = new ArrayList<ListOfDealsInformationToLender>();
		if (user != null) {

			String groupName = null;
			Integer groupId = 0;
			if (user.getLenderGroupId() == 0) {
				groupName = "New Lender";
				groupId = 0;
			} else {
				groupId = 3;
				groupName = oxyLendersGroupRepo.getLenderGroupNameByid(groupId);

			}
			List<Object[]> listOfDeals = null;
			Integer totalCount = 0;
			String dealStatusForFrontEnd = null;
			String dealStatus = null;
			Integer achiveCount = 0;
			Integer NotAchievedCount = 0;

			String dealType = pageRequestDto.getDealType();
			if ("CLOSED".equalsIgnoreCase(dealType)) {
				dealStatusForFrontEnd = "Achieved";
				dealStatus = "ACHIEVED";
			} else if ("HAPPENING".equalsIgnoreCase(dealType)) {
				dealStatusForFrontEnd = "Not Achieved";
				dealStatus = "NOTATACHIEVED";
			}
			if (dealStatus != null) {
				listOfDeals = oxyBorrowersDealsInformationRepo.getListOfDealsInformationForNormalDeals(pageNo, pageSize,
						dealStatus);
				if (listOfDeals != null && !listOfDeals.isEmpty()) {
					totalCount = oxyBorrowersDealsInformationRepo
							.getListOfDealsInformationCountForNormalDeals(dealStatus);
				}
			} else if ("CURRENT".equalsIgnoreCase(dealType)) {
				listOfDeals = oxyBorrowersDealsInformationRepo.getListOfDealInCurrentDate(pageNo, pageSize);

				achiveCount = oxyBorrowersDealsInformationRepo.getListOfDealsInformationCountForNormalDeals("ACHIEVED");
				logger.info("achiveCount" + achiveCount);

				NotAchievedCount = oxyBorrowersDealsInformationRepo
						.getListOfDealsInformationCountForNormalDeals("NOTATACHIEVED");
				logger.info("NotAchieved" + NotAchievedCount);

				if (listOfDeals != null && !listOfDeals.isEmpty()) {
					totalCount = oxyBorrowersDealsInformationRepo.getListOfDealsInformationCountForNormalDeals();
				}
			}
			lendersDealsResponseDto.setTotalCount(totalCount);
			lendersDealsResponseDto.setEmail(user.getEmail());
			lendersDealsResponseDto.setMobileNumber(user.getMobileNumber());
			lendersDealsResponseDto.setUserId(user.getId());
			if (listOfDeals != null && !listOfDeals.isEmpty()) {
				Iterator it = listOfDeals.iterator();
				while (it.hasNext()) {
					Object[] e = (Object[]) it.next();
					ListOfDealsInformationToLender listOfDealsInformation = new ListOfDealsInformationToLender();
					int dealId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
					double dealAmount = Double.parseDouble(e[3] == null ? "0" : e[3].toString());
					listOfDealsInformation.setGroupId(groupId);
					listOfDealsInformation.setGroupName(groupName);
					listOfDealsInformation.setDealId(dealId);
					listOfDealsInformation.setDealName(e[1] == null ? " " : e[1].toString());
					listOfDealsInformation.setBorrowerName(e[2] == null ? " " : e[2].toString());
					listOfDealsInformation.setRateOfInterest(Double.parseDouble(e[6] == null ? "0" : e[6].toString()));
					listOfDealsInformation.setFundsAcceptanceStartDate(e[4] == null ? " " : e[4].toString());
					listOfDealsInformation.setFundsAcceptanceEndDate(e[5] == null ? " " : e[5].toString());
					listOfDealsInformation.setDealAmount(dealAmount);
					listOfDealsInformation.setDealLink(e[7] == null ? " " : e[7].toString());
					listOfDealsInformation.setDuration(Integer.parseInt(e[8] == null ? "0" : e[8].toString()));
					if (e[17] == null) {
						listOfDealsInformation.setFundingStatus("Not Achieved");
					} else {
						listOfDealsInformation.setFundingStatus("Achieved");
					}
					listOfDealsInformation.setDealStatus(dealStatus);

					if (e[17] != null) {
						String lastParticipationDate = null;

						Date date = new Date();
						try {
							date = simpleDateFormat1.parse(e[17].toString());

							lastParticipationDate = addfiveAndHalfHoursToTimeStamp(date);

							listOfDealsInformation.setLastParticipationDate(lastParticipationDate);

						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}

					OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
							.getFirstParticipationDate(dealId);

					if (oxyLendersAcceptedDeals != null) {

						String firstParticipationDate = addfiveAndHalfHoursToTimeStamp(
								oxyLendersAcceptedDeals.getReceivedOn());

						listOfDealsInformation.setFirstParticipationDate(firstParticipationDate);

					}

					if (dealStatus != null && dealStatus != null && dealStatus.equalsIgnoreCase("ACHIEVED")) {
						listOfDealsInformation.setLenderPaticipateStatus(false);
					} else {
						if (e[15].toString()
								.equals(OxyBorrowersDealsInformation.ParticipationLenderType.ANY.toString())) {
							listOfDealsInformation.setLenderPaticipateStatus(true);
						} else {
							List<OxyLendersAcceptedDeals> oxylendersAcceptedDealsList = oxyLendersAcceptedDealsRepo
									.findbyUserId(userId);

							if (oxylendersAcceptedDealsList != null && !oxylendersAcceptedDealsList.isEmpty()) {

								OxyLendersAcceptedDeals oxylendersAcceptedDeals = oxyLendersAcceptedDealsRepo
										.toCheckUserAlreadyInvoledInDeal(userId, dealId);

								if (oxylendersAcceptedDeals == null) {
									listOfDealsInformation.setLenderPaticipateStatus(false);
								} else {
									listOfDealsInformation.setLenderPaticipateStatus(true);
								}
							} else {
								listOfDealsInformation.setLenderPaticipateStatus(true);
							}
						}

					}
					listOfDealsInformation.setDealCreatedType(e[15].toString());
					Double firstParticipation = oxyLendersAcceptedDealsRepo.getListOfLendersParticipatedAmount(dealId);
					Double updatedSum = lendersPaticipationUpdationRepo.getDealUpdatedSumValue(dealId);
					double amountParticipated = 0.0;
					if (firstParticipation != null) {
						if (updatedSum != null) {
							amountParticipated = firstParticipation + updatedSum;
						} else {
							amountParticipated = firstParticipation;
						}

					}
					double remainingAmount = 0.0;
					listOfDealsInformation
							.setTotalPaticipatedAmount(BigDecimal.valueOf(amountParticipated).toBigInteger());
					if (amountParticipated > 0) {
						remainingAmount = dealAmount - amountParticipated;
						listOfDealsInformation.setRemainingAmountToPaticipateInDeal(
								BigDecimal.valueOf(remainingAmount).toBigInteger());
					} else {
						listOfDealsInformation
								.setRemainingAmountToPaticipateInDeal(BigDecimal.valueOf(dealAmount).toBigInteger());
					}
					listOfDealsInformation.setMinimumAmountInDeal(BigDecimal
							.valueOf(Double.parseDouble(e[16] == null ? "0" : e[16].toString())).toBigInteger());
					listOfDealsInformation.setLenderPaticipationLimit(BigDecimal
							.valueOf(Double.parseDouble(e[13] == null ? "0" : e[13].toString())).toBigInteger());
					String listOfBorrowerIdsMappedTodeal = e[19] == null ? null : e[19].toString();

					List<Map<String, Integer>> listOfMaps = new ArrayList<>();

					if (listOfBorrowerIdsMappedTodeal != null && !listOfBorrowerIdsMappedTodeal.isEmpty()) {
						listOfDealsInformation.setBorrowersMappedStatus(true);
						String[] ids = listOfBorrowerIdsMappedTodeal.split(",");
						for (String s : ids) {
							Map<String, Integer> listOfBorrowerIds = new HashMap<String, Integer>();
							listOfBorrowerIds.put("borrowerId", Integer.parseInt(s));
							listOfMaps.add(listOfBorrowerIds);
						}

					}

					listOfDealsInformation.setListOfBorrowersMappedTodeal(listOfMaps);
					listOfDealsInformation.setWithdrawStatus(e[20] == null ? null : e[20].toString());
					listOfDealsInformation
							.setRoiForWithdraw(Double.parseDouble(e[21] == null ? "0" : e[21].toString()));
					listOfDealsInformation.setMessageSentToLenders(e[12] == null ? null : e[12].toString());
					Double roi = oxyDealsRateofinterestToLendersgroupRepo.getMaxRoiToDeal(dealId);
					listOfDealsInformation.setRateOfInterest(roi);
					OxyDealsRateofinterestToLendersgroup rateOfInterest = oxyDealsRateofinterestToLendersgroupRepo
							.findByDealIdAndGroupId(dealId, groupId);
					// displaying rate of interest monthly or yearly

					Double roi1 = (rateOfInterest != null) ? (rateOfInterest.getYearlyInterest() > 0
							? rateOfInterest.getYearlyInterest()
							: (rateOfInterest.getHalfInterest() > 0 ? rateOfInterest.getHalfInterest()
									: (rateOfInterest.getQuartelyInterest() > 0 ? rateOfInterest.getQuartelyInterest()
											: (rateOfInterest.getEndofthedealInterest() > 0
													? rateOfInterest.getEndofthedealInterest()
													: rateOfInterest.getMonthlyInterest()))))
							: 0.0;

					String type = (rateOfInterest != null)
							? (rateOfInterest.getYearlyInterest() > 0 ? "YEARLY"
									: (rateOfInterest.getHalfInterest() > 0 ? "HALFYEARLY"
											: (rateOfInterest.getQuartelyInterest() > 0 ? "QUARTERLY"
													: (rateOfInterest.getEndofthedealInterest() > 0
															? "ENDOFTHEDEALINTEREST"
															: "MONTHLY"))))
							: null;

					logger.info(roi1 + "RATEOFINTERST :" + rateOfInterest);

					listOfDealsInformation.setRepaymentType(type);
					listOfDealsInformationToLender.add(listOfDealsInformation);
				}
			}

		}
		lendersDealsResponseDto.setListOfDealsInformationToLender(listOfDealsInformationToLender);
		return lendersDealsResponseDto;

	}

	public String addfiveAndHalfHoursToTimeStamp(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR_OF_DAY, 5);

		calendar.add(Calendar.MINUTE, 30);

		return simpleDateFormat.format(calendar.getTime());

	}

	@Override
	public OxyLendersAcceptedDealsResponseDto updateLenderDealInformation(
			OxyLendersAcceptedDealsRequestDto oxyLendersAcceptedDealsRequestDto) {

		OxyLendersAcceptedDealsResponseDto oxyLendersAcceptedDealsResponseDto = new OxyLendersAcceptedDealsResponseDto();

		String message = null;

		User user = userRepo.findById(oxyLendersAcceptedDealsRequestDto.getUserId()).get();

		if (user.getBankDetails() == null || user.getBankDetails().getAccountNumber().length() == 0) {
			throw new OperationNotAllowedException("Please fill your bank details in profile section",
					ErrorCodes.PERMISSION_DENIED);
		}

		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
				.findDealAlreadyGiven(oxyLendersAcceptedDealsRequestDto.getDealId());

		if (OxyBorrowersDealsInformation.DealParticipationStatus.ACHIEVED
				.equals(oxyBorrowersDealsInformation.getDealParticipationStatus())) {
			throw new OperationNotAllowedException("You cannot participate as this deal is closed.",
					ErrorCodes.PERMISSION_DENIED);

		}

		double currentWalletAmount = 0.0;
		Double walletAmount = userRepo.userWalletAmount(oxyLendersAcceptedDealsRequestDto.getUserId());
		if (walletAmount != null) {
			currentWalletAmount = walletAmount;
		}
		if (currentWalletAmount <= 0
				|| (currentWalletAmount < oxyLendersAcceptedDealsRequestDto.getParticipatedAmount())) {
			throw new OperationNotAllowedException(
					"You are wallet is not having sufficient amount to paticipate in the deal",
					ErrorCodes.LOWER_AMOUNT);
		}

		Double processingFee = 0.0;
		Double totalPaticipationAmount = 0.0;

		// only new lender participation validation starts
		DecimalFormat myFormat = new DecimalFormat("#,###");
		if (oxyBorrowersDealsInformation != null) {

			if (oxyBorrowersDealsInformation.getParticipationLenderType().equals(ParticipationLenderType.NEW)) {

				List<OxyLendersAcceptedDeals> oxylendersAcceptedDealsList = oxyLendersAcceptedDealsRepo
						.findbyUserId(oxyLendersAcceptedDealsRequestDto.getUserId());

				if (oxylendersAcceptedDealsList != null && !oxylendersAcceptedDealsList.isEmpty()) {

					OxyLendersAcceptedDeals oxylendersAcceptedDeals = oxyLendersAcceptedDealsRepo
							.toCheckUserAlreadyInvoledInDeal(oxyLendersAcceptedDealsRequestDto.getUserId(),
									oxyLendersAcceptedDealsRequestDto.getDealId());

					if (oxylendersAcceptedDeals == null) {
						throw new OperationNotAllowedException(
								"Newly registered users or Existing users who haven't participated yet in any deal "
										+ "can only participate in this deal.",
								ErrorCodes.PERMISSION_DENIED);
					}
				}

			} // only new lender participation validation ends

			// lender fee validation starts

			String lenderFeeStatus = "";

			Double totalDealAmount = oxyLendersAcceptedDealsRepo
					.getTotalPaticipation(oxyLendersAcceptedDealsRequestDto.getDealId()); // totalAmountPaticipated
			if (totalDealAmount != null) {
				totalPaticipationAmount = totalDealAmount;

			}
			logger.info("totalPaticipationAmount" + totalPaticipationAmount);

			double maxmumLimit = 0.0;

			String messageToWtappTest = null;

			Double lendersOutstandingamount = oxyLendersAcceptedDealsRepo // outstandingAmount
					.getOutstandingAmount(oxyLendersAcceptedDealsRequestDto.getUserId());

			if (lendersOutstandingamount != null) {
				maxmumLimit = lendersOutstandingamount;
			} else {
				maxmumLimit = 0.0;

			}

			String mobileNumber = user.getPersonalDetails().getWhatsAppNumber() != null
					? user.getPersonalDetails().getWhatsAppNumber()
					: "91" + user.getMobileNumber();

			String lenderName = user.getPersonalDetails().getFirstName() + " "
					+ user.getPersonalDetails().getLastName();

			Calendar calendar = Calendar.getInstance();
			String currentDate = expectedDateFormat.format(calendar.getTime());

			OxyLendersAcceptedDeals oxyLendersAccepted = oxyLendersAcceptedDealsRepo.toCheckUserAlreadyInvoledInDeal(
					oxyLendersAcceptedDealsRequestDto.getUserId(), oxyLendersAcceptedDealsRequestDto.getDealId());

			String feeStatus = null;
			Double feeAmount = 0.0;
			Integer dealId = 0;

			OxyLendersAcceptedDeals oxylenders = oxyLendersAcceptedDealsRepo
					.findUserExistingDealsFeeStatus(oxyLendersAcceptedDealsRequestDto.getUserId());
			LendersPaticipationUpdation participationUpdationFee = lendersPaticipationUpdationRepo
					.findUserExistingDealsFeeStatusInUpdation(oxyLendersAcceptedDealsRequestDto.getUserId());

			if (oxylenders != null) {
				feeStatus = oxylenders.getFeeStatus().toString();
				feeAmount = oxylenders.getProcessingFee();
				dealId = oxylenders.getDealId();
			} else if (participationUpdationFee != null) {
				feeStatus = participationUpdationFee.getFeeStatus().toString();
				feeAmount = participationUpdationFee.getProcessingFee();
				dealId = participationUpdationFee.getDealId();
			} else {
				feeStatus = "COMPLETED";
			}

			String lenderDealName = null;
			OxyBorrowersDealsInformation dealName = oxyBorrowersDealsInformationRepo.findDealNameForLenderFee(dealId);
			if (dealName != null) {
				lenderDealName = dealName.getDealName();
			}
			if (!feeStatus.equals("COMPLETED")) {
				throw new OperationNotAllowedException("Your processing fee of INR " + feeAmount + " for deal ID "
						+ dealId + " (deal name: " + lenderDealName
						+ ") is currently pending. Please make the payment through the wallet / payment gateway provided below.",
						ErrorCodes.AMOUNT_EXCEEDED);
			}

			if (oxyLendersAccepted == null) {
				if (totalPaticipationAmount < oxyBorrowersDealsInformation.getDealAmount()
						&& (totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto
								.getParticipatedAmount() <= oxyBorrowersDealsInformation.getDealAmount())
						|| (totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto
								.getParticipatedAmount() < oxyBorrowersDealsInformation.getDealAmount())) {

					if (oxyBorrowersDealsInformation.getPaticipationLimitToLenders() > 0.0) {
						if (oxyBorrowersDealsInformation.getMinimumPaticipationAmount() != null) {

							Double currentDealAmount = currentDealAmount(oxyLendersAcceptedDealsRequestDto.getDealId());
							if (currentDealAmount != null) {
								if (currentDealAmount > 0 && currentDealAmount < oxyBorrowersDealsInformation
										.getMinimumPaticipationAmount()) {
									if (!oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()
											.equals(currentDealAmount)) {
										throw new OperationNotAllowedException(
												"your paticipation amount is less than the remaining amount. please check the remaining amount is INR "
														+ BigDecimal.valueOf(currentDealAmount).toBigInteger(),
												ErrorCodes.LIMIT_REACHED);
									}

								} else if (oxyLendersAcceptedDealsRequestDto
										.getParticipatedAmount() < oxyBorrowersDealsInformation
												.getMinimumPaticipationAmount()) {
									throw new OperationNotAllowedException(
											"your paticipation amount is less than the minimum amount. please note that the minimum amount is INR "
													+ BigDecimal.valueOf(
															oxyBorrowersDealsInformation.getMinimumPaticipationAmount())
															.toBigInteger(),
											ErrorCodes.LIMIT_REACHED);
								}
							}
						}

						if (oxyLendersAcceptedDealsRequestDto.getParticipatedAmount() > oxyBorrowersDealsInformation
								.getPaticipationLimitToLenders()) {

							throw new OperationNotAllowedException(
									"You have exceeded the paticipation limit. please note that the maximum amount is INR "
											+ BigDecimal.valueOf(
													oxyBorrowersDealsInformation.getPaticipationLimitToLenders())
													.toBigInteger(),
									ErrorCodes.LIMIT_REACHED);
						}

					}

					Double remainingAmountToParticipate = Double.parseDouble(maximumOutstandingAmount) - maxmumLimit;
					if (!oxyBorrowersDealsInformation.getDealType()
							.equals(OxyBorrowersDealsInformation.DealType.EQUITY)) {
						if ((maxmumLimit + oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()) > Double
								.parseDouble(maximumOutstandingAmount)) {
							throw new IllegalValueException(
									"The maximum lender Participation amount should be " + this.maximumOutstandingAmount
											+ " Your total participated amount is " + maxmumLimit
											+ " You can participate with " + remainingAmountToParticipate + " only",
									ErrorCodes.PATICIPATION_LIMIT);
						}
					}

					// update validity for an existing/new lender if there is a lifeTimeWaiver

					if (oxyBorrowersDealsInformation.getLifeTimeWaiver() && oxyLendersAcceptedDealsRequestDto
							.getParticipatedAmount() >= oxyBorrowersDealsInformation.getLifeTimeWaiverLimit()) {

						LenderRenewalDetails details = lenderRenewalDetailsRepo
								.findingUserInTable(oxyLendersAcceptedDealsRequestDto.getUserId());

						if (user != null && details != null) {
							user.setLenderGroupId(user.getLenderGroupId());
						} else {
							user.setLenderGroupId(7);
						}
						userRepo.save(user);

						logger.info("lenderRenewalDetails:" + details);

						Date paymentDate = new Date();
						if (details != null) {
							if (details.getValidityDate() != null) {

								logger.info("oldValidityDate" + details.getOldValidityDate());
								details.setOldValidityDate(details.getValidityDate());

								if (paymentDate.compareTo(details.getValidityDate()) < 0) {
									details.setPaymentDate(details.getValidityDate());
								} else {
									details.setPaymentDate(paymentDate);
								}
								Calendar c = Calendar.getInstance();
								int years = 14;
								c.setTime(details.getPaymentDate());
								c.add(Calendar.YEAR, years);
								details.setValidityDate(c.getTime());

								logger.info("newValidityDate" + details.getValidityDate());

								Date newDate = c.getTime();

								String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(newDate);

								lenderRenewalDetailsRepo.save(details);

								String msgToLender = "Dear *" + user.getPersonalDetails().getFirstName().trim() + "*"
										+ "\n" + "\nGreetings from Oxyloans!" + "\n"
										+ "we are happy to inform you that your oxyloans  membership validity is extended till"
										+ " *" + modifiedDate + "* from *" + details.getPaymentDate() + "*. \n\n"
										+ "Please write to us if you have any concerns using the below link:" + "\n"
										+ "https://www.oxyloans.com/new/lenderInquiries" + "\n\n" + "Thanks"
										+ "\nOxyloans Team";

								whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, msgToLender);

								String mailsubject = "Lender Validity Renewal";
								TemplateContext context = new TemplateContext();

								LocalDate currentDate1 = LocalDate.now();

								context.put("currentDate", currentDate1);
								context.put("name", lenderName);
								context.put("modifiedDate", modifiedDate);
								context.put("validityDate", details.getPaymentDate());

								EmailRequest emailRequest = new EmailRequest(
										new String[] { user.getEmail(), "sudisridhar9@gmail.com" },
										"lender_renewal_updation.template", context, mailsubject, "Oxyloans");

								EmailResponse emailResponse = emailService.sendEmail(emailRequest);

								if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
									try {
										throw new MessagingException(emailResponse.getErrorMessage());
									} catch (MessagingException e2) {

										e2.printStackTrace();
									}
								}
							}
						} else {
							LenderRenewalDetails renewalDetails = new LenderRenewalDetails();
							Calendar c = Calendar.getInstance();
							Date currentDate1 = new Date();

							int years = 14;
							c.setTime(currentDate1);
							c.add(Calendar.YEAR, years);
							renewalDetails.setValidityDate(c.getTime());

							logger.info("newValidityDate" + renewalDetails.getValidityDate());

							renewalDetails.setRenewalStatus(LenderRenewalDetails.RenewalStatus.NotYetRenewed);
							renewalDetails.setUser_id(oxyLendersAcceptedDealsRequestDto.getUserId());
							lenderRenewalDetailsRepo.save(renewalDetails);
						}

						lenderFeeStatus = PayuStatus.COMPLETED.toString();

					} else if (oxyLendersAcceptedDealsRequestDto.getLenderFeeId() != 0) {
						LenderPayuDetails lenderFeeDetails = lenderPayuDetailsRepo
								.findById(oxyLendersAcceptedDealsRequestDto.getLenderFeeId()).orElse(null);

						if (lenderFeeDetails != null) {
							lenderFeeStatus = lenderFeeDetails.getStatus().toString();
							if (!lenderFeeDetails.getStatus().equals(PayuStatus.COMPLETED)) {
								throw new OperationNotAllowedException("Lender fee pending",
										ErrorCodes.PERMISSION_DENIED);
							}
						} else {
							throw new OperationNotAllowedException("Lender fee pending", ErrorCodes.PERMISSION_DENIED);
						}
					}
					// lender fee validation ends

					logger.info("totalPaticipationAmountAchievement1" + totalPaticipationAmount
							+ oxyLendersAcceptedDealsRequestDto.getParticipatedAmount());

					if ((totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto
							.getParticipatedAmount() >= oxyBorrowersDealsInformation.getDealAmount())
							|| totalPaticipationAmount >= oxyBorrowersDealsInformation.getDealAmount()) {
						oxyBorrowersDealsInformation.setDealParticipationStatus(
								OxyBorrowersDealsInformation.DealParticipationStatus.ACHIEVED);
						oxyBorrowersDealsInformation.setDealAcheivedDate(new Date());
						oxyBorrowersDealsInformationRepo.save(oxyBorrowersDealsInformation);
					}

					lenderFeeStatus = oxyLendersAcceptedDealsRequestDto.getFeeStatus();

					OxyLendersAcceptedDeals oxyLendersAcceptedDeals = new OxyLendersAcceptedDeals();
					oxyLendersAcceptedDeals.setUserId(oxyLendersAcceptedDealsRequestDto.getUserId());
					oxyLendersAcceptedDeals.setDealId(oxyLendersAcceptedDealsRequestDto.getDealId());
					oxyLendersAcceptedDeals.setGroupId(oxyLendersAcceptedDealsRequestDto.getGroupId());
					oxyLendersAcceptedDeals.setProcessingFee(oxyLendersAcceptedDealsRequestDto.getProcessingFee());

					oxyLendersAcceptedDeals.setFeeStatus(OxyLendersAcceptedDeals.FeeStatus.valueOf(lenderFeeStatus));

					oxyLendersAcceptedDeals
							.setParticipatedAmount(oxyLendersAcceptedDealsRequestDto.getParticipatedAmount());
					double roi = lenderRateOfInterestToDeal(oxyLendersAcceptedDealsRequestDto.getDealId(),
							user.getLenderGroupId(), oxyLendersAcceptedDealsRequestDto.getLenderReturnType());
					oxyLendersAcceptedDeals.setRateofinterest(roi);

					oxyLendersAcceptedDeals.setLenderReturnsType(
							LenderReturnsType.valueOf(oxyLendersAcceptedDealsRequestDto.getLenderReturnType()));
					oxyLendersAcceptedDeals.setReceivedOn(new Date());
					if (oxyLendersAcceptedDealsRequestDto.getProcessingFee() != null) {
						processingFee = oxyLendersAcceptedDealsRequestDto.getProcessingFee();
					}

					oxyLendersAcceptedDeals.setLenderFeeId(oxyLendersAcceptedDealsRequestDto.getLenderFeeId());
					oxyLendersAcceptedDeals.setLenderParticipationFrom(OxyLendersAcceptedDeals.LenderParticipationFrom
							.valueOf(oxyLendersAcceptedDealsRequestDto.getLenderParticipationFrom()));
					if (oxyLendersAcceptedDealsRequestDto.getAccountType() != null) {
						oxyLendersAcceptedDeals
								.setPrincipalReturningAccountType(OxyLendersAcceptedDeals.PrincipalReturningAccountType
										.valueOf(oxyLendersAcceptedDealsRequestDto.getAccountType()));
					}
					message = "updated";
					oxyLendersAcceptedDealsRepo.save(oxyLendersAcceptedDeals);
					lenderWalletHistoryServiceRepo.lenderTransactionHistory(
							oxyLendersAcceptedDealsRequestDto.getUserId(),
							oxyLendersAcceptedDealsRequestDto.getParticipatedAmount(), "DEBIT", "P",
							oxyLendersAcceptedDealsRequestDto.getDealId());
					lenderWalletHistoryServiceRepo.calculatingOutstandingAmount(
							oxyLendersAcceptedDealsRequestDto.getUserId(),
							oxyLendersAcceptedDealsRequestDto.getParticipatedAmount(), "CREDIT", "P",
							oxyLendersAcceptedDealsRequestDto.getDealId());

					BigInteger particiaptionAmount = BigDecimal
							.valueOf(
									totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto.getParticipatedAmount())
							.toBigInteger();
					BigInteger dealAmount = BigDecimal.valueOf(oxyBorrowersDealsInformation.getDealAmount())
							.toBigInteger();
					BigInteger updatedAmount = BigDecimal
							.valueOf(oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()).toBigInteger();

					if (user.isTestUser()) {
						messageToWtappTest = " *This is a test user* ";
					}

					if (!user.isTestUser()) {
						String messageToWtapp = "*Hello " + (messageToWtappTest != null ? messageToWtappTest : "")
								+ lenderName + "(LR" + user.getId() + ") ,*" + "\n\n"
								+ "Congratulations on your successful participation in the deal named *"
								+ oxyBorrowersDealsInformation.getDealName() + "* with an amount of INR *"
								+ myFormat.format(updatedAmount) + "* on " + currentDate + "." + "\n\n"
								+ "With this, the total participation amount has now reached INR *"
								+ myFormat.format(particiaptionAmount) + "* for the Deal Valued at INR "
								+ myFormat.format(dealAmount) + "." + "\n\n" + "The current participation score is *"
								+ myFormat.format(particiaptionAmount) + "*/*" + myFormat.format(dealAmount) + "*.";

						whatappService.sendingWhatsappMessageWithNewInstance("917702795895-1630419207@g.us",
								messageToWtapp);
						whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, messageToWtapp);
					}

					if (oxyBorrowersDealsInformation.getFeeStatusToParticipate()
							.equals(OxyBorrowersDealsInformation.FeeStatusToParticipate.MANDATORY)
							&& oxyLendersAcceptedDeals.getFeeStatus()
									.equals(OxyLendersAcceptedDeals.FeeStatus.PENDING)) {
						if (!user.isTestUser()) {
							String messageToWtap = "*Hello " + (messageToWtappTest != null ? messageToWtappTest : "")
									+ lenderName + " (LR" + user.getId() + "),*" + "\n\n"
									+ "Please note that there is a pending fee associated with your participation in the deal named \""
									+ oxyBorrowersDealsInformation.getDealName() + "\".\n"
									+ "The fee amount for this deal is: INR *"
									+ oxyLendersAcceptedDeals.getProcessingFee() + "*.\n\n"
									+ "Please write to us if you have any concerns using the below link:\n"
									+ "https://www.oxyloans.com/new/lenderInquiries\n\n" + "Thanks,\nOxyloans Team";

							whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, messageToWtap);
						}

						if (oxyBorrowersDealsInformation.getParticipationLenderType()
								.equals(ParticipationLenderType.ANY)) {
							// String intpaymentCal ="";
							String firstInterestPaymentlink = "";

							String directryPath1 = intpaymentCal;

							logger.info("directryPath::" + directryPath1);

							/*
							 * String fileName = "InterestFaq"; logger.info("fileName::"+fileName);
							 * 
							 * String filePath = directryPath1 + fileName +".docx" ; File f =
							 * newFile(filePath);
							 * logger.info("filpathLocation@@@@@@@@@@....:"+f.getAbsolutePath());
							 */

							String mailSubject1 = "OXY_FIRST_INT_CALCULATION";

							String templateName = "lender_First_Interest_Payment.template";

							LocalDate currentDate2 = LocalDate.now();
							TemplateContext context = new TemplateContext();
							context.put("CurrentDate", currentDate2);
							context.put("firstInterestPaymentlink", firstInterestPaymentlink);
							context.put("userName", lenderName);
							context.put("intpaymentCal", intpaymentCal);

							EmailRequest emailRequest1 = new EmailRequest(
									new String[] { user.getEmail(), "saikumargadi16@gmail.com" }, templateName, context,
									mailSubject1, new String[] { directryPath1 });

							logger.info("emailRequest1" + emailRequest1);

							EmailResponse emailResponseFromService = emailService.sendEmail(emailRequest1);
							logger.info("emailResponseFromService" + emailResponseFromService);

							if (emailResponseFromService.getStatus() == EmailResponse.Status.FAILED) {
								try {
									throw new MessagingException(emailResponseFromService.getErrorMessage());
								} catch (MessagingException e2) {
									e2.printStackTrace();
								}
							}

						}

					}

					oxyLendersAcceptedDealsResponseDto.setDealId(oxyLendersAcceptedDealsRequestDto.getDealId());
					oxyLendersAcceptedDealsResponseDto.setLenderId(oxyLendersAcceptedDealsRequestDto.getUserId());
					oxyLendersAcceptedDealsResponseDto.setParticipatedId(oxyLendersAcceptedDeals.getId());
					oxyLendersAcceptedDealsResponseDto.setFeeStatus(oxyLendersAcceptedDeals.getFeeStatus().toString());
					oxyLendersAcceptedDealsResponseDto.setProcessingFee(oxyLendersAcceptedDeals.getProcessingFee());

					logger.info("Lender referral Bonus starts...................................");

					Integer refereeId = oxyLendersAcceptedDealsRequestDto.getUserId();

					LenderReferenceDetails refereeInfo = lenderReferenceDetailsRepo.findRefereeInfo(refereeId);
					if (!(oxyBorrowersDealsInformation.getDealType())
							.equals(OxyBorrowersDealsInformation.DealType.EQUITY)) {
						if (refereeInfo != null) {

							Double sumOfParticipationUpdatedAmount = lendersPaticipationUpdationRepo
									.findingSumOfParticipationUpdatedAmount(refereeId);

							if (sumOfParticipationUpdatedAmount == null) {
								sumOfParticipationUpdatedAmount = 0.0;
							}

							Double sumOfParticipatedAmountForFirstTime = oxyLendersAcceptedDealsRepo
									.findingSumOfParticipatedAmountForLender(refereeId);

							Double sumOfParticipatedAmount = sumOfParticipationUpdatedAmount
									+ sumOfParticipatedAmountForFirstTime;

							Double sumOfAmountInReferralTable = 0.0;
							sumOfAmountInReferralTable = lenderReferralBonusUpdatedRepo.findingSumOfAmount(refereeId);

							Double participationAmount = oxyLendersAcceptedDealsRequestDto.getParticipatedAmount();

							Double amount = 0.0;

							Integer countOfDeals = oxyLendersAcceptedDealsRepo.findCountOfDeals(refereeId);

							if (sumOfParticipatedAmount != null) {
								if (sumOfAmountInReferralTable != null) {
									amount = (participationAmount / 1000);
								} else {

									Double amountForReferee = lenderReferralBonusUpdatedRepo
											.getAmountForReferee(refereeInfo.getRefereeId());
									if (amountForReferee != null) {
										if (amountForReferee < 1000) {

											if (countOfDeals == 1) {
												if (participationAmount < 100000) {
													amount = (participationAmount / 100);
												} else if (participationAmount > 100000) {

													Double remainingAmount = participationAmount - 100000;
													Double amount1 = (remainingAmount / 500);
													Double amount2 = 1000 + amount1;

													amount = amount2 - amountForReferee;
												} else if (participationAmount == 100000) {
													Double amount1 = (participationAmount / 2000);
													Double amount2 = 1000 + amount1;

													amount = amount2 - amountForReferee;
												}
											} else {
												if (sumOfParticipatedAmount > 100000) {
													Double remainingAmount = (sumOfParticipatedAmount - 100000);

													Double amount2 = (remainingAmount / 1000);

													Double totalAmount = 1000 + amount2;

													amount = totalAmount - amountForReferee;
												} else {
													amount = (participationAmount / 100);
												}
											}
										} else if (amountForReferee >= 1000) {
											amount = (participationAmount / 1000);
										}

									} else {
										if (participationAmount >= 100000) {
											Double remainingAmount = participationAmount - 100000;
											Double amount1 = (remainingAmount / 1000);
											amount = 1000 + amount1;

										} else {
											amount = (participationAmount / 100);
										}

									}
								}

								refereeInfo.setStatus(LenderReferenceDetails.Status.Lent);

								LenderReferralBonusUpdated referralBonusDetails = new LenderReferralBonusUpdated();
								referralBonusDetails.setReferrerUserId(refereeInfo.getReferrerId());
								referralBonusDetails.setAmount(amount);
								referralBonusDetails.setRefereeUserId(refereeId);
								referralBonusDetails.setDealId(oxyLendersAcceptedDealsRequestDto.getDealId());
								referralBonusDetails.setParticipatedOn(new Date());
								referralBonusDetails.setParticipatedAmount(
										oxyLendersAcceptedDealsRequestDto.getParticipatedAmount());

								lenderReferralBonusUpdatedRepo.save(referralBonusDetails);

								Double sumOfAmountUnpaid = lenderReferenceDetailsRepo
										.findingAmountForUnpaid(refereeInfo.getRefereeId());
								if (sumOfAmountUnpaid != null) {
									refereeInfo.setAmount(sumOfAmountUnpaid);
									lenderReferenceDetailsRepo.save(refereeInfo);
								} else {
									refereeInfo.setAmount(0.0);
									lenderReferenceDetailsRepo.save(refereeInfo);
								}

							}
						}
					}

					logger.info("Lender referral Bonus ends...................................");
				} else {
					throw new OperationNotAllowedException("Paticipation completed and deal value achieved",
							ErrorCodes.AMOUNT_EXCEEDED);
				}

			} else {
				Double pendingFee = lendersPaticipationUpdationRepo.findingSumOfAmountUpdatedLenderProcessingFee(
						oxyLendersAcceptedDealsRequestDto.getUserId(), oxyLendersAcceptedDealsRequestDto.getDealId());
				double feeValue = 0.0;
				if (!oxyLendersAccepted.getFeeStatus().equals(OxyLendersAcceptedDeals.FeeStatus.COMPLETED)) {
					feeValue += oxyLendersAccepted.getProcessingFee();
				}
				if (pendingFee != null) {
					feeValue += pendingFee;
				}
				if (!oxyLendersAccepted.getFeeStatus().equals(OxyLendersAcceptedDeals.FeeStatus.COMPLETED)
						|| pendingFee != null) {
					throw new OperationNotAllowedException(
							"your  processing fee for the first participation is the dealType "
									+ oxyBorrowersDealsInformation.getDealType() + " currently pending for Deal ID:"
									+ oxyLendersAccepted.getDealId() + "The amount to be paid is:" + feeValue + ".",
							ErrorCodes.AMOUNT_EXCEEDED);

				}
				if (oxyBorrowersDealsInformation.getDealType().equals(OxyBorrowersDealsInformation.DealType.PERSONAL)) {
					throw new OperationNotAllowedException("you should not participate twice in salaried deal",
							ErrorCodes.ALREDY_IN_PROGRESS);
				}

				if (oxyLendersAcceptedDealsRequestDto.getParticipatedAmount() != null) {
					if (totalPaticipationAmount < oxyBorrowersDealsInformation.getDealAmount()
							&& (totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto
									.getParticipatedAmount() <= oxyBorrowersDealsInformation.getDealAmount())
							|| (totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto
									.getParticipatedAmount() < oxyBorrowersDealsInformation.getDealAmount())) {

						Double participationUpdatedAmount = lendersPaticipationUpdationRepo
								.getSumOfLenderUpdatedAmountToDeal(oxyLendersAcceptedDealsRequestDto.getUserId(),
										oxyLendersAcceptedDealsRequestDto.getDealId());
						Double alreadyPaticipatedAmount = 0.0;
						Double totalPaticipatedAmount = 0.0;
						if (participationUpdatedAmount != null) {
							alreadyPaticipatedAmount = oxyLendersAccepted.getParticipatedAmount()
									+ participationUpdatedAmount
									+ oxyLendersAcceptedDealsRequestDto.getParticipatedAmount();
							totalPaticipatedAmount = oxyLendersAccepted.getParticipatedAmount()
									+ participationUpdatedAmount;
						} else {
							alreadyPaticipatedAmount = oxyLendersAccepted.getParticipatedAmount()
									+ oxyLendersAcceptedDealsRequestDto.getParticipatedAmount();
							totalPaticipatedAmount = oxyLendersAccepted.getParticipatedAmount();
						}

						if (alreadyPaticipatedAmount > oxyBorrowersDealsInformation.getPaticipationLimitToLenders()) {

							throw new OperationNotAllowedException(
									"You have exceeded the paticipation limit. please note that the maximum amount is INR "
											+ BigDecimal.valueOf(
													oxyBorrowersDealsInformation.getPaticipationLimitToLenders())
													.toBigInteger(),
									ErrorCodes.LIMIT_REACHED);
						}

						Double remainingAmountToParticipate = Double.parseDouble(maximumOutstandingAmount)
								- maxmumLimit;
						if (!oxyBorrowersDealsInformation.getDealType()
								.equals(OxyBorrowersDealsInformation.DealType.EQUITY)) {
							if ((maxmumLimit + oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()) > Double
									.parseDouble(maximumOutstandingAmount)) {
								throw new IllegalValueException(
										"The maximum lender Participation amount should be "
												+ this.maximumOutstandingAmount + " Your total participated amount is "
												+ maxmumLimit + " You can participate with "
												+ remainingAmountToParticipate + " only",
										ErrorCodes.PATICIPATION_LIMIT);
							}
						}
						// update validity for an existing/new lender if there is a lifeTimeWaiver

						if (oxyBorrowersDealsInformation.getLifeTimeWaiver()
								&& alreadyPaticipatedAmount >= oxyBorrowersDealsInformation.getLifeTimeWaiverLimit()) {

							LenderRenewalDetails details = lenderRenewalDetailsRepo
									.findingUserInTable(oxyLendersAcceptedDealsRequestDto.getUserId());

							if (user != null && details != null) {
								user.setLenderGroupId(user.getLenderGroupId());
							} else {
								user.setLenderGroupId(7);
							}
							userRepo.save(user);

							logger.info("lenderRenewalDetails:" + details);

							Date paymentDate = new Date();
							if (details != null) {
								if (details.getValidityDate() != null) {

									logger.info("oldValidityDate" + details.getOldValidityDate());
									details.setOldValidityDate(details.getValidityDate());

									if (paymentDate.compareTo(details.getValidityDate()) < 0) {
										details.setPaymentDate(details.getValidityDate());
									} else {
										details.setPaymentDate(paymentDate);
									}
									Calendar c = Calendar.getInstance();
									int years = 14;
									c.setTime(details.getPaymentDate());
									c.add(Calendar.YEAR, years);
									details.setValidityDate(c.getTime());

									logger.info("newValidityDate" + details.getValidityDate());

									Date newDate = c.getTime();

									String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(newDate);

									lenderRenewalDetailsRepo.save(details);

									String msgToLender = "Dear *" + user.getPersonalDetails().getFirstName().trim()
											+ "*" + "\n" + "\nGreetings from Oxyloans!" + "\n"
											+ "we are happy to inform you that your oxyloans  membership validity is extended till"
											+ " *" + modifiedDate + "* from *" + details.getPaymentDate() + "*. \n\n"
											+ "Please write to us if you have any concerns using the below link:" + "\n"
											+ "https://www.oxyloans.com/new/lenderInquiries" + "\n\n" + "Thanks"
											+ "\nOxyloans Team";

									whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, msgToLender);

									String mailsubject = "Lender Validity Renewal";
									TemplateContext context = new TemplateContext();

									LocalDate currentDate1 = LocalDate.now();

									context.put("currentDate", currentDate1);
									context.put("name", lenderName);
									context.put("modifiedDate", modifiedDate);
									context.put("validityDate", details.getPaymentDate());

									EmailRequest emailRequest = new EmailRequest(
											new String[] { user.getEmail(), "sudisridhar9@gmail.com" },
											"lender_renewal_updation.template", context, mailsubject, "Oxyloans");

									EmailResponse emailResponse = emailService.sendEmail(emailRequest);

									if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
										try {
											throw new MessagingException(emailResponse.getErrorMessage());
										} catch (MessagingException e2) {

											e2.printStackTrace();
										}
									}
								}
							} else {
								LenderRenewalDetails renewalDetails = new LenderRenewalDetails();
								Calendar c = Calendar.getInstance();
								Date currentDate1 = new Date();

								int years = 14;
								c.setTime(currentDate1);
								c.add(Calendar.YEAR, years);
								renewalDetails.setValidityDate(c.getTime());

								logger.info("newValidityDate" + renewalDetails.getValidityDate());

								renewalDetails.setRenewalStatus(LenderRenewalDetails.RenewalStatus.NotYetRenewed);
								renewalDetails.setUser_id(oxyLendersAcceptedDealsRequestDto.getUserId());
								lenderRenewalDetailsRepo.save(renewalDetails);
							}
							lenderFeeStatus = PayuStatus.COMPLETED.toString();

						} else if (oxyLendersAcceptedDealsRequestDto.getLenderFeeId() != 0) {
							LenderPayuDetails lenderFeeDetails = lenderPayuDetailsRepo
									.findById(oxyLendersAcceptedDealsRequestDto.getLenderFeeId()).orElse(null);

							if (lenderFeeDetails != null) {
								lenderFeeStatus = lenderFeeDetails.getStatus().toString();
								if (!lenderFeeDetails.getStatus().equals(PayuStatus.COMPLETED)) {
									throw new OperationNotAllowedException("Lender fee pending",
											ErrorCodes.PERMISSION_DENIED);
								}
							} else {
								throw new OperationNotAllowedException("Lender fee pending",
										ErrorCodes.PERMISSION_DENIED);
							}
						}
						// lender fee validation ends

						logger.info("totalPaticipationAmountAchievement2" + totalPaticipationAmount
								+ oxyLendersAcceptedDealsRequestDto.getParticipatedAmount());

						if (totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto
								.getParticipatedAmount() >= oxyBorrowersDealsInformation.getDealAmount()) {
							oxyBorrowersDealsInformation.setDealParticipationStatus(
									OxyBorrowersDealsInformation.DealParticipationStatus.ACHIEVED);
							oxyBorrowersDealsInformation.setDealAcheivedDate(new Date());
							oxyBorrowersDealsInformationRepo.save(oxyBorrowersDealsInformation);
						}

						message = "updated";

						BigInteger particiaptionAmount = BigDecimal.valueOf(
								totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto.getParticipatedAmount())
								.toBigInteger();

						BigInteger dealAmount = BigDecimal.valueOf(oxyBorrowersDealsInformation.getDealAmount())
								.toBigInteger();

						BigInteger updatedAmount = BigDecimal.valueOf(oxyLendersAccepted.getParticipatedAmount()
								+ oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()).toBigInteger();

						BigInteger oldPaticiaptionAmount = BigDecimal
								.valueOf(oxyLendersAccepted.getParticipatedAmount()).toBigInteger();

						BigInteger currentAddedAmount = BigDecimal
								.valueOf(oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()).toBigInteger();

						lenderFeeStatus = oxyLendersAcceptedDealsRequestDto.getFeeStatus();

						LendersPaticipationUpdation lendersPaticipationUpdation = new LendersPaticipationUpdation();
						lendersPaticipationUpdation.setUserId(oxyLendersAcceptedDealsRequestDto.getUserId());
						lendersPaticipationUpdation.setDealId(oxyLendersAcceptedDealsRequestDto.getDealId());
						lendersPaticipationUpdation
								.setUpdationAmount(oxyLendersAcceptedDealsRequestDto.getParticipatedAmount());
						lendersPaticipationUpdation.setType(LendersPaticipationUpdation.Type.ADD);
						lendersPaticipationUpdation.setUpdatedOn(new Date());

						lendersPaticipationUpdation
								.setProcessingFee(oxyLendersAcceptedDealsRequestDto.getProcessingFee());

						lendersPaticipationUpdation.setDealType(LendersPaticipationUpdation.DealType
								.valueOf(oxyBorrowersDealsInformation.getDealType().toString()));

						lendersPaticipationUpdation
								.setFeeStatus(LendersPaticipationUpdation.FeeStatus.valueOf(lenderFeeStatus));

						lendersPaticipationUpdation
								.setLenderParticipationFrom(LendersPaticipationUpdation.LenderParticipationFrom
										.valueOf(oxyLendersAcceptedDealsRequestDto.getLenderParticipationFrom()));

						lendersPaticipationUpdationRepo.save(lendersPaticipationUpdation);

						lenderWalletHistoryServiceRepo.lenderTransactionHistory(
								oxyLendersAcceptedDealsRequestDto.getUserId(),
								oxyLendersAcceptedDealsRequestDto.getParticipatedAmount(), "DEBIT", "PU",
								oxyLendersAcceptedDealsRequestDto.getDealId());
						lenderWalletHistoryServiceRepo.calculatingOutstandingAmount(
								oxyLendersAcceptedDealsRequestDto.getUserId(),
								oxyLendersAcceptedDealsRequestDto.getParticipatedAmount(), "CREDIT", "PU",
								oxyLendersAcceptedDealsRequestDto.getDealId());

						if (user.isTestUser()) {
							messageToWtappTest = " *This is a test user* ";
						}

						if (!user.isTestUser()) {

							String messageToWtapp = "*Hello " + (messageToWtappTest != null ? messageToWtappTest : "")
									+ lenderName + "(LR" + user.getId() + ") ,*" + "\n\n"
									+ "Congratulations on your successful participation in the deal named *"
									+ oxyBorrowersDealsInformation.getDealName() + "* with an amount of INR *"
									+ myFormat.format(currentAddedAmount) + "* on " + currentDate + "." + "\n\n"
									+ "With this, the total participation amount has now reached INR *"
									+ myFormat.format(particiaptionAmount) + "* for the deal valued at INR "
									+ myFormat.format(dealAmount) + "." + "\n\n"
									+ "The current participation score is *" + myFormat.format(particiaptionAmount)
									+ "*/*" + myFormat.format(dealAmount) + "*." + "\n\n" + "*Note:* "
									+ "You have increased your participation amount from INR "
									+ myFormat.format(BigDecimal.valueOf(totalPaticipatedAmount).toBigInteger())
									+ " to INR *"
									+ myFormat.format(BigDecimal.valueOf(alreadyPaticipatedAmount).toBigInteger())
									+ "*.";

							whatappService.sendingWhatsappMessageWithNewInstance("917702795895-1630419207@g.us",
									messageToWtapp);
							whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, messageToWtapp);

						}

						if (oxyBorrowersDealsInformation.getFeeStatusToParticipate()
								.equals(OxyBorrowersDealsInformation.FeeStatusToParticipate.MANDATORY)
								&& lendersPaticipationUpdation.getFeeStatus()
										.equals(LendersPaticipationUpdation.FeeStatus.PENDING)) {
							if (!user.isTestUser()) {
								String messageToWtap = "*Hello "
										+ (messageToWtappTest != null ? messageToWtappTest : "") + lenderName + " (LR"
										+ user.getId() + "),*" + "\n\n"
										+ "Please note that there is a pending fee associated with your participation in the deal named \""
										+ oxyBorrowersDealsInformation.getDealName() + "\".\n"
										+ "The fee amount for this deal is: INR *"
										+ lendersPaticipationUpdation.getProcessingFee() + "*.\n\n"
										+ "Please write to us if you have any concerns using the below link:\n"
										+ "https://www.oxyloans.com/new/lenderInquiries\n\n" + "Thanks,\nOxyloans Team";

								whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, messageToWtap);
							}
						}
						if (oxyLendersAcceptedDealsRequestDto.getAccountType() != null) {
							oxyLendersAccepted.setPrincipalReturningAccountType(
									OxyLendersAcceptedDeals.PrincipalReturningAccountType
											.valueOf(oxyLendersAcceptedDealsRequestDto.getAccountType()));
							oxyLendersAcceptedDealsRepo.save(oxyLendersAccepted);
						}

						oxyLendersAcceptedDealsResponseDto.setDealId(oxyLendersAcceptedDealsRequestDto.getDealId());
						oxyLendersAcceptedDealsResponseDto.setLenderId(oxyLendersAcceptedDealsRequestDto.getUserId());
						oxyLendersAcceptedDealsResponseDto.setParticipatedId(lendersPaticipationUpdation.getId());
						oxyLendersAcceptedDealsResponseDto
								.setFeeStatus(lendersPaticipationUpdation.getFeeStatus().toString());
						oxyLendersAcceptedDealsResponseDto
								.setProcessingFee(lendersPaticipationUpdation.getProcessingFee());

						logger.info("Lender referral Bonus starts1...................................");

						Integer refereeId = oxyLendersAcceptedDealsRequestDto.getUserId();

						LenderReferenceDetails refereeInfo = lenderReferenceDetailsRepo.findRefereeInfo(refereeId);

						if (!(oxyBorrowersDealsInformation.getDealType())
								.equals(OxyBorrowersDealsInformation.DealType.EQUITY)) {
							if (refereeInfo != null) {

								Double amount = 0.0;

								Double sumOfParticipationUpdatedAmount = lendersPaticipationUpdationRepo
										.findingSumOfParticipationUpdatedAmount(refereeId);

								if (sumOfParticipationUpdatedAmount == null) {
									sumOfParticipationUpdatedAmount = 0.0;
								}

								Double sumOfParticipatedAmountForFirstTime = oxyLendersAcceptedDealsRepo
										.findingSumOfParticipatedAmountForLender(refereeId);

								Double sumOfParticipatedAmount = sumOfParticipationUpdatedAmount
										+ sumOfParticipatedAmountForFirstTime;

								Double amountForReferee = lenderReferralBonusUpdatedRepo
										.getAmountForReferee(refereeInfo.getRefereeId());

								if (amountForReferee != null) {
									if (amountForReferee < 1000) {

										if (sumOfParticipatedAmount <= 100000) {

											Double amount1 = (oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()
													/ 100);
											amount = amount1;
										} else if (sumOfParticipatedAmount > 100000) {
											Double remainingAmount = (sumOfParticipatedAmount - 100000);

											if (amountForReferee >= 500) {

												Double amount2 = (remainingAmount / 1000);

												Double reference = 1000 + amount2;

												amount = reference - amountForReferee;

											} else if (amountForReferee < 500) {

												Double amount2 = (remainingAmount / 1000);

												Double totalAmount = 1000 + amount2;

												amount = totalAmount;
											}

										}

									} else if (amountForReferee >= 1000) {

										Double amount1 = (oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()
												/ 1000);

										amount = amount1;

									}
								}

								LenderReferralBonusUpdated referralDetails = new LenderReferralBonusUpdated();
								referralDetails.setReferrerUserId(refereeInfo.getReferrerId());
								referralDetails.setAmount(amount);
								referralDetails.setRefereeUserId(refereeId);
								referralDetails.setDealId(oxyLendersAcceptedDealsRequestDto.getDealId());
								referralDetails.setParticipatedOn(new Date());
								referralDetails.setParticipatedAmount(
										oxyLendersAcceptedDealsRequestDto.getParticipatedAmount());

								lenderReferralBonusUpdatedRepo.save(referralDetails);

								Double sumOfAmountUnpaid = lenderReferenceDetailsRepo
										.findingAmountForUnpaid(refereeInfo.getRefereeId());
								if (sumOfAmountUnpaid != null) {
									refereeInfo.setAmount(sumOfAmountUnpaid);
									lenderReferenceDetailsRepo.save(refereeInfo);
								} else {
									refereeInfo.setAmount(0.0);
									lenderReferenceDetailsRepo.save(refereeInfo);
								}
							}
						}

						logger.info("Lender referral Bonus ends1...................................");

					} else {
						throw new OperationNotAllowedException("Paticipation completed and deal value achieved",
								ErrorCodes.AMOUNT_EXCEEDED);
					}
				}

			}
		}

		oxyLendersAcceptedDealsResponseDto.setProcessingFee(processingFee);

		oxyLendersAcceptedDealsResponseDto.setDealId(oxyLendersAcceptedDealsRequestDto.getDealId());

		oxyLendersAcceptedDealsResponseDto.setStatus(message);

		ListOfWhatappGroupNames listOfWhatappGroupNames = new ListOfWhatappGroupNames();
		listOfWhatappGroupNames.setDealName(oxyBorrowersDealsInformation.getDealName());
		try {
			com.oxyloans.response.user.StatusResponseDto statusResponseDto = notifications
					.sendingNotificationsToLendersInDealPending(listOfWhatappGroupNames);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return oxyLendersAcceptedDealsResponseDto;

	}

	public double lenderRateOfInterestToDeal(int dealId, int groupId, String lenderReturnType) {
		double roi = 0.0;
		OxyDealsRateofinterestToLendersgroup oxyDealsRateofinterest = null;
		if (groupId == 0 || groupId == 7) {
			oxyDealsRateofinterest = oxyDealsRateofinterestToLendersgroupRepo.getDealMappedToSingleGroup(dealId, 0);

		} else {
			oxyDealsRateofinterest = oxyDealsRateofinterestToLendersgroupRepo.getDealMappedToSingleGroup(dealId, 3);
		}
		if (lenderReturnType.equalsIgnoreCase(OxyLendersAcceptedDeals.LenderReturnsType.MONTHLY.toString())) {
			roi = oxyDealsRateofinterest.getMonthlyInterest();
		}
		if (lenderReturnType.equalsIgnoreCase(OxyLendersAcceptedDeals.LenderReturnsType.QUARTELY.toString())) {
			roi = oxyDealsRateofinterest.getQuartelyInterest();
		}
		if (lenderReturnType.equalsIgnoreCase(OxyLendersAcceptedDeals.LenderReturnsType.HALFLY.toString())) {
			roi = oxyDealsRateofinterest.getHalfInterest();
		}
		if (lenderReturnType.equalsIgnoreCase(OxyLendersAcceptedDeals.LenderReturnsType.YEARLY.toString())) {
			roi = oxyDealsRateofinterest.getYearlyInterest();
		}
		if (lenderReturnType.equalsIgnoreCase(OxyLendersAcceptedDeals.LenderReturnsType.ENDOFTHEDEAL.toString())) {
			roi = oxyDealsRateofinterest.getEndofthedealInterest();
		}
		return roi;

	}

	@Override
	public DealInformationRoiToLender getSigleDealInformation(int userId, int dealId) {
		User user = userRepo.findById(userId).get();
		Double lenderTotalParticipation = null;
		DealInformationRoiToLender listOfDealsInformation = new DealInformationRoiToLender();
		if (user != null) {
			OxyBorrowersDealsInformation oxyBorrowersDeals = oxyBorrowersDealsInformationRepo
					.findDealAlreadyGiven(dealId);
			if (oxyBorrowersDeals != null) {

				String groupName = user.getLenderGroupId() == 0 ? "NewLender"
						: user.getLenderGroupId() == 7 ? "OXYMARCH09" : "OxyPremiuimLenders";
				Integer lenderGroupId = user.getLenderGroupId() == 0 ? 0 : user.getLenderGroupId() == 7 ? 0 : 3;

				OxyDealsRateofinterestToLendersgroup oxyDealsRateofinterestToLendersgroup = oxyDealsRateofinterestToLendersgroupRepo
						.getDealMappedToSingleGroup(dealId, lenderGroupId);
				if (oxyDealsRateofinterestToLendersgroup != null) {
					listOfDealsInformation
							.setMonthlyInterest(oxyDealsRateofinterestToLendersgroup.getMonthlyInterest());
					listOfDealsInformation
							.setQuartlyInterest(oxyDealsRateofinterestToLendersgroup.getQuartelyInterest());
					listOfDealsInformation.setHalfInterest(oxyDealsRateofinterestToLendersgroup.getHalfInterest());
					listOfDealsInformation.setYearlyInterest(oxyDealsRateofinterestToLendersgroup.getYearlyInterest());
					listOfDealsInformation
							.setEndofthedealInterest(oxyDealsRateofinterestToLendersgroup.getEndofthedealInterest());
				}

				listOfDealsInformation.setGroupId(lenderGroupId);
				listOfDealsInformation.setGroupName(groupName);
				listOfDealsInformation.setDealName(oxyBorrowersDeals.getDealName());
				listOfDealsInformation.setBorrowerName(oxyBorrowersDeals.getBorrowerName());
				listOfDealsInformation.setDealAmount(oxyBorrowersDeals.getDealAmount());
				listOfDealsInformation
						.setFeeStatusToParticipate(oxyBorrowersDeals.getFeeStatusToParticipate().toString());
				Double totalPaticipation = oxyLendersAcceptedDealsRepo.getTotalPaticipation(dealId);

				double amountParticipated = 0.0;
				if (totalPaticipation != null) {

					amountParticipated = totalPaticipation;

				}
				double remainingAmount = 0.0;
				if (amountParticipated > 0) {
					remainingAmount = oxyBorrowersDeals.getDealAmount() - amountParticipated;
					listOfDealsInformation.setRemainingAmountInDeal(BigDecimal.valueOf(remainingAmount).toBigInteger());
				} else {
					remainingAmount = oxyBorrowersDeals.getDealAmount();
					listOfDealsInformation.setRemainingAmountInDeal(
							BigDecimal.valueOf(oxyBorrowersDeals.getDealAmount()).toBigInteger());
				}

				lenderTotalParticipation = oxyLendersAcceptedDealsRepo.totalDealPaticipation(userId, dealId);

				listOfDealsInformation.setLenderParticipationTotal(
						lenderTotalParticipation != null ? BigDecimal.valueOf(lenderTotalParticipation).toBigInteger()
								: null);

				if (oxyBorrowersDeals.getLifeTimeWaiver() != null) {
					listOfDealsInformation.setLifeTimeWaiver(oxyBorrowersDeals.getLifeTimeWaiver());
				}
				if (oxyBorrowersDeals.getLifeTimeWaiverLimit() != null) {
					listOfDealsInformation.setLifeTimeWaiverLimit(
							BigDecimal.valueOf(oxyBorrowersDeals.getLifeTimeWaiverLimit()).toBigInteger());
				}

				listOfDealsInformation.setRateOfInterest(oxyBorrowersDeals.getBorrowerRateofinterest());
				listOfDealsInformation.setDuration(oxyBorrowersDeals.getDuration());
				listOfDealsInformation
						.setFundStartDate(expectedDateFormat.format(oxyBorrowersDeals.getFundsAcceptanceStartDate()));
				listOfDealsInformation
						.setFundEndDate(expectedDateFormat.format(oxyBorrowersDeals.getFundsAcceptanceEndDate()));
				if (oxyBorrowersDeals.getPaticipationLimitToLenders() != null) {
					listOfDealsInformation
							.setLenderParticiptionLimit(oxyBorrowersDeals.getPaticipationLimitToLenders());
				}
				if (oxyBorrowersDeals.getMinimumPaticipationAmount() != null) {
					listOfDealsInformation.setMinimumPaticipationAmount(
							BigDecimal.valueOf(oxyBorrowersDeals.getMinimumPaticipationAmount()).toBigInteger());
				}

				if (remainingAmount > 0.0) {
					listOfDealsInformation.setFundingStatus("HAPPENING");
				} else {
					listOfDealsInformation.setFundingStatus("COMPLETED");
				}
				listOfDealsInformation.setDealLink(oxyBorrowersDeals.getDealLink());
				listOfDealsInformation.setDealType(oxyBorrowersDeals.getDealType().toString());

				LenderRenewalDetails renewalDetailss = lenderRenewalDetailsRepo
						.findingRenewalDetailsBasedOnUserIdStatus(userId);

				if (renewalDetailss != null) {
					listOfDealsInformation.setValidityStatus(false);
					listOfDealsInformation.setLenderValidityStatus(false);
				} else {
					listOfDealsInformation.setValidityStatus(true);
					listOfDealsInformation.setLenderValidityStatus(true);
				}
			}
		}
		if (lenderTotalParticipation != null) {

			LenderPaticipatedResponseDto lenderPaticipatedResponseDto = new LenderPaticipatedResponseDto();

			lenderPaticipatedResponseDto.setDealAmount(lenderTotalParticipation);

			listOfDealsInformation.setLenderPaticipatedResponseDto(lenderPaticipatedResponseDto);
		}
		return listOfDealsInformation;

	}

	@Override
	public LenderPaticipatedDeal getLenderPaticipatedDealsInformation(int userId, PaginationRequestDto pageRequestDto) {
		int pageNo = pageRequestDto.getPageNo();
		int pageSize = pageRequestDto.getPageSize();
		pageNo = (pageSize * (pageNo - 1));

		LenderPaticipatedDeal lenderPaticipatedDeal = new LenderPaticipatedDeal();
		Integer count = 0;

		Integer value = oxyLendersAcceptedDealsRepo.getLenderPaticipatedDealsCountExpectEquity(userId);
		if (value != null) {
			count = value;
		}
		List<LenderPaticipatedResponseDto> listOfdeasToLender = new ArrayList<LenderPaticipatedResponseDto>();
		// try {
		List<Object[]> listOfdeals = oxyLendersAcceptedDealsRepo.getLenderPaticipatedDealsExpectEquity(userId, pageNo,
				pageSize);
		if (listOfdeals != null && !listOfdeals.isEmpty()) {
			Iterator it = listOfdeals.iterator();
			while (it.hasNext()) {
				Object[] e = (Object[]) it.next();
				LenderPaticipatedResponseDto lenderPaticipatedResponseDto = new LenderPaticipatedResponseDto();
				Integer dealId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
				OxyBorrowersDealsInformation oxyBorrowersDeals = oxyBorrowersDealsInformationRepo
						.findDealAlreadyGiven(dealId);
				if (oxyBorrowersDeals != null) {
					lenderPaticipatedResponseDto.setDealName(oxyBorrowersDeals.getDealName());
					lenderPaticipatedResponseDto.setDealBorrowerName(oxyBorrowersDeals.getBorrowerName());
					lenderPaticipatedResponseDto.setDealAmount(oxyBorrowersDeals.getDealAmount());
					lenderPaticipatedResponseDto.setDealRateofinterest(oxyBorrowersDeals.getBorrowerRateofinterest());
					lenderPaticipatedResponseDto.setDealDuration(oxyBorrowersDeals.getDuration());
					lenderPaticipatedResponseDto
							.setParticipationStatus(oxyBorrowersDeals.getDealParticipationStatus().toString());
					lenderPaticipatedResponseDto
							.setFirstInterestDate(expectedDateFormat.format(oxyBorrowersDeals.getLoanActiveDate()));

					if (oxyBorrowersDeals.getDealAcheivedDate() != null) {
						String lastParticipationDate = simpleDateFormat.format(oxyBorrowersDeals.getDealAcheivedDate());

						lenderPaticipatedResponseDto.setLastParticipationDate(lastParticipationDate);
					}

					OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
							.getFirstParticipationDate(dealId);

					if (oxyLendersAcceptedDeals != null) {

						String lastParticipationDate = simpleDateFormat.format(oxyLendersAcceptedDeals.getReceivedOn());
						lenderPaticipatedResponseDto.setFirstParticipationDate(lastParticipationDate);
					}
					lenderPaticipatedResponseDto.setDealType(oxyBorrowersDeals.getDealType().toString());

					OxyLendersAcceptedDeals oxylendersAccepted = oxyLendersAcceptedDealsRepo
							.toCheckUserAlreadyInvoledInDeal(userId, dealId);
					if (oxylendersAccepted != null) {
						lenderPaticipatedResponseDto
								.setAccountType(oxylendersAccepted.getPrincipalReturningAccountType().toString());

					}

					if (oxyBorrowersDeals.getDealParticipationStatus()
							.equals(OxyBorrowersDealsInformation.DealParticipationStatus.ACHIEVED)) {
						lenderPaticipatedResponseDto.setLenderPaticipateStatus(false);
					} else {
						if (oxyBorrowersDeals.getParticipationLenderType()
								.equals(OxyBorrowersDealsInformation.ParticipationLenderType.ANY)) {
							lenderPaticipatedResponseDto.setLenderPaticipateStatus(true);
						} else {
							List<OxyLendersAcceptedDeals> oxylendersAcceptedDealsList = oxyLendersAcceptedDealsRepo
									.findbyUserId(userId);

							if (oxylendersAcceptedDealsList != null && !oxylendersAcceptedDealsList.isEmpty()) {

								OxyLendersAcceptedDeals oxylendersAcceptedDeals = oxyLendersAcceptedDealsRepo
										.toCheckUserAlreadyInvoledInDeal(userId, dealId);

								if (oxylendersAcceptedDeals == null) {
									lenderPaticipatedResponseDto.setLenderPaticipateStatus(false);
								} else {
									lenderPaticipatedResponseDto.setLenderPaticipateStatus(true);
								}
							} else {
								lenderPaticipatedResponseDto.setLenderPaticipateStatus(true);
							}
						}

					}
					lenderPaticipatedResponseDto
							.setDealCreatedType(oxyBorrowersDeals.getParticipationLenderType().toString());
				}
				lenderPaticipatedResponseDto.setDealId(Integer.parseInt(e[0] == null ? "0" : e[0].toString()));
				lenderPaticipatedResponseDto.setGroupId(Integer.parseInt(e[1] == null ? "0" : e[1].toString()));
				Double dealAmount = 0.0;
				Double participationUpdated = lendersPaticipationUpdationRepo.getSumOfLenderUpdatedAmountToDeal(userId,
						dealId);
				if (participationUpdated != null) {
					dealAmount = Double.parseDouble(e[2] == null ? "0" : e[2].toString()) + participationUpdated;
				} else {
					dealAmount = Double.parseDouble(e[2] == null ? "0" : e[2].toString());
				}
				lenderPaticipatedResponseDto.setPaticipatedAmount(dealAmount);
				Double sumPricipalReturnedAmount = lendersReturnsRepo.getSumOfAmountReturned(userId, "LENDERPRINCIPAL",
						dealId);
				Double currentValue = 0.0;
				Double participationCurrentValue = 0.0;
				Double currentParticipation = 0.0;
				String currentStatus = null;
				if (sumPricipalReturnedAmount != null) {
					currentValue = dealAmount - sumPricipalReturnedAmount;
				} else {
					currentValue = dealAmount;
				}

				Double sumOfWithDramAmount = lendersReturnsRepo.getLenderWithdrawSumValue(userId, dealId);
				if (sumOfWithDramAmount != null) {
					participationCurrentValue = currentValue - sumOfWithDramAmount;
				} else {
					participationCurrentValue = currentValue;
				}

				Double sumOfWalletAmount = lenderOxyWalletNativeRepo.paticipatedAmountMovedToWalletSum(userId, dealId);
				if (sumOfWalletAmount != null) {
					currentParticipation = participationCurrentValue - sumOfWalletAmount;
				} else {
					currentParticipation = participationCurrentValue;
				}
				if (oxyBorrowersDeals.getOxyLoanRequestId() > 0) {
					LoanRequest loanRequest = loanRequestRepo.findById(oxyBorrowersDeals.getOxyLoanRequestId()).get();
					if (loanRequest != null) {
						OxyLoan oxyLoan = oxyLoanRepo.checkLoanDisbursedToLender(userId, loanRequest.getUserId(),
								dealId);
						if (oxyLoan != null) {
							lenderPaticipatedResponseDto.setLoanStatementValueCheck("Disbursed");

							lenderPaticipatedResponseDto.setLoanId(oxyLoan.getId());
						} else {
							lenderPaticipatedResponseDto.setLoanStatementValueCheck("NotYetDisbursed");

						}
					}

				} else if (sumPricipalReturnedAmount == null && sumOfWithDramAmount == null
						&& sumOfWalletAmount == null) {
					lenderPaticipatedResponseDto.setLoanStatementValueCheck(null);
					lenderPaticipatedResponseDto.setPricipalReturned("NotYetReturned");
				} else {
					lenderPaticipatedResponseDto.setLoanStatementValueCheck("notNull");
					lenderPaticipatedResponseDto.setPricipalReturned("Returned");
				}

				if (participationCurrentValue == 0) {
					currentStatus = "COMPLETED";
				} else {
					currentStatus = "RUNNING";
				}
				lenderPaticipatedResponseDto.setCurrentValue(BigDecimal.valueOf(currentParticipation).toBigInteger());
				lenderPaticipatedResponseDto.setCurrentStatus(currentStatus);
				lenderPaticipatedResponseDto.setProcessingFee(Double.parseDouble(e[3] == null ? "0" : e[3].toString()));
				lenderPaticipatedResponseDto.setFeeStatus(e[4] == null ? " " : e[4].toString());
				lenderPaticipatedResponseDto.setLederReturnType(e[5] == null ? " " : e[5].toString());
				OxyBorrowersDealsInformation oxyBorrowers = oxyBorrowersDealsInformationRepo
						.toGetCalculationType(dealId);
				double rateOfInterest = Double.valueOf(e[6] == null ? "0" : e[6].toString());
				DecimalFormat df = new DecimalFormat("#.##");
				if (oxyBorrowers != null) {
					lenderPaticipatedResponseDto.setRateOfInterest(rateOfInterest);
				} else {
					lenderPaticipatedResponseDto.setRateOfInterest(Double.parseDouble(df.format(rateOfInterest / 12)));
				}

				lenderPaticipatedResponseDto.setRegisteredDate(e[7] == null ? " " : e[7].toString());

				if (e[8].toString().equals(OxyLendersAcceptedDeals.ParticipationState.PARTICIPATED.toString())) {
					lenderPaticipatedResponseDto.setEditOptionConformation(e[8].toString());
				} else {
					lenderPaticipatedResponseDto.setEditOptionConformation(null);
				}
				listOfdeasToLender.add(lenderPaticipatedResponseDto);
			}

		}
		lenderPaticipatedDeal.setCount(count);
		lenderPaticipatedDeal.setLenderPaticipatedResponseDto(listOfdeasToLender);

		/*
		 * } catch (Exception e) {
		 * logger.info("Exception in getLenderPaticipatedDealsInformation method "); }
		 */
		return lenderPaticipatedDeal;

	}

	@Override
	public LenderPaticipatedDeal getListOfLendersByDealId(int dealId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public PayuMoneyPaymentDetaisResponce lenderFeeUpdation(String transactionNumber,
			PayuMoneyPaymentDetailsRequest request) {
		LenderPayuDetails lenderPayuDetails = lenderPayuDetailsRepo.findByTransactionNumber(transactionNumber);
		logger.info("transactionNumber for payU" + transactionNumber);
		Calendar currentDate = Calendar.getInstance();
		PayuMoneyPaymentDetaisResponce response = new PayuMoneyPaymentDetaisResponce();
		if (lenderPayuDetails != null) {

			LenderRenewalDetails details = lenderRenewalDetailsRepo
					.findingUserInTable(lenderPayuDetails.getLenderUserId());

			if (lenderPayuDetails.getAmount() >= 5900) {
				logger.info("paid 5900 or more it");
				if (request.getPayuStatus().equalsIgnoreCase("success")) {
					User user = userRepo.findById(lenderPayuDetails.getLenderUserId()).get();

					if (user != null) {
						if (details != null) {
							user.setLenderGroupId(user.getLenderGroupId());
						} else {
							user.setLenderGroupId(7);
						}
					}
					// user.setLenderGroupId(7);
					userRepo.save(user);

					lenderPayuDetails.setPaymentDate(currentDate.getTime());
					lenderPayuDetails
							.setStatus(request.getPayuStatus().equalsIgnoreCase("success") ? PayuStatus.COMPLETED
									: PayuStatus.PENDING);
					lenderPayuDetails.setPayuTransactionNumber(request.getPayuTransactionNumber());

					response.setAmount(lenderPayuDetails.getAmount().toString());
					response.setStatus(request.getPayuStatus().equalsIgnoreCase("success") ? PayuStatus.COMPLETED.name()
							: PayuStatus.PENDING.name());
					response.setId(lenderPayuDetails.getLenderUserId());

					response.setLenderFeeId(lenderPayuDetails.getId());

					lenderPayuDetailsRepo.save(lenderPayuDetails);

					if (details != null) {

						if (lenderPayuDetails.getStatus().equals(LenderPayuDetails.PayuStatus.COMPLETED)
								&& lenderPayuDetails.getPaymentDate() != null) {
							String paymentReceivedOn = "";
							SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
							if (details.getPaymentReceivedOn() != null) {
								paymentReceivedOn = f.format(details.getPaymentReceivedOn());
							}
							String paymentDate = f.format(lenderPayuDetails.getPaymentDate());
							if (details.getPaymentReceivedOn() == null || !paymentReceivedOn.equals(paymentDate)) {
								if (lenderPayuDetails.getPaymentDate().compareTo(details.getValidityDate()) < 0) {

									details.setPaymentDate(details.getValidityDate());
								} else {
									details.setPaymentDate(lenderPayuDetails.getPaymentDate());
								}
								details.setAmount(lenderPayuDetails.getAmount());
								details.setRenewalStatus(LenderRenewalDetails.RenewalStatus.Renewed);
								details.setPaymentReceivedOn(new Date());
								lenderRenewalDetailsRepo.save(details);

								Calendar c = Calendar.getInstance();

								c.setTime(details.getPaymentDate());

								c.add(Calendar.YEAR, 1);
								Date newDate = c.getTime();

								String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(newDate);

								User lender = userRepo.findByIdNum(details.getUser_id());
								String msgToLender = "Dear *" + lender.getPersonalDetails().getFirstName().trim()
										+ "(LR" + lender.getId() + ")" + "*" + "\n" + "\nGreetings from Oxyloans!"
										+ "\n"
										+ "we are happy to inform you that your oxyloans  membership validity is extended till"
										+ " *" + modifiedDate + "* from *" + details.getValidityDate() + "*. \n\n"
										+ "Please write to us if you have any concerns using the below link:" + "\n"
										+ "https://www.oxyloans.com/new/lenderInquiries" + "\n\n" + "Thanks"
										+ "\nOxyloans Team";

								String mobileNumber = "";
								if (lender != null) {
									if (lender.getPersonalDetails().getWhatsAppNumber() != null) {
										mobileNumber = lender.getPersonalDetails().getWhatsAppNumber();
									} else {
										mobileNumber = "91" + lender.getMobileNumber();
									}

								}

								whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, msgToLender);

								String mailsubject = "Membership Validity Is Renewed";
								TemplateContext context = new TemplateContext();

								String lenderName = "";
								if (lender.getPersonalDetails().getFirstName() != null) {
									lenderName = lender.getPersonalDetails().getFirstName();
								} else {
									lenderName = "";
								}

								LocalDate currentDate1 = LocalDate.now();

								context.put("currentDate", currentDate1);
								context.put("name", lenderName);
								context.put("modifiedDate", modifiedDate);
								context.put("validityDate", details.getValidityDate());

								EmailRequest emailRequest = new EmailRequest(
										new String[] { lender.getEmail(), "archana.n@oxyloans.com",
												"ramadevi@oxyloans.com" },
										"lender_renewal_updation.template", context, mailsubject, "Oxyloans");

								EmailResponse emailResponse = emailService.sendEmail(emailRequest);

								if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
									try {
										throw new MessagingException(emailResponse.getErrorMessage());
									} catch (MessagingException e2) {

										e2.printStackTrace();
									}
								}
							}
						}
					} else {
						if (lenderPayuDetails.getStatus().equals(LenderPayuDetails.PayuStatus.COMPLETED)
								&& lenderPayuDetails.getPaymentDate() != null) {

							LenderRenewalDetails renewal = lenderRenewalDetailsRepo
									.findingUser(lenderPayuDetails.getLenderUserId());

							if (renewal == null) {
								LenderRenewalDetails renewalDetails = new LenderRenewalDetails();

								Calendar c = Calendar.getInstance();
								Date newDate = null;
								c.setTime(lenderPayuDetails.getPaymentDate());
								c.add(Calendar.YEAR, 1);
								newDate = c.getTime();

								renewalDetails.setUser_id(lenderPayuDetails.getLenderUserId());
								renewalDetails.setValidityDate(newDate);
								renewalDetails.setRenewalStatus(LenderRenewalDetails.RenewalStatus.NotYetRenewed);
								renewalDetails.setPaymentReceivedOn(new Date());
								lenderRenewalDetailsRepo.save(renewalDetails);
							}
						}
					}

					/*
					 * List<OxyLendersAcceptedDeals> lenderAcceptedDealsList =
					 * oxyLendersAcceptedDealsRepo
					 * .findbyUserId(lenderPayuDetails.getLenderUserId()); if
					 * (lenderAcceptedDealsList != null && !lenderAcceptedDealsList.isEmpty()) { for
					 * (OxyLendersAcceptedDeals lenderAccepetdDeal : lenderAcceptedDealsList) {
					 * String lenderReturnType =
					 * lenderAccepetdDeal.getLenderReturnsType().toString(); int dealId =
					 * lenderAccepetdDeal.getDealId(); int groupId = 2;
					 * OxyDealsRateofinterestToLendersgroup oxyDealsRateofinterestToLenders =
					 * oxyDealsRateofinterestToLendersgroupRepo .getDealMappedToSingleGroup(dealId,
					 * groupId); Double rateOfInterest = 0.0; if (oxyDealsRateofinterestToLenders !=
					 * null) { if (lenderReturnType
					 * .equals(OxyLendersAcceptedDeals.LenderReturnsType.MONTHLY.toString())) {
					 * rateOfInterest = oxyDealsRateofinterestToLenders.getMonthlyInterest(); } if
					 * (lenderReturnType
					 * .equals(OxyLendersAcceptedDeals.LenderReturnsType.QUARTELY.toString())) {
					 * rateOfInterest = oxyDealsRateofinterestToLenders.getQuartelyInterest(); } if
					 * (lenderReturnType
					 * .equals(OxyLendersAcceptedDeals.LenderReturnsType.HALFLY.toString())) {
					 * rateOfInterest = oxyDealsRateofinterestToLenders.getHalfInterest(); } if
					 * (lenderReturnType
					 * .equals(OxyLendersAcceptedDeals.LenderReturnsType.YEARLY.toString())) {
					 * rateOfInterest = oxyDealsRateofinterestToLenders.getYearlyInterest(); } if
					 * (lenderReturnType
					 * .equals(OxyLendersAcceptedDeals.LenderReturnsType.ENDOFTHEDEAL.toString())) {
					 * rateOfInterest = oxyDealsRateofinterestToLenders.getEndofthedealInterest(); }
					 * } lenderAccepetdDeal.setRateofinterest(rateOfInterest);
					 * lenderAccepetdDeal.setGroupId(2); lenderAccepetdDeal.setFeeStatus(
					 * request.getPayuStatus().equalsIgnoreCase("success") ? FeeStatus.COMPLETED :
					 * FeeStatus.PENDING); oxyLendersAcceptedDealsRepo.save(lenderAccepetdDeal); } }
					 */

				}
			} else {
				logger.info("paid less than 5900");
				/*
				 * OxyLendersAcceptedDeals accepetedDeals =
				 * oxyLendersAcceptedDealsRepo.toCheckUserAlreadyInvoledInDeal(
				 * lenderPayuDetails.getLenderUserId(), lenderPayuDetails.getDealId());
				 */
				// if (accepetedDeals != null) {
				/*
				 * accepetedDeals
				 * .setFeeStatus(request.getPayuStatus().equalsIgnoreCase("success") ?
				 * FeeStatus.COMPLETED : FeeStatus.PENDING);
				 */
				lenderPayuDetails.setPaymentDate(currentDate.getTime());
				lenderPayuDetails.setStatus(request.getPayuStatus().equalsIgnoreCase("success") ? PayuStatus.COMPLETED
						: PayuStatus.PENDING);
				lenderPayuDetails.setPayuTransactionNumber(request.getPayuTransactionNumber());

				response.setAmount(lenderPayuDetails.getAmount().toString());
				response.setStatus(request.getPayuStatus().equalsIgnoreCase("success") ? PayuStatus.COMPLETED.name()
						: PayuStatus.PENDING.name());
				response.setId(lenderPayuDetails.getLenderUserId());

				response.setLenderFeeId(lenderPayuDetails.getId());

				lenderPayuDetailsRepo.save(lenderPayuDetails);
				// oxyLendersAcceptedDealsRepo.save(accepetedDeals);
				// }

			}

		} else {
			throw new NoSuchElementException("No records present with oxyLoansTransactionNumber " + transactionNumber);
		}
		return response;
	}

	@Override
	public DealLevelResponseDto dealIdBasedAgreementsGeneration(int dealId, DealLevelRequestDto dealLevelRequestDto)
			throws PdfGeenrationException, IOException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public WhatappInformation getWhatappGroupsInformation(String type) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderReferenceDetails toEditTheLenderReferenceInformation(Integer refereeId, Integer referrerId,
			LenderReferenceRequestDto requestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public ScrowWalletResponse gettingLenderSrowWalletTransactions(int id, PaginationRequestDto pageRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public List<OxyLendersGroup> gettingListOfOxyLendersGroups() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	/*
	 * @Override public CommentsResponseDto updatingLenderToLendersGroup(Integer
	 * userId, OxyLendersGroups oxyLenderGroup) { throw new
	 * OperationNotAllowedException("You are not authorized to do this operation",
	 * ErrorCodes.PERMISSION_DENIED); }
	 */

	@Override
	public LenderMappedToGroupIdResponseDto gettingListOfLendersMappedToGroupName(OxyLendersGroups groupName) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanEmiCardResponseDto loanPreCloseByPlatForm(int loanId) {
		throw new OperationNotAllowedException("Not Allowed to comment at present", ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	@Transactional
	public SearchResultsDto<LoanResponseDto> searchEnachMandate(int userId, SearchRequestDto searchRequestDto) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		Sort sort = null;
		if (searchRequestDto.getSortBy() != null) {
			sort = Sort.by(Direction.valueOf(searchRequestDto.getSortOrder().name()), searchRequestDto.getSortBy());
		} else {
			sort = Sort.by(Direction.DESC, "loanActiveDate");
		}
		Pageable searchPage = PageRequest.of(searchRequestDto.getPage().getPageNo() - 1,
				searchRequestDto.getPage().getPageSize(), sort);
		Specification<OxyLoan> spec = null;
		try {
			spec = specificationProvider.construct(OxyLoan.class, searchRequestDto);
		} catch (ClassNotFoundException e) {
			logger.error(e, e);
		} catch (IllegalAccessException e) {
			logger.error(e, e);
		} catch (InvocationTargetException e) {
			logger.error(e, e);
		} catch (NoSuchMethodException e) {
			logger.error(e, e);
		} catch (InstantiationException e) {
			logger.error(e, e);
		}
		Page<OxyLoan> findAll = oxyLoanRepo.findAll(spec, searchPage);
		SearchResultsDto<LoanResponseDto> results = new SearchResultsDto<LoanResponseDto>();
		if (findAll != null) {
			results.setTotalCount(Long.valueOf(findAll.getTotalElements()).intValue());
			results.setPageNo(searchRequestDto.getPage().getPageNo());
			results.setPageCount(findAll.getNumberOfElements());
			List<LoanResponseDto> loanRequests = new ArrayList<LoanResponseDto>();
			;
			results.setResults(loanRequests);
			for (OxyLoan request : findAll.getContent()) {
				int count = 0;

				int size = 0;

				LoanRequest loanRequest = loanRequestRepo
						.findByUserIdAndParentRequestIdIsNull(request.getBorrowerUserId());

				if (loanRequest != null) {

					if (loanRequest.getRepaymentMethodForBorrower().equalsIgnoreCase(RepaymentMethod.PI.name())) {
						size = 1;
					} else if (loanRequest.getRepaymentMethodForBorrower().equalsIgnoreCase(RepaymentMethod.I.name())) {
						size = 2;
					}
				}

				for (int i = 1; i <= size; i++) {
					loanRequests.add(populateLoanDetails(request, count));
					count += 1;

				}
			}
		}
		return results;
	}

	@Override
	public List<PaymentUploadHistoryResponseDto> readingWhatsAppPaymentScreenshots(WhatsappMessagesDto request)
			throws IOException, ParseException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public ReadingCommitmentAmountDto readingCommittmentAmount(BorrowersDealsRequestDto borrowersDealsRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderMappedToGroupIdResponseDto gettingLenderCurrentWalletBalanceInfo(OxyLendersGroups oxyLendersGroups) {

		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderMappedToGroupIdResponseDto gettingLenderCurrentWalletBalanceExcelSheet(
			OxyLendersGroups oxyLendersGroups) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public WhatappInformation gettingListOfDealsNames() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public WhatsappCampaignDto sendwhatsAppCampaignMessage(WhatsappCampaignDto whatsappCampaignDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public EnachScheduledMailResponseDto sendingMailToReferrerAboutReference1() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public EquityLendersListsDto lendersInAllEquityDeals() {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public com.oxyloans.response.user.StatusResponseDto getBorrowersMsgContent(String type) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public com.oxyloans.response.user.StatusResponseDto sendNotoficationsToBorrowerFromAdmin(
			SpreadSheetRequestDto request) throws IOException, GeneralSecurityException, InterruptedException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public com.oxyloans.response.user.StatusResponseDto getUnsentBorrowerNotifications(SpreadSheetRequestDto request)
			throws GeneralSecurityException, IOException, InterruptedException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public com.oxyloans.response.user.StatusResponseDto writeNotificationsResponseMessages(
			SpreadSheetRequestDto request)
			throws GeneralSecurityException, IOException, InterruptedException, ParseException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public WhatappInformation gettingListOfWhatsAppGroupNamesBasedOnGroupName(String groupName) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	public Double getLenderCurrentWalletBalance(int userId) {
		Double lenderCurrentWalletBalance = 0.0;
		Double holdAmount = 0.0;
		Double dealValue = 0.0;
		Double dealValueWithAgreements = 0.0;
		Double equityAmount = 0.0;
		Double oldWalletAmount = 0.0;
		Double dealAmount = oxyLendersAcceptedDealsRepo.getDealParticipationAmountIncludingClosedDeals(userId);
		Double dealUpdatedAmount = lendersPaticipationUpdationRepo
				.getSumOfLenderUpdatedAmountButNotInEquityIncludingClosedDeals(userId);
		if (dealAmount != null) {
			if (dealUpdatedAmount != null) {
				dealValue = dealAmount + dealUpdatedAmount;
			} else {
				dealValue = dealAmount;
			}
		}

		Double dealAmountWithAgreements = oxyLendersAcceptedDealsRepo.getDealParticipationAmountAfterAgreements(userId);
		if (dealAmountWithAgreements != null) {
			dealValueWithAgreements = dealAmountWithAgreements;
		}
		holdAmount = dealValue - dealValueWithAgreements;

		Double equityValue = oxyLendersAcceptedDealsRepo.getDealParticipationAmountForEquity(userId);
		Double dealUpdatedAmountInEquity = lendersPaticipationUpdationRepo.getSumOfLenderUpdatedAmountInEquity(userId);
		if (equityValue != null) {
			if (dealUpdatedAmountInEquity != null) {
				equityAmount = equityValue + dealUpdatedAmountInEquity;
			} else {
				equityAmount = equityValue;
			}
		}
		Double lenderWalletCreditAmount = lenderOxyWalletNativeRepo.lenderWalletCreditAmount(userId);
		Double lenderWalletdebitAmount = lenderOxyWalletNativeRepo.lenderWalletDebitAmount(userId);
		Double lenderWalletInprocessAmount = lenderOxyWalletNativeRepo.lenderWalletInprocessAmount(userId);

		if (!(lenderWalletInprocessAmount != null && lenderWalletInprocessAmount > 0)) {
			lenderWalletInprocessAmount = 0d;
		}
		if (!(lenderWalletCreditAmount != null && lenderWalletCreditAmount > 0)) {
			lenderWalletCreditAmount = 0d;
		}
		if (!(lenderWalletdebitAmount != null && lenderWalletdebitAmount > 0)) {
			lenderWalletdebitAmount = 0d;
		}
		Double lenderWalletAmount = lenderWalletCreditAmount - lenderWalletdebitAmount;
		if (lenderWalletAmount != null && lenderWalletAmount > 0) {
			oldWalletAmount = lenderWalletAmount - lenderWalletInprocessAmount;
		} else {
			oldWalletAmount = 0.0;
		}

		lenderCurrentWalletBalance = oldWalletAmount - holdAmount - equityAmount;
		return lenderCurrentWalletBalance;

	}

	@Override
	public LenderPaticipatedDeal getListOfLendersByDealIdForExcelSheet(int dealId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderReferenceResponse readingLenderReferenceToSheet() throws ParseException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public DealLevelResponseDto dealIdBasedAgreementsGenerationForZaggle(int dealId,
			DealLevelRequestDto dealLevelRequestDto) throws PdfGeenrationException, IOException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LoanResponseDto uploadEsignedAgreementPdfforDeal(int userId, Integer loanRequestId,
			UploadAgreementRequestDto agreementFileRequest) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	@Transactional
	public LoanResponseDto updateLoanRequestNewMethod(int userId, LoanRequestDto loanRequestDto) {
		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		Integer loanRequestId = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId()).getId();
		return updatingUserRequest(userId, loanRequestId, loanRequestDto, false);
	}

	private LoanResponseDto updatingUserRequest(int userId, Integer loanRequestId, LoanRequestDto loanRequestDto,
			boolean overrideCommitementAmount) {
		LoanRequest loanRequest = loanRequestRepo.findById(loanRequestId).get();
		if (loanRequest.getLoanOfferedAmount() == null) {
			if (loanRequestDto.getStatus().equalsIgnoreCase("Edit")
					|| loanRequestDto.getStatus().equalsIgnoreCase("Requested")) {
				if (loanRequestDto.getLoanRequestAmount() != null) {
					// if (overrideCommitementAmount) {
					loanRequest.setLoanRequestAmount(loanRequestDto.getLoanRequestAmount());
					// }
				}
				if (loanRequestDto.getDuration() != null) {
					loanRequest.setDuration(loanRequestDto.getDuration());
				}
				if (loanRequestDto.getLoanPurpose() != null) {
					loanRequest.setLoanPurpose(loanRequestDto.getLoanPurpose());
				}
				if (loanRequestDto.getRateOfInterest() != null) {
					loanRequest.setRateOfInterest(loanRequestDto.getRateOfInterest());
				}
				if (loanRequestDto.getRepaymentMethod() != null) {
					loanRequest.setRepaymentMethod(RepaymentMethod.valueOf(loanRequestDto.getRepaymentMethod()));
				}
				if (loanRequestDto.getExpectedDate() != null) {
					try {
						loanRequest.setExpectedDate(expectedDateFormat.parse(loanRequestDto.getExpectedDate()));
					} catch (ParseException e) {
						logger.error("Error: ", e);
						throw new DataFormatException("Illegal Date format. It should be dd/MM/yyyy",
								ErrorCodes.INVALID_DATE_FORMAT);
					}
				}
				if (loanRequestDto.getDurationType().equalsIgnoreCase("months")) {
					loanRequest.setDurationType(DurationType.Months);
				} else if (loanRequestDto.getDurationType().equalsIgnoreCase("days")) {
					loanRequest.setDurationType(DurationType.Days);
				}
				loanRequest.setLoanStatus(LoanRequest.LoanStatus.valueOf(loanRequestDto.getStatus()));
			} else if (loanRequestDto.getStatus().equalsIgnoreCase("Hold")) {
				loanRequest.setLoanStatus(LoanRequest.LoanStatus.Hold);
			} else {
				loanRequest.setLoanStatus(LoanRequest.LoanStatus.Rejected);
			}
			loanRequest = loanRequestRepo.save(loanRequest);
		} else {
			throw new OperationNotAllowedException("admin sent an offer now you can not change your loan request",
					ErrorCodes.ACTION_ALREDY_DONE);
		}
		LoanResponseDto loanResponseDto = new LoanResponseDto();
		loanResponseDto.setUserId(userId);
		loanResponseDto.setLoanRequestId(loanRequest.getId());
		loanResponseDto.setLoanRequest(loanRequest.getLoanRequestId());
		return loanResponseDto;
	}

	@Override
	public LenderMappedToGroupIdResponseDto gettingLenderCurrentWalletBalanceExcelSheetTwo(
			OxyLendersGroups oxyLendersGroups) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	public Double currentDealAmount(int dealId) {
		Double currentAmount = 0.0;
		OxyBorrowersDealsInformation OxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
				.findDealAlreadyGiven(dealId);
		Double firstPaticipationSum = oxyLendersAcceptedDealsRepo.getListOfLendersParticipatedAmount(dealId);
		Double secondPaticipationSum = lendersPaticipationUpdationRepo.getDealUpdatedSumValue(dealId);
		if (firstPaticipationSum == null && secondPaticipationSum == null) {
			currentAmount = OxyBorrowersDealsInformation.getDealAmount();
		} else if (firstPaticipationSum != null && secondPaticipationSum == null) {
			currentAmount = (OxyBorrowersDealsInformation.getDealAmount() - firstPaticipationSum);
		} else if (firstPaticipationSum != null && secondPaticipationSum != null) {
			currentAmount = (OxyBorrowersDealsInformation.getDealAmount()
					- (firstPaticipationSum + secondPaticipationSum));
		}
		return currentAmount;
	}

	@Override
	@Transactional
	public EsignInitResponse esignForApplicationLevel(Integer userId, Integer applicationId,
			UploadAgreementRequestDto agreementFileRequest) {

		logger.info("ESIGN started");

		User user = userRepo.findById(userId).get();
		authorizationService.hasPermission(user);
		LoanRequest oxyLoanRequest = loanRequestRepo.findById(applicationId).get();

		String appId = "APBR" + applicationId;
		Integer noOfLoansForApplication = oxyLoanRepo.findingNoOfLoansForApp(appId);

		Integer noOfLoansActiveForApplication = oxyLoanRepo.findingNoOfLoansActiveForApp(appId);

		EsignInitResponse esignResponse = null;

		if (noOfLoansForApplication == noOfLoansActiveForApplication) {
			throw new ActionNotAllowedException("You had esigned this document already", ErrorCodes.ACTION_ALREDY_DONE);
		} else {

			if (oxyLoanRequest == null) {
				logger.info("NO LOAN PRESENT" + applicationId);
				throw new NoSuchElementException("No Loan present to esign against");
			}

			// String appId = "APBR" + applicationId;
			List<OxyLoan> finalLoan = oxyLoanRepo.findByLoanRequestIdAndLoanRespondIdAndApplicationId(appId);

			String esignTypeString = agreementFileRequest.geteSignType();
			EsignType esignType = (esignTypeString == null) ? EsignType.AADHAR : EsignType.valueOf(esignTypeString);

			EsignInitRequest esignRequest = new EsignInitRequest();
			switch (esignType) {

			case EsignType.MOBILE:
				Random random = new Random();
				String otpValue = String.format("%04d", random.nextInt(10000));
				String otpSessionId = mobileUtil.sendCustomOtp(user.getMobileNumber(), otpValue,
						this.esignTemplateName);
				esignResponse = new EsignInitResponse();
				esignResponse.setId(otpSessionId);

				break;
			}
			EsignTransactions esignTransaction = new EsignTransactions();
			esignTransaction.setEsignType(esignType);
			esignTransaction.setTransactionId(esignResponse.getId());
			esignTransaction.setCoOrdinateX(
					esignRequest.getxCoordinate() == null ? -1 : Integer.valueOf(esignRequest.getxCoordinate()));
			esignTransaction.setCoOrdinateY(
					esignRequest.getyCoordinate() == null ? -1 : Integer.valueOf(esignRequest.getyCoordinate()));
			esignTransaction.setEsignedDocDownloaded(false);
			esignTransaction.setTransactonStatus(TransactonStatus.INITIATED);
			esignTransaction = esignTransactionsRepo.save(esignTransaction);
			if (finalLoan != null && !finalLoan.isEmpty()) {
				for (OxyLoan oxyLoan : finalLoan) {

					if (user.getPrimaryType() == PrimaryType.LENDER) {
						oxyLoan.setLenderEsignId(esignTransaction.getId());
					} else {
						oxyLoan.setBorrowerEsignId(esignTransaction.getId());
						oxyLoan.setLenderEsignId(0000); // done by manual process
					}
					oxyLoan = oxyLoanRepo.save(oxyLoan);

					logger.info("esign response before" + applicationId);
				}
			}
			esignResponse.setWebhook_security_key(null);

		}

		return esignResponse;

	}

	@Override
	@Transactional
	public LoanResponseDto uploadEsignedAgreementPdfforDealInApplicationLevel(int userId, Integer applicationId,
			UploadAgreementRequestDto agreementFileRequest) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	@Transactional
	public List<ApplicationLevelEnachResponseDto> searchEnachMandateApplicationLevel(int userId) {

		List<ApplicationLevelEnachResponseDto> response = new ArrayList<ApplicationLevelEnachResponseDto>();

		User user = userRepo.findByIdNum(userId);

		if (user != null) {
			LoanRequest loanRequest = loanRequestRepo.findByUserIdAndParentRequestIdIsNull(user.getId());

			if (loanRequest != null) {
				List<ApplicationLevelEnachMandate> enach = applicationLevelEnachMandateRepo
						.findByApplicationIdToActivateEnach(loanRequest.getId());

				if (enach != null && !enach.isEmpty()) {
					for (ApplicationLevelEnachMandate appLevelEnach : enach) {
						ApplicationLevelEnachResponseDto enachApplication = new ApplicationLevelEnachResponseDto();
						enachApplication.setName(user.getPersonalDetails().getFirstName() + " "
								+ user.getPersonalDetails().getLastName());

						enachApplication.setApplicationId(("APBR" + appLevelEnach.getApplicationId()) == null ? ""
								: ("APBR" + appLevelEnach.getApplicationId()));

						enachApplication.setAmountToActivate(appLevelEnach.getMaxAmount());
						enachApplication.setDuration(loanRequest.getDurationBySir());
						enachApplication.setRateOfInterest(loanRequest.getRateOfInterestToBorrower());
						enachApplication.setType(appLevelEnach.getIsPrincialOrInterest());
						if (appLevelEnach.getMandateStatus() != null) {
							enachApplication.setEnachMandateStatus(appLevelEnach.getMandateStatus().toString());
						} else {
							enachApplication.setEnachMandateStatus("");
						}
						if (appLevelEnach.getMandateStatus() != null) {
							if (appLevelEnach
									.getMandateStatus() == ApplicationLevelEnachMandate.MandateStatus.SUCCESS) {
								enachApplication.setEnachActivatedStatus(true);
							} else {
								enachApplication.setEnachActivatedStatus(false);
							}
						}

						enachApplication.setId(appLevelEnach.getId());

						response.add(enachApplication);
					}
				}

			}
		}

		return response;

	}

	@Override
	public String updatingAmountBasedOnDisbursment(Integer applicationId,
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public String sendingEmailNotifiaction(int dealId) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public String autoPaticiaption(int dealId) {
		OxyBorrowersDealsInformation borrowersDealsRequestDto = oxyBorrowersDealsInformationRepo
				.findDealAlreadyGiven(dealId);
		if (borrowersDealsRequestDto != null) {

			if ((borrowersDealsRequestDto.getDealType() == OxyBorrowersDealsInformation.DealType.NORMAL)
					|| (borrowersDealsRequestDto.getDealType() == OxyBorrowersDealsInformation.DealType.ESCROW)
					|| borrowersDealsRequestDto.getDealType() == OxyBorrowersDealsInformation.DealType.TEST) {
				List<LenderWalletHistoryResponseDto> listOflenders = lenderAutoInvestConfigServive.autoPaticipation(
						borrowersDealsRequestDto.getDealType().toString(),
						borrowersDealsRequestDto.getMinimumPaticipationAmount(),
						borrowersDealsRequestDto.getFeeStatusToParticipate().toString());
				if (listOflenders != null && !listOflenders.isEmpty()) {
					listOflenders.forEach(lenders -> {
						int lenderId = lenders.getUserId();
						String accountType = lenders.getPrincipalReturnAccountType();
						OxyLendersAcceptedDealsRequestDto oxyLendersAcceptedDealsRequestDto = new OxyLendersAcceptedDealsRequestDto();
						oxyLendersAcceptedDealsRequestDto.setUserId(lenders.getUserId());
						User user = userRepo.findById(lenders.getUserId()).get();
						if (user != null) {
							oxyLendersAcceptedDealsRequestDto.setGroupId(user.getLenderGroupId());
						}
						oxyLendersAcceptedDealsRequestDto.setDealId(dealId);
						oxyLendersAcceptedDealsRequestDto
								.setParticipatedAmount(borrowersDealsRequestDto.getMinimumPaticipationAmount());
						oxyLendersAcceptedDealsRequestDto
								.setLenderReturnType(OxyLendersAcceptedDeals.LenderReturnsType.MONTHLY.toString());
						oxyLendersAcceptedDealsRequestDto.setLenderFeeId(0);
						// oxyLendersAcceptedDealsRequestDto.setLenderReturnType(accountType);
						oxyLendersAcceptedDealsRequestDto
								.setPaticipationState(OxyLendersAcceptedDeals.PaticipationState.AUTOLEND.toString());
						lenderAutoPaticipation(oxyLendersAcceptedDealsRequestDto);

					});

				}
			}
		}
		return "success";

	}

	public OxyLendersAcceptedDealsResponseDto lenderAutoPaticipation(
			OxyLendersAcceptedDealsRequestDto oxyLendersAcceptedDealsRequestDto) {
		logger.info("lenderAutoPaticipation method start");

		int exceptionCount = 0;
		String message = null;
		Integer userId = oxyLendersAcceptedDealsRequestDto.getUserId();
		Integer dealId = oxyLendersAcceptedDealsRequestDto.getDealId();

		User user = userRepo.findById(userId).get();
		Integer id = oxyLendersAcceptedDealsRequestDto.getDealId();
		OxyBorrowersDealsInformation oxyBorrowersDealsInformation = oxyBorrowersDealsInformationRepo
				.findDealAlreadyGiven(id);
		logger.info("oxyBorrowersDealsInformation" + id);

		Double currentWalletAmount = getLenderCurrentWalletBalance(oxyLendersAcceptedDealsRequestDto.getUserId());
		logger.info("currentWalletAmount" + oxyLendersAcceptedDealsRequestDto.getUserId());

		if (currentWalletAmount <= 0) {
			logger.info("You are wallet is not having sufficient amount to paticipate in the deal in Auto-lending : "
					+ ErrorCodes.LOWER_AMOUNT + "userId :" + userId);
			exceptionCount += 1;
		}
		if (oxyLendersAcceptedDealsRequestDto.getParticipatedAmount() != null) {
			if (currentWalletAmount < oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()) {
				logger.info(
						"You are wallet is not having sufficient amount to paticipate in the deal load your wallet : "
								+ ErrorCodes.LOWER_AMOUNT + "userId :" + userId);
				exceptionCount += 1;

			}
		}
		Double processingFee = 0.0;
		Double totalPaticipationAmount = 0.0;

		// only new lender participation validation starts
		DecimalFormat myFormat = new DecimalFormat("#,###");
		if (oxyBorrowersDealsInformation != null) {

			if (oxyBorrowersDealsInformation.getParticipationLenderType().equals(ParticipationLenderType.NEW)) {

				List<OxyLendersAcceptedDeals> oxylendersAcceptedDealsList = oxyLendersAcceptedDealsRepo
						.findbyUserId(userId);

				if (oxylendersAcceptedDealsList != null && !oxylendersAcceptedDealsList.isEmpty()) {

					OxyLendersAcceptedDeals oxylendersAcceptedDeals = oxyLendersAcceptedDealsRepo
							.toCheckUserAlreadyInvoledInDeal(userId, dealId);

					logger.info("oxylendersAcceptedDeals:" + userId, dealId);

					if (oxylendersAcceptedDeals == null) {
						logger.info(
								"Newly registered users or Existing users who haven't participated yet in any deal can only participate in this deal : "
										+ ErrorCodes.PERMISSION_DENIED + "userId :" + userId);
						exceptionCount += 1;
					}
				}

			} // only new lender participation validation ends

			// lender fee validation starts

			String lenderFeeStatus = "";

			if (oxyLendersAcceptedDealsRequestDto.getLenderFeeId() != 0) {
				LenderPayuDetails lenderFeeDetails = lenderPayuDetailsRepo
						.findById(oxyLendersAcceptedDealsRequestDto.getLenderFeeId()).get();

				if (lenderFeeDetails != null) {

					lenderFeeStatus = lenderFeeDetails.getStatus().toString();
					if (!lenderFeeDetails.getStatus().equals(PayuStatus.COMPLETED)) {

						logger.info("Lender fee pending" + ErrorCodes.PERMISSION_DENIED + "userId :" + userId);
						exceptionCount += 1;

					}
				} else {
					logger.info("Lender fee pending" + ErrorCodes.PERMISSION_DENIED + "userId :" + userId);
					exceptionCount += 1;
				}

			}
			// lender fee validation ends

			Double totalDealAmount = oxyLendersAcceptedDealsRepo.getListOfLendersParticipatedAmount(id);
			logger.info("totalDealAmount" + id);

			Double lenderUpdatedAmount = lendersPaticipationUpdationRepo.getDealUpdatedSumValue(id);
			logger.info("lenderUpdatedAmount" + id);

			if (totalDealAmount != null) {
				if (lenderUpdatedAmount == null) {
					totalPaticipationAmount = totalDealAmount;
				} else {
					totalPaticipationAmount = totalDealAmount + lenderUpdatedAmount;
				}

			}

			OxyLendersAcceptedDeals oxyLendersAccepted = oxyLendersAcceptedDealsRepo
					.toCheckUserAlreadyInvoledInDeal(userId, dealId);

			logger.info("oxyLendersAccepted" + userId, dealId);

			double maxmumLimit = 0.0;
			int closedDealsCount = 0;
			List<Integer> listOfActiveDealsIds = oxyBorrowersDealsInformationRepo.getListOfActiveDealIds(userId);

			logger.info("listOfActiveDealsIds:" + userId);

			if (listOfActiveDealsIds != null && !listOfActiveDealsIds.isEmpty()) {
				for (int i = 0; i < listOfActiveDealsIds.size(); i++) {
					int idOfDeal = listOfActiveDealsIds.get(i);
					Double participationSum = 0.0;

					Double firstParticipationAmount = oxyLendersAcceptedDealsRepo
							.getDealParticipationAmountToLender(userId, idOfDeal);

					logger.info("firstParticipationAmount:" + userId, idOfDeal);

					Double participationUpdatedSum = lendersPaticipationUpdationRepo
							.getSumOfLenderUpdatedAmountToDeal(userId, idOfDeal);
					Double sumPricipalReturnedAmount = lendersReturnsRepo.getSumOfAmountReturned(userId,
							"LENDERPRINCIPAL", idOfDeal);
					Double sumOfWithDramAmount = lendersReturnsRepo.getLenderWithdrawSumValue(userId, idOfDeal);
					Double currentValue = 0.0;
					Double participationCurrentValue = 0.0;
					if (firstParticipationAmount != null) {
						if (participationUpdatedSum != null) {
							participationSum = firstParticipationAmount + participationUpdatedSum;
						} else {
							participationSum = firstParticipationAmount;
						}
					}
					if (sumPricipalReturnedAmount != null) {
						currentValue = participationSum - sumPricipalReturnedAmount;
					} else {
						currentValue = participationSum;
					}
					if (sumOfWithDramAmount != null) {
						participationCurrentValue = currentValue - sumOfWithDramAmount;
					} else {
						participationCurrentValue = currentValue;
					}

					Double currentParticipation = 0.0;
					Double sumOfAmountReturnedToWallet = lenderOxyWalletNativeRepo
							.paticipatedAmountMovedToWalletSum(userId, idOfDeal);
					if (sumOfAmountReturnedToWallet != null) {
						currentParticipation = participationCurrentValue - sumOfAmountReturnedToWallet;
					} else {
						currentParticipation = participationCurrentValue;
					}

					Double current = 0.0;
					Double holdAmount = userHoldAmountMappedToDealRepo.principalDebited(userId, dealId);
					if (holdAmount != null) {
						current = currentParticipation - holdAmount;
					} else {
						current = currentParticipation;
					}

					OxyBorrowersDealsInformation oxyBorrowersDeals = oxyBorrowersDealsInformationRepo
							.findDealAlreadyGiven(idOfDeal);

					logger.info("oxyBorrowersDeals" + idOfDeal);

					if (oxyBorrowersDeals != null) {
						if (oxyBorrowersDeals.getBorrowerclosingstatus()
								.equals(OxyBorrowersDealsInformation.BorrowerClosingStatus.CLOSED)) {
							current = 0.0;
						}
					}

					if (current == 0) {
						// totalClosedDealsAmount = totalClosedDealsAmount + participationSum;
						closedDealsCount = closedDealsCount + 1;
					} else {
						maxmumLimit = maxmumLimit + current;
						// activeDealsCount = activeDealsCount + 1;
					}

				}
			}

			logger.info("maxmumLimit amount:" + maxmumLimit + " " + user.getId());
			String mobileNumber = null;

			if (user.getPersonalDetails().getWhatsAppNumber() != null) {
				mobileNumber = user.getPersonalDetails().getWhatsAppNumber();
			} else {
				mobileNumber = "91" + user.getMobileNumber();
			}

			Calendar calendar = Calendar.getInstance();
			String currentDate = expectedDateFormat.format(calendar.getTime());
			String lenderName = user.getPersonalDetails().getFirstName() + " "
					+ user.getPersonalDetails().getLastName();

			if (oxyLendersAccepted == null) {
				if (totalPaticipationAmount < oxyBorrowersDealsInformation.getDealAmount()
						&& (totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto
								.getParticipatedAmount() <= oxyBorrowersDealsInformation.getDealAmount())
						|| (totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto
								.getParticipatedAmount() < oxyBorrowersDealsInformation.getDealAmount())) {

					if (oxyBorrowersDealsInformation.getPaticipationLimitToLenders() > 0.0) {
						if (oxyBorrowersDealsInformation.getMinimumPaticipationAmount() != null) {

							Double currentDealAmount = currentDealAmount(dealId);
							if (currentDealAmount != null) {
								if (currentDealAmount > 0 && currentDealAmount < oxyBorrowersDealsInformation
										.getMinimumPaticipationAmount()) {
									if (!oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()
											.equals(currentDealAmount)) {
										logger.info(
												"your paticipation amount is less than the remaining amount. please check the remaining amount is INR "
														+ BigDecimal.valueOf(currentDealAmount).toBigInteger()
														+ ErrorCodes.LIMIT_REACHED + "userId :" + userId);
										exceptionCount += 1;
									}

								} else if (oxyLendersAcceptedDealsRequestDto
										.getParticipatedAmount() < oxyBorrowersDealsInformation
												.getMinimumPaticipationAmount()) {
									logger.info(
											"your paticipation amount is less than the minimum amount. please note that the minimum amount is INR "
													+ BigDecimal.valueOf(
															oxyBorrowersDealsInformation.getMinimumPaticipationAmount())
															.toBigInteger()
													+ ErrorCodes.LIMIT_REACHED + "userId :" + userId);
									exceptionCount += 1;
								}
							}
						}

						if (oxyLendersAcceptedDealsRequestDto.getParticipatedAmount() > oxyBorrowersDealsInformation
								.getPaticipationLimitToLenders()) {
							logger.info(
									"You have exceeded the paticipation limit. please note that the maximum amount is INR "
											+ BigDecimal.valueOf(
													oxyBorrowersDealsInformation.getPaticipationLimitToLenders())
													.toBigInteger()
											+ ErrorCodes.LIMIT_REACHED + "userId :" + userId);
							exceptionCount += 1;
						}

					}

					Double remainingAmountToParticipate = Double.parseDouble(maximumOutstandingAmount) - maxmumLimit;
					if (!oxyBorrowersDealsInformation.getDealType()
							.equals(OxyBorrowersDealsInformation.DealType.EQUITY)) {
						if ((maxmumLimit + oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()) > Double
								.parseDouble(maximumOutstandingAmount)) {

							logger.info(
									"The maximum lender Participation amount should be " + this.maximumOutstandingAmount
											+ " Your total participated amount is " + maxmumLimit
											+ " You can participate with " + remainingAmountToParticipate + " only",
									ErrorCodes.PATICIPATION_LIMIT + "userId :" + userId);
							exceptionCount += 1;
						}
					}
					if (exceptionCount == 0) {
						if (totalPaticipationAmount
								+ oxyLendersAcceptedDealsRequestDto
										.getParticipatedAmount() == oxyBorrowersDealsInformation.getDealAmount()
								|| totalPaticipationAmount > oxyBorrowersDealsInformation.getDealAmount()) {
							oxyBorrowersDealsInformation.setDealParticipationStatus(
									OxyBorrowersDealsInformation.DealParticipationStatus.ACHIEVED);
							oxyBorrowersDealsInformation.setDealAcheivedDate(new Date());
							oxyBorrowersDealsInformationRepo.save(oxyBorrowersDealsInformation);
						}
						OxyLendersAcceptedDeals oxyLendersAcceptedDeals = new OxyLendersAcceptedDeals();
						oxyLendersAcceptedDeals.setUserId(oxyLendersAcceptedDealsRequestDto.getUserId());
						oxyLendersAcceptedDeals.setDealId(oxyLendersAcceptedDealsRequestDto.getDealId());
						oxyLendersAcceptedDeals.setGroupId(oxyLendersAcceptedDealsRequestDto.getGroupId());
						oxyLendersAcceptedDeals.setProcessingFee(oxyLendersAcceptedDealsRequestDto.getProcessingFee());
						if (oxyLendersAcceptedDealsRequestDto.getGroupId() != 0) {
							oxyLendersAcceptedDeals.setFeeStatus(OxyLendersAcceptedDeals.FeeStatus.COMPLETED);
						} else {
							if (oxyBorrowersDealsInformation.getFeeStatusToParticipate()
									.equals(OxyBorrowersDealsInformation.FeeStatusToParticipate.MANDATORY)) {
								oxyLendersAcceptedDeals.setFeeStatus(FeeStatus.valueOf(lenderFeeStatus));
							} else {
								oxyLendersAcceptedDeals.setFeeStatus(OxyLendersAcceptedDeals.FeeStatus.COMPLETED);
							}
						}

						oxyLendersAcceptedDeals
								.setParticipatedAmount(oxyLendersAcceptedDealsRequestDto.getParticipatedAmount());
						double roi = lenderRateOfInterestToDeal(oxyLendersAcceptedDealsRequestDto.getDealId(),
								user.getLenderGroupId(), oxyLendersAcceptedDealsRequestDto.getLenderReturnType());
						oxyLendersAcceptedDeals.setRateofinterest(roi);
						// oxyLendersAcceptedDeals.setRateofinterest(oxyLendersAcceptedDealsRequestDto.getRateofInterest());
						oxyLendersAcceptedDeals.setLenderReturnsType(
								LenderReturnsType.valueOf(oxyLendersAcceptedDealsRequestDto.getLenderReturnType()));
						oxyLendersAcceptedDeals.setReceivedOn(new Date());
						if (oxyLendersAcceptedDealsRequestDto.getProcessingFee() != null) {
							processingFee = oxyLendersAcceptedDealsRequestDto.getProcessingFee();
						}

						oxyLendersAcceptedDeals.setLenderFeeId(oxyLendersAcceptedDealsRequestDto.getLenderFeeId());
						if (oxyLendersAcceptedDealsRequestDto.getAccountType() != null) {
							oxyLendersAcceptedDeals.setPrincipalReturningAccountType(
									OxyLendersAcceptedDeals.PrincipalReturningAccountType
											.valueOf(oxyLendersAcceptedDealsRequestDto.getAccountType()));
						}
						message = "updated";
						if (oxyLendersAcceptedDealsRequestDto.getPaticipationState() != null) {
							oxyLendersAcceptedDeals.setPaticipationState(OxyLendersAcceptedDeals.PaticipationState
									.valueOf(oxyLendersAcceptedDealsRequestDto.getPaticipationState()));
						}
						oxyLendersAcceptedDealsRepo.save(oxyLendersAcceptedDeals);
						lenderWalletHistoryServiceRepo.lenderTransactionHistory(
								oxyLendersAcceptedDealsRequestDto.getUserId(),
								oxyLendersAcceptedDealsRequestDto.getParticipatedAmount(), "DEBIT", "P",
								oxyLendersAcceptedDealsRequestDto.getDealId());

						BigInteger particiaptionAmount = BigDecimal.valueOf(
								totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto.getParticipatedAmount())
								.toBigInteger();
						BigInteger dealAmount = BigDecimal.valueOf(oxyBorrowersDealsInformation.getDealAmount())
								.toBigInteger();
						BigInteger updatedAmount = BigDecimal
								.valueOf(oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()).toBigInteger();

						String messageToWtapp = "*Hello " + lenderName + " (LR" + user.getId() + "),*\n"
								+ "Thanks for choosing Auto-Lending. You have successfully participated in the deal name "
								+ oxyBorrowersDealsInformation.getDealName() + " with INR *"
								+ myFormat.format(updatedAmount) + "* on " + currentDate + ".\n"
								+ "With this amount, the participation amount has reached INR "
								+ myFormat.format(particiaptionAmount) + " till now for the Deal Value INR "
								+ myFormat.format(dealAmount) + ".\n" + "The current score is "
								+ myFormat.format(particiaptionAmount) + "/" + myFormat.format(dealAmount) + ".";

						whatappService.sendingWhatsappMessageWithNewInstance("917702795895-1630419207@g.us",
								messageToWtapp);
						whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, messageToWtapp);

						logger.info("Lender referral Bonus starts...................................");

						Integer refereeId = oxyLendersAcceptedDealsRequestDto.getUserId();

						LenderReferenceDetails refereeInfo = lenderReferenceDetailsRepo.findRefereeInfo(refereeId);
						if (!(oxyBorrowersDealsInformation.getDealType())
								.equals(OxyBorrowersDealsInformation.DealType.EQUITY)) {
							if (refereeInfo != null) {

								Double sumOfParticipationUpdatedAmount = lendersPaticipationUpdationRepo
										.findingSumOfParticipationUpdatedAmount(refereeId);

								if (sumOfParticipationUpdatedAmount == null) {
									sumOfParticipationUpdatedAmount = 0.0;
								}

								Double sumOfParticipatedAmountForFirstTime = oxyLendersAcceptedDealsRepo
										.findingSumOfParticipatedAmountForLender(refereeId);

								Double sumOfParticipatedAmount = sumOfParticipationUpdatedAmount
										+ sumOfParticipatedAmountForFirstTime;

								Double sumOfAmountInReferralTable = 0.0;
								sumOfAmountInReferralTable = lenderReferralBonusUpdatedRepo
										.findingSumOfAmount(refereeId);

								Double participationAmount = oxyLendersAcceptedDealsRequestDto.getParticipatedAmount();

								Double amount = 0.0;

								Integer countOfDeals = oxyLendersAcceptedDealsRepo.findCountOfDeals(refereeId);

								if (sumOfParticipatedAmount != null) {
									if (sumOfAmountInReferralTable != null) {
										amount = (participationAmount / 1000);
									} else {

										Double amountForReferee = lenderReferralBonusUpdatedRepo
												.getAmountForReferee(refereeInfo.getRefereeId());
										if (amountForReferee != null) {
											if (amountForReferee < 1000) {

												if (countOfDeals == 1) {
													if (participationAmount < 100000) {
														amount = (participationAmount / 100);
													} else if (participationAmount > 100000) {

														Double remainingAmount = participationAmount - 100000;
														Double amount1 = (remainingAmount / 500);
														Double amount2 = 1000 + amount1;

														amount = amount2 - amountForReferee;
													} else if (participationAmount == 100000) {
														Double amount1 = (participationAmount / 2000);
														Double amount2 = 1000 + amount1;

														amount = amount2 - amountForReferee;
													}
												} else {
													if (sumOfParticipatedAmount > 100000) {
														Double remainingAmount = (sumOfParticipatedAmount - 100000);

														Double amount2 = (remainingAmount / 1000);

														Double totalAmount = 1000 + amount2;

														amount = totalAmount - amountForReferee;
													} else {
														amount = (participationAmount / 100);
													}
												}
											} else if (amountForReferee >= 1000) {
												amount = (participationAmount / 1000);
											}

										} else {
											if (participationAmount >= 100000) {
												Double remainingAmount = participationAmount - 100000;
												Double amount1 = (remainingAmount / 1000);
												amount = 1000 + amount1;

											} else {
												amount = (participationAmount / 100);
											}

										}
									}

									refereeInfo.setStatus(LenderReferenceDetails.Status.Lent);

									LenderReferralBonusUpdated referralBonusDetails = new LenderReferralBonusUpdated();
									referralBonusDetails.setReferrerUserId(refereeInfo.getReferrerId());
									referralBonusDetails.setAmount(amount);
									referralBonusDetails.setRefereeUserId(refereeId);
									referralBonusDetails.setDealId(id);
									referralBonusDetails.setParticipatedOn(new Date());
									referralBonusDetails.setParticipatedAmount(
											oxyLendersAcceptedDealsRequestDto.getParticipatedAmount());

									lenderReferralBonusUpdatedRepo.save(referralBonusDetails);

									Double sumOfAmountUnpaid = lenderReferenceDetailsRepo
											.findingAmountForUnpaid(refereeInfo.getRefereeId());
									if (sumOfAmountUnpaid != null) {
										refereeInfo.setAmount(sumOfAmountUnpaid);
										lenderReferenceDetailsRepo.save(refereeInfo);
									} else {
										refereeInfo.setAmount(0.0);
										lenderReferenceDetailsRepo.save(refereeInfo);
									}

								}
							}
						}

						logger.info("Lender referral Bonus ends...................................");
					}
				}

			} else {
				if (oxyBorrowersDealsInformation.getDealType().equals(OxyBorrowersDealsInformation.DealType.PERSONAL)) {
					logger.info("you should not participate twice in salaried deal",
							ErrorCodes.ALREDY_IN_PROGRESS + "userId :" + userId);
					exceptionCount += 1;
				}

				if (oxyLendersAcceptedDealsRequestDto.getParticipatedAmount() != null) {
					if (totalPaticipationAmount < oxyBorrowersDealsInformation.getDealAmount()
							&& (totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto
									.getParticipatedAmount() <= oxyBorrowersDealsInformation.getDealAmount())
							|| (totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto
									.getParticipatedAmount() < oxyBorrowersDealsInformation.getDealAmount())) {

						Double participationUpdatedAmount = lendersPaticipationUpdationRepo
								.getSumOfLenderUpdatedAmountToDeal(oxyLendersAcceptedDealsRequestDto.getUserId(),
										dealId);
						Double alreadyPaticipatedAmount = 0.0;
						Double totalPaticipatedAmount = 0.0;
						if (participationUpdatedAmount != null) {
							alreadyPaticipatedAmount = oxyLendersAccepted.getParticipatedAmount()
									+ participationUpdatedAmount
									+ oxyLendersAcceptedDealsRequestDto.getParticipatedAmount();
							totalPaticipatedAmount = oxyLendersAccepted.getParticipatedAmount()
									+ participationUpdatedAmount;
						} else {
							alreadyPaticipatedAmount = oxyLendersAccepted.getParticipatedAmount()
									+ oxyLendersAcceptedDealsRequestDto.getParticipatedAmount();
							totalPaticipatedAmount = oxyLendersAccepted.getParticipatedAmount();
						}

						if (alreadyPaticipatedAmount > oxyBorrowersDealsInformation.getPaticipationLimitToLenders()) {

							logger.info(
									"You have exceeded the paticipation limit. please note that the maximum amount is INR "
											+ BigDecimal.valueOf(
													oxyBorrowersDealsInformation.getPaticipationLimitToLenders())
													.toBigInteger(),
									ErrorCodes.LIMIT_REACHED + "userId :" + userId);
							exceptionCount += 1;
						}

						Double remainingAmountToParticipate = Double.parseDouble(maximumOutstandingAmount)
								- maxmumLimit;
						if (!oxyBorrowersDealsInformation.getDealType()
								.equals(OxyBorrowersDealsInformation.DealType.EQUITY)) {
							if ((maxmumLimit + oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()) > Double
									.parseDouble(maximumOutstandingAmount)) {
								logger.info(
										"The maximum lender Participation amount should be "
												+ this.maximumOutstandingAmount + " Your total participated amount is "
												+ maxmumLimit + " You can participate with "
												+ remainingAmountToParticipate + " only",
										ErrorCodes.PATICIPATION_LIMIT + "userId :" + userId);
								exceptionCount += 1;
							}
						}
						if (exceptionCount == 0) {

							if (totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto
									.getParticipatedAmount() == oxyBorrowersDealsInformation.getDealAmount()) {
								oxyBorrowersDealsInformation.setDealParticipationStatus(
										OxyBorrowersDealsInformation.DealParticipationStatus.ACHIEVED);
								oxyBorrowersDealsInformation.setDealAcheivedDate(new Date());
								oxyBorrowersDealsInformationRepo.save(oxyBorrowersDealsInformation);
							}

							message = "updated";

							BigInteger particiaptionAmount = BigDecimal.valueOf(
									totalPaticipationAmount + oxyLendersAcceptedDealsRequestDto.getParticipatedAmount())
									.toBigInteger();

							BigInteger dealAmount = BigDecimal.valueOf(oxyBorrowersDealsInformation.getDealAmount())
									.toBigInteger();

							BigInteger updatedAmount = BigDecimal.valueOf(oxyLendersAccepted.getParticipatedAmount()
									+ oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()).toBigInteger();

							BigInteger oldPaticiaptionAmount = BigDecimal
									.valueOf(oxyLendersAccepted.getParticipatedAmount()).toBigInteger();

							BigInteger currentAddedAmount = BigDecimal
									.valueOf(oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()).toBigInteger();

							String messageToWtapp = "*Hello " + lenderName + "(LR" + user.getId() + ") ,*"
									+ " You have successfully participated in deal name "
									+ oxyBorrowersDealsInformation.getDealName() + " with INR *"
									+ myFormat.format(currentAddedAmount) + "* on " + currentDate
									+ ". With this amount the participation amount has reached INR "
									+ myFormat.format(particiaptionAmount) + " till now for the Deal Value INR "
									+ myFormat.format(dealAmount) + ". current score is "
									+ myFormat.format(particiaptionAmount) + "/" + myFormat.format(dealAmount) + "."
									+ "Note "
									+ "* You have updated your participation amount from INR In Auto-Lending *"
									+ myFormat.format(BigDecimal.valueOf(totalPaticipatedAmount).toBigInteger())
									+ " to INR "
									+ myFormat.format(BigDecimal.valueOf(alreadyPaticipatedAmount).toBigInteger())
									+ ".";

							whatappService.sendingWhatsappMessageWithNewInstance("917702795895-1630419207@g.us",
									messageToWtapp);
							whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, messageToWtapp);

							LendersPaticipationUpdation lendersPaticipationUpdation = new LendersPaticipationUpdation();
							lendersPaticipationUpdation.setUserId(userId);
							lendersPaticipationUpdation.setDealId(dealId);
							lendersPaticipationUpdation
									.setUpdationAmount(oxyLendersAcceptedDealsRequestDto.getParticipatedAmount());
							// lendersPaticipationUpdation.setType(LendersPaticipationUpdation.Type.ADD);
							lendersPaticipationUpdation.setUpdatedOn(new Date());

							lendersPaticipationUpdation.setDealType(LendersPaticipationUpdation.DealType
									.valueOf(oxyBorrowersDealsInformation.getDealType().toString()));

							lendersPaticipationUpdationRepo.save(lendersPaticipationUpdation);

							lenderWalletHistoryServiceRepo.lenderTransactionHistory(userId,
									oxyLendersAcceptedDealsRequestDto.getParticipatedAmount(), "DEBIT", "PU", dealId);

							if (oxyLendersAcceptedDealsRequestDto.getAccountType() != null) {
								oxyLendersAccepted.setPrincipalReturningAccountType(
										OxyLendersAcceptedDeals.PrincipalReturningAccountType
												.valueOf(oxyLendersAcceptedDealsRequestDto.getAccountType()));
								oxyLendersAcceptedDealsRepo.save(oxyLendersAccepted);
							}

							logger.info("Lender referral Bonus starts1...................................");

							Integer refereeId = oxyLendersAcceptedDealsRequestDto.getUserId();

							LenderReferenceDetails refereeInfo = lenderReferenceDetailsRepo.findRefereeInfo(refereeId);

							if (!(oxyBorrowersDealsInformation.getDealType())
									.equals(OxyBorrowersDealsInformation.DealType.EQUITY)) {
								if (refereeInfo != null) {

									Double amount = 0.0;

									Double sumOfParticipationUpdatedAmount = lendersPaticipationUpdationRepo
											.findingSumOfParticipationUpdatedAmount(refereeId);

									if (sumOfParticipationUpdatedAmount == null) {
										sumOfParticipationUpdatedAmount = 0.0;
									}

									Double sumOfParticipatedAmountForFirstTime = oxyLendersAcceptedDealsRepo
											.findingSumOfParticipatedAmountForLender(refereeId);

									Double sumOfParticipatedAmount = sumOfParticipationUpdatedAmount
											+ sumOfParticipatedAmountForFirstTime;

									Double amountForReferee = lenderReferralBonusUpdatedRepo
											.getAmountForReferee(refereeInfo.getRefereeId());

									if (amountForReferee != null) {
										if (amountForReferee < 1000) {

											if (sumOfParticipatedAmount <= 100000) {

												Double amount1 = (oxyLendersAcceptedDealsRequestDto
														.getParticipatedAmount() / 100);
												amount = amount1;
											} else if (sumOfParticipatedAmount > 100000) {
												Double remainingAmount = (sumOfParticipatedAmount - 100000);

												if (amountForReferee >= 500) {

													Double amount2 = (remainingAmount / 1000);

													Double reference = 1000 + amount2;

													amount = reference - amountForReferee;

												} else if (amountForReferee < 500) {

													Double amount2 = (remainingAmount / 1000);

													Double totalAmount = 1000 + amount2;

													amount = totalAmount;
												}

											}

										} else if (amountForReferee >= 1000) {

											Double amount1 = (oxyLendersAcceptedDealsRequestDto.getParticipatedAmount()
													/ 1000);

											amount = amount1;

										}
									}

									LenderReferralBonusUpdated referralDetails = new LenderReferralBonusUpdated();
									referralDetails.setReferrerUserId(refereeInfo.getReferrerId());
									referralDetails.setAmount(amount);
									referralDetails.setRefereeUserId(refereeId);
									referralDetails.setDealId(id);
									referralDetails.setParticipatedOn(new Date());
									referralDetails.setParticipatedAmount(
											oxyLendersAcceptedDealsRequestDto.getParticipatedAmount());

									lenderReferralBonusUpdatedRepo.save(referralDetails);

									Double sumOfAmountUnpaid = lenderReferenceDetailsRepo
											.findingAmountForUnpaid(refereeInfo.getRefereeId());
									if (sumOfAmountUnpaid != null) {
										refereeInfo.setAmount(sumOfAmountUnpaid);
										lenderReferenceDetailsRepo.save(refereeInfo);
									} else {
										refereeInfo.setAmount(0.0);
										lenderReferenceDetailsRepo.save(refereeInfo);
									}
								}
							}

							logger.info("Lender referral Bonus ends1...................................");
						}
					}

				}

			}
		}

		OxyLendersAcceptedDealsResponseDto oxyLendersAcceptedDealsResponseDto = new OxyLendersAcceptedDealsResponseDto();

		oxyLendersAcceptedDealsResponseDto.setProcessingFee(processingFee);

		oxyLendersAcceptedDealsResponseDto.setDealId(id);

		oxyLendersAcceptedDealsResponseDto.setStatus(message);

		ListOfWhatappGroupNames listOfWhatappGroupNames = new ListOfWhatappGroupNames();
		listOfWhatappGroupNames.setDealName(oxyBorrowersDealsInformation.getDealName());
		try {
			com.oxyloans.response.user.StatusResponseDto statusResponseDto = notifications
					.sendingNotificationsToLendersInDealPending(listOfWhatappGroupNames);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("lenderAutoparticipation method ends ");
		return oxyLendersAcceptedDealsResponseDto;

	}

	public DealLevelRequestDto automaticSendingOffer(BorrowersDealsRequestDto borrowersDealsRequestDto) {
		throw new OperationNotAllowedException("automaticSendingOffer", ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public PayuMoneyPaymentDetaisResponce lenderFeePayments(String transactionNumber,
			PayuMoneyPaymentDetailsRequest request) {

		logger.info("lender fee payment method start");

		LenderPayuDetails lenderPayuDetails = lenderPayuDetailsRepo.findByTransactionNumber(transactionNumber);

		logger.info("trasnactionNumber" + transactionNumber);

		Calendar currentDate = Calendar.getInstance();
		PayuMoneyPaymentDetaisResponce response = new PayuMoneyPaymentDetaisResponce();

		if (lenderPayuDetails != null) {

			LenderRenewalDetails lenderRenewalPayments = lenderRenewalDetailsRepo
					.findingUserInTable(lenderPayuDetails.getLenderUserId());

			logger.info("userId" + lenderPayuDetails.getLenderUserId());

			if (lenderPayuDetails.getLenderFeePayments().equals(LenderPayuDetails.LenderFeePayments.LIFETIME)) {
				lenderRenewal(lenderPayuDetails, request, currentDate, lenderRenewalPayments);

				if (lenderRenewalPayments != null) {

					lenderPaymentRenewal(lenderPayuDetails, lenderRenewalPayments);
				} else {
					processNewRenewal(lenderPayuDetails);
				}

			} else if (lenderPayuDetails.getLenderFeePayments().equals(LenderPayuDetails.LenderFeePayments.TENYEARS)) {

				lenderRenewal(lenderPayuDetails, request, currentDate, lenderRenewalPayments);

				if (lenderRenewalPayments != null) {

					lenderPaymentRenewal(lenderPayuDetails, lenderRenewalPayments);
				} else {
					processNewRenewal(lenderPayuDetails);

				}
			} else if (lenderPayuDetails.getLenderFeePayments().equals(LenderPayuDetails.LenderFeePayments.FIVEYEARS)) {

				lenderRenewal(lenderPayuDetails, request, currentDate, lenderRenewalPayments);

				if (lenderRenewalPayments != null) {

					lenderPaymentRenewal(lenderPayuDetails, lenderRenewalPayments);
				} else {
					processNewRenewal(lenderPayuDetails);

				}
			} else if (lenderPayuDetails.getLenderFeePayments().equals(LenderPayuDetails.LenderFeePayments.PERYEAR)) {

				lenderRenewal(lenderPayuDetails, request, currentDate, lenderRenewalPayments);

				if (lenderRenewalPayments != null) {

					lenderPaymentRenewal(lenderPayuDetails, lenderRenewalPayments);
				} else {
					processNewRenewal(lenderPayuDetails);

				}
			} else if (lenderPayuDetails.getLenderFeePayments()
					.equals(LenderPayuDetails.LenderFeePayments.HALFYEARLY)) {

				lenderRenewal(lenderPayuDetails, request, currentDate, lenderRenewalPayments);

				if (lenderRenewalPayments != null) {

					lenderPaymentRenewal(lenderPayuDetails, lenderRenewalPayments);
				} else {
					processNewRenewal(lenderPayuDetails);

				}
			} else if (lenderPayuDetails.getLenderFeePayments().equals(LenderPayuDetails.LenderFeePayments.QUARTERLY)) {

				lenderRenewal(lenderPayuDetails, request, currentDate, lenderRenewalPayments);

				if (lenderRenewalPayments != null) {

					lenderPaymentRenewal(lenderPayuDetails, lenderRenewalPayments);
				} else {
					processNewRenewal(lenderPayuDetails);

				}
			} else if (lenderPayuDetails.getLenderFeePayments().equals(LenderPayuDetails.LenderFeePayments.MONTHLY)) {

				lenderRenewal(lenderPayuDetails, request, currentDate, lenderRenewalPayments);

				if (lenderRenewalPayments != null) {

					lenderPaymentRenewal(lenderPayuDetails, lenderRenewalPayments);
				} else {
					processNewRenewal(lenderPayuDetails);

				}
			} else {

				OxyLendersAcceptedDeals accepetedDeals = oxyLendersAcceptedDealsRepo
						.toCheckUserAlreadyInvoledInDealForPendingFee(lenderPayuDetails.getLenderUserId(),
								lenderPayuDetails.getDealId());
				LendersPaticipationUpdation lendersPaticipationUpdation = lendersPaticipationUpdationRepo
						.toCheckUserAlreadyInvoledInPartcipatedDeal(lenderPayuDetails.getLenderUserId(),
								lenderPayuDetails.getDealId());
				logger.info("userId:" + lenderPayuDetails.getLenderUserId());

				logger.info("dealId:" + lenderPayuDetails.getDealId());
				String paticipationFrom = null;

				Date participationOn = null;

				logger.info("lender participation exists");
				if (accepetedDeals != null) {
					accepetedDeals
							.setFeeStatus(request.getPayuStatus().equalsIgnoreCase("success") ? FeeStatus.COMPLETED
									: FeeStatus.PENDING);
					paticipationFrom = accepetedDeals.getLenderParticipationFrom().toString();
					participationOn = accepetedDeals.getReceivedOn();

					lenderPayuDetails.setUserStatus(LenderPayuDetails.UserStatus.PARTICIPATION);
					lenderPayuDetails.setParticipatedTableId(accepetedDeals.getId());

					lenderPayuDetailsRepo.save(lenderPayuDetails);
					if (lenderRenewalPayments != null) {
						accepetedDeals.setGroupId(0);
					}

					oxyLendersAcceptedDealsRepo.save(accepetedDeals);
				} else {
					lendersPaticipationUpdation.setFeeStatus(request.getPayuStatus().equalsIgnoreCase("success")
							? LendersPaticipationUpdation.FeeStatus.COMPLETED
							: LendersPaticipationUpdation.FeeStatus.PENDING);

					lenderPayuDetails.setUserStatus(LenderPayuDetails.UserStatus.UPDATION);
					lenderPayuDetails.setParticipatedTableId(lendersPaticipationUpdation.getId());

					lenderPayuDetailsRepo.save(lenderPayuDetails);
					paticipationFrom = lendersPaticipationUpdation.getLenderParticipationFrom().toString();

					participationOn = lendersPaticipationUpdation.getUpdatedOn();

					lendersPaticipationUpdationRepo.save(lendersPaticipationUpdation);
				}

				User lender = userRepo.findByIdNum(lenderPayuDetails.getLenderUserId());
				String mobileNumber = "";
				String msgToLender = null;
				String dealName = null;
				String msgToAdmin = null;
				if (lender != null) {

					String firstName = lender.getPersonalDetails().getFirstName();

					Integer lenderdealId = lenderPayuDetails.getDealId();

					Double feeAmount = lenderPayuDetails.getAmount();

					OxyBorrowersDealsInformation oxyBorrowerDeals = oxyBorrowersDealsInformationRepo
							.findDealNameByDealId(lenderdealId);

					if (oxyBorrowerDeals != null) {
						dealName = oxyBorrowerDeals.getDealName();
					}

					Calendar current = Calendar.getInstance();
					String expectedDate = expectedDateFormat.format(current.getTime());
					String lenderParticipated = expectedDateFormat.format(participationOn);

					msgToLender = "Dear *" + firstName + "*\n\n" + "Greetings from Oxyloans!\n"
							+ "Thank you for your fee payment of INR " + feeAmount
							+ " and participation in the deal named " + dealName + " on " + expectedDate + ".\n\n"
							+ "Please write to us if you have any concerns using the below link:\n"
							+ "https://www.oxyloans.com/new/lenderInquiries\n\n" + "Thanks,\nOxyloans Team";

					mobileNumber = lender.getPersonalDetails().getWhatsAppNumber() != null
							? lender.getPersonalDetails().getWhatsAppNumber()
							: "91" + lender.getMobileNumber();

					msgToAdmin = "Hi Team,\n\n" + "*Mr/Mrs " + firstName + " (LR" + lender.getId() + ")*"
							+ " 's payment of " + "*INR " + BigDecimal.valueOf(feeAmount).toBigInteger()
							+ " is successful*" + " and participation in the deal named " + dealName + " (dealId: "
							+ lenderdealId + ") on" + expectedDate + " is paid from " + "*cashfree*" + " through " + "*"
							+ paticipationFrom + "*" + "\n" + " LenderParticipatedOn: " + "*" + lenderParticipated + "*"
							+ "\n\n" + "*Thanks*,\n" + "*OxyLoans Team*";
				}
				whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, msgToLender);
				if (!lender.isTestUser()) {
					whatappService.sendingWhatsappMessageWithNewInstance("120363209483399207@g.us", msgToAdmin);
				}
			}

			lenderPayuDetails.setPaymentDate(currentDate.getTime());
			lenderPayuDetails.setStatus(
					request.getPayuStatus().equalsIgnoreCase("success") ? PayuStatus.COMPLETED : PayuStatus.PENDING);
			lenderPayuDetails.setPayuTransactionNumber(request.getPayuTransactionNumber());

			response.setAmount(lenderPayuDetails.getAmount().toString());
			response.setStatus(request.getPayuStatus().equalsIgnoreCase("success") ? PayuStatus.COMPLETED.name()
					: PayuStatus.PENDING.name());
			response.setId(lenderPayuDetails.getLenderUserId());
			response.setLenderFeeId(lenderPayuDetails.getId());

			lenderPayuDetailsRepo.save(lenderPayuDetails);

		} else {
			throw new NoSuchElementException("No records present with oxyLoansTransactionNumber " + transactionNumber);
		}
		logger.info("lender fee payment method start");

		return response;
	}

	private void lenderRenewal(LenderPayuDetails lenderPayuDetails, PayuMoneyPaymentDetailsRequest request,
			Calendar currentDate, LenderRenewalDetails lenderRenewalPayments) {
		if (request.getPayuStatus().equalsIgnoreCase("success")) {
			User user = userRepo.findById(lenderPayuDetails.getLenderUserId()).orElse(null);
			if (user != null && lenderRenewalPayments != null) {
				user.setLenderGroupId(user.getLenderGroupId());
			} else {
				user.setLenderGroupId(7);
			}
			userRepo.save(user);
		}

		lenderPayuDetails.setPaymentDate(currentDate.getTime());
		lenderPayuDetails.setStatus(
				request.getPayuStatus().equalsIgnoreCase("success") ? PayuStatus.COMPLETED : PayuStatus.PENDING);
		lenderPayuDetails.setPayuTransactionNumber(request.getPayuTransactionNumber());

		lenderPayuDetailsRepo.save(lenderPayuDetails);
	}

	private void lenderPaymentRenewal(LenderPayuDetails lenderPayuDetails, LenderRenewalDetails lenderRenewalPayments) {

		if (lenderPayuDetails.getStatus().equals(LenderPayuDetails.PayuStatus.COMPLETED)
				&& lenderPayuDetails.getPaymentDate() != null) {

			lenderPayuDetails.setUserStatus(LenderPayuDetails.UserStatus.RENEWAL);
			lenderPayuDetailsRepo.save(lenderPayuDetails);

			String paymentReceivedOn = "";
			SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
			if (lenderRenewalPayments.getPaymentReceivedOn() != null) {
				paymentReceivedOn = f.format(lenderRenewalPayments.getPaymentReceivedOn());
			}
			String paymentDate = f.format(lenderPayuDetails.getPaymentDate());
			// if (lenderRenewalPayments.getPaymentReceivedOn() == null ||
			// !paymentReceivedOn.equals(paymentDate)) {
			if (lenderPayuDetails.getPaymentDate().compareTo(lenderRenewalPayments.getValidityDate()) < 0) {

				lenderRenewalPayments.setPaymentDate(lenderRenewalPayments.getValidityDate());
			} else {
				lenderRenewalPayments.setPaymentDate(lenderPayuDetails.getPaymentDate());
			}
			lenderRenewalPayments.setAmount(lenderPayuDetails.getAmount());
			lenderRenewalPayments.setRenewalStatus(LenderRenewalDetails.RenewalStatus.Renewed);
			// lenderRenewalPayments.setPaymentReceivedOn(new Date());
			lenderRenewalDetailsRepo.save(lenderRenewalPayments);

			Calendar c = Calendar.getInstance();

			c.setTime(lenderRenewalPayments.getPaymentDate());

			int yearsToAdd = 0;

			if (lenderPayuDetails.getLenderFeePayments().equals(LenderPayuDetails.LenderFeePayments.LIFETIME)) {
				yearsToAdd = 14;

			} else if (lenderPayuDetails.getLenderFeePayments().equals(LenderPayuDetails.LenderFeePayments.TENYEARS)) {
				yearsToAdd = 10;

			} else if (lenderPayuDetails.getLenderFeePayments().equals(LenderPayuDetails.LenderFeePayments.FIVEYEARS)) {
				yearsToAdd = 5;

			} else if (lenderPayuDetails.getLenderFeePayments().equals(LenderPayuDetails.LenderFeePayments.PERYEAR)) {
				yearsToAdd = 1;

			} else if (lenderPayuDetails.getLenderFeePayments()
					.equals(LenderPayuDetails.LenderFeePayments.HALFYEARLY)) {
				c.add(Calendar.MONTH, 6);

			} else if (lenderPayuDetails.getLenderFeePayments().equals(LenderPayuDetails.LenderFeePayments.QUARTERLY)) {
				c.add(Calendar.MONTH, 3);

			} else if (lenderPayuDetails.getLenderFeePayments().equals(LenderPayuDetails.LenderFeePayments.MONTHLY)) {
				c.add(Calendar.MONTH, 1);
			}

			if (yearsToAdd > 0) {
				c.add(Calendar.YEAR, yearsToAdd);
			}

			Date newDate = c.getTime();

			LenderRenewalDetails lenderRenewalDetails = new LenderRenewalDetails();

			lenderRenewalDetails.setUser_id(lenderRenewalPayments.getUser_id());
			lenderRenewalDetails.setValidityDate(newDate);
			lenderRenewalDetails.setRenewalStatus(LenderRenewalDetails.RenewalStatus.NotYetRenewed);
			lenderRenewalDetails.setPaymentReceivedOn(new Date());
			lenderRenewalDetailsRepo.save(lenderRenewalDetails);

			String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(newDate);

			User lender = userRepo.findByIdNum(lenderRenewalPayments.getUser_id());
			String msgToLender = "Dear *" + lender.getPersonalDetails().getFirstName().trim() + "*" + "\n"
					+ "\nGreetings from Oxyloans!" + "\n"
					+ "we are happy to inform you that your oxyloans  membership validity is extended till" + " *"
					+ modifiedDate + "* from *" + lenderRenewalPayments.getValidityDate() + "*. \n\n"
					+ "Please write to us if you have any concerns using the below link:" + "\n"
					+ "https://www.oxyloans.com/new/lenderInquiries" + "\n\n" + "Thanks" + "\nOxyloans Team";

			Calendar current = Calendar.getInstance();
			String expectedDate = expectedDateFormat.format(current.getTime());

			String msgToAdmin = "Hi Team,\n" + "*Mr/Mrs " + lender.getPersonalDetails().getFirstName().trim() + " (LR"
					+ lender.getId() + ")*" + " membership is extended till" + modifiedDate + "from "
					+ lenderRenewalPayments.getValidityDate() + ".\n\n" + "*Payment Type:* "
					+ lenderPayuDetails.getLenderFeePayments() + "\n\n" + "*Amount:* "
					+ BigDecimal.valueOf(lenderPayuDetails.getAmount()).toBigInteger() + "\n\n" + "*Payment Date:* "
					+ expectedDate + "\n\n" + "Thanks,\nOxyLoans Team";

			String mobileNumber = "";

			if (lender != null) {
				if (lender.getPersonalDetails().getWhatsAppNumber() != null) {
					mobileNumber = lender.getPersonalDetails().getWhatsAppNumber();
				} else {
					mobileNumber = "91" + lender.getMobileNumber();
				}

			}

			whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, msgToLender);
			if (!lender.isTestUser()) {

				whatappService.sendingWhatsappMessageWithNewInstance("120363209483399207@g.us", msgToAdmin);
			}
			String mailsubject = "Lender Validity Renewal";
			TemplateContext context = new TemplateContext();

			String lenderName = "";
			if (lender.getPersonalDetails().getFirstName() != null) {
				lenderName = lender.getPersonalDetails().getFirstName();
			} else {
				lenderName = "";
			}

			LocalDate currentDate1 = LocalDate.now();

			context.put("currentDate", currentDate1);
			context.put("name", lenderName);
			context.put("modifiedDate", modifiedDate);
			context.put("validityDate", lenderRenewalPayments.getValidityDate());

			EmailRequest emailRequest = new EmailRequest(new String[] { lender.getEmail(), "sudisridhar9@gmail.com" },
					"lender_renewal_updation.template", context, mailsubject, "Oxyloans");

			EmailResponse emailResponse = emailService.sendEmail(emailRequest);

			if (emailResponse.getStatus() == EmailResponse.Status.FAILED) {
				try {
					throw new MessagingException(emailResponse.getErrorMessage());
				} catch (MessagingException e2) {

					e2.printStackTrace();
				}
			}
			// }
		}
	}

	private void processNewRenewal(LenderPayuDetails lenderPayuDetails) {
		if (lenderPayuDetails.getStatus().equals(LenderPayuDetails.PayuStatus.COMPLETED)
				&& lenderPayuDetails.getPaymentDate() != null) {

			LenderRenewalDetails renewal = lenderRenewalDetailsRepo.findingUser(lenderPayuDetails.getLenderUserId());

			if (renewal == null) {

				lenderPayuDetails.setUserStatus(LenderPayuDetails.UserStatus.NOTYETRENEWAL);
				lenderPayuDetailsRepo.save(lenderPayuDetails);

				LenderRenewalDetails renewalDetails = new LenderRenewalDetails();

				Calendar c = Calendar.getInstance();
				Date newDate = null;
				c.setTime(lenderPayuDetails.getPaymentDate());

				int yearsToAdd = 0;

				if (lenderPayuDetails.getLenderFeePayments().equals(LenderPayuDetails.LenderFeePayments.LIFETIME)) {
					yearsToAdd = 14;

				} else if (lenderPayuDetails.getLenderFeePayments()
						.equals(LenderPayuDetails.LenderFeePayments.TENYEARS)) {
					yearsToAdd = 10;

				} else if (lenderPayuDetails.getLenderFeePayments()
						.equals(LenderPayuDetails.LenderFeePayments.FIVEYEARS)) {
					yearsToAdd = 5;

				} else if (lenderPayuDetails.getLenderFeePayments()
						.equals(LenderPayuDetails.LenderFeePayments.PERYEAR)) {
					yearsToAdd = 1;

				} else if (lenderPayuDetails.getLenderFeePayments()
						.equals(LenderPayuDetails.LenderFeePayments.HALFYEARLY)) {
					c.add(Calendar.MONTH, 6);

				} else if (lenderPayuDetails.getLenderFeePayments()
						.equals(LenderPayuDetails.LenderFeePayments.QUARTERLY)) {
					c.add(Calendar.MONTH, 3);

				} else if (lenderPayuDetails.getLenderFeePayments()
						.equals(LenderPayuDetails.LenderFeePayments.MONTHLY)) {
					c.add(Calendar.MONTH, 1);
				}

				if (yearsToAdd > 0) {
					c.add(Calendar.YEAR, yearsToAdd);
				}

				newDate = c.getTime();

				renewalDetails.setUser_id(lenderPayuDetails.getLenderUserId());
				renewalDetails.setValidityDate(newDate);
				renewalDetails.setRenewalStatus(LenderRenewalDetails.RenewalStatus.NotYetRenewed);
				renewalDetails.setPaymentReceivedOn(new Date());
				lenderRenewalDetailsRepo.save(renewalDetails);

				User lender = userRepo.findByIdNum(renewalDetails.getUser_id());
				String mobileNumber = "";
				String msgToLender = null;

				String msgToAdmin = null;
				if (lender != null) {

					String firstName = lender.getPersonalDetails().getFirstName();

					Date validityDate = renewalDetails.getValidityDate();

					String expectedLenderDate = expectedDateFormat.format(validityDate);

					msgToLender = "Dear *" + firstName + "*\n\n" + "Greetings from Oxyloans!\n"
							+ "We are happy to inform you that your Oxyloans membership is extended until *"
							+ expectedLenderDate + "*.\n\n"
							+ "Please write to us if you have any concerns using the below link:\n"
							+ "https://www.oxyloans.com/new/lenderInquiries\n\n" + "Thanks,\nOxyloans Team";

					mobileNumber = lender.getPersonalDetails().getWhatsAppNumber() != null
							? lender.getPersonalDetails().getWhatsAppNumber()
							: "91" + lender.getMobileNumber();

					Calendar current = Calendar.getInstance();
					String expectedDate = expectedDateFormat.format(current.getTime());

					msgToAdmin = "HI Team,\n" + "*Mr/Mrs " + firstName + " (LR" + lender.getId() + ")*"
							+ " membership is extended  " + expectedLenderDate + ".\n\n" + " *Payment Type:* "
							+ lenderPayuDetails.getLenderFeePayments() + "\n\n" + " *Amount:* "
							+ lenderPayuDetails.getAmount() + "\n\n" + " *Payment Date:* " + expectedDate
							+ "\n\nThanks,\nOxyLoans Team";
				}
				whatappService.sendingIndividualMessageUsingUltraApi(mobileNumber, msgToLender);
				if (!lender.isTestUser()) {

					whatappService.sendingWhatsappMessageWithNewInstance("120363209483399207@g.us", msgToAdmin);
				}
			}
		}

	}

	@Override
	public LenderInvestmentDataDtoResponse lenderTotalInvestmentData(Integer userId) {
		LenderInvestmentDataDtoResponse lenderInvestmentDataDtoResponse = new LenderInvestmentDataDtoResponse();
		User user = userRepo.findById(userId).get();

		Double lenderTotalInvestmentAmount = 0.0;

		Double lenderStudentTotalAmount = 0.0;

		Double lenderEquityDealsTotal = 0.0;

		if (user != null) {

			Integer lenderParticipatedTotalDeals = oxyLendersAcceptedDealsRepo.findTotalDealsParticipated(userId);

			Double lenderFirstParticipation = oxyLendersAcceptedDealsRepo.findingSumOfAmountParticipatedUser(userId);
			Double lenderPaticipationUpdation = lendersPaticipationUpdationRepo.findingSumOfAmountUpdatedUser(userId);
			if (lenderFirstParticipation != null) {
				if (lenderPaticipationUpdation != null) {
					lenderTotalInvestmentAmount = lenderFirstParticipation + lenderPaticipationUpdation;
				} else {
					lenderTotalInvestmentAmount = lenderFirstParticipation;
				}
			}
			Double lenderStudentDeals = oxyLendersAcceptedDealsRepo.findLenderParticipationInStudentDeals(userId);

			Double lenderStudentUpadtionDeals = lendersPaticipationUpdationRepo.findLenderStudentUpdationDeals(userId);

			if (lenderStudentDeals != null) {
				if (lenderStudentUpadtionDeals != null) {
					lenderStudentTotalAmount = lenderStudentDeals + lenderStudentUpadtionDeals;
				} else {
					lenderStudentTotalAmount = lenderStudentDeals;
				}
			}

			Double lendeEquityDeals = oxyLendersAcceptedDealsRepo.lenderEquityDeals(userId);

			Double lendeEquityUpdation = lendersPaticipationUpdationRepo.findLenderEquityDeals(userId);

			if (lendeEquityDeals != null) {
				if (lendeEquityUpdation != null) {
					lenderEquityDealsTotal = lendeEquityDeals + lendeEquityUpdation;
				} else {
					lenderEquityDealsTotal = lendeEquityDeals;
				}
			}

			lenderInvestmentDataDtoResponse.setUserId(userId);
			lenderInvestmentDataDtoResponse.setParticipatedStudentDealsAmount(lenderStudentTotalAmount);
			lenderInvestmentDataDtoResponse.setParticipatedDealsCount(lenderParticipatedTotalDeals);
			lenderInvestmentDataDtoResponse.setLenderTotalParticipationAmount(lenderTotalInvestmentAmount);
			lenderInvestmentDataDtoResponse.setParticipatedEquityDealsAmount(lenderEquityDealsTotal);

		}
		return lenderInvestmentDataDtoResponse;
	}

	@Override
	public LenderReferenceResponse readingResponseForReferenceBonus1(
			LenderReferenceRequestDto lenderReferenceRequestDto) throws ParseException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);

	}

	@Override
	public LenderReferenceResponse readingCommentsOfRadhaSir1(LenderReferenceRequestDto lenderReferenceRequestDto)
			throws ParseException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderReferenceResponse readingLenderReferenceToSheet1() throws ParseException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public BorrowerAutoLendingResponse getLendersUsingAutoLending() {
		logger.info("getLendersUsingAutoLending method start");

		OxyLendersAcceptedDealsRequestDto oxyLendersAcceptedDealsRequestDto = new OxyLendersAcceptedDealsRequestDto();

		List<LenderAutoInvestConfig> listOfEnableUsers = lenderAutoInvestConfigRepo.autoLendingEnableUsers();

		List<OxyBorrowersDealsInformation> listOfOpenDeals = oxyBorrowersDealsInformationRepo.getListOfOpenDeals();

		if (listOfOpenDeals != null && !listOfOpenDeals.isEmpty()) {

			for (OxyBorrowersDealsInformation dealsInformation : listOfOpenDeals) {

				oxyLendersAcceptedDealsRequestDto.setDealId(dealsInformation.getId());

				if (listOfEnableUsers != null && !listOfEnableUsers.isEmpty()) {

					for (LenderAutoInvestConfig lenders : listOfEnableUsers) {

						oxyLendersAcceptedDealsRequestDto.setUserId(lenders.getUserId());
						User user = userRepo.findById(lenders.getUserId()).get();
						if (user != null) {
							oxyLendersAcceptedDealsRequestDto.setGroupId(user.getLenderGroupId());
						}
						oxyLendersAcceptedDealsRequestDto.setDealId(dealsInformation.getId());
						oxyLendersAcceptedDealsRequestDto
								.setParticipatedAmount(dealsInformation.getMinimumPaticipationAmount());
						oxyLendersAcceptedDealsRequestDto
								.setLenderReturnType(OxyLendersAcceptedDeals.LenderReturnsType.MONTHLY.toString());
						oxyLendersAcceptedDealsRequestDto.setLenderFeeId(0);
						// oxyLendersAcceptedDealsRequestDto.setLenderReturnType(accountType);
						oxyLendersAcceptedDealsRequestDto
								.setPaticipationState(OxyLendersAcceptedDeals.PaticipationState.AUTOLEND.toString());

						Integer userId = oxyLendersAcceptedDealsRequestDto.getDealId();
						Integer dealId = oxyLendersAcceptedDealsRequestDto.getUserId();

						OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
								.toCheckUserAlreadyInvoledInDeal(userId, dealId);
						if (oxyLendersAcceptedDeals == null) {
							lenderAutoPaticipation(oxyLendersAcceptedDealsRequestDto);

						}

					}

				}

			}
		}
		Integer openDealsInfo = oxyBorrowersDealsInformationRepo.findByOpenDeals();

		Integer listOfActiveLender = lenderAutoInvestConfigRepo.autoLenderAlreadyEnabled1();

		String messageToWtapp = "Hi Team" + "\n" + "Total Open Deals: " + openDealsInfo + "\n" + "Deals In Open: "
				+ "dealsInOpen" + "\n" + "Active Lenders In AutoLending: " + listOfActiveLender;

		whatappServiceRepo.sendingIndividualMessageUsingUltraApi("917702795895-1630419500@g.us", messageToWtapp);
		BorrowerAutoLendingResponse response = new BorrowerAutoLendingResponse();
		response.setMessage("success");
		logger.info("getLendersUsingAutoLending method ends");

		return response;
	}

}
