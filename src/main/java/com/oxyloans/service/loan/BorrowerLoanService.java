package com.oxyloans.service.loan;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.internal.guava.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.oxyloans.customexceptions.DataFormatException;
import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.entityborrowersdealsdto.ClosedDealsInformation;
import com.oxyloans.entity.loan.InterestDetails;
import com.oxyloans.entity.loan.LoanEmiCard;
import com.oxyloans.entity.loan.LoanRequest.LoanStatus;
import com.oxyloans.entity.loan.OxyLoan;
import com.oxyloans.entity.loan.PaymentHistory;
import com.oxyloans.entity.loan.PenaltyDetails;
import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.file.FileRequest;
import com.oxyloans.file.FileResponse;
import com.oxyloans.file.FileRequest.FileType;
import com.oxyloans.repo.loan.InterestDetailsRepo;
import com.oxyloans.repo.loan.LoanEmiCardRepo;
import com.oxyloans.repo.loan.LoansByApplicationNativeRepo;
import com.oxyloans.repo.loan.PaymentHistoryDetailsRepo;
import com.oxyloans.repo.loan.PenaltyDetailsRepo;
import com.oxyloans.repo.loan.UpdEmiByApplicationRepo;
import com.oxyloans.repo.user.UserRepo;
import com.oxyloans.request.user.KycFileRequest;
import com.oxyloans.request.user.KycFileResponse;
import com.oxyloans.request.user.KycFileRequest.KycType;
import com.oxyloans.response.user.EmisPaidInformation;
import com.oxyloans.response.user.EnachScheduledMailResponseDto;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.emailservice.IEmailService;
import com.oxyloans.serviceloan.BorrowerAccountDetailsDto;
import com.oxyloans.serviceloan.LoanEmiCardPaymentDetailsRequestDto;
import com.oxyloans.serviceloan.LoanEmiCardPaymentDetailsResponseDto;
import com.oxyloans.serviceloan.LoanEmiCardResponseDto;
import com.oxyloans.serviceloan.OxyAccountDetailsDto;
import com.oxyloans.serviceloan.OxyDashboardDetailsDto;

@Service("BORROWER")
public class BorrowerLoanService extends AbstractLoanService<BorrowerAccountDetailsDto> {

	private String modeOfPayment = "PAYU";

	private final Logger logger = LogManager.getLogger(BorrowerLoanService.class);

	@Autowired
	AdminLoanService adminLoanService;

	@Autowired
	private LoanEmiCardRepo loanEmiCardRepo;

	@Autowired
	private UpdEmiByApplicationRepo updEmiByApplicationRepo;

	private static final String DATE_FORMAT = "dd/MM/yyyy";

	@Autowired
	private LoansByApplicationNativeRepo loansByApplicationNativeRepo;

	@Autowired
	private PenaltyDetailsRepo penaltyDetailsRepo;

	@Autowired
	private InterestDetailsRepo interestDetailsRepo;

	@Autowired
	private PaymentHistoryDetailsRepo paymentHistoryDetailsRepo;

	@Autowired
	private IEmailService emailService;

	private SimpleDateFormat dateFormate = new SimpleDateFormat("dd/MM/yyyy");

	@Autowired
	private UserRepo userRepo;

	@Value("${emiFilePath}")
	private String emiFilePath;

	@Override
	protected BorrowerAccountDetailsDto populatedDetails(User user, Object[] objects) {
		BorrowerAccountDetailsDto oxyAccountDetailsDto = new BorrowerAccountDetailsDto();

		oxyAccountDetailsDto.setAmountDisbursed(objects[0] == null ? 0d : Math.round((Double) objects[0]));
		oxyAccountDetailsDto.setInterestPaid(objects[1] == null ? 0d : Math.round((Double) objects[1]));
		oxyAccountDetailsDto.setTotalTransactionFee(objects[2] == null ? 0d : Math.round((Double) objects[2]));
		oxyAccountDetailsDto.setPrincipalReceived(objects[3] == null ? 0d : Math.round((Double) objects[3]));
		if (oxyAccountDetailsDto.getAmountDisbursed() != null) {
			oxyAccountDetailsDto.setOutstandingAmount(
					oxyAccountDetailsDto.getAmountDisbursed() - oxyAccountDetailsDto.getPrincipalReceived());
		}
		Long failedEmisCount = (Long) objects[4];
		oxyAccountDetailsDto.setNoOfFailedEmis(failedEmisCount == null ? 0 : failedEmisCount.intValue());

		return oxyAccountDetailsDto;
	}

	@Override
	@Transactional
	public LoanEmiCardResponseDto emiPaidByBorrowerUsingPaymentGateway(String oxyLoansTransactionNumber,
			LoanEmiCardResponseDto request) {
		LoanEmiCard loanEmiCard = loanEmiCardRepo.findByTransactionNumber(oxyLoansTransactionNumber);
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		if (loanEmiCard == null) {
			throw new NoSuchElementException("No records present with oxyLoansTransactionNumber "
					+ oxyLoansTransactionNumber + " and emi number ");
		}
		if (!loanEmiCard.getTransactionNumber().equals(oxyLoansTransactionNumber)) {
			throw new ActionNotAllowedException("Generated  TransactionNumber is not Valid",
					ErrorCodes.PAYU_TransactionNumber_NOT_EQUALS);
		}
		LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto = new LoanEmiCardPaymentDetailsRequestDto();
		loanEmiCardPaymentDetailsRequestDto.setAmountPaidDate(dateFormat.format(Calendar.getInstance().getTime()));
		loanEmiCardPaymentDetailsRequestDto.setEmiId(loanEmiCard.getId());
		loanEmiCardPaymentDetailsRequestDto.setLoanId("" + loanEmiCard.getLoanId());
		loanEmiCardPaymentDetailsRequestDto.setModeOfPayment(modeOfPayment);
		loanEmiCardPaymentDetailsRequestDto.setTransactionReferenceNumber(request.getPayuTransactionNumber());
		loanEmiCardPaymentDetailsRequestDto.setPartialPaymentAmount(
				request.getPartPayAmount() == null ? 0d : Double.parseDouble(request.getPartPayAmount()));
		loanEmiCardPaymentDetailsRequestDto.setPaymentStatus("PARTPAID");
		adminLoanService.getUnpaidLoanEmiDetails(loanEmiCard.getLoanId(), loanEmiCardPaymentDetailsRequestDto);
		LoanEmiCardResponseDto response = new LoanEmiCardResponseDto();
		response.setId(loanEmiCard.getId());
		return response;
	}

	@Override
	@Transactional
	public LoanEmiCardPaymentDetailsResponseDto updEmisByApplication(
			LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto) {

		if (loanEmiCardPaymentDetailsRequestDto.getApplicationId() == null) {
			throw new DataFormatException("Application Id should not be empty", ErrorCodes.DATA_FETCH_ERROR);
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		LoanEmiCardPaymentDetailsResponseDto response = new LoanEmiCardPaymentDetailsResponseDto();
		String amountPaidDate = loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate();
		Date date1 = null;
		try {
			date1 = new SimpleDateFormat("dd/MM/yyyy").parse(amountPaidDate);
		} catch (ParseException e) {

			e.printStackTrace();
		}
		int countForpenality = 0;
		int countWayOff = 0;
		if (loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount() > 0) {
			List<Object[]> objList = updEmiByApplicationRepo.getEMIDetailsByApplicationId(
					Integer.parseInt(loanEmiCardPaymentDetailsRequestDto.getApplicationId()));
			if (objList == null) {
				throw new DataFormatException("No Loans Found for this application", ErrorCodes.DATA_FETCH_ERROR);
			}
		}
		int borrowerApplicationId = Integer.parseInt(loanEmiCardPaymentDetailsRequestDto.getApplicationId());
		LoanEmiCardResponseDto penaltyWaiveOffValue = adminLoanService
				.calculatePendingPenaltyTillDate(borrowerApplicationId);
		double excessAmount1 = 0.0;
		double pendingPenaltyTillDate = penaltyWaiveOffValue.getPenaltyDue();
		if (loanEmiCardPaymentDetailsRequestDto.getPenalty() <= pendingPenaltyTillDate) {
			double penaltyWavieOff = loanEmiCardPaymentDetailsRequestDto.getPenalty();
			if (penaltyWavieOff > 0) {
				List<Integer> listOfEmiNumbers = penaltyDetailsRepo.getpenaltyTilldateEmiNumbers(borrowerApplicationId);
				if (listOfEmiNumbers != null && !listOfEmiNumbers.isEmpty()) {

					double excessAmountForPenaltyWavieOff = 0.0;
					for (int i = 0; i < listOfEmiNumbers.size(); i++) {
						countWayOff = countWayOff + 1;
						int emiNumberForApplication = listOfEmiNumbers.get(i);

						PenaltyDetails penaltyDetailsForEmiNumber = penaltyDetailsRepo
								.findByEmiNumberAndBorrowerParentRequestid(emiNumberForApplication,
										borrowerApplicationId);
						if (penaltyDetailsForEmiNumber != null) {
							if (countWayOff == 1) {
								if (penaltyDetailsForEmiNumber.getPenaltyDue() > 0) {
									if (penaltyWavieOff >= penaltyDetailsForEmiNumber.getPenaltyDue()) {
										excessAmountForPenaltyWavieOff = penaltyWavieOff
												- penaltyDetailsForEmiNumber.getPenaltyDue();
										penaltyDetailsForEmiNumber
												.setPenaltyWaivedOff(penaltyDetailsForEmiNumber.getPenaltyWaivedOff()
														+ penaltyDetailsForEmiNumber.getPenaltyDue());
										penaltyDetailsForEmiNumber.setPenaltyWaivedDate(date1);
										penaltyDetailsForEmiNumber.setPenaltyDue(0.0);
										penaltyDetailsForEmiNumber
												.setPenaltyPaid(penaltyDetailsForEmiNumber.getPenalty());
										penaltyDetailsForEmiNumber.setPenaltyPaidOn(Calendar.getInstance().getTime());

										List<Integer> listLoansIds = oxyLoanRepo
												.getloansForAnApplication(borrowerApplicationId);
										for (int i1 = 0; i1 < listLoansIds.size(); i1++) {
											int loanId = listLoansIds.get(i1);
											LoanEmiCard loanEmiCard = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId,
													emiNumberForApplication);
											try {
												loanEmiCard.setEmiPaidOn(expectedDateFormat.parse(
														loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
											} catch (ParseException e) {

												e.printStackTrace();
											}
											loanEmiCard.setRemainingEmiAmount(loanEmiCard.getEmiAmount());
											loanEmiCard.setStatus(loanEmiCard.getStatus().COMPLETED);
											loanEmiCard.setModeOfPayment(
													loanEmiCardPaymentDetailsRequestDto.getModeOfPayment());
											loanEmiCard.setTransactionNumber(loanEmiCardPaymentDetailsRequestDto
													.getTransactionReferenceNumber());
											loanEmiCard.setEmiLateFeeCharges(0);
											loanEmiCardRepo.save(loanEmiCard);
										}

									} else {
										penaltyDetailsForEmiNumber.setPenaltyDue(penaltyWavieOff);
										penaltyDetailsForEmiNumber.setPenaltyWaivedOff(
												penaltyDetailsForEmiNumber.getPenaltyWaivedOff() + penaltyWavieOff);
										penaltyDetailsForEmiNumber.setPenaltyWaivedDate(date1);
										excessAmountForPenaltyWavieOff = 0;
									}
									penaltyDetailsRepo.save(penaltyDetailsForEmiNumber);
								} else {
									if (penaltyWavieOff >= penaltyDetailsForEmiNumber.getPenalty()) {
										int emiNumber = penaltyDetailsForEmiNumber.getEmiNumber();

										List<Integer> listLoansIds = oxyLoanRepo
												.getloansForAnApplication(borrowerApplicationId);
										int count = 0;
										for (int i1 = 0; i1 < listLoansIds.size(); i1++) {
											int loanId = listLoansIds.get(i1);
											int loanSize = listLoansIds.size();

											LoanEmiCard loanEmiCard = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId,
													emiNumber);
											if (loanEmiCard
													.getStatus() == com.oxyloans.entity.loan.LoanEmiCard.Status.COMPLETED
													&& loanEmiCard.getEmiLateFeeCharges() == 0) {
												loanEmiCard.setEmiPaidOn(Calendar.getInstance().getTime());
												count = count + 1;
												loanEmiCardRepo.save(loanEmiCard);
											}
											if (loanSize == count) {
												penaltyDetailsForEmiNumber
														.setPenaltyPaidOn(Calendar.getInstance().getTime());

											}
										}
										excessAmountForPenaltyWavieOff = penaltyWavieOff
												- penaltyDetailsForEmiNumber.getPenalty();

										penaltyDetailsForEmiNumber
												.setPenaltyWaivedOff(penaltyDetailsForEmiNumber.getPenaltyWaivedOff()
														+ penaltyDetailsForEmiNumber.getPenalty());
										penaltyDetailsForEmiNumber.setPenaltyWaivedDate(date1);
										penaltyDetailsForEmiNumber.setPenalty(0.0);

									} else {
										penaltyDetailsForEmiNumber
												.setPenalty(penaltyDetailsForEmiNumber.getPenalty() - penaltyWavieOff);
										penaltyDetailsForEmiNumber.setPenaltyWaivedOff(
												penaltyDetailsForEmiNumber.getPenaltyWaivedOff() + penaltyWavieOff);
										penaltyDetailsForEmiNumber.setPenaltyWaivedDate(date1);
										excessAmountForPenaltyWavieOff = 0;

									}

									penaltyDetailsRepo.save(penaltyDetailsForEmiNumber);
								}

							} else {
								if (excessAmountForPenaltyWavieOff > 0) {
									if (excessAmountForPenaltyWavieOff >= penaltyDetailsForEmiNumber.getPenalty()) {

										excessAmountForPenaltyWavieOff = excessAmountForPenaltyWavieOff
												- penaltyDetailsForEmiNumber.getPenalty();

										penaltyDetailsForEmiNumber
												.setPenaltyWaivedOff(penaltyDetailsForEmiNumber.getPenaltyWaivedOff()
														+ penaltyDetailsForEmiNumber.getPenalty());
										penaltyDetailsForEmiNumber.setPenalty(0.0);
										penaltyDetailsForEmiNumber.setPenaltyWaivedDate(date1);

									} else {
										penaltyDetailsForEmiNumber.setPenalty(penaltyDetailsForEmiNumber.getPenalty()
												- excessAmountForPenaltyWavieOff);
										penaltyDetailsForEmiNumber
												.setPenaltyWaivedOff(penaltyDetailsForEmiNumber.getPenaltyWaivedOff()
														+ excessAmountForPenaltyWavieOff);
										penaltyDetailsForEmiNumber.setPenaltyWaivedDate(date1);
										excessAmountForPenaltyWavieOff = 0;
									}
									penaltyDetailsRepo.save(penaltyDetailsForEmiNumber);

								}

							}

						}
					}
				}
			}
		} else {
			throw new OperationNotAllowedException("penaltyWaiveOff is greater than the penaltyDue",
					ErrorCodes.ENITITY_NOT_FOUND);
		}
		List<LoanEmiCardResponseDto> applicationDetails = adminLoanService
				.getApplicationLevelPenalityWithEmi(borrowerApplicationId);
		if (applicationDetails == null || applicationDetails.isEmpty()) {
			throw new OperationNotAllowedException("interest and penalty is not calculated",
					ErrorCodes.ENITITY_NOT_FOUND);
		}
		int countValue = 0;

		double applicationDue = 0.0;
		int paymentHistoryCountFullPayment = 0;
		int paymentHistoryCountPartPayment = 0;
		applicationDue = penaltyWaiveOffValue.getTotalApplicationLevelDue();
		if (loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount() <= applicationDue) {
			for (LoanEmiCardResponseDto applicationinfo : applicationDetails) {

				if (applicationinfo.getEmiPaidOn() == null) {
					if (countValue > 0) {
						loanEmiCardPaymentDetailsRequestDto.setPartialPaymentAmount(0.0);
					}
					if (loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount() >= applicationinfo
							.getDueAmountWithPenality() && applicationinfo.getDueAmountWithPenality() > 0) {
						if (paymentHistoryCountFullPayment == 0) {
							paymentHistoryCountFullPayment = paymentHistoryCountFullPayment + 1;
							PaymentHistory paymentHistory = new PaymentHistory();
							paymentHistory.setLoanRequestId("APBR" + borrowerApplicationId);
							paymentHistory.setAmount(applicationinfo.getDueAmountWithPenality());
							try {
								paymentHistory.setAmountPaidDate(expectedDateFormat
										.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
							} catch (ParseException e) {

								e.printStackTrace();
							}
							paymentHistoryDetailsRepo.save(paymentHistory);

						}

						PenaltyDetails penaltyDetails = penaltyDetailsRepo.findByEmiNumberAndBorrowerParentRequestid(
								applicationinfo.getEmiNumber(), borrowerApplicationId);
						List<Integer> listLoansIds = oxyLoanRepo.getloansForAnApplication(borrowerApplicationId);
						for (int i = 0; i < listLoansIds.size(); i++) {
							int loanId = listLoansIds.get(i);
							LoanEmiCard loanEmiCard = loanEmiCardRepo.findByLoanIdAndEmiNumber(loanId,
									applicationinfo.getEmiNumber());
							try {
								loanEmiCard.setEmiPaidOn(expectedDateFormat
										.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
							} catch (ParseException e) {

								e.printStackTrace();
							}
							loanEmiCard.setRemainingEmiAmount(loanEmiCard.getEmiAmount());
							loanEmiCard.setStatus(loanEmiCard.getStatus().COMPLETED);
							loanEmiCard.setEmiLateFeeCharges(0);
							loanEmiCard.setModeOfPayment(loanEmiCardPaymentDetailsRequestDto.getModeOfPayment());
							loanEmiCard.setTransactionNumber(
									loanEmiCardPaymentDetailsRequestDto.getTransactionReferenceNumber());
							loanEmiCard.setUpdatedUserName(loanEmiCardPaymentDetailsRequestDto.getUpdatedUserName());
							InterestDetails interestDetailsInfo = interestDetailsRepo
									.findByloanIdAndEmiNumber(loanEmiCard.getLoanId(), loanEmiCard.getEmiNumber());
							if (interestDetailsInfo != null) {
								interestDetailsInfo.setInterest(interestDetailsInfo.getInterest());
								interestDetailsInfo.setInterestPaid(new Date());
								interestDetailsRepo.save(interestDetailsInfo);
							}

							loanEmiCardRepo.save(loanEmiCard);
							Integer userId = oxyLoanRepo.getLenderEmail(loanId);
							User user = userRepo.findById(userId).get();
							String email = user.getEmail();
							/*
							 * TemplateContext context = new TemplateContext();
							 * 
							 * String firstName = user.getPersonalDetails().getFirstName().substring(0,
							 * 1).toUpperCase() + user.getPersonalDetails().getFirstName().substring(1);
							 * String lastName = user.getPersonalDetails().getLastName().substring(0,
							 * 1).toUpperCase() + user.getPersonalDetails().getLastName().substring(1);
							 * context.put("lenderName", firstName + " " + lastName);
							 * context.put("emiAmount", loanEmiCard.getEmiAmount()); // context.put("date",
							 * dateFormate.format(new Date())); context.put("date",
							 * loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()); String mailsubject
							 * = "Full payment"; // Mail Goes to Borrower so subject is like this.. String
							 * emailTemplateName = "full-payment.template"; EmailRequest emailRequest = new
							 * EmailRequest(new String[] { email }, mailsubject, emailTemplateName,
							 * context); EmailResponse emailResponse = emailService.sendEmail(emailRequest);
							 * if (emailResponse.getStatus() == EmailResponse.Status.FAILED) { throw new
							 * RuntimeException(emailResponse.getErrorMessage()); }
							 */
						}
						penaltyDetails.setPenaltyPaid(penaltyDetails.getPenalty());
						penaltyDetails.setPenaltyDue(0);
						penaltyDetails.setPenaltyPaidOn(Calendar.getInstance().getTime());
						penaltyDetailsRepo.save(penaltyDetails);
						excessAmount1 = loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount()
								- applicationinfo.getDueAmountWithPenality();
						if (excessAmount1 > 0) {
							loanEmiCardPaymentDetailsRequestDto
									.setApplicationId(loanEmiCardPaymentDetailsRequestDto.getApplicationId());
							loanEmiCardPaymentDetailsRequestDto.setPartialPaymentAmount(excessAmount1);
							loanEmiCardPaymentDetailsRequestDto.setPenalty(0.0);
							updEmisByApplication(loanEmiCardPaymentDetailsRequestDto);
						} else {
							loanEmiCardPaymentDetailsRequestDto
									.setApplicationId(loanEmiCardPaymentDetailsRequestDto.getApplicationId());
							loanEmiCardPaymentDetailsRequestDto.setPartialPaymentAmount(0.0);
							loanEmiCardPaymentDetailsRequestDto.setPenalty(0.0);
							updEmisByApplication(loanEmiCardPaymentDetailsRequestDto);
							break;
						}

					} else {
						int count = 0;
						int size = 0;
						double excess = 0;
						double afterSubstraction = loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount();
						if (paymentHistoryCountPartPayment == 0 && afterSubstraction > 0) {

							paymentHistoryCountPartPayment = paymentHistoryCountPartPayment + 1;
							PaymentHistory paymentHistory = new PaymentHistory();
							paymentHistory.setLoanRequestId("APBR" + borrowerApplicationId);
							paymentHistory.setAmount(afterSubstraction);
							try {
								paymentHistory.setAmountPaidDate(expectedDateFormat
										.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
							} catch (ParseException e) {

								e.printStackTrace();
							}
							paymentHistoryDetailsRepo.save(paymentHistory);
						}

						if (afterSubstraction > 0) {
							PenaltyDetails penaltyValue = penaltyDetailsRepo
									.getApplicationLevelDueAmount(borrowerApplicationId);

							if (penaltyValue != null) {

								double remainingAmount = penaltyValue.getPenaltyDue() - afterSubstraction;

								penaltyValue.setPenaltyDue(remainingAmount);

								penaltyDetailsRepo.save(penaltyValue);

								afterSubstraction = 0;
								break;

							}
						}

						List<Integer> listLoansIds = oxyLoanRepo.getloansForAnApplication(borrowerApplicationId);
						if (listLoansIds != null && !listLoansIds.isEmpty()) {
							size = listLoansIds.size();

							if (afterSubstraction > 0) {
								for (int i = 0; i < listLoansIds.size(); i++) {
									int loanId = listLoansIds.get(i);

									countValue = countValue + 1;
									count = count + 1;
									Double emiAmount = 0.0;

									Double emiDistributeAMount = 0.0;
									int applicationNumber = Integer
											.parseInt(loanEmiCardPaymentDetailsRequestDto.getApplicationId());
									Double emiAmount1 = 0.0;
									emiAmount1 = loansByApplicationNativeRepo.getFirstUnpaidEmis(applicationNumber);
									if (emiAmount1 == null) {

										emiAmount1 = loansByApplicationNativeRepo.getEmSecondMonth(applicationNumber);
									} else {

										emiAmount1 = loansByApplicationNativeRepo
												.getFirstEmiAmountApplicationLevel(applicationNumber);
									}

									if ((afterSubstraction < emiAmount1 && afterSubstraction != 0)
											|| afterSubstraction > emiAmount1) {
										emiAmount = loanEmiCardRepo.getFirstEmiAmount(loanId);
										if (emiAmount == null) {
											emiAmount = loanEmiCardRepo.getSecondEmiAmount(loanId);
										}

										double percentage = (emiAmount * 100) / emiAmount1;
										emiDistributeAMount = (afterSubstraction * percentage) / 100;

									}

									else if (afterSubstraction == emiAmount1) {
										emiAmount = loanEmiCardRepo.getFirstEmiAmount(loanId);
										if (emiAmount == null) {
											emiAmount = loanEmiCardRepo.getSecondEmiAmount(loanId);
										}

										if (count == 1) {
											if (afterSubstraction > emiAmount) {
												excess = afterSubstraction - emiAmount;
												emiDistributeAMount = emiAmount;
											} else {
												emiDistributeAMount = afterSubstraction;
												excess = 0;
											}
										} else {
											if (excess > emiAmount) {
												excess = excess - emiAmount;
												emiDistributeAMount = emiAmount;
											} else {
												emiDistributeAMount = excess;
												excess = 0;
											}
										}
									}

									if (emiDistributeAMount > 0) {
										LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto1 = new LoanEmiCardPaymentDetailsRequestDto();

										loanEmiCardPaymentDetailsRequestDto1.setAmountPaidDate(
												loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate());

										loanEmiCardPaymentDetailsRequestDto1.setModeOfPayment(
												loanEmiCardPaymentDetailsRequestDto.getModeOfPayment());
										loanEmiCardPaymentDetailsRequestDto1.setTransactionReferenceNumber(
												loanEmiCardPaymentDetailsRequestDto.getTransactionReferenceNumber());

										loanEmiCardPaymentDetailsRequestDto1
												.setPartialPaymentAmount(emiDistributeAMount);

										loanEmiCardPaymentDetailsRequestDto1.setPaymentStatus("PARTPAID");
										loanEmiCardPaymentDetailsRequestDto1.setUpdatedUserName(
												loanEmiCardPaymentDetailsRequestDto.getUpdatedUserName());
										int resp = adminLoanService.getUnpaidLoanEmiDetails(loanId,
												loanEmiCardPaymentDetailsRequestDto1);

										response.setEmiId(resp);
									}

								}
							}

						}
						if (count == size) {
							List<Object[]> objectsValue = oxyLoanRepo.getExcessAmountPaid(borrowerApplicationId);
							if (objectsValue != null && !objectsValue.isEmpty()) {
								Iterator it = objectsValue.iterator();
								while (it.hasNext()) {
									Object e2[] = (Object[]) it.next();
									int emiNumber = Integer.parseInt(e2[0] == null ? "0" : e2[0].toString());
									double excessAmountValue = Double
											.parseDouble(e2[1] == null ? "0" : e2[1].toString());
									if (excessAmountValue > 0) {
										PenaltyDetails penalityValue = penaltyDetailsRepo
												.findByEmiNumberAndBorrowerParentRequestid(emiNumber,
														borrowerApplicationId);
										if (excessAmountValue >= penalityValue.getPenalty()) {
											penalityValue.setPenaltyPaid(penalityValue.getPenalty());
											penalityValue.setPenaltyPaidOn(Calendar.getInstance().getTime());
											penalityValue.setPenaltyDue(0.0);
										} else {
											penalityValue.setPenaltyDue(penalityValue.getPenalty() - excessAmountValue);
											// excessAmountValue = 0;
										}
										penaltyDetailsRepo.save(penalityValue);
										excessAmountValue = 0;
									}
									// break;
									else {
										loanEmiCardPaymentDetailsRequestDto.setApplicationId(
												loanEmiCardPaymentDetailsRequestDto.getApplicationId());
										loanEmiCardPaymentDetailsRequestDto.setPartialPaymentAmount(0.0);
										loanEmiCardPaymentDetailsRequestDto.setPenalty(0.0);
										updEmisByApplication(loanEmiCardPaymentDetailsRequestDto);
										break;
									}
								}
							}
						}
					}
				}
			}
		} else {
			double exactAmount = loanEmiCardPaymentDetailsRequestDto.getPartialPaymentAmount() - applicationDue;

			List<Integer> listLoansIds = oxyLoanRepo.getloansForAnApplication(borrowerApplicationId);
			for (int i = 0; i < listLoansIds.size(); i++) {
				int loanId = listLoansIds.get(i);
				OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get();
				oxyLoan.setLoanStatus(LoanStatus.Closed);
				oxyLoanRepo.save(oxyLoan);
				List<LoanEmiCard> loanEmiCard = loanEmiCardRepo.findByLoanId(loanId);
				for (LoanEmiCard loan : loanEmiCard) {
					if (loan.getEmiNumber() == 1) {
						loan.setPenality(exactAmount);
					}
					loan.setRemainingEmiAmount(loan.getEmiAmount());
					loan.setEmiLateFeeCharges(0);
					loan.setStatus(loan.getStatus().COMPLETED);
					try {
						loan.setEmiPaidOn(
								expectedDateFormat.parse(loanEmiCardPaymentDetailsRequestDto.getAmountPaidDate()));
					} catch (ParseException e) {

						e.printStackTrace();
					}
					loan.setModeOfPayment(loanEmiCardPaymentDetailsRequestDto.getModeOfPayment());
					loan.setTransactionNumber(loanEmiCardPaymentDetailsRequestDto.getTransactionReferenceNumber());
					loanEmiCardRepo.save(loan);
					List<PenaltyDetails> penaltyDetails = penaltyDetailsRepo
							.findByBorrowerParentRequestid(borrowerApplicationId);
					for (PenaltyDetails penalty : penaltyDetails) {
						if (penalty.getPenalty() > 0) {
							penalty.setPenaltyPaid(penalty.getPenalty());
							penalty.setPenaltyDue(0.0);
							penalty.setPenaltyPaidOn(Calendar.getInstance().getTime());
						} else {
							penalty.setPenaltyPaid(0.0);
							penalty.setPenaltyDue(0.0);
							penalty.setPenaltyPaidOn(Calendar.getInstance().getTime());
						}
						penaltyDetailsRepo.save(penalty);
					}
				}
			}
		}

		int paidEmisCount = countValue(borrowerApplicationId);
		logger.info("UpdEmisByApplication method end !!!!!!");

		return response;
	}

	public int countValue(int borrowerApplicationId) {
		List<PenaltyDetails> penalityDetails = penaltyDetailsRepo.findByBorrowerParentRequestid(borrowerApplicationId);
		int countForPaidEmis = 0;
		int countForDuration = 0;

		if (penalityDetails != null) {

			for (PenaltyDetails penaltyDetailsInfo : penalityDetails) {
				countForDuration = countForDuration + 1;
				if (penaltyDetailsInfo.getPenaltyPaidOn() != null) {
					countForPaidEmis = countForPaidEmis + 1;
				}
			}

			if (countForDuration == countForPaidEmis) {
				List<Integer> listLoansIds = oxyLoanRepo.getloansForAnApplication(borrowerApplicationId);
				for (int i = 0; i < listLoansIds.size(); i++) {
					int loanId = listLoansIds.get(i);
					OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get();
					oxyLoan.setLoanStatus(LoanStatus.Closed);
					oxyLoanRepo.save(oxyLoan);

				}
			}
		}
		return countForPaidEmis;
	}

	@Override
	public OxyDashboardDetailsDto borrowerDashBordDetails(int userId) {
		List<LoanStatus> loanStatuses = new ArrayList<LoanStatus>();
		loanStatuses.add(LoanStatus.Closed);
		loanStatuses.add(LoanStatus.Agreed);
		loanStatuses.add(LoanStatus.Active);
		User user = userRepo.findById(userId).get();
		Object sumOfBorrowerAllLoans = null;
		List<Object[]> countByLoanStatus = null;
		Object sumOfAllClosedLoans = null;
		Object sumOfAllAgreedLoans = null;
		Object sumOfAllActiveLoans = null;

		sumOfBorrowerAllLoans = oxyLoanRepo.sumOfBorrowerAllLoans(userId, loanStatuses.toArray(new LoanStatus[0]));
		countByLoanStatus = oxyLoanRepo.borrowerCountByLoanStatus(userId, loanStatuses.toArray(new LoanStatus[0]));
		sumOfAllClosedLoans = oxyLoanRepo.sumOfBorrowerAllLoans(userId, new LoanStatus[] { LoanStatus.Closed });
		sumOfAllAgreedLoans = oxyLoanRepo.sumOfBorrowerAllLoans(userId, new LoanStatus[] { LoanStatus.Agreed });
		sumOfAllActiveLoans = oxyLoanRepo.sumOfBorrowerAllLoans(userId, new LoanStatus[] { LoanStatus.Active });
		Object[] objects = (Object[]) sumOfBorrowerAllLoans;
		Object[] closedLoansObjects = (Object[]) sumOfAllClosedLoans;
		Object[] agreedLoansObjects = (Object[]) sumOfAllAgreedLoans;
		Object[] activeLoansObjects = (Object[]) sumOfAllActiveLoans;
		Map<LoanStatus, Integer> counts = new HashMap<LoanStatus, Integer>();

		BorrowerAccountDetailsDto borrowerAccountDetailsDto = populatedDetails(user, objects);

		OxyAccountDetailsDto oxyAccountDetailsDto = (OxyAccountDetailsDto) borrowerAccountDetailsDto;

		Map<LoanStatus, BigDecimal> amounts = new HashMap<LoanStatus, BigDecimal>();
		if (countByLoanStatus != null && countByLoanStatus.size() > 0) {
			for (Object[] entries : countByLoanStatus) {

				LoanStatus loanStatus = LoanStatus.valueOf(entries[0].toString());
				counts.put(loanStatus, Integer.valueOf(entries[1].toString()));
				BigDecimal amount = amounts.get(loanStatus);
				if (amount == null) {
					amounts.put(loanStatus, new BigDecimal((Double) objects[0]));
				} else {
					amounts.put(loanStatus, amount.add(new BigDecimal((Double) objects[0])));
				}
			}

			if (!counts.isEmpty()) {
				int activeCount = counts.containsKey(LoanStatus.Active) ? counts.get(LoanStatus.Active) : 0;
				int closedCount = counts.containsKey(LoanStatus.Closed) ? counts.get(LoanStatus.Closed) : 0;
				oxyAccountDetailsDto.setNoOfActiveLoans(activeCount);
				oxyAccountDetailsDto.setNoOfClosedLoans(closedCount);
				oxyAccountDetailsDto.setNoOfDisbursedLoans(activeCount + closedCount);
			}

			Integer activeLoansCount = oxyLoanRepo.getApplicationCount(userId, LoanStatus.Active.toString());
			Integer closedLoansCount = oxyLoanRepo.getApplicationCount(userId, LoanStatus.Closed.toString());
			int active = activeLoansCount == null ? 0 : activeLoansCount;
			int closed = closedLoansCount == null ? 0 : closedLoansCount;
			oxyAccountDetailsDto.setNoOfActiveApplications(active);
			oxyAccountDetailsDto.setNoOfClosedApplications(closed);
			oxyAccountDetailsDto.setNoOfDisbursedApplications(active + closed);

			oxyAccountDetailsDto.setNoOfFailedEmis(((Long) objects[4]).intValue());
			oxyAccountDetailsDto
					.setActiveLoansAmount(activeLoansObjects[0] == null ? 0d : (double) activeLoansObjects[0]);
			oxyAccountDetailsDto
					.setClosedLoansAmount(closedLoansObjects[0] == null ? 0d : (double) closedLoansObjects[0]);
			oxyAccountDetailsDto
					.setInProcessAmount(agreedLoansObjects[0] == null ? 0d : (double) agreedLoansObjects[0]);
			oxyAccountDetailsDto.setAmountDisbursed(oxyAccountDetailsDto.getAmountDisbursed().doubleValue()
					- oxyAccountDetailsDto.getInProcessAmount().doubleValue());
		}

		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			oxyAccountDetailsDto.setOutstandingAmount(oxyAccountDetailsDto.getLoanOfferedAmount().doubleValue()
					- oxyAccountDetailsDto.getInProcessAmount().doubleValue()
					- (activeLoansObjects[0] == null ? 0d : (double) activeLoansObjects[0]));
		}

		if (user.getPrimaryType() == PrimaryType.BORROWER) {
			oxyAccountDetailsDto.setMyTrackingStatus(statusForBorrower(userId));
		}

		if (user.getExperianSummary() != null && user.getUserProfileRisk() != null) {
			oxyAccountDetailsDto
					.setOxyScore(user.getExperianSummary().getScore() + user.getUserProfileRisk().getTotalScore());
		}
		Double borrowerFee = loanRequestRepo.sumOfBorrowerFee(userId);
		oxyAccountDetailsDto.setTotalTransactionFee(borrowerFee == null ? 0.0d : borrowerFee);
		int noOfLoanRequests = loanRequestRepo.countOfLoanRequest(userId);
		oxyAccountDetailsDto.setNoOfLoanRequests(noOfLoanRequests);
		int noOfLoanreponce = loanRequestRepo.countOfResponces(userId);
		oxyAccountDetailsDto.setNoOfLoanResponses(noOfLoanreponce);
		Object CommitAmount = loanRequestRepo.sumOfCommitmentAmount(0, userId);
		Integer noOfApplications = loanRequestRepo.sumOfApplications(userId);
		oxyAccountDetailsDto.setNoOfApplications(noOfApplications == null ? 0 : noOfApplications);

		if (CommitAmount == null) {
			double amount = 0.0d;
			oxyAccountDetailsDto.setCommitmentAmount(amount); // Total Commitment
		} else {
			double amount = (double) CommitAmount;
			oxyAccountDetailsDto.setCommitmentAmount(amount); // Total Commitment
		}
		List<Object[]> principulAndInterest = loanEmiCardRepo.getApplicationPrincipulandInterest(userId);
		for (Object[] obj : principulAndInterest) {
			oxyAccountDetailsDto.setPrincipalReceived(obj[0] == null ? 0.0d : Math.round((double) obj[0]));
			oxyAccountDetailsDto.setInterestPaid(obj[1] == null ? 0.0d : Math.round((double) obj[1]));
		}
		Double loanOfferedAmount = loanRequestRepo.sumByLoanOfferedAmount(userId);
		if (loanOfferedAmount == null) {
			oxyAccountDetailsDto.setLoanOfferedAmount(oxyAccountDetailsDto.getAmountDisbursed());
		} else {
			oxyAccountDetailsDto.setLoanOfferedAmount(loanOfferedAmount == null ? 0.0d : loanOfferedAmount);
		}
		return oxyAccountDetailsDto;
	}

	@Override
	public KycFileResponse getDataFromExcel() {
		List<EmisPaidInformation> dtos = new ArrayList<EmisPaidInformation>();
		FileInputStream file;

		KycFileResponse fileResponse = new KycFileResponse();
		ByteArrayOutputStream output = null;
		FileOutputStream outFile = null;

		try {
			file = new FileInputStream(new File(emiFilePath));

			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);

			Iterator<Row> rowIterator1 = sheet.iterator();
			List<Row> rowIterator = Lists.newArrayList(rowIterator1);

			XSSFWorkbook workbook1 = new XSSFWorkbook();
			XSSFSheet spreadsheet = workbook1.createSheet("update emi details");
			Row row0 = spreadsheet.createRow(0);

			Cell cell = row0.createCell(0);
			cell.setCellValue("S.no");

			cell = row0.createCell(1);
			cell.setCellValue("Borrower Id");

			cell = row0.createCell(2);
			cell.setCellValue("status");

			int rowCount = 0;

			for (Row row : rowIterator.subList(1, rowIterator.size())) {

				EmisPaidInformation emisPaidInformation = new EmisPaidInformation();
				emisPaidInformation.setsNo(row.getCell(0).getNumericCellValue());
				emisPaidInformation.setDisbursedDate(row.getCell(1).getDateCellValue());
				emisPaidInformation.setBorrowerName(row.getCell(2).getStringCellValue());
				emisPaidInformation.setBorrowerID(row.getCell(3).getStringCellValue());

				emisPaidInformation.setLoanAmount(row.getCell(9).getNumericCellValue());
				emisPaidInformation.setRateOfInaterest(row.getCell(10).getNumericCellValue());
				emisPaidInformation.setDuration((int) row.getCell(11).getNumericCellValue());
				emisPaidInformation.setEmiStartDate(row.getCell(12).getDateCellValue());

				String uniqueNumber = row.getCell(3).getStringCellValue();
				double rateOfInterest = row.getCell(10).getNumericCellValue();
				int duration = (int) row.getCell(11).getNumericCellValue();
				int emiPaidCount = (int) row.getCell(16).getNumericCellValue();
				int penaltyValue = (int) row.getCell(19).getNumericCellValue();
				int partPayment = (int) row.getCell(22).getNumericCellValue();
				int amountPending = (int) row.getCell(20).getNumericCellValue();
				int totalPenalty = (int) row.getCell(21).getNumericCellValue();

				if (!uniqueNumber.isEmpty()) {
					LoanEmiCardResponseDto loanEmiCardResponseDto = new LoanEmiCardResponseDto();
					loanEmiCardResponseDto.setRateOfInterest(rateOfInterest);
					loanEmiCardResponseDto.setDuration(duration);
					loanEmiCardResponseDto.setEmisPaid(emiPaidCount);
					loanEmiCardResponseDto.setPartPayAmount(String.valueOf(partPayment));
					loanEmiCardResponseDto.setAmountPending(Double.valueOf(amountPending));
					loanEmiCardResponseDto.setTotalPenlty(Double.valueOf(totalPenalty));
					double penaltyValueConversion = penaltyValue;
					loanEmiCardResponseDto.setPenalty(penaltyValueConversion);

					String message = updatePaidEmis(uniqueNumber, loanEmiCardResponseDto);
					String emiUpdatedStatus = null;

					if (message.equals("SUCCESS")) {

						emiUpdatedStatus = "updated";
					} else {

						emiUpdatedStatus = "not updated";
					}

					dtos.add(emisPaidInformation);

					Row row1 = spreadsheet.createRow(++rowCount);

					Cell cell1 = row1.createCell(0);
					cell1.setCellValue(row.getCell(0).getNumericCellValue());

					cell1 = row1.createCell(1);
					cell1.setCellValue(uniqueNumber);

					cell1 = row1.createCell(2);
					cell1.setCellValue(emiUpdatedStatus);

				}
			}

			file.close();

			output = new ByteArrayOutputStream();
			workbook1.write(output);
			FileRequest fileRequest = new FileRequest();
			KycFileRequest kyc = new KycFileRequest();
			InputStream fileStream = new ByteArrayInputStream(output.toByteArray());
			fileRequest.setInputStream(fileStream);
			kyc.setFileName(LocalDate.now() + "emisupdated.xlsx");

			fileRequest.setFileName(kyc.getFileName());
			fileRequest.setFileType(FileType.Agreement);
			fileRequest.setFilePrifix(FileType.Agreement.name());
			fileManagementService.putFile(fileRequest);
			FileResponse file1 = fileManagementService.getFile(fileRequest);
			fileResponse.setDownloadUrl(file1.getDownloadUrl());
			fileResponse.setFileName(LocalDate.now() + "emisupdated.xlsx");
			fileResponse.setKycType(KycType.EMISDETAILS);
			boolean deleted = fileManagementService.deleteFile(fileRequest);
			logger.warn("Deleted the File : " + deleted);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return fileResponse;
	}

	public String updatePaidEmis(String uniqueNumber, LoanEmiCardResponseDto loanEmiCardResponseDto) {
		Double rateOfInterest = loanEmiCardResponseDto.getRateOfInterest();
		Integer duration = loanEmiCardResponseDto.getDuration();
		Integer emiPaidCount = loanEmiCardResponseDto.getEmisPaid();
		Double penalty = loanEmiCardResponseDto.getPenalty();
		Double partPayment = Double.parseDouble(loanEmiCardResponseDto.getPartPayAmount());
		Double amountPending = loanEmiCardResponseDto.getAmountPending();
		Double totalPenalty = loanEmiCardResponseDto.getTotalPenlty();
		String message = null;
		User user = userRepo.findByUniqueNumber(uniqueNumber);
		if (user != null) {
			Integer borrowerId = user.getId();
			List<Integer> oxyloan = oxyLoanRepo.findByBorrowerIdAndRateOfInterestAndDuration(borrowerId, rateOfInterest,
					duration);
			for (int i1 = 0; i1 < oxyloan.size(); i1++) {
				int borrowerUserId = oxyloan.get(i1);
				List<Integer> listOfLoanIdsForApplication = oxyLoanRepo.getloansForAnApplication(borrowerUserId);
				if (listOfLoanIdsForApplication != null) {
					for (int i = 0; i < listOfLoanIdsForApplication.size(); i++) {
						int loanId = listOfLoanIdsForApplication.get(i);
						List<LoanEmiCard> loanEmiCard = loanEmiCardRepo.findByLoanIdOrderByEmiNumber(loanId);
						int count = 0;
						int countForLoanid = 0;
						if (loanEmiCard != null && !loanEmiCard.isEmpty()) {
							for (LoanEmiCard individualLoanInfo : loanEmiCard) {
								if (emiPaidCount > 0 && count < emiPaidCount) {

									individualLoanInfo.setEmiPaidOn(individualLoanInfo.getEmiDueOn());
									individualLoanInfo.setStatus(com.oxyloans.entity.loan.LoanEmiCard.Status.COMPLETED);
									individualLoanInfo.setEmiLateFeeCharges(0.0);
									InterestDetails interestDetails = interestDetailsRepo
											.findByloanIdAndEmiNumber(loanId, individualLoanInfo.getEmiNumber());
									if (interestDetails != null) {
										interestDetails.setInterestPaid(individualLoanInfo.getEmiDueOn());
										interestDetailsRepo.save(interestDetails);
									}
									loanEmiCardRepo.save(individualLoanInfo);
									if (countForLoanid == 0) {
										int borrowerParentRequestid = borrowerUserId;
										PenaltyDetails penalityDetails = penaltyDetailsRepo
												.findByEmiNumberAndBorrowerParentRequestid(
														individualLoanInfo.getEmiNumber(), borrowerParentRequestid);
										int parentRequestID = borrowerParentRequestid;
										List<Object[]> unpaidPenaltyList = penaltyDetailsRepo
												.getEmisUnPaidInfo(parentRequestID);
										if (unpaidPenaltyList != null && !unpaidPenaltyList.isEmpty()) {
											Iterator it = unpaidPenaltyList.iterator();
											while (it.hasNext()) {
												Object e[] = (Object[]) it.next();
												int emiNumber = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
												PenaltyDetails penalityDetails1 = penaltyDetailsRepo
														.findByEmiNumberAndBorrowerParentRequestid(emiNumber,
																borrowerParentRequestid);
												penalityDetails1.setPenalty(penalty);
												penaltyDetailsRepo.save(penalityDetails1);
											}
											if (penalityDetails != null) {
												penalityDetails.setPenaltyPaidOn(individualLoanInfo.getEmiDueOn());
												penaltyDetailsRepo.save(penalityDetails);
											}

										}
									}

								}
								count = count + 1;
							}
						}

						countForLoanid = countForLoanid + 1;
					}
					logger.info("updating data to loanEmiCard ended");

				}

			}
			if (partPayment > 0) {
				List<Object[]> listApplicationLevelDetails = oxyLoanRepo.getApplicationLevelObjects(borrowerId,
						rateOfInterest, duration);
				if (listApplicationLevelDetails != null && !listApplicationLevelDetails.isEmpty()) {
					Iterator it = listApplicationLevelDetails.iterator();
					int count = 0;

					while (it.hasNext()) {

						Object e[] = (Object[]) it.next();
						int loanId = Integer.parseInt(e[1] == null ? "0" : e[1].toString());
						OxyLoan oxyLoan = oxyLoanRepo.findById(loanId).get();
						if (count == 0) {
							if (partPayment > 0 && amountPending == 0 && totalPenalty == 0) {
								oxyLoan.setPrincipalRepaid(partPayment);
								oxyLoan.setLoanStatus(LoanStatus.Closed);
								count = count + 1;
							}
						} else {
							oxyLoan.setLoanStatus(LoanStatus.Closed);

						}
						oxyLoanRepo.save(oxyLoan);
					}

				}
				logger.info("closing of the loan method ended");
				if (partPayment > 0 && amountPending != 0 && totalPenalty != 0) {
					List<Integer> oxyloan1 = oxyLoanRepo.findByBorrowerIdAndRateOfInterestAndDuration(borrowerId,
							rateOfInterest, duration);
					for (int i1 = 0; i1 < oxyloan1.size(); i1++) {

						int borrowerUserId = oxyloan1.get(i1);
						LoanEmiCardPaymentDetailsRequestDto loanEmiCardPaymentDetailsRequestDto = new LoanEmiCardPaymentDetailsRequestDto();
						loanEmiCardPaymentDetailsRequestDto.setApplicationId(String.valueOf(borrowerUserId));
						loanEmiCardPaymentDetailsRequestDto.setPartialPaymentAmount(partPayment);
						loanEmiCardPaymentDetailsRequestDto.setPenalty(0.0);
						loanEmiCardPaymentDetailsRequestDto.setTransactionReferenceNumber(String.valueOf(12345));
						Date date = new Date();
						SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
						String currentDate = formatter.format(date);
						loanEmiCardPaymentDetailsRequestDto.setAmountPaidDate(currentDate);
						updEmisByApplication(loanEmiCardPaymentDetailsRequestDto);
					}

				}
			}

			message = "SUCCESS";
		} else {
			message = "FAIL";

		}

		return message;

	}

	@Override
	public ClosedDealsInformation getLenderClosedDealsInfo(int userId, PaginationRequestDto pageRequestDto) {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public EnachScheduledMailResponseDto readingLenderReferenceInfoToExcelSheet1() throws ParseException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}
}
