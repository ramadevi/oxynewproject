package com.oxyloans.service.loan;

import com.oxyloans.service.OxyRuntimeException;

public class LimitExceededException extends OxyRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public LimitExceededException(String message, String errorCode) {
		super(message, errorCode);
	}

}
