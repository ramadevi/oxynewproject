package com.oxyloans.service.loan;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oxyloans.customexceptions.ErrorCodes;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceResponse;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsResponseDto;
import com.oxyloans.entityborrowersdealsdto.ClosedDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.LendersPaticipationUpdationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformation;
import com.oxyloans.entity.borrowers.deals.information.OxyBorrowersDealsInformationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDeals;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDealsRepo;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletNativeRepo;
import com.oxyloans.entity.lender.returns.LendersReturnsRepo;
import com.oxyloans.entity.user.User;
import com.oxyloans.request.user.LenderReferenceRequestDto;
import com.oxyloans.response.user.EnachScheduledMailResponseDto;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.service.OperationNotAllowedException;
import com.oxyloans.serviceloan.LenderAccountDetailsDto;

@Service("LENDER")
public class LenderLoanService extends AbstractLoanService<LenderAccountDetailsDto> {

	private final org.apache.logging.log4j.Logger logger = LogManager.getLogger(AdminLoanService.class);

	@Autowired
	private OxyBorrowersDealsInformationRepo oxyBorrowersDealsInformationRepo;

	@Autowired
	private OxyLendersAcceptedDealsRepo oxyLendersAcceptedDealsRepo;

	@Autowired
	private LendersPaticipationUpdationRepo lendersPaticipationUpdationRepo;

	@Autowired
	private LendersReturnsRepo lendersReturnsRepo;

	@Autowired
	private LenderOxyWalletNativeRepo lenderOxyWalletNativeRepo;

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Override
	protected LenderAccountDetailsDto populatedDetails(User user, Object[] objects) {

		LenderAccountDetailsDto oxyAccountDetailsDto = new LenderAccountDetailsDto();

		oxyAccountDetailsDto.setAmountDisbursed(objects[0] == null ? 0d : Math.round((Double) objects[0]));
		oxyAccountDetailsDto.setInterestPaid(objects[1] == null ? 0d : Math.round((Double) objects[1]));
		oxyAccountDetailsDto.setTotalTransactionFee(objects[2] == null ? 0d : Math.round((Double) objects[2]));
		oxyAccountDetailsDto.setPrincipalReceived(objects[3] == null ? 0d : Math.round((Double) objects[3]));
		if (oxyAccountDetailsDto.getAmountDisbursed() != null) {
			oxyAccountDetailsDto.setOutstandingAmount(
					oxyAccountDetailsDto.getAmountDisbursed() - oxyAccountDetailsDto.getPrincipalReceived());
		}

		return oxyAccountDetailsDto;
	}

	@Override
	public ClosedDealsInformation getLenderClosedDealsInfo(int userId, PaginationRequestDto pageRequestDto) {
		DecimalFormat df = new DecimalFormat("0.00");
		ClosedDealsInformation closedDealsInformation = new ClosedDealsInformation();
		List<BorrowersDealsResponseDto> closedDeals = new ArrayList<BorrowersDealsResponseDto>();
		try {
			Integer pageSize = pageRequestDto.getPageSize();
			Integer pageNo = (pageRequestDto.getPageSize() * (pageRequestDto.getPageNo() - 1));
			List<Object[]> listOfClosedDeals = oxyBorrowersDealsInformationRepo
					.getLenderPaticipatedClosedDealInfo(userId, pageNo, pageSize);
			if (listOfClosedDeals != null && !listOfClosedDeals.isEmpty()) {
				Iterator it = listOfClosedDeals.iterator();
				while (it.hasNext()) {
					Object[] e = (Object[]) it.next();
					BorrowersDealsResponseDto borrowersDealsResponseDto = new BorrowersDealsResponseDto();
					Integer dealId = Integer.parseInt(e[0] == null ? "0" : e[0].toString());
					borrowersDealsResponseDto.setDealId(dealId);
					borrowersDealsResponseDto.setDealName(e[1] == null ? " " : e[1].toString());
					borrowersDealsResponseDto.setBorrowerDealClosingStatus(e[2] == null ? " " : e[2].toString());
					borrowersDealsResponseDto
							.setBorrowerClosedDate(expectedDateFormat.format(dateFormat.parse(e[3].toString())));
					OxyLendersAcceptedDeals oxyLendersAcceptedDeals = oxyLendersAcceptedDealsRepo
							.toCheckUserAlreadyInvoledInDeal(userId, dealId);
					Double totalUpdatedSum = lendersPaticipationUpdationRepo.getSumOfLenderUpdatedAmountToDeal(userId,
							dealId);
					Double totalParticipation = 0.0;
					if (totalUpdatedSum != null) {
						totalParticipation = oxyLendersAcceptedDeals.getParticipatedAmount() + totalUpdatedSum;
					} else {
						totalParticipation = oxyLendersAcceptedDeals.getParticipatedAmount();
					}
					borrowersDealsResponseDto
							.setLenderReturnType(oxyLendersAcceptedDeals.getLenderReturnsType().toString());
					borrowersDealsResponseDto.setLenderParticipatedDate(
							expectedDateFormat.format(oxyLendersAcceptedDeals.getReceivedOn()));
					OxyBorrowersDealsInformation dealsInformation = oxyBorrowersDealsInformationRepo.findById(dealId)
							.get();
					if (dealsInformation != null) {
						borrowersDealsResponseDto
								.setLoanActiveDate(expectedDateFormat.format(dealsInformation.getLoanActiveDate()));
						borrowersDealsResponseDto.setFundsAcceptanceStartDate(
								expectedDateFormat.format(dealsInformation.getFundsAcceptanceStartDate()));
					}
					Double sumOfInterest = lendersReturnsRepo.getSumOfAmountReturnedForInterest(userId, dealId);
					if (sumOfInterest != null) {
						borrowersDealsResponseDto
								.setTotalInterestEarned(BigDecimal.valueOf(sumOfInterest).toBigInteger());
					}
					Double movedFromPaticipationToWallet = lenderOxyWalletNativeRepo
							.findingSumOfAmountMovedFromDealToWallet(dealId, userId);
					if (movedFromPaticipationToWallet != null) {
						borrowersDealsResponseDto.setMovedFromPaticipationToWallet(
								BigDecimal.valueOf(movedFromPaticipationToWallet).toBigInteger());
					}

					OxyBorrowersDealsInformation oxyBorrowersDeals = oxyBorrowersDealsInformationRepo
							.toGetCalculationType(dealId);
					double rateOfInterest = 0.0;
					if (oxyBorrowersDeals != null) {

						rateOfInterest = oxyLendersAcceptedDeals.getRateofinterest();
					} else {
						rateOfInterest = oxyLendersAcceptedDeals.getRateofinterest() / 12;
					}
					borrowersDealsResponseDto.setRateOfInterest(Double.valueOf(df.format(rateOfInterest)));

					borrowersDealsResponseDto
							.setTotalPaticipation(BigDecimal.valueOf(totalParticipation).toBigInteger());
					closedDeals.add(borrowersDealsResponseDto);
				}
				closedDealsInformation
						.setClosedDealsCount(oxyBorrowersDealsInformationRepo.getClosedDealIdsCount(userId));
				closedDealsInformation.setBorrowersDealsResponseDto(closedDeals);
			}

		} catch (Exception e) {
			logger.info("Exception in getLenderClosedDealsInfo method");
		}

		return closedDealsInformation;

	}

	@Override
	public EnachScheduledMailResponseDto readingLenderReferenceInfoToExcelSheet1() throws ParseException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

	@Override
	public LenderReferenceResponse readingLenderReferenceToSheet1() throws ParseException {
		throw new OperationNotAllowedException("You are not authorized to do this operation",
				ErrorCodes.PERMISSION_DENIED);
	}

}
