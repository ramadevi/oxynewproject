package com.oxyloans.service.loan;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Map;
import java.text.ParseException;
import javax.mail.MessagingException;
import com.oxyloans.entityborrowersdealsdto.DealTenureUpdationResponse;
import com.oxyloans.autoinvest.LenderParticipationUsingAutoLendingResponse;
import com.oxyloans.borrower.noc.BorrowerDealNotificationRequestfordeals;
import com.oxyloans.borrower.noc.BorrowerNocRequestDto;
import com.oxyloans.borrower.noc.BorrowerNocResponseDto;
import com.oxyloans.cms.AfterApprovalFilesDto;
import com.oxyloans.cms.BeforeApprovalFilesDto;
import com.oxyloans.cms.CmsPaymentsStatisticsDto;
import com.oxyloans.cms.IntiatedFileDto;
import com.oxyloans.cms.NotificationsDto;
import com.oxyloans.cms.PaymentDetailDto;
import com.oxyloans.cms.RejectedFilesRequest;
import com.oxyloans.cms.TotalDealLevelInterestsDto;
import com.oxyloans.dashboard.LenderInterestBasedOnDeals;
import com.oxyloans.dashboard.NewLenderDashboardRequestDto;
import com.oxyloans.dashboard.NewLenderDashboardResponseDto;
import com.oxyloans.emi.BorrowerLoanRequestDto;
import com.oxyloans.emi.BorrowerLoanResponseDto;
import com.oxyloans.entityborrowersdealsdto.BorrowerDealNotificationRequest;
import com.oxyloans.entityborrowersdealsdto.BorrowerDealNotificationResponse;
import com.oxyloans.entityborrowersdealsdto.BorrowerFdDataDtoResponse;
import com.oxyloans.entityborrowersdealsdto.BorrowerOfferAcceptedStatusResponse;
import com.oxyloans.entityborrowersdealsdto.BorrowerPaymentsResponse;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsList;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsRequestDto;
import com.oxyloans.entityborrowersdealsdto.BorrowersDealsResponseDto;
import com.oxyloans.entityborrowersdealsdto.ChatBotRequest;
import com.oxyloans.entityborrowersdealsdto.ChatBotResponse;
import com.oxyloans.entityborrowersdealsdto.ClosedDealsInformation;
import com.oxyloans.entityborrowersdealsdto.DealLevelRequestDto;
import com.oxyloans.entityborrowersdealsdto.DealLevelResponseDto;
import com.oxyloans.entityborrowersdealsdto.DealLoanActiveDateCalculationRequest;
import com.oxyloans.entityborrowersdealsdto.DealLoanActiveDateCalculationResponse;
import com.oxyloans.entityborrowersdealsdto.DealQuarterlyReportsRequest;
import com.oxyloans.entityborrowersdealsdto.DealQuarterlyReportsResponse;
import com.oxyloans.entityborrowersdealsdto.DealsResponseDto;
import com.oxyloans.entityborrowersdealsdto.FdAmountResponseDto;
import com.oxyloans.entityborrowersdealsdto.GoogleSheetClosedDealsResponse;
import com.oxyloans.entityborrowersdealsdto.GoogleSheetResponse;
import com.oxyloans.entityborrowersdealsdto.GoogleSheetUserPersonalResponse;
import com.oxyloans.entityborrowersdealsdto.InterestReadRequest;
import com.oxyloans.entityborrowersdealsdto.LenderClosedDealsInformation;
import com.oxyloans.entityborrowersdealsdto.LenderDealsStatisticsInformation;
import com.oxyloans.entityborrowersdealsdto.LenderFurtherPaymentDetails;
import com.oxyloans.entityborrowersdealsdto.LenderInterestRequestDto;
import com.oxyloans.entityborrowersdealsdto.LenderInterestResponseDto;
import com.oxyloans.entityborrowersdealsdto.LenderPaidAmountRequestDto;
import com.oxyloans.entityborrowersdealsdto.LenderPaidAmountResponseDto;
import com.oxyloans.entityborrowersdealsdto.LenderParticipationUpdatedInfo;
import com.oxyloans.entityborrowersdealsdto.LenderPaticipatedDeal;
import com.oxyloans.entityborrowersdealsdto.LenderPaticipatedResponseDto;
import com.oxyloans.entityborrowersdealsdto.LendersRunningAmountInfomation;
import com.oxyloans.entityborrowersdealsdto.LendersWalletRequestDto;
import com.oxyloans.entityborrowersdealsdto.LendersWalletResponseDto;
import com.oxyloans.entityborrowersdealsdto.ListOfDealsInformationToLender;
import com.oxyloans.entityborrowersdealsdto.ListOfLenderInterestDetails;
import com.oxyloans.entityborrowersdealsdto.NewLenderExcelSheetGenerateResponse;
import com.oxyloans.entityborrowersdealsdto.NotificationGrapicalImageResponse;
import com.oxyloans.entityborrowersdealsdto.OxyLendersAcceptedDealsRequestDto;
import com.oxyloans.entityborrowersdealsdto.OxyLendersAcceptedDealsResponseDto;
import com.oxyloans.entityborrowersdealsdto.PendingInterestsDto;
import com.oxyloans.entityborrowersdealsdto.PerDealUsersCountResponse;
import com.oxyloans.entityborrowersdealsdto.PrincipalReturningStatusBasedData;
import com.oxyloans.entityborrowersdealsdto.PrincipalReturningStatusRequestDto;
import com.oxyloans.entityborrowersdealsdto.RemainderForInterestPaying;
import com.oxyloans.entityborrowersdealsdto.RunningDealInformation;
import com.oxyloans.entityborrowersdealsdto.TenureExtendsResponse;
import com.oxyloans.entityborrowersdealsdto.UserAmountGoogleSheetResponse;
import com.oxyloans.entityborrowersdealsdto.UserPanNumberRequest;
import com.oxyloans.entityborrowersdealsdto.UserPanNumberResponse;
import com.oxyloans.entityborrowersdealsdto.WebHookRequest;
import com.oxyloans.entityborrowersdealsdto.WebHookResponse;
import com.oxyloans.entity.expertSeekers.AdviseSeekersResponseDto;
import com.oxyloans.entity.expertSeekers.ExpertiseRequestDto;
import com.oxyloans.equity.deals.info.EquityDealsResponse;
import com.oxyloans.file.FileResponse;
import com.oxyloans.icici.dto.BeforeApprovalFiles;
import com.oxyloans.icici.dto.IciciExcelsheetApprovalRequestDto;
import com.oxyloans.icici.dto.LenderInterestPaymentsDto;
import com.oxyloans.icici.upi.TransactionDetailsFromQrCode;
import com.oxyloans.lender.participationToWallet.LenderPaticipationResponseDto;
import com.oxyloans.lender.participationToWallet.ListOfLendersMovingToWallet;
import com.oxyloans.lender.wallet.DealLevelInterestsDto;
import com.oxyloans.lender.wallet.LenderFeeDetailsDto;
import com.oxyloans.lender.wallet.LenderFeeDetailsHistoryDto;
import com.oxyloans.lender.wallet.LenderFeeDetailsResponse;
import com.oxyloans.lender.wallet.LenderFeeFilesResponse;
import com.oxyloans.lender.wallet.LenderFeeInWalletDto;
import com.oxyloans.lender.wallet.LenderFeeTransactions;
import com.oxyloans.lender.wallet.LenderMemberShipDetails;
import com.oxyloans.lender.wallet.PrincpleReturnsResponseDto;
import com.oxyloans.lender.wallet.ScrowLenderTransactionResponse;
import com.oxyloans.lender.wallet.UpdateLenderFeeData;
import com.oxyloans.lender.wallet.WalletRequestDto;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsFromDealsRequestDto;
import com.oxyloans.lender.withdrawalfunds.dto.LenderWithdrawalFundsFromDealsResponseDto;
import com.oxyloans.paymentstats.PaymentsStatsResponseDto;
import com.oxyloans.paytm.ListOfPaytmTransferedUsersDto;
import com.oxyloans.poolingaccount.PoolingAccountRequestDto;
import com.oxyloans.poolingaccount.PoolingAccountResponseDto;
import com.oxyloans.request.CampaignRequestDto;
import com.oxyloans.request.LenderReferralRequestDto;
import com.oxyloans.request.ReferralRequestDto;
import com.oxyloans.request.StudentRequestDto;
import com.oxyloans.request.TransactionAlertsDto;
import com.oxyloans.request.user.KycFileRequest;
import com.oxyloans.request.user.KycFileResponse;
import com.oxyloans.request.user.MemberShipValidityRequest;
import com.oxyloans.request.user.MonthlyValidityResponse;
import com.oxyloans.request.user.SpreadSheetRequestDto;
import com.oxyloans.request.user.TopFiftyLendersListRequsetDto;
import com.oxyloans.request.user.UserQueryDetailsRequestDto;
import com.oxyloans.request.user.UserRequest;
import com.oxyloans.response.admin.RBIStatsResponse;
import com.oxyloans.response.user.ActiveAmountResponse;
import com.oxyloans.response.user.ActiveLendersCountResponse;
import com.oxyloans.response.user.BankTransactionsDto;
import com.oxyloans.response.user.BorrowerReferenceResponse;
import com.oxyloans.response.user.CommentsResponseDto;
import com.oxyloans.response.user.DownLoadLinkResponse;
import com.oxyloans.response.user.EquityInvestorsExcelDto;
import com.oxyloans.response.user.FinancialYearResponseDto;
import com.oxyloans.response.user.HighestReferralBonusResponse;
import com.oxyloans.response.user.IciciExcelInterestsFiles;
import com.oxyloans.response.user.LenderBonusAmountStatus;
import com.oxyloans.response.user.LenderReferenceAmountResponseDto;
import com.oxyloans.response.user.LenderRenewalResponse;
import com.oxyloans.response.user.MaximumAmountInfo;
import com.oxyloans.response.user.OxyLendersGroups;
import com.oxyloans.response.user.PageRequestDto;
import com.oxyloans.response.user.PaginationRequestDto;
import com.oxyloans.response.user.ReadingCommitmentAmountDto;
import com.oxyloans.response.user.ReferralBonusBreakUpRequestDto;
import com.oxyloans.response.user.ReferralBonusBreakUpResponseDto;
import com.oxyloans.response.user.ReferralResponse;
import com.oxyloans.response.user.StatusResponseDto;
import com.oxyloans.response.user.TopFiftyLendersListDto;
import com.oxyloans.response.user.UniqueLendersResponseDto;
import com.oxyloans.response.user.UserPendingQuereisResponseDto;
import com.oxyloans.response.user.UserQueryDetailsResponse;
import com.oxyloans.response.user.UserQueryDetailsResponseDto;
import com.oxyloans.response.user.UserResponse;
import com.oxyloans.serviceloan.AdminDashboardDetails;
import com.oxyloans.serviceloan.DealLevelInterestPaymentsDto;
import com.oxyloans.serviceloan.InterestsApprovalDto;
import com.oxyloans.serviceloan.ProfileRiskCalculationDto;
import com.oxyloans.stats.InterestAmountResponse;
import com.oxyloans.stats.LenderDetailsResponseDto;
import com.oxyloans.stats.StatsRequestDto;
import com.oxyloans.usertypedto.ListOfBorrowersBasedOnType;
import com.oxyloans.usertypedto.OxyUserTypeRequestDto;
import com.oxyloans.usertypedto.OxyUserTypeResponseDto;
import com.oxyloans.webhooks.WebhookChangesDto;
import com.oxyloans.whatsappdto.StatusCheckDto;

public interface NewAdminLoanServiceRepo {

	public ListOfPaytmTransferedUsersDto getListOfPaytmBasedOnMonthAndYear(String month, String year,
			PaginationRequestDto pageRequestDto);

	public LendersWalletResponseDto getCurrentWalletBalance(LendersWalletRequestDto lendersWalletRequestDto);

	public ListOfLenderInterestDetails getListOfLendersInterestDetails(int dealId, String month, String year);

	public LenderInterestResponseDto updateLenderInterestAmount(int dealId,
			LenderInterestRequestDto lenderInterestRequestDto);

	public StatusResponseDto googleSheetWhatsappCampaign(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException;

	public UserQueryDetailsResponse gettingListOfUserQueryDetails(PaginationRequestDto pageRequestDto);

	public StatusResponseDto resolvingUSerQueryDetails(UserQueryDetailsRequestDto userQueryDetailsRequestDto);

	public UserResponse gettingMobileAndEmailOfUser(Integer userId);

	public DealLevelResponseDto dealLevelClosing(DealLevelRequestDto dealLevelRequestDto);

	public EquityInvestorsExcelDto getEquityInvestorsExcel(int dealId);

	public BorrowersDealsList getAllBorrowerDeals(String type);

	public LenderParticipationUpdatedInfo getLenderParticipationUpdatedDetails(int userId, int dealId);

	public AdviseSeekersResponseDto getListOfUniqueLenders(PaginationRequestDto request);

	public StatusResponseDto seekingSuggestionFromLenders(ExpertiseRequestDto request);

	public UniqueLendersResponseDto getUniqueLenderDetails(String userId);

	public UserResponse sendEmailOtp(UserRequest userRequest) throws MessagingException;

	public StatusResponseDto updateMobileNumberAndEmail(UserRequest request);

	public LenderDealsStatisticsInformation getTotalDealsStatistics(int userId);

	public DealLevelResponseDto updatingDealParticipationStatus(int dealId, DealLevelRequestDto dealLevelRequestDto);

	public List<RemainderForInterestPaying> sendingRemainderForPayingInterestToLenders()
			throws FileNotFoundException, IOException;

	public LenderRenewalResponse validityDetailsOfMembershipToGroup();

	public ClosedDealsInformation getClosedDealInformation(PaginationRequestDto pageRequestDto);

	public LenderReferenceAmountResponseDto gettingLenderReferenceInfoBasedOnId(Integer userId,
			LenderReferralRequestDto lenderReferralRequestDto);

	public NewLenderDashboardResponseDto lenderNewDashboard(NewLenderDashboardRequestDto newLenderDashboardRequestDto);

	public OxyLendersAcceptedDealsResponseDto insertingPaticipationForOldDeals(
			OxyLendersAcceptedDealsRequestDto oxyLendersAcceptedDealsRequestDto);

	public NewLenderDashboardResponseDto excelsForNewLenderDashboard(
			NewLenderDashboardRequestDto newLenderDashboardRequestDto);

	public LenderPaticipatedDeal getLenderPaticipatedAllDealsInfo(int userId, PaginationRequestDto pageRequestDto);

	public LenderBonusAmountStatus gettingReferralBonusAmountStatus(Integer userId);

	public LenderBonusAmountStatus gettingReferralStatusInfo(Integer userId);

	public UserQueryDetailsResponse gettingListOfUserQueryDetailsBasedOnPrimaryType(
			UserQueryDetailsRequestDto userQueryDetailsRequestDto);

	public UserQueryDetailsResponse gettingCountOfQueriesBasedOnStatus(
			UserQueryDetailsRequestDto userQueryDetailsRequestDto);

	public DealLevelInterestPaymentsDto interestDetailsForDeal(IciciExcelsheetApprovalRequestDto request);

	public DealLevelInterestPaymentsDto approvingInterestsForLenders(IciciExcelsheetApprovalRequestDto request);

	public DealLevelInterestPaymentsDto updateInterestsInfoExcelSheet(IciciExcelsheetApprovalRequestDto request);

	public List<InterestsApprovalDto> interestsInfoBreakUp(IciciExcelsheetApprovalRequestDto request);

	public List<DealLevelInterestPaymentsDto> currentDateDealsInfo(String monthName, String year, String status,
			int startDate, int endDate);

	public List<TotalDealLevelInterestsDto> newdealLevelInterestPayments(DealLevelInterestsDto dealLevelInterestsDto);

	public List<TotalDealLevelInterestsDto> dealLevelInterestPaymentsInfo(String monthName, String year, String status,
			int satrtDate, int endDate);

	public BorrowersDealsResponseDto getAllDealDataByName(DealLevelRequestDto dealLevelRequestDto);

	public LenderReferenceAmountResponseDto gettingPaidAndUnpaidBonusAmount(
			LenderBonusAmountStatus lenderBonusAmountStatus);

	public LenderReferenceAmountResponseDto gettingDownloadLinkForreferralAmountBasedOnStatus(
			LenderBonusAmountStatus lenderBonusAmountStatus);

	public UserQueryDetailsResponse gettingListOfUserQueryDetailsBasedOnId(
			UserQueryDetailsRequestDto userQueryDetailsRequestDto);

	public List<UserPendingQuereisResponseDto> gettingListOfUserPendingQueriesBasedOnId(Integer id);

	public LenderInterestBasedOnDeals getLenderReturnsInfo(NewLenderDashboardRequestDto newLenderDashboardRequestDto);

	public List<IciciExcelInterestsFiles> updatingFromICICIResponseFiles(IciciExcelsheetApprovalRequestDto request);

	public OxyUserTypeResponseDto typesOfBorrowersAndDetails(OxyUserTypeRequestDto OxyUserTypeRequestDto);

	public ListOfBorrowersBasedOnType getListOfUsersBasedOnType(PaginationRequestDto pageRequestDto);

	public OxyUserTypeResponseDto getSingleUserType(int userTypeId);

	public UserResponse uploadingCompanyDocDriveLink(Integer userId, UserRequest request);

	public List<HighestReferralBonusResponse> gettingHighestReferralBonus();

	public List<LenderRenewalResponse> sendingValidityDetailsOfMembershipToLender();

	public RBIStatsResponse gettingStatsBasedOnDates(LendersWalletRequestDto requestDto);

	public RBIStatsResponse gettingStatsBasedOnDatesToExcelSheet(LendersWalletRequestDto requestDto);

	public BorrowerReferenceResponse borrowerEligibilityDetails(Integer userId);

	public StatusResponseDto utmBasedExcelDownload(String primaryType, String utm);

	public StatusResponseDto emailCampaignToLenderAndBorrower(CampaignRequestDto request);

	public StatusResponseDto moveFileToApprovalFolder(IciciExcelsheetApprovalRequestDto request);

	public List<BeforeApprovalFilesDto> getListOfFilesFromBeforeApproval(IciciExcelsheetApprovalRequestDto request);

	public LenderPaticipationResponseDto movingPrincipalToWallet(int dealId,
			ListOfLendersMovingToWallet lenderPaticipationRequest);

	public PoolingAccountResponseDto getPoolingAccountDetails(PoolingAccountRequestDto poolingAccountRequestDto);

	public List<String> listOfPrincipalReturned(int userId, int dealId);

	public RunningDealInformation getAllRunningDeals(String dealType);

	public ReadingCommitmentAmountDto readingParticipationAmounts(BorrowersDealsRequestDto borrowersDealsRequestDto);

	public BankTransactionsDto getBankTransactions(TransactionAlertsDto request);

	public TransactionDetailsFromQrCode qrTransactionDetails(PaginationRequestDto pageRequestDto);

	public BankTransactionsDto getCurrentDayBankTransactions();

	public LendersRunningAmountInfomation LendersRunningAmountInfomation(PaginationRequestDto pageRequestDto);

	public BorrowerLoanResponseDto borrowerEmiDetails(BorrowerLoanRequestDto borrowerLoanRequestDto);

	public BorrowerOfferAcceptedStatusResponse borrowerOfferStatusCheck(int loanRequestId);

	public DealLoanActiveDateCalculationResponse firstInterestCalculation(
			DealLoanActiveDateCalculationRequest dealLoanActiveDateCalculationRequest);

	public BorrowerOfferAcceptedStatusResponse commentsGivenByAdmin(int loanRequestId);

	public DealLevelInterestPaymentsDto approvingInterestsForLenderss(IciciExcelsheetApprovalRequestDto request);

	public List<DealLevelInterestPaymentsDto> dealLevelInterestCmsInfo(String monthName, String year);

	public LenderClosedDealsInformation lenderClosedDealLoanStatement(int userId, int dealId);

	public ClosedDealsInformation lenderClosedDealsDownload(int userId);

	public List<PendingInterestsDto> pendingH2HNotifications();

	public Map<String, Boolean> freeWhatsappMessagesThroughExcel(KycFileRequest fileReqest, String mobileNumber,
			String message);

	public Map<String, Boolean> freeWhatsappMessages(StatusCheckDto request);

	public PaymentsStatsResponseDto getMonthlyPaymentStats(String month, String year);

	public DealLevelInterestPaymentsDto interestDetailsForDeall(IciciExcelsheetApprovalRequestDto request);

	public List<IciciExcelInterestsFiles> updatingFromICICIResponseFiles();

	public String closingLendersPaticipationStatus();

	public BorrowerNocResponseDto generateNocForBorrower(BorrowerNocRequestDto borrowerNocRequestDto)
			throws FileNotFoundException;

	public LenderPaticipationResponseDto movingPrincipalToH2hForWallet(Integer dealId,
			ListOfLendersMovingToWallet lenderPaticipationRequest);

	public DealLevelResponseDto displayingAssertDeals(PaginationRequestDto pageRequestDto);

	public UserResponse sendMobileOtpForAdviseSeekers(UserRequest userRequest);

	public LenderPaticipatedResponseDto principalReturnAccountType(
			OxyLendersAcceptedDealsRequestDto oxyLendersAcceptedDealsRequestDto);

	public String verifyEndPoint(String mode, String challenge, String verifytoken);

	public List<DealLevelResponseDto> initiatingEmailWhileInitiatingIntOrPrincipal(DealLevelRequestDto requestDto);

	// public WebhookChangesDto sampleNotification(WebhookChangesDto request);

	public ProfileRiskCalculationDto calculateprofileRisk(int userId);

	public WebhookChangesDto sampleNotification(String signature, WebhookChangesDto request);

	public KycFileResponse downloadingRunningDealsInfo(Integer userId);

	public List<FileResponse> readRejectedFilesInCms();

	public CmsPaymentsStatisticsDto getCmsStats(String date);

	public StatusResponseDto whatsappAndEmailCampaign(SpreadSheetRequestDto request)
			throws GeneralSecurityException, IOException, InterruptedException;

	public CommentsResponseDto updatingLendersValidityStatus(Integer userId, OxyLendersGroups oxyLenderGroup);

	public List<ActiveAmountResponse> highestLendingActiveAmountOfLenders();

	public KycFileResponse downloadLinkForNotParticipatedInDeal(StatsRequestDto requestDto);

	public List<LenderDetailsResponseDto> refereeDetailsInformation(Integer userId);

	public DealLevelResponseDto notificationsBeforedealCreation(DealLevelRequestDto dealLevelRequestDto);

	public List<UserQueryDetailsResponseDto> listOfQueryHistoryRaisedByUser(
			UserQueryDetailsRequestDto userQueryDetailsRequestDto);

	public List<DealsResponseDto> principalEndNotifications();

	public LenderPaticipatedDeal partiallyClosedDealsOfUser(Integer userId);

	public LenderPaticipatedDeal partiallyClosedDealsBasedOnPagination(Integer userId, PageRequestDto pageRequestDto);

	public NewLenderDashboardResponseDto closedDealsForUser(NewLenderDashboardRequestDto newLenderDashboardRequestDto);

	public NewLenderDashboardResponseDto closedDealsForUserBasedOnPagination(
			NewLenderDashboardRequestDto newLenderDashboardRequestDto);

	public LenderPaticipatedDeal runningDealsInfo(Integer userId);

	public LenderPaticipatedDeal runningDealsInfoBasedOnPagination(Integer userId, PageRequestDto pageRequestDto);

	public ClosedDealsInformation lenderClosedDealsDownloadForUser(Integer userId);

	public KycFileResponse partiallyClosedDealsDownload(Integer userId);

	public RejectedFilesRequest showRejectedFilesInCms();

	public String getDealEndNotifications();

	public ScrowLenderTransactionResponse deductingFeeFromWallet(WalletRequestDto walletRequestDto);

	public ScrowLenderTransactionResponse deductingLenderFeeFromWallet(WalletRequestDto walletRequestDto);

	public LenderFeeDetailsResponse lenderFeesInAdmin(LenderFeeDetailsHistoryDto lenderFeeDetailsDto);

	public List<LenderFeeDetailsResponse> getAllFeesDetails();

	public LenderFeeDetailsDto getPendingFeeStatusLenders();

	public List<PrincipalReturningStatusBasedData> statusBasedPrincipalReturned(
			PrincipalReturningStatusRequestDto principalReturn);

	public List<AfterApprovalFilesDto> filesInApprovalFolder();

	public List<BeforeApprovalFiles> filesBeforeApproval();

	public List<AfterApprovalFilesDto> filesInApprovalFolder(NotificationsDto request);

	public LenderFeeFilesResponse listOfLenderFeePaymentDetailsInWallet(LenderFeeInWalletDto lenderFeeInWalletDto);

	public LenderInterestPaymentsDto filesAfterApprovalNotReading();

	public LenderInterestPaymentsDto filesAfterApprovalNotReadingInAdmin();

	public LenderFeeTransactions lenderFeePaymentDetails(WalletRequestDto walletRequestDto);

	public LenderFeeTransactions feeDetailsForLenderBasedOnUserId(Integer userId, WalletRequestDto requestDto);

	public List<FinancialYearResponseDto> listOfFinancialYearsEarningData(Integer userId);

	public UserQueryDetailsResponseDto toCancelTheRequestRaisedByTheUser(UserQueryDetailsRequestDto requestDto);

	public MaximumAmountInfo toGetMaximumLimit();

	public LenderWithdrawalFundsFromDealsResponseDto toCancelTheWithdrawRequestRaisedByTheUser(
			LenderWithdrawalFundsFromDealsRequestDto requestDto);

	public String generatingFileForFeeFromWallet(WalletRequestDto requestDto);

	public InterestAmountResponse monthlyInterestEarningsBasedOnDates(StatsRequestDto requestDto);

	public BorrowersDealsList getDealsInfo(PageRequestDto request);

	public EquityDealsResponse listOfEquityDealsOfLender(Integer userId);

	public DealQuarterlyReportsResponse userQaurterlyReport(DealQuarterlyReportsRequest request, int id);

	public String usersQaurterlyReports(DealQuarterlyReportsRequest request);

	public ReferralResponse listOfReferralBonusInfo(ReferralRequestDto requestDto);

	public List<IciciExcelInterestsFiles> updatingICICIFilesOnCurrentDate();

	public List<BorrowerPaymentsResponse> dealPaymentToBorrower(int userId);

	public UserResponse takingInfoFromStudent(StudentRequestDto requestDto);

	public String FilesNotMovedOutput();

	public String lenderparticapatedAmount();

	public LenderFurtherPaymentDetails getfurtherMonthInterestDate(int userId, int dealId);

	public DealLevelResponseDto dealReopen(BorrowersDealsRequestDto borrowersDealsRequestDto);

	public TenureExtendsResponse extendsDealDuration(BorrowersDealsRequestDto borrowersDealsRequestDto);

	public void sendRemainderToLenderParticipationAmountReachesToLimit();

	public AdminDashboardDetails adminDashboard();

	public BorrowersDealsResponseDto dealOpenInfuture();

	public LenderParticipationUsingAutoLendingResponse lendersParticipatedUsingAutoLending(int userId, int pageNumber,
			int pageSize);

	public String principalAndLastInterestMissedPaymentsReminder();

	public List<DealTenureUpdationResponse> enableRemainderBeforeDealClouse() throws ParseException;

	public String missingPaymentReminder() throws ParseException;

	public String sendWithDrawnAmountReminder() throws ParseException;

	public String checkAndSendWalletToWalletRequestsRemainder() throws ParseException;

	public List<ReferralBonusBreakUpResponseDto> referralDealBasedBreakUp(
			ReferralBonusBreakUpRequestDto referralBonusBreakUpRequestDto);

	public BorrowerDealNotificationResponse notificationTodevTeam();

	public KycFileResponse downloadLinkForGettingActiveAmount(int pageNo, int pageSize);

	public BorrowerDealNotificationResponse notificationTodevTeamWeekly();

	public BorrowerDealNotificationResponse notificationTodevTeamMonthlyDev(
			BorrowerDealNotificationRequest borrowerDealNotificationRequest);

	public NewLenderExcelSheetGenerateResponse excelSheetGenerateForNewLenders1(
			BorrowerDealNotificationRequest borrowerDealNotificationRequest);

	public List<IciciExcelInterestsFiles> updatingFromICICIResponseFiles1(IciciExcelsheetApprovalRequestDto request);

	public List<IciciExcelInterestsFiles> updatingFromICICIResponseFiles1();

	public List<IciciExcelInterestsFiles> updatingICICIFilesOnCurrentDate1();

	public String outstandingCalculation();

	public String updateLendersLifetimeStatus(Integer dealId);

	public List<PrincpleReturnsResponseDto> lenderTotalReturns(Integer userId);

	public BorrowerDealNotificationResponse notificationTodevTeamMonthly();

	public LenderFeeTransactions lenderFeeDetaislOnUserId(Integer userId, WalletRequestDto requestDto);

	public List<NotificationGrapicalImageResponse> grapicalImageAdminModule(
			BorrowerDealNotificationRequest borrowerDealNotificationRequest);

	public ChatBotResponse getMessageIdForParticularMsg(ChatBotRequest chatBotRequest);

	public WebHookResponse saveWebHook(WebHookRequest webHookRequest);

	public List<IntiatedFileDto> getIntiatedFilesBeforeFolderWithBetweenDates(PaymentDetailDto paymentDetailDto);

	public String closingLendersPaticipationBy9oclock();

	public LenderReferenceAmountResponseDto displayPaidAmount(LenderBonusAmountStatus lenderBonusAmountStatus);

	public LenderReferenceAmountResponseDto displayMonthlyReferrersAmount(
			LenderBonusAmountStatus lenderBonusAmountStatus);

	public List<GoogleSheetResponse> googleSheetOxyloans(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException;

	public String missedAgreementsReminders();

	public PerDealUsersCountResponse perDealUsersCountAndAmount();

	public String usersWithotBankdetailUpdationParticipatedInDeals();

	public MonthlyValidityResponse userMemberShipExpireBetweenDates(
			MemberShipValidityRequest memberShipValidityRequest);

	public TopFiftyLendersListDto getTopFiftyLendersList(TopFiftyLendersListRequsetDto topFiftyLendersListRequsetDto);

	public DownLoadLinkResponse gettingHighestReferralBonusLenders(Integer number);

	public DownLoadLinkResponse activLendersParicipationAmount();

	public GoogleSheetUserPersonalResponse googleSheetOxyloansUserData(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException;

	public List<UserAmountGoogleSheetResponse> googleSheetOxyloansAmountUserData(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException;

	public GoogleSheetClosedDealsResponse googleSheetOxyloansClosedDealsCount(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException;

	public LenderPaidAmountResponseDto lenderFeePaidAmount(LenderPaidAmountRequestDto request);

	public String topBonusEarnedUsersToWhatsmessage();

	public FdAmountResponseDto fdAmount();

	public NewLenderExcelSheetGenerateResponse excelSheetGenerateForRunningDeals(int year, int month,
			String dealpaticipationstatus, BorrowerDealNotificationRequest borrowerDealNotificationRequest);

	public NewLenderExcelSheetGenerateResponse excelSheetGenerateForclosedDeals(int year, int month,
			String borrowerclosingstatus, BorrowerDealNotificationRequest borrowerDealNotificationRequest);

	public List<GoogleSheetResponse> googleSpreadSheets(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException;

	public GoogleSheetUserPersonalResponse spreadSheetOxyloansUserData(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException;

	public List<UserAmountGoogleSheetResponse> spreadSheetOxyloansAmountUserData(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException;

	public GoogleSheetClosedDealsResponse spreadSheetClosedDealsCount(SpreadSheetRequestDto request)
			throws IOException, GeneralSecurityException, InterruptedException;

	public UserPanNumberResponse updatePanDob(UserPanNumberRequest request);

	public NewLenderExcelSheetGenerateResponse excelSheetGenerateforDeals(
			BorrowerDealNotificationRequestfordeals borrowerDealNotificationRequestForDeals);

	public NewLenderExcelSheetGenerateResponse excelSheetGenerateforClosedindateDeals(
			BorrowerDealNotificationRequestfordeals borrowerDealNotificationRequestForDeals);

	public List<LenderMemberShipDetails> getLendersValidityStatus();

	public String updateLenderFeeInformation(UpdateLenderFeeData updateLenderFeeData);

	public ActiveLendersCountResponse activLendersParicipationAmountAndCount(PaginationRequestDto paginationRequestDto);
	
	public List<ListOfDealsInformationToLender> getCurentDateDeals(PaginationRequestDto paginationRequestDto);

	public String sendReminderForInterestMissing();

	public LenderFeeFilesResponse listOfLenderFeePaymentDetailsInWallet1(LenderFeeInWalletDto lenderFeeInWalletDto);

	public LenderInterestPaymentsDto filesAfterApprovalReadingInAdmin(InterestReadRequest request);

	public BorrowerFdDataDtoResponse borrowerFdDetailsData();

	public String generatingNewFileForFeeFromWallet(WalletRequestDto requestDto);


}
