package com.oxyloans.service.loan;

import java.util.Date;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oxyloans.entity.borrowers.deals.information.LendersPaticipationUpdationRepo;
import com.oxyloans.entity.borrowers.deals.information.OxyLendersAcceptedDealsRepo;
import com.oxyloans.entity.lender.outstanding.OxyLendersOutstandingamount;
import com.oxyloans.entity.lender.outstanding.OxyLendersOutstandingamountRepo;
import com.oxyloans.entity.lender.oxywallet.LenderOxyWalletNativeRepo;
import com.oxyloans.entity.lender.oxywallet.LenderTransactionHistory;
import com.oxyloans.entity.lender.oxywallet.LenderTransactionHistory.AmountSubType;
import com.oxyloans.entity.lender.oxywallet.LenderTransactionHistory.AmountType;
import com.oxyloans.entity.lender.oxywallet.LenderTransactionHistoryRepo;
import com.oxyloans.repo.user.UserRepo;

@Service
public class LenderWalletHistoryService implements LenderWalletHistoryServiceRepo {

	private final Logger logger = LogManager.getLogger(AdminLoanService.class);

	@Autowired
	private LenderTransactionHistoryRepo lenderHistoryRepo;

	@Autowired
	private OxyLendersAcceptedDealsRepo oxyLendersAcceptedDealsRepo;

	@Autowired
	private LendersPaticipationUpdationRepo lendersPaticipationUpdationRepo;

	@Autowired
	private LenderOxyWalletNativeRepo lenderOxyWalletNativeRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private OxyLendersOutstandingamountRepo oxyLendersOutstandingamountRepo;

	@Override
	@Transactional
	public String lenderTransactionHistory(int userId, double amount, String type, String subType, int dealId) {

		LenderTransactionHistory lenderTransactionHistoryRepo = lenderHistoryRepo.lastTransactionInfo(userId);

		if (lenderTransactionHistoryRepo != null) {
			LenderTransactionHistory lenderTransactionHistory = new LenderTransactionHistory();
			lenderTransactionHistory.setUserId(userId);
			lenderTransactionHistory.setAmount(amount);
			lenderTransactionHistory.setAmountType(LenderTransactionHistory.AmountType.valueOf(type));
			lenderTransactionHistory.setAmountSubType(LenderTransactionHistory.AmountSubType.valueOf(subType));
			lenderTransactionHistory.setPreviousBalance(lenderTransactionHistoryRepo.getCurrentAmount());
			if (type.equalsIgnoreCase("CREDIT")) {
				lenderTransactionHistory.setCurrentAmount(lenderTransactionHistoryRepo.getCurrentAmount() + amount);
			} else {
				lenderTransactionHistory.setCurrentAmount(lenderTransactionHistoryRepo.getCurrentAmount() - amount);
			}
			lenderTransactionHistory.setPaidDate(new Date());
			lenderTransactionHistory.setDealId(dealId);
			lenderTransactionHistory.setCreatedOn(new Date());
			lenderTransactionHistory.setIsCurrent(true);
			lenderHistoryRepo.save(lenderTransactionHistory); // new record

			lenderTransactionHistoryRepo.setIsCurrent(false); // old record history
			lenderHistoryRepo.save(lenderTransactionHistoryRepo);

		} else {
			double walletAmount = 0.0;
			Double walletAmountFromDb = userRepo.userWalletAmount(userId);
			if (walletAmountFromDb != null) {
				walletAmount = walletAmountFromDb;
			}

			LenderTransactionHistory lenderTransactionHistory = new LenderTransactionHistory();
			lenderTransactionHistory.setUserId(userId);
			lenderTransactionHistory.setAmount(0.0);
			lenderTransactionHistory
					.setAmountType(LenderTransactionHistory.AmountType.valueOf(AmountType.CREDIT.toString()));
			lenderTransactionHistory
					.setAmountSubType(LenderTransactionHistory.AmountSubType.valueOf(AmountSubType.MW.toString()));
			lenderTransactionHistory.setPreviousBalance(0.0);
			lenderTransactionHistory.setCurrentAmount(walletAmount);
			lenderTransactionHistory.setPaidDate(new Date());
			lenderTransactionHistory.setDealId(dealId);
			lenderTransactionHistory.setCreatedOn(new Date());

			lenderTransactionHistory.setIsCurrent(true);
			lenderHistoryRepo.save(lenderTransactionHistory);

		}

		return "null";

	}

	@Override
	@Transactional
	public String calculatingOutstandingAmount(int userId, double amount, String type, String subType, int dealId) {

		OxyLendersOutstandingamount oxyLendersOutstandingamount = oxyLendersOutstandingamountRepo
				.getUserIdWithLimit(userId);
		if (oxyLendersOutstandingamount != null) {
			logger.info("logger" + "step1" + userId);
			OxyLendersOutstandingamount oxyLendersOutstanding = new OxyLendersOutstandingamount();
			if (type.equalsIgnoreCase("CREDIT")) {
				oxyLendersOutstanding.setOutstandingAmount(oxyLendersOutstandingamount.getOutstandingAmount() + amount);
			} else {
				oxyLendersOutstanding.setOutstandingAmount(oxyLendersOutstandingamount.getOutstandingAmount() - amount);
			}
			oxyLendersOutstanding.setPreviousBalance(oxyLendersOutstandingamount.getOutstandingAmount());
			oxyLendersOutstanding.setAmount(amount);
			oxyLendersOutstanding.setUserId(userId);
			oxyLendersOutstanding.setIsCurrent(true);
			oxyLendersOutstanding.setAmountType(OxyLendersOutstandingamount.AmountType.valueOf(type));
			oxyLendersOutstanding.setAmountSubType(OxyLendersOutstandingamount.AmountSubType.valueOf(subType));
			oxyLendersOutstandingamountRepo.save(oxyLendersOutstanding);

			oxyLendersOutstandingamount.setIsCurrent(false);
			oxyLendersOutstandingamountRepo.save(oxyLendersOutstandingamount);

		} else {
			logger.info("logger" + "step2" + userId);
			Double oustanding = oxyLendersAcceptedDealsRepo.getOutstandingAmount(userId);
			OxyLendersOutstandingamount oxyLendersOutstanding = new OxyLendersOutstandingamount();
			if (oustanding != null) {
				oxyLendersOutstanding.setOutstandingAmount(oustanding);
				oxyLendersOutstanding.setUserId(userId);
				oxyLendersOutstanding.setIsCurrent(true);
				oxyLendersOutstandingamountRepo.save(oxyLendersOutstanding);
			}
		}

		return subType;

	}

}
