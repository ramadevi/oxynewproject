package com.oxyloans.service.loan;

import java.util.List;

import com.oxyloans.serviceloan.LoanResponseDto;

public class BucketPdfresponse {

	private List<LoanResponseDto> loanResponseDto;

	private String downloadUrl;

	public List<LoanResponseDto> getLoanResponseDto() {
		return loanResponseDto;
	}

	public void setLoanResponseDto(List<LoanResponseDto> loanResponseDto) {
		this.loanResponseDto = loanResponseDto;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

}
