package com.oxyloans.service.ondc;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.oxyloans.whatappservice.WhatappService;

@Service
public class SellerServiceImple implements SellerService {

	private final Logger logger = LogManager.getLogger(WhatappService.class);

	@Override
	@Transactional
	public SellerMessageResponseDto buyerSearchForSeller(SellerMessageRequestDto sellerMessageRequestDto) {
		logger.info("buyerSearchForSeller method start");
		SellerMessageResponseDto sellerMessageResponseDto = new SellerMessageResponseDto();
		ContextDto contextDto = sellerMessageRequestDto.getContext();
		sellerMessageResponseDto.setContext(contextDto);
		Message message = new Message();
		AckResponse ackResponse = new AckResponse();
		ackResponse.setStatus("ACK");
		message.setAck(ackResponse);
		sellerMessageResponseDto.setMessage(message);
		logger.info("buyerSearchForSeller method end");
		return sellerMessageResponseDto;

	}

}
