package com.oxyloans.service.ondc;

public interface SellerService {

	public SellerMessageResponseDto buyerSearchForSeller(SellerMessageRequestDto sellerMessageRequestDto);
}
