package com.oxyloans.repository.esignrepo;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.EsignTransactions;

@Repository
public interface EsignTransactionsRepo extends PagingAndSortingRepository<EsignTransactions, Integer> {
	
}
