package com.oxyloans.repository.cms;

import java.util.List;

import com.oxyloans.entity.cms.LenderCmsPaymentsDetails;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface LenderCmsPaymentsDetailsRepo extends PagingAndSortingRepository<LenderCmsPaymentsDetails, Integer>,
		JpaSpecificationExecutor<LenderCmsPaymentsDetails> {

	@Query(value = "select * from public.lender_cms_payments_details where lender_cms_payments_id=:id and transaction_status=:transactionStatus", nativeQuery = true)
	public List<LenderCmsPaymentsDetails> findByLenderCmsPaymentsIdBasedOnStatus(@Param("id") Integer id,
			@Param("transactionStatus") String transactionStatus);

	@Modifying(clearAutomatically = true)
	@Query(value = "update public.lender_cms_payments_details set transaction_status=:status where lender_cms_payments_id=:id ", nativeQuery = true)
	public int updateTransactionStatus(@Param("id") Integer id, @Param("status") String status);

	@Query(value = "select * from public.lender_cms_payments_details where lender_cms_payments_id=:id and lender_id=:userId ", nativeQuery = true)
	public LenderCmsPaymentsDetails findBylenderId(@Param("id") Integer id, @Param("userId") int userId);

	@Query(value = "select sum(amount) from public.lender_cms_payments_details where lender_cms_payments_id=:id", nativeQuery = true)
	public Double getTotalInteretAmountById(@Param("id") Integer id);

	@Query(value = "select * from public.lender_cms_payments_details where lender_cms_payments_id=:id and lender_id=:userId and amount=:amount ", nativeQuery = true)
	public LenderCmsPaymentsDetails findBylenderIdAndAmount(@Param("id") Integer id, @Param("userId") int userId,
			@Param("amount") Double amount);

	@Query(value = "select sum(amount) from public.lender_cms_payments_details where lender_cms_payments_id=:id and transaction_status=:status", nativeQuery = true)
	public Double getTotalInteretAmountByIdAndTransactionStatus(@Param("id") Integer id,
			@Param("status") String status);

	@Query(value = "select sum(amount) from public.lender_cms_payments_details where lender_cms_payments_id=:id and transaction_status in ('APPROVED','SUCCESS')", nativeQuery = true)
	public Double getTotalInteretAmountByIdAndTransactionStatus(@Param("id") Integer id);

	@Query(value = "select * from public.lender_cms_payments_details where lender_cms_payments_id=:id and lender_id=:userId and cast(amount as INTEGER)=:amount", nativeQuery = true)
	public LenderCmsPaymentsDetails findByLenderCmsPaymentsDetails(@Param("id") Integer id,
			@Param("userId") Integer userId, @Param("amount") Integer amount);

	@Query(value = "select * from public.lender_cms_payments_details where lender_cms_payments_id=:id", nativeQuery = true)
	public List<LenderCmsPaymentsDetails> findByLenderCmsPaymentsId(@Param("id") Integer id);

	public LenderCmsPaymentsDetails findByLenderCmsPaymentsIdAndLenderId(Integer id, Integer userId);

	@Query(value = "select * from public.lender_cms_payments_details where transaction_status='APPROVED' and lender_cms_payments_id=:id order by id asc limit 1", nativeQuery = true)
	public LenderCmsPaymentsDetails findLenderInterestFilesInApprovedStatus(@Param("id") Integer id);

}
