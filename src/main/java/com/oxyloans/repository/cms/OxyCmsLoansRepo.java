package com.oxyloans.repository.cms;

import java.util.List;

import com.oxyloans.entity.cms.OxyCmsLoans;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OxyCmsLoansRepo
		extends PagingAndSortingRepository<OxyCmsLoans, Integer>, JpaSpecificationExecutor<OxyCmsLoans> {

	@Query(value = "select * from public.oxy_cms_loans where deal_id=:dealId and user_id=:borrowerId and transaction_status='INITIATED'", nativeQuery = true)
	public OxyCmsLoans findByUserIdAndDealId(@Param("dealId") Integer dealId, @Param("borrowerId") Integer borrowerId);

	@Query(value = "select COALESCE(SUM(amount),0) from public.oxy_cms_loans where deal_id=:dealId and user_id=:borrowerId and transaction_status='EXECUTED'", nativeQuery = true)
	public Double sumOfAmountTransfered(@Param("borrowerId") Integer borrowerId, @Param("dealId") Integer dealId);

	@Query(value = "select * from public.oxy_cms_loans where file_name=:fileName", nativeQuery = true)
	public OxyCmsLoans findbyFileName(@Param("fileName") String fileName);

	@Query(value = "SELECT * FROM public.oxy_cms_loans where user_id=:borrowerId and borrower_came_from='PARTNER' and transaction_status='INITIATED'", nativeQuery = true)
	public OxyCmsLoans amountInInitiatedState(@Param("borrowerId") Integer borrowerId);

	@Query(value = "SELECT * FROM public.oxy_cms_loans where user_id=:borrowerId and borrower_came_from='PARTNER' and transaction_status='EXECUTED'", nativeQuery = true)
	public OxyCmsLoans amountInExecutedState(@Param("borrowerId") Integer borrowerId);

	@Query(value = "select user_id from public.oxy_cms_loans where borrower_came_from='PARTNER' and transaction_status='EXECUTED'", nativeQuery = true)
	public List<Integer> findingListOfUsersBasedOnUtm();

	@Query(value = "select * from public.oxy_cms_loans where file_name_borrowerfee=:fileName", nativeQuery = true)
	public OxyCmsLoans findbyFileNameForFee(@Param("fileName") String fileName);

	
}
