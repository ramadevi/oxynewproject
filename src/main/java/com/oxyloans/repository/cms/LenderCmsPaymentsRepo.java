package com.oxyloans.repository.cms;

import java.util.Date;
import java.util.List;

import com.oxyloans.entity.cms.LenderCmsPayments;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface LenderCmsPaymentsRepo
		extends PagingAndSortingRepository<LenderCmsPayments, Integer>, JpaSpecificationExecutor<LenderCmsPayments> {

	@Query(value = "select * from public.lender_cms_payments where deal_id=:dealId and to_char(payment_date,'dd-MM-yyyy')=:paymentDate and lender_returns_type='LENDERINTEREST'", nativeQuery = true)
	public LenderCmsPayments findbyDealIdAndPaymentDate(@Param("dealId") Integer dealId,
			@Param("paymentDate") String paymentDate);

	@Query(value = "select * from public.lender_cms_payments where file_name=:fileName", nativeQuery = true)
	public LenderCmsPayments findbyFileName(@Param("fileName") String fileName);

	@Query(value = "select * from public.lender_cms_payments where deal_id=:dealId and to_char(payment_date,'MM-YYYY')=:paymentDate and file_execution_status in ('INITIATED','EXECUTED') and file_name is not null", nativeQuery = true)
	public LenderCmsPayments findByInterestPaymentDate(@Param("dealId") Integer dealId,
			@Param("paymentDate") String paymentDate);

	@Query(value = "select * from public.lender_cms_payments where deal_id=:dealId and to_char(payment_date,'dd-MM-yyyy')=:paidDateInRequiredFormat and lender_returns_type='LENDERINTEREST' and file_execution_status='EXECUTED'", nativeQuery = true)
	public LenderCmsPayments findbyDealIdAndPaymentDate1(@Param("dealId") Integer dealId,
			@Param("paidDateInRequiredFormat") String paidDateInRequiredFormat);

	@Query(value = "select * from public.lender_cms_payments where to_char(payment_date,'dd-MM-yyyy')=:date and file_execution_status in ('EXECUTED') and file_name is not null and lender_returns_type=:amountType", nativeQuery = true)
	public List<LenderCmsPayments> findAllInGivenDate(@Param("date") String date,
			@Param("amountType") String amountType);

	@Query(value = "select * from public.lender_cms_payments where deal_id=:dealId and to_char(payment_date,'MM-YYYY')=:paymentDate and file_execution_status in ('INITIATED','EXECUTED') and file_name is not null and lender_returns_type='LENDERINTEREST'", nativeQuery = true)
	public LenderCmsPayments findByInterestPaymentDateForInterests(@Param("dealId") Integer dealId,
			@Param("paymentDate") String paymentDate);

	@Query(value = "select * from public.lender_cms_payments where deal_id=:dealId and to_char(payment_date,'MM-YYYY')=:paymentDate and file_execution_status in ('EXECUTED') and file_name is not null and lender_returns_type='LENDERINTEREST' limit 1", nativeQuery = true)
	public LenderCmsPayments findByInterestPaymentDateAnsExecutedFiles(@Param("dealId") Integer dealId,
			@Param("paymentDate") String paymentDate);

	@Query(value = "SELECT * \r\n" + "FROM lender_cms_payments \r\n"
			+ "WHERE payment_date >= CURRENT_DATE - interval '10' day\r\n"
			+ "and file_name is not null and file_execution_status='EXECUTED' order by id desc", nativeQuery = true)
	public List<LenderCmsPayments> findAllRejected();

	@Query(value = "select * from public.lender_cms_payments where  file_execution_status in ('ONHOLD') and file_name is not null ", nativeQuery = true)
	public List<LenderCmsPayments> findAllOnHoldFiles();

	@Query(value = "SELECT * FROM public.lender_cms_payments where (file_execution_status='EXECUTED' OR file_execution_status='ONHOLD') and (lender_returns_type='LENDERINTEREST' OR lender_returns_type='PRINCIPALINTEREST' OR lender_returns_type='WITHDRAWALINTEREST') and deal_id=:dealId order by payment_date desc", nativeQuery = true)
	public List<LenderCmsPayments> getInterestDetailSummary(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lender_cms_payments where (file_execution_status='EXECUTED' OR file_execution_status='ONHOLD') and (lender_returns_type='LENDERWITHDRAW' OR lender_returns_type='PRINCIPALTOWALLET' OR lender_returns_type='LENDERPRINCIPAL') and deal_id=:dealId order by payment_date desc", nativeQuery = true)
	public List<LenderCmsPayments> getPrincipalDetailSummary(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lender_cms_payments where file_execution_status='EXECUTED' and lender_returns_type='LENDERINTEREST' and deal_id=:dealId limit 1", nativeQuery = true)
	public LenderCmsPayments previousInterestGiven(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lender_cms_payments where TO_CHAR(payment_date, 'DD/MM/YYYY')=:principalDate and deal_id=:dealId and lender_returns_type='LENDERINTEREST'", nativeQuery = true)
	public LenderCmsPayments previousInterestGiven(@Param("dealId") Integer dealId,
			@Param("principalDate") String principalDate);

	@Query(value = "SELECT * FROM public.lender_cms_payments where user_id>0 and payment_date>=:startDate and payment_date<=:endDate and lender_returns_type='LOAN' and file_execution_status='EXECUTED' order by payment_date desc", nativeQuery = true)
	public List<LenderCmsPayments> loanExecutedFromSystem(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT * FROM public.lender_cms_payments where lender_returns_type='LOAN' and file_execution_status='EXECUTED' and user_id>0 ORDER BY payment_date desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<LenderCmsPayments> loanExecutedFromSystemWithPagination(@Param("pageNo") Integer pageNo,
			@Param("pageSize") Integer pageSize);

	@Query(value = "SELECT count(id) FROM public.lender_cms_payments where lender_returns_type='LOAN' and file_execution_status='EXECUTED' and user_id>0", nativeQuery = true)
	public Integer loanExecutedFromSystemCount();

	@Query(value = "select * from public.lender_cms_payments where file_execution_status='INITIATED' and file_name is not null order by payment_date desc", nativeQuery = true)
	public List<LenderCmsPayments> findListOfInitiatedFiles();

	@Query(value = "SELECT count(id) FROM public.lender_cms_payments where file_execution_status='INITIATED' and file_name is not null", nativeQuery = true)
	public Integer countFiles();

	@Query(value = "SELECT count(id) FROM public.lender_cms_payments where lender_returns_type='LOAN' and payment_date=CURRENT_DATE and file_execution_status='EXECUTED';", nativeQuery = true)

	public Integer findByLoanCount();

	@Query(value = "SELECT sum(total_amount) FROM public.lender_cms_payments where lender_returns_type='LOAN' and payment_date=CURRENT_DATE and file_execution_status='EXECUTED';", nativeQuery = true)

	public Double findByBorrowerAmount();

	@Query(value = "SELECT * FROM public.lender_cms_payments WHERE file_execution_status='EXECUTED' AND lender_returns_type='LENDERINTEREST' AND payment_date = CURRENT_DATE - INTERVAL '1' DAY AND file_name IS NOT NULL ORDER BY payment_date DESC", nativeQuery = true)
	public List<LenderCmsPayments> findLenderInterestExecutedFiles();

	@Query(value = "SELECT count(id) FROM public.lender_cms_payments where lender_returns_type='LOAN' and date(payment_date)>=:startDate and date(payment_date)<=:endDate and file_execution_status='EXECUTED';", nativeQuery = true)

	public Integer findByLoanCountAdminModule(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT sum(total_amount) FROM public.lender_cms_payments where lender_returns_type='LOAN' and date(payment_date)>=:startDate and date(payment_date)<=:endDate and file_execution_status='EXECUTED';", nativeQuery = true)

	public Double findByBorrowerAmountAdminModule(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT * FROM public.lender_cms_payments WHERE  date(payment_date)>= :startDate AND date(payment_date)<= :endDate AND file_execution_status =:fileStatus ORDER BY payment_date DESC", nativeQuery = true)
	public List<LenderCmsPayments> getIntiatedfile(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
			@Param("fileStatus") String fileStatus);

	@Query(value = "SELECT * FROM public.lender_cms_payments WHERE  date(payment_date)>= :startDate AND date(payment_date)<= :endDate AND (file_execution_status = 'INITIATED' OR file_execution_status = 'EXECUTED') ORDER BY file_execution_status = 'INITIATED' DESC", nativeQuery = true)
	public List<LenderCmsPayments> getBothExecutedFiles(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT * FROM public.lender_cms_payments where lender_returns_type='LOAN' AND file_execution_status='EXECUTED' AND user_id!=0", nativeQuery = true)

	public List<LenderCmsPayments> findByFdAmountBorrower();

	@Query(value = "SELECT * FROM public.lender_cms_payments WHERE deal_id=:dealId and date(payment_date)= :readingDate ORDER BY file_execution_status = 'INITIATED' DESC", nativeQuery = true)
	public LenderCmsPayments findFileNameByDate(@Param("dealId") Integer dealId,
			@Param("readingDate") Date readingDate);
}
