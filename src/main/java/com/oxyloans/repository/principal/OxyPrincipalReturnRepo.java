package com.oxyloans.repository.principal;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.lender.oxywallet.LenderOxyWallet;

public interface OxyPrincipalReturnRepo
		extends PagingAndSortingRepository<OxyPrincipalReturn, Integer>, JpaSpecificationExecutor<OxyPrincipalReturn> {

	@Query(value = "SELECT * FROM public.oxy_principal_return where deal_id=:dealId and status=:status and amount_type='PRINCIPALTOBANK' order by id desc", nativeQuery = true)
	List<OxyPrincipalReturn> listOfPrincipalReturningBasedOnDealId(@Param("dealId") Integer dealId,
			@Param("status") String status);

	@Query(value = "SELECT DISTINCT date(principal_approved_on) FROM public.oxy_principal_return where deal_id=:dealId and status=:status and amount_type='PRINCIPALTOBANK'", nativeQuery = true)
	String getDateBasedOnDealId(@Param("dealId") Integer dealId, @Param("status") String status);

	@Query(value = "SELECT * FROM public.oxy_principal_return where deal_id=:dealId and status=:status and amount_type='PRINCIPALTOBANK'", nativeQuery = true)
	List<OxyPrincipalReturn> principalMovingToH2H(@Param("dealId") Integer dealId, @Param("status") String status);

	@Query(value = "SELECT * FROM public.oxy_principal_return where user_id=:userId and deal_id=:dealId and principal_amount=:principalAmount and status='INITIATED' and amount_type='PRINCIPALTOBANK'", nativeQuery = true)
	OxyPrincipalReturn checkPricipalAleadyGivenStatus(@Param("userId") Integer userId, @Param("dealId") Integer dealId,
			@Param("principalAmount") double principalAmount);

	@Query(value = "select * from public.oxy_principal_return where user_id=:userId and deal_id=:dealId and amount_type='PRINCIPALTOBANK' order by id desc limit 1", nativeQuery = true)
	OxyPrincipalReturn principalAmountReturnedToAccount(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.oxy_principal_return where deal_id=:dealId and user_id=:userId and principal_amount=:amount and amount_type in ('PRINCIPALTOBANK','PRINCIPALTOWALLET') ", nativeQuery = true)
	OxyPrincipalReturn findByUserIdDealIdAndAmount(@Param("dealId") Integer dealId, @Param("userId") Integer userId,
			@Param("amount") Integer amount);

	@Query(value = "SELECT * FROM public.oxy_principal_return where deal_id=:dealId and status='INITIATED' and amount_type='PRINCIPALTOWALLET'", nativeQuery = true)
	List<OxyPrincipalReturn> interestMovingToH2HToPrincipalToWallet(@Param("dealId") Integer dealId);

	@Query(value = "select sum(principal_amount) from public.oxy_principal_return where deal_id=:dealId and user_id=:userId", nativeQuery = true)
	Double principalAmountReturn(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT *  FROM oxy_principal_return \r\n"
			+ "WHERE principal_approved_on >= CURRENT_DATE - INTERVAL '10 days'\r\n"
			+ "order by id desc", nativeQuery = true)
	List<OxyPrincipalReturn> findAllInLastTenDays();

	@Query(value = "SELECT * FROM public.oxy_principal_return where deal_id=:dealId and user_id=:userId and principal_amount=:amount and to_char(principal_approved_on,'dd-MM-yyyy')=:dateString ", nativeQuery = true)
	OxyPrincipalReturn findbyUserIdDealIdAndAmount(@Param("userId") int userId, @Param("dealId") Integer dealId,
			@Param("amount") int amount, @Param("dateString") String dateString);

	@Query(value = "SELECT * FROM public.oxy_principal_return where deal_id=:dealId and amount_type='PRINCIPALTOWALLET'", nativeQuery = true)
	List<OxyPrincipalReturn> findAllPrincipalToWalletUsers(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.oxy_principal_return where deal_id=:dealId and amount_type='PRINCIPALTOBANK'", nativeQuery = true)
	List<OxyPrincipalReturn> findAllPrincipalToBankUsers(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.oxy_principal_return where user_id=:userId and deal_id=:dealId", nativeQuery = true)
	OxyPrincipalReturn principalReturnedInitiated(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.oxy_principal_return where deal_id=:dealId and date(principal_approved_on)=:principalDate and amount_type='PRINCIPALTOBANK'", nativeQuery = true)
	List<OxyPrincipalReturn> listOfPrincipalInInitiated(@Param("dealId") Integer dealId,
			@Param("principalDate") Date principalDate);

	@Query(value = "SELECT * FROM public.oxy_principal_return where deal_id=:dealId and date(interest_approved_on)=:interestDate and amount_type='PRINCIPALTOBANK'", nativeQuery = true)
	List<OxyPrincipalReturn> listOfInterestInInitiated(@Param("dealId") Integer dealId,
			@Param("interestDate") Date interestDate);

	@Query(value = "SELECT * FROM public.oxy_principal_return where user_id=:userId and deal_id=:dealId and date(interest_approved_on)=:interestDate and amount_type='PRINCIPALTOBANK'", nativeQuery = true)
	OxyPrincipalReturn interestReturned(@Param("userId") Integer userId, @Param("dealId") Integer dealId,
			@Param("interestDate") Date interestDate);

	@Query(value = "SELECT * FROM public.oxy_principal_return where user_id=:userId and deal_id=:dealId and date(principal_approved_on)=:principalDate and amount_type='PRINCIPALTOBANK'", nativeQuery = true)
	OxyPrincipalReturn principalReturned(@Param("userId") Integer userId, @Param("dealId") Integer dealId,
			@Param("principalDate") Date principalDate);

	@Query(value = "select * from public.oxy_principal_return where (to_char(principal_approved_on,'dd-MM-yyyy')=:date \r\n"
			+ "or to_char(principal_modified_on,'dd-MM-yyyy')=:date) and amount_type=:amountType", nativeQuery = true)
	List<OxyPrincipalReturn> getTransactionsOnGivenDate(@Param("date") String date,
			@Param("amountType") String amountType);

	@Query(value = "SELECT * FROM public.oxy_principal_return where user_id=:userId and deal_id=:dealId and status!='INITIATED'", nativeQuery = true)
	Double sumOfPrincipalAmountReturned(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT distinct a.deal_id,date(a.principal_approved_on),b.deal_name FROM public.oxy_principal_return a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.principal_status='AFTER' and amount_type='PRINCIPALTOBANK'", nativeQuery = true)
	List<Object[]> movedToAfterApprovalReadReportsPendingForPrincipal();

	@Query(value = "select sum(principal_amount) from public.oxy_principal_return where deal_id=:dealId and to_char(principal_approved_on, 'yyyy-MM-dd')=:principalDate and amount_type='PRINCIPALTOBANK'", nativeQuery = true)
	Double sumPrincipalReturnedToDeal(@Param("dealId") Integer dealId, @Param("principalDate") String principalDate);

	@Query(value = "SELECT distinct a.deal_id,date(a.interest_approved_on),b.deal_name FROM public.oxy_principal_return a join public.oxy_borrowers_deals_information b on a.deal_id=b.id  where a.interest_status='AFTER' and amount_type='PRINCIPALTOBANK'", nativeQuery = true)
	List<Object[]> movedToAfterApprovalReadReportsPendingForInterest();

	@Query(value = "select sum(interest_amount) from public.oxy_principal_return where deal_id=:dealId and to_char(interest_approved_on, 'yyyy-MM-dd')=:interestDate and amount_type='PRINCIPALTOBANK'", nativeQuery = true)
	Double sumInterestReturnedToDeal(@Param("dealId") Integer dealId, @Param("interestDate") String interestDate);

	@Query(value = "SELECT sum(principal_amount) FROM public.oxy_principal_return where deal_id=:dealId and status!='INITIATED'", nativeQuery = true)
	Double sumOfOxyPrincipalAmount(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(principal_amount) FROM public.oxy_principal_return where (status='PRINCIPALRETURNED' OR status='INTERESTRETURNED') and date(principal_approved_on)=current_date", nativeQuery = true)
	Double sumOfOxyPrincipalAmount();

	@Query(value = "SELECT sum(interest_amount) FROM public.oxy_principal_return where  status='INTERESTRETURNED' and date(interest_approved_on)=current_date", nativeQuery = true)
	Double findByInterestAmount();

	@Query(value = "SELECT sum(interest_amount) FROM public.oxy_principal_return where deal_id=:dealId and   status='INTERESTRETURNED' and date(interest_approved_on)=current_date", nativeQuery = true)
	Double findByInterestAmount(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(principal_amount) FROM public.oxy_principal_return where deal_id=:dealId and  (status='PRINCIPALRETURNED' OR status='INTERESTRETURNED') and date(principal_approved_on)=current_date", nativeQuery = true)
	Double findByOxyPrincipalAmount(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(principal_amount) FROM public.oxy_principal_return where  (status='PRINCIPALRETURNED' OR status='INTERESTRETURNED') and principal_approved_on >= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)

	Double sumOfOxyPrincipalAmountWeeklyData();

	@Query(value = "SELECT sum(interest_amount) FROM public.oxy_principal_return where  status='INTERESTRETURNED' and interest_approved_on >= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)

	Double findByInterestAmountWeeklyData();

	@Query(value = "SELECT sum(interest_amount) FROM public.oxy_principal_return where deal_id=:dealId and  status='INTERESTRETURNED' and interest_approved_on >= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)
	Double findByInterestAmountWeeklyData(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(principal_amount) FROM public.oxy_principal_return where deal_id=:dealId and  (status='PRINCIPALRETURNED' OR status='INTERESTRETURNED') and principal_approved_on >= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)

	Double findByOxyPrincipalAmountWeeklydata(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(principal_amount) FROM public.oxy_principal_return where (status='PRINCIPALRETURNED' OR status='INTERESTRETURNED') and  date(principal_approved_on)>=:startDate and date(principal_approved_on)<=:endDate", nativeQuery = true)

	Double sumOfOxyPrincipalAmountMonthlyDataWithInput(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT sum(interest_amount) FROM public.oxy_principal_return where status='INTERESTRETURNED' and  date(interest_approved_on)>=:startDate and date(interest_approved_on)<=:endDate", nativeQuery = true)

	Double findByInterestAmountMonthlyDataWithInput(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT sum(interest_amount) FROM public.oxy_principal_return where deal_id=:dealId and status='INTERESTRETURNED' and  date(interest_approved_on)>=:startDate and date(interest_approved_on)<=:endDate", nativeQuery = true)

	Double findByInterestAmountMonthlyDealWiseDataWithInput(@Param("dealId") Integer dealId,
			@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT sum(principal_amount) FROM public.oxy_principal_return where deal_id=:dealId and (status='PRINCIPALRETURNED' OR status='INTERESTRETURNED') and  date(principal_approved_on)>=:startDate and date(principal_approved_on)<=:endDate", nativeQuery = true)

	Double findByOxyPrincipalAmountMonthlyDataWithInput(@Param("dealId") Integer dealId,
			@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT sum(principal_amount) FROM public.oxy_principal_return where (status='PRINCIPALRETURNED' OR status='INTERESTRETURNED') and date_trunc('month', principal_approved_on) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)

	Double sumOfOxyPrincipalAmountMonthlyData();

	@Query(value = "SELECT sum(interest_amount) FROM public.oxy_principal_return where  status='INTERESTRETURNED' and date_trunc('month', interest_approved_on) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)

	Double findByInterestAmountMonthlyData();

	@Query(value = "SELECT sum(interest_amount) FROM public.oxy_principal_return where deal_id=:dealId and  status='INTERESTRETURNED' and date_trunc('month', interest_approved_on) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)

	Double findByInterestAmountMonthlyDataBasedOnDealId(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(principal_amount) FROM public.oxy_principal_return where deal_id=:dealId and  (status='PRINCIPALRETURNED' OR status='INTERESTRETURNED') and date_trunc('month', principal_approved_on) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)

	Double findByOxyPrincipalAmountMonthlyDataBasedOnDealId(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * \r\n" + "FROM public.oxy_principal_return \r\n" + "WHERE status = 'PRINCIPALRETURNED' \r\n"
			+ "  AND amount_type = 'PRINCIPALTOBANK' \r\n" + "  AND principal_status = 'AFTER' \r\n"
			+ "  AND date(principal_approved_on) = date(CURRENT_DATE - INTERVAL '1 day');\r\n" + "", nativeQuery = true)
	List<OxyPrincipalReturn> findAfterFilesNotReading();

	@Query(value = "SELECT * \r\n" + "FROM public.oxy_principal_return \r\n" + "WHERE status = 'INTERESTRETURNED' \r\n"
			+ "  AND amount_type = 'PRINCIPALTOBANK' \r\n" + "  AND interest_status = 'AFTER' \r\n"
			+ "  AND date(interest_approved_on) = date(CURRENT_DATE - INTERVAL '1 day');\r\n" + "", nativeQuery = true)
	List<OxyPrincipalReturn> findAfterIntrestFilesNotReading();

}
