package com.oxyloans.repository.lenderreferencedetailesrepo;

import java.util.Date;
import java.util.List;

import com.oxyloans.entity.LenderReferenceDetails.ReferralBonusReading;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ReferralBonusReadingRepo extends PagingAndSortingRepository<ReferralBonusReading, Integer>{

	@Query(value = "select * from public.referral_bonus_reading where status='GENERATED'" ,nativeQuery = true)
	public List<ReferralBonusReading> findByStatus();
	
	@Query(value = "SELECT a.* FROM public.referral_bonus_reading a "
            + "JOIN lender_cms_payments b ON date(a.sheet_generated_on) = date(b.payment_date) "
            + "WHERE a.status = 'GENERATED'",
            nativeQuery = true)
    public List<ReferralBonusReading> findByStatusAndPaymentDate();

	@Query(value = "select * from public.referral_bonus_reading where user_id=:referrerId AND status='INITIATED'", nativeQuery = true)
	public ReferralBonusReading findByUserId(@Param("referrerId") Integer referrerId);

	@Query(value = "SELECT * FROM public.referral_bonus_reading WHERE user_id = :referrerId AND status='GENERATED' AND sheet_generated_on IS NOT NULL", nativeQuery = true)
	public ReferralBonusReading findByData2(@Param("referrerId") Integer referrerId);

	
	
	@Query(value = "SELECT * " +
	        "FROM public.referral_bonus_reading " +
            "WHERE EXTRACT(MONTH FROM sheet_generated_on) = :month " +
            "  AND EXTRACT(YEAR FROM sheet_generated_on) = :year " +
            "  AND status = 'EXCUTED' " +
            "LIMIT :pageSize OFFSET :pageNo", nativeQuery = true)
    public List<ReferralBonusReading> findingReferrersDetails3(@Param("month") int month,
                                                       @Param("year") int year,
                                                       @Param("pageNo") int pageNo,
                                                       @Param("pageSize") int pageSize);

	@Query(value = "SELECT * " +
            "FROM public.referral_bonus_reading " +
            "WHERE EXTRACT(MONTH FROM sheet_generated_on) = :month " +
            "  AND EXTRACT(YEAR FROM sheet_generated_on) = :year " +
            "  AND status = 'EXCUTED' " +
            "  AND user_id = :referrerId " +
            "LIMIT :pageSize OFFSET :pageNo", nativeQuery = true)
public List<ReferralBonusReading> findingReferrersDetails4(@Param("referrerId") int referrerId,
                                                  @Param("pageNo") int pageNo,
                                                  @Param("pageSize") int pageSize,
                                                  @Param("month") int month,
                                                  @Param("year") int year);

	@Query(value = "select count(*) from public.Referral_Bonus_Reading WHERE status ='EXCUTED' AND excuted_date IS NOT NULL", nativeQuery = true)
	public Integer totalCountOfReferees();
	
	@Query(value = "select * from referral_bonus_reading WHERE status='GENERATED' AND DATE(sheet_generated_on) = :createdOn", nativeQuery = true)
	public List<ReferralBonusReading> findlistOfcreatedOn(@Param("createdOn") Date createdOn);
	
	@Query(value = "select * from public.referral_bonus_reading where status = 'GENERATED' AND sheet_generated_on IS NOT NULL AND lender_cms_payments_id=:id", nativeQuery = true)
	public List<ReferralBonusReading> findByCmsPaymentsId(@Param("id") Integer id);

	@Query(value = "SELECT * \r\n"
			+ "FROM referral_bonus_reading\r\n"
			+ "WHERE status = 'EXCUTED'\r\n"
			+ "AND excuted_date IS NOT NULL\r\n"
			+ "AND EXTRACT(YEAR FROM excuted_date) = EXTRACT(YEAR FROM CURRENT_DATE)\r\n"
			+ "AND EXTRACT(MONTH FROM excuted_date) = EXTRACT(MONTH FROM CURRENT_DATE)\r\n"
			+ "ORDER BY total_amount DESC\r\n"
			+ "LIMIT 3;\r\n"
			+ "",nativeQuery = true)
	public List<ReferralBonusReading> findByStatusBasedResult();
}
