package com.oxyloans.repository.lenderreferencedetailesrepo;

import com.oxyloans.entity.LenderReferenceDetails.ReferralBonusDetails;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ReferralBonusDetailsRepo extends PagingAndSortingRepository<ReferralBonusDetails, Integer> {

}
