package com.oxyloans.repository.lenderpayu;

import java.util.Date;
import java.util.List;

import com.oxyloans.entity.lenders.payu.LenderPayuDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LenderPayuDetailsRepo extends PagingAndSortingRepository<LenderPayuDetails, Integer> {

	@Query(value = "select * from public.lender_payu_details where transaction_number=:txnNumber order by id desc limit 1", nativeQuery = true)
	public LenderPayuDetails findByTransactionNumber(@Param("txnNumber") String txnNumber);

	@Query(value = "select * from public.lender_payu_details where lender_user_id=:userId and status='COMPLETED' and deal_id=:dealId", nativeQuery = true)
	public LenderPayuDetails findByLenderUserIdAndDealId(@Param("userId") int userId, @Param("dealId") int dealId);

	@Query(value = "select sum(amount) from public.lender_payu_details where status='COMPLETED'", nativeQuery = true)
	public Double getTotalFeeReceivedThroughPayu();

	@Query(value = "select sum(amount) from public.lender_payu_details where status='COMPLETED' and to_char(payment_date, 'mm-yyyy')=:monthAndYear", nativeQuery = true)
	public Double getFeeReceivedInGivenMonth(@Param("monthAndYear") String monthAndYear);

	@Query(value = "select * from public.lender_payu_details where status='COMPLETED' and to_char(payment_date, 'mm-yyyy')=:monthAndYear", nativeQuery = true)
	public List<LenderPayuDetails> getListOfAmountsReceivedWithGivenDate(@Param("monthAndYear") String monthAndYear);

	@Query(value = "select * from public.lender_payu_details where lender_user_id=:userId and deal_id=:dealId and status='COMPLETED'", nativeQuery = true)
	public LenderPayuDetails findByLenderUserIdAndDealIdAndStatus(@Param("userId") int userId,
			@Param("dealId") int dealId);

	@Query(value = "SELECT * FROM public.lender_payu_details where status='COMPLETED' and \r\n"
			+ "lender_user_id=:userId order by id asc limit 1", nativeQuery = true)
	public LenderPayuDetails findingValidityDetails(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM PUBLIC.lender_payu_details where (status is null or status='PENDING')\r\n"
			+ "and lender_user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public LenderPayuDetails findByLenderUserIdAndDealIdandFeestatus(@Param("userId") int userId,
			@Param("dealId") int dealId);

	@Query(value = "select count(*) from public.lender_payu_details where lender_user_id=:referrerId and \r\n"
			+ "to_char(payment_date,'yyyy-MM-dd')>=:validity\r\n"
			+ "and to_char(payment_date,'yyyy-MM-dd')<=:currentDate and status='COMPLETED'", nativeQuery = true)
	public Integer findingCountOfPaymentsDoneForPerDealFromValidityDateToCurrentDate(
			@Param("referrerId") Integer referrerId, @Param("validity") String validity,
			@Param("currentDate") String currentDate);

	@Query(value = "select lender_user_id,transaction_number,amount,payment_date,deal_id from public.lender_payu_details \r\n"
			+ "where status='COMPLETED' and substring(transaction_number,1,6)!='OXYLRV' and deal_id!=1 order by id desc", nativeQuery = true)
	public List<Object[]> findListOfLenderFeeThroughPayu();

	@Query(value = "select lender_user_id,transaction_number,amount,payment_date,deal_id from public.lender_payu_details \r\n"
			+ "where status='COMPLETED'  and deal_id=1 and transaction_number is null order by id desc", nativeQuery = true)
	public List<Object[]> findListOfLenderFeeThroughHDFC();

	@Query(value = "select lender_user_id,transaction_number,amount,payment_date,deal_id from\r\n"
			+ "public.lender_payu_details where status='COMPLETED' and deal_id!=1 and lender_user_id=:userId order by id desc", nativeQuery = true)
	public List<Object[]> lenderPaidFeeThroughPayu(@Param("userId") Integer userId);

	@Query(value = "select lender_user_id,transaction_number,amount,payment_date,deal_id public.lender_payu_details \r\n"
			+ "where status='COMPLETED'  and deal_id=1 and transaction_number is null and lender_user_id=:userId order by id desc", nativeQuery = true)
	public List<Object[]> lenderPaidFeeThroughHDFC(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_payu_details where order_id=:order_id ", nativeQuery = true)
	public LenderPayuDetails findByOrderId(@Param("order_id") String order_id);

	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
			+ "    FROM public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
			+ "      AND a.lender_fee_payments = 'MONTHLY' \r\n" + "      AND b.test_user = false\r\n"
			+ " ) subquery;", nativeQuery = true)

	public Integer findBylenderMonthlyCount();

//	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
//			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
//			+ "    FROM public.lender_payu_details a\r\n"
//			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
//			+ "      AND a.lender_fee_payments = 'QUARTERLY' \r\n"
//			+ "      AND date(a.payment_date) BETWEEN '2023-04-01' AND '2024-03-30'\r\n"
//			+ "      AND b.test_user = false\r\n" + ") subquery;\r\n", nativeQuery = true)
	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
			+ "    FROM public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
			+ "      AND a.lender_fee_payments = 'QUARTERLY' \r\n" + "      AND b.test_user = false\r\n"
			+ " ) subquery;", nativeQuery = true)

	public Integer findByLenderQuarterly();

//	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
//			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
//			+ "    FROM public.lender_payu_details a\r\n"
//			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
//			+ "      AND a.lender_fee_payments = 'HALFYEARLY' \r\n"
//			+ "      AND date(a.payment_date) BETWEEN '2023-04-01' AND '2024-03-30'\r\n"
//			+ "      AND b.test_user = false\r\n" + ") subquery;\r\n", nativeQuery = true)

	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
			+ "    FROM public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
			+ "      AND a.lender_fee_payments = 'HALFYEARLY' \r\n" + "      AND b.test_user = false\r\n"
			+ " ) subquery;", nativeQuery = true)
	public Integer findByLenderHalfyearly();

//	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
//			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
//			+ "    FROM public.lender_payu_details a\r\n"
//			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
//			+ "      AND a.lender_fee_payments = 'PERYEAR' \r\n"
//			+ "      AND date(a.payment_date) BETWEEN '2023-04-01' AND '2024-03-30'\r\n"
//			+ "      AND b.test_user = false\r\n" + ") subquery;\r\n", nativeQuery = true)

	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
			+ "    FROM public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
			+ "      AND a.lender_fee_payments = 'PERYEAR' \r\n" + "      AND b.test_user = false\r\n"
			+ " ) subquery;", nativeQuery = true)
	public Integer findByLenderYearly();

//	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
//			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
//			+ "    FROM public.lender_payu_details a\r\n"
//			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
//			+ "      AND a.lender_fee_payments = 'TENYEARS' \r\n"
//			+ "      AND date(a.payment_date) BETWEEN '2023-04-01' AND '2024-03-30'\r\n"
//			+ "      AND b.test_user = false\r\n" + ") subquery;\r\n", nativeQuery = true)

	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
			+ "    FROM public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
			+ "      AND a.lender_fee_payments = 'TENYEARS' \r\n" + "      AND b.test_user = false\r\n"
			+ " ) subquery;", nativeQuery = true)
	public Integer findByLenderTenYears();

//	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
//			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
//			+ "    FROM public.lender_payu_details a\r\n"
//			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
//			+ "      AND a.lender_fee_payments = 'FIVEYEARS' \r\n"
//			+ "      AND date(a.payment_date) BETWEEN '2023-04-01' AND '2024-03-30'\r\n"
//			+ "      AND b.test_user = false\r\n" + ") subquery;\r\n", nativeQuery = true)

	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
			+ "    FROM public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
			+ "      AND a.lender_fee_payments = 'FIVEYEARS' \r\n" + "      AND b.test_user = false\r\n"
			+ " ) subquery;", nativeQuery = true)
	public Integer findByLenderFiveYearly();

	@Query(value = "SELECT * FROM public.lender_payu_details WHERE status = 'COMPLETED' AND deal_id != 1 AND lender_user_id = :userId ORDER BY id DESC", nativeQuery = true)
	List<LenderPayuDetails> findByLenderUserIdPaymentType(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.lender_payu_details WHERE status = 'COMPLETED' and deal_id=1 and transaction_number is null and lender_user_id=:userId order by id desc", nativeQuery = true)
	List<LenderPayuDetails> findByLenderUserIdPaymentTypewithHdfc(@Param("userId") Integer userId);

	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
			+ "    FROM public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
			+ "      AND a.lender_fee_payments = 'MONTHLY' \r\n"
			+ "      AND date(a.payment_date) BETWEEN :startDate AND :endDate\r\n" + "      AND b.test_user = false\r\n"
			+ ") subquery;\r\n", nativeQuery = true)
	public Integer findBylenderMonthlyCountAdminModule(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
			+ "    FROM public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
			+ "      AND a.lender_fee_payments = 'QUARTERLY' \r\n"
			+ "      AND date(a.payment_date) BETWEEN :startDate AND :endDate\r\n" + "      AND b.test_user = false\r\n"
			+ ") subquery;\r\n", nativeQuery = true)
	public Integer findByLenderQuarterlyAdminModule(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
			+ "    FROM public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
			+ "      AND a.lender_fee_payments = 'HALFYEARLY' \r\n"
			+ "      AND date(a.payment_date) BETWEEN :startDate AND :endDate\r\n" + "      AND b.test_user = false\r\n"
			+ ") subquery;\r\n", nativeQuery = true)

	public Integer findByLenderHalfyearlyAdminModule(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
			+ "    FROM public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
			+ "      AND a.lender_fee_payments = 'PERYEAR' \r\n"
			+ "      AND date(a.payment_date) BETWEEN :startDate AND :endDate\r\n" + "      AND b.test_user = false\r\n"
			+ ") subquery;\r\n", nativeQuery = true)

	public Integer findByLenderYearlyAdminModule(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
			+ "    FROM public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
			+ "      AND a.lender_fee_payments = 'FIVEYEARS' \r\n"
			+ "      AND date(a.payment_date) BETWEEN :startDate AND :endDate\r\n" + "      AND b.test_user = false\r\n"
			+ ") subquery;\r\n", nativeQuery = true)

	public Integer findByLenderFiveYearlyAdminModule(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
			+ "    FROM public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
			+ "      AND a.lender_fee_payments = 'TENYEARS' \r\n"
			+ "      AND date(a.payment_date) BETWEEN :startDate AND :endDate\r\n" + "      AND b.test_user = false\r\n"
			+ ") subquery;\r\n", nativeQuery = true)
	public Integer findByLenderTenYearsAdminModule(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT SUM(lender_user_count) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT COUNT(DISTINCT a.lender_user_id) AS lender_user_count\r\n"
			+ "    FROM public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "    WHERE a.status = 'COMPLETED' \r\n"
			+ "      AND a.lender_fee_payments = 'LIFETIME' \r\n"
			+ "      AND date(a.payment_date) BETWEEN :startDate AND :endDate\r\n" + "      AND b.test_user = false\r\n"
			+ ") subquery;\r\n", nativeQuery = true)
	public Integer findByLifeTimeFeeAdminModule(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT\r\n" + "  COUNT(DISTINCT subquery.user_id) AS total_users,\r\n"
			+ "  COALESCE(SUM(subquery.lender_user_amount), 0) AS total_amount\r\n" + "FROM (\r\n" + "  SELECT\r\n"
			+ "    COALESCE(SUM(a.amount), 0) AS lender_user_amount,\r\n" + "    a.lender_fee_payments,\r\n"
			+ "    b.id AS user_id\r\n" + "  FROM\r\n" + "    public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "  WHERE\r\n"
			+ "    a.status = 'COMPLETED' \r\n" + "    AND b.test_user = false \r\n"
			+ "	AND date(a.payment_date) = CURRENT_DATE\r\n" + "    AND a.transaction_number LIKE 'OXYLRV%'\r\n"
			+ "  GROUP BY\r\n" + "    a.lender_fee_payments, b.id\r\n" + ") subquery;\r\n", nativeQuery = true)
	public List<Object[]> findByFromWalletFeeCollectedDailyWise();

	@Query(value = "SELECT\r\n" + "  COUNT(DISTINCT subquery.user_id) AS total_users,\r\n"
			+ "  COALESCE(SUM(subquery.lender_user_amount), 0) AS total_amount\r\n" + "FROM (\r\n" + "  SELECT\r\n"
			+ "    COALESCE(SUM(a.amount), 0) AS lender_user_amount,\r\n" + "    a.lender_fee_payments,\r\n"
			+ "    b.id AS user_id\r\n" + "  FROM\r\n" + "    public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "  WHERE\r\n"
			+ "    a.status = 'COMPLETED' \r\n" + "    AND b.test_user = false \r\n"
			+ "	AND date(a.payment_date) = CURRENT_DATE\r\n" + "    AND a.transaction_number LIKE 'order%'\r\n"
			+ "  GROUP BY\r\n" + "    a.lender_fee_payments, b.id\r\n" + ") subquery;", nativeQuery = true)
	public List<Object[]> findByFromCashFeeFeeCollectedDailyWise();

	@Query(value = "SELECT\r\n" + "			  COALESCE(SUM(subquery.lender_user_amount), 0) AS total_amount\r\n"
			+ "			FROM (\r\n" + "			 SELECT\r\n"
			+ "			    COALESCE(SUM(a.amount), 0) AS lender_user_amount,\r\n"
			+ "			   a.lender_fee_payments,\r\n" + "			  b.id AS user_id\r\n" + "			  FROM\r\n"
			+ "			  public.lender_payu_details a\r\n"
			+ "			   LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "			 WHERE\r\n"
			+ "			   a.status = 'COMPLETED' \r\n" + "			   AND b.test_user = false \r\n"
			+ "			AND date(a.payment_date) = CURRENT_DATE\r\n" + "			 \r\n" + "			 GROUP BY\r\n"
			+ "			    a.lender_fee_payments, b.id\r\n" + "		) subquery;", nativeQuery = true)
	public Double findByFeeCollectedDaily();

	@Query(value = "SELECT\r\n" + "  COUNT(DISTINCT subquery.user_id) AS total_users,\r\n"
			+ "  COALESCE(SUM(subquery.lender_user_amount), 0) AS total_amount\r\n" + "FROM (\r\n" + "  SELECT\r\n"
			+ "    COALESCE(SUM(a.amount), 0) AS lender_user_amount,\r\n" + "    a.lender_fee_payments,\r\n"
			+ "    b.id AS user_id\r\n" + "  FROM\r\n" + "    public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "  WHERE\r\n"
			+ "    a.status = 'COMPLETED' \r\n" + "    AND b.test_user = false \r\n"
			+ "    AND date_trunc('month', a.payment_date) = date_trunc('month', CURRENT_DATE)\r\n"
			+ "    AND a.transaction_number LIKE 'OXYLRV%'\r\n" + "  GROUP BY\r\n"
			+ "    a.lender_fee_payments, b.id\r\n" + ") subquery;\r\n", nativeQuery = true)

	public List<Object[]> findByFromWalletFeeCollected();

	@Query(value = "SELECT\r\n" + "  COUNT(DISTINCT subquery.user_id) AS total_users,\r\n"
			+ "  COALESCE(SUM(subquery.lender_user_amount), 0) AS total_amount\r\n" + "FROM (\r\n" + "  SELECT\r\n"
			+ "    COALESCE(SUM(a.amount), 0) AS lender_user_amount,\r\n" + "    a.lender_fee_payments,\r\n"
			+ "    b.id AS user_id\r\n" + "  FROM\r\n" + "    public.lender_payu_details a\r\n"
			+ "    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "  WHERE\r\n"
			+ "    a.status = 'COMPLETED' \r\n" + "    AND b.test_user = false \r\n"
			+ "    AND date_trunc('month', a.payment_date) = date_trunc('month', CURRENT_DATE)\r\n"
			+ "    AND a.transaction_number LIKE 'order%'\r\n" + "  GROUP BY\r\n"
			+ "    a.lender_fee_payments, b.id\r\n" + ") subquery;\r\n", nativeQuery = true)
	public List<Object[]> findByFromCashFeeCollected();

	@Query(value = "SELECT\r\n" + "			  COALESCE(SUM(subquery.lender_user_amount), 0) AS total_amount\r\n"
			+ "			FROM (\r\n" + "		  SELECT\r\n"
			+ "			    COALESCE(SUM(a.amount), 0) AS lender_user_amount,\r\n"
			+ "			    a.lender_fee_payments,\r\n" + "			   b.id AS user_id\r\n" + "			  FROM\r\n"
			+ "			    public.lender_payu_details a\r\n"
			+ "			    LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "			 WHERE\r\n"
			+ "			    a.status = 'COMPLETED' \r\n" + "			   AND b.test_user = false \r\n"
			+ "			    AND date_trunc('month', a.payment_date) = date_trunc('month', CURRENT_DATE)\r\n"
			+ "			  GROUP BY\r\n" + "		   a.lender_fee_payments, b.id\r\n"
			+ "	) subquery;", nativeQuery = true)
	public Double findByTotalFeeCollectedMonthly();

	@Query(value = "select lender_user_id,transaction_number,amount,payment_date,deal_id from public.lender_payu_details \r\n"
			+ "where status='COMPLETED' and substring(transaction_number,1,6)='OXYLRV' and deal_id!=1 order by id desc", nativeQuery = true)
	public List<Object[]> findListOfLenderFeeThroughWalletInPayu();

	@Query(value = "select lender_user_id,transaction_number,amount,payment_date,deal_id from\r\n"
			+ "public.lender_payu_details where status='COMPLETED' and deal_id!=1 and lender_user_id=:userId order by id desc", nativeQuery = true)
	public List<Object[]> lenderPaidFeeThroughWalletInPayu(@Param("userId") Integer userId);

	@Query(value = "SELECT\r\n" + "			  COUNT(DISTINCT subquery.user_id) AS total_users,\r\n"
			+ "			 COALESCE(SUM(subquery.lender_user_amount), 0) AS total_amount\r\n" + "			FROM (\r\n"
			+ "			 SELECT\r\n" + "			  COALESCE(SUM(a.amount), 0) AS lender_user_amount,\r\n"
			+ "			  a.lender_fee_payments,\r\n" + "			   b.id AS user_id\r\n" + "			 FROM\r\n"
			+ "			  public.lender_payu_details a\r\n"
			+ "			  LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "			 WHERE\r\n"
			+ "		   a.status = 'COMPLETED' \r\n" + "			  AND b.test_user = false \r\n"
			+ "			  AND   a.payment_date >= CURRENT_DATE - INTERVAL '6 days'\r\n"
			+ "			  AND a.transaction_number LIKE 'OXYLRV%'\r\n" + "			  GROUP BY\r\n"
			+ "			   a.lender_fee_payments, b.id\r\n" + "			) subquery;\r\n", nativeQuery = true)
	public List<Object[]> findByFromWalletFeeCollectedWeekly();

	@Query(value = "SELECT\r\n" + "			  COUNT(DISTINCT subquery.user_id) AS total_users,\r\n"
			+ "			 COALESCE(SUM(subquery.lender_user_amount), 0) AS total_amount\r\n" + "			FROM (\r\n"
			+ "			 SELECT\r\n" + "			  COALESCE(SUM(a.amount), 0) AS lender_user_amount,\r\n"
			+ "			  a.lender_fee_payments,\r\n" + "			   b.id AS user_id\r\n" + "			 FROM\r\n"
			+ "			  public.lender_payu_details a\r\n"
			+ "			  LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "			 WHERE\r\n"
			+ "		   a.status = 'COMPLETED' \r\n" + "			  AND b.test_user = false \r\n"
			+ "			  AND   a.payment_date >= CURRENT_DATE - INTERVAL '6 days'\r\n"
			+ "			  AND a.transaction_number LIKE 'order%'\r\n" + "			  GROUP BY\r\n"
			+ "			   a.lender_fee_payments, b.id\r\n" + "			) subquery;\r\n", nativeQuery = true)

	public List<Object[]> findByFromCashFeeCollectedWeekly();

	@Query(value = "SELECT\r\n" + "			  COALESCE(SUM(subquery.lender_user_amount), 0) AS total_amount\r\n"
			+ "			FROM (\r\n" + "			 SELECT\r\n"
			+ "			    COALESCE(SUM(a.amount), 0) AS lender_user_amount,\r\n"
			+ "			   a.lender_fee_payments,\r\n" + "			  b.id AS user_id\r\n" + "			  FROM\r\n"
			+ "			  public.lender_payu_details a\r\n"
			+ "			   LEFT JOIN public.user b ON b.id = a.lender_user_id \r\n" + "			 WHERE\r\n"
			+ "			   a.status = 'COMPLETED' \r\n" + "			   AND b.test_user = false \r\n"
			+ "			AND date(a.payment_date) >= CURRENT_DATE - INTERVAL '6 days'\r\n" + "			 \r\n"
			+ "			 GROUP BY\r\n" + "			    a.lender_fee_payments, b.id\r\n"
			+ "		) subquery;", nativeQuery = true)
	public Double findByFeeCollectedDailyWeekly();

	@Query(value = "SELECT \r\n" + "    a.lender_user_id AS user_id,\r\n" + "    COUNT(*) AS user_count,\r\n"
			+ "    SUM(a.amount) AS total_amount\r\n" + "FROM \r\n" + "    public.lender_payu_details a \r\n"
			+ "JOIN \r\n" + "    public.user b ON b.id = a.lender_user_id\r\n" + "WHERE \r\n"
			+ "    a.status = 'COMPLETED' \r\n" + "    AND a.lender_fee_payments = 'PERDEAL'  \r\n"
			+ "    AND b.test_user = false\r\n"
			+ "    AND DATE(a.payment_date) BETWEEN '2023-04-01' AND '2024-03-30'\r\n" + "GROUP BY \r\n"
			+ "    a.lender_user_id;", nativeQuery = true)
	public List<Object[]> findByPerDealCount();

	@Query(value = "SELECT * FROM (" + "    SELECT" + "        a.lender_user_id," + "        a.lender_name,"
			+ "        b.email," + "        b.mobile_number," + "        b.pincode," + "        a.amount,"
			+ "        a.payment_date,"
			+ "        ROW_NUMBER() OVER (PARTITION BY a.lender_user_id ORDER BY a.payment_date DESC) AS rn"
			+ "    FROM" + "        public.lender_payu_details a" + "    JOIN"
			+ "        public.user b ON a.lender_user_id = b.id" + "    WHERE" + "        a.status = 'COMPLETED'"
			+ "        AND b.test_user = false" + "        AND a.lender_fee_payments = :lenderFeePayment"
			+ "        AND b.primary_type != 'ADMIN'" + "        AND a.payment_date BETWEEN :startDate AND :endDate"
			+ ") AS ranked_data" + " WHERE rn = 1" + " ORDER BY payment_date DESC;", nativeQuery = true)
	public List<Object[]> findByLenderPaidAmount(@Param("lenderFeePayment") String lenderFeePayment,
			@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT count(a.lender_user_id)\r\n" + "FROM public.lender_payu_details a\r\n"
			+ "JOIN public.lender_renewal_details b ON a.lender_user_id = b.user_id\r\n"
			+ "JOIN public.user c ON b.user_id = c.id\r\n" + "WHERE a.lender_fee_payments = 'LIFETIME'\r\n"
			+ "  AND a.status = 'COMPLETED'\r\n" + "  AND b.renewal_status = 'NotYetRenewed'\r\n"
			+ "  AND b.validity_date >= '2030-01-01'\r\n" + "  AND c.test_user = false", nativeQuery = true)
	public Integer findByLenderFeePaidUsers();

	@Query(value = "SELECT *\r\n" + "FROM public.lender_payu_details where lender_user_id=:user_id\r\n"
			+ "ORDER BY payment_date DESC\r\n" + "LIMIT 1;", nativeQuery = true)
	public LenderPayuDetails findByLenderIdMemberShipValidity(@Param("user_id") Integer user_id);

	@Query(value = "select * from public.lender_payu_details where lender_user_id=:userId and status='COMPLETED' and lender_fee_payments='LIFETIME'", nativeQuery = true)
	public LenderPayuDetails findUserValidity(@Param("userId") Integer userId);
}
