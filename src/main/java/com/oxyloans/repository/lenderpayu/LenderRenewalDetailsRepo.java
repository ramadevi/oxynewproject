package com.oxyloans.repository.lenderpayu;

import java.util.Date;
import java.util.List;

import com.oxyloans.entity.lenders.payu.LenderRenewalDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface LenderRenewalDetailsRepo extends PagingAndSortingRepository<LenderRenewalDetails, Integer> {

	@Query(value = "select * from public.lender_renewal_details where user_id=:userId order by id asc limit 1", nativeQuery = true)
	public LenderRenewalDetails findingValidityAndUser(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_renewal_details where user_id=:userId and renewal_status='Renewed' \r\n"
			+ "order by id desc limit 1", nativeQuery = true)
	public LenderRenewalDetails gettingPaymentDate(@Param("userId") Integer userId);

	@Query(value = "select count(*) from public.lender_renewal_details where user_id=:userId and (renewal_status!='Renewed' AND renewal_status!='REDUNDANT')", nativeQuery = true)
	public Integer findingCount(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_renewal_details where user_id=:userId order by id desc limit 1", nativeQuery = true)
	public LenderRenewalDetails findingValidityDate(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_renewal_details where user_id=:lenderUserId and renewal_status='NotYetRenewed' order by id desc limit 1", nativeQuery = true)
	public LenderRenewalDetails findingUserInTable(@Param("lenderUserId") Integer lenderUserId);

	@Query(value = "select * from public.lender_payu_details where lender_user_id=:lenderUserId and status='COMPLETED' order by id desc limit 1", nativeQuery = true)
	public LenderRenewalDetails findingUserAndStatus(@Param("lenderUserId") Integer lenderUserId);

	@Query(value = "select * from public.lender_renewal_details where renewal_status='NotYetRenewed' and user_id=:userId", nativeQuery = true)
	public LenderRenewalDetails findingRenewalDetailsBasedOnUserId(@Param("userId") Integer userId);

	@Query(value = "select distinct(user_id) from public.lender_renewal_details where renewal_status='NotYetRenewed'", nativeQuery = true)
	public List<Integer> findingListOfUserIds();

	@Query(value = "select * from public.lender_renewal_details where renewal_status='NotYetRenewed' and user_id=:userId order by id desc limit 1", nativeQuery = true)
	public LenderRenewalDetails findingValidityDateForUser(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_renewal_details where user_id=:lenderUserId limit 1", nativeQuery = true)
	public LenderRenewalDetails findingUser(@Param("lenderUserId") Integer lenderUserId);

	@Query(value = "select count(*) from public.lender_renewal_details where user_id=:userId", nativeQuery = true)
	public Integer findingLenderRenewalDetails(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_renewal_details where user_id=:userId and renewal_status='NotYetRenewed'\r\n"
			+ "order by id desc limit 1", nativeQuery = true)
	public LenderRenewalDetails findingUserRenewalDetails(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_renewal_details where renewal_status='NotYetRenewed' and user_id=:userId ORDER BY user_id DESC LIMIT 1", nativeQuery = true)
	public LenderRenewalDetails findingRenewalDetailsBasedOnUserIdNum(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_renewal_details where renewal_status='NotYetRenewed' and user_id=:userId   AND (validity_date - CURRENT_DATE) >=0\r\n"
			+ " ORDER BY user_id DESC LIMIT 1", nativeQuery = true)
	public LenderRenewalDetails findingRenewalDetailsBasedOnUserIdStatus(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_renewal_details where renewal_status='NotYetRenewed' and user_id=:userId   AND (validity_date - CURRENT_DATE) >=0\r\n"
			+ " ORDER BY user_id DESC LIMIT 1", nativeQuery = true)
	public LenderRenewalDetails findLenderValidityDate(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_renewal_details where renewal_status='NotYetRenewed' and user_id=:userId ORDER BY user_id DESC LIMIT 1", nativeQuery = true)
	public LenderRenewalDetails findUserValidityDate(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.lender_renewal_details WHERE  date(validity_date)>=:startDate  AND date(validity_date)<=:endDate  and renewal_status='NotYetRenewed' order by validity_date  limit :pageSize offset :pageNo\r\n"
			+ "", nativeQuery = true)
	public List<LenderRenewalDetails> getLenderValidityExpireDetailsBetweenDates(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate, @Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "SELECT\r\n" + "    a.user_id,\r\n" + "    b.first_name,\r\n" + "    c.mobile_number,\r\n"
			+ "    c.email,\r\n" + "    b.address,\r\n" + "    c.pincode,\r\n" + "    a.amount,\r\n"
			+ "    a.payment_received_on\r\n" + "FROM\r\n" + "    public.lender_renewal_details a\r\n" + "JOIN\r\n"
			+ "    user_personal_details b ON a.user_id = b.user_id\r\n" + "JOIN\r\n"
			+ "    public.user c ON a.user_id = c.id\r\n" + "WHERE\r\n" + "    a.validity_date >= '2030-01-01'\r\n"
			+ "    AND a.renewal_status = 'NotYetRenewed' \r\n" + "    AND a.payment_received_on >= :startDate \r\n"
			+ "    AND a.payment_received_on <= :endDate \r\n" + "ORDER BY\r\n"
			+ "    a.payment_received_on DESC", nativeQuery = true)
	public List<Object[]> findBylenderdata(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "\r\n" + "SELECT COUNT(a.user_id) AS users_count\r\n" + "FROM public.lender_renewal_details a\r\n"
			+ "JOIN public.user b ON a.user_id = b.id\r\n" + "WHERE b.test_user = false\r\n"
			+ "AND a.validity_date >= '2030-01-01' \r\n"
			+ "AND a.renewal_status = 'NotYetRenewed';", nativeQuery = true)
	public Integer findByLifeTimeWaiverCount();

	@Query(value = "SELECT \r\n" + "    a.user_id, \r\n" + "    a.validity_date, \r\n" + "    b.amount, \r\n"
			+ "    a.renewal_status, \r\n" + "    b.status, \r\n" + "    b.payment_date,\r\n" + "    c.first_name,\r\n"
			+ "    c.last_name,\r\n" + "    d.mobile_number,\r\n" + "    b.lender_fee_payments\r\n" + "FROM \r\n"
			+ "    public.lender_renewal_details a\r\n" + "JOIN \r\n"
			+ "    lender_payu_details b ON a.user_id = b.lender_user_id\r\n" + "LEFT JOIN \r\n"
			+ "    user_personal_details c ON c.user_id = a.user_id\r\n" + "LEFT JOIN \r\n"
			+ "    public.user d ON a.user_id = d.id\r\n" + "WHERE \r\n" + "    a.renewal_status = 'NotYetRenewed' \r\n"
			+ "    AND b.status = 'COMPLETED';\r\n" + "", nativeQuery = true)
	public List<Object[]> findLendersValidityStatus();

	@Query(value = "SELECT a.lender_fee_payments,a.lender_user_id,a.status,b.first_name,b.last_name,c.mobile_number,a.payment_date\r\n"
			+ "FROM public.lender_payu_details a \r\n"
			+ "JOIN user_personal_details b ON b.user_id=a.lender_user_id\r\n"
			+ "LEFT JOIN public.user c ON  b.user_id = c.id \r\n"
			+ "where  a.lender_fee_payments = 'PERDEAL' AND a.status='COMPLETED'", nativeQuery = true)
	public List<Object[]> findLenderPerDealsData();

	@Query(value = "  SELECT a.user_id, a.validity_date, a.renewal_status, c.first_name, c.last_name, d.mobile_number\r\n"
			+ "FROM public.lender_renewal_details a\r\n"
			+ "LEFT JOIN user_personal_details c ON c.user_id = a.user_id\r\n"
			+ "LEFT JOIN public.user d ON a.user_id = d.id\r\n"
			+ "WHERE a.renewal_status = 'NotYetRenewed' AND a.validity_date>='2030-01-01'", nativeQuery = true)
	public List<Object[]> findWaiverUsers();

	@Query(value = "SELECT *\r\n" + "FROM public.lender_renewal_details\r\n" + "WHERE user_id = :userId\r\n"
			+ "AND renewal_status = 'NotYetRenewed'\r\n" + "AND validity_date IS NOT NULL\r\n" + "", nativeQuery = true)
	public LenderRenewalDetails findLenderValidity(@Param("userId") Integer userId);
}
