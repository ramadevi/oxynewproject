package com.oxyloans.repository.lenderpayu;

import java.util.List;

import com.oxyloans.entity.lenders.payu.LenderFeeDetailsHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LenderFeeDetailsHistoryRepo extends JpaRepository<LenderFeeDetailsHistory, Integer> {

	@Query(value = "SELECT * FROM public.lender_fee_details_history WHERE lender_fee_payments = :lenderFeeStatus AND is_current = true", nativeQuery = true)
	LenderFeeDetailsHistory findByLenderFeePaymentStatus(@Param("lenderFeeStatus") String lenderFeeStatus);

	@Query(value = "SELECT * \r\n" + "FROM public.lender_fee_details_history \r\n" + "WHERE is_current = true \r\n"
			+ "AND lender_fee_payments IS NOT NULL;\r\n" + "", nativeQuery = true)
	List<LenderFeeDetailsHistory> findByBooleanStatus();

	@Query(value = "SELECT * FROM lender_fee_details_history WHERE lender_fee_payments = :lenderFeePayments AND fee_amount = :feeAmount AND is_current = true", nativeQuery = true)
	LenderFeeDetailsHistory findByPaymentStatusAndAmount(@Param("lenderFeePayments") String lenderFeePayments,
			@Param("feeAmount") Double feeAmount);
}
