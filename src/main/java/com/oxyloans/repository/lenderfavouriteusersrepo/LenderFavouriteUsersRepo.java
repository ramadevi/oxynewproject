package com.oxyloans.repository.lenderfavouriteusersrepo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.user.LenderFavouriteUsers.LenderFavouriteUsers;

@Repository
public interface LenderFavouriteUsersRepo extends PagingAndSortingRepository<LenderFavouriteUsers, Integer>,
		JpaSpecificationExecutor<LenderFavouriteUsers> {

	public List<LenderFavouriteUsers> findByBorrowerUserId(int userId);

	public LenderFavouriteUsers findByLenderUserIdAndBorrowerUserId(Integer lenderUserId, Integer borrowerUserId);

	@Query(value = "select * from public.lender_favourite_users where lender_user_id=:userId", nativeQuery = true)
	public LenderFavouriteUsers findbyLenderUserId(int userId);

}
