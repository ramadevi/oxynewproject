package com.oxyloans.repository.user;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.user.UserQueryDetails;

public interface UserQueryDetailsRepo extends PagingAndSortingRepository<UserQueryDetails, Integer> {

	@Query(value = "select * from public.user_query_details order by id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<UserQueryDetails> findingListOfUserQueryDetails(@Param("pageNo") Integer pageNo,
			@Param("pageSize") Integer pageSize);

	@Query(value = "select count(id) from public.user_query_details", nativeQuery = true)
	public Integer findingCountOfQueryDetails();

	@Query(value = "select * from public.user_query_details where id=:id and user_id=:userId", nativeQuery = true)
	public UserQueryDetails findingQueryByIdAndUserId(@Param("id") Integer id, @Param("userId") Integer userId);

	@Query(value = "select * from public.user_query_details a join public.user b on a.user_id=b.id where b.primary_type=:primaryType \r\n"
			+ " order by a.id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<UserQueryDetails> findingUsersBasedOnPrimaryType(@Param("primaryType") String primaryType,
			@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "select * from public.user_query_details a join public.user b on a.user_id=b.id where b.primary_type=:primaryType\r\n"
			+ " and a.status=:modified order by a.id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<UserQueryDetails> findingUsersBasedOnStatus(@Param("primaryType") String primaryType,
			@Param("modified") String modified, @Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "select count(*) from public.user_query_details a join public.user b on a.user_id=b.id where b.primary_type=:primaryType", nativeQuery = true)
	public Integer findingCountOfUsers(@Param("primaryType") String primaryType);

	@Query(value = "select count(*) from public.user_query_details a join public.user b on a.user_id=b.id where b.primary_type=:primaryType\r\n"
			+ "and a.status=:modified", nativeQuery = true)
	public Integer findingCountOfUsersBasedOnStatus(@Param("primaryType") String primaryType,
			@Param("modified") String modified);

	@Query(value = "select count(*) from public.user_query_details where status=:modified", nativeQuery = true)
	public Integer findingCountOfQueries(@Param("modified") String modified);

	@Query(value = "select * from public.user_query_details where user_id=:userId\r\n"
			+ "and status=:modified order by id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<UserQueryDetails> findingUsersBasedOnStatusAndId(@Param("userId") Integer userId,
			@Param("modified") String modified, @Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "select count(*) from public.user_query_details where user_id=:userId\r\n"
			+ "and status=:modified", nativeQuery = true)
	public Integer findingCountOfUsersBasedOnStatusAndId(@Param("userId") Integer userId,
			@Param("modified") String modified);

	@Query(value = "select * from public.user_query_details where user_id=:userId order by id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<UserQueryDetails> findingUsersBasedOnId(@Param("userId") Integer userId,
			@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "select count(*) from public.user_query_details where user_id=:userId", nativeQuery = true)
	public Integer findingCountOfQueries(@Param("userId") Integer userId);

	@Query(value = "select * from public.user_query_details where id=:id", nativeQuery = true)
	public UserQueryDetails findingQueryById(@Param("id") Integer id);

	@Query(value = "select * from public.user_query_details a join public.user b on a.user_id=b.id where b.primary_type=:primaryType\r\n"
			+ " and a.status=:modified order by a.responded_on desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<UserQueryDetails> findingUsersBasedOnCompletedStatus(@Param("primaryType")String primaryType, @Param("modified")String modified,
			@Param("pageNo")Integer pageNo, @Param("pageSize")Integer pageSize);

	@Query(value = "select count(*) from user_query_details where user_id =:id and status='Completed'", nativeQuery = true)
	public Integer approveCountByUserId(@Param("id") Integer id);

	@Query(value = "select count(*) from user_query_details where user_id =:id and status='Cancelled'", nativeQuery = true)
	public Integer cancelCountByUserId(@Param("id") Integer id);

	@Query(value = "select count(*) from user_query_details where user_id =:id and status='Pending'", nativeQuery = true)
	public int pendingCountByUserId(@Param("id") Integer id);


}
