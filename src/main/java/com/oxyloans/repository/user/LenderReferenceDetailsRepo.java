package com.oxyloans.repository.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.LenderReferenceDetails.LenderReferenceDetails;
import com.oxyloans.response.user.LenderReferenceResponseDto;

@Repository
public interface LenderReferenceDetailsRepo extends PagingAndSortingRepository<LenderReferenceDetails, Integer>,
		JpaSpecificationExecutor<LenderReferenceDetails> {

	LenderReferenceDetails save(LenderReferenceResponseDto reference);

	@Query(value = "select referee_email,referee_mobilenumber from public.lender_reference_details where referrer_id=:referrerId", nativeQuery = true)
	public List<Object[]> gettingLenderReferenceInfo(@Param("referrerId") Integer referrerId);

	@Query(value = "select * from public.lender_reference_details where referrer_id=:referrerId and referee_mobilenumber=:refereeMobileNumber", nativeQuery = true)
	public LenderReferenceDetails findingReferenceInfo(@Param("referrerId") Integer referrerId,
			@Param("refereeMobileNumber") String refereeMobileNumber);

	public LenderReferenceDetails findByRefereeEmail(String email);

	public LenderReferenceDetails findByRefereeMobileNumber(String mobileNumber);

	@Query(value = "select * from public.lender_reference_details where referrer_id=:lenderId order by id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<LenderReferenceDetails> displayLenderRefereesInfo(@Param("lenderId") Integer lenderId,
			@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "select count(*) from public.lender_reference_details where referrer_id=:lenderId", nativeQuery = true)
	public Integer countOfReferees(@Param("lenderId") Integer lenderId);

	@Query(value = "select * from public.lender_reference_details where referrer_id=:lenderId and status='Lent' order by id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<LenderReferenceDetails> gettingLenderRefereesRegistered(@Param("lenderId") Integer lenderId,
			@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "select count(*) from public.lender_reference_details where referrer_id=:lenderId and status='Lent'", nativeQuery = true)
	public Integer gettingCountOfRefereesRegistered(@Param("lenderId") Integer lenderId);

	@Query(value = "select * from public.lender_reference_details order by id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<LenderReferenceDetails> findAll(@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "select count(*) from public.lender_reference_details", nativeQuery = true)
	public Integer totalCountOfReferees();

	@Query(value = "select sum(b.disbursment_amount) from public.user a join public.oxy_loan b\r\n"
			+ "on a.id=b.lender_user_id join public.lender_reference_details c\r\n"
			+ "on c.referee_id = b.lender_user_id where c.referee_id=:refereeId and b.admin_comments='DISBURSED' and\r\n"
			+ "b.borrower_disbursed_date >= '2021-04-29'", nativeQuery = true)
	public Double getRefrrerInfo(@Param("refereeId") Integer refereeId);

	@Query(value = "select * from public.lender_reference_details where referee_id=:refereeId", nativeQuery = true)
	public LenderReferenceDetails findRefereeInfo(@Param("refereeId") Integer refereeId);

	@Query(value = "select amount from public.lender_reference_details where referee_id=:refereeId", nativeQuery = true)
	public Double findingAmountForReferrer(@Param("refereeId") Integer refereeId);

	@Query(value = "SELECT * FROM public.lender_reference_details where referrer_id=:referrerId and referee_mobilenumber=:refereeMobileNumber", nativeQuery = true)
	public LenderReferenceDetails findingReferrerInfo(@Param("referrerId") Integer referrerId,
			@Param("refereeMobileNumber") String refereeMobileNumber);

	@Query(value = "SELECT * FROM public.lender_reference_details where referrer_id=:referrerId and referee_mobilenumber=:refereeMobileNumber", nativeQuery = true)
	public LenderReferenceDetails findingReferrerInformation(@Param("referrerId") Integer referrerId,
			@Param("refereeMobileNumber") String refereeMobileNumber);

	@Query(value = "SELECT * FROM public.lender_reference_details where referee_mobilenumber=:refereeMobileNumber", nativeQuery = true)
	public LenderReferenceDetails findingReferrerInformation(@Param("refereeMobileNumber") String refereeMobileNumber);

	@Query(value = "select * from public.lender_reference_details where id=:id", nativeQuery = true)
	public LenderReferenceDetails gettingReferenceInfo(@Param("id") Integer id);

	@Query(value = "SELECT * FROM public.lender_reference_details where referee_id=:refereeId", nativeQuery = true)
	public LenderReferenceDetails gettingReferralAmountId(@Param("refereeId") Integer refereeId);

	@Query(value = "select c.disbursment_amount,c.borrower_disbursed_date from public.lender_reference_details a join public.user b on a.referee_id=b.id\r\n"
			+ "join public.oxy_loan c on c.lender_user_id =b.id\r\n"
			+ "where a.referee_id=:refereeId and a.status='Lent' and c.borrower_disbursed_date is not null", nativeQuery = true)
	public List<Object[]> getListOfReferees(@Param("refereeId") Integer refereeId);

	@Query(value = "select count(c.borrower_disbursed_date) from public.lender_reference_details a join public.user b on a.referee_id=b.id\r\n"
			+ "join public.oxy_loan c on c.lender_user_id =b.id\r\n"
			+ "where a.referee_id=:refereeId and a.status='Lent' limit :pageSize offset :pageNo\r\n"
			+ "", nativeQuery = true)
	public Integer getCountOfDisbursments(@Param("refereeId") Integer refereeId, @Param("pageNo") Integer pageNo,
			@Param("pageSize") Integer pageSize);

	@Query(value = "select count(a.referee_user_id) from public.lender_referral_bonus_updated a\r\n"
			+ "join public.lender_reference_details b on a.referee_user_id=b.referee_id where b.status='Lent' and b.amount>0\r\n"
			+ "and a.payment_status='Unpaid'", nativeQuery = true)
	public Integer gettingCountOfLenderReferralBonusDetails();

	@Query(value = "select * from public.lender_reference_details where referee_id=:refereeId and referrer_id=:referrerId", nativeQuery = true)
	public LenderReferenceDetails findingRefereeAndReferrerInfo(@Param("refereeId") Integer refereeId,
			@Param("referrerId") Integer referrerId);

	@Query(value = "select distinct(referrer_id) from public.lender_reference_details", nativeQuery = true)
	public List<Integer> findingReferrers();

	@Query(value = "select * from public.lender_reference_details where referrer_id=:id", nativeQuery = true)
	public List<LenderReferenceDetails> gettingListOfReferees(@Param("id") Integer id);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referee_user_id=:refereeId and payment_status='Unpaid'\r\n"
			+ "", nativeQuery = true)
	Double findingAmountForUnpaid(@Param("refereeId") Integer refereeId);

	@Query(value = "select * from public.lender_reference_details where referee_id=:refereeId", nativeQuery = true)
	public LenderReferenceDetails findingRefereeAndReferrerInfo(@Param("refereeId") Integer refereeId);

	@Query(value = "select count(id) from public.lender_reference_details where upper(referee_email) =upper(:emailAddress)", nativeQuery = true)
	public Integer findByRefereeEmailInEqualIgnoranceCase(@Param("emailAddress") String emailAddress);

	@Query(value = "select * from public.lender_reference_details where referrer_id=:userId and user_primarytype=:primaryType order by id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<LenderReferenceDetails> gettingListOfRefereesBasedOnPrimaryType(@Param("userId") Integer userId,
			@Param("primaryType") String primaryType, @Param("pageNo") Integer pageNo,
			@Param("pageSize") Integer pageSize);

	@Query(value = "select * from public.lender_reference_details where referrer_id=:id order by id desc limit :pageSize offset :pageNo"
			+ "", nativeQuery = true)
	public List<LenderReferenceDetails> gettingListOfRefereesBasedOnPageSize(@Param("id") Integer id,
			@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "select count(*) from public.lender_reference_details where referrer_id=:userId and user_primarytype=:primaryType", nativeQuery = true)
	public Integer findingCountOfReferees(@Param("userId") Integer userId, @Param("primaryType") String primaryType);

	@Query(value = "select count(*) from public.lender_reference_details where referrer_id=:userId", nativeQuery = true)
	public Integer findingCountOfRefereesBasedOnId(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_reference_details where referee_id=:refereeId", nativeQuery = true)
	public LenderReferenceDetails findingRefereeAndReferrerInfoBasedOnId(@Param("refereeId") Integer refereeId);

	@Query(value = "select * from public.lender_reference_details where referee_id=:id", nativeQuery = true)
	public LenderReferenceDetails findingReferrerInfo(@Param("id") Integer id);

	/*
	 * @Query(value =
	 * "SELECT * FROM public.lender_reference_details where referee_email=:email and referee_mobilenumber=:mobileNumber and source='ReferralLink'"
	 * , nativeQuery = true) public LenderReferenceDetails
	 * checkingRefereeAlreadyInvitedForNonNri(@Param("email") String email,
	 * 
	 * @Param("mobileNumber") String mobileNumber);
	 */

	@Query(value = "SELECT * FROM public.lender_reference_details where referrer_id=:userId and referee_email=:email", nativeQuery = true)
	public LenderReferenceDetails checkingRefereeAlreadyInvitedForNri(@Param("userId") Integer userId,
			@Param("email") String email);

	@Query(value = "select count(*) from public.lender_reference_details where referrer_id=:userId and status != 'Invited'", nativeQuery = true)
	public Integer findingReferrerInformation(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_reference_details where referee_mobilenumber=:mobileNumber or \r\n"
			+ "upper(referee_email)=upper(:email)", nativeQuery = true)
	public List<LenderReferenceDetails> findingRefereeExistsOrNot(@Param("mobileNumber") String mobileNumber,
			@Param("email") String email);

	@Query(value = "SELECT * FROM public.lender_reference_details where referee_email=:email and referee_mobilenumber=:mobileNumber", nativeQuery = true)
	public LenderReferenceDetails checkingRefereeAlreadyInvitedForNonNriForPartner(@Param("email") String email,
			@Param("mobileNumber") String mobileNumber);

	@Query(value = "select count(referee_id) from public.lender_reference_details where referrer_id=:lenderId and upper(status)!=upper('Invited')", nativeQuery = true)
	public Integer findingNumberOfRefereesReferredByUser(@Param("lenderId") Integer lenderId);

	@Query(value = "select a.referee_id,concat(b.first_name,' ',b.last_name),to_char(c.registered_on,'yyyy-MM-dd'),c.email,\r\n"
			+ "c.mobile_number from public.lender_reference_details a join \r\n"
			+ "public.user_personal_details b on a.referee_id=b.user_id join public.user c on c.id=b.user_id\r\n"
			+ "where a.referrer_id=:userId", nativeQuery = true)
	public List<Object[]> findingListOfRefereesBasedOnUserId(@Param("userId") Integer userId);

	@Query(value = "select count(referrer_id) from public.lender_reference_details where referrer_id =:referrerId",nativeQuery = true)
	public Integer finByTotalCount(@Param("referrerId") Integer referrerId);


}
