package com.oxyloans.repository.user;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.user.UserPendingQueries;

public interface UserPendingQueriesRepo extends PagingAndSortingRepository<UserPendingQueries, Integer> {

	//@Query(value = "select * from public.user_pending_queries where table_id=:id and resolved_by!='User'", nativeQuery = true)
	@Query(value = "SELECT * FROM public.user_pending_queries WHERE table_id =:id and responded_by is not null ORDER BY responded_on DESC" ,nativeQuery = true)
	public List<UserPendingQueries> findingPendingQueiresBasedOnId(@Param("id") Integer id);

	@Query(value = "select * from public.user_pending_queries where table_id=:id and resolved_by='User'", nativeQuery = true)
	public List<UserPendingQueries> findingListOfQueriesRaisedByUser(@Param("id") Integer id);

}
