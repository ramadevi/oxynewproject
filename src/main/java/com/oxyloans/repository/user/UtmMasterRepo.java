package com.oxyloans.repository.user;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


import com.oxyloans.entity.user.UtmMaster;


@Repository
public interface UtmMasterRepo extends PagingAndSortingRepository<UtmMaster, Integer>{
	List<UtmMaster> findAll();

}
