package com.oxyloans.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.user.UserOxyAccount;

@Repository
public interface UserOxyAccountRepo extends PagingAndSortingRepository<UserOxyAccount, Integer> {

}
