package com.oxyloans.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.user.PersonalDetailsModifiedHistory;

@Repository
public interface PersonalDetailsModifiedHistoryRepo
		extends PagingAndSortingRepository<PersonalDetailsModifiedHistory, Integer> {

}
