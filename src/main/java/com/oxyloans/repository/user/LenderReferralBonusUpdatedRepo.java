package com.oxyloans.repository.user;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.oxyloans.entity.LenderReferenceDetails.LenderReferralBonusUpdated;

@Repository
public interface LenderReferralBonusUpdatedRepo extends PagingAndSortingRepository<LenderReferralBonusUpdated, Integer>,
		JpaSpecificationExecutor<LenderReferralBonusUpdated> {

	@Query(value = "select a.referee_user_id,b.referee_name,a.referrer_user_id,a.amount,a.payment_status,b.status,a.radhasir_comments_bonus from public.lender_referral_bonus_updated a \r\n"
			+ "join public.lender_reference_details b on a.referee_user_id=b.referee_id where b.status='Lent' and b.amount>0\r\n"
			+ "and a.payment_status='Unpaid'", nativeQuery = true)
	public List<Object[]> findingReferralBonusDetails();

	@Query(value = "select a.id,a.referee_user_id,b.referee_name,a.referrer_user_id,a.amount,a.payment_status,b.status \r\n"
			+ "from public.lender_referral_bonus_updated a \r\n"
			+ "join public.lender_reference_details b on a.referee_user_id=b.referee_id where b.status='Lent' \r\n"
			+ "and b.amount>0\r\n" + "and a.payment_status='Unpaid'", nativeQuery = true)
	public List<Object[]> findingReferralBonusAmountDetails();

	@Query(value = "select a.id,a.referee_user_id,b.referee_name,a.referrer_user_id,a.amount,a.payment_status,b.status \r\n"
			+ "from public.lender_referral_bonus_updated a\r\n"
			+ "join public.lender_reference_details b on a.referee_user_id=b.referee_id where b.status='Lent' \r\n"
			+ "and b.amount>0\r\n"
			+ "and a.payment_status='Unpaid' and a.radhasir_comments_bonus='Approved'", nativeQuery = true)
	public List<Object[]> gettingReferralAmountDetails();

	@Query(value = "select * from public.lender_referral_bonus_updated where id=:referralId", nativeQuery = true)
	public LenderReferralBonusUpdated getIdOfReferrer(@Param("referralId") Integer referralId);

	@Query(value = "SELECT * FROM public.lender_referral_bonus_updated where referee_user_id=:refereeId and payment_status='Unpaid'", nativeQuery = true)
	public LenderReferralBonusUpdated findRefereeInfoInReferralBonus(@Param("refereeId") Integer refereeId);

	@Query(value = "select sum(a.amount) from public.lender_referral_bonus_updated a join public.lender_reference_details b\r\n"
			+ "on a.referee_user_id=b.referee_id where b.referee_id=:refereeId and a.payment_status='Paid'", nativeQuery = true)
	public Double findingRefereeBonusAmount(@Param("refereeId") Integer refereeId);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referee_user_id=:refereeId", nativeQuery = true)
	public Double getBonusAmount(@Param("refereeId") Integer refereeId);

	@Query(value = "SELECT referee_user_id, referrer_user_id, amount, payment_status, transferred_on, radhasir_comments_bonus, deal_id, \r\n"
			+ "participated_on, participated_amount,updated_bonus,remarks FROM public.lender_referral_bonus_updated where referee_user_id=:refereeId", nativeQuery = true)
	public List<Object[]> getReferralPaymentStatus(@Param("refereeId") Integer refereeId);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referrer_user_id=:referrerId", nativeQuery = true)
	public Double findingSumOfAmountEarnedByReferrer(@Param("referrerId") Integer referrerId);

	@Query(value = "select * from public.lender_referral_bonus_updated where referrer_user_id=:id and referee_user_id=:refereeId", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingListOfReferralBonus(@Param("id") Integer id,
			@Param("refereeId") Integer refereeId);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referee_user_id=:refereeId and payment_status='Paid' and amount>1000", nativeQuery = true)
	public Double findingSumOfAmount(@Param("refereeId") Integer refereeId);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where \r\n"
			+ "referee_user_id=:refereeId", nativeQuery = true)
	public Double getAmountForReferee(@Param("refereeId") Integer refereeId);

	@Query(value = "select * from public.lender_referral_bonus_updated where referee_user_id=:refereeId and deal_id=:id and payment_status='Unpaid' and radhasir_comments_bonus is null", nativeQuery = true)
	public LenderReferralBonusUpdated findingAmountBasedOnDealId(@Param("refereeId") Integer refereeId,
			@Param("id") Integer id);

	@Query(value = "select * from public.lender_referral_bonus_updated where id=:id", nativeQuery = true)
	public LenderReferralBonusUpdated gettingReferralInfo(@Param("id") Integer id);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referee_user_id=:refereeId and payment_status='Unpaid'", nativeQuery = true)
	public Double findingSumOfAmountInUnPaidState(@Param("refereeId") Integer refereeId);

	@Query(value = "select referee_id from public.lender_reference_details where status='Lent'", nativeQuery = true)
	public List<Integer> findingRefereesList();

	@Query(value = "select amount from public.lender_referral_bonus_updated where \r\n"
			+ "referee_user_id=:refereeId and deal_id=:id", nativeQuery = true)
	public Double getAmountForRefereeForDeal(@Param("refereeId") Integer refereeId, @Param("id") Integer id);

	@Query(value = "select distinct(transferred_on) from public.lender_referral_bonus_updated where payment_status='Paid'", nativeQuery = true)
	public List<Date> gettingListOfPaidDates();

	@Query(value = "select * from public.lender_referral_bonus_updated where extract(Year from transferred_on)=:year\r\n"
			+ "and extract(Month from transferred_on)=:month and extract(Day from transferred_on)=:day and payment_status='Paid' limit :pageSize offset :pageNo\r\n"
			+ "", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingListOfLenders(@Param("year") int year, @Param("month") int month,
			@Param("day") int day, @Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where extract(Year from transferred_on)=:year\r\n"
			+ "and extract(Month from transferred_on)=:month and extract(Day from transferred_on)=:day and payment_status='Paid'\r\n"
			+ "", nativeQuery = true)
	public Double gettingTotalPaidAmount(@Param("year") int year, @Param("month") int month, @Param("day") int day);

	@Query(value = "select count(*) from public.lender_referral_bonus_updated where extract(Year from transferred_on)=:year\r\n"
			+ "and extract(Month from transferred_on)=:month and extract(Day from transferred_on)=:day and payment_status='Paid'\r\n"
			+ "", nativeQuery = true)
	public Integer findingCountOfLenders(@Param("year") int year, @Param("month") int month, @Param("day") int day);

	@Query(value = "SELECT count(id) FROM public.lender_referral_bonus_updated where referrer_user_id=:referrerId", nativeQuery = true)
	public Integer getTotalReferrerBonusCount(@Param("referrerId") Integer referrerId);

	@Query(value = "SELECT referee_user_id,amount,payment_status,transferred_on,deal_id,participated_on,participated_amount FROM public.lender_referral_bonus_updated where referrer_user_id=:referrerId order by id asc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getTotalReferrerDetailsByAscOrder(@Param("referrerId") Integer referrerId,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "SELECT referee_user_id,amount,payment_status,transferred_on,deal_id,participated_on,participated_amount FROM public.lender_referral_bonus_updated where referrer_user_id=:referrerId order by id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getTotalReferrerDetailsByDescOrder(@Param("referrerId") Integer referrerId,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referrer_user_id=:userId", nativeQuery = true)
	public Double findingTotalSumOfEarnedAmount(@Param("userId") Integer userId);

	@Query(value = "WITH RankedDeals AS (\r\n"
			+ "    SELECT deal_id, referrer_user_id, referee_user_id, payment_status, updated_bonus,amount, transferred_on, remarks,participated_on,participated_amount,\r\n"
			+ "           ROW_NUMBER() OVER (PARTITION BY deal_id ORDER BY transferred_on DESC) as rn\r\n"
			+ "    FROM public.lender_referral_bonus_updated\r\n" + "    WHERE referrer_user_id = :referrerId\r\n"
			+ "   \r\n" + ")\r\n"
			+ "SELECT deal_id, referrer_user_id, referee_user_id, payment_status, updated_bonus,amount, transferred_on, remarks,participated_on,participated_amount\r\n"
			+ "FROM RankedDeals\r\n" + "WHERE rn = 1\r\n" + "ORDER BY payment_status DESC", nativeQuery = true)
	public List<Object[]> getTotalReferrerDetails(@Param("referrerId") Integer referrerId);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referrer_user_id=:referrerId and referee_user_id=:refereeId\r\n"
			+ "", nativeQuery = true)
	public Double findingSumOfAmount(@Param("referrerId") Integer referrerId, @Param("refereeId") Integer refereeId);

	@Query(value = "select * from public.lender_referral_bonus_updated where referrer_user_id=:id", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingListOfReferralBonusForReferrer(@Param("id") Integer id);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referrer_user_id=:userId and payment_status='Paid'", nativeQuery = true)
	public Double sumOfEarnedAmountRecevied(@Param("userId") Integer userId);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referrer_user_id=:userId and payment_status='Unpaid'", nativeQuery = true)
	public Double sumOfEarnedAmountNotRecevied(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_referral_bonus_updated where referrer_user_id=:userId and payment_status=:paymentStatus order by id desc\r\n"
			+ "", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingListOfBonusAmountBasedOnStatus(@Param("userId") Integer userId,
			@Param("paymentStatus") String paymentStatus);

	@Query(value = "select * from public.lender_referral_bonus_updated where referrer_user_id=:userId and payment_status=:paymentStatus order by id desc limit :pageSize offset :pageNo\r\n"
			+ "", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingListOfBonusAmountBasedOnStatusAndPagination(
			@Param("userId") Integer userId, @Param("paymentStatus") String paymentStatus,
			@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "select count(*) from public.lender_referral_bonus_updated where referrer_user_id=:userId and payment_status=:paymentStatus\r\n"
			+ "", nativeQuery = true)
	public Integer findingCountOfBonusAmountBasedOnStatus(@Param("userId") Integer userId,
			@Param("paymentStatus") String paymentStatus);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referrer_user_id=:userId and payment_status='Paid'", nativeQuery = true)
	public Double findingSumOfPaidAmount(@Param("userId") Integer userId);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referrer_user_id=:userId and payment_status='Unpaid'", nativeQuery = true)
	public Double findingSumOfAmountUnpaidAmount(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_referral_bonus_updated where referrer_user_id=:userId order by id desc limit :pageSize offset :pageNo\r\n"
			+ "", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingListOfBonusAmountBaPagination(@Param("userId") Integer userId,
			@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "select count(*) from public.lender_referral_bonus_updated where referrer_user_id=:userId\r\n"
			+ "", nativeQuery = true)
	public Integer findingCountOfBonusAmount(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_referral_bonus_updated where referrer_user_id=:userId order by id desc\r\n"
			+ "", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingListOfBonusAmount(@Param("userId") Integer userId);

	@Query(value = "select distinct(referrer_user_id) from public.lender_referral_bonus_updated where payment_status='Unpaid' order by referrer_user_id desc", nativeQuery = true)
	public List<Integer> findingListOfReferees();

	/*
	 * @Query(value =
	 * "select distinct(referrer_user_id) from public.lender_referral_bonus_updated where payment_status='Unpaid' and referrer_user_id=39315"
	 * , nativeQuery = true) public List<Integer> findingListOfReferees();
	 */
	@Query(value = "select  distinct(a.referee_user_id) from public.lender_referral_bonus_updated a join public.lender_reference_details b\r\n"
			+ "on a.referee_user_id=b.referee_id\r\n"
			+ "where b.referrer_id=:referrerId and b.status !='Disbursed' and a.payment_status='Unpaid' group by a.referee_user_id", nativeQuery = true)
	public List<Integer> findingListOfRefereesOfReferrer(@Param("referrerId") Integer referrerId);

	@Query(value = "select distinct(deal_id) from public.lender_referral_bonus_updated where referrer_user_id=:referrerId and referee_user_id=:refereeId and payment_status='Unpaid'", nativeQuery = true)
	public List<Integer> findingListOfDealIdsForReferee(@Param("referrerId") Integer referrerId,
			@Param("refereeId") Integer refereeId);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referee_user_id=:refereeId and deal_id=:dealId", nativeQuery = true)
	public Double findingSumOfAmountBasedOnRerefeeIdAndDealId(@Param("refereeId") Integer refereeId,
			@Param("dealId") Integer dealId);

	@Query(value = "select * from public.lender_referral_bonus_updated where referee_user_id=:refereeId and deal_id=:dealId and payment_status='Unpaid'", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingListOfDealInfo(@Param("dealId") Integer dealId,
			@Param("refereeId") Integer refereeId);

	@Query(value = "select distinct(deal_id) from public.lender_referral_bonus_updated where payment_status='Unpaid' and referee_user_id=:refereeId", nativeQuery = true)
	public List<Integer> findingDealIds(@Param("refereeId") Integer refereeId);

	@Query(value = "select * from public.lender_referral_bonus_updated a join public.lender_reference_details b on a.referee_user_id=b.referee_id where\r\n"
			+ "deal_id=:dealId and referee_id=:refereeId and payment_status='Unpaid'", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingRefereeInfo(@Param("refereeId") Integer refereeId,

			@Param("dealId") Integer dealId);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where deal_id=:dealId and referee_user_id=:refereeId", nativeQuery = true)
	public Double findingSumOfAmountBasedOnDealId(@Param("dealId") Integer dealId,
			@Param("refereeId") Integer refereeId);

	@Query(value = "select * from public.lender_referral_bonus_updated where id=:id", nativeQuery = true)
	public LenderReferralBonusUpdated findingPaymentStatus(@Param("id") Integer id);

	@Query(value = "select * from public.lender_referral_bonus_updated where payment_status='Unpaid' and referee_user_id=:refereeId and deal_id=0", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingListOfDealIdsZero(@Param("refereeId") Integer refereeId);

	@Query(value = "select  sum(a.amount) from public.lender_referral_bonus_updated a join public.lender_reference_details b\r\n"
			+ "on a.referee_user_id=b.referee_id\r\n"
			+ "where b.referrer_id=:referrerId and b.status='Disbursed' and a.payment_status='Unpaid'", nativeQuery = true)
	public Double findingSumOfAmountForDisbursedState(@Param("referrerId") Integer referrerId);

	@Query(value = "select  * from public.lender_referral_bonus_updated a join public.lender_reference_details b\r\n"
			+ "on a.referee_user_id=b.referee_id\r\n"
			+ "where b.referrer_id=:referrerId and b.status='Disbursed'  and a.payment_status='Unpaid'", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingListOfBonusForDisbursedState(
			@Param("referrerId") Integer referrerId);

	@Query(value = "select referrer_user_id,sum(amount) from public.lender_referral_bonus_updated where participated_on>=:lastDayOfQuarter\r\n"
			+ "group by referrer_user_id order by sum(amount) desc limit 3", nativeQuery = true)
	public List<Object[]> findingListOfHighestReferralBonus(@Param("lastDayOfQuarter") LocalDate lastDayOfQuarter);

	@Query(value = "select * from public.lender_referral_bonus_updated where referee_user_id=:refereeId and deal_id=:dealId", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingListOfDealAndRefereeIdsForAmountUpdation(
			@Param("refereeId") Integer refereeId, @Param("dealId") Integer dealId);

	@Query(value = "select * from public.lender_referral_bonus_updated where referee_user_id=:refereeNewId and deal_id=:tableId", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingPaymnetStatusForDeal(@Param("tableId") Integer tableId,
			@Param("refereeNewId") int refereeNewId);

	@Query(value = "select * from public.lender_referral_bonus_updated where referrer_user_id=:userId", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingReferralBonus(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_referral_bonus_updated where referrer_user_id=:referrerId and referee_user_id=:refereeId", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingReferralBonusForRefereeAndReferrer(
			@Param("referrerId") Integer referrerId, @Param("refereeId") Integer refereeId);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referee_user_id=:refereeId and payment_status='Paid'", nativeQuery = true)
	public Double findingSumOfAmountEarnedByRefereeNew(Integer refereeId);

	@Query(value = "select * from public.lender_referral_bonus_updated where referrer_user_id=:referrerId and updated_bonus =0", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingListOfRefereesInfo(@Param("referrerId") Integer referrerId);

	@Query(value = "select distinct(referee_user_id) from public.lender_referral_bonus_updated where referrer_user_id=:referrerId and updated_bonus !=0", nativeQuery = true)
	public List<Integer> findingListOfReferees(@Param("referrerId") Integer referrerId);

	@Query(value = "SELECT deal_id, referrer_user_id, referee_user_id, payment_status, updated_bonus, transferred_on, remarks\r\n"
			+ "FROM (\r\n"
			+ "    SELECT deal_id, referrer_user_id, referee_user_id, payment_status, updated_bonus, transferred_on, remarks,\r\n"
			+ "           ROW_NUMBER() OVER (PARTITION BY deal_id ORDER BY transferred_on DESC) as rn\r\n"
			+ "    FROM public.lender_referral_bonus_updated\r\n" + "    WHERE referrer_user_id = :referrerId\r\n"
			+ "        AND updated_bonus != 0\r\n" + "        AND referee_user_id = :refereeId\r\n" + ") t\r\n"
			+ "WHERE rn = 1\r\n" + "ORDER BY deal_id", nativeQuery = true)
	public List<Object[]> findingListOfDistinctDealIds(@Param("refereeId") Integer refereeId,
			@Param("referrerId") Integer referrerId);

	@Query(value = "select distinct(referrer_user_id) from public.lender_referral_bonus_updated\r\n"
			+ "where payment_status='Paid' and to_char(transferred_on,'yyyy-MM-dd')>=:startDate\r\n"
			+ "and to_char(transferred_on,'yyyy-MM-dd')<=:endDate", nativeQuery = true)
	public List<Integer> findListOfReferrerIds(@Param("startDate") String startDate, @Param("endDate") String endDate);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated  where payment_status='Paid' and \r\n"
			+ "to_char(transferred_on,'yyyy-MM-dd')>=:startDate\r\n"
			+ "and to_char(transferred_on,'yyyy-MM-dd')<=:endDate and updated_bonus=0 and\r\n"
			+ "referrer_user_id=:referrerId", nativeQuery = true)
	public Double findSumOfReferralAmountByReferrer(@Param("referrerId") Integer referrerId,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	@Query(value = "select distinct(deal_id),updated_bonus,referee_user_id from public.lender_referral_bonus_updated  where payment_status='Paid' and \r\n"
			+ "to_char(transferred_on,'yyyy-MM-dd')>=:startDate\r\n"
			+ "and to_char(transferred_on,'yyyy-MM-dd')<=:endDate and updated_bonus!=0 and\r\n"
			+ "referrer_user_id=:referrerId group by deal_id,updated_bonus,referee_user_id", nativeQuery = true)
	public List<Object[]> findingListOfAmountDetailsForReferrer(@Param("referrerId") Integer referrerId,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	@Query(value = "select sum(amount) from public.lender_referral_bonus_updated where referrer_user_id=:referrerId and payment_status='Paid'", nativeQuery = true)
	public Double findingSumOfAmountEarnedByReferrerId(@Param("referrerId") Integer referrerId);

	public List<LenderReferralBonusUpdated> findByReferrerUserIdAndRefereeUserIdAndDealId(int referrerUserId,
			int refereeUserId, int dealId);

	@Query(value = "SELECT * FROM public.lender_referral_bonus_updated WHERE referee_user_id = :refereeId AND deal_id = :dealNumber AND payment_status = 'Unpaid' ORDER BY participated_on DESC LIMIT 1", nativeQuery = true)
	public Integer findByIdBasedOnRefereeIdAndDealId(@Param("refereeId") Integer refereeId,
			@Param("dealNumber") Integer dealNumber);

	

	@Query(value = "select * from public.lender_referral_bonus_updated where referee_user_id=:refereeId and deal_id=:dealId and payment_status='Unpaid'", nativeQuery = true)
	public List<LenderReferralBonusUpdated> findingListOfDealInfo1(@Param("refereeId") Integer refereeId ,@Param("dealId") Integer dealId);

	 
	@Query(value ="SELECT  SUM(amount) AS total_amount\r\n"
	    		+ "FROM lender_referral_bonus_updated\r\n"
	    		+ "WHERE referee_user_id = :refereeId AND deal_id = :dealId AND payment_status = 'Paid'",nativeQuery =true)
		public Double findingreferralDetails(@Param("refereeId")int refereeId, @Param("dealId")int dealId);

	@Query(value = "SELECT SUM(participated_amount) " +
            "FROM lender_referral_bonus_updated " +
            "WHERE referee_user_id = :refereeId AND deal_id = :dealId AND payment_status = 'Paid'", nativeQuery = true)
    public Double findParticipationAmt(@Param("refereeId") int refereeId, @Param("dealId") int dealId);

	 @Query(value = "select * from public.lender_referral_bonus_updated where referee_user_id=:refereeId and  deal_id=:dealId and payment_status='Paid' LIMIT 1", nativeQuery = true)
		public List<LenderReferralBonusUpdated> findingListOfDealInfo3(@Param("refereeId") Integer refereeId, @Param("dealId") Integer dealId);

	 @Query(value = "SELECT DISTINCT updated_bonus FROM public.lender_referral_bonus_updated WHERE referee_user_id = :refereId AND payment_status = 'Paid'", nativeQuery = true)
		public Double findUpdatedAmount1(@Param("refereId") Integer refereId);

	 @Query(value = "select * from public.lender_referral_bonus_updated where referee_user_id=:refereeId and  deal_id=:dealId and payment_status='Paid' LIMIT 1", nativeQuery = true)
		public LenderReferralBonusUpdated findingListOfRefInfo(@Param("refereeId") Integer refereeId, @Param("dealId") Integer dealId);

	 @Query(value = "SELECT DISTINCT updated_bonus FROM public.lender_referral_bonus_updated WHERE referee_user_id = :refereeId1 AND deal_id =:dealId1 AND payment_status = 'Paid'", nativeQuery = true)
		public Double findUpdatedAmount(@Param("refereeId1") Integer refereeId1, @Param("dealId1") Integer dealId1);


		@Query(value = "SELECT * FROM public.lender_referral_bonus_updated WHERE referee_user_id = :refereeId AND payment_status = 'Paid' LIMIT 1", nativeQuery = true)
	    public LenderReferralBonusUpdated findByRefereeUserId(@Param("refereeId") int refereeId);

		@Query(value = "select * from public.lender_referral_bonus_updated where referee_user_id=:refereeId and  deal_id=:dealId and payment_status='Paid' LIMIT 1", nativeQuery = true)
		public LenderReferralBonusUpdated  findingListOfDealInf(@Param("refereeId") Integer refereeId, @Param("dealId") Integer dealId);

		 @Query(value = "SELECT referrer_user_id,\r\n"
		    		+ "       SUM(CASE WHEN updated_bonus > 0 THEN updated_bonus ELSE amount END) AS sumamount,\r\n"
		    		+ "       mobile_number,\r\n"
		    		+ "       email\r\n"
		    		+ "FROM (\r\n"
		    		+ "    SELECT a.referrer_user_id,\r\n"
		    		+ "           a.updated_bonus,\r\n"
		    		+ "           a.amount,\r\n"
		    		+ "           b.mobile_number,\r\n"
		    		+ "           b.email,\r\n"
		    		+ "           ROW_NUMBER() OVER (PARTITION BY a.referrer_user_id, a.referee_user_id, a.deal_id ORDER BY a.updated_bonus DESC) AS row_num\r\n"
		    		+ "    FROM public.lender_referral_bonus_updated a\r\n"
		    		+ "    JOIN public.user b ON a.referrer_user_id = b.id\r\n"
		    		+ "    WHERE a.payment_status = 'Paid'\r\n"
		    		+ "      AND b.test_user = false\r\n"
		    		+ ") AS subquery\r\n"
		    		+ "WHERE (updated_bonus > 0 AND row_num = 1) OR (updated_bonus = 0)\r\n"
		    		+ "GROUP BY referrer_user_id, mobile_number, email\r\n"
		    		+ "ORDER BY sumamount DESC\r\n"
		    		+ "LIMIT :number", nativeQuery = true)
		    public List<Object[]> findingHighestEarndeReferralBonus(@Param("number") Integer number);

		
}
