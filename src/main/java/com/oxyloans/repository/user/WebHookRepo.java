package com.oxyloans.repository.user;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.oxyloans.entity.user.WebHook;

@Repository
public interface WebHookRepo extends PagingAndSortingRepository<WebHook, Integer>,
JpaSpecificationExecutor<WebHook>{
	
	@Query(value = "SELECT * FROM public.web_hook\r\n"
			+ "WHERE notification_to = 'ADMINSTATS'\r\n"
			+ "  AND is_currnt = true\r\n"
			+ "  AND id IS NOT NULL\r\n"
			+ "ORDER BY time_stamp_from DESC;\r\n"
			+ "", nativeQuery = true)

	public WebHook findByAdminStats();
	
	@Query(value = "SELECT * FROM public.web_hook\r\n"
			+ "WHERE notification_to = 'MONTHLY'\r\n"
			+ "  AND is_currnt = true\r\n"
			+ "  AND id IS NOT NULL\r\n"
			+ "ORDER BY time_stamp_from DESC;\r\n"
			+ "", nativeQuery = true)

	public WebHook findByMonthlystats();

	@Query(value = "SELECT * FROM public.web_hook\r\n"
			+ "WHERE notification_to = 'EVERYMONTHLY'\r\n"
			+ "  AND is_currnt = true\r\n"
			+ "  AND id IS NOT NULL\r\n"
			+ "ORDER BY time_stamp_from DESC;\r\n"
			+ "", nativeQuery = true)
	public WebHook findByEveryMonthlystats();

	@Query(value = "SELECT * FROM public.web_hook\r\n"
			+ "WHERE notification_to = 'WEEKLY'\r\n"
			+ "  AND is_currnt = true\r\n"
			+ "  AND id IS NOT NULL\r\n"
			+ "ORDER BY time_stamp_from DESC;\r\n"
			+ "", nativeQuery = true)
	public WebHook findByWeeklystats();



}
