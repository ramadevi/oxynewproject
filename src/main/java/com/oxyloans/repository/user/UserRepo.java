package com.oxyloans.repository.user;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.user.User;
import com.oxyloans.entity.user.User.PrimaryType;

@Repository
public interface UserRepo extends PagingAndSortingRepository<User, Integer> {

	public User findByEmailIgnoreCase(String email);

	public User findByEmailToken(String email_token);

	public User findByMobileNumber(String mobileNumber);

	public List<User> findAll();

	public Long countByPrimaryType(PrimaryType lender);

	public List<User> findByPrimaryType(PrimaryType lender);

	public int countByRegisteredOnGreaterThanEqual(Date registerOn);

	public User findByMobileNumberAndAdminComments(String mobileNumber, String string);

	String sql = "SELECT distinct urchin_tracking_module FROM public.user where urchin_tracking_module is not null";

	@Query(value = sql, nativeQuery = true)
	List<String> getUtmFields();

	String lendersLoanInformation = "SELECT * FROM  lendersLoanInformation(:pageSize,:pageNumber)";

	@Query(value = lendersLoanInformation, nativeQuery = true)
	List<Integer> getLendersLoanInformation(@Param("pageSize") Integer pageSize,
			@Param("pageNumber") Integer pageNumber);

	String numberOfUsers = "select count(*) From public.user where primary_type=:primaryType";

	@Query(value = numberOfUsers, nativeQuery = true)
	Integer getcount(String primaryType);

	String borrowersInformation = "SELECT count(*) FROM public.user where primary_type='BORROWER' AND status='REGISTERED' AND admin_comments is null";

	@Query(value = borrowersInformation, nativeQuery = true)
	Integer getBorrowersInformation();

	String lendersInformation = "SELECT count(*) FROM public.user where primary_type='LENDER' AND status='REGISTERED'";

	@Query(value = lendersInformation, nativeQuery = true)
	Integer getLendersInformation();

	String seningEmailBeforeEmiDate = "select * from SendingEmailsBeforeEmiDate()";

	@Query(value = seningEmailBeforeEmiDate, nativeQuery = true)
	List<Object[]> sendingEmailsToBorrowersBeforeEmiDate();

	String numberOfBorrowersInterested = "select count(*) from public.user a join public.oxy_loan_request b on a.id=b.user_id where a.admin_comments='INTERESTED' and a.primary_type='BORROWER'";

	@Query(value = numberOfBorrowersInterested, nativeQuery = true)
	Integer getInterestedCount();

	String kycPendingBorrowers = "SELECT \r\n" + "  a.email,a.registered_on,c.first_name,c.last_name,,a.id\r\n"
			+ "FROM \r\n"
			+ "  public.\"user\" a LEFT join  public.user_document_status b on a.id=b.user_id  join  public.user_personal_details c on a.id=c.user_id where  a.primary_type='BORROWER' and b.user_id isnull and a.email is not null ;\r\n"
			+ "  \r\n" + "";

	@Query(value = kycPendingBorrowers, nativeQuery = true)
	List<Object[]> getkycPendingBorrowers();

	String kycPendingLenders = "SELECT \r\n" + "  a.email,a.registered_on,c.first_name,c.last_name,a.id\r\n"
			+ "FROM \r\n"
			+ "  public.\"user\" a LEFT join  public.user_document_status b on a.id=b.user_id  join  public.user_personal_details c on a.id=c.user_id where  a.primary_type='LENDER' and b.user_id isnull and a.email is not null ;\r\n"
			+ "  \r\n" + "";

	@Query(value = kycPendingLenders, nativeQuery = true)
	List<Object[]> getkycPendingLenders();

	String borrowersLoanInformation = "SELECT * FROM  borrowersLoanInformation(:pageSize,:pageNumber)";

	@Query(value = borrowersLoanInformation, nativeQuery = true)
	List<Integer> getBorrowersLoanInformation(@Param("pageSize") Integer pageSize,
			@Param("pageNumber") Integer pageNumber);

	String countLenderLoanInfo = "select count(*) from(SELECT distinct a.id From public.user a join public.oxy_loan b on a.id=b.lender_user_id where a.primary_type='LENDER' and b.lender_user_id is not null and b.borrower_disbursed_date is not null)tab";

	@Query(value = countLenderLoanInfo, nativeQuery = true)
	Integer getLendersCount();

	String countBorrowerLoanInfo = "select count(*) from(SELECT distinct a.id From public.user a join public.oxy_loan b on a.id=b.borrower_user_id where a.primary_type='BORROWER'and b.borrower_user_id is not null and b.borrower_disbursed_date is not null)tab";

	@Query(value = countBorrowerLoanInfo, nativeQuery = true)
	Integer getBorrowerscount();

	String searchByIdForBorrowers = "SELECT distinct a.id From public.user a join public.oxy_loan b on a.id=b.borrower_user_id where a.id=:userId and b.borrower_user_id is not null and b.borrower_disbursed_date is not null";

	@Query(value = searchByIdForBorrowers, nativeQuery = true)
	Integer getIdForBorrower(@Param("userId") Integer userId);

	String searchByIdForLenders = "SELECT distinct a.id From public.user a join public.oxy_loan b on a.id=b.lender_user_id where a.id=:userId and b.borrower_user_id is not null and b.borrower_disbursed_date is not null";

	@Query(value = searchByIdForLenders, nativeQuery = true)
	Integer getIdForLender(@Param("userId") Integer userId);

	public User findByUniqueNumber(String uniqueNumber);

	public User findByVanNumber(String vanNumber);

	@Query(value = "select * from public.user where unique_number like %:borrowerUnique% and primary_type='BORROWER' and status='ACTIVE'", nativeQuery = true)
	public List<User> findBorrowerBasedOnUnique(@Param("borrowerUnique") String borrowerUnique);

	@Query(value = "select a.borrower_unique_number,a.amount,a.paid_date,a.updated_on,a.payment_type,a.user_id,b.file_name,b.file_path,a.id from public.payment_upload_history a join public.user_document_status b on a.document_uploaded_id=b.id where b.user_id=:userId and document_sub_type='PAYMENTSCREENSHOT'", nativeQuery = true)
	public List<Object[]> getCountOfPaymentScreenshot(@Param("userId") Integer userId);

	@Query(value = "select count(b.user_id) from public.payment_upload_history a join public.user_document_status b on a.document_uploaded_id=b.id where b.user_id=:userId and document_sub_type='PAYMENTSCREENSHOT'", nativeQuery = true)
	public Integer getCountOfPaymentScreenshotUploaded(@Param("userId") Integer userId);

	@Query(value = "SELECT concat(b.first_name,' ',b.last_name),a.loan_id,a.disbursment_amount,a.loan_status,sum(c.emi_interst_amount) as profit,a.rate_of_interest ,a.duration,count(c.emi_paid_on) as countOfEmiPaid FROM public.oxy_loan a join public.user_personal_details b on a.borrower_user_id=b.user_id join public.loan_emi_card c on a.id=c.loan_id where a.lender_user_id=:lenderId and c.emi_paid_on BETWEEN :startDate1 AND :endDate1 and a.loan_status !='ADMINREJECTED' group by concat(b.first_name,' ',b.last_name),a.loan_id,a.disbursment_amount,a.loan_status,a.rate_of_interest ,a.duration order by a.loan_id", nativeQuery = true)
	public List<Object[]> getLenderProfitInfo(@Param("lenderId") Integer lenderId,
			@Param("startDate1") Timestamp startDate1, @Param("endDate1") Timestamp endDate1);

	@Query(value = "select * from public.user where id=:id", nativeQuery = true)
	public User findByIdNum(@Param("id") Integer id);

	@Query(value = "select id from public.user where primary_type='LENDER' and status='ACTIVE' order by registered_on limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Integer> getLenderIds(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select count(id) from public.user where primary_type='LENDER' and status='ACTIVE' ", nativeQuery = true)
	public Integer getLenderIdsCountValue();

	@Query(value = "SELECT DISTINCT loan_owner FROM public.user where LENGTH(loan_owner)>2 order by loan_owner;", nativeQuery = true)
	public List<String> getLoanOwners();

	@Query(value = "SELECT a.id,b.first_name,b.last_name from public.user a join public.user_personal_details b on a.id=b.user_id where loan_owner=:loanOwner", nativeQuery = true)
	public List<Object[]> getBorrowersMappedToLoanOwner(@Param("loanOwner") String loanOwner);

	@Query(value = "SELECT * FROM public.\"user\" where id=:id", nativeQuery = true)
	public User getDetailsById(@Param("id") Integer id);

	@Query(value = "SELECT a.id,a.email,a.mobile_number,b.first_name,b.last_name FROM public.\"user\" a join  public.user_personal_details b on a.id=b.user_id where a.primary_type='LENDER' and a.lender_group_id=:groupId", nativeQuery = true)
	public List<Object[]> getListOfLendersMappedToGroupId(@Param("groupId") Integer groupId);

	@Query(value = "SELECT count(id) FROM public.\"user\" where primary_type='LENDER' and lender_group_id=:groupId", nativeQuery = true)
	public Integer getListOfLendersMappedToGroupIdCount(@Param("groupId") Integer groupId);

	@Query(value = "select b.borrower_user_id,c.first_name,c.last_name,b.disbursment_amount,b.borrower_disbursed_date\r\n"
			+ "from public.user a join public.oxy_loan b on a.id=b.borrower_user_id join \r\n"
			+ "public.user_personal_details c on a.id=c.user_id where b.loan_status='Active' \r\n"
			+ "and b.borrower_disbursed_date is not null and b.lender_user_id=:lenderId and a.loan_owner is null\r\n"
			+ "limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> findingLenderBorrowerLoansNotBelongsToLoanOwner(@Param("lenderId") Integer lenderId,
			@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "select b.borrower_user_id,c.first_name,c.last_name,b.disbursment_amount,b.borrower_disbursed_date \r\n"
			+ "from public.user a join public.oxy_loan b on a.id=b.borrower_user_id join \r\n"
			+ "public.user_personal_details c on a.id=c.user_id where b.loan_status='Active' \r\n"
			+ "and b.borrower_disbursed_date is not null and b.lender_user_id=:lenderId and a.loan_owner=:loanOwnerName\r\n"
			+ "limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> findingLenderBorrowerLoanBasedOnLoanOwner(@Param("loanOwnerName") String loanOwnerName,
			@Param("lenderId") Integer lenderId, @Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "select distinct(a.loan_owner) from public.user a join public.oxy_loan b \r\n"
			+ "on a.id=b.borrower_user_id where b.loan_status='Active' and b.borrower_disbursed_date is not null and\r\n"
			+ "b.lender_user_id=:lenderId limit :pageSize offset :pageNo", nativeQuery = true)
	public List<String> findingLoanOwnersForLender(@Param("lenderId") Integer lenderId, @Param("pageNo") Integer pageNo,
			@Param("pageSize") Integer pageSize);

	@Query(value = "SELECT id FROM public.\"user\" where loan_owner=:loanOwner and primary_type='BORROWER'", nativeQuery = true)
	public List<Integer> getListOfBorrowersMappedToLoanOwner(@Param("loanOwner") String loanOwner);

	@Query(value = "select whatsapp_number from public.user_personal_details where user_id=:id", nativeQuery = true)
	public String findingWhatsAppNumberForTheUser(@Param("id") Integer id);

	@Query(value = "select sum(transaction_amount) from public.lender_scrow_wallet where TO_CHAR(transaction_date,'YYYY-MM-DD')>=:fundsAcceptanceStartDate and TO_CHAR(transaction_date,'YYYY-MM-DD') <=:fundsAcceptanceEndDate and status='APPROVED' and transactiontype='credit'", nativeQuery = true)
	public BigInteger findingSumOfAmountOFLendersTransfered(
			@Param("fundsAcceptanceStartDate") String fundsAcceptanceStartDate,
			@Param("fundsAcceptanceEndDate") String fundsAcceptanceEndDate);

	@Query(value = "select * from public.user a join public.oxy_lenders_group b on a.lender_group_id=b.id \r\n"
			+ "where b.lender_group_name=:groupName limit :pageSize offset :pageNo", nativeQuery = true)
	public List<User> findingListOfLendersMappedToGroup(@Param("groupName") String groupName,
			@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select * from public.user where lender_group_id=0 and primary_type='LENDER' limit :pageSize offset :pageNo", nativeQuery = true)
	public List<User> findingListOfNewLenders(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select count(*) from public.user where lender_group_id=0 and primary_type='LENDER'", nativeQuery = true)
	public Integer getCountOfNewLenders();

	@Query(value = "select count(*) from public.user a join public.oxy_lenders_group b on a.lender_group_id=b.id \r\n"
			+ "where b.lender_group_name=:groupName", nativeQuery = true)
	public Integer getCountOfLendersMAppedToGroup(@Param("groupName") String groupName);

	@Query(value = "select * from public.user a join public.oxy_lenders_group b on a.lender_group_id=b.id \r\n"
			+ "where b.lender_group_name=:groupName", nativeQuery = true)
	public List<User> findingListOfLendersMappedToGroupNoPaging(@Param("groupName") String groupName);

	@Query(value = "select * from public.user where lender_group_id=0 and primary_type='LENDER'", nativeQuery = true)
	public List<User> findingListOfNewLendersNoPaging();

	@Query(value = "SELECT sum(transaction_amount) FROM public.lender_scrow_wallet where remarks LIKE :dealName", nativeQuery = true)
	public BigInteger findingSumOfAmountOFLendersTransfered(@Param("dealName") String dealName);

	@Query(value = "SELECT * FROM public.user where van_number=:vanNumber", nativeQuery = true)
	public User findBorrowerByVanNumber(@Param("vanNumber") String vanNumber);

	@Query(value = "SELECT sum(transaction_amount) FROM public.lender_scrow_wallet where remarks LIKE :dealName", nativeQuery = true)
	public BigInteger findingSumOfAmountOFLendersTransferedByReamarks(@Param("dealName") String dealName);

	@Query(value = "select * from public.user where primary_type='LENDER' and lender_group_id !=0", nativeQuery = true)
	public List<User> findingListOfUsers();

	@Query(value = "select count(id) from public.user where upper(email) =upper(:emailAddress)", nativeQuery = true)
	public Integer findByEmailUsingEqualIgnoranceCase(@Param("emailAddress") String emailAddress);

	@Query(value = "SELECT * FROM public.user a join public.oxy_loan_request b on (b.user_id=a.id) \r\n"
			+ "where upper(a.loan_owner)=upper(:utm) and a.primary_type='BORROWER' and b.parent_request_id is null order by b.loan_requested_date", nativeQuery = true)
	public List<User> findbyUtmAndParentRequestIdisNull(@Param("utm") String utm);

	@Query(value = "SELECT * FROM public.\"user\" where primary_type='ADMIN'", nativeQuery = true)
	public User getAdminDetails();

	@Query(value = "select user_id,sum(transaction_amount) from public.lender_scrow_wallet where status='APPROVED' and transactiontype='credit'\r\n"
			+ "and TO_CHAR(transaction_date,'YYYY-MM-DD')>=:startDate and TO_CHAR(transaction_date,'YYYY-MM-DD')<=:endDate and\r\n"
			+ "user_id != 6680 group by user_id having sum(transaction_amount)<=5000000 order by sum(transaction_amount)  desc limit 30;\r\n"
			+ "", nativeQuery = true)
	public List<Object[]> findingTop30Lenders(@Param("startDate") String startDate, @Param("endDate") String endDate);

	@Query(value = "select borrower_user_id,sum(disbursment_amount) from public.oxy_loan where loan_status='Active'\r\n"
			+ "and borrower_disbursed_date is not null and admin_comments='DISBURSED' and \r\n"
			+ "TO_CHAR(borrower_disbursed_date,'YYYY-MM-DD')>=:startDate and\r\n"
			+ "TO_CHAR(borrower_disbursed_date,'YYYY-MM-DD')<=:endDate group by borrower_user_id having\r\n"
			+ "sum(disbursment_amount)<=1000000 order by\r\n"
			+ "sum(disbursment_amount) desc limit 30", nativeQuery = true)
	public List<Object[]> findingTop30Borrowers(@Param("startDate") String startDate, @Param("endDate") String endDate);

	@Query(value = "select * from public.user where primary_type=:primaryType and urchin_tracking_module=:utm order by id", nativeQuery = true)
	public List<User> findbyUtmAndprimaryType(@Param("primaryType") String primaryType, @Param("utm") String utm);

	@Query(value = "SELECT * FROM public.\"user\" where primary_type in ('BORROWER','LENDER') order by id", nativeQuery = true)
	public List<User> findByAllBorrowersAndLenders();

	@Query(value = "SELECT * FROM public.\"user\" where loan_owner is null and id=:id", nativeQuery = true)
	public User findBorrowerMappedToLoanOwner(@Param("id") Integer id);

	@Query(value = "select * from public.user where primary_type='LENDER' and upper(urchin_tracking_module)=upper(:utmName)", nativeQuery = true)
	public List<User> fingingListOfLendersBasedOnUtm(@Param("utmName") String utmName);

	@Query(value = "select * from public.user where primary_type='BORROWER' and upper(urchin_tracking_module)=upper(:utmName)", nativeQuery = true)
	public List<User> findingListOfBorrowers(@Param("utmName") String utmName);

	@Query(value = "select count(*) from public.user where primary_type='LENDER' and upper(urchin_tracking_module)=upper(:utmName)", nativeQuery = true)
	public Integer findingCountOfLenders(@Param("utmName") String utmName);

	@Query(value = "select count(*) from public.user where primary_type='BORROWER' and upper(urchin_tracking_module)=upper(:utmName)", nativeQuery = true)
	public Integer findingCountOfBorrowers(String utmName);

	@Query(value = "select count(*) from public.user where upper(urchin_tracking_module)=upper(:utmName) and \r\n"
			+ "TO_CHAR(registered_on,'yyyy-MM-dd')=:time", nativeQuery = true)
	public Integer findingUsersRegisteredToday(@Param("utmName") String utmName, @Param("time") String time);

	@Query(value = "select count(*) from public.user where upper(urchin_tracking_module)=upper(:utmName)", nativeQuery = true)
	public Integer findingAllUsersBasedOnUtm(@Param("utmName") String utmName);

	@Query(value = "select distinct lw.user_id,pd.first_name,pd.last_name,\r\n"
			+ "(select CAST(SUM(A.transaction_amount)  AS DOUBLE PRECISION) from public.lender_scrow_wallet A where u.id = A.user_id AND A.status='APPROVED' AND transactiontype='credit') as \"credit\",\r\n"
			+ "(select CAST(SUM(A.transaction_amount)  AS DOUBLE PRECISION)from public.lender_scrow_wallet A where u.id = A.user_id AND A.status='APPROVED' AND transactiontype='debit') as \"debit\",\r\n"
			+ "(select sum(ol.disbursment_amount) from public.oxy_loan ol where u.id=ol.lender_user_id AND ol.loan_status in('Active','Agreed','Closed') AND date_trunc('day', ol.loan_accepted_date) >= '2020-02-14') as \"Aggrements\" ,\r\n"
			+ "(SELECT sum(O.disbursment_amount) FROM public.oxy_loan O where u.id =O.lender_user_id and (O.loan_status='Agreed' OR O.loan_status='Active' OR O.loan_status='Closed') and O.deal_id>0) as \"AggrementsToDeals\",\r\n"
			+ "(SELECT sum(a.participated_amount) FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where u.id=a.user_id) as \"firstPaticipation\",\r\n"
			+ "(SELECT sum(c.updation_amount) FROM public.lenders_paticipation_updation c join public.oxy_borrowers_deals_information b on c.deal_id=b.id where u.id=c.user_id) as \"secondPaticipation\"\r\n"
			+ "from public.user u INNER JOIN public.user_personal_details pd ON pd.user_id=u.id join public.lender_scrow_wallet lw on u.id=lw.user_id where u.primary_type='LENDER'  order by lw.user_id ASC", nativeQuery = true)
	public List<Object[]> findingListOfUsersData();

	@Query(value = "select id from public.user where urchin_tracking_module=:partnerName and primary_type='LENDER'", nativeQuery = true)
	public List<Integer> listOfLendersMappedToPartner(@Param("partnerName") String partnerName);


	@Query(value = "SELECT DISTINCT b.email \r\n"
			+ "FROM public.oxy_lenders_accepted_deals a \r\n"
			+ "JOIN public.user b ON a.user_id = b.id \r\n"
			+ "WHERE b.email_approval = 'ACTIVE' \r\n"
			+ "AND b.email_verified = 'true'\r\n"
			+ "AND b.test_user = 'false'\r\n"
			+ "ORDER BY b.email DESC;\r\n"
			+ "", nativeQuery = true)
	public List<String> listActiveLendersMails();


	@Query(value = "SELECT a.id FROM public.user a join public.oxy_loan_request b on a.id=b.user_id join public.loan_offerd_amount c on b.id=c.id where a.loan_owner=:dealName and a.primary_type='BORROWER' and b.parent_request_id is null and c.loan_offerd_status='LOANOFFERACCEPTED' order by c.loan_offered_amount desc", nativeQuery = true)
	public List<Integer> borroweridsMappedToDeal(@Param("dealName") String dealName);

	@Query(value = "select a.mobile_number as mobilenumber,a.email,concat(c.first_name,' ',c.last_name) as name,a.id as lenderid\r\n"
			+ ",date(a.registered_on) as registeredDate from public.user a join public.user_personal_details c \r\n"
			+ "on a.id=c.user_id WHERE NOT EXISTS (select * from public.oxy_lenders_accepted_deals b \r\n"
			+ "where a.id=b.user_id) and\r\n"
			+ "to_char(registered_on, 'yyyy-MM-dd')>=:startDate and to_char(registered_on, 'yyyy-MM-dd')<=:endDate \r\n"
			+ "and a.primary_type='LENDER' order by a.id", nativeQuery = true)
	public List<Object[]> findingListOfLendersNotParticipatedInDeal(@Param("startDate") String startDate,
			@Param("endDate") String endDate);

	@Query(value = "select a.mobile_number as mobilenumber,a.email,concat(c.first_name,' ',c.last_name) as name,a.id as lenderid\r\n"
			+ ",date(a.registered_on) as registeredDate from public.user a join \r\n"
			+ "public.user_personal_details c on a.id=c.user_id WHERE EXISTS \r\n"
			+ "(select * from public.oxy_lenders_accepted_deals b where a.id=b.user_id) \r\n"
			+ "and to_char(registered_on, 'yyyy-MM-dd')>=:startDate and \r\n"
			+ "to_char(registered_on, 'yyyy-MM-dd')<=:endDate and a.primary_type='LENDER' order by a.id", nativeQuery = true)
	public List<Object[]> findingListOfLendersParticipatedInDeal(@Param("startDate") String startDate,
			@Param("endDate") String endDate);

	@Query(value = "select * from public.user where primary_type='LENDER' ", nativeQuery = true)
	public List<User> findingListOfLenders();

	@Query(value = "select distinct a.id from public.user a join public.oxy_lenders_accepted_deals b on a.id=b.user_id where a.primary_type='LENDER' and a.test_user!=true order by a.id desc", nativeQuery = true)
	public List<Integer> findingListOfActivesLender();

	@Query(value = "SELECT * \r\n"
			+ "FROM public.user  \r\n"
			+ "WHERE email_approval = 'ACTIVE' \r\n"
			+ "AND primary_type = 'LENDER'\r\n"
			+ "AND email_verified = 'true' \r\n"
			+ "AND test_user = 'false' \r\n"
			+ "AND email IS NOT NULL", nativeQuery = true)
	     public List<User> getTotalActiveLenders();

	public User[] findUsersWithPersonalDetailsByPersonalDetailsWhatsAppNumber(String whatsappNumber);

	public int countByRegisteredOnGreaterThanEqualAndPrimaryType(Date time, PrimaryType borrower);

	@Query(value = "select * from public.user where test_user ='true' ", nativeQuery = true)
	public List<User> getListIfTestUser();

	@Query(value ="WITH subquery AS (\r\n"
			+ "   SELECT\r\n"
			+ "        u.id,\r\n"
			+ "        pd.first_name,\r\n"
			+ "        pd.last_name,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT CAST(SUM(COALESCE(A.transaction_amount, 0)) AS DOUBLE PRECISION)\r\n"
			+ "            FROM public.lender_scrow_wallet A\r\n"
			+ "            WHERE u.id = A.user_id AND A.status = 'APPROVED' AND transactiontype = 'credit'\r\n"
			+ "        ), 0) as a,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT CAST(SUM(COALESCE(A.transaction_amount, 0)) AS DOUBLE PRECISION)\r\n"
			+ "            FROM public.lender_scrow_wallet A\r\n"
			+ "            WHERE u.id = A.user_id AND A.status = 'APPROVED' AND transactiontype = 'debit'\r\n"
			+ "        ), 0) as b,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(ol.disbursment_amount, 0))\r\n"
			+ "            FROM public.oxy_loan ol\r\n"
			+ "            WHERE u.id = ol.lender_user_id\r\n"
			+ "            AND ol.loan_status IN ('Active', 'Agreed', 'Closed')\r\n"
			+ "            AND date_trunc('day', ol.loan_accepted_date) >= '2020-02-14'\r\n"
			+ "        ), 0) as c,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(O.disbursment_amount, 0))\r\n"
			+ "            FROM public.oxy_loan O\r\n"
			+ "            WHERE u.id = O.lender_user_id\r\n"
			+ "            AND (O.loan_status = 'Agreed' OR O.loan_status = 'Active' OR O.loan_status = 'Closed')\r\n"
			+ "            AND O.deal_id > 0\r\n"
			+ "        ), 0) as d,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(a.participated_amount, 0))\r\n"
			+ "            FROM public.oxy_lenders_accepted_deals a\r\n"
			+ "            JOIN public.oxy_borrowers_deals_information b ON a.deal_id = b.id\r\n"
			+ "            WHERE u.id = a.user_id\r\n"
			+ "        ), 0) as e,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(c.updation_amount, 0))\r\n"
			+ "            FROM public.lenders_paticipation_updation c\r\n"
			+ "            JOIN public.oxy_borrowers_deals_information b ON c.deal_id = b.id\r\n"
			+ "            WHERE u.id = c.user_id\r\n"
			+ "        ), 0) as f\r\n"
			+ "    FROM public.user u\r\n"
			+ "    INNER JOIN public.user_personal_details pd ON pd.user_id = u.id\r\n"
			+ "    WHERE u.primary_type = 'LENDER' and u.test_user=false\r\n"
			+ ")\r\n"
			+ "SELECT\r\n"
			+ "    id,\r\n"
			+ "    first_name,\r\n"
			+ "    last_name,\r\n"
			+ "    (a - (b + c) - ((e + f) - d)) as w\r\n"
			+ "FROM subquery\r\n"
			+ "WHERE (a - (b + c) - ((e + f) - d)) > 0\r\n"
			+ "ORDER BY id ASC", nativeQuery = true)
	public List<Object[]> findingListOfUsersWalletGreaterThanZero();

	@Query(value ="WITH subquery AS (\r\n"
			+ "   SELECT\r\n"
			+ "        u.id,\r\n"
			+ "        pd.first_name,\r\n"
			+ "        pd.last_name,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT CAST(SUM(COALESCE(A.transaction_amount, 0)) AS DOUBLE PRECISION)\r\n"
			+ "            FROM public.lender_scrow_wallet A\r\n"
			+ "            WHERE u.id = A.user_id AND A.status = 'APPROVED' AND transactiontype = 'credit'\r\n"
			+ "        ), 0) as a,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT CAST(SUM(COALESCE(A.transaction_amount, 0)) AS DOUBLE PRECISION)\r\n"
			+ "            FROM public.lender_scrow_wallet A\r\n"
			+ "            WHERE u.id = A.user_id AND A.status = 'APPROVED' AND transactiontype = 'debit'\r\n"
			+ "        ), 0) as b,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(ol.disbursment_amount, 0))\r\n"
			+ "            FROM public.oxy_loan ol\r\n"
			+ "            WHERE u.id = ol.lender_user_id\r\n"
			+ "            AND ol.loan_status IN ('Active', 'Agreed', 'Closed')\r\n"
			+ "            AND date_trunc('day', ol.loan_accepted_date) >= '2020-02-14'\r\n"
			+ "        ), 0) as c,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(O.disbursment_amount, 0))\r\n"
			+ "            FROM public.oxy_loan O\r\n"
			+ "            WHERE u.id = O.lender_user_id\r\n"
			+ "            AND (O.loan_status = 'Agreed' OR O.loan_status = 'Active' OR O.loan_status = 'Closed')\r\n"
			+ "            AND O.deal_id > 0\r\n"
			+ "        ), 0) as d,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(a.participated_amount, 0))\r\n"
			+ "            FROM public.oxy_lenders_accepted_deals a\r\n"
			+ "            JOIN public.oxy_borrowers_deals_information b ON a.deal_id = b.id\r\n"
			+ "            WHERE u.id = a.user_id\r\n"
			+ "        ), 0) as e,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(c.updation_amount, 0))\r\n"
			+ "            FROM public.lenders_paticipation_updation c\r\n"
			+ "            JOIN public.oxy_borrowers_deals_information b ON c.deal_id = b.id\r\n"
			+ "            WHERE u.id = c.user_id\r\n"
			+ "        ), 0) as f\r\n"
			+ "    FROM public.user u\r\n"
			+ "    INNER JOIN public.user_personal_details pd ON pd.user_id = u.id\r\n"
			+ "    WHERE u.primary_type = 'LENDER' and u.test_user=false\r\n"
			+ ")\r\n"
			+ "SELECT\r\n"
			+ "    id,\r\n"
			+ "    first_name,\r\n"
			+ "    last_name,\r\n"
			+ "    (a - (b + c) - ((e + f) - d)) as w\r\n"
			+ "FROM subquery\r\n"
			+ "WHERE (a - (b + c) - ((e + f) - d)) = 0\r\n"
			+ "ORDER BY id ASC;", nativeQuery = true)
	public List<Object[]> findingListOfUsersWalletEqualsZero();

	@Query(value ="WITH subquery AS (\r\n"
			+ "   SELECT\r\n"
			+ "        u.id,\r\n"
			+ "        pd.first_name,\r\n"
			+ "        pd.last_name,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT CAST(SUM(COALESCE(A.transaction_amount, 0)) AS DOUBLE PRECISION)\r\n"
			+ "            FROM public.lender_scrow_wallet A\r\n"
			+ "            WHERE u.id = A.user_id AND A.status = 'APPROVED' AND transactiontype = 'credit'\r\n"
			+ "        ), 0) as a,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT CAST(SUM(COALESCE(A.transaction_amount, 0)) AS DOUBLE PRECISION)\r\n"
			+ "            FROM public.lender_scrow_wallet A\r\n"
			+ "            WHERE u.id = A.user_id AND A.status = 'APPROVED' AND transactiontype = 'debit'\r\n"
			+ "        ), 0) as b,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(ol.disbursment_amount, 0))\r\n"
			+ "            FROM public.oxy_loan ol\r\n"
			+ "            WHERE u.id = ol.lender_user_id\r\n"
			+ "            AND ol.loan_status IN ('Active', 'Agreed', 'Closed')\r\n"
			+ "            AND date_trunc('day', ol.loan_accepted_date) >= '2020-02-14'\r\n"
			+ "        ), 0) as c,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(O.disbursment_amount, 0))\r\n"
			+ "            FROM public.oxy_loan O\r\n"
			+ "            WHERE u.id = O.lender_user_id\r\n"
			+ "            AND (O.loan_status = 'Agreed' OR O.loan_status = 'Active' OR O.loan_status = 'Closed')\r\n"
			+ "            AND O.deal_id > 0\r\n"
			+ "        ), 0) as d,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(a.participated_amount, 0))\r\n"
			+ "            FROM public.oxy_lenders_accepted_deals a\r\n"
			+ "            JOIN public.oxy_borrowers_deals_information b ON a.deal_id = b.id\r\n"
			+ "            WHERE u.id = a.user_id\r\n"
			+ "        ), 0) as e,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(c.updation_amount, 0))\r\n"
			+ "            FROM public.lenders_paticipation_updation c\r\n"
			+ "            JOIN public.oxy_borrowers_deals_information b ON c.deal_id = b.id\r\n"
			+ "            WHERE u.id = c.user_id\r\n"
			+ "        ), 0) as f\r\n"
			+ "    FROM public.user u\r\n"
			+ "    INNER JOIN public.user_personal_details pd ON pd.user_id = u.id\r\n"
			+ "    WHERE u.primary_type = 'LENDER' and u.test_user=false\r\n"
			+ ")\r\n"
			+ "SELECT\r\n"
			+ "    id,\r\n"
			+ "    first_name,\r\n"
			+ "    last_name,\r\n"
			+ "    (a - (b + c) - ((e + f) - d)) as w\r\n"
			+ "FROM subquery\r\n"
			+ "WHERE (a - (b + c) - ((e + f) - d)) < 0\r\n"
			+ "ORDER BY id ASC;", nativeQuery = true)
	public List<Object[]> findingListOfUsersWalletLessThanZero();

	@Query(value = "select distinct a.id from public.user a join public.oxy_lenders_accepted_deals b on a.id=b.user_id where a.primary_type='LENDER' and a.test_user!=true order by a.id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Integer> getListOfRealUsers(@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "select count(id) from public.user where primary_type='LENDER' and test_user=false and date(registered_on)=current_date", nativeQuery = true)

	public Integer findByUserRegistrationCount();

	@Query(value = "select count(id) from public.user where primary_type='LENDER' and test_user=false and  registered_on >= CURRENT_DATE - INTERVAL '7 days'", nativeQuery = true)

	public Integer findByUserRegistrationWeeklyCount();

	@Query(value = "SELECT * FROM public.user where test_user=false and  primary_type='LENDER' and date(registered_on)>=:startDate and date(registered_on)<=:endDate", nativeQuery = true)

	List<User> findByUserId(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT COUNT(id) FROM public.user where  primary_type='LENDER' and  test_user=false and date(registered_on)>=:startDate and date(registered_on)<=:endDate", nativeQuery = true)

	public Integer findByUserRegistrationMonthlyCountWithInput(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT count(id) FROM public.user WHERE  primary_type='LENDER' AND test_user=false AND  date_trunc('month', registered_on) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)

	public Integer findByUserRegistrationMonthlyCount();

	@Query(value ="WITH subquery AS (\r\n"
			+ "    SELECT\r\n"
			+ "        u.id,\r\n"
			+ "        pd.first_name,\r\n"
			+ "        pd.last_name,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT CAST(SUM(COALESCE(A.transaction_amount, 0)) AS DOUBLE PRECISION)\r\n"
			+ "            FROM public.lender_scrow_wallet A\r\n"
			+ "            WHERE u.id = A.user_id AND A.status = 'APPROVED' AND transactiontype = 'credit'\r\n"
			+ "        ), 0) as a,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT CAST(SUM(COALESCE(A.transaction_amount, 0)) AS DOUBLE PRECISION)\r\n"
			+ "            FROM public.lender_scrow_wallet A\r\n"
			+ "            WHERE u.id = A.user_id AND A.status = 'APPROVED' AND transactiontype = 'debit'\r\n"
			+ "        ), 0) as b,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(ol.disbursment_amount, 0))\r\n"
			+ "            FROM public.oxy_loan ol\r\n"
			+ "            WHERE u.id = ol.lender_user_id\r\n"
			+ "            AND ol.loan_status IN ('Active', 'Agreed', 'Closed')\r\n"
			+ "            AND date_trunc('day', ol.loan_accepted_date) >= '2020-02-14'\r\n"
			+ "        ), 0) as c,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(O.disbursment_amount, 0))\r\n"
			+ "            FROM public.oxy_loan O\r\n"
			+ "            WHERE u.id = O.lender_user_id\r\n"
			+ "            AND (O.loan_status = 'Agreed' OR O.loan_status = 'Active' OR O.loan_status = 'Closed')\r\n"
			+ "            AND O.deal_id > 0\r\n"
			+ "        ), 0) as d,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(a.participated_amount, 0))\r\n"
			+ "            FROM public.oxy_lenders_accepted_deals a\r\n"
			+ "            JOIN public.oxy_borrowers_deals_information b ON a.deal_id = b.id\r\n"
			+ "            WHERE u.id = a.user_id\r\n"
			+ "        ), 0) as e,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(c.updation_amount, 0))\r\n"
			+ "            FROM public.lenders_paticipation_updation c\r\n"
			+ "            JOIN public.oxy_borrowers_deals_information b ON c.deal_id = b.id\r\n"
			+ "            WHERE u.id = c.user_id\r\n"
			+ "        ), 0) as f\r\n"
			+ "    FROM public.user u\r\n"
			+ "    INNER JOIN public.user_personal_details pd ON pd.user_id = u.id\r\n"
			+ "    WHERE u.id =:id\r\n"
			+ ")\r\n"
			+ "SELECT\r\n"
			+ "    (a - (b + c) - ((e + f) - d)) as w\r\n"
			+ "FROM subquery\r\n"
			+ "WHERE (a - (b + c) - ((e + f) - d)) > 0;", nativeQuery = true)
	public Double userWalletAmount(@Param("id") Integer id);

	@Query(value = "SELECT count(id) FROM public.user where primary_type='LENDER' and test_user=false", nativeQuery = true)
	public Integer findByTotalLenderRegistration();
	
	@Query(value = "SELECT count(id) FROM public.user where primary_type='BORROWER' and test_user=false", nativeQuery = true)

	public Integer findByTotalBorrowerRegistrationAcount();
	
	@Query(value = "SELECT * FROM public.user where id =:id and test_user=false", nativeQuery = true)
	public User findByUserId(@Param("id") Integer userId);

	@Query(value = "select count(id) from public.user where primary_type='LENDER' and test_user=false and date(registered_on)=current_date", nativeQuery = true)

	public Integer findByLenderRegistrationToday();
	@Query(value = "select count(id) from public.user where primary_type='BORROWER' and test_user=false and date(registered_on)=current_date", nativeQuery = true)

	public Integer findByBorrowerRegistrationToday();

	@Query(value = "select count(id) from public.user where primary_type='LENDER' and test_user=false and date(registered_on)>=:startDate and date(registered_on)<=:endDate", nativeQuery = true)

	public Integer findByLenderRegistrationAdminModule(@Param("startDate") Date startDate,@Param("endDate") Date endDate);
	
	
	@Query(value = "select count(id) from public.user where primary_type='BORROWER' and test_user=false and date(registered_on)>=:startDate and date(registered_on)<=:endDate", nativeQuery = true)

	public Integer findByBorrowerRegistrationAdminModule(@Param("startDate") Date startDate,@Param("endDate") Date endDate);

	@Query(value = "select * from public.user where id=:referrerId", nativeQuery = true)
	public User findByIdNum4(@Param("referrerId") Integer referrerId);

	@Query(value = "select * from public.user where id=:referrer", nativeQuery = true)
	public User findByIdNum3(@Param("referrer") Integer referrer);
	
    @Query(value = "select * from public.user where id=:refereeId", nativeQuery = true)
	public User findByIdNum5(@Param("refereeId") int refereeId);

    @Query(value = "select * from public.user where id=:refereId", nativeQuery = true)
	public User findByIdNum6(@Param("refereId") Integer refereId);

	@Query(value = "SELECT u.*\r\n"
			+ "FROM \"user\" u\r\n"
			+ "INNER JOIN user_personal_details pd ON u.id = pd.user_id\r\n"
			+ "WHERE pd.whatsapp_number =:whatsappNumber AND u.whastapp_verified = 'true'", nativeQuery = true)
	public List<User> findByPersonalDetailsWhatsAppNumber(@Param("whatsappNumber") String whatsappNumber);

	
	@Query(value = "select * from public.user where id=:userId ", nativeQuery = true)

	public User findByUser(@Param("userId") Integer userId);
	
	@Query(value = "select * from public.user where primary_type='LENDER' and test_user=false", nativeQuery = true)

	public List<User> findByTotalUsers();

	@Query(value ="WITH subquery AS (\r\n"
			+ "    SELECT\r\n"
			+ "        u.id,\r\n"
			+ "        pd.first_name,\r\n"
			+ "        pd.last_name,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT CAST(SUM(COALESCE(A.transaction_amount, 0)) AS DOUBLE PRECISION)\r\n"
			+ "            FROM public.lender_scrow_wallet A\r\n"
			+ "            WHERE u.id = A.user_id AND A.status = 'APPROVED' AND transactiontype = 'credit'\r\n"
			+ "        ), 0) as a,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT CAST(SUM(COALESCE(A.transaction_amount, 0)) AS DOUBLE PRECISION)\r\n"
			+ "            FROM public.lender_scrow_wallet A\r\n"
			+ "            WHERE u.id = A.user_id AND A.status = 'APPROVED' AND transactiontype = 'debit'\r\n"
			+ "        ), 0) as b,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(ol.disbursment_amount, 0))\r\n"
			+ "            FROM public.oxy_loan ol\r\n"
			+ "            WHERE u.id = ol.lender_user_id\r\n"
			+ "            AND ol.loan_status IN ('Active', 'Agreed', 'Closed')\r\n"
			+ "            AND date_trunc('day', ol.loan_accepted_date) >= '2020-02-14'\r\n"
			+ "        ), 0) as c,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(O.disbursment_amount, 0))\r\n"
			+ "            FROM public.oxy_loan O\r\n"
			+ "            WHERE u.id = O.lender_user_id\r\n"
			+ "            AND (O.loan_status = 'Agreed' OR O.loan_status = 'Active' OR O.loan_status = 'Closed')\r\n"
			+ "            AND O.deal_id > 0\r\n"
			+ "        ), 0) as d,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(a.participated_amount, 0))\r\n"
			+ "            FROM public.oxy_lenders_accepted_deals a\r\n"
			+ "            JOIN public.oxy_borrowers_deals_information b ON a.deal_id = b.id\r\n"
			+ "            WHERE u.id = a.user_id\r\n"
			+ "        ), 0) as e,\r\n"
			+ "        COALESCE((\r\n"
			+ "            SELECT SUM(COALESCE(c.updation_amount, 0))\r\n"
			+ "            FROM public.lenders_paticipation_updation c\r\n"
			+ "            JOIN public.oxy_borrowers_deals_information b ON c.deal_id = b.id\r\n"
			+ "            WHERE u.id = c.user_id\r\n"
			+ "        ), 0) as f\r\n"
			+ "    FROM public.user u\r\n"
			+ "    INNER JOIN public.user_personal_details pd ON pd.user_id = u.id\r\n"
			+ "    WHERE u.id =:userId\r\n"
			+ ")\r\n"
			+ "SELECT\r\n"
			+ "    (a - (b + c) - ((e + f) - d)) as w\r\n"
			+ "FROM subquery\r\n"
			+ "WHERE (a - (b + c) - ((e + f) - d)) > 0;", nativeQuery = true)
	public Double userWalletAmount1(@Param("userId") Integer userId);

	@Query(value = "select * from public.user where id=:userId ", nativeQuery = true)

	public User findByBorrowerFd(@Param("userId") Integer userId);
	
}
