package com.oxyloans.repository.socialloginrepo;

import com.oxyloans.entity.socialLogin.SocialLoginDetails;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SocialLoginDetailsRepo extends PagingAndSortingRepository<SocialLoginDetails, Integer> {

	@Query(value = "select * from public.social_login_details where google_user_id=:userId", nativeQuery = true)
	public SocialLoginDetails findByGoogleUserId(@Param("userId") String userId);

	@Query(value = "select * from public.social_login_details where facebook_user_id=:facebookUserId", nativeQuery = true)
	public SocialLoginDetails findbyFacebookUserId(@Param("facebookUserId") String facebookUserId);

	@Query(value = "select * from public.social_login_details where google_user_id=:googleUserId and user_id=:id ", nativeQuery = true)
	public SocialLoginDetails findByGoogleUserIdAndUserId(@Param("googleUserId") String googleUserId,
			@Param("id") Integer id);

	@Query(value = "select * from public.social_login_details where facebook_user_id=:facebookUserId and user_id=:id ", nativeQuery = true)
	public SocialLoginDetails findByFacebookUserIdAndUserId(@Param("facebookUserId") String facebookUserId,
			@Param("id") Integer id);

	@Query(value = "select * from public.social_login_details where user_id=:id ", nativeQuery = true)
	public SocialLoginDetails findByUserId(@Param("id") Integer id);

}
