package com.oxyloans.repository.iciciupirepo;

import java.util.List;

import com.oxyloans.entity.iciciupi.OxyLoanTransactionAlerts;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface OxyLoanTransactionAlertsRepo extends PagingAndSortingRepository<OxyLoanTransactionAlerts, Integer>,
		JpaSpecificationExecutor<OxyLoanTransactionAlerts> {

	@Query(value = "SELECT * FROM public.oxy_loan_transaction_alerts where transaction_initiated_date=:transactionDate and bank_transaction_id=:bankId and transaction_completed_date=:valueDate and amount=:amount and transaction_type=:transactionType", nativeQuery = true)
	public OxyLoanTransactionAlerts checkTransactionAlertAlreadyUpdate(@Param("transactionDate") String transactionDate,
			@Param("bankId") String bankId, @Param("valueDate") String valueDate, @Param("amount") String amount,
			@Param("transactionType") String transactionType);

	@Query(value = "select sum(CAST(amount AS DOUBLE precision)) from public.oxy_loan_transaction_alerts where transaction_type=:transactionType", nativeQuery = true)
	public Double findTotalTransactionAmountBasedOnTransactionType(@Param("transactionType") String transactionType);

	@Query(value = "select sum(CAST(amount AS DOUBLE precision)) from public.oxy_loan_transaction_alerts where transaction_type=:transactionType and  \r\n"
			+ "to_char(to_date(transaction_completed_date, 'dd/MM-YYYY'),'mm/yyyy')=:monthAndYear", nativeQuery = true)
	public Double transactionAmountBasedOnTransactionTypeinCurrentMonth(@Param("monthAndYear") String monthAndYear,
			@Param("transactionType") String transactionType);

	@Query(value = "select * from public.oxy_loan_transaction_alerts where transaction_type=:transactionType and  \r\n"
			+ "to_char(to_date(transaction_completed_date, 'dd/MM/YYYY'),'mm/yyyy')=:monthAndYear", nativeQuery = true)
	public List<OxyLoanTransactionAlerts> findTransactionsBasedOnTransactionTypeinCurrentMonth(
			@Param("monthAndYear") String monthAndYear, @Param("transactionType") String transactionType);

	@Query(value = "select * from public.oxy_loan_transaction_alerts where  transaction_type=:transactionType and account_number=:accountNumber and\r\n"
			+ "  to_date(transaction_completed_date,'dd/MM/yyyy')>=to_date(:fromDate,'dd/MM/yyyy') and to_date(transaction_completed_date,'dd/MM/yyyy')<=to_date(:toDate,'dd/MM/yyyy') ", nativeQuery = true)
	public List<OxyLoanTransactionAlerts> findTransactionsBasedOnTransactionTypeinGivenDates(
			@Param("fromDate") String fromDate, @Param("toDate") String toDate,
			@Param("transactionType") String transactionType, @Param("accountNumber") String accountNumber);

	@Query(value = "select sum(CAST(amount AS DOUBLE precision)) from public.oxy_loan_transaction_alerts where transaction_type=:transactionType and account_number=:accountNumber \r\n"
			+ " and to_date(transaction_completed_date,'dd/MM/yyyy')>=to_date(:fromDate,'dd/MM/yyyy') and to_date(transaction_completed_date,'dd/MM/yyyy')<=to_date(:toDate,'dd/MM/yyyy') ", nativeQuery = true)
	public Double transactionAmountBasedOnTransactionTypeinGivenDates(@Param("fromDate") String fromDate,
			@Param("toDate") String toDate, @Param("transactionType") String transactionType,
			@Param("accountNumber") String accountNumber);

	@Query(value = "select sum(CAST(amount AS DOUBLE precision)) from public.oxy_loan_transaction_alerts where transaction_type=:transactionType and account_number=:accountNumber ", nativeQuery = true)
	public Double transactionAmountBasedOnTransactionTypeinGivenDates(@Param("transactionType") String transactionType,
			@Param("accountNumber") String accountNumber);

	@Query(value = "select sum(CAST(amount AS DOUBLE precision)) from public.oxy_loan_transaction_alerts where transaction_type=:transactionType and account_number=:accountNumber \r\n"
			+ " and to_date(transaction_completed_date,'dd/MM/yyyy')=to_date(:currentDate,'dd/MM/yyyy') ", nativeQuery = true)
	public Double getCurrentDayTotalTransaction(@Param("currentDate") String currentDate,
			@Param("transactionType") String transactionType, @Param("accountNumber") String accountNumber);
}
