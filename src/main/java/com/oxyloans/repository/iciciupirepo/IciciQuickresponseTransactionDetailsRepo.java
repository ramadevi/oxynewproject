package com.oxyloans.repository.iciciupirepo;

import java.util.List;

import com.oxyloans.entity.iciciupi.IciciQuickresponseTransactionDetails;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface IciciQuickresponseTransactionDetailsRepo
		extends PagingAndSortingRepository<IciciQuickresponseTransactionDetails, Integer>,
		JpaSpecificationExecutor<IciciQuickresponseTransactionDetails> {

	@Query(value = "SELECT * FROM public.icici_quickresponse_transaction_details where merchant_transaction_id=:merchantTranId", nativeQuery = true)
	public IciciQuickresponseTransactionDetails getTransactionDetailsByMerchantId(
			@Param("merchantTranId") String merchantTranId);

	@Query(value = "SELECT * FROM public.icici_quickresponse_transaction_details where upper(ref_id)=upper(:refId) order by id desc limit 1", nativeQuery = true)
	public IciciQuickresponseTransactionDetails getTransactionDetailsByRefId(@Param("refId") String refId);

	@Query(value = "SELECT * FROM public.icici_quickresponse_transaction_details where user_id=:userId and transaction_status='SUCCESS' limit :pageSize offset :pageNo", nativeQuery = true)
	public List<IciciQuickresponseTransactionDetails> listOfSuccessTransactions(@Param("userId") Integer userId,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "SELECT count(id) FROM public.icici_quickresponse_transaction_details where user_id=:userId and transaction_status='SUCCESS'", nativeQuery = true)
	public Integer listOfSuccessTransactionsCount(@Param("userId") Integer userId);

}
