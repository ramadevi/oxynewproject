package com.oxyloans.repository.iciciupirepo;

import com.oxyloans.entity.iciciupi.ICICIUpiTransactionDetails;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ICICIUpiTransactionDetailsRepo extends PagingAndSortingRepository<ICICIUpiTransactionDetails, Integer>,
		JpaSpecificationExecutor<ICICIUpiTransactionDetails> {

	@Query(value = "SELECT * FROM public.icici_upi_transaction_details where merchant_tran_id=:merchantTranId", nativeQuery = true)
	public ICICIUpiTransactionDetails getTransactionDetailsByMerchantId(@Param("merchantTranId") String merchantTranId);

	@Query(value = "SELECT * FROM public.icici_upi_transaction_details where merchant_tran_id=:merchantTranId and bank_rrn=:bankRrn", nativeQuery = true)
	public ICICIUpiTransactionDetails getTransactionDetailsByMerchantId(@Param("merchantTranId") String merchantTranId,
			@Param("bankRrn") String bankRrn);

}
