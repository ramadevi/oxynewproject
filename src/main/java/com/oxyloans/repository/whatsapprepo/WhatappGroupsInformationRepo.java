package com.oxyloans.repository.whatsapprepo;

import java.util.List;

import com.oxyloans.entity.whatapp.WhatappGroupsInformation;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WhatappGroupsInformationRepo extends PagingAndSortingRepository<WhatappGroupsInformation, Integer>,
		JpaSpecificationExecutor<WhatappGroupsInformation> {

	@Query(value = "SELECT chat_id,group_name FROM public.whatapp_groups_information", nativeQuery = true)
	public List<Object[]> getListOfWhatappGroupDetails();

	@Query(value = "SELECT * FROM public.whatapp_groups_information", nativeQuery = true)
	public List<WhatappGroupsInformation> getListOfWhatappGroupNames();

	@Query(value = "SELECT count(id) FROM public.whatapp_groups_information", nativeQuery = true)
	public Integer getListOfWhatappGroupNamesCount();

	@Query(value = "SELECT chat_id FROM public.whatapp_groups_information where group_name=:groupName", nativeQuery = true)
	public String getChatid(@Param("groupName") String groupName);

	@Query(value = "select group_name from public.whatapp_groups_information where upper(group_name) like %:groupName% or\r\n"
			+ "lower(group_name) like %:groupName%", nativeQuery = true)
	public List<String> findingGroupNamesBasedOnGroupName(@Param("groupName") String groupName);

	@Query(value = "SELECT count(id) FROM public.whatapp_groups_information where chat_type=:chatType", nativeQuery = true)
	public Integer getListOfNamesCountBasedOnChatType(@Param("chatType") String chatType);

	@Query(value = "SELECT * FROM public.whatapp_groups_information where chat_type=:chatType", nativeQuery = true)
	public List<WhatappGroupsInformation> getListOfNamesBasedOnChatType(@Param("chatType") String chatType);

}
