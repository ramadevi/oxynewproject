package com.oxyloans.repository.whatsapprepo;

import com.oxyloans.entity.whatapp.OxyFreeWhatsapp;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OxyFreeWhatsappRepo
		extends PagingAndSortingRepository<OxyFreeWhatsapp, Integer>, JpaSpecificationExecutor<OxyFreeWhatsapp> {

	@Query(value = "select * from public.oxy_free_whatsapp where mobile_number=:mobileNumber", nativeQuery = true)
	public OxyFreeWhatsapp findbyMobileNumber(String mobileNumber);

}
