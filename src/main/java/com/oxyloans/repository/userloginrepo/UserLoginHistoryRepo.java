package com.oxyloans.repository.userloginrepo;

import com.oxyloans.entity.userlogin.UserLoginHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserLoginHistoryRepo extends JpaRepository<UserLoginHistory, Integer> {

}
