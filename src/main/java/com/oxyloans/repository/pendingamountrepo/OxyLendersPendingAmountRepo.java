package com.oxyloans.repository.pendingamountrepo;

import com.oxyloans.entity.pendingamount.OxyLendersPendingAmount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.oxyloans.entity.pendingamount.OxyLendersPendingAmount.Status;

public interface OxyLendersPendingAmountRepo extends PagingAndSortingRepository<OxyLendersPendingAmount, Integer>,
		JpaSpecificationExecutor<OxyLendersPendingAmount> {

	public Page<OxyLendersPendingAmount> findByStatusOrderByCreatedDateDesc(Status created, Pageable pageable);
}
