package com.oxyloans.repository.poolingaccountrepo;

import java.util.List;

import com.oxyloans.entity.poolingAccount.OxyPoolingAccountDetails;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface OxyPoolingAccountDetailsRepo extends PagingAndSortingRepository<OxyPoolingAccountDetails, Integer>,
		JpaSpecificationExecutor<OxyPoolingAccountDetails> {

	@Query(value = "SELECT * FROM public.oxy_pooling_account_details order by id asc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<OxyPoolingAccountDetails> totalPollingLenders(@Param("pageSize") Integer pageSize,
			@Param("pageNo") Integer pageNo);

	@Query(value = "SELECT count(id) FROM public.oxy_pooling_account_details", nativeQuery = true)
	public Integer totalPollingCount();

	@Query(value = "SELECT sum(current_amount) FROM public.oxy_pooling_account_details", nativeQuery = true)
	public Double sumOfCurrentValue();

	@Query(value = "SELECT sum(interest_amount)  FROM public.oxy_pooling_account_details", nativeQuery = true)
	public Double sumOfMonthlyInterestValue();

}
