package com.oxyloans.repository.expertseekers;

import java.util.List;

import com.oxyloans.entity.expertSeekers.ExpertAdviseSeekers;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ExpertAdviseSeekersRepo extends PagingAndSortingRepository<ExpertAdviseSeekers, Integer> {

	@Query(value = "select * from public.expert_advise_seekers where mobile_number=:mobileNumber and lender_reference_id is null", nativeQuery = true)
	public ExpertAdviseSeekers findbyMobileNumberAndLenderReferenceId(@Param("mobileNumber") String mobileNumber);
	
	
	@Query(value = "select * from public.expert_advise_seekers where mobile_number=:mobileNumber", nativeQuery = true)
	public ExpertAdviseSeekers findbyMobileNumber(@Param("mobileNumber") String mobileNumber);

	@Query(value = "select * from public.expert_advise_seekers where mail_id=:mailId", nativeQuery = true)
	public ExpertAdviseSeekers findbyEmail(@Param("mailId") String  mailId);


	@Query(value = "select * from public.expert_advise_seekers where advisor_id=:userId", nativeQuery = true)
	public List<ExpertAdviseSeekers> findByAdvisorId(@Param("userId") int userId);

	
	
}
