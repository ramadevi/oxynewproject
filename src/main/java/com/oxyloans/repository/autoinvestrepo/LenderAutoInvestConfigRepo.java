package com.oxyloans.repository.autoinvestrepo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.autoinvest.entity.LenderAutoInvestConfig;

public interface LenderAutoInvestConfigRepo extends PagingAndSortingRepository<LenderAutoInvestConfig, Integer>{

	public LenderAutoInvestConfig findByUserId(Integer userId);
	
	@Query(value = "SELECT * FROM public.lender_auto_invest_config where user_id=:userId and date(created_on)>='2023-06-26'", nativeQuery = true)
	public LenderAutoInvestConfig autoLenderAlreadyEnabled(@Param("userId") Integer userId);

	
	//@Query(value = "select * FROM public.lender_auto_invest_config where status='ACTIVE' and date(created_on)>='2023-06-26' and (deal_type=:dealType or deal_type='BOTH')", nativeQuery = true)
	
	@Query(value ="SELECT * \r\n" + "FROM public.user AS u\r\n" +
			  "JOIN public.lender_auto_invest_config AS b ON u.id = b.user_id\r\n" +
			  "WHERE u.primary_type = 'LENDER' \r\n" + "    AND u.test_user = 'true' \r\n"
			  + "    AND b.status = 'ACTIVE' \r\n" +
			  "    AND DATE(b.created_on) >= '2023-01-05' \r\n" +
			  "    AND (b.deal_type =:dealType OR b.deal_type = 'BOTH')\r\n" + ""
			  ,nativeQuery =true)
	public List<LenderAutoInvestConfig> listOfLendersAutoLendingEnabled(@Param("dealType") String dealType);

	
	@Query(value = "SELECT * FROM public.lender_auto_invest_config where status ='ACTIVE'", nativeQuery = true)
	public List<LenderAutoInvestConfig> autoLendingEnableUsers();

	@Query(value = "SELECT Count(user_id) FROM public.lender_auto_invest_config where status ='ACTIVE'", nativeQuery = true)
	public Integer autoLenderAlreadyEnabled1();
	
}
