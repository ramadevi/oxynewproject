package com.oxyloans.repository.autoinvestrepo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.autoinvest.entity.LenderAutoInvestConfig;

public interface LenderAutoInvestConfigNativeRepo extends PagingAndSortingRepository<LenderAutoInvestConfig, Integer>{
	
	String sql = "SELECT * FROM Search_AutoInvestLenders(:firstName,:lastName,:userId,:pageSize,:pageNumber)";

	@Query(value = sql, nativeQuery = true)
	List<Object[]> loansByApplication(@Param("firstName") String firstName,@Param("lastName") String lastName,@Param("userId") Integer userId, @Param("pageSize") Integer pageSize,
			@Param("pageNumber") Integer pageNumber);
	
	String countRecords = "SELECT * FROM COUNT_Search_AutoInvestLenders(:firstName,:lastName,:userId,:pageSize,:pageNumber)";

	@Query(value = countRecords, nativeQuery = true)
	Integer countTranRecords(@Param("firstName") String firstName,@Param("lastName") String lastName,@Param("userId") Integer userId,@Param("pageSize") Integer pageSize,@Param("pageNumber") Integer pageNumber);
	
	
	String allAutoInvestLenders = "SELECT * FROM Get_All_AutoInvestLenders()";

	@Query(value = allAutoInvestLenders, nativeQuery = true)
	List<Object[]> allAutoInvestLenders();
	
	String allborrowerForAutoInvest = "SELECT * FROM Get_ALL_BORROWERS_TO_AUTOINVEST()";

	@Query(value = allborrowerForAutoInvest, nativeQuery = true)
	List<Object[]> allborrowerForAutoInvest();
	
	
	String getAgreedLoanIds = "SELECT A.loan_id from OXY_LOAN_REQUEST A " + 
			"JOIN oxy_loan B " + 
			"ON A.loan_id=B.loan_id " + 
			"where A.loanprocesstype='autoinvest' " + 
			"AND B.loan_status='Agreed' AND lender_esign_id is null";

	@Query(value = getAgreedLoanIds, nativeQuery = true)
	List<String> getAllAutoInvestAgreedLoanIds();
	
	
	

}
