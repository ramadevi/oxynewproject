package com.oxyloans.repository.autoinvestrepo;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oxyloans.autoinvest.entity.LenderAutoInvestConfigArchive;

public interface LenderAutoInvestConfigArchiveRepo extends PagingAndSortingRepository<LenderAutoInvestConfigArchive, Integer>{
	
	public List<LenderAutoInvestConfigArchive> findByUserIdOrderByCreatedOnDesc(Integer userId);

}
