package com.oxyloans.repository.borrowerpayments;

import com.oxyloans.entity.borrowerpayments.BorrowerRepaymentsHistory;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface BorrowerRepaymentsHistoryRepo extends PagingAndSortingRepository<BorrowerRepaymentsHistory, Integer>,
		JpaSpecificationExecutor<BorrowerRepaymentsHistory> {

	@Query(value = "SELECT * FROM public.borrower_repayments_history WHERE borrower_payments_id = :borrowerPaymentId ORDER BY created_on DESC LIMIT 1", nativeQuery = true)
	public BorrowerRepaymentsHistory findByBorrowerPaymentsId(@Param("borrowerPaymentId") Integer borrowerPaymentId);

}
