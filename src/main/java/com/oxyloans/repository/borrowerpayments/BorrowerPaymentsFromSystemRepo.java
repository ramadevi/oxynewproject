package com.oxyloans.repository.borrowerpayments;

import java.util.List;

import com.oxyloans.entity.borrowerpayments.BorrowerPaymentsFromSystem;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BorrowerPaymentsFromSystemRepo extends PagingAndSortingRepository<BorrowerPaymentsFromSystem, Integer>,
		JpaSpecificationExecutor<BorrowerPaymentsFromSystem> {

	public List<BorrowerPaymentsFromSystem> findByBorrowerPaymentsId(Integer id);

}
