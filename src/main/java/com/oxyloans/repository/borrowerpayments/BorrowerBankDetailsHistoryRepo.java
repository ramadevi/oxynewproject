package com.oxyloans.repository.borrowerpayments;

import com.oxyloans.entity.borrowerpayments.BorrowerBankDetailsHistory;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BorrowerBankDetailsHistoryRepo extends PagingAndSortingRepository<BorrowerBankDetailsHistory, Integer>,
		JpaSpecificationExecutor<BorrowerBankDetailsHistory> {

}
