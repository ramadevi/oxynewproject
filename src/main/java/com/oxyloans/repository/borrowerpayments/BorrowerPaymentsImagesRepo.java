package com.oxyloans.repository.borrowerpayments;

import java.util.Date;
import java.util.List;

import com.oxyloans.entity.borrowerpayments.BorrowerPaymentsImages;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.oxyloans.entity.borrowerpayments.BorrowerPaymentsImages.DocumentStatus;

public interface BorrowerPaymentsImagesRepo extends PagingAndSortingRepository<BorrowerPaymentsImages, Integer>,
		JpaSpecificationExecutor<BorrowerPaymentsImages> {

	@Query(value = "SELECT * FROM public.borrower_payments_images where document_status=:status and account_type=:bankType order by created_on desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<BorrowerPaymentsImages> listOfUsersByBankTypeAndStatus(String bankType, String status, int pageNo,
			int pageSize);

	@Query(value = "SELECT count(id) FROM public.borrower_payments_images where document_status=:status and account_type=:bankType", nativeQuery = true)
	public Integer usersByBankTypeAndStatusCount(String bankType, String status);

	public List<BorrowerPaymentsImages> findByBorrowerPaymentsIdOrderByIdDesc(Integer id);

	public List<BorrowerPaymentsImages> findByDocumentStatusEquals(DocumentStatus approved);

	public List<BorrowerPaymentsImages> findByInvoiceUrlNotNull();

	public List<BorrowerPaymentsImages> findByInvoiceUrlNotNullAndCreatedOnBetween(Date startDate, Date endDate);

}
