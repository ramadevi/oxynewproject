package com.oxyloans.repository.borrowerpayments;

import java.util.Date;
import java.util.List;

import com.oxyloans.entity.borrowerpayments.BorrowerPayments;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.borrowerpayments.BorrowerPayments.PayStatus;

public interface BorrowerPaymentsRepo
		extends PagingAndSortingRepository<BorrowerPayments, Integer>, JpaSpecificationExecutor<BorrowerPayments> {

	@Query(value = "SELECT sum(amount) FROM public.borrower_payments where borrower_payment_type=:amountType and  to_char(payment_date,'dd-mm-yyyy')=:date ", nativeQuery = true)
	public Double getSumOfAmountInGivenDate(@Param("date") String date, @Param("amountType") String amountType);

	@Query(value = "SELECT * FROM public.borrower_payments where user_id=:userId and (fd_status='CREATED' OR fd_status='SAVED')", nativeQuery = true)
	public BorrowerPayments findByUserId(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.borrower_payments where user_id=:userId limit 1", nativeQuery = true)
	public BorrowerPayments findByUserIdWithLimit(@Param("userId") Integer userId);

	public List<BorrowerPayments> findByDealIdAndPayStatus(Integer dealId, PayStatus status);

	@Query(value = "SELECT * FROM public.borrower_payments where LENGTH(account_number)>5 order by fd_created desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<BorrowerPayments> getListOfNewBorrowersBankDetails(Integer pageNo, Integer pageSize);

	@Query(value = "SELECT count(id) FROM public.borrower_payments where LENGTH(account_number)>5", nativeQuery = true)
	public Integer getListOfNewBorrowersBankDetailsCount();

	@Query(value = "SELECT * FROM public.borrower_payments where fd_created is not null order by fd_created desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<BorrowerPayments> getListOfFdCreatedUsers(Integer pageNo, Integer pageSize);

	@Query(value = "SELECT count(id) FROM public.borrower_payments where fd_created is not null", nativeQuery = true)
	public Integer getListOfFdCreatedUsersCount();

	@Query(value = "SELECT * FROM public.borrower_payments where fd_status='CLOSED' order by fd_created desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<BorrowerPayments> getListOfFdClosed(Integer pageNo, Integer pageSize);

	@Query(value = "SELECT count(id) FROM public.borrower_payments where fd_status='CLOSED'", nativeQuery = true)
	public Integer getListOfFdClosedCount();

	@Query(value = "select id, user_id, deal_id, payment_date, amount, remarks, borrower_payment_type, pay_status, account_number, ifsc, name, created_on, city, branch, bank_name, lead_by, consultancy, country, university, student_mobile_number, funding_type, roi, fd_amount, bank_details_verified_on, fd_created, fd_validity, bank_verified_status, per_day_interest, interest_earned_on_fd, fd_status, fd_closed_date, bank_choosen,days,payments_collection,loan_type from (SELECT id, user_id, deal_id, payment_date, amount, remarks, borrower_payment_type, pay_status, account_number, ifsc, name, created_on, city, branch, bank_name, lead_by, consultancy, country, university, student_mobile_number, funding_type, roi, fd_amount, bank_details_verified_on, fd_created, fd_validity, bank_verified_status, per_day_interest, interest_earned_on_fd, fd_status, fd_closed_date, bank_choosen,fd_validity-CURRENT_DATE as days,payments_collection,loan_type  FROM public.borrower_payments) as a1 where LENGTH(account_number)>5 and fd_status!='CLOSED' order by a1.days asc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> listOfRunningFds(Integer pageNo, Integer pageSize);

	@Query(value = "select count(id) from (SELECT *,fd_validity-CURRENT_DATE as days  FROM public.borrower_payments) as a1 where LENGTH(account_number)>5 and fd_status!='CLOSED'", nativeQuery = true)
	public Integer listOfRunningFdsCount();

	@Query(value = "select id, user_id, deal_id, payment_date, amount, remarks, borrower_payment_type, pay_status, account_number, ifsc, name, created_on, city, branch, bank_name, lead_by, consultancy, country, university, student_mobile_number, funding_type, roi, fd_amount, bank_details_verified_on, fd_created, fd_validity, bank_verified_status, per_day_interest, interest_earned_on_fd, fd_status, fd_closed_date, bank_choosen,payments_collection,loan_type from (SELECT id, user_id, deal_id, payment_date, amount, remarks, borrower_payment_type, pay_status, account_number, ifsc, name, created_on, city, branch, bank_name, lead_by, consultancy, country, university, student_mobile_number, funding_type, roi, fd_amount, bank_details_verified_on, fd_created, fd_validity, bank_verified_status, per_day_interest, interest_earned_on_fd, fd_status, fd_closed_date, bank_choosen,fd_validity-CURRENT_DATE as days,payments_collection,loan_type  FROM public.borrower_payments) as a1 where (a1.days<0 or a1.days is null) and fd_status!='CLOSED' and LENGTH(account_number)>5 order by a1.days asc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> listOfNegativeFds(Integer pageNo, Integer pageSize);

	@Query(value = "select count(id) from (SELECT *,fd_validity-CURRENT_DATE as days  FROM public.borrower_payments) as a1 where (a1.days<0 or a1.days is null) and fd_status!='CLOSED' and LENGTH(account_number)>5", nativeQuery = true)
	public Integer listOfNegativeFdsCount();

	@Query(value = "SELECT * FROM public.borrower_payments where user_id=:userId ORDER BY id asc limit 1", nativeQuery = true)
	public BorrowerPayments fdDetailsWithUserid(@Param("userId") Integer userId);

	public List<BorrowerPayments> findByFdCreatedIsNotNull();

	public List<BorrowerPayments> findByFdCreatedGreaterThanEqualAndFdCreatedLessThanEqual(Date startDate,
			Date endDate);

	@Query(value = "SELECT * FROM public.borrower_payments e where EXTRACT(YEAR FROM fd_created)=:year and EXTRACT(MONTH FROM fd_created)=:month", nativeQuery = true)
	public List<BorrowerPayments> findByFdCreatedYearAndMonth(@Param("year") int year, @Param("month") int month);

	@Query(value = "SELECT distinct bank_name FROM public.borrower_payments where length(bank_name)>2 order by bank_name desc", nativeQuery = true)
	public List<String> listOfBanks();

	@Query(value = "SELECT distinct consultancy FROM public.borrower_payments where length(consultancy)>2 order by consultancy desc", nativeQuery = true)
	public List<String> listOfConsultancy();

	public List<BorrowerPayments> findByFundingType(
			BorrowerPayments.FundingType fd);

	public List<BorrowerPayments> findByBankName(String bankName);

	public List<BorrowerPayments> findByConsultancy(String consultancy);

	@Query(value = "SELECT * FROM public.borrower_payments where (fd_status='CREATED' OR fd_status='SAVED') and user_id=:userId", nativeQuery = true)
	public BorrowerPayments findByUserIdAndStatusNotAtClosed(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.borrower_payments where fd_status !='CLOSED'", nativeQuery = true)
	public List<BorrowerPayments> getListOfActiveFdDetails();

	public List<BorrowerPayments> findByFdClosedDateGreaterThanEqualAndFdClosedDateLessThanEqual(Date startDate,
			Date endDate);

	@Query(value = "SELECT * FROM public.borrower_payments where user_id=:userId ORDER BY id asc limit 1", nativeQuery = true)
	public BorrowerPayments findLoanApplicationIdByUser(@Param("userId") Integer userId);

	
	@Query(value = "SELECT * \r\n"
			+ "FROM public.borrower_payments a JOIN public.user b ON a.user_id=b.id\r\n"
			+ "WHERE b.test_user=false and  a.funding_type = 'FD' \r\n"
			+ "  AND a.account_number IS NOT NULL \r\n"
			+ "  AND a.user_id > 0;", nativeQuery = true)

	public List<BorrowerPayments> findByBorrowerData();

}
