package com.oxyloans.repository.borrowerdealinformationrepo;

import java.math.BigInteger;
import java.util.List;

import com.oxyloans.entity.borrowersdealsinformation.LendersPaticipationUpdation;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LendersPaticipationUpdationRepo
		extends PagingAndSortingRepository<LendersPaticipationUpdation, Integer>,
		JpaSpecificationExecutor<LendersPaticipationUpdation> {

	@Query(value = "SELECT * FROM public.lenders_paticipation_updation where user_id=:userId and deal_id=:dealId and (to_char(updated_on,'MM/YYYY')<=:monthAndYear or  to_char(updated_on,'MM/YYYY')>=:monthAndYear) order by id desc", nativeQuery = true)
	public List<LendersPaticipationUpdation> getLenderAddedAreUpdate(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId, @Param("monthAndYear") String monthAndYear);

	@Query(value = "SELECT sum(updation_amount) FROM public.lenders_paticipation_updation where deal_id=:dealId", nativeQuery = true)
	public Double getDealUpdatedSumValue(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(updation_amount) FROM public.lenders_paticipation_updation where user_id=:userId", nativeQuery = true)
	public Double getSumOfLenderUpdatedAmount(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.lenders_paticipation_updation where user_id=:userId and deal_id=:dealId order by id asc", nativeQuery = true)
	public List<LendersPaticipationUpdation> getParticipationUpdatedAmounts(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(updation_amount) FROM public.lenders_paticipation_updation where user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public Double getSumOfLenderUpdatedAmountToDeal(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(a.updation_amount) FROM public.lenders_paticipation_updation a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and a.deal_type!='EQUITY' and b.borrower_closing_status='NOTYETCLOSED'", nativeQuery = true)
	public Double getSumOfLenderUpdatedAmountButNotInEquity(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(updation_amount) FROM public.lenders_paticipation_updation where user_id=:userId and deal_type='EQUITY'", nativeQuery = true)
	public Double getSumOfLenderUpdatedAmountInEquity(@Param("userId") Integer userId);

	@Query(value = "select sum(b.updation_amount) from public.user a join public.lenders_paticipation_updation b on a.id=b.user_id join public.oxy_borrowers_deals_information c on b.deal_id=c.id where a.lender_group_id=:lenderGroupId and c.borrower_closing_status='NOTYETCLOSED'", nativeQuery = true)
	public BigInteger sumOfParticipationUpdatedBasedOnLenderGroup(@Param("lenderGroupId") Integer lenderGroupId);

	@Query(value = "select sum(b.updation_amount) from public.user a join public.lenders_paticipation_updation b on a.id=b.user_id join public.oxy_borrowers_deals_information c on b.deal_id=c.id where a.lender_group_id=:lenderGroupId and c.borrower_closing_status='NOTYETCLOSED' and TO_CHAR(b.updated_on,'mm/yyyy')=:monthAndYear", nativeQuery = true)
	public BigInteger sumOfParticipatedUpdatedBasedOnMonthAndYear(@Param("lenderGroupId") Integer lenderGroupId,
			@Param("monthAndYear") String monthAndYear);

	@Query(value = "select sum(b.updation_amount) from public.user a join public.lenders_paticipation_updation b on a.id=b.user_id join public.oxy_borrowers_deals_information c on b.deal_id=c.id where a.lender_group_id=:lenderGroupId and c.borrower_closing_status='NOTYETCLOSED' and b.deal_id=:dealId", nativeQuery = true)
	public BigInteger sumOfParticipatedUpdatedBasedOnLenderGroupAndDealId(@Param("lenderGroupId") Integer lenderGroupId,
			@Param("dealId") Integer dealId);

	@Query(value = "select sum(b.updation_amount) from public.user a join public.lenders_paticipation_updation b on a.id=b.user_id join public.oxy_borrowers_deals_information c on b.deal_id=c.id where a.lender_group_id=:lenderGroupId and c.borrower_closing_status='NOTYETCLOSED' and TO_CHAR(b.updated_on,'mm/yyyy')=:monthAndYear and b.deal_id=:dealId", nativeQuery = true)
	public BigInteger sumOfParticipatedUpdatedBasedOnLenderGroupAndDealIdByMonthAndYear(
			@Param("lenderGroupId") Integer lenderGroupId, @Param("dealId") Integer dealId,
			@Param("monthAndYear") String monthAndYear);

	@Query(value = "SELECT sum(a.participated_amount+b.updation_amount),a.user_id,a.lender_returns_type,a.rateofinterest  FROM public.oxy_lenders_accepted_deals a join public.lenders_paticipation_updation b on a.user_id=b.user_id and a.deal_id=b.deal_id where a.deal_id=:dealId and a.fee_status='COMPLETED' and a.participation_state='NOTPARTICIPATED' GROUP BY a.user_id,a.lender_returns_type,a.rateofinterest", nativeQuery = true)
	public List<Object[]> getListOfLendersUpdatedInfo(@Param("dealId") Integer dealId);

	@Query(value = "select sum(a.participated_amount) FROM public.oxy_lenders_accepted_deals a join \r\n"
			+ "public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:refereeId and \r\n"
			+ "to_char(a.received_on, 'yyyy-mm-dd') >= '2021-08-04' and b.deal_type!='EQUITY'", nativeQuery = true)
	public Double findingSumOfParticipatedAmountForLender(@Param("refereeId") Integer refereeId);

	@Query(value = "select sum(a.updation_amount) FROM public.lenders_paticipation_updation a join \r\n"
			+ "public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:refereeId and \r\n"
			+ "to_char(a.updated_on, 'yyyy-mm-dd') >= '2021-08-04' and b.deal_type!='EQUITY'", nativeQuery = true)
	public Double findingSumOfParticipationUpdatedAmount(Integer refereeId);

	@Query(value = "SELECT sum(a.updation_amount) FROM public.lenders_paticipation_updation a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and a.deal_type!='EQUITY'", nativeQuery = true)
	public Double getSumOfLenderUpdatedAmountButNotInEquityIncludingClosedDeals(@Param("userId") Integer userId);

	@Query(value = "select sum(updation_amount) from public.lenders_paticipation_updation where user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public Double findingSumOfAmountUpdated(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "select sum(updation_amount) from public.lenders_paticipation_updation where user_id=:id", nativeQuery = true)
	public Double findingSumOfAmountUpdatedByUser(@Param("id") Integer id);

	@Query(value = "select * from public.lenders_paticipation_updation where user_id=:userId", nativeQuery = true)
	public List<LendersPaticipationUpdation> findingListOfDealsUpdated(@Param("userId") int userId);

	@Query(value = "select b.deal_name,b.loan_active_date,a.updation_amount,a.updated_on,\r\n"
			+ "b.funds_acceptance_start_date,b.duration,a.deal_id from public.lenders_paticipation_updation a join\r\n"
			+ "public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and b.deal_type!='EQUITY' and b.borrower_closing_status='NOTYETCLOSED'", nativeQuery = true)
	public List<Object[]> findingListOfUpdatedDeals(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(updation_amount) FROM public.lenders_paticipation_updation where user_id=:userId and deal_id=:dealId and (updated_on>= To_date(:fromDate,'dd-MM-yyyy') and  updated_on< To_date(:toDate,'dd-MM-yyyy'))", nativeQuery = true)
	public Double getSumOfUpdatedAmountInGivenDates(@Param("userId") Integer userId, @Param("dealId") Integer dealId,
			@Param("fromDate") String fromDate, @Param("toDate") String toDate);

	public List<LendersPaticipationUpdation> findByDealId(Integer id);

	@Query(value = "SELECT * FROM public.lenders_paticipation_updation where user_id=:userId and deal_id=:dealId and fee_status='PENDING' order by id desc limit 1", nativeQuery = true)
	public LendersPaticipationUpdation toCheckUserAlreadyInvoledInPartcipatedDeal(@Param("userId") Integer lenderUserId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lenders_paticipation_updation where user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public LendersPaticipationUpdation toCheckUserAlreadyParticipationInvoledInDeal(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "select sum(processing_fee) from public.lenders_paticipation_updation where user_id=:userId and deal_id=:dealId and fee_status='PENDING'", nativeQuery = true)
	public Double findingSumOfAmountUpdatedLenderProcessingFee(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "select sum(updation_amount) from public.lenders_paticipation_updation where user_id=:userId", nativeQuery = true)
	public Double findingSumOfAmountUpdatedUser(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(a.updation_amount) FROM public.lenders_paticipation_updation a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and a.deal_type='STUDENT'", nativeQuery = true)
	public Double findLenderStudentUpdationDeals(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(a.updation_amount) FROM public.lenders_paticipation_updation a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and a.deal_type='EQUITY'", nativeQuery = true)
	public Double findLenderEquityDeals(@Param("userId") Integer userId);

	@Query(value = "SELECT SUM(updation_amount)\r\n" + "FROM public.lenders_paticipation_updation\r\n"
			+ "WHERE user_id = :userId\r\n"
			+ "  AND DATE(updated_on) >= DATE(:fromDate) AND DATE(updated_on) <= DATE(:toDate)", nativeQuery = true)
	public Double getTotalDealUpdationAmountByUserId(@Param("userId") Integer userId,
			@Param("fromDate") String fromDate, @Param("toDate") String endDate);

	@Query(value = "SELECT * FROM public.lenders_paticipation_updation where user_id=:userId and deal_id=:dealId and fee_status='PENDING' order by id desc limit 1", nativeQuery = true)
	public LendersPaticipationUpdation findLendersParticipationUpdation(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lenders_paticipation_updation WHERE fee_status='PENDING'and user_id>0 ORDER BY updated_on desc", nativeQuery = true)
	public List<LendersPaticipationUpdation> findLendeUpdationPending();

	@Query(value = "SELECT *\r\n" + "FROM public.lenders_paticipation_updation a\r\n"
			+ "JOIN public.oxy_borrowers_deals_information b ON a.deal_id = b.id\r\n" + "WHERE a.user_id =:userId\r\n"
			+ "AND a.fee_status = 'PENDING'\r\n" + " order by a.id asc limit 1", nativeQuery = true)
	public LendersPaticipationUpdation findUserExistingDealsFeeStatusInUpdation(@Param("userId") Integer userId);

}
