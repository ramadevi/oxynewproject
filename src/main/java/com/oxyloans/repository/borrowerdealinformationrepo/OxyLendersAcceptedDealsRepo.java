package com.oxyloans.repository.borrowerdealinformationrepo;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import com.oxyloans.entity.borrowersdealsinformation.OxyLendersAcceptedDeals;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.borrowersdealsinformation.OxyLendersAcceptedDeals.PaticipationState;

@Repository
public interface OxyLendersAcceptedDealsRepo extends PagingAndSortingRepository<OxyLendersAcceptedDeals, Integer>,
		JpaSpecificationExecutor<OxyLendersAcceptedDeals> {

	@Query(value = "SELECT * FROM public.oxy_lenders_accepted_deals where user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public OxyLendersAcceptedDeals toCheckUserAlreadyInvoledInDeal(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT a.deal_id,a.group_id,a.participated_amount,a.processing_fee,a.fee_status,a.lender_returns_type,a.rateofinterest,date(a.received_on),a.participation_state FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and b.borrower_closing_status='NOTYETCLOSED' and b.deal_type!='EQUITY' order by a.deal_id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getLenderPaticipatedDealsExpectEquity(@Param("userId") Integer userId,
			@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "SELECT count(a.id) FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and b.borrower_closing_status='NOTYETCLOSED' and b.deal_type!='EQUITY'", nativeQuery = true)
	public Integer getLenderPaticipatedDealsCountExpectEquity(@Param("userId") Integer userId);

	@Query(value = "SELECT count(id) FROM public.oxy_lenders_accepted_deals where deal_id=:dealId", nativeQuery = true)
	public Integer getListOfLendersMappedToDealIdCount(@Param("dealId") Integer dealId);

	@Query(value = "SELECT user_id,group_id,participated_amount,processing_fee,fee_status,lender_returns_type,rateofinterest,date(received_on),participation_state,principal_returning_account_type FROM public.oxy_lenders_accepted_deals where deal_id=:dealId", nativeQuery = true)
	public List<Object[]> getListOfLendersMappedToDealId(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(a.participated_amount) FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where user_id=:userId and b.deal_type!='EQUITY' and b.borrower_closing_status='NOTYETCLOSED'", nativeQuery = true)
	public Double getDealParticipationAmount(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where lender_user_id=:userId and (loan_status='Agreed' OR loan_status='Active' OR loan_status='Closed') and deal_id>0", nativeQuery = true)
	public Double getDealParticipationAmountAfterAgreements(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(a.participated_amount) FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where user_id=:userId and b.deal_type='EQUITY'", nativeQuery = true)
	public Double getDealParticipationAmountForEquity(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(participated_amount) FROM public.oxy_lenders_accepted_deals where deal_id=:dealId", nativeQuery = true)
	public Double getListOfLendersParticipatedAmount(@Param("dealId") Integer dealId);

	@Query(value = "SELECT user_id,participated_amount,lender_returns_type,rateofinterest FROM public.oxy_lenders_accepted_deals where deal_id=:dealId order by participated_amount desc", nativeQuery = true)
	public List<Object[]> getListOfLendersByDealId(@Param("dealId") Integer dealId);

	@Query(value = "select sum(participated_amount) from public.oxy_lenders_accepted_deals where deal_id=:id", nativeQuery = true)
	public BigInteger findingSumOfParticipatedAmount(@Param("id") Integer id);

	@Query(value = "select * from public.oxy_lenders_accepted_deals where user_id=:lenderUserId", nativeQuery = true)
	public List<OxyLendersAcceptedDeals> findbyUserId(@Param("lenderUserId") Integer lenderUserId);

	@Query(value = "select * from public.oxy_lenders_accepted_deals where user_id=:id", nativeQuery = true)
	public List<OxyLendersAcceptedDeals> findingListOfLendersAcceptedDeals(@Param("id") Integer id);

	@Query(value = "select sum(b.participated_amount) FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id where b.group_id=:lenderGroupId and a.borrower_closing_status='NOTYETCLOSED'", nativeQuery = true)
	public BigInteger sumOfParticipatedAmountBasedOnLenderGroup(@Param("lenderGroupId") Integer lenderGroupId);

	@Query(value = "select sum(b.participated_amount) FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id where b.group_id=:lenderGroupId and a.borrower_closing_status='NOTYETCLOSED' and a.id=:dealId", nativeQuery = true)
	public BigInteger sumOfParticipatedAmountBasedOnLenderGroupAndDealId(@Param("lenderGroupId") Integer lenderGroupId,
			@Param("dealId") Integer dealId);

	@Query(value = "select sum(a.participated_amount) FROM public.oxy_lenders_accepted_deals a join \r\n"
			+ "public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:refereeId and \r\n"
			+ "to_char(a.received_on, 'yyyy-mm-dd') >= '2021-08-04' and b.deal_type!='EQUITY'", nativeQuery = true)
	public Double findingSumOfParticipatedAmountForLender(@Param("refereeId") Integer refereeId);

	@Query(value = "select * FROM public.oxy_lenders_accepted_deals where deal_id=:dealId", nativeQuery = true)
	public List<OxyLendersAcceptedDeals> findByDealId(@Param("dealId") Integer dealId);

	@Query(value = "select sum(b.participated_amount) FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id where b.group_id=:lenderGroupId and TO_CHAR(b.received_on,'mm/yyyy')=:monthAndYear and a.borrower_closing_status='NOTYETCLOSED'", nativeQuery = true)
	public BigInteger sumOfParticipatedAmountBasedOnMonthAndYear(@Param("lenderGroupId") Integer lenderGroupId,
			@Param("monthAndYear") String monthAndYear);

	@Query(value = "select sum(b.participated_amount) FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id where b.group_id=:lenderGroupId and TO_CHAR(b.received_on,'mm/yyyy')=:monthAndYear and a.borrower_closing_status='NOTYETCLOSED' and b.deal_id=:dealId", nativeQuery = true)
	public BigInteger sumOfParticipatedAmountBasedOnLenderGroupAndDealIdByMonthAndYear(
			@Param("lenderGroupId") Integer lenderGroupId, @Param("dealId") Integer dealId,
			@Param("monthAndYear") String monthAndYear);

	@Query(value = "select count(*) from public.oxy_lenders_accepted_deals where user_id=:refereeId", nativeQuery = true)
	public Integer findCountOfDeals(@Param("refereeId") Integer refereeId);

	@Query(value = "select participated_amount from public.oxy_lenders_accepted_deals where user_id=:refereeId and deal_id=:id", nativeQuery = true)
	public Double findinAmountBeforeUpdation(@Param("refereeId") Integer refereeId, @Param("id") Integer id);

	@Query(value = "select a.participated_amount,a.received_on,b.deal_name,b.deal_type from public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b\r\n"
			+ "on a.deal_id=b.id where a.user_id=:refereeId and a.deal_id=:dealId and b.deal_type!='EQUITY'", nativeQuery = true)
	public List<Object[]> findingParticipatedAmountBasedOnDealId(@Param("refereeId") Integer refereeId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT DISTINCT a.user_id,CONCAT(b.first_name,' ',b.last_name),c.mobile_number,c.email\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a \r\n"
			+ "join public.user_personal_details b on a.user_id=b.user_id\r\n"
			+ "join public.user c on b.user_id=c.id where b.user_expertise='true' order by a.user_id asc limit :pageSize offset :pageNo ", nativeQuery = true)
	public List<Object[]> findAllUniqueLenders(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "SELECT DISTINCT CONCAT(b.first_name,' ',b.last_name),c.mobile_number,c.email\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a \r\n"
			+ "join public.user_personal_details b on a.user_id=b.user_id\r\n"
			+ "join public.user c on b.user_id=c.id where b.user_expertise='true' and a.user_id=:userId ", nativeQuery = true)
	public List<Object[]> findUniqueLendersByUserId(@Param("userId") int userId);

	@Query(value = "SELECT count(DISTINCT a.user_id) FROM public.oxy_lenders_accepted_deals a \r\n"
			+ "join public.user_personal_details b on a.user_id=b.user_id\r\n"
			+ "join public.user c on b.user_id=c.id where b.user_expertise='true'", nativeQuery = true)
	public Integer getCountOfAllUniqueLenders();

	@Query(value = "select sum(updation_amount) from public.lenders_paticipation_updation where user_id=:refereeId and deal_id=:dealId", nativeQuery = true)
	public Double findingTotalParticipatedAmountUpdated(@Param("refereeId") Integer refereeId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(participated_amount) FROM public.oxy_lenders_accepted_deals where deal_id=:dealId and user_id=:userId", nativeQuery = true)
	public Double getDealParticipationAmountToLender(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "select DATE(received_on) from public.oxy_lenders_accepted_deals where deal_id=:dealId and user_id=:refereeId", nativeQuery = true)
	public String findParticipatedDateBasedOnDealId(@Param("dealId") Integer dealId,
			@Param("refereeId") Integer refereeId);

	@Query(value = "SELECT  a.deal_id,date(a.received_on) as c,a.participated_amount,a.rateofinterest,b.deal_name,b.borrower_closing_status,date(b.borrower_closed_date) as d,b.loan_active_date,b.duration FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and b.deal_type!='EQUITY' order by a.id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getLenderParticipatedDealsBasedOnDescOrder(@Param("userId") Integer userId,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "SELECT a.deal_id,date(a.received_on) as c,a.participated_amount,a.rateofinterest,b.deal_name,b.borrower_closing_status,date(b.borrower_closed_date) as d,b.loan_active_date,b.duration FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and b.deal_type!='EQUITY' order by a.id asc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getLenderParticipatedDealsBasedOnAscOrder(@Param("userId") Integer userId,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "SELECT count(a.id) FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where b.deal_type!='EQUITY' and a.user_id=:userId", nativeQuery = true)
	public Integer getLenderParticipatedDeals(@Param("userId") Integer userId);

	@Query(value = "SELECT  a.deal_id,date(a.received_on),a.participated_amount,a.rateofinterest,b.deal_name,b.borrower_closing_status FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId order by a.id asc", nativeQuery = true)
	public List<Object[]> getLenderParticipatedDealsList(@Param("userId") Integer userId);

	@Query(value = "select * from public.oxy_lenders_accepted_deals where user_id=:id and deal_id=32", nativeQuery = true)
	public OxyLendersAcceptedDeals findingLenderParticipatedIn3YRS(@Param("id") Integer id);

	@Query(value = "select * from public.oxy_lenders_accepted_deals where user_id=:id and deal_id=31", nativeQuery = true)
	public OxyLendersAcceptedDeals findingLendersParticipatedIN2YRS(@Param("id") Integer id);

	@Query(value = "select * from public.oxy_lenders_accepted_deals where user_id=:userId order by id asc limit 1", nativeQuery = true)
	public OxyLendersAcceptedDeals findingLenderParticipatedInDeal(@Param("userId") Integer userId);

	@Query(value = "SELECT count(a.id) FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId", nativeQuery = true)
	public Integer getLenderPaticipatedAllDealsCount(@Param("userId") Integer userId);

	@Query(value = "SELECT a.deal_id,a.group_id,a.participated_amount,a.processing_fee,a.fee_status,a.lender_returns_type,a.rateofinterest,date(a.received_on),a.participation_state,b.borrower_closing_status FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId order by a.deal_id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getLenderPaticipatedAllDeals(@Param("userId") Integer userId, @Param("pageNo") int pageNo,
			@Param("pageSize") int pageSize);

	@Query(value = "select * from public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b\r\n"
			+ "on a.deal_id=b.id where a.deal_id!=31 and a.deal_id!=32 and a.group_id=1 and a.received_on <='2021-06-30' \r\n"
			+ "and a.user_id=:id order by a.id asc limit 1", nativeQuery = true)
	public OxyLendersAcceptedDeals findingUserBelongsToGroupOne(@Param("id") Integer id);

	@Query(value = "select * from public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b\r\n"
			+ "on a.deal_id=b.id where a.deal_id!=31 and a.deal_id!=32 \r\n"
			+ "and Date(a.received_on) > '2021-06-30'\r\n" + "and a.user_id=:id order by a.id asc limit 1\r\n"
			+ "", nativeQuery = true)
	public OxyLendersAcceptedDeals findingLendersParticipatedINDeals(@Param("id") Integer id);

	@Query(value = "SELECT sum(a.participated_amount) FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where user_id=:userId and b.deal_type!='EQUITY'", nativeQuery = true)
	public Double getDealParticipationAmountIncludingClosedDeals(@Param("userId") Integer userId);

	@Query(value = "select * from public.oxy_lenders_accepted_deals where user_id=:refereeId and deal_id=:dealId", nativeQuery = true)
	public OxyLendersAcceptedDeals findingLenderParticipatedInDeal(@Param("refereeId") Integer refereeId,
			@Param("dealId") Integer dealId);

	@Query(value = "select * from public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b \r\n"
			+ "on a.deal_id=b.id where a.user_id=:id and b.deal_type!='EQUITY'\r\n" + "", nativeQuery = true)
	public List<OxyLendersAcceptedDeals> findingListOfDealsParticipatedByLender(@Param("id") Integer id);

	@Query(value = "select participated_amount from public.oxy_lenders_accepted_deals where deal_id=:dealId and user_id=:userId", nativeQuery = true)
	public Double findingAmountParticipatedInDeal(@Param("dealId") Integer dealId, @Param("userId") Integer userId);

	@Query(value = "select sum(updation_amount) from public.lenders_paticipation_updation where user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public Double findingAmountParticipatedUpdatedInDeal(@Param("dealId") Integer dealId,
			@Param("userId") Integer userId);

	@Query(value = "(select count(t1.dealid) from\r\n" + "\r\n" + "(select sum(s) as y,id as dealid from(\r\n"
			+ "select a.deal_id as id,sum(a.participated_amount) as s from public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where b.borrower_closing_status='NOTYETCLOSED' and b.deal_type=:dealType and a.user_id=:userId group by a.deal_id union all \r\n"
			+ "select a.deal_id as id,coalesce(sum(a.updation_amount),0) as s from public.lenders_paticipation_updation a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where b.borrower_closing_status='NOTYETCLOSED' and b.deal_type=:dealType and a.user_id=:userId group by a.deal_id) as y group by id ) as t1\r\n"
			+ "        \r\n" + "left join \r\n" + "\r\n" + "(select sum(z) as m,id as dealid from (\r\n"
			+ "select c.deal_id as id,sum(c.amount) as z from public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id join public.lenders_returns c on (c.deal_id=a.deal_id and c.user_id=a.user_id) where b.borrower_closing_status='NOTYETCLOSED' and b.deal_type=:dealType and a.user_id=:userId and (c.amount_type='LENDERPRINCIPAL' or c.amount_type='LENDERWITHDRAW') and c.status='INITIATED' group by c.deal_id union all\r\n"
			+ "select c.deal_id as id,sum(c.transaction_amount) as z from public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id join public.lender_scrow_wallet c on (c.deal_id=a.deal_id and c.user_id=a.user_id) where b.borrower_closing_status='NOTYETCLOSED' and b.deal_type=:dealType and a.user_id=:userId group by c.deal_id) as m group by id) as t2 on t1.dealid=t2.dealid where (t1.y-coalesce(t2.m, 0))>0)", nativeQuery = true)
	public Integer getLenderPaticipatedDealsBasedOnTypeCount(@Param("userId") Integer userId,
			@Param("dealType") String dealType);

	@Query(value = "(select t1.y,coalesce(t2.m, 0),(t1.y-coalesce(t2.m, 0)) as amount,  t1.dealid from\r\n" + "\r\n"
			+ "(select sum(s) as y,id as dealid from(\r\n"
			+ "select a.deal_id as id,sum(a.participated_amount) as s from public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where b.borrower_closing_status='NOTYETCLOSED' and b.deal_type=:dealType and a.user_id=:userId group by a.deal_id union all \r\n"
			+ "select a.deal_id as id,coalesce(sum(a.updation_amount),0) as s from public.lenders_paticipation_updation a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where b.borrower_closing_status='NOTYETCLOSED' and b.deal_type=:dealType and a.user_id=:userId group by a.deal_id) as y group by id ) as t1\r\n"
			+ "        \r\n" + "left join \r\n" + "\r\n" + "(select sum(z) as m,id as dealid from (\r\n"
			+ "select c.deal_id as id,sum(c.amount) as z from public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id join public.lenders_returns c on (c.deal_id=a.deal_id and c.user_id=a.user_id) where b.borrower_closing_status='NOTYETCLOSED' and b.deal_type=:dealType and a.user_id=:userId and (c.amount_type='LENDERPRINCIPAL' or c.amount_type='LENDERWITHDRAW') and c.status='INITIATED' group by c.deal_id union all\r\n"
			+ "select c.deal_id as id,sum(c.transaction_amount) as z from public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id join public.lender_scrow_wallet c on (c.deal_id=a.deal_id and c.user_id=a.user_id) where b.borrower_closing_status='NOTYETCLOSED' and b.deal_type=:dealType and a.user_id=:userId group by c.deal_id) as m group by id) as t2 on t1.dealid=t2.dealid where (t1.y-coalesce(t2.m, 0))>0) limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getLenderPaticipatedDealsBasedOnTypes(@Param("userId") Integer userId,
			@Param("dealType") String dealType, @Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select distinct(user_id) from public.oxy_lenders_accepted_deals where TO_CHAR(received_on,'YYYY-MM-DD') <= :endDate", nativeQuery = true)
	public List<Integer> findingListOfLendersParticipated(@Param("endDate") String endDate);

	@Query(value = "select * from public.oxy_lenders_accepted_deals where TO_CHAR(received_on,'YYYY-MM-DD') <= :endDate\r\n"
			+ "and user_id=:id", nativeQuery = true)
	public List<OxyLendersAcceptedDeals> findingListOfDealsParticipatedByLenderBasedOnDate(@Param("id") Integer id,
			@Param("endDate") String endDate);

	@Query(value = "select distinct(user_id) from public.oxy_lenders_accepted_deals where TO_CHAR(received_on,'YYYY-MM-DD') >=:startDate and\r\n"
			+ "TO_CHAR(received_on,'YYYY-MM-DD') <= :endDate", nativeQuery = true)
	public List<Integer> findingListOfLendersParticipatedBasedOnStartAndEndDate(@Param("startDate") String startDate,
			@Param("endDate") String endDate);

	@Query(value = "select * from public.oxy_lenders_accepted_deals where TO_CHAR(received_on,'YYYY-MM-DD') >= :startDate and\r\n"
			+ "TO_CHAR(received_on,'YYYY-MM-DD') <= :endDate and user_id=:id", nativeQuery = true)
	public List<OxyLendersAcceptedDeals> findingListOfDealsParticipatedByLenderBasedOnDateStartAndEnd(
			@Param("id") Integer id, @Param("startDate") String startDate, @Param("endDate") String endDate);

	@Query(value = "select sum(participated_amount) from public.oxy_lenders_accepted_deals where deal_id=:dealId", nativeQuery = true)
	public Double findingSumOfAmountParticipatedInDeal(@Param("dealId") int dealId);

	@Query(value = "select sum(updation_amount) from public.lenders_paticipation_updation where deal_id=:dealId", nativeQuery = true)
	public Double findingSumOfAmountUpdatedInDeal(@Param("dealId") int dealId);

	@Query(value = "SELECT Distinct b.user_id FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id  where a.borrower_closing_status='NOTYETCLOSED' and a.deal_type!='EQUITY' order by b.user_id limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Integer> listOfLendersRunningDeals(@Param("pageSize") Integer pageSize,
			@Param("pageNo") Integer pageNo);

	@Query(value = "SELECT count(Distinct b.user_id) FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id  where a.borrower_closing_status='NOTYETCLOSED' and a.deal_type!='EQUITY'", nativeQuery = true)
	public Integer listOfLendersRunningDealsCount();

	@Query(value = "SELECT b.deal_id FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id  where a.borrower_closing_status='NOTYETCLOSED' and a.deal_type!='EQUITY' and b.user_id=:userId", nativeQuery = true)
	public List<Integer> runningDealIdsToLender(@Param("userId") Integer userId);

	@Query(value = "select sum(participated_amount) from public.oxy_lenders_accepted_deals where user_id=:id", nativeQuery = true)
	public Double findingSumOfAmountParticipatedByUser(@Param("id") Integer id);

	@Query(value = "(SELECT a.deal_id,b.deal_name,b.deal_amount,date(a.paid_date),a.amount,a.remarks,b.borrower_closing_status,a.status FROM public.lenders_returns a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and (a.amount_type='LENDERPRINCIPAL' OR a.amount_type='LENDERWITHDRAW') and a.status='INITIATED') union \r\n"
			+ "\r\n"
			+ "(select a.deal_id,b.deal_name,b.deal_amount,date(a.transaction_date),a.transaction_amount,a.scrow_account_number,b.borrower_closing_status,a.comments from public.lender_scrow_wallet a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and a.deal_id>0) order by date desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getLenderPrincipalReturnedBasedOnAscOrder(@Param("userId") Integer userId,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "(SELECT a.deal_id,b.deal_name,b.deal_amount,date(a.paid_date),a.amount,a.remarks,b.borrower_closing_status,a.status FROM public.lenders_returns a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and (a.amount_type='LENDERPRINCIPAL' OR a.amount_type='LENDERWITHDRAW') and a.status='INITIATED') union \r\n"
			+ "\r\n"
			+ "(select a.deal_id,b.deal_name,b.deal_amount,date(a.transaction_date),a.transaction_amount,a.scrow_account_number,b.borrower_closing_status,a.comments from public.lender_scrow_wallet a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and a.deal_id>0) order by date desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getLenderPrincipalReturnedBasedOnDescOrder(@Param("userId") Integer userId,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "(SELECT a.deal_id,b.deal_name,b.deal_amount,date(a.paid_date),a.amount,a.remarks FROM public.lenders_returns a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and (a.amount_type='LENDERPRINCIPAL' OR a.amount_type='LENDERWITHDRAW') and a.status='INITIATED') union \r\n"
			+ "\r\n"
			+ "(select a.deal_id,b.deal_name,b.deal_amount,date(a.transaction_date),a.transaction_amount,a.scrow_account_number from public.lender_scrow_wallet a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and a.deal_id>0) order by date desc", nativeQuery = true)
	public List<Object[]> lenderPaticipationClosedDeals(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.oxy_lenders_accepted_deals where user_id=:userId limit 1", nativeQuery = true)
	public OxyLendersAcceptedDeals lenderParticipatedInDeals(@Param("userId") Integer userId);

	@Query(value = "select * from public.oxy_lenders_accepted_deals where user_id=:userId", nativeQuery = true)
	public List<OxyLendersAcceptedDeals> findListOfParticipatedDeals(@Param("userId") int userId);

	@Query(value = "select sum(participated_amount) from public.oxy_lenders_accepted_deals where deal_id=:dealId", nativeQuery = true)
	public BigInteger findingSumOfAmountParticipatedInDeal1(@Param("dealId") int dealId);

	@Query(value = "select sum(updation_amount) from public.lenders_paticipation_updation where deal_id=:dealId", nativeQuery = true)
	public BigInteger findingSumOfAmountUpdatedInDeal1(@Param("dealId") int dealId);

	@Query(value = "select (participated_amount) + (select COALESCE (sum(updation_amount),0) - (select COALESCE (sum(amount),0) from public.lenders_returns where user_id=:userId and deal_id=:dealId and amount_type='LENDERPRINCIPAL' and status='INITIATED')\r\n"
			+ " from lenders_paticipation_updation where user_id=:userId and deal_id=:dealId) from oxy_lenders_accepted_deals where user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public Double remainingAmountInDeal(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT count(a.deal_id) FROM public.lenders_returns a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and (a.amount_type='LENDERPRINCIPAL' OR a.amount_type='LENDERWITHDRAW') and a.status='INITIATED'", nativeQuery = true)
	public Integer principalReturnedToAccountCount(@Param("userId") Integer userId);

	@Query(value = "select count(a.deal_id) from public.lender_scrow_wallet a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and a.deal_id>0", nativeQuery = true)
	public Integer principalReturnedToWalletCount(@Param("userId") Integer userId);

	@Query(value = "select * from  public.oxy_lenders_accepted_deals where deal_id=:dealId", nativeQuery = true)
	public List<OxyLendersAcceptedDeals> findingListOfLendersParticipatedInDeal(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.oxy_lenders_accepted_deals where deal_id=:dealId order by id asc limit 1", nativeQuery = true)
	public OxyLendersAcceptedDeals getFirstParticipationDate(@Param("dealId") Integer dealId);

	@Query(value = "select lender_returns_type from public.oxy_lenders_accepted_deals where user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public String findingPayoutTypeForDeal(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "select rateofinterest from public.oxy_lenders_accepted_deals where user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public Double findingRateOfInterestForDeal(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "select count(*) from public.oxy_lenders_accepted_deals where deal_id=:id", nativeQuery = true)
	public Integer findingNoOfLendersParticipatedInDeal(@Param("id") Integer id);

	@Query(value = "select user_id,sum(amount) from\r\n"
			+ "(select user_id,sum(updation_amount) as amount from public.lenders_paticipation_updation where deal_id=:dealId group by user_id union all\r\n"
			+ "\r\n"
			+ "select user_id,sum(participated_amount) as amount from public.oxy_lenders_accepted_deals where deal_id=:dealId group by user_id ) as participated  group by user_id order by user_id", nativeQuery = true)
	public List<Object[]> totalLendersWithParticipatedAmount(@Param("dealId") Integer dealId);

	@Query(value = "select deal_id from public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information\r\n"
			+ "b on a.deal_id=b.id where a.user_id=:userId and b.borrower_closing_status='CLOSED'", nativeQuery = true)
	public List<Integer> findingListOfDealIdsParticipatedByUser(@Param("userId") Integer userId);

	@Query(value = "select user_id,sum(s1) from\r\n"
			+ "(select sum(participated_amount) as s1,user_id from public.oxy_lenders_accepted_deals\r\n"
			+ "group by user_id\r\n" + "union\r\n"
			+ "select sum(updation_amount) as s1 ,user_id from public.lenders_paticipation_updation\r\n"
			+ "group by user_id) as x group by user_id order by user_id desc", nativeQuery = true)
	public List<Object[]> findingListOfLenders();

	@Query(value = "select sum(s1) from \r\n"
			+ "(select sum(participated_amount) as s1 from public.oxy_lenders_accepted_deals where \r\n"
			+ " user_id=:refereeId and to_char(received_on,'yyyy-MM-dd')>=:startDate and to_char(received_on,'yyyy-MM-dd')<=:endDate\r\n"
			+ " union\r\n" + " select sum(updation_amount) as s1  from public.lenders_paticipation_updation\r\n"
			+ " where user_id=:refereeId and to_char(updated_on,'yyyy-MM-dd')>=:startDate\r\n"
			+ " and to_char(updated_on,'yyyy-MM-dd')<=:endDate) as x", nativeQuery = true)
	public Double findingSumOfParticipatedAmountOfReferee(@Param("startDate") String startDate,
			@Param("endDate") String endDate, @Param("refereeId") Integer refereeId);

	@Query(value = "select * from public.oxy_lenders_accepted_deals where to_char(received_on,'yyyy-MM-dd')>=:startDate \r\n"
			+ "and to_char(received_on,'yyyy-MM-dd')<=:endDate and user_id=:lenderId order by id desc limit 1;", nativeQuery = true)
	public OxyLendersAcceptedDeals findingLastParticipationDate(@Param("startDate") String startDate,
			@Param("endDate") String endDate, @Param("lenderId") Integer lenderId);

	@Query(value = "select sum(s1) from \r\n"
			+ "(select sum(participated_amount) as s1 from public.oxy_lenders_accepted_deals where  \r\n"
			+ " user_id=:userId and deal_id=:dealId union all\r\n"
			+ " select sum(updation_amount) as s1  from public.lenders_paticipation_updation\r\n"
			+ " where user_id=:userId and deal_id=:dealId ) as x", nativeQuery = true)
	public Double findingSumOfParticipatedAmountTotal(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "(SELECT a.deal_id,b.deal_name,b.deal_amount,date(a.paid_date),a.amount,a.remarks,b.borrower_closing_status FROM public.lenders_returns a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and (a.amount_type='LENDERPRINCIPAL' OR a.amount_type='LENDERWITHDRAW') and a.status='INITIATED') union \r\n"
			+ "\r\n"
			+ "(select a.deal_id,b.deal_name,b.deal_amount,date(a.transaction_date),a.transaction_amount,a.scrow_account_number,b.borrower_closing_status from public.lender_scrow_wallet a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and a.deal_id>0) order by date desc", nativeQuery = true)
	public List<Object[]> getLenderPrincipalReturnedForUser(@Param("userId") Integer userId);

	@Query(value = "SELECT a.deal_id,a.group_id,a.participated_amount,a.processing_fee,\r\n"
			+ "a.fee_status,a.lender_returns_type,a.rateofinterest,date(a.received_on),\r\n"
			+ "a.participation_state FROM public.oxy_lenders_accepted_deals a join \r\n"
			+ "public.oxy_borrowers_deals_information b on a.deal_id=b.id where \r\n"
			+ "a.user_id=:userId and b.borrower_closing_status='NOTYETCLOSED' and \r\n"
			+ "b.deal_type!='EQUITY' order by a.deal_id desc", nativeQuery = true)
	public List<Object[]> getLenderPaticipatedDealsExpectEquityWithoutPagination(@Param("userId") Integer userId);

	@Query(value = "select distinct(user_id) from public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b\r\n"
			+ "on a.deal_id=b.id where b.deal_type!='EQUITY' order by user_id", nativeQuery = true)
	public List<Integer> findingListOfUsers();

	@Query(value = "select a.user_id,a.deal_id,a.participated_amount,a.lender_returns_type,a.rateofinterest,\r\n"
			+ "a.received_on,b.deal_name,shares from public.oxy_lenders_accepted_deals a join \r\n"
			+ "public.oxy_borrowers_deals_information b on a.deal_id=b.id where b.deal_type='EQUITY'\r\n"
			+ "and b.borrower_closing_status='NOTYETCLOSED' and a.user_id=:userId", nativeQuery = true)
	public List<Object[]> findingListOfEquityDeals(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(participated_amount)\r\n" + "FROM public.oxy_lenders_accepted_deals \r\n"
			+ "where deal_id=:dealId and user_id=:userId \r\n"
			+ "and (received_on>=  To_date(:fromDate,'dd-MM-yyyy') and  received_on< To_date(:toDate,'dd-MM-yyyy'))", nativeQuery = true)
	public Double getDealParticipationInGivenDates(@Param("userId") Integer userId, @Param("dealId") Integer dealId,
			@Param("fromDate") String fromDate, @Param("toDate") String toDate);

	public OxyLendersAcceptedDeals findFirstByOrderByIdAsc();

	@Query(value = "select sum(participated_amount) from public.oxy_lenders_accepted_deals a join \r\n"
			+ "public.oxy_borrowers_deals_information b on a.deal_id=b.id where b.deal_type='EQUITY'\r\n"
			+ "and b.borrower_closing_status='NOTYETCLOSED' and a.user_id=:userId", nativeQuery = true)
	public Double findingSumOfEquityAmount(@Param("userId") Integer userId);

	@Query(value = "SELECT distinct user_id FROM public.oxy_lenders_accepted_deals", nativeQuery = true)
	public List<Integer> getLenderIdParticitatedIntoDeal();

	@Query(value = "SELECT count(id) FROM public.oxy_lenders_accepted_deals where user_id=:lenderUserId", nativeQuery = true)
	public Integer findByUserId(@Param("lenderUserId") Integer lenderUserId);

	@Query(value = "SELECT user_id FROM public.oxy_lenders_accepted_deals where deal_id=:dealId", nativeQuery = true)
	public List<Integer> getLenderIdsParticitatedInDeal(@Param("dealId") Integer dealId);

	public Page<OxyLendersAcceptedDeals> findByUserIdAndPaticipationStateOrderByReceivedOnDesc(int userId,
			PaticipationState autolend, Pageable pageable);

	@Query(value = "SELECT sum(participated_amount)\r\n" + "FROM public.oxy_lenders_accepted_deals a\r\n"
			+ "JOIN public.user b ON a.user_id = b.id\r\n" + "WHERE \r\n" + "  date(a.received_on) = CURRENT_DATE \r\n"
			+ "  AND b.test_user = false ", nativeQuery = true)
	public Double findingSumOfAmountParticipatedInDeal();

	@Query(value = "SELECT sum(updation_amount)\r\n" + "FROM public.lenders_paticipation_updation a\r\n"
			+ "JOIN public.user b ON a.user_id = b.id\r\n" + "WHERE \r\n" + " date(a.created_on) = CURRENT_DATE \r\n"
			+ " AND b.test_user = false ", nativeQuery = true)
	public Double findingSumOfAmountUpdatedInDeal();

	@Query(value = "SELECT * FROM public.oxy_lenders_accepted_deals where user_id=:userId and deal_id=:dealId and fee_status='PENDING'", nativeQuery = true)
	public OxyLendersAcceptedDeals toCheckUserAlreadyInvoledInDealForPendingFee(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT COUNT(DISTINCT a.user_id) AS total_unique_deal_count\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + " date(a.received_on) = CURRENT_DATE \r\n"
			+ " AND b.test_user = false;", nativeQuery = true)
	public Integer findByPaticipationCount();

	@Query(value = "SELECT COUNT(DISTINCT a.user_id) AS total_unique_deal_count\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a \r\n" + "LEFT JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE DATE(a.received_on) = CURRENT_DATE \r\n" + "AND a.group_id = 0\r\n"
			+ "AND b.test_user = false;", nativeQuery = true)
	public Integer findByNewLenderCount();

	@Query(value = "SELECT \r\n" + "    SUM(a.participated_amount) AS total_amount_daily\r\n" + "FROM \r\n"
			+ "    public.oxy_lenders_accepted_deals a\r\n" + "LEFT JOIN \r\n"
			+ "    public.user b ON b.id = a.user_id\r\n" + "WHERE \r\n" + "    a.group_id = 0 and test_user=false\r\n"
			+ "    AND DATE(a.received_on) = CURRENT_DATE", nativeQuery = true)

	public Double findBySumOfNewLenderAmount();

	@Query(value = "SELECT SUM(a.participated_amount) AS total_participated_amount\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + " a.received_on >= CURRENT_DATE - INTERVAL '7 days'\r\n"
			+ " AND b.test_user = false;", nativeQuery = true)
	public Double findingSumOfAmountParticipatedInDealWeeklydata();

	@Query(value = "SELECT SUM(a.updation_amount) AS total_participated_amount\r\n"
			+ "FROM public.lenders_paticipation_updation a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + "    a.created_on >= CURRENT_DATE - INTERVAL '6 days'\r\n"
			+ "    AND b.test_user = false;", nativeQuery = true)

	public Double findingSumOfAmountUpdatedInDealWeeklyData();

	@Query(value = "SELECT\r\n" + "    COUNT(DISTINCT a.user_id) AS user_count_weekly\r\n" + "FROM \r\n"
			+ "    public.oxy_lenders_accepted_deals a\r\n" + "LEFT JOIN \r\n"
			+ "    public.user b ON b.id = a.user_id\r\n" + "WHERE \r\n"
			+ "    a.group_id = 0 AND b.test_user=false\r\n"
			+ "    AND DATE(a.received_on) >= CURRENT_DATE - INTERVAL '6 days'\r\n", nativeQuery = true)
	public Integer findByNewLenderCountWeekly();

	@Query(value = "SELECT \r\n" + "    SUM(a.participated_amount) AS total_amount_weekly\r\n" + "FROM \r\n"
			+ "    public.oxy_lenders_accepted_deals a\r\n" + "LEFT JOIN \r\n"
			+ "    public.user b ON b.id = a.user_id\r\n" + "WHERE \r\n" + "    a.group_id = 0  and test_user=false\r\n"
			+ " AND DATE(a.received_on) >= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)
	public Double findBySumOfNewLenderAmountWeekly();

	@Query(value = "SELECT COUNT(DISTINCT a.user_id) AS total_unique_deal_count\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + " a.received_on >= CURRENT_DATE - INTERVAL '6 days'\r\n"
			+ " AND b.test_user = false;", nativeQuery = true)

	public Integer findByPaticipationWeeklyCount();

	@Query(value = "select COUNT(id) from public.lenders_paticipation_updation where created_on >= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)

	public Integer findByUpdationWeeklyCount();

	@Query(value = "SELECT SUM(a.participated_amount) AS total_participated_amount\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + "    DATE(a.received_on) >= :startDate\r\n" + "    AND DATE(a.received_on) <= :endDate\r\n"
			+ "    AND b.test_user = false;\r\n", nativeQuery = true)
	public Double findingSumOfAmountParticipatedInDealMonthlyDataWithInput(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT SUM(a.updation_amount) AS total_participated_amount\r\n"
			+ "FROM public.lenders_paticipation_updation a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + "    DATE(a.created_on) >= :startDate\r\n" + "    AND DATE(a.created_on) <= :endDate\r\n"
			+ "    AND b.test_user = false;\r\n", nativeQuery = true)
	public Double findingSumOfAmountUpdatedInDealMonthlyDataWithInput(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT COUNT(id) FROM public.oxy_lenders_accepted_deals where user_id=:userId and date(received_on)>=:startDate and date(received_on)<=:endDate", nativeQuery = true)

	public Integer findByNewLenderCountMonthlyWithInput(@Param("userId") Integer userId,
			@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT sum(participated_amount)  FROM public.oxy_lenders_accepted_deals where user_id=:userId and  date(received_on)>=:startDate and date(received_on)<=:endDate", nativeQuery = true)

	public Double findByNewLenderParticipation(@Param("userId") Integer userId, @Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT sum(updation_amount)  FROM public.lenders_paticipation_updation where user_id=:userId and  date(created_on)>=:startDate and date(created_on)<=:endDate", nativeQuery = true)

	public Double findByNewLenderUpdationAmount(@Param("userId") Integer userId, @Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT SUM(a.participated_amount) AS total_participated_amount\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + "    date_trunc('month', a.received_on) = date_trunc('month', CURRENT_DATE)\r\n"
			+ "    AND b.test_user = false;\r\n", nativeQuery = true)
	public Double findingSumOfAmountParticipatedInDealMonthlyData();

	@Query(value = "SELECT SUM(a.updation_amount) AS total_participated_amount\r\n"
			+ "FROM public.lenders_paticipation_updation a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + "    date_trunc('month', a.created_on) = date_trunc('month', CURRENT_DATE)\r\n"
			+ "    AND b.test_user = false;\r\n", nativeQuery = true)
	public Double findingSumOfAmountUpdatedInDealMonthlyData();

	@Query(value = "SELECT COUNT(DISTINCT a.user_id) AS unique_user_count\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + "    b.test_user = false\r\n"
			+ "    AND date_trunc('month', a.received_on) = date_trunc('month', CURRENT_DATE)\r\n"
			+ "    AND a.group_id = 0", nativeQuery = true)

	public Integer findByNewLenderCountMonthly();

	@Query(value = "SELECT COALESCE(SUM(a.participated_amount), 0) AS total_participation_amount\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + "    b.test_user = false\r\n"
			+ "    AND date_trunc('month', a.received_on) = date_trunc('month', CURRENT_DATE)\r\n"
			+ "    AND a.group_id = 0;\r\n", nativeQuery = true)

	public Double findBySumOfNewLenderAmountMonthly();

	@Query(value = "SELECT COUNT(DISTINCT a.user_id) AS unique_user_count\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + "    date_trunc('month', a.received_on) = date_trunc('month', CURRENT_DATE)\r\n"
			+ "    AND b.test_user = false;\r\n", nativeQuery = true)

	public Integer findByLenderPaticipationMonthlyCount();

	@Query(value = "SELECT COUNT(DISTINCT a.user_id) AS unique_user_count\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + "    DATE(a.received_on) >= :startDate\r\n" + "    AND DATE(a.received_on) <= :endDate\r\n"
			+ "    AND b.test_user = false;\r\n", nativeQuery = true)
	public Integer findByLenderPaticipationMonthlyCountWithInput(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

//	@Query(value = "SELECT  SUM(total_amount) AS total_amount_daily\r\n" + "FROM (\r\n"
//			+ "    SELECT a.user_id, SUM(a.updation_amount) AS total_amount\r\n"
//			+ "    FROM public.lenders_paticipation_updation a\r\n" + "    JOIN public.user b ON a.user_id = b.id\r\n"
//			+ "    WHERE \r\n" + "        a.created_on >= CURRENT_DATE \r\n" + "        AND a.user_id NOT IN (\r\n"
//			+ "            SELECT user_id\r\n" + "            FROM public.lenders_paticipation_updation \r\n"
//			+ "            WHERE created_on < CURRENT_DATE \r\n" + "        )\r\n"
//			+ "        AND b.test_user = false \r\n" + "    GROUP BY a.user_id\r\n"
//			+ ") AS first_time_participants_daily;\r\n", nativeQuery = true)
//	public Double findBySumOfNewLenderUpdationAmount();

//	@Query(value = "SELECT SUM(total_amount) AS total_amount_weekly\r\n" + "FROM (\r\n"
//			+ "    SELECT a.user_id, COALESCE(SUM(a.updation_amount), 0) AS total_amount\r\n"
//			+ "    FROM public.lenders_paticipation_updation a\r\n" + "    JOIN public.user b ON a.user_id = b.id\r\n"
//			+ "    WHERE \r\n" + "        a.created_on >= CURRENT_DATE - INTERVAL '7 days'\r\n"
//			+ "        AND a.user_id NOT IN (\r\n" + "            SELECT user_id\r\n"
//			+ "            FROM public.lenders_paticipation_updation \r\n"
//			+ "            WHERE created_on < CURRENT_DATE - INTERVAL '7 days'\r\n" + "        )\r\n"
//			+ "        AND b.test_user = false\r\n" + "    GROUP BY a.user_id\r\n"
//			+ ") AS first_time_participants_weekly;\r\n", nativeQuery = true)
//	public Double findBySumOfNewLendersUpdationAmountWeekly();

	@Query(value = "SELECT  COALESCE(SUM(participated_amount), 0) AS total_participation_amount\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + "    b.test_user = false\r\n" + "    AND a.received_on BETWEEN :startDate AND :endDate\r\n"
			+ "    AND a.group_id = 0", nativeQuery = true)

	public Double findByNewLenderParticipationAmountWithInput(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

//	@Query(value = "SELECT SUM(total_participation_amount) AS total_sum_participation_amount\r\n" + "FROM (\r\n"
//			+ "    SELECT user_id, COALESCE(SUM(updation_amount), 0) AS total_participation_amount\r\n"
//			+ "    FROM public.lenders_paticipation_updation a\r\n" + "    JOIN public.user b ON a.user_id = b.id\r\n"
//			+ "    WHERE \r\n" + "        a.created_on BETWEEN :startDate AND :endDate\r\n"
//			+ "        AND a.user_id NOT IN (\r\n" + "            SELECT user_id\r\n"
//			+ "            FROM public.lenders_paticipation_updation \r\n"
//			+ "            WHERE created_on < :startDate\r\n" + "        )\r\n" + "        AND b.test_user = false\r\n"
//			+ "    GROUP BY a.user_id\r\n" + ") AS first_time_participants;\r\n", nativeQuery = true)
//
//	public Double findByNewLenderUpdationAmountWithInput(@Param("startDate") Date startDate,
//			@Param("endDate") Date endDate);

	@Query(value = "SELECT COUNT(*) AS total_count\r\n" + "FROM (\r\n"
			+ "    SELECT user_id, COALESCE(SUM(participated_amount), 0) AS total_participation_amount\r\n"
			+ "    FROM public.oxy_lenders_accepted_deals a\r\n" + "    JOIN public.user b ON a.user_id = b.id\r\n"
			+ "    WHERE \r\n" + "        a.received_on BETWEEN :startDate AND :endDate\r\n"
			+ "        AND a.user_id NOT IN (\r\n" + "            SELECT user_id\r\n"
			+ "            FROM public.oxy_lenders_accepted_deals \r\n"
			+ "            WHERE received_on < :startDate\r\n" + "        )\r\n" + "        AND b.test_user = false\r\n"
			+ "    GROUP BY a.user_id\r\n" + ") AS first_time_participants;\r\n", nativeQuery = true)

	public Integer findByNewLendersCountWithInput(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

//	@Query(value = "SELECT SUM(total_participation_amount) AS total_sum_participation_amount\r\n" + "FROM (\r\n"
//			+ "    SELECT a.user_id, COALESCE(SUM(a.updation_amount), 0) AS total_participation_amount\r\n"
//			+ "    FROM public.lenders_paticipation_updation a\r\n" + "    JOIN public.user b ON a.user_id = b.id\r\n"
//			+ "    WHERE \r\n" + "        date_trunc('month', a.created_on) = date_trunc('month', CURRENT_DATE)\r\n"
//			+ "        AND a.user_id NOT IN (\r\n" + "            SELECT user_id\r\n"
//			+ "            FROM public.lenders_paticipation_updation \r\n"
//			+ "            WHERE date_trunc('month', created_on) < date_trunc('month', CURRENT_DATE)\r\n"
//			+ "        )\r\n" + "        AND b.test_user = false\r\n" + "    GROUP BY a.user_id\r\n"
//			+ ") AS first_time_participants;", nativeQuery = true)
//
//	public Double findByNewLenderUpdationAmountMonthly();

	@Query(value = "SELECT\r\n" + "    subquery_outer.user_id,\r\n" + "    subquery_outer.email,\r\n"
			+ "    subquery_outer.mobile_number,\r\n" + "	   subquery_outer.registered_on,\r\n"
			+ "    COALESCE(SUM(subquery_outer.participated_amount), 0) AS total_sum_participated_amount,\r\n"
			+ "    COALESCE(SUM(up.updation_amount), 0) AS total_sum_updation_amount\r\n" + "FROM\r\n" + "    (\r\n"
			+ "        SELECT\r\n" + "            user_id,\r\n" + "            email,\r\n"
			+ "            mobile_number,\r\n" + "		    registered_on,\r\n"
			+ "            COALESCE(SUM(participated_amount), 0) AS participated_amount,\r\n"
			+ "            ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY MAX(received_on) DESC) AS row_num\r\n"
			+ "        FROM\r\n" + "            (\r\n" + "                SELECT DISTINCT\r\n"
			+ "                    a.user_id,\r\n" + "                    b.email,\r\n"
			+ "                    b.mobile_number,\r\n" + "                    a.participated_amount,\r\n"
			+ "				    b.registered_on,\r\n" + "                    MAX(a.received_on) AS received_on\r\n"
			+ "                FROM\r\n" + "                    public.oxy_lenders_accepted_deals a\r\n"
			+ "                JOIN\r\n" + "                    public.user b ON a.user_id = b.id\r\n"
			+ "                WHERE\r\n"
			+ "                    DATE(a.received_on) BETWEEN :startDate  AND :endDate\r\n"
			+ "                    AND NOT EXISTS (\r\n" + "                        SELECT 1\r\n"
			+ "                        FROM public.oxy_lenders_accepted_deals AS previous_data\r\n"
			+ "                        WHERE\r\n" + "                            previous_data.user_id = a.user_id\r\n"
			+ "                            AND DATE(previous_data.received_on) < :startDate\r\n"
			+ "                    ) AND b.test_user = false\r\n" + "                GROUP BY\r\n"
			+ "                    a.user_id, b.email, b.mobile_number, a.participated_amount, b.registered_on\r\n"
			+ "            ) AS subquery\r\n" + "        GROUP BY\r\n"
			+ "            user_id, email, mobile_number, registered_on\r\n" + "    ) AS subquery_outer\r\n"
			+ "LEFT JOIN\r\n" + "    (\r\n" + "        SELECT\r\n" + "            user_id,\r\n"
			+ "            COALESCE(SUM(updation_amount), 0) AS updation_amount\r\n" + "        FROM\r\n"
			+ "            public.lenders_paticipation_updation\r\n" + "        WHERE\r\n"
			+ "            DATE(updated_on) BETWEEN :startDate AND :endDate\r\n" + "        GROUP BY\r\n"
			+ "            user_id\r\n" + "    ) AS up ON subquery_outer.user_id = up.user_id\r\n" + "GROUP BY\r\n"
			+ "    subquery_outer.user_id,\r\n" + "    subquery_outer.email,\r\n"
			+ "    subquery_outer.mobile_number,\r\n" + "	   subquery_outer.registered_on\r\n" + "ORDER BY\r\n"
			+ " subquery_outer.user_id DESC;\r\n", nativeQuery = true)
	List<Object[]> findByNewLenderParticipationAndUpdationAmount(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT\r\n" + "    SUM(total_outstanding_amount) AS total_sum_outstanding_amount\r\n" + "FROM (\r\n"
			+ "    SELECT\r\n" + "        id,\r\n" + "        SUM(outstanding_amount) AS total_outstanding_amount\r\n"
			+ "    FROM (\r\n" + "        -- Your existing subquery\r\n" + "        SELECT a.id,\r\n"
			+ "        COALESCE(b.participated_amount, 0) AS first,\r\n"
			+ "        COALESCE(SUM(c.updation_amount), 0) AS second,\r\n"
			+ "        COALESCE(d.transaction_amount, 0) AS wallet,\r\n"
			+ "        COALESCE(SUM(e.amount), 0) AS account,\r\n"
			+ "        (COALESCE(b.participated_amount, 0) + COALESCE(SUM(c.updation_amount), 0)) - (COALESCE(d.transaction_amount, 0) + COALESCE(SUM(e.amount), 0)) AS outstanding_amount\r\n"
			+ "        FROM public.oxy_borrowers_deals_information a\r\n"
			+ "        JOIN public.oxy_lenders_accepted_deals b ON a.id = b.deal_id\r\n"
			+ "        LEFT JOIN public.lenders_paticipation_updation c ON a.id = c.deal_id AND b.user_id = c.user_id\r\n"
			+ "        LEFT JOIN public.lender_scrow_wallet d ON a.id = d.deal_id AND b.user_id = d.user_id\r\n"
			+ "        LEFT JOIN public.lenders_returns e ON a.id = e.deal_id AND b.user_id = e.user_id AND e.status='INITIATED'\r\n"
			+ "            AND (e.amount_type='LENDERWITHDRAW' OR e.amount_type='LENDERPRINCIPAL')\r\n"
			+ "        WHERE b.user_id =:userId AND a.deal_type != 'EQUITY'\r\n"
			+ "            AND a.borrower_closing_status = 'NOTYETCLOSED'\r\n" + "        GROUP BY\r\n"
			+ "        a.id, b.participated_amount, d.transaction_amount\r\n" + "    HAVING\r\n"
			+ "        (COALESCE(b.participated_amount, 0) + COALESCE(SUM(c.updation_amount), 0)) - (COALESCE(d.transaction_amount, 0) + COALESCE(SUM(e.amount), 0)) > 0\r\n"
			+ "  ) AS subquery\r\n" + "    GROUP BY\r\n" + "        id\r\n" + ") AS total_sum_outstanding\r\n"
			+ "", nativeQuery = true)
	public Double getOutstandingAmount(@Param("userId") Integer userId);

	@Query(value = "SELECT SUM(total_amount) AS overall_total_amount\r\n" + "FROM (\r\n" + "    SELECT\r\n"
			+ "        COALESCE(SUM(participated_amount), 0) AS total_amount\r\n" + "    FROM\r\n"
			+ "        public.oxy_lenders_accepted_deals\r\n" + "    WHERE\r\n" + "        deal_id =:dealId\r\n"
			+ "    UNION\r\n" + "    SELECT\r\n" + "        COALESCE(SUM(updation_amount), 0) AS total_amount\r\n"
			+ "    FROM\r\n" + "        public.lenders_paticipation_updation\r\n" + "    WHERE\r\n"
			+ "        deal_id =:dealId\r\n" + ") AS subquery", nativeQuery = true)
	public Double getTotalPaticipation(@Param("dealId") Integer dealId);

	@Query(value = "SELECT \r\n"
			+ "   a.participated_amount + COALESCE(SUM(b.updation_amount), 0) AS total_amount_combined\r\n"
			+ "FROM \r\n" + "    public.oxy_lenders_accepted_deals a\r\n" + "LEFT JOIN \r\n"
			+ "    public.lenders_paticipation_updation b ON a.user_id = b.user_id AND a.deal_id = b.deal_id\r\n"
			+ "WHERE \r\n" + "    a.user_id =:userId AND a.deal_id =:dealId\r\n" + "GROUP BY \r\n"
			+ "    a.participated_amount", nativeQuery = true)

	public Double totalDealPaticipation(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT COUNT(DISTINCT subquery.id) AS id_count\r\n" + "FROM (\r\n" + "    SELECT\r\n"
			+ "        a.id,\r\n" + "        COALESCE(b.participated_amount, 0) AS first,\r\n"
			+ "        COALESCE(SUM(c.updation_amount), 0) AS second,\r\n"
			+ "        COALESCE(d.transaction_amount, 0) AS wallet,\r\n"
			+ "        COALESCE(SUM(e.amount), 0) AS account,\r\n"
			+ "        (COALESCE(b.participated_amount, 0) + COALESCE(SUM(c.updation_amount), 0)) - (COALESCE(d.transaction_amount, 0) + COALESCE(SUM(e.amount), 0)) AS outstanding_amount\r\n"
			+ "    FROM\r\n" + "        public.oxy_borrowers_deals_information a\r\n" + "    JOIN\r\n"
			+ "        public.oxy_lenders_accepted_deals b ON a.id = b.deal_id\r\n" + "    LEFT JOIN\r\n"
			+ "        public.lenders_paticipation_updation c ON a.id = c.deal_id AND b.user_id = c.user_id\r\n"
			+ "    LEFT JOIN\r\n"
			+ "        public.lender_scrow_wallet d ON a.id = d.deal_id AND b.user_id = d.user_id\r\n"
			+ "    LEFT JOIN\r\n"
			+ "        public.lenders_returns e ON a.id = e.deal_id AND b.user_id = e.user_id AND e.status='INITIATED' AND (e.amount_type='LENDERWITHDRAW' or e.amount_type='LENDERPRINCIPAL')\r\n"
			+ "    WHERE\r\n" + "        b.user_id =:userId\r\n" + "        AND a.deal_type != 'EQUITY'\r\n"
			+ "        AND a.borrower_closing_status = 'NOTYETCLOSED'\r\n" + "    GROUP BY\r\n"
			+ "        a.id, b.participated_amount, d.transaction_amount\r\n" + "    HAVING\r\n"
			+ "        (COALESCE(b.participated_amount, 0) + COALESCE(SUM(c.updation_amount), 0)) - (COALESCE(d.transaction_amount, 0) + COALESCE(SUM(e.amount), 0)) > 0\r\n"
			+ ") subquery", nativeQuery = true)
	public Integer activeDealUserParticipationCount(@Param("userId") Integer userId);

	@Query(value = "SELECT COUNT(DISTINCT subquery.id) AS id_count\r\n" + "FROM (\r\n" + "    SELECT\r\n"
			+ "        a.id,\r\n" + "        COALESCE(b.participated_amount, 0) AS first,\r\n"
			+ "        COALESCE(SUM(c.updation_amount), 0) AS second\r\n" + "    FROM\r\n"
			+ "        public.oxy_borrowers_deals_information a\r\n" + "    JOIN\r\n"
			+ "        public.oxy_lenders_accepted_deals b ON a.id = b.deal_id\r\n" + "    LEFT JOIN\r\n"
			+ "        public.lenders_paticipation_updation c ON a.id = c.deal_id AND b.user_id = c.user_id\r\n"
			+ "    WHERE\r\n" + "        b.user_id =:userId\r\n" + "        AND a.deal_type != 'EQUITY'\r\n"
			+ "    GROUP BY\r\n" + "        a.id, b.participated_amount\r\n" + ") subquery", nativeQuery = true)
	public Integer overralDealCount(@Param("userId") Integer userId);

	@Query(value = "WITH result_set AS (\r\n" + "    SELECT\r\n" + "        a.id,\r\n"
			+ "        COALESCE(b.participated_amount, 0) AS first,\r\n"
			+ "        COALESCE(SUM(c.updation_amount), 0) AS second\r\n" + "    FROM\r\n"
			+ "        public.oxy_borrowers_deals_information a\r\n" + "    JOIN\r\n"
			+ "        public.oxy_lenders_accepted_deals b ON a.id = b.deal_id\r\n" + "    LEFT JOIN\r\n"
			+ "        public.lenders_paticipation_updation c ON a.id = c.deal_id AND b.user_id = c.user_id\r\n"
			+ "    WHERE\r\n" + "        b.user_id =:userId\r\n" + "        AND a.deal_type != 'EQUITY'\r\n"
			+ "    GROUP BY\r\n" + "        a.id, b.participated_amount\r\n" + ")\r\n" + "SELECT\r\n"
			+ "    (SUM(COALESCE(first,0)) +\r\n" + "    SUM(COALESCE(second,0)))\r\n"
			+ "FROM result_set", nativeQuery = true)
	public Double overralParticipationAmount(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.oxy_lenders_accepted_deals WHERE fee_status='PENDING'and user_id>0 ORDER BY received_on desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<OxyLendersAcceptedDeals> findLenderFeeStatus(@Param("pageNo") Integer pageNo,
			@Param("pageSize") Integer pageSize);

	@Query(value = "SELECT count(DISTINCT a.user_id) \r\n"
			+ "FROM public.oxy_lenders_accepted_deals a JOIN public.user b ON a.user_id= b.id\r\n"
			+ "WHERE b.test_user = false;", nativeQuery = true)

	public Integer findByTotalActiveLenders();

	@Query(value = "SELECT  a.user_id FROM public.oxy_lenders_accepted_deals a left join public.user b on a.user_id=b.id where b.test_user=false group by a.user_id ORDER BY a.user_id desc", nativeQuery = true)
	public List<Integer> realLenders();

	@Query(value = "SELECT b.user_id, " + "COALESCE(SUM(a.life_time_waiver_limit), 0) AS lifeTimeWaiver, "
			+ "COALESCE(SUM(b.participated_Amount), 0) AS totalParticipationAmount, "
			+ "COALESCE(SUM(c.updation_Amount), 0) AS totalParticipationUpdation "
			+ "FROM public.oxy_borrowers_deals_information a "
			+ "LEFT JOIN public.oxy_lenders_accepted_deals b ON b.deal_id = a.id "
			+ "LEFT JOIN public.lenders_paticipation_updation c ON c.user_id = b.user_id AND c.deal_id = a.id "
			+ "WHERE a.life_time_waiver = true AND a.id = :dealId " + "GROUP BY b.user_id", nativeQuery = true)

	public List<Object[]> getTotalParticipationByUser(@Param("dealId") Integer dealId);

	@Query(value = "SELECT count(id) FROM public.oxy_lenders_accepted_deals where user_id=:userId", nativeQuery = true)
	public Integer findTotalDealsParticipated(@Param("userId") Integer userId);

	@Query(value = "select sum(participated_amount) from public.oxy_lenders_accepted_deals where user_id=:userId", nativeQuery = true)
	public Double findingSumOfAmountParticipatedUser(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(a.participated_amount) FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where user_id=:userId and b.deal_type='STUDENT' ", nativeQuery = true)
	public Double findLenderParticipationInStudentDeals(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(a.participated_amount) FROM public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where user_id=:userId and b.deal_type='EQUITY'", nativeQuery = true)
	public Double lenderEquityDeals(@Param("userId") Integer userId);

	@Query(value = "SELECT SUM(participated_amount)\r\n" + "FROM public.oxy_lenders_accepted_deals\r\n"
			+ "WHERE user_id = :userId \r\n" + "  AND DATE(received_on) >= DATE(:fromDate\r\n"
			+ ") AND DATE(received_on) <= DATE(:toDate\r\n" + ")\r\n" + "", nativeQuery = true)
	public Double getTotalDealParticipationAmountByUserId(@Param("userId") Integer userId,
			@Param("fromDate") String fromDate, @Param("toDate") String toDate);

	@Query(value = "SELECT SUM(a.participated_amount)\r\n" + "FROM public.oxy_lenders_accepted_deals a\r\n"
			+ "LEFT JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE a.deal_id = :dealId AND b.test_user = false;\r\n", nativeQuery = true)

	public Double findByActiveLenderAmount(@Param("dealId") Integer dealId);

	@Query(value = "SELECT SUM(a.updation_amount)\r\n" + "FROM public.lenders_paticipation_updation a\r\n"
			+ "LEFT JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE a.deal_id = :dealId AND b.test_user = false;\r\n", nativeQuery = true)

	public Double findByUpdationActiveLenderAmount(@Param("dealId") Integer dealId);

	@Query(value = "SELECT SUM(a.participated_amount)\r\n" + "FROM public.oxy_lenders_accepted_deals a\r\n"
			+ "LEFT JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE a.deal_id = :dealId AND a.group_id=0 AND b.test_user = false;\r\n", nativeQuery = true)

	public Double findByNewLendersAmount(@Param("dealId") Integer dealId);

	@Query(value = "SELECT COUNT(DISTINCT a.user_id) AS unique_user_count\r\n"
			+ "FROM public.oxy_lenders_accepted_deals a\r\n" + "JOIN public.user b ON a.user_id = b.id\r\n"
			+ "WHERE \r\n" + "    DATE(a.received_on) >= :startDate\r\n" + "    AND DATE(a.received_on) <= :endDate\r\n"
			+ "    AND b.test_user = false;\r\n", nativeQuery = true)

	public Integer findByTotalActiveLendersAdminModule(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT * FROM public.oxy_lenders_accepted_deals WHERE fee_status='PENDING'and user_id>0 ORDER BY received_on desc", nativeQuery = true)
	public List<OxyLendersAcceptedDeals> findLenderFeeStatusPending();

	@Query(value = "   SELECT\r\n"
			+ "    COALESCE(SUM(subquery_outer.participated_amount), 0) + COALESCE(SUM(up.updation_amount), 0) AS total_sum_both_amounts\r\n"
			+ "FROM\r\n" + "    (\r\n" + "        SELECT\r\n" + "            user_id,\r\n"
			+ "            COALESCE(SUM(participated_amount), 0) AS participated_amount\r\n" + "        FROM\r\n"
			+ "            (\r\n" + "                SELECT DISTINCT\r\n" + "                    a.user_id,\r\n"
			+ "                    a.participated_amount\r\n" + "                FROM\r\n"
			+ "                    public.oxy_lenders_accepted_deals a\r\n" + "                JOIN\r\n"
			+ "                    public.user b ON a.user_id = b.id\r\n" + "                WHERE\r\n"
			+ "                    DATE(a.received_on) = CURRENT_DATE\r\n" + "                    AND NOT EXISTS (\r\n"
			+ "                        SELECT 1\r\n"
			+ "                        FROM public.oxy_lenders_accepted_deals AS previous_data\r\n"
			+ "                        WHERE\r\n" + "                            previous_data.user_id = a.user_id\r\n"
			+ "                            AND DATE(previous_data.received_on) < CURRENT_DATE\r\n"
			+ "                    )\r\n" + "                    AND b.test_user = false\r\n"
			+ "                GROUP BY\r\n" + "                    a.user_id, a.participated_amount\r\n"
			+ "            ) AS subquery\r\n" + "        GROUP BY\r\n" + "            user_id\r\n"
			+ "    ) AS subquery_outer\r\n" + "LEFT JOIN\r\n" + "    (\r\n" + "        SELECT\r\n"
			+ "            user_id,\r\n" + "            COALESCE(SUM(updation_amount), 0) AS updation_amount\r\n"
			+ "        FROM\r\n" + "            public.lenders_paticipation_updation\r\n" + "        WHERE\r\n"
			+ "            DATE(updated_on) = CURRENT_DATE\r\n" + "        GROUP BY\r\n" + "            user_id\r\n"
			+ "    ) AS up ON subquery_outer.user_id = up.user_id;\r\n" + "", nativeQuery = true)
	public Double findByFirstTimeParticipatedAmountDailyWise();

	@Query(value = "SELECT\r\n" + "COUNT(DISTINCT subquery_outer.user_id) AS total_unique_users\r\n" + "FROM\r\n"
			+ "(\r\n" + "   SELECT\r\n" + "        user_id\r\n" + "FROM\r\n" + "       (\r\n"
			+ "            SELECT DISTINCT\r\n" + "               a.user_id,\r\n"
			+ "           a.participated_amount\r\n" + "           FROM\r\n"
			+ "               public.oxy_lenders_accepted_deals a\r\n" + "           JOIN\r\n"
			+ "                public.user b ON a.user_id = b.id\r\n" + "            WHERE\r\n"
			+ "             DATE(a.received_on) = CURRENT_DATE	\r\n" + "                AND NOT EXISTS (\r\n"
			+ "               SELECT 1\r\n"
			+ "                   FROM public.oxy_lenders_accepted_deals AS previous_data\r\n"
			+ "                  WHERE\r\n" + "                        previous_data.user_id = a.user_id\r\n"
			+ "           AND  DATE(previous_data.received_on) < CURRENT_DATE	\r\n"
			+ "               ) AND b.test_user = false\r\n" + "            GROUP BY\r\n"
			+ "               a.user_id, a.participated_amount\r\n" + "        ) AS subquery\r\n" + "GROUP BY\r\n"
			+ "        user_id\r\n" + ") AS subquery_outer\r\n" + "LEFT JOIN\r\n" + "(\r\n" + "    SELECT\r\n"
			+ "        user_id,\r\n" + "        COALESCE(SUM(updation_amount), 0) AS updation_amount\r\n"
			+ "    FROM\r\n" + "        public.lenders_paticipation_updation\r\n" + "    WHERE\r\n"
			+ "	      DATE(updated_on) = CURRENT_DATE  \r\n" + "\r\n" + "    GROUP BY\r\n" + "        user_id\r\n"
			+ ") AS up ON subquery_outer.user_id = up.user_id;\r\n" + " ", nativeQuery = true)

	public Integer findByFirstTimeParticipationCountDailyWise();

	@Query(value = "SELECT\r\n" + "		COUNT(DISTINCT subquery_outer.user_id) AS total_unique_users\r\n"
			+ "		FROM\r\n" + "		(\r\n" + "		   SELECT\r\n" + "		       user_id\r\n" + "		FROM\r\n"
			+ "		      (\r\n" + "		           SELECT DISTINCT\r\n" + "		              a.user_id,\r\n"
			+ "		          a.participated_amount\r\n" + "		           FROM\r\n"
			+ "		               public.oxy_lenders_accepted_deals a\r\n" + "		           JOIN\r\n"
			+ "		                public.user b ON a.user_id = b.id\r\n" + "		            WHERE\r\n"
			+ "                 DATE (received_on) >= CURRENT_DATE - INTERVAL '7 days'		              \r\n"
			+ "				  AND NOT EXISTS (\r\n" + "		               SELECT 1\r\n"
			+ "		                 FROM public.oxy_lenders_accepted_deals AS previous_data\r\n"
			+ "		                  WHERE\r\n"
			+ "		                        previous_data.user_id = a.user_id\r\n"
			+ "		          AND  DATE(previous_data.received_on) < CURRENT_DATE - INTERVAL '6 days'	\r\n"
			+ "		               ) AND b.test_user = false\r\n" + "	           GROUP BY\r\n"
			+ "		              a.user_id, a.participated_amount\r\n" + "	        ) AS subquery\r\n"
			+ "		GROUP BY\r\n" + "		        user_id\r\n" + "		) AS subquery_outer\r\n"
			+ "		LEFT JOIN\r\n" + "		(\r\n" + "		    SELECT\r\n" + "		       user_id,\r\n"
			+ "		       COALESCE(SUM(updation_amount), 0) AS updation_amount\r\n" + "		    FROM\r\n"
			+ "		       public.lenders_paticipation_updation\r\n" + "		  WHERE\r\n"
			+ "		      DATE(updated_on) >= CURRENT_DATE - INTERVAL '6 days'	  \r\n" + "	\r\n"
			+ "		    GROUP BY\r\n" + "	       user_id\r\n"
			+ "		) AS up ON subquery_outer.user_id = up.user_id;\r\n", nativeQuery = true)

	public Integer findByFirstTimeParticipationCountWeekly();

	@Query(value = "SELECT\r\n"
			+ "    COALESCE(SUM(subquery_outer.participated_amount), 0) + COALESCE(SUM(up.updation_amount), 0) AS total_sum_both_amounts\r\n"
			+ "FROM\r\n" + "    (\r\n" + "        SELECT\r\n" + "            user_id,\r\n"
			+ "            COALESCE(SUM(participated_amount), 0) AS participated_amount\r\n" + "        FROM\r\n"
			+ "            (\r\n" + "                SELECT\r\n" + "                    a.user_id,\r\n"
			+ "                    COALESCE(SUM(a.participated_amount), 0) AS participated_amount\r\n"
			+ "                FROM\r\n" + "                    public.oxy_lenders_accepted_deals a\r\n"
			+ "                JOIN\r\n" + "                    public.user b ON a.user_id = b.id\r\n"
			+ "                WHERE\r\n"
			+ "                    DATE(a.received_on) >= CURRENT_DATE - INTERVAL '6 days'\r\n"
			+ "                    AND NOT EXISTS (\r\n" + "                        SELECT 1\r\n"
			+ "                        FROM public.oxy_lenders_accepted_deals AS previous_data\r\n"
			+ "                        WHERE\r\n" + "                            previous_data.user_id = a.user_id\r\n"
			+ "                            AND DATE(previous_data.received_on) < CURRENT_DATE - INTERVAL '7 days'\r\n"
			+ "                    )\r\n" + "                    AND b.test_user = false\r\n"
			+ "                GROUP BY\r\n" + "                    a.user_id\r\n" + "            ) AS subquery\r\n"
			+ "        GROUP BY\r\n" + "            user_id\r\n" + "    ) AS subquery_outer\r\n" + "LEFT JOIN\r\n"
			+ "    (\r\n" + "        SELECT\r\n" + "            user_id,\r\n"
			+ "            COALESCE(SUM(updation_amount), 0) AS updation_amount\r\n" + "        FROM\r\n"
			+ "            public.lenders_paticipation_updation\r\n" + "        WHERE\r\n"
			+ "            DATE(updated_on) >= CURRENT_DATE - INTERVAL '6 days'\r\n" + "        GROUP BY\r\n"
			+ "            user_id\r\n" + "    ) AS up ON subquery_outer.user_id = up.user_id;\r\n", nativeQuery = true)
	public Double findByFirstTimeParticipationAmountWeekly();

	@Query(value = "SELECT\r\n"
			+ "    COALESCE(SUM(subquery_outer.participated_amount), 0) + COALESCE(SUM(up.updation_amount), 0) AS total_sum_both_amounts\r\n"
			+ "FROM\r\n" + "    (\r\n" + "        SELECT\r\n" + "            user_id,\r\n"
			+ "            COALESCE(SUM(participated_amount), 0) AS participated_amount\r\n" + "        FROM\r\n"
			+ "            (\r\n" + "                SELECT DISTINCT\r\n" + "                    a.user_id,\r\n"
			+ "                    a.participated_amount\r\n" + "                FROM\r\n"
			+ "                    public.oxy_lenders_accepted_deals a\r\n" + "                JOIN\r\n"
			+ "                    public.user b ON a.user_id = b.id\r\n" + "                WHERE\r\n"
			+ "                    date_trunc('month', a.received_on) = date_trunc('month', CURRENT_DATE)\r\n"
			+ "                    AND NOT EXISTS (\r\n" + "                        SELECT 1\r\n"
			+ "                        FROM public.oxy_lenders_accepted_deals AS previous_data\r\n"
			+ "                        WHERE\r\n" + "                            previous_data.user_id = a.user_id\r\n"
			+ "                            AND DATE(previous_data.received_on) < date_trunc('month', CURRENT_DATE)\r\n"
			+ "                    )\r\n" + "                    AND b.test_user = false\r\n"
			+ "                GROUP BY\r\n" + "                    a.user_id, a.participated_amount\r\n"
			+ "            ) AS subquery\r\n" + "        GROUP BY\r\n" + "            user_id\r\n"
			+ "    ) AS subquery_outer\r\n" + "LEFT JOIN\r\n" + "    (\r\n" + "        SELECT\r\n"
			+ "            user_id,\r\n" + "            COALESCE(SUM(updation_amount), 0) AS updation_amount\r\n"
			+ "        FROM\r\n" + "            public.lenders_paticipation_updation\r\n" + "        WHERE\r\n"
			+ "            date_trunc('month', updated_on) = date_trunc('month', CURRENT_DATE)\r\n"
			+ "        GROUP BY\r\n" + "            user_id\r\n"
			+ "    ) AS up ON subquery_outer.user_id = up.user_id;\r\n" + "", nativeQuery = true)

	public Double findByFirstTimePaticipationAmount();

	@Query(value = "SELECT\r\n" + "    COUNT(DISTINCT subquery_outer.user_id) AS total_unique_users\r\n" + "FROM\r\n"
			+ "    (\r\n" + "        SELECT\r\n" + "            user_id\r\n" + "        FROM\r\n" + "            (\r\n"
			+ "                SELECT DISTINCT\r\n" + "                    a.user_id,\r\n"
			+ "                    a.participated_amount\r\n" + "                FROM\r\n"
			+ "                    public.oxy_lenders_accepted_deals a\r\n" + "                JOIN\r\n"
			+ "                    public.user b ON a.user_id = b.id\r\n" + "                WHERE\r\n"
			+ "                    date_trunc('month', a.received_on) = date_trunc('month', CURRENT_DATE)\r\n"
			+ "                    AND NOT EXISTS (\r\n" + "                        SELECT 1\r\n"
			+ "                        FROM public.oxy_lenders_accepted_deals AS previous_data\r\n"
			+ "                        WHERE\r\n" + "                            previous_data.user_id = a.user_id\r\n"
			+ "                            AND DATE(previous_data.received_on) < date_trunc('month', CURRENT_DATE)\r\n"
			+ "                    ) AND b.test_user = false\r\n" + "                GROUP BY\r\n"
			+ "                    a.user_id, a.participated_amount\r\n" + "            ) AS subquery\r\n"
			+ "        GROUP BY\r\n" + "            user_id\r\n" + "    ) AS subquery_outer\r\n" + "LEFT JOIN\r\n"
			+ "    (\r\n" + "        SELECT\r\n" + "            user_id,\r\n"
			+ "            COALESCE(SUM(updation_amount), 0) AS updation_amount\r\n" + "        FROM\r\n"
			+ "            public.lenders_paticipation_updation\r\n" + "        WHERE\r\n"
			+ "            date_trunc('month', updated_on) = date_trunc('month', CURRENT_DATE)\r\n"
			+ "        GROUP BY\r\n" + "            user_id\r\n"
			+ "    ) AS up ON subquery_outer.user_id = up.user_id;\r\n", nativeQuery = true)
	public Integer findByFirstTimeParticipationLendersCount();

	@Query(value = "SELECT *\r\n" + "FROM public.oxy_lenders_accepted_deals a\r\n"
			+ "JOIN public.oxy_borrowers_deals_information b ON a.deal_id = b.id\r\n" + "WHERE a.user_id = :userId\r\n"
			+ "AND a.fee_status = 'PENDING'\r\n" + "ORDER BY a.id ASC\r\n" + "LIMIT 1;\r\n" + "", nativeQuery = true)
	public OxyLendersAcceptedDeals findUserExistingDealsFeeStatus(@Param("userId") Integer userId);

	@Query(value = "SELECT DISTINCT \r\n" + "    a.user_id,\r\n" + "    b.deal_name,\r\n"
			+ "    CONCAT(c.first_name, ' ', c.last_name) AS full_name,\r\n" + "    d.mobile_number,\r\n"
			+ "    a.received_on,\r\n" + "    a.deal_id,\r\n" + "    d.email\r\n" + "FROM \r\n"
			+ "    public.oxy_lenders_accepted_deals a\r\n" + "JOIN \r\n"
			+ "    public.oxy_borrowers_deals_information b ON b.id = a.deal_id\r\n" + "JOIN \r\n"
			+ "    public.user_personal_details c ON a.user_id = c.user_id\r\n" + "JOIN \r\n"
			+ "    public.user d ON a.user_id = d.id\r\n" + "WHERE \r\n" + "    d.test_user = 'false' \r\n"
			+ "    AND b.deal_type != 'TEST'\r\n", nativeQuery = true)
	public List<Object[]> withoutBankDeatilParticipatedUsers();

	@Query(value = "SELECT \r\n"
			+ "    u.id AS user_id, \r\n"
			+ "    u.email, \r\n"
			+ "    u.mobile_number, \r\n"
			+ "    u.state, \r\n"
			+ "    u.city, \r\n"
			+ "    u.pincode, \r\n"
			+ "    u.registered_on, \r\n"
			+ "    COALESCE(a.total_participation_amount, 0) + COALESCE(b.total_updation_amount, 0) AS total_participation_amount,\r\n"
			+ "    MIN(d.received_on) AS first_participation_date\r\n"
			+ "FROM \r\n"
			+ "    public.user u \r\n"
			+ "LEFT JOIN \r\n"
			+ "    (SELECT \r\n"
			+ "        user_id, \r\n"
			+ "        SUM(COALESCE(participated_amount, 0)) AS total_participation_amount \r\n"
			+ "    FROM \r\n"
			+ "        public.oxy_lenders_accepted_deals \r\n"
			+ "    GROUP BY \r\n"
			+ "        user_id) a ON u.id = a.user_id \r\n"
			+ "LEFT JOIN \r\n"
			+ "    (SELECT \r\n"
			+ "        user_id, \r\n"
			+ "        SUM(COALESCE(updation_amount, 0)) AS total_updation_amount \r\n"
			+ "    FROM \r\n"
			+ "        public.lenders_paticipation_updation \r\n"
			+ "    GROUP BY \r\n"
			+ "        user_id) b ON u.id = b.user_id \r\n"
			+ "LEFT JOIN \r\n"
			+ "    public.oxy_lenders_accepted_deals d ON u.id = d.user_id\r\n"
			+ "WHERE \r\n"
			+ "    u.primary_type != 'ADMIN' \r\n"
			+ "    AND u.id != 6680 \r\n"
			+ "    AND a.user_id IS NOT NULL \r\n"
			+ "    AND d.received_on BETWEEN :startDate AND :endDate \r\n"
			+ "GROUP BY \r\n"
			+ "    u.id, u.email, u.mobile_number, u.state, u.city, u.pincode, u.registered_on, a.total_participation_amount, b.total_updation_amount\r\n"
			+ "ORDER BY \r\n"
			+ "    total_participation_amount DESC \r\n"
			+ "LIMIT :limit\r\n"
			,nativeQuery = true)
	
	public List<Object[]> getTotalParticipationByUsers(@Param("limit") int limit ,@Param("startDate") Date startDate,
		    @Param("endDate") Date endDate);
	
	
	

	@Query(value = "select * from public.oxy_lenders_accepted_deals where deal_id=:id order by id desc limit 1", nativeQuery = true)
	public OxyLendersAcceptedDeals findDealIdData(@Param("id") Integer id);

	@Query(value = "SELECT\r\n" + "    SUM(total_outstanding_amount) AS total_sum_outstanding_amount,\r\n"
			+ "    SUM(total_transaction_amount) AS total_sum_transaction_amount,\r\n"
			+ "    SUM(total_participation_amount) AS total_sum_participation_amount\r\n" + "FROM (\r\n"
			+ "    SELECT\r\n" + "        id,\r\n" + "        SUM(outstanding_amount) AS total_outstanding_amount,\r\n"
			+ "        SUM(transaction_amount) AS total_transaction_amount,\r\n"
			+ "        SUM(participation_amount) AS total_participation_amount\r\n" + "    FROM (\r\n"
			+ "        -- Your existing subquery\r\n" + "        SELECT\r\n" + "            a.id,\r\n"
			+ "            COALESCE(b.participated_amount, 0) AS first,\r\n"
			+ "            COALESCE(SUM(c.updation_amount), 0) AS second,\r\n"
			+ "            COALESCE(d.transaction_amount, 0) AS wallet,\r\n"
			+ "            COALESCE(SUM(e.amount), 0) AS account,\r\n"
			+ "            (COALESCE(b.participated_amount, 0) + COALESCE(SUM(c.updation_amount), 0)) - (COALESCE(d.transaction_amount, 0) + COALESCE(SUM(e.amount), 0)) AS outstanding_amount,\r\n"
			+ "            COALESCE(d.transaction_amount, 0) + COALESCE(SUM(e.amount), 0) AS transaction_amount,\r\n"
			+ "            COALESCE(b.participated_amount, 0) + COALESCE(SUM(c.updation_amount), 0) AS participation_amount\r\n"
			+ "        FROM\r\n" + "            public.oxy_borrowers_deals_information a\r\n" + "        JOIN\r\n"
			+ "            public.oxy_lenders_accepted_deals b ON a.id = b.deal_id\r\n" + "        LEFT JOIN\r\n"
			+ "            public.lenders_paticipation_updation c ON a.id = c.deal_id AND b.user_id = c.user_id\r\n"
			+ "        LEFT JOIN\r\n"
			+ "            public.lender_scrow_wallet d ON a.id = d.deal_id AND b.user_id = d.user_id\r\n"
			+ "        LEFT JOIN\r\n"
			+ "            public.lenders_returns e ON a.id = e.deal_id AND b.user_id = e.user_id AND e.status='INITIATED' AND (e.amount_type='LENDERWITHDRAW' or e.amount_type='LENDERPRINCIPAL')\r\n"
			+ "        WHERE\r\n" + "             a.id =:id\r\n" + "            AND a.deal_type != 'EQUITY'\r\n"
			+ "            AND a.borrower_closing_status = 'NOTYETCLOSED'\r\n" + "        GROUP BY\r\n"
			+ "            a.id, b.participated_amount, d.transaction_amount\r\n" + "    ) AS subquery\r\n"
			+ "    GROUP BY\r\n" + "        id\r\n" + ") AS total_sum_outstanding;\r\n" + "", nativeQuery = true)
	public List<Object[]> getLenderTotalParticipationAndWithrawalAmount(@Param("id") Integer id);

	@Query(value = "SELECT " + "a.user_id, " + "u.email, " + "u.mobile_number, " + "u.state, " + "u.city, "
			+ "u.pincode, "
			+ "COALESCE(a.total_participation_amount, 0) + COALESCE(b.total_updation_amount, 0) AS total_participation_amount "
			+ "FROM " + "public.user u " + "LEFT JOIN " + "(SELECT " + "user_id, "
			+ "SUM(COALESCE(participated_amount, 0)) AS total_participation_amount " + "FROM "
			+ "public.oxy_lenders_accepted_deals " + "GROUP BY " + "user_id) a ON u.id = a.user_id " + "LEFT JOIN "
			+ "(SELECT " + "user_id, " + "SUM(COALESCE(updation_amount, 0)) AS total_updation_amount " + "FROM "
			+ "public.lenders_paticipation_updation " + "GROUP BY " + "user_id) b ON u.id = b.user_id " + "WHERE "
			+ "u.primary_type != 'ADMIN' " + "AND u.id != 6680 " + "AND a.user_id IS NOT NULL "
			+ "AND u.test_user = false " + "ORDER BY " + "u.id ASC", nativeQuery = true)
	public List<Object[]> getTotalParticipationsByUsers();

	@Query(value = "SELECT *\r\n" + "FROM public.user a\r\n"
			+ "JOIN public.oxy_lenders_accepted_deals b ON a.id = b.user_id\r\n"
			+ "JOIN public.lender_renewal_details c ON a.id = c.user_id\r\n"
			+ "WHERE c.validity_date >= '2030-01-01'\r\n" + "AND c.renewal_status = 'NotYetRenewed'\r\n"
			+ "AND a.id = :id \r\n" + "ORDER BY b.id \r\n" + "LIMIT 1;", nativeQuery = true)
	public OxyLendersAcceptedDeals findNormalUsers(@Param("id") Integer id);

	@Query(value = "SELECT * \r\n"
			+ "FROM public.oxy_lenders_accepted_deals \r\n"
			+ "WHERE received_on >= :startDate \r\n"
			+ "  AND received_on <= :endDate \r\n"
			+ "  AND user_id = :id \r\n"
			+ "LIMIT 1;", nativeQuery = true)
	public OxyLendersAcceptedDeals findByTotalUsers(@Param("id")Integer id,@Param("startDate") Date startDate,
		    @Param("endDate") Date endDate);

	
	
	 @Query(value = "SELECT " +
	            "a.user_id, " +
	            "u.email, " +
	            "u.mobile_number, " +
	            "u.state, " +
	            "u.city, " +
	            "u.pincode, " +
	            "COALESCE(a.total_participation_amount, 0) + COALESCE(b.total_updation_amount, 0) AS total_participation_amount " +
	            "FROM " +
	            "public.user u " +
	            "LEFT JOIN " +
	            "(SELECT " +
	            "user_id, " +
	            "SUM(COALESCE(participated_amount, 0)) AS total_participation_amount " +
	            "FROM " +
	            "public.oxy_lenders_accepted_deals " +
	            "GROUP BY " +
	            "user_id) a ON u.id = a.user_id " +
	            "LEFT JOIN " +
	            "(SELECT " +
	            "user_id, " +
	            "SUM(COALESCE(updation_amount, 0)) AS total_updation_amount " +
	            "FROM " +
	            "public.lenders_paticipation_updation " +
	            "GROUP BY " +
	            "user_id) b ON u.id = b.user_id " +
	            "WHERE " +
	            "u.primary_type != 'ADMIN' " +
	            "AND u.id != 6680 " +
	            "AND a.user_id IS NOT NULL " +
	            "AND u.test_user = false " +  // Add a space before ORDER BY
	            "ORDER BY " +
	            "u.id ASC "+ "LIMIT :pageSize OFFSET :pageNo", nativeQuery = true)
	    List<Object[]> getTotalParticipationByUsers1(
	    		@Param("pageNo") int pageNo,
                @Param("pageSize") int pageSize);

	    
	  @Query(value = "SELECT COUNT(*) " +
	            "FROM public.user u " +
	            "LEFT JOIN ( " +
	            "    SELECT user_id " +
	            "    FROM public.oxy_lenders_accepted_deals " +
	            "    GROUP BY user_id " +
	            ") a ON u.id = a.user_id " +
	            "LEFT JOIN ( " +
	            "    SELECT user_id " +
	            "    FROM public.lenders_paticipation_updation " +
	            "    GROUP BY user_id " +
	            ") b ON u.id = b.user_id " +
	            "WHERE u.primary_type != 'ADMIN' " +
	            "AND u.id != 6680 " +
	            "AND a.user_id IS NOT NULL " +
	            "AND u.test_user = false", nativeQuery = true)
	    public int totalCoutOfactiveLenders();



}
