package com.oxyloans.repository.borrowerdealinformationrepo;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import com.oxyloans.entity.borrowersdealsinformation.OxyBorrowersDealsInformation;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.borrowersdealsinformation.OxyBorrowersDealsInformation.DealParticipationStatus;

@Repository
public interface OxyBorrowersDealsInformationRepo
		extends PagingAndSortingRepository<OxyBorrowersDealsInformation, Integer>,
		JpaSpecificationExecutor<OxyBorrowersDealsInformation> {

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where id=:id", nativeQuery = true)
	public OxyBorrowersDealsInformation findDealAlreadyGiven(@Param("id") Integer id);

	@Query(value = "SELECT id,deal_name,borrower_name,deal_amount,funds_acceptance_start_date,funds_acceptance_end_date,borrower_rateofinterest,deal_link,duration,whatapp_reponse_link,link_sent_to_lenders_group,loan_active_date,whatapp_message_to_lenders,paticipation_limit_to_lenders,borrower_closing_status,participation_lender_type,minimum_paticipation_amount,deal_achieved_date,emi_end_date,borrowers_ids_mapped_to_deal,any_time_withdrawal,roi_for_withdrawal FROM public.oxy_borrowers_deals_information where deal_type='NORMAL' and deal_paticipation_status=:dealStatus order by id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getListOfDealsInformationForNormalDeals(@Param("pageNo") int pageNo,
			@Param("pageSize") int pageSize, @Param("dealStatus") String dealStatus);

	@Query(value = "SELECT count(id) FROM public.oxy_borrowers_deals_information where deal_type='NORMAL' and deal_paticipation_status=:dealStatus", nativeQuery = true)
	public Integer getListOfDealsInformationCountForNormalDeals(@Param("dealStatus") String dealStatus);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where CURRENT_DATE<= funds_acceptance_end_date and id=:id", nativeQuery = true)
	public OxyBorrowersDealsInformation toCheckDealEndDateCompleted(@Param("id") Integer id);

	@Query(value = "SELECT deal_name FROM public.oxy_borrowers_deals_information", nativeQuery = true)
	public List<String> getListOfDealsNames();

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where deal_name=:dealName", nativeQuery = true)
	public OxyBorrowersDealsInformation findByDealName(@Param("dealName") String dealName);

	@Query(value = "select * from public.oxy_borrowers_deals_information where whatapp_chatid=:chatId", nativeQuery = true)
	public OxyBorrowersDealsInformation findbyWhatsAppChatId(@Param("chatId") String chatId);

	@Query(value = "select id,funds_acceptance_start_date,funds_acceptance_end_date,deal_name,whatapp_chatid,link_sent_to_lenders_group from \r\n"
			+ "public.oxy_borrowers_deals_information where deal_status='NOTATACHIEVED' order by id", nativeQuery = true)
	public List<Object[]> findingListOfDeals();

	@Query(value = "select deal_amount from public.oxy_borrowers_deals_information where id=:dealId", nativeQuery = true)
	public BigInteger getDealAmountForDeal(@Param("dealId") Integer dealId);

	@Query(value = "SELECT count(id) FROM public.oxy_borrowers_deals_information where borrower_closing_status='NOTYETCLOSED'", nativeQuery = true)
	public Integer getListOfDealsInformationCount();

	@Query(value = "SELECT id,deal_name,borrower_name,deal_amount,funds_acceptance_start_date,funds_acceptance_end_date,borrower_rateofinterest,deal_link,duration,whatapp_reponse_link,link_sent_to_lenders_group,loan_active_date,whatapp_message_to_lenders,paticipation_limit_to_lenders,borrower_closing_status,date(borrower_closed_date),oxy_loan_request_id,participation_lender_type,minimum_paticipation_amount,deal_achieved_date,borrowers_ids_mapped_to_deal,any_time_withdrawal,roi_for_withdrawal FROM public.oxy_borrowers_deals_information where deal_type=:dealName and deal_paticipation_status=:dealStatus order by id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getListOfDealsInformationForEquityDeals(@Param("pageNo") int pageNo,
			@Param("pageSize") int pageSize, @Param("dealStatus") String dealStatus,
			@Param("dealName") String dealName);

	@Query(value = "SELECT count(id) FROM public.oxy_borrowers_deals_information where deal_type=:dealName and deal_paticipation_status=:dealStatus", nativeQuery = true)
	public Integer getListOfDealsInformationCountForEquityDeals(@Param("dealStatus") String dealStatus,
			@Param("dealName") String dealName);

	@Query(value = "select * from public.oxy_borrowers_deals_information where borrower_closing_status='NOTYETCLOSED' order by id limit :pageSize offset :pageNo", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getListOfDeals(@Param("pageNo") int pageNo,
			@Param("pageSize") int pageSize);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where deal_type='EQUITY'", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getEquityDealsList();

	@Query(value = "SELECT count(id) FROM public.oxy_borrowers_deals_information where deal_type='EQUITY'", nativeQuery = true)
	public Integer getListOfEquityDeals();

	@Query(value = "select * from public.oxy_borrowers_deals_information where id=:dealId", nativeQuery = true)
	public OxyBorrowersDealsInformation findBydealId(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where deal_type=:type", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getDealsListBasedOnDealType(@Param("type") String type);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where funds_acceptance_start_date>='2021-10-04' and id=:dealId", nativeQuery = true)
	public OxyBorrowersDealsInformation toGetCalculationType(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where borrower_closing_status='NOTYETCLOSED' order by id desc", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getListOfRunningDeals();

	@Query(value = "SELECT count(id) FROM public.oxy_borrowers_deals_information where borrower_closing_status='NOTYETCLOSED' and deal_type=:dealType", nativeQuery = true)
	public Integer runningDealsCountByType(@Param("dealType") String dealType);

	@Query(value = "SELECT sum(deal_amount) FROM public.oxy_borrowers_deals_information where borrower_closing_status='NOTYETCLOSED'  and deal_type=:dealType", nativeQuery = true)
	public BigInteger getTotalRunningDealsAmountByType(@Param("dealType") String dealType);

	@Query(value = "SELECT a.id FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id where b.user_id=:userId and a.deal_type!='EQUITY'", nativeQuery = true)
	public List<Integer> getListOfActiveDealIds(@Param("userId") Integer userId);

	@Query(value = "SELECT a.id FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id where a.borrower_closing_status='CLOSED' and b.user_id=:userId and a.deal_type!='EQUITY'", nativeQuery = true)
	public List<Integer> getListOfClosedDealIds(@Param("userId") Integer userId);

	@Query(value = "SELECT count(a.id) FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id where a.borrower_closing_status='NOTYETCLOSED' and b.user_id=:userId and a.deal_type!='EQUITY'", nativeQuery = true)
	public Integer getActiveDealIdsCount(@Param("userId") Integer userId);

	@Query(value = "SELECT count(a.id) FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id where a.borrower_closing_status='CLOSED' and b.user_id=:userId and a.deal_type!='EQUITY'", nativeQuery = true)
	public Integer getClosedDealIdsCount(@Param("userId") Integer userId);

	/*
	 * @Query(value =
	 * "select * from public.oxy_borrowers_deals_information where \r\n" +
	 * "(extract(day from loan_active_date)>=:nextDate1 and extract(day from loan_active_date)<=:nextDate)\r\n"
	 * +
	 * "and (extract(month from received_on)!=:currentMonth or extract(year from received_on)!=:currentYear)\r\n"
	 * +
	 * "and borrower_closing_status!='CLOSED' and deal_type!='EQUITY' and deal_type!='TEST' order by id"
	 * , nativeQuery = true) public List<OxyBorrowersDealsInformation>
	 * findingListOfDealsBasedOnNextDateForInterestPay(
	 * 
	 * @Param("nextDate") int nextDate, @Param("nextDate1") int nextDate1,
	 * 
	 * @Param("currentMonth") Integer currentMonth, @Param("currentYear") Integer
	 * currentYear);
	 */

	@Query(value = "select * from public.oxy_borrowers_deals_information where (extract(day from loan_active_date)=:nextDate)\r\n"
			+ "and (extract(month from received_on)!=:currentMonth or extract(year from received_on)!=:currentYear)\r\n"
			+ "and borrower_closing_status!='CLOSED' and deal_type!='EQUITY' and deal_type!='TEST' order by id", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> findingListOfDealsBasedOnNextDateForInterestPay(
			@Param("nextDate") int nextDate, @Param("currentMonth") Integer currentMonth,
			@Param("currentYear") Integer currentYear);

	@Query(value = "select deal_name from public.oxy_borrowers_deals_information where id=:dealId", nativeQuery = true)
	public String findingDealNameBasedOnId(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where borrower_closing_status='CLOSED' and borrower_closed_date is not null order by borrower_closed_date desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getTotalClosedDeals(@Param("pageNo") Integer pageNo,
			@Param("pageSize") Integer pageSize);

	@Query(value = "SELECT count(id) FROM public.oxy_borrowers_deals_information where borrower_closing_status='CLOSED' and borrower_closed_date is not null", nativeQuery = true)
	public Integer getTotalClosedDealIdsCount();

	@Query(value = "SELECT a.id,a.deal_name,a.borrower_closing_status,a.borrower_closed_date FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id where a.borrower_closing_status='CLOSED' and a.borrower_closed_date is not null and b.user_id=:userId order by a.borrower_closed_date desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getLenderPaticipatedClosedDealInfo(@Param("userId") Integer userId,
			@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "SELECT count(a.id) FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id where a.borrower_closing_status='NOTYETCLOSED' and b.user_id=:userId and a.deal_type='EQUITY'", nativeQuery = true)
	public Integer getActiveEquityDealIdsCount(@Param("userId") Integer userId);

	@Query(value = "SELECT a.id FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id where a.borrower_closing_status='NOTYETCLOSED' and b.user_id=:userId and a.deal_type='EQUITY'", nativeQuery = true)
	public List<Integer> getListOfActiveEquityDealIds(@Param("userId") Integer userId);

	@Query(value = "select * from public.oxy_borrowers_deals_information where to_char(loan_active_date,'yyyy')<=:year and borrower_closing_status='NOTYETCLOSED' and deal_type!='TEST'", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> findingListOfDealsBasedOnDealActiveMonthAndYear(
			@Param("year") String year);

	@Query(value = "select * from public.oxy_borrowers_deals_information where id=:dealId", nativeQuery = true)
	public OxyBorrowersDealsInformation findingDealName(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * from public.oxy_borrowers_deals_information where id=:dealId", nativeQuery = true)
	public OxyBorrowersDealsInformation findingDealInfo(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where borrower_closing_status='NOTYETCLOSED' and deal_type=:dealType order by id desc", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getListOfRunningDealsBasedOnDealType(@Param("dealType") String dealType);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where upper(deal_name)=upper(:dealName) limit 1", nativeQuery = true)
	public OxyBorrowersDealsInformation dealNameAlreadyGiven(@Param("dealName") String dealName);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where TO_CHAR(received_on,'YYYY-MM-DD')>='2022-03-18' and id=:dealId", nativeQuery = true)
	public OxyBorrowersDealsInformation dealGroupUpdationStatus(@Param("dealId") Integer dealId);

	@Query(value = "SELECT id FROM public.oxy_borrowers_deals_information where deal_paticipation_status='NOTATACHIEVED' order by id desc", nativeQuery = true)
	public List<Integer> dealsPaticipationNotClosed();

	@Query(value = "select * from public.oxy_borrowers_deals_information where (to_char(loan_active_date,'yyyy')<=:year and borrower_closing_status='NOTYETCLOSED') and ((EXTRACT('day' FROM loan_active_date) BETWEEN (EXTRACT('day' FROM current_timestamp)-8) AND (EXTRACT('day' FROM current_timestamp)+5)));", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> findingListOfDealsBasedOnDealActiveMonthAndYear1(
			@Param("year") String year);

	@Query(value = "select * from public.oxy_borrowers_deals_information where to_char(loan_active_date,'yyyy')<=:year and borrower_closing_status='NOTYETCLOSED' order by EXTRACT('day' FROM loan_active_date) limit :pageSize offset :pageNo", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> findingListOfDealsBasedOnDealActiveMonthAndYearWithPagination(
			@Param("year") String year, @Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "select count(*) from public.oxy_borrowers_deals_information where to_char(loan_active_date,'yyyy')<=:year and borrower_closing_status='NOTYETCLOSED'", nativeQuery = true)
	public Integer countOfDealsBasedOnDealActiveMonthAndYear(String year);

	@Query(value = "SELECT a.id,a.deal_name,a.deal_amount,a.funds_acceptance_start_date,date(a.borrower_closed_date),count(b.id) FROM public.oxy_borrowers_deals_information a join public.oxy_lenders_accepted_deals b on a.id=b.deal_id where borrower_closing_status='CLOSED' and borrower_closed_date is not null and deal_subtype='ASSET' group by a.id,a.deal_name,a.deal_amount,a.funds_acceptance_start_date,date(a.borrower_closed_date) limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> listAssertBasedDeals(@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

	@Query(value = "SELECT count(id) FROM public.oxy_borrowers_deals_information where borrower_closing_status='CLOSED' and borrower_closed_date is not null and deal_subtype='ASSET'", nativeQuery = true)
	public Integer assertBasedDealsCount();

	@Query(value = "SELECT id,deal_amount,deal_name FROM public.oxy_borrowers_deals_information where deal_subtype='STUDENT' and agreements_status='NOTYETGENERATED' and deal_type='NORMAL' and funds_acceptance_start_date>='2022-08-17' and length(borrowers_ids_mapped_to_deal)>0", nativeQuery = true)
	public List<Object[]> agreementPendingForStudentDeals();

	@Query(value = "SELECT coalesce(sum(participated_amount),0)+(select coalesce(sum(updation_amount),0) from public.lenders_paticipation_updation where deal_id=:dealId) FROM public.oxy_lenders_accepted_deals where deal_id=:dealId", nativeQuery = true)
	public Double paticipatedDealAmount(@Param("dealId") Integer dealId);

	@Query(value = "select b.deal_name,b.loan_active_date,a.participated_amount,a.lender_returns_type,\r\n"
			+ "a.received_on,b.funds_acceptance_start_date,b.duration,a.rateofinterest from\r\n"
			+ "public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b \r\n"
			+ "on a.deal_id=b.id where b.borrower_closing_status='NOTYETCLOSED' \r\n"
			+ "and a.user_id=:userId and deal_type!='EQUITY' order by b.id asc", nativeQuery = true)
	public List<Object[]> findingListOfRunningDeals(@Param("userId") Integer userId);

	@Query(value = "(select b.deal_name,b.loan_active_date,a.participated_amount,a.received_on,\r\n"
			+ " b.funds_acceptance_start_date,b.duration,a.deal_id from\r\n"
			+ " public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b \r\n"
			+ " on a.deal_id=b.id where b.borrower_closing_status='NOTYETCLOSED' and \r\n"
			+ " a.user_id=:userId and deal_type!='EQUITY' order by a.deal_id ) union  all\r\n"
			+ " (select b.deal_name,b.loan_active_date,a.updation_amount,a.updated_on,\r\n"
			+ "  b.funds_acceptance_start_date,b.duration,a.deal_id from \r\n"
			+ "  public.lenders_paticipation_updation a join public.oxy_borrowers_deals_information b\r\n"
			+ "  on a.deal_id=b.id where a.user_id=:userId and  b.deal_type!='EQUITY' and\r\n"
			+ "  b.borrower_closing_status='NOTYETCLOSED' order by a.deal_id) order by deal_id\r\n"
			+ "  ", nativeQuery = true)
	public List<Object[]> findingListOfDeals(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information o where borrower_closing_status='NOTYETCLOSED'   \r\n"
			+ "and age(loan_active_date)>='0' and extract(year from age(loan_active_date))*12+extract(month from age(loan_active_date))+1>=o.duration  order by id desc", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> findDurationCompletedDeals();

	@Query(value = "select a.deal_id from public.oxy_lenders_accepted_deals a join \r\n"
			+ "public.oxy_borrowers_deals_information b on a.deal_id=b.id\r\n"
			+ "where b.borrower_closing_status='NOTYETCLOSED' and \r\n"
			+ "a.user_id=:userId and deal_type!='EQUITY' order by a.deal_id", nativeQuery = true)
	public List<Integer> findingListOfParticipatedDealsForUser(@Param("userId") Integer userId);

	@Query(value = "select * from public.oxy_borrowers_deals_information where to_char(loan_active_date,'yyyy')<=:year and borrower_closing_status='NOTYETCLOSED' and (EXTRACT('day' FROM loan_active_date)>=:startDate and (EXTRACT('day' FROM loan_active_date)<=:endDate)) and deal_type!='TEST'", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> findingListOfDealsBasedOnDealActiveMonthAndYearInBetweendDates(
			@Param("year") String year, @Param("startDate") int startDate, @Param("endDate") int endDate);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where deal_paticipation_status=:dealStatus order by id asc ", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> listOfDealsInformation(@Param("dealStatus") String dealStatus);

	public List<OxyBorrowersDealsInformation> findByDealParticipationStatus(DealParticipationStatus notatachieved);

	@Query(value = "SELECT *\r\n" + " FROM public.oxy_borrowers_deals_information\r\n"
			+ " WHERE deal_type!='TEST' AND borrower_closing_status = 'NOTYETCLOSED' \r\n"
			+ "  AND id NOT IN (51, 83) \r\n" + "ORDER BY id ASC;\r\n" + "", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getListOfDeals();

	@Query(value = "select count(id) from public.oxy_borrowers_deals_information where deal_type!='TEST' and  date(received_on)=current_date", nativeQuery = true)
	public Integer findByfundsAcceptanceStartDate();

	@Query(value = "select sum(deal_amount) from public.oxy_borrowers_deals_information where deal_type!='TEST' and  date(received_on)=current_date", nativeQuery = true)
	public Double findBySumofDealValue();

	@Query(value = "select count(id) from public.oxy_borrowers_deals_information where deal_type!='TEST' and  borrower_closing_status='CLOSED' and date(borrower_closed_date)=current_date", nativeQuery = true)
	public Integer findByBorrowerClosedDate();

	@Query(value = "select id from public.oxy_borrowers_deals_information where deal_type!='TEST'", nativeQuery = true)

	public List<Integer> getListOfIds();

	@Query(value = "select count(id) from public.oxy_borrowers_deals_information where deal_type!='TEST' and   received_on >= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)
	public Integer findByfundsAcceptanceStartDateWeeklyData();

	@Query(value = "select sum(deal_amount) from public.oxy_borrowers_deals_information where deal_type!='TEST' and   received_on >= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)
	public Double findBySumofDealValueWeeklyData();

	@Query(value = "select count(id) from public.oxy_borrowers_deals_information where deal_type!='TEST' and  borrower_closing_status='CLOSED' and borrower_closed_date>= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)
	public Integer findByBorrowerClosedDateWeeklyData();

	@Query(value = "SELECT COUNT(id) FROM public.oxy_borrowers_deals_information where deal_type!='TEST' and date(received_on)>=:startDate and date(received_on)<=:endDate", nativeQuery = true)

	public Integer findByfundsAcceptanceStartDateMonthlyDataWithInput(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT sum(deal_amount) FROM public.oxy_borrowers_deals_information where deal_type!='TEST' and  date(received_on)>=:startDate and date(received_on)<=:endDate", nativeQuery = true)

	public Double findBySumofDealValueMonthlyDataWithInput(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT COUNT(id) FROM public.oxy_borrowers_deals_information where deal_type!='TEST' and  borrower_closing_status='CLOSED' and  date(borrower_closed_date)>=:startDate and date(borrower_closed_date)<=:endDate", nativeQuery = true)

	public Integer findByBorrowerClosedDateMonthlyDataWithInput(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT COUNT(id) FROM public.oxy_borrowers_deals_information WHERE deal_type!='TEST' and date_trunc('month', received_on) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)

	public Integer findByfundsAcceptanceStartDateMonthlyData();

	@Query(value = "select sum(deal_amount) from public.oxy_borrowers_deals_information where deal_type!='TEST' and  date_trunc('month', received_on) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)

	public Double findBySumofDealValueMonthlydata();

	@Query(value = "select count(id) from public.oxy_borrowers_deals_information where deal_type!='TEST' and  borrower_closing_status='CLOSED' and date_trunc('month',borrower_closed_date) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)

	public Integer findByBorrowerClosedDateMonthlyData();

	@Query(value = "SELECT " + "    id, " + "    deal_name, " + "    borrower_name, " + "    deal_amount, "
			+ "    funds_acceptance_start_date, " + "    funds_acceptance_end_date, " + "    borrower_rateofinterest, "
			+ "    deal_link, " + "    duration, " + "    whatapp_reponse_link, " + "    link_sent_to_lenders_group, "
			+ "    loan_active_date, " + "    whatapp_message_to_lenders, " + "    paticipation_limit_to_lenders, "
			+ "    borrower_closing_status, " + "    participation_lender_type, " + "    minimum_paticipation_amount, "
			+ "    deal_achieved_date, " + "    emi_end_date, " + "    borrowers_ids_mapped_to_deal, "
			+ "    any_time_withdrawal, " + "    roi_for_withdrawal " + "FROM "
			+ "    public.oxy_borrowers_deals_information " + "WHERE " + "    deal_type = 'NORMAL' "
			+ "    AND deal_paticipation_status = 'NOTATACHIEVED' " + "    AND deal_open_status = 'NOW' "
			+ "    AND TO_CHAR(received_on, 'YYYY-MM-DD') = TO_CHAR(CURRENT_DATE, 'YYYY-MM-DD') " + "ORDER BY "
			+ "    id DESC " + "LIMIT " + "    :pageSize " + "OFFSET " + "    :pageNo", nativeQuery = true)

	public List<Object[]> getListOfDealInCurrentDate(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "SELECT count(id) FROM public.oxy_borrowers_deals_information where deal_type='NORMAL'", nativeQuery = true)
	public Integer getListOfDealsInformationCountForNormalDeals();

//	@Query(value = "SELECT \r\n" + "  COALESCE(SUM(user_count), 0) AS total_user_count\r\n" + "FROM (\r\n"
//			+ "    SELECT \r\n" + "        b.user_id,\r\n" + "        COUNT(DISTINCT b.user_id) AS user_count,\r\n"
//			+ "        SUM(COALESCE(b.participated_amount, 0) + COALESCE(c.updation_amount, 0)) AS total_participation_and_updation_amount\r\n"
//			+ "    FROM\r\n" + "        oxy_borrowers_deals_information a\r\n" + "    LEFT JOIN\r\n"
//			+ "        public.oxy_lenders_accepted_deals b ON a.id = b.deal_id AND date(b.received_on) BETWEEN '2023-04-01' AND '2024-03-30'\r\n"
//			+ "    LEFT JOIN\r\n"
//			+ "        public.lenders_paticipation_updation c ON a.id = c.deal_id AND c.user_id = b.user_id AND date(c.created_on) BETWEEN '2023-04-01' AND '2024-03-30'\r\n"
//			+ "    LEFT JOIN\r\n" + "        public.user d ON d.id = b.user_id \r\n" + "    WHERE\r\n"
//			+ "        a.life_time_waiver = true \r\n" + "        AND a.deal_type != 'TEST'\r\n"
//			+ "        AND d.test_user = false\r\n"
//			+ "        AND date(a.received_on) BETWEEN '2023-04-01' AND '2024-03-30'\r\n"
//			+ "        AND COALESCE(b.participated_amount, 0) + COALESCE(c.updation_amount, 0) >= a.life_time_waiver_limit\r\n"
//			+ "    GROUP BY\r\n" + "        a.id, a.life_time_waiver, b.user_id\r\n"
//			+ ") AS subquery;", nativeQuery = true)
//	@Query(value = "\r\n"
//			+ "SELECT COUNT(a.user_id) AS users_count\r\n"
//			+ "FROM public.lender_renewal_details a\r\n"
//			+ "JOIN public.user b ON a.user_id = b.id\r\n"
//			+ "WHERE b.test_user = false\r\n"
//			+ "AND a.validity_date >= '2030-01-01' \r\n"
//			+ "AND a.renewal_status = 'NotYetRenewed';", nativeQuery = true)
//
//	public Integer findByLifeTimeWaiverCount();

	@Query(value = "SELECT  count(id) FROM public.oxy_borrowers_deals_information where deal_type!='TEST'", nativeQuery = true)
	public Integer countForTotalDeal();

	@Query(value = "SELECT\r\n" + "			  COUNT(*) AS total_deals,\r\n"
			+ "			 COALESCE(SUM(EXTRACT(EPOCH FROM deal_achieved_date - received_on) / 3600),0) AS total_hours_difference\r\n"
			+ "			FROM\r\n" + "			  public.oxy_borrowers_deals_information\r\n" + "			WHERE\r\n"
			+ "			 deal_paticipation_status = 'ACHIEVED' AND\r\n" + "			  deal_type != 'TEST' AND\r\n"
			+ "			 date(deal_achieved_date)=current_date;", nativeQuery = true)
	public List<Object[]> findByAvgParticipationTimeDailyWise();

	@Query(value = "SELECT\r\n" + "  COUNT(*) AS total_deals,\r\n"
			+ " COALESCE(SUM(EXTRACT(EPOCH FROM deal_achieved_date - received_on) / 3600),0) AS total_hours_difference\r\n"
			+ "FROM\r\n" + "  public.oxy_borrowers_deals_information\r\n" + "WHERE\r\n"
			+ "  deal_paticipation_status = 'ACHIEVED' AND\r\n" + "  deal_type != 'TEST' AND\r\n"
			+ "  date_trunc('month', deal_achieved_date) = date_trunc('month', CURRENT_DATE);", nativeQuery = true)

	public List<Object[]> findByAvgParticipationTimeforDealsMonthlyWise();

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where id=:lenderdealId", nativeQuery = true)
	public OxyBorrowersDealsInformation findDealNameByDealId(@Param("lenderdealId") Integer lenderdealId);

	@Query(value = "SELECT  count(id) FROM public.oxy_borrowers_deals_information where deal_type!='TEST' and deal_type!='EQUITY'", nativeQuery = true)
	public Integer totalDealsCount();
//	@Query(value = "select * from public.oxy_borrowers_deals_information WHERE borrower_closing_status='NOTYETCLOSED' and deal_type!='TEST'", nativeQuery = true)
//
//	public List<OxyBorrowersDealsInformation> getActiveDeals();

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where id=:dealId", nativeQuery = true)
	public OxyBorrowersDealsInformation findDealNameForLenderFee(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where id=:dealId", nativeQuery = true)
	public OxyBorrowersDealsInformation findLenderPaymentDealName(@Param("dealId") Integer dealId);

	@Query(value = "SELECT\r\n" + "    COUNT(*) AS total_deals,\r\n"
			+ "    COALESCE(SUM(EXTRACT(EPOCH FROM deal_achieved_date - received_on) / 3600), 0) AS total_hours_difference\r\n"
			+ "FROM\r\n" + "    public.oxy_borrowers_deals_information\r\n" + "WHERE\r\n"
			+ "    deal_paticipation_status = 'ACHIEVED'\r\n" + "    AND deal_type != 'TEST'\r\n"
			+ "    AND date(deal_achieved_date) >= CURRENT_DATE - INTERVAL '6 days';", nativeQuery = true)

	public List<Object[]> findByAvgParticipationTimeWeekly();

	@Query(value = "SELECT Count(id) FROM public.oxy_borrowers_deals_information where  deal_paticipation_status='NOTATACHIEVED' ", nativeQuery = true)
	public Integer findByOpenDeals();

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information where  deal_paticipation_status='NOTATACHIEVED'and deal_type!='TEST'", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getListOfOpenDeals();

	@Query(value = "SELECT id,deal_amount,deal_name FROM public.oxy_borrowers_deals_information where deal_subtype='STUDENT' and borrower_closing_status = 'NOTYETCLOSED' and agreements_status='NOTYETGENERATED' and deal_type='NORMAL' and funds_acceptance_start_date>='2022-08-17' and length(borrowers_ids_mapped_to_deal)>=0", nativeQuery = true)
	public List<Object[]> pendingAgreementForStudentDeals();

	@Query(value = "select * from public.oxy_borrowers_deals_information where loan_active_date =:startDate and borrower_closing_status = 'NOTYETCLOSED' and deal_type != 'TEST'", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> findCurrentDateInterestPayments(@Param("startDate") Date startDate);

	@Query(value = "select * from public.oxy_borrowers_deals_information where to_char(loan_active_date,'yyyy')<=:year and borrower_closing_status='NOTYETCLOSED' and (EXTRACT('day' FROM loan_active_date)>=:startDate and (EXTRACT('day' FROM loan_active_date)<=:endDate))", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> findListOfCurrentDateInterestDeals(@Param("year") String year,
			@Param("startDate") int startDate, @Param("endDate") int endDate);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information WHERE EXTRACT(year from received_on) = :year AND EXTRACT(MONTH FROM received_on) = :month AND deal_paticipation_status  = :dealpaticipationstatus ", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getListOfDealsInformationForNormalDealsMonthlyCreated(
			@Param("year") int year, @Param("month") int month, @Param("dealpaticipationstatus") String dealtype);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information WHERE EXTRACT(YEAR FROM borrower_closed_date) = :year AND EXTRACT(MONTH FROM borrower_closed_date) = :month AND borrower_closing_status  = :borrowerclosingstatus ", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getListOfDealsInformationForNormalDealsMonthlyClosed(
			@Param("year") int year, @Param("month") int month, @Param("borrowerclosingstatus") String dealtype);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information WHERE EXTRACT(year from deal_achieved_date) = :year AND EXTRACT(Month FROM deal_achieved_date) = :month AND deal_paticipation_status  = :dealpaticipationstatus ", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getListOfDealsInformationForNormalDealsMonthlyachieved(
			@Param("year") int year, @Param("month") int month, @Param("dealpaticipationstatus") String dealtype);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information WHERE deal_paticipation_status = 'NOTATACHIEVED' AND Date(received_on) >= :startDate AND Date(received_on) <=:endDate", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getListOfDealsInformationForrunningDeals(
			@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information WHERE deal_paticipation_status = 'ACHIEVED' AND Date(deal_achieved_date) >= :startDate AND Date(deal_achieved_date) <=:endDate", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getListOfDealsInformationForachieved(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT * FROM public.oxy_borrowers_deals_information WHERE borrower_closing_status = 'CLOSED' AND Date(borrower_closed_date) >= :startDate AND Date(borrower_closed_date) <=:endDate", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> getListOfDealsInformationForclosed(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "select * from public.oxy_borrowers_deals_information where id=:dealId", nativeQuery = true)
	public OxyBorrowersDealsInformation findHoldAmountInterestById(@Param("dealId") int dealId);

	@Query(value = "SELECT * \r\n" + "FROM public.oxy_borrowers_deals_information \r\n"
			+ "WHERE date(received_on) = CURRENT_DATE and deal_type='NORMAL'\r\n" + "", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> findDealsByCurrentDate();

	@Query(value = "SELECT * \r\n" + "FROM public.oxy_borrowers_deals_information \r\n"
			+ "WHERE date(received_on) = CURRENT_DATE and deal_type='TEST'\r\n" + "", nativeQuery = true)
	public List<OxyBorrowersDealsInformation> findTestDealsByCurrentDate();

}
