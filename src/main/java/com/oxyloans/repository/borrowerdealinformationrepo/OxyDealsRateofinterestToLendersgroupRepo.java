package com.oxyloans.repository.borrowerdealinformationrepo;

import java.util.List;

import com.oxyloans.entity.borrowersdealsinformation.OxyDealsRateofinterestToLendersgroup;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OxyDealsRateofinterestToLendersgroupRepo
		extends PagingAndSortingRepository<OxyDealsRateofinterestToLendersgroup, Integer>,
		JpaSpecificationExecutor<OxyDealsRateofinterestToLendersgroup> {

	@Query(value = "SELECT group_id,monthly_interest,quartely_interest,half_interest,yearly_interest,endofthedeal_interest FROM public.oxy_deals_rateofinterest_to_lendersgroup where deal_id=:dealId", nativeQuery = true)
	public List<Object[]> getDealMappedToGroupOfLenders(@Param("dealId") Integer dealId);

	@Query(value = "select * FROM public.oxy_deals_rateofinterest_to_lendersgroup where deal_id=:dealId and group_id=:groupId", nativeQuery = true)
	public OxyDealsRateofinterestToLendersgroup getDealMappedToSingleGroup(@Param("dealId") Integer dealId,
			@Param("groupId") Integer groupId);

	@Query(value = "SELECT AVG(yearly_interest) FROM public.oxy_deals_rateofinterest_to_lendersgroup where deal_id=:dealId", nativeQuery = true)
	public Double getAverageValuForLender(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.oxy_deals_rateofinterest_to_lendersgroup where deal_id=:dealId and group_id=:groupId", nativeQuery = true)
	public OxyDealsRateofinterestToLendersgroup dealTypeRateOfInterestMonthly(@Param("dealId") Integer dealId,
			@Param("groupId") Integer groupId);

	@Query(value = "SELECT * FROM public.oxy_deals_rateofinterest_to_lendersgroup where deal_id=:dealId order by id limit 1", nativeQuery = true)
	public OxyDealsRateofinterestToLendersgroup findByDealId(@Param("dealId") Integer dealId);

	@Query(value = "select max(number) from (\r\n"
			+ "(select max(monthly_interest) as number from  public.oxy_deals_rateofinterest_to_lendersgroup where deal_id=:dealId) union all\r\n"
			+ "(select max(quartely_interest) as number from  public.oxy_deals_rateofinterest_to_lendersgroup where deal_id=:dealId) union all\r\n"
			+ "(select max(half_interest) as number from  public.oxy_deals_rateofinterest_to_lendersgroup where deal_id=:dealId) union all\r\n"
			+ "(select max(yearly_interest) as number from  public.oxy_deals_rateofinterest_to_lendersgroup where deal_id=:dealId) union all\r\n"
			+ "(select max(endofthedeal_interest) as number from  public.oxy_deals_rateofinterest_to_lendersgroup where deal_id=:dealId) ) as result", nativeQuery = true)
	public Double getMaxRoiToDeal(@Param("dealId") Integer dealId);

	@Query(value = "select * FROM public.oxy_deals_rateofinterest_to_lendersgroup where deal_id=:dealId and group_id=:groupId", nativeQuery = true)
	public OxyDealsRateofinterestToLendersgroup findByDealIdAndGroupId(@Param("dealId") int dealId,
			@Param("groupId") int groupId);

	@Query(value = "select * FROM public.oxy_deals_rateofinterest_to_lendersgroup where deal_id=:dealId1 and group_id=:groupId", nativeQuery = true)
	public OxyDealsRateofinterestToLendersgroup findByIdNumAndGroupId(@Param("dealId1") int dealId1,
			@Param("groupId") int groupId);

	@Query(value = "SELECT * FROM public.oxy_deals_rateofinterest_to_lendersgroup \r\n"
			+ "WHERE deal_id =:dealId\r\n"
			+ "AND group_id IN (0, 3) limit 1", nativeQuery = true)
	public OxyDealsRateofinterestToLendersgroup getDealRoiByDealId(@Param("dealId") int dealId);



}
