package com.oxyloans.repository.borrowerdealinformationrepo;

import com.oxyloans.entity.borrowersdealsinformation.OxyBorrowersDealsInformationHistory;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface OxyBorrowersDealsInformationHistoryRepo extends PagingAndSortingRepository<OxyBorrowersDealsInformationHistory, Integer>,
JpaSpecificationExecutor<OxyBorrowersDealsInformationHistory> {

@Query(value = "select * from public.oxy_borrowers_deals_information_history where id=:dealId", nativeQuery = true)
public OxyBorrowersDealsInformationHistory findBydealId(@Param("dealId") Integer dealId);

}
