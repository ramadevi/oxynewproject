package com.oxyloans.repository.loan;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.loan.LoanEmiCardPaymentDetails;

@Repository
public interface LoanEmiCardPaymentDetailsRepo extends PagingAndSortingRepository<LoanEmiCardPaymentDetails, Integer>,
		JpaSpecificationExecutor<LoanEmiCardPaymentDetails> {

	public List<LoanEmiCardPaymentDetails> findByEmiId(int emiId);

	String getlatestrecord = "select id from  public.loan_emi_card_payment_details where emi_id=:paymentDetailsId order by id desc;";

	@Query(value = getlatestrecord, nativeQuery = true)
	List<Integer> getEmiIdsForPaymentDetails(@Param("paymentDetailsId") Integer paymentDetailsId);

	public List<LoanEmiCardPaymentDetails> findByEmiIdAndAmountPaidDate(int id1, Date paidDate);

}
