package com.oxyloans.repository.loan;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.loan.LoanEmiCard;

public interface UpdEmiByApplicationRepo extends PagingAndSortingRepository<LoanEmiCard, Integer>, JpaSpecificationExecutor<LoanEmiCard>{

	String emiDetails = "SELECT * FROM getemidetailsbyapplication(:applicationId)";

	@Query(value = emiDetails, nativeQuery = true)
	public List<Object[]> getEMIDetailsByApplicationId(@Param("applicationId") Integer applicationId);
}
