package com.oxyloans.repository.loan;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.loan.PaytmTransactionDetails;

public interface PaytmTransactionDetailsRepo extends PagingAndSortingRepository<PaytmTransactionDetails, Integer>,
		JpaSpecificationExecutor<PaytmTransactionDetails> {
	@Query(value = "SELECT * FROM public.paytm_transaction_details where van_number=:vanNumber", nativeQuery = true)
	public List<Object[]> getListOfTransactionDetails(@Param("vanNumber") String vanNumber);

	@Query(value = "SELECT * FROM public.paytm_transaction_details where van_number=:vanNumber order by  id desc;", nativeQuery = true)
	public List<Object[]> getLatestTransactionDetails(@Param("vanNumber") String vanNumber);

	@Query(value = "select sum(CAST(amount AS DECIMAL(10,2))) FROM public.paytm_transaction_details where to_char(updated_on,'mm-yyyy')=:givenDate", nativeQuery = true)
	public Double getPaytmTransactionBasedOnMonthAndYear(@Param("givenDate") String givenDate);

	@Query(value = "select van_number,amount,beneficiary_accountnumber,beneficiary_ifsc,remitter_accountnumber,remitter_ifsc,remitter_name,bank_txn_identifier,transaction_request_id,transfer_mode,date(updated_on) FROM public.paytm_transaction_details where to_char(updated_on,'mm-yyyy')=:givenDate order by id limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getListOfPaytmAmounts(@Param("givenDate") String givenDate, @Param("pageNo") int pageNo,
			@Param("pageSize") int pageSize);

	@Query(value = "select count(id) FROM public.paytm_transaction_details where to_char(updated_on,'mm-yyyy')=:givenDate", nativeQuery = true)
	public Integer getListOfPaytmAmountsCount(@Param("givenDate") String givenDate);

}
