package com.oxyloans.repository.loan;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oxyloans.entity.user.AddressDetails;
import com.oxyloans.entity.user.AddressDetails.Type;



public interface AddressDetailsRepo  extends PagingAndSortingRepository<AddressDetails, Integer> {
	
	public AddressDetails findByUserIdAndType(Integer userId, Type Type);
		
    public List<AddressDetails> findByUserId(Integer userid);
	
	public AddressDetails findByuserIdAndType(Integer userId, AddressDetails.Type type);
   
}
