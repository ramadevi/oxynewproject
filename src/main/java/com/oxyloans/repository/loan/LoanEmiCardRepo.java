package com.oxyloans.repository.loan;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.loan.LoanEmiCard;

@Repository
public interface LoanEmiCardRepo
		extends PagingAndSortingRepository<LoanEmiCard, Integer>, JpaSpecificationExecutor<LoanEmiCard> {

	public LoanEmiCard findByLoanIdAndEmiNumber(int loanId, int emiNumber);

	public List<LoanEmiCard> findByLoanIdOrderByEmiNumber(int loanId);

	public int countByLoanIdAndEmiNumberLessThanAndEmiPaidOnNull(int loanId, int emiNumber);

	public LoanEmiCard findTop1ByLoanIdAndAndEmiPaidOnNullOrderByEmiNumber(int loanId);

	public LoanEmiCard findByTransactionNumber(String oxyLoansTransactionNumber);

	public List<LoanEmiCard> findByLoanId(int loanId);

	String emiDetails = "SELECT  A.clnt_txn_ref, A.mandate_reg_no,B.loan_id,B.emi_amount,B.emi_number,B.emi_due_on,B.id \r\n"
			+ "	FROM public.enach_mandate_response A \r\n" + "	JOIN\r\n" + "	public.enach_mandate C\r\n"
			+ "	ON C.id=A.enach_mandate_id\r\n" + "	JOIN public.loan_emi_card B \r\n"
			+ "	ON B.loan_id= C.oxy_loan_id \r\n"
			+ "	where B.emi_due_on=:emiDueDate AND A.txn_msg='SUCCESS' AND emi_paid_on is NULL";

	@Query(value = emiDetails, nativeQuery = true)
	public List<Object[]> getEMIDetailsByEMIDate(@Param("emiDueDate") Date emiDueDate);

	String transactionVerification = "SELECT * FROM public.loan_emi_card where enachstatus=:enachStatus AND enachscheduleddate=:scheduledDate";

	@Query(value = transactionVerification, nativeQuery = true)
	public List<LoanEmiCard> getEnachDetailsForTransactionVerification(@Param("enachStatus") String enachStatus,
			@Param("scheduledDate") String scheduledDate);

	String emiDetails1 = "SELECT R.clnt_txn_ref, R.mandate_reg_no,A.loan_id,A.emi_amount,A.emi_number,A.emi_due_on,A.id"
			+ "           FROM public.enach_mandate_response R	JOIN public.enach_mandate N"
			+ "			  ON N.transaction_id=R.clnt_txn_ref JOIN"
			+ "			  loan_emi_card A	ON A.loan_id= N.oxy_loan_id"
			+ "			  JOIN oxy_loan B ON A.loan_id=B.id"
			+ "			  JOIN oxy_loan_request C ON C.loan_id= B.loan_id"
			+ "			  WHERE C.parent_request_id IN (SELECT DISTINCT E.parent_request_id FROM"
			+ "					 oxy_loan_request E JOIN oxy_loan_request F"
			+ "					 ON F.id=E.parent_request_id JOIN loan_offerd_amount D"
			+ "					 ON F.loan_request_id = D.loan_request_offered_id"
			+ "					 WHERE D.loan_request_offered_id=:applicationId)"
			+ "			  AND A.emi_due_on=:emiDueDate AND R.txn_msg='SUCCESS' AND A.emi_paid_on is NULL";

	@Query(value = emiDetails1, nativeQuery = true)
	public List<Object[]> getEMIDetailsForMiniumEnachDebit(@Param("emiDueDate") Date emiDueDate,
			@Param("applicationId") String applicationId);

	public List<LoanEmiCard> findByLoanId(List<Integer> loanId);

	String calculatingPenality = "SELECT \r\n"
			+ "  a.id,a.loan_id,a.emi_amount,a.emi_due_on,a.remaining_emi_amount,a.remaining_penalty_and_remaining_emi,a.status,b.rate_of_interest\r\n"
			+ "FROM \r\n"
			+ "  public.loan_emi_card a join  public.oxy_loan b on a.loan_id=b.id where emi_paid_on is null AND emi_due_on<CURRENT_TIMESTAMP AND status='INITIATED';\r\n";

	@Query(value = calculatingPenality, nativeQuery = true)
	public List<Object[]> getPenalityCalculation();

	@Query(value = "SELECT count(*),SUM(emi_amount) from   public.loan_emi_card where emi_paid_on is null and status='INITIATED'", nativeQuery = true)
	public List<Object[]> getNoPendingEmisAndPendingAmount();

	@Query(value = "select count(*),SUM(emi_amount) from loan_emi_card where status!='ADMINREJECTED' and emi_due_on>=cast(date_trunc('month', current_date)as date) and emi_due_on<=cast( date_trunc('month',current_date)as date)+'1month'\\:\\:interval-'1day'\\:\\:interval", nativeQuery = true)
	public List<Object[]> getCurrntTotalEmiCount();

	@Query(value = "select count(*) from loan_emi_card where status!='ADMINREJECTED'", nativeQuery = true)
	public Integer getTotalEmiCount();

	@Query(value = "select count(*),SUM(B.emi_amount)   from oxy_loan_request R  join loan_offerd_amount L on(R.id=L.id) join oxy_loan A on(r.id=A.borrower_parent_requestid) join public.user U on(R.user_id=U.id) join user_personal_details P on(P.user_id=U.id) left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is  NULL  and now()\\:\\:date - B.emi_due_on\\:\\:date<=:endCount and now()\\:\\:date - B.emi_due_on\\:\\:date>=:startCount and (R.parent_request_id is Null or R.parent_request_id=0) and (A.borrower_disbursed_date is NOT NULL)", nativeQuery = true)
	public List<Object[]> getSumAmountBucketsAndCount(@Param("startCount") int startCount,
			@Param("endCount") int endCount);

	@Query(value = "select count(*),SUM(B.emi_amount)  from oxy_loan_request R join loan_offerd_amount L on(R.id=L.id)  join oxy_loan A on(r.id=A.borrower_parent_requestid) join public.user U on(R.user_id=U.id) join user_personal_details P on(P.user_id=U.id) left join loan_emi_card B on(A.id=B.loan_id) where (B.emi_paid_on is  NULL) and (now()\\:\\:date - B.emi_due_on\\:\\:date>=:startCount) and  (R.parent_request_id is Null or R.parent_request_id=0) and (A.borrower_disbursed_date is  NULL)", nativeQuery = true)
	public List<Object[]> getSumAmountAndCount(@Param("startCount") int startCount);

	@Query(value = "select B.emi_amount,B.emi_due_on,loan_id,now()\\:\\:date - B.emi_due_on\\:\\:date as diff_days,B.emi_number from  loan_emi_card B  where B.emi_paid_on is  NULL  and now()\\:\\:date - B.emi_due_on\\:\\:date<=:endcount and now()\\:\\:date - B.emi_due_on\\:\\:date>=:startcount and loan_id=:loanid", nativeQuery = true)
	public List<Object[]> getBucketEmis(@Param("startcount") int startCount, @Param("endcount") int endCount,
			@Param("loanid") int loanId);

	@Query(value = "select B.emi_amount,B.emi_due_on,loan_id,now()\\:\\:date - B.emi_due_on\\:\\:date as diff_days,B.emi_number  from  loan_emi_card B  where B.emi_paid_on is  NULL  and now()\\:\\:date - B.emi_due_on\\:\\:date>=:startcount and loan_id=:loanid", nativeQuery = true)
	public List<Object[]> getNpaBucketEmis(@Param("startcount") int startCount, @Param("loanid") int loanId);

	@Query(value = "SELECT * FROM public.loan_emi_card where loan_id=:loanId order by emi_number", nativeQuery = true)
	public List<LoanEmiCard> getLoanEmiCardDetails(@Param("loanId") int loanId);

	@Query(value = "select sum(emi_amount),R.loan_request_id from loan_emi_card L join oxy_loan o on(L.loan_id=o.id) join oxy_loan_request R on(o.borrower_parent_requestid=R.id) where o.borrower_user_id=:borrowerId and o.admin_comments='DISBURSED' and L.emi_due_on=:time and status='INITIATED' group by R.loan_request_id", nativeQuery = true)
	public List<Object[]> findByEmiDueOnAndEmiPaidOnIsNull(@Param("time") Date time,
			@Param("borrowerId") int borrowerId);

	@Query(value = "SELECT \r\n" + "  emi_amount\r\n" + "FROM \r\n"
			+ "  public.loan_emi_card where loan_id=:loanId and emi_number=1 and emi_paid_on is null;", nativeQuery = true)
	public Double getFirstEmiAmount(@Param("loanId") int loanId);

	@Query(value = "SELECT \r\n" + "  emi_amount\r\n" + "FROM \r\n"
			+ "  public.loan_emi_card where loan_id=:loanId and emi_number=2;\r\n" + "\r\n" + "", nativeQuery = true)
	public Double getSecondEmiAmount(@Param("loanId") int loanId);

	@Query(value = "SELECT emi_number,emi_amount FROM public.loan_emi_card where loan_id=:loanId and emi_paid_on is null order by emi_number", nativeQuery = true)
	public List<Object[]> getUnpaidEmiDetails(@Param("loanId") int loanId);

	@Query(value = "select Sum(emi_principal_amount) P,Sum(emi_interst_amount) A from  loan_emi_card E join  oxy_loan O on(E.loan_id=O.id) where O.borrower_user_id=:userid and E.emi_paid_on is not null", nativeQuery = true)
	public List<Object[]> getApplicationPrincipulandInterest(@Param("userid") int userId);

	@Query(value = "\r\n"
			+ "SELECT count(id) FROM public.loan_emi_card where loan_id=:loanId and emi_due_on>=CURRENT_DATE and emi_paid_on is null;", nativeQuery = true)
	public Integer getFurtherEmisCount(@Param("loanId") int loanId);

	@Query(value = "SELECT * FROM public.loan_emi_card where loan_id=:loanId and emi_due_on=CURRENT_DATE ;", nativeQuery = true)
	public LoanEmiCard getLoanEmiInfo(@Param("loanId") int loanId);

	@Query(value = "SELECT count(id) FROM public.loan_emi_card where loan_id=:loanId and emi_paid_on is not null;\r\n"
			+ "", nativeQuery = true)
	public Integer getPaidEmisCount(@Param("loanId") int loanId);

	@Query(value = "\r\n" + "  \r\n"
			+ "  SELECT count(id) FROM public.loan_emi_card where loan_id=:loanId and emi_paid_on is null;\r\n"
			+ "", nativeQuery = true)
	public Integer getUnpaidEmisCount(@Param("loanId") int loanId);

	@Query(value = "SELECT count(id) FROM public.loan_emi_card where loan_id=:loanId and emi_due_on<=CURRENT_DATE", nativeQuery = true)
	public Integer getEmisToBePaid(@Param("loanId") int loanId);

	@Query(value = "\r\n"
			+ "SELECT loan_id,emi_number,emi_amount,status,remaining_emi_amount,emi_late_fee_charges FROM public.loan_emi_card where loan_id=:loanId and emi_due_on<=CURRENT_DATE and emi_paid_on is null", nativeQuery = true)
	public List<Object[]> getDueInformation(@Param("loanId") int loanId);

	@Query(value = "\r\n" + "\r\n"
			+ "SELECT sum(emi_principal_amount) as x,sum(emi_amount)as y FROM public.loan_emi_card where loan_id=:loanId and emi_due_on>=CURRENT_DATE and emi_paid_on is null", nativeQuery = true)
	public List<Object[]> getEmiAmountAndprincipalAmount(@Param("loanId") int loanId);

	@Query(value = "SELECT emi_number FROM public.loan_emi_card where loan_id=:loanId and emi_due_on<=CURRENT_DATE and emi_paid_on is null order by emi_number", nativeQuery = true)
	public List<Integer> getDueInformationEmiNumbers(@Param("loanId") int loanId);

	@Query(value = "\r\n"
			+ "SELECT SUM(emi_late_fee_charges) FROM public.loan_emi_card where loan_id=:loanId and emi_due_on<=CURRENT_DATE and emi_paid_on is null;", nativeQuery = true)
	public Double getInterestPendingTillDate(@Param("loanId") int loanId);

	@Query(value = "\r\n"
			+ "SELECT count(*) FROM public.loan_emi_card where loan_id=:loanId and emi_due_on<=CURRENT_DATE and emi_paid_on is null and emi_late_fee_charges>0;", nativeQuery = true)
	public Integer getEmisPendingInterestCount(@Param("loanId") int loanId);

	@Query(value = "SELECT emi_number FROM public.loan_emi_card where loan_id=:loanId and emi_due_on<=CURRENT_DATE and emi_paid_on is null and emi_late_fee_charges>0 order by emi_number;", nativeQuery = true)
	public List<Integer> getEmiNumbers(@Param("loanId") int loanId);

	@Query(value = "SELECT d.unique_number,CONCAT(c.first_name,'',c.last_name),a.loan_id,a.emi_amount,a.emi_due_on\r\n"
			+ "FROM public.loan_emi_card a join public.oxy_loan b on (a.loan_id=b.id) join public.user_personal_details c \r\n"
			+ "on (b.borrower_user_id=c.user_id) join public.\"user\" d on (c.user_id=d.id) \r\n"
			+ "join public.enach_mandate e on (b.id=e.oxy_loan_id) \r\n"
			+ "where a.emi_due_on= :monthDate and a.emi_paid_on is null and e.mandate_status='SUCCESS' order by a.loan_id;  ", nativeQuery = true)
	public List<Object[]> findAllEnachScheduled(@Param("monthDate") Date monthtDate);

	@Query(value = "SELECT sum(c.emi_amount) FROM public.oxy_loan a join public.user_personal_details b on a.borrower_user_id=b.user_id join public.loan_emi_card c on a.id=c.loan_id where a.id=:loanId and a.loan_status='Active' and a.admin_comments is not null and c.emi_paid_on is not null group by a.id order by a.id;", nativeQuery = true)
	public Double getEmiAmountByLoanId(@Param("loanId") int loanId);

	@Query(value = "SELECT sum(c.emi_interst_amount) FROM public.oxy_loan a join public.user_personal_details b on a.borrower_user_id=b.user_id join public.loan_emi_card c on a.id=c.loan_id where a.id=:loanId and a.loan_status='Active' and a.admin_comments is not null and c.emi_paid_on is not null group by a.id order by a.id;", nativeQuery = true)
	public Double getInterestAmount(@Param("loanId") int loanId);

	@Query(value = "SELECT count(c.id) FROM public.oxy_loan a join public.user_personal_details b on a.borrower_user_id=b.user_id join public.loan_emi_card c on a.id=c.loan_id where a.id=:loanId and a.loan_status='Active' and a.admin_comments is not null and c.emi_paid_on is not null group by a.id order by a.id;", nativeQuery = true)
	public Integer getEmisPaidCount(@Param("loanId") int loanId);

	@Query(value = "SELECT count(id) FROM public.loan_emi_card where emi_due_on<=CURRENT_DATE and loan_id=:loanId", nativeQuery = true)
	public Integer getEmisToBeRecevied(@Param("loanId") int loanId);

	@Query(value = "SELECT * FROM public.loan_emi_card where loan_id=:loanId and emi_number=1", nativeQuery = true)
	public LoanEmiCard getFirstEmiDetails(@Param("loanId") int loanId);

	@Query(value = "SELECT * FROM public.loan_emi_card where loan_id=:loanId and emi_number=:emiNumber", nativeQuery = true)
	public LoanEmiCard getLastEmiDetails(@Param("loanId") int loanId, @Param("emiNumber") int emiNumber);

	@Query(value = "select sum(l.emi_amount) from loan_emi_card l join public.enach_transaction_verification_response v on (l.emi_number=v.emi_number and l.loan_id=v.loan_id) where v.enachstatus='SUCCESS' and v.transactiondate=:givenDate", nativeQuery = true)
	public Double getAmountThroughEcs(@Param("givenDate") String givenDate);

	@Query(value = "select sum(partial_payment_amount) from public.loan_emi_card_payment_details where to_char(amount_paid_date, 'mm-yyyy')=:givenDate and mode_of_payment='PAYU'", nativeQuery = true)
	public Double getAmountThroughPayU(@Param("givenDate") String givenDate);

	@Query(value = "select sum(partial_payment_amount) from public.loan_emi_card l \r\n"
			+ "			join public.loan_emi_card_payment_details d on (l.id=d.emi_id) where\r\n"
			+ "		to_char(d.amount_paid_date, 'mm-yyyy')=:givenDate and l.emi_paid_on is null and d.payment_status='PARTPAID'", nativeQuery = true)
	public Double getPartPaidAmount(@Param("givenDate") String givenDate);

	@Query(value = "select sum(partial_payment_amount) from public.loan_emi_card l \r\n"
			+ "			join public.loan_emi_card_payment_details d on (l.id=d.emi_id) where\r\n"
			+ "		to_char(d.amount_paid_date, 'mm-yyyy')=:givenDate and l.emi_paid_on is null and d.payment_status='FULLYPAID'", nativeQuery = true)
	public Double getFullAmount(@Param("givenDate") String givenDate);

	@Query(value = "SELECT emi_due_on FROM public.loan_emi_card where loan_id=:loanId and emi_number=1", nativeQuery = true)
	public String getFirstEmiDate(@Param("loanId") int loanId);

	@Query(value = "SELECT count(c.id) FROM public.oxy_loan a join public.user_personal_details b on a.borrower_user_id=b.user_id join public.loan_emi_card c on a.id=c.loan_id where a.id=:loanId and a.loan_status='Active' and a.admin_comments is not null and c.emi_paid_on is not null and (c.emi_due_on>='2020-04-01' and c.emi_due_on<='2021-03-31') group by a.id order by a.id;\r\n"
			+ "", nativeQuery = true)
	public Integer getEmisPaidCountForActiveLoans(@Param("loanId") int loanId);

	@Query(value = "SELECT count(c.id) FROM public.oxy_loan a join public.user_personal_details b on a.borrower_user_id=b.user_id join public.loan_emi_card c on a.id=c.loan_id where a.id=:loanId and a.loan_status='Closed' and a.admin_comments is not null and c.emi_paid_on is not null and (c.emi_due_on>='2020-04-01' and c.emi_due_on<='2021-03-31') group by a.id order by a.id;\r\n"
			+ "", nativeQuery = true)
	public Integer getEmisPaidCountForClosedLoans(@Param("loanId") int loanId);

	@Query(value = "SELECT count(id) FROM public.loan_emi_card where loan_id=:loanId and emi_paid_on is null and emi_due_on<=CURRENT_DATE;", nativeQuery = true)
	public Integer getEmisPendingCount(@Param("loanId") int loanId);

	@Query(value = "SELECT a.id,a.lender_user_id,b.first_name,b.last_name,a.borrower_parent_requestid,a.disbursment_amount FROM public.oxy_loan a join public.user_personal_details b on a.lender_user_id=b.user_id where a.borrower_user_id=:userId and a.loan_status='Active' and a.borrower_disbursed_date is not null order by a.loan_id;\r\n"
			+ "", nativeQuery = true)
	public List<Object[]> getLendersMappedToBorrower(@Param("userId") int userId);

	@Query(value = "SELECT sum(emi_amount+emi_late_fee_charges) FROM public.loan_emi_card where emi_due_on<=CURRENT_DATE and emi_paid_on is null and loan_id=:loanId", nativeQuery = true)
	public Double getEmiAmountPendingTillDate(@Param("loanId") int loanId);

	@Query(value = "update public.loan_emi_card set emi_paid_on=:dealClosedDate,status='COMPLETED' where loan_id=:loanId RETURNING 1", nativeQuery = true)
	public List<Integer> updatingPaidDate(@Param("loanId") Integer loanId,
			@Param("dealClosedDate") Timestamp dealClosedDate);

	@Query(value="select sum(a.emi_amount) from public.loan_emi_card a join \r\n"
			+ "public.oxy_loan b on a.loan_id=b.id where b.borrower_parent_requestid=:id",nativeQuery = true)
	public Double findingSumOfEmiAmountToBePaid(@Param("id")Integer id);

	@Query(value="select sum(a.emi_amount) from public.loan_emi_card a join \r\n"
			+ "public.oxy_loan b on a.loan_id=b.id where b.borrower_parent_requestid=:id\r\n"
			+ "and a.emi_paid_on is not null",nativeQuery = true)
	public Double findingSumOfEmiAmountPaid(@Param("id")Integer id);

	@Query(value="select count(*) from public.loan_emi_card a join \r\n"
			+ "public.oxy_loan b on a.loan_id=b.id where b.borrower_parent_requestid=:id\r\n"
			+ "and a.emi_due_on > CURRENT_DATE and a.emi_paid_on is null",nativeQuery = true)
	public Integer findingNoOfEmisToBePaidFuture(@Param("id")Integer id);

	@Query(value="select sum(a.emi_amount) from public.loan_emi_card a join \r\n"
			+ "public.oxy_loan b on a.loan_id=b.id where b.borrower_parent_requestid=:id\r\n"
			+ "and a.emi_due_on > CURRENT_DATE and a.emi_paid_on is null",nativeQuery = true)
	public Double findingSumOfEmiAmountToBePaidInFuture(@Param("id")Integer id);

	@Query(value="select count(*) from public.loan_emi_card a join \r\n"
			+ "public.oxy_loan b on a.loan_id=b.id where b.borrower_parent_requestid=:id\r\n"
			+ "and a.emi_paid_on is not null",nativeQuery = true)
	public Integer findingTotalNoOfEmisPaid(@Param("id")Integer id);

	@Query(value="select * from public.loan_emi_card a join \r\n"
			+ "public.oxy_loan b on a.loan_id=b.id where b.borrower_parent_requestid=:id\r\n"
			+ "and a.emi_paid_on is null",nativeQuery = true)
	public List<LoanEmiCard> findingListOfEmis(@Param("id")Integer id);

}
