package com.oxyloans.repository.loan;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.loan.PenaltyDetails;

public interface PenaltyDetailsRepo
		extends PagingAndSortingRepository<PenaltyDetails, Integer>, JpaSpecificationExecutor<PenaltyDetails> {

	List<PenaltyDetails> findByBorrowerParentRequestid(int borrowerUserId);

	public PenaltyDetails findByEmiNumberAndBorrowerParentRequestid(Integer emiNumber, Integer borrowerParentRequestid);

	@Query(value = "SELECT * FROM public.penalty_details where borrower_parent_requestid=:applicationNumber and penalty_due>0;", nativeQuery = true)
	public PenaltyDetails getApplicationLevelDueAmount(@Param("applicationNumber") int applicationNumber);

	@Query(value = "SELECT p.emi_number FROM public.loan_emi_card a join  public.oxy_loan b on a.loan_id=b.id join public.penalty_details p on b.borrower_parent_requestid=p.borrower_parent_requestid where p.borrower_parent_requestid=:borrowerApplicationId and p.penalty>0 and p.penalty_paid_on is null and emi_paid_on is null AND emi_due_on<=CURRENT_TIMESTAMP group by p.emi_number order by emi_number ;\r\n"
			+ "", nativeQuery = true)
	public List<Integer> getpenaltyTilldateEmiNumbers(@Param("borrowerApplicationId") int borrowerApplicationId);

	@Query(value = "\r\n" + "\r\n"
			+ "SELECT sum(penalty_waived_off) FROM public.penalty_details where borrower_parent_requestid=:borrowerApplicationId", nativeQuery = true)
	public Double getpenaltyWaiveOff(@Param("borrowerApplicationId") int borrowerApplicationId);

	@Query(value = "\r\n"
			+ "SELECT count(*) FROM public.penalty_details where borrower_parent_requestid=:parentRequestID and penalty_paid_on is null;", nativeQuery = true)
	public Integer getEmisUnPaidCount(@Param("parentRequestID") int parentRequestID);

	@Query(value = "\r\n" + "\r\n"
			+ "SELECT emi_number,penalty FROM public.penalty_details where borrower_parent_requestid=:parentRequestID and penalty_paid_on is null", nativeQuery = true)
	public List<Object[]> getEmisUnPaidInfo(@Param("parentRequestID") int parentRequestID);

	@Query(value = "update public.penalty_details set penalty=0.0,penalty_paid_on=:dealClosedDate where borrower_parent_requestid=:borrowerParentRequestId RETURNING 1", nativeQuery = true)
	public List<Integer> closedByApplicationId(@Param("borrowerParentRequestId") Integer borrowerParentRequestId,
			@Param("dealClosedDate") Timestamp dealClosedDate);

	public List<PenaltyDetails> findByBorrowerParentRequestidOrderByEmiNumber(int borrowerApplicationId);

}