package com.oxyloans.repository.loan;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ApplicationLevelLoanEmiCardRepo
		extends PagingAndSortingRepository<ApplicationLevelLoanEmiCard, Integer>,
		JpaSpecificationExecutor<ApplicationLevelLoanEmiCard> {

	@Query(value = "select * from public.applicationlevel_loan_emicard where application_id=:applicationId", nativeQuery = true)
	public ApplicationLevelLoanEmiCard findByApplicationId(Integer applicationId);

	public List<ApplicationLevelLoanEmiCard> findByApplicationIdOrderByEmiNumber(Integer applicationId);

	@Query(value = "SELECT  A.clnt_txn_ref, A.mandate_reg_no,B.application_id,B.emi_amount,B.emi_number,B.emi_due_on,B.id \r\n"
			+ "FROM public.applevel_enach_mandate_response A JOIN	public.applicationlevel_enach_mandate C\r\n"
			+ "ON C.id=A.enach_mandate_id	JOIN public.applicationlevel_loan_emicard B \r\n"
			+ "ON B.application_id= C.application_id \r\n"
			+ "where B.emi_due_on=:emiDueDate AND A.txn_msg='SUCCESS' AND emi_paid_on is NULL", nativeQuery = true)
	public List<Object[]> getEMIDetailsByEMIDate(@Param("emiDueDate") Date emiDueDate);

	@Query(value = "SELECT emi_number,emi_amount FROM public.applicationlevel_loan_emicard where\r\n"
			+ "application_id=:applicationId and emi_paid_on is null order by emi_number", nativeQuery = true)
	public List<Object[]> getUnpaidEmiDetailsForAppLevel(@Param("applicationId") int applicationId);

	@Query(value = "select * from public.applicationlevel_loan_emicard where emi_number=1 and application_id=:applicationId", nativeQuery = true)
	public ApplicationLevelLoanEmiCard findFirstEmiBasedOnApplicationId(@Param("applicationId") Integer applicationId);

	@Query(value = "select sum(emi_amount) from public.applicationlevel_loan_emicard where application_id=:id", nativeQuery = true)
	public Double findingSumOfEmiAmountToBePaid(@Param("id") Integer id);

	@Query(value = "select sum(emi_amount) from public.applicationlevel_loan_emicard \r\n"
			+ "where application_id=:id and status='Paid'", nativeQuery = true)
	public Double findingSumOfEmiAmountPaid(@Param("id") Integer id);

	@Query(value = "select * from public.applicationlevel_loan_emicard where emi_number=:duration and application_id=:applicationId", nativeQuery = true)
	public ApplicationLevelLoanEmiCard findingLastEmiInfo(@Param("duration") Integer duration,
			@Param("applicationId") Integer applicationId);

	@Query(value = "select count(*) from public.applicationlevel_loan_emicard where emi_due_on>CURRENT_DATE\r\n"
			+ "and application_id=:id", nativeQuery = true)
	public Integer findingNoOfEmisToBePaidFuture(@Param("id") Integer id);

	@Query(value = "select sum(emi_Amount) from public.applicationlevel_loan_emicard where emi_due_on>CURRENT_DATE\r\n"
			+ "and application_id=:id", nativeQuery = true)
	public Double findingSumOfEmiAmountToBePaidInFuture(@Param("id") Integer id);

	@Query(value = "select count(*) from public.applicationlevel_loan_emicard where status='Paid'\r\n"
			+ "and application_id=:id", nativeQuery = true)
	public Integer findingTotalNoOfEmisPaid(@Param("id") Integer id);

	@Query(value = "select * from public.applicationlevel_loan_emicard where status='Unpaid'\r\n"
			+ "and application_id=:id order by emi_number asc", nativeQuery = true)
	public List<ApplicationLevelLoanEmiCard> findingListOfEmis(@Param("id") Integer id);

	@Query(value = "select * from public.applicationlevel_loan_emicard where application_id=:appId and emi_number=:emiId", nativeQuery = true)
	public ApplicationLevelLoanEmiCard findingEmiInfoBasedOnEmiNumberAndAppId(@Param("appId") Integer applicationId,
			@Param("emiId") Integer emiId);

}
