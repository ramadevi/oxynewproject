package com.oxyloans.repository.loan;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.oxyloans.entity.loan.PaymentHistory;

public interface PaymentHistoryDetailsRepo
		extends PagingAndSortingRepository<PaymentHistory, Integer>, JpaSpecificationExecutor<PaymentHistory> {

	//List<PaymentHistory> findByApplicationId(String upperCase);

}
