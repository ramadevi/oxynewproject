package com.oxyloans.repository.loan;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.loan.LoanRequest;
import com.oxyloans.entity.user.User.PrimaryType;
import com.oxyloans.entity.user.User.Status;

@Repository
public interface OxyLoanRequestRepo
		extends PagingAndSortingRepository<LoanRequest, Integer>, JpaSpecificationExecutor<LoanRequest> {

	public LoanRequest findByUserIdAndParentRequestIdIsNull(Integer userId);

	public int countByUserIdAndParentRequestIdIsNotNull(Integer userId);

	public int countByParentRequestUserId(Integer userId);

	public int countByParentRequestId(Integer parentRequestId);

	public LoanRequest findByLoanId(String loanId);

	@Query("SELECT SUM(loanRequestAmount) FROM LoanRequest WHERE userPrimaryType = :primaryType AND parentRequestId IS NULL")
	public Double sumByUserPrimaryTypeAndParentRequestIdIsNull(PrimaryType primaryType);

	public void deleteByUserIdAndParentRequestIdIsNotNull(int userId);

	public List<LoanRequest> findByUserId(Integer userId);

	@Query("SELECT SUM(loanRequestAmount) FROM LoanRequest WHERE parentRequestId = :id AND adminComments=:string")
	public Double sumByOfferedApprovedAmount(Integer id, String string);

	@Query("SELECT SUM(loanRequestAmount) FROM LoanRequest WHERE userId = :id AND adminComments=:string")
	public Double sumByRequestApprovedAmount(Integer id, String string);

	public LoanRequest findByUserIdAndParentRequestId(int userId, Integer parentRequestId);

	public LoanRequest findByUserIdAndLoanRequestId(int userId, String loanRequestedId);

	public LoanRequest findByLoanRequestId(String applicationId);

	public LoanRequest findBytxnNumber(String oxyLoansTransactionNumber);

	@Query("SELECT SUM(loanRequestAmount) FROM LoanRequest WHERE userPrimaryType = :primaryType")
	public Double sumByUserPrimaryType(PrimaryType primaryType);

	@Query(value = "SELECT SUM(loan_offered_amount) FROM oxy_loan_request inner join loan_offerd_amount on(oxy_loan_request.id=loan_offerd_amount.id) WHERE user_primary_type =:string and parent_request_id is null", nativeQuery = true)
	public Double sumByUserPrimaryTypeAndLoanOfferedAmount(String string);

	public List<LoanRequest> findByParentRequestId(int borrowerId);

	public List<LoanRequest> findByLoanId(List<String> loanIds);

	String numberOffersSentCount = "select count(*) from public.user a join public.oxy_loan_request b on a.primary_type=b.user_primary_type join public.loan_offerd_amount c on  b.id=c.id where a.primary_type='BORROWER' and a.admin_comments='INTERESTED' and c.loan_offerd_status='INITIATED';\r\n"
			+ "";

	@Query(value = numberOffersSentCount, nativeQuery = true)
	Integer getNumberOffersSentCount();

	@Query(value = "select distinct R.user_primary_type ,R.id as lonrequest_id,R.loan_request_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id as user_id,L.loan_offered_amount  from oxy_loan_request R  join loan_offerd_amount L on(R.id=L.id) join oxy_loan A on(r.id=A.borrower_parent_requestid) join public.user U on(R.user_id=U.id) join user_personal_details P on(P.user_id=U.id) left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is  NULL  and now()\\:\\:date - B.emi_due_on\\:\\:date<=:endcount and now()\\:\\:date - B.emi_due_on\\:\\:date>= :startcount and (R.parent_request_id is Null or R.parent_request_id=0) and (A.borrower_disbursed_date is NOT NULL) limit :pagesize offset :pageno", nativeQuery = true)
	public List<Object[]> getApplicationBuckets(@Param("startcount") int startCount, @Param("endcount") int endCount,
			@Param("pageno") int pageNo, @Param("pagesize") int pageSize);

	@Query(value = "select count(*) from (select distinct R.user_primary_type ,R.id as lonrequest_id,R.loan_request_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id as user_id,L.loan_offered_amount,R.parent_request_id  from oxy_loan_request R  join loan_offerd_amount L on(R.id=L.id) join oxy_loan A on(r.id=A.borrower_parent_requestid) join public.user U on(R.user_id=U.id) join user_personal_details P on(P.user_id=U.id) left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is  NULL and  now()\\:\\:date - B.emi_due_on\\:\\:date<=:endcount and now()\\:\\:date - B.emi_due_on\\:\\:date>=:startcount and (R.parent_request_id is Null or R.parent_request_id=0) and (A.borrower_disbursed_date is NOT NULL))tab", nativeQuery = true)
	public Integer getApplicationBucketscount(@Param("startcount") int startCount, @Param("endcount") int endCount);

	@Query(value = "select distinct  R.user_primary_type ,R.id as lonrequest_id,R.loan_request_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id as user_id,L.loan_offered_amount  from oxy_loan_request R join loan_offerd_amount L on(R.id=L.id)  join oxy_loan A on(r.id=A.borrower_parent_requestid) join public.user U on(R.user_id=U.id) join user_personal_details P on(P.user_id=U.id) left join loan_emi_card B on(A.id=B.loan_id) where (B.emi_paid_on is  NULL) and now()\\:\\:date - B.emi_due_on\\:\\:date>=:startcount and  (R.parent_request_id is Null or R.parent_request_id=0) and (A.borrower_disbursed_date is  NULL) limit :pagesize offset :pageno", nativeQuery = true)
	public List<Object[]> getApplicationNpaBuckets(@Param("startcount") int startCount, @Param("pageno") int pageNo,
			@Param("pagesize") int pageSize);

	@Query(value = "select count(*) from (select distinct R.user_primary_type ,R.id as lonrequest_id,R.loan_request_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id as user_id,L.loan_offered_amount,R.parent_request_id  from oxy_loan_request R  join loan_offerd_amount L on(R.id=L.id) join oxy_loan A on(r.id=A.borrower_parent_requestid) join public.user U on(R.user_id=U.id) join user_personal_details P on(P.user_id=U.id) left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is  NULL  and now()\\:\\:date - B.emi_due_on\\:\\:date>=:startcount and (R.parent_request_id is Null or R.parent_request_id=0) and (A.borrower_disbursed_date is NOT NULL))tab", nativeQuery = true)
	public Integer getApplicationNpaBucketscount(@Param("startcount") int startCount);

	@Query(value = "select distinct R.user_primary_type ,R.id as lonrequest_id,R.loan_request_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id as user_id,L.loan_offered_amount  from oxy_loan_request R join loan_offerd_amount L on(R.id=L.id)  join oxy_loan A on(r.id=A.borrower_parent_requestid) join public.user U on(R.user_id=U.id) join user_personal_details P on(P.user_id=U.id) left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is  NULL and now()\\:\\:date - B.emi_due_on\\:\\:date<=:endcount and now()\\:\\:date - B.emi_due_on\\:\\:date>=:startcount and (R.parent_request_id is Null or R.parent_request_id=0) and (A.borrower_disbursed_date is NOT NULL) and R.user_id=:userid", nativeQuery = true)
	public List<Object[]> getApplicationBucketsByUserId(@Param("startcount") int startCount,
			@Param("endcount") int endCount, @Param("userid") int userId);

	@Query(value = "select distinct R.user_primary_type ,R.id as lonrequest_id,R.loan_request_amount,u.mobile_number,U.email,P.first_name,p.last_name,R.loan_request_id,U.id as user_id,L.loan_offered_amount  from oxy_loan_request R join loan_offerd_amount L on(R.id=L.id)  join oxy_loan A on(r.id=A.borrower_parent_requestid) join public.user U on(R.user_id=U.id) join user_personal_details P on(P.user_id=U.id) left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is  NULL  and now()\\:\\:date - B.emi_due_on\\:\\:date<=:startcount and (R.parent_request_id is Null or R.parent_request_id=0) and (A.borrower_disbursed_date is NOT NULL) and R.user_id=:userid", nativeQuery = true)
	public List<Object[]> getApplicationNpaBucketsByUserId(@Param("startcount") int startCount,
			@Param("userid") int userId);

	@Query(value = "SELECT a.loan_request_id,b.id,b.email,b.mobile_number,b.city,c.first_name,c.last_name FROM public.oxy_loan_request a join  public.user b on b.id=a.user_id join  public.user_personal_details c on b.id=c.user_id where a.loan_request_id=:borrowerParentRequestId", nativeQuery = true)
	public List<Object[]> searchByBorrowerParentRequestId(
			@Param("borrowerParentRequestId") String borrowerParentRequestId);

	@Query(value = "SELECT a.loan_request_id,b.id,b.email,b.mobile_number,b.city,c.first_name,c.last_name FROM public.oxy_loan_request a join public.user b on b.id=a.user_id  join public.user_personal_details c on b.id=c.user_id where a.user_id=:borrowerId and a.parent_request_id is null;\r\n", nativeQuery = true)
	public List<Object[]> searchByBorrowerId(@Param("borrowerId") int borrowerId);

	@Query("SELECT SUM(loanRequestAmount) FROM LoanRequest WHERE ((parentRequestId Is null ) OR  (parentRequestId=:parentRequestId)) And loanStatus='Requested'")
	public Object sumOfCommitmentAmount(@Param("parentRequestId") Integer parentRequestId);

	@Query(value = "select  sum(borrower_fee) from loan_offerd_amount l join(select distinct borrower_parent_requestid from oxy_loan where loan_status='Active' or loan_status='Closed') o on(l.id=o.borrower_parent_requestid)", nativeQuery = true)
	public Double totalBorrowerFee();

	@Query("SELECT SUM(loanRequestAmount) FROM LoanRequest WHERE ((parentRequestId Is null ) OR  (parentRequestId=:parentRequestId)) And loanStatus='Requested' And userId=:userId")
	public Object sumOfCommitmentAmount(@Param("parentRequestId") Integer parentRequestId,
			@Param("userId") Integer userId);

	@Query(value = "select sum(loan_offered_amount) from loan_offerd_amount where user_id=:userid", nativeQuery = true)
	public Double sumByLoanOfferedAmount(@Param("userid") Integer userId);

	@Query(value = "SELECT Count(*) FROM oxy_loan_request WHERE ((parent_request_id Is Not null ) OR  (parent_request_id!=0)) And loan_status='Requested' And user_id=:userid", nativeQuery = true)
	public int countOfLoanRequest(@Param("userid") Integer userId);

	@Query(value = "select count(*) from  oxy_loan_request where parent_request_id in(select id from oxy_loan_request where ((parent_request_id Is  null ) OR  (parent_request_id=0)) and user_id=:userid)", nativeQuery = true)
	public int countOfResponces(@Param("userid") Integer userId);

	@Query(value = "select sum(borrower_fee) from loan_offerd_amount where payu_status is not null and user_id=:userid", nativeQuery = true)
	public Double sumOfBorrowerFee(@Param("userid") int userId);

	@Query(value = "select count(*) from oxy_loan_request where (parent_request_id=0 or parent_request_id is null) and user_id=:userid", nativeQuery = true)
	public Integer sumOfApplications(@Param("userid") int userId);

	@Query(value = "SELECT id,loan_offered_amount,loan_offerd_status,accepeted_on,borrower_fee FROM public.loan_offerd_amount where accepeted_on is not null and borrower_fee>1 and (loan_offerd_status='LOANOFFERACCEPTED' OR loan_offerd_status='LOANOFFERCOMPLETED') order by id;", nativeQuery = true)
	public List<Object[]> getTotalLoanOfferedBorrowers();

	@Query(value = "SELECT loan_offered_amount FROM public.loan_offerd_amount where user_id=:borrowerId and loan_offerd_status='LOANOFFERACCEPTED'", nativeQuery = true)
	public Double checkCurrentApplication(@Param("borrowerId") Integer borrowerId);

	@Query(value = "select r.id,sum(l.emi_amount),u.loan_owner,o.loan_id from public.oxy_loan_request r join public.oxy_loan o on (r.id=o.borrower_parent_requestid)\r\n"
			+ "join public.loan_emi_card l on (o.id=l.loan_id) join public.user u on (u.id=r.user_id) where o.loan_status='Active'\r\n"
			+ "and o.admin_comments='DISBURSED' and r.user_id=:userId group by o.loan_id,u.loan_owner,r.id", nativeQuery = true)
	public List<Object[]> findAllBorrowerRunningApplications(@Param("userId") Integer userId);

	@Query(value = "select sum(disbursment_amount) from public.oxy_loan where borrower_user_id=:userId and \r\n"
			+ "lender_esigned=true and borrower_esigned=true and borrower_parent_requestid=:applicationId", nativeQuery = true)
	public Double findingSumOfDisbursmentAmount(@Param("userId") int userId,
			@Param("applicationId") Integer applicationId);

	@Query(value = "select * from public.oxy_loan_request where application_id=:applicationId", nativeQuery = true)
	public List<LoanRequest> findingListOfLoansBasedOnAppId(@Param("applicationId") String applicationId);

	@Query(value = "select * from public.oxy_loan_request a join public.loan_offerd_amount b on a.id=b.id\r\n"
			+ "where to_char(accepeted_on,'yyyy-MM-dd')>='2022-07-27' and b.id=:id", nativeQuery = true)
	public LoanRequest findingLoanOfferedAcceptedDate(@Param("id") Integer id);
}
