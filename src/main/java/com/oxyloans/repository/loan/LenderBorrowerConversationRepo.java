package com.oxyloans.repository.loan;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.loan.LenderBorrowerConversation;

@Repository
public interface LenderBorrowerConversationRepo extends PagingAndSortingRepository<LenderBorrowerConversation, Integer> {
	
	public int countByLenderUserIdAndBorrowerUserId (int lenderUserId, int borrowerUserId);
	
	public void deleteByLenderUserIdAndBorrowerUserId (int lenderUserId, int borrowerUserId);
	
}
