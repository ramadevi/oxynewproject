package com.oxyloans.repository.loan;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.loan.LoanRequest.LoanStatus;
import com.oxyloans.entity.loan.OxyLoan;

@Repository
public interface OxyLoanRepo extends PagingAndSortingRepository<OxyLoan, Integer>, JpaSpecificationExecutor<OxyLoan> {

	@Query("SELECT SUM(disbursmentAmount), SUM(loanInterestPaid), SUM(lenderTransactionFee), SUM(principalRepaid), SUM(noOfFailedEmis) FROM OxyLoan WHERE lenderUserId = :lenderUserId AND"
			+ " loanStatus IN :loanStatuses")
	public Object sumOfLenderAllLoans(@Param("lenderUserId") Integer lenderUserId,
			@Param("loanStatuses") LoanStatus... loanStatuses);

	@Query("SELECT SUM(disbursmentAmount), SUM(loanInterestPaid), SUM(borrowerTransactionFee), SUM(principalRepaid), SUM(noOfFailedEmis) FROM OxyLoan WHERE borrowerUserId = :borrowerUserId AND"
			+ " loanStatus IN :loanStatuses")
	public Object sumOfBorrowerAllLoans(@Param("borrowerUserId") Integer borrowerUserId,
			@Param("loanStatuses") LoanStatus... loanStatuses);

	@Query("SELECT loanStatus, count(*) FROM OxyLoan WHERE lenderUserId = :lenderUserId AND loanStatus IN :loanStatuses GROUP BY loanStatus")
	public List<Object[]> lenderCountByLoanStatus(@Param("lenderUserId") Integer lenderUserId,
			@Param("loanStatuses") LoanStatus... loanStatuses);

	@Query("SELECT loanStatus, count(*) FROM OxyLoan WHERE borrowerUserId = :borrowerUserId AND loanStatus IN :loanStatuses GROUP BY loanStatus")
	public List<Object[]> borrowerCountByLoanStatus(@Param("borrowerUserId") Integer lenderUserId,
			@Param("loanStatuses") LoanStatus... loanStatuses);

	public OxyLoan findByLoanRequestIdAndLoanRespondId(Integer loanRequestId, Integer loanRespondId);

	public List<OxyLoan> findByLenderUserIdAndBorrowerUserIdAndLoanStatusIn(Integer lenderUserId,
			Integer borrowerUserId, List<LoanStatus> loanStatuses);

	public int countByLoanStatusIn(LoanStatus... loanStatuses);

	public OxyLoan findByLoanId(String loanId);

	// public List<OxyLoan> findByBorrowerUserId(int id);

	public List<OxyLoan> findByLenderUserIdAndBorrowerUserIdAndBorrowerParentRequestIdAndLenderParentRequestIdAndLoanStatusIn(
			Integer requestedUser, Integer id, Integer id2, Integer id3, List<LoanStatus> loanStatuses);

	public OxyLoan findByBorrowerUserIdAndBorrowerParentRequestId(Integer userId, Integer id);

	@Query("SELECT SUM(disbursmentAmount) FROM OxyLoan WHERE adminComments=:string")
	public Double findByDisbursedLoanAmount(String string);

	@Query("SELECT SUM(disbursmentAmount), SUM(loanInterestPaid), SUM(borrowerTransactionFee), SUM(principalRepaid), SUM(noOfFailedEmis) FROM OxyLoan WHERE borrowerUserId = :borrowerUserId AND borrowerParentRequestId=:id AND"
			+ " loanStatus IN :loanStatuses")
	public Object sumOfBorrowerAllLoansByCurrentLoan(@Param("borrowerUserId") int userId, @Param("id") Integer id,
			@Param("loanStatuses") LoanStatus[] array);

	@Query("SELECT loanStatus, count(*) FROM OxyLoan WHERE borrowerUserId = :borrowerUserId AND borrowerParentRequestId=:borrowerParentRequestId AND loanStatus IN :loanStatuses GROUP BY loanStatus")
	public List<Object[]> borrowerCountByLoanStatusOfCurrentLoan(@Param("borrowerUserId") int borrowerUserId,
			@Param("borrowerParentRequestId") int borrowerParentRequestId, @Param("loanStatuses") LoanStatus[] array);

	public List<OxyLoan> findByBorrowerParentRequestId(Integer id);

	public List<OxyLoan> findByLenderParentRequestId(Integer id);

	@Query(value = "select sum(loan_emi_card.emi_amount) from oxy_loan  inner join loan_emi_card on(oxy_loan.id=loan_emi_card.loan_id) where loan_emi_card.emi_paid_on is null and oxy_loan.lender_user_id=:id", nativeQuery = true)
	public Double findSumOfOutstandingAmount(@Param("id") Integer id);

	// public List<OxyLoan> findByLenderUserId(int userId);

	String lenderTransactions = "select loan_id,borrower_user_id,disbursment_amount,loan_accepted_date,borrower_disbursed_date from "
			+ " public.oxy_loan B\r\n" + "where B.lender_user_id=:userId \r\n"
			+ "AND B.loan_status in('Active','Agreed')"
			+ "AND date_trunc('day', B.loan_accepted_date) >= '2020-02-14' ORDER BY loan_accepted_date ASC";

	@Query(value = lenderTransactions, nativeQuery = true)
	public List<Object[]> getDisburmentInformation(@Param("userId") Integer userId);

	String individualLoansInformation = "SELECT * FROM getLoanDetails(:lenderUserId,:pageSize,:pageNumber)";

	@Query(value = individualLoansInformation, nativeQuery = true)
	List<Object[]> getIndividualLoans(@Param("lenderUserId") Integer lenderUserId, @Param("pageSize") Integer pageSize,
			@Param("pageNumber") Integer pageNumber);

	String individualLoansInformationForBorrower = "SELECT * FROM getLoanDetailsForBorrower(:borrowerUserId,:pageSize,:pageNumber)";

	@Query(value = individualLoansInformationForBorrower, nativeQuery = true)
	List<Object[]> getIndividualLoansForBorrower(@Param("borrowerUserId") Integer borrowerUserId,
			@Param("pageSize") Integer pageSize, @Param("pageNumber") Integer pageNumber);

	@Query(value = "select o.id,o.loan_id,o.lender_user_id,o.borrower_user_id,o.disbursment_amount,o.borrower_disbursed_date  from oxy_loan o   LEFT  join loan_emi_card l on(o.id=l.loan_id) where l.emi_paid_on is null and l.emi_due_on>=:montStartDate and l.emi_due_on<=:montEndtDate  and o.borrower_disbursed_date is NOT NULL ORDER  BY o.loan_id limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getCurrentMonthEmis(@Param("montStartDate") Date monthStartDate,
			@Param("montEndtDate") Date montEndtDate, @Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select DISTINCT o.id,o.loan_id,o.lender_user_id,o.borrower_user_id,o.disbursment_amount,o.borrower_disbursed_date  from oxy_loan o   LEFT  join loan_emi_card l on(o.id=l.loan_id) where l.emi_paid_on is null and l.emi_due_on>=:montStartDate  and o.borrower_disbursed_date is NOT NULL ORDER  BY o.loan_id limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getFeatureMonthEmi(@Param("montStartDate") Date monthStartDate, @Param("pageNo") int pageNo,
			@Param("pageSize") int pageSize);

	@Query(value = "select DISTINCT o.id,o.loan_id,o.lender_user_id,o.borrower_user_id,o.disbursment_amount,o.borrower_disbursed_date  from oxy_loan o   LEFT  join loan_emi_card l on(o.id=l.loan_id) where l.emi_paid_on is null  and o.borrower_disbursed_date is NOT NULL ORDER  BY o.loan_id limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> gettotalEmis(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "SELECT count(*) from (SELECT distinct B.loan_id from OXY_LOAN A\r\n"
			+ "					 JOIN LOAN_EMI_CARD B\r\n" + "					 ON B.loan_id=A.id\r\n"
			+ "					 WHERE B.emi_paid_on IS NULL AND\r\n"
			+ "					A.borrower_disbursed_date is NOT NULL AND B.emi_due_on BETWEEN :montStartDate AND :montEndtDate) tab", nativeQuery = true)
	public Integer getCurrentcount(@Param("montStartDate") Date monthStartDate,
			@Param("montEndtDate") Date montEndtDate);

	@Query(value = " SELECT count(*) from (SELECT distinct B.loan_id from OXY_LOAN A\r\n"
			+ "					 JOIN LOAN_EMI_CARD B\r\n" + "					 ON B.loan_id=A.id\r\n"
			+ "					 WHERE B.emi_paid_on IS NULL AND\r\n"
			+ "					A.borrower_disbursed_date is NOT NULL AND B.emi_due_on>=:montStartDate)tab", nativeQuery = true)
	public Integer getFeatureCount(@Param("montStartDate") Date monthStartDate);

	@Query(value = "SELECT count(*) from (SELECT distinct B.loan_id from  OXY_LOAN A JOIN LOAN_EMI_CARD B ON B.loan_id=A.id WHERE B.emi_paid_on IS NULL and A.borrower_disbursed_date is NOT NULL) tab", nativeQuery = true)
	public Integer getTotal();

	@Query(value = "select A.id,A.loan_id,A.lender_user_id,A.borrower_user_id,A.disbursment_amount,A.borrower_disbursed_date from oxy_loan A left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is NULL and A.borrower_disbursed_date is NOT NULL and now()\\:\\:date - B.emi_due_on\\:\\:date<= :endCount and now()\\:\\:date - B.emi_due_on\\:\\:date>= :startCount limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getBuckets(@Param("startCount") int startCount, @Param("endCount") int endCount,
			@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select count(*) from oxy_loan A left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is NULL and A.borrower_disbursed_date is NOT NULL and now()\\:\\:date - B.emi_due_on\\:\\:date<= :endCount and now()\\:\\:date - B.emi_due_on\\:\\:date>= :startCount", nativeQuery = true)
	public Integer getBucketCount(@Param("startCount") int startCount, @Param("endCount") int endCount);

	@Query(value = "select A.id,A.loan_id,A.lender_user_id,A.borrower_user_id,A.disbursment_amount,A.borrower_disbursed_date from oxy_loan A left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is NULL and A.borrower_disbursed_date is NOT NULL and now()\\:\\:date - B.emi_due_on\\:\\:date>= :startCount limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getBucketsNpa(@Param("startCount") int startCount, @Param("pageNo") int pageNo,
			@Param("pageSize") int pageSize);

	@Query(value = "select count(*) from oxy_loan A left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is NULL and A.borrower_disbursed_date is NOT NULL and now()\\:\\:date - B.emi_due_on\\:\\:date>= :startCount", nativeQuery = true)
	public Integer getBucketCountNpa(@Param("startCount") int startCount);

	@Query(value = "select count(*) from oxy_loan a where a.loan_status='Agreed'", nativeQuery = true)
	public Integer getcountNumberOfLoansInAgreedState();

	@Query(value = "select count(*) from oxy_loan a where a.loan_status='Active' and a.borrower_disbursed_date is null", nativeQuery = true)
	public Integer getcountPendingAtDisbusmentDate();

	// userid search

	@Query(value = "select A.id,A.loan_id,A.lender_user_id,A.borrower_user_id,A.disbursment_amount,A.borrower_disbursed_date from oxy_loan A left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is NULL and A.borrower_disbursed_date is NOT NULL and now()\\:\\:date - B.emi_due_on\\:\\:date<= :endCount and now()\\:\\:date - B.emi_due_on\\:\\:date>= :startCount and borrower_user_id=:userid limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getBucketsByUserId(@Param("startCount") int startCount, @Param("endCount") int endCount,
			@Param("userid") int userID, @Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select A.id,A.loan_id,A.lender_user_id,A.borrower_user_id,A.disbursment_amount,A.borrower_disbursed_date from oxy_loan A left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is NULL and A.borrower_disbursed_date is NOT NULL and now()\\:\\:date - B.emi_due_on\\:\\:date>= :startCount and borrower_user_id=:userid limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getBucketsNpaByUserId(@Param("startCount") int startCount, @Param("userid") int userId,
			@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select count(*) from oxy_loan A left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is NULL and A.borrower_disbursed_date is NOT NULL and now()\\:\\:date - B.emi_due_on\\:\\:date<= :endCount and now()\\:\\:date - B.emi_due_on\\:\\:date>= :startCount and borrower_user_id=:userid", nativeQuery = true)
	public Integer getBucketsByUserIdCount(@Param("startCount") int startCount, @Param("endCount") int endCount,
			@Param("userid") int userID);

	@Query(value = "select count(*) from oxy_loan A left join loan_emi_card B on(A.id=B.loan_id) where B.emi_paid_on is NULL and A.borrower_disbursed_date is NOT NULL and now()\\:\\:date - B.emi_due_on\\:\\:date>= :startCount and borrower_user_id=:userid", nativeQuery = true)
	public Integer getBucketsNpaByUserIdCount(@Param("startCount") int startCount, @Param("userid") int userId);

	@Query(value = "select o.id,o.loan_id,o.lender_user_id,o.borrower_user_id,o.disbursment_amount,o.borrower_disbursed_date  from oxy_loan o   LEFT  join loan_emi_card l on(o.id=l.loan_id) where l.emi_paid_on is null and l.emi_due_on>=:montStartDate and l.emi_due_on<=:montEndtDate  and o.borrower_disbursed_date is NOT NULL and borrower_user_id=:userid ORDER  BY o.loan_id limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getCurrentMonthEmisByUserId(@Param("montStartDate") Date monthStartDate,
			@Param("montEndtDate") Date montEndtDate, @Param("userid") int userId, @Param("pageNo") int pageNo,
			@Param("pageSize") int pageSize);

	@Query(value = "select DISTINCT o.id,o.loan_id,o.lender_user_id,o.borrower_user_id,o.disbursment_amount,o.borrower_disbursed_date  from oxy_loan o   LEFT  join loan_emi_card l on(o.id=l.loan_id) where l.emi_paid_on is null and l.emi_due_on>=:montStartDate  and o.borrower_disbursed_date is NOT NULL and borrower_user_id=:userid ORDER  BY o.loan_id limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getFeatureMonthEmiByUserId(@Param("montStartDate") Date monthStartDate,
			@Param("userid") int userId, @Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select DISTINCT o.id,o.loan_id,o.lender_user_id,o.borrower_user_id,o.disbursment_amount,o.borrower_disbursed_date  from oxy_loan o   LEFT  join loan_emi_card l on(o.id=l.loan_id) where l.emi_paid_on is null  and o.borrower_disbursed_date is NOT NULL and borrower_user_id=:userid ORDER  BY o.loan_id limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> gettotalEmisByUserId(@Param("userid") int userId, @Param("pageNo") int pageNo,
			@Param("pageSize") int pageSize);

	@Query(value = "\r\n"
			+ "select distinct a.loan_request_id,d.id,d.city,d.email,d.mobile_number,e.first_name,e.last_name from oxy_loan_request a  join oxy_loan b  on (a.id=b.borrower_parent_requestid) join loan_emi_card c on (b.id=c.loan_id) join public.user d on (a.user_id=d.id) join user_personal_details e on (a.user_id=e.user_id) where (a.parent_request_id is null or a.parent_request_id=0) and c.emi_paid_on is null and b.borrower_disbursed_date is NOT NULL and c.emi_due_on>=:monthStartDate and c.emi_due_on<=:monthEndtDate ORDER  BY a.loan_request_id limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getCurrentMonthEmisBasedOnApplication(@Param("monthStartDate") Date monthStartDate,
			@Param("monthEndtDate") Date monthEndtDate, @Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select distinct a.loan_request_id,d.id,d.city,d.email,d.mobile_number,e.first_name,e.last_name from oxy_loan_request a  join oxy_loan b  on (a.id=b.borrower_parent_requestid) join loan_emi_card c on (b.id=c.loan_id) join public.user d on (a.user_id=d.id) join user_personal_details e on (a.user_id=e.user_id) where (a.parent_request_id is null or a.parent_request_id=0) and c.emi_paid_on is null and b.borrower_disbursed_date is NOT NULL and c.emi_due_on>=:monthStartDate  ORDER  BY a.loan_request_id limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getFurtherMonthEmisBasedOnApplication(@Param("monthStartDate") Date monthStartDate,
			@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select distinct a.loan_request_id,d.id,d.city,d.email,d.mobile_number,e.first_name,e.last_name from oxy_loan_request a  join oxy_loan b  on (a.id=b.borrower_parent_requestid) join loan_emi_card c on (b.id=c.loan_id) join public.user d on (a.user_id=d.id) join user_personal_details e on (a.user_id=e.user_id) where (a.parent_request_id is null or a.parent_request_id=0) and c.emi_paid_on is null and b.borrower_disbursed_date is NOT NULL ORDER  BY a.loan_request_id limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getPendingBasedOnApplication(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select count(*) from (select distinct a.loan_request_id,d.id,d.city,d.email,d.mobile_number,e.first_name,e.last_name from oxy_loan_request a  join oxy_loan b  on (a.id=b.borrower_parent_requestid) join loan_emi_card c on (b.id=c.loan_id) join public.user d on (a.user_id=d.id) join user_personal_details e on (a.user_id=e.user_id) where (a.parent_request_id is null or a.parent_request_id=0) and (c.emi_paid_on is null) and (b.borrower_disbursed_date is NOT NULL) and c.emi_due_on>=:monthStartDate and c.emi_due_on<=:monthEndtDate)tab", nativeQuery = true)
	public Integer getCurrentMonthCount(@Param("monthStartDate") Date monthStartDate,
			@Param("monthEndtDate") Date monthEndtDate);

	@Query(value = "select count(*) from (select distinct a.loan_request_id,d.id,d.city,d.email,d.mobile_number,e.first_name,e.last_name from oxy_loan_request a  join oxy_loan b  on (a.id=b.borrower_parent_requestid) join loan_emi_card c on (b.id=c.loan_id) join public.user d on (a.user_id=d.id) join user_personal_details e on (a.user_id=e.user_id) where (a.parent_request_id is null or a.parent_request_id=0) and c.emi_paid_on is null and b.borrower_disbursed_date is NOT NULL and c.emi_due_on>=:monthStartDate)tab", nativeQuery = true)
	public Integer getFurtherMonthCount(@Param("monthStartDate") Date monthStartDate);

	@Query(value = "select count(*) from (select distinct a.loan_request_id,d.id,d.city,d.email,d.mobile_number,e.first_name,e.last_name from oxy_loan_request a  join oxy_loan b  on (a.id=b.borrower_parent_requestid) join loan_emi_card c on (b.id=c.loan_id) join public.user d on (a.user_id=d.id) join user_personal_details e on (a.user_id=e.user_id) where (a.parent_request_id is null or a.parent_request_id=0) and c.emi_paid_on is null and b.borrower_disbursed_date is NOT NULL )tab", nativeQuery = true)
	public Integer getPendingCount();

	@Query(value = "select c.emi_number,c.emi_amount,c.emi_due_on,c.penality from oxy_loan_request a  join oxy_loan b  on (a.id=b.borrower_parent_requestid) join loan_emi_card c on (b.id=c.loan_id) join public.user d on (a.user_id=d.id) join user_personal_details e on (a.user_id=e.user_id) where (a.parent_request_id is null or a.parent_request_id=0) and c.emi_paid_on is null and b.borrower_disbursed_date is NOT NULL and c.emi_due_on>=:monthStartDate and c.emi_due_on<=:monthEndtDate and c.loan_id=:loanId ORDER  BY c.emi_number", nativeQuery = true)
	public List<Object[]> getCurrentMonthEmisByLoanId(@Param("monthStartDate") Date monthStartDate,
			@Param("monthEndtDate") Date monthEndtDate, @Param("loanId") int loanId);

	@Query(value = "select c.emi_number,c.emi_amount,c.emi_due_on,c.penality from oxy_loan_request a  join oxy_loan b  on (a.id=b.borrower_parent_requestid) join loan_emi_card c on (b.id=c.loan_id) join public.user d on (a.user_id=d.id) join user_personal_details e on (a.user_id=e.user_id) where (a.parent_request_id is null or a.parent_request_id=0) and c.emi_paid_on is null and b.borrower_disbursed_date is NOT NULL and c.emi_due_on>=:monthStartDate  and c.loan_id=:loanId ORDER  BY c.emi_number", nativeQuery = true)
	public List<Object[]> getFurtherMonthEmisByLoanId(@Param("monthStartDate") Date monthStartDate,
			@Param("loanId") int loanId);

	@Query(value = "select c.emi_number,c.emi_amount,c.emi_due_on,c.penality from oxy_loan_request a  join oxy_loan b  on (a.id=b.borrower_parent_requestid) join loan_emi_card c on (b.id=c.loan_id) join public.user d on (a.user_id=d.id) join user_personal_details e on (a.user_id=e.user_id) where (a.parent_request_id is null or a.parent_request_id=0) and c.emi_paid_on is null and b.borrower_disbursed_date is NOT NULL and c.loan_id=:loanId ORDER  BY c.emi_number;\r\n", nativeQuery = true)
	public List<Object[]> getPendingEmisByLoanId(@Param("loanId") int loanId);

	@Query(value = "select DISTINCT L.emi_number ,L.emi_due_on from loan_emi_card L join oxy_loan o on(L.loan_id=o.id) where o.loan_status='Active' and o.admin_comments='DISBURSED' and o.borrower_parent_requestid=:borrowerUserId group by L.emi_due_on,L.emi_number order by L.emi_due_on ;\r\n"
			+ "", nativeQuery = true)
	public List<Object[]> getDueDateForApplication(@Param("borrowerUserId") int borrowerUserId);

	@Query(value = " select L.emi_number ,sum(L.emi_amount) as x ,sum(L.emi_late_fee_charges) as y from loan_emi_card L join oxy_loan o on(L.loan_id=o.id) where o.loan_status='Active' and o.admin_comments='DISBURSED' and o.borrower_parent_requestid=:borrowerApplicationId  group by L.emi_number order by L.emi_number;\r\n"
			+ "\r\n" + "", nativeQuery = true)
	public List<Object[]> getSumOfEmiAndInterest(@Param("borrowerApplicationId") int borrowerApplicationId);

	@Query(value = "SELECT id FROM public.oxy_loan where borrower_parent_requestid=:borrowerUserId and (loan_status='Active') and admin_comments='DISBURSED';", nativeQuery = true)
	public List<Integer> getloansForAnApplication(@Param("borrowerUserId") int borrowerUserId);

	@Query(value = "select DISTINCT L.emi_number ,sum(L.excess_of_emi_amount) from loan_emi_card L join oxy_loan o on(L.loan_id=o.id) where o.loan_status='Active' and o.admin_comments='DISBURSED' and o.borrower_parent_requestid=:borrowerUserId and emi_paid_on is null group by L.emi_number order by L.emi_number  ;\r\n"
			+ "", nativeQuery = true)
	public List<Object[]> getExcessAmountPaid(@Param("borrowerUserId") int borrowerUserId);

	@Query(value = "\r\n"
			+ "SELECT sum(disbursment_amount) FROM  public.oxy_loan where borrower_parent_requestid=:borrowerApplicationId and (loan_status='Active') and admin_comments='DISBURSED';", nativeQuery = true)
	public Double getDisburmentAmount(@Param("borrowerApplicationId") int borrowerApplicationId);

	@Query(value = "SELECT a.lender_user_id,b.first_name,b.last_name,a.id FROM public.oxy_loan a join   public.user_personal_details b on a.lender_user_id=b.user_id where a.borrower_parent_requestid=:borrowerApplicationId and (a.loan_status='Active') and a.admin_comments='DISBURSED' order by a.lender_user_id;\r\n"
			+ " ", nativeQuery = true)
	public List<Object[]> getLendersForApplication(@Param("borrowerApplicationId") int borrowerApplicationId);

	@Query(value = "select count(distinct(borrower_parent_requestid)) from oxy_loan where borrower_user_id=:userid and loan_status=:loanstatus", nativeQuery = true)
	public Integer getApplicationCount(@Param("userid") int userId, @Param("loanstatus") String loanStatus);

	@Query(value = "select sum(L.emi_amount) as x from loan_emi_card L join oxy_loan o on(L.loan_id=o.id) where (o.loan_status='Active') and o.admin_comments='DISBURSED' and o.borrower_parent_requestid=:borrowerApplicationId and L.emi_number=:emiNumber group by L.emi_number order by L.emi_number;", nativeQuery = true)
	public double getEmiAmount(@Param("borrowerApplicationId") int borrowerApplicationId,
			@Param("emiNumber") int emiNumber);

	@Query(value = "SELECT lender_user_id FROM public.oxy_loan where id=:loanId", nativeQuery = true)
	public Integer getLenderEmail(@Param("loanId") int loanId);

	@Query(value = "\r\n"
			+ "SELECT lender_parent_requestid FROM public.oxy_loan where loan_status='Active' and admin_comments='DISBURSED' order by lender_parent_requestid;"
			+ "", nativeQuery = true)
	public List<Integer> getLenderApplicationIds();

	@Query(value = "\r\n" + "\r\n"
			+ "  SELECT id FROM public.oxy_loan where lender_parent_requestid=:lenderApplicationId order by id;"
			+ "", nativeQuery = true)
	public List<Integer> getLoanIdsForApplication(@Param("lenderApplicationId") int lenderApplicationId);

	@Query(value = "\r\n"
			+ "SELECT Distinct borrower_parent_requestid FROM public.oxy_loan where borrower_user_id=:borrowerId and rate_of_interest=:rateOfInterest and duration=:duration and loan_status='Active'and admin_comments='DISBURSED'", nativeQuery = true)
	public List<Integer> findByBorrowerIdAndRateOfInterestAndDuration(@Param("borrowerId") Integer borrowerId,
			@Param("rateOfInterest") Double rateOfInterest, @Param("duration") Integer duration);

	@Query(value = "SELECT Distinct borrower_parent_requestid,id FROM public.oxy_loan where borrower_user_id=:borrowerId and rate_of_interest=:rateOfInterest and duration=:duration and loan_status='Active'and admin_comments='DISBURSED' order by id;", nativeQuery = true)
	public List<Object[]> getApplicationLevelObjects(@Param("borrowerId") Integer borrowerId,
			@Param("rateOfInterest") Double rateOfInterest, @Param("duration") Integer duration);

	@Query(value = "SELECT sum(b.emi_amount) FROM public.oxy_loan a join public.loan_emi_card b on a.id=b.loan_id where a.borrower_parent_requestid=:borrowerApplicationId and a.loan_status='Active'and a.admin_comments='DISBURSED' and b.emi_number=1;", nativeQuery = true)
	public Double getFirstEmiAmountForApplication(@Param("borrowerApplicationId") int borrowerApplicationId);

	@Query(value = "SELECT sum(b.emi_amount) FROM public.oxy_loan a join public.loan_emi_card b on a.id=b.loan_id where a.borrower_parent_requestid=:borrowerApplicationId and a.loan_status='Active'and a.admin_comments='DISBURSED' and b.emi_number=2;", nativeQuery = true)
	public Double getSecondEmiAmountForApplication(@Param("borrowerApplicationId") int borrowerApplicationId);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where borrower_user_id=:userId and borrower_disbursed_date is not null and admin_comments is not null;", nativeQuery = true)
	public Double getDisbursmentAmountForOldUsers(@Param("userId") int userId);

	@Query(value = "SELECT * FROM public.oxy_loan a where a.lender_user_id=:id and a.loan_status='Active' and a.admin_comments='DISBURSED' order by id;", nativeQuery = true)
	public List<OxyLoan> findByLenderUserId(@Param("id") int id);

	@Query(value = "SELECT * FROM public.oxy_loan a where a.borrower_user_id=:id and a.loan_status='Active' and a.admin_comments='DISBURSED' order by id;", nativeQuery = true)
	public List<OxyLoan> findByBorrowerUserId(@Param("id") int id);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where borrower_user_id=:borrowerUserId and lender_user_id=:lenderUserId and  (loan_status='Agreed' OR loan_status='Active');", nativeQuery = true)
	public Double getDisbursedAmount(@Param("borrowerUserId") int borrowerUserId,
			@Param("lenderUserId") int lenderUserId);

	@Query(value = "SELECT sum(borrower_transaction_fee) FROM public.oxy_loan where EXTRACT(MONTH FROM borrower_disbursed_date)=:month and EXTRACT(YEAR FROM borrower_disbursed_date)=:year", nativeQuery = true)
	public Double getSumOfBorrersFeeBasedOndate(@Param("month") int month, @Param("year") int year);

	@Query(value = "SELECT DISTINCT borrower_parent_requestid FROM public.oxy_loan where EXTRACT(MONTH FROM borrower_disbursed_date)=:month and EXTRACT(YEAR FROM borrower_disbursed_date)=:year", nativeQuery = true)
	public List<Integer> getApplictionIdsBasedOnDate(@Param("month") int month, @Param("year") int year);

	@Query(value = "SELECT sum(borrower_transaction_fee) FROM public.oxy_loan where EXTRACT(MONTH FROM borrower_disbursed_date)=:month and EXTRACT(YEAR FROM borrower_disbursed_date)=:year and borrower_parent_requestid=:borrowerParentRequestId", nativeQuery = true)
	public Double getBorrowerFeeBasedOnDateAndId(@Param("month") int month, @Param("year") int year,
			@Param("borrowerParentRequestId") int borrowerParentRequestId);

	@Query(value = "SELECT DISTINCT id FROM public.oxy_loan where EXTRACT(MONTH FROM borrower_disbursed_date)=:month and EXTRACT(YEAR FROM borrower_disbursed_date)=:year and borrower_parent_requestid=:borrowerParentRequestId", nativeQuery = true)
	public List<Integer> getPercentageForFee(@Param("month") int month, @Param("year") int year,
			@Param("borrowerParentRequestId") int borrowerParentRequestId);

	@Query(value = "SELECT sum(borrower_transaction_fee) from public.oxy_loan", nativeQuery = true)
	public Double getTotalFree();

	@Query(value = "SELECT id,disbursment_amount,borrower_disbursed_date FROM public.oxy_loan where borrower_parent_requestid=:borrowerParentRequestId and borrower_disbursed_date>=:accepetedDate and admin_comments is not null;", nativeQuery = true)
	public List<Object[]> getBorrowersBasedOnApplicationId(
			@Param("borrowerParentRequestId") int borrowerParentRequestId, @Param("accepetedDate") Date accepetedDate);

	@Query(value = "\r\n"
			+ "SELECT count(id) FROM public.oxy_loan where borrower_parent_requestid=:borrowerParentRequestId and loan_status='Agreed'", nativeQuery = true)
	public Integer getAgreedLoansCount(@Param("borrowerParentRequestId") int borrowerParentRequestId);

	@Query(value = "SELECT loan_id FROM public.oxy_loan where loan_status='Active' and borrower_parent_requestid=:borrowerParentRequestId", nativeQuery = true)
	public List<String> getLoanIdsBasedOnApplicationId(@Param("borrowerParentRequestId") int borrowerParentRequestId);

	@Query(value = "SELECT concat(b.first_name,' ',b.last_name),a.disbursment_amount,a.id,a.loan_status,a.duration,a.repayment_method,d.loan_owner,extract(MONTH from a.borrower_disbursed_date) FROM public.oxy_loan a join public.user_personal_details b on a.borrower_user_id=b.user_id join public.loan_emi_card c on a.id=c.loan_id join  public.\"user\" d on b.user_id=d.id where a.lender_user_id=:userId and a.loan_status='Closed' and a.admin_comments is not null group by  concat(b.first_name,' ',b.last_name),a.disbursment_amount,a.id,a.loan_status,a.duration,a.repayment_method,d.loan_owner,extract(MONTH from a.borrower_disbursed_date) order by a.id;\r\n"
			+ "", nativeQuery = true)
	public List<Object[]> getLenderClosedLoansInfo(@Param("userId") int userId);

	@Query(value = "SELECT emi_due_on FROM public.loan_emi_card where loan_id=:loanId and emi_number=1;", nativeQuery = true)
	public Date getFirstEmiDueDate(@Param("loanId") int loanId);

	@Query(value = "SELECT emi_amount FROM public.loan_emi_card where loan_id=:loanId and emi_number=1;", nativeQuery = true)
	public Double getFirstEmiAmount(@Param("loanId") int loanId);

	@Query(value = "SELECT concat(b.first_name,' ',b.last_name),a.disbursment_amount,a.id,a.loan_status,a.duration,a.repayment_method,d.loan_owner,extract(MONTH from a.borrower_disbursed_date) FROM public.oxy_loan a join public.user_personal_details b on a.borrower_user_id=b.user_id join public.loan_emi_card c on a.id=c.loan_id join  public.\"user\" d on b.user_id=d.id where a.lender_user_id=:userId and a.loan_status='Active' and a.admin_comments is not null group by  concat(b.first_name,' ',b.last_name),a.disbursment_amount,a.id,a.loan_status,a.duration,a.repayment_method,d.loan_owner,extract(MONTH from a.borrower_disbursed_date) order by a.id;\r\n"
			+ "", nativeQuery = true)
	public List<Object[]> getLenderActiveLoanInfo(@Param("userId") int userId);

	@Query(value = " select o.loan_id,o.disbursment_amount,o.loan_status,sum(l.emi_interst_amount) as Profit,\r\n"
			+ " concat(p.first_name,' ',p.last_name) as borrower_name,count(o.loan_id)\r\n"
			+ " from public.oxy_loan o\r\n"
			+ " join public.loan_emi_card l on (o.id=l.loan_id) join public.user_personal_details P on (o.borrower_user_id=P.user_id)\r\n"
			+ " where o.lender_user_id=:id and (current_date-INTERVAL '1 month'* :monthNo <=l.emi_paid_on) and o.loan_status  in ('Active','Closed') \r\n"
			+ "  group by o.loan_id,o.disbursment_amount,o.loan_status,p.first_name,p.last_name", nativeQuery = true)
	public List<Object[]> getLenderProfit(@Param("id") Integer id, @Param("monthNo") int monthNo);

	@Query(value = "SELECT a.user_id,a.transaction_amount,a.transaction_date,CONCAT(b.first_name,' ',b.last_name),c.unique_number,c.registered_on FROM public.lender_scrow_wallet a join public.user_personal_details b on a.user_id=b.user_id join public.user c on a.user_id=c.id where a.transaction_date >= :startDate and a.transaction_date <= :endDate and a.status='APPROVED' order by c.registered_on;", nativeQuery = true)
	public List<Object[]> getLenderWalletDetails(@Param("startDate") Timestamp startDate,
			@Param("endDate") Timestamp endDate);

	@Query(value = "SELECT count(id) FROM public.oxy_loan where (borrower_user_id=35644 OR borrower_user_id=35646 OR borrower_user_id=35641 OR borrower_user_id=35640 OR borrower_user_id=35210 OR borrower_user_id=35196) and lender_user_id=:lenderUserId and admin_comments='DISBURSED' and loan_status='Active';", nativeQuery = true)
	public Integer getLoanIdsMappedToLenderIdCount(@Param("lenderUserId") int lenderUserId);

	@Query(value = "SELECT id FROM public.oxy_loan where (borrower_user_id=35644 OR borrower_user_id=35646 OR borrower_user_id=35641 OR borrower_user_id=35640 OR borrower_user_id=35210 OR borrower_user_id=35196) and lender_user_id=:lenderUserId and admin_comments='DISBURSED' and loan_status='Active';", nativeQuery = true)
	public List<Integer> getLoanIdsMappedToLenderId(@Param("lenderUserId") int lenderUserId);

	/*
	 * @Query(value =
	 * "SELECT count(id) FROM public.oxy_loan where borrower_user_id=36548 and lender_user_id=:lenderUserId and admin_comments='DISBURSED' and loan_status='Active';"
	 * , nativeQuery = true) public Integer
	 * getLoanIdsMappedToLenderIdCount(@Param("lenderUserId") int lenderUserId);
	 * 
	 * @Query(value =
	 * "SELECT id FROM public.oxy_loan where borrower_user_id=36548 and lender_user_id=:lenderUserId and admin_comments='DISBURSED' and loan_status='Active';"
	 * , nativeQuery = true) public List<Integer>
	 * getLoanIdsMappedToLenderId(@Param("lenderUserId") int lenderUserId);
	 */
	@Query(value = "SELECT sum(borrower_transaction_fee) FROM public.oxy_loan where admin_comments is not null and to_char(borrower_disbursed_date, 'mm-yyyy')=:givenDate", nativeQuery = true)
	public Double getBorrowerMonthFeeSumValue(@Param("givenDate") String giveDate);

	@Query(value = "SELECT a.borrower_user_id,CONCAT(b.first_name,' ',b.last_name),sum(a.borrower_transaction_fee) FROM \r\n"
			+ "public.oxy_loan a join  public.user_personal_details b on a.borrower_user_id=b.user_id where a.admin_comments is not null and to_char(a.borrower_disbursed_date, 'mm-yyyy')=:givenDate group by a.borrower_user_id,b.first_name,b.last_name order by a.borrower_user_id;", nativeQuery = true)
	public List<Object[]> getListOfBorrowersWithFeeValue(@Param("givenDate") String giveDate);

	@Query(value = "SELECT b.lender_user_id,CONCAT(c.first_name,' ',c.last_name),d.account_number,b.borrower_user_id,b.borrower_disbursed_date,b.loan_id,a.emi_amount,a.emi_due_on,DATE(a.emi_paid_on) FROM public.loan_emi_card a join public.oxy_loan b on a.loan_id=b.id join public.user_personal_details c on b.lender_user_id=c.user_id join public.user_bank_details d on c.user_id=d.user_id where extract(MONTH from a.emi_due_on)=:currentMonth and extract(YEAR from a.emi_due_on)=:year and b.loan_status='Active' and b.admin_comments is not null order by b.lender_user_id;\r\n"
			+ "", nativeQuery = true)
	public List<Object[]> gettingLendersLoanInfo(@Param("currentMonth") Integer currentMonth,
			@Param("year") Integer year);

	@Query(value = "select distinct(a.user_id),CONCAT(a.first_name,' ',a.last_name),e.account_number,sum(d.emi_amount),d.emi_due_on from public.user_personal_details a join public.user b on b.id=a.user_id join public.oxy_loan c on c.lender_user_id=a.user_id join public.loan_emi_card d on d.loan_id=c.id join public.user_bank_details e on a.user_id=e.user_id  where\r\n"
			+ "extract(month from d.emi_due_on)=:currentMonth and extract(year from d.emi_due_on)=:year and c.loan_status='Active' and c.admin_comments is not null group by a.user_id,CONCAT(a.first_name,' ',a.last_name),e.account_number,d.emi_due_on order by a.user_id\r\n"
			+ "limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getLenderLoansEmiInfo(@Param("currentMonth") Integer currentMonth,
			@Param("year") Integer year, @Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "select count(distinct(a.user_id))\r\n"
			+ "from public.user_personal_details a join public.user b on b.id=a.user_id join public.oxy_loan c on\r\n"
			+ "c.lender_user_id=a.user_id join public.loan_emi_card d on d.loan_id=c.id \r\n"
			+ "join public.user_bank_details e on a.user_id=e.user_id  where \r\n"
			+ "extract(month from d.emi_due_on)=:currentMonth and extract(year from d.emi_due_on)=:year and c.loan_status='Active'\r\n"
			+ "and c.admin_comments is not null", nativeQuery = true)
	public Integer getCountOfTotalLenders(@Param("currentMonth") Integer currentMonth, @Param("year") Integer year);

	@Query(value = "SELECT b.lender_user_id,CONCAT(c.first_name,' ',c.last_name),d.account_number,b.borrower_user_id,\r\n"
			+ "b.borrower_disbursed_date,b.loan_id,a.emi_amount,a.emi_due_on,DATE(a.emi_paid_on) FROM \r\n"
			+ "public.loan_emi_card a join public.oxy_loan b on a.loan_id=b.id join public.user_personal_details \r\n"
			+ "c on b.lender_user_id=c.user_id join public.user_bank_details d on c.user_id=d.user_id \r\n"
			+ "where b.lender_user_id=:lenderId and \r\n"
			+ "extract(MONTH from a.emi_due_on)=date_part('month', (SELECT current_timestamp)) and \r\n"
			+ "extract(YEAR from a.emi_due_on)=date_part('year', (SELECT current_timestamp)) and\r\n"
			+ "b.loan_status='Active' and b.admin_comments is not null", nativeQuery = true)
	public List<Object[]> getLenderLoanInfoBasedOnLenderId(@Param("lenderId") Integer lenderId);

	@Query(value = "SELECT b.borrower_user_id,b.borrower_disbursed_date,b.loan_id,a.emi_amount,a.emi_due_on,DATE(a.emi_paid_on) FROM \r\n"
			+ "public.loan_emi_card a join public.oxy_loan b on a.loan_id=b.id join public.user_personal_details \r\n"
			+ "c on b.lender_user_id=c.user_id join public.user_bank_details d on c.user_id=d.user_id \r\n"
			+ "where b.lender_user_id=:lenderId and \r\n" + "extract(MONTH from a.emi_due_on)=:month and \r\n"
			+ "extract(YEAR from a.emi_due_on)=:year and \r\n"
			+ "b.loan_status='Active' and b.admin_comments is not null", nativeQuery = true)
	public List<Object[]> gettingLendersLoanInformation(@Param("lenderId") Integer lenderId,
			@Param("month") Integer month, @Param("year") Integer year);

	@Query(value = "SELECT b.lender_user_id,CONCAT(c.first_name,' ',c.last_name),d.account_number,sum(a.emi_amount) FROM \r\n"
			+ "public.loan_emi_card a join public.oxy_loan b on a.loan_id=b.id join public.user_personal_details \r\n"
			+ "c on b.lender_user_id=c.user_id join public.user_bank_details d on c.user_id=d.user_id \r\n"
			+ "where b.lender_user_id=:lenderId and \r\n" + "extract(MONTH from a.emi_due_on)=:month and \r\n"
			+ "extract(YEAR from a.emi_due_on)=:year and \r\n"
			+ "b.loan_status='Active' and b.admin_comments is not null \r\n"
			+ "group by b.lender_user_id,CONCAT(c.first_name,' ',c.last_name),d.account_number;", nativeQuery = true)
	public List<Object[]> getLenderInfo(@Param("lenderId") Integer lenderId, @Param("month") Integer month,
			@Param("year") Integer year);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where borrower_user_id=:userId and admin_comments is not null and loan_status='Active';", nativeQuery = true)
	public Double getTotalActiveAmount(int userId);

	@Query(value = "SELECT count(id) FROM public.oxy_loan where borrower_parent_requestid=:id and admin_comments is not null;", nativeQuery = true)
	public Integer getCountValueOfDisbursedLoansBasedOnId(@Param("id") Integer id);

	@Query(value = "select sum(disbursment_amount) from public.oxy_loan where borrower_user_id=:borrowerUserId and lender_user_id=:lenderUserId and loan_status='Active' and admin_comments is not null", nativeQuery = true)
	public Double getTotalAmountDisbursedForParticularrUsers(@Param("borrowerUserId") Integer borrowerUserId,
			@Param("lenderUserId") Integer lenderUserId);

	@Query(value = "SELECT * FROM public.oxy_loan where id=:loanId and (loan_status='Agreed' OR loan_status='Active') and admin_comments is null;", nativeQuery = true)
	public OxyLoan getDetailsByLoanIdToReject(int loanId);

	@Query(value = "SELECT * FROM public.oxy_loan where id=:loanId and loan_accepted_date>='2021-04-01';", nativeQuery = true)
	public OxyLoan getLenderAndBorrowerAgreement(int loanId);

	@Query(value = "SELECT a.loan_id FROM public.oxy_loan a join public.oxy_loan_request b on a.loan_id=b.loan_id where a.borrower_parent_requestid=:borrowerParentRequestId and a.admin_comments is null and a.loan_status='Active' and (b.enachtype='MANUALENACH' OR b.is_ecs_activated='t');", nativeQuery = true)
	public List<String> getAllLoansByParentRequestId(@Param("borrowerParentRequestId") int borrowerParentRequestId);

	@Query(value = "SELECT sum(a.disbursment_amount) FROM public.oxy_loan a join public.oxy_loan_request b on a.loan_id=b.loan_id where a.borrower_parent_requestid=:borrowerParentRequestId and a.admin_comments is null and a.loan_status='Active' and (b.enachtype='MANUALENACH' OR b.is_ecs_activated='t');", nativeQuery = true)
	public Double getDisbursmentAmountBeforeDisbursment(
			@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	@Query(value = "SELECT count(a.id) FROM public.oxy_loan a join public.oxy_loan_request b on a.loan_id=b.loan_id where a.loan_status='Active' and (b.enachtype='MANUALENACH' or b.is_ecs_activated='t') and a.admin_comments is null;", nativeQuery = true)
	public Integer getDisbusmentPendingLoanIdsDetailsCount();

	@Query(value = "SELECT a.id,a.lender_user_id,a.borrower_user_id,a.disbursment_amount,a.duration,b.loan_request_id,b.enachtype,a.loan_id FROM public.oxy_loan a join public.oxy_loan_request b on a.loan_id=b.loan_id where a.loan_status='Active' and (b.enachtype='MANUALENACH' or b.is_ecs_activated='t') and a.admin_comments is null limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getDisbusmentPendingLoanIdsDetails(@Param("pageSize") int pageSize,
			@Param("pageNo") int pageNo);

	@Query(value = "select o.loan_id from public.oxy_loan o join public.oxy_loan_request r on(o.loan_id=r.loan_id) where  o.borrower_parent_requestid=:loanRequestID and o.loan_status='Agreed' and r.admin_comments is null", nativeQuery = true)
	public List<String> fetchApplicationLevelLoansToApprove(@Param("loanRequestID") Integer loanRequestID);

	@Query(value = "select count(o.loan_id) from public.oxy_loan o join public.oxy_loan_request r on(o.loan_id=r.loan_id) \r\n"
			+ "where  o.borrower_parent_requestid=:loanRequestID and o.loan_status='Agreed' and r.admin_comments is null", nativeQuery = true)
	public Integer getCountOfLoansToApprove(@Param("loanRequestID") Integer loanRequestID);

	@Query(value = "SELECT a.loan_id,a.lender_user_id,a.borrower_user_id,a.disbursment_amount,b.enachtype,b.loan_request_id FROM public.oxy_loan a join public.oxy_loan_request b on a.loan_id=b.loan_id where a.loan_id=:loanId and a.loan_status='Active' and (b.enachtype='MANUALENACH' or b.is_ecs_activated='t') and a.admin_comments is null;", nativeQuery = true)
	public List<Object[]> searchByLoanIdAndDisburmentPending(@Param("loanId") String loanId);

	@Query(value = " SELECT  count(a.loan_id) FROM public.oxy_loan a join public.oxy_loan_request b on a.loan_id=b.loan_id where a.lender_user_id=:userId and a.loan_status='Active' and (b.enachtype='MANUALENACH' or b.is_ecs_activated='t') and a.admin_comments is null", nativeQuery = true)
	public Integer countByLenderIdAndDisburmentPending(@Param("userId") Integer userId);

	@Query(value = "SELECT  a.loan_id,a.lender_user_id,a.borrower_user_id,a.disbursment_amount,b.enachtype,b.loan_request_id FROM public.oxy_loan a join public.oxy_loan_request b on a.loan_id=b.loan_id where a.lender_user_id=:userId and a.loan_status='Active' and (b.enachtype='MANUALENACH' or b.is_ecs_activated='t') and a.admin_comments is null;", nativeQuery = true)
	public List<Object[]> searchByLenderIdAndDisburmentPending(@Param("userId") Integer userId);

	@Query(value = " SELECT  count(a.loan_id) FROM public.oxy_loan a join public.oxy_loan_request b on a.loan_id=b.loan_id where a.borrower_user_id=:userId and a.loan_status='Active' and (b.enachtype='MANUALENACH' or b.is_ecs_activated='t') and a.admin_comments is null", nativeQuery = true)
	public Integer countByBorrowerIdAndDisburmentPending(@Param("userId") Integer userId);

	@Query(value = "SELECT  a.loan_id,a.lender_user_id,a.borrower_user_id,a.disbursment_amount,b.enachtype,b.loan_request_id FROM public.oxy_loan a join public.oxy_loan_request b on a.loan_id=b.loan_id where a.borrower_user_id=:userId and a.loan_status='Active' and (b.enachtype='MANUALENACH' or b.is_ecs_activated='t') and a.admin_comments is null;", nativeQuery = true)
	public List<Object[]> searchByBorrowerIdAndDisburmentPending(@Param("userId") Integer userId);

	@Query(value = "select o.loan_id,o.lender_user_id,o.disbursment_amount from public.oxy_loan o join public.oxy_loan_request r on(o.loan_id=r.loan_id) \r\n"
			+ "where  o.borrower_parent_requestid=:loanRequestID and o.loan_status='Active' \r\n"
			+ "and (r.enachtype='MANUALENACH' or r.is_ecs_activated='t') and o.admin_comments is null", nativeQuery = true)
	public List<Object[]> fetchBorrowerApplicationLoans(@Param("loanRequestID") Integer loanRequestID);

	@Query(value = "select count(o.loan_id) from public.oxy_loan o join public.oxy_loan_request r on(o.loan_id=r.loan_id) \r\n"
			+ "where  o.borrower_parent_requestid=:loanRequestID and o.loan_status='Active' \r\n"
			+ "and (r.enachtype='MANUALENACH' or r.is_ecs_activated='t') and o.admin_comments is null ", nativeQuery = true)
	public Integer getTotalCountOfLoans(@Param("loanRequestID") Integer loanRequestID);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where borrower_user_id=:borrowerId and deal_id=:dealId", nativeQuery = true)
	public Double checkAlreadyAgreementsAgenerated(@Param("borrowerId") Integer borrowerId,
			@Param("dealId") Integer dealId);

	@Query(value = "select a.id,a.email,a.mobile_number,concat(b.first_name,'',b.last_name),a.lender_group_id,c.lender_group_name from \r\n"
			+ "public.user a join public.user_personal_details b on a.id=b.user_id join \r\n"
			+ "public.oxy_lenders_group c on c.id=a.lender_group_id where a.primary_type='LENDER' and c.lender_group_name=:groupName\r\n"
			+ "limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> findingListOfLendersMappedToGroup(@Param("groupName") String groupName,
			@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where deal_id=:dealId and admin_comments is null", nativeQuery = true)
	public Double getDealIdDisbursmentPendingAmount(@Param("dealId") Integer dealId);

	@Query(value = "SELECT loan_id FROM public.oxy_loan where deal_id=:dealId and admin_comments is null", nativeQuery = true)
	public List<String> getListOfDisbursmentPendingLoanIds(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where borrower_parent_requestid=:borrowerParentRequestid and (is_ecs_activated='f' OR is_ecs_activated='false' OR (is_ecs_activated is null)) and loan_status='Active'", nativeQuery = true)
	public Double getTotalActiveAmountByParentId(int borrowerParentRequestid);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where lender_user_id=:lenderId and deal_id=:dealId", nativeQuery = true)
	public Double checkAlreadyAgreementsAgeneratedToLender(@Param("lenderId") Integer lenderId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where deal_id=:dealId and admin_comments='DISBURSED'", nativeQuery = true)
	public Double getDealIdDisbursmentCompletedAmount(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(Disbursment_amount) FROM public.oxy_loan where loan_status='Active' and admin_comments='DISBURSED' and lender_user_id=:userId and deal_id=0", nativeQuery = true)
	public Double getLenderActiveAmountButNotDealAmount(@Param("userId") Integer userId);

	@Query(value = "update public.oxy_loan set loan_status='Closed' where deal_id=:dealId RETURNING 1", nativeQuery = true)
	public List<Integer> dealLevelClosing(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * from public.oxy_loan where deal_id=:dealId and admin_comments='DISBURSED' and loan_status='Active'", nativeQuery = true)
	public List<OxyLoan> getListLoansBasedOnDealId(@Param("dealId") Integer dealId);

	@Query(value = "select * from public.oxy_loan where lender_user_id=:userId and borrower_user_id=:borrowerId and loan_status='Active' ", nativeQuery = true)
	public OxyLoan findbyBorrowerUserIdAndLenderUserIdAndLanStatus(@Param("userId") Integer userId,
			@Param("borrowerId") int borrowerId);

	@Query(value = "select * from public.oxy_loan where lender_user_id=:id and loan_status='Active' \r\n"
			+ "and borrower_disbursed_date is not null and admin_comments='DISBURSED'limit 1", nativeQuery = true)
	public OxyLoan findingMappedBorrower(@Param("id") Integer id);

	@Query(value = "select * from public.oxy_loan where borrower_user_id=:id and loan_status='Active' \r\n"
			+ "and borrower_disbursed_date is not null and admin_comments='DISBURSED' limit 1", nativeQuery = true)
	public OxyLoan findingMappedLender(@Param("id") Integer id);

	@Query(value = "select sum(disbursment_amount) from public.oxy_loan where loan_status='Active' and admin_comments='DISBURSED'\r\n"
			+ "and borrower_disbursed_date is not null and borrower_user_id=:id\r\n" + "", nativeQuery = true)
	public Double findingSumOfActiveDisbursmentAmount(@Param("id") Integer id);

	@Query(value = "select sum(disbursment_amount) from public.oxy_loan where loan_status='Closed' and admin_comments='DISBURSED'\r\n"
			+ "and borrower_disbursed_date is not null and borrower_user_id=:id\r\n" + "", nativeQuery = true)
	public Double findingSumOfClosedDisbursmentAmount(@Param("id") Integer id);

	@Query(value = "select distinct(borrower_user_id) from public.oxy_loan where\r\n"
			+ " borrower_disbursed_date is not null and admin_comments='DISBURSED' and \r\n"
			+ "TO_CHAR(borrower_disbursed_date,'YYYY-MM-DD')<=:endDate", nativeQuery = true)
	public List<Integer> findingListOfBorrowersDisbursed(@Param("endDate") String endDate);

	@Query(value = "select distinct(borrower_user_id) from public.oxy_loan where\r\n"
			+ "borrower_disbursed_date is not null and admin_comments='DISBURSED' and \r\n"
			+ "TO_CHAR(borrower_disbursed_date,'YYYY-MM-DD')>=:startDate and TO_CHAR(borrower_disbursed_date,'YYYY-MM-DD')<=:endDate", nativeQuery = true)
	public List<Integer> findingListOfBorrowersDisbursedBasedOnStartAndEndDate(@Param("startDate") String startDate,
			@Param("endDate") String endDate);

	@Query(value = "SELECT count(DISTINCT borrower_parent_requestid) FROM public.oxy_loan Where borrower_user_id=:userId and loan_status='Active' and admin_comments='DISBURSED'", nativeQuery = true)
	public Integer findingNumberOfApplicationsToBorrower(@Param("userId") Integer userId);

	@Query(value = "SELECT  DISTINCT borrower_parent_requestid FROM public.oxy_loan Where borrower_user_id=:userId and loan_status='Active' and admin_comments='DISBURSED'", nativeQuery = true)
	public Integer getCurrentApplication(@Param("userId") Integer userId);

	@Query(value = "SELECT id FROM public.oxy_loan Where borrower_user_id=:userId and loan_status='Active' and admin_comments='DISBURSED'", nativeQuery = true)
	public List<Integer> findNumberOfActiveLoans(@Param("userId") Integer userId);

	@Query(value = "select count(loan_id) from public.oxy_loan where borrower_user_id=:id", nativeQuery = true)
	public Integer findingCountOfLoans(@Param("id") Integer id);

	@Query(value = "select loan_request_id from public.oxy_loan_request where user_id=:id and (parent_request_id is null or parent_request_id=0)", nativeQuery = true)
	public List<String> findingListOfApplications(@Param("id") Integer id);

	@Query(value = "select sum(disbursment_amount) from public.oxy_loan where borrower_user_id=:id", nativeQuery = true)
	public Double findingSumOfAmountDisbursedForBorrower(@Param("id") Integer id);

	@Query(value = "SELECT * FROM public.oxy_loan where lender_user_id=:lenderId and borrower_user_id=:borrowerId and deal_id=:dealId and admin_comments='DISBURSED'", nativeQuery = true)
	public OxyLoan checkLoanDisbursedToLender(@Param("lenderId") Integer lenderId,
			@Param("borrowerId") Integer borrowerId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT COALESCE(SUM(disbursment_amount),0) FROM public.oxy_loan where borrower_user_id=:borrowerId and deal_id=:dealId and loan_status='Active' and admin_comments='DISBURSED' ", nativeQuery = true)
	public Double sumOfBorrowerDisbursmentAmountToDeal(@Param("borrowerId") Integer borrowerId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT a.loan_id FROM public.oxy_loan a join public.enach_mandate b on a.id=b.oxy_loan_id where b.mandate_status='SUCCESS' and a.borrower_parent_requestid=:borrowerParentRequestId", nativeQuery = true)
	public List<String> getAllLoansEnachActivatedByParentRequestId(
			@Param("borrowerParentRequestId") int borrowerParentRequestId);

	@Query(value = "SELECT count(*) from public.oxy_loan where admin_comments='DISBURSED' and loan_status='Active' and to_char(borrower_disbursed_date, 'mm-yyyy')=:monthAndYear ", nativeQuery = true)
	public Integer countOfLoansDisbursedInGivenMonth(@Param("monthAndYear") String monthAndYear);

	@Query(value = "SELECT sum(disbursment_amount) from public.oxy_loan where admin_comments='DISBURSED' and loan_status='Active' and to_char(borrower_disbursed_date, 'mm-yyyy')=:monthAndYear ", nativeQuery = true)
	public Double totalLoansDisbursedInGivenMonth(@Param("monthAndYear") String monthAndYear);

	@Query(value = "SELECT borrower_parent_requestid from public.oxy_loan where admin_comments='DISBURSED' and loan_status='Active' and to_char(borrower_disbursed_date, 'mm-yyyy')=:monthAndYear group by borrower_parent_requestid ", nativeQuery = true)
	public List<Integer> getBorrowerParentRequestIds(@Param("monthAndYear") String monthAndYear);

	@Query(value = "select id from public.oxy_loan where borrower_user_id=:id and loan_status='Active' and borrower_disbursed_date is not null order by id desc", nativeQuery = true)
	public List<Integer> borrowerActiveAndDisbursedLoans(@Param("id") Integer id);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where borrower_parent_requestid=:appId and (loan_status='Agreed' OR loan_status='Active' OR loan_status='Closed' OR loan_status='Hold')", nativeQuery = true)
	public Double acceptedAmountToBorrowerBasedOnParentRequestId(@Param("appId") Integer appId);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where lender_user_id=:userId and (loan_status='Agreed' OR loan_status='Active' OR loan_status='Closed' OR loan_status='Hold')", nativeQuery = true)
	public Double acceptedAmountToLender(@Param("userId") Integer userId);

	@Query(value = "select sum(disbursment_amount) from public.oxy_loan where borrower_parent_requestid=:appId and \r\n"
			+ "loan_status='Active' and admin_comments='DISBURSED'", nativeQuery = true)
	public Double findingSumOfAmountDisbursedBasedOnParentId(@Param("appId") Integer appId);

	@Query(value = "select * from public.oxy_loan where application_id=:appId", nativeQuery = true)
	public List<OxyLoan> findByLoanRequestIdAndLoanRespondIdAndApplicationId(@Param("appId") String appId);

	@Query(value = "select count(*) from public.oxy_loan where application_id=:appId", nativeQuery = true)
	public Integer findingNoOfLoansForApp(@Param("appId") String appId);

	@Query(value = "select count(*) from public.oxy_loan where application_id=:appId and loan_status='Active'", nativeQuery = true)
	public Integer findingNoOfLoansActiveForApp(@Param("appId") String appId);

	@Query(value = "select * from public.oxy_loan where borrower_parent_requestid=:id limit 1", nativeQuery = true)
	public OxyLoan findingDisbursmentInfo(@Param("id") Integer id);

	@Query(value = "select * from public.oxy_loan where application_id=:loanRequestId", nativeQuery = true)
	public List<OxyLoan> findingListOfLoansByAppId(String loanRequestId);

	@Query(value = "SELECT loan_id FROM public.oxy_loan where (is_ecs_activated='t' OR is_ecs_activated='true') and borrower_parent_requestid=:parentId and loan_status='Active' and borrower_disbursed_date is null", nativeQuery = true)
	public List<String> disbursementpending(@Param("parentId") Integer parentId);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where borrower_parent_requestid=:parentId and loan_status='Active'", nativeQuery = true)
	public Double esignStatus(@Param("parentId") Integer parentId);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where (is_ecs_activated='t' OR is_ecs_activated='true') and borrower_parent_requestid=:parentId and loan_status='Active' ", nativeQuery = true)
	public Double enachStatus(@Param("parentId") Integer parentId);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where borrower_parent_requestid=:userId and loan_status='Hold'", nativeQuery = true)
	public Double sumOfHoldAmountToBorrower(@Param("userId") Integer userId);

	@Query(value = "select sum(disbursment_amount) from public.oxy_loan where application_id=:appId and \r\n"
			+ "loan_status='Active' and admin_comments='DISBURSED'", nativeQuery = true)
	public Double findingSumOfAmountDisbursedBasedOnAppId(@Param("appId") String appId);

	@Query(value = "select sum(disbursment_amount) from public.oxy_loan where borrower_user_id=:userId and \r\n"
			+ "admin_comments='DISBURSED' and borrower_parent_requestid=:id", nativeQuery = true)
	public Double findingSumOfDisbursmentAmount(@Param("userId") Integer userId, @Param("id") Integer id);

	@Query(value = "select count(*) from public.oxy_loan where borrower_parent_requestid=:id", nativeQuery = true)
	public Integer findingCountOfLoansBasedOnParentRequestId(@Param("id") Integer id);

	@Query(value = "select count(*) from public.oxy_loan where borrower_parent_requestid=:id and borrower_disbursed_date \r\n"
			+ "    is not null", nativeQuery = true)
	public Integer findingNoOfLoansDisbursed(@Param("id") Integer id);

	@Query(value = "SELECT sum(disbursment_amount) FROM public.oxy_loan where deal_id=:dealId and loan_status='Active' ", nativeQuery = true)
	public Double agreementsGeneratedToDealAndActiveStatus(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.oxy_loan where loan_status='Active' and borrower_disbursed_date is null and borrower_parent_requestid=:borrowerParentRequestId", nativeQuery = true)
	public List<OxyLoan> disbursingStudentLoans(@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	public List<OxyLoan> findByDealId(int dealId);

}
