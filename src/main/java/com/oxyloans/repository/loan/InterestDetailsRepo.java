package com.oxyloans.repository.loan;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.loan.InterestDetails;

public interface InterestDetailsRepo
		extends PagingAndSortingRepository<InterestDetails, Integer>, JpaSpecificationExecutor<InterestDetails> {

	String getlatestrecord = "\r\n"
			+ "  SELECT * FROM public.interest_details where loan_id=:loanId and emi_number=:emiNumber";

	@Query(value = getlatestrecord, nativeQuery = true)
	InterestDetails getInterestAmount(@Param("loanId") int loanId, @Param("emiNumber") int emiNumber);

	String getInterestPaidToLoan = " SELECT sum(interest) FROM public.interest_details where loan_id=:loanId and emi_number=:emiNumber  and interest_paid is not null ;";

	@Query(value = getInterestPaidToLoan, nativeQuery = true)
	Double getInterestAmountPaid(@Param("loanId") int loanId, @Param("emiNumber") int emiNumber);

	InterestDetails findByloanIdAndEmiNumber(Integer loanId, Integer emiNumber);

	public List<InterestDetails> findByloanId(int loanId);

	@Query(value = "update public.interest_details set difference_in_days=0,interest=0.0,interest_paid=:dealClosedDate where loan_id=:loanId RETURNING 1", nativeQuery = true)
	public List<Integer> closingInterestDetails(@Param("loanId") Integer loanId,
			@Param("dealClosedDate") Timestamp dealClosedDate);
}