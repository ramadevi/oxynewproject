package com.oxyloans.repository.loan;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.loan.OxyLoan;

public interface LoansByApplicationNativeRepo extends JpaRepository<OxyLoan, Serializable> {

	String sql = "SELECT * FROM ReportApplicationData(:firstName,:lastName,:userId,:pageSize,:pageNumber)";

	@Query(value = sql, nativeQuery = true)
	List<Object[]> loansByApplication(@Param("firstName") String firstName, @Param("lastName") String lastName,
			@Param("userId") Integer userId, @Param("pageSize") Integer pageSize,
			@Param("pageNumber") Integer pageNumber);

	String countRecords = "SELECT * FROM COUNT_ReportApplicationData(:firstName,:lastName,:userId)";

	@Query(value = countRecords, nativeQuery = true)
	Integer countTranRecords(@Param("firstName") String firstName, @Param("lastName") String lastName,
			@Param("userId") Integer userId);

	String emipenaltyDetails = "SELECT DISTINCT G.loan_id, MAX(emi_amount),SUM(penality)\r\n" + "FROM Oxy_Loan D "
			+ "JOIN LOAN_EMI_CARD G " + "ON G.loan_id=D.id "
			+ "Where D.borrower_parent_requestid=:borrowerParentRequestId AND D.loan_status = 'Active'"
			+ "AND D.ADMIN_COMMENTS='DISBURSED' " + "GROUP BY G.loan_id";

	@Query(value = emipenaltyDetails, nativeQuery = true)
	List<Object[]> emiAndPenaltyDetails(@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	@Query(value = "select Sum(emi_amount) from loan_emi_card L join oxy_loan o on(L.loan_id=o.id) where o.loan_status='Active' and o.borrower_parent_requestid=:borrowerParentRequestId and L.emi_number=1 and L.emi_paid_on is null", nativeQuery = true)
	public Double getFirstUnpaidEmis(@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	@Query(value = "select Sum(emi_amount) from loan_emi_card L join oxy_loan o on(L.loan_id=o.id)  where  o.loan_status='Active' and o.borrower_parent_requestid=:borrowerParentRequestId and L.emi_number=2", nativeQuery = true)
	public Double getEmSecondMonth(@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	@Query(value = "select Sum(emi_amount) from loan_emi_card L join oxy_loan o on(L.loan_id=o.id) where o.loan_status='Active' and o.borrower_parent_requestid=:borrowerParentRequestId and L.emi_number=1", nativeQuery = true)
	public Double getFirstEmiAmountApplicationLevel(@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	@Query(value = "select sum(L.remaining_emi_amount) as total_amount from loan_emi_card L join oxy_loan o on(L.loan_id=o.id)  where (status='INPROCESS' or status='COMPLETED')and o.loan_status='Active' and o.borrower_parent_requestid=:borrowerParentRequestId", nativeQuery = true)
	public Double getAmountPaidByApplication(@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	@Query(value = "select sum(L.emi_amount) as total_amount,count(L.emi_amount) as total_count from loan_emi_card L join oxy_loan o on(L.loan_id=o.id)  where status='INITIATED'and o.loan_status='Active' and o.borrower_parent_requestid=:borrowerParentRequestId", nativeQuery = true)
	public List<Object[]> getAmountNotPaid(@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	@Query(value = "select sum(L.remaining_emi_amount) as current_count,Sum(emi_principal_amount) as total_principul_amount from loan_emi_card L join oxy_loan o on(L.loan_id=o.id)  where (status='INPROCESS' or status='COMPLETED')and o.loan_status='Active' and o.borrower_parent_requestid=:borrowerParentRequestId and emi_due_on<=:time", nativeQuery = true)
	public List<Object[]> getAmountPaidByApplicationCurrentMonth(@Param("time") Date time,
			@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	@Query(value = "select sum(L.emi_amount) as current_not_paid,count(L.emi_amount)as current_no_paid,Sum(L.penality) as total_penality from loan_emi_card L join oxy_loan o on(L.loan_id=o.id)  where status='INITIATED'and o.loan_status='Active' and o.borrower_parent_requestid=:borrowerParentRequestId and emi_due_on<=:time", nativeQuery = true)
	public List<Object[]> getAmountNotPaidCurrentMonth(@Param("time") Date time,
			@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	@Query(value = "select max(R.Duration),L.emi_due_on  from oxy_loan_request R join oxy_loan o on(R.loan_id=o.loan_id) join loan_emi_card L on (L.loan_id=o.id) and o.loan_status='Active' and o.borrower_parent_requestid=:borrowerParentRequestId and  L.emi_number=1 group by L.emi_due_on", nativeQuery = true)
	public List<Object[]> getMaxDurationAndstartDate(@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	@Query(value = "select sum(L.emi_amount) as feature_amount,count(L.emi_amount)feature_count from loan_emi_card L join oxy_loan o on(L.loan_id=o.id)  where status='INITIATED'and o.loan_status='Active' and o.borrower_parent_requestid=:borrowerParentRequestId and emi_due_on>=:time", nativeQuery = true)
	public List<Object[]> getAmountNotPaidfeature(@Param("time") Date time,
			@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	String sql3 = "SELECT C.loan_request_id,SUM(D.disbursment_Amount)DisbursedAmount,C.user_id, MAX(D.borrower_disbursed_date),C.id from oxy_loan_request C JOIN Oxy_Loan D ON C.id = D.borrower_parent_requestid JOIN PUBLIC.USER E ON E.id = C.user_id JOIN user_personal_details F ON F.user_id=E.id WHERE C.user_primary_type='BORROWER' AND D.loan_status='Active' AND (C.parent_request_id is null OR C.parent_request_id=0) AND D.ADMIN_COMMENTS='DISBURSED' AND D.borrower_parent_requestid not in ('296473',\r\n"
			+ "'296636',\r\n" + "'297863',\r\n" + "'304146',\r\n" + "'306701',\r\n" + "'307927',\r\n"
			+ "'320185',\r\n" + "'328229',\r\n" + "'329287',\r\n" + "'329593',\r\n" + "'331415',\r\n" + "'331424',\r\n"
			+ "'336178',\r\n" + "'336323',\r\n" + "'337262',\r\n" + "'338028',\r\n" + "'339334',\r\n" + "'339557',\r\n"
			+ "'339644',\r\n" + "'340889',\r\n" + "'341524',\r\n" + "'341525',\r\n" + "'294772',\r\n" + "'295758',\r\n"
			+ "'298785',\r\n" + "'304268',\r\n" + "'323321',\r\n" + "'325055',\r\n" + "'328010',\r\n"
			+ "'330281') Group BY C.loan_request_id,C.user_id,C.id ORDER BY C.User_id desc";

	@Query(value = sql3, nativeQuery = true)
	List<Object[]> penalityForApplicationLevel();

}
