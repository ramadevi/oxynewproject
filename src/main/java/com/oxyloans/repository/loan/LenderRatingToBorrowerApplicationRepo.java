package com.oxyloans.repository.loan;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.loan.LenderRatingToBorrowerApplication;

public interface LenderRatingToBorrowerApplicationRepo
		extends PagingAndSortingRepository<LenderRatingToBorrowerApplication, Integer>,
		JpaSpecificationExecutor<LenderRatingToBorrowerApplication> {

	public LenderRatingToBorrowerApplication findByLenderIdAndParentRequestId(int lenderId, Integer parentRequestId);

	@Query(value = "SELECT count(id) FROM public.lender_rating_to_borrower_application where parent_request_id=:parentRequestId", nativeQuery = true)
	public Integer getCountValueOfLenders(@Param("parentRequestId") int parentRequestId);

	@Query(value = "SELECT sum(rating) FROM public.lender_rating_to_borrower_application where parent_request_id=:parentRequestId", nativeQuery = true)
	public Integer getSumValueOfRating(@Param("parentRequestId") int parentRequestId);

}
