package com.oxyloans.repository.loan;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.loan.PaymentUploadHistory;

public interface PaymentUploadHistoryRepo extends PagingAndSortingRepository<PaymentUploadHistory, Integer> {

	public PaymentUploadHistory findByDocumentUploadedId(int id);

	@Query(value = "SELECT * FROM public.payment_upload_history where document_uploaded_id=:documentId and (status='UPLOADED' OR status='NOTYETREFLECTED')", nativeQuery = true)
	public PaymentUploadHistory findByPaymentForApproving(@Param("documentId") int documentId);

	@Query(value = "SELECT * FROM public.payment_upload_history where document_uploaded_id=:documentId and (status='UPLOADED')", nativeQuery = true)
	public PaymentUploadHistory findByPaymentForNotAtReflected(@Param("documentId") int documentId);

	@Query(value = "select borrower_unique_number,borrower_name,amount,paid_date,payment_type,user_id,updated_name,id,status,document_uploaded_id,comments from public.payment_upload_history  where status=:status order by paid_date limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getUploadedList(@Param("status") String status, @Param("pageNo") int pageNo,
			@Param("pageSize") int pageSize);

	@Query(value = "select count(a.id) from public.payment_upload_history a where a.status=:status", nativeQuery = true)
	public Integer getCountOfPaymentUpload(@Param("status") String status);

	@Query(value = "select borrower_unique_number,borrower_name,amount,updated_name,paid_date,user_id,payment_type,status from public.payment_upload_history where current_date-paid_date<=:days and status=:paymentStatus ORDER BY paid_date desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getUploadedStatistics(@Param("days") int days, @Param("pageSize") int pageSize,
			@Param("pageNo") int pageNo, @Param("paymentStatus") String paymentStatus);

	@Query(value = "select count(id) from public.payment_upload_history where current_date-paid_date<=:days and status=:paymentStatus", nativeQuery = true)
	public Integer getUploadedStatisticsCount(@Param("days") int days, @Param("paymentStatus") String paymentStatus);

	@Query(value = "select user_id,borrower_unique_number,borrower_name,amount,updated_name,paid_date from public.payment_upload_history\r\n"
			+ "where status=:paymentStatus and updated_name=:updatedName", nativeQuery = true)
	public List<Object[]> getInfoByOxyMembers(@Param("updatedName") String updatedName,
			@Param("paymentStatus") String paymentStatus);

	@Query(value = "select * from public.payment_upload_history\r\n"
			+ "where status=:paymentStatus and borrower_unique_number=:borrowerUniqueNumber", nativeQuery = true)
	public List<PaymentUploadHistory> listOfPaymentsByBorrower(
			@Param("borrowerUniqueNumber") String borrowerUniqueNumber, @Param("paymentStatus") String paymentStatus);

	@Query(value = "select * from public.payment_upload_history where user_id=:borrowerId and amount=:amount and TO_CHAR(paid_date,'dd/MM/yyyy')=:paidDate", nativeQuery = true)
	public Object findByAmountAndPaidDateAndUserId(@Param("borrowerId") Integer borrowerId,
			@Param("amount") Double amount, @Param("paidDate") String paidDate);

	@Query(value = "SELECT count(id) FROM public.payment_upload_history where status='UPLOADED'", nativeQuery = true)
	public Integer getPaymentHaveToApproveTillDateCount();

	@Query(value = "SELECT count(id) FROM public.payment_upload_history where status='APPROVED'", nativeQuery = true)
	public Integer getPaymentHadApprovedCount();

	@Query(value = "SELECT count(id) FROM public.payment_upload_history where paid_date=CURRENT_DATE and (status='UPLOADED' OR status='NOTYETREFLECTED' OR  status='APPROVED')", nativeQuery = true)
	public Integer getPaymentUploadedToday();

	@Query(value = "select sum(amount) from public.payment_upload_history where status='APPROVED' and to_char(paid_date, 'mm-yyyy')=:givenDate ", nativeQuery = true)
	public Double getAmountThroughPaymentScreesnShot(@Param("givenDate") String givenDate);

	@Query(value = "select sum(amount) from public.payment_upload_history where status='APPROVED' and payment_type='FULLPAYMENT' and to_char(paid_date, 'mm-yyyy')=:givenDate ", nativeQuery = true)
	public Double getFullPaymentThroughPaymentScreesnShot(@Param("givenDate") String givenDate);

	@Query(value = "select sum(amount) from public.payment_upload_history where status='APPROVED' and payment_type='PARTPAYMENT' and to_char(paid_date, 'mm-yyyy')=:givenDate ", nativeQuery = true)
	public Double getPartPaymentThroughPaymentScreesnShot(@Param("givenDate") String givenDate);

	@Query(value = "select sum(h.amount),h.user_id from payment_upload_history h where h.status='APPROVED' and to_char(h.paid_date, 'mm-yyyy')=:givenDate \r\n"
			+ "and h.user_id=:ownerId and h.borrower_name=:ownerName group by h.user_id", nativeQuery = true)
	public List<Object[]> getLoanOwnerAmount(@Param("givenDate") String givenDate, @Param("ownerId") int ownerId,
			@Param("ownerName") String ownerName);

	@Query(value = "select * from public.payment_upload_history where \r\n"
			+ "status='APPROVED' and to_char(paid_date, 'mm-yyyy')=:givenDate and user_id=:userId and borrower_name=:ownerName ", nativeQuery = true)
	public List<PaymentUploadHistory> getAmountsWithDate(@Param("givenDate") String givenDate,
			@Param("userId") int userId, @Param("ownerName") String ownerName);

	@Query(value = "select * from public.payment_upload_history where whats_app_message_no=:messageNumber", nativeQuery = true)
	public PaymentUploadHistory findByMessageNumber(@Param("messageNumber") String messageNumber);

	@Query(value = "SELECT count(id) FROM public.payment_upload_history where status='NOTYETREFLECTED'", nativeQuery = true)
	public Integer getPaymentNotAtReflectedCount();
}
