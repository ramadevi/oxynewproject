package com.oxyloans.repository.lenderoxywallet;

import com.oxyloans.entity.lenders.oxywallet.LenderICICITransactionRequest;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LenderICICITransactionRequestRepo
		extends PagingAndSortingRepository<LenderICICITransactionRequest, LenderICICITransactionRequest>,
		JpaSpecificationExecutor<LenderICICITransactionRequest> {

}
