package com.oxyloans.repository.lenderoxywallet;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.oxyloans.entity.lenders.oxywallet.LenderOxyWallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface LenderOxyWalletNativeRepo extends JpaRepository<LenderOxyWallet, Serializable> {

	String sql = "SELECT * FROM GetSrowWalletData(:userId,:pageSize,:pageNumber)";

	@Query(value = sql, nativeQuery = true)
	List<Object[]> getLenderWalletDetails(@Param("userId") Integer userId, @Param("pageSize") Integer pageSize,
			@Param("pageNumber") Integer pageNumber);

	String searchWallet = "SELECT * FROM SearchESrowWalletData(:firstName,:lastName,:pageSize,:pageNumber)";

	@Query(value = searchWallet, nativeQuery = true)
	List<Object[]> searchLenderWalletDetails(@Param("firstName") String firstName, @Param("lastName") String lastName,
			@Param("pageSize") Integer pageSize, @Param("pageNumber") Integer pageNumber);

	String countRecords = "SELECT * FROM Count_SearchESrowWalletData(:firstName,:lastName)";

	@Query(value = countRecords, nativeQuery = true)
	Integer countTranRecords(@Param("firstName") String firstName, @Param("lastName") String lastName);

	String cicReport = "SELECT * FROM reportoxydata()";

	@Query(value = cicReport, nativeQuery = true)
	List<Object[]> generateCICReport();

	String lenderWalletAmountQuery = "select SUM(A.transaction_amount) as walletamount from public.lender_scrow_wallet A \r\n"
			+ "where A.user_id=:userId \r\n" + "and A.status='APPROVED' AND transactiontype='credit'";

	@Query(value = lenderWalletAmountQuery, nativeQuery = true)
	Double lenderWalletCreditAmount(@Param("userId") Integer userId);

	String lenderWalletDebitAmountQuery = "select SUM(A.transaction_amount) as walletamount from public.lender_scrow_wallet A \r\n"
			+ "where A.user_id=:userId \r\n" + "and A.status='APPROVED' AND transactiontype='debit'";

	@Query(value = lenderWalletDebitAmountQuery, nativeQuery = true)
	Double lenderWalletDebitAmount(@Param("userId") Integer userId);

	String lenderWalletInprocessAmount = "select sum(B.disbursment_amount) as walletamount from "
			+ " public.oxy_loan B\r\n" + "where B.lender_user_id=:userId \r\n"
			+ "AND B.loan_status in('Active','Agreed','Closed')"
			+ "AND date_trunc('day', B.loan_accepted_date) >= '2020-02-14'";

	@Query(value = lenderWalletInprocessAmount, nativeQuery = true)
	Double lenderWalletInprocessAmount(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(B.disbursment_amount) FROM  public.oxy_loan B where B.loan_status in('Agreed') AND B.borrower_user_id=:userId AND borrower_parent_requestid=:borrowerParentRequestId", nativeQuery = true)
	public Double findsumOfInProecss(@Param("userId") Integer userId,
			@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	@Query(value = "SELECT sum(B.disbursment_amount) FROM  public.oxy_loan B where B.loan_status in('Closed') AND B.borrower_user_id=:userId AND borrower_parent_requestid=:borrowerParentRequestId", nativeQuery = true)
	public Double findsumOfClosedAmount(@Param("userId") Integer userId,
			@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	@Query(value = "SELECT sum(B.disbursment_amount) FROM  public.oxy_loan B where B.loan_status in('Active') AND B.borrower_user_id=:userId AND borrower_parent_requestid=:borrowerParentRequestId", nativeQuery = true)
	public Double findsumOfDisbursedAmount(@Param("userId") Integer userId,
			@Param("borrowerParentRequestId") Integer borrowerParentRequestId);

	String lenderTransactionPendingInfo = "SELECT count(*) FROM   public.user a join public.lender_scrow_wallet b on a.id=b.user_id where b.status='UPLOADED' AND b.created_date>= '2020-02-14'";

	@Query(value = lenderTransactionPendingInfo, nativeQuery = true)
	Integer getLenderTransactionPendingInfo();

	@Query(value = "SELECT id,scrow_account_number,transaction_amount,date(transaction_date),document_uploaded_id FROM public.lender_scrow_wallet where user_id=:userId and status='APPROVED';", nativeQuery = true)
	List<Object[]> getLenderWalletInfo(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(transaction_amount) FROM public.lender_scrow_wallet where user_id=:userId and status='APPROVED';", nativeQuery = true)
	public Double getWalletAmount(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(transaction_amount) FROM public.lender_scrow_wallet where user_id=:userId and status='APPROVED' and date(transaction_date)='2021-05-01'", nativeQuery = true)
	public Double getWalletAmountOnApril(@Param("userId") Integer userId);

	@Query(value = "SELECT transaction_amount, date(transaction_date) FROM public.lender_scrow_wallet where user_id=:userId and status='APPROVED' and transactiontype='credit'", nativeQuery = true)
	public List<Object[]> getLenderCreditedAmountDetails(@Param("userId") Integer userId);

	@Query(value = "select c.user_id,e.first_name,e.last_name,c.scrow_account_number,c.transaction_amount,c.transaction_date,\r\n"
			+ "d.file_name,c.status,c.comments,c.created_by,c.created_date,d.document_type,d.document_sub_type,d.id,\r\n"
			+ "d.file_path from public.user a join public.user_bank_details b on a.id=b.user_id join public.lender_scrow_wallet c on\r\n"
			+ "b.user_id=c.user_id join public.user_document_status d on c.document_uploaded_id=d.id join\r\n"
			+ "public.user_personal_details e on d.user_id=e.user_id where a.id=:id and\r\n"
			+ "d.document_sub_type='TRANSACTIONSS' limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> findScrowWalletTransactions(int id, int pageNo, int pageSize);

	@Query(value = "select id from public.lender_scrow_wallet where document_uploaded_id=:id1", nativeQuery = true)
	public Integer gettingIdFromLenderScrowWallet(@Param("id1") Integer id1);

	@Query(value = "SELECT DISTINCT user_id from public.lender_scrow_wallet where transactiontype='credit' and status='APPROVED'", nativeQuery = true)
	public List<Integer> getListOfLendersIds();

	@Query(value = "select count(id) from public.lender_scrow_wallet a where a.user_id=:userId and a.status='APPROVED' and a.transactiontype='credit'", nativeQuery = true)
	public Integer totalLenderWalletCreditedCount(@Param("userId") Integer userId);

	@Query(value = "select a.transaction_amount,date(a.transaction_date),a.remarks,a.comments,a.wallet_transfer_id from public.lender_scrow_wallet a where a.user_id=:userId and a.status='APPROVED' and a.transactiontype='credit' and a.deal_id=0 order by a.transaction_date asc,a.id asc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getListOfLendersWalletCreditedBasedOnAscOrder(@Param("userId") Integer userId,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "select a.transaction_amount,date(a.transaction_date),a.remarks,a.comments,a.wallet_transfer_id from public.lender_scrow_wallet a where a.user_id=:userId and a.status='APPROVED' and a.transactiontype='credit' and a.deal_id=0 order by a.transaction_date desc,a.id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getListOfLendersWalletCreditedBasedOnDescOrder(@Param("userId") Integer userId,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "select count(id) from public.lender_scrow_wallet a where a.user_id=:userId and a.status='APPROVED' and a.transactiontype='debit'", nativeQuery = true)
	public Integer totalLenderWalletDebitedCount(@Param("userId") Integer userId);

	@Query(value = "select a.transaction_amount,date(a.transaction_date),a.remarks from public.lender_scrow_wallet a where a.user_id=:userId and a.status='APPROVED' and a.transactiontype='debit' order by a.transaction_date asc,a.id asc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getListOfLendersWalletDebitedBasedOnAscOrder(@Param("userId") Integer userId,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "select a.transaction_amount,date(a.transaction_date),a.remarks from public.lender_scrow_wallet a where a.user_id=:userId and a.status='APPROVED' and a.transactiontype='debit' order by a.transaction_date desc,a.id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getListOfLendersWalletDebitedBasedOnDescOrder(@Param("userId") Integer userId,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "select a.transaction_amount,date(a.transaction_date),a.remarks from public.lender_scrow_wallet a where a.user_id=:userId and a.status='APPROVED' and a.transactiontype='credit' order by a.transaction_date asc", nativeQuery = true)
	public List<Object[]> getListOfLendersWalletCredited(@Param("userId") Integer userId);

	@Query(value = "select a.transaction_amount,date(a.transaction_date),a.remarks from public.lender_scrow_wallet a where a.user_id=:userId and a.status='APPROVED' and a.transactiontype='debit' order by a.transaction_date asc", nativeQuery = true)
	public List<Object[]> getListOfLendersWalletDebited(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(transaction_amount) FROM public.lender_scrow_wallet where user_id=:userId and status='APPROVED' and transactiontype='credit' and deal_id=:dealId", nativeQuery = true)
	public Double paticipatedAmountMovedToWalletSum(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lender_scrow_wallet where user_id=:userId and status='APPROVED' and transactiontype='credit' and deal_id=:dealId", nativeQuery = true)
	public List<LenderOxyWallet> paticipatedAmountMovedToWalletInfo(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "select * FROM  public.lender_scrow_wallet where deal_id=:dealId and user_id=:userId and principal_to_wallet='PRINCIPALRETURNED' OR principal_to_wallet='INTERESTRETURNED' order by transaction_date desc limit 1", nativeQuery = true)
	public LenderOxyWallet getLatestPrincipalReturn(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "select * from public.lender_scrow_wallet where deal_id=:dealId and principal_to_wallet='PRINCIPALRETURNED' order by id desc", nativeQuery = true)
	public List<LenderOxyWallet> interestToWalletLoadedFromParticipation(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lender_scrow_wallet where user_id=:userId and deal_id=:dealId and transaction_amount=:amount and principal_to_wallet='PRINCIPALRETURNED'", nativeQuery = true)
	public LenderOxyWallet checkPaticiationLoadedToWallet(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId, @Param("amount") Integer amount);

	@Query(value = "select sum(transaction_amount) from public.lender_scrow_wallet where user_id=:userId and deal_id=:dealId\r\n"
			+ "and transactiontype='credit'", nativeQuery = true)
	public Double findingSumOfAmountMovedFromDealToWallet(@Param("dealId") Integer dealId,
			@Param("userId") Integer userId);

	@Query(value = "select sum(transaction_amount) from public.lender_scrow_wallet where  deal_id=:dealId\r\n"
			+ "and status ='APPROVED' and transactiontype='credit'", nativeQuery = true)
	public Double findingSumOfAmountMovedFromDealToWalletForDeal(@Param("dealId") int dealId);

	@Query(value = "SELECT user_id,transaction_amount,date(transaction_date),icici_decrypted_data FROM public.lender_scrow_wallet where remarks='QR' order by id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> transactionsFromQr(@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "SELECT count(id) FROM public.lender_scrow_wallet where remarks='QR'", nativeQuery = true)
	public Integer transactionsFromQrCount();

	@Query(value = "SELECT sum(transaction_amount) FROM public.lender_scrow_wallet where user_id=:userId and deal_id>0", nativeQuery = true)
	public Double sumOfAmountLoadedToWalletFromParticipation(@Param("userId") int userId);

	@Query(value = "SELECT sum(transaction_amount) FROM public.lender_scrow_wallet where deal_id=:dealId and transactiontype='credit' and status='APPROVED'", nativeQuery = true)
	public Double sumOfAmountReturnedFromDealToWallet(@Param("dealId") int dealId);

	@Query(value = "SELECT a.transaction_amount,date(a.transaction_date),a.remarks,a.deal_id,c.lender_returns_type,c.rateofinterest,c.received_on,b.borrower_closed_date,b.funds_acceptance_start_date FROM public.lender_scrow_wallet a join public.oxy_borrowers_deals_information b on a.deal_id=b.id join public.oxy_lenders_accepted_deals c  on (b.id=c.deal_id and a.user_id=c.user_id) where a.user_id=:userId and a.deal_id>0 and b.borrower_closing_status='CLOSED' and transactiontype='credit' and a.status='APPROVED'", nativeQuery = true)
	public List<Object[]> amountLoadedFromDeals(@Param("userId") Integer userId);

	@Query(value = "select * from public.lender_scrow_wallet where user_id=:userId and deal_id=:dealId order by transaction_date desc limit 1", nativeQuery = true)
	public LenderOxyWallet principalReturnToWallet(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT  DISTINCT F.loan_request_id ParentApplicationID,A.user_id,\r\n"
			+ " concat(A.first_name,' ',A.last_name)username,to_char(A.dob,'DDMMYYYY')DOB,\r\n"
			+ "CASE WHEN A.gender='M' OR A.gender='Male' THEN 1 WHEN A.gender='F' OR A.gender='Female' THEN 2  END Gender,A.pan_number,E.mobile_number TelephoneNoMobile,\r\n"
			+ "C.office_contact_number TelephoneNoOffice,E.email EmailID1,\r\n"
			+ "(SELECT CONCAT(D.house_number,'',D.street,'',D.area,'',D.city) FROM user_address_details D WHERE A.User_id=D.user_id LIMIT 1 )PermanantAddress,\r\n"
			+ "E.State StateCode,E.pincode,\r\n"
			+ "(SELECT Z.standard_location_id FROM user_address_details Z WHERE  A.User_id=Z.user_id LIMIT 1)AddressCategory1,\r\n"
			+ "\r\n" + "F.loan_request_id NewAccountNo,\r\n"
			+ "MAX(to_char(H.borrower_disbursed_date,'DDMMYYYY')) DateDisbursed,\r\n"
			+ "MAX(to_char((SELECT max(X.emi_paid_on)from loan_emi_card X where H.id=X.loan_id and X.emi_paid_on IS NOT NULL),'DDMMYYYY'))DateofLastPayment,\r\n"
			+ "to_char(NOW(),'DDMMYYYY') DateReported,\r\n"
			+ "cast(SUM(H.disbursment_amount) as bigint) Sanctioned,\r\n"
			+ "SUM((SELECT H.disbursment_amount-SUM(Y.emi_principal_amount)from  loan_emi_card Y where f.id=Y.loan_id and y.emi_paid_on IS NOT NULL))Balance,      \r\n"
			+ "MAX(H.rate_of_interest) RateofInterest,MAX(H.duration) RepaymentTenure,\r\n"
			+ "MAX((SELECT CASE WHEN min(emi_due_on) < NOW()THEN DATE_PART('day',  NOW() - min(emi_due_on)) ELSE 0 END\r\n"
			+ " from loan_emi_card Z where H.id=Z.loan_id and Z.emi_paid_on IS NULL ))PastDueDate\r\n"
			+ "from public.user_personal_details A\r\n" + "JOIN user_bank_details B  ON A.user_id=B.user_id  \r\n"
			+ "JOIN user_professional_details C ON A.user_id=C.user_id  \r\n"
			+ "JOIN public.user E ON A.user_id =E.id  \r\n" + "JOIN oxy_loan_request F ON F.User_id=E.ID\r\n" + "\r\n"
			+ "JOIN Oxy_Loan H ON H.borrower_parent_requestid = F.ID\r\n" + "\r\n" + "WHERE\r\n" + "\r\n"
			+ "H.loan_status='Active'\r\n" + "AND H.borrower_disbursed_date IS NOT NULL\r\n"
			+ "AND (F.parent_request_id is null OR F.parent_request_id=0) AND \r\n"
			+ "H.borrower_disbursed_date>=:startdate and\r\n" + "H.borrower_disbursed_date<= :enddate\r\n" + "\r\n"
			+ "GROUP BY F.loan_request_id ,A.user_id,concat(A.first_name,' ',A.last_name),to_char(A.dob,'DDMMYYYY'),\r\n"
			+ "A.Gender,A.pan_number,E.mobile_number,C.office_contact_number ,E.email ,\r\n" + "\r\n"
			+ "E.State ,E.pincode,\r\n" + "F.loan_request_id\r\n" + " ORDER BY F.loan_request_id", nativeQuery = true)
	public List<Object[]> generateCICReportBasedOnDates(@Param("startdate") Timestamp startDate1,
			@Param("enddate") Timestamp endDate1);

	@Query(value = "SELECT count(*) FROM public.lender_scrow_wallet where deal_id=:dealId and transactiontype='credit' and status='APPROVED'", nativeQuery = true)
	public Integer countOfAmountReturnedFromDealToWallet(@Param("dealId") int dealId);

	@Query(value = "SELECT * FROM public.lender_scrow_wallet where deal_id=:dealId and transactiontype='credit' and status='UPLOADED' and transaction_amount=:amount and user_id=:userId", nativeQuery = true)
	public LenderOxyWallet findbyUserIdDealIdAndStatus(@Param("userId") Integer userId, @Param("dealId") int dealId,
			@Param("amount") Integer amount);

	@Query(value = "(select user_id,transaction_amount,date(transaction_date) as date,transactiontype as comments,scrow_account_number as type,wallet_transfer_id,deal_id from public.lender_scrow_wallet where user_id=:userId and transactiontype='credit' and status='APPROVED')union all\r\n"
			+ "(select user_id,transaction_amount,date(transaction_date) as date,transactiontype as comments,scrow_account_number as type,wallet_transfer_id,deal_id from public.lender_scrow_wallet where user_id=:userId and transactiontype='debit' and status='APPROVED')union all\r\n"
			+ "(select a.lender_user_id,a.disbursment_amount,a.loan_accepted_date as date,a.loan_status as comments,(b.first_name) as type,a.borrower_parent_requestid,a.lender_parent_requestid from public.oxy_loan a join public.user_personal_details b on a.borrower_user_id=b.user_id where a.lender_user_id=:userId AND a.loan_status in('Active','Agreed','Closed') AND date_trunc('day', a.loan_accepted_date) >= '2020-02-14' and a.deal_id=0)union all\r\n"
			+ "(select a.user_id,a.participated_amount,date(a.received_on) as date ,a.participation_state as comments,b.deal_name as type,b.id,a.deal_id from public.oxy_lenders_accepted_deals a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId)union all\r\n"
			+ "(select a.user_id,a.updation_amount,a.updated_on as date,a.deal_type as comments,b.deal_name as type,b.id,a.deal_id from public.lenders_paticipation_updation a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId) order by date desc", nativeQuery = true)
	public List<Object[]> transactionHistory(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.lender_scrow_wallet where deal_id=:dealId and transactiontype='credit' and user_id=:userId", nativeQuery = true)
	public LenderOxyWallet principalReturnedToWallet(@Param("userId") Integer userId, @Param("dealId") int dealId);

	@Query(value = "select sum(transaction_amount) from public.lender_scrow_wallet where deal_id!=0\r\n"
			+ "and status='APPROVED' and user_id=:userId", nativeQuery = true)
	public Double findingTotalAmountMovedFromDealToWallet(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.lender_scrow_wallet where transactiontype='credit' and status='APPROVED' and transaction_amount=:amount and user_id=:userId and to_char(transaction_date,'dd-MM-yyyy')=:paidDate and (deal_id is null OR deal_id=0) order by user_id limit 1", nativeQuery = true)
	LenderOxyWallet findbyUserIdAndPaidDate(@Param("userId") Integer userId, @Param("paidDate") String paidDate,
			@Param("amount") int amount);

	@Query(value = "select * from public.lender_scrow_wallet where user_id=:userId and deal_id=:dealId and transactiontype='credit'", nativeQuery = true)
	public LenderOxyWallet findingWalletInfoBasedOnDeal(@Param("userId") Integer userId, @Param("dealId") int dealId);

	@Query(value = "SELECT a.transaction_amount,date(a.transaction_date),a.remarks,a.deal_id,c.lender_returns_type,c.rateofinterest,c.received_on,b.borrower_closed_date,b.funds_acceptance_start_date FROM public.lender_scrow_wallet a join public.oxy_borrowers_deals_information b on a.deal_id=b.id join public.oxy_lenders_accepted_deals c  on (b.id=c.deal_id and a.user_id=c.user_id) where a.user_id=:userId and a.deal_id>0  and transactiontype='credit' and a.status='APPROVED'", nativeQuery = true)
	public List<Object[]> amountLoadedFromDealsForUser(@Param("userId") Integer userId);

	List<LenderOxyWallet> findByDealId(int dealId);

	@Query(value = "select * from public.lender_scrow_wallet where to_char(transaction_date,'dd-MM-yyyy')=:date and transactiontype='credit' and comments=:paymentMode ", nativeQuery = true)
	List<LenderOxyWallet> getAllAmountsInGivenDate(@Param("date") String date,
			@Param("paymentMode") String paymentMode);

	@Query(value = "SELECT * FROM public.lender_scrow_wallet where wallet_transfer_id is not null and wallet_transfer_id>0 and transactiontype='debit' and user_id=:userId", nativeQuery = true)
	List<LenderOxyWallet> getWalletToWalletDebitedHistory(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(transaction_amount) FROM public.lender_scrow_wallet where user_id=:userId and status='APPROVED' and transactiontype='credit' and deal_id=:dealId and (transaction_date>=  To_date(:fromDate,'dd-MM-yyyy') and  transaction_date< To_date(:toDate,'dd-MM-yyyy'))", nativeQuery = true)
	public Double paticipatedAmountMovedToWalletSum(@Param("userId") Integer userId, @Param("dealId") Integer dealId,
			@Param("fromDate") String fromDate, @Param("toDate") String toDate);

	String lenderWalletAmountQueryInGivenDates = "select SUM(A.transaction_amount) as walletamount from public.lender_scrow_wallet A \r\n"
			+ "where (transaction_date>=  To_date(:fromDate,'dd-MM-yyyy') and  transaction_date< To_date(:toDate,'dd-MM-yyyy')) and A.user_id=:userId and A.status='APPROVED' AND transactiontype='credit' ";

	@Query(value = lenderWalletAmountQueryInGivenDates, nativeQuery = true)
	Double lenderWalletCreditAmountInGivenDates(@Param("userId") Integer userId, @Param("fromDate") String fromDate,
			@Param("toDate") String toDate);

	@Query(value = "select distinct to_char(transaction_date,'dd/MM/yyyy'),sum(transaction_amount) from public.lender_scrow_wallet where deal_id=:dealId and comments='MANUVAL' and status='APPROVED' and transactiontype='credit' group by to_char(transaction_date,'dd/MM/yyyy')", nativeQuery = true)
	public List<Object[]> manuvalPrincipalReturn(@Param("dealId") Integer dealId);
	
	@Query(value = "select sum(transaction_amount) from public.lender_scrow_wallet where deal_id=:dealId and comments='MANUVAL'", nativeQuery = true)
	public Double findingSumOfAmountMovedFromDealToWalletForManual(@Param("dealId") int dealId);

	@Query(value = "select sum(transaction_amount) from public.lender_scrow_wallet where deal_id=:dealId and user_id=:userId and principal_to_wallet='PRINCIPALRETURNED' ", nativeQuery = true)
	Double findingSumOfPrincipleAmountMovedFromDealToWallet(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "select * from public.lender_scrow_wallet where user_id=:userId and deal_id=:dealId and transactiontype='credit'", nativeQuery = true)
	public LenderOxyWallet principalAmountReturnedToLenders(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "select sum(transaction_amount) from public.lender_scrow_wallet where principal_to_wallet='PRINCIPALRETURNED' and transactiontype='credit' and status='APPROVED' and date(transaction_date)=current_date", nativeQuery = true)
	Double findingSumOfAmountMovedFromDealToWalletForManual();

	@Query(value = "select sum(transaction_amount) from public.lender_scrow_wallet where deal_id=:dealId and  principal_to_wallet='PRINCIPALRETURNED' and transactiontype='credit' and status='APPROVED' and date(transaction_date)=current_date", nativeQuery = true)

	Double findByPrincipalReturnFromWallet(@Param("dealId") Integer dealId);

	@Query(value = "select sum(transaction_amount) from public.lender_scrow_wallet where principal_to_wallet='PRINCIPALRETURNED' and transactiontype='credit' and status='APPROVED' and date(transaction_date)>= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)

	Double findingSumOfAmountMovedFromDealToWalletForManualWeeklyData();

	@Query(value = "select sum(transaction_amount) from public.lender_scrow_wallet where deal_id=:dealId and principal_to_wallet='PRINCIPALRETURNED' and comments='MANUVAL' and date(transaction_date)>= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)

	Double findByPrincipalReturnFromWalletweekly(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(transaction_amount) FROM public.lender_scrow_wallet where principal_to_wallet='PRINCIPALRETURNED'  and comments='MANUVAL'and  date(transaction_date)>=:startDate and date(transaction_date)<=:endDate", nativeQuery = true)

	Double findingSumOfAmountMovedFromDealToWalletForManualMonthlyDataWithInput(@Param("startDate") Date startDate,@Param("endDate") Date endDate);
	@Query(value = "SELECT sum(transaction_amount) FROM public.lender_scrow_wallet where deal_id=:dealId and  principal_to_wallet='PRINCIPALRETURNED'  and comments='MANUVAL' and date(transaction_date)>=:startDate and date(transaction_date)<=:endDate", nativeQuery = true)


	Double findByPrincipalReturnFromWalletMonthlyWithInput(@Param("dealId")Integer dealId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);


	@Query(value = "select sum(transaction_amount) from public.lender_scrow_wallet where deal_id=:dealId and principal_to_wallet='PRINCIPALRETURNED' and comments='MANUVAL' and date_trunc('month', transaction_date) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)
	
	Double findByPrincipalReturnFromWalletMonthly(@Param("dealId") Integer dealId);

	@Query(value = "select sum(transaction_amount) from public.lender_scrow_wallet where  principal_to_wallet='PRINCIPALRETURNED' and comments='MANUVAL' and date_trunc('month', transaction_date) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)

	Double findingSumOfAmountMovedFromDealToWalletForManualMonthlyData();


	
	
}
