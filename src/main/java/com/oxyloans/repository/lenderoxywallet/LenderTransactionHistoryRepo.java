package com.oxyloans.repository.lenderoxywallet;

import com.oxyloans.entity.lenders.oxywallet.LenderTransactionHistory;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface LenderTransactionHistoryRepo extends PagingAndSortingRepository<LenderTransactionHistory, Integer>,
		JpaSpecificationExecutor<LenderTransactionHistory> {

	@Query(value = "SELECT * FROM public.lender_transaction_history where user_id=:userId order by paid_date desc,id desc limit 1", nativeQuery = true)
	public LenderTransactionHistory lastTransactionInfo(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.lender_transaction_history where user_id=:userId and to_char(paid_date,'dd-MM-yyyy')=:paidDate and amount=:amount and deal_id=0 order by id desc limit 1", nativeQuery = true)
	public LenderTransactionHistory principalReturn(@Param("userId") Integer userId, @Param("paidDate") String paidDate,
			@Param("amount") double amount);
}
