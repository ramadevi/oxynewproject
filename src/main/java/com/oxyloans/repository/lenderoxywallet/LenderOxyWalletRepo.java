package com.oxyloans.repository.lenderoxywallet;

import java.util.Date;
import java.util.List;

import com.oxyloans.entity.lenders.oxywallet.LenderOxyWallet;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface LenderOxyWalletRepo extends PagingAndSortingRepository<LenderOxyWallet, LenderOxyWallet>,
		JpaSpecificationExecutor<LenderOxyWallet> {

	public Integer countByUserId(Integer userId);

	public LenderOxyWallet findById(Integer id);

	public List<LenderOxyWallet> findByUserId(int userId);

	public LenderOxyWallet findByWithdrawFundsId(Integer id);

	public List<LenderOxyWallet> findByUserIdOrderByTransactionDateDesc(int userId);

	public LenderOxyWallet findByuniqueTransactionReference(String utr);

	@Query(value = "select * from public.lender_scrow_wallet where user_id=:refereeId and deal_id=:dealId and status='APPROVED'\r\n"
			+ "and transactiontype='credit'", nativeQuery = true)
	public LenderOxyWallet findingLenderPrincipalMovedToWallet(@Param("refereeId") Integer refereeId,
			@Param("dealId") Integer dealId);

	@Query(value = "select * from lender_scrow_wallet where unique_transaction_reference=:utr", nativeQuery = true)
	public LenderOxyWallet findingByUniqueTransactionReference(@Param("utr") String utr);

	@Query(value = "select user_id,scrow_account_number,transaction_amount,transaction_date,created_date from public.lender_scrow_wallet where transactiontype='debit' and \r\n"
			+ "comments='Debited Fee From Wallet' order by id desc", nativeQuery = true)
	public List<Object[]> findListOfLenderFeeThroughWallet();

	@Query(value = "select user_id,scrow_account_number,transaction_amount,transaction_date from \r\n"
			+ "public.lender_scrow_wallet where transactiontype='debit' and \r\n"
			+ "comments='Debited Fee From Wallet' and user_id=:userId order by id desc", nativeQuery = true)
	public List<Object[]> lenderPaidFeeThroughWallet(@Param("userId") Integer userId);

	@Query(value = "SELECT \r\n" + "    a.id,\r\n" + "    a.user_id,\r\n" + "    a.scrow_account_number,\r\n"
			+ "    a.transaction_amount,\r\n" + "    a.transaction_date,\r\n" + "    c.first_name,\r\n"
			+ "    c.last_name,\r\n" + "    b.mobile_number\r\n" + "FROM \r\n" + "    public.lender_scrow_wallet a\r\n"
			+ "JOIN \r\n" + "    public.user b ON a.user_id = b.id\r\n" + "JOIN \r\n"
			+ "    public.user_personal_details c ON b.id = c.user_id\r\n" + "WHERE \r\n"
			+ "    a.transactiontype = 'debit'\r\n" + "    AND b.test_user = false\r\n"
			+ "    AND DATE(a.transaction_date) >=:startDate\r\n" + "    AND DATE(a.transaction_date) <=:endDate\r\n"
			+ "    AND a.comments IS NULL\r\n" + "ORDER BY \r\n" + "    a.id DESC;\r\n" + "", nativeQuery = true)
	public List<Object[]> listOfLenderPaidFeeThroughWallet(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);
	

	 @Query(value = "select SUM(transaction_amount) as walletamount from public.lender_scrow_wallet \r\n"
	 		+ "where user_id=:lendrId and status='APPROVED' AND transactiontype='credit'", nativeQuery = true)
	 public  Double findByLenderCreditAmount(@Param("lendrId") Integer lendrId);

	@Query(value = "select SUM(transaction_amount) as walletamount from public.lender_scrow_wallet \r\n"
	 		+ "where user_id=:lendrId and status='APPROVED' AND transactiontype='debit'", nativeQuery = true)
	 public  Double findByLenderdebitAmount(@Param("lendrId") Integer lendrId);
}
