package com.oxyloans.repository.lenderhold;

import java.util.List;

import com.oxyloans.entity.lenders.hold.UserHoldAmountMappedToDeal;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.lenders.hold.UserHoldAmountMappedToDeal.AmountType;
import com.oxyloans.entity.lenders.hold.UserHoldAmountMappedToDeal.Status;
import com.oxyloans.entity.user.User;

public interface UserHoldAmountMappedToDealRepo extends PagingAndSortingRepository<UserHoldAmountMappedToDeal, Integer>,
		JpaSpecificationExecutor<UserHoldAmountMappedToDeal> {

	public List<UserHoldAmountMappedToDeal> findByuserHoldAmountDetailsId(Integer userHoldAmountDetailsId);

	public UserHoldAmountMappedToDeal findByUserIdAndDealIdAndAmountTypeAndStatus(int userId, int dealId,
			AmountType amountType, Status open);

	@Query(value = "SELECT sum(hold_amount) FROM public.user_hold_amount_mapped_to_deals where user_id=:userId and deal_id=:dealId and amount_type='PRINCIPAL' and status='CLOSE'", nativeQuery = true)
	public Double principalDebited(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.user_hold_amount_mapped_to_deals where user_id=:userId and deal_id=:dealId and amount_type='PRINCIPAL' and status='CLOSE' ORDER BY created_on desc limit 1", nativeQuery = true)
	public UserHoldAmountMappedToDeal principalReturned(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);
}
