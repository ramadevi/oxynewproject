package com.oxyloans.repository.lenderhold;

import java.util.List;

import com.oxyloans.entity.lenders.hold.UserHoldAmountDetail;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserHoldAmountDetailRepo extends PagingAndSortingRepository<UserHoldAmountDetail, Integer>,
		JpaSpecificationExecutor<UserHoldAmountDetail> {

	public List<UserHoldAmountDetail> findAll();

	public List<UserHoldAmountDetail> findByUserId(int userId);
}
