package com.oxyloans.repository.lendergroup;

import java.util.List;

import com.oxyloans.entity.lenders.group.OxyLendersGroup;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OxyLendersGroupRepo
		extends PagingAndSortingRepository<OxyLendersGroup, Integer>, JpaSpecificationExecutor<OxyLendersGroup> {

	@Query(value = "SELECT lender_group_name FROM public.oxy_lenders_group where id=:id", nativeQuery = true)
	public String getLenderGroupNameByid(@Param("id") Integer id);

	@Query(value="select * from public.oxy_lenders_group",nativeQuery = true)
	public List<OxyLendersGroup> findingListOfLenderGroups();

	@Query(value="select id from public.oxy_lenders_group where lender_group_name=:groupName",nativeQuery = true)
	public Integer gettingOxyLenderGroupId(@Param("groupName")String groupName);

	@Query(value = "SELECT lender_group_name FROM public.oxy_lenders_group where id=:lenderGroupId", nativeQuery = true)
	public String getLenderGroupNameUsingId(@Param("lenderGroupId") Integer lenderGroupId);

}
