package com.oxyloans.repository.userdocumentstatus;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.user.Document.status.UserDocumentStatus;
import com.oxyloans.entity.user.Document.status.UserDocumentStatus.DocumentType;

@Repository
public interface UserDocumentStatusRepo extends PagingAndSortingRepository<UserDocumentStatus, Integer> {

	public List<UserDocumentStatus> findByUserIdAndDocumentType(Integer userId, DocumentType documentType);

	public UserDocumentStatus findByUserIdAndDocumentTypeAndDocumentSubType(Integer userId, DocumentType documentType,
			String documentSubType);

	public List<UserDocumentStatus> findByUserId(int userId);

	public UserDocumentStatus findByIdAndUserId(Integer id, Integer userId);

	public UserDocumentStatus findByUserIdAndDocumentSubType(int userId, String string);

	@Query(value = "SELECT * FROM public.user_document_status where user_id=:userId and document_sub_type=:documentSubType", nativeQuery = true)
	public UserDocumentStatus getDetailsByDocumentType(@Param("userId") int userId,
			@Param("documentSubType") String documentSubType);

	@Query(value = "select id from public.user_document_status where id=:documentId", nativeQuery = true)
	public Integer findByIdNum(@Param("documentId") Integer documentId);

	@Query(value = "select * from public.user_document_status where \r\n"
			+ "document_sub_type='USERQUERYSCREENSHOT' and user_id=:id and id=:documentId", nativeQuery = true)
	public UserDocumentStatus findByUserIdDocumentSubTypeAndId(@Param("documentId") Integer documentId,
			@Param("id") Integer id);

	@Query(value = "select * from public.user_document_status where \r\n"
			+ "document_sub_type='USERQUERYSCREENSHOT' and id=:documentId", nativeQuery = true)
	public UserDocumentStatus findByUserIdDocumentSubTypeAndIdByAdmin(@Param("documentId") Integer documentId);

	@Query(value = "select * from public.user_document_status where document_sub_type=:kycType\r\n"
			+ "AND user_id=:partnerId and document_type='PartnerAgreementType'", nativeQuery = true)
	public UserDocumentStatus findingPartnerAgreementType(@Param("kycType") String kycType,
			@Param("partnerId") Integer partnerId);

	@Query(value = "select * from public.user_document_status where user_id=:id and \r\n"
			+ "document_type='PartnerAgreementType' and document_sub_type='PARTNERNDA'", nativeQuery = true)
	public UserDocumentStatus findIsPartnerUploadedNDA(@Param("id") Integer id);

	@Query(value = "select * from public.user_document_status where user_id=:id and \r\n"
			+ "document_type='PartnerAgreementType' and document_sub_type='PARTNERMOU'", nativeQuery = true)
	public UserDocumentStatus findIsPartnerUploadedMOU(@Param("id") Integer id);
}
