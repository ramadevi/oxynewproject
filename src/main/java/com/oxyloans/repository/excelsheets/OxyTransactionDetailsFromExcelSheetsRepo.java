package com.oxyloans.repository.excelsheets;

import com.oxyloans.entity.excelsheets.OxyTransactionDetailsFromExcelSheets;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface OxyTransactionDetailsFromExcelSheetsRepo
		extends PagingAndSortingRepository<OxyTransactionDetailsFromExcelSheets, Integer>,
		JpaSpecificationExecutor<OxyTransactionDetailsFromExcelSheets> {

	@Query(value = "SELECT * FROM public.oxy_transaction_details_from_excelsheets where debited_towords=:debitedTowords and to_char(transferred_date, 'dd/mm/yyyy')=:paidDate", nativeQuery = true)
	public OxyTransactionDetailsFromExcelSheets getTransactionExcelSheetByDateAndType(
			@Param("debitedTowords") String debitedTowords, @Param("paidDate") String paidDate);

	@Query(value = "SELECT remarks FROM public.oxy_transaction_details_from_excelsheets where debited_towords=:debitedTowords and to_char(transferred_date, 'yyyy-mm-dd')=:paidDate", nativeQuery = true)
	public String getAdminRemarks(@Param("debitedTowords") String debitedTowords, @Param("paidDate") String paidDate);

	@Query(value = "SELECT * FROM public.oxy_transaction_details_from_excelsheets where remarks like :remarks and to_char(transferred_date,'dd/mm/yyyy')=:interestDate", nativeQuery = true)
	public OxyTransactionDetailsFromExcelSheets getApprovalFromAdminInUpdated(@Param("remarks") String remarks,
			@Param("interestDate") String interestDate);

	@Query(value = "SELECT * FROM public.oxy_transaction_details_from_excelsheets where remarks like :remarks and to_char(transferred_date,'dd/mm/yyyy')=:interestDate and file_name='APPROVED'", nativeQuery = true)
	public OxyTransactionDetailsFromExcelSheets getStatusApproved(@Param("remarks") String remarks,
			@Param("interestDate") String interestDate);

	@Query(value = "SELECT * FROM public.oxy_transaction_details_from_excelsheets where file_name=:fileName", nativeQuery = true)
	public OxyTransactionDetailsFromExcelSheets getTransactionExcelSheetByName(@Param("fileName") String fileName);
}
