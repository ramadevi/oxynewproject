package com.oxyloans.repository.excelsheets;

import java.util.List;

import com.oxyloans.entity.excelsheets.IciciExcelsheetApprovalsInformation;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface IciciExcelsheetApprovalsInformationRepo
		extends PagingAndSortingRepository<IciciExcelsheetApprovalsInformation, Integer>,
		JpaSpecificationExecutor<IciciExcelsheetApprovalsInformation> {

	@Query(value = "SELECT * FROM public.icici_excelsheet_approvals_information where deal_id=:dealId and to_char(returns_date, 'dd/mm/yyyy')=:returnsDate", nativeQuery = true)
	public IciciExcelsheetApprovalsInformation getTransactionExcelSheetByDateAndType(@Param("dealId") Integer dealId,
			@Param("returnsDate") String returnsDate);

	@Query(value = "SELECT * FROM public.icici_excelsheet_approvals_information where deal_id=:dealId and month_name_and_year=:monthNameAndYear", nativeQuery = true)
	public IciciExcelsheetApprovalsInformation findByDealIdAndMonthNameAndYear(@Param("dealId") Integer dealId,
			@Param("monthNameAndYear") String monthNameAndYear);

	@Query(value = "SELECT * FROM public.icici_excelsheet_approvals_information where deal_id=:dealId  and to_char(actual_payment_date, 'mm-yyyy')=:actualPaymentDate", nativeQuery = true)
	public IciciExcelsheetApprovalsInformation findByDealIdAndActualPaymentDate(@Param("dealId") Integer dealId,
			@Param("actualPaymentDate") String actualPaymentDate);

	@Query(value = "SELECT * FROM public.icici_excelsheet_approvals_information where deal_id=:dealId and month_name_and_year=:monthNameAndYear and to_char(actual_payment_date, 'dd-mm-yyyy')=:actualPaymentDate", nativeQuery = true)
	public IciciExcelsheetApprovalsInformation findByDealIdAndMonthNameAndYear1(@Param("dealId") Integer dealId,
			@Param("monthNameAndYear") String monthNameAndYear, @Param("actualPaymentDate") String actualPaymentDate);

	@Query(value = "SELECT * FROM public.icici_excelsheet_approvals_information where deal_id=:dealId and month_name_and_year=:monthNameAndYear", nativeQuery = true)
	public List<IciciExcelsheetApprovalsInformation> findByDealIdAndMonthNameAndYears(@Param("dealId") Integer dealId,
			@Param("monthNameAndYear") String monthNameAndYear);
}
