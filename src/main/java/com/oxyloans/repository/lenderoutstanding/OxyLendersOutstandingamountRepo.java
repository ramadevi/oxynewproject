package com.oxyloans.repository.lenderoutstanding;

import com.oxyloans.entity.lenders.outstanding.OxyLendersOutstandingamount;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface OxyLendersOutstandingamountRepo
		extends PagingAndSortingRepository<OxyLendersOutstandingamount, Integer>,
		JpaSpecificationExecutor<OxyLendersOutstandingamount> {

	@Query(value = "SELECT * FROM public.oxy_lenders_outstandingamount where user_id=:userId order by id desc limit 1", nativeQuery = true)
	public OxyLendersOutstandingamount getUserIdWithLimit(@Param("userId") Integer userId);

	public OxyLendersOutstandingamount findByUserIdAndIsCurrentTrue(int userId);

}
