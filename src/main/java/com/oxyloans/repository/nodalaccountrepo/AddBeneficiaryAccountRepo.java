package com.oxyloans.repository.nodalaccountrepo;

import java.util.List;

import com.oxyloans.entity.nodalAccount.AddBeneficiaryAccount;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface AddBeneficiaryAccountRepo extends PagingAndSortingRepository<AddBeneficiaryAccount, Integer>,
		JpaSpecificationExecutor<AddBeneficiaryAccount> {
	public List<AddBeneficiaryAccount> findByBeneficiaryAccountnumber(String beneficiaryAccountNumber);

	@Query(value = "SELECT DISTINCT beneficiary_accountnumber FROM public.add_beneficiary_account", nativeQuery = true)
	public List<String> getListOfBeneficiaryAccountnumbers();

	@Query(value = "SELECT * FROM public.add_beneficiary_account where beneficiary_accountnumber=:beneficiaryAccountnumber", nativeQuery = true)
	public List<Object[]> getUsersByBeneficiaryAccountnumber(
			@Param("beneficiaryAccountnumber") String beneficiaryAccountnumber);

	String sql = "SELECT DISTINCT beneficiary_accountnumber FROM public.add_beneficiary_account where user_id=:userId ";

	@Query(value = sql, nativeQuery = true)
	List<String> getBeneficiaryByUserId(@Param("userId") Integer userId);

	@Query(value = "SELECT count(DISTINCT beneficiary_accountnumber) FROM public.add_beneficiary_account where user_id=:userId ", nativeQuery = true)
	public Integer getBeneficiaryCountBasedOnUserId(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.add_beneficiary_account where user_id=:userId and beneficiary_accountnumber=:beneficiaryAccountnumber and amount=:amount and (status='ADDED' OR status='EDITED') order by id ;", nativeQuery = true)
	public List<AddBeneficiaryAccount> getUsersByuserIdAndAccountNumberAndAmount(@Param("userId") int userId,
			@Param("beneficiaryAccountnumber") String beneficiaryAccountnumber, @Param("amount") double amount);

	@Query(value = "SELECT * FROM public.add_beneficiary_account where (status='ADDED' OR status='EDITED') and amount>0", nativeQuery = true)
	List<Object[]> getFundTransferPendingUsers();

	@Query(value = "SELECT count(id) FROM public.add_beneficiary_account where (status='ADDED' OR status='EDITED') and amount>0", nativeQuery = true)
	public Integer getFundTransferPendingUsersCount();

	@Query(value = "SELECT * FROM public.add_beneficiary_account where user_id=:userId and beneficiary_accountnumber=:beneficiaryAccountnumber and amount=:amount and (status='ADDED' OR status='EDITED')", nativeQuery = true)
	public AddBeneficiaryAccount getBeneficiaryDetailsAlreadyAdd(@Param("userId") Integer userId,
			@Param("beneficiaryAccountnumber") String beneficiaryAccountnumber, @Param("amount") double amount);

	@Query(value = "\r\n"
			+ "SELECT * FROM public.add_beneficiary_account where user_id=:userId and beneficiary_accountnumber=:beneficiaryAccountnumber order by id;", nativeQuery = true)
	public List<Object[]> getBeneficiaryAdded(@Param("userId") Integer userId,
			@Param("beneficiaryAccountnumber") String beneficiaryAccountnumber);

	@Query(value = "SELECT sum(amount) FROM public.add_beneficiary_account where user_id=:userId and borrower_parent_requestid=:borrowerParentRequestid", nativeQuery = true)
	public Double getSumOfBeneficiaryAmount(@Param("userId") Integer userId,
			@Param("borrowerParentRequestid") Integer borrowerParentRequestid);

	@Query(value = "SELECT * FROM public.add_beneficiary_account where user_id=:userId and beneficiary_accountnumber=:beneficiaryAccountnumber and amount=0", nativeQuery = true)
	public AddBeneficiaryAccount getDetailsByAccountNoAndId(@Param("userId") Integer userId,
			@Param("beneficiaryAccountnumber") String beneficiaryAccountnumber);

	@Query(value = "SELECT DISTINCT beneficiary_accountnumber FROM public.add_beneficiary_account where user_id=:userId", nativeQuery = true)
	List<String> getBeneficiaryAccountDetails(@Param("userId") Integer userId);

	@Query(value = "SELECT DISTINCT user_id FROM public.add_beneficiary_account", nativeQuery = true)
	public List<Integer> getListOfBeneficiaryAddedBorrower();

	@Query(value = "SELECT Count(DISTINCT user_id) FROM public.add_beneficiary_account;", nativeQuery = true)
	public Integer getCountForBeneficiaryAddedBorrower();

	@Query(value = "\r\n"
			+ "SELECT * FROM public.add_beneficiary_account where user_id=:userId and (status='ADDED' OR status='EDITED') and amount>0", nativeQuery = true)
	List<Object[]> getFundTransferPendingUsers(@Param("userId") int userId);

	@Query(value = "SELECT count(id) FROM public.add_beneficiary_account where user_id=:userId and (status='ADDED' OR status='EDITED') and amount>0", nativeQuery = true)
	public Integer getFundTransferPendingUsersCountById(@Param("userId") int userId);

	@Query(value = "\r\n"
			+ "SELECT * FROM public.add_beneficiary_account where user_id=:userId and (status='COMPLETED') and amount>0", nativeQuery = true)
	List<Object[]> getFundTransferCompletedById(@Param("userId") int userId);

	@Query(value = "SELECT * FROM public.add_beneficiary_account where user_id=:userId and beneficiary_accountnumber=:beneficiaryAccountnumber and (status='ADDED' OR status='EDITED') and amount=0", nativeQuery = true)
	public AddBeneficiaryAccount getUsersByuserIdAndAccountNumber(@Param("userId") int userId,
			@Param("beneficiaryAccountnumber") String beneficiaryAccountnumber);

	@Query(value = "SELECT * FROM public.add_beneficiary_account where user_id=:userId and beneficiary_accountnumber=:beneficiaryAccountnumber and amount=0", nativeQuery = true)
	public AddBeneficiaryAccount getFirstBeneficiaryAddedDetails(@Param("userId") Integer userId,
			@Param("beneficiaryAccountnumber") String beneficiaryAccountnumber);

}
