package com.oxyloans.repository.nodalaccountrepo;

import java.util.List;

import com.oxyloans.entity.nodalAccount.FundTransfer;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface FundTransferRepo
		extends PagingAndSortingRepository<FundTransfer, Integer>, JpaSpecificationExecutor<FundTransfer> {
	@Query(value = "SELECT sum(amount) FROM public.fund_transfer where user_id=:userId", nativeQuery = true)
	public Double getSumOfFundTransferAmount(@Param("userId") int userId);

	@Query(value = "SELECT * FROM public.fund_transfer", nativeQuery = true)
	List<Object[]> getTotalFundTransferDetails();

	@Query(value = "SELECT count(id) FROM public.fund_transfer", nativeQuery = true)
	public Integer getFundTransferCount();

	@Query(value = "SELECT * FROM public.fund_transfer where user_id=:userId", nativeQuery = true)
	List<Object[]> getTotalFundTransferDetailsByUserId(@Param("userId") int userId);

	@Query(value = "SELECT count(id) FROM public.fund_transfer where user_id=:userId", nativeQuery = true)
	public Integer getFundTransferCountByUserId(@Param("userId") int userId);

}
