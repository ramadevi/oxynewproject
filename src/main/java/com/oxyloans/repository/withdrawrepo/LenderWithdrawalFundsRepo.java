package com.oxyloans.repository.withdrawrepo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.lender.withdrawfunds.entity.LenderWithdrawalFunds;

public interface LenderWithdrawalFundsRepo extends PagingAndSortingRepository<LenderWithdrawalFunds, Integer> {

	@Query(value = " select * from public.lender_withdrawal_funds where id=:id and user_id=:userId", nativeQuery = true)
	public LenderWithdrawalFunds findingWithdrawRequestFromWallet(@Param("id") Integer id,
			@Param("userId") Integer userId);
}
