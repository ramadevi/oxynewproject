package com.oxyloans.repository.withdrawrepo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.lender.withdrawfunds.entity.LenderWithdrawalFunds;

public interface LenderWithdrawalFundsNativeRepo extends PagingAndSortingRepository<LenderWithdrawalFunds, Integer> {

	String sql = "SELECT * FROM Search_WithDrawalFundsLenders(:firstName,:lastName,:userId,:pageSize,:pageNumber)";

	@Query(value = sql, nativeQuery = true)
	List<Object[]> withdrawalFunds(@Param("firstName") String firstName, @Param("lastName") String lastName,

			@Param("userId") Integer userId, @Param("pageSize") Integer pageSize,

			@Param("pageNumber") Integer pageNumber);

	String countRecords = "SELECT * FROM COUNT_Search_WithDrawalFundsLenders(:firstName,:lastName,:userId,:pageSize,:pageNumber)";

	@Query(value = countRecords, nativeQuery = true)
	Integer countTranRecords(@Param("firstName") String firstName, @Param("lastName") String lastName,

			@Param("userId") Integer userId, @Param("pageSize") Integer pageSize,

			@Param("pageNumber") Integer pageNumber);

	@Query(value = "select * from public.lender_withdrawal_funds where status='REQUESTED' and receiver_user_id=0", nativeQuery = true)
	public List<LenderWithdrawalFunds> findIntiatedTransactions();

	@Query(value = "select * from public.lender_withdrawal_funds where status='INITIATED' and receiver_user_id>0", nativeQuery = true)
	public List<LenderWithdrawalFunds> findIntiatedWalletToWalletRequests();

	@Query(value = "select * from public.lender_withdrawal_funds where status='REQUESTED' and user_id=:userId and receiver_user_id=0", nativeQuery = true)
	public LenderWithdrawalFunds findbyUserId(Integer userId);

	@Query(value = "select sum(amount) from public.lender_withdrawal_funds where status='REQUESTED' and user_id=:userId and receiver_user_id=0", nativeQuery = true)
	public Double getTotalWithdrawlAmount(@Param("userId") int userId);

	@Query(value = "SELECT * FROM public.lender_withdrawal_funds where user_id=:senderId and receiver_user_id=:receiverId and status='INITIATED'", nativeQuery = true)
	public LenderWithdrawalFunds walletAmountAlreadyRequested(@Param("senderId") Integer senderId,
			@Param("receiverId") Integer receiverId);

	// @Query(value = "select * from public.lender_withdrawal_funds where
	// receiver_user_id>0 order by date(created_on) desc limit :pageSize offset
	// :pageNumber", nativeQuery = true)

	@Query(value = "select a.* from public.lender_withdrawal_funds a JOIN public.user b ON a.user_id = b.id  where b.test_user=false and  receiver_user_id>0 order by amount_expected_date desc limit :pageSize offset :pageNumber", nativeQuery = true)

	public List<LenderWithdrawalFunds> walletToWalletTransactionRequests(@Param("pageSize") Integer pageSize,
			@Param("pageNumber") Integer pageNumber);

	@Query(value = "select count(id) from public.lender_withdrawal_funds where receiver_user_id>0", nativeQuery = true)
	public Integer walletToWalletTransactionRequestsCount();

	@Query(value = "select * from public.lender_withdrawal_funds where user_id=:userId and receiver_user_id>0", nativeQuery = true)
	List<LenderWithdrawalFunds> findByUserId(@Param("userId") Integer userId);

	@Query(value = "select a.* from public.lender_withdrawal_funds a JOIN public.user b ON a.user_id = b.id  where b.test_user=true and  receiver_user_id>0 order by amount_expected_date desc limit :pageSize offset :pageNumber", nativeQuery = true)

	List<LenderWithdrawalFunds> walletToWalletTransactionRequestsTestLenders(@Param("pageSize") Integer pageSize,
			@Param("pageNumber") Integer pageNumber);

	@Query(value = "SELECT * FROM public.lender_withdrawal_funds where receiver_user_id>0 and status='INITIATED' and user_id=:userId order by id asc", nativeQuery = true)

	List<LenderWithdrawalFunds> getListOfWalletToWalletRequestRaised(@Param("userId") Integer userId);
	
	@Query(value = "SELECT * FROM public.lender_withdrawal_funds where status='REQUESTED' and user_id=:userId and receiver_user_id=0 order by id asc", nativeQuery = true)

	List<LenderWithdrawalFunds> getListOfWalletWithdrawRaised(@Param("userId") Integer userId);
	
	

}
