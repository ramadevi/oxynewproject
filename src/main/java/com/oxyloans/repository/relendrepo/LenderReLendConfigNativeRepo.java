package com.oxyloans.repository.relendrepo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.relend.entity.LenderReLendConfig;

public interface LenderReLendConfigNativeRepo extends PagingAndSortingRepository<LenderReLendConfig, Integer>{
	
	String sql = "SELECT * FROM Search_ReLendLenders(:firstName,:lastName,:userId,:pageSize,:pageNumber)";

	@Query(value = sql, nativeQuery = true)
	List<Object[]> searchReLendLenders(@Param("firstName") String firstName,@Param("lastName") String lastName,@Param("userId") Integer userId, @Param("pageSize") Integer pageSize,
			@Param("pageNumber") Integer pageNumber);
	
	String countRecords = "SELECT * FROM COUNT_Search_ReLendLenders(:firstName,:lastName,:userId,:pageSize,:pageNumber)";

	@Query(value = countRecords, nativeQuery = true)
	Integer countTranRecords(@Param("firstName") String firstName,@Param("lastName") String lastName,@Param("userId") Integer userId,@Param("pageSize") Integer pageSize,@Param("pageNumber") Integer pageNumber);

	
	String totalRecievedAmountQuery = "SELECT sum(emi_amount) FROM loan_emi_card A " + 
			"JOIN OXY_LOAN B " + 
			"ON A.loan_id=B.id " + 
			"where B.lender_user_id=:lenderUserId " + 
			"AND A.escrowtranferflag=0 and A.emi_paid_on is not null " + 
			"AND date_trunc('day', A.emi_paid_on) >= :relendConsiderDate ";

	@Query(value = totalRecievedAmountQuery, nativeQuery = true)
	Double totalReceivedEmiAmountAvbleForEscrow(@Param("relendConsiderDate") Date relendConsiderDate,@Param("lenderUserId") Integer lenderUserId);
	
	
	String totalAmountAddedToEscrowQuery = "SELECT sum(emi_amount) FROM loan_emi_card A " + 
			"JOIN OXY_LOAN B " + 
			"ON A.loan_id=B.id " + 
			"where B.lender_user_id=:lenderUserId " + 
			"AND A.escrowtranferflag=1 and A.emi_paid_on is not null " + 
			"AND date_trunc('day', A.emi_paid_on) >= :relendConsiderDate ";

	@Query(value = totalAmountAddedToEscrowQuery, nativeQuery = true)
	Double totalAmountAddedToEscrow(@Param("relendConsiderDate") Date relendConsiderDate,@Param("lenderUserId") Integer lenderUserId);
	
	
	String getEmiIdsQuery = "SELECT A.id FROM loan_emi_card A " + 
			"JOIN OXY_LOAN B " + 
			"ON A.loan_id=B.id " + 
			"where B.lender_user_id=:lenderUserId " + 
			"AND A.escrowtranferflag=0 and A.emi_paid_on is not null " + 
			"AND date_trunc('day', A.emi_paid_on) >= :relendConsiderDate ";

	@Query(value = getEmiIdsQuery, nativeQuery = true)
	List<Integer> getEmiIds(@Param("relendConsiderDate") Date relendConsiderDate,@Param("lenderUserId") Integer lenderUserId);
}
