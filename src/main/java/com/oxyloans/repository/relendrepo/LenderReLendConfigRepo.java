package com.oxyloans.repository.relendrepo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oxyloans.relend.entity.LenderReLendConfig;

public interface LenderReLendConfigRepo extends PagingAndSortingRepository<LenderReLendConfig, Integer>{

	public LenderReLendConfig findByUserId(Integer userId);
}
