package com.oxyloans.repository.relendrepo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.oxyloans.relend.entity.LenderReLendConfigArchive;

public interface LenderReLendConfigArchiveRepo extends PagingAndSortingRepository<LenderReLendConfigArchive, Integer>{

}
