package com.oxyloans.repository.enach;

import com.oxyloans.entity.enach.EnachMandateExceptionResponse;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EnachMandateExceptionResponseRepo extends PagingAndSortingRepository<EnachMandateExceptionResponse, Integer>{

	public EnachMandateExceptionResponse findByEnachMandateId(Integer mandateId);

	public EnachMandateExceptionResponse findByTpslTxnId(String transactionId);
}
