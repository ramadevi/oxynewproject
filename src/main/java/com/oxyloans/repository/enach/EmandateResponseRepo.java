package com.oxyloans.repository.enach;

import java.util.List;

import com.oxyloans.entity.enach.EnachMandateResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmandateResponseRepo extends PagingAndSortingRepository<EnachMandateResponse, Integer> {

	public EnachMandateResponse findByEnachMandateId(Integer mandateId);

	public EnachMandateResponse findByTpslTxnId(String transactionId);

	@Query(value = "SELECT loan_id,emi_number,created_on FROM public.enach_transaction_verification_response where enachstatus='SUCCESS' order by loan_id;", nativeQuery = true)
	public List<Object[]> getEnachSuccessUsersData();

}
