package com.oxyloans.repository.enach;

import com.oxyloans.entity.enach.AppLevelEnachMandateResponse;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AppLevelEnachMandateResponseRepo
		extends PagingAndSortingRepository<AppLevelEnachMandateResponse, Integer> {

	public AppLevelEnachMandateResponse findByEnachMandateId(Integer id);

}
