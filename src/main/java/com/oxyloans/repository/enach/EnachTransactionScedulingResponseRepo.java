package com.oxyloans.repository.enach;

import java.util.List;

import com.oxyloans.entity.enach.TransactionSchedulingResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EnachTransactionScedulingResponseRepo extends PagingAndSortingRepository<TransactionSchedulingResponse, Integer>{

	
	String transactionVerification = "SELECT * FROM public.enach_transaction_scheduling_response where is_verified=false";
	@Query(value = transactionVerification, nativeQuery = true)
	public List<TransactionSchedulingResponse> getEnachDetailsForTransactionVerification();
	
}
