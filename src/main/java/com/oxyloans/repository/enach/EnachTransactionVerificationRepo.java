package com.oxyloans.repository.enach;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.enach.TransactionVerificationResponse;

public interface EnachTransactionVerificationRepo
		extends PagingAndSortingRepository<TransactionVerificationResponse, Integer> {

	@Query(value = "select count(*),sum(B.emi_amount) AS C,Sum(B.emi_interst_amount) AS D from  enach_transaction_verification_response A join loan_emi_card B ON(A.loan_id=B.loan_id and A.emi_number=B.emi_number)where A.enachstatus='SUCCESS' and emi_due_on>=cast(date_trunc('month', current_date)as date) and emi_due_on<=cast( date_trunc('month',current_date)as date)+'1month'\\:\\:interval-'1day'\\:\\:interval and emi_paid_on is not null", nativeQuery = true)
	public List<Object[]> getNoOfEMIProcessedAndAmount();

	@Query(value = "select count(*),sum(emi_amount) from  enach_transaction_verification_response A join loan_emi_card B ON(A.loan_id=B.loan_id and A.emi_number=B.emi_number)where A.enachstatus='FAILED' and emi_due_on>=cast(date_trunc('month', current_date)as date) and emi_due_on<=cast( date_trunc('month',current_date)as date)+'1month'\\:\\:interval-'1day'\\:\\:interval", nativeQuery = true)
	public List<Object[]> getNoOfEMINotProcessedAndAmount();

	@Query(value = "SELECT  d.unique_number,CONCAT(c.first_name,'',c.last_name),a.loan_id FROM public.enach_transaction_verification_response a join public.oxy_loan b on a.loan_id=b.id join public.user_personal_details c on b.borrower_user_id=c.user_id  join public.user d on c.user_id=d.id where a.enachstatus='SUCCESS' and a.transactiondate=:modifiedDate", nativeQuery = true)
	public List<Object[]> findDetailsOfEnachTransactionResponses(@Param("modifiedDate") String modifiedDate);

}
