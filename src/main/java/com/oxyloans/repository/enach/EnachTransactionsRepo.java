package com.oxyloans.repository.enach;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.enach.EnachTransactions;

@Repository
public interface EnachTransactionsRepo extends PagingAndSortingRepository<EnachTransactions, Integer>, JpaSpecificationExecutor<EnachTransactions>  {
	
}
