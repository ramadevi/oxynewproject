package com.oxyloans.repository.enach;

import java.util.List;

import com.oxyloans.entity.enach.ApplicationLevelEnachMandate;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ApplicationLevelEnachMandateRepo
		extends PagingAndSortingRepository<ApplicationLevelEnachMandate, Integer>,
		JpaSpecificationExecutor<ApplicationLevelEnachMandate> {

	@Query(value = "select * from public.applicationlevel_enach_mandate where application_id=:applicationId", nativeQuery = true)
	public ApplicationLevelEnachMandate findByApplicationId(@Param("applicationId") Integer applicationId);

	@Query(value = "select * from public.applicationlevel_enach_mandate where application_id=:id", nativeQuery = true)
	public List<ApplicationLevelEnachMandate> findByApplicationIdToActivateEnach(@Param("id") Integer id);

	@Query(value = "select * from public.applicationlevel_enach_mandate where transaction_id=:transactionId", nativeQuery = true)
	public ApplicationLevelEnachMandate findBymerchantTransactionIdentifierForAppLevel(
			@Param("transactionId") String transactionId);

	@Query(value = "select * from public.applicationlevel_enach_mandate where application_id=:applicationId", nativeQuery = true)
	public List<ApplicationLevelEnachMandate> findingListByApplicationId(@Param("applicationId") Integer applicationId);

	@Query(value = "select * from public.applicationlevel_enach_mandate where mandate_status='SUCCESS' and application_id=:id and upper(is_principal_or_interest)=upper('PI')", nativeQuery = true)
	public ApplicationLevelEnachMandate findingIsEnachActivatedForPI(@Param("id") Integer id);

	@Query(value = "select * from public.applicationlevel_enach_mandate where \r\n"
			+ "mandate_status='SUCCESS' and application_id=:id and upper(is_principal_or_interest)=upper('P')", nativeQuery = true)
	public ApplicationLevelEnachMandate findingIsEnachActivatedForP(@Param("id") Integer id);

	@Query(value = "select * from public.applicationlevel_enach_mandate where \r\n"
			+ "mandate_status='SUCCESS' and application_id=:id and upper(is_principal_or_interest)=upper('I')", nativeQuery = true)
	public ApplicationLevelEnachMandate findingIsEnachActivatedForI(Integer id);
}