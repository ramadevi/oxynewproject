package com.oxyloans.repository.enach;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.enach.EnachMinimumWithdrawUsers;

public interface EnachMinimumWithdrawUsersRepo extends PagingAndSortingRepository<EnachMinimumWithdrawUsers, Integer>{
	
	public EnachMinimumWithdrawUsers findByBorrowerId(Integer borrowerId);
	
	String sql = "SELECT * FROM Get_Enachminiumwithdrwusers(:pageSize,:pageNumber)";

	@Query(value = sql, nativeQuery = true)
	List<Object[]> getEnachMinimumWithdrawUsers(@Param("pageSize") Integer pageSize,
			@Param("pageNumber") Integer pageNumber);
	
	String countRecords = "SELECT * FROM COUNT_EnachMiniumWithdrawUsers()";

	@Query(value = countRecords, nativeQuery = true)
	Integer countTranRecords();
	
	
	String searchEnachMinimumWithdrawUsers = "SELECT * FROM Search_Enachminiumwithdrwusers(:firstName,:lastName,:userId,:pageSize,:pageNumber)";

	@Query(value = searchEnachMinimumWithdrawUsers, nativeQuery = true)
	List<Object[]> searchEnachMinimumWithdrawUsers(@Param("firstName") String firstName,@Param("lastName") String lastName,@Param("userId") Integer userId,@Param("pageSize") Integer pageSize,
			@Param("pageNumber") Integer pageNumber);
	
	String searchCountRecords = "SELECT * FROM COUNT_Search_Enachminiumwithdrwusers(:firstName,:lastName,:userId,:pageSize,:pageNumber)";

	@Query(value = searchCountRecords, nativeQuery = true)
	Integer seacrhCountTranRecords(@Param("firstName") String firstName,@Param("lastName") String lastName,@Param("userId") Integer userId,@Param("pageSize") Integer pageSize,
			@Param("pageNumber") Integer pageNumber);

}
