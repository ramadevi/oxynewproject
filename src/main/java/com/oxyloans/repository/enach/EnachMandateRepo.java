package com.oxyloans.repository.enach;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.enach.EnachMandate;

@Repository
public interface EnachMandateRepo extends PagingAndSortingRepository<EnachMandate, Integer>, JpaSpecificationExecutor<EnachMandate> {

    public EnachMandate findByMandateTransactionId(String merchantTransactionIdentifier);
   
    @Query(value = "SELECT * FROM enach_mandate where transaction_id=:merchantTransactionIdentifier",nativeQuery = true)
    public EnachMandate findBymerchantTransactionIdentifier(@Param("merchantTransactionIdentifier") String merchantTransactionIdentifier);
    
    @Query(value = "update public.enach_mandate set mandate_status=:status where transaction_id=::merchantTransactionIdentifier",nativeQuery = true)
    public void updateTransactionStatus(@Param("status") String status,@Param("merchantTransactionIdentifier") String merchantTransactionIdentifier);

	public EnachMandate findByoxyLoanId(int parseInt);

	@Query(value = "select * from public.enach_mandate where oxy_loan_id=:loanId order by id", nativeQuery = true)
	public List<EnachMandate> findByOxyLoanId(@Param("loanId") int loanId);

}
