package com.oxyloans.repository.enach;

import java.util.List;

import com.oxyloans.entity.enach.AmountUpdationBasedOnDisbursment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface AmountUpdationBasedOnDisbursmentRepo
		extends PagingAndSortingRepository<AmountUpdationBasedOnDisbursment, Integer> {

	@Query(value = "select * from public.amount_updation_basedon_disbursment where application_id=:applicationId and emi_number=:emiId", nativeQuery = true)
	public List<AmountUpdationBasedOnDisbursment> listOfDetailsBasedOnEmiNumberAndApplicationId(
			@Param("applicationId") Integer applicationId, @Param("emiId") Integer emiId);

	@Query(value = "select * from public.amount_updation_basedon_disbursment where application_id=:applicationId and emi_number=:emiId and user_id=:lenderUserId\r\n"
			+ "", nativeQuery = true)
	public AmountUpdationBasedOnDisbursment findingUsersBasedOnAppIdEmiId(@Param("applicationId") Integer applicationId,
			@Param("emiId") Integer emiId, @Param("lenderUserId") Integer lenderUserId);

}
