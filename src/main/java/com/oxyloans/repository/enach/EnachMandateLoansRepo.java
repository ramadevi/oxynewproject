package com.oxyloans.repository.enach;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.oxyloans.entity.enach.EnachMandateLoans;

@Repository
public interface EnachMandateLoansRepo extends PagingAndSortingRepository<EnachMandateLoans, Integer>, JpaSpecificationExecutor<EnachMandateLoans> {

}
