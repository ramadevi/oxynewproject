package com.oxyloans.repository.enach;

import java.util.List;

import com.oxyloans.entity.enach.TransactionSchedulingResponseAppLevel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EnachTransactionScedulingResponseRepoAppLevel extends PagingAndSortingRepository<TransactionSchedulingResponseAppLevel, Integer> {

	@Query(value = "SELECT * FROM public.enach_transaction_scheduling_response_applevel where is_verified=false", nativeQuery = true)
	public List<TransactionSchedulingResponseAppLevel> getEnachDetailsForTransactionVerification();
	
}
