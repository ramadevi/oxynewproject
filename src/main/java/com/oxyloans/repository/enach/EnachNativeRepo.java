package com.oxyloans.repository.enach;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oxyloans.entity.lender.oxywallet.LenderOxyWallet;

public interface EnachNativeRepo extends JpaRepository<LenderOxyWallet, Serializable> {

	String sql = "SELECT b.unique_number,a.clnt_txn_ref,a.consumerid,a.tpsl_txn_time from public.enach_mandate_response a\r\n"
			+ "JOIN public.enach_mandate c\r\n" + "ON a.enach_mandate_id=c.id\r\n" + "JOIN public.oxy_loan d \r\n"
			+ "ON d.id=c.oxy_loan_id\r\n" + "JOIN public.user b\r\n" + "ON b.id=d.borrower_user_id \r\n"
			+ "where UPPER(a.txn_msg)='INITIATED' OR a.bankreferenceidentifier is null";

	@Query(value = sql, nativeQuery = true)
	List<Object[]> getInitiatedMandateResponses();

	String updQuery = "SELECT public.updateenachdetails(:pbankreferenceidentifier,:pmandateverificationresponsejson,:ptransactionid,:pmandate_status,:pis_ecs_activated)";

	@Query(value = updQuery, nativeQuery = true)
	public void updateMandateVerificationResponses(@Param("pbankreferenceidentifier") String pbankreferenceidentifier,
			@Param("pmandateverificationresponsejson") String pmandateverificationresponsejson,
			@Param("ptransactionid") String ptransactionid, @Param("pmandate_status") String pmandate_status,
			@Param("pis_ecs_activated") Boolean pis_ecs_activated);

}
