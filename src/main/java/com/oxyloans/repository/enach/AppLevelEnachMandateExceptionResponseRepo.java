package com.oxyloans.repository.enach;

import com.oxyloans.entity.enach.AppLevelEnachMandateExceptionResponse;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AppLevelEnachMandateExceptionResponseRepo
		extends PagingAndSortingRepository<AppLevelEnachMandateExceptionResponse, Integer> {

}
