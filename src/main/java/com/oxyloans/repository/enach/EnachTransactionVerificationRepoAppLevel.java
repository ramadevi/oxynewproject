package com.oxyloans.repository.enach;


import org.springframework.data.repository.PagingAndSortingRepository;


import com.oxyloans.entity.enach.TransactionVerificationResponseAppLevel;


public interface EnachTransactionVerificationRepoAppLevel
		extends PagingAndSortingRepository<TransactionVerificationResponseAppLevel, Integer> {

}
