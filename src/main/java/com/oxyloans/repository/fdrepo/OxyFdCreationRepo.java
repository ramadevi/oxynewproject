package com.oxyloans.repository.fdrepo;

import com.oxyloans.entity.fd.OxyFdCreation;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface OxyFdCreationRepo
		extends PagingAndSortingRepository<OxyFdCreation, Integer>, JpaSpecificationExecutor<OxyFdCreation> {

	@Query(value = "SELECT * FROM public.oxy_fd_creation where user_id=:userId", nativeQuery = true)
	public OxyFdCreation fdStatusUpdationUsingUserid(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.oxy_fd_creation where account_number=:accountNo", nativeQuery = true)
	public OxyFdCreation searchUserByAccountNumber(@Param("accountNo") String accountNo);

}
