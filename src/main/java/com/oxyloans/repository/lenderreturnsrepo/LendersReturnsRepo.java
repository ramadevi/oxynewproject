package com.oxyloans.repository.lenderreturnsrepo;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import com.oxyloans.entity.lenders.returns.LendersReturns;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface LendersReturnsRepo
		extends PagingAndSortingRepository<LendersReturns, Integer>, JpaSpecificationExecutor<LendersReturns> {

	@Query(value = "SELECT * FROM public.lenders_returns where user_id=:lenderId and paid_date=:paidDate and amount_type=:deditedType and remarks=:remarks", nativeQuery = true)
	public LendersReturns getAmountAlreadyUpdated(@Param("lenderId") Integer lenderId, @Param("paidDate") Date paidDate,
			@Param("deditedType") String deditedType, @Param("remarks") String remarks);

	@Query(value = "SELECT * FROM public.lenders_returns where user_id=:userId and amount_type=:amountType and amount_type!='REJECTED'", nativeQuery = true)
	public List<Object[]> getLenderAmountByType(@Param("userId") Integer userId,
			@Param("amountType") String amountType);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST') and user_id=:userId and amount_type!='REJECTED'", nativeQuery = true)
	public Double getSumOfInterestAmount(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERPRINCIPAL' and user_id=:userId and amount_type!='REJECTED'", nativeQuery = true)
	public Double getSumOfPrincipalAmount(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED'  and user_id=:userId and amount_type!='REJECTED'", nativeQuery = true)
	public Double getSumOfWithDraw(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where user_id=:userId and deal_id=:dealId and to_char(paid_date, 'mm/yyyy')=:emiReceivingDate and (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST')", nativeQuery = true)
	public Double interestReturnedBasedOnPaidDate(@Param("userId") Integer userId, @Param("dealId") Integer dealId,
			@Param("emiReceivingDate") String emiReceivingDate);

	@Query(value = "SELECT * FROM public.lenders_returns where remarks LIKE :remarks and to_char(paid_date, 'dd/mm/yyyy')=:emiReceivingDate", nativeQuery = true)
	public List<Object[]> checkInterestAmountAlreadyUpdated(@Param("remarks") String remarks,
			@Param("emiReceivingDate") String emiReceivingDate);

	@Query(value = "SELECT * FROM public.lenders_returns where user_id=:lenderId and  to_char(paid_date, 'dd/mm/yyyy')=:paidDate and remarks LIKE :remarks", nativeQuery = true)
	public LendersReturns getInterestAlreadyUpdated(@Param("lenderId") Integer lenderId,
			@Param("paidDate") String paidDate, @Param("remarks") String remarks);

	@Query(value = "SELECT DISTINCT deal_id FROM public.lenders_returns where user_id=:userId and amount_type=:amountType order by deal_id asc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Integer> getLenderReturnesBasedOnAscOrder(@Param("userId") Integer userId,
			@Param("amountType") String amountType, @Param("pageSize") Integer pageSize,
			@Param("pageNo") Integer pageNo);

	@Query(value = "SELECT DISTINCT deal_id FROM public.lenders_returns where user_id=:userId and amount_type=:amountType order by deal_id desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Integer> getLenderReturnesBasedOnDescOrder(@Param("userId") Integer userId,
			@Param("amountType") String amountType, @Param("pageSize") Integer pageSize,
			@Param("pageNo") Integer pageNo);

	@Query(value = "SELECT count(DISTINCT deal_id) FROM public.lenders_returns where user_id=:userId and amount_type=:amountType and deal_id>0", nativeQuery = true)
	public Integer getLenderReturnesCount(@Param("userId") Integer userId, @Param("amountType") String amountType);

	@Query(value = "SELECT a.amount,a.paid_date,b.id,b.deal_name,b.funds_acceptance_start_date,b.borrower_closing_status,date(b.borrower_closed_date) as closed,date(c.received_on) as paticiapted,c.lender_returns_type,c.rateofinterest FROM public.lenders_returns a join public.oxy_borrowers_deals_information b on a.deal_id=b.id join public.oxy_lenders_accepted_deals c on (a.deal_id=c.deal_id and a.user_id=c.user_id) where (a.amount_type='LENDERINTEREST' OR a.amount_type='PRINCIPALINTEREST' OR a.amount_type='WITHDRAWALINTEREST') and a.user_id=:userId and a.deal_id not in ('63','64','65','66','67','68','69','70','71','72','73','74','75','76','77','78','79','5') and a.deal_id!=0 order by a.paid_date desc", nativeQuery = true)
	public List<Object[]> getLenderInterestReturnedForNewDeals(@Param("userId") Integer userId);

	@Query(value = "select a.amount,a.paid_date,a.remarks,b.monthly_interest,c.borrower_closing_status,date(c.borrower_closed_date) from public.lenders_returns a join public.oxy_deals_rateofinterest_to_lendersgroup b on a.deal_id=b.deal_id join public.oxy_borrowers_deals_information c on a.deal_id=c.id where a.user_id=:userId and a.deal_id in ('63','64','65','66','67','68','69','70','71','72','73','74','75','76','77','78','79','5') and a.deal_id!=0 and (a.amount_type='LENDERINTEREST' OR a.amount_type='PRINCIPALINTEREST' OR a.amount_type='WITHDRAWALINTEREST') and b.group_id=1", nativeQuery = true)
	public List<Object[]> getLenderInterestReturnedForOldDeals(@Param("userId") Integer userId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type=:amountType and user_id=:userId and amount_type!='REJECTED' and deal_id=:dealId", nativeQuery = true)
	public Double getSumOfAmountReturned(@Param("userId") Integer userId, @Param("amountType") String amountType,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT amount,paid_date,remarks FROM public.lenders_returns where (amount_type=:amountType OR amount_type='LENDERWITHDRAW' ) and status='INITIATED' and user_id=:userId and amount_type!='REJECTED' and deal_id=:dealId order by paid_date asc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getLenderListBasedOndealIdBasedOnAsc(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId, @Param("amountType") String amountType,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "SELECT amount,paid_date,remarks FROM public.lenders_returns where amount_type=:amountType and user_id=:userId and amount_type!='REJECTED' and deal_id=:dealId order by paid_date desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getLenderListBasedOndealIdBasedOnDesc(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId, @Param("amountType") String amountType,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "SELECT count(id) FROM public.lenders_returns where (amount_type=:amountType OR amount_type='LENDERWITHDRAW' ) and status='INITIATED' and user_id=:userId and amount_type!='REJECTED' and deal_id=:dealId", nativeQuery = true)
	public Integer getLenderReturnedCountToSpecificDeal(@Param("userId") Integer userId,
			@Param("amountType") String amountType, @Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lenders_returns where user_id=:userId and deal_id=:dealId and (amount_type='LENDERPRINCIPAL' OR amount_type='LENDERWITHDRAW') and status='INITIATED'  order by paid_date desc limit 1", nativeQuery = true)
	public LendersReturns getLatestPricipalReturnedDate(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED' and user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public Double getLenderWithdrawSumValue(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='REQUESTED' and user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public Double getLenderWithdrawRequestedSumValue(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	// @Query(value = "SELECT id,user_id,amount,deal_id,date(requested_on),status
	// FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and
	// (status='REQUESTED' OR status='H2HAPPROVAL' OR status='APPROVED') order by
	// status desc,date(requested_on) asc limit :pageSize offset :pageNo",
	// nativeQuery = true)

	@Query(value = "SELECT a.id, a.user_id, a.amount, a.deal_id, date(a.requested_on) , a.status "
			+ "FROM public.lenders_returns a " + "JOIN public.user b ON a.user_id = b.id "
			+ "WHERE a.amount_type = 'LENDERWITHDRAW' "
			+ "    AND (a.status = 'REQUESTED' OR a.status = 'H2HAPPROVAL' OR a.status = 'APPROVED') "
			+ "    AND b.test_user = false " + "ORDER BY a.id DESC "
			+ "LIMIT :pageSize OFFSET :pageNo", nativeQuery = true)
	public List<Object[]> getLendersWithdrawRequests(@Param("pageSize") Integer pageSize,
			@Param("pageNo") Integer pageNo);

	@Query(value = "SELECT count(id) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and (status='REQUESTED' OR status='H2HAPPROVAL' OR status='APPROVED')", nativeQuery = true)
	public Integer getLendersWithdrawRequestsCountValue();

	@Query(value = "SELECT * FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='REQUESTED' and user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public List<LendersReturns> getLenderWithdrawRequestedInfo(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lenders_returns where user_id=:userId and deal_id=:dealId and (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST') order by paid_date desc limit 1", nativeQuery = true)
	public LendersReturns getLenderLatestInterestReturnedDate(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED' and user_id=:userId and deal_id=:dealId order by paid_date desc limit 1", nativeQuery = true)
	public LendersReturns getLenderWithdrawLatestApproval(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "select * from public.lenders_returns where user_id=:refereeId and deal_id=:dealId and\r\n"
			+ "(amount_type='LENDERWITHDRAW' OR amount_type='LENDERPRINCIPAL') and status='INITIATED'\r\n"
			+ "order by paid_date asc limit 1", nativeQuery = true)
	public LendersReturns findingWithdralDate(@Param("refereeId") Integer refereeId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='APPROVED' and user_id=:userId order by paid_date desc", nativeQuery = true)
	public List<LendersReturns> getLenderWithdrawalsApprovedButNotDisbursed(@Param("userId") Integer userId);

	@Query(value = "select SUM(amount) from public.lenders_returns where (amount_type='LENDERWITHDRAW' or amount_type='LENDERPRINCIPAL')\r\n"
			+ "and status='INITIATED' and user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public Double findingSumOfWithdrawlAmount(@Param("dealId") Integer dealId, @Param("userId") Integer userId);

	@Query(value = "SELECT SUM(amount) FROM public.lenders_returns where (amount_type='LENDERPRINCIPAL' OR amount_type='LENDERWITHDRAW') and status='INITIATED' and amount_type!='REJECTED' and deal_id=:dealId", nativeQuery = true)
	public Double getSumValueReturnedToDeal(@Param("dealId") Integer dealId);

	@Query(value = "SELECT paid_date,remarks,amount,days_difference FROM public.lenders_returns where user_id=:userId and deal_id not in ('23','4','20','21','22','27','39','38') and (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST') order by paid_date asc,id asc,remarks asc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getLenderInterestReturnedBasedOnAscOrder(@Param("userId") Integer userId,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "SELECT paid_date,remarks,amount,days_difference FROM public.lenders_returns where user_id=:userId and deal_id not in ('23','4','20','21','22','27','39','38') and (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST') order by paid_date desc,id desc,remarks desc limit :pageSize offset :pageNo", nativeQuery = true)
	public List<Object[]> getLenderInterestReturnedBasedOnDescOrder(@Param("userId") Integer userId,
			@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo);

	@Query(value = "SELECT count(id) FROM public.lenders_returns where user_id=:userId and deal_id not in ('23','4','20','21','22','27','39','38') and (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST')", nativeQuery = true)
	public Integer getLenderInterestReturnedCount(@Param("userId") Integer userId);

	@Query(value = "SELECT amount,paid_date,remarks FROM public.lenders_returns where (amount_type=:amountType OR amount_type='LENDERWITHDRAW' ) and status='INITIATED' and user_id=:userId and amount_type!='REJECTED' and deal_id=:dealId order by paid_date asc", nativeQuery = true)
	public List<Object[]> getLenderReturnedAmount(@Param("userId") Integer userId, @Param("dealId") Integer dealId,
			@Param("amountType") String amountType);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST') and user_id=:userId and amount_type!='REJECTED' and deal_id=:dealId", nativeQuery = true)
	public Double getSumOfAmountReturnedForInterest(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "select sum(amount) from public.lenders_returns where (amount_type='LENDERPRINCIPAL' or amount_type='LENDERWITHDRAW') and\r\n"
			+ "status ='INITIATED' and deal_id=:dealId", nativeQuery = true)
	public Double findingSumOfPrincipalAmountReturned(@Param("dealId") int dealId);

	@Query(value = "SELECT * FROM public.lenders_returns where (amount_type='LENDERPRINCIPAL' OR amount_type='LENDERWITHDRAW')  and status='INITIATED' and user_id=:userId and amount_type!='REJECTED' and deal_id=:dealId order by paid_date asc", nativeQuery = true)
	public List<LendersReturns> principalReturn(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "select sum(amount) from public.lenders_returns where (amount_type='LENDERPRINCIPAL' or amount_type='LENDERWITHDRAW') and\r\n"
			+ "status ='INITIATED' and user_id=:userId and deal_id>0", nativeQuery = true)
	public Double sumOfPrincipalAmountReturnedToLender(@Param("userId") int userId);

	@Query(value = "select * from public.lenders_returns where deal_id=:dealId and status='REQUESTED'", nativeQuery = true)
	public List<LendersReturns> findingWithdrawalsInRequestedState(@Param("dealId") Integer dealId);

	@Query(value = "select * from public.lenders_returns where deal_id=:dealId and status='INITIATED' \r\n"
			+ "and (amount_type='LENDERPRINCIPAL' or amount_type='LENDERWITHDRAW')", nativeQuery = true)
	public List<LendersReturns> findingWithdrawalIntiatedList(@Param("dealId") Integer dealId);

	@Query(value = "SELECT distinct a.deal_id FROM public.lenders_returns a join public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.user_id=:userId and (a.amount_type='LENDERINTEREST' OR a.amount_type='PRINCIPALINTEREST' OR a.amount_type='WITHDRAWALINTEREST') and a.paid_date>=:startDate and a.paid_date<=:endDate and a.deal_id>0 and b.deal_type!='EQUITY'", nativeQuery = true)
	public List<Integer> interestReturnedDealIds(@Param("userId") int userId, @Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "select sum(amount) from public.lenders_returns where (amount_type='LENDERPRINCIPAL' or amount_type='LENDERWITHDRAW') and\r\n"
			+ "status ='INITIATED' and user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public Double principalAmountReturnedToLenderBasedOnDeal(@Param("userId") int userId,
			@Param("dealId") Integer dealId);

	@Query(value = "select sum(amount) from public.lenders_returns a join public.oxy_borrowers_deals_information b on a.deal_id=b.id\r\n"
			+ "where user_id=:userId and\r\n"
			+ "(amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST') \r\n"
			+ "and to_char(paid_date,'yyyy-mm-dd')>=:startingYear and to_char(paid_date,'yyyy-mm-dd')<=:endingYear\r\n"
			+ "and deal_type!='EQUITY'", nativeQuery = true)
	public Double totalInterestEarnedInFinancialYear(@Param("userId") int userId,
			@Param("startingYear") String startingYear, @Param("endingYear") String endingYear);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where user_id=:userId and (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST') and paid_date>=:startDate and paid_date<=:endingDate and deal_id=:dealId", nativeQuery = true)
	public Double interestEarnedInFinancialYearToDeal(@Param("userId") int userId, @Param("startDate") Date startDate,
			@Param("endingDate") Date endingDate, @Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lenders_returns where user_id=:userId and (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST') and paid_date>=:startDate and paid_date<=:endingDate and deal_id=0 order by id desc", nativeQuery = true)
	public List<LendersReturns> oldDealsFinancialYearInterestEarned(@Param("userId") int userId,
			@Param("startDate") Date startDate, @Param("endingDate") Date endingDate);

	@Query(value = "SELECT * FROM public.lenders_returns where user_id=:userId and deal_id=:dealId and (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST' OR amount_type='LENDERPRINCIPAL' OR amount_type='LENDERWITHDRAW') and status='INITIATED' order by paid_date,id", nativeQuery = true)
	public List<LendersReturns> amountReturnedToLenders(@Param("userId") int userId, @Param("dealId") int dealId);

	@Query(value = "SELECT * FROM public.lenders_returns where file_name_for_principal_withdrawal=:fileName", nativeQuery = true)
	public LendersReturns principalWithDrawalFileName(@Param("fileName") String fileName);

	@Query(value = "SELECT * FROM public.lenders_returns where file_name_for_interest=:fileName", nativeQuery = true)
	public LendersReturns interestFileName(@Param("fileName") String fileName);

	@Query(value = "select a.remarks,a.amount,a.deal_id,a.amount_type,a.paid_date,a.id,c.lender_returns_type,c.rateofinterest,c.received_on,b.borrower_closed_date,b.funds_acceptance_start_date  from public.lenders_returns a join public.oxy_borrowers_deals_information b on a.deal_id=b.id join public.oxy_lenders_accepted_deals c on (a.deal_id=c.deal_id and a.user_id=c.user_id) where a.deal_id not in ('79','78','76') and a.user_id=:userId and (a.amount_type='LENDERINTEREST' OR a.amount_type='PRINCIPALINTEREST' OR a.amount_type='WITHDRAWALINTEREST' OR a.amount_type='LENDERPRINCIPAL' OR a.amount_type='LENDERWITHDRAW') and a.status='INITIATED' and b.borrower_closing_status='CLOSED'", nativeQuery = true)
	public List<Object[]> totalClosedDeals(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.lenders_returns where user_id=:userId and deal_id=:dealId and (amount_type='LENDERPRINCIPAL' or amount_type='LENDERWITHDRAW') order by paid_date desc limit 1", nativeQuery = true)
	public LendersReturns principalReturnedDate(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "select * from public.lenders_returns where user_id=:userId and status='INITIATED' and amount_type='LENDERWITHDRAW'", nativeQuery = true)
	public List<LendersReturns> findingListOfWithdrawFormDeals(@Param("userId") int userId);

	@Query(value = "SELECT count(*) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status!='INITIATED' and deal_id=:dealId", nativeQuery = true)
	public Integer getCountOfTotalWithDrawls(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status!='INITIATED' and deal_id=:dealId", nativeQuery = true)
	public Double getLenderWithdrawApprovalValue(@Param("dealId") Integer dealId);

	@Query(value = "select sum(amount) from public.lenders_returns where (amount_type='LENDERPRINCIPAL' or amount_type='LENDERWITHDRAW') and\r\n"
			+ "status ='INITIATED' and deal_id=:dealId", nativeQuery = true)
	public BigInteger findingSumOfPrincipalAmountReturned1(@Param("dealId") int dealId);

	@Query(value = "select sum(amount) from public.lenders_returns where user_id=:userId and deal_id=:dealId\r\n"
			+ "and (amount_type='LENDERINTEREST' or amount_type='PRINCIPALINTEREST' or amount_type='WITHDRAWALINTEREST')\r\n"
			+ "and status='INITIATED'", nativeQuery = true)
	public BigInteger findingSumOfInterestAmount(@Param("dealId") Integer dealId, @Param("userId") Integer userId);

	@Query(value = "select sum(amount) from public.lenders_returns where user_id=:userId and deal_id=:dealId\r\n"
			+ "and (amount_type='LENDERWITHDRAW' or amount_type='LENDERPRINCIPAL')\r\n"
			+ "and status='INITIATED';", nativeQuery = true)
	public BigInteger findingSumOfPrincipalAmountForDeal(@Param("dealId") Integer dealId,
			@Param("userId") Integer userId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type in('LENDERINTEREST','PRINCIPALINTEREST','WITHDRAWALINTEREST') and status='INITIATED'  and to_char(paid_date,'dd-mm-yyyy')=:date and amount_type!='REJECTED'", nativeQuery = true)
	public Double getSumOfInterestAmountInGivenDate(@Param("date") String date);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type=:amountType and status='INITIATED'  and to_char(paid_date,'dd-mm-yyyy')=:date and amount_type!='REJECTED'", nativeQuery = true)
	public Double getSumOfAmountInGivenDate(@Param("date") String date, @Param("amountType") String amountType);

	@Query(value = "select * from public.lenders_returns where user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public LendersReturns findingIsDealIsPresent(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "select sum(amount) from public.lenders_returns where (amount_type!='LENDERINTEREST'\r\n"
			+ "OR amount_type!='LENDERPRINCIPALINTEREST' or amount_type!='LENDERWITHDRAWINTEREST')\r\n"
			+ "and status='INITIATED' and user_id=:userId", nativeQuery = true)
	public Double findingSumOfWithdrawnAmountOfLender(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.lenders_returns where user_id=:userId and deal_id=:dealId and to_char(actual_date, 'mm/yyyy')=:emiReceivingDate and (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST')", nativeQuery = true)
	public LendersReturns interestReturnedBasedOnActuvalDate(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId, @Param("emiReceivingDate") String emiReceivingDate);

	@Query(value = "SELECT * FROM public.lenders_returns where user_id=:userId and deal_id=:dealId and to_char(paid_date, 'mm/yyyy')=:emiReceivingDate and (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST') order by paid_date desc limit 1", nativeQuery = true)
	public LendersReturns interestReturnedBasedOnPaidDateList(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId, @Param("emiReceivingDate") String emiReceivingDate);

	@Query(value = "select * from public.lenders_returns where deal_id=:dealId and to_char(actual_date,'MM-YYYY')=:paymentDate and amount_type in ('LENDERINTEREST')", nativeQuery = true)
	public List<LendersReturns> findByDealIdInParticularMonthAndYear(@Param("dealId") Integer dealId,
			@Param("paymentDate") String paymentDate);

	@Query(value = "select a.remarks,a.amount,a.deal_id,a.amount_type,a.paid_date,a.id,c.lender_returns_type,c.rateofinterest,c.received_on,b.borrower_closed_date,b.funds_acceptance_start_date  from public.lenders_returns a join public.oxy_borrowers_deals_information b on a.deal_id=b.id join public.oxy_lenders_accepted_deals c on (a.deal_id=c.deal_id and a.user_id=c.user_id) where a.deal_id not in ('79','78','76') and a.user_id=:userId and (a.amount_type='LENDERINTEREST' OR a.amount_type='PRINCIPALINTEREST' OR a.amount_type='WITHDRAWALINTEREST' OR a.amount_type='LENDERPRINCIPAL' OR a.amount_type='LENDERWITHDRAW') and a.status='INITIATED'", nativeQuery = true)
	public List<Object[]> totalClosedDealsForUser(@Param("userId") Integer userId);

	@Query(value = "select * from public.lenders_returns where user_id=:userId and amount_type='LENDERWITHDRAW' order by id desc", nativeQuery = true)
	public List<LendersReturns> findingListOfWithdrawFormDealsForUser(@Param("userId") Integer userId);

	public List<LendersReturns> findByDealId(int dealId);

	@Query(value = "SELECT * FROM public.lenders_returns where amount_type=:amountType and status='INITIATED'  and to_char(paid_date,'dd-mm-yyyy')=:date and amount_type!='REJECTED'", nativeQuery = true)
	public List<LendersReturns> listOfAmountsInGivenDate(@Param("date") String date,
			@Param("amountType") String amountType);

	@Query(value = "SELECT sum(a.amount) FROM public.lenders_returns a join public.oxy_borrowers_deals_information b \r\n"
			+ "on a.deal_id=b.id where a.user_id=:userId and \r\n"
			+ "(a.amount_type='LENDERINTEREST' OR a.amount_type='PRINCIPALINTEREST' OR a.amount_type='WITHDRAWALINTEREST')\r\n"
			+ "and to_char(a.paid_date,'yyyy-mm-dd')>=:startDate and to_char(a.paid_date,'yyyy-mm-dd')<=:endDate and a.deal_id>0 and b.deal_type!='EQUITY'\r\n"
			+ "", nativeQuery = true)
	public BigInteger totalInterestEarnedInFinancialYearBasedOnDate(@Param("userId") Integer userId,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	@Query(value = "select * from public.lenders_returns where id=:id and user_id=:userId", nativeQuery = true)
	public LendersReturns findingWithdrawRequestFromDeal(@Param("id") Integer id, @Param("userId") Integer userId);

	@Query(value = "select user_id,amount,paid_date,deal_id from public.lenders_returns a join public.oxy_borrowers_deals_information b\r\n"
			+ "on a.deal_id=b.id \r\n"
			+ "where (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST')\r\n"
			+ "and user_id=:userId and to_char(paid_date,'yyyy-MM')=:requiredDate and deal_type!='EQUITY'\r\n"
			+ "and status='INITIATED'", nativeQuery = true)
	public List<Object[]> findingInterestDetails(@Param("userId") Integer userId,
			@Param("requiredDate") String requiredDate);

	@Query(value = "select user_id,amount,paid_date,deal_id from public.lenders_returns a join public.oxy_borrowers_deals_information b\r\n"
			+ "on a.deal_id=b.id \r\n"
			+ "where (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST')\r\n"
			+ "and user_id=:userId and to_char(paid_date,'yyyy-MM-dd')>=:startDate and to_char(paid_date,'yyyy-MM-dd')<=:endDate \r\n"
			+ "and deal_type!='EQUITY' and status='INITIATED'", nativeQuery = true)
	public List<Object[]> findInterestDetailsBasedOnDates(@Param("userId") Integer userId,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	@Query(value = "select sum(amount) from public.lenders_returns a join \r\n"
			+ "public.oxy_borrowers_deals_information b on a.deal_id=b.id where a.status='INITIATED'\r\n"
			+ "and a.user_id=:userId and b.deal_type='EQUITY'", nativeQuery = true)
	public Double findingSumOfEquityInterestEarned(@Param("userId") Integer userId);

	@Query(value = "select deal_id,amount,paid_date,deal_name from public.lenders_returns a join \r\n"
			+ "public.oxy_borrowers_deals_information b on a.deal_id=b.id\r\n"
			+ "where status='INITIATED' and user_id=:userId and deal_type='EQUITY'", nativeQuery = true)
	public List<Object[]> findingListOfEquityInterests(@Param("userId") Integer userId);

	@Query(value = "select deal_id,amount,paid_date from public.lenders_returns where status='INITIATED'\r\n"
			+ "and user_id=:lenderId and deal_id=:dealId", nativeQuery = true)
	public List<Object[]> findingListOfEquityInterestDetails(@Param("lenderId") Integer lenderId,
			@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type=:amountType and user_id=:userId and amount_type!='REJECTED' and deal_id=:dealId and (paid_date>=  To_date(:fromDate,'dd-MM-yyyy') and  paid_date< To_date(:toDate,'dd-MM-yyyy'))", nativeQuery = true)
	public Double getSumOfAmountReturnedInGivenDates(@Param("userId") Integer userId,
			@Param("amountType") String amountType, @Param("dealId") Integer dealId, @Param("fromDate") String fromDate,
			@Param("toDate") String toDate);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED' and user_id=:userId and deal_id=:dealId and (paid_date>=  To_date(:fromDate,'dd-MM-yyyy') and  paid_date< To_date(:toDate,'dd-MM-yyyy'))", nativeQuery = true)
	public Double getTotalWithdrawalValueInGivenDates(@Param("userId") Integer userId, @Param("dealId") Integer dealId,
			@Param("fromDate") String fromDate, @Param("toDate") String toDate);

	@Query(value = "select sum(amount) from public.lenders_returns where amount_type='LENDERWITHDRAW' and (status='APPROVED' OR	status='H2HAPPROVAL') and user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public Double findingSumOfWithdrawApproved(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lenders_returns where  amount_type='LENDERWITHDRAW' and status='REQUESTED' and deal_id=:dealId", nativeQuery = true)
	public List<LendersReturns> getLenderWithdrawRequestedList(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED' and user_id=:userId and deal_id=:dealId", nativeQuery = true)
	public Double lenderWithdrawStatusBasedSumValue(@Param("userId") Integer userId, @Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lenders_returns where (amount_type='LENDERINTEREST' OR amount_type='LENDERPRINCIPAL' OR amount_type='PRINCIPALINTEREST') and user_id=:userId limit 1", nativeQuery = true)
	public LendersReturns previousInterestAndPrincipalStatus(@Param("userId") Integer userId);

	@Query(value = "select * from public.lenders_returns where user_id=:userId and amount_type='LENDERWITHDRAW' and status!='INITIATED' order by id desc", nativeQuery = true)
	public List<LendersReturns> findingListOfWithdrawAndStatusNotInitiated(@Param("userId") Integer userId);

	@Query(value = "SELECT * FROM public.lenders_returns where user_id=:userId and deal_id=:dealId and to_char(paid_date, 'mm/yyyy')=:emiReceivingDate and (amount_type='LENDERINTEREST' OR amount_type='PRINCIPALINTEREST' OR amount_type='WITHDRAWALINTEREST') order by paid_date desc limit 1", nativeQuery = true)
	public LendersReturns uniqueLenderReturns(@Param("userId") Integer userId, @Param("dealId") Integer dealId,
			@Param("emiReceivingDate") String emiReceivingDate);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where deal_id=:dealId and  to_char(paid_date,'dd-mm-yyyy')=:paidDate and amount_type=:amountType", nativeQuery = true)
	public Double sumOfAmountReturnedToDeal(@Param("dealId") Integer dealId, @Param("paidDate") String paidDate,
			@Param("amountType") String amountType);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED' and deal_id=:dealId", nativeQuery = true)
	public Double lenderWithdrawSumValue(@Param("dealId") Integer dealId);

	@Query(value = "SELECT * FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='REQUESTED' ", nativeQuery = true)
	public List<LendersReturns> getListOfLendersReturnsRequested();

	@Query(value = "select * from public.lenders_returns where user_id=:userId and deal_id=:dealId and (amount_type='LENDERWITHDRAW' OR amount_type='LENDERPRINCIPAL') and status='INITIATED' order by id desc", nativeQuery = true)
	public LendersReturns findPrincipalReturnStatusInitiated(@Param("userId") Integer userId,
			@Param("dealId") Integer dealId);

	@Query(value = "select sum(amount) from public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED'  and date(paid_date)=current_date", nativeQuery = true)

	public Double lenderWithdrawSumValueDaily();

	@Query(value = "select sum(amount) from public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED' and deal_id=:dealId and date(paid_date)=current_date", nativeQuery = true)

	public Double lenderWithdrawSumValueWithDealId(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED' and  paid_date >= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)

	public Double lenderWithdrawSumValueWeekly();

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED' and deal_id=:dealId and  paid_date >= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)

	public Double lenderWithdrawSumValueWeekly(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED'  and DATE(paid_date)>=:startDate and DATE(paid_date)<=:endDate", nativeQuery = true)

	public Double lenderWithdrawSumValueWithInput(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED'  and deal_id=:dealId  and DATE(paid_date)>=:startDate and DATE(paid_date)<=:endDate", nativeQuery = true)

	public Double finByWithDrawValueBasedOnDealId(@Param("dealId") Integer dealId, @Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED'  and date_trunc('month', paid_date) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)
	public Double lenderWithdrawSumValueMonthlyData();

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where amount_type='LENDERWITHDRAW' and status='INITIATED' and deal_id=:dealId and date_trunc('month', paid_date) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)

	public Double lenderWithdrawSumValueMonthlyDataWithDealId(@Param("dealId") Integer dealId);

	@Query(value = "SELECT SUM(amount)\r\n" + "FROM public.lenders_returns\r\n"
			+ "WHERE (amount_type = 'LENDERWITHDRAW' OR amount_type = 'PRINCIPALINTEREST')\r\n"
			+ "  AND status = 'INITIATED'\r\n" + "  AND user_id = :userId\r\n"
			+ "  AND DATE(paid_date) >= DATE(:fromDate) AND DATE(paid_date) <= DATE(:toDate)\r\n", nativeQuery = true)
	public Double getSumOfReturnAmountByUseId(@Param("userId") Integer userId, @Param("fromDate") String fromDate,
			@Param("toDate") String toDate);

	@Query(value = "SELECT a.id, a.user_id, a.amount, a.deal_id, date(a.requested_on) , a.status "
			+ "FROM public.lenders_returns a " + "JOIN public.user b ON a.user_id = b.id "
			+ "WHERE a.amount_type = 'LENDERWITHDRAW' "
			+ "    AND (a.status = 'REQUESTED' OR a.status = 'H2HAPPROVAL' OR a.status = 'APPROVED') "
			+ "    AND b.test_user = true " + "ORDER BY a.id DESC "
			+ "LIMIT :pageSize OFFSET :pageNo", nativeQuery = true)
	public List<Object[]> getTestLendersWithdrawRequests(@Param("pageSize") Integer pageSize,
			@Param("pageNo") Integer pageNo);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where deal_id=:dealId and (amount_type='LENDERINTEREST' OR amount_type='WITHDRAWALINTEREST' OR amount_type='PRINCIPALINTEREST') and   status='INITIATED' and date(created_on)=current_date", nativeQuery = true)

	public Double findByInterestAmount(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where  status='INITIATED' and (amount_type='LENDERINTEREST' OR amount_type='WITHDRAWALINTEREST' OR amount_type='PRINCIPALINTEREST')  and date(created_on)=current_date", nativeQuery = true)

	public Double findByInterestAmount();

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where deal_id=:dealId and  status='INITIATED' and (amount_type='LENDERINTEREST' OR amount_type='WITHDRAWALINTEREST' OR amount_type='PRINCIPALINTEREST') and created_on >= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)

	public Double findByInterestAmountWeeklyData(@Param("dealId") Integer dealId);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where  status='INITIATED' and (amount_type='LENDERINTEREST' OR amount_type='WITHDRAWALINTEREST' OR amount_type='PRINCIPALINTEREST') and created_on >= CURRENT_DATE - INTERVAL '6 days'", nativeQuery = true)

	public Double findByInterestAmountWeeklyData();

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where  status='INITIATED' and (amount_type='LENDERINTEREST' OR amount_type='WITHDRAWALINTEREST' OR amount_type='PRINCIPALINTEREST') and date_trunc('month', created_on) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)

	public Double findByInterestAmountMonthlyData();

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where deal_id=:dealId and  status='INITIATED' and (amount_type='LENDERINTEREST' OR amount_type='WITHDRAWALINTEREST' OR amount_type='PRINCIPALINTEREST') and date_trunc('month', created_on) = date_trunc('month', CURRENT_DATE)", nativeQuery = true)

	public Double findByInterestAmountMonthlyDataBasedOnDealId(@Param("dealId") Integer dealId);

	@Query(value = "select * from public.lenders_returns where deal_id = :dealId and user_id = :lenderId and amount_type = 'LENDERINTEREST' and DATE(actual_date) = :readingDate", nativeQuery = true)
	public LendersReturns findByDealIdAndPaymentDate(@Param("dealId") Integer dealId,
			@Param("lenderId") Integer lenderId, @Param("readingDate") Date readingDate);

	@Query(value = "SELECT distinct deal_id FROM public.lenders_returns where status='INITIATED' and amount_type='LENDERINTEREST'", nativeQuery = true)

	public List<Integer> findByInterestAmountLenderAmount();

	//@Query(value = "SELECT sum(amount) FROM public.lenders_returns where deal_id=:dealId and  amount_type='LENDERINTEREST' and status='INITIATED' and date(created_on)>=:startDate and date(created_on)<=:endDate", nativeQuery = true)
//	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where deal_id=:dealId and  amount_type='LENDERINTEREST' and   status='INITIATED' and date(created_on)>=:startDate and date(created_on)<=:endDate", nativeQuery = true)
//
//	public Double findByInterestAmountLenderInterest(@Param("dealId") Integer dealId,
//			@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where deal_id=CAST(:dealId AS INTEGER) and amount_type='LENDERINTEREST' and status='INITIATED' and date(created_on)>=:startDate and date(created_on)<=:endDate", nativeQuery = true)
	public Double findByInterestAmountLenderInterest(@Param("dealId") Integer dealId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	@Query(value = "SELECT distinct deal_id FROM public.lenders_returns where  status='INITIATED' and amount_type='WITHDRAWALINTEREST' ", nativeQuery = true)

	public List<Integer> findByInterestAmountCurrentDateWithdDrawalInterest();

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where deal_id=:dealId and  amount_type='WITHDRAWALINTEREST' and   status='INITIATED' and date(created_on)>=:startDate and date(created_on)<=:endDate", nativeQuery = true)

	public Double findByInterestAmountWithDrawalInterest(@Param("dealId") Integer dealId,
			@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT distinct deal_id FROM public.lenders_returns where  status='INITIATED' and amount_type='PRINCIPALINTEREST' ", nativeQuery = true)

	public List<Integer> findByInterestAmountCurrentDatePrincipalInterest();

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where deal_id=:dealId and  amount_type='PRINCIPALINTEREST' and   status='INITIATED' and date(created_on)>=:startDate and date(created_on)<=:endDate", nativeQuery = true)

	public Double findByInterestAmountPrincipalInterest(@Param("dealId") Integer dealId,
			@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "SELECT distinct deal_id FROM public.lenders_returns where  status='INITIATED' and (amount_type='LENDERPRINCIPAL' OR amount_type='PRINCIPALTOWALLET') ", nativeQuery = true)

	public List<Integer> findByInterestAmountCurrentDatePrincipal();

	@Query(value = "SELECT sum(amount) FROM public.lenders_returns where deal_id=:dealId and (amount_type='LENDERPRINCIPAL' OR amount_type='PRINCIPALTOWALLET') and   status='INITIATED' and date(created_on)>=:startDate and date(created_on)<=:endDate", nativeQuery = true)

	public Double findByPrincipalAmountCurrentDAte(@Param("dealId") Integer dealId, @Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	
	@Query(value = "select sum(amount) from public.lenders_returns where deal_id =:dealId and amount_type ='LENDERINTEREST' and Date(created_on) >=:startDate\r\n"
			+ "	and Date(created_on)<=:endDate", nativeQuery = true)

	public Double findByInterestAmountLenderInterestData(@Param("dealId") int dealId,  @Param("startDate") Date startDate,@Param("endDate") Date endDate);
}
