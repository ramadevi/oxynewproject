#COPY (SELECT CONCAT('mv aadhaar/"', file_name, '"'),CONCAT('oxyloans_documents/', user_id, '/"AADHAR_', file_name, '"') FROM user_document_status where document_sub_type = 'AADHAR') TO '/tmp/aadhar.csv' DELIMITER ',';
#COPY (SELECT CONCAT('mv pancardno/"', file_name, '"'),CONCAT('oxyloans_documents/', user_id, '/"PAN_', file_name, '"') FROM user_document_status where document_sub_type = 'PAN') TO '/tmp/pan.csv' DELIMITER ',';
#COPY (SELECT CONCAT('mv passport/"', file_name, '"'),CONCAT('oxyloans_documents/', user_id, '/"PASSPORT_', file_name, '"') FROM user_document_status where document_sub_type = 'PASSPORT') TO '/tmp/passport.csv' DELIMITER ',';
#COPY (SELECT CONCAT('mv avatar/"', file_name, '"'),CONCAT('oxyloans_documents/', user_id, '/"PROFILEPIC_', file_name, '"') FROM user_document_status where document_sub_type = 'PROFILEPIC') TO '/tmp/avatar.csv' DELIMITER ',';



sed -i '' 's/^/mkdir -p oxyloans_documents\//g' users.csv
mv users.csv users.sh
sh users.sh

sed -i '' 's/^/mv aadhaar\/"/g' aadhar.csv
sed -i '' 's/,/" oxyloans_documents\//' aadhar.csv
sed -i '' 's/$/\//' aadhar.csv
mv aadhar.csv aadhar.sh
sh aadhar.sh

sed -i '' 's/^/mv pancardno\/"/g' pan.csv
sed -i '' 's/,/" oxyloans_documents\//' pan.csv
sed -i '' 's/$/\//' pan.csv
mv pan.csv pan.sh
sh pan.sh

sed -i '' 's/^/mv passport\/"/g' passport.csv
sed -i '' 's/,/" oxyloans_documents\//' passport.csv
sed -i '' 's/$/\//' passport.csv
mv passport.csv passport.sh
sh passport.sh

sed -i '' 's/^/mv passport\/"/g' avatar.csv
sed -i '' 's/,/" oxyloans_documents\//' avatar.csv
sed -i '' 's/$/\//' avatar.csv
mv avatar.csv avatar.sh
sh avatar.sh

cd agreements
ls | xargs -I {} mv {} Agreement_{}

