-- psql -U damodharreddy -d oxyloans -a -f ~/personal/codebase/oxyloans/oxyloans.com/migration.sql 

TRUNCATE public."user" CASCADE;

TRUNCATE public."user_personal_details";

TRUNCATE public."user_document_status";

TRUNCATE public."user_address_details";

TRUNCATE public."user_bank_details";

TRUNCATE public."user_financial_details";

TRUNCATE public."user_professional_details";

TRUNCATE public."oxy_loan_request";

TRUNCATE public."oxy_loan";

TRUNCATE public."loan_emi_card";

TRUNCATE public."esign_transactions";

TRUNCATE public."lender_borrower_conversation";

INSERT INTO public."user"
(id, email, email_sent, email_verified, unique_number, mobile_number, primary_type, status, email_sent_on, mobile_otp_session, registered_on, modified_on, old_applicatoin_user) 
 (SELECT id, email, true, "emailConfirmed",  "uniqueNumber", id, 
   CASE WHEN primaryrole = '{Borrower}' THEN 'BORROWER' 
        WHEN primaryrole = '{Lender}' THEN 'LENDER' 
        WHEN "uniqueNumber" IS NOT NULL AND "uniqueNumber" LIKE 'BR%' THEN 'BORROWER'
        WHEN "uniqueNumber" IS NOT NULL AND "uniqueNumber" LIKE 'LR%' THEN 'LENDER'
   END,
   CASE WHEN "emailConfirmed" = 't' THEN 'EMAIL_VERIFIED'
 		ELSE 'REGISTERED'
   END,
   now(),
   '',
   now(),
   now(),
   true
 FROM livedata_new.users 
 WHERE (primaryrole = '{Borrower}' 
  OR primaryrole = '{Lender}')
  AND email not in 
	(SELECT email from (SELECT email, count(*) from livedata_new.users group by email having count(*) > 1)AS x));


INSERT INTO public."user"
(id, email, email_sent, email_verified, unique_number, mobile_number, primary_type, status, email_sent_on, mobile_otp_session, registered_on, modified_on, old_applicatoin_user) 
 (SELECT id, email, true, "emailConfirmed",  "uniqueNumber", id, 
   CASE WHEN primaryrole = '{Borrower}' THEN 'BORROWER' 
        WHEN primaryrole = '{Lender}' THEN 'LENDER' 
        WHEN "uniqueNumber" IS NOT NULL AND "uniqueNumber" LIKE 'BR%' THEN 'BORROWER'
        WHEN "uniqueNumber" IS NOT NULL AND "uniqueNumber" LIKE 'LR%' THEN 'LENDER'
   END,
   CASE WHEN "emailConfirmed" = 't' THEN 'EMAIL_VERIFIED'
 		ELSE 'REGISTERED'
   END,
   now(),
   '',
   now(),
   now(),
   true
 FROM livedata_new.users 	
WHERE services!='{SalesAgent}' AND services != '{SuperAdmin}'
 AND (primaryrole != '{Borrower}' 
  AND primaryrole != '{Lender}')
  AND "uniqueNumber" IS NOT NULL and "uniqueNumber" != ''
  AND email not in 
	(SELECT email from (SELECT email, count(*) from livedata_new.users group by email having count(*) > 1)AS x));

INSERT INTO public."user"
(email, mobile_number, password, email_token, email_sent, email_verified, email_sent_on, email_verified_on, mobile_otp_session, mobile_otp_sent, mobile_number_verified, status, primary_type, registered_on, modified_on, salt, unique_number)
 VALUES ('admin@oxyloans.com','1234567890', '120F4031E31D8B3B0D217F09B2A7C1DA2D565C70B2C0E9D77FB302DC8201E67E12BD8853FA49F178979C8B6BC971635FC1A166BEF60B5A0886FCACD86BA71AB9', '9f67479bf8be4dd0b55e5b1c80211911', 't', 't', now(),  now(), '4ee14db5-1428-457f-92f8-42687458de6f', 'f', 't', 'ACTIVE', 'ADMIN', now(), now(), '0d002213-4e6e-46e4-a924-fe548e0bb4c7e6d702f5-7','ADDDD');

UPDATE public."user" 
SET password = '120F4031E31D8B3B0D217F09B2A7C1DA2D565C70B2C0E9D77FB302DC8201E67E12BD8853FA49F178979C8B6BC971635FC1A166BEF60B5A0886FCACD86BA71AB9',
	salt = '0d002213-4e6e-46e4-a924-fe548e0bb4c7e6d702f5-7',
	primary_type = 'ADMIN'
where email = 'admin@oxyloans.com';

UPDATE public."user"
SET registered_on = d.createddatetime,
	modified_on = d.modifieddatetime
FROM livedata_new.userdetails d
WHERE public."user".id = d.userid;

-- users.firstname to user_personal_details 
INSERT INTO public."user_personal_details"
(user_id, first_name, last_name, father_name, dob, gender, marital_status, created_at, modified_at, address)
	(SELECT l.userid, l.firstName, l.lastName, 'olduserfathername', dateofbirth, 
		CASE 
			WHEN gender IS NULL 
			THEN 'U' 
			ELSE gender
		END, maritalstatus, createddatetime, modifieddatetime, 'olduseraddress' 
	 FROM livedata_new.personaldetails l
	 INNER JOIN public."user" u 
	 ON u.id = l.userid
	);


-- user_document_status to documents
INSERT INTO public."user_document_status"
(user_id, document_type, document_sub_type, status, file_path, file_name)
	(SELECT d.userid, 'Kyc', 'AADHAR', 'ACCEPTED', CONCAT('S3://oxyloansv1/', d.userid, '/AADHAR_', 
		SPLIT_PART(d.aadhaardocumentname, '/', 3)),
		SPLIT_PART(d.aadhaardocumentname, '/', 3)
	 FROM livedata_new.documents d
	 INNER JOIN public."user" u 
	 ON u.id = d.userid
	 WHERE d.aadhaardocumentname is not null and d.aadhaardocumentname != '' 
	);

INSERT INTO public."user_document_status"
(user_id, document_type, document_sub_type, status, file_path, file_name)
	(SELECT d.userid, 'Kyc', 'PAN', 'ACCEPTED', CONCAT('S3://oxyloansv1/', d.userid, '/PAN_', 
		SPLIT_PART(d.pancarddocumentname, '/', 3)),
		SPLIT_PART(d.pancarddocumentname, '/', 3)
	 FROM livedata_new.documents d
	 INNER JOIN public."user" u 
	 ON u.id = d.userid
	 WHERE d.pancarddocumentname is not null and d.pancarddocumentname != '' 
	);
	
INSERT INTO public."user_document_status"
(user_id, document_type, document_sub_type, status, file_path, file_name)
	(SELECT d.userid, 'Kyc', 'PASSPORT', 'ACCEPTED', CONCAT('S3://oxyloansv1/', d.userid, '/PASSPORT_', 
		SPLIT_PART(d.passportdocumentname, '/', 3)),
		SPLIT_PART(d.passportdocumentname, '/', 3)
	 FROM livedata_new.documents d
	 INNER JOIN public."user" u 
	 ON u.id = d.userid
	 WHERE d.passportdocumentname is not null and d.passportdocumentname != '' 
	);
	
INSERT INTO public."user_document_status"
(user_id, document_type, document_sub_type, status, file_path, file_name)
	(SELECT d.userid, 'Kyc', 'BANKSTATEMENT', 'ACCEPTED', CONCAT('S3://oxyloansv1/', d.userid, '/BANKSTATEMENT_', 
		SPLIT_PART(d.bankdocumentname, '/', 3)),
		SPLIT_PART(d.bankdocumentname, '/', 3)
	 FROM livedata_new.documents d
	 INNER JOIN public."user" u 
	 ON u.id = d.userid
	 WHERE d.bankdocumentname is not null and d.bankdocumentname != '' 
	);
	
INSERT INTO public."user_document_status"
(user_id, document_type, document_sub_type, status, file_path, file_name)
	(SELECT d.userid, 'Kyc', 'PROFILEPIC', 'ACCEPTED', CONCAT('S3://oxyloansv1/', d.userid, '/PROFILEPIC_', 
		SPLIT_PART(d.avatar, '/', 2)),
		SPLIT_PART(d.avatar, '/', 2)
	 FROM livedata_new.personaldetails d
	 INNER JOIN public."user" u 
	 ON u.id = d.userid
	 WHERE d.avatar is not null
	);

INSERT INTO public."user_address_details"
(user_id, type, house_number, street, area, city)
	(SELECT a.userid, 'PERMANENT', a.housenumber, a.streetname, a.area, a.villagename
	 FROM livedata_new.addresses a
	 INNER JOIN public."user" u 
	 ON u.id = a.userid
	 WHERE a.addresstype = 'Permanent' 
	);
	
INSERT INTO public."user_address_details"
(user_id, type, house_number, street, area, city)
	(SELECT a.userid, 'PRESENT', a.housenumber, a.streetname, a.area, a.villagename
	 FROM livedata_new.addresses a
	 INNER JOIN public."user" u 
	 ON u.id = a.userid
	 WHERE a.addresstype = 'Present' 
	);
	
DELETE FROM livedata_new.addresses a
USING livedata_new.addresses b 
WHERE a.id < b.id AND a.userid = b.userid AND a.addresstype = b.addresstype;

INSERT INTO public."user_address_details"
(user_id, type, house_number, street, area, city)
	(SELECT a.userid, 'OFFICE', a.housenumber, a.streetname, a.area, a.villagename
	 FROM livedata_new.addresses a
	 INNER JOIN public."user" u 
	 ON u.id = a.userid
	 WHERE a.addresstype = 'Office' 
	);

DELETE FROM livedata_new.financialinformation a
USING livedata_new.financialinformation b 
WHERE a.id < b.id AND a.userid = b.userid;

INSERT INTO public."user_bank_details"
(user_id, account_number, bank_name, branch_name, account_type, ifsc_code, address)
	(SELECT f.userid, f.bankaccountnumber, f.bankname, f.bankbranch, f.accounttype, f.ifsccode, f.bankaddress
	 FROM livedata_new.financialinformation f
	 INNER JOIN public."user" u 
	 ON u.id = f.userid
	);

INSERT INTO public."user_financial_details"
(user_id, monthly_emi, credit_amount, existing_loan_amount, credit_cards_repayment_amount, other_sources_income, net_monthly_income, avg_monthly_expenses)
	(SELECT f.userid, f.totalmonthlyemi, f.outstandingamountcreditcard, f.outstandingloanamount, 0, 0, f.netmonthlyincome, f.avgmonthlyexpenses
	 FROM livedata_new.financialinformation f
	 INNER JOIN public."user" u 
	 ON u.id = f.userid
	);

INSERT INTO public."user_professional_details"
(user_id, employment, company_name, designation, no_of_jobs_changed, work_experience, description, highest_qualification, field_of_study, college, year_of_passing)
	(SELECT p.userid, p.employmenttype, p.companyname, p.designation, p.numberofjobchanges, p.workingexperience, p.description, 'UPDATEQUALIFICATION','UPDATEFIELDSTUDY', 'UPDATECOLLEGE', 1800
	 FROM livedata_new.professionaldetails p
	 INNER JOIN public."user" u 
	 ON u.id = p.userid
	);
	
UPDATE public."user_professional_details"
SET highest_qualification = e.degree,
	field_of_study = e.fieldofstuday,
	college = e.school,
	year_of_passing = e.endyear	
FROM livedata_new.educationdetails e
WHERE public."user_professional_details".user_id = e.userid
	AND isactive = 't';

UPDATE public."user_professional_details"
SET employment = 'PUBLIC' 
WHERE employment = 'Public Sector';

UPDATE public."user_professional_details"
SET employment = 'PRIVATE' 
WHERE employment = 'Private';

UPDATE public."user_professional_details"
SET employment = 'GOVERMENT' 
WHERE employment = 'Government';

DELETE FROM public."user_professional_details" WHERE employment = '';

INSERT INTO public."oxy_loan_request" 
(user_id, user_primary_type, loan_request_amount, rate_of_interest, loan_requested_date, expected_date, disbursment_amount, loan_status, loan_request_id, duration, repayment_method)
	(SELECT id, primary_type, 0, 24, '1900-01-01', '1900-01-01', 0, 'Requested', 'REPLACEXXX', 24, 'PI'
		FROM public."user");
		
ALTER TABLE public."oxy_loan" ADD COLUMN old_loan_id INT;

ALTER TABLE public."oxy_loan" ADD COLUMN old_offer_id INT;

ALTER TABLE public."oxy_loan_request" ADD COLUMN old_offer_id INT;

ALTER TABLE public."oxy_loan_request" ADD COLUMN old_offer_to_user_id INT;

UPDATE public."oxy_loan_request" olr
SET loan_request_amount = l.amount,
	rate_of_interest = l.interestrate
FROM (SELECT lenderid, sum(amount) as amount, max(interestrate) as interestrate, max(loanduration) as loanduration FROM livedata_new."loans" group by lenderid) AS l
WHERE olr.user_id = l.lenderid;

UPDATE public."oxy_loan_request" olr
SET loan_request_amount = l.amount,
	rate_of_interest = l.interestrate,
	duration = l.loanduration
FROM (SELECT borrowerid, sum(amount) as amount, max(interestrate) as interestrate, max(loanduration) as loanduration FROM livedata_new."loans" group by borrowerid) AS l
WHERE olr.user_id = l.borrowerid;

ALTER TABLE public."oxy_loan_request" ALTER COLUMN comments SET TYPE VARCHAR(1000);

INSERT INTO public."oxy_loan_request"
(user_id, user_primary_type, loan_request_amount, rate_of_interest, loan_requested_date, expected_date, disbursment_amount, loan_status, loan_request_id, duration, repayment_method, comments, loan_purpose, parent_request_id, old_offer_id)
	(SELECT offeredfrom, 
			CASE WHEN offertype = 'LendOffer' THEN 'LENDER'
				 ELSE 'BORROWER'
			END, 
			amount, 
			interestrate,
			createddatetime,
			date,
			amount,
			CASE WHEN loanstatus = 'SoftAgreementGenerated' THEN 'Accepted'
				 WHEN loanstatus = 'RequestAccepted' THEN 'Conversation'
				 WHEN loanstatus = 'OfferAccepted' THEN 'Accepted'
				 WHEN loanstatus = 'OfferRejected' THEN 'Rejected'
				 WHEN loanstatus = 'RequestRejected' THEN 'Rejected'
				 ELSE 'Requested'
			END,
			applicationid,
			loanduration,
			CASE WHEN repaymentmethod = 'InterestPrincipalLater' THEN 'I'
				 ELSE 'PI'
			END,
			description,
			loanpurpose,
			-1000,
			id
		FROM livedata_new."loanoffers" l);
		
UPDATE public.oxy_loan_request 
SET old_offer_to_user_id = l.offeredto
FROM livedata_new.loanoffers l
WHERE parent_request_id IS NOT NULL
AND user_id = l.offeredfrom;
		
UPDATE public.oxy_loan_request AS x
SET parent_request_id = o.id
FROM public.oxy_loan_request o
WHERE x.parent_request_id is not null
AND x.old_offer_to_user_id = o.user_id;

UPDATE public.oxy_loan_request AS x
SET expected_date = o.date
FROM livedata_new.loanoffers o
WHERE x.parent_request_id is null
AND user_id = o.offeredfrom;

ALTER TABLE public.oxy_loan ALTER COLUMN loan_active_date TYPE TIMESTAMP;

INSERT INTO public.oxy_loan
(id, loan_id, oxy_loan_request_id, oxy_loan_respond_id, lender_user_id, borrower_user_id, disbursment_amount, duration, rate_of_interest, loan_status, loan_active_date, old_loan_id, old_offer_id, loan_accepted_date, lender_disbursed_date, borrower_disbursed_date)
	(SELECT id, 
			applicationnumber, 
			-1 * id,
			-1,
			lenderid, 
			borrowerid, 
			amount, 
			loanduration, 
			interestrate, 
			'Active', 
			date,
			id, 
			offerid,
			'1900-01-01',
			'1900-01-01',
			'1900-01-01'
	 FROM livedata_new.loans);
	 
DELETE FROM public.oxy_loan a
USING public.oxy_loan b 
WHERE a.old_loan_id < b.old_loan_id AND a.old_offer_id = b.old_offer_id;
	 
UPDATE public.oxy_loan
SET oxy_loan_respond_id = o.id,
	oxy_loan_request_id = o.parent_request_id,
	repayment_method = (CASE WHEN o.repayment_method = 'InterestPrincipalLater' 
							 THEN 'I' 
							 ELSE 'PI' 
						END)
FROM public.oxy_loan_request o
WHERE oxy_loan.old_offer_id = o.old_offer_id;

UPDATE public.oxy_loan
SET lender_esigned = s.islendersigned,
	borrower_esigned = s.isborrowersigned
FROM livedata_new.signedaggrements s
WHERE s.offerid = oxy_loan.old_offer_id;

UPDATE public.oxy_loan_request
SET loan_requested_date = d.registered_on
FROM public."user" d
WHERE d.id = oxy_loan_request.user_id
AND oxy_loan_request.parent_request_id is null;

UPDATE public.oxy_loan_request
SET loan_request_id = (CASE WHEN oxy_loan_request.user_primary_type = 'LENDER' 
							 THEN CONCAT('APLR', oxy_loan_request.id)
							 ELSE CONCAT('APBR', oxy_loan_request.id)
						END)
WHERE loan_request_id = 'REPLACEXXX';

UPDATE public.user_personal_details
SET father_name = d.fathername
FROM livedata_new.personaldetails d
WHERE d.userid = user_personal_details.user_id;

ALTER TABLE livedata_new.contactdetails ADD COLUMN migrated BOOLEAN DEFAULT TRUE;

UPDATE livedata_new.contactdetails a
SET migrated = false
FROM livedata_new.contactdetails b 
WHERE a.id < b.id AND a.mobilenumber = b.mobilenumber;

ALTER TABLE public."user" ALTER COLUMN mobile_number SET DATA TYPE VARCHAR(29);

UPDATE public."user"
SET mobile_number = d.mobilenumber
FROM livedata_new.contactdetails d
WHERE d.userid = "user".id AND d.migrated = true;

UPDATE public.oxy_loan_request 
SET loan_id = o.loan_id
FROM public.oxy_loan o
WHERE oxy_loan_request.old_offer_id = o.old_offer_id; 

SELECT setval('oxy_loan_id_seq', (SELECT max(id) FROM public."user"));

SELECT setval('esign_transactions_id_seq', (SELECT max(id) FROM public.esign_transactions));

SELECT setval('oxy_loan_id_seq', (SELECT max(id) FROM public.oxy_loan));

SELECT setval('oxy_loan_request_id_seq', (SELECT max(id) FROM public.oxy_loan_request));

SELECT setval('user_address_details_id_seq', (SELECT max(id) FROM public.user_address_details));

SELECT setval('user_document_status_id_seq', (SELECT max(id) FROM public.user_document_status));

SELECT setval('loan_emi_card_id_seq', (SELECT max(id) FROM public.loan_emi_card));


UPDATE public."user" 
SET status = 'ACTIVE',
	email_verified = 't'
FROM public.oxy_loan_request o
WHERE "user".id = o.user_id
AND o.parent_request_id is null
AND o.loan_request_amount > 0;